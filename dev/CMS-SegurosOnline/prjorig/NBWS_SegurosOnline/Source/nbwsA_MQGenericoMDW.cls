VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_MQGenericoMDW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'******************************************************************************
'COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
'LIMITED 2007. ALL RIGHTS RESERVED

'This software is only to be used for the purpose for which it has been provided.
'No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
'system or translated in any human or computer language in any way or for any other
'purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
'Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
'offence, which can result in heavy fines and payment of substantial damages
'******************************************************************************
'Nombre del Modulo: lbaw_MQMW
'Fecha de Creaci�n: 23/07/2007
'PPcR: xxxxxxx -x
'Desarrollador: Gabriel E. D'Agnone
'Descripci�n:  Envia xml soap hacia Middleware y retorna PDF via MQ

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog
Private mvarConsultaRealizada       As String
Private mvarCancelacionManual       As Boolean

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

Const gcteQueueManager       As String = "//QUEUEMANAGER"
Const gctePutQueue           As String = "//PUTQUEUE"
Const gcteGetQueue           As String = "//GETQUEUE"
Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"

Const gcteClassMQConnection As String = "WD.Frame2MQ"

Const mcteClassName                 As String = "nbwsA_Transacciones.nbwsA_MQGenericoMDW"
Const mcteSubDirName                As String = "DefinicionesMDW"
Const mcteLogPath                   As String = "LogMDW"
'
Public Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal pvarContextInfo As String) As Long

Const wcteFnName       As String = "IAction_Execute"

Dim wvarGMOWaitInterval         As Long
Dim wvarStep                    As Long
Dim wvarx                       As Long
Dim wvarNroArch                 As Long

Dim warrayDatos()               As Byte

Dim wvarDatosPDF                As String
Dim wvarRaiz                    As String
Dim wvarInputMQ                 As String
Dim wvarError                   As String
Dim wvarTextoError              As String
Dim wvarUsuario                 As String
Dim wvarDefinitionFile          As String
Dim wvarConfFileName            As String
Dim wvarTimeStamp               As String
Dim wvarCDATA                   As String
Dim wvarLoguear                 As String
Dim wvarNomArchivo              As String
Dim wvarTextoALoguear           As String
Dim wvarExisteArchivo           As String
Dim wvarHoraInicio              As String
Dim wvarRespuestaFiltrada       As String


Dim wvarPasada1                 As String
Dim wvarPasada2                 As String

Dim wvarTiempo                  As Single

Dim wobjXMLDefinition           As MSXML2.DOMDocument
Dim wobjXMLConfig               As MSXML2.DOMDocument
Dim wobjXMLRequest              As MSXML2.DOMDocument
Dim wobjXMLInputMQ              As MSXML2.DOMDocument
Dim wobjXMLOutputMQ             As MSXML2.DOMDocument
Dim wvarXMLPDF                  As MSXML2.DOMDocument
Dim wobjXMLAux                  As MSXML2.DOMDocument

Dim wobjNodoBase64              As MSXML2.IXMLDOMElement
Dim wobjNodo                    As MSXML2.IXMLDOMElement
Dim wobjNodoAux                 As MSXML2.IXMLDOMElement

Dim wobjCdataNodo               As MSXML2.IXMLDOMCDATASection

Dim wobjNodosEntrada            As MSXML2.IXMLDOMNodeList

Dim wobjXMLSobre                As MSXML2.IXMLDOMElement
Dim wobjNodoEntrada             As MSXML2.IXMLDOMElement

Dim wvarFaultCode               As String
Dim wvarNodoPDF                 As String
Dim wvarNodoRaiz                As String
Dim RespuestaMQ                 As String
Dim wvarMQError                 As Long
Dim wobjFrame2MQ                As HSBCInterfaces.IAction

On Error GoTo ErrorHandler
wvarHoraInicio = Now()
wvarTiempo = Timer

wvarStep = 10
    
    ' Crea Objetos XMLDom -----------------------------
    
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLDefinition = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLInputMQ = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    '-----------------------------------------------------
    
    wvarStep = 20
    ' Carga XML del Request ASP---------------------------
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '-----------------------------------------------------
    
    ' Carga Nombre de Archivo de Definici�n------------
    If Not wobjXMLRequest.selectSingleNode("//DEFINICION") Is Nothing Then
        wvarDefinitionFile = wobjXMLRequest.selectSingleNode("//DEFINICION").Text
    Else
        wvarTextoError = " No se encontr� nodo DEFINICION en Request o XML mal formado"
        GoTo ManejoError
    End If
    '----------------------------------------------------
    
    ' Carga Archivo de Definici�n----------------------------
    wobjXMLDefinition.async = False
    wobjXMLDefinition.Load App.Path & "\" & mcteSubDirName & "\" & wvarDefinitionFile
    wvarStep = 30
    
    ' Carga Nombre de Archivo de Definici�n MQ --------------------
    wvarConfFileName = wobjXMLDefinition.selectSingleNode("//DEFINICION/MQCONFIGFILE").Text
    wvarStep = 40
    
    ' Carga Tiempo de time-out MQ------------------------------
    wvarGMOWaitInterval = CLng(wobjXMLDefinition.selectSingleNode("//DEFINICION/TIMEOUT").Text)
    wvarStep = 50
    
    ' Carga Archivo de Configuraci�n MQ --------------------------
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & wvarConfFileName
    wobjXMLConfig.selectSingleNode(gcteGMOWaitInterval).Text = wvarGMOWaitInterval
    
    ' Carga nombre Archivo de Logueo
    wvarLoguear = "NO"
    
    If Not wobjXMLDefinition.selectSingleNode("//DEFINICION/GENERARLOG") Is Nothing Then
        wvarLoguear = "SI"
        If Not wobjXMLDefinition.selectSingleNode("//DEFINICION/GENERARLOG").Attributes.getNamedItem("LogFile") Is Nothing Then
            wvarNomArchivo = wobjXMLDefinition.selectSingleNode("//DEFINICION/GENERARLOG").Attributes.getNamedItem("LogFile").Text
        End If
    End If
            
    ' Carga Nodo Sobre de la Definici�n--------------------------
    wvarStep = 60
    Set wobjXMLSobre = wobjXMLDefinition.selectSingleNode("//XMLDEFINICION").firstChild
    
    wvarStep = 70
    ' Carga el Sobre en la variable de Input a MQ---------------------
    wvarInputMQ = wobjXMLSobre.xml
    
    wvarStep = 80
    ' carga Envelope de xml definicion
    wobjXMLInputMQ.async = False
    wobjXMLInputMQ.loadXML wvarInputMQ
    wobjXMLInputMQ.resolveExternals = True
    
    ' Carga los Parametos de Entrada desde la Definicion-------------
    
    Set wobjNodosEntrada = wobjXMLDefinition.selectNodes("//ENTRADA/PARAMETRO")
    
    wvarStep = 90
    
    ' Recorre los Parametros----------------------------------------
    
    wvarCDATA = "NO"
    
    For Each wobjNodoEntrada In wobjNodosEntrada
        
        'Que existan todos los nodos definidos, en el Request
        
        If (wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text) Is Nothing) And (wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing) Then
            
            wvarTextoError = " No se encontr� nodo " & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text & " en Request"
        
        GoTo ManejoError
        
        Else
            
            'Que Exista Nodo Raiz
            If UCase(wobjNodoEntrada.Attributes.getNamedItem("Tipo").Text) = "RAIZ" Then
                
                'MC - Agregado ----------------------------------------------------------
                If Not wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing Then
                
                    wvarNodoRaiz = wobjNodoEntrada.Attributes.getNamedItem("Default").Text
                
                Else
                
                    wvarNodoRaiz = wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).Text
                
                End If
                'MC - Fin Agregado ------------------------------------------------------
                
                If Not wobjNodoEntrada.Attributes.getNamedItem("CDATA") Is Nothing Then
                    
                    wvarCDATA = wobjNodoEntrada.Attributes.getNamedItem("CDATA").Text
                
                End If
    
            End If
            
        End If
        
    Next
    
    If wvarNodoRaiz <> "" Then
    
      For Each wobjNodoEntrada In wobjNodosEntrada
    
        Select Case UCase(wobjNodoEntrada.Attributes.getNamedItem("Tipo").Text)
            
            Case "BASE64"
                
                wvarNodoPDF = wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text
                
                wvarDatosPDF = "<?xml version=""1.0"" encoding=""ISO-8859-1""?>" & wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).firstChild.xml
                
                ' Redimensiona Array de Bytes para alojar datos a convertir a BASE64
                
                ReDim warrayDatos(Len(wvarDatosPDF))
    
                For wvarx = 1 To Len(wvarDatosPDF)
    
                    warrayDatos(wvarx - 1) = CByte(Asc(Mid(wvarDatosPDF, wvarx, 1)))
       
                Next wvarx
                
                Set wobjNodoBase64 = wobjXMLInputMQ.createElement(wvarNodoPDF)

                wobjXMLInputMQ.selectSingleNode("//" & wvarNodoRaiz).appendChild wobjNodoBase64
    
                wobjNodoBase64.setAttribute "xmlns:dt", "urn:schemas-microsoft-com:datatypes"
    
                wobjNodoBase64.dataType = "bin.base64"
    
                wobjNodoBase64.nodeTypedValue = warrayDatos
                
                Set wobjNodoBase64 = Nothing
                
            Case "TEXTO"
                
                wvarNodoPDF = wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text
                
                If Not wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing Then
                    wvarDatosPDF = wobjNodoEntrada.Attributes.getNamedItem("Default").Text
                Else
                    wvarDatosPDF = wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).Text
                End If
                
                Set wobjNodo = wobjXMLInputMQ.createElement(wvarNodoPDF)
                wobjXMLInputMQ.selectSingleNode("//" & wvarNodoRaiz).appendChild wobjNodo
                wobjNodo.nodeTypedValue = wvarDatosPDF
                Set wobjNodo = Nothing
                
            Case "XML"
            
                wvarNodoPDF = wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text
                
                'MHC: 16/01/2009 - Se corrige el agregado de XML al InputMQ
                'Se corrige para permitir el env�o de multiples archivos
                If Not wobjNodoEntrada.Attributes.getNamedItem("Default") Is Nothing Then
                    Set wobjNodo = wobjXMLInputMQ.createElement(wvarNodoPDF)
                        wobjNodo.Text = wobjNodoEntrada.Attributes.getNamedItem("Default").Text
                Else
                    Set wobjNodo = wobjXMLRequest.selectSingleNode("//" & wobjNodoEntrada.Attributes.getNamedItem("Nombre").Text).cloneNode(True)
                End If
                
                wobjXMLInputMQ.selectSingleNode("//" & wvarNodoRaiz).appendChild wobjNodo.cloneNode(True)
                Set wobjNodo = Nothing
                Set wobjXMLAux = Nothing
            Case Else
            
          End Select
      Next wobjNodoEntrada
    End If
    
    wvarStep = 100
   
    ' Reemplaza nodo Raiz por un CDATA
    If UCase(wvarCDATA) = "SI" Then
        wvarRaiz = wobjXMLInputMQ.selectSingleNode("//" & wvarNodoRaiz).xml
        Set wobjCdataNodo = wobjXMLInputMQ.createCDATASection(wvarRaiz)
        Set wobjNodo = wobjXMLInputMQ.selectSingleNode("//" & wvarNodoRaiz)
        wobjXMLInputMQ.selectSingleNode("//" & wvarNodoRaiz).parentNode.replaceChild wobjCdataNodo, wobjNodo
    End If
    
    wvarStep = 110
    '---------------------------------------
    'Carga usuario ComPlus
    wvarUsuario = wobjXMLConfig.selectSingleNode("//USERID").Text
    wobjXMLInputMQ.selectSingleNode("//UserId").Text = wvarUsuario
    '---------------------------------------
    
    wvarStep = 120
    wvarTimeStamp = Year(Now()) & "-" & Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "T" & Right("00" & Hour(Now()), 2) & ":" & Right("00" & Minute(Now()), 2) & ":" & Right("00" & Second(Now()), 2) & ".000000-03:00"
    wobjXMLInputMQ.selectSingleNode("//MsgCreatTmsp").Text = wvarTimeStamp
        
    wvarStep = 130
    'Instancia y llama a MQ ---------------------------------
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wobjXMLInputMQ.xml, RespuestaMQ, wobjXMLConfig.xml)
    Set wobjFrame2MQ = Nothing
           
    If wvarMQError <> 0 Then
        wvarStep = 131
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        
        'DA este c�digo es para que aparezca un log en el APP en caso de haber problemas al conectar con MQ de Middleware
                mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, _
                mcteClassName & "--" & mvarConsultaRealizada, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & RespuestaMQ & " Area:" & " Hora:" & Now(), _
                vbLogEventTypeError
                
        'DA - 26/09/2007: se quita este c�digo ya que es un error controlado.
        'mobjCOM_Context.SetAbort
        'IAction_Execute = 1
    
    Else
        wvarStep = 132
        If Asc(Right(RespuestaMQ, 1)) = 0 Then
            wvarRespuestaFiltrada = Left(RespuestaMQ, Len(RespuestaMQ) - 1)
        Else
            wvarRespuestaFiltrada = RespuestaMQ
        End If
            
        wvarStep = 133
        Set wvarXMLPDF = CreateObject("MSXML2.DOMDocument")
        wvarXMLPDF.async = False
        wvarXMLPDF.loadXML wvarRespuestaFiltrada
        
        wvarStep = 134
        If Not wvarXMLPDF.selectSingleNode("//" & Replace(wvarNodoRaiz, "share:", "") & "Return") Is Nothing Then
            wvarPasada1 = wvarXMLPDF.selectSingleNode("//" & Replace(wvarNodoRaiz, "share:", "") & "Return").xml
        Else
            wvarPasada1 = ""
        End If
        
        wvarStep = 135
        If Not wvarXMLPDF.selectSingleNode("//faultstring") Is Nothing Then
            wvarFaultCode = "<faultstring>" & wvarXMLPDF.selectSingleNode("//faultstring").Text & "</faultstring>"
        Else
            wvarFaultCode = ""
        End If
        
        wvarStep = 136
        pvarResponse = "<Response><Estado resultado='true' />"
        pvarResponse = pvarResponse & wvarPasada1
        pvarResponse = pvarResponse & wvarFaultCode
        pvarResponse = pvarResponse & "</Response>"
        
        wvarStep = 137
        
        'DA - 26/09/2007: Se manda mas abajo este c�digo
        'mobjCOM_Context.SetComplete
        'IAction_Execute = 0
        
    End If
        
    wvarStep = 140
    
    If wvarLoguear = "SI" Then
            wvarNroArch = FreeFile()
            wvarExisteArchivo = ""
            wvarExisteArchivo = Dir(App.Path & "\" & mcteLogPath & "\" & wvarNomArchivo)
            
            If wvarExisteArchivo <> "" Then Kill App.Path & "\" & mcteLogPath & "\" & wvarNomArchivo
    
            Open App.Path & "\" & mcteLogPath & "\" & wvarNomArchivo For Binary As wvarNroArch
            
            wvarTextoALoguear = "<LOGs><LOG InicioConsulta=""" & wvarHoraInicio & """ FinConsulta=""" & Now() & """ TiempoIncurrido=""" & Timer - wvarTiempo & " seg""" & ">" & _
                                "<AreaIn>" & wobjXMLInputMQ.xml & "</AreaIn>" & _
                                "<AreaOut>" & RespuestaMQ & "</AreaOut>" & _
                                "</LOG></LOGs>"
                                
            Put wvarNroArch, , wvarTextoALoguear
            Close
    End If
    
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
        
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLRequest = Nothing
    Set wobjXMLInputMQ = Nothing
    Set wobjNodoBase64 = Nothing
    Set wobjXMLDefinition = Nothing
    Set wobjXMLConfig = Nothing
    Set wobjXMLSobre = Nothing
    Set wobjNodosEntrada = Nothing
    Set wobjNodoEntrada = Nothing
    Set wobjNodo = Nothing
    Set wobjXMLSobre = Nothing
    
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
       mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & _
                             Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort

Resume ClearObjects

ManejoError:

wvarError = Err.Description

pvarResponse = "<Response><Estado resultado='false' mensaje=' Linea: " & wvarStep & "-" & wvarError & wvarTextoError & " ' /></Response>"

GoTo ClearObjects



End Function


Private Sub ObjectControl_Activate()

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")


End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Function DecodificaBase64(ByVal pDatos As String) As String
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Dim wobjXMLPdf           As MSXML2.DOMDocument
    Dim wobjoNode            As MSXML2.IXMLDOMNode
    Dim wobjStream           As ADODB.Stream
    
    Set wobjXMLPdf = CreateObject("MSXML2.DOMDocument")
    wobjXMLPdf.async = False
    wobjXMLPdf.loadXML "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & pDatos & "</PDF>"
    
    Set wobjoNode = wobjXMLPdf.documentElement.selectSingleNode("//PDF")
    
    Set wobjStream = CreateObject("ADODB.Stream")
        wobjStream.Charset = "Windows-1252"
        wobjStream.Mode = 0
        wobjStream.Type = 1
        wobjStream.Open
        wobjStream.Write wobjoNode.nodeTypedValue
        wobjStream.Position = 0
        wobjStream.Type = 2
        DecodificaBase64 = wobjStream.ReadText
        wobjStream.Close

    Set wobjXMLPdf = Nothing
    Set wobjoNode = Nothing
    Set wobjStream = Nothing
    
End Function



