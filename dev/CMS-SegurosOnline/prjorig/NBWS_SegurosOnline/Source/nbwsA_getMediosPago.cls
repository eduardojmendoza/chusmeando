VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_getMediosPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName              As String = "nbwsA_Transacciones.nbwsA_getMediosPago"
Const mcteParam_COBROTIP         As String = "//COBROTIP"
Const mcteParam_COBROTIPDES      As String = "//COBROTIPDES"
Const mcteNBWSMediosDePago       As String = "XMLs\NBWSMediosDePago.xml"
Const mcteNBWSMediosDePago_XSL   As String = "XSLs\NBWSMediosDePago.xsl"
Const mcteNBWSMediosDePago_Equivalencias As String = "XMLs\NBWSMediosDePago_Equivalencias.xml"


'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument
    Dim wobjXMLResponse             As MSXML2.DOMDocument
    Dim wobjXSLMediosDePago         As MSXML2.DOMDocument
    Dim wobjNBWSMediosDePago        As MSXML2.DOMDocument
    Dim wobjNBWSMediosDePago_Equivalencias As MSXML2.DOMDocument
    Dim oNodo                       As MSXML2.IXMLDOMElement
    Dim oOPTION                     As MSXML2.IXMLDOMElement
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarCOBROTIP        As String
    Dim wvarCOBROTIPDES     As String
    Dim wvarPREGUNTA        As String
    Dim wvarRespuesta       As String
    Dim wvarMEDIO_PAGO_COD   As Integer
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML (Request)
    '
    wvarStep = 20
    wvarCOBROTIP = Trim(wobjXMLRequest.selectSingleNode(mcteParam_COBROTIP).Text)
    wvarCOBROTIPDES = Trim(wobjXMLRequest.selectSingleNode(mcteParam_COBROTIPDES).Text)
    '
    'Levanta el XML de medios de pago
    wvarStep = 30
    Set wobjNBWSMediosDePago = CreateObject("MSXML2.DOMDocument")
        wobjNBWSMediosDePago.async = False
        wobjNBWSMediosDePago.Load (App.Path & "\" & mcteNBWSMediosDePago)
    '
    'Levanta el XML de equivalencias
    wvarStep = 40
    Set wobjNBWSMediosDePago_Equivalencias = CreateObject("MSXML2.DOMDocument")
        wobjNBWSMediosDePago_Equivalencias.async = False
        wobjNBWSMediosDePago_Equivalencias.Load (App.Path & "\" & mcteNBWSMediosDePago_Equivalencias)
    
    'Levanta XSL para armar el combo de salida
    wvarStep = 50
    Set wobjXSLMediosDePago = CreateObject("MSXML2.DOMDocument")
        wobjXSLMediosDePago.async = False
    Call wobjXSLMediosDePago.Load(App.Path & "\" & mcteNBWSMediosDePago_XSL)
    '
    'Transforma el XML en el combo de opciones
    wvarStep = 60
    wobjNBWSMediosDePago.loadXML wobjNBWSMediosDePago.transformNode(wobjXSLMediosDePago)
    '
    'Manda el COBROTIP que vino en el GRAN MENSAJE como valor para la verificaci�n del PID dentro del combo
    wvarStep = 70
    If Not wobjNBWSMediosDePago_Equivalencias.selectSingleNode("EQUIVALENCIAS/EQUIVALENCIA[COBROTIP='" & wvarCOBROTIP & "']/MEDIO_PAGO_COD") Is Nothing Then
        'Si existe en el xml de equivalencias, levanta el valor de ah�.
        wvarMEDIO_PAGO_COD = wobjNBWSMediosDePago_Equivalencias.selectSingleNode("EQUIVALENCIAS/EQUIVALENCIA[COBROTIP='" & wvarCOBROTIP & "']/MEDIO_PAGO_COD").Text
        wobjNBWSMediosDePago.selectSingleNode("//OPTION[@value = '" & wvarMEDIO_PAGO_COD & "']").Attributes(0).Text = wvarCOBROTIP
    Else
        'Si NO existe en el xml de equivalencias, arma el combo con el COBROTIP y COBROTIPDES que vino del AIS
        'Esto es porque en el AIS pueden dar de alta COBROTIPs y los mismos pueden no existir en la tabla de equivalencias del front, ya que el XML del front nunca se entera si en el AIS se abri� un nuevo COBROTIP.
        Set oOPTION = wobjNBWSMediosDePago.createElement("OPTION")
            oOPTION.Text = wvarCOBROTIPDES
            oOPTION.setAttribute "value", wvarCOBROTIP
            wobjNBWSMediosDePago.selectSingleNode("//OPTIONS").appendChild oOPTION
    End If

    '
    wvarStep = 80
    '
    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado>" & wobjNBWSMediosDePago.selectSingleNode("//OPTIONS").xml & "</Response>"
    '
    wvarStep = 90
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjNBWSMediosDePago = Nothing
    Set wobjNBWSMediosDePago_Equivalencias = Nothing
    Set wobjXSLMediosDePago = Nothing
    Set oOPTION = Nothing
    '
    wvarStep = 100
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjNBWSMediosDePago = Nothing
    Set wobjNBWSMediosDePago_Equivalencias = Nothing
    Set wobjXSLMediosDePago = Nothing
    Set oOPTION = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


