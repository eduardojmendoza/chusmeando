VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_ccBaja"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'******************************************************************************
'Fecha de Modificaci�n: 30/09/2011
'PPCR: 2011-00389
'Desarrollador: Gabriel D'Agnone
'Descripci�n: Anexo I - Se envia suscripcion al AIS
'******************************************************************************
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011.
' ALL RIGHTS RESERVED
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
' serious civil and criminal offence, which can result in heavy fines and payment
' of substantial damages.
'
' Nombre del Fuente: nbwsA_Alta.cls
' Fecha de Creaci�n: desconocido
' PPcR: desconocido
' Desarrollador: Desconocido
' Descripci�n: se agrega Copyright para cumplir con las normas de QA
'
'******************************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_ccBaja"


Const mcteParam_DOCUMTIP        As String = "//DOCUMTIP"
Const mcteParam_DOCUMDAT        As String = "//DOCUMDAT"
Const mcteParam_MAIL            As String = "//MAIL"

'Constantes de XML de definiciones para SQL Generico
Const mcteParam_XMLSQLGENBAJA As String = "P_NBWS_SetEstadoBaja.xml"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 12)  As String
Const mcteMsg_OK                As Integer = 0
Const mcteMsg_ERROR             As Integer = 1

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    Dim wobjXMLResponse             As MSXML2.DOMDocument
    '
    Dim wobjXMLRespuesta1184        As MSXML2.DOMDocument
    Dim wobjXMLRespuesta1185        As MSXML2.DOMDocument
    Dim wobjXMLProductosAIS_List    As MSXML2.IXMLDOMNodeList
    Dim wobjXMLProductosAIS_Node    As MSXML2.IXMLDOMNode
    
    Dim wvarMail                    As String
    Dim wvarError                   As String
    Dim wvarRequest                 As String
    Dim wvarResponse                As String
    
    Dim wvarDOCUMTIP                As String
    Dim wvarDOCUMDAT                As String
    
    
     'PARAMETROS LLAMADA 1185
    
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    Dim wvarSWSUSCRI        As String
    Dim wvarSWCONFIR        As String
   ' Dim wvarMail            As String
    Dim wvarCLAVE           As String
    Dim wvarSWCLAVE         As String
    Dim wvarSWTIPOSUS       As String
    
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_ERROR) = getMensaje(mcteClassName, "mcteMsg_ERROR")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 30
    With wobjXMLRequest
        wvarStep = 40
        wvarMail = .selectSingleNode(mcteParam_MAIL).Text
        wvarDOCUMTIP = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarDOCUMDAT = .selectSingleNode(mcteParam_DOCUMDAT).Text
        
    End With
    '
    
'***********************************************************************
' GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
'***********************************************************************

    
                
  
                  
      wvarRequest = "<Request>" & _
                    "<DOCUMTIP>" & wvarDOCUMTIP & "</DOCUMTIP>" & _
                    "<DOCUMDAT>" & wvarDOCUMDAT & "</DOCUMDAT>" & _
                    "<MAIL>" & wvarMail & "</MAIL>" & _
                  "</Request>"
     wvarStep = 50
              
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_sInicio")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
                        
    wvarStep = 60
                        

    'Carga las dos respuestas en un XML.
    Set wobjXMLRespuesta1184 = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuesta1184.async = False
    Call wobjXMLRespuesta1184.loadXML(wvarResponse)
    '
    wvarStep = 70
    'Verifica que no haya pinchado el COM+
    If wobjXMLRespuesta1184.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        '
     wvarStep = 80
        'Fall� Response 1
        
       wvarError = mcteMsg_ERROR
        'Err.Description = "Response : fall� COM+ de Request 1184"
        'GoTo ErrorHandler
        '
    End If
    
    wvarStep = 90
  
    
    wvarStep = 110
    'Levanta en un listado cada producto del cliente
    ' SE DAN DE BAJA TODAS LAS POLIZAS QUE ESTAN SUSCRIPTAS A EPOLIZA Y NBWS
    Set wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes("//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and MAS_DE_CINCO_CERT='N']")
    
If wobjXMLProductosAIS_List.length > 0 Then
  
  wvarStep = 120
  'Recorre p�lizas de clientes.
   wvarRequest = "<Request id=""1""  actionCode=""nbwsA_MQGenericoAIS"" >" & _
                            "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" & _
                            "<EPOLIZAS>"
                            
     wvarStep = 130
      
    For Each wobjXMLProductosAIS_Node In wobjXMLProductosAIS_List
        '
       wvarStep = 140
         wvarRAMOPCOD = wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text
         wvarPOLIZANN = wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text
         wvarPOLIZSEC = wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text
         wvarCERTIPOL = wobjXMLProductosAIS_Node.selectSingleNode("CERTIPOL").Text
         wvarCERTIANN = wobjXMLProductosAIS_Node.selectSingleNode("CERTIANN").Text
         wvarCERTISEC = wobjXMLProductosAIS_Node.selectSingleNode("CERTISEC").Text
         wvarSWSUSCRI = "N"   ' "N" PARA LAS BAJAS
         wvarCLAVE = ""
         wvarSWCLAVE = ""
         wvarSWTIPOSUS = "W"  ' ????
           
                        
         wvarRequest = wvarRequest & "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" & _
                            "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                            "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                            "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                            "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                            "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                            "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                            "<SWSUSCRI>" & wvarSWSUSCRI & "</SWSUSCRI>" & _
                            "<MAIL>" & wvarMail & "</MAIL>" & _
                            "<CLAVE>" & wvarCLAVE & "</CLAVE>" & _
                            "<SW-CLAVE>" & wvarSWCLAVE & "</SW-CLAVE>" & _
                            "<SWTIPOSUS>" & wvarSWTIPOSUS & "</SWTIPOSUS>" & _
                            "</EPOLIZA>"
        
        
        
    Next
    
      wvarRequest = wvarRequest & "</EPOLIZAS></Request>"
      
       wvarRequest = "<Request>" & wvarRequest & "</Request>"
      
    wvarStep = 150
      
    Call cmdp_ExecuteTrnMulti("nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse)
 
    '
    wvarStep = 160
   
   
   
    Set wobjXMLRespuesta1185 = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuesta1185.async = False
    Call wobjXMLRespuesta1185.loadXML(wvarResponse)
    
    wvarStep = 170
    
    If wobjXMLRespuesta1185.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        '
      wvarStep = 180
      
        wvarError = mcteMsg_ERROR
        'Err.Description = "Response : fall� COM+ de Request 1185"
        'GoTo ErrorHandler
        '
    End If
    End If
                
'***********************************************************************
' FIN ANEXO I
'***********************************************************************
    
If wvarError = mcteMsg_OK Then

    wvarStep = 190
    wvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENBAJA & "</DEFINICION>" & _
                        "<MAIL>" & wvarMail & "</MAIL>" & _
                   "</Request>"
    '
    wvarStep = 200
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
  End If
  

    wvarStep = 290
    pvarResponse = "<Response>" & _
                        "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                        "<CODRESULTADO>OK</CODRESULTADO>" & _
                        "<CODERROR>" & wvarError & "</CODERROR>" & _
                        "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                    "</Response>"
    '
    'Finaliza y libera objetos
    wvarStep = 700
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLRespuesta1184 = Nothing
    Set wobjXMLRespuesta1185 = Nothing
    '
    
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~


    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLRespuesta1184 = Nothing
    Set wobjXMLRespuesta1185 = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub










