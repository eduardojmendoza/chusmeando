VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_getCupItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "nbwsA_Transacciones.nbwsA_getCupItem"
Const mcteParam_NROREC      As String = "//NROREC"
Const mcteParam_CIAASCOD    As String = "//CIAASCOD"
'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjCmdProcessor    As Object
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    Dim wvarNROREC          As String
    Dim wvarCIAASCOD        As String
    Dim wvarFecCatalogo     As String
    Dim wvarExecReturn      As String
    '
    Dim wvarDocKey
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)
    '
    wvarStep = 20
    wvarNROREC = wobjXMLRequest.selectSingleNode(mcteParam_NROREC).Text
    wvarCIAASCOD = wobjXMLRequest.selectSingleNode(mcteParam_CIAASCOD).Text
    '
    wvarStep = 30
    If (wvarCIAASCOD = "0001") Then
        'Cat�logo de LBA (MA)
        'DA - 25/08/2009: LG dijo en la reuni�n que el cat�logo es el de OVs, no el de MA.
        wvarRequest = "<Request><VALOR_CLAVE>" & wvarNROREC & "</VALOR_CLAVE></Request>"
        wvarRequest = cmdp_FormatRequest("lbaw_OVSQLGETCatalogCup", "lbaw_OVSQLGETCatalogCup.biz", wvarRequest)
    Else
        'Cat�logo de NYL (MA)
        'DA - 25/08/2009: LG dijo en la reuni�n que el cat�logo es el de OVs, no el de MA.
        wvarRequest = "<Request><DEFINICION>Consulta_CatalogoCup.xml</DEFINICION><VALOR_CLAVE>" & wvarNROREC & "</VALOR_CLAVE></Request>"
        wvarRequest = cmdp_FormatRequest("nylw_OVSQLGen", "nylw_OVSQLGen.biz", wvarRequest)
    End If
    '
    wvarStep = 40
    'DOING:
    Set wobjCmdProcessor = CreateObject("HSBC_ASP.CmdProcessor")
    wvarExecReturn = wobjCmdProcessor.Execute(wvarRequest, wvarResponse)
    '
    wvarStep = 50
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponse.async = False
    Call wobjXMLResponse.loadXML(wvarResponse)
    '
    wvarStep = 60
    If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        '
        wvarStep = 70
        Err.Description = "Error en COM+ de cat�logo de ColdView"
        GoTo ErrorHandler
        '
    Else
        If (wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text) <> "true" Then
            '
            wvarStep = 80
            Err.Description = "Error en COM+ de cat�logo de ColdView"
            GoTo ErrorHandler
            '
        Else
            '
            wvarStep = 90
            wvarFecCatalogo = wobjXMLResponse.selectSingleNode("//FEC_CATALOGO").Text
            Set wobjCmdProcessor = Nothing
            '
            'DA - 18/09/2009: el DocKey es distinto para LBA y NYL
            wvarStep = 100
            If wvarCIAASCOD = "0001" Then
                wvarDocKey = "LBA_CHOV"
            Else
                wvarDocKey = "NYL_CHOV"
            End If
            '
            wvarStep = 105
            wvarRequest = "<Request><CATALOG>LBA - AIS COBRANZAS</CATALOG><PAGENBR></PAGENBR><ISSUEITEMNBR>1</ISSUEITEMNBR><DATEISSUE>" & wvarFecCatalogo & "</DATEISSUE><IDX_BUSQUEDA>" & wvarNROREC & "</IDX_BUSQUEDA><IssueDateFrom>" & wvarFecCatalogo & "</IssueDateFrom><IssueDateTo>" & wvarFecCatalogo & "</IssueDateTo><QueryClause><![CDATA[(factura = '" & wvarNROREC & "')]]></QueryClause><DocKey>" & wvarDocKey & "</DocKey><PDFNoprint>0</PDFNoprint><PDFNomodify>-1</PDFNomodify><PDFNocopy>0</PDFNocopy><PDFNoannots>-1</PDFNoannots><ACCESSWAY>NBWS</ACCESSWAY></Request>"
            wvarRequest = cmdp_FormatRequest("GetXMLPDF_LBA", "GetXMLPDF_LBA.biz", wvarRequest)
            '
            wvarStep = 110
            Set wobjCmdProcessor = CreateObject("HSBC_ASP.CmdProcessor")
            wvarExecReturn = wobjCmdProcessor.Execute(wvarRequest, wvarResponse)
        
            wvarStep = 120
            Call wobjXMLResponse.loadXML(wvarResponse)
        
            If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
                '
                wvarStep = 130
                Err.Description = "Error en el COM+ de recupero de PDF de ColdView"
                GoTo ErrorHandler
                '
            Else
            
                If (UCase(wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text) <> "TRUE") Then
                    '
                    wvarStep = 140
                    Err.Description = "No se pudo recuperar el PDF de ColdView"
                    GoTo ErrorHandler
                    '
                Else
                    '
                    wvarStep = 150
                    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado><PDFSTREAM>" & wobjXMLResponse.documentElement.selectSingleNode("//PDFSTREAM").Text & "</PDFSTREAM></Response>"
                    '
                End If
            End If
        End If
    End If
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjCmdProcessor = Nothing
    '
    wvarStep = 160
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjCmdProcessor = Nothing
    '
    If wvarStep = 140 Then
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeWarning
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    End If
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

