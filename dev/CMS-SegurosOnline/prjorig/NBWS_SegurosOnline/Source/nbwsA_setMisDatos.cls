VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_setMisDatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'******************************************************************************
'Fecha de Modificaci�n: 30/09/2011
'PPCR: 2011-00389
'Desarrollador: Gabriel D'Agnone
'Descripci�n: Anexo I - Se envia suscripcion al AIS
'******************************************************************************
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011.
' ALL RIGHTS RESERVED
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
' serious civil and criminal offence, which can result in heavy fines and payment
' of substantial damages.
'
' Nombre del Fuente: nbwsA_Alta.cls
' Fecha de Creaci�n: desconocido
' PPcR: desconocido
' Desarrollador: Desconocido
' Descripci�n: se agrega Copyright para cumplir con las normas de QA
'
'******************************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName              As String = "nbwsA_Transacciones.nbwsA_setMisDatos"

Const mcteParam_XMLSQLGENVALUSR      As String = "P_NBWS_ValidaUsuario.xml"
Const mcteParam_XMLSQLGENUPDDATPERS  As String = "P_NBWS_UpdateDatosPersonales.xml"


Const mcteParam_DOCUMTIP        As String = "//DOCUMTIP"
Const mcteParam_DOCUMDAT        As String = "//DOCUMDAT"

Const mcteParam_EMAIL_ORIGINAL   As String = "//EMAIL_ORIGINAL"
Const mcteParam_EMAIL_NUEVO      As String = "//EMAIL_NUEVO"
Const mcteParam_PREGUNTA         As String = "//PREGUNTA"
Const mcteParam_RESPUESTA        As String = "//RESPUESTA"
Const mcteParam_VALIDAR_USUARIO  As String = "//VALIDAR_USUARIO"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 1)  As String
Const mcteMsg_OK                     As Integer = 0
Const mcteMsg_EXISTEUSR              As Integer = 1

 
   


'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLDoc          As MSXML2.DOMDocument
    
    Dim wobjXMLRespuesta1184        As MSXML2.DOMDocument
    Dim wobjXMLRespuesta1185        As MSXML2.DOMDocument
    Dim wobjXMLProductosAIS_List    As MSXML2.IXMLDOMNodeList
    Dim wobjXMLProductosAIS_Node    As MSXML2.IXMLDOMNode
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarEMAIL_ORIGINAL  As String
    Dim wvarEMAIL_NUEVO     As String
    Dim wvarPREGUNTA        As String
    Dim wvarRespuesta       As String
    Dim wvarVALIDAR_USUARIO As String
    '
    Dim wvarError           As Integer
    Dim wvarExisteUsr       As String
    
    Dim wvarDOCUMTIP                As String
    Dim wvarDOCUMDAT                As String
    
      'PARAMETROS LLAMADA 1185
    
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    Dim wvarSWSUSCRI        As String
    Dim wvarSWCONFIR        As String
    Dim wvarCLAVE           As String
    Dim wvarSWCLAVE         As String
    Dim wvarSWTIPOSUS       As String
    
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML (Request)
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEUSR) = getMensaje(mcteClassName, "mcteMsg_EXISTEUSR")
    wvarError = mcteMsg_OK
    '
    wvarStep = 20
    wvarEMAIL_ORIGINAL = Trim(wobjXMLRequest.selectSingleNode(mcteParam_EMAIL_ORIGINAL).Text)
    wvarEMAIL_NUEVO = Trim(wobjXMLRequest.selectSingleNode(mcteParam_EMAIL_NUEVO).Text)
    wvarPREGUNTA = Trim(wobjXMLRequest.selectSingleNode(mcteParam_PREGUNTA).Text)
    wvarRespuesta = Trim(wobjXMLRequest.selectSingleNode(mcteParam_RESPUESTA).Text)
    wvarVALIDAR_USUARIO = Trim(wobjXMLRequest.selectSingleNode(mcteParam_VALIDAR_USUARIO).Text)
    
     With wobjXMLRequest
       
       
        wvarDOCUMTIP = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarDOCUMDAT = .selectSingleNode(mcteParam_DOCUMDAT).Text
        
    End With
    '
    wvarStep = 30
    If wvarVALIDAR_USUARIO = "S" Then
        wvarExisteUsr = fncValidarUsuario(wvarEMAIL_NUEVO)
    Else
        'Si la p�gina web NO pide validar usuario, es porque el cliente no modific� la direcci�n de email.
        'Con lo cual simulamos que el cliente no existe para que no arroje error de CLIENTE EXISTENTE
        wvarExisteUsr = "N"
    End If
    '
    wvarStep = 35
    If wvarExisteUsr = "S" Then
        wvarError = mcteMsg_EXISTEUSR
    Else
        wvarError = mcteMsg_OK
    End If
    '
    wvarStep = 40
    If wvarError = mcteMsg_OK Then
    
    wvarStep = 50
    
'***********************************************************************
' GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
'***********************************************************************
                  
      wvarRequest = "<Request>" & _
                    "<DOCUMTIP>" & wvarDOCUMTIP & "</DOCUMTIP>" & _
                    "<DOCUMDAT>" & wvarDOCUMDAT & "</DOCUMDAT>" & _
                    "<MAIL>" & wvarEMAIL_ORIGINAL & "</MAIL>" & _
                  "</Request>"
                  
      wvarStep = 60
              
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_sInicio")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
                        

    '
    wvarStep = 70
    'Carga las dos respuestas en un XML.
    Set wobjXMLRespuesta1184 = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuesta1184.async = False
    Call wobjXMLRespuesta1184.loadXML(wvarResponse)
    '
    wvarStep = 80
    'Verifica que no haya pinchado el COM+
    If wobjXMLRespuesta1184.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        '
        wvarStep = 90
        'Fall� Response 1
        Err.Description = "Response : fall� COM+ de Request 1184"
        GoTo ErrorHandler
        '
    End If
    
   
    'Levanta en un listado cada producto del cliente
   
    wvarStep = 100
       Set wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes("//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and SWSUSCRI!='S' and MAS_DE_CINCO_CERT='N']")
 wvarStep = 110
 
  If wobjXMLProductosAIS_List.length > 0 Then
  
  wvarStep = 120
   'Recorre p�lizas de clientes.
   wvarRequest = "<Request id=""1""  actionCode=""nbwsA_MQGenericoAIS"" >" & _
                            "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" & _
                            "<EPOLIZAS>"
                            
    wvarStep = 130
      
    For Each wobjXMLProductosAIS_Node In wobjXMLProductosAIS_List
        '
         wvarRAMOPCOD = wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text
         wvarPOLIZANN = wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text
         wvarPOLIZSEC = wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text
         wvarCERTIPOL = wobjXMLProductosAIS_Node.selectSingleNode("CERTIPOL").Text
         wvarCERTIANN = wobjXMLProductosAIS_Node.selectSingleNode("CERTIANN").Text
         wvarCERTISEC = wobjXMLProductosAIS_Node.selectSingleNode("CERTISEC").Text
         wvarSWSUSCRI = "S"   ' "S" PARA LAS ALTAS
         wvarCLAVE = ""
         wvarSWCLAVE = "U"
         wvarSWTIPOSUS = "W"  ' "W" CUANDO ES SUSCRIPCION A SEGUROS ON LINE
           
                        
         wvarRequest = wvarRequest & "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" & _
                            "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                            "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                            "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                            "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                            "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                            "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                            "<SWSUSCRI>" & wvarSWSUSCRI & "</SWSUSCRI>" & _
                            "<MAIL>" & wvarEMAIL_NUEVO & "</MAIL>" & _
                            "<CLAVE>" & wvarCLAVE & "</CLAVE>" & _
                            "<SW-CLAVE>" & wvarSWCLAVE & "</SW-CLAVE>" & _
                            "<SWTIPOSUS>" & wvarSWTIPOSUS & "</SWTIPOSUS>" & _
                            "</EPOLIZA>"
        
        
        
    Next
    
      wvarRequest = wvarRequest & "</EPOLIZAS></Request>"
      
      wvarRequest = "<Request>" & wvarRequest & "</Request>"
      
    wvarStep = 140
    Call cmdp_ExecuteTrnMulti("nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse)
 
    '
    wvarStep = 150
   
   
   
    Set wobjXMLRespuesta1185 = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuesta1185.async = False
    Call wobjXMLRespuesta1185.loadXML(wvarResponse)
    
    If wobjXMLRespuesta1185.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        '
        
        Err.Description = "Response : fall� COM+ de Request 1185"
        GoTo ErrorHandler
        '
    End If
      
    End If
                
'***********************************************************************
' FIN ANEXO I
'***********************************************************************
    

    
    
        '
        wvarStep = 160
        wvarRequest = "<Request><DEFINICION>" & mcteParam_XMLSQLGENUPDDATPERS & "</DEFINICION><MAIL>" & wvarEMAIL_ORIGINAL & "</MAIL><MAILNUEVO>" & wvarEMAIL_NUEVO & "</MAILNUEVO><PREGUNTA>" & CapicomEncrypt(wvarPREGUNTA) & "</PREGUNTA><RESPUESTA>" & CapicomEncrypt(wvarRespuesta) & "</RESPUESTA></Request>"
        '
        wvarStep = 170
        Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
        Call wobjClass.Execute(wvarRequest, wvarResponse, "")
        Set wobjClass = Nothing
        '
        wvarStep = 180
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            wobjXMLResponse.async = False
        Call wobjXMLResponse.loadXML(wvarResponse)
        '
        If Not wobjXMLResponse.selectSingleNode("//Response") Is Nothing Then
            wvarStep = 190
                        
            
            Response = "<Response><Estado resultado=""true"" mensaje=""" & mcteMsg_DESCRIPTION(wvarError) & """></Estado></Response>"
        Else
'            Err.Description = "Pinch� el COM+ de SQL Generico: setMisDatos"
'            GoTo ErrorHandler
            wvarStep = 200
            Response = "<Response><Estado resultado=""false"" mensaje=""No es posible continuar con la operaci&oacute;n solicitada. Por favor intente m&aacute;s tarde. Muchas gracias.""></Estado></Response>"
        End If
        '
    Else
        wvarStep = 210
        Response = "<Response><Estado resultado=""false"" mensaje=""" & mcteMsg_DESCRIPTION(wvarError) & """></Estado></Response>"
    End If
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLRespuesta1184 = Nothing
    Set wobjXMLRespuesta1185 = Nothing
    '
    wvarStep = 220
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

 Response = "<Response><Estado resultado=""false"" mensaje=""" & mcteMsg_DESCRIPTION(wvarError) & """></Estado></Response>"
  
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLRespuesta1184 = Nothing
    Set wobjXMLRespuesta1185 = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & Request, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

Private Function fncValidarUsuario(ByVal pvarUsuario As String) As String
    '
    Dim mvarRequest     As String
    Dim mvarResponse    As String
    Dim mvarRetVal      As String
    Dim mobjXMLResponse As MSXML2.DOMDocument
    Dim mobjClass       As HSBCInterfaces.IAction
    '
    mvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENVALUSR & "</DEFINICION>" & _
                        "<MAIL>" & pvarUsuario & "</MAIL>" & _
                   "</Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    Set mobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    mobjXMLResponse.async = False
    mobjXMLResponse.loadXML mvarResponse
    '
    'Analiza resultado del SP
    If Not mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If mobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            If mobjXMLResponse.selectSingleNode("//EXISTE").Text = "S" Or mobjXMLResponse.selectSingleNode("//EXISTE").Text = "N" Then
                '
                'DA - 09/10/2009: puede que el usuario exista pero est� dado de baja.
                If mobjXMLResponse.selectSingleNode("//ESTADO").Text <> "A" Then
                    mvarRetVal = "N"
                Else
                    mvarRetVal = mobjXMLResponse.selectSingleNode("//EXISTE").Text
                End If
                '
            End If
        End If
    End If
    '
    If mvarRetVal = "" Then
        mvarRetVal = "ERR"
    End If
    '
    fncValidarUsuario = mvarRetVal
    Set mobjClass = Nothing
    Set mobjXMLResponse = Nothing
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

