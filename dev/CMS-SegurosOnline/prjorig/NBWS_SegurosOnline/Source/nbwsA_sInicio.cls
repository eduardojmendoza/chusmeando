VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_sInicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_sInicio"

'Parametros XML de Entrada
Const mcteParam_DOCUMTIP      As String = "//DOCUMTIP"
Const mcteParam_DOCUMDAT      As String = "//DOCUMDAT"
Const mcteParam_SESSION_ID    As String = "//SESSION_ID"
Const wcteXSL_sInicio         As String = "XSLs\sInicio.xsl"


' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLRespuestas   As MSXML2.DOMDocument
    Dim wobjXSLSalida       As MSXML2.DOMDocument
    Dim wobjXMLProductosAIS             As MSXML2.DOMDocument
    Dim wobjXMLProductosHabilitados     As MSXML2.DOMDocument
    Dim wobjXMLVendedoresHabilitados    As MSXML2.DOMDocument
    Dim wobjXMLPolizasExcluidas         As MSXML2.DOMDocument
    Dim wobjXMLNBWS_TempFilesServer     As MSXML2.DOMDocument
    Dim wobjXML_AUX                     As MSXML2.DOMDocument
    '
    Dim wobjXMLProductosAIS_List         As MSXML2.IXMLDOMNodeList
    Dim wobjXMLProductosAIS_Node         As MSXML2.IXMLDOMNode
    Dim woNodoProductoHabilitadoNBWS        As MSXML2.IXMLDOMNode
    Dim woNodoProductoHabilitadoNavegacion  As MSXML2.IXMLDOMNode
    Dim woNodoPermiteEPoliza                As MSXML2.IXMLDOMNode
    Dim woNodoPolizaExcluida                As MSXML2.IXMLDOMNode
    Dim woNodoPositiveID                    As MSXML2.IXMLDOMNode
    Dim woNodoMasDeCincoCert                As MSXML2.IXMLDOMNode
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarResponse_XML    As String
    Dim wvarResponse_HTML   As String
    Dim wvarFalseAIS        As String
    '
    'Par�metros de entrada
    Dim wvarDocumtip        As String
    Dim wvarDocumdat        As String
    Dim wvarSESSION_ID      As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        wvarStep = 20
        wvarDocumtip = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarDocumdat = .selectSingleNode(mcteParam_DOCUMDAT).Text
        '
        wvarStep = 21
        'DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
        If Not .selectSingleNode(mcteParam_SESSION_ID) Is Nothing Then
            wvarSESSION_ID = .selectSingleNode(mcteParam_SESSION_ID).Text
        Else
            wvarSESSION_ID = ""
        End If
        '
    End With
    '
    wvarStep = 30
    'Arma XML de entrada al COM+ Multithreading
    '
    wvarRequest = "<Request>" & _
                        "<Request id=""1"" actionCode=""nbwsA_MQGenericoAIS"">" & _
                        "   <DEFINICION>LBA_1184_ProductosClientes.xml</DEFINICION>" & _
                        "   <AplicarXSL>LBA_1184_ProductosClientes.xsl</AplicarXSL>" & _
                        "   <TIPODOCU>" & wvarDocumtip & "</TIPODOCU>" & _
                        "   <NUMEDOCU>" & wvarDocumdat & "</NUMEDOCU>" & _
                        "</Request>" & _
                    "</Request>"

    wvarStep = 40
    'Ejecuta la funci�n Multithreading
    Call cmdp_ExecuteTrnMulti("", "", wvarRequest, wvarResponse)
    '
    wvarStep = 50
    'Carga las dos respuestas en un XML.
    Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuestas.async = False
    Call wobjXMLRespuestas.loadXML(wvarResponse)
    '
    wvarStep = 55
    'Verifica que no haya pinchado el COM+
    If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado") Is Nothing Then
        '
        'Fall� Response 1
        Err.Description = "Response 1: fall� COM+ de Request 1"
        GoTo ErrorHandler
        '
    End If
    '
    wvarStep = 56
    'If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/Estado") Is Nothing Then
        '
        'Fall� Response 2
    '    Err.Description = "Response 2: fall� COM+ de Request 2"
    '    GoTo ErrorHandler
        '
    'End If
    '
    'Anduvieron los dos llamados al AIS.
    'Arma un XML con los productos de las dos compa��as
    wvarResponse = ""
    '
    wvarStep = 57
    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/PRODUCTOS") Is Nothing Then
        wvarResponse = wvarResponse & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/PRODUCTOS").xml
    Else
        wvarResponse = wvarResponse & ""
        wvarFalseAIS = wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado/@mensaje").Text
    End If
    '
    wvarStep = 58
    'If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/CAMPOS/PRODUCTOS") Is Nothing Then
    '    wvarResponse = wvarResponse & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/CAMPOS/PRODUCTOS").xml
    'Else
    '    wvarResponse = wvarResponse & ""
    '    wvarFalseAIS = wvarFalseAIS & Chr(13) & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/Estado/@mensaje").Text
    'End If
    '
    wvarStep = 59
    'DA - 06/08/2009: si no existe el cliente tanto en LBA como en NYL
    If wvarResponse = "" Then
        GoTo ClienteNoExiste:
    End If
    '
    wvarResponse = "<Response>" & Replace(Replace(wvarResponse, "<PRODUCTOS>", ""), "</PRODUCTOS>", "") & "</Response>"
    '
    wvarStep = 60
    Set wobjXMLProductosAIS = CreateObject("MSXML2.DOMDocument")
        wobjXMLProductosAIS.async = False
    Call wobjXMLProductosAIS.loadXML(wvarResponse)
    '
    wvarStep = 61
    wobjXMLProductosAIS.selectSingleNode("//Response").appendChild wobjXMLRespuestas.selectSingleNode("//CLIENSEC").cloneNode(True)
    wobjXMLProductosAIS.selectSingleNode("//Response").appendChild wobjXMLRespuestas.selectSingleNode("//TIPODOCU").cloneNode(True)
    wobjXMLProductosAIS.selectSingleNode("//Response").appendChild wobjXMLRespuestas.selectSingleNode("//NUMEDOCU").cloneNode(True)
    wobjXMLProductosAIS.selectSingleNode("//Response").appendChild wobjXMLRespuestas.selectSingleNode("//CLIENAP1").cloneNode(True)
    wobjXMLProductosAIS.selectSingleNode("//Response").appendChild wobjXMLRespuestas.selectSingleNode("//CLIENAP2").cloneNode(True)
    wobjXMLProductosAIS.selectSingleNode("//Response").appendChild wobjXMLRespuestas.selectSingleNode("//CLIENNOM").cloneNode(True)
    '
    wvarStep = 62
    'Levanta XML de productos habilitados para NBWS.
    Set wobjXMLProductosHabilitados = CreateObject("MSXML2.DOMDocument")
        wobjXMLProductosHabilitados.async = False
    Call wobjXMLProductosHabilitados.Load(App.Path & "\" & wcteProductosHabilitados)
    '
    wvarStep = 70
    'Levanta en un listado cada producto del cliente
    Set wobjXMLProductosAIS_List = wobjXMLProductosAIS.selectNodes("//PRODUCTO")
    '
    wvarStep = 80
    'Pide al SQL listado de vendedores de todos los AGENTCLA y AGENTCOD de las p�lizas del cliente.
    Set wobjXMLVendedoresHabilitados = CreateObject("MSXML2.DOMDocument")
        wobjXMLVendedoresHabilitados.async = False
    Call wobjXMLVendedoresHabilitados.loadXML(fncVendedoresHabilitados(wobjXMLProductosAIS_List))
    '
    wvarStep = 85

    'DA - 31/08/2009: Pregunta al SQL si las p�lizas del cliente est�n exclu�das del NBWS (p�lizas encuadradas)
    Set wobjXMLPolizasExcluidas = CreateObject("MSXML2.DOMDocument")
        wobjXMLPolizasExcluidas.async = False
    Call wobjXMLPolizasExcluidas.loadXML(fncExclusionDePolizas(wobjXMLProductosAIS_List))
    '
    wvarStep = 90
    'Recorre p�lizas de clientes.
    For Each wobjXMLProductosAIS_Node In wobjXMLProductosAIS_List
        '
        wvarStep = 100
        '
        Set woNodoProductoHabilitadoNBWS = wobjXMLProductosAIS.createElement("HABILITADO_NBWS")
        Set woNodoProductoHabilitadoNavegacion = wobjXMLProductosAIS.createElement("HABILITADO_NAVEGACION")
        Set woNodoPermiteEPoliza = wobjXMLProductosAIS.createElement("HABILITADO_EPOLIZA")
        Set woNodoPolizaExcluida = wobjXMLProductosAIS.createElement("POLIZA_EXCLUIDA")
        Set woNodoPositiveID = wobjXMLProductosAIS.createElement("POSITIVEID")
        Set woNodoMasDeCincoCert = wobjXMLProductosAIS.createElement("MAS_DE_CINCO_CERT")
        '
        'Verifica si el producto est� habilitado en NBWS (xPath al XML de Productos Habilitados)
        If Not wobjXMLProductosHabilitados.selectSingleNode("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "']") Is Nothing Then
            '
            wvarStep = 105
            'Manda el RAMOPDES de la tabla de productos habilitados
            wobjXMLProductosAIS_Node.selectSingleNode("RAMOPDES").Text = wobjXMLProductosHabilitados.selectSingleNode("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "']/RAMOPDES").Text
            wobjXMLProductosAIS_Node.selectSingleNode("TIPOPROD").Text = wobjXMLProductosHabilitados.selectSingleNode("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "']/TIPOPRODU").Text
            
            wvarStep = 110
            'Marca producto habilitado para el NBWS
            woNodoProductoHabilitadoNBWS.Text = "S"
            wobjXMLProductosAIS_Node.appendChild woNodoProductoHabilitadoNBWS
            
            wvarStep = 115
            'Indica a qu� Positive ID debe redirigir en caso de corresponder
            woNodoPositiveID.Text = wobjXMLProductosHabilitados.selectSingleNode("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "']/POSITIVEID").Text
            wobjXMLProductosAIS_Node.appendChild woNodoPositiveID
            '
            wvarStep = 120
            'DA - 31/08/2009: Verifica en SQL si la p�liza no est� exclu�da (p�lizas encuadradas)
            If Not wobjXMLPolizasExcluidas.selectSingleNode("//EXCLUSION[CIAASCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("CIAASCOD").Text & "' and RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "' and POLIZANN='" & CLng(wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text) & "' and POLIZSEC='" & CLng(wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text) & "']") Is Nothing Then
                '
                wvarStep = 130
                'Marca la p�liza como exclu�da del NBWS
                woNodoPolizaExcluida.Text = "S"
                '
            Else
                '
                wvarStep = 140
                'Marca la p�liza como NO exclu�da del NBWS (es decir, inclu�da)
                woNodoPolizaExcluida.Text = "N"
                '
            End If
            '
            wvarStep = 145
            wobjXMLProductosAIS_Node.appendChild woNodoPolizaExcluida
            '
            wvarStep = 150
            'Verifica en SQL si el producto es navegable seg�n qui�n haya vendido la p�liza
            If Not wobjXMLVendedoresHabilitados.selectSingleNode("//VENDEDOR[AGENTCLA='" & wobjXMLProductosAIS_Node.selectSingleNode("AGENTCLA").Text & "' and AGENTCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("AGENTCOD").Text & "']") Is Nothing Then
                '
                'Marca producto como habilitado para la navegaci�n
                wvarStep = 151
                woNodoProductoHabilitadoNavegacion.Text = "S"
                wobjXMLProductosAIS_Node.appendChild woNodoProductoHabilitadoNavegacion
                '
                'Verifica que la p�liza en cuesti�n no tenga mas de 5 certificados
                If (wobjXMLProductosAIS.selectNodes("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "' and POLIZANN = '" & wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text & "' and POLIZSEC = '" & wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text & "']").length > 5) Then
                    '
                    wvarStep = 152
                    'Indica que el producto tiene mas de 5 certificados
                    woNodoMasDeCincoCert.Text = "S"
                Else
                    wvarStep = 153
                    'Indica que el producto NO tiene mas de 5 certificados
                    woNodoMasDeCincoCert.Text = "N"
                End If
                '
                wvarStep = 154
                wobjXMLProductosAIS_Node.appendChild woNodoMasDeCincoCert
                '
            Else
                '
                wvarStep = 160
                'Marca producto NO habilitado para la navegaci�n
                woNodoProductoHabilitadoNavegacion.Text = "N"
                wobjXMLProductosAIS_Node.appendChild woNodoProductoHabilitadoNavegacion
                wvarStep = 161
                'Verifica que la p�liza en cuesti�n no tenga mas de 5 certificados
                If (wobjXMLProductosAIS.selectNodes("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "' and POLIZANN = '" & wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text & "' and POLIZSEC = '" & wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text & "']").length > 5) Then
                    '
                    wvarStep = 162
                    'Indica que el producto tiene mas de 5 certificados
                    woNodoMasDeCincoCert.Text = "S"
                Else
                    wvarStep = 163
                    'Indica que el producto NO tiene mas de 5 certificados
                    woNodoMasDeCincoCert.Text = "N"
                End If
                '
                wvarStep = 164
                wobjXMLProductosAIS_Node.appendChild woNodoMasDeCincoCert
                '
            End If
            '
            'Verifica si el producto est� habilitado para ePoliza (xPath al XML de Productos Habilitados)
            If Not wobjXMLProductosHabilitados.selectSingleNode("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "' and EPOLIZA = 'S']") Is Nothing Then
                wvarStep = 170
                'Marca producto habilitado para la ePoliza
                woNodoPermiteEPoliza.Text = "S"
                 '
            Else
                wvarStep = 180
                'Marca producto NO habilitado para la ePoliza
                woNodoPermiteEPoliza.Text = "N"
                '
            End If
            '
            wvarStep = 181
            wobjXMLProductosAIS_Node.appendChild woNodoPermiteEPoliza
            '
        Else
            '
            wvarStep = 190
            'Marca producto NO habilitado para el NBWS (no est� en la tabla de Productos Habilitados para NBWS)
            woNodoProductoHabilitadoNBWS.Text = "N"
            wobjXMLProductosAIS_Node.appendChild woNodoProductoHabilitadoNBWS
            '
            wvarStep = 200
            'Marca producto NO habilitado para la navegaci�n (sin pasar por la validaci�n)
            woNodoProductoHabilitadoNavegacion.Text = "N"
            wobjXMLProductosAIS_Node.appendChild woNodoProductoHabilitadoNavegacion
            '
            wvarStep = 201
            'Verifica si el producto est� habilitado para ePoliza (xPath al XML de Productos Habilitados)
            If Not wobjXMLProductosHabilitados.selectSingleNode("//PRODUCTO[RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "' and EPOLIZA = 'S']") Is Nothing Then
                wvarStep = 202
                'Marca producto habilitado para la ePoliza
                woNodoPermiteEPoliza.Text = "S"
                 '
            Else
                wvarStep = 203
                'Marca producto NO habilitado para la ePoliza
                woNodoPermiteEPoliza.Text = "N"
                '
            End If
            '
            wvarStep = 204
            wobjXMLProductosAIS_Node.appendChild woNodoPermiteEPoliza
            '
            wvarStep = 205
            'DA - 31/08/2009: Verifica en SQL si la p�liza no est� exclu�da (p�lizas encuadradas)
            If Not wobjXMLPolizasExcluidas.selectSingleNode("//EXCLUSION[CIAASCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("CIAASCOD").Text & "' and RAMOPCOD='" & wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text & "' and POLIZANN='" & CLng(wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text) & "' and POLIZSEC='" & CLng(wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text) & "']") Is Nothing Then
                '
                wvarStep = 206
                'Marca la p�liza como exclu�da del NBWS
                woNodoPolizaExcluida.Text = "S"
                '
            Else
                '
                wvarStep = 207
                'Marca la p�liza como NO exclu�da del NBWS (es decir, inclu�da)
                woNodoPolizaExcluida.Text = "N"
                '
            End If
            '
            wvarStep = 208
            wobjXMLProductosAIS_Node.appendChild woNodoPolizaExcluida
            '
        End If
        '
        Set woNodoProductoHabilitadoNBWS = Nothing
        Set woNodoProductoHabilitadoNavegacion = Nothing
        Set woNodoPermiteEPoliza = Nothing
        Set woNodoPositiveID = Nothing
        Set woNodoPolizaExcluida = Nothing
        '
        wvarStep = 210
        wobjXMLProductosAIS_List.nextNode
        '
    Next
    '
    wvarStep = 220
    'Levanta XSL para armar XML de salida
    Set wobjXSLSalida = CreateObject("MSXML2.DOMDocument")
        wobjXSLSalida.async = False
    Call wobjXSLSalida.Load(App.Path & "\" & wcteXSL_sInicio)
    '
    wvarStep = 240
    'Devuelve respuesta en formato XML y HTML
    wvarResponse_XML = wobjXMLProductosAIS.xml
    '
    wvarStep = 250
    wvarResponse_HTML = wobjXMLProductosAIS.transformNode(wobjXSLSalida)
    '
    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado><Response_XML>" & wvarResponse_XML & "</Response_XML><Response_HTML><![CDATA[" & wvarResponse_HTML & "]]></Response_HTML></Response>"
    '
ClienteNoExiste:
    wvarStep = 260
    If wvarResponse = "" Then
        Response = "<Response><Estado resultado=""false"" mensaje=""" & wvarFalseAIS & """></Estado><Response_XML></Response_XML><Response_HTML>" & wvarFalseAIS & "</Response_HTML></Response>"
    End If
    '
    wvarStep = 270
    'DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
    If wvarSESSION_ID <> "" Then
        '
        wvarStep = 271
        'Levanta XML de configuraci�n donde se indica la ruta de grabaci�n de archivos temporales.
        Set wobjXMLNBWS_TempFilesServer = CreateObject("MSXML2.DOMDocument")
            wobjXMLNBWS_TempFilesServer.async = False
        Call wobjXMLNBWS_TempFilesServer.Load(App.Path & "\" & wcteNBWS_TempFilesServer)
        '
        wvarStep = 272
        'Guarda en temporal el XML de INICIO
        If Not wobjXMLProductosAIS Is Nothing Then
            wobjXMLProductosAIS.Save wobjXMLNBWS_TempFilesServer.selectSingleNode("//PATH").Text & "\sINICIO_" & wvarSESSION_ID & ".xml"
        Else
            wvarStep = 273
            'DA - 30/11/2009: cuando no hay p�lizas en NYL ni LBA
            Set wobjXML_AUX = CreateObject("MSXML2.DOMDocument")
                wobjXML_AUX.async = False
                wobjXML_AUX.loadXML (Response)
                wobjXML_AUX.Save wobjXMLNBWS_TempFilesServer.selectSingleNode("//PATH").Text & "\sINICIO_" & wvarSESSION_ID & ".xml"
            Set wobjXML_AUX = Nothing
            '
        End If
        '
        Set wobjXMLNBWS_TempFilesServer = Nothing
        '
    End If
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLRespuestas = Nothing
    Set wobjXMLProductosAIS = Nothing
    Set wobjXMLProductosHabilitados = Nothing
    Set wobjXMLProductosAIS_List = Nothing
    Set wobjXMLProductosAIS_Node = Nothing
    Set wobjXMLVendedoresHabilitados = Nothing
    Set wobjXSLSalida = Nothing
    '
    wvarStep = 280
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLRespuestas = Nothing
    Set wobjXMLProductosAIS = Nothing
    Set wobjXMLProductosHabilitados = Nothing
    Set wobjXMLProductosAIS_List = Nothing
    Set wobjXMLProductosAIS_Node = Nothing
    Set wobjXMLVendedoresHabilitados = Nothing
    Set wobjXSLSalida = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


'***************************************************************
'*** Devuelve un XML con los vendedores habilitados
'***************************************************************
Public Function fncVendedoresHabilitados(pobjXMLProductosAIS_List As IXMLDOMNodeList) As String
    '
    Const mcteParam_DEFINICION_SQL      As String = "P_NBWS_ObtenerVendedores.xml"
    Dim oNodoProducto                   As MSXML2.IXMLDOMNode
    Dim mobjClass                       As HSBCInterfaces.IAction
    Dim mvarRequest                     As String
    Dim mvarResponse                    As String
    Dim mobjXMLVendedoresHabilitados    As MSXML2.DOMDocument
    '
    mvarRequest = ""
    'Arma el XML de entrada
    For Each oNodoProducto In pobjXMLProductosAIS_List
        '
        mvarRequest = mvarRequest & "<AGENTE AGENTCLA=""" & oNodoProducto.selectSingleNode("AGENTCLA").Text & """ AGENTCOD=""" & oNodoProducto.selectSingleNode("AGENTCOD").Text & """></AGENTE>"
        pobjXMLProductosAIS_List.nextNode
        '
    Next
    '
    'Llama al COM+ gen�rico de SQL con un listado de AGENTCLA y AGENTCOD traidos de los productos del cliente
    mvarRequest = "<Request><DEFINICION>" & mcteParam_DEFINICION_SQL & "</DEFINICION><ROOT>" & mvarRequest & "</ROOT></Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    'Levanta el resultado de la ejecuci�n para analizar la respuesta
    Set mobjXMLVendedoresHabilitados = CreateObject("MSXML2.DOMDocument")
        mobjXMLVendedoresHabilitados.async = False
    Call mobjXMLVendedoresHabilitados.loadXML(mvarResponse)
    '
    'Verifica respuesta del COM+
    If Not mobjXMLVendedoresHabilitados.selectSingleNode("//Response") Is Nothing Then
        fncVendedoresHabilitados = mobjXMLVendedoresHabilitados.selectSingleNode("//Response").xml
    Else
        fncVendedoresHabilitados = "<Response></Response>"
    End If
    '
    Set mobjXMLVendedoresHabilitados = Nothing
    '
End Function

'***************************************************************
'*** En base a un listado de p�lizas, determina si alguna de ellas est� exclu�da para el NBWS
'***************************************************************
Public Function fncExclusionDePolizas(pobjXMLProductosAIS_List As IXMLDOMNodeList) As String
    '
    Const mcteParam_DEFINICION_SQL      As String = "P_NBWS_ObtenerExclusiones.xml"
    Dim oNodoProducto                   As MSXML2.IXMLDOMNode
    Dim mobjClass                       As HSBCInterfaces.IAction
    Dim mvarRequest                     As String
    Dim mvarResponse                    As String
    Dim mobjXMLPolizasExcluidas         As MSXML2.DOMDocument
    '
    mvarRequest = ""
    'Arma el XML de entrada
    For Each oNodoProducto In pobjXMLProductosAIS_List
        '
        mvarRequest = mvarRequest & "<POLIZA CIAASCOD=""" & oNodoProducto.selectSingleNode("CIAASCOD").Text & """ RAMOPCOD=""" & oNodoProducto.selectSingleNode("RAMOPCOD").Text & """ POLIZANN=""" & oNodoProducto.selectSingleNode("POLIZANN").Text & """ POLIZSEC=""" & oNodoProducto.selectSingleNode("POLIZSEC").Text & """></POLIZA>"
        pobjXMLProductosAIS_List.nextNode
        '
    Next
    '
    'Llama al COM+ gen�rico de SQL con un listado de AGENTCLA y AGENTCOD traidos de los productos del cliente
    mvarRequest = "<Request><DEFINICION>" & mcteParam_DEFINICION_SQL & "</DEFINICION><ROOT>" & mvarRequest & "</ROOT></Request>"
    '
    Set mobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    Set mobjClass = Nothing
    '
    'Levanta el resultado de la ejecuci�n para analizar la respuesta
    Set mobjXMLPolizasExcluidas = CreateObject("MSXML2.DOMDocument")
        mobjXMLPolizasExcluidas.async = False
    Call mobjXMLPolizasExcluidas.loadXML(mvarResponse)
    '
    'Verifica respuesta del COM+
    If Not mobjXMLPolizasExcluidas.selectSingleNode("//Response") Is Nothing Then
        fncExclusionDePolizas = mobjXMLPolizasExcluidas.selectSingleNode("//Response").xml
    Else
        fncExclusionDePolizas = "<Response></Response>"
    End If
    '
    Set mobjXMLPolizasExcluidas = Nothing
    '
End Function

