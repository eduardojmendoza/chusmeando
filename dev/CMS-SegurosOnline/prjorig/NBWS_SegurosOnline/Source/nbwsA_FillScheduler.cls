VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_FillScheduler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Request para probar <Request><INSCTRL>N</INSCTRL></Request>

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Archivo de Configuracion
Const mcteEPolizaConfig             As String = "NBWS_ePolizaConfig.xml"

'Datos de la accion
Const mcteClassName                 As String = "nbwsA_Transacciones.nbwsA_FillScheduler"

'Stored Procedures
Const mcteStoreProcInsBandejaSCH    As String = "P_CP_SA_BANDEJA_INSERT"

' ANTERIORES
Const mcteStoreProcInsControl       As String = "P_ALERTAS_INSERT_CONTROL"

' SI NUEVO
Const mcteStoreProcInsLog           As String = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG"
'
Const mcteParam_INSCTRL             As String = "//INSCTRL"
'
Const mcteOperacion                 As String = "FILLSCH"

Type NODOINSERTASCH
    CIAASCOD            As String
    RAMOPCOD            As String
    POLIZANN            As String
    POLIZSEC            As String
    CERTIPOL            As String
    CERTIANN            As String
    CERTISEC            As String
    OPERAPOL            As String
    DOCUMTIP            As String
    DOCUMDAT            As String
    CLIENAP1            As String
    CLIENAP2            As String
    CLIENNOM            As String
    NOMARCH             As String
    FORMUDES            As String
    SWSUSCRI            As String
    MAIL                As String
    CLAVE               As String
    SWCLAVE             As String
    SWENDOSO            As String
    OPERATIP            As String
    ESTADO              As String
    CODESTAD            As String
End Type

Dim mvarNodoInsertaSch              As NODOINSERTASCH





Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarStep            As Long
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarResponseNYL     As String
    Dim wvarResponseLBA     As String
    Dim wvarResult          As String
    '
  
    Dim wobjXMLRespuestas       As MSXML2.DOMDocument
    
    Dim wobjXMLConfigNode   As MSXML2.IXMLDOMNode
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    '
    Dim mvarIDCONTROL       As Integer
    Dim mvarINSCTRL         As String
    Dim mvarResponseAIS     As String
    Dim mvarEnvioMDW        As Boolean
    Dim mvarProcConErrores  As Boolean
    Dim mvarProcesar        As Boolean
    Dim mvarPendientes      As Integer
    Dim mvarMSGError        As String
    Dim mvarFECHA           As String
    Dim mvarCODOP           As String
    Dim wvarCount           As Integer
     Dim wvarFalseAIS        As String
    
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializaci�n de variables
    wvarStep = 10
    mvarFECHA = Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "-" & Year(Now())
    mvarINSCTRL = "N"
    '
    'Lectura de par�metros
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 30
    With wobjXMLRequest
        If Not .selectSingleNode(mcteParam_INSCTRL) Is Nothing Then
            mvarINSCTRL = .selectSingleNode(mcteParam_INSCTRL).Text
        End If
    End With
    '
    'Registraci�n en Log de inicio de ejecuci�n
    wvarStep = 40
    'insertLog mcteOperacion, "I-NBWS_FILLSCH - Nueva ejecuci�n"
    
   wvarRequest = ""
   
            
    wvarStep = 50
    'P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
    wvarRequest = wvarRequest & "<Request id=""1"" actionCode=""nbwsA_MQGenericoAIS"" ciaascod=""0001"">" & _
                                        "   <DEFINICION>LBA_1187_ListadoePolizas.xml</DEFINICION>" & _
                                        "   <LNK-CIAASCOD>0001</LNK-CIAASCOD>" & _
                                        "</Request>"
   


   
    wvarStep = 60
    'Ejecuta la funci�n Multithreading
    wvarRequest = "<Request>" & wvarRequest & "</Request>"
    Call cmdp_ExecuteTrnMulti("nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse)
    
    
     Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
     wobjXMLRespuestas.async = False
     
    
     
        wobjXMLRespuestas.loadXML wvarResponse
        
        'Carga las dos respuestas en un XML.
        
  wvarStep = 70
       If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado") Is Nothing Then
        '
        'Fall� Response 1
        Err.Description = "Response 1: fall� COM+ de Request 1"
        GoTo ErrorHandler
        '
    End If
    '
    wvarStep = 80
   
        wvarResponseNYL = ""
  
    wvarStep = 90
     
        If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/IMPRESOS") Is Nothing Then
            wvarResponseLBA = wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS").xml
             wvarResponseLBA = Replace(Replace(wvarResponseLBA, "<CAMPOS>", ""), "</CAMPOS>", "")
        
            wvarResponseLBA = "<Response>" & Replace(Replace(wvarResponseLBA, "<IMPRESOS>", ""), "</IMPRESOS>", "") & "</Response>"
        
        Else
            wvarResponseLBA = ""
            wvarFalseAIS = wvarFalseAIS & Chr(13) & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado/@mensaje").Text
        End If
     wvarStep = 100
    
    If wvarResponseLBA <> "" Then
    
    
        If Not insertaImpresoSCH(wvarResponseLBA, "0001", mvarFECHA) Then GoTo ErrorHandler:
       
    
    End If
    
    If wvarResponseLBA = "" Then GoTo FalseAIS:
      
      
    wvarStep = 110
     
    
    'Registraci�n en Log de fin de proceso
    
    
    If mvarProcConErrores Then
        'insertLog mcteOperacion, "E- Fin NBWS_FILLSCH. Estado: ERROR"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    Else
        'insertControlSCH
       ' If mvarINSCTRL = "S" Then
           ' insertControlSCH mvarFECHA
      '  End If
      
       ' insertLog mcteOperacion, "0- Fin NBWS_FILLSCH. Estado: OK"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    End If
    
    wvarStep = 120
FalseAIS:
    If wvarResponse = "" Then
        pvarResponse = "<Response><Estado resultado=""false"" mensaje=""" & wvarFalseAIS & """></Estado><Response_XML></Response_XML><Response_HTML>" & wvarFalseAIS & "</Response_HTML></Response>"
    End If
    '
    'FIN
    '
    wvarStep = 130
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    wvarStep = 140
    
    Exit Function
    
    
    
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    'Inserta error en Log
    'insertLog mcteOperacion, mcteClassName & " - " & _
                     wcteFnName & " - " & _
                     wvarStep & " - " & _
                     Err.Number & " - " & _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & _
                     vbLogEventTypeError
    
    'insertLog mcteOperacion, "99- Fin NBWS_FILLSCH. Estado: ABORTADO POR ERROR"
          
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Function insertaImpresoSCH(ByVal pvarResponse, ByVal pvarCiaascod, ByVal pvarFECHA) As Boolean

    Dim mvarx As Integer
    Dim wvarNombreArchivos      As String
    Dim wobjXMLImpresos         As MSXML2.DOMDocument
    Dim wobjXMLDOMArchivos      As MSXML2.DOMDocument
    Dim wobjXSLResponse         As MSXML2.DOMDocument
    
    Dim wobjXMLNode             As MSXML2.IXMLDOMNode
    Dim wobjXMLImpresosList     As MSXML2.IXMLDOMNodeList
    Dim wobjXMLNombreArchivos   As MSXML2.IXMLDOMNodeList
    Dim wobjXMLFormudes         As MSXML2.IXMLDOMNodeList
    Dim wobjXMLArchivos         As IXMLDOMNode
    Dim wvarResult              As String
    
    Set wobjXMLImpresos = CreateObject("MSXML2.DOMDocument")
    wobjXMLImpresos.setProperty "SelectionLanguage", "XPath"
    wobjXMLImpresos.async = False
    Call wobjXMLImpresos.loadXML(pvarResponse)
    
    Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
    
     wobjXSLResponse.async = False
    Call wobjXSLResponse.loadXML(p_GetXSL())
    
    
      wvarResult = Replace(wobjXMLImpresos.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
         
    Set wobjXMLDOMArchivos = CreateObject("MSXML2.DOMDocument")
    wobjXMLDOMArchivos.setProperty "SelectionLanguage", "XPath"
    wobjXMLDOMArchivos.async = False
    Call wobjXMLDOMArchivos.loadXML(wvarResult)
    
   
   'wvarResult = wobjXMLImpresos.transformNode(wobjXSLResponse)
          
    mvarNodoInsertaSch.NOMARCH = ""
    
    
    
    Set wobjXMLImpresosList = wobjXMLDOMArchivos.selectNodes("//IMPRESO[not(AGRUPAR=preceding-sibling::IMPRESO/AGRUPAR)]")
                       
         
         For Each wobjXMLNode In wobjXMLImpresosList
                                
                mvarNodoInsertaSch.NOMARCH = ""
                mvarNodoInsertaSch.CIAASCOD = pvarCiaascod
                mvarNodoInsertaSch.RAMOPCOD = wobjXMLNode.selectSingleNode("RAMOPCOD").Text
                mvarNodoInsertaSch.POLIZANN = wobjXMLNode.selectSingleNode("POLIZANN").Text
                mvarNodoInsertaSch.POLIZSEC = wobjXMLNode.selectSingleNode("POLIZSEC").Text
                mvarNodoInsertaSch.CERTIPOL = wobjXMLNode.selectSingleNode("CERTIPOL").Text
                mvarNodoInsertaSch.CERTIANN = wobjXMLNode.selectSingleNode("CERTIANN").Text
                mvarNodoInsertaSch.CERTISEC = wobjXMLNode.selectSingleNode("CERTISEC").Text
                mvarNodoInsertaSch.OPERAPOL = wobjXMLNode.selectSingleNode("OPERAPOL").Text
                mvarNodoInsertaSch.DOCUMTIP = wobjXMLNode.selectSingleNode("DOCUMTIP").Text
                mvarNodoInsertaSch.DOCUMDAT = wobjXMLNode.selectSingleNode("DOCUMDAT").Text
                mvarNodoInsertaSch.CLIENAP1 = wobjXMLNode.selectSingleNode("CLIENAP1").Text
                mvarNodoInsertaSch.CLIENAP2 = wobjXMLNode.selectSingleNode("CLIENAP2").Text
                mvarNodoInsertaSch.CLIENNOM = wobjXMLNode.selectSingleNode("CLIENNOM").Text
              
              
              Set wobjXMLNombreArchivos = wobjXMLDOMArchivos.selectNodes("//IMPRESO[CIAASCOD='" & mvarNodoInsertaSch.CIAASCOD & _
               "' and RAMOPCOD='" & mvarNodoInsertaSch.RAMOPCOD & _
               "' and POLIZANN='" & mvarNodoInsertaSch.POLIZANN & _
               "' and POLIZSEC='" & mvarNodoInsertaSch.POLIZSEC & _
               "' and CERTIPOL='" & mvarNodoInsertaSch.CERTIPOL & _
               "' and CERTIANN='" & mvarNodoInsertaSch.CERTIANN & _
               "' and CERTISEC='" & mvarNodoInsertaSch.CERTISEC & _
               "' and OPERAPOL='" & mvarNodoInsertaSch.OPERAPOL & _
               "' ]/NOMARCH")
               
               Set wobjXMLFormudes = wobjXMLDOMArchivos.selectNodes("//IMPRESO[CIAASCOD='" & mvarNodoInsertaSch.CIAASCOD & _
               "' and RAMOPCOD='" & mvarNodoInsertaSch.RAMOPCOD & _
               "' and POLIZANN='" & mvarNodoInsertaSch.POLIZANN & _
               "' and POLIZSEC='" & mvarNodoInsertaSch.POLIZSEC & _
               "' and CERTIPOL='" & mvarNodoInsertaSch.CERTIPOL & _
               "' and CERTIANN='" & mvarNodoInsertaSch.CERTIANN & _
               "' and CERTISEC='" & mvarNodoInsertaSch.CERTISEC & _
               "' and OPERAPOL='" & mvarNodoInsertaSch.OPERAPOL & _
               "' ]/FORMUDES")
               
              
               
               For mvarx = 0 To wobjXMLNombreArchivos.length - 1
                
                    mvarNodoInsertaSch.NOMARCH = mvarNodoInsertaSch.NOMARCH & "<ARCHIVO>" & _
                                                 Replace(wobjXMLNombreArchivos.Item(mvarx).xml, Chr(32), "_") & _
                                                 wobjXMLFormudes.Item(mvarx).xml & "</ARCHIVO>"
                
                Next
                
                
                mvarNodoInsertaSch.SWSUSCRI = wobjXMLNode.selectSingleNode("SWSUSCRI").Text
                mvarNodoInsertaSch.MAIL = wobjXMLNode.selectSingleNode("MAIL").Text
                mvarNodoInsertaSch.CLAVE = wobjXMLNode.selectSingleNode("CLAVE").Text
                mvarNodoInsertaSch.SWCLAVE = wobjXMLNode.selectSingleNode("SW-CLAVE").Text
                mvarNodoInsertaSch.SWENDOSO = wobjXMLNode.selectSingleNode("SWENDOSO").Text
                mvarNodoInsertaSch.OPERATIP = wobjXMLNode.selectSingleNode("OPERATIP").Text
                mvarNodoInsertaSch.ESTADO = wobjXMLNode.selectSingleNode("ESTADO").Text
                mvarNodoInsertaSch.CODESTAD = wobjXMLNode.selectSingleNode("CODESTAD").Text
                
         
            If mvarNodoInsertaSch.RAMOPCOD <> "" Then
            ' Inserta c/u de los registros en la Bandeja de Jobs
                If Not (insertScheduler(mvarNodoInsertaSch, pvarFECHA)) Then
                    insertaImpresoSCH = False
                    Exit Function
                
                End If
            End If
        
        Next
    
    Set wobjXMLNode = Nothing
    Set wobjXMLImpresos = Nothing
    insertaImpresoSCH = True


End Function


Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "<xsl:template match='/'>"
     wvarStrXSL = wvarStrXSL & " <Request>"
    wvarStrXSL = wvarStrXSL & "  <xsl:for-each select='//IMPRESO'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:sort select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)' data-type='text' order='ascending'/>"
    wvarStrXSL = wvarStrXSL & " <xsl:if test='RAMOPCOD != """"' >"
    
    wvarStrXSL = wvarStrXSL & " <IMPRESO>"
    
    wvarStrXSL = wvarStrXSL & "<AGRUPAR><xsl:value-of select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)'/></AGRUPAR>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CIAASCOD'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='RAMOPCOD'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='POLIZANN'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='POLIZSEC'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CERTIPOL'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CERTIANN'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CERTISEC'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='OPERAPOL'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='FORMUDES'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='DOCUMTIP'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='DOCUMDAT'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CLIENAP1'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CLIENAP2'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CLIENNOM'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='NOMARCH'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='SWSUSCRI'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='MAIL'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CLAVE'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='SW-CLAVE'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='SWENDOSO'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='OPERATIP'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='ESTADO'/>"
    wvarStrXSL = wvarStrXSL & "    <xsl:copy-of select='CODESTAD'/>"
    wvarStrXSL = wvarStrXSL & " </IMPRESO>"
    wvarStrXSL = wvarStrXSL & " </xsl:if >"
   
    wvarStrXSL = wvarStrXSL & "   </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & " </Request>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function


Private Function insertScheduler(ByRef pvarNodo As NODOINSERTASCH, ByVal pvarFECHA As String)
    '
    Dim wvarCodusu          As String
    Dim wvarAplicacion      As String
    Dim wvarDataIn          As String
    '
    wvarAplicacion = "NBWS_ePoliza_Exe"
    
    wvarCodusu = "NBWSFILLSCH"
    wvarDataIn = "<CIAASCOD>" & pvarNodo.CIAASCOD & "</CIAASCOD>" & _
                 "<RAMOPCOD>" & pvarNodo.RAMOPCOD & "</RAMOPCOD>" & _
                 "<POLIZANN>" & pvarNodo.POLIZANN & "</POLIZANN>" & _
                 "<POLIZSEC>" & pvarNodo.POLIZSEC & "</POLIZSEC>" & _
                 "<CERTIPOL>" & pvarNodo.CERTIPOL & "</CERTIPOL>" & _
                 "<CERTIANN>" & pvarNodo.CERTIANN & "</CERTIANN>" & _
                 "<CERTISEC>" & pvarNodo.CERTISEC & "</CERTISEC>" & _
                 "<OPERAPOL>" & pvarNodo.OPERAPOL & "</OPERAPOL>" & _
                 "<DOCUMTIP>" & pvarNodo.DOCUMTIP & "</DOCUMTIP>" & _
                 "<DOCUMDAT>" & pvarNodo.DOCUMDAT & "</DOCUMDAT>" & _
                 "<CLIENAP1>" & pvarNodo.CLIENAP1 & "</CLIENAP1>" & _
                 "<CLIENAP2>" & pvarNodo.CLIENAP2 & "</CLIENAP2>" & _
                 "<CLIENNOM>" & pvarNodo.CLIENNOM & "</CLIENNOM>" & _
                 "<ARCHIVOS>" & pvarNodo.NOMARCH & "</ARCHIVOS>" & _
                 "<FORMUDES>" & pvarNodo.FORMUDES & "</FORMUDES>" & _
                 "<SWSUSCRI>" & pvarNodo.SWSUSCRI & "</SWSUSCRI>" & _
                 "<MAIL>" & pvarNodo.MAIL & "</MAIL>" & _
                 "<CLAVE>" & pvarNodo.CLAVE & "</CLAVE>" & _
                 "<SW-CLAVE>" & pvarNodo.SWCLAVE & "</SW-CLAVE>" & _
                 "<SWENDOSO>" & pvarNodo.SWENDOSO & "</SWENDOSO>" & _
                 "<OPERATIP>" & pvarNodo.OPERATIP & "</OPERATIP>" & _
                 "<ESTADO>" & pvarNodo.ESTADO & "</ESTADO>" & _
                 "<CODESTAD>" & pvarNodo.CODESTAD & "</CODESTAD>"
  
   
  
 
   wvarDataIn = Replace(wvarDataIn, "&", "")
   insertScheduler = insertBandejaSCH(wvarAplicacion, wvarCodusu, wvarDataIn, pvarFECHA)
   
  
   
    
End Function



Private Function insertBandejaSCH(pvarAplicacion As String, pvarCODUSU As String, pvarDataIn As String, pvarFECHA As String) As Boolean
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarCodusu          As String
    Dim wvarAplicacion      As String
    Dim wvarDataIn          As String
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBACTIONS)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsBandejaSCH
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@APLIC", adVarChar, adParamInput, 20, pvarAplicacion)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SOLICITUD", adInteger, adParamInput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PASO", adInteger, adParamInput, , 100)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHAEXEC", adChar, adParamInput, 10, pvarFECHA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DATAIN", adChar, adParamInput, 5000, Left(pvarDataIn, 5000))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@COD_USU", adChar, adParamInput, 20, pvarCODUSU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        insertBandejaSCH = True
    Else
        insertBandejaSCH = False
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Function insertLog(ByVal pvarCODOP As String, ByVal pvarDescripcion As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarReqRespInTxt    As Boolean
    Dim mvarDebugCode       As Integer
    '
    
    wvarReqRespInTxt = False
    '
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\" & mcteEPolizaConfig
    '1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = CInt(wobjXMLConfig.selectSingleNode("//DEBUG").Text)
    '
    If InStr(1, UCase(pvarDescripcion), "<REQUEST>", vbTextCompare) > 0 Or InStr(1, UCase(pvarDescripcion), "<RESPONSE>", vbTextCompare) > 0 Then
        wvarReqRespInTxt = True
    End If
    '
    If mvarDebugCode >= 1 And Not wvarReqRespInTxt Then
        debugToBD pvarCODOP, pvarDescripcion
    End If
    If mvarDebugCode = 3 Or (mvarDebugCode = 2 And Not wvarReqRespInTxt) Then
        debugToFile pvarCODOP, pvarDescripcion
    End If
    '
End Function

Private Function debugToBD(ByVal pvarCODOP As String, ByVal pvarDescripci�n As String) As Boolean
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBLOG)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsLog
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, Left(pvarCODOP, 6))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DESCRIPCION", adChar, adParamInput, 100, Left(pvarDescripci�n, 100))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        debugToBD = True
    Else
        debugToBD = False
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Sub debugToFile(ByVal pvarCODOP As String, ByVal pvarDescripcion As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarText            As String
    Dim wvarFileName        As String
    Dim wvarNroArch         As Long
    '
    wvarText = "(" & Date & "-" & Time & "):" & pvarDescripcion
        
    wvarFileName = "debug-" & pvarCODOP & "-" & Year(Date) & Right("00" & Month(Date), 2) & Right("00" & Day(Date), 2) & ".log"
    wvarNroArch = FreeFile()
    Open App.Path & "\DEBUG\" & wvarFileName For Append As #1
    Write #1, wvarText
    Close #1
    '
End Sub







Private Sub ObjectControl_Activate()

    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub












