Attribute VB_Name = "ModEncryptDecrypt"
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : ModEncryptDecrypt
' File Name : ModEncryptDecrypt.bas
' Creation Date: 29/11/2005
' Programmer : Fernando Osores / Lucas De Merlier
' Abstract :    Encripta y Desencripta un string utilizando el secreto indicado.
' *****************************************************************
Option Explicit

' Secreto. Debe ser fijo. No puede ser modificado.
Public Const mcteClave = "Rg-Hs+Fo$Lg!2009=DeadLine�cAm#HSBC"

' *****************************************************************
' Function : CapicomEncrypt
' Abstract : Encripta un string.
' Synopsis : CapicomEncrypt(ByVal strText As String) As String
' *****************************************************************
Public Function CapicomEncrypt(ByVal strText As String) As String
On Error GoTo ErrorHandler
    Dim objCapicom As CAPICOM.EncryptedData
    Set objCapicom = New CAPICOM.EncryptedData

    If strText <> "" Then

        objCapicom.Algorithm = CAPICOM_ENCRYPTION_ALGORITHM_RC4
        objCapicom.SetSecret mcteClave
        objCapicom.Content = strText
        CapicomEncrypt = StrToHex(objCapicom.Encrypt())

    Else

        CapicomEncrypt = ""

    End If

    Set objCapicom = Nothing

    Exit Function
ErrorHandler:
    Set objCapicom = Nothing
    CapicomEncrypt = Err.Description & ": " & strText
End Function

' *****************************************************************
' Function : CapicomDecrypt
' Abstract : Desencripta un string
' Synopsis : CapicomDecrypt(ByVal strText As String) As String
' *****************************************************************
Public Function CapicomDecrypt(ByVal strText As String) As String
On Error GoTo ErrorHandler
    Dim objCapicom As CAPICOM.EncryptedData
    Set objCapicom = New CAPICOM.EncryptedData

    If strText <> "" Then

        objCapicom.Algorithm = CAPICOM_ENCRYPTION_ALGORITHM_RC4
        objCapicom.SetSecret mcteClave
        objCapicom.Decrypt (HexToStr(strText))
        CapicomDecrypt = objCapicom.Content

    Else

        CapicomDecrypt = ""

    End If

    Set objCapicom = Nothing
    Exit Function
ErrorHandler:
    Set objCapicom = Nothing
    CapicomDecrypt = Err.Description & ": " & strText
End Function

Public Function HexToStr(strHex As String) As String

    Dim strAux As String
    Dim i As Integer
    
    strAux = ""
    
    For i = 1 To Len(strHex) Step 2
        strAux = strAux + Chr("&H" + Mid(strHex, i, 2))
    Next

    HexToStr = strAux

End Function

Public Function StrToHex(str As String) As String

    Dim strAux As String
    Dim i As Integer
    
    strAux = ""
    
    For i = 1 To Len(str)
        If Len(Hex(Asc(Mid(str, i, 1)))) = 1 Then
            strAux = strAux + "0"
        End If
        strAux = strAux + Hex(Asc(Mid(str, i, 1)))
    Next
    StrToHex = strAux

End Function

