VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_getResuList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
'TODO: poner COPYRIGHT
' *****************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_getResuList"
Const mcteParam_RAMOPCOD        As String = "//RAMOPCOD"
Const mcteParam_POLIZANN        As String = "//POLIZANN"
Const mcteParam_POLIZSEC        As String = "//POLIZSEC"
Const mcteParam_CERTIPOL        As String = "//CERTIPOL"
Const mcteParam_CERTIANN        As String = "//CERTIANN"
Const mcteParam_CERTISEC        As String = "//CERTISEC"
Const mcteParam_SUPLENUM        As String = "//SUPLENUM"

'
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLRespuestas   As MSXML2.DOMDocument
    '
    Dim wvarCount As Integer
    '
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    Dim wvarSUPLENUM        As String
    '
    Dim wvarMesHoy          As String
    Dim wvarDiaHoy          As String
    '
    Dim wvarPeriodo1        As String
    Dim wvarPeriodo2        As String
    Dim wvarPeriodo3        As String

    Dim wvarIMPRESO1        As String
    Dim wvarIMPRESO2        As String
    Dim wvarIMPRESO3        As String

    Dim wvarUltimo          As String
    Dim wvarIMPRESO         As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Carga XML de entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        wvarStep = 20
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarPOLIZANN = .selectSingleNode(mcteParam_POLIZANN).Text
        wvarPOLIZSEC = .selectSingleNode(mcteParam_POLIZSEC).Text
        wvarCERTIPOL = .selectSingleNode(mcteParam_CERTIPOL).Text
        wvarCERTIANN = .selectSingleNode(mcteParam_CERTIANN).Text
        wvarCERTISEC = .selectSingleNode(mcteParam_CERTISEC).Text
        wvarSUPLENUM = .selectSingleNode(mcteParam_SUPLENUM).Text
        '
    End With
    '
    wvarStep = 30
    'Calcula los per�odos
    wvarMesHoy = Right("00" & Month(Now()), 2)
    wvarDiaHoy = Right("00" & Day(Now()), 2)
    '
    If wvarMesHoy < 6 Then
        Call fncCalculaPeriodos1(wvarPeriodo1, wvarPeriodo2, wvarPeriodo3)
    Else
        If wvarMesHoy = 6 Then
            If wvarDiaHoy = 30 Then
                Call fncCalculaPeriodos2(wvarPeriodo1, wvarPeriodo2, wvarPeriodo3)
            Else
                Call fncCalculaPeriodos1(wvarPeriodo1, wvarPeriodo2, wvarPeriodo3)
            End If
        Else
            Call fncCalculaPeriodos2(wvarPeriodo1, wvarPeriodo2, wvarPeriodo3)
        End If
    End If
    '
'DA - 14/07/2009: se decidi� entre AG y DA y MC que se mostrar�n las impresoritas hardcodeadas x un tema de performance

'''''''''''''''''    wvarStep = 40
'''''''''''''''''    wvarRequest = wvarRequest & "<Request id=""1"" actionCode=""lbaw_OVMWGen"" ciaascod=""0020"">" & _
'''''''''''''''''                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
'''''''''''''''''                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
'''''''''''''''''                                "    <coldViewMetadata>poli=" & wvarRAMOPCOD & "-" & wvarPOLIZANN & "-" & wvarPOLIZSEC & "-" & wvarCERTIPOL & "-" & wvarCERTIANN & "-" & wvarCERTISEC & "-" & wvarSUPLENUM & ";fhas=" & wvarPeriodo1 & "</coldViewMetadata>" & _
'''''''''''''''''                                "</Request>"
'''''''''''''''''    wvarStep = 50
'''''''''''''''''    wvarRequest = wvarRequest & "<Request id=""2"" actionCode=""lbaw_OVMWGen"" ciaascod=""0020"">" & _
'''''''''''''''''                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
'''''''''''''''''                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
'''''''''''''''''                                "    <coldViewMetadata>poli=" & wvarRAMOPCOD & "-" & wvarPOLIZANN & "-" & wvarPOLIZSEC & "-" & wvarCERTIPOL & "-" & wvarCERTIANN & "-" & wvarCERTISEC & "-" & wvarSUPLENUM & ";fhas=" & wvarPeriodo2 & "</coldViewMetadata>" & _
'''''''''''''''''                                "</Request>"
'''''''''''''''''    wvarStep = 60
'''''''''''''''''    wvarRequest = wvarRequest & "<Request id=""3"" actionCode=""lbaw_OVMWGen"" ciaascod=""0020"">" & _
'''''''''''''''''                                "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" & _
'''''''''''''''''                                "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" & _
'''''''''''''''''                                "    <coldViewMetadata>poli=" & wvarRAMOPCOD & "-" & wvarPOLIZANN & "-" & wvarPOLIZSEC & "-" & wvarCERTIPOL & "-" & wvarCERTIANN & "-" & wvarCERTISEC & "-" & wvarSUPLENUM & ";fhas=" & wvarPeriodo3 & "</coldViewMetadata>" & _
'''''''''''''''''                                "</Request>"
'''''''''''''''''    '
'''''''''''''''''    wvarStep = 70
'''''''''''''''''    'Ejecuta la funci�n Multithreading
'''''''''''''''''    wvarRequest = "<Request>" & wvarRequest & "</Request>"
'''''''''''''''''    Call cmdp_ExecuteTrnMulti("lbaw_OVMWGen", "lbaw_OVMWGen.biz", wvarRequest, wvarResponse)
'''''''''''''''''    '
'''''''''''''''''    wvarStep = 80
'''''''''''''''''    'Carga las respuestas
'''''''''''''''''    Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
'''''''''''''''''        wobjXMLRespuestas.async = False
'''''''''''''''''    Call wobjXMLRespuestas.loadXML(wvarResponse)
'''''''''''''''''    '
'''''''''''''''''    wvarStep = 90
'''''''''''''''''    'Verifica si pinch� el COM+
'''''''''''''''''    If wobjXMLRespuestas.selectSingleNode("//Response") Is Nothing Then
'''''''''''''''''        '
'''''''''''''''''        'Pinch� el COM+
'''''''''''''''''        Err.Description = "Fall� la ejecuci�n de la funci�n Multithreading."
'''''''''''''''''        GoTo ErrorHandler
'''''''''''''''''    Else
'''''''''''''''''        '
'''''''''''''''''        wvarStep = 100
'''''''''''''''''        If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']") Is Nothing Then
'''''''''''''''''            '
'''''''''''''''''            'TODO: Pinch� el COM+ para el per�odo 1. Ver qu� hacer.
'''''''''''''''''            '
'''''''''''''''''        Else
'''''''''''''''''            wvarStep = 110
'''''''''''''''''            If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']") Is Nothing Then
'''''''''''''''''                '
'''''''''''''''''                'TODO: Pinch� el COM+ para el per�odo 2. Ver qu� hacer.
'''''''''''''''''                '
'''''''''''''''''            Else
'''''''''''''''''                wvarStep = 120
'''''''''''''''''                If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='3']") Is Nothing Then
'''''''''''''''''                    '
'''''''''''''''''                    'TODO: Pinch� el COM+ para el per�odo 3. Ver qu� hacer.
'''''''''''''''''                    '
'''''''''''''''''                Else
'''''''''''''''''                    '
'''''''''''''''''                    'Anduvieron los 3 COM+
'''''''''''''''''                    wvarStep = 130
'''''''''''''''''                    wvarIMPRESO1 = ""
'''''''''''''''''                    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/retrieveReportListReturn//item") Is Nothing Then
'''''''''''''''''                        'Recupera el ITEM
'''''''''''''''''                        wvarUltimo = wobjXMLRespuestas.selectNodes("Response/Response[@id='1']/retrieveReportListReturn//item").length
'''''''''''''''''                        wvarIMPRESO1 = "<IMPRESO1><PERIODO>" & wvarPeriodo1 & "</PERIODO><ITEM>" & wobjXMLRespuestas.selectNodes("Response/Response[@id='1']/retrieveReportListReturn//item").Item(wvarUltimo).Text & "</ITEM></IMPRESO1>"
'''''''''''''''''                        '
'''''''''''''''''                    Else
'''''''''''''''''                        'no hay item
'''''''''''''''''                        wvarIMPRESO1 = "<IMPRESO1><PERIODO>" & wvarPeriodo1 & "</PERIODO><ITEM></ITEM></IMPRESO1>"
'''''''''''''''''                    End If
'''''''''''''''''                    '
'''''''''''''''''                    wvarStep = 140
'''''''''''''''''                    wvarIMPRESO2 = ""
'''''''''''''''''                    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/retrieveReportListReturn//item") Is Nothing Then
'''''''''''''''''                        'Recupera el ITEM
'''''''''''''''''                        wvarUltimo = wobjXMLRespuestas.selectNodes("Response/Response[@id='2']/retrieveReportListReturn//item").length
'''''''''''''''''                        wvarIMPRESO2 = "<IMPRESO2><PERIODO>" & wvarPeriodo2 & "</PERIODO><ITEM>" & wobjXMLRespuestas.selectNodes("Response/Response[@id='2']/retrieveReportListReturn//item").Item(wvarUltimo).Text & "</ITEM></IMPRESO2>"
'''''''''''''''''                    Else
'''''''''''''''''                        'no hay item
'''''''''''''''''                        wvarIMPRESO2 = "<IMPRESO2><PERIODO>" & wvarPeriodo2 & "</PERIODO><ITEM></ITEM></IMPRESO2>"
'''''''''''''''''                    End If
'''''''''''''''''                    '
'''''''''''''''''                    wvarStep = 150
'''''''''''''''''                    wvarIMPRESO3 = ""
'''''''''''''''''                    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='3']/retrieveReportListReturn//item") Is Nothing Then
'''''''''''''''''                        'Recupera el ITEM
'''''''''''''''''                        wvarUltimo = wobjXMLRespuestas.selectNodes("Response/Response[@id='3']/retrieveReportListReturn//item").length
'''''''''''''''''                        wvarIMPRESO3 = "<IMPRESO3><PERIODO>" & wvarPeriodo3 & "</PERIODO><ITEM>" & wobjXMLRespuestas.selectNodes("Response/Response[@id='3']/retrieveReportListReturn//item").Item(wvarUltimo).Text & "</ITEM></IMPRESO3>"
'''''''''''''''''                    Else
'''''''''''''''''                        'no hay item
'''''''''''''''''                        wvarIMPRESO3 = "<IMPRESO3><PERIODO>" & wvarPeriodo3 & "</PERIODO><ITEM></ITEM></IMPRESO3>"
'''''''''''''''''                    End If
'''''''''''''''''                    '
'''''''''''''''''                End If
'''''''''''''''''            End If
'''''''''''''''''        End If
'''''''''''''''''    End If
    '
    '
    wvarIMPRESO1 = "<IMPRESO1><PERIODO>" & wvarPeriodo1 & "</PERIODO></IMPRESO1>"
    wvarIMPRESO2 = "<IMPRESO2><PERIODO>" & wvarPeriodo2 & "</PERIODO></IMPRESO2>"
    wvarIMPRESO3 = "<IMPRESO3><PERIODO>" & wvarPeriodo3 & "</PERIODO></IMPRESO3>"
    '
    Response = "<Response><Estado resultado=""true"" mensaje=""""></Estado>" & wvarIMPRESO1 & wvarIMPRESO2 & wvarIMPRESO3 & "</Response>"
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLRespuestas = Nothing
    '
    wvarStep = 190
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLRespuestas = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    mobjCOM_Context.SetAbort
    IAction_Execute = 1
    '
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



