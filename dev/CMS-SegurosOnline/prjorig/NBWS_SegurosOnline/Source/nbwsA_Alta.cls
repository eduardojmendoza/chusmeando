VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_Alta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'******************************************************************************
'Fecha de Modificaci�n: 30/09/2011
'PPCR: 2011-00389
'Desarrollador: Gabriel D'Agnone
'Descripci�n: Anexo I - Se envia suscripcion al AIS
'******************************************************************************
'Fecha de Modificaci�n: 17/08/2011
'Ticket: 648546
'Desarrollador: Leonardo Ruiz
'Descripci�n: Demoledores de la Burocracia: diferenciar los mails cuando son
'por altas desde la OV o desde otro canal.
'******************************************************************************
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011.
' ALL RIGHTS RESERVED
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
' serious civil and criminal offence, which can result in heavy fines and payment
' of substantial damages.
'
' Nombre del Fuente: nbwsA_Alta.cls
' Fecha de Creaci�n: desconocido
' PPcR: desconocido
' Desarrollador: Desconocido
' Descripci�n: se agrega Copyright para cumplir con las normas de QA
'
'******************************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Archivo Configuracion CAI / CAP
Const mcteEMailTemplateConfig             As String = "NBWS_EnvioMailConfig.xml"


'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_Alta"

Const mcteParam_MAIL            As String = "//MAIL"
Const mcteParam_DOCUMTIP        As String = "//DOCUMTIP"
Const mcteParam_DOCUMDAT        As String = "//DOCUMDAT"
Const mcteParam_VIAINSCRIPCION  As String = "//VIAINSCRIPCION"
Const mcteParam_RESPONSABLEALTA As String = "//RESPONSABLEALTA"
Const mcteParam_CONFORMIDAD     As String = "//CONFORMIDAD"

'Constantes de XML de definiciones para SQL Generico
Const mcteParam_XMLSQLGENREGI As String = "P_NBWS_Registracion.xml"

'Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
Private mcteMsg_DESCRIPTION(0 To 12)  As String
Const mcteMsg_OK                As Integer = 0
Const mcteMsg_ERROR             As Integer = 1

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    Dim wobjXMLResponse             As MSXML2.DOMDocument
    
    Dim wobjXMLRespuesta1184        As MSXML2.DOMDocument
    Dim wobjXMLRespuesta1185        As MSXML2.DOMDocument
    Dim wobjXMLProductosAIS_List    As MSXML2.IXMLDOMNodeList
    Dim wobjXMLProductosAIS_Node    As MSXML2.IXMLDOMNode
    '
    Dim wobjRequestSQL              As String
    Dim wobjResponseSQL             As String
    Dim wobjRequestAIS              As String
    Dim wobjResponseAIS             As String
    '
    Dim wvarMail                    As String
    Dim wvarCAI                     As String
    Dim wvarDocumtip                As String
    Dim wvarDocumdat                As String
    Dim wvarRESPONSABLEALTA         As String
    Dim wvarVIAINSCRIPCION          As String
    Dim wvarCONFORMIDAD             As String
    Dim wvarError                   As String
    Dim wvarCAIEnc                  As String
    Dim wvarRequest                 As String
    Dim wvarResponse                As String
    Dim wvarRetVal                  As String
    
    'PARAMETROS LLAMADA 1185
    
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    Dim wvarSWSUSCRI        As String
    Dim wvarSWCONFIR        As String
   ' Dim wvarMail            As String
    Dim wvarCLAVE           As String
    Dim wvarSWCLAVE         As String
    Dim wvarSWTIPOSUS       As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarError = mcteMsg_OK
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_ERROR) = getMensaje(mcteClassName, "mcteMsg_ERROR")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    
    With wobjXMLRequest
        wvarStep = 25
        wvarMail = .selectSingleNode(mcteParam_MAIL).Text
        wvarStep = 30
        wvarDocumtip = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarStep = 35
        wvarDocumdat = .selectSingleNode(mcteParam_DOCUMDAT).Text
        wvarStep = 40
        wvarVIAINSCRIPCION = .selectSingleNode(mcteParam_VIAINSCRIPCION).Text
        wvarStep = 45
        wvarRESPONSABLEALTA = .selectSingleNode(mcteParam_RESPONSABLEALTA).Text
        wvarStep = 50
        wvarCONFORMIDAD = .selectSingleNode(mcteParam_CONFORMIDAD).Text
    End With
    
'***********************************************************************
' GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
'***********************************************************************
    
    wvarRequest = "<Request>" & _
                    "<DOCUMTIP>" & wvarDocumtip & "</DOCUMTIP>" & _
                    "<DOCUMDAT>" & wvarDocumdat & "</DOCUMDAT>" & _
                    "<MAIL>" & wvarMail & "</MAIL>" & _
                  "</Request>"
                  
    wvarStep = 60
              
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_sInicio")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
                        

    '
    wvarStep = 70
    'Carga las dos respuestas en un XML.
    Set wobjXMLRespuesta1184 = CreateObject("MSXML2.DOMDocument")
        wobjXMLRespuesta1184.async = False
    Call wobjXMLRespuesta1184.loadXML(wvarResponse)
    '
    wvarStep = 80
    'Verifica que no haya pinchado el COM+
    If wobjXMLRespuesta1184.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        '
        wvarStep = 90
        'SI NO PUEDO OBTENER RESPUESTA DEL 1184 (FALLO AIS O MQ)
        wvarRetVal = "ERROR"
        wvarError = mcteMsg_ERROR
        
        'Err.Description = "Response : fall� COM+ de Request 1184"
        'GoTo ErrorHandler
        '
    End If
    
    
    'Levanta en un listado cada producto del cliente
    
    wvarStep = 100
    Set wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes("//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and SWSUSCRI!='S' and MAS_DE_CINCO_CERT='N']")
    
    wvarStep = 110
    If wobjXMLProductosAIS_List.length > 0 Then
        wvarStep = 120
        wvarRequest = "<Request id=""1""  actionCode=""nbwsA_MQGenericoAIS"" >" & _
                            "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" & _
                            "<EPOLIZAS>"
        wvarStep = 130
      
       'Recorre p�lizas de clientes.
        For Each wobjXMLProductosAIS_Node In wobjXMLProductosAIS_List
        '
            wvarRAMOPCOD = wobjXMLProductosAIS_Node.selectSingleNode("RAMOPCOD").Text
            wvarPOLIZANN = wobjXMLProductosAIS_Node.selectSingleNode("POLIZANN").Text
            wvarPOLIZSEC = wobjXMLProductosAIS_Node.selectSingleNode("POLIZSEC").Text
            wvarCERTIPOL = wobjXMLProductosAIS_Node.selectSingleNode("CERTIPOL").Text
            wvarCERTIANN = wobjXMLProductosAIS_Node.selectSingleNode("CERTIANN").Text
            wvarCERTISEC = wobjXMLProductosAIS_Node.selectSingleNode("CERTISEC").Text
            wvarSWSUSCRI = "S"   ' "S"
            wvarCLAVE = ""
            wvarSWCLAVE = ""
            wvarSWTIPOSUS = "W"  ' "W" CUANDO ES SUSCRIPCION A SEGUROS ON LINE
           
                        
            wvarRequest = wvarRequest & "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" & _
                            "<RAMOPCOD>" & wvarRAMOPCOD & "</RAMOPCOD>" & _
                            "<POLIZANN>" & wvarPOLIZANN & "</POLIZANN>" & _
                            "<POLIZSEC>" & wvarPOLIZSEC & "</POLIZSEC>" & _
                            "<CERTIPOL>" & wvarCERTIPOL & "</CERTIPOL>" & _
                            "<CERTIANN>" & wvarCERTIANN & "</CERTIANN>" & _
                            "<CERTISEC>" & wvarCERTISEC & "</CERTISEC>" & _
                            "<SWSUSCRI>" & wvarSWSUSCRI & "</SWSUSCRI>" & _
                            "<MAIL>" & wvarMail & "</MAIL>" & _
                            "<CLAVE>" & wvarCLAVE & "</CLAVE>" & _
                            "<SW-CLAVE>" & wvarSWCLAVE & "</SW-CLAVE>" & _
                            "<SWTIPOSUS>" & wvarSWTIPOSUS & "</SWTIPOSUS>" & _
                            "</EPOLIZA>"
        
        Next
    
        wvarRequest = wvarRequest & "</EPOLIZAS></Request>"
        
        wvarRequest = "<Request>" & wvarRequest & "</Request>"
        
        wvarStep = 140
      
        Call cmdp_ExecuteTrnMulti("nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse)
 
         wvarStep = 150
        
         Set wobjXMLRespuesta1185 = CreateObject("MSXML2.DOMDocument")
             wobjXMLRespuesta1185.async = False
         Call wobjXMLRespuesta1185.loadXML(wvarResponse)
         
         
         wvarStep = 160
         If wobjXMLRespuesta1185.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
             '
             wvarStep = 160
             wvarRetVal = "ERROR"
             wvarError = mcteMsg_ERROR
             ' Err.Description = "Response : fall� COM+ de Request 1185"
             ' GoTo ErrorHandler
             '
         End If
    
    End If ' Fin si no tiene p�lizas a mandar por 1185
      
                
'***********************************************************************
' FIN ANEXO I
'***********************************************************************
  'SI DA ERROR LA CONSULTA 1184 O LA ACTUALIZACION 1185
  If wvarError = mcteMsg_OK Then
    
    '
    wvarStep = 170
    wvarCAI = UCase(generar_RNDSTR(8))
    wvarStep = 180
    wvarCAIEnc = CapicomEncrypt(wvarCAI)
    '
    wvarStep = 190
    wvarRequest = "<Request>" & _
                        "<DEFINICION>" & mcteParam_XMLSQLGENREGI & "</DEFINICION>" & _
                        "<MAIL>" & wvarMail & "</MAIL>" & _
                        "<CAI><![CDATA[" & wvarCAIEnc & "]]></CAI>" & _
                        "<DOCUMTIP>" & wvarDocumtip & "</DOCUMTIP>" & _
                        "<DOCUMDAT>" & wvarDocumdat & "</DOCUMDAT>" & _
                        "<RESPONSABLEALTA>" & wvarRESPONSABLEALTA & "</RESPONSABLEALTA>" & _
                        "<VIAINSCRIPCION>" & wvarVIAINSCRIPCION & "</VIAINSCRIPCION>" & _
                        "<CONFORMIDAD>" & wvarCONFORMIDAD & "</CONFORMIDAD>" & _
                   "</Request>"
    '
    wvarStep = 200
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    
    
    '
    wvarStep = 250
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML wvarResponse
    '
    'Analiza resultado del SP
    wvarStep = 260
    If Not wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wobjXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            wvarStep = 270
            If wobjXMLResponse.selectSingleNode("//RESULTADO").Text = "0" Then
                wvarRetVal = "OK"
                fncEnviarMail wvarMail, wvarCAI, wvarVIAINSCRIPCION
               
            Else
                wvarRetVal = "ERROR"
                wvarError = mcteMsg_ERROR
            End If
        End If
    End If
    '
    '
    
End If

    wvarStep = 290
    pvarResponse = "<Response>" & _
                        "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                        "<CODRESULTADO>" & wvarRetVal & "</CODRESULTADO>" & _
                        "<CODERROR>" & wvarError & "</CODERROR>" & _
                        "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                    "</Response>"
    '
    'Finaliza y libera objetos
    wvarStep = 700
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLRespuesta1184 = Nothing
    Set wobjXMLRespuesta1185 = Nothing
    
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

 

    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & wvarRequest, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXMLRespuesta1184 = Nothing
    Set wobjXMLRespuesta1185 = Nothing
    
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function




'GD: 17/11/2009 -  Envio de mail via SOAPMW

Private Function fncEnviarMail(pvarMail As String, pvarCAI As String, pvarVIAINSCRIPCION As String) As Boolean
    '
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarXMLResponseMdw  As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarResponseMdw     As String
    Dim mvarRequestMDW      As String
    Dim mvarTemplateCAI     As String
    Dim mvarMailPrueba      As String
    Dim mvarCopiaOculta     As String
    '
    
     'Se obtienen los par�metros desde XML
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\" & mcteEMailTemplateConfig
    
    'LR 17/08/2011 diferenciar los mails cuando son por altas desde la OV o desde otro canal
    If pvarVIAINSCRIPCION = "OV" Then
        mvarTemplateCAI = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/ENVIOCAIOV").Text
    Else
        mvarTemplateCAI = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/ENVIOCAI").Text
    End If
   
    'GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    ' correos con info no deseada accidentalmente
    
    If Not wobjXMLConfig.selectSingleNode("//MAILPRUEBA") Is Nothing Then
        mvarMailPrueba = wobjXMLConfig.selectSingleNode("//MAILPRUEBA").Text
    Else
        mvarMailPrueba = ""
    End If
    
    If Not wobjXMLConfig.selectSingleNode("//COPIAOCULTA") Is Nothing Then
        mvarCopiaOculta = wobjXMLConfig.selectSingleNode("//COPIAOCULTA").Text
    Else
        mvarCopiaOculta = ""
    End If
    
    'Se cambia envio de mail por medio de MQ.
    mvarRequestMDW = "<Request>" & _
                        "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" & _
                        "<Raiz>share:sendPdfReport</Raiz>" & _
                        "<files/>" & _
                        "<applicationId>NBWS</applicationId>" & _
                        "<reportId/>" & _
                        "<recipientTO>" & pvarMail & "</recipientTO>" & _
                        "<recipientsCC/>" & _
                        "<recipientsBCC>" & mvarCopiaOculta & "</recipientsBCC>" & _
                        "<templateFileName>" & mvarTemplateCAI & "</templateFileName>" & _
                        "<parametersTemplate>CLAVE=" & pvarCAI & "</parametersTemplate>" & _
                        "<from/>" & _
                        "<replyTO/>" & _
                        "<bodyText/>" & _
                        "<subject/>" & _
                        "<importance/>" & _
                        "<imagePathFile/>" & _
                        "<attachPassword/>" & _
                        "<attachFileName/>" & _
                    "</Request>"
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
    Call wobjClass.Execute(mvarRequestMDW, wvarResponseMdw, "")
    '
    Set wobjClass = Nothing
    '
    Set wvarXMLResponseMdw = CreateObject("MSXML2.DOMDocument")
        wvarXMLResponseMdw.async = False
        wvarXMLResponseMdw.loadXML (wvarResponseMdw)
    '
    If Not wvarXMLResponseMdw.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wvarXMLResponseMdw.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            If wvarXMLResponseMdw.selectSingleNode("//Response/faultstring") Is Nothing Then
                fncEnviarMail = True
            Else
                fncEnviarMail = False
            End If
        Else
            If Not wvarXMLResponseMdw.selectSingleNode("//Response/faultstring") Is Nothing Then
                fncEnviarMail = False
            Else
                fncEnviarMail = False
            End If
            fncEnviarMail = False
        End If
    Else
        fncEnviarMail = False
    End If
    '
    Set wobjClass = Nothing
    Set wvarXMLResponseMdw = Nothing
    Set wobjXMLConfig = Nothing
    
End Function

'GD 17-11-2009 Funci�n anterior de envio de CAI

Private Function fncEnviarMail_bak(pvarMail As String, pvarCAI As String) As Boolean
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarXMLRequest      As MSXML2.DOMDocument
    Dim wvarXMLResponse     As MSXML2.DOMDocument
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    Dim wobjXMLCODOPParam   As MSXML2.DOMDocument
    Dim wvarRellamar        As Boolean
    Dim wvarResponseAIS     As String
    Dim wvarCODOPDesc       As String
    '
    Set wobjXMLParametro = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametro.Load App.Path & "\XMLs\NBWSEnvioCAI.xml"
    '
    agregarParametro wobjXMLParametro, "%%CAI%%", pvarCAI
    '
    wobjXMLParametro.selectSingleNode("//TO").Text = pvarMail
    '
    wvarRequest = wobjXMLParametro.selectSingleNode("//Request").xml
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_EnviarMail")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    Set wvarXMLResponse = CreateObject("MSXML2.DOMDocument")
        wvarXMLResponse.async = False
        wvarXMLResponse.loadXML (wvarResponse)
    '
    If Not wvarXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wvarXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            fncEnviarMail_bak = True
        Else
            fncEnviarMail_bak = False
        End If
    Else
        fncEnviarMail_bak = False
    End If
    '
    Set wvarXMLResponse = Nothing
    Set wobjClass = Nothing
    Set wobjXMLParametro = Nothing
End Function

Private Sub agregarParametro(ByRef pobjXMLParametro As MSXML2.DOMDocument, pvarParam As String, pvarValor As String)
    Dim wobjXMLElement      As MSXML2.IXMLDOMElement
    Dim wobjXMLElementAux   As MSXML2.IXMLDOMElement
    Dim wobjXMLElementCda   As MSXML2.IXMLDOMCDATASection
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    
    Set wobjXMLElement = pobjXMLParametro.createElement("PARAMETRO")
    '
    Set wobjXMLElementAux = pobjXMLParametro.createElement("PARAM_NOMBRE")
    Set wobjXMLElementCda = pobjXMLParametro.createCDATASection(pvarParam)
        wobjXMLElementAux.appendChild wobjXMLElementCda
        wobjXMLElement.appendChild wobjXMLElementAux
    Set wobjXMLElementAux = pobjXMLParametro.createElement("PARAM_VALOR")
    Set wobjXMLElementCda = pobjXMLParametro.createCDATASection(pvarValor)
        wobjXMLElementAux.appendChild wobjXMLElementCda
    '
    wobjXMLElement.appendChild wobjXMLElementAux
    '
    pobjXMLParametro.selectSingleNode("//PARAMETROS").appendChild wobjXMLElement
    '
End Sub

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub


