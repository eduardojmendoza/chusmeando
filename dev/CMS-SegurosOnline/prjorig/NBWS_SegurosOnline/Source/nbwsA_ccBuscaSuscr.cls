VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_ccBuscaSuscr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "nbwsA_Transacciones.nbwsA_ccBuscaSuscr"

Const mcteParam_ACCION          As String = "//ACCION"
Const mcteParam_DOCUMDAT        As String = "//DOCUMDAT"
Const mcteParam_DOCUMTIP        As String = "//DOCUMTIP"
Const mcteParam_VIAINSCRIPCION  As String = "//VIAINSCRIPCION"
Const mcteParam_DEFINICION      As String = "DEFINICION"
Const mcteParam_XMLSQLGENERICO  As String = "P_NBWS_ClienteSuscripto.xml"

'Constantes de error
Private mcteMsg_DESCRIPTION(0 To 16)  As String
Const mcteMsg_OK                    As Integer = 0
Const mcteMsg_EXISTEUSR             As Integer = 1
Const mcteMsg_EXISTEDOC             As Integer = 2
Const mcteMsg_EXISTEMAIL            As Integer = 3
Const mcteMsg_NOPRODNBWS            As Integer = 4
Const mcteMsg_NOPRODNAVEXVEND       As Integer = 5
Const mcteMsg_NOPRODNAVEXCERT       As Integer = 6
Const mcteMsg_NOEXISTEUSR           As Integer = 7
Const mcteMsg_NOEXISTEUSRSQL        As Integer = 8
Const mcteMsg_NOEXISTEUSRAIS        As Integer = 9
Const mcteMsg_ERRCONSULTA           As Integer = 10
Const mcteMsg_ERRCONSULTASQL        As Integer = 11
Const mcteMsg_ERRCONSULTAAIS        As Integer = 12
Const mcteMsg_NOPRODNBWS_HAB        As Integer = 13
Const mcteMsg_NOPRODNBWS_POSITIVEID As Integer = 14
Const mcteMsg_POLIZAEXCLUIDA        As Integer = 15
Const mcteMsg_NO_RESULT_BAJA        As Integer = 16

Private Function IAction_Execute(ByVal pvarRequest As String, pvarResponse As String, ByVal ContextInfo As String) As Long
    Const wcteFnName                As String = "IAction_Execute"
    Dim wobjClass                   As HSBCInterfaces.IAction
    Dim wvarStep                    As Integer
    '
    Dim wobjXMLRequest              As MSXML2.DOMDocument 'XML con el request
    Dim wobjXMLResponseSQL          As MSXML2.DOMDocument 'XML con el response del SQL
    Dim wobjXMLResponseAIS          As MSXML2.DOMDocument 'XML con el response del AIS
    '
    Dim wobjNodo                    As MSXML2.IXMLDOMNode
    Dim wobjNodePoliza              As MSXML2.IXMLDOMNode
    Dim wobjNodeList_ProdHab        As MSXML2.IXMLDOMNodeList
    Dim wobjNodeList_ProdNav        As MSXML2.IXMLDOMNodeList
    '
    Dim wobjRequestSQL              As String
    Dim wobjResponseSQL             As String
    Dim wobjRequestAIS              As String
    Dim wobjResponseAIS             As String
    '
    Dim wvarACCION                  As String
    Dim wvarDocumtip                As String
    Dim wvarDocumdat                As String
    Dim wvarVIAINSCRIPCION          As String
    Dim wvarCLIENNOM                As String
    Dim wvarCLIENAP1                As String
    Dim wvarCLIENAP2                As String
    Dim wvarReturnMsg               As String
    Dim wvarError                   As String
    Dim wvarRespPolizas             As String
    Dim wvarDescNavegable           As String
    Dim wvarPERMITE_ALTA            As String
    Dim wvarFiltroBusq              As String
    Dim wvarRespuesta               As String
    Dim wvarExisteUsrSQL            As Boolean
    Dim wvarFalla                   As Boolean
    Dim wvarAgregar                 As Boolean
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializacion de variables
    wvarStep = 10
    wvarCLIENNOM = ""
    wvarCLIENAP1 = ""
    wvarCLIENAP2 = ""
    wvarError = mcteMsg_OK
    wvarExisteUsrSQL = False
    wvarReturnMsg = ""
    wvarFalla = False
    '
    'Definicion de mensajes de error
    wvarStep = 15
    mcteMsg_DESCRIPTION(mcteMsg_OK) = getMensaje(mcteClassName, "mcteMsg_OK")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEUSR) = getMensaje(mcteClassName, "mcteMsg_EXISTEUSR")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEDOC) = getMensaje(mcteClassName, "mcteMsg_EXISTEDOC")
    mcteMsg_DESCRIPTION(mcteMsg_EXISTEMAIL) = getMensaje(mcteClassName, "mcteMsg_EXISTEMAIL")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNBWS) = getMensaje(mcteClassName, "mcteMsg_NOPRODNBWS")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNAVEXVEND) = getMensaje(mcteClassName, "mcteMsg_NOPRODNAVEXVEND")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNAVEXCERT) = getMensaje(mcteClassName, "mcteMsg_NOPRODNAVEXCERT")
    mcteMsg_DESCRIPTION(mcteMsg_NOEXISTEUSR) = getMensaje(mcteClassName, "mcteMsg_NOEXISTEUSR")
    mcteMsg_DESCRIPTION(mcteMsg_NOEXISTEUSRSQL) = getMensaje(mcteClassName, "mcteMsg_NOEXISTEUSRSQL")
    mcteMsg_DESCRIPTION(mcteMsg_NOEXISTEUSRAIS) = getMensaje(mcteClassName, "mcteMsg_NOEXISTEUSRAIS")
    mcteMsg_DESCRIPTION(mcteMsg_ERRCONSULTA) = getMensaje(mcteClassName, "mcteMsg_ERRCONSULTA")
    mcteMsg_DESCRIPTION(mcteMsg_ERRCONSULTASQL) = getMensaje(mcteClassName, "mcteMsg_ERRCONSULTASQL")
    mcteMsg_DESCRIPTION(mcteMsg_ERRCONSULTAAIS) = getMensaje(mcteClassName, "mcteMsg_ERRCONSULTAAIS")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNBWS_HAB) = getMensaje(mcteClassName, "mcteMsg_NOPRODNBWS_HAB")
    mcteMsg_DESCRIPTION(mcteMsg_NOPRODNBWS_POSITIVEID) = getMensaje(mcteClassName, "mcteMsg_NOPRODNBWS_POSITIVEID")
    mcteMsg_DESCRIPTION(mcteMsg_POLIZAEXCLUIDA) = getMensaje(mcteClassName, "mcteMsg_POLIZAEXCLUIDA")
    mcteMsg_DESCRIPTION(mcteMsg_NO_RESULT_BAJA) = getMensaje(mcteClassName, "mcteMsg_NO_RESULT_BAJA")
    '
    'Carga del REQUEST
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML pvarRequest
    '
    'Obtiene parametros
    wvarStep = 30
    With wobjXMLRequest
        wvarACCION = .selectSingleNode(mcteParam_ACCION).Text
        wvarDocumtip = .selectSingleNode(mcteParam_DOCUMTIP).Text
        wvarDocumdat = .selectSingleNode(mcteParam_DOCUMDAT).Text
        If Not .selectSingleNode(mcteParam_VIAINSCRIPCION) Is Nothing Then
            wvarVIAINSCRIPCION = .selectSingleNode(mcteParam_VIAINSCRIPCION).Text
        Else
            wvarVIAINSCRIPCION = ""
        End If
    End With
    '
    '-----------------------------------------------------------------------------------
    'Paso 1 - SQL NBWS
    '-----------------------------------------------------------------------------------
    'Consulta SP de consulta de suscriptos mediante el SQL Generico de NBWS
    wvarStep = 40
    Set wobjNodo = wobjXMLRequest.createElement(mcteParam_DEFINICION)
    wobjNodo.Text = mcteParam_XMLSQLGENERICO
    wobjXMLRequest.selectSingleNode("//Request").appendChild wobjNodo.cloneNode(True)
    wvarStep = 45
    wobjRequestSQL = wobjXMLRequest.xml
    '
    wvarStep = 50
    Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_SQLGenerico")
    Call wobjClass.Execute(wobjRequestSQL, wobjResponseSQL, "")
    Set wobjClass = Nothing
    '
    wvarStep = 60
    Set wobjXMLResponseSQL = CreateObject("MSXML2.DOMDocument")
    wobjXMLResponseSQL.async = False
    wobjXMLResponseSQL.loadXML wobjResponseSQL
    '
    'Analiza resultado del SP
    wvarStep = 80
    If Not wobjXMLResponseSQL.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wobjXMLResponseSQL.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            '
            wvarStep = 90
            If wobjXMLResponseSQL.selectSingleNode("//CORRECTO").Text = "S" Then
                'Existe usuario en SQL de NBWS (COINCIDE: mail / tipo doc / nro doc)
                wvarExisteUsrSQL = True
            Else
                'No existe usuario con ese mail y dni, se analiza si existe usuario con alguno de
                'estos datos por separado
                wvarStep = 100
                If Trim(wobjXMLResponseSQL.selectSingleNode("//MAIL").Text) <> "" Then
                    'Existe un usuario con el mismo numero y tipo de documento, se informa mail
                    wvarStep = 110
                    wvarError = mcteMsg_EXISTEDOC
                    wvarReturnMsg = Trim(wobjXMLResponseSQL.selectSingleNode("//MAIL").Text)
                ElseIf Trim(wobjXMLResponseSQL.selectSingleNode("//DOCUMTIP").Text) <> "0" And Trim(wobjXMLResponseSQL.selectSingleNode("//DOCUMDAT").Text) <> "0" Then
                    'Existe un usuario con el mismo mail, se informa numero y tipo de documento
                    wvarStep = 120
                    wvarError = mcteMsg_EXISTEMAIL
                    wvarReturnMsg = Trim(wobjXMLResponseSQL.selectSingleNode("//DOCUMTIP").Text) & "|" & Trim(wobjXMLResponseSQL.selectSingleNode("//DOCUMDAT").Text)
                Else
                    wvarStep = 125
                    wvarError = mcteMsg_NOEXISTEUSRSQL
                End If
            End If
        Else
            wvarError = mcteMsg_ERRCONSULTASQL
        End If
    Else
        wvarError = mcteMsg_ERRCONSULTASQL
    End If
    'TODO: ac� tendr�a que devolver el estado de validaci�n de SQL
    '-----------------------------------------------------------------------------------
    'Paso 2 - AIS
    '-----------------------------------------------------------------------------------
    If wvarACCION = "A" Or (wvarError <> mcteMsg_NOEXISTEUSRSQL And wvarError <> mcteMsg_ERRCONSULTASQL) Then
        'Se recuperan de AIS los datos del tipo y numero de documento informados
        wvarStep = 130
        Set wobjClass = mobjCOM_Context.CreateInstance("nbwsA_Transacciones.nbwsA_sInicio")
        Call wobjClass.Execute(pvarRequest, wobjResponseAIS, "")
        Set wobjClass = Nothing
        '
        wvarStep = 140
        Set wobjXMLResponseAIS = CreateObject("MSXML2.DOMDocument")
        wobjXMLResponseAIS.async = False
        wobjXMLResponseAIS.loadXML wobjResponseAIS
        '
        wvarStep = 150
        If Not wobjXMLResponseAIS Is Nothing Then
            If Not wobjXMLResponseAIS.selectSingleNode("//Response/Estado") Is Nothing Then
                If Not wobjXMLResponseAIS.selectSingleNode("//Response/Estado[@resultado='true']") Is Nothing Then
                    '
                    'Recupera Nombre
                    wvarStep = 160
                    If Not wobjXMLResponseAIS.selectSingleNode("//CLIENNOM") Is Nothing Then
                        wvarCLIENNOM = wobjXMLResponseAIS.selectSingleNode("//CLIENNOM").Text
                    End If
                    'Recupera Apellido 1
                    wvarStep = 170
                    If Not wobjXMLResponseAIS.selectSingleNode("//CLIENAP1") Is Nothing Then
                        wvarCLIENAP1 = wobjXMLResponseAIS.selectSingleNode("//CLIENAP1").Text
                    End If
                    'Recupera Apellido 2
                    wvarStep = 180
                    If Not wobjXMLResponseAIS.selectSingleNode("//CLIENAP2") Is Nothing Then
                        wvarCLIENAP2 = wobjXMLResponseAIS.selectSingleNode("//CLIENAP2").Text
                    End If
                Else
                    'Error: No se encuentra cliente en AIS o se encuentra y no tiene p�lizas en estado vigente
                    wvarError = mcteMsg_NOEXISTEUSRAIS 'TODO: discriminar si no existe el cliente o si existe y no tiene p�lizas
                End If
            Else
                'Error: Error en la ejecuci�n del componente de AIS
                wvarError = mcteMsg_ERRCONSULTAAIS
            End If
        Else
            'Error: Error en la ejecuci�n del componente de AIS
            wvarError = mcteMsg_ERRCONSULTAAIS
        End If
    End If
    'TODO: ac� tendr�a que devolver el estado de validaci�n de AIS
    '-----------------------------------------------------------------------------------
    'Paso 3 - Generacion de Response
    '-----------------------------------------------------------------------------------
    wvarStep = 200
    'Si se ocurri� error en la consulta devuelve false
    If wvarError = mcteMsg_ERRCONSULTA Or wvarError = mcteMsg_ERRCONSULTASQL Or wvarError = mcteMsg_ERRCONSULTAAIS Then
        wvarStep = 210
        wvarFalla = True
    Else
        'Dependiendo de la accion, analiza y arma la salida
        If wvarACCION = "A" Then
            'Si es Alta analiza los productos
            wvarStep = 220
            If wvarExisteUsrSQL Or wvarError = mcteMsg_EXISTEMAIL Or wvarError = mcteMsg_EXISTEDOC Then
                'Si el usuario existe en NBWS, devuelve error
                wvarStep = 230
                If wvarExisteUsrSQL Then
                    wvarError = mcteMsg_EXISTEUSR
                End If
            Else
                'Obtiene los productos habilitados del listado de productos del AIS
                Set wobjNodeList_ProdHab = wobjXMLResponseAIS.selectNodes("//PRODUCTO[HABILITADO_NBWS='S']")
                '
                'Si no tiene productos habilitados por tabla de Productos Habilitados
                If wobjNodeList_ProdHab.length = 0 Then
                    wvarStep = 240
                    If wobjXMLResponseAIS.selectNodes("//PRODUCTO[HABILITADO_NBWS='N']").length <> 0 Then
                        'DA - 24/08/2009: hay productos vigentes en AIS pero est�n NO HABILITADOS por tabla de Productos Habilitados
                        wvarStep = 241
                        wvarError = mcteMsg_NOPRODNBWS_HAB
                    Else
                        'DA - 24/08/2009: NO hay productos en AIS
                        wvarStep = 242
                        wvarError = mcteMsg_NOPRODNBWS
                    End If
                    
                Else
                    'Recorre cada producto, asociandoles una descripcion de si son o no navegables y porque
                    wvarStep = 250
                    For Each wobjNodePoliza In wobjNodeList_ProdHab

                        wvarDescNavegable = ""
                        'Si la poliza esta marcada con X significa que la misma tiene mas de 1 certificado y ya
                        'fue procesada, por lo tanto se saltea
                        wvarStep = 260
                        If InStr(wobjNodePoliza.selectSingleNode("HABILITADO_NAVEGACION").Text, "X") = 0 Then
                            '
                            'DA - 31/08/2009: pregunta si est� exclu�da
                            wvarStep = 261
                            If wobjNodePoliza.selectSingleNode("POLIZA_EXCLUIDA").Text = "S" Then
                                wvarDescNavegable = "El cliente posee �nicamente p�lizas que no estar�n inclu�das en el servicio por razones comerciales por lo que no es posible continuar con la solicitud de alta."
                            Else
                                wvarStep = 262
                                If wobjNodePoliza.selectSingleNode("HABILITADO_NAVEGACION").Text = "N" Then
                                    '
                                    'DA - 05/11/2009: para evitar mostrar TODOS los certificados
                                    If wobjNodePoliza.selectSingleNode("TIPOPROD").Text = "G" Then
                                        wvarFiltroBusq = "//PRODUCTO[" & _
                                                         "./RAMOPCOD='" & wobjNodePoliza.selectSingleNode("RAMOPCOD").Text & "'" & _
                                                         "and ./POLIZANN='" & wobjNodePoliza.selectSingleNode("POLIZANN").Text & "'" & _
                                                         "and ./POLIZSEC='" & wobjNodePoliza.selectSingleNode("POLIZSEC").Text & "'" & _
                                                         "and ./TIPOPROD='G']"
                                        wvarStep = 263
                                        Set wobjNodeList_ProdNav = wobjXMLResponseAIS.selectNodes(wvarFiltroBusq)
                                        '
                                        wvarStep = 264
                                        If wobjNodeList_ProdNav.length > 1 Then
                                            wvarDescNavegable = "El cliente posee p�lizas inclu�das en el servicio pero han sido adquiridas a trav�s de un productor o broker por lo que no es posible continuar con la solicitud de alta."
                                        End If
                                        wvarStep = 265
                                        'Marca dentro de wobjNodeList_ProdHab todos los certificados con X para evitar que se reprocesen y as� salga agrupado en una sola l�nea cuando se trate de una p�liza con mas de un certificado.
                                        fncMarcarPolizas wobjNodeList_ProdHab, wobjNodePoliza
                                    Else
                                        wvarDescNavegable = "El cliente posee p�lizas inclu�das en el servicio pero han sido adquiridas a trav�s de un productor o broker por lo que no es posible continuar con la solicitud de alta."
                                    End If
                                    '
                                Else
                                    wvarStep = 266
                                    If wobjNodePoliza.selectSingleNode("TIPOPROD").Text = "G" Then
                                        'Verifica si la poliza tiene mas de 5 certificados
                                        wvarStep = 270
                                        wvarFiltroBusq = "//PRODUCTO[" & _
                                                         "./RAMOPCOD='" & wobjNodePoliza.selectSingleNode("RAMOPCOD").Text & "'" & _
                                                         "and ./POLIZANN='" & wobjNodePoliza.selectSingleNode("POLIZANN").Text & "'" & _
                                                         "and ./POLIZSEC='" & wobjNodePoliza.selectSingleNode("POLIZSEC").Text & "'" & _
                                                         "and ./TIPOPROD='G']"
                                        Set wobjNodeList_ProdNav = wobjXMLResponseAIS.selectNodes(wvarFiltroBusq)
                                        '
                                        wvarStep = 271
                                        If wobjNodeList_ProdNav.length > 5 Then
                                            wvarDescNavegable = "El cliente posee �nicamente p�lizas que no estar�n inclu�das en el servicio por razones comerciales por lo que no es posible continuar con la solicitud de alta."
                                        End If
                                        'Marca dentro de wobjNodeList_ProdHab todos los certificados con X para evitar que se reprocesen.
                                        fncMarcarPolizas wobjNodeList_ProdHab, wobjNodePoliza
                                    End If
                                    '
                                End If
                                '
                            End If
                            '
                            wvarPERMITE_ALTA = "N"
                            If wvarDescNavegable = "" Then
                                wvarDescNavegable = "La p�liza es navegable"
                                wvarPERMITE_ALTA = "S"
                            End If
                            '
                            wvarStep = 280
                            
                            wvarRespPolizas = wvarRespPolizas & _
                                            "<POLIZA>" & _
                                                "<RAMOPCOD>" & wobjNodePoliza.selectSingleNode("RAMOPCOD").Text & "</RAMOPCOD>" & _
                                                "<POLIZANN>" & wobjNodePoliza.selectSingleNode("POLIZANN").Text & "</POLIZANN>" & _
                                                "<POLIZSEC>" & wobjNodePoliza.selectSingleNode("POLIZSEC").Text & "</POLIZSEC>" & _
                                                "<CERTIPOL>" & wobjNodePoliza.selectSingleNode("CERTIPOL").Text & "</CERTIPOL>" & _
                                                "<CERTIANN>" & wobjNodePoliza.selectSingleNode("CERTIANN").Text & "</CERTIANN>" & _
                                                "<CERTISEC>" & wobjNodePoliza.selectSingleNode("CERTISEC").Text & "</CERTISEC>" & _
                                                "<POSITIVEID>" & wobjNodePoliza.selectSingleNode("POSITIVEID").Text & "</POSITIVEID>" & _
                                                "<TIPOPROD>" & wobjNodePoliza.selectSingleNode("TIPOPROD").Text & "</TIPOPROD>" & _
                                                "<HABILITADO_NBWS>" & wobjNodePoliza.selectSingleNode("HABILITADO_NBWS").Text & "</HABILITADO_NBWS>" & _
                                                "<RESULTADO_VALIDACION>" & wvarDescNavegable & "</RESULTADO_VALIDACION>" & _
                                                "<PERMITE_ALTA>" & wvarPERMITE_ALTA & "</PERMITE_ALTA>" & _
                                            "</POLIZA>"
                        End If
                    Next
                End If
            End If
            wvarStep = 290
            If wvarError = mcteMsg_OK Or wvarError = mcteMsg_NOEXISTEUSRSQL Then
                wvarStep = 300
                wvarRespuesta = "<CODRESULTADO>OK</CODRESULTADO>" & _
                                "<CODERROR>" & wvarError & "</CODERROR>" & _
                                "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                                "<DESCRIPCION>" & wvarReturnMsg & "</DESCRIPCION>" & _
                                "<POLIZAS>" & wvarRespPolizas & "</POLIZAS>" & _
                                "<CLIENNOM>" & wvarCLIENNOM & "</CLIENNOM>" & _
                                "<CLIENAP1>" & wvarCLIENAP1 & "</CLIENAP1>" & _
                                "<CLIENAP2>" & wvarCLIENAP2 & "</CLIENAP2>" & _
                                wobjXMLResponseAIS.selectSingleNode("//Response_XML").xml
            Else
                wvarStep = 310
                wvarRespuesta = "<CODRESULTADO>ERROR</CODRESULTADO>" & _
                                "<CODERROR>" & wvarError & "</CODERROR>" & _
                                "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                                "<DESCRIPCION>" & wvarReturnMsg & "</DESCRIPCION>"
            End If
        Else
            'Si es Baja o Consulta la salida es igual
            wvarStep = 330
            If wvarExisteUsrSQL Then
                wvarStep = 340
                wvarRespuesta = "<CODRESULTADO>OK</CODRESULTADO>" & _
                                "<CODERROR>" & wvarError & "</CODERROR>" & _
                                "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                                "<DESCRIPCION>" & wvarReturnMsg & "</DESCRIPCION>" & _
                                "<CLIENNOM>" & wvarCLIENNOM & "</CLIENNOM>" & _
                                "<CLIENAP1>" & wvarCLIENAP1 & "</CLIENAP1>" & _
                                "<CLIENAP2>" & wvarCLIENAP2 & "</CLIENAP2>"
            Else
                wvarStep = 350
                'DA - 22/09/2009: se agrega esto para mejorar el mensaje al usuario de call centre.
                If wvarVIAINSCRIPCION = "CC" Then
                    wvarError = mcteMsg_NO_RESULT_BAJA
                End If
                '
                wvarRespuesta = "<CODRESULTADO>ERROR</CODRESULTADO>" & _
                                "<CODERROR>" & wvarError & "</CODERROR>" & _
                                "<MSGRESULTADO>" & mcteMsg_DESCRIPTION(wvarError) & "</MSGRESULTADO>" & _
                                "<DESCRIPCION>" & wvarReturnMsg & "</DESCRIPCION>"
            End If
        End If
    End If
    '
    wvarStep = 360
    If wvarFalla Then
        wvarStep = 370
        pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "false" & Chr(34) & " estado=" & Chr(34) & wvarError & Chr(34) & " mensaje=" & Chr(34) & mcteMsg_DESCRIPTION(wvarError) & Chr(34) & "/>" & _
                        "</Response>"
    Else
        wvarStep = 380
         pvarResponse = "<Response>" & _
                            "<Estado resultado=" & Chr(34) & "true" & Chr(34) & " estado=" & Chr(34) & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & _
                            wvarRespuesta & _
                        "</Response>"
    End If
    '
    'Finaliza y libera objetos
    wvarStep = 390
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponseSQL = Nothing
    Set wobjXMLResponseAIS = Nothing
    Set wobjNodo = Nothing
    Set wobjNodePoliza = Nothing
    Set wobjNodeList_ProdHab = Nothing
    Set wobjNodeList_ProdNav = Nothing
    '
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponseSQL = Nothing
    Set wobjXMLResponseAIS = Nothing
    Set wobjNodo = Nothing
    Set wobjNodePoliza = Nothing
    Set wobjNodeList_ProdHab = Nothing
    Set wobjNodeList_ProdNav = Nothing
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
      
Private Sub fncMarcarPolizas(ByRef pobjNodeList As MSXML2.IXMLDOMNodeList, ByRef pobjNodePoliza As MSXML2.IXMLDOMNode)
    '
    Dim mobjNode As MSXML2.IXMLDOMNode
    '
    For Each mobjNode In pobjNodeList
        If mobjNode.selectSingleNode("RAMOPCOD").Text = pobjNodePoliza.selectSingleNode("RAMOPCOD").Text _
           And mobjNode.selectSingleNode("POLIZANN").Text = pobjNodePoliza.selectSingleNode("POLIZANN").Text _
           And mobjNode.selectSingleNode("POLIZSEC").Text = pobjNodePoliza.selectSingleNode("POLIZSEC").Text _
           And mobjNode.selectSingleNode("TIPOPROD").Text = pobjNodePoliza.selectSingleNode("TIPOPROD").Text Then
            'DA - 24/11/2009: Defect 62 de Mercury
            mobjNode.selectSingleNode("HABILITADO_NAVEGACION").Text = mobjNode.selectSingleNode("HABILITADO_NAVEGACION").Text & "X"
        End If
    Next
End Sub
      
Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub




