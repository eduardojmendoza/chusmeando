Attribute VB_Name = "ModGeneral"
Option Explicit

' UDL a la base de datos.
Public Const gcteDBLOG As String = "lbawA_OfVirtualLBA.udl" '"OVNYL_cotizaciones.udl"
Public Const gcteDBCAM As String = "camA_OficinaVirtual.udl"
Public Const gcteDBACTIONS As String = "lbaw_SCH2Fwk.udl"



' Parametros XML de Configuracion
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"
Public Const wcteProductosHabilitados   As String = "XMLs\ProductosHabilitados.xml"
Public Const wcteNBWS_TempFilesServer   As String = "NBWS_TempFilesServer.xml"

Public Const gcteClassMQConnection As String = "WD.Frame2MQ"


Public Function MidAsString(pvarStringCompleto As String, ByRef pvarActualCounter As Long, pvarLongitud As Long) As String
    MidAsString = Mid(pvarStringCompleto, pvarActualCounter, pvarLongitud)
    pvarActualCounter = pvarActualCounter + pvarLongitud
End Function

Public Function CompleteZero(pvarString As String, pvarLongitud As Long) As String
    Dim wvarCounter As Long
    Dim wvarstrTemp As String
    For wvarCounter = 1 To pvarLongitud
        wvarstrTemp = wvarstrTemp & "0"
    Next
    CompleteZero = Right(wvarstrTemp & pvarString, pvarLongitud)
End Function

Public Function GetErrorInformacionDato(pobjXMLContenedor As IXMLDOMNode, Optional pvarPathDato As String, Optional pvarTipoDato As String, Optional pobjLongitud As IXMLDOMNode, Optional pobjDecimales As IXMLDOMNode, Optional pobjDefault As IXMLDOMNode, Optional pobjObligatorio As IXMLDOMNode) As String
    Dim wobjNodoValor As IXMLDOMNode
    Dim wvarDatoValue As String
    Dim wvarCounter As Long
    Dim wvarCampoNumerico As Double
    Dim wvarIsDatoObligatorio As Boolean
    
    If Not pobjObligatorio Is Nothing Then
        If pobjObligatorio.Text = "SI" Then
            'Es un Dato Obligatorio
            wvarIsDatoObligatorio = True
            Set wobjNodoValor = pobjXMLContenedor.selectSingleNode("./" & Mid(pvarPathDato, 3))
            If wobjNodoValor Is Nothing Then
                GetErrorInformacionDato = "Nodo Obligatorio " & Mid(pvarPathDato, 3) & " NO INFORMADO"
                GoTo ClearObjects:
            End If
        End If
    End If
    
    Set wobjNodoValor = pobjXMLContenedor.selectSingleNode("./" & Mid(pvarPathDato, 3))
    If wobjNodoValor Is Nothing Then
        If Not pobjDefault Is Nothing Then wvarDatoValue = pobjDefault.Text
    Else
        wvarDatoValue = wobjNodoValor.Text
    End If
    '
    Select Case pvarTipoDato
        Case "TEXTO", "TEXTOIZQUIERDA": 'Dato del Tipo String
            If wvarIsDatoObligatorio And Trim(wvarDatoValue) = "" Then
                GetErrorInformacionDato = "Campo Obligatorio " & Mid(pvarPathDato, 3) & " SIN VALOR INGRESADO"
                GoTo ClearObjects
            End If
        Case "ENTERO", "DECIMAL": 'Dato del Tipo Numerico
            If wvarDatoValue = "" Then wvarDatoValue = "0"
            If Not IsNumeric(wvarDatoValue) Then
                GetErrorInformacionDato = "Campo " & Mid(pvarPathDato, 3) & " CON FORMATO INVALIDO (Valor Informado: " & wvarDatoValue & ". Requerido: Numerico)"
                GoTo ClearObjects
            Else
                If wvarIsDatoObligatorio And CDbl(wvarDatoValue) = 0 Then
                    GetErrorInformacionDato = "Campo Obligatorio " & Mid(pvarPathDato, 3) & " SIN VALOR INGRESADO"
                    GoTo ClearObjects
                End If
            End If
        Case "FECHA": 'Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
            If wvarIsDatoObligatorio And Not (wvarDatoValue Like "*/*/*") Then
                GetErrorInformacionDato = "Campo Obligatorio " & Mid(pvarPathDato, 3) & " VALOR INGRESADO EN FORMATO INVALIDO (Valor Informado: " & wvarDatoValue & ". Requerido: dd/mm/yyyy)"
            End If
    End Select
    '
ClearObjects:
    Set wobjNodoValor = Nothing
End Function

Public Function GetRequestRetornado(pobjXMLRequest As DOMDocument, pobjXMLRequestDef As IXMLDOMNode, pvarStrRetorno) As IXMLDOMNode
Dim wobjNodoRequestDef  As IXMLDOMNode
Dim wobjNodoVectorDef As IXMLDOMNode
Dim wobjNewNodo         As IXMLDOMNode
Dim pvarStartCount      As Long
Dim wvarLastValue       As String
Dim wvarCount           As Long

    pvarStartCount = 5 'Desestimo el Numero de mensaje
    '
    For Each wobjNodoRequestDef In pobjXMLRequestDef.childNodes
        '
        If wobjNodoRequestDef.Attributes.getNamedItem("Cantidad") Is Nothing Then
            'No proceso los vectores del Request
            If pobjXMLRequest.selectSingleNode("//" & wobjNodoRequestDef.Attributes.getNamedItem("Nombre").Text) Is Nothing Then
                Set wobjNewNodo = pobjXMLRequest.createElement(wobjNodoRequestDef.Attributes.getNamedItem("Nombre").Text)
                pobjXMLRequest.childNodes(0).appendChild wobjNewNodo
            Else
                Set wobjNewNodo = pobjXMLRequest.selectSingleNode("//" & wobjNodoRequestDef.Attributes.getNamedItem("Nombre").Text)
            End If
            '
            If wobjNodoRequestDef.Attributes.getNamedItem("Decimales") Is Nothing Then
                wvarLastValue = Mid(pvarStrRetorno, pvarStartCount, wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text)
            Else
                wvarLastValue = Mid(pvarStrRetorno, pvarStartCount, wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text + wobjNodoRequestDef.Attributes.getNamedItem("Decimales").Text)
            End If
            '
            Select Case wobjNodoRequestDef.Attributes.getNamedItem("TipoDato").Text
                Case "ENTERO"
                    wobjNewNodo.Text = Val(wvarLastValue)
                Case "DECIMAL"
                    wobjNewNodo.Text = Val(wvarLastValue) / (10 ^ Val(wobjNodoRequestDef.Attributes.getNamedItem("Decimales").Text))
                Case "FECHA"
                    wobjNewNodo.Text = Right(wvarLastValue, 2) & "/" & Mid(wvarLastValue, 5, 2) & "/" & Left(wvarLastValue, 4)
                Case Else
                    wobjNewNodo.Text = Trim(wvarLastValue)
            End Select
            If wobjNodoRequestDef.Attributes.getNamedItem("Decimales") Is Nothing Then
                pvarStartCount = pvarStartCount + wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text
            Else
                pvarStartCount = pvarStartCount + wobjNodoRequestDef.Attributes.getNamedItem("Enteros").Text + wobjNodoRequestDef.Attributes.getNamedItem("Decimales").Text
            End If
        Else
            'Salteo todos los registros del vector
            For wvarCount = 1 To wobjNodoRequestDef.Attributes.getNamedItem("Cantidad").Text
                For Each wobjNodoVectorDef In wobjNodoRequestDef.childNodes
                    If wobjNodoVectorDef.Attributes.getNamedItem("Decimales") Is Nothing Then
                        pvarStartCount = pvarStartCount + wobjNodoVectorDef.Attributes.getNamedItem("Enteros").Text
                    Else
                        pvarStartCount = pvarStartCount + wobjNodoVectorDef.Attributes.getNamedItem("Enteros").Text + wobjNodoVectorDef.Attributes.getNamedItem("Decimales").Text
                    End If
                Next wobjNodoVectorDef
            Next wvarCount
        End If
        '
    Next wobjNodoRequestDef
    '
    Set GetRequestRetornado = pobjXMLRequest.childNodes(0)
    Set wobjNewNodo = Nothing
    Set wobjNodoRequestDef = Nothing
End Function

'***************************************************************
'*** Ejecuta COM+ Multithreading
'***************************************************************
'Ejemplo de pvarRequest:
'<Request>
'   <Request id="1" actionCode="lbaw_OVVerifReimpresion" ciaascod="0001"/>
'   <Request id="2" actionCode="lbaw_OVVerifReimpresion" ciaascod="0001"/>
'   <Request id="3" actionCode="lbaw_OVVerifReimpresion" ciaascod="0020"/>
'   <Request id="4" actionCode="lbaw_OVVerifReimpresion" ciaascod="0020"/>
'</Request>
'***************************************************************
Public Function cmdp_ExecuteTrnMulti(ByVal pvarActionCode As String, ByVal pvarSchemaFile As String, ByVal pvarRequest As String, pvarResponse As String)
    Dim wobjCmdProcessor        As HSBCInterfaces.ICmdProcessor
    Dim wobjCmdProcessorArray   As Object
    Dim wvarRequest             As String
    Dim wvarExecReturn          As String
    Dim wvarCount               As Integer
    Dim i                       As Integer
    Dim wvarRequestMultiple     As Variant
    Dim wvarResponseMultiple    As Variant
    Dim wvarResultMultiple      As Variant
    Dim wvarResultMultipleId    As Variant
    Dim wvarResultMultipleCIAASCOD    As Variant
    Dim wvarCountOK             As Integer
    Dim wvarCountBAD            As Integer

    Dim wobjXMLDocRequest       As MSXML2.DOMDocument
    Dim wobjXMLDocResponse      As MSXML2.DOMDocument
    Dim objNodeList             As MSXML2.IXMLDOMNodeList
    Dim wobjNode                As MSXML2.IXMLDOMNode
    Dim nodeActionCode          As MSXML2.IXMLDOMNode
    Dim nodeAttribute           As MSXML2.IXMLDOMAttribute
    Dim wvarResponseText        As String
    Dim wvarActionCode          As String

    Set wobjXMLDocRequest = CreateObject("MSXML2.DOMDocument")
        wobjXMLDocRequest.async = False
        wobjXMLDocRequest.loadXML (pvarRequest)

    Set objNodeList = wobjXMLDocRequest.selectNodes("//Request/Request")
    wvarCount = objNodeList.length
    If wvarCount = 0 Then
        wvarRequest = cmdp_FormatRequest(pvarActionCode, pvarSchemaFile, pvarRequest)
        '
        Set wobjCmdProcessor = CreateObject("HSBC_ASP.CmdProcessor")
        wvarExecReturn = wobjCmdProcessor.Execute(pvarRequest, pvarResponse)
        '
    Else
        On Error Resume Next

        ReDim wvarRequestMultiple(wvarCount)
        ReDim wvarResponseMultiple(wvarCount)
        ReDim wvarResultMultiple(wvarCount)
        ReDim wvarResultMultipleId(wvarCount)
        ReDim wvarResultMultipleCIAASCOD(wvarCount)

        i = 0
        While i < wvarCount
            '
            Set wobjNode = objNodeList.Item(i)
            Err.Clear
            '
            'Recupera el id para devolverlo en el response
            wvarResultMultipleId(i) = wobjNode.Attributes.getNamedItem("id").Text
            '
            'Si se sete� el ciaascod en el request, lo recupera para devolverlo en el response
            If Not wobjNode.Attributes.getNamedItem("ciaascod") Is Nothing Then
                wvarResultMultipleCIAASCOD(i) = wobjNode.Attributes.getNamedItem("ciaascod").Text
            Else
                wvarResultMultipleCIAASCOD(i) = Null
            End If
            '
            If Err <> 0 Then
                wvarResultMultipleId(i) = Null
                wvarResultMultipleCIAASCOD(i) = Null
            End If
            '
            Set nodeActionCode = wobjNode.Attributes.getNamedItem("actionCode")
                wvarActionCode = nodeActionCode.Text
            wvarRequestMultiple(i) = cmdp_FormatRequest(wvarActionCode, pvarSchemaFile, wobjNode.xml)
            i = i + 1
        Wend
        '
        Set wobjCmdProcessorArray = CreateObject("WD.CmdProcessorArray")
        wvarExecReturn = wobjCmdProcessorArray.Execute(wvarRequestMultiple, wvarResponseMultiple, wvarResultMultiple)
        '
        wvarCountOK = 0
        wvarCountBAD = 0
        i = 0
        While i < wvarCount
            If wvarResultMultiple(i) = 0 Then
                wvarCountOK = wvarCountOK + 1
            Else
                wvarCountBAD = wvarCountBAD + 1
            End If
            i = i + 1
        Wend

        pvarResponse = "<Response>" & Chr(13) & Chr(10)
        pvarResponse = pvarResponse & "<count>"
        pvarResponse = pvarResponse & wvarCount
        pvarResponse = pvarResponse & "</count>" & Chr(13) & Chr(10)
        pvarResponse = pvarResponse & "<wvarCountOK>"
        pvarResponse = pvarResponse & wvarCountOK
        pvarResponse = pvarResponse & "</wvarCountOK>" & Chr(13) & Chr(10)
        pvarResponse = pvarResponse & "<wvarCountBAD>"
        pvarResponse = pvarResponse & wvarCountBAD
        pvarResponse = pvarResponse & "</wvarCountBAD>" & Chr(13) & Chr(10)

        i = 0
        While i < wvarCount
            wvarResponseText = wvarResponseMultiple(i)
            '
            If Trim(CStr(wvarResponseText)) = "" Then
                wvarResponseText = "<Response><Estado resultado='false' mensaje='Codigo de Error: " & wvarResultMultiple(i) & "'/></Response>"
            End If
            '
            If Not IsNull(wvarResultMultipleId(i)) Then
                '
                'Carga el XML de respuesta
                Set wobjXMLDocResponse = CreateObject("MSXML2.DOMDocument")
                    wobjXMLDocResponse.async = False
                    wobjXMLDocResponse.loadXML (wvarResponseText)
                '
                'Agrega el atributo id para identificar la ejecuci�n del COM+
                Set wobjNode = wobjXMLDocResponse.selectSingleNode("//Response")
                Set nodeAttribute = wobjXMLDocResponse.createAttribute("id")
                    nodeAttribute.Value = wvarResultMultipleId(i)
                    wobjNode.Attributes.setNamedItem nodeAttribute
                '
                'Agrega el atributo ciaascod que vino en el request para poder identificar a la compa��a (NYL / LBA)
                If Not IsNull(wvarResultMultipleCIAASCOD(i)) Then
                    Set nodeAttribute = wobjXMLDocResponse.createAttribute("ciaascod")
                        nodeAttribute.Value = wvarResultMultipleCIAASCOD(i)
                        wobjNode.Attributes.setNamedItem nodeAttribute
                End If
                '
                wvarResponseText = wobjXMLDocResponse.xml
            End If
            '
            pvarResponse = pvarResponse & wvarResponseText
            i = i + 1
        Wend
        pvarResponse = pvarResponse & "</Response>"
        '
    End If
    '
    cmdp_ExecuteTrnMulti = wvarExecReturn
    '
    Set wobjXMLDocRequest = Nothing
    Set wobjXMLDocResponse = Nothing
    Set objNodeList = Nothing
    Set wobjNode = Nothing
    Set nodeActionCode = Nothing
    Set wobjCmdProcessor = Nothing
    Set wobjCmdProcessorArray = Nothing
    Set nodeAttribute = Nothing
'
End Function

'***************************************************************
'*** Formatea el documento para la transacci�n
'***************************************************************
Public Function cmdp_FormatRequest(pvarActionCode, pvarSchemaFile, pvarBody)
    Dim wvarRequest     As String
    Dim wvarSchema      As String
    Dim xXML_UseSchema  As Boolean
    Dim xXML_PATH       As String
    Dim xAPP_CODE       As String
    
    xXML_UseSchema = False
    '
    'wvarRequest = "<?xml version='1.0' encoding='UTF-8'?>"
    wvarRequest = ""
    wvarSchema = ""
    '
    If xXML_UseSchema Then
        wvarSchema = " xmlns='x-schema:" & xXML_PATH & pvarSchemaFile & "'"
    End If
    wvarRequest = wvarRequest & _
                  "<HSBC_MSG" & wvarSchema & ">" & _
                      "<HEADER>" & _
                          "<APPLICATION_CODE>" & _
                              xAPP_CODE & _
                          "</APPLICATION_CODE>" & _
                          "<ACTION_CODE>" & _
                              pvarActionCode & _
                          "</ACTION_CODE>" & _
                      "</HEADER>" & _
                      "<BODY>" & _
                           pvarBody & _
                      "</BODY>" & _
                  "</HSBC_MSG>"
    '
    cmdp_FormatRequest = wvarRequest
End Function

'*****************************************************************
'*** Genera un string random, de longitud indicada por parametro.
'*****************************************************************

Public Function generar_RNDSTR(Optional strLength As Integer) As String
' Esta funci�n genera un string de longitud strLength aleatorio.
' Si la longitud no se especifica, se asume igual a 4.
    On Error GoTo ErrorHandler
    
    Dim strCode     As String
    Dim nroAux      As Integer
    
    strCode = ""
    
    Randomize
    
    If strLength = 0 Then
        strLength = 4
    End If

    While Len(strCode) < strLength
        nroAux = Int((92 * Rnd) + 33)
        If (nroAux >= 48 And nroAux <= 57) _
            Or (nroAux >= 65 And nroAux <= 90) _
            Or (nroAux >= 97 And nroAux <= 122) _
            Then
            ' 48:57 --> 0..9
            ' 65:90 --> a..z
            ' 97:122 -> A..Z
            
            strCode = strCode & Chr(nroAux)
        End If
    Wend
    
    generar_RNDSTR = strCode
    
    Exit Function

ErrorHandler:
    generar_RNDSTR = Empty

End Function
Function fncCalculaPeriodos1(ByRef pPeriodo1, ByRef pPeriodo2, ByRef pPeriodo3)
    'Arma los per�odos de los resumenes a mostrar
    pPeriodo1 = Year(Now()) - 1 & "1231"
    pPeriodo2 = Year(Now()) - 1 & "0630"
    pPeriodo3 = Year(Now()) - 2 & "1231"
    '
End Function

Function fncCalculaPeriodos2(ByRef pPeriodo1, ByRef pPeriodo2, ByRef pPeriodo3)
    'Arma los per�odos de los resumenes a mostrar
    pPeriodo1 = Year(Now()) & "0630"
    pPeriodo2 = Year(Now()) - 1 & "1231"
    pPeriodo3 = Year(Now()) - 1 & "0630"
    '
End Function

'Recupera la descripci�n del mensaje desde mensajes.xml en base a la clase y cte de mensaje
Public Function getMensaje(pvarClassName, pvarMsgCte) As String
    Dim wobjXMLMsg      As MSXML2.DOMDocument
    
    Set wobjXMLMsg = CreateObject("MSXML2.DOMDocument")
        wobjXMLMsg.async = False
        wobjXMLMsg.Load (App.Path & "\XMLs\Mensajes.xml")
    
    If Not wobjXMLMsg.selectSingleNode("//CLASE[@nombre='" & pvarClassName & "']/MENSAJE[@ERROR='" & pvarMsgCte & "']") Is Nothing Then
        getMensaje = wobjXMLMsg.selectSingleNode("//CLASE[@nombre='" & pvarClassName & "']/MENSAJE[@ERROR='" & pvarMsgCte & "']").Text
    Else
        getMensaje = "Mensaje de error no definido"
    End If
End Function
