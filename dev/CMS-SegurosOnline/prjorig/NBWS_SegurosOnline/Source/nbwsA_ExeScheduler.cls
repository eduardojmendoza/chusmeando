VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "nbwsA_ExeScheduler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'******************************************************************************
'Fecha de Modificaci�n: 20/07/2010
'Ticket:
'Desarrollador: Daniel Armentano
'Descripci�n: se reemplaza los espacios en blanco del EMAIL por gui�n bajo.
'
'******************************************************************************
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010.
' ALL RIGHTS RESERVED
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
' serious civil and criminal offence, which can result in heavy fines and payment
' of substantial damages.
'
' Nombre del Fuente: nbwsA_ExeScheduler.cls
' Fecha de Creaci�n: desconocido
' PPcR: desconocido
' Desarrollador: Desconocido
' Descripci�n: se agrega Copyright para cumplir con las normas de QA
'
'******************************************************************************
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "nbwsA_Transacciones.nbwsA_ExeScheduler"

'Stored Procedures


Const mcteStoreProcInsLog           As String = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG"

Const mcteEPolizaConfig             As String = "NBWS_ePolizaConfig.xml"

Const mcteOperacion                 As String = "EXESCH"

Type NODOSREQUEST
    CIAASCOD            As String
    RAMOPCOD            As String
    POLIZANN            As String
    POLIZSEC            As String
    CERTIPOL            As String
    CERTIANN            As String
    CERTISEC            As String
    OPERAPOL            As String
    DOCUMTIP            As String
    DOCUMDAT            As String
    CLIENAP1            As String
    CLIENAP2            As String
    CLIENNOM            As String
    NOMARCH             As String
    FORMUDES            As String
    SWSUSCRI            As String
    MAIL                As String
    CLAVE               As String
    SWCLAVE             As String
    SWENDOSO            As String
    OPERATIP            As String
    ESTADO              As String
    CODESTAD            As String
End Type

Type TEMPLATEMAIL
    AUSSYSPASS          As String
    HOMSYSPASS          As String
    NYLSYSPASS          As String
    AUSUSRPASS          As String
    HOMUSRPASS          As String
    NYLUSRPASS          As String
   

End Type

Dim mvarNodosRequest             As NODOSREQUEST


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarStep            As Long
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim mvarTemplate        As TEMPLATEMAIL
    Dim wobjXMLRespuestas   As MSXML2.DOMDocument
    Dim wobjXMLResponseAIS  As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLListadoProd  As MSXML2.DOMDocument
    Dim wobjXMLArchivos     As MSXML2.DOMDocument
    Dim wobjXMLNode         As MSXML2.IXMLDOMNode
    Dim wobjXMLArchivo      As MSXML2.IXMLDOMNode
    Dim wobjXMLConfig       As MSXML2.DOMDocument    '
    Dim mvarResponseAIS     As String
    Dim mvarEnvioMDW        As Boolean
    Dim mvarProcConErrores  As Boolean
    Dim mvarErrorRecuperaPDF As Boolean
    Dim mvarProcesar        As Boolean
    Dim mvarPendientes      As Integer
    Dim mvarMSGError        As String
    Dim mvarPathPDF         As String
    Dim mvarCodError        As String
    Dim mvarEstadoEnvio     As String
    Dim mvarEstadoLog       As String
    Dim wvarPDFBase64       As String
    Dim mvarRequestMDW      As String
    Dim wvarFalseAIS        As String
    Dim wvarPDFArchivos     As String
    Dim wvarArchivo         As String
    Dim wvarNombre          As String
    Dim mvarTemplateEmail   As String
    Dim mvarMailPrueba      As String
    Dim mvarCopiaOculta     As String
    Dim mvarBorrarArchivo   As Boolean
    
    Dim mvarTemplateLBAALTAAUTO             As String
    Dim mvarTemplateLBAENDOSOAUTO           As String
    Dim mvarTemplateLBACARTERAAUTO          As String
    
    Dim mvarTemplateLBAALTANOAUTO             As String
    Dim mvarTemplateLBAENDOSONOAUTO           As String
    Dim mvarTemplateLBACARTERANOAUTO          As String
    
    'PG - 06-08-2012 RCP1
    Dim mvarTemplateLBAALTANORCP As String
    Dim mvarTemplateLBAENDOSONORCP As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicializaci�n de variables
    mvarProcConErrores = False
    mvarCodError = ""
    
    wvarStep = 10
    'pvarRequest = Replace(pvarRequest, "&", "")
    'DoEvents
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        'Dim wvarNroArch As Long
        'wvarNroArch = FreeFile()
        'Open App.Path & "\wvarStep10.txt" For Append As #1
        'Print #1, wobjXMLRequest.xml
        'Close #1
    End With
    
    wvarStep = 20
    
    'Se obtienen los par�metros desde XML
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\" & mcteEPolizaConfig
    
    wvarStep = 30
    mvarPathPDF = wobjXMLConfig.selectSingleNode("//PATHPDF").Text
    wvarStep = 31
    mvarTemplate.AUSSYSPASS = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/AUSSYSPASS").Text
    wvarStep = 32
    mvarTemplate.HOMSYSPASS = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/HOMSYSPASS").Text
    wvarStep = 33
    mvarTemplate.NYLSYSPASS = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/NYLSYSPASS").Text
    wvarStep = 34
    mvarTemplate.AUSUSRPASS = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/AUSUSRPASS").Text
    wvarStep = 35
    mvarTemplate.HOMUSRPASS = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/HOMUSRPASS").Text
    wvarStep = 36
    mvarTemplate.NYLUSRPASS = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/NYLUSRPASS").Text
    
    wvarStep = 40
    ' GED: ANEXO I - SE AGREGAN NUEVAS TEMPLATES
    mvarTemplateLBAALTAAUTO = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBAALTAAUTO").Text
    mvarTemplateLBAENDOSOAUTO = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBAENDOSOAUTO").Text
    mvarTemplateLBACARTERAAUTO = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBACARTERAAUTO").Text
    
     wvarStep = 50
    mvarTemplateLBAALTANOAUTO = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBAALTANOAUTO").Text
    mvarTemplateLBAENDOSONOAUTO = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBAENDOSONOAUTO").Text
    mvarTemplateLBACARTERANOAUTO = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBACARTERANOAUTO").Text
    
    'PG - 06-08-2012 RCP1
    wvarStep = 55
    mvarTemplateLBAALTANORCP = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBAALTARCP").Text
    mvarTemplateLBAENDOSONORCP = wobjXMLConfig.selectSingleNode("//TEMPLATEMAIL/LBAENDOSORCP").Text
  
    'GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    ' correos con info no deseada accidentalmente
     wvarStep = 60
    
    If Not wobjXMLConfig.selectSingleNode("//MAILPRUEBA") Is Nothing Then
        mvarMailPrueba = wobjXMLConfig.selectSingleNode("//MAILPRUEBA").Text
    Else
        mvarMailPrueba = ""
    End If
    
     wvarStep = 70
    If Not wobjXMLConfig.selectSingleNode("//COPIAOCULTA") Is Nothing Then
        mvarCopiaOculta = wobjXMLConfig.selectSingleNode("//COPIAOCULTA").Text
    Else
        mvarCopiaOculta = ""
    End If
    
     wvarStep = 80
    
    If Not wobjXMLConfig.selectSingleNode("//BORRARARCHIVO") Is Nothing Then
        If UCase(wobjXMLConfig.selectSingleNode("//BORRARARCHIVO").Text) = "S" Then
            mvarBorrarArchivo = True
        Else
            mvarBorrarArchivo = False
        End If
    Else
            mvarBorrarArchivo = False
    End If
    
    '
    wvarStep = 90
    With wobjXMLRequest
        mvarNodosRequest.CIAASCOD = .selectSingleNode("//CIAASCOD").Text
        mvarNodosRequest.RAMOPCOD = .selectSingleNode("//RAMOPCOD").Text
        mvarNodosRequest.POLIZANN = .selectSingleNode("//POLIZANN").Text
        mvarNodosRequest.POLIZSEC = .selectSingleNode("//POLIZSEC").Text
        mvarNodosRequest.CERTIPOL = .selectSingleNode("//CERTIPOL").Text
        mvarNodosRequest.CERTIANN = .selectSingleNode("//CERTIANN").Text
        mvarNodosRequest.CERTISEC = .selectSingleNode("//CERTISEC").Text
        mvarNodosRequest.OPERAPOL = .selectSingleNode("//OPERAPOL").Text
        mvarNodosRequest.DOCUMTIP = .selectSingleNode("//DOCUMTIP").Text
        mvarNodosRequest.DOCUMDAT = .selectSingleNode("//DOCUMDAT").Text
        mvarNodosRequest.CLIENAP1 = .selectSingleNode("//CLIENAP1").Text
        mvarNodosRequest.CLIENAP2 = .selectSingleNode("//CLIENAP2").Text
        mvarNodosRequest.CLIENNOM = .selectSingleNode("//CLIENNOM").Text
        mvarNodosRequest.NOMARCH = .selectSingleNode("//ARCHIVOS").xml
        mvarNodosRequest.FORMUDES = .selectSingleNode("//FORMUDES").Text
        mvarNodosRequest.SWSUSCRI = .selectSingleNode("//SWSUSCRI").Text
        If mvarMailPrueba = "" Then
            'DA - 20/07/2010: se reemplaza el espacio en blanco por gui�n bajo ya
            'que hay fallos en las entregas de los emails por este motivo, porque
            'el MQ Gen�rico que viene del AIS reemplaza los guiones bajos por espacios.
            mvarNodosRequest.MAIL = Replace(.selectSingleNode("//MAIL").Text, " ", "_")
        Else
            mvarNodosRequest.MAIL = mvarMailPrueba
        End If
            
        mvarNodosRequest.CLAVE = .selectSingleNode("//CLAVE").Text
        mvarNodosRequest.SWCLAVE = .selectSingleNode("//SW-CLAVE").Text
        mvarNodosRequest.SWENDOSO = .selectSingleNode("//SWENDOSO").Text
        mvarNodosRequest.OPERATIP = .selectSingleNode("//OPERATIP").Text
        mvarNodosRequest.ESTADO = .selectSingleNode("//ESTADO").Text
        mvarNodosRequest.DOCUMTIP = .selectSingleNode("//DOCUMTIP").Text
        mvarNodosRequest.CODESTAD = .selectSingleNode("//CODESTAD").Text
      
    End With
    
    'GED Asigna template segun producto
    'If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And mvarNodosRequest.SWCLAVE = "S" Then mvarTemplateEmail = mvarTemplate.AUSSYSPASS
    'If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And mvarNodosRequest.SWCLAVE = "U" Then mvarTemplateEmail = mvarTemplate.AUSUSRPASS
    
    'If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And mvarNodosRequest.SWCLAVE = "S" Then mvarTemplateEmail = mvarTemplate.HOMSYSPASS
    'If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And mvarNodosRequest.SWCLAVE = "U" Then mvarTemplateEmail = mvarTemplate.HOMUSRPASS
    
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And Trim(mvarNodosRequest.OPERATIP) = "8" Then mvarTemplateEmail = mvarTemplateLBAALTAAUTO
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And Trim(mvarNodosRequest.OPERATIP) = "9" Then mvarTemplateEmail = mvarTemplateLBAENDOSOAUTO
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "AUS" And Trim(mvarNodosRequest.OPERATIP) = "13" Then mvarTemplateEmail = mvarTemplateLBACARTERAAUTO
    
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And Trim(mvarNodosRequest.OPERATIP) = "8" Then mvarTemplateEmail = mvarTemplateLBAALTANOAUTO
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And Trim(mvarNodosRequest.OPERATIP) = "9" Then mvarTemplateEmail = mvarTemplateLBAENDOSONOAUTO
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "HOM" And Trim(mvarNodosRequest.OPERATIP) = "13" Then mvarTemplateEmail = mvarTemplateLBACARTERANOAUTO
   
    'PG - 06-08-2012 RCP1
    wvarStep = 95
    'If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" Then
    '    pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    '    Exit Function
    'End If
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" And Trim(mvarNodosRequest.OPERATIP) = "8" Then mvarTemplateEmail = mvarTemplateLBAALTANORCP
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" And Trim(mvarNodosRequest.OPERATIP) = "9" Then mvarTemplateEmail = mvarTemplateLBAENDOSONORCP
    If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" And Trim(mvarNodosRequest.OPERATIP) = "13" Then mvarTemplateEmail = mvarTemplateLBAENDOSONOAUTO
      
    '
    'Registraci�n en Log de inicio de ejecuci�n
    wvarStep = 100
    'insertLog mcteOperacion, "I-OVEXESCH - Nueva ejecuci�n - (CLIENTE:" & mvarNodosRequest.DOCUMDAT & " " & mvarNodosRequest.DOCUMTIP & " - IMPRESO: " & mvarNodosRequest.FORMUDES & ")"
    '
    'En caso de error se resume para evitar el corte del proceso (se realiza control de err.Number)
    'On Error Resume Next
    '
    'Recuperaci�n de ARCHIVO del file server
    wvarPDFBase64 = ""
    mvarErrorRecuperaPDF = False
    
     Set wobjXMLArchivos = CreateObject("MSXML2.DOMDocument")
    With wobjXMLArchivos
        .async = False
        Call .loadXML(mvarNodosRequest.NOMARCH)
        '
    End With
    
    '************************************************************************************
    ' Recupera del file server c/u de los .pdf
    '*****************************************************************************
    For Each wobjXMLArchivo In wobjXMLArchivos.selectNodes("//ARCHIVOS/ARCHIVO")
    
    
        wvarArchivo = wobjXMLArchivo.selectSingleNode("NOMARCH").Text
        wvarNombre = wobjXMLArchivo.selectSingleNode("FORMUDES").Text
        If Right(wvarNombre, 8) = "Patronal" Then
            wvarNombre = "Poliza de RC Patronal"
        End If
    
        wvarStep = 110
        
        
        If wvarArchivo <> "" Then
            wvarPDFBase64 = getFileToBase64(mvarPathPDF, wvarArchivo, wvarNombre & ".pdf", mvarBorrarArchivo)
        Else
            Exit For
        End If
        
        wvarStep = 120
        
        If wvarPDFBase64 = "" Then
            mvarErrorRecuperaPDF = True
            Exit For
        Else
        
            wvarPDFArchivos = wvarPDFArchivos & wvarPDFBase64
    
        End If
    
    Next
    
    
    'Armar Request de envio a MDW
    wvarStep = 130
    
    If Not mvarErrorRecuperaPDF Then
    
        'Se cambia envio de mail por medio de MQ.
        If Left(mvarNodosRequest.RAMOPCOD, 3) = "RCP" Then
            mvarRequestMDW = "<Request>" & _
                               "<DEFINICION>ismSendPdfReportCAC.xml</DEFINICION>" & _
                               "<Raiz>share:sendPdfReport</Raiz>" & _
                               "<files><files>" & wvarPDFArchivos & "</files></files>" & _
                               "<applicationId>NBWS</applicationId>" & _
                               "<reportId/>" & _
                               "<recipientTO>" & mvarNodosRequest.MAIL & "</recipientTO>" & _
                               "<recipientsCC/>" & _
                               "<recipientsBCC>" & mvarCopiaOculta & "</recipientsBCC>" & _
                               "<templateFileName>" & mvarTemplateEmail & "</templateFileName>" & _
                               "<parametersTemplate/>" & _
                               "<from/>" & _
                               "<replyTO/>" & _
                               "<bodyText/>" & _
                               "<subject/>" & _
                               "<importance/>" & _
                               "<imagePathFile/>" & _
                               "<attachPassword/>" & _
                               "<attachFileName/>" & _
                           "</Request>"
        Else
            mvarRequestMDW = "<Request>" & _
                               "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" & _
                               "<Raiz>share:sendPdfReport</Raiz>" & _
                               "<files/>" & _
                               "<applicationId>NBWS</applicationId>" & _
                               "<reportId/>" & _
                               "<recipientTO>" & mvarNodosRequest.MAIL & "</recipientTO>" & _
                               "<recipientsCC/>" & _
                               "<recipientsBCC>" & mvarCopiaOculta & "</recipientsBCC>" & _
                               "<templateFileName>" & mvarTemplateEmail & "</templateFileName>" & _
                               "<parametersTemplate/>" & _
                               "<from/>" & _
                               "<replyTO/>" & _
                               "<bodyText/>" & _
                               "<subject/>" & _
                               "<importance/>" & _
                               "<imagePathFile/>" & _
                               "<attachPassword/>" & _
                               "<attachFileName/>" & _
                           "</Request>"
        End If
        
    wvarStep = 140
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Dim wvarNroArch As Long
    'wvarNroArch = FreeFile()
    'Open App.Path & "\wvarStep140.txt" For Append As #1
    'Print #1, wvarPDFArchivos
    'Close #1
    
    '************************************************************************************
    ' Env�a mail via MDW
    '*****************************************************************************
    
   
        'If Left(mvarNodosRequest.RAMOPCOD, 3) <> "RCP" Then
            If Err.Number = 0 Then
                mvarEnvioMDW = enviarXMLaMDW(mvarRequestMDW, mvarMSGError)
            Else
                mvarMSGError = Err.Description
            End If
        'Else
        '    mvarEnvioMDW = False
        'End If
    
    End If
'***************************************************************************************
'Procesa el envio del resultado del envio de mail al AIS mensaje 1188
'***************************************************************************************
wvarStep = 150
    
    'Si da error MDW envio estado "F"allo
    mvarEstadoLog = "E"
     
     If mvarErrorRecuperaPDF Or Not mvarEnvioMDW Then mvarEstadoLog = "P"
     
     
       
    If mvarErrorRecuperaPDF Or Not mvarEnvioMDW Then mvarEstadoEnvio = "F"
    
       
    ' Si da OK MDW y no hubo ningun otro error "E"nviado
    If mvarEnvioMDW And Err.Number = 0 Then mvarEstadoEnvio = "E"
       
     wvarStep = 160
    insertLog mcteOperacion, mvarNodosRequest.RAMOPCOD & "-" & Right("00" & mvarNodosRequest.POLIZANN, 2) & "-" & Right("000000" & mvarNodosRequest.POLIZSEC, 6) & _
                            "/" & Right("0000" & mvarNodosRequest.CERTIPOL, 4) & "-" & Right("0000" & mvarNodosRequest.CERTIANN, 4) & "-" & _
                            Right("000000" & mvarNodosRequest.CERTISEC, 6), mvarNodosRequest.CLIENAP1 & " " & mvarNodosRequest.CLIENAP2 & _
                            ", " & mvarNodosRequest.CLIENNOM, mvarNodosRequest.MAIL, mvarEstadoLog, mvarNodosRequest.DOCUMTIP, mvarNodosRequest.DOCUMDAT
     
       
       
    wvarRequest = ""
    
   
                wvarStep = 110
                'P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
                wvarRequest = wvarRequest & "<Request id=""1"" actionCode=""nbwsA_MQGenericoAIS"" ciaascod=""0001"">" & _
                    "<DEFINICION>LBA_1188_EnvioPoliza.xml</DEFINICION>" & _
                    "<LNK-CIAASCOD>0001</LNK-CIAASCOD>" & _
                    "<RAMOPCOD>" & mvarNodosRequest.RAMOPCOD & "</RAMOPCOD>" & _
                    "<POLIZANN>" & mvarNodosRequest.POLIZANN & "</POLIZANN>" & _
                    "<POLIZSEC>" & mvarNodosRequest.POLIZSEC & "</POLIZSEC>" & _
                    "<CERTIPOL>" & mvarNodosRequest.CERTIPOL & "</CERTIPOL>" & _
                    "<CERTIANN>" & mvarNodosRequest.CERTIANN & "</CERTIANN>" & _
                    "<CERTISEC>" & mvarNodosRequest.CERTISEC & "</CERTISEC>" & _
                    "<OPERAPOL>" & mvarNodosRequest.OPERAPOL & "</OPERAPOL>" & _
                    "<CODIESTA>" & mvarEstadoEnvio & "</CODIESTA>" & _
                "</Request>"

   
    
   
    wvarStep = 170
    'Ejecuta la funci�n Multithreading
    wvarRequest = "<Request>" & wvarRequest & "</Request>"
    Call cmdp_ExecuteTrnMulti("nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse)
     
    
     Set wobjXMLRespuestas = CreateObject("MSXML2.DOMDocument")
     wobjXMLRespuestas.async = False
        wobjXMLRespuestas.loadXML wvarResponse
        'Carga las dos respuestas en un XML.
  
       If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado") Is Nothing Then
        '
        'Fall� Response 1
        Err.Description = "Response 1: fall� COM+ de Request 1"
        GoTo ErrorHandler
        '
    End If
    
    wvarStep = 180
    
      
    'Arma un XML con los productos de las dos compa��as
    wvarResponse = ""
    '
    wvarStep = 190
    If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/CANTRECI") Is Nothing Then
        wvarResponse = wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/CAMPOS/CANTRECI").xml
    Else
        wvarResponse = ""
        wvarFalseAIS = wobjXMLRespuestas.selectSingleNode("Response/Response[@id='1']/Estado/@mensaje").Text
    End If
    '
    
    If wvarResponse = "0" Then
    
        'insertLog mcteOperacion, "E-Fin OVEXESCH.  Estado: ERROR - " & " No se impacto resultado de env�o OK en el AIS"
    
    End If
    
    wvarStep = 200
     
    
    If wvarResponse = "" Then
        GoTo FalseAIS:
    End If
       
    '
    'Reestablece el estado de On Error
    On Error GoTo ErrorHandler
    '
    'Registraci�n en Log de fin de proceso
    wvarStep = 210
    If mvarProcConErrores Then
        'insertLog mcteOperacion, "E-Fin EXESCH.  Estado: ERROR - " & mvarMSGError
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & mvarMSGError & "</Response>"
    Else
        'insertLog mcteOperacion, "0-Fin EXESCH.  Estado: OK"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    End If
    'FIN
    
FalseAIS:
    
    
    '
    wvarStep = 220
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    wvarStep = 230
    Set wobjXMLResponseAIS = Nothing
    Set wobjXMLRequest = Nothing
    Set wobjXMLListadoProd = Nothing
    Set wobjXMLNode = Nothing
    Exit Function
    
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    'Inserta error en Log
    'insertLog mcteOperacion, mcteClassName & " - " & _
                     wcteFnName & " - " & _
                     wvarStep & " - " & _
                     Err.Number & " - " & _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & _
                     vbLogEventTypeError
    
    'insertLog mcteOperacion, "99-Fin EXESCH.  Estado: ABORTADO POR ERROR"
          
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function getFileToBase64(ByVal pvarPath As String, ByVal pvarNomArchivo As String, ByVal pvarFormuDes As String, ByVal pvarBorrarArchivo) As String

        Dim wobjXMLImpreso          As MSXML2.DOMDocument
        Dim wobjEle                 As IXMLDOMElement
        Dim wvarFileName            As String
        Dim wvarFileGet             As Integer
        Dim wvarArrBytes()          As Byte
        Dim wvarStep As Integer
        
            
        wvarFileName = pvarPath & "\" & pvarNomArchivo

        If Dir(wvarFileName) = "" Then
        
            getFileToBase64 = ""
            
        Else
        
            wvarStep = 300
            
            Set wobjXMLImpreso = CreateObject("MSXML2.DOMDocument")
            wobjXMLImpreso.async = False
            'wobjXMLPdf.loadXML "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & pDatos & "</PDF>"
    

            wobjXMLImpreso.loadXML "<FileParam></FileParam>"
            
            wobjXMLImpreso.resolveExternals = True
        
            Set wobjEle = wobjXMLImpreso.createElement("content")
        
            wobjXMLImpreso.selectSingleNode("//FileParam").appendChild wobjEle
        
            wobjEle.setAttribute "xmlns:dt", "urn:schemas-microsoft-com:datatypes"
            
            wobjEle.dataType = "bin.base64"
            
            wvarStep = 310
            
            'Leo el binario y lo guardo en wvarArrBytes
            wvarFileGet = FreeFile()
            Open wvarFileName For Binary Access Read As wvarFileGet
            
            wvarStep = 320
            
            ReDim wvarArrBytes(FileLen(wvarFileName) - 1)
            Get wvarFileGet, , wvarArrBytes
            Close wvarFileGet
            
            'Cargo el Array en el XML
            wobjEle.nodeTypedValue = wvarArrBytes
            
            Set wobjEle = wobjXMLImpreso.createElement("name")
             wobjEle.Text = pvarFormuDes
             
             wobjXMLImpreso.selectSingleNode("//FileParam").appendChild wobjEle
            
            
            getFileToBase64 = wobjXMLImpreso.xml
            '
           
            
            'GED  borrar archivo
            '
            If pvarBorrarArchivo Then Kill wvarFileName
            '
            wvarStep = 330
        End If



End Function

Private Function insertLog(ByVal pvarCODOP As String, ByVal pvarDescripcion As String, ByVal pvarCliente As String, ByVal pvarEmail As String, ByVal pvarEstado As String, ByVal pvarDocumtip As String, ByVal pvarDocumdat As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarReqRespInTxt    As Boolean
    Dim mvarDebugCode       As Integer
    '
    wvarReqRespInTxt = False
    '
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\" & mcteEPolizaConfig
    '1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = CInt(wobjXMLConfig.selectSingleNode("//DEBUG").Text)
    '
    If InStr(1, UCase(pvarDescripcion), "<REQUEST>", vbTextCompare) > 0 Or InStr(1, UCase(pvarDescripcion), "<RESPONSE>", vbTextCompare) > 0 Then
        wvarReqRespInTxt = True
    End If
    '
    If mvarDebugCode >= 1 And Not wvarReqRespInTxt Then
        debugToBD pvarCODOP, pvarDescripcion, pvarCliente, pvarEmail, pvarEstado, pvarDocumtip, pvarDocumdat
    End If
    If mvarDebugCode = 3 Or (mvarDebugCode = 2 And Not wvarReqRespInTxt) Then
        debugToFile pvarCODOP, pvarDescripcion, pvarCliente, pvarEmail, pvarEstado, pvarDocumtip, pvarDocumdat
    End If
    '
End Function

Private Function debugToBD(ByVal pvarCODOP As String, ByVal pvarDescripcion As String, ByVal pvarCliente As String, ByVal pvarEmail As String, ByVal pvarEstado As String, ByVal pvarDocumtip As String, ByVal pvarDocumdat As String) As Boolean
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    'GED 24-11-2009 Prueba de DBConnection
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    'Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    
    
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBLOG)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsLog
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, Left(pvarCODOP, 6))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DESCRIPCION", adChar, adParamInput, 100, Left(pvarDescripcion, 100))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
     Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTE", adChar, adParamInput, 70, Left(pvarCliente, 70))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAIL", adChar, adParamInput, 50, Left(pvarEmail, 50))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADO", adChar, adParamInput, 3, Left(pvarEstado, 3))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOCUMTIP", adNumeric, adParamInput, , pvarDocumtip)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOCUMDAT", adNumeric, adParamInput, , pvarDocumdat)
    wobjDBParm.Precision = 11
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        debugToBD = True
    Else
        debugToBD = False
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Sub debugToFile(ByVal pvarCODOP As String, ByVal pvarDescripcion As String, ByVal pvarCliente As String, ByVal pvarEmail As String, ByVal pvarEstado As String, ByVal pvarDocumtip As String, ByVal pvarDocumdat As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarText            As String
    Dim wvarFileName        As String
    Dim wvarNroArch         As Long
    '
    wvarText = "(" & Date & "-" & Time & "):" & pvarDescripcion & "-" & pvarCliente & "-" & pvarEmail & "-" & pvarEstado & "-" & pvarDocumtip & "-" & pvarDocumdat
        
    wvarFileName = "debug-" & pvarCODOP & "-" & Year(Date) & Right("00" & Month(Date), 2) & Right("00" & Day(Date), 2) & ".log"
    wvarNroArch = FreeFile()
    Open App.Path & "\DEBUG\" & wvarFileName For Append As #1
    Write #1, wvarText
    Close #1
    '
End Sub



Private Function enviarXMLaMDW(pvarRequestMdw As String, ByRef pvarMsgError As String) As Boolean
    '
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarXMLResponseMdw  As MSXML2.DOMDocument
    Dim wvarResponseMdw     As String
    
    Set wobjClass = mobjCOM_Context.CreateInstance("LBAA_MWGenerico.lbaw_MQMW")
    Call wobjClass.Execute(pvarRequestMdw, wvarResponseMdw, "")
    '
    Set wobjClass = Nothing
    '
    Set wvarXMLResponseMdw = CreateObject("MSXML2.DOMDocument")
        wvarXMLResponseMdw.async = False
        wvarXMLResponseMdw.loadXML (wvarResponseMdw)
    '
    If Not wvarXMLResponseMdw.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wvarXMLResponseMdw.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            If wvarXMLResponseMdw.selectSingleNode("//Response/faultstring") Is Nothing Then
                enviarXMLaMDW = True
            Else
                enviarXMLaMDW = False
                pvarMsgError = wvarXMLResponseMdw.selectSingleNode("//Response/faultstring").Text
            End If
        Else
            If Not wvarXMLResponseMdw.selectSingleNode("//Response/faultstring") Is Nothing Then
                pvarMsgError = wvarXMLResponseMdw.selectSingleNode("//Response/faultstring").Text
            Else
                pvarMsgError = "Error en la ejecuci�n de LBAA_MWGenerico.lbaw_MQMW"
            End If
            enviarXMLaMDW = False
        End If
    Else
        pvarMsgError = "Error en la ejecuci�n de LBAA_MWGenerico.lbaw_MQMW"
        enviarXMLaMDW = False
    End If
    '
    Set wobjClass = Nothing
    Set wvarXMLResponseMdw = Nothing
        
End Function



Private Sub ObjectControl_Activate()

    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub












