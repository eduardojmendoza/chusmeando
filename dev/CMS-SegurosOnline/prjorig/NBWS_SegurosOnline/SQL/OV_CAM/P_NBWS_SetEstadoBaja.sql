SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO




/*
---------------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Programador: Matias Coaker
Nombre del Stored:  P_NBWS_SetEstadoBaja
Fecha de Creaci�n: 10/06/2009
Numero de PPCR: 50055/6010662
Descripci�n: Modifica el estado del usuario a Inactivo.
---------------------------------------------------------------------------------------
*/

ALTER   PROCEDURE P_NBWS_SetEstadoBaja
@MAIL char(50)
AS

SET NOCOUNT ON
SET xact_abort ON

BEGIN TRAN

UPDATE 	NBWS_Usuarios
       	SET Estado = 'B'
       	WHERE Mail=@MAIL AND ESTADO='A'

IF @@error <> 0
BEGIN
	ROLLBACK TRAN
	RETURN -1
END

COMMIT TRANSACTION

RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

