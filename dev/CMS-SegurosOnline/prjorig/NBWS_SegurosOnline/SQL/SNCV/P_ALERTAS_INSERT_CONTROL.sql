SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




/*
---------------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Programador: Matias Coaker
Nombre del Stored:  P_ALERTAS_INSERT_CONTROL
Fecha de Creaci�n: 17/03/2008
Numero de PPCR: 2008-00791
Descripci�n: Este stored se crea para hacer un INSERT de datos en la tabla de
alertas_control.
---------------------------------------------------------------------------------------
*/

ALTER  PROC P_ALERTAS_INSERT_CONTROL
	@IDSUSCRIPTOS INT,
	@CODOP CHAR(6),
	@IDCONTROL INT OUTPUT
AS
SET NOCOUNT ON

DECLARE @FECHA DATETIME
SET @FECHA = GETDATE()
SET xact_abort ON

BEGIN TRAN
	
	-- ESTADOS INICIAL = P (PENDIENTE)

	INSERT INTO ALERTAS_CONTROL 
	(IDSUSCRIPTOS, CODOP, FECHA, ESTADO)
	VALUES
	(@IDSUSCRIPTOS, @CODOP, @FECHA, 'P')
	IF(@@ERROR <> 0)
	BEGIN
		--ERROR
		ROLLBACK TRANSACTION
		RETURN -1
	END
COMMIT TRAN

SET xact_abort OFF

SELECT @IDCONTROL = @@identity




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

