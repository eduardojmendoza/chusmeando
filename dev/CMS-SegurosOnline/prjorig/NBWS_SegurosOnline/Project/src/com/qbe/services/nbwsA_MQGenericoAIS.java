package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_MQGenericoAIS implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * ************************************************************************************
   * ************************************************************************************
   * ****                   RUTA DE LOS ARCHIVOS DE DEFINICION                       ****
   * ****                   \\ntfsapp01des\Comp\NBWS\DefinicionesAIS                 ****
   * ************************************************************************************
   * ************************************************************************************
   * ************************************************************************************
   * DATOS PROPIOS DE CADA CLASE, TODO LO DEMAS SE PUEDE COPIAR SIN PROBLEMAS A UNA NUEVA CLASE
   * PARA DIVIDIR LA CARGA
   * ************************************************************************************
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_MQGenericoAIS";
  static final String mcteSubDirName = "DefinicionesAIS";
  static final String mcteLogPath = "LogAIS";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  private String mvarConsultaRealizada = "";
  private boolean mvarCancelacionManual = false;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * ************************************************************************************
   * *************************************************************
   * DE ACA PARA ABAJO SE PUEDE COPIAR TODO A CUALQUIER OTRA CLASE
   * *************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeWarning = new Variant();
    Variant vbLogEventTypeError = new Variant();
    String wvarOpID = "";
    XmlDomExtended wobjXMLPrueba = null;
    XmlDomExtended wobjXMLDefinition = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLLog = null;
    XmlDomExtended wobjXMLOldLog = null;
    XmlDomExtended wobjXMLRespuesta = null;
    org.w3c.dom.NodeList wobjNodosEntrada = null;
    org.w3c.dom.Node wobjNodoEntrada = null;
    org.w3c.dom.Element wobjNewNodo = null;
    MQConnectionConnector wobjFrame2MQ = null;
    int wvarGMOWaitInterval = 0;
    int wvarStep = 0;
    String wvarResult = "";
    java.util.Date wvarFechaConsulta = DateTime.EmptyDate;
    String wvarArea = "";
    String wvarLastArea = "";
    int wvarPos = 0;
    int wvarMaxLen = 0;
    String strParseString = "";
    String strParseStringTemp = "";
    String wvarstrXMLEnvioArea = "";
    String wvarEstado = "";
    String wvarCodigoError = "";
    String wvarConfFileName = "";
    String wvarDefinitionFile = "";
    String wvarXMLAreas = "";
    String wvarErrorValidacion = "";
    String wvarError = "";
    String wvarRequestRetorno = "";
    String wvarPathLog = "";
    boolean wvarAppendRequest = false;
    boolean wvarConsultarMQ = false;
    int wvarCantRellamRest = 0;
    String wvarXMLHarcodeado = "";
    String wvarAreaHarcodeada = "";
    int wvarCantLlamadasALoguear = 0;
    int wvarCounter = 0;
    int wvarCantLlamadasLogueadas = 0;
    String wvarFiltroEstado = "";
    String wvarDescripcion = "";
    int wvarMQError = 0;
    //
    //
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarFechaConsulta = DateTime.now();
      //
      // ***************************************************************************************************
      //Levanto los XMLs de Configuracion del area MQ y del Request de la Paguna
      // ***************************************************************************************************
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLDefinition = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" )  );
      mvarConsultaRealizada = Strings.left( wvarDefinitionFile, Strings.len( wvarDefinitionFile ) - 4 );
      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );
      //
      wvarStep = 30;
      wvarOpID = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/MENSAJE" )  );
      //
      wvarStep = 31;
      wvarConfFileName = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//MQCONFIGFILE" )  );
      //
      wvarStep = 32;
      wvarGMOWaitInterval = Obj.toInt( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//TIMEOUT" )  ) );
      //
      // ***************************************************************************************************
      //Recorro los campos del XML y voy armando el area MQ segun los par�metros que llegan desde la p�gina
      // ***************************************************************************************************
      // *********************************************************************************************************************************
      //NOTA IMPORTANTISIMA: Este componente solo permite un nivel para los vectores, o sea que no funciona con un vector adentro de otro
      // *********************************************************************************************************************************
      wvarStep = 40;
      wobjNodosEntrada = wobjXMLDefinition.selectNodes( "//ENTRADA/CAMPO" ) ;
      wvarArea = "";
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
        if( wobjNodoEntrada.getChildNodes().getLength() == 0 )
        {
          //Es un Dato Normal
          wvarArea = wvarArea + invoke( "GetDatoFormateado", new Variant[] { new Variant(wobjXMLRequest.getDocument().getChildNodes().item( 0 )), new Variant("//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant( XmlDomExtended .getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("TipoDato") ) )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Enteros") )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Decimales") )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Default") )), new Variant(wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Obligatorio") )) } );
        }
        else
        {
          //Es un Vector
          wvarArea = wvarArea + invoke( "GetVectorDatos", new Variant[] { new Variant(wobjXMLRequest.getDocument().getChildNodes().item( 0 )), new Variant("//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(wobjNodoEntrada.getChildNodes()), new Variant((int)Math.rint( VB.val( new Variant( XmlDomExtended .getText( wobjNodoEntrada.getAttributes().getNamedItem( new Variant("Cantidad") ) )) ) )) } );
        }
      }
      //
      wvarStep = 50;
      //
      // ***********************************************************************
      //Se puede utilizar un Area IN / OUT preestablecidas para hacer pruebas
      // ***********************************************************************
      //
      wvarStep = 55;
      if( ! ((wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//AREAINMQ" )  == (org.w3c.dom.Node) null)) )
      {
        if( ! (wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" )  == (org.w3c.dom.Node) null) )
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarArea = Strings.mid( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//AREAOUTMQ" )  ), 5 );
        }
        else
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarArea = Strings.mid( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//AREAOUTMQ" )  ), 5 );
        }
      }
      //
      wvarStep = 55;
      if( ! ((wobjXMLRequest.selectSingleNode( "//AREAINMQ" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//AREAINMQ" )  == (org.w3c.dom.Node) null)) )
      {
        if( ! (wobjXMLRequest.selectSingleNode( "//AREAINMQ" )  == (org.w3c.dom.Node) null) )
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarAreaHarcodeada = Strings.mid( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//AREAINMQ" )  ), 5 );
        }
        else
        {
          //Desestimo el Nro de Mensaje porque se agrega despues
          wvarAreaHarcodeada = Strings.mid( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//AREAINMQ" )  ), 5 );
        }
      }
      //
      wvarPos = Strings.len( wvarOpID ) + Strings.len( wvarArea ) + 1;
      wvarXMLAreas = "<AreaIN>" + wvarOpID + wvarArea + "</AreaIN>";
      //
      // *****************************************************************
      //Si solo se solicita el Area MQ la devuelvo y salgo del componente
      // *****************************************************************
      //
      wvarStep = 60;
      if( ! ((wobjXMLRequest.selectSingleNode( "//SOLORETORNARAREA" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//SOLORETORNARAREA" )  == (org.w3c.dom.Node) null)) )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No se Procesa por la existencia del TAG SOLORETORNARAREA" + String.valueOf( (char)(34) ) + " />" + wvarXMLAreas + "</Response>" );
        //unsup GoTo Salir
      }
      //
      // ***********************************************************************
      //Se puede utilizar un XML de respuesta preestablecido para hacer pruebas
      // ***********************************************************************
      //
      wvarStep = 70;
      wvarConsultarMQ = true;
      if( ! ((wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//XMLPRUEBA" )  == (org.w3c.dom.Node) null)) )
      {
        wvarConsultarMQ = false;
        wobjXMLPrueba = new XmlDomExtended();
        if( ! (wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" )  == (org.w3c.dom.Node) null) )
        {
          wobjXMLPrueba.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + XmlDomExtended.getText( /*unsup wobjXMLRequest.selectSingleNode( "//XMLPRUEBA" ) */ ) );
        }
        else
        {
          wobjXMLPrueba.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + XmlDomExtended.getText( /*unsup wobjXMLDefinition.selectSingleNode( "//XMLPRUEBA" ) */ ) );
        }
        wvarXMLHarcodeado = XmlDomExtended.marshal(wobjXMLPrueba.getDocument().getDocumentElement());
        wobjXMLPrueba = null;
      }
      //
      wvarStep = 75;
      //
      // ***********************************************************************
      //Se puede utilizar un area preestablecida de Respuesta para hacer pruebas
      // ***********************************************************************
      //
      wvarCantRellamRest = 1;
      if( ! (wobjXMLDefinition.selectSingleNode( "//PAGINASRETORNADAS" )  == (org.w3c.dom.Node) null) )
      {
        wvarCantRellamRest = Obj.toInt( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//PAGINASRETORNADAS" )  ) );
      }
      if( ! (wobjXMLRequest.selectSingleNode( "//PAGINASRETORNADAS" )  == (org.w3c.dom.Node) null) )
      {
        wvarCantRellamRest = Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//PAGINASRETORNADAS" )  ) );
      }
      //
      if( wvarConsultarMQ )
      {
        //
        // ******************************************************************************************
        //Levanto los datos de la cola de MQ del archivo de configuraci�n para actualizar el TimeOut
        // ******************************************************************************************
        //
        wvarStep = 80;
        //
        wobjXMLConfig = new XmlDomExtended();
        wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wvarConfFileName));
        XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( ModGeneral.gcteGMOWaitInterval ) , String.valueOf( wvarGMOWaitInterval ) );
        //
        wvarStep = 90;
        //
        // ***************************************************
        //Componente que realiza la conexion MQ
        // ***************************************************
        //
        RealizarRellamado: 
        //
        if( wvarAreaHarcodeada.equals( "" ) )
        {
          wobjFrame2MQ = MQConnectionConnector.newInstance();
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarOpID & wvarArea, strParseStringHolder, wobjXMLConfig.xml);
	strParseString = strParseStringHolder.getValue();

        }
        else
        {
          strParseString = wvarAreaHarcodeada;
          wvarMQError = 0;
        }
        //
        wvarStep = 100;
        //
        //
        if( wvarMQError != 0 )
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
          //unsup GoTo Terminar
        }
        //
        wvarStep = 120;
        //
        wvarXMLAreas = wvarXMLAreas + "<AreaOUT>" + strParseString + "</AreaOUT>";
        //
        // ****************************************************************
        //Agregamos las Areas en el XML de respuesta si es que solicitaron
        // ****************************************************************
        //
        if( ! ((wobjXMLRequest.selectSingleNode( "//RETORNARAREAS" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//RETORNARAREAS" )  == (org.w3c.dom.Node) null)) )
        {
          wvarstrXMLEnvioArea = "<AreasMQ>" + wvarXMLAreas + "</AreasMQ>";
        }
        else
        {
          wvarstrXMLEnvioArea = "";
        }
        //
        wvarStep = 245;
        //
        // ****************************************************************
        //Agregamos en el XML de respuesta el Request si es que lo Solicitaron
        // ****************************************************************
        //
        if( ! ((wobjXMLRequest.selectSingleNode( "//RETORNARREQUEST" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//RETORNARREQUEST" )  == (org.w3c.dom.Node) null)) )
        {
          wvarRequestRetorno = Request;
          wvarAppendRequest = true;
        }
        else
        {
          wvarRequestRetorno = "";
          wvarAppendRequest = false;
        }
      }
      //
      wvarStep = 250;
      //Corto el estado
      wvarEstado = Strings.mid( strParseString, 19, 2 );
      //Corto el Codigo de Error
      wvarCodigoError = Strings.mid( strParseString, 21, 2 );
      //
      // ************************************************************
      //Cortamos el Area en caso que se determine el maximo del area
      // ************************************************************
      if( ! (wobjXMLDefinition.selectSingleNode( "//SALIDA" ) .getAttributes().getNamedItem( "MaxLen" ) == (org.w3c.dom.Node) null) )
      {
        wvarMaxLen = Obj.toInt( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//SALIDA" ) .getAttributes().getNamedItem( "MaxLen" ) ) );
        strParseString = Strings.mid( strParseString, 1, (wvarMaxLen + wvarPos) - 1 );
      }
      //
      // ***********************************************************************
      //Vamos Armando el Area en el String Temporal para agregar los rellamados
      // ***********************************************************************
      if( strParseStringTemp.equals( "" ) )
      {
        strParseStringTemp = strParseString;
      }
      else
      {
        strParseStringTemp = strParseStringTemp + Strings.mid( strParseString, wvarPos );
      }
      //
      if( (wvarEstado.equals( "ER" )) || ! ((wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']") )  == (org.w3c.dom.Node) null)) )
      {
        //
        // **********************************************************************************************
        //En el XML De Definicion se pueden establecer Estados Errones con descripciones predeterminadas
        // **********************************************************************************************
        wvarStep = 260;
        if( wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']") )  == (org.w3c.dom.Node) null )
        {
          wvarDescripcion = "SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA";
        }
        else
        {
          if( (!Strings.trim( wvarCodigoError ).equals( "" )) && ! ((wobjXMLDefinition.selectSingleNode( ("//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "' and @CodigoError='" + wvarCodigoError + "']") )  == (org.w3c.dom.Node) null)) )
          {
            wvarDescripcion = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "' and @CodigoError='" + wvarCodigoError + "']" ) .getAttributes().getNamedItem( "Descripcion" ) );
          }
          else
          {
            wvarDescripcion = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//ESTADOERRONEO[@CodigoEstado='" + wvarEstado + "']" ) .getAttributes().getNamedItem( "Descripcion" ) );
          }
        }
        Response.set( "<Response><Estado resultado='false' mensaje='" + wvarDescripcion + "' />" + wvarRequestRetorno + wvarstrXMLEnvioArea + "</Response>" );
        //
      }
      else if( (wvarEstado.equals( "TR" )) && ! ((wvarCantRellamRest == 1)) )
      {
        //Area Para el Rellamado
        wvarArea = Strings.mid( strParseString, 5, wvarPos - 1 );
        wvarCantRellamRest = wvarCantRellamRest - 1;
        //unsup GoTo RealizarRellamado
      }
      else
      {
        //
        wvarStep = 280;
        //
        //Area de la ultima llamada
        wvarLastArea = Strings.mid( strParseString, 1, wvarPos - 1 );
        strParseStringTemp = wvarLastArea + Strings.mid( strParseStringTemp, wvarPos );
        //
        wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseStringTemp), new Variant(wobjXMLDefinition.selectSingleNode( new Variant("//SALIDA") ) ), new Variant(wobjXMLRequest), new Variant(wvarAppendRequest), new Variant(wvarXMLHarcodeado) } );
        //
        wvarStep = 340;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + wvarstrXMLEnvioArea + "</Response>" );
        //
        if( ! (wobjXMLDefinition.selectSingleNode( "//SALIDA" ) .getAttributes().getNamedItem( "xPathParaVerificarExistenciaDeDatos" ) == (org.w3c.dom.Node) null) )
        {
          // ********************************************************************************************************
          //Se verifica la existencia de datos en la Respuesta segun el atributo xPathParaVerificarExistenciaDeDatos
          // ********************************************************************************************************
          wobjXMLRespuesta = new XmlDomExtended();
          if( wobjXMLRespuesta.loadXML( wvarResult ) )
          {
            if( wobjXMLRespuesta.selectNodes( XmlDomExtended .getText( null (*unsup wobjXMLDefinition.selectSingleNode( "//SALIDA" ) *).getAttributes().getNamedItem( "xPathParaVerificarExistenciaDeDatos" ) ) ) .getLength() == 0 )
            {
              Response.set( "<Response><Estado resultado='false' mensaje='NO SE ENCONTRARON DATOS' />" + wvarResult + wvarstrXMLEnvioArea + "</Response>" );
            }
          }
        }
        //
      }
      //
      Salir: 
      wvarStep = 350;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      Terminar: 

      try 
      {

        wvarStep = 360;
        wvarStep = 361;
        //
        // ****************************************************************
        //Generamos un Log en caso que se solicite
        // ****************************************************************
        //
        wvarCantLlamadasALoguear = 1;
        wvarFiltroEstado = "SIN_DETERMINAR";
        if( ! ((wobjXMLRequest.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null)) )
        {
          //DA
          wvarStep = 362;
          wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + wvarDefinitionFile;
          //
          if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null) )
          {
            if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
            {
              wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
            }
            if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
            {
              wvarCantLlamadasALoguear = Obj.toInt( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
            }
            if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "EstadosNoLogueables" ) == (org.w3c.dom.Node) null) )
            {
              wvarFiltroEstado = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "EstadosNoLogueables" ) );
            }
          }
          else
          {
            if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
            {
              wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
            }
            if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
            {
              wvarCantLlamadasALoguear = Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
            }
            if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "EstadosNoLogueables" ) == (org.w3c.dom.Node) null) )
            {
              wvarFiltroEstado = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "EstadosNoLogueables" ) );
            }
          }
          //
          //DA
          wvarStep = 363;
          if( ! (wvarPathLog.matches( "*\\" )) && ! (Obj.toBoolean( (wvarEstado.matches( "*" ) + wvarFiltroEstado + "*") )) )
          {

            //Genero el Log solo si en el nodo se especifico un nombre de archivo
            wobjXMLLog = new XmlDomExtended();
            //
            //DA
            wvarStep = 364;
            wobjXMLLog.loadXML( "<LOGs><LOG MensajeMQ='" + wvarOpID + "' InicioConsulta='" + DateTime.format( wvarFechaConsulta, "dd/MM/yyyy HH:mm:ss" ) + "' TiempoIncurrido = '" + (int)Math.rint( (DateTime.diff( wvarFechaConsulta, DateTime.now() ) * 86400) ) + " Seg' >" + Request + Response + "</LOG></LOGs>" );
            //
            //DA
            wvarStep = 365;
            wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreasMQ" );
            /*unsup wobjXMLLog.selectSingleNode( "//LOG" ) */.appendChild( wobjNewNodo );
            //
            //DA
            wvarStep = 366;
            //Agrego las areas a mano porque el LoadXML Falla con strings tan grandes como el Area Out
            wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreaIN" );
            /*unsup wobjXMLLog.selectSingleNode( "//LOG/AreasMQ" ) */.appendChild( wobjNewNodo );
            XmlDomExtended.setText( wobjNewNodo, wvarOpID + wvarArea );
            //
            //DA
            wvarStep = 368;
            wobjNewNodo = wobjXMLLog.getDocument().createElement( "AreaOUT" );
            /*unsup wobjXMLLog.selectSingleNode( "//LOG/AreasMQ" ) */.appendChild( wobjNewNodo );
            XmlDomExtended.setText( wobjNewNodo, strParseString );
            //
            //DA
            wvarStep = 369;
            if( !"" /*unsup this.Dir( wvarPathLog, 0 ) */.equals( "" ) )
            {
              //DA
              wvarStep = 370;
              //Ya existe el Log, entonces agrego la nueva consulta al LOG
              wobjXMLOldLog = new XmlDomExtended();
              wobjXMLOldLog.load( wvarPathLog );
              //DA
              wvarStep = 371;
              if( wobjXMLOldLog.selectSingleNode( "//LOGs" )  == (org.w3c.dom.Node) null )
              {
                // *********LOG NO VALIDO***********
                wobjXMLLog.save( wvarPathLog );
              }
              else
              {
                //DA
                wvarStep = 372;
                wvarCantLlamadasLogueadas = wobjXMLOldLog.selectSingleNode( "//LOGs" ) .getChildNodes().getLength();
                for( wvarCounter = 1; wvarCounter <= ((wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear) + 1); wvarCounter++ )
                {
                  /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.removeChild( wobjXMLOldLog.selectSingleNode( "//LOGs" ) .getChildNodes().item( 0 ) );
                }
                //DA
                wvarStep = 373;
                /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.appendChild( wobjXMLLog.selectSingleNode( "//LOGs" ) .getChildNodes().item( 0 ) );
                //DA
                wvarStep = 374;
                wobjXMLOldLog.save( wvarPathLog );
              }
            }
            else
            {
              //DA
              wvarStep = 375;
              wobjXMLLog.save( wvarPathLog );
            }
          }
        }
        //
        //~~~~~~~~~~~~~~~
        ClearObjects: 
        //~~~~~~~~~~~~~~~
        wobjXMLOldLog = null;
        wobjXMLLog = null;
        wobjXMLRequest = null;
        wobjXMLDefinition = null;
        wobjXMLRespuesta = null;
        wobjNewNodo = (org.w3c.dom.Element) null;
        wobjXMLConfig = null;
        return IAction_Execute;
        //
        //~~~~~~~~~~~~~~~
      }
      catch( Exception _e_ )
      {
        Err.set( _e_ );
        try 
        {
          //~~~~~~~~~~~~~~~
          //
          wvarError = Err.getError().getDescription();
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Warning= [" + Err.getError().getNumber() + "] - " + wvarError + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeWarning );

          if( mvarCancelacionManual )
          {
            Response.set( "<Response><Estado resultado='false' mensaje='" + wvarError + "' /></Response>" );
          }

          /*TBD mobjCOM_Context.SetComplete() ;*/
          IAction_Execute = 0;

          //unsup Resume ClearObjects
          Err.clear();
          return IAction_Execute;
          //~~~~~~~~~~~~~~~
        }
        catch( Exception _e2_ )
        {
        }
      }
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wvarError = Err.getError().getDescription();
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + wvarError + " Mensaje:" + wvarOpID + " Area:" + wvarOpID + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );

        if( mvarCancelacionManual )
        {
          Response.set( "<Response><Estado resultado='false' mensaje='" + wvarError + "' /></Response>" );
          /*TBD mobjCOM_Context.SetComplete() ;*/
          IAction_Execute = 0;
        }
        else
        {
          /*TBD mobjCOM_Context.SetAbort() ;*/
          IAction_Execute = 1;
        }
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( int pvarPos, String pstrParseString, org.w3c.dom.Node pobjNodoDefiniciones, diamondedge.util.XmlDom pobjXMLRequest, boolean pvarAppendRequest, String pvarXMLHarcodeado ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarEvalString = "";
    XmlDomExtended wobjXslDOMResp = null;
    XmlDomExtended wobjXmlDOMResp = null;
    XmlDomExtended wobjXMLRequestTemp = null;
    String wvarXSLFile = "";
    org.w3c.dom.Node wobjNodoXSL = null;
    org.w3c.dom.Node wobjNodoParseo = null;
    String wvarNuevoXML = "";

    wobjXmlDOMResp = new XmlDomExtended();
    if( pvarXMLHarcodeado.equals( "" ) )
    {
      // *****************************************************************************
      //Envio el Area Recibida junto con el primer Pattern para que se genere el XML
      // *****************************************************************************
      wvarEvalString = Strings.mid( pstrParseString, pvarPos );
      //En la primera llamada determino si es un unico registro o un vector
      if( pobjNodoDefiniciones.getChildNodes().item( 0 ).getChildNodes().getLength() > 1 )
      {
        //Es un campo compuesto, se entiende que no es un vector
        wobjNodoParseo = invoke( "GetNodoParseado", new Variant[] { new Variant(wvarEvalString), new Variant(pobjNodoDefiniciones.getChildNodes().item( 0 )), new Variant(1) } );
      }
      else
      {
        //Tiene definido el largo de un solo campo, se entiene que es un vector
        wobjNodoParseo = invoke( "GetNodoParseado", new Variant[] { new Variant(wvarEvalString), new Variant(pobjNodoDefiniciones.getChildNodes().item( 0 )), new Variant(-1) } );
      }
      if( ! (wobjNodoParseo == (org.w3c.dom.Node) null) )
      {
        wobjXmlDOMResp.loadXML( Strings.replace( Strings.replace( wobjNodoParseo.toString(), "&gt;", ">" ), "&lt;", "<" ) );
      }
    }
    else
    {
      // *****************************************************************************
      //Genero el XML con lo harcodeado
      // *****************************************************************************
      wobjXmlDOMResp.loadXML( pvarXMLHarcodeado );
    }
    //
    //Quiere decir que levanto un XML Valido
    if( !wobjXmlDOMResp.getDocument().getDocumentElement().toString().equals( "" ) )
    {
      //
      // *************************************************************************************
      //Se agrega el Request en el area de respuesta en caso que haya sido solicitado
      //Esto sirve para utilizarlo si se aplica un XSL que necesite algun dato de la llamada
      // *************************************************************************************
      if( pvarAppendRequest )
      {
        wobjXMLRequestTemp = new XmlDomExtended();
        wobjXMLRequestTemp.loadXML( pobjXMLRequest.getDocument().getDocumentElement().toString() );
        wobjXmlDOMResp.getDocument().getChildNodes().item( 0 ).appendChild( ModGeneral.GetRequestRetornado( wobjXMLRequestTemp, /*unsup pobjNodoDefiniciones.selectSingleNode( "//ENTRADA" ) */, Strings.mid( pstrParseString, 1, pvarPos ) ) );
        wobjXMLRequestTemp = null;
      }
      //
      // ***********************************************************************************
      //Aplico el o los XSL en caso que se solicite y efectivamente existan para aplicar
      // ***********************************************************************************
      wobjXslDOMResp = new XmlDomExtended();
      for( int nwobjNodoXSL = 0; nwobjNodoXSL < pobjXMLRequest.selectNodes( "//AplicarXSL" ) .getLength(); nwobjNodoXSL++ )
      {
        wobjNodoXSL = pobjXMLRequest.selectNodes( "//AplicarXSL" ) .item( nwobjNodoXSL );
        wvarXSLFile = XmlDomExtended.getText( wobjNodoXSL );
        if( !"" /*unsup this.Dir( (System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarXSLFile), 0 ) */.equals( "" ) )
        {
          wobjXslDOMResp.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarXSLFile );
          wvarNuevoXML = wobjXmlDOMResp.transformNode( wobjXslDOMResp ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          if( ! (wobjXmlDOMResp.loadXML( wvarNuevoXML )) )
          {
            //Si no pudo levantar el XML luego de transformado, retorno el resultado de esa transformacion sin importar las que sigan despues
            ParseoMensaje = wvarNuevoXML;
            return IAction_Execute;
          }
        }
      }
    }
    ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());
    //
    ClearObjects: 
    wobjNodoXSL = (org.w3c.dom.Node) null;
    wobjXmlDOMResp = null;
    wobjXslDOMResp = null;
    return ParseoMensaje;
  }

  private org.w3c.dom.Node GetNodoParseado( String pvarstrToParse, org.w3c.dom.Node pobjXMLDefinicion, int pvarCantidadRegistros ) throws Exception
  {
    org.w3c.dom.Node GetNodoParseado = null;
    org.w3c.dom.Element wobjNodo = null;
    org.w3c.dom.Element wobjNewNodo = null;
    org.w3c.dom.Element wobjNewNodoCopy = null;
    org.w3c.dom.Element wobjNodoContenedor = null;
    XmlDomExtended wobjXMLDOM = null;
    String wvarstrXML = "";
    int wvarCounter = 0;
    VbScript_RegExp wobjRegExp = new VbScript_RegExp();
    MatchCollection wobjColMatch;
    Match wobjMatch = null;
    String wvarParseEvalString = "";
    int wvarCcurrRegistro = 0;
    Variant wvarLastValue = new Variant();
    String wvarNombreOcurrencia = "";
    boolean wvarAreaConDatos = false;
    //
    //
    //RegExp
    //MatchCollection
    //Match
    // ************************************
    //Conversion del Area de Salida en XML
    // ************************************
    //
    wobjXMLDOM = new XmlDomExtended();
    wobjRegExp = new VbScript_RegExp();

    wvarParseEvalString = Strings.replace( pvarstrToParse, " ", "_" );
    wvarstrXML = XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Nombre" ) );
    wobjXMLDOM.loadXML( "<" + wvarstrXML + "></" + wvarstrXML + ">" );

    if( ! (pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) == (org.w3c.dom.Node) null) )
    {
      wvarNombreOcurrencia = XmlDomExtended.getText( pobjXMLDefinicion.getAttributes().getNamedItem( "NombrePorOcurrencia" ) );
    }
    else
    {
      wvarNombreOcurrencia = "";
    }
    //
    // *******************************
    //Evaluo el area segun el Pattern
    // *******************************
    wobjRegExp.setGlobal( true ); 
    wobjRegExp.setPattern( XmlDomExtended .getText( pobjXMLDefinicion.getAttributes().getNamedItem( "Pattern" ) ) );
    wobjColMatch = wobjRegExp.Execute( wvarParseEvalString );
    //
    // *************************************
    //Recorro el resultado de la evaluacion
    // *************************************
    wvarCcurrRegistro = 0;
    wvarLastValue.set( "---" );
    wvarAreaConDatos = true;
    for( int nwobjMatch = 0; nwobjMatch < wobjColMatch.getCount(); nwobjMatch++ )
    {
      //Cada Registro Encontrado
      if( ! ((wvarCcurrRegistro < pvarCantidadRegistros)) && (pvarCantidadRegistros != -1) )
      {
        break;
      }
      //
      if( !wvarNombreOcurrencia.equals( "" ) )
      {
        wobjNodoContenedor = wobjXMLDOM.getDocument().createElement( wvarNombreOcurrencia );
        /*unsup wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ) */.appendChild( wobjNodoContenedor );
      }
      else
      {
        wobjNodoContenedor = (org.w3c.dom.Element) wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ) ;
      }
      //
      wvarCcurrRegistro = wvarCcurrRegistro + 1;
      //
      for( wvarCounter = 0; wvarCounter <= (pobjXMLDefinicion.getChildNodes().getLength() - 1); wvarCounter++ )
      {
        //Aca esta la definicion de cada campo
        //
        wobjNodo = (org.w3c.dom.Element) pobjXMLDefinicion.getChildNodes().item( wvarCounter );
        if( wobjNodo.getChildNodes().getLength() == 0 )
        {
          //error: function 'SubMatches' was not found.
          //unsup: wvarLastValue = Trim(Strings.replace(wobjMatch.SubMatches(wvarCounter), "_", " "))
          if( Strings.toUpperCase( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "Visible" ) ) ).equals( "SI" ) )
          {
            wobjNewNodo = wobjXMLDOM.getDocument().createElement( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "Nombre" ) ) );
            wobjNodoContenedor.appendChild( wobjNewNodo );
            //
            
            if( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
            {
              XmlDomExtended.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue.toString() ) ) );
            }
            else if( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
            {
              XmlDomExtended.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue.toString() ) / Math.pow( 10, VB.val( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "Decimales" ) ) ) ) ) );
            }
            else if( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
            {
              XmlDomExtended.setText( wobjNewNodo, Strings.right( wvarLastValue.toString(), 2 ) + "/" + Strings.mid( wvarLastValue.toString(), 5, 2 ) + "/" + Strings.left( wvarLastValue.toString(), 4 ) );
            }
            else
            {
              XmlDomExtended.setText( wobjNewNodo, Strings.trim( wvarLastValue.toString() ) );
            }
            //
            wobjNewNodoCopy = (org.w3c.dom.Element) null;
            if( ! (wobjNodo.getAttributes().getNamedItem( "CopyTo" ) == (org.w3c.dom.Node) null) )
            {
              wobjNewNodoCopy = wobjXMLDOM.getDocument().createElement( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "CopyTo" ) ) );
              wobjNodoContenedor.appendChild( wobjNewNodoCopy );
              XmlDomExtended.setText( wobjNewNodoCopy, XmlDomExtended.getText( wobjNewNodo ) );
            }
            //
            //Verifico si se solicita algun Formato en especial del Origen
            if( ! (wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) == (org.w3c.dom.Node) null) )
            {
              XmlDomExtended.setText( wobjNewNodo, Strings.format( XmlDomExtended .getText( wobjNewNodo ), XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuput" ) ) ) );
            }
            //
            //Verifico si se solicita algun Formato en especial del Destino
            if( ! ((wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) == (org.w3c.dom.Node) null)) && ! ((wobjNewNodoCopy == (org.w3c.dom.Element) null)) )
            {
              XmlDomExtended.setText( wobjNewNodoCopy, Strings.format( XmlDomExtended .getText( wobjNewNodo ), XmlDomExtended.getText( wobjNodo.getAttributes().getNamedItem( "FormatOuputCopy" ) ) ) );
            }
            //
            //Verifico si se solicita cubrir el valor del campo en un CData
            if( ! (wobjNodo.getAttributes().getNamedItem( "CData" ) == (org.w3c.dom.Node) null) )
            {
              if( XmlDomExtended .getText( wobjNodo.getAttributes().getNamedItem( "CData" ) ).equals( "SI" ) )
              {
                XmlDomExtended.setText( wobjNewNodo, "<![CDATA[" + XmlDomExtended.getText( wobjNewNodo ) + "]]>" );
              }
            }
          }
          if( wobjNodo.selectNodes( ".[./@Obligatorio='SI']" ) .getLength() != 0 )
          {
            if( ( XmlDomExtended .getText( wobjNewNodo ).equals( "" )) || ( XmlDomExtended .getText( wobjNewNodo ).equals( "0" )) )
            {
              //El Dato Obligatorio no esta informado, por lo que no debe retornar ni este registro ni todos sus hijos
              wvarAreaConDatos = false;
            }
          }
        }
        else
        {
          //El nodo tiene hijos por lo que se debe procesar con un Pattern nuevo
          //MC - 30/08/2007 Si el valor anterior es muy grande lo reemplaza por -1
          if( Strings.len( wvarLastValue.toString() ) > 10 )
          {
            wvarLastValue.set( -1 );
          }
          if( ! (wvarLastValue.isNumeric()) )
          {
            wvarLastValue.set( -1 );
          }
          //error: function 'SubMatches' was not found.
          //unsup: Set wobjNewNodo = GetNodoParseado(wobjMatch.SubMatches(wvarCounter), wobjNodo, Val(wvarLastValue))
          if( ! (wobjNewNodo == (org.w3c.dom.Element) null) )
          {
            /*unsup wobjXMLDOM.selectSingleNode( "//" + wvarstrXML ) */.appendChild( wobjNewNodo );
          }
        }
      }
    }

    if( wvarAreaConDatos )
    {
      GetNodoParseado = wobjXMLDOM.getDocument().getChildNodes().item( 0 );
    }

    ClearObjects: 
    wobjNodoContenedor = (org.w3c.dom.Element) null;
    wobjXMLDOM = null;
    wobjNewNodo = (org.w3c.dom.Element) null;
    wobjNewNodoCopy = (org.w3c.dom.Element) null;
    wobjNodo = (org.w3c.dom.Element) null;
    wobjRegExp = (VbScript_RegExp) null;
    wobjMatch = null;
    wobjColMatch = (void) null;
    return GetNodoParseado;
  }

  private String GetVectorDatos( org.w3c.dom.Node pobjNodo, String pVarPathVector, org.w3c.dom.NodeList pobjSubNodosSolicitados, int pVarCantElementos ) throws Exception
  {
    String GetVectorDatos = "";
    int wvarCounterRelleno = 0;
    int wvarActualCounter = 0;
    int wvarActualNodoSolicitado = 0;
    String wvarNodoNombre = "";
    int wvarNodoTipoDato = 0;
    int wvarNodoLargoDato = 0;
    int wvarNodoCantDecimales = 0;
    org.w3c.dom.NodeList wobjSelectedNodos = null;
    org.w3c.dom.Element Nodo = null;

    wvarActualCounter = 1;
    if( ! (pobjNodo == (org.w3c.dom.Node) null) )
    {
      wobjSelectedNodos = pobjNodo.selectNodes( pVarPathVector ) ;

      for( wvarActualCounter = 0; wvarActualCounter <= (wobjSelectedNodos.getLength() - 1); wvarActualCounter++ )
      {
        //Recorro todos los nodos que formaran el vector
        for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= pobjSubNodosSolicitados.getLength(); wvarActualNodoSolicitado++ )
        {
          //Recorro todos los nodos que se solicitan para cada uno
          Nodo = (org.w3c.dom.Element) pobjSubNodosSolicitados.item( wvarActualNodoSolicitado - 1 );
          if( Nodo.getChildNodes().getLength() == 0 )
          {
            //Es un Dato simple
            GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(wobjSelectedNodos.item( wvarActualCounter )), new Variant("./" + XmlDomExtended.getText( Nodo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant( XmlDomExtended .getText( Nodo.getAttributes().getNamedItem( new Variant("TipoDato") ) )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Enteros") )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Decimales") )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Obligatorio") )), new Variant(null) } );
          }
        }
        if( wvarActualCounter == (pVarCantElementos - 1) )
        {
          return IAction_Execute;
        }
      }
    }
    for( wvarCounterRelleno = wvarActualCounter; wvarCounterRelleno <= (pVarCantElementos - 1); wvarCounterRelleno++ )
    {
      for( wvarActualNodoSolicitado = 1; wvarActualNodoSolicitado <= pobjSubNodosSolicitados.getLength(); wvarActualNodoSolicitado++ )
      {
        Nodo = (org.w3c.dom.Element) pobjSubNodosSolicitados.item( wvarActualNodoSolicitado - 1 );
        if( Nodo.getChildNodes().getLength() == 0 )
        {
          //Es un Dato simple
          GetVectorDatos = GetVectorDatos + invoke( "GetDatoFormateado", new Variant[] { new Variant(null), new Variant("//" + XmlDomExtended.getText( Nodo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant( XmlDomExtended .getText( Nodo.getAttributes().getNamedItem( new Variant("TipoDato") ) )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Enteros") )), new Variant(Nodo.getAttributes().getNamedItem( new Variant("Decimales") )), new Variant(null), new Variant(null) } );
        }
      }
    }
    ClearObjects: 
    wobjSelectedNodos = (org.w3c.dom.NodeList) null;
    return GetVectorDatos;
  }

  private String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault, org.w3c.dom.Node pobjObligatorio ) throws Exception
  {
    String GetDatoFormateado = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;
    String wvarErrorValidacion = "";
    String[] Arr = null;

    if( ! (pobjXMLContenedor == (org.w3c.dom.Node) null) )
    {
      //
      wvarErrorValidacion = ModGeneral.GetErrorInformacionDato( pobjXMLContenedor, pvarPathDato, pvarTipoDato, pobjLongitud, pobjDecimales, pobjDefault, pobjObligatorio );
      if( ! (wvarErrorValidacion.equals( "" )) )
      {
        mvarCancelacionManual = true;
        Err.raise( -1, mcteClassName + "--" + mvarConsultaRealizada, wvarErrorValidacion );
      }
      //
      wobjNodoValor = pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) ;
      if( wobjNodoValor == (org.w3c.dom.Node) null )
      {
        if( ! (pobjDefault == (org.w3c.dom.Node) null) )
        {
          wvarDatoValue = XmlDomExtended.getText( pobjDefault );
        }
      }
      else
      {
        wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
      }
    }
    //
    
    if( pvarTipoDato.equals( "TEXTO" ) )
    {
      //Dato del Tipo String
      GetDatoFormateado = Strings.left( wvarDatoValue + Strings.space( (int)Math.rint( VB.val( XmlDomExtended .getText( pobjLongitud ) ) ) ), (int)Math.rint( VB.val( XmlDomExtended .getText( pobjLongitud ) ) ) );
    }
    else if( pvarTipoDato.equals( "ENTERO" ) )
    {
      //Dato del Tipo Entero
      GetDatoFormateado = ModGeneral.CompleteZero( wvarDatoValue, (int)Math.rint( VB.val( XmlDomExtended .getText( pobjLongitud ) ) ) );
    }
    else if( pvarTipoDato.equals( "DECIMAL" ) )
    {
      //Dato del Tipo "Con Decimales"
      if( new Variant( wvarDatoValue ).isNumeric() )
      {
        wvarCampoNumerico = Obj.toDouble( Strings.replace( wvarDatoValue, ",", "." ) );
      }
      for( wvarCounter = 1; wvarCounter <= Obj.toInt( XmlDomExtended .getText( pobjDecimales ) ); wvarCounter++ )
      {
        wvarCampoNumerico = wvarCampoNumerico * 10;
      }
      GetDatoFormateado = ModGeneral.CompleteZero( String.valueOf( wvarCampoNumerico ), (int)Math.rint( VB.val( XmlDomExtended .getText( pobjLongitud ) ) + VB.val( XmlDomExtended .getText( pobjDecimales ) ) ) );
    }
    else if( pvarTipoDato.equals( "FECHA" ) )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( wvarDatoValue.matches( "*/*/*" ) )
      {
        Arr = Strings.split( wvarDatoValue, "/", -1 );
        GetDatoFormateado = Arr[2] + ModGeneral.CompleteZero( Arr[1], 2 ) + ModGeneral.CompleteZero( Arr[0], 2 );
      }
      else
      {
        GetDatoFormateado = "00000000";
      }
    }
    else if( pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
    {
      //Dato del Tipo String alineado a la izquierda
      GetDatoFormateado = Strings.right( Strings.space( (int)Math.rint( VB.val( XmlDomExtended .getText( pobjLongitud ) ) ) ) + wvarDatoValue, (int)Math.rint( VB.val( XmlDomExtended .getText( pobjLongitud ) ) ) );
    }
    //
    return GetDatoFormateado;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
