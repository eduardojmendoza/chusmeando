package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Request para probar <Request><INSCTRL>N</INSCTRL></Request>
 * Objetos del FrameWork
 */

public class nbwsA_FillScheduler implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Archivo de Configuracion
   */
  static final String mcteEPolizaConfig = "NBWS_ePolizaConfig.xml";
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_FillScheduler";
  /**
   * Stored Procedures
   */
  static final String mcteStoreProcInsBandejaSCH = "P_CP_SA_BANDEJA_INSERT";
  /**
   *  ANTERIORES
   */
  static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
  /**
   *  SI NUEVO
   */
  static final String mcteStoreProcInsLog = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG";
  /**
   * 
   */
  static final String mcteParam_INSCTRL = "//INSCTRL";
  /**
   * 
   */
  static final String mcteOperacion = "FILLSCH";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  private com.qbe.services.NODOINSERTASCH mvarNodoInsertaSch = new com.qbe.services.NODOINSERTASCH();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    String wvarRequest = "";
    Variant wvarResponse = new Variant();
    String wvarResponseNYL = "";
    String wvarResponseLBA = "";
    String wvarResult = "";
    XmlDomExtended wobjXMLRespuestas = null;
    org.w3c.dom.Node wobjXMLConfigNode = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLRequest = null;
    int mvarIDCONTROL = 0;
    String mvarINSCTRL = "";
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    String mvarFECHA = "";
    String mvarCODOP = "";
    int wvarCount = 0;
    String wvarFalseAIS = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializaci�n de variables
      wvarStep = 10;
      mvarFECHA = Strings.right( "00" + DateTime.month( DateTime.now() ), 2 ) + "-" + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + "-" + DateTime.year( DateTime.now() );
      mvarINSCTRL = "N";
      //
      //Lectura de par�metros
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 30;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_INSCTRL )  == (org.w3c.dom.Node) null) )
      {
        mvarINSCTRL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSCTRL )  );
      }
      //
      //Registraci�n en Log de inicio de ejecuci�n
      wvarStep = 40;
      //insertLog mcteOperacion, "I-NBWS_FILLSCH - Nueva ejecuci�n"
      wvarRequest = "";


      wvarStep = 50;
      //P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
      wvarRequest = wvarRequest + "<Request id=\"1\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "   <DEFINICION>LBA_1187_ListadoePolizas.xml</DEFINICION>" + "   <LNK-CIAASCOD>0001</LNK-CIAASCOD>" + "</Request>";




      wvarStep = 60;
      //Ejecuta la funci�n Multithreading
      wvarRequest = "<Request>" + wvarRequest + "</Request>";
      ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );


      wobjXMLRespuestas = new XmlDomExtended();



      wobjXMLRespuestas.loadXML( wvarResponse.toString() );

      //Carga las dos respuestas en un XML.
      wvarStep = 70;
      if( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado" )  == (org.w3c.dom.Node) null )
      {
        //
        //Fall� Response 1
        Err.getError().setDescription( "Response 1: fall� COM+ de Request 1" );
        //unsup GoTo ErrorHandler
        //
      }
      //
      wvarStep = 80;

      wvarResponseNYL = "";

      wvarStep = 90;

      if( ! (wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/IMPRESOS" )  == (org.w3c.dom.Node) null) )
      {
        wvarResponseLBA = XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS" ));
        wvarResponseLBA = Strings.replace( Strings.replace( wvarResponseLBA, "<CAMPOS>", "" ), "</CAMPOS>", "" );

        wvarResponseLBA = "<Response>" + Strings.replace( Strings.replace( wvarResponseLBA, "<IMPRESOS>", "" ), "</IMPRESOS>", "" ) + "</Response>";

      }
      else
      {
        wvarResponseLBA = "";
        wvarFalseAIS = wvarFalseAIS + String.valueOf( (char)(13) ) + XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado/@mensaje" )  );
      }
      wvarStep = 100;

      if( !wvarResponseLBA.equals( "" ) )
      {


        if( ! (invoke( "insertaImpresoSCH", new Variant[] { new Variant(wvarResponseLBA), new Variant("0001"), new Variant(mvarFECHA) } )) )
        {
          //unsup GoTo ErrorHandler
        }


      }

      if( wvarResponseLBA.equals( "" ) )
      {
        //unsup GoTo FalseAIS
      }


      wvarStep = 110;


      //Registraci�n en Log de fin de proceso
      if( mvarProcConErrores )
      {
        //insertLog mcteOperacion, "E- Fin NBWS_FILLSCH. Estado: ERROR"
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        //insertControlSCH
        // If mvarINSCTRL = "S" Then
        // insertControlSCH mvarFECHA
        //  End If
        // insertLog mcteOperacion, "0- Fin NBWS_FILLSCH. Estado: OK"
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }

      wvarStep = 120;
      FalseAIS: 
      if( wvarResponse.toString().equals( "" ) )
      {
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarFalseAIS + "\"></Estado><Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
      }
      //
      //FIN
      //
      wvarStep = 130;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 140;

      return IAction_Execute;



      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        //insertLog mcteOperacion, mcteClassName & " - " & _
        //                      wcteFnName & " - " & _
        //error: syntax error: near " & ":
        //unsup: wvarStep & " - " & _
        //insertLog mcteOperacion, "99- Fin NBWS_FILLSCH. Estado: ABORTADO POR ERROR"
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  public boolean insertaImpresoSCH( String pvarResponse, String pvarCiaascod, String pvarFECHA ) throws Exception
  {
    boolean insertaImpresoSCH = false;
    int mvarx = 0;
    String wvarNombreArchivos = "";
    XmlDomExtended wobjXMLImpresos = null;
    XmlDomExtended wobjXMLDOMArchivos = null;
    XmlDomExtended wobjXSLResponse = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.NodeList wobjXMLImpresosList = null;
    org.w3c.dom.NodeList wobjXMLNombreArchivos = null;
    org.w3c.dom.NodeList wobjXMLFormudes = null;
    org.w3c.dom.Node wobjXMLArchivos = null;
    String wvarResult = "";



    wobjXMLImpresos = new XmlDomExtended();
    /*unsup wobjXMLImpresos.setProperty( "SelectionLanguage", "XPath" ) */;
    wobjXMLImpresos.loadXML( pvarResponse );

    wobjXSLResponse = new XmlDomExtended();
    wobjXSLResponse.loadXML( p_GetXSL());


    wvarResult = wobjXMLImpresos.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

    wobjXMLDOMArchivos = new XmlDomExtended();
    /*unsup wobjXMLDOMArchivos.setProperty( "SelectionLanguage", "XPath" ) */;
    wobjXMLDOMArchivos.loadXML( wvarResult );


    //wvarResult = wobjXMLImpresos.transformNode(wobjXSLResponse)
    mvarNodoInsertaSch.NOMARCH = "";



    wobjXMLImpresosList = wobjXMLDOMArchivos.selectNodes( "//IMPRESO[not(AGRUPAR=preceding-sibling::IMPRESO/AGRUPAR)]" ) ;


    for( int nwobjXMLNode = 0; nwobjXMLNode < wobjXMLImpresosList.getLength(); nwobjXMLNode++ )
    {
      wobjXMLNode = wobjXMLImpresosList.item( nwobjXMLNode );

      mvarNodoInsertaSch.NOMARCH = "";
      mvarNodoInsertaSch.CIAASCOD = pvarCiaascod;
      mvarNodoInsertaSch.RAMOPCOD = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "RAMOPCOD" )  );
      mvarNodoInsertaSch.POLIZANN = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "POLIZANN" )  );
      mvarNodoInsertaSch.POLIZSEC = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "POLIZSEC" )  );
      mvarNodoInsertaSch.CERTIPOL = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CERTIPOL" )  );
      mvarNodoInsertaSch.CERTIANN = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CERTIANN" )  );
      mvarNodoInsertaSch.CERTISEC = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CERTISEC" )  );
      mvarNodoInsertaSch.OPERAPOL = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "OPERAPOL" )  );
      mvarNodoInsertaSch.DOCUMTIP = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "DOCUMTIP" )  );
      mvarNodoInsertaSch.DOCUMDAT = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "DOCUMDAT" )  );
      mvarNodoInsertaSch.CLIENAP1 = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CLIENAP1" )  );
      mvarNodoInsertaSch.CLIENAP2 = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CLIENAP2" )  );
      mvarNodoInsertaSch.CLIENNOM = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CLIENNOM" )  );


      wobjXMLNombreArchivos = wobjXMLDOMArchivos.selectNodes( "//IMPRESO[CIAASCOD='" + mvarNodoInsertaSch.CIAASCOD + "' and RAMOPCOD='" + mvarNodoInsertaSch.RAMOPCOD + "' and POLIZANN='" + mvarNodoInsertaSch.POLIZANN + "' and POLIZSEC='" + mvarNodoInsertaSch.POLIZSEC + "' and CERTIPOL='" + mvarNodoInsertaSch.CERTIPOL + "' and CERTIANN='" + mvarNodoInsertaSch.CERTIANN + "' and CERTISEC='" + mvarNodoInsertaSch.CERTISEC + "' and OPERAPOL='" + mvarNodoInsertaSch.OPERAPOL + "' ]/NOMARCH" ) ;

      wobjXMLFormudes = wobjXMLDOMArchivos.selectNodes( "//IMPRESO[CIAASCOD='" + mvarNodoInsertaSch.CIAASCOD + "' and RAMOPCOD='" + mvarNodoInsertaSch.RAMOPCOD + "' and POLIZANN='" + mvarNodoInsertaSch.POLIZANN + "' and POLIZSEC='" + mvarNodoInsertaSch.POLIZSEC + "' and CERTIPOL='" + mvarNodoInsertaSch.CERTIPOL + "' and CERTIANN='" + mvarNodoInsertaSch.CERTIANN + "' and CERTISEC='" + mvarNodoInsertaSch.CERTISEC + "' and OPERAPOL='" + mvarNodoInsertaSch.OPERAPOL + "' ]/FORMUDES" ) ;



      for( mvarx = 0; mvarx <= (wobjXMLNombreArchivos.getLength() - 1); mvarx++ )
      {

        mvarNodoInsertaSch.NOMARCH = mvarNodoInsertaSch.NOMARCH + "<ARCHIVO>" + Strings.replace( XmlDomExtended.marshal(wobjXMLNombreArchivos.item( mvarx )), String.valueOf( (char)(32) ), "_" ) + XmlDomExtended.marshal(wobjXMLFormudes.item( mvarx )) + "</ARCHIVO>";

      }


      mvarNodoInsertaSch.SWSUSCRI = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "SWSUSCRI" )  );
      mvarNodoInsertaSch.MAIL = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "MAIL" )  );
      mvarNodoInsertaSch.CLAVE = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CLAVE" )  );
      mvarNodoInsertaSch.SWCLAVE = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "SW-CLAVE" )  );
      mvarNodoInsertaSch.SWENDOSO = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "SWENDOSO" )  );
      mvarNodoInsertaSch.OPERATIP = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "OPERATIP" )  );
      mvarNodoInsertaSch.ESTADO = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "ESTADO" )  );
      mvarNodoInsertaSch.CODESTAD = XmlDomExtended.getText( wobjXMLNode.selectSingleNode( "CODESTAD" )  );


      if( !mvarNodoInsertaSch.RAMOPCOD.equals( "" ) )
      {
        // Inserta c/u de los registros en la Bandeja de Jobs
        if( ! (invoke( "insertScheduler", new Variant[] { new Variant(mvarNodoInsertaSch), new Variant(pvarFECHA) } )) )
        {
          insertaImpresoSCH = false;
          return insertaImpresoSCH;

        }
      }

    }

    wobjXMLNode = (org.w3c.dom.Node) null;
    wobjXMLImpresos = null;
    insertaImpresoSCH = true;


    return insertaImpresoSCH;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:template match='/'>";
    wvarStrXSL = wvarStrXSL + " <Request>";
    wvarStrXSL = wvarStrXSL + "  <xsl:for-each select='//IMPRESO'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:sort select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)' data-type='text' order='ascending'/>";
    wvarStrXSL = wvarStrXSL + " <xsl:if test='RAMOPCOD != \"\"' >";

    wvarStrXSL = wvarStrXSL + " <IMPRESO>";

    wvarStrXSL = wvarStrXSL + "<AGRUPAR><xsl:value-of select='concat(CIAASCOD,RAMOPCOD,POLIZANN,POLIZSEC,CERTIPOL,CERTIANN,CERTISEC,OPERAPOL)'/></AGRUPAR>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CIAASCOD'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='RAMOPCOD'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='POLIZANN'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='POLIZSEC'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTIPOL'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTIANN'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CERTISEC'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='OPERAPOL'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='FORMUDES'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='DOCUMTIP'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='DOCUMDAT'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENAP1'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENAP2'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLIENNOM'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='NOMARCH'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SWSUSCRI'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='MAIL'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CLAVE'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SW-CLAVE'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='SWENDOSO'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='OPERATIP'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='ESTADO'/>";
    wvarStrXSL = wvarStrXSL + "    <xsl:copy-of select='CODESTAD'/>";
    wvarStrXSL = wvarStrXSL + " </IMPRESO>";
    wvarStrXSL = wvarStrXSL + " </xsl:if >";

    wvarStrXSL = wvarStrXSL + "   </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + " </Request>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private boolean insertScheduler( com.qbe.services.NODOINSERTASCH pvarNodo, String pvarFECHA ) throws Exception
  {
    boolean insertScheduler = false;
    String wvarCodusu = "";
    String wvarAplicacion = "";
    String wvarDataIn = "";
    //
    //
    wvarAplicacion = "NBWS_ePoliza_Exe";

    wvarCodusu = "NBWSFILLSCH";
    wvarDataIn = "<CIAASCOD>" + pvarNodo.CIAASCOD + "</CIAASCOD>" + "<RAMOPCOD>" + pvarNodo.RAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + pvarNodo.POLIZANN + "</POLIZANN>" + "<POLIZSEC>" + pvarNodo.POLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + pvarNodo.CERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + pvarNodo.CERTIANN + "</CERTIANN>" + "<CERTISEC>" + pvarNodo.CERTISEC + "</CERTISEC>" + "<OPERAPOL>" + pvarNodo.OPERAPOL + "</OPERAPOL>" + "<DOCUMTIP>" + pvarNodo.DOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + pvarNodo.DOCUMDAT + "</DOCUMDAT>" + "<CLIENAP1>" + pvarNodo.CLIENAP1 + "</CLIENAP1>" + "<CLIENAP2>" + pvarNodo.CLIENAP2 + "</CLIENAP2>" + "<CLIENNOM>" + pvarNodo.CLIENNOM + "</CLIENNOM>" + "<ARCHIVOS>" + pvarNodo.NOMARCH + "</ARCHIVOS>" + "<FORMUDES>" + pvarNodo.FORMUDES + "</FORMUDES>" + "<SWSUSCRI>" + pvarNodo.SWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + pvarNodo.MAIL + "</MAIL>" + "<CLAVE>" + pvarNodo.CLAVE + "</CLAVE>" + "<SW-CLAVE>" + pvarNodo.SWCLAVE + "</SW-CLAVE>" + "<SWENDOSO>" + pvarNodo.SWENDOSO + "</SWENDOSO>" + "<OPERATIP>" + pvarNodo.OPERATIP + "</OPERATIP>" + "<ESTADO>" + pvarNodo.ESTADO + "</ESTADO>" + "<CODESTAD>" + pvarNodo.CODESTAD + "</CODESTAD>";




    wvarDataIn = Strings.replace( wvarDataIn, "&", "" );
    insertScheduler = invoke( "insertBandejaSCH", new Variant[] { new Variant(wvarAplicacion), new Variant(wvarCodusu), new Variant(wvarDataIn), new Variant(pvarFECHA) } );




    return insertScheduler;
  }

  private boolean insertBandejaSCH( String pvarAplicacion, String pvarCODUSU, String pvarDataIn, String pvarFECHA ) throws Exception
  {
    boolean insertBandejaSCH = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    String wvarCodusu = "";
    String wvarAplicacion = "";
    String wvarDataIn = "";
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDBACTIONS);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsBandejaSCH );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    
    
    //
    
    
    //
    
    
    //
    
    
    //
    
    
    //
    
    
    //
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      insertBandejaSCH = true;
    }
    else
    {
      insertBandejaSCH = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return insertBandejaSCH;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEPolizaConfig));
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      invoke( "debugToBD", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion) } );
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      invoke( "debugToFile", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion) } );
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripci�n ) throws Exception
  {
    boolean debugToBD = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDBLOG);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    
    
    //
    
    
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    XmlDomExtended wobjXMLConfig = null;
    String wvarText = "";
    String wvarFileName = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion;

    wvarFileName = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend( System.getProperty("user.dir") + "\\DEBUG\\" + wvarFileName, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;


    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
