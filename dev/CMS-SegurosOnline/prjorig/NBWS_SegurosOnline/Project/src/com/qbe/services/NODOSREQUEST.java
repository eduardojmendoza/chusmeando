package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class NODOSREQUEST implements com.qbe.services.Cloneable
{
  public String CIAASCOD = "";
  public String RAMOPCOD = "";
  public String POLIZANN = "";
  public String POLIZSEC = "";
  public String CERTIPOL = "";
  public String CERTIANN = "";
  public String CERTISEC = "";
  public String OPERAPOL = "";
  public String DOCUMTIP = "";
  public String DOCUMDAT = "";
  public String CLIENAP1 = "";
  public String CLIENAP2 = "";
  public String CLIENNOM = "";
  public String NOMARCH = "";
  public String FORMUDES = "";
  public String SWSUSCRI = "";
  public String MAIL = "";
  public String CLAVE = "";
  public String SWCLAVE = "";
  public String SWENDOSO = "";
  public String OPERATIP = "";
  public String ESTADO = "";
  public String CODESTAD = "";

  public Object clone()
  {
    try 
    {
      NODOSREQUEST clone = (NODOSREQUEST) super.clone();
      //todo: make clones of any members that are objects ie. clone.obj = obj.clone();
      return clone;
    } 
    catch( CloneNotSupportedException e ) {}
    return null;
  }
}
