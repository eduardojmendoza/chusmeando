package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_setBlanqueo implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_setBlanqueo";
  static final String mcteParam_MAIL = "//MAIL";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_RESPUESTA = "//RESPUESTA";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENCLISUS = "P_NBWS_ClienteSuscripto.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  static final int mcteMsg_DATOSINVALIDOS = 2;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarMail = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarRespuesta = "";
    int wvarError = 0;
    String wvarEstadoIdentificador = "";
    String wvarEstadoPassword = "";
    boolean wvarUsrValido = false;
    boolean wvarResChk = false;
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = mcteMsg_OK;
      wvarResChk = false;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      mcteMsg_DESCRIPTION[mcteMsg_DATOSINVALIDOS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_DATOSINVALIDOS" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      wvarStep = 60;
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarStep = 70;
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      wvarStep = 80;
      wvarRespuesta = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RESPUESTA )  );
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      wvarUsrValido = invoke( "fncValidarDatosUsuario", new Variant[] { new Variant(wvarMail), new Variant(wvarDocumtip), new Variant(wvarDocumdat) } );
      if( ! (wvarUsrValido) )
      {
        wvarError = mcteMsg_DATOSINVALIDOS;
      }
      //
      wvarStep = 190;
      if( wvarError == mcteMsg_OK )
      {
        //
        wvarResChk = invoke( "fncChkRespuesta", new Variant[] { new Variant(wvarMail), new Variant(wvarRespuesta) } );
        //
        if( ! (wvarResChk) )
        {
          wvarStep = 200;
          wvarError = mcteMsg_DATOSINVALIDOS;
        }
        else
        {
          invoke( "fncDesbloquear", new Variant[] { new Variant(wvarMail) } );
        }
      }

      if( wvarError == mcteMsg_OK )
      {
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      else
      {
        wvarStep = 210;
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 500;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean fncValidarDatosUsuario( String pvarMail, String pvarDocumtip, String pvarDocumdat ) throws Exception
  {
    boolean fncValidarDatosUsuario = false;
    String mvarRequest = "";
    String mvarResponse = "";
    boolean mvarRetVal = false;
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRetVal = false;
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENCLISUS + "</DEFINICION>" + "<DOCUMTIP>" + pvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + pvarDocumdat + "</DOCUMDAT>" + "<MAIL>" + pvarMail + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        //
        if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//CORRECTO" )  ).equals( "S" ) )
        {
          mvarRetVal = true;
        }
      }
    }
    //
    fncValidarDatosUsuario = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarDatosUsuario;
  }

  private boolean fncChkRespuesta( String pvarMail, String pvarRespuesta ) throws Exception
  {
    boolean fncChkRespuesta = false;
    String mvarRequest = "";
    String mvarResponse = "";
    com.qbe.services.HSBCInterfaces.IAction mobjClass = null;
    XmlDomExtended wobjXMLResponse = null;
    String wvarRespuesta = "";
    boolean mvarRetVal = false;
    //
    //
    mvarRetVal = false;
    //
    mvarRequest = "<Request>" + "<EMAIL>" + pvarMail + "</EMAIL>" + "</Request>";
    //
    mobjClass = new Variant( new com.qbe.services.nbwsA_getMisDatos() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.nbwsA_getMisDatos().toObject());
    mobjClass.Execute( mvarRequest, mvarResponse, "" );
    mobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
    //
    wobjXMLResponse = new XmlDomExtended();
    wobjXMLResponse.loadXML( mvarResponse );
    //
    if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        wvarRespuesta = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//RESPUESTA" )  );
        if( Strings.toUpperCase( wvarRespuesta ).equals( Strings.toUpperCase( pvarRespuesta ) ) )
        {
          mvarRetVal = true;
        }
      }
    }
    //
    fncChkRespuesta = mvarRetVal;
    //
    return fncChkRespuesta;
  }

  private boolean fncDesbloquear( String pvarMail ) throws Exception
  {
    boolean fncDesbloquear = false;
    String mvarRequest = "";
    String mvarResponse = "";
    com.qbe.services.HSBCInterfaces.IAction mobjClass = null;
    XmlDomExtended wobjXMLResponse = null;
    String wvarRespuesta = "";
    boolean mvarRetVal = false;
    //
    //
    mvarRetVal = false;
    //
    mvarRequest = "<Request>" + "<MAIL>" + pvarMail + "</MAIL>" + "</Request>";
    //
    mobjClass = new Variant( new com.qbe.services.nbwsA_ccDesbloquea() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.nbwsA_ccDesbloquea().toObject());
    mobjClass.Execute( mvarRequest, mvarResponse, "" );
    mobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
    //
    wobjXMLResponse = new XmlDomExtended();
    wobjXMLResponse.loadXML( mvarResponse );
    //
    if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarRetVal = true;
      }
    }
    //
    fncDesbloquear = mvarRetVal;
    //
    return fncDesbloquear;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
