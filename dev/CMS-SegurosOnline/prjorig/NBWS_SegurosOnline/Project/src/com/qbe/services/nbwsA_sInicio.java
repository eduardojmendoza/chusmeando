package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_sInicio implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_sInicio";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_SESSION_ID = "//SESSION_ID";
  static final String wcteXSL_sInicio = "XSLs\\sInicio.xsl";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";
  /**
   * static variable for method: fncVendedoresHabilitados
   * static variable for method: fncExclusionDePolizas
   */
  private final String mcteParam_DEFINICION_SQL = "P_NBWS_ObtenerExclusiones.xml";

  /**
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXSLSalida = null;
    XmlDomExtended wobjXMLProductosAIS = null;
    XmlDomExtended wobjXMLProductosHabilitados = null;
    XmlDomExtended wobjXMLVendedoresHabilitados = null;
    XmlDomExtended wobjXMLPolizasExcluidas = null;
    XmlDomExtended wobjXMLNBWS_TempFilesServer = null;
    XmlDomExtended wobjXML_AUX = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    org.w3c.dom.Element woNodoProductoHabilitadoNBWS = null;
    org.w3c.dom.Element woNodoProductoHabilitadoNavegacion = null;
    org.w3c.dom.Element woNodoPermiteEPoliza = null;
    org.w3c.dom.Element woNodoPolizaExcluida = null;
    org.w3c.dom.Element woNodoPositiveID = null;
    org.w3c.dom.Element woNodoMasDeCincoCert = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarResponse_XML = "";
    String wvarResponse_HTML = "";
    String wvarFalseAIS = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarSESSION_ID = "";
    //
    //
    //
    //
    //Par�metros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      //
      wvarStep = 21;
      //DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SESSION_ID )  == (org.w3c.dom.Node) null) )
      {
        wvarSESSION_ID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SESSION_ID )  );
      }
      else
      {
        wvarSESSION_ID = "";
      }
      //
      //
      wvarStep = 30;
      //Arma XML de entrada al COM+ Multithreading
      //
      wvarRequest = "<Request>" + "<Request id=\"1\" actionCode=\"nbwsA_MQGenericoAIS\">" + "   <DEFINICION>LBA_1184_ProductosClientes.xml</DEFINICION>" + "   <AplicarXSL>LBA_1184_ProductosClientes.xsl</AplicarXSL>" + "   <TIPODOCU>" + wvarDocumtip + "</TIPODOCU>" + "   <NUMEDOCU>" + wvarDocumdat + "</NUMEDOCU>" + "</Request>" + "</Request>";

      wvarStep = 40;
      //Ejecuta la funci�n Multithreading
      ModGeneral.cmdp_ExecuteTrnMulti( "", "", wvarRequest, new Variant( wvarResponse )/*warning: ByRef value change will be lost.*/ );
      //
      wvarStep = 50;
      //Carga las dos respuestas en un XML.
      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse );
      //
      wvarStep = 55;
      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado" )  == (org.w3c.dom.Node) null )
      {
        //
        //Fall� Response 1
        Err.getError().setDescription( "Response 1: fall� COM+ de Request 1" );
        //unsup GoTo ErrorHandler
        //
      }
      //
      wvarStep = 56;
      //If wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/Estado") Is Nothing Then
      //
      //Fall� Response 2
      //    Err.Description = "Response 2: fall� COM+ de Request 2"
      //    GoTo ErrorHandler
      //
      //End If
      //
      //Anduvieron los dos llamados al AIS.
      //Arma un XML con los productos de las dos compa��as
      wvarResponse = "";
      //
      wvarStep = 57;
      if( ! (wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/PRODUCTOS" )  == (org.w3c.dom.Node) null) )
      {
        wvarResponse = wvarResponse + XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/PRODUCTOS" ));
      }
      else
      {
        wvarResponse = wvarResponse + "";
        wvarFalseAIS = XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado/@mensaje" )  );
      }
      //
      wvarStep = 58;
      //If Not wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/CAMPOS/PRODUCTOS") Is Nothing Then
      //    wvarResponse = wvarResponse & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/CAMPOS/PRODUCTOS")
      //Else
      //    wvarResponse = wvarResponse & ""
      //    wvarFalseAIS = wvarFalseAIS & Chr(13) & wobjXMLRespuestas.selectSingleNode("Response/Response[@id='2']/Estado/@mensaje").Text
      //End If
      //
      wvarStep = 59;
      //DA - 06/08/2009: si no existe el cliente tanto en LBA como en NYL
      if( wvarResponse.equals( "" ) )
      {
        //unsup GoTo ClienteNoExiste
      }
      //
      wvarResponse = "<Response>" + Strings.replace( Strings.replace( wvarResponse, "<PRODUCTOS>", "" ), "</PRODUCTOS>", "" ) + "</Response>";
      //
      wvarStep = 60;
      wobjXMLProductosAIS = new XmlDomExtended();
      wobjXMLProductosAIS.loadXML( wvarResponse );
      //
      wvarStep = 61;
      /*unsup wobjXMLProductosAIS.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRespuestas.selectSingleNode( "//CLIENSEC" ) .cloneNode( true ) );
      /*unsup wobjXMLProductosAIS.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRespuestas.selectSingleNode( "//TIPODOCU" ) .cloneNode( true ) );
      /*unsup wobjXMLProductosAIS.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRespuestas.selectSingleNode( "//NUMEDOCU" ) .cloneNode( true ) );
      /*unsup wobjXMLProductosAIS.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRespuestas.selectSingleNode( "//CLIENAP1" ) .cloneNode( true ) );
      /*unsup wobjXMLProductosAIS.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRespuestas.selectSingleNode( "//CLIENAP2" ) .cloneNode( true ) );
      /*unsup wobjXMLProductosAIS.selectSingleNode( "//Response" ) */.appendChild( wobjXMLRespuestas.selectSingleNode( "//CLIENNOM" ) .cloneNode( true ) );
      //
      wvarStep = 62;
      //Levanta XML de productos habilitados para NBWS.
      wobjXMLProductosHabilitados = new XmlDomExtended();
      wobjXMLProductosHabilitados.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteProductosHabilitados));
      //
      wvarStep = 70;
      //Levanta en un listado cada producto del cliente
      wobjXMLProductosAIS_List = wobjXMLProductosAIS.selectNodes( "//PRODUCTO" ) ;
      //
      wvarStep = 80;
      //Pide al SQL listado de vendedores de todos los AGENTCLA y AGENTCOD de las p�lizas del cliente.
      wobjXMLVendedoresHabilitados = new XmlDomExtended();
      wobjXMLVendedoresHabilitados.loadXML( invoke( "fncVendedoresHabilitados", new Variant[] { new Variant(wobjXMLProductosAIS_List) } ) );
      //
      wvarStep = 85;

      //DA - 31/08/2009: Pregunta al SQL si las p�lizas del cliente est�n exclu�das del NBWS (p�lizas encuadradas)
      wobjXMLPolizasExcluidas = new XmlDomExtended();
      wobjXMLPolizasExcluidas.loadXML( invoke( "fncExclusionDePolizas", new Variant[] { new Variant(wobjXMLProductosAIS_List) } ) );
      //
      wvarStep = 90;
      //Recorre p�lizas de clientes.
      for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
      {
        wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
        //
        wvarStep = 100;
        //
        woNodoProductoHabilitadoNBWS = wobjXMLProductosAIS.getDocument().createElement( "HABILITADO_NBWS" );
        woNodoProductoHabilitadoNavegacion = wobjXMLProductosAIS.getDocument().createElement( "HABILITADO_NAVEGACION" );
        woNodoPermiteEPoliza = wobjXMLProductosAIS.getDocument().createElement( "HABILITADO_EPOLIZA" );
        woNodoPolizaExcluida = wobjXMLProductosAIS.getDocument().createElement( "POLIZA_EXCLUIDA" );
        woNodoPositiveID = wobjXMLProductosAIS.getDocument().createElement( "POSITIVEID" );
        woNodoMasDeCincoCert = wobjXMLProductosAIS.getDocument().createElement( "MAS_DE_CINCO_CERT" );
        //
        //Verifica si el producto est� habilitado en NBWS (xPath al XML de Productos Habilitados)
        if( ! (null /*unsup wobjXMLProductosHabilitados.selectSingleNode( ("//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "']") ) */ == (org.w3c.dom.Node) null) )
        {
          //
          wvarStep = 105;
          //Manda el RAMOPDES de la tabla de productos habilitados
          XmlDomExtended.setText( wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPDES" ) , XmlDomExtended.getText( null /*unsup wobjXMLProductosHabilitados.selectSingleNode( "//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "']/RAMOPDES" ) */ ) );
          XmlDomExtended.setText( wobjXMLProductosAIS_Node.selectSingleNode( "TIPOPROD" ) , XmlDomExtended.getText( null /*unsup wobjXMLProductosHabilitados.selectSingleNode( "//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "']/TIPOPRODU" ) */ ) );

          wvarStep = 110;
          //Marca producto habilitado para el NBWS
          XmlDomExtended.setText( woNodoProductoHabilitadoNBWS, "S" );
          wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNBWS );

          wvarStep = 115;
          //Indica a qu� Positive ID debe redirigir en caso de corresponder
          XmlDomExtended.setText( woNodoPositiveID, XmlDomExtended.getText( null /*unsup wobjXMLProductosHabilitados.selectSingleNode( "//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "']/POSITIVEID" ) */ ) );
          wobjXMLProductosAIS_Node.appendChild( woNodoPositiveID );
          //
          wvarStep = 120;
          //DA - 31/08/2009: Verifica en SQL si la p�liza no est� exclu�da (p�lizas encuadradas)
          if( ! (null /*unsup wobjXMLPolizasExcluidas.selectSingleNode( ("//EXCLUSION[CIAASCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "CIAASCOD" ) *) ) + "' and RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "' and POLIZANN='" + Obj.toInt( XmlDomExtended .getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" ) *) ) ) + "' and POLIZSEC='" + Obj.toInt( XmlDomExtended .getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" ) *) ) ) + "']") ) */ == (org.w3c.dom.Node) null) )
          {
            //
            wvarStep = 130;
            //Marca la p�liza como exclu�da del NBWS
            XmlDomExtended.setText( woNodoPolizaExcluida, "S" );
            //
          }
          else
          {
            //
            wvarStep = 140;
            //Marca la p�liza como NO exclu�da del NBWS (es decir, inclu�da)
            XmlDomExtended.setText( woNodoPolizaExcluida, "N" );
            //
          }
          //
          wvarStep = 145;
          wobjXMLProductosAIS_Node.appendChild( woNodoPolizaExcluida );
          //
          wvarStep = 150;
          //Verifica en SQL si el producto es navegable seg�n qui�n haya vendido la p�liza
          if( ! (null /*unsup wobjXMLVendedoresHabilitados.selectSingleNode( ("//VENDEDOR[AGENTCLA='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "AGENTCLA" ) *) ) + "' and AGENTCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "AGENTCOD" ) *) ) + "']") ) */ == (org.w3c.dom.Node) null) )
          {
            //
            //Marca producto como habilitado para la navegaci�n
            wvarStep = 151;
            XmlDomExtended.setText( woNodoProductoHabilitadoNavegacion, "S" );
            wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNavegacion );
            //
            //Verifica que la p�liza en cuesti�n no tenga mas de 5 certificados
            if( wobjXMLProductosAIS.selectNodes( ("//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "' and POLIZANN = '" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" ) *) ) + "' and POLIZSEC = '" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" ) *) ) + "']") ) .getLength() > 5 )
            {
              //
              wvarStep = 152;
              //Indica que el producto tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "S" );
            }
            else
            {
              wvarStep = 153;
              //Indica que el producto NO tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "N" );
            }
            //
            wvarStep = 154;
            wobjXMLProductosAIS_Node.appendChild( woNodoMasDeCincoCert );
            //
          }
          else
          {
            //
            wvarStep = 160;
            //Marca producto NO habilitado para la navegaci�n
            XmlDomExtended.setText( woNodoProductoHabilitadoNavegacion, "N" );
            wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNavegacion );
            wvarStep = 161;
            //Verifica que la p�liza en cuesti�n no tenga mas de 5 certificados
            if( wobjXMLProductosAIS.selectNodes( ("//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "' and POLIZANN = '" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" ) *) ) + "' and POLIZSEC = '" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" ) *) ) + "']") ) .getLength() > 5 )
            {
              //
              wvarStep = 162;
              //Indica que el producto tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "S" );
            }
            else
            {
              wvarStep = 163;
              //Indica que el producto NO tiene mas de 5 certificados
              XmlDomExtended.setText( woNodoMasDeCincoCert, "N" );
            }
            //
            wvarStep = 164;
            wobjXMLProductosAIS_Node.appendChild( woNodoMasDeCincoCert );
            //
          }
          //
          //Verifica si el producto est� habilitado para ePoliza (xPath al XML de Productos Habilitados)
          if( ! (null /*unsup wobjXMLProductosHabilitados.selectSingleNode( ("//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "' and EPOLIZA = 'S']") ) */ == (org.w3c.dom.Node) null) )
          {
            wvarStep = 170;
            //Marca producto habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "S" );
            //
          }
          else
          {
            wvarStep = 180;
            //Marca producto NO habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "N" );
            //
          }
          //
          wvarStep = 181;
          wobjXMLProductosAIS_Node.appendChild( woNodoPermiteEPoliza );
          //
        }
        else
        {
          //
          wvarStep = 190;
          //Marca producto NO habilitado para el NBWS (no est� en la tabla de Productos Habilitados para NBWS)
          XmlDomExtended.setText( woNodoProductoHabilitadoNBWS, "N" );
          wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNBWS );
          //
          wvarStep = 200;
          //Marca producto NO habilitado para la navegaci�n (sin pasar por la validaci�n)
          XmlDomExtended.setText( woNodoProductoHabilitadoNavegacion, "N" );
          wobjXMLProductosAIS_Node.appendChild( woNodoProductoHabilitadoNavegacion );
          //
          wvarStep = 201;
          //Verifica si el producto est� habilitado para ePoliza (xPath al XML de Productos Habilitados)
          if( ! (null /*unsup wobjXMLProductosHabilitados.selectSingleNode( ("//PRODUCTO[RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "' and EPOLIZA = 'S']") ) */ == (org.w3c.dom.Node) null) )
          {
            wvarStep = 202;
            //Marca producto habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "S" );
            //
          }
          else
          {
            wvarStep = 203;
            //Marca producto NO habilitado para la ePoliza
            XmlDomExtended.setText( woNodoPermiteEPoliza, "N" );
            //
          }
          //
          wvarStep = 204;
          wobjXMLProductosAIS_Node.appendChild( woNodoPermiteEPoliza );
          //
          wvarStep = 205;
          //DA - 31/08/2009: Verifica en SQL si la p�liza no est� exclu�da (p�lizas encuadradas)
          if( ! (null /*unsup wobjXMLPolizasExcluidas.selectSingleNode( ("//EXCLUSION[CIAASCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "CIAASCOD" ) *) ) + "' and RAMOPCOD='" + XmlDomExtended.getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" ) *) ) + "' and POLIZANN='" + Obj.toInt( XmlDomExtended .getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" ) *) ) ) + "' and POLIZSEC='" + Obj.toInt( XmlDomExtended .getText( null (*unsup wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" ) *) ) ) + "']") ) */ == (org.w3c.dom.Node) null) )
          {
            //
            wvarStep = 206;
            //Marca la p�liza como exclu�da del NBWS
            XmlDomExtended.setText( woNodoPolizaExcluida, "S" );
            //
          }
          else
          {
            //
            wvarStep = 207;
            //Marca la p�liza como NO exclu�da del NBWS (es decir, inclu�da)
            XmlDomExtended.setText( woNodoPolizaExcluida, "N" );
            //
          }
          //
          wvarStep = 208;
          wobjXMLProductosAIS_Node.appendChild( woNodoPolizaExcluida );
          //
        }
        //
        woNodoProductoHabilitadoNBWS = (org.w3c.dom.Element) null;
        woNodoProductoHabilitadoNavegacion = (org.w3c.dom.Element) null;
        woNodoPermiteEPoliza = (org.w3c.dom.Element) null;
        woNodoPositiveID = (org.w3c.dom.Element) null;
        woNodoPolizaExcluida = (org.w3c.dom.Element) null;
        //
        wvarStep = 210;
        /*unsup wobjXMLProductosAIS_List.nextNode() */;
        //
      }
      //
      wvarStep = 220;
      //Levanta XSL para armar XML de salida
      wobjXSLSalida = new XmlDomExtended();
      wobjXSLSalida.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wcteXSL_sInicio));
      //
      wvarStep = 240;
      //Devuelve respuesta en formato XML y HTML
      wvarResponse_XML = XmlDomExtended.marshal(wobjXMLProductosAIS.getDocument().getDocumentElement());
      //
      wvarStep = 250;
      wvarResponse_HTML = wobjXMLProductosAIS.transformNode( wobjXSLSalida ).toString();
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_XML>" + wvarResponse_XML + "</Response_XML><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML></Response>" );
      //
      ClienteNoExiste: 
      wvarStep = 260;
      if( wvarResponse.equals( "" ) )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarFalseAIS + "\"></Estado><Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
      }
      //
      wvarStep = 270;
      //DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
      if( !wvarSESSION_ID.equals( "" ) )
      {
        //
        wvarStep = 271;
        //Levanta XML de configuraci�n donde se indica la ruta de grabaci�n de archivos temporales.
        wobjXMLNBWS_TempFilesServer = new XmlDomExtended();
        wobjXMLNBWS_TempFilesServer.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteNBWS_TempFilesServer));
        //
        wvarStep = 272;
        //Guarda en temporal el XML de INICIO
        if( ! (wobjXMLProductosAIS == ( XmlDomExtended ) null) )
        {
          wobjXMLProductosAIS.save( XmlDomExtended .getText( /*unsup wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ) */ ) + "\\sINICIO_" + wvarSESSION_ID + "" );
        }
        else
        {
          wvarStep = 273;
          //DA - 30/11/2009: cuando no hay p�lizas en NYL ni LBA
          wobjXML_AUX = new XmlDomExtended();
          wobjXML_AUX.loadXML( Response.toString() );
          wobjXML_AUX.save( XmlDomExtended .getText( /*unsup wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ) */ ) + "\\sINICIO_" + wvarSESSION_ID + "" );
          wobjXML_AUX = null;
          //
        }
        //
        wobjXMLNBWS_TempFilesServer = null;
        //
      }
      //
      wobjXMLRequest = null;
      wobjXMLRespuestas = null;
      wobjXMLProductosAIS = null;
      wobjXMLProductosHabilitados = null;
      wobjXMLProductosAIS_List = (org.w3c.dom.NodeList) null;
      wobjXMLProductosAIS_Node = (org.w3c.dom.Node) null;
      wobjXMLVendedoresHabilitados = null;
      wobjXSLSalida = null;
      //
      wvarStep = 280;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLRespuestas = null;
        wobjXMLProductosAIS = null;
        wobjXMLProductosHabilitados = null;
        wobjXMLProductosAIS_List = (org.w3c.dom.NodeList) null;
        wobjXMLProductosAIS_Node = (org.w3c.dom.Node) null;
        wobjXMLVendedoresHabilitados = null;
        wobjXSLSalida = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * ***************************************************************
   * *** Devuelve un XML con los vendedores habilitados
   * ***************************************************************
   */
  public String fncVendedoresHabilitados( org.w3c.dom.NodeList pobjXMLProductosAIS_List ) throws Exception
  {
    String fncVendedoresHabilitados = "";
    org.w3c.dom.Node oNodoProducto = null;
    com.qbe.services.HSBCInterfaces.IAction mobjClass = null;
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLVendedoresHabilitados = null;
    //
    //
    mvarRequest = "";
    //Arma el XML de entrada
    for( int noNodoProducto = 0; noNodoProducto < pobjXMLProductosAIS_List.getLength(); noNodoProducto++ )
    {
      oNodoProducto = pobjXMLProductosAIS_List.item( noNodoProducto );
      //
      mvarRequest = mvarRequest + "<AGENTE AGENTCLA=\"" + XmlDomExtended.getText( oNodoProducto.selectSingleNode( "AGENTCLA" )  ) + "\" AGENTCOD=\"" + XmlDomExtended.getText( oNodoProducto.selectSingleNode( "AGENTCOD" )  ) + "\"></AGENTE>";
      /*unsup pobjXMLProductosAIS_List.nextNode() */;
      //
    }
    //
    //Llama al COM+ gen�rico de SQL con un listado de AGENTCLA y AGENTCOD traidos de los productos del cliente
    mvarRequest = "<Request><DEFINICION>" + mcteParam_DEFINICION_SQL + "</DEFINICION><ROOT>" + mvarRequest + "</ROOT></Request>";
    //
    mobjClass = new Variant( new com.qbe.services.nbwsA_SQLGenerico() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.nbwsA_SQLGenerico().toObject());
    mobjClass.Execute( mvarRequest, mvarResponse, "" );
    mobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
    //
    //Levanta el resultado de la ejecuci�n para analizar la respuesta
    mobjXMLVendedoresHabilitados = new XmlDomExtended();
    mobjXMLVendedoresHabilitados.loadXML( mvarResponse );
    //
    //Verifica respuesta del COM+
    if( ! (mobjXMLVendedoresHabilitados.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
    {
      fncVendedoresHabilitados = XmlDomExtended.marshal(mobjXMLVendedoresHabilitados.selectSingleNode( "//Response" ));
    }
    else
    {
      fncVendedoresHabilitados = "<Response></Response>";
    }
    //
    mobjXMLVendedoresHabilitados = null;
    //
    return fncVendedoresHabilitados;
  }

  /**
   * ***************************************************************
   * *** En base a un listado de p�lizas, determina si alguna de ellas est� exclu�da para el NBWS
   * ***************************************************************
   */
  public String fncExclusionDePolizas( org.w3c.dom.NodeList pobjXMLProductosAIS_List ) throws Exception
  {
    String fncExclusionDePolizas = "";
    org.w3c.dom.Node oNodoProducto = null;
    com.qbe.services.HSBCInterfaces.IAction mobjClass = null;
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLPolizasExcluidas = null;
    //
    //
    mvarRequest = "";
    //Arma el XML de entrada
    for( int noNodoProducto = 0; noNodoProducto < pobjXMLProductosAIS_List.getLength(); noNodoProducto++ )
    {
      oNodoProducto = pobjXMLProductosAIS_List.item( noNodoProducto );
      //
      mvarRequest = mvarRequest + "<POLIZA CIAASCOD=\"" + XmlDomExtended.getText( oNodoProducto.selectSingleNode( "CIAASCOD" )  ) + "\" RAMOPCOD=\"" + XmlDomExtended.getText( oNodoProducto.selectSingleNode( "RAMOPCOD" )  ) + "\" POLIZANN=\"" + XmlDomExtended.getText( oNodoProducto.selectSingleNode( "POLIZANN" )  ) + "\" POLIZSEC=\"" + XmlDomExtended.getText( oNodoProducto.selectSingleNode( "POLIZSEC" )  ) + "\"></POLIZA>";
      /*unsup pobjXMLProductosAIS_List.nextNode() */;
      //
    }
    //
    //Llama al COM+ gen�rico de SQL con un listado de AGENTCLA y AGENTCOD traidos de los productos del cliente
    mvarRequest = "<Request><DEFINICION>" + mcteParam_DEFINICION_SQL + "</DEFINICION><ROOT>" + mvarRequest + "</ROOT></Request>";
    //
    mobjClass = new Variant( new com.qbe.services.nbwsA_SQLGenerico() )((com.qbe.services.HSBCInterfaces.IAction) new com.qbe.services.nbwsA_SQLGenerico().toObject());
    mobjClass.Execute( mvarRequest, mvarResponse, "" );
    mobjClass = (com.qbe.services.HSBCInterfaces.IAction) null;
    //
    //Levanta el resultado de la ejecuci�n para analizar la respuesta
    mobjXMLPolizasExcluidas = new XmlDomExtended();
    mobjXMLPolizasExcluidas.loadXML( mvarResponse );
    //
    //Verifica respuesta del COM+
    if( ! (mobjXMLPolizasExcluidas.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
    {
      fncExclusionDePolizas = XmlDomExtended.marshal(mobjXMLPolizasExcluidas.selectSingleNode( "//Response" ));
    }
    else
    {
      fncExclusionDePolizas = "<Response></Response>";
    }
    //
    mobjXMLPolizasExcluidas = null;
    //
    return fncExclusionDePolizas;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
