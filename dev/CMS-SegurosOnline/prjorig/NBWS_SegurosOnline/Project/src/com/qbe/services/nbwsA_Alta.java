package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ******************************************************************************
 * Fecha de Modificaci�n: 30/09/2011
 * PPCR: 2011-00389
 * Desarrollador: Gabriel D'Agnone
 * Descripci�n: Anexo I - Se envia suscripcion al AIS
 * ******************************************************************************
 * Fecha de Modificaci�n: 17/08/2011
 * Ticket: 648546
 * Desarrollador: Leonardo Ruiz
 * Descripci�n: Demoledores de la Burocracia: diferenciar los mails cuando son
 * por altas desde la OV o desde otro canal.
 * ******************************************************************************
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011.
 *  ALL RIGHTS RESERVED
 *  This software is only to be used for the purpose for which it has been provided.
 *  No part of it is to be reproduced, disassembled, transmitted, stored in a
 *  retrieval system or translated in any human or computer language in any way or
 *  for any other purposes whatsoever without the prior written consent of the Hong
 *  Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
 *  serious civil and criminal offence, which can result in heavy fines and payment
 *  of substantial damages.
 * 
 *  Nombre del Fuente: nbwsA_Alta.cls
 *  Fecha de Creaci�n: desconocido
 *  PPcR: desconocido
 *  Desarrollador: Desconocido
 *  Descripci�n: se agrega Copyright para cumplir con las normas de QA
 * 
 * ******************************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_Alta implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Archivo Configuracion CAI / CAP
   */
  static final String mcteEMailTemplateConfig = "NBWS_EnvioMailConfig.xml";
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_Alta";
  static final String mcteParam_MAIL = "//MAIL";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_VIAINSCRIPCION = "//VIAINSCRIPCION";
  static final String mcteParam_RESPONSABLEALTA = "//RESPONSABLEALTA";
  static final String mcteParam_CONFORMIDAD = "//CONFORMIDAD";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENREGI = "P_NBWS_Registracion.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_ERROR = 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    nbwsA_Transacciones.nbwsA_SQLGenerico wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLRespuesta1184 = null;
    XmlDomExtended wobjXMLRespuesta1185 = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarMail = "";
    String wvarCAI = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarRESPONSABLEALTA = "";
    String wvarVIAINSCRIPCION = "";
    String wvarCONFORMIDAD = "";
    String wvarError = "";
    String wvarCAIEnc = "";
    String wvarRequest = "";
    Variant wvarResponse = new Variant();
    String wvarRetVal = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarSWTIPOSUS = "";
    //
    //XML con el request
    //
    //
    //PARAMETROS LLAMADA 1185
    // Dim wvarMail            As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_ERROR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERROR" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );

      wvarStep = 25;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      wvarStep = 30;
      wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarStep = 35;
      wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );
      wvarStep = 40;
      wvarVIAINSCRIPCION = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VIAINSCRIPCION )  );
      wvarStep = 45;
      wvarRESPONSABLEALTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RESPONSABLEALTA )  );
      wvarStep = 50;
      wvarCONFORMIDAD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CONFORMIDAD )  );

      // ***********************************************************************
      // GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
      // ***********************************************************************
      wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + "<MAIL>" + wvarMail + "</MAIL>" + "</Request>";

      wvarStep = 60;

      wobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = (nbwsA_Transacciones.nbwsA_SQLGenerico) null;


      //
      wvarStep = 70;
      //Carga las dos respuestas en un XML.
      wobjXMLRespuesta1184 = new XmlDomExtended();
      wobjXMLRespuesta1184.loadXML( wvarResponse.toString() );
      //
      wvarStep = 80;
      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuesta1184.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 90;
        //SI NO PUEDO OBTENER RESPUESTA DEL 1184 (FALLO AIS O MQ)
        wvarRetVal = "ERROR";
        wvarError = String.valueOf( mcteMsg_ERROR );

        //Err.Description = "Response : fall� COM+ de Request 1184"
        //GoTo ErrorHandler
        //
      }


      //Levanta en un listado cada producto del cliente
      wvarStep = 100;
      wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes( "//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and SWSUSCRI!='S' and MAS_DE_CINCO_CERT='N']" ) ;

      wvarStep = 110;
      // Fin si no tiene p�lizas a mandar por 1185
      if( wobjXMLProductosAIS_List.getLength() > 0 )
      {
        wvarStep = 120;
        wvarRequest = "<Request id=\"1\"  actionCode=\"nbwsA_MQGenericoAIS\" >" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";
        wvarStep = 130;

        //Recorre p�lizas de clientes.
        for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
        {
          wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
          //
          wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTISEC" )  );
          // "S"
          wvarSWSUSCRI = "S";
          wvarCLAVE = "";
          wvarSWCLAVE = "";
          // "W" CUANDO ES SUSCRIPCION A SEGUROS ON LINE
          wvarSWTIPOSUS = "W";


          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS>" + "</EPOLIZA>";

        }

        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

        wvarRequest = "<Request>" + wvarRequest + "</Request>";

        wvarStep = 140;

        ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );

        wvarStep = 150;

        wobjXMLRespuesta1185 = new XmlDomExtended();
        wobjXMLRespuesta1185.loadXML( wvarResponse.toString() );


        wvarStep = 160;
        if( wobjXMLRespuesta1185.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 160;
          wvarRetVal = "ERROR";
          wvarError = String.valueOf( mcteMsg_ERROR );
          // Err.Description = "Response : fall� COM+ de Request 1185"
          // GoTo ErrorHandler
          //
        }

      }


      // ***********************************************************************
      // FIN ANEXO I
      // ***********************************************************************
      //SI DA ERROR LA CONSULTA 1184 O LA ACTUALIZACION 1185
      if( Obj.toInt( wvarError ) == mcteMsg_OK )
      {

        //
        wvarStep = 170;
        wvarCAI = Strings.toUpperCase( ModGeneral.generar_RNDSTR( new Variant( 8 )/*warning: ByRef value change will be lost.*/ ) );
        wvarStep = 180;
        wvarCAIEnc = ModEncryptDecrypt.CapicomEncrypt( wvarCAI );
        //
        wvarStep = 190;
        wvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENREGI + "</DEFINICION>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CAI><![CDATA[" + wvarCAIEnc + "]]></CAI>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + "<RESPONSABLEALTA>" + wvarRESPONSABLEALTA + "</RESPONSABLEALTA>" + "<VIAINSCRIPCION>" + wvarVIAINSCRIPCION + "</VIAINSCRIPCION>" + "<CONFORMIDAD>" + wvarCONFORMIDAD + "</CONFORMIDAD>" + "</Request>";
        //
        wvarStep = 200;
        wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
        wobjClass.Execute( wvarRequest, wvarResponse, "" );
        wobjClass = (nbwsA_Transacciones.nbwsA_SQLGenerico) null;


        //
        wvarStep = 250;
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResponse.toString() );
        //
        //Analiza resultado del SP
        wvarStep = 260;
        if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
        {
          if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
          {
            //
            wvarStep = 270;
            if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//RESULTADO" )  ).equals( "0" ) )
            {
              wvarRetVal = "OK";
              invoke( "fncEnviarMail", new Variant[] { new Variant(wvarMail), new Variant(wvarCAI), new Variant(wvarVIAINSCRIPCION) } );

            }
            else
            {
              wvarRetVal = "ERROR";
              wvarError = String.valueOf( mcteMsg_ERROR );
            }
          }
        }
        //
        //
      }

      wvarStep = 290;
      pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>" + wvarRetVal + "</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      wobjXMLRespuesta1184 = null;
      wobjXMLRespuesta1185 = null;

      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + wvarRequest, vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        wobjXMLRespuesta1184 = null;
        wobjXMLRespuesta1185 = null;

        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   * GD: 17/11/2009 -  Envio de mail via SOAPMW
   */
  private boolean fncEnviarMail( String pvarMail, String pvarCAI, String pvarVIAINSCRIPCION ) throws Exception
  {
    boolean fncEnviarMail = false;
    Object wobjClass = null;
    XmlDomExtended wvarXMLResponseMdw = null;
    XmlDomExtended wobjXMLConfig = null;
    String wvarResponseMdw = "";
    String mvarRequestMDW = "";
    String mvarTemplateCAI = "";
    String mvarMailPrueba = "";
    String mvarCopiaOculta = "";
    //
    //
    //Se obtienen los par�metros desde XML
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(mcteEMailTemplateConfig));

    //LR 17/08/2011 diferenciar los mails cuando son por altas desde la OV o desde otro canal
    if( pvarVIAINSCRIPCION.equals( "OV" ) )
    {
      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAIOV" )  );
    }
    else
    {
      mvarTemplateCAI = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//TEMPLATEMAIL/ENVIOCAI" )  );
    }

    //GED: para que no envie al mail que viene si no al que se define para pruebas, asi se evita mandar al exterior
    // correos con info no deseada accidentalmente
    if( ! (wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  == (org.w3c.dom.Node) null) )
    {
      mvarMailPrueba = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MAILPRUEBA" )  );
    }
    else
    {
      mvarMailPrueba = "";
    }

    if( ! (wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  == (org.w3c.dom.Node) null) )
    {
      mvarCopiaOculta = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//COPIAOCULTA" )  );
    }
    else
    {
      mvarCopiaOculta = "";
    }

    //Se cambia envio de mail por medio de MQ.
    mvarRequestMDW = "<Request>" + "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>" + "<Raiz>share:sendPdfReport</Raiz>" + "<files/>" + "<applicationId>NBWS</applicationId>" + "<reportId/>" + "<recipientTO>" + pvarMail + "</recipientTO>" + "<recipientsCC/>" + "<recipientsBCC>" + mvarCopiaOculta + "</recipientsBCC>" + "<templateFileName>" + mvarTemplateCAI + "</templateFileName>" + "<parametersTemplate>CLAVE=" + pvarCAI + "</parametersTemplate>" + "<from/>" + "<replyTO/>" + "<bodyText/>" + "<subject/>" + "<importance/>" + "<imagePathFile/>" + "<attachPassword/>" + "<attachFileName/>" + "</Request>";
    //
    wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(mvarRequestMDW, wvarResponseMdw, "")
    //
    wobjClass = null;
    //
    wvarXMLResponseMdw = new XmlDomExtended();
    wvarXMLResponseMdw.loadXML( wvarResponseMdw );
    //
    if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        if( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null )
        {
          fncEnviarMail = true;
        }
        else
        {
          fncEnviarMail = false;
        }
      }
      else
      {
        if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null) )
        {
          fncEnviarMail = false;
        }
        else
        {
          fncEnviarMail = false;
        }
        fncEnviarMail = false;
      }
    }
    else
    {
      fncEnviarMail = false;
    }
    //
    wobjClass = null;
    wvarXMLResponseMdw = null;
    wobjXMLConfig = null;

    return fncEnviarMail;
  }

  /**
   * GD 17-11-2009 Funci�n anterior de envio de CAI
   */
  private boolean fncEnviarMail_bak( String pvarMail, String pvarCAI ) throws Exception
  {
    boolean fncEnviarMail_bak = false;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wobjXMLParametro = null;
    XmlDomExtended wobjXMLCODOPParam = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    String wvarCODOPDesc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XMLs\\NBWSEnvioCAI.xml" );
    //
    invoke( "agregarParametro", new Variant[] { new Variant(wobjXMLParametro), new Variant("%%CAI%%"), new Variant(pvarCAI) } );
    //
    XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//TO" ) , pvarMail );
    //
    wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
    //
    wobjClass = new cam_OficinaVirtual.camA_EnviarMail();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    wobjClass = null;
    //
    wvarXMLResponse = new XmlDomExtended();
    wvarXMLResponse.loadXML( wvarResponse );
    //
    if( ! (wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        fncEnviarMail_bak = true;
      }
      else
      {
        fncEnviarMail_bak = false;
      }
    }
    else
    {
      fncEnviarMail_bak = false;
    }
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    return fncEnviarMail_bak;
  }

  private void agregarParametro( XmlDomExtended pobjXMLParametro, String pvarParam, String pvarValor ) throws Exception
  {
    org.w3c.dom.Element wobjXMLElement = null;
    org.w3c.dom.Element wobjXMLElementAux = null;
    org.w3c.dom.CDATASection wobjXMLElementCda = null;
    XmlDomExtended wobjXMLParametro = null;

    wobjXMLElement = pobjXMLParametro.getDocument().createElement( "PARAMETRO" );
    //
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_NOMBRE" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarParam );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    wobjXMLElement.appendChild( wobjXMLElementAux );
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_VALOR" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarValor );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    //
    wobjXMLElement.appendChild( wobjXMLElementAux );
    //
    /*unsup pobjXMLParametro.selectSingleNode( "//PARAMETROS" ) */.appendChild( wobjXMLElement );
    //
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
