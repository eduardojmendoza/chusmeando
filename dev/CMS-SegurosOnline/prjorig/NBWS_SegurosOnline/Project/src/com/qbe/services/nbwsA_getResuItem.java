package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getResuItem implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getResuItem";
  static final String mcteParam_NROPOLIZA = "//NROPOLIZA";
  static final String mcteParam_PERIODO = "//PERIODO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant vbLogEventTypeWarning = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    HSBC_ASP.CmdProcessor wobjCmdProcessor = new HSBC_ASP.CmdProcessor();
    String wvarExecReturn = "";
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    int wvarITEM = 0;
    int wvarUltimo = 0;
    String wvarBASE64 = "";
    String wvarNROPOLIZA = "";
    String wvarPERIODO = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarNROPOLIZA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NROPOLIZA )  );
      wvarPERIODO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PERIODO )  );
      //
      wvarStep = 30;
      wvarRequest = wvarRequest + "<Request>" + "    <DEFINICION>ismRetrieveReportList.xml</DEFINICION>" + "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" + "    <coldViewMetadata>poli=" + wvarNROPOLIZA + ";fhas=" + wvarPERIODO + "</coldViewMetadata>" + "</Request>";

      wvarRequest = ModGeneral.cmdp_FormatRequest( "lbaw_OVMWGen", "lbaw_OVMWGen.biz", wvarRequest );
      //
      wvarStep = 40;
      wobjCmdProcessor = new HSBC_ASP.CmdProcessor();
      wvarExecReturn = String.valueOf( wobjCmdProcessor.Execute( wvarRequest, wvarResponse ) );
      //
      wvarStep = 50;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarResponse );

      if( ! (wobjXMLResponse.selectSingleNode( "Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( ! (wobjXMLResponse.selectSingleNode( "Response/retrieveReportListReturn//item" )  == (org.w3c.dom.Node) null) )
        {
          //
          wvarStep = 60;
          //Recupera el �ltimo ITEM (se asume que ese es el PDF a mostrar)
          wvarUltimo = wobjXMLResponse.selectNodes( "Response/retrieveReportListReturn//item" ) .getLength();
          wvarITEM = Obj.toInt( XmlDomExtended .getText( wobjXMLResponse.selectNodes( "Response/retrieveReportListReturn//item" ) .item( wvarUltimo - 1 ) ) );
          //
        }
        else
        {
          //
          wvarStep = 70;
          //No hay item
          Err.getError().setDescription( "No se encontraron items en coldview." );
          //unsup GoTo ErrorHandler
          //
        }
      }
      else
      {
        wvarStep = 80;
        Err.getError().setDescription( "Pinch� el COM+ de recupero de ITEMS" );
        //unsup GoTo ErrorHandler
      }
      //
      wobjCmdProcessor = (HSBC_ASP.CmdProcessor) null;
      //
      wvarStep = 90;
      wvarRequest = "";
      //DA - 10/06/2011: se pasa el par�metro fhas por ticket 619330
      wvarRequest = wvarRequest + "<Request>" + "    <DEFINICION>ismRetrieveReport.xml</DEFINICION>" + "    <coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/NYLPrinting/HSBCNYLVida/SegurosComplejos/Semestrales</coldViewPubNode>" + "    <coldViewMetadata>poli=" + wvarNROPOLIZA + ";fhas=" + wvarPERIODO + "</coldViewMetadata>" + "    <item>" + wvarITEM + "</item>" + "</Request>";

      wvarRequest = ModGeneral.cmdp_FormatRequest( "lbaw_OVMWGen", "lbaw_OVMWGen.biz", wvarRequest );
      //
      wvarStep = 100;
      wobjCmdProcessor = new HSBC_ASP.CmdProcessor();
      wvarResponse = "";
      wvarExecReturn = String.valueOf( wobjCmdProcessor.Execute( wvarRequest, wvarResponse ) );
      //
      wvarStep = 110;
      wobjXMLResponse.loadXML( wvarResponse );

      wvarStep = 120;
      if( ! (wobjXMLResponse.selectSingleNode( "Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( ! (wobjXMLResponse.selectSingleNode( "//report" )  == (org.w3c.dom.Node) null) )
        {
          //
          wvarStep = 130;
          //Recupera el base 64
          wvarBASE64 = "<PDFSTREAM>" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//report" )  ) + "</PDFSTREAM>";
          //
        }
        else
        {
          //
          wvarStep = 140;
          //No hay PDF
          Err.getError().setDescription( "No se encontr� el PDF en coldview." );
          //unsup GoTo ErrorHandler
          //
        }
      }
      else
      {
        wvarStep = 150;
        Err.getError().setDescription( "Pinch� el COM+ de recupero de PDF" );
        //unsup GoTo ErrorHandler
      }
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado>" + wvarBASE64 + "</Response>" );
      //
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      //
      wvarStep = 190;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        //
        if( wvarStep == 70 )
        {
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeWarning );
        }
        else
        {
          mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        }
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
