package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ******************************************************************************
 * Fecha de Modificaci�n: 30/09/2011
 * PPCR: 2011-00389
 * Desarrollador: Gabriel D'Agnone
 * Descripci�n: Anexo I - Se envia suscripcion al AIS
 * ******************************************************************************
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011.
 *  ALL RIGHTS RESERVED
 *  This software is only to be used for the purpose for which it has been provided.
 *  No part of it is to be reproduced, disassembled, transmitted, stored in a
 *  retrieval system or translated in any human or computer language in any way or
 *  for any other purposes whatsoever without the prior written consent of the Hong
 *  Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
 *  serious civil and criminal offence, which can result in heavy fines and payment
 *  of substantial damages.
 * 
 *  Nombre del Fuente: nbwsA_Alta.cls
 *  Fecha de Creaci�n: desconocido
 *  PPcR: desconocido
 *  Desarrollador: Desconocido
 *  Descripci�n: se agrega Copyright para cumplir con las normas de QA
 * 
 * ******************************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_ccBaja implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ccBaja";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_MAIL = "//MAIL";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENBAJA = "P_NBWS_SetEstadoBaja.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_ERROR = 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    nbwsA_Transacciones.nbwsA_SQLGenerico wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLRespuesta1184 = null;
    XmlDomExtended wobjXMLRespuesta1185 = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    String wvarMail = "";
    String wvarError = "";
    String wvarRequest = "";
    Variant wvarResponse = new Variant();
    String wvarDOCUMTIP = "";
    String wvarDOCUMDAT = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarSWTIPOSUS = "";
    //
    //XML con el request
    //
    //PARAMETROS LLAMADA 1185
    // Dim wvarMail            As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_ERROR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_ERROR" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 30;
      wvarStep = 40;
      wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MAIL )  );
      wvarDOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDOCUMDAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );

      //
      // ***********************************************************************
      // GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
      // ***********************************************************************
      wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDOCUMDAT + "</DOCUMDAT>" + "<MAIL>" + wvarMail + "</MAIL>" + "</Request>";
      wvarStep = 50;

      wobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = (nbwsA_Transacciones.nbwsA_SQLGenerico) null;

      wvarStep = 60;


      //Carga las dos respuestas en un XML.
      wobjXMLRespuesta1184 = new XmlDomExtended();
      wobjXMLRespuesta1184.loadXML( wvarResponse.toString() );
      //
      wvarStep = 70;
      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuesta1184.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 80;
        //Fall� Response 1
        wvarError = String.valueOf( mcteMsg_ERROR );
        //Err.Description = "Response : fall� COM+ de Request 1184"
        //GoTo ErrorHandler
        //
      }

      wvarStep = 90;


      wvarStep = 110;
      //Levanta en un listado cada producto del cliente
      // SE DAN DE BAJA TODAS LAS POLIZAS QUE ESTAN SUSCRIPTAS A EPOLIZA Y NBWS
      wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes( "//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and MAS_DE_CINCO_CERT='N']" ) ;

      if( wobjXMLProductosAIS_List.getLength() > 0 )
      {

        wvarStep = 120;
        //Recorre p�lizas de clientes.
        wvarRequest = "<Request id=\"1\"  actionCode=\"nbwsA_MQGenericoAIS\" >" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";

        wvarStep = 130;

        for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
        {
          wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
          //
          wvarStep = 140;
          wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTISEC" )  );
          // "N" PARA LAS BAJAS
          wvarSWSUSCRI = "N";
          wvarCLAVE = "";
          wvarSWCLAVE = "";
          // ????
          wvarSWTIPOSUS = "W";


          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS>" + "</EPOLIZA>";



        }

        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

        wvarRequest = "<Request>" + wvarRequest + "</Request>";

        wvarStep = 150;

        ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );

        //
        wvarStep = 160;



        wobjXMLRespuesta1185 = new XmlDomExtended();
        wobjXMLRespuesta1185.loadXML( wvarResponse.toString() );

        wvarStep = 170;

        if( wobjXMLRespuesta1185.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 180;

          wvarError = String.valueOf( mcteMsg_ERROR );
          //Err.Description = "Response : fall� COM+ de Request 1185"
          //GoTo ErrorHandler
          //
        }
      }

      // ***********************************************************************
      // FIN ANEXO I
      // ***********************************************************************
      if( Obj.toInt( wvarError ) == mcteMsg_OK )
      {

        wvarStep = 190;
        wvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENBAJA + "</DEFINICION>" + "<MAIL>" + wvarMail + "</MAIL>" + "</Request>";
        //
        wvarStep = 200;
        wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
        wobjClass.Execute( wvarRequest, wvarResponse, "" );
        wobjClass = (nbwsA_Transacciones.nbwsA_SQLGenerico) null;
        //
      }


      wvarStep = 290;
      pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      wobjXMLRespuesta1184 = null;
      wobjXMLRespuesta1185 = null;
      //
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        wobjXMLRespuesta1184 = null;
        wobjXMLRespuesta1185 = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
