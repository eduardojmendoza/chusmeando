package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_setPassword implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_setPassword";
  static final String mcteParam_USUARIO = "//USUARIO";
  static final String mcteParam_PASSWORD = "//PASSWORD";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENUPDPAS = "P_NBWS_UpdatePassword.xml";
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarUsuario = "";
    String wvarPASSWORD = "";
    int wvarError = 0;
    Variant wvarEstadoIdentificador = new Variant();
    Variant wvarEstadoPassword = new Variant();
    String wvarEstadoUsr = "";
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = mcteMsg_OK;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarUsuario = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarStep = 60;
      wvarPASSWORD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PASSWORD )  );
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      wvarEstadoUsr = invoke( "fncValidarUsuario", new Variant[] { new Variant(wvarUsuario), new Variant(wvarEstadoIdentificador), new Variant(wvarEstadoPassword) } );
      if( wvarEstadoUsr.equals( "ERR" ) )
      {
        wvarError = mcteMsg_USRNOEXISTE;
      }
      //
      //Si el login es previo a la verificacion del RCC
      wvarStep = 190;
      if( wvarError == mcteMsg_OK )
      {
        //
        invoke( "fncSetPassword", new Variant[] { new Variant(wvarUsuario), new Variant(wvarPASSWORD) } );
        //
        wvarStep = 200;
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      else
      {
        wvarStep = 220;
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 500;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String fncValidarUsuario( String pvarUsuario, Variant pvarEstadoIdentificador, Variant pvarEstadoPassword ) throws Exception
  {
    String fncValidarUsuario = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENVALUSR + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        //
        if( ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "S" )) || ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "N" )) )
        {
          mvarRetVal = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//ESTADO" )  );
          pvarEstadoIdentificador.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOIDENTIFICADOR" )  ) );
          pvarEstadoPassword.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOPASSWORD" )  ) );
        }
      }
    }
    //
    if( mvarRetVal.equals( "" ) )
    {
      mvarRetVal = "ERR";
    }
    //
    fncValidarUsuario = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarUsuario;
  }

  private boolean fncSetPassword( String pvarUsuario, String pvarPassword ) throws Exception
  {
    boolean fncSetPassword = false;
    String mvarRequest = "";
    String mvarResponse = "";
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENUPDPAS + "</DEFINICION>" + "<PASSWORD>" + ModEncryptDecrypt.CapicomEncrypt( Strings.toUpperCase( pvarPassword ) ) + "</PASSWORD>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    return fncSetPassword;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
