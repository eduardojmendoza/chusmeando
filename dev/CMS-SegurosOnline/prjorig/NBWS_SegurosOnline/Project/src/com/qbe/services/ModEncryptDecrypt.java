package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : ModEncryptDecrypt
 *  File Name : ModEncryptDecrypt.bas
 *  Creation Date: 29/11/2005
 *  Programmer : Fernando Osores / Lucas De Merlier
 *  Abstract :    Encripta y Desencripta un string utilizando el secreto indicado.
 *  *****************************************************************
 *  Secreto. Debe ser fijo. No puede ser modificado.
 */

public class ModEncryptDecrypt
{
  public static final String mcteClave = "Rg-Hs+Fo$Lg!2009=DeadLine�cAm#HSBC";

  /**
   *  *****************************************************************
   *  Function : CapicomEncrypt
   *  Abstract : Encripta un string.
   *  Synopsis : CapicomEncrypt(ByVal strText As String) As String
   *  *****************************************************************
   */
  public static String CapicomEncrypt( String strText )
  {
    String CapicomEncrypt = "";
    Variant CAPICOM_ENCRYPTION_ALGORITHM_RC4 = new Variant();
    com.qbe.services.CAPICOM.EncryptedData objCapicom = null;
    try 
    {
      objCapicom = new com.qbe.services.CAPICOM.EncryptedData();

      if( !strText.equals( "" ) )
      {

        new Variant( objCapicom ).setValue( "Algorithm", CAPICOM_ENCRYPTION_ALGORITHM_RC4 );
        objCapicom.SetSecret( mcteClave );
        new Variant( objCapicom ).setValue( "Content", new Variant( strText ) );
        CapicomEncrypt = StrToHex( objCapicom.Encrypt().toString() );

      }
      else
      {

        CapicomEncrypt = "";

      }

      objCapicom = (com.qbe.services.CAPICOM.EncryptedData) null;

      return CapicomEncrypt;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        objCapicom = (com.qbe.services.CAPICOM.EncryptedData) null;
        CapicomEncrypt = Err.getError().getDescription() + ": " + strText;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return CapicomEncrypt;
  }

  /**
   *  *****************************************************************
   *  Function : CapicomDecrypt
   *  Abstract : Desencripta un string
   *  Synopsis : CapicomDecrypt(ByVal strText As String) As String
   *  *****************************************************************
   */
  public static String CapicomDecrypt( String strText )
  {
    String CapicomDecrypt = "";
    Variant CAPICOM_ENCRYPTION_ALGORITHM_RC4 = new Variant();
    com.qbe.services.CAPICOM.EncryptedData objCapicom = null;
    try 
    {
      objCapicom = new com.qbe.services.CAPICOM.EncryptedData();

      if( !strText.equals( "" ) )
      {

        new Variant( objCapicom ).setValue( "Algorithm", CAPICOM_ENCRYPTION_ALGORITHM_RC4 );
        objCapicom.SetSecret( mcteClave );
        objCapicom.Decrypt( HexToStr( strText ) );
        CapicomDecrypt = new Variant( objCapicom ).getValue( "Content" ).toString();

      }
      else
      {

        CapicomDecrypt = "";

      }

      objCapicom = (com.qbe.services.CAPICOM.EncryptedData) null;
      return CapicomDecrypt;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        objCapicom = (com.qbe.services.CAPICOM.EncryptedData) null;
        CapicomDecrypt = Err.getError().getDescription() + ": " + strText;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return CapicomDecrypt;
  }

  public static String HexToStr( String strHex ) throws Exception
  {
    String HexToStr = "";
    String strAux = "";
    int i = 0;


    strAux = "";

    for( i = 1; i <= Strings.len( strHex ); i += 2 )
    {
      strAux = strAux + String.valueOf( (char)(Obj.toInt( "&H" + Strings.mid( strHex, i, 2 ) )) );
    }

    HexToStr = strAux;

    return HexToStr;
  }

  public static String StrToHex( String str ) throws Exception
  {
    String StrToHex = "";
    String strAux = "";
    int i = 0;


    strAux = "";

    for( i = 1; i <= Strings.len( str ); i++ )
    {
      if( Strings.len( Integer.toHexString( Strings.asc( Strings.mid( str, i, 1 ) ) ) ) == 1 )
      {
        strAux = strAux + "0";
      }
      strAux = strAux + Integer.toHexString( Strings.asc( Strings.mid( str, i, 1 ) ) );
    }
    StrToHex = strAux;

    return StrToHex;
  }
}
