package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ******************************************************************************
 * Fecha de Modificaci�n: 30/09/2011
 * PPCR: 2011-00389
 * Desarrollador: Gabriel D'Agnone
 * Descripci�n: Anexo I - Se envia suscripcion al AIS
 * ******************************************************************************
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011.
 *  ALL RIGHTS RESERVED
 *  This software is only to be used for the purpose for which it has been provided.
 *  No part of it is to be reproduced, disassembled, transmitted, stored in a
 *  retrieval system or translated in any human or computer language in any way or
 *  for any other purposes whatsoever without the prior written consent of the Hong
 *  Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a
 *  serious civil and criminal offence, which can result in heavy fines and payment
 *  of substantial damages.
 * 
 *  Nombre del Fuente: nbwsA_Alta.cls
 *  Fecha de Creaci�n: desconocido
 *  PPcR: desconocido
 *  Desarrollador: Desconocido
 *  Descripci�n: se agrega Copyright para cumplir con las normas de QA
 * 
 * ******************************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_setMisDatos implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_setMisDatos";
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final String mcteParam_XMLSQLGENUPDDATPERS = "P_NBWS_UpdateDatosPersonales.xml";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_EMAIL_ORIGINAL = "//EMAIL_ORIGINAL";
  static final String mcteParam_EMAIL_NUEVO = "//EMAIL_NUEVO";
  static final String mcteParam_PREGUNTA = "//PREGUNTA";
  static final String mcteParam_RESPUESTA = "//RESPUESTA";
  static final String mcteParam_VALIDAR_USUARIO = "//VALIDAR_USUARIO";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_EXISTEUSR = 1;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[2];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLDoc = null;
    XmlDomExtended wobjXMLRespuesta1184 = null;
    XmlDomExtended wobjXMLRespuesta1185 = null;
    org.w3c.dom.NodeList wobjXMLProductosAIS_List = null;
    org.w3c.dom.Node wobjXMLProductosAIS_Node = null;
    int wvarStep = 0;
    String wvarRequest = "";
    Variant wvarResponse = new Variant();
    nbwsA_Transacciones.nbwsA_SQLGenerico wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    String wvarEMAIL_ORIGINAL = "";
    String wvarEMAIL_NUEVO = "";
    String wvarPREGUNTA = "";
    String wvarRespuesta = "";
    String wvarVALIDAR_USUARIO = "";
    int wvarError = 0;
    String wvarExisteUsr = "";
    String wvarDOCUMTIP = "";
    String wvarDOCUMDAT = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarSWTIPOSUS = "";
    //
    //
    //
    //PARAMETROS LLAMADA 1185
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_EXISTEUSR] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_EXISTEUSR" );
      wvarError = mcteMsg_OK;
      //
      wvarStep = 20;
      wvarEMAIL_ORIGINAL = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_ORIGINAL )  ) );
      wvarEMAIL_NUEVO = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL_NUEVO )  ) );
      wvarPREGUNTA = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_PREGUNTA )  ) );
      wvarRespuesta = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_RESPUESTA )  ) );
      wvarVALIDAR_USUARIO = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_VALIDAR_USUARIO )  ) );



      wvarDOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  );
      wvarDOCUMDAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT )  );

      //
      wvarStep = 30;
      if( wvarVALIDAR_USUARIO.equals( "S" ) )
      {
        wvarExisteUsr = invoke( "fncValidarUsuario", new Variant[] { new Variant(wvarEMAIL_NUEVO) } );
      }
      else
      {
        //Si la p�gina web NO pide validar usuario, es porque el cliente no modific� la direcci�n de email.
        //Con lo cual simulamos que el cliente no existe para que no arroje error de CLIENTE EXISTENTE
        wvarExisteUsr = "N";
      }
      //
      wvarStep = 35;
      if( wvarExisteUsr.equals( "S" ) )
      {
        wvarError = mcteMsg_EXISTEUSR;
      }
      else
      {
        wvarError = mcteMsg_OK;
      }
      //
      wvarStep = 40;
      if( wvarError == mcteMsg_OK )
      {

        wvarStep = 50;

        // ***********************************************************************
        // GED 30/09/2011 - ANEXO I: SE AGREGA ENVIO DE SUSCRIPCION AL AIS
        // ***********************************************************************
        wvarRequest = "<Request>" + "<DOCUMTIP>" + wvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDOCUMDAT + "</DOCUMDAT>" + "<MAIL>" + wvarEMAIL_ORIGINAL + "</MAIL>" + "</Request>";

        wvarStep = 60;

        wobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
        //error: function 'Execute' was not found.
        //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
        wobjClass = (nbwsA_Transacciones.nbwsA_SQLGenerico) null;


        //
        wvarStep = 70;
        //Carga las dos respuestas en un XML.
        wobjXMLRespuesta1184 = new XmlDomExtended();
        wobjXMLRespuesta1184.loadXML( wvarResponse.toString() );
        //
        wvarStep = 80;
        //Verifica que no haya pinchado el COM+
        if( wobjXMLRespuesta1184.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
        {
          //
          wvarStep = 90;
          //Fall� Response 1
          Err.getError().setDescription( "Response : fall� COM+ de Request 1184" );
          //unsup GoTo ErrorHandler
          //
        }


        //Levanta en un listado cada producto del cliente
        wvarStep = 100;
        wobjXMLProductosAIS_List = wobjXMLRespuesta1184.selectNodes( "//Response/PRODUCTO[CIAASCOD='0001' and HABILITADO_EPOLIZA='S' and SWSUSCRI!='S' and MAS_DE_CINCO_CERT='N']" ) ;
        wvarStep = 110;

        if( wobjXMLProductosAIS_List.getLength() > 0 )
        {

          wvarStep = 120;
          //Recorre p�lizas de clientes.
          wvarRequest = "<Request id=\"1\"  actionCode=\"nbwsA_MQGenericoAIS\" >" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";

          wvarStep = 130;

          for( int nwobjXMLProductosAIS_Node = 0; nwobjXMLProductosAIS_Node < wobjXMLProductosAIS_List.getLength(); nwobjXMLProductosAIS_Node++ )
          {
            wobjXMLProductosAIS_Node = wobjXMLProductosAIS_List.item( nwobjXMLProductosAIS_Node );
            //
            wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "RAMOPCOD" )  );
            wvarPOLIZANN = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "POLIZANN" )  );
            wvarPOLIZSEC = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "POLIZSEC" )  );
            wvarCERTIPOL = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTIPOL" )  );
            wvarCERTIANN = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTIANN" )  );
            wvarCERTISEC = XmlDomExtended.getText( wobjXMLProductosAIS_Node.selectSingleNode( "CERTISEC" )  );
            // "S" PARA LAS ALTAS
            wvarSWSUSCRI = "S";
            wvarCLAVE = "";
            wvarSWCLAVE = "U";
            // "W" CUANDO ES SUSCRIPCION A SEGUROS ON LINE
            wvarSWTIPOSUS = "W";


            wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarEMAIL_NUEVO + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS>" + "</EPOLIZA>";



          }

          wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

          wvarRequest = "<Request>" + wvarRequest + "</Request>";

          wvarStep = 140;
          ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );

          //
          wvarStep = 150;



          wobjXMLRespuesta1185 = new XmlDomExtended();
          wobjXMLRespuesta1185.loadXML( wvarResponse.toString() );

          if( wobjXMLRespuesta1185.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null )
          {
            //
            Err.getError().setDescription( "Response : fall� COM+ de Request 1185" );
            //unsup GoTo ErrorHandler
            //
          }

        }

        // ***********************************************************************
        // FIN ANEXO I
        // ***********************************************************************
        //
        wvarStep = 160;
        wvarRequest = "<Request><DEFINICION>" + mcteParam_XMLSQLGENUPDDATPERS + "</DEFINICION><MAIL>" + wvarEMAIL_ORIGINAL + "</MAIL><MAILNUEVO>" + wvarEMAIL_NUEVO + "</MAILNUEVO><PREGUNTA>" + ModEncryptDecrypt.CapicomEncrypt( wvarPREGUNTA ) + "</PREGUNTA><RESPUESTA>" + ModEncryptDecrypt.CapicomEncrypt( wvarRespuesta ) + "</RESPUESTA></Request>";
        //
        wvarStep = 170;
        wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
        wobjClass.Execute( wvarRequest, wvarResponse, "" );
        wobjClass = (nbwsA_Transacciones.nbwsA_SQLGenerico) null;
        //
        wvarStep = 180;
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarResponse.toString() );
        //
        if( ! (wobjXMLResponse.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null) )
        {
          wvarStep = 190;


          Response.set( "<Response><Estado resultado=\"true\" mensaje=\"" + mcteMsg_DESCRIPTION[wvarError] + "\"></Estado></Response>" );
        }
        else
        {
          //            Err.Description = "Pinch� el COM+ de SQL Generico: setMisDatos"
          //            GoTo ErrorHandler
          wvarStep = 200;
          Response.set( "<Response><Estado resultado=\"false\" mensaje=\"No es posible continuar con la operaci&oacute;n solicitada. Por favor intente m&aacute;s tarde. Muchas gracias.\"></Estado></Response>" );
        }
        //
      }
      else
      {
        wvarStep = 210;
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteMsg_DESCRIPTION[wvarError] + "\"></Estado></Response>" );
      }
      //
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      wobjXMLRespuesta1184 = null;
      wobjXMLRespuesta1185 = null;
      //
      wvarStep = 220;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + mcteMsg_DESCRIPTION[wvarError] + "\"></Estado></Response>" );

        wobjXMLRequest = null;
        wobjXMLResponse = null;
        wobjXMLRespuesta1184 = null;
        wobjXMLRespuesta1185 = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + Request, vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String fncValidarUsuario( String pvarUsuario ) throws Exception
  {
    String fncValidarUsuario = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENVALUSR + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        //
        if( ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "S" )) || ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "N" )) )
        {
          //
          //DA - 09/10/2009: puede que el usuario exista pero est� dado de baja.
          if( !XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//ESTADO" )  ).equals( "A" ) )
          {
            mvarRetVal = "N";
          }
          else
          {
            mvarRetVal = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  );
          }
          //
        }
      }
    }
    //
    if( mvarRetVal.equals( "" ) )
    {
      mvarRetVal = "ERR";
    }
    //
    fncValidarUsuario = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarUsuario;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
