package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_sCobranzas implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_sCobranzas";
  /**
   * 
   */
  static final String mcteLBA_SeguimientoCobranzas = "LBA_1430_SeguimientoCobranzas.xml";
  static final String mcteNYL_SeguimientoCobranzas = "NYL_1430_SeguimientoCobranzas.xml";
  static final String wcteXSL_sCobranzas = "XSLs\\sCobranzas.xsl";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXSLSalida = null;
    org.w3c.dom.NodeList wobjXML_PRODUCTOS_List = null;
    org.w3c.dom.Node wobjXML_PRODUCTOS_Node = null;
    org.w3c.dom.Element wobjElemento = null;
    org.w3c.dom.Element oNodeFechaActual = null;
    int wvarCount = 0;
    int wvarStep = 0;
    String wvarRequest = "";
    Variant wvarResponse = new Variant();
    String wvarResponse_HTML = "";
    String wvarResponse_XML = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanta las p�lizas a recorrer
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      //Solo p�lizas est�n habilitadas para NBWS y si est�n habilitadas para ser navegadas en el sitio
      wobjXML_PRODUCTOS_List = wobjXMLRequest.selectNodes( "Request/PRODUCTOS/PRODUCTO[./HABILITADO_NBWS = 'S' and ./HABILITADO_NAVEGACION = 'S'  and ./POLIZA_EXCLUIDA = 'N']" ) ;
      wvarRequest = "";
      wvarCount = 1;
      //Recorre cada p�liza
      for( int nwobjXML_PRODUCTOS_Node = 0; nwobjXML_PRODUCTOS_Node < wobjXML_PRODUCTOS_List.getLength(); nwobjXML_PRODUCTOS_Node++ )
      {
        wobjXML_PRODUCTOS_Node = wobjXML_PRODUCTOS_List.item( nwobjXML_PRODUCTOS_Node );
        //
        wvarStep = 30;
        //Se fija de qu� compa��a es la p�liza
        if( XmlDomExtended .getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CIAASCOD" )  ).equals( "0020" ) )
        {
          //
          wvarStep = 40;
          //P�liza de NYL: Arma XML de entrada a la funci�n Multithreading (muchos request)
          wvarRequest = wvarRequest + "<Request id=\"" + wvarCount + "\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0020\">" + "   <DEFINICION>" + mcteNYL_SeguimientoCobranzas + "</DEFINICION>" + "   <RAMOPCOD>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "RAMOPCOD" )  ) + "</RAMOPCOD>" + "   <POLIZANN>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZANN" )  ) + "</POLIZANN>" + "   <POLIZSEC>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZSEC" )  ) + "</POLIZSEC>" + "   <CERTIPOL>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIPOL" )  ) + "</CERTIPOL>" + "   <CERTIANN>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIANN" )  ) + "</CERTIANN>" + "   <CERTISEC>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTISEC" )  ) + "</CERTISEC>" + "   <SUPLENUM>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "SUPLENUM" )  ) + "</SUPLENUM>" + "</Request>";
        }
        else
        {
          //
          wvarStep = 50;
          //P�liza de LBA: Arma XML de entrada a la funci�n Multithreading (muchos request)
          wvarRequest = wvarRequest + "<Request id=\"" + wvarCount + "\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "   <DEFINICION>" + mcteLBA_SeguimientoCobranzas + "</DEFINICION>" + "   <RAMOPCOD>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "RAMOPCOD" )  ) + "</RAMOPCOD>" + "   <POLIZANN>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZANN" )  ) + "</POLIZANN>" + "   <POLIZSEC>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "POLIZSEC" )  ) + "</POLIZSEC>" + "   <CERTIPOL>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIPOL" )  ) + "</CERTIPOL>" + "   <CERTIANN>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTIANN" )  ) + "</CERTIANN>" + "   <CERTISEC>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "CERTISEC" )  ) + "</CERTISEC>" + "   <SUPLENUM>" + XmlDomExtended.getText( wobjXML_PRODUCTOS_Node.selectSingleNode( "SUPLENUM" )  ) + "</SUPLENUM>" + "</Request>";
        }
        wvarCount = wvarCount + 1;
        /*unsup wobjXML_PRODUCTOS_List.nextNode() */;
        //
      }
      //
      wvarStep = 60;
      //Ejecuta la funci�n Multithreading
      wvarRequest = "<Request>" + wvarRequest + "</Request>";
      ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, wvarResponse );
      //
      wvarStep = 70;
      //Carga las respuestas
      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse.toString() );
      //
      wvarStep = 80;
      //Verifica si pinch� el COM+
      if( wobjXMLRespuestas.selectSingleNode( "//Response" )  == (org.w3c.dom.Node) null )
      {
        //
        wvarStep = 90;
        //Pinch� el COM+
        Err.getError().setDescription( "Fall� la ejecuci�n de la funci�n Multithreading." );
        //unsup GoTo ErrorHandler
      }
      else
      {
        //
        //Analiza las respuestas en base al request
        /*unsup wobjXML_PRODUCTOS_List.reset() */;
        wvarCount = 1;
        wvarResponse_HTML = "";
        for( int nwobjXML_PRODUCTOS_Node = 0; nwobjXML_PRODUCTOS_Node < wobjXML_PRODUCTOS_List.getLength(); nwobjXML_PRODUCTOS_Node++ )
        {
          wobjXML_PRODUCTOS_Node = wobjXML_PRODUCTOS_List.item( nwobjXML_PRODUCTOS_Node );
          //
          wvarStep = 100;
          if( wobjXMLRespuestas.selectSingleNode( ("Response/Response[@id='" + wvarCount + "']") )  == (org.w3c.dom.Node) null )
          {
            //
            wvarStep = 110;
            //Pinch� el COM+ para ese PRODUCTO.
            //En este caso el XSL no va a tener el response, con lo cual no dibujar� ninguna impresorita.
            //
          }
          else
          {
            //
            wvarStep = 120;
            wobjElemento = (org.w3c.dom.Element) wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='" + wvarCount + "']" ) .cloneNode( true );
            wobjXML_PRODUCTOS_Node.appendChild( wobjElemento );
            //
          }
          //
          //Al nodo original le agrega la respuesta
          wvarStep = 130;
          wvarResponse_XML = wvarResponse_XML + wobjXML_PRODUCTOS_Node.toString();
          wvarCount = wvarCount + 1;
          /*unsup wobjXML_PRODUCTOS_List.nextNode() */;
        }
        //
      }
      //
      wvarStep = 140;
      //Levanta XSL para armar XML de salida
      wobjXSLSalida = new XmlDomExtended();
      wobjXSLSalida.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wcteXSL_sCobranzas));
      //
      wvarStep = 150;
      //Devuelve respuesta en formato HTML
      wobjXMLRespuestas.loadXML( "<PRODUCTOS>" + wvarResponse_XML + "</PRODUCTOS>" );
      //
      wvarStep = 155;
      //DA - 04/09/2009: manda la fecha actual para marcar vencidos en rojo
      oNodeFechaActual = wobjXMLRespuestas.getDocument().createElement( "FECHA_ACTUAL" );
      XmlDomExtended.setText( oNodeFechaActual, DateTime.year( DateTime.now() ) + Strings.right( "00" + DateTime.month( DateTime.now() ), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) );
      /*unsup wobjXMLRespuestas.selectSingleNode( "//PRODUCTOS" ) */.appendChild( oNodeFechaActual );

      wvarStep = 160;
      wvarResponse_HTML = wobjXMLRespuestas.transformNode( wobjXSLSalida ).toString();
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML><Response_XML>" + wobjXMLRespuestas.getDocument().getDocumentElement().toString() + "</Response_XML></Response>" );
      //
      wobjXMLRequest = null;
      wobjXMLRespuestas = null;
      wobjXML_PRODUCTOS_List = (org.w3c.dom.NodeList) null;
      wobjXML_PRODUCTOS_Node = (org.w3c.dom.Node) null;
      wobjElemento = (org.w3c.dom.Element) null;
      oNodeFechaActual = (org.w3c.dom.Element) null;
      wobjXSLSalida = null;
      //
      wvarStep = 170;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLRespuestas = null;
        wobjXML_PRODUCTOS_List = (org.w3c.dom.NodeList) null;
        wobjXML_PRODUCTOS_Node = (org.w3c.dom.Node) null;
        wobjElemento = (org.w3c.dom.Element) null;
        oNodeFechaActual = (org.w3c.dom.Element) null;
        wobjXSLSalida = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
