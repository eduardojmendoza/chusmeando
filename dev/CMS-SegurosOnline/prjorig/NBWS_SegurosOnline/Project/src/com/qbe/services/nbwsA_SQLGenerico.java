package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public class nbwsA_SQLGenerico implements VBObjectClass
{
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_SQLGenerico";
  static final String mcteSubDirName = "DefinicionesSQL";
  static final String mcteLogPath = "LogSQL";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  private String mvarConsultaRealizada = "";
  private String mvarStrCmdExec = "";
  /**
   * XML con constantes de ADO para tipos de dato
   */
  private XmlDomExtended wobjCnstADO = null;
  /**
   * static variable for method: IAction_Execute
   * static variable for method: generarCommand
   * static variable for method: generarXMLSalida
   * static variable for method: etiquetar
   */
  private final String wcteFnName = "etiquetar";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLOldLog = null;
    XmlDomExtended wobjXMLLog = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLDefinition = null;
    XmlDomExtended wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLRecordsets = null;
    org.w3c.dom.Node wobjNodoRec = null;
    String wvarDefinitionFile = "";
    String wvarStoredProcedure = "";
    String wvarTipoSP = "";
    String wvarUDL = "";
    String wobjSalida = "";
    HSBC.DBConnection com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wobjRecordset = null;
    String wvarPathLog = "";
    java.util.Date wvarFechaConsulta = DateTime.EmptyDate;
    int wvarCantLlamadasLogueadas = 0;
    int wvarCantLlamadasALoguear = 0;
    int wvarCounter = 0;

    //XML de definici�n
    //XML con el request
    //Nodos descriptores de recordsets
    //Nodo descriptor de un recordset
    //nombre del archivo XML de definici�n
    //nombre del stored procedure
    //tipo de store procedure. C (consulta) o ABM
    //nombre del archivo UDL
    //String de salida
    //interface de conexi�n a la base de datos
    //conexi�n a la base de datos
    //objeto recordset
    //Para LOGs
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarFechaConsulta = DateTime.now();
      //
      //carga del REQUEST
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );

      //Carga del XML de definici�n
      wvarStep = 20;
      wobjXMLDefinition = new XmlDomExtended();
      wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" )  );
      mvarConsultaRealizada = Strings.left( wvarDefinitionFile, Strings.len( wvarDefinitionFile ) - 4 );


      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );

      //Carga del nombre del SP
      wvarStep = 30;
      wvarStoredProcedure = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/NOMBRE" )  );

      //Carga del tipo de SP
      wvarStep = 40;
      wvarTipoSP = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/TIPO" )  );

      //Carga del UDL
      wvarStep = 50;
      wvarUDL = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/UDL" )  );

      //selecci�n del tipo de interface de conexi�n dependiendo de si el SP hace altas, modificaciones o bajas o no
      wvarStep = 60;
      if( wvarTipoSP.equals( "ABM" ) )
      {
        com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      }
      else
      {
        com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      }

      //conexi�n a la base de datos
      wvarStep = 70;
      wobjDBCnn = (Connection) (Connection)( wobjHSBC_DBCnn.GetDBConnection( wvarUDL ) );


      //'''''''''''''''''''''''''''
      //armado de la llamada al SP'
      //'''''''''''''''''''''''''''
      wvarStep = 80;
      wobjDBCmd = new Command();
      wobjDBCmd = invoke( "generarCommand", new Variant[] { new Variant(wobjDBCnn), new Variant(wvarStoredProcedure), new Variant(wobjXMLDefinition.selectNodes( new Variant("//ENTRADA/PARAMETRO") ) */), new Variant(null /*unsup wobjXMLRequest.selectSingleNode( new Variant("Request") ) ) } );


      //'''''''''''''''''
      //ejecuci�n del SP'
      //'''''''''''''''''
      wvarStep = 90;
      wobjRecordset = wobjDBCmd.execute();



      //''''''''''''''''''''
      //armado de la salida'
      //''''''''''''''''''''
      //Carga del nodos descriptores de recordsets
      wvarStep = 100;
      wobjXMLRecordsets = wobjXMLDefinition.selectNodes( "//SALIDA/RECORDSET" ) ;

      wvarStep = 110;
      wobjSalida = "";

      //por cada recordset definido en el XML de definici�n procesa un recordset devuelto por la consulta
      for( int nwobjNodoRec = 0; nwobjNodoRec < wobjXMLRecordsets.getLength(); nwobjNodoRec++ )
      {
        wobjNodoRec = wobjXMLRecordsets.item( nwobjNodoRec );
        //si hay menos recordsets que los indicados en la definici�n tira un error
        if( wobjRecordset == (Recordset) null )
        {
          Err.raise( 2504, "", "ERROR EN XML DE DEFINICION - se definieron m�s recordsets que la cantidad devuelta por el stored procedure" );
        }
        wobjSalida = wobjSalida + invoke( "generarXMLSalida", new Variant[] { new Variant(wobjRecordset)/*warning: ByRef value change will be lost.*/, new Variant(wobjNodoRec) } );
        wobjRecordset = wobjRecordset.nextRecordset();
      }

      wvarStep = 120;
      if( ! (wobjXMLDefinition.selectSingleNode( "//RETORNARREQUEST" )  == (org.w3c.dom.Node) null) )
      {
        wobjSalida = wobjSalida + Request;
      }

      wvarStep = 130;
      Response.set( invoke( "etiquetar", new Variant[] { new Variant("Response"), new Variant(wobjSalida) } ) );

      // ****************************************************************
      //Generamos un Log en caso que se solicite
      // ****************************************************************
      //
      if( ! ((wobjXMLRequest.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null)) || ! ((wobjXMLDefinition.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null)) )
      {
        //
        wvarStep = 135;
        wvarCantLlamadasALoguear = 1;
        if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) == (org.w3c.dom.Node) null) )
        {
          wvarCantLlamadasALoguear = Obj.toInt( XmlDomExtended .getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "CantLlamadasALoguear" ) ) );
        }
        //
        wvarStep = 140;
        wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + wvarDefinitionFile;
        //
        wvarStep = 150;
        if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" )  == (org.w3c.dom.Node) null) )
        {
          if( ! (wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
          {
            wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
          }
        }
        else
        {
          if( ! (wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
          {
            wvarPathLog = System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
          }
        }
        //
        wvarStep = 160;
        if( ! (wvarPathLog.matches( "*\\" )) )
        {

          //Genero el Log solo si en el nodo se especifico un nombre de archivo
          wobjXMLLog = new XmlDomExtended();
          //
          wvarStep = 170;
          wobjXMLLog.loadXML( "<LOGs><LOG StoredProcedure='" + wvarStoredProcedure + "' InicioConsulta='" + DateTime.format( wvarFechaConsulta, "dd/MM/yyyy HH:mm:ss" ) + "' TiempoIncurrido = '" + (int)Math.rint( (DateTime.diff( wvarFechaConsulta, DateTime.now() ) * 86400) ) + " Seg' >" + Request + Response + "</LOG></LOGs>" );
          //
          wvarStep = 180;
          //
          if( !"" /*unsup this.Dir( wvarPathLog, 0 ) */.equals( "" ) )
          {
            wvarStep = 190;
            //Ya existe el Log, entonces agrego la nueva consulta al LOG
            wobjXMLOldLog = new XmlDomExtended();
            wobjXMLOldLog.load( wvarPathLog );
            wvarStep = 200;
            if( wobjXMLOldLog.selectSingleNode( "//LOGs" )  == (org.w3c.dom.Node) null )
            {
              // *********LOG NO VALIDO***********
              wobjXMLLog.save( wvarPathLog );
            }
            else
            {
              //''''                    wvarStep = 210
              //''''                    wobjXMLOldLog.selectSingleNode("//LOGs").appendChild wobjXMLLog.selectSingleNode("//LOGs").childNodes(0)
              //''''                    wvarStep = 220
              //''''                    wobjXMLOldLog.Save wvarPathLog
              //
              //DA
              wvarStep = 210;
              wvarCantLlamadasLogueadas = wobjXMLOldLog.selectSingleNode( "//LOGs" ) .getChildNodes().getLength();
              for( wvarCounter = 1; wvarCounter <= ((wvarCantLlamadasLogueadas - wvarCantLlamadasALoguear) + 1); wvarCounter++ )
              {
                /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.removeChild( wobjXMLOldLog.selectSingleNode( "//LOGs" ) .getChildNodes().item( 0 ) );
              }
              //
              //DA
              wvarStep = 220;
              /*unsup wobjXMLOldLog.selectSingleNode( "//LOGs" ) */.appendChild( wobjXMLLog.selectSingleNode( "//LOGs" ) .getChildNodes().item( 0 ) );
              //DA
              wvarStep = 225;
              wobjXMLOldLog.save( wvarPathLog );
              //
            }
          }
          else
          {
            wvarStep = 230;
            wobjXMLLog.save( wvarPathLog );
          }
        }
      }

      // ****************************************************************
      //FIN generaci�n de LOG
      // ****************************************************************
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjRecordset = (Recordset) null;
        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = (HSBC.DBConnection) null;
        wobjXMLRequest = null;

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private Command generarCommand( Connection pobjDBCnn, String pobjStoredProcedure, org.w3c.dom.NodeList pobjNodos, org.w3c.dom.Node pobjXMLRequest )
  {
    Command generarCommand = null;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    Command wobjCommand = null;
    Parameter wobjParameter = null;
    org.w3c.dom.Node wobjNodeCampo = null;
    String wvarNombrePrm = "";
    int wvarTipoPrm = 0;
    String wvarValuePrm = "";

    //Command a crear
    //parametro
    // Nodo con campo de entrada
    //Nombre de un par�metro
    //Tipo de un par�metro
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;

      wobjCommand = new Command();
      wobjCommand.setActiveConnection( pobjDBCnn );

      wobjCommand.setCommandText( pobjStoredProcedure );
      wobjCommand.setCommandType( AdoConst.adCmdStoredProc );

      //Se almacena la cadena de ejecucion
      mvarStrCmdExec = pobjStoredProcedure + Strings.space( 1 );

      //por cada campo de entrada se crea un par�metro y se agrega al Command
      for( int nwobjNodeCampo = 0; nwobjNodeCampo < pobjNodos.getLength(); nwobjNodeCampo++ )
      {
        wobjNodeCampo = pobjNodos.item( nwobjNodeCampo );

        wvarStep = 20;
        wvarNombrePrm = XmlDomExtended.getText( wobjNodeCampo.getAttributes().getNamedItem( "Nombre" ) );

        wvarStep = 30;
        wvarTipoPrm = Obj.toInt( XmlDomExtended .getText( wobjCnstADO.selectSingleNode( "//DATATYPE[@name=\"" + XmlDomExtended.getText( wobjNodeCampo.getAttributes().getNamedItem( "Type" ) ) + "\"]" )  ) );
        wobjParameter = new Parameter( "", AdoConst.adInteger, AdoConst.adParamInput, 0, null );
        wobjParameter.setName( "@" + wvarNombrePrm );
        wobjParameter.setType( wvarTipoPrm );
        wobjParameter.setDirection( AdoConst.adParamInput );

        wvarStep = 40;

        if( ! (wobjNodeCampo.getAttributes().getNamedItem( "Xml" ) == (org.w3c.dom.Node) null) )
        {
          wvarValuePrm = pobjXMLRequest.selectSingleNode( wvarNombrePrm ) .toString();
        }
        else
        {
          if( pobjXMLRequest.selectSingleNode( wvarNombrePrm )  == (org.w3c.dom.Node) null )
          {
            wvarValuePrm = XmlDomExtended.getText( wobjNodeCampo.getAttributes().getNamedItem( "Default" ) );
          }
          else
          {
            wvarValuePrm = XmlDomExtended.getText( pobjXMLRequest.selectSingleNode( wvarNombrePrm )  );
          }
        }

        //Actual manera de agregar par�metros
        
        if( wvarTipoPrm == 129 || wvarTipoPrm == 200 || wvarTipoPrm == 201 )
        {
          //adodb.adVarChar adodb.adChar adLongVarChar(text)
          mvarStrCmdExec = mvarStrCmdExec + "'" + wvarValuePrm + "',";
        }
        else
        {
          mvarStrCmdExec = mvarStrCmdExec + wvarValuePrm + ",";
        }

        //los tipos de dato num�rico y decimal (131 y 14) requieren determinar
        //Precision y NumericScale. Los demas requieres determinar Size
        if( (wvarTipoPrm == this.getValue( "ADODB.adNumeric" ).toInt()) || (wvarTipoPrm == this.getValue( "ADODB.adDecimal" ).toInt()) )
        {
          wvarStep = 50;
          wobjParameter.setPrecision( (byte)Obj.toInt( XmlDomExtended .getText( wobjNodeCampo.getAttributes().getNamedItem( "Precision" ) ) ) );

          wvarStep = 60;
          wobjParameter.setScale( (byte)Obj.toInt( XmlDomExtended .getText( wobjNodeCampo.getAttributes().getNamedItem( "Scale" ) ) ) );

        }
        else
        {

          //Tipo text
          if( wvarTipoPrm == this.getValue( "ADODB.adLongVarChar" ).toInt() )
          {
            wvarStep = 70;
            wobjParameter.setSize( Strings.len( wvarValuePrm ) );
          }
          else
          {
            wvarStep = 75;
            wobjParameter.setSize( Obj.toInt( XmlDomExtended .getText( wobjNodeCampo.getAttributes().getNamedItem( "Size" ) ) ) );
          }

        }

        //Tipo text
        if( wvarTipoPrm == this.getValue( "ADODB.adLongVarChar" ).toInt() )
        {
          wvarStep = 80;
          wobjParameter.appendChunk( new Variant( wvarValuePrm ) );
        }
        else
        {
          wvarStep = 85;
          wobjParameter.setValue( new Variant( wvarValuePrm ) );
        }




        wvarStep = 90;
        wobjCommand.getParameters().append( wobjParameter );
      }


      //Se elimina el ultimo caracter(coma) en la cadena de ejecucion
      wvarStep = 100;
      mvarStrCmdExec = Strings.left( mvarStrCmdExec, Strings.len( mvarStrCmdExec ) - 1 );

      generarCommand = wobjCommand;

      return generarCommand;

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Parametro: " + wvarNombrePrm, vbLogEventTypeError );

        wobjCommand = (Command) null;

        generarCommand = (Command) null;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return generarCommand;
  }

  private String generarXMLSalida( Recordset pobjRecordset, org.w3c.dom.Node pobjNodo )
  {
    String generarXMLSalida = "";
    Variant vbLogEventTypeError = new Variant();
    int wvari = 0;
    int wvarStep = 0;
    org.w3c.dom.NodeList wobjNodosCampos = null;
    org.w3c.dom.Node wobjNodoCampo = null;
    String wobjNomRecordset = "";
    String wobjNomFila = "";
    String wobjNomCampo = "";
    Field wobjCampo = null;
    String wobjAux = "";
    String wobjAuxRegs = "";
    int wvar = 0;


    //Etiqueta de salida para un recordset
    //Etiqueta de salida para una fila
    //Etiqueta de salida para un campo
    //campo de un recordset
    //almacena temporalmente la salida
    //almacena temporalmente la salida // REGISTROS
    //contador
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wobjAux = "";

      wobjNomRecordset = XmlDomExtended.getText( pobjNodo.getAttributes().getNamedItem( "Nombre" ) );
      wobjNomFila = XmlDomExtended.getText( pobjNodo.getAttributes().getNamedItem( "NombreFila" ) );

      wobjNodosCampos = pobjNodo.selectNodes( "//RECORDSET[@Nombre=\"" + wobjNomRecordset + "\"]/CAMPO" ) ;

      wvarStep = 10;
      //procesa cada registro del recordset
      while( ! (pobjRecordset.isEOF()) )
      {
        //si no coincide la cantidad de campos del recordset con la cantidad de campos definidas en el XML tira error
        if( wobjNodosCampos.getLength() != pobjRecordset.getFields().getCount() )
        {
          Err.raise( 2504, "", "ERROR EN XML DE DEFINICION - No coincide la cantidad de campos devueltos por el stored procedure (" + pobjRecordset.getFields().getCount() + " campos) con la cantidad de campos definidos en el XML (" + wobjNodosCampos.getLength() + " campos)" );
        }

        wvarStep = 20;
        wvari = 0;
        //procesa cada campo del registro
        for( int nwobjCampo = 0; nwobjCampo < pobjRecordset.getFields().getCount(); nwobjCampo++ )
        {
          wobjCampo = pobjRecordset.getFields().getField(nwobjCampo);
          wobjNodoCampo = wobjNodosCampos.item( wvari );
          //si debe mostrarse el campo, muestra su valor con las etiquetas indicadas
          if( wobjNodoCampo.getAttributes().getNamedItem( "OCULTO" ) == (org.w3c.dom.Node) null )
          {
            if( wobjNodoCampo.getAttributes().getNamedItem( "CDATA" ) == (org.w3c.dom.Node) null )
            {
              wobjAux = wobjAux + invoke( "etiquetar", new Variant[] { new Variant( XmlDomExtended .getText( wobjNodoCampo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant(wobjCampo.getValue().toString()) } );
            }
            else
            {
              wobjAux = wobjAux + invoke( "etiquetar", new Variant[] { new Variant( XmlDomExtended .getText( wobjNodoCampo.getAttributes().getNamedItem( new Variant("Nombre") ) )), new Variant("<![CDATA[" + wobjCampo.getValue() + "]]>") } );
            }
          }
          wvari = wvari + 1;
        }

        wvarStep = 30;
        wobjAuxRegs = wobjAuxRegs + invoke( "etiquetar", new Variant[] { new Variant(wobjNomFila), new Variant(wobjAux) } );
        wobjAux = "";
        pobjRecordset.moveNext();
      }

      wvarStep = 40;

      generarXMLSalida = "<Estado resultado=\"true\"/>" + invoke( "etiquetar", new Variant[] { new Variant(wobjNomRecordset), new Variant(wobjAuxRegs) } );

      return generarXMLSalida;

      //~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //warning: modifying ByRef argument 'pobjRecordset'. It must be a Variant.
        pobjRecordset = (Recordset) null;
        wobjCampo = (Field) null;
        generarXMLSalida = "<Response><Estado resultado='false'></Response>";
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return generarXMLSalida;
  }

  private String etiquetar( String pobjEtiqueta, String pvarContenido )
  {
    String etiquetar = "";
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      etiquetar = "<" + pobjEtiqueta + ">" + pvarContenido + "</" + pobjEtiqueta + ">";

      return etiquetar;

      //~~~~~~~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mvarConsultaRealizada, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        etiquetar = "1";
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return etiquetar;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    wobjCnstADO = new XmlDomExtended();
    wobjCnstADO.load( System.getProperty("user.dir") + "\\constantesADO.xml" );
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
