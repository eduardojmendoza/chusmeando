package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getPassword implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getPassword";
  static final String mcteParam_EMAIL = "//EMAIL";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLDoc = null;
    org.w3c.dom.Element oNodo = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    String wvarEMAIL = "";
    String wvarPASSWORD = "";
    String wvarEstadoPassword = "";
    String wvarDescripcion = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarEMAIL = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL )  ) );
      //
      wvarStep = 30;
      wvarRequest = "<Request><DEFINICION>P_NBWS_ObtenerPassword.xml</DEFINICION><MAIL>" + wvarEMAIL + "</MAIL></Request>";
      //
      wvarStep = 40;
      wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = null;
      //
      wvarStep = 50;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarResponse );
      //
      if( ! (wobjXMLResponse.selectSingleNode( "//PASSWORD" )  == (org.w3c.dom.Node) null) )
      {
        wvarStep = 60;
        wvarPASSWORD = ModEncryptDecrypt.CapicomDecrypt( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//PASSWORD" )  ) );
        wvarEstadoPassword = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//ESTADOPASSWORD" )  );
        wvarDescripcion = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//DESCRIPCION" )  );
      }
      else
      {
        wvarStep = 70;
        Err.getError().setDescription( "No se encontraron datos en el SQL o pinch� el COM+ de SQL Generico" );
        //unsup GoTo ErrorHandler
      }
      //
      wvarStep = 80;
      //
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado>" + "<PASSWORD>" + wvarPASSWORD + "</PASSWORD><ESTADOPASSWORD>" + wvarEstadoPassword + "</ESTADOPASSWORD><DESCRIPCION>" + wvarDescripcion + "</DESCRIPCION></Response>" );
      //
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      //
      wvarStep = 90;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
