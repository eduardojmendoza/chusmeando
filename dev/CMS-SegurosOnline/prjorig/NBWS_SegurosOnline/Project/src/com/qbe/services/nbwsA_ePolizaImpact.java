package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * ENTRADA
 * Listado de p�lizas
 * RAMOPCOD
 * POLIZANN
 * POLIZSEC
 * CERTIPOL
 * CERTIANN
 * CERTISEC
 * SUPLENUM
 * email
 * pwd
 * CONFORMIDAD
 * PROCESO
 * llamar al mensaje de AIS que har� el impacto de los datos en el Sistema Central.
 * SALIDA
 * Resultado de la operaci�n
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_ePolizaImpact implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_ePolizaImpact";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SWSUSCRI = "//SWSUSCRI";
  static final String mcteParam_MAIL = "//MAIL";
  static final String mcteParam_CLAVE = "//CLAVE";
  static final String mcteParam_SWCLAVE = "//SW-CLAVE";
  /**
   * Parametro XML de Salida
   */
  static final String mcteParam_SWCONFIR = "//SWCONFIR";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * Fin constantes
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Object wobjClass = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLRespuestas = null;
    XmlDomExtended wobjXSLSalida = null;
    XmlDomExtended wobjXMLEpolizas = null;
    org.w3c.dom.NodeList wobjXML_POLIZAS_List = null;
    org.w3c.dom.Node wobjXML_POLIZAS_Node = null;
    org.w3c.dom.Element wobjElemento = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarResponse_XML = "";
    String wvarResponse_HTML = "";
    String wvarFalseAIS = "";
    String wvarDocumtip = "";
    String wvarDocumdat = "";
    String wvarCLIENNOM = "";
    String wvarCLIENAPE = "";
    String wvarCIAASCOD = "";
    String wvarRAMOPCOD = "";
    String wvarPOLIZANN = "";
    String wvarPOLIZSEC = "";
    String wvarCERTIPOL = "";
    String wvarCERTIANN = "";
    String wvarCERTISEC = "";
    String wvarSWSUSCRI = "";
    String wvarSWCONFIR = "";
    String wvarMail = "";
    String wvarCLAVE = "";
    String wvarSWCLAVE = "";
    String wvarCONFORME = "";
    String wvarSWTIPOSUS = "";
    int wvarContador = 0;

    //
    //Par�metros de entrada
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;

      //
      //
      wvarStep = 30;
      //Arma XML de entrada al COM+ Multithreading
      //
      wobjXML_POLIZAS_List = wobjXMLRequest.selectNodes( "//Request/EPOLIZAS/EPOLIZA[CIAASCOD='0001']" ) ;


      //Recorre cada p�liza
      wvarRequest = "";
      wvarContador = 1;

      if( wobjXML_POLIZAS_List.getLength() == 0 )
      {

        wvarContador = 0;

      }
      else
      {


        wvarRequest = wvarRequest + "<Request id=\"" + wvarContador + "\"  actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0001\">" + "<DEFINICION>LBA_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";

        for( int nwobjXML_POLIZAS_Node = 0; nwobjXML_POLIZAS_Node < wobjXML_POLIZAS_List.getLength(); nwobjXML_POLIZAS_Node++ )
        {
          wobjXML_POLIZAS_Node = wobjXML_POLIZAS_List.item( nwobjXML_POLIZAS_Node );

          wvarRAMOPCOD = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTISEC" )  );
          wvarSWSUSCRI = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "SWSUSCRI" )  );
          wvarMail = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "MAIL" )  );
          wvarCLAVE = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CLAVE" )  );
          wvarSWCLAVE = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "SW-CLAVE" )  );

          //GED 20-10-2011 - ANEXO I
          wvarSWTIPOSUS = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "SWTIPOSUS" )  );

          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0001</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE>" + "<SWTIPOSUS>" + wvarSWTIPOSUS + "</SWTIPOSUS></EPOLIZA>";


        }


        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

      }


      wobjXML_POLIZAS_List = (org.w3c.dom.NodeList) null;

      wobjXML_POLIZAS_List = wobjXMLRequest.selectNodes( "//Request/EPOLIZAS/EPOLIZA[CIAASCOD='0020']" ) ;

      if( wobjXML_POLIZAS_List.getLength() != 0 )
      {

        wvarContador = wvarContador + 1;


        wvarRequest = wvarRequest + "<Request id=\"" + wvarContador + "\" actionCode=\"nbwsA_MQGenericoAIS\" ciaascod=\"0020\">" + "<DEFINICION>NYL_1185_SusyDesus_epoliza.xml</DEFINICION>" + "<EPOLIZAS>";


        for( int nwobjXML_POLIZAS_Node = 0; nwobjXML_POLIZAS_Node < wobjXML_POLIZAS_List.getLength(); nwobjXML_POLIZAS_Node++ )
        {
          wobjXML_POLIZAS_Node = wobjXML_POLIZAS_List.item( nwobjXML_POLIZAS_Node );

          wvarRAMOPCOD = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "RAMOPCOD" )  );
          wvarPOLIZANN = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "POLIZANN" )  );
          wvarPOLIZSEC = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "POLIZSEC" )  );
          wvarCERTIPOL = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTIPOL" )  );
          wvarCERTIANN = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTIANN" )  );
          wvarCERTISEC = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTISEC" )  );
          wvarSWSUSCRI = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "SWSUSCRI" )  );
          wvarMail = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "MAIL" )  );
          wvarCLAVE = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CLAVE" )  );
          wvarSWCLAVE = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "SW-CLAVE" )  );

          wvarRequest = wvarRequest + "<EPOLIZA><CIAASCOD>0020</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWSUSCRI>" + wvarSWSUSCRI + "</SWSUSCRI>" + "<MAIL>" + wvarMail + "</MAIL>" + "<CLAVE>" + wvarCLAVE + "</CLAVE>" + "<SW-CLAVE>" + wvarSWCLAVE + "</SW-CLAVE></EPOLIZA>";

        }

        wvarRequest = wvarRequest + "</EPOLIZAS></Request>";

      }

      wvarRequest = "<Request>" + wvarRequest + "</Request>";

      wvarStep = 40;
      //Ejecuta la funci�n Multithreading
      ModGeneral.cmdp_ExecuteTrnMulti( "nbwsA_MQGenericoAIS", "nbwsA_MQGenericoAIS.biz", wvarRequest, new Variant( wvarResponse )/*warning: ByRef value change will be lost.*/ );

      //
      wvarStep = 50;
      //Carga las dos respuestas en un XML.
      wobjXMLRespuestas = new XmlDomExtended();
      wobjXMLRespuestas.loadXML( wvarResponse );
      //
      wvarStep = 60;

      //Verifica que no haya pinchado el COM+
      if( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado" )  == (org.w3c.dom.Node) null )
      {
        //
        //Fall� Response 1
        Err.getError().setDescription( "Response 1: fall� COM+ de Request 1" );
        //unsup GoTo ErrorHandler
        //
      }

      wvarStep = 70;
      if( wvarContador == 2 )
      {
        if( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/Estado" )  == (org.w3c.dom.Node) null )
        {
          //
          //Fall� Response 2
          Err.getError().setDescription( "Response 2: fall� COM+ de Request 2" );
          //unsup GoTo ErrorHandler
          //
        }
      }
      //Anduvieron los dos llamados al AIS.
      //Arma un XML con los productos de las dos compa��as
      wvarResponse = "";
      //
      wvarStep = 80;
      if( ! (wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/EPOLIZAS" )  == (org.w3c.dom.Node) null) )
      {
        wvarResponse = wvarResponse + XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/CAMPOS/EPOLIZAS" ));
      }
      else
      {
        wvarResponse = wvarResponse + "";
        wvarFalseAIS = XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='1']/Estado/@mensaje" )  );
      }
      //
      wvarStep = 90;
      if( wvarContador == 2 )
      {
        if( ! (wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/CAMPOS/EPOLIZAS" )  == (org.w3c.dom.Node) null) )
        {
          wvarResponse = wvarResponse + XmlDomExtended.marshal(wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/CAMPOS/EPOLIZAS" ));
        }
        else
        {
          wvarResponse = wvarResponse + "";
          wvarFalseAIS = wvarFalseAIS + String.valueOf( (char)(13) ) + XmlDomExtended.getText( wobjXMLRespuestas.selectSingleNode( "Response/Response[@id='2']/Estado/@mensaje" )  );
        }
      }
      if( wvarResponse.equals( "" ) )
      {
        //unsup GoTo FalseAIS
      }

      wvarResponse = "<Response>" + Strings.replace( Strings.replace( wvarResponse, "<EPOLIZAS>", "" ), "</EPOLIZAS>", "" ) + "</Response>";

      wvarStep = 100;

      wobjXMLEpolizas = new XmlDomExtended();
      wobjXMLEpolizas.loadXML( wvarResponse );


      wobjXML_POLIZAS_List = wobjXMLEpolizas.selectNodes( "Response/EPOLIZA" ) ;

      wvarResponse_XML = "<EPOLIZAS>";
      wvarStep = 110;
      for( int nwobjXML_POLIZAS_Node = 0; nwobjXML_POLIZAS_Node < wobjXML_POLIZAS_List.getLength(); nwobjXML_POLIZAS_Node++ )
      {
        wobjXML_POLIZAS_Node = wobjXML_POLIZAS_List.item( nwobjXML_POLIZAS_Node );

        wvarCIAASCOD = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CIAASCOD" )  );
        wvarRAMOPCOD = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "RAMOPCOD" )  );
        wvarPOLIZANN = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "POLIZANN" )  );
        wvarPOLIZSEC = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "POLIZSEC" )  );
        wvarCERTIPOL = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTIPOL" )  );
        wvarCERTIANN = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTIANN" )  );
        wvarCERTISEC = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "CERTISEC" )  );
        wvarSWCONFIR = XmlDomExtended.getText( wobjXML_POLIZAS_Node.selectSingleNode( "SWCONFIR" )  );

        wvarStep = 120;

        if( (!Strings.trimRight( Strings.trimLeft( wvarCIAASCOD ) ).equals( "" )) && (!Strings.trimRight( Strings.trimLeft( wvarRAMOPCOD ) ).equals( "" )) )
        {
          wvarResponse_XML = wvarResponse_XML + "<EPOLIZA>" + "<CIAASCOD>" + wvarCIAASCOD + "</CIAASCOD>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SWCONFIR>" + wvarSWCONFIR + "</SWCONFIR>" + "</EPOLIZA>";


          //Si se confirma la recepci�n del AIS grabo en el SQL
          wvarCONFORME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/CONFORME" )  );



          if( (wvarSWCONFIR.equals( "S" )) && (wvarCONFORME.equals( "S" )) )
          {
            wvarDocumtip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/DOCUMTIP" )  );
            wvarDocumdat = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/DOCUMDAT" )  );
            wvarMail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/MAIL" )  );
            wvarCLIENNOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/CLIENNOM" )  );
            wvarCLIENAPE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//EPOLIZA[RAMOPCOD='" + wvarRAMOPCOD + "' and POLIZANN='" + wvarPOLIZANN + "' and POLIZSEC='" + wvarPOLIZSEC + "' and CERTIPOL='" + wvarCERTIPOL + "' and CERTIANN='" + wvarCERTIANN + "' and CERTISEC='" + wvarCERTISEC + "']/CLIENAPE" )  );

            wvarStep = 130;

            wvarRequest = "<Request>" + "<DEFINICION>P_NBWS_Suscripciones_Insert.xml</DEFINICION>" + "<RAMOPCOD>" + wvarRAMOPCOD + "</RAMOPCOD>" + "<POLIZANN>" + wvarPOLIZANN + "</POLIZANN>" + "<POLIZSEC>" + wvarPOLIZSEC + "</POLIZSEC>" + "<CERTIPOL>" + wvarCERTIPOL + "</CERTIPOL>" + "<CERTIANN>" + wvarCERTIANN + "</CERTIANN>" + "<CERTISEC>" + wvarCERTISEC + "</CERTISEC>" + "<SUPLENUM>0</SUPLENUM>" + "<TIPO>EPOL</TIPO>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>" + "<EMAIL>" + wvarMail + "</EMAIL>" + "<VIA_SUSCRIPC>NBWS</VIA_SUSCRIPC>" + "<NOMBRE>" + wvarCLIENNOM + "</NOMBRE>" + "<APELLIDO>" + wvarCLIENAPE + "</APELLIDO>" + "</Request>";

            wvarStep = 140;
            wobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
            //error: function 'Execute' was not found.
            //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
            wobjClass = null;


          }
        }

      }
      wvarStep = 150;

      wvarResponse_XML = wvarResponse_XML + "</EPOLIZAS>";





      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"></Estado><Response_XML>" + wvarResponse_XML + "</Response_XML><Response_HTML><![CDATA[" + wvarResponse_HTML + "]]></Response_HTML></Response>" );


      FalseAIS: 
      if( wvarResponse.equals( "" ) )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"" + wvarFalseAIS + "\"></Estado><Response_XML></Response_XML><Response_HTML>" + wvarFalseAIS + "</Response_HTML></Response>" );
      }

      wobjXMLRequest = null;
      wobjXMLRespuestas = null;
      wobjXSLSalida = null;
      wobjXMLEpolizas = null;
      wobjXML_POLIZAS_Node = (org.w3c.dom.Node) null;
      wobjElemento = (org.w3c.dom.Element) null;


      // On Error Resume Next (optionally ignored)
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLRespuestas = null;
        wobjXSLSalida = null;
        wobjXMLEpolizas = null;
        wobjXML_POLIZAS_Node = (org.w3c.dom.Node) null;
        wobjElemento = (org.w3c.dom.Element) null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + wvarRequest, vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
