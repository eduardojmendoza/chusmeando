package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 * TODO: poner COPYRIGHT
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class nbwsA_getXMLsINICIO implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_getXMLsINICIO";
  static final String mcteParam_SESSION_ID = "//SESSION_ID";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXMLNBWS_TempFilesServer = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarSESSION_ID = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Carga XML de entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarSESSION_ID = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SESSION_ID )  );
      //
      //DA - 16/10/2009: se agrega esto para guardar en temporal el XML de inicio y evitar XSS en los ASPs.
      if( !wvarSESSION_ID.equals( "" ) )
      {
        //
        wvarStep = 30;
        //Levanta XML de configuraci�n donde se indica la ruta de grabaci�n de archivos temporales.
        wobjXMLNBWS_TempFilesServer = new XmlDomExtended();
        wobjXMLNBWS_TempFilesServer.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteNBWS_TempFilesServer));
        //
        wvarStep = 40;
        //Levanta el XML de solapa Inicio
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.load( XmlDomExtended .getText( /*unsup wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ) */ ) + "\\sINICIO_" + wvarSESSION_ID + "" );
        //
      }
      //
      wvarStep = 50;
      if( ! (wobjXMLResponse.selectSingleNode( "//PRODUCTO" )  == (org.w3c.dom.Node) null) )
      {
        Response.set( wobjXMLResponse.getDocument().getDocumentElement().toString() );
      }
      else
      {
        Response.set( "" );
      }
      //
      wvarStep = 60;
      wobjXMLRequest = null;
      wobjXMLResponse = null;
      wobjXMLNBWS_TempFilesServer = null;
      //
      wvarStep = 70;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        wobjXMLNBWS_TempFilesServer = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        /*TBD mobjCOM_Context.SetAbort() ;*/
        IAction_Execute = 1;
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
