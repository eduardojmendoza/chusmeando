package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_SOAPMW implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_SOAPMW";
  static final String mcteSubDirName = "DefinicionesMDW";
  static final String mcteLogPath = "LogMDW";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  private String mvarConsultaRealizada = "";
  private boolean mvarCancelacionManual = false;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   */
  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarHoraInicio = "";
    float wvarTiempo = 0;
    String wvarDefinitionFile = "";
    String wvarTextoError = "";
    String wvarURLWS = "";
    String wvarSoapActionURL = "";
    boolean wvarLoguear = false;
    String wvarNomArchivo = "";
    String wvarCDATA = "";
    String wvarNodo = "";
    String wvarDatos = "";
    String wvarTextoALoguear = "";
    int wvarx = 0;
    String wvarTimeStamp = "";
    String wobjXMLOutputSOAP = "";
    int wvarNroArch = 0;
    String wvarExisteArchivo = "";
    String wvarConfFileName = "";
    XmlDomExtended wobjXMLDefinition = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLInputSOAP = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLAux = null;
    org.w3c.dom.Element wobjNodoBase64 = null;
    org.w3c.dom.NodeList wobjNodosEntrada = null;
    org.w3c.dom.Node wobjNodoEntrada = null;
    com.qbe.services.HSBCInterfaces.IAction wobjFrame2MQ = null;
    org.w3c.dom.Element wobjNodo = null;
    Variant[] warrayDatos = (Variant[]) VB.initArray( new Variant[Strings.len( wvarDatos )+1], Variant.class );
    //
    //
    //
    //
    try 
    {
      wvarHoraInicio = DateTime.format( DateTime.now() );
      wvarTiempo = DateTime.timer();
      //
      wvarStep = 10;
      // Crea Objetos XMLDom -----------------------------
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLDefinition = new XmlDomExtended();
      wobjXMLInputSOAP = new XmlDomExtended();
      wobjXMLConfig = new XmlDomExtended();
      //-----------------------------------------------------
      //
      wvarStep = 20;
      // Carga XML del Request ASP---------------------------
      wobjXMLRequest.loadXML( pvarRequest );
      //-----------------------------------------------------
      //
      // Carga Nombre de Archivo de Definici�n------------
      if( ! (wobjXMLRequest.selectSingleNode( "//DEFINICION" )  == (org.w3c.dom.Node) null) )
      {
        wvarDefinitionFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//DEFINICION" )  );
      }
      else
      {
        wvarTextoError = " No se encontr� nodo DEFINICION en Request o XML mal formado";
        Err.raise( -1, "", wvarTextoError );
      }
      //----------------------------------------------------
      //
      // Carga Archivo de Definici�n----------------------------
      wvarStep = 30;
      wobjXMLDefinition.load( System.getProperty("user.dir") + "\\" + mcteSubDirName + "\\" + wvarDefinitionFile );
      if( wobjXMLDefinition.getParseError().getErrorCode() != 0 )
      {
        Err.raise( wobjXMLDefinition.getParseError().getErrorCode(), "", wobjXMLDefinition.getParseError().getMessage() );
      }
      wvarStep = 35;
      //
      // Carga Nombre de Archivo de Definici�n SOAP --------------------
      wvarConfFileName = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/SOAPCONFIGFILE" )  );
      wvarStep = 40;
      //
      // Carga Archivo de Configuraci�n SOAP --------------------------
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wvarConfFileName));
      wvarStep = 45;
      //
      // Carga la URL del WebService y le agrega el server --------------------
      wvarURLWS = XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//SERVER" )  ) + XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/URL" )  );
      wvarStep = 50;
      //
      // Carga el valor de SOAP ACTION ------------------------------
      wvarSoapActionURL = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/SOAPACTION" )  );
      wvarStep = 55;
      //
      // Carga nombre Archivo de Logueo
      wvarLoguear = false;
      //
      if( ! (wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" )  == (org.w3c.dom.Node) null) )
      {
        wvarLoguear = true;
        if( ! (wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) == (org.w3c.dom.Node) null) )
        {
          wvarNomArchivo = XmlDomExtended.getText( wobjXMLDefinition.selectSingleNode( "//DEFINICION/GENERARLOG" ) .getAttributes().getNamedItem( "LogFile" ) );
        }
      }
      //
      wvarStep = 80;
      // carga Envelope de xml definicion
      wobjXMLInputSOAP.loadXML( /*unsup wobjXMLDefinition.selectSingleNode( "//XMLDEFINICION" ) */.getFirstChild().toString() );
      //unsup wobjXMLInputSOAP.resolveExternals = true;
      //
      // Carga los Parametos de Entrada desde la Definicion-------------
      //
      wobjNodosEntrada = wobjXMLDefinition.selectNodes( "//ENTRADA/PARAMETRO" ) ;
      wvarStep = 90;
      //
      // Recorre los Parametros----------------------------------------
      wvarCDATA = "NO";
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
        //Que existan todos los nodos definidos, en el Request
        if( (wobjXMLRequest.selectSingleNode( ("//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) )) )  == (org.w3c.dom.Node) null) && (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
        {
          wvarTextoError = " No se encontr� nodo " + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) + " en Request";
          Err.raise( -1, "", wvarTextoError );
        }
      }
      //
      for( int nwobjNodoEntrada = 0; nwobjNodoEntrada < wobjNodosEntrada.getLength(); nwobjNodoEntrada++ )
      {
        wobjNodoEntrada = wobjNodosEntrada.item( nwobjNodoEntrada );
        
        if( Strings.toUpperCase( XmlDomExtended .getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "BASE64" ) )
        {
          wvarNodo = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          wvarDatos = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" + wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) .getFirstChild().toString();
          // Redimensiona Array de Bytes para alojar datos a convertir a BASE64
          for( wvarx = 1; wvarx <= Strings.len( wvarDatos ); wvarx++ )
          {
            warrayDatos[wvarx - 1].set( (byte) Strings.asc( Strings.mid( wvarDatos, wvarx, 1 ) ) );
          }
          wobjNodoBase64 = (org.w3c.dom.Element) wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) ;
          wobjNodoBase64.setAttribute( "xmlns:dt", "urn:schemas-microsoft-com:datatypes" );
          //unsup wobjNodoBase64.dataType = "bin.base64";
          //unsup wobjNodoBase64.nodeTypedValue = warrayDatos;
          wobjNodoBase64 = (org.w3c.dom.Element) null;
        }
        else if( Strings.toUpperCase( XmlDomExtended .getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "TEXTO" ) )
        {
          wvarNodo = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
          {
            wvarDatos = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
          }
          else
          {
            wvarDatos = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) )  );
          }
          wobjNodo = (org.w3c.dom.Element) wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) ;
          //unsup wobjNodo.nodeTypedValue = wvarDatos;
          wobjNodo = (org.w3c.dom.Element) null;
        }
        else if( Strings.toUpperCase( XmlDomExtended .getText( wobjNodoEntrada.getAttributes().getNamedItem( "Tipo" ) ) ).equals( "XML" ) )
        {

          wvarNodo = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) );
          if( ! (wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) == (org.w3c.dom.Node) null) )
          {
            wvarDatos = XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Default" ) );
          }
          else
          {
            wvarDatos = wobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoEntrada.getAttributes().getNamedItem( "Nombre" ) ) ) .toString();


          }

          wobjXMLAux = new XmlDomExtended();
          wobjXMLAux.loadXML( wvarDatos );

          wobjNodo = (org.w3c.dom.Element) wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) ;

          // GED 02/10/2009 - Se  modifica para que  admita envio de multiples archivos
          /*unsup wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo ) */.getParentNode().replaceChild( wobjXMLAux.selectSingleNode( "//" + wvarNodo ) , wobjXMLInputSOAP.selectSingleNode( "//" + wvarNodo )  );
          wobjNodo = (org.w3c.dom.Element) null;
          wobjXMLAux = null;
        }
        else
        {
        }
      }
      //
      wvarStep = 100;
      //wvarTimeStamp = Year(Now()) & "-" & Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "T" & Right("00" & Hour(Now()), 2) & ":" & Right("00" & Minute(Now()), 2) & ":" & Right("00" & Second(Now()), 2) & ".000000-03:00"
      //wobjXMLInputSOAP.selectSingleNode("//MsgCreatTmsp").Text = wvarTimeStamp
      //
      wvarStep = 130;
      //Env�a el mensaje por SOAP
      wobjXMLOutputSOAP = invoke( "PostWebService", new Variant[] { new Variant(wvarURLWS), new Variant(wvarSoapActionURL), new Variant(wobjXMLInputSOAP.getDocument().getDocumentElement().toString()) } );
      //
      wvarStep = 140;
      // Genera log de la ejecuci�n
      if( wvarLoguear )
      {
        wvarNroArch = FileSystem.getFreeFile();
        wvarExisteArchivo = "";
        wvarExisteArchivo = "" /*unsup this.Dir( System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + wvarNomArchivo, 0 ) */;

        if( !wvarExisteArchivo.equals( "" ) )
        {
          FileSystem.kill( System.getProperty("user.dir") + "\\" + mcteLogPath + "\\" + wvarNomArchivo );
        }

        //error: Opening files for 'Binary' access is not supported.
        //unsup: Open App.Path & "\" & mcteLogPath & "\" & wvarNomArchivo For Binary As wvarNroArch
        wvarTextoALoguear = "<LOGs><LOG InicioConsulta=\"" + wvarHoraInicio + "\" FinConsulta=\"" + DateTime.now() + "\" TiempoIncurrido=\"" + (DateTime.timer() - wvarTiempo) + " seg\"" + ">" + "<AreaIn>" + wobjXMLInputSOAP.getDocument().getDocumentElement().toString() + "</AreaIn>" + "<AreaOut>" + wobjXMLOutputSOAP + "</AreaOut>" + "</LOG></LOGs>";

        /*unsup this.Put( wvarNroArch, 0, new Variant( wvarTextoALoguear ) ) */;
        FileSystem.close();
      }
      //
      pvarResponse.set( "<Response><Estado resultado='true' />" + wobjXMLOutputSOAP + "</Response>" );
      //
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wobjXMLRequest = null;
      wobjXMLInputSOAP = null;
      wobjNodoBase64 = (org.w3c.dom.Element) null;
      wobjXMLDefinition = null;
      wobjXMLConfig = null;
      wobjNodosEntrada = (org.w3c.dom.NodeList) null;
      wobjNodoEntrada = (org.w3c.dom.Node) null;
      wobjNodo = (org.w3c.dom.Element) null;
      //
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + Err.getError().getDescription() + String.valueOf( (char)(34) ) + " /></Response>" );
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        //
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String PostWebService( String pvarURL, String pvarSoapActionUrl, String pvarXmlBody ) throws Exception
  {
    String PostWebService = "";
    XmlDomExtended mobjDom = null;
    MSXML2.XMLHTTP mobjXmlHttp = new MSXML2.XMLHTTP();
    String mvarStrRet = "";
    double mvarintPos1 = 0.0;
    double mvarintPos2 = 0.0;
    //
    // Crea objetos DOMDocument y XMLHTTP
    mobjDom = new XmlDomExtended();
    mobjXmlHttp = new MSXML2.XMLHTTP();
    //
    // carga XML
    mobjDom.loadXML( pvarXmlBody );
    //
    // Abre el webservice
    mobjXmlHttp.Open( "POST", pvarURL, false );
    //
    // Crea encabezados
    mobjXmlHttp.setRequestHeader( "Content-Type", "text/xml; charset=utf-8" );
    mobjXmlHttp.setRequestHeader( "SOAPAction", pvarSoapActionUrl );
    //
    // Envia XML
    mobjXmlHttp.send( mobjDom.getDocument().getDocumentElement().toString() );
    //
    // Obtiene la respuesta del envio
    mvarStrRet = mobjXmlHttp.responseText.toString();
    //
    // Cierra el objeto
    mobjXmlHttp = (MSXML2.XMLHTTP) null;
    //
    // Extrae el resultado
    mvarintPos1 = Strings.find( mvarStrRet, "Result>" ) + 7;
    mvarintPos2 = Strings.find( mvarStrRet, "</" );
    if( (mvarintPos1 > 7) && (mvarintPos2 > 0) )
    {
      mvarStrRet = Strings.mid( mvarStrRet, (int)Math.rint( mvarintPos1 ), (int)Math.rint( mvarintPos2 - mvarintPos1 ) );
    }
    //
    // Devuelve el resultado
    PostWebService = mvarStrRet;
    return PostWebService;
  }

  public void Activate() throws Exception
  {

    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/


  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
