package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_Ingresar implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_Ingresar";
  static final String mcteParam_USUARIO = "//USUARIO";
  static final String mcteParam_PASSWORD = "//PASSWORD";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_RCC = "//RCCs";
  static final String mcteParam_IPORIGEN = "//IPORIGEN";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENCLISUS = "P_NBWS_ClienteSuscripto.xml";
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final String mcteParam_XMLSQLGENOBTIDN = "P_NBWS_ObtenerIdentificador.xml";
  static final String mcteParam_XMLSQLGENGUARCC = "P_NBWS_GuardarUltimoRCC.xml";
  static final String mcteParam_XMLSQLGENRCCVAL = "P_NBWS_RegistrarRCCValido.xml";
  static final String mcteParam_XMLSQLGENRCCFAL = "P_NBWS_RegistrarRCCFallido.xml";
  static final String mcteParam_XMLSQLGENOBTPAS = "P_NBWS_ObtenerPassword.xml";
  static final String mcteParam_XMLSQLGENPASVAL = "P_NBWS_RegistrarPasswordValido.xml";
  static final String mcteParam_XMLSQLGENPASFAL = "P_NBWS_RegistrarPasswordFallido.xml";
  static final String mcteParam_XMLSQLGENLASLOG = "P_NBWS_LastLogon.xml";
  static final String mcteParam_XMLSQLGENREGACC = "P_NBWS_RegistrarAcceso.xml";
  /**
   * Constante
   */
  static final String mcteParam_RCCs = "//rcc";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  static final int mcteMsg_USRNOACTIVO = 2;
  static final int mcteMsg_IDNBLOQUEADO = 3;
  static final int mcteMsg_RCCINVALIDO = 4;
  static final int mcteMsg_PASSEXPIRADA = 5;
  static final int mcteMsg_PASSBLOQUEADA = 6;
  static final int mcteMsg_PASSINVALIDA = 7;
  static final int mcteMsg_POLIZANOEMITIDA_OV = 8;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[14];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarUsuario = "";
    String wvarPASSWORD = "";
    boolean wvarPassValida = false;
    Variant wvarDocumtip = new Variant();
    Variant wvarDocumdat = new Variant();
    Variant wvarCLIENNOM = new Variant();
    Variant wvarCLIENAPE = new Variant();
    String wvarRCCs = "";
    String wvarRCC = "";
    boolean wvarRCCValido = false;
    String wvarIPOrigen = "";
    String wvarError = "";
    boolean wvarUsrValido = false;
    String wvarEstadoUsr = "";
    Variant wvarIdentificador = new Variant();
    Variant wvarEstadoIdentificador = new Variant();
    Variant wvarRCCPrevio = new Variant();
    Variant wvarEstadoPassword = new Variant();
    boolean wvarLoginSinRCC = false;
    String wvarLastLogon = "";
    String wvarBodyResp = "";
    Variant wvarComboPID = new Variant();
    String wvarProximoPaso = "";
    Variant wvarVIA_INSCRIPCION = new Variant();
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = String.valueOf( mcteMsg_OK );
      wvarLoginSinRCC = true;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOACTIVO] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOACTIVO" );
      mcteMsg_DESCRIPTION[mcteMsg_IDNBLOQUEADO] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_IDNBLOQUEADO" );
      mcteMsg_DESCRIPTION[mcteMsg_RCCINVALIDO] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_RCCINVALIDO" );
      mcteMsg_DESCRIPTION[mcteMsg_PASSEXPIRADA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_PASSEXPIRADA" );
      mcteMsg_DESCRIPTION[mcteMsg_PASSBLOQUEADA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_PASSBLOQUEADA" );
      mcteMsg_DESCRIPTION[mcteMsg_PASSINVALIDA] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_PASSINVALIDA" );
      mcteMsg_DESCRIPTION[mcteMsg_POLIZANOEMITIDA_OV] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_POLIZANOEMITIDA_OV" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarUsuario = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarStep = 60;
      wvarPASSWORD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PASSWORD )  );
      wvarStep = 70;
      wvarIPOrigen = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IPORIGEN )  );
      wvarStep = 80;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  == (org.w3c.dom.Node) null) )
      {
        wvarStep = 90;
        wvarDocumtip.set( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP )  ) );
        wvarStep = 100;
        wvarRCCs = wobjXMLRequest.selectSingleNode( mcteParam_RCC ) .toString();
        wvarStep = 130;
        wvarLoginSinRCC = false;
      }
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      wvarEstadoUsr = invoke( "fncValidarUsuario", new Variant[] { new Variant(wvarUsuario), new Variant(wvarEstadoIdentificador), new Variant(wvarEstadoPassword) } );
      if( wvarEstadoUsr.equals( "ERR" ) )
      {
        wvarError = String.valueOf( mcteMsg_USRNOEXISTE );
      }
      else
      {
        wvarStep = 160;
        //2) Verificar estado del usuario
        if( !wvarEstadoUsr.equals( "A" ) )
        {
          wvarError = String.valueOf( mcteMsg_USRNOACTIVO );
        }
        else
        {
          //3) Verificar estado diferente a bloqueado
          wvarStep = 180;
          if( wvarEstadoIdentificador.toInt() == 2 )
          {
            wvarError = String.valueOf( mcteMsg_IDNBLOQUEADO );
          }
        }
      }
      //
      //Si el login es previo a la verificacion del RCC
      wvarStep = 190;
      if( wvarLoginSinRCC )
      {
        if( Obj.toInt( wvarError ) == mcteMsg_OK )
        {
          //4) Verificar RCC preexistente
          //Obtener identificador
          wvarStep = 170;
          invoke( "fncObtenerIdentificador", new Variant[] { new Variant(wvarUsuario), new Variant(wvarDocumtip), new Variant(wvarIdentificador), new Variant(wvarRCCPrevio) } );
          //
          if( Strings.len( wvarRCCPrevio.toString() ) == 0 )
          {
            //Generar RCC y guardarlo
            wvarStep = 210;
            wvarRCCs = invoke( "fncGenerarRCC", new Variant[] { new Variant(wvarUsuario), new Variant(wvarIdentificador.toString()) } );
            wvarStep = 220;
            invoke( "fncGuardarRCC", new Variant[] { new Variant(wvarUsuario), new Variant(wvarRCCs) } );
          }
          else
          {
            wvarStep = 230;
            wvarRCCs = wvarRCCPrevio.toString();
          }
          wvarStep = 240;
          pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + wvarRCCs + "</Response>" );
        }
        else
        {
          wvarStep = 250;
          pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
        }
      }
      else
      {
        //LOGIN CON RCC ESPECIFICADO
        //4) valida el identificador en base al RCC ingresado
        wvarStep = 300;
        wvarRCCValido = invoke( "fncValidarRCC", new Variant[] { new Variant(wvarUsuario), new Variant(wvarDocumtip), new Variant(wvarRCCs) } );
        if( ! (wvarRCCValido) )
        {
          wvarStep = 310;
          wvarError = String.valueOf( mcteMsg_RCCINVALIDO );
          wvarStep = 320;
          invoke( "fncRegistrarRCCFallido", new Variant[] { new Variant(wvarUsuario) } );
        }
        else
        {
          //5) Registra IDN Valido
          wvarStep = 330;
          invoke( "fncRegistrarRCCValido", new Variant[] { new Variant(wvarUsuario) } );
          //Obtiene Estado Password y verifica que no este bloqueado o expirado
          if( wvarEstadoPassword.toInt() == 1 )
          {
            wvarStep = 340;
            wvarError = String.valueOf( mcteMsg_PASSEXPIRADA );
            //DA - 18/11/2009: en caso que la PWD est� bloqueada con CAI pendiente
          }
          else if( (wvarEstadoPassword.toInt() == 2) || (wvarEstadoPassword.toInt() == 5) )
          {
            wvarStep = 350;
            wvarError = String.valueOf( mcteMsg_PASSBLOQUEADA );
          }
          else
          {
            //6) Valida la password
            wvarStep = 360;
            wvarPassValida = invoke( "fncValidarPass", new Variant[] { new Variant(wvarUsuario), new Variant(wvarPASSWORD) } );
            if( ! (wvarPassValida) )
            {
              wvarStep = 370;
              wvarError = String.valueOf( mcteMsg_PASSINVALIDA );
              invoke( "fncRegistrarPassFallida", new Variant[] { new Variant(wvarUsuario) } );
            }
            else
            {
              wvarStep = 380;
              invoke( "fncRegistrarPassValida", new Variant[] { new Variant(wvarUsuario) } );
            }
          }
        }
        //
        //7) Verifica si es CAI o CAP o OK
        //Genera respuesta dependiendo del caso
        wvarStep = 400;
        if( Obj.toInt( wvarError ) == mcteMsg_OK )
        {
          if( wvarEstadoPassword.toInt() == 0 )
          {
            //Ingreso OK
            wvarStep = 410;
            wvarLastLogon = invoke( "fncObtenerLastLogon", new Variant[] { new Variant(wvarUsuario) } );
            wvarStep = 420;
            invoke( "fncObtenerNomApeCliente", new Variant[] { new Variant(wvarUsuario), new Variant(wvarCLIENNOM), new Variant(wvarCLIENAPE) } );
            wvarStep = 425;
            invoke( "fncObtenerIdentificador", new Variant[] { new Variant(wvarUsuario), new Variant(wvarDocumtip), new Variant(wvarDocumdat), new Variant(wvarRCCPrevio) } );
            wvarStep = 430;
            wvarBodyResp = "<INGRESO>OK</INGRESO>" + "<CLIENNOM>" + wvarCLIENNOM + "</CLIENNOM>" + "<CLIENAPE>" + wvarCLIENAPE + "</CLIENAPE>" + "<LASTLOGON>" + wvarLastLogon + "</LASTLOGON>" + "<DOCUMTIP>" + wvarDocumtip + "</DOCUMTIP>" + "<DOCUMDAT>" + wvarDocumdat + "</DOCUMDAT>";

            //DA - 06/11/2009: borra temporales de sINICIO para evitar sobrecagar al file server.
            wvarStep = 431;
            invoke( "fncBorrarTemporales", new Variant[] {} );

          }
          else if( wvarEstadoPassword.toInt() == 3 )
          {
            //Ingreso CAI
            wvarStep = 440;
            wvarProximoPaso = invoke( "fncAnalizarCAI", new Variant[] { new Variant(wvarUsuario), new Variant(wvarComboPID), new Variant(wvarVIA_INSCRIPCION) } );
            wvarStep = 460;
            if( wvarProximoPaso.equals( "ERR_OV" ) )
            {
              //DA - 02/10/2009: se agrega la via de inscripci�n.
              wvarError = String.valueOf( mcteMsg_POLIZANOEMITIDA_OV );
              wvarBodyResp = "<INGRESO>CAI</INGRESO>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "<ACCION>" + wvarProximoPaso + "</ACCION>" + "<COMBOPID>" + wvarComboPID + "</COMBOPID>" + "<VIA_INSCRIPCION>" + wvarVIA_INSCRIPCION + "</VIA_INSCRIPCION>";
            }
            else
            {
              wvarBodyResp = "<INGRESO>CAI</INGRESO>" + "<ACCION>" + wvarProximoPaso + "</ACCION>" + "<COMBOPID>" + wvarComboPID + "</COMBOPID>" + "<VIA_INSCRIPCION>" + wvarVIA_INSCRIPCION + "</VIA_INSCRIPCION>";
            }

          }
          else if( wvarEstadoPassword.toInt() == 4 )
          {
            wvarStep = 470;
            wvarBodyResp = "<INGRESO>CAP</INGRESO>";
          }
          wvarStep = 480;
          pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + wvarBodyResp + "</Response>" );
        }
        else
        {
          wvarStep = 490;
          pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[Obj.toInt( wvarError )] + "</MSGRESULTADO>" + "</Response>" );
        }
        //Registra el acceso en log
        wvarStep = 500;
        invoke( "fncRegistrarAcceso", new Variant[] { new Variant(wvarUsuario), new Variant(Obj.toInt( new Variant(wvarError) )), new Variant(wvarIPOrigen) } );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 700;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String fncValidarUsuario( String pvarUsuario, Variant pvarEstadoIdentificador, Variant pvarEstadoPassword ) throws Exception
  {
    String fncValidarUsuario = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENVALUSR + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        //
        if( ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "S" )) || ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "N" )) )
        {
          mvarRetVal = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//ESTADO" )  );
          pvarEstadoIdentificador.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOIDENTIFICADOR" )  ) );
          pvarEstadoPassword.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOPASSWORD" )  ) );
        }
      }
    }
    //
    if( mvarRetVal.equals( "" ) )
    {
      mvarRetVal = "ERR";
    }
    //
    fncValidarUsuario = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarUsuario;
  }

  private String fncObtenerIdentificador( String pvarUsuario, Variant pvarDocumtip, Variant pvarDocumdat, Variant pvarRCCPrevio ) throws Exception
  {
    String fncObtenerIdentificador = "";
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENOBTIDN + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        pvarDocumtip.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//DOCUMTIP" )  ) );
        pvarDocumdat.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//DOCUMDAT" )  ) );
        pvarRCCPrevio.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//RCCULTIMO" )  ) );
      }
    }
    //
    if( pvarDocumdat.toString().equals( "" ) )
    {
      pvarDocumdat.set( "ERR" );
    }
    //
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncObtenerIdentificador;
  }

  private String fncGenerarRCC( String pvarUsuario, String pvarIdentificador ) throws Exception
  {
    String fncGenerarRCC = "";
    int wvarRCC1 = 0;
    int wvarRCC2 = 0;
    int wvarRCC3 = 0;
    String wvarIDN = "";
    String wvarResult = "";
    //
    //
    VB.randomize();
    //
    wvarRCC1 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    //
    wvarRCC2 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    while( wvarRCC2 == wvarRCC1 )
    {
      wvarRCC2 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    }
    //
    wvarRCC3 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    while( (wvarRCC3 == wvarRCC1) || (wvarRCC3 == wvarRCC2) )
    {
      wvarRCC3 = (int)Math.rint( Math.floor( ((Strings.len( pvarIdentificador ) - 3) * VB.rnd()) + 1 ) + 2 );
    }
    //
    wvarIDN = pvarIdentificador;
    wvarIDN = Strings.mid( wvarIDN, 1, (wvarRCC1 - 1) ) + "X" + Strings.mid( wvarIDN, (wvarRCC1 + 1), Strings.len( wvarIDN ) );
    wvarIDN = Strings.mid( wvarIDN, 1, (wvarRCC2 - 1) ) + "X" + Strings.mid( wvarIDN, (wvarRCC2 + 1), Strings.len( wvarIDN ) );
    wvarIDN = Strings.mid( wvarIDN, 1, (wvarRCC3 - 1) ) + "X" + Strings.mid( wvarIDN, (wvarRCC3 + 1), Strings.len( wvarIDN ) );
    //
    wvarResult = "<RCCs longitud=" + String.valueOf( (char)(34) ) + Strings.len( pvarIdentificador ) + String.valueOf( (char)(34) ) + " idn=" + String.valueOf( (char)(34) ) + wvarIDN + String.valueOf( (char)(34) ) + ">" + "<rcc id=" + String.valueOf( (char)(34) ) + "1" + String.valueOf( (char)(34) ) + ">" + wvarRCC1 + "</rcc>" + "<rcc id=" + String.valueOf( (char)(34) ) + "2" + String.valueOf( (char)(34) ) + ">" + wvarRCC2 + "</rcc>" + "<rcc id=" + String.valueOf( (char)(34) ) + "3" + String.valueOf( (char)(34) ) + ">" + wvarRCC3 + "</rcc>" + "</RCCs>";
    //
    fncGenerarRCC = wvarResult;
    //
    return fncGenerarRCC;
  }

  private boolean fncGuardarRCC( String pvarUsuario, String pvarRCC ) throws Exception
  {
    boolean fncGuardarRCC = false;
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRetVal = String.valueOf( true );
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENGUARCC + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "<ULTIMORCC><![CDATA[" + pvarRCC + "]]></ULTIMORCC>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    fncGuardarRCC = Obj.toBoolean( mvarRetVal );
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncGuardarRCC;
  }

  private boolean fncValidarRCC( String pvarUsuario, Variant pvarDocumtip, String pvarRCC ) throws Exception
  {
    boolean fncValidarRCC = false;
    String mvarRequest = "";
    String mvarResponse = "";
    boolean mvarRetVal = false;
    boolean mvarErrorRCC = false;
    XmlDomExtended mobjXMLResponse = null;
    XmlDomExtended mobjXMLRCC = null;
    XmlDomExtended mobjXMLUltimoRCC = null;
    org.w3c.dom.Node mobjXMLRequestNode = null;
    Object mobjClass = null;
    String mvarDOCUMDAT = "";
    String mvarDOCUMTIP = "";
    String mvarRCCULTIMO = "";
    String mvarRCC = "";
    //
    //
    mvarRetVal = false;
    mvarErrorRCC = false;
    mobjXMLRCC = new XmlDomExtended();
    mobjXMLRCC.loadXML( pvarRCC );
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENOBTIDN + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarDOCUMDAT = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DOCUMDAT" )  );
        mvarDOCUMTIP = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DOCUMTIP" )  );
        mvarRCCULTIMO = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//RCCULTIMO" )  );
      }
    }
    //
    if( Strings.trim( mvarRCCULTIMO ).equals( "" ) )
    {
      mvarRetVal = false;
    }
    else
    {
      //
      mobjXMLUltimoRCC = new XmlDomExtended();
      mobjXMLUltimoRCC.loadXML( mvarRCCULTIMO );
      //
      //Primero verifica que los documtip sean iguales
      if( pvarDocumtip.toInt() == Obj.toInt( mvarDOCUMTIP ) )
      {
        //Luego verifica cada uno de los caracteres del rcc
        for( int nmobjXMLRequestNode = 0; nmobjXMLRequestNode < mobjXMLRCC.selectNodes( mcteParam_RCCs ) .getLength(); nmobjXMLRequestNode++ )
        {
          mobjXMLRequestNode = mobjXMLRCC.selectNodes( mcteParam_RCCs ) .item( nmobjXMLRequestNode );
          mvarRCC = XmlDomExtended.getText( mobjXMLUltimoRCC.selectSingleNode( "//rcc[@id=" + XmlDomExtended.getText( mobjXMLRequestNode.getAttributes().item( 0 ) ) + "]" )  );
          //comparacion de lo ingresado con el caracter correspondiente
          if( !Strings.mid( mvarDOCUMDAT, Obj.toInt( mvarRCC ), 1 ).equals( XmlDomExtended .getText( mobjXMLRequestNode ) ) )
          {
            mvarErrorRCC = true;
            break;
          }
        }
        if( ! (mvarErrorRCC) )
        {
          mvarRetVal = true;
        }
      }
    }
    //
    fncValidarRCC = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    mobjXMLRCC = null;
    mobjXMLUltimoRCC = null;
    //
    return fncValidarRCC;
  }

  private Variant fncRegistrarRCCFallido( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarRCCFallido = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENRCCFAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    return fncRegistrarRCCFallido;
  }

  private Variant fncRegistrarRCCValido( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarRCCValido = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENRCCVAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    return fncRegistrarRCCValido;
  }

  private boolean fncValidarPass( String pvarUsuario, String pvarPassword ) throws Exception
  {
    boolean fncValidarPass = false;
    String mvarRequest = "";
    String mvarResponse = "";
    boolean mvarRetVal = false;
    boolean mvarErrorRCC = false;
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    String mvarPASSENC = "";
    String mvarPASSWORD = "";
    //
    //
    mvarRetVal = false;
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENOBTPAS + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarPASSENC = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//PASSWORD" )  );
      }
    }
    //
    mvarPASSWORD = ModEncryptDecrypt.CapicomDecrypt( mvarPASSENC );
    //
    //Primero verifica que las passwords sean iguales
    if( Strings.toUpperCase( pvarPassword ).equals( Strings.toUpperCase( mvarPASSWORD ) ) )
    {
      mvarRetVal = true;
    }
    //
    fncValidarPass = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarPass;
  }

  private Variant fncRegistrarPassFallida( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarPassFallida = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENPASFAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    return fncRegistrarPassFallida;
  }

  private Variant fncRegistrarPassValida( String pvarUsuario ) throws Exception
  {
    Variant fncRegistrarPassValida = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENPASVAL + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    return fncRegistrarPassValida;
  }

  private String fncObtenerLastLogon( String pvarUsuario ) throws Exception
  {
    String fncObtenerLastLogon = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    String mvarFECHA = "";
    String mvarTIPOACCESO = "";
    String mvarDESCRIPCION = "";
    String mvarDIRECCION = "";
    //
    //
    mvarRetVal = "";
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENLASLOG + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarFECHA = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//FECHA" )  );
        mvarTIPOACCESO = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//TIPOACCESO" )  );
        mvarDESCRIPCION = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DESCRIPCION" )  );
        mvarDIRECCION = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//DIRECCION" )  );
        //DA - 06/10/2009: a pedido de LG "Estetica solapas.doc" se le d� este formato.
        mvarRetVal = Strings.format( mvarFECHA, "dd/mm/yyyy\\, \\a \\la\\s h:mm" );
      }
    }
    //
    fncObtenerLastLogon = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncObtenerLastLogon;
  }

  private Variant fncRegistrarAcceso( String pvarUsuario, int pvarError, String pvarIPOrigen ) throws Exception
  {
    Variant fncRegistrarAcceso = new Variant();
    Object mobjClass = null;
    String mvarRequest = "";
    String mvarResponse = "";
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENREGACC + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "<TIPOACCESO>" + pvarError + "</TIPOACCESO>" + "<IPADDRESS>" + pvarIPOrigen + "</IPADDRESS>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    return fncRegistrarAcceso;
  }

  private Variant fncObtenerNomApeCliente( String pvarUsuario, Variant pvarCLIENNOM, Variant pvarCLIENAPE ) throws Exception
  {
    Variant fncObtenerNomApeCliente = new Variant();
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    Variant mvarDOCUMTIP = new Variant();
    Variant mvarDOCUMDAT = new Variant();
    Variant mvarRCCPrevio = new Variant();
    XmlDomExtended mobjXMLResponse = null;
    XmlDomExtended wobjXMLResponseAIS = null;
    Object wobjClass = null;
    String wobjResponseAIS = "";
    //
    //
    pvarCLIENNOM.set( "" );
    pvarCLIENAPE.set( "" );
    //
    invoke( "fncObtenerIdentificador", new Variant[] { new Variant(pvarUsuario), new Variant(mvarDOCUMTIP), new Variant(mvarDOCUMDAT), new Variant(mvarRCCPrevio) } );
    //
    mvarRequest = "<Request>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "</Request>";
    wobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(mvarRequest, wobjResponseAIS, "")
    wobjClass = null;
    //
    wobjXMLResponseAIS = new XmlDomExtended();
    wobjXMLResponseAIS.loadXML( wobjResponseAIS );
    //
    if( ! (wobjXMLResponseAIS == ( XmlDomExtended ) null) )
    {
      if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado" )  == (org.w3c.dom.Node) null) )
      {
        if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado[@resultado='true']" )  == (org.w3c.dom.Node) null) )
        {
          //Recupera Nombre
          if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENNOM" )  == (org.w3c.dom.Node) null) )
          {
            pvarCLIENNOM.set( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENNOM" )  ) );
          }
          //Recupera Apellido 1
          if( ! (wobjXMLResponseAIS.selectSingleNode( "//CLIENAP1" )  == (org.w3c.dom.Node) null) )
          {
            pvarCLIENAPE.set( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//CLIENAP1" )  ) );
          }
        }
      }
    }
    //
    mobjXMLResponse = null;
    wobjXMLResponseAIS = null;
    return fncObtenerNomApeCliente;
  }

  private String fncAnalizarCAI( String pvarUsuario, Variant pPositiveId, Variant pVIA_INSCRIPCION ) throws Exception
  {
    String fncAnalizarCAI = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    String mvarVIAINSCR = "";
    String mvarXMLPositiveID = "";
    boolean mvarPolizasValidas = false;
    XmlDomExtended mobjXMLResponse = null;
    XmlDomExtended wobjXMLResponseAIS = null;
    org.w3c.dom.NodeList wobjNodeList = null;
    org.w3c.dom.Node wobjNode = null;
    org.w3c.dom.Node mobjNodo = null;
    nbwsA_Transacciones.nbwsA_sInicio mobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
    String mvarPasoSiguiente = "";
    Variant mvarDOCUMTIP = new Variant();
    Variant mvarDOCUMDAT = new Variant();
    Variant mvarRCCPrevio = new Variant();
    int mvarI = 0;
    String mvarResp = "";
    String mvarOption = "";
    String mvarTIPOPROD = "";
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String mvarPOSITIVEID = "";
    String mvarDato2 = "";
    String mvarDato3 = "";
    String mvarCOBRODES = "";
    //
    //
    mvarPolizasValidas = false;
    mvarXMLPositiveID = "";
    //
    invoke( "fncObtenerIdentificador", new Variant[] { new Variant(pvarUsuario), new Variant(mvarDOCUMTIP), new Variant(mvarDOCUMDAT), new Variant(mvarRCCPrevio) } );
    //
    //Obtiene la via de inscripcion del cliente (CC, OV, NBWS)
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENCLISUS + "</DEFINICION>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = (nbwsA_Transacciones.nbwsA_sInicio) null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        mvarVIAINSCR = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//VIAINSCRIPCION" )  );
      }
    }
    //
    //DA - 02/10/2009: devuelve la v�a de inscripci�n
    pVIA_INSCRIPCION.set( mvarVIAINSCR );
    //
    //Dependiendo de la via de inscripcion, analiza cual es el paso siguiente
    
    if( mvarVIAINSCR.equals( "CC" ) )
    {
      mvarPasoSiguiente = "2";
    }
    else if( mvarVIAINSCR.equals( "OV" ) || mvarVIAINSCR.equals( "NBWS" ) )
    {
      mvarRequest = "<Request>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "</Request>";
      mobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
      mobjClass.Execute( mvarRequest, mvarResponse, "" );
      mobjClass = (nbwsA_Transacciones.nbwsA_sInicio) null;
      //
      wobjXMLResponseAIS = new XmlDomExtended();
      wobjXMLResponseAIS.loadXML( mvarResponse );
      //
      mvarResp = "";
      if( ! (wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          //DA - 01/09/2009: se agrega la validaci�n de p�lizas exclu�das
          //Se muestran todos los productos del cliente, tenga o no Positive ID.
          //Set wobjNodeList = wobjXMLResponseAIS.selectNodes("//PRODUCTO[HABILITADO_NBWS='S' and POSITIVEID!=' ']")
          wobjNodeList = wobjXMLResponseAIS.selectNodes( "//PRODUCTO[HABILITADO_NBWS='S' and POLIZA_EXCLUIDA ='N' and HABILITADO_NAVEGACION='S' and MAS_DE_CINCO_CERT='N']" ) ;

          if( wobjNodeList.getLength() == 0 )
          {
            //
            mvarResp = "<OPTION VALUE=\"0\">Usted no posee productos habilitados para NBWS</OPTION>";
          }
          else
          {
            //Si hay polizas navegables, arma el XML para el combo y validacion de PositiveID
            //DA - 18/09/2009: en una call con LG pidi� que se agrupen los productos. Es decir, si tiene mas de un AUS1 mostrar solo uno.
            mvarPolizasValidas = true;
            for( int nmobjNodo = 0; nmobjNodo < wobjNodeList.getLength(); nmobjNodo++ )
            {
              mobjNodo = wobjNodeList.item( nmobjNodo );
              mvarTIPOPROD = XmlDomExtended.getText( mobjNodo.selectSingleNode( "TIPOPROD" )  );
              mvarRAMOPCOD = XmlDomExtended.getText( mobjNodo.selectSingleNode( "RAMOPCOD" )  );
              mvarPOLIZANN = XmlDomExtended.getText( mobjNodo.selectSingleNode( "POLIZANN" )  );
              mvarPOLIZSEC = XmlDomExtended.getText( mobjNodo.selectSingleNode( "POLIZSEC" )  );
              mvarCERTIPOL = XmlDomExtended.getText( mobjNodo.selectSingleNode( "CERTIPOL" )  );
              mvarCERTIANN = XmlDomExtended.getText( mobjNodo.selectSingleNode( "CERTIANN" )  );
              mvarCERTISEC = XmlDomExtended.getText( mobjNodo.selectSingleNode( "CERTISEC" )  );
              mvarPOSITIVEID = XmlDomExtended.getText( mobjNodo.selectSingleNode( "POSITIVEID" )  );
              mvarDato2 = XmlDomExtended.getText( mobjNodo.selectSingleNode( "DATO2" )  );
              mvarDato3 = XmlDomExtended.getText( mobjNodo.selectSingleNode( "DATO3" )  );
              mvarCOBRODES = XmlDomExtended.getText( mobjNodo.selectSingleNode( "COBRODES" )  );
              //
              if( (mvarTIPOPROD.equals( "G" )) && (mvarCERTISEC.equals( "0" )) )
              {
                //DA - 11/11/2009: LG en un mail del 11/11/2009 pidi� que para los productos GENERALES no se muestre la p�liza colectiva con CERTISEC = 0
              }
              else
              {
                mvarOption = "<OPTION PID=" + String.valueOf( (char)(34) ) + mvarPOSITIVEID + String.valueOf( (char)(34) ) + " value=" + String.valueOf( (char)(34) ) + mvarRAMOPCOD + "|" + mvarPOLIZANN + "|" + mvarPOLIZSEC + "|" + mvarCERTIPOL + "|" + mvarCERTIANN + "|" + mvarCERTISEC + String.valueOf( (char)(34) ) + " DATO2=" + String.valueOf( (char)(34) ) + mvarDato2 + String.valueOf( (char)(34) ) + " DATO3=" + String.valueOf( (char)(34) ) + mvarDato3 + String.valueOf( (char)(34) ) + " COBRODES=" + String.valueOf( (char)(34) ) + mvarCOBRODES + String.valueOf( (char)(34) ) + ">";
                //
                if( (mvarTIPOPROD.equals( "M" )) || (mvarTIPOPROD.equals( "G" )) )
                {
                  //DA - 18/09/2009: LG en una call dijo que se quite el nro de p�liza
                  //Esto rompe el esquema de los combos del PositiveID de NYL. Hablar con AG cuando vuelva de vacaciones.
                  //En la reuni�n del 20/10/2009 se acord� dejar el combo como est�.
                  mvarOption = mvarOption + XmlDomExtended.getText( mobjNodo.selectSingleNode( "RAMOPDES" )  ) + " (" + mvarRAMOPCOD + "-" + Strings.right( ("00" + mvarPOLIZANN), 2 ) + "-" + Strings.right( ("000000" + mvarPOLIZSEC), 6 ) + "/" + Strings.right( ("0000" + mvarCERTIPOL), 4 ) + "-" + Strings.right( ("0000" + mvarCERTIANN), 4 ) + "-" + Strings.right( ("000000" + mvarCERTISEC), 6 ) + ")";
                }
                else
                {
                  //DA - 18/09/2009: LG en una call dijo que se quite el nro de p�liza
                  //Esto rompe el esquema de los combos del PositiveID de NYL. Hablar con AG cuando vuelva de vacaciones.
                  //En la reuni�n del 20/10/2009 se acord� dejar el combo como est�.
                  mvarOption = mvarOption + XmlDomExtended.getText( mobjNodo.selectSingleNode( "RAMOPDES" )  ) + " (" + mvarRAMOPCOD + "-" + Strings.right( ("00" + mvarPOLIZANN), 2 ) + "-" + Strings.right( ("000000" + mvarPOLIZSEC), 6 ) + ")";
                }
                mvarOption = mvarOption + "</OPTION>";
              }
              //
              //Verifica que la poliza no este cargada ya en el combo
              //Caso Productos de Cartera general con mas de un certificado
              if( Strings.find( mvarResp, (mvarRAMOPCOD + " " + mvarPOLIZANN + "-" + mvarPOLIZSEC) ) == 0 )
              {
                mvarResp = mvarResp + mvarOption;
              }
              //
              mvarOption = "";
              //
            }
          }
        }
      }
      //
      pPositiveId.set( mvarResp );
      //
      if( mvarPolizasValidas )
      {
        mvarPasoSiguiente = "1";
      }
      else
      {
        //
        if( mvarVIAINSCR.equals( "OV" ) )
        {
          mvarPasoSiguiente = "ERR_OV";
        }
        else
        {
          mvarPasoSiguiente = "E";
        }
        //
      }
      //
      wobjXMLResponseAIS = null;
    }
    //
    fncAnalizarCAI = mvarPasoSiguiente;
    mobjXMLResponse = null;
    //
    return fncAnalizarCAI;
  }

  private Variant fncBorrarTemporales() throws Exception
  {
    Variant fncBorrarTemporales = new Variant();
    String mvarDIA_HOY = "";
    String mvarMES_HOY = "";
    int mvarANO_HOY = 0;
    String mvarFECHA_HOY = "";
    String mvarDIA_FILE = "";
    String mvarMES_FILE = "";
    int mvarANO_FILE = 0;
    String mvarFECHA_FILE = "";
    XmlDomExtended wobjXMLNBWS_TempFilesServer = null;
    Object mvarFSO = null;
    java.io.File mvarFOLDER = null;
    java.io.File[] mvarFILES = null;
    java.io.File mvarFILE = null;




    //Levanta XML de configuraci�n donde se indica la ruta de grabaci�n de archivos temporales.
    wobjXMLNBWS_TempFilesServer = new XmlDomExtended();
    wobjXMLNBWS_TempFilesServer.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.wcteNBWS_TempFilesServer));
    //
    mvarFSO = new Object();

    mvarFOLDER = new java.io.File( XmlDomExtended .getText( wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" )  ) );
    mvarFILES = mvarFOLDER.listFiles();

    mvarDIA_HOY = Strings.right( "00" + DateTime.day( DateTime.now() ), 2 );
    mvarMES_HOY = Strings.right( "00" + DateTime.month( DateTime.now() ), 2 );
    mvarANO_HOY = DateTime.year( DateTime.now() );

    mvarFECHA_HOY = mvarANO_HOY + mvarMES_HOY + mvarDIA_HOY;

    for( int nmvarFILE = 0; nmvarFILE < mvarFILES.length; nmvarFILE++ )
    {
      mvarFILE = mvarFILES[nmvarFILE];
      //
      mvarDIA_FILE = Strings.right( "00" + DateTime.day( new java.util.Date( mvarFILE.lastModified() ) ), 2 );
      mvarMES_FILE = Strings.right( "00" + DateTime.month( new java.util.Date( mvarFILE.lastModified() ) ), 2 );
      mvarANO_FILE = DateTime.year( new java.util.Date( mvarFILE.lastModified() ) );
      //
      mvarFECHA_FILE = mvarANO_FILE + mvarMES_FILE + mvarDIA_FILE;
      //
      if( (Obj.toInt( mvarFECHA_HOY ) - Obj.toInt( mvarFECHA_FILE )) > 1 )
      {
        FileSystem.kill( XmlDomExtended .getText( /*unsup wobjXMLNBWS_TempFilesServer.selectSingleNode( "//PATH" ) */ ) + "\\" + mvarFILE.getName() );
      }
      //
    }
    //
    wobjXMLNBWS_TempFilesServer = null;
    mvarFSO = (Object) null;
    mvarFOLDER = (java.io.File) null;
    mvarFILES = (java.io.File[]) null;
    mvarFILE = (java.io.File) null;

    return fncBorrarTemporales;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
