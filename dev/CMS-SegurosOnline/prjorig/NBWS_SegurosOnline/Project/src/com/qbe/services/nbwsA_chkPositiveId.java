package com.qbe.services.segurosOnline.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.segurosOnline.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class nbwsA_chkPositiveId implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "nbwsA_Transacciones.nbwsA_chkPositiveId";
  static final String mcteParam_USUARIO = "//USUARIO";
  static final String mcteParam_PRODUCTO = "//PRODUCTO";
  static final String mcteParam_DATO1 = "//DATO1";
  static final String mcteParam_DATO2 = "//DATO2";
  static final String mcteParam_DATO3 = "//DATO3";
  static final String mcteParam_DATO4 = "//DATO4";
  /**
   * Constantes de XML de definiciones para SQL Generico
   */
  static final String mcteParam_XMLSQLGENVALUSR = "P_NBWS_ValidaUsuario.xml";
  static final String mcteParam_XMLSQLGENOBTIDN = "P_NBWS_ObtenerIdentificador.xml";
  static final int mcteMsg_OK = 0;
  static final int mcteMsg_USRNOEXISTE = 1;
  static final int mcteMsg_DATOSINVALIDOS = 2;
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * Constantes de error (los n�meros matchean los de la tabla SQL tipoAccesos)
   */
  private String[] mcteMsg_DESCRIPTION = new String[13];
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    XmlDomExtended wobjXMLRequest = null;
    String wobjRequestSQL = "";
    String wobjResponseSQL = "";
    String wobjRequestAIS = "";
    String wobjResponseAIS = "";
    String wvarUsuario = "";
    String wvarProducto = "";
    String wvarDato1 = "";
    String wvarDato2 = "";
    String wvarDato3 = "";
    String wvarDato4 = "";
    int wvarError = 0;
    Variant wvarEstadoIdentificador = new Variant();
    Variant wvarEstadoPassword = new Variant();
    String wvarEstadoUsr = "";
    boolean wvarResChk = false;
    //
    //XML con el request
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializacion de variables
      wvarStep = 10;
      wvarError = mcteMsg_OK;
      wvarResChk = false;
      //
      //Definicion de mensajes de error
      wvarStep = 15;
      mcteMsg_DESCRIPTION[mcteMsg_OK] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_OK" );
      mcteMsg_DESCRIPTION[mcteMsg_USRNOEXISTE] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_USRNOEXISTE" );
      mcteMsg_DESCRIPTION[mcteMsg_DATOSINVALIDOS] = ModGeneral.getMensaje( mcteClassName, "mcteMsg_DATOSINVALIDOS" );
      //
      //Carga del REQUEST
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //Obtiene parametros
      wvarStep = 40;
      wvarStep = 50;
      wvarUsuario = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarStep = 60;
      wvarProducto = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PRODUCTO )  );
      wvarStep = 70;
      wvarDato1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO1 )  );
      wvarStep = 80;
      wvarDato2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO2 )  );
      wvarStep = 90;
      wvarDato3 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO3 )  );
      wvarStep = 100;
      wvarDato4 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATO4 )  );
      //
      //1) Validar existencia de usuario
      wvarStep = 150;
      wvarEstadoUsr = invoke( "fncValidarUsuario", new Variant[] { new Variant(wvarUsuario), new Variant(wvarEstadoIdentificador), new Variant(wvarEstadoPassword) } );
      if( wvarEstadoUsr.equals( "ERR" ) )
      {
        wvarError = mcteMsg_USRNOEXISTE;
      }
      else
      {
        wvarError = mcteMsg_OK;
      }
      //
      wvarStep = 190;
      if( wvarError == mcteMsg_OK )
      {
        //
        wvarResChk = invoke( "fncChkPositiveId", new Variant[] { new Variant(wvarUsuario), new Variant(wvarProducto), new Variant(wvarDato1), new Variant(wvarDato2), new Variant(wvarDato3), new Variant(wvarDato4) } );
        //
        if( wvarResChk )
        {
          wvarStep = 200;
          pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>OK</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
        }
        else
        {
          wvarError = mcteMsg_DATOSINVALIDOS;
          wvarStep = 210;
          pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
        }
      }
      else
      {
        wvarStep = 220;
        pvarResponse.set( "<Response>" + "<Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " estado=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + "<CODRESULTADO>ERR</CODRESULTADO>" + "<CODERROR>" + wvarError + "</CODERROR>" + "<MSGRESULTADO>" + mcteMsg_DESCRIPTION[wvarError] + "</MSGRESULTADO>" + "</Response>" );
      }
      //
      //Finaliza y libera objetos
      wvarStep = 500;
      wobjXMLRequest = null;
      //
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        //
        wobjXMLRequest = null;
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String fncValidarUsuario( String pvarUsuario, Variant pvarEstadoIdentificador, Variant pvarEstadoPassword ) throws Exception
  {
    String fncValidarUsuario = "";
    String mvarRequest = "";
    String mvarResponse = "";
    String mvarRetVal = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENVALUSR + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        //
        if( ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "S" )) || ( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//EXISTE" )  ).equals( "N" )) )
        {
          mvarRetVal = XmlDomExtended.getText( mobjXMLResponse.selectSingleNode( "//ESTADO" )  );
          pvarEstadoIdentificador.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOIDENTIFICADOR" )  ) );
          pvarEstadoPassword.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//ESTADOPASSWORD" )  ) );
        }
      }
    }
    //
    if( mvarRetVal.equals( "" ) )
    {
      mvarRetVal = "ERR";
    }
    //
    fncValidarUsuario = mvarRetVal;
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncValidarUsuario;
  }

  private boolean fncChkPositiveId( String pvarUsuario, String pvarProducto, String pvarDato1, String pvarDato2, String pvarDato3, String pvarDato4 ) throws Exception
  {
    boolean fncChkPositiveId = false;
    String mvarRequest = "";
    String mvarResponse = "";
    Object mobjClass = null;
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.NodeList wobjNodeList = null;
    boolean mvarRetVal = false;
    Variant mvarDOCUMTIP = new Variant();
    Variant mvarDOCUMDAT = new Variant();
    Variant mvarRCCPrevio = new Variant();
    Variant wArrProducto = new Variant();
    String mvarRAMOPCOD = "";
    String mvarPOLIZANN = "";
    String mvarPOLIZSEC = "";
    String mvarCERTIPOL = "";
    String mvarCERTIANN = "";
    String mvarCERTISEC = "";
    String wvarFiltro = "";
    String mvarDato1 = "";
    String mvarDato2 = "";
    String mvarDato3 = "";
    String mvarDato4 = "";
    //
    //
    mvarRetVal = false;
    //
    invoke( "fncObtenerIdentificador", new Variant[] { new Variant(pvarUsuario), new Variant(mvarDOCUMTIP), new Variant(mvarDOCUMDAT), new Variant(mvarRCCPrevio) } );
    //
    mvarRequest = "<Request>" + "<DOCUMTIP>" + mvarDOCUMTIP + "</DOCUMTIP>" + "<DOCUMDAT>" + mvarDOCUMDAT + "</DOCUMDAT>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_sInicio();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    wobjXMLResponse = new XmlDomExtended();
    wobjXMLResponse.loadXML( mvarResponse );
    //
    //Replica los pasos inversos de nbwsA_Ingresar
    //
    wArrProducto.set( Strings.split( pvarProducto, "|", -1 ) );
    mvarRAMOPCOD = wArrProducto.getValueAt( 0 ).toString();
    mvarPOLIZANN = wArrProducto.getValueAt( 1 ).toString();
    mvarPOLIZSEC = wArrProducto.getValueAt( 2 ).toString();
    mvarCERTIPOL = wArrProducto.getValueAt( 3 ).toString();
    mvarCERTIANN = wArrProducto.getValueAt( 4 ).toString();
    mvarCERTISEC = wArrProducto.getValueAt( 5 ).toString();
    //
    //Arma el query XPATH
    //
    if( ! (new Variant( pvarDato1 ).isNumeric()) )
    {
      mvarDato1 = "'" + Strings.toUpperCase( pvarDato1 ) + "'";
    }
    else
    {
      mvarDato1 = pvarDato1;
    }
    if( ! (new Variant( pvarDato2 ).isNumeric()) )
    {
      mvarDato2 = "'" + Strings.toUpperCase( pvarDato2 ) + "'";
    }
    else
    {
      mvarDato2 = pvarDato2;
    }
    if( ! (new Variant( pvarDato3 ).isNumeric()) )
    {
      mvarDato3 = "'" + Strings.toUpperCase( pvarDato3 ) + "'";
    }
    else
    {
      mvarDato3 = pvarDato3;
    }
    if( ! (new Variant( pvarDato4 ).isNumeric()) )
    {
      mvarDato4 = "'" + Strings.toUpperCase( pvarDato4 ) + "'";
    }
    else
    {
      mvarDato4 = pvarDato4;
    }
    //
    wvarFiltro = "//PRODUCTO[RAMOPCOD='" + mvarRAMOPCOD + "'" + " and POLIZANN='" + mvarPOLIZANN + "'" + " and POLIZSEC='" + mvarPOLIZSEC + "'" + " and CERTIPOL='" + mvarCERTIPOL + "'" + " and CERTIANN='" + mvarCERTIANN + "'" + " and CERTISEC='" + mvarCERTISEC + "'" + " and DATO1=" + mvarDato1 + " and DATO2=" + mvarDato2 + " and DATO3=" + mvarDato3 + " and DATO4=" + mvarDato4 + "]";
    //
    if( ! (wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        wobjNodeList = wobjXMLResponse.selectNodes( wvarFiltro ) ;
        if( wobjNodeList.getLength() > 0 )
        {
          mvarRetVal = true;
        }
      }
    }
    //
    fncChkPositiveId = mvarRetVal;
    //
    return fncChkPositiveId;
  }

  private String fncObtenerIdentificador( String pvarUsuario, Variant pvarDocumtip, Variant pvarDocumdat, Variant pvarRCCPrevio ) throws Exception
  {
    String fncObtenerIdentificador = "";
    String mvarRequest = "";
    String mvarResponse = "";
    XmlDomExtended mobjXMLResponse = null;
    Object mobjClass = null;
    //
    //
    mvarRequest = "<Request>" + "<DEFINICION>" + mcteParam_XMLSQLGENOBTIDN + "</DEFINICION>" + "<MAIL>" + pvarUsuario + "</MAIL>" + "</Request>";
    //
    mobjClass = new nbwsA_Transacciones.nbwsA_SQLGenerico();
    //error: function 'Execute' was not found.
    //unsup: Call mobjClass.Execute(mvarRequest, mvarResponse, "")
    mobjClass = null;
    //
    mobjXMLResponse = new XmlDomExtended();
    mobjXMLResponse.loadXML( mvarResponse );
    //
    //Analiza resultado del SP
    if( ! (mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        pvarDocumtip.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//DOCUMTIP" )  ) );
        pvarDocumdat.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//DOCUMDAT" )  ) );
        pvarRCCPrevio.set( XmlDomExtended .getText( mobjXMLResponse.selectSingleNode( "//RCCULTIMO" )  ) );
      }
    }
    //
    if( pvarDocumdat.toString().equals( "" ) )
    {
      pvarDocumdat.set( "ERR" );
    }
    //
    mobjClass = null;
    mobjXMLResponse = null;
    //
    return fncObtenerIdentificador;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
