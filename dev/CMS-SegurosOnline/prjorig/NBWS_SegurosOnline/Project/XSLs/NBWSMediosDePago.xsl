<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: NBWSMediosDePago.xsl

	Fecha de Creación: 31/07/2009
	
	PPcR: 50055/6010662
	
	Desarrollador: Leonardo Ruiz
	
	Descripción: XSL asociado a los Medios de Pagos
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/">
		<OPTIONS>
			<xsl:for-each select="//MEDIO_PAGO">
				<xsl:sort select="./MEDIO_PAGO_DES" order="ascending" data-type="text"/>
				<OPTION>
					<xsl:attribute name="value"><xsl:value-of select="MEDIO_PAGO_COD"/></xsl:attribute>
					<xsl:value-of select="MEDIO_PAGO_DES"/>
				</OPTION>
			</xsl:for-each>
		</OPTIONS>
	</xsl:template>
</xsl:stylesheet>

