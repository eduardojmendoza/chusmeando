package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetNroCot implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_GetNroCot";
  static final String mcteStoreProc = "SPSNCV_NROCOT";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Variant gcteDB = new Variant();
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Recordset wrstDBResultDate = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wobjDBParm = new Parameter( "@ORIGEN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "1" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 60;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 70;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 80;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 90;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 100;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 120;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wobjDBCmd = new Command();
        //
        wvarStep = 64;
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( invoke( "p_GetSQLQuery", new Variant[] {} ) );
        wobjDBCmd.setCommandType( AdoConst.adCmdText );
        //
        wvarStep = 66;
        wrstDBResultDate = wobjDBCmd.execute();
        wrstDBResultDate.setActiveConnection( (Connection) null );
        //
        wvarStep = 70;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 80;
        /*unsup wrstDBResultDate.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 90;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSLDate", new Variant[] {} ) );
        //
        wvarStep = 100;
        wvarResult = "<ROW>" + wvarResult + Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" ) + "</ROW>";
        //
        wvarStep = 120;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;

        wvarStep = 130;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 140;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      wvarStep = 150;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 160;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 170;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 180;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 190;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 200;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NROCOT'><xsl:value-of select='@NROCOT' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private String p_GetXSLDate() throws Exception
  {
    String p_GetXSLDate = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FECHA_DIA'><xsl:value-of select='@FECHA_DIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FECHA_SIGUIENTE'><xsl:value-of select='@FECHA_SIGUIENTE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLDate = wvarStrXSL;
    return p_GetXSLDate;
  }

  private String p_GetSQLQuery() throws Exception
  {
    String p_GetSQLQuery = "";
    String wvarSQL = "";

    wvarSQL = "SET NOCOUNT ON" + System.getProperty("line.separator");
    wvarSQL = wvarSQL + "DECLARE @AUXDATE DATETIME" + System.getProperty("line.separator");
    wvarSQL = wvarSQL + "SELECT @AUXDATE = DATEADD(MONTH,1,GETDATE())" + System.getProperty("line.separator");
    wvarSQL = wvarSQL + "SELECT CONVERT(VARCHAR,GETDATE(),112) AS FECHA_DIA, CONVERT(VARCHAR,DATEPART(YEAR,@AUXDATE)) + RIGHT('0' + CONVERT(VARCHAR,DATEPART(MONTH,@AUXDATE)),2) + '01' AS FECHA_SIGUIENTE";

    p_GetSQLQuery = wvarSQL;
    return p_GetSQLQuery;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
