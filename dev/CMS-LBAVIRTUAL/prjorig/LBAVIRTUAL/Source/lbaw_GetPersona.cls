VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OfVirtualLBA.lbaw_GetPersona"
Const mcteStoreProc         As String = "SPSNCV_PROD_SIFMPERS_SELECT_ITEM"
Const mcteStoreProcDom      As String = "SPSNCV_PROD_SIFMDOMI_SELECT"
Const mcteStoreProcCta      As String = "SPSNCV_PROD_SIFMCUEN_SELECT"

'Parametros XML de Entrada
Const mcteParam_CLIENSEC    As String = "//CLIENSEC"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wrstDBResultDom     As ADODB.Recordset
    Dim wrstDBResultCta     As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCLIENSEC        As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 50
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 60
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 70
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 80
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 90
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 100
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 120
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        ' A partir de aca domicilios
        wvarStep = 130
        Set wobjDBCmd = CreateObject("ADODB.Command")
        '
        With wobjDBCmd
            Set .ActiveConnection = wobjDBCnn
            .CommandText = mcteStoreProcDom
            .CommandType = adCmdStoredProc
        End With
        '
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
        wobjDBParm.Precision = 9
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 140
        Set wrstDBResultDom = wobjDBCmd.Execute
        Set wrstDBResultDom.ActiveConnection = Nothing
        '
        wvarStep = 150
        If Not wrstDBResultDom.EOF Then
            '
            wvarStep = 160
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 180
            wrstDBResultDom.Save wobjXMLResponse, adPersistXML
            '
            wvarStep = 190
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSLDom())
            '
            wvarStep = 200
            wvarResult = wvarResult & "<DOMICILIOS>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</DOMICILIOS>"
            '
            wvarStep = 210
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        Else
            wvarStep = 230
            wvarResult = wvarResult & "<DOMICILIOS></DOMICILIOS>"
        End If
        '
        ' fin domicilios
        ' a partir de aca cuentas
        '
        wvarStep = 240
        Set wobjDBCmd = CreateObject("ADODB.Command")
        '
        With wobjDBCmd
            Set .ActiveConnection = wobjDBCnn
            .CommandText = mcteStoreProcCta
            .CommandType = adCmdStoredProc
        End With
        '
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
        wobjDBParm.Precision = 9
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 250
        Set wrstDBResultCta = wobjDBCmd.Execute
        Set wrstDBResultCta.ActiveConnection = Nothing
        '
        wvarStep = 260
        If Not wrstDBResultCta.EOF Then
            '
            wvarStep = 270
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 280
            wrstDBResultCta.Save wobjXMLResponse, adPersistXML
            '
            wvarStep = 290
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSLCta())
            '
            wvarStep = 300
            wvarResult = "<ROW>" & wvarResult & "<CUENTAS>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</CUENTAS>" & "</ROW>"
            '
            wvarStep = 310
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        Else
            wvarStep = 320
            wvarResult = "<ROW>" & wvarResult & "<CUENTAS></CUENTAS>" & "</ROW>"
        End If
        ' Fin Cuentas
        '
        wvarStep = 330
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 340
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 350
    Set wobjDBCmd = Nothing
    '
    wvarStep = 360
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 370
    Set wobjDBCnn = Nothing
    '
    wvarStep = 380
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 390
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 400
    Set wrstDBResult = Nothing
    '
    wvarStep = 410
    If Not wrstDBResultDom Is Nothing Then
        If wrstDBResultDom.State = adStateOpen Then wrstDBResultDom.Close
    End If
    '
    wvarStep = 420
    Set wrstDBResultDom = Nothing
    '
    wvarStep = 430
    If Not wrstDBResultCta Is Nothing Then
        If wrstDBResultCta.State = adStateOpen Then wrstDBResultCta.Close
    End If
    '
    wvarStep = 420
    Set wrstDBResultCta = Nothing
    '
    wvarStep = 430
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='PERSONA'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PAISSDES'><xsl:value-of select='@PAISSDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOCUMDAB'><xsl:value-of select='@DOCUMDAB' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENDES'><xsl:value-of select='@CLIENDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENAP1'><xsl:value-of select='@CLIENAP1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENAP2'><xsl:value-of select='@CLIENAP2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENNOM'><xsl:value-of select='@CLIENNOM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMANN'><xsl:value-of select='@NACIMANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMMES'><xsl:value-of select='@NACIMMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMDIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSEX'><xsl:value-of select='@CLIENSEX' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENEST'><xsl:value-of select='@CLIENEST' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='IDIOMCOD'><xsl:value-of select='@IDIOMCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMHIJOS'><xsl:value-of select='@NUMHIJOS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EFECTANN'><xsl:value-of select='@EFECTANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EFECTMES'><xsl:value-of select='@EFECTMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EFECTDIA'><xsl:value-of select='@EFECTDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENTIP'><xsl:value-of select='@CLIENTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENFUM'><xsl:value-of select='@CLIENFUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENDOZ'><xsl:value-of select='@CLIENDOZ' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ABRIDTIP'><xsl:value-of select='@ABRIDTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ABRIDNUM'><xsl:value-of select='@ABRIDNUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENORG'><xsl:value-of select='@CLIENORG' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PERSOTIP'><xsl:value-of select='@PERSOTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENCLA'><xsl:value-of select='@CLIENCLA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FALLEANN'><xsl:value-of select='@FALLEANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FALLEMES'><xsl:value-of select='@FALLEMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FALLEDIA'><xsl:value-of select='@FALLEDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FBAJAANN'><xsl:value-of select='@FBAJAANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FBAJAMES'><xsl:value-of select='@FBAJAMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FBAJADIA'><xsl:value-of select='@FBAJADIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='USUARCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PAISSDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOCUMDAB'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENAP1'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENAP2'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENNOM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENSEX'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENEST'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENTIP'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENFUM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENDOZ'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ABRIDTIP'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ABRIDNUM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENORG'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PERSOTIP'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='EMAIL'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

Private Function p_GetXSLDom() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DOMICILIO'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICCAL'><xsl:value-of select='@DOMICCAL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICDOM'><xsl:value-of select='@DOMICDOM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICDNU'><xsl:value-of select='@DOMICDNU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICESC'><xsl:value-of select='@DOMICESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPIS'><xsl:value-of select='@DOMICPIS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPTA'><xsl:value-of select='@DOMICPTA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPOB'><xsl:value-of select='@DOMICPOB' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICCPO'><xsl:value-of select='@DOMICCPO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVICOD'><xsl:value-of select='@PROVICOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TELCOD'><xsl:value-of select='@TELCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TELNRO'><xsl:value-of select='@TELNRO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICSEC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICCAL'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICDOM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICDNU'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICESC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPIS'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPTA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPOB'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PAISSCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TELCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TELNRO'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDom = wvarStrXSL
End Function
Private Function p_GetXSLCta() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CUENTA'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPOCUEN'><xsl:value-of select='@TIPOCUEN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBROTIP'><xsl:value-of select='@COBROTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='BANCOCOD'><xsl:value-of select='@BANCOCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUCURCOD'><xsl:value-of select='@SUCURCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENTDC'><xsl:value-of select='@CUENTDC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENNUME'><xsl:value-of select='@CUENNUME' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TARJECOD'><xsl:value-of select='@TARJECOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='VENCIANN'><xsl:value-of select='@VENCIANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='VENCIMES'><xsl:value-of select='@VENCIMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='VENCIDIA'><xsl:value-of select='@VENCIDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='USUARCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TIPOCUEN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COBROTIP'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUENTDC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUENNUME'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLCta = wvarStrXSL
End Function




'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub








