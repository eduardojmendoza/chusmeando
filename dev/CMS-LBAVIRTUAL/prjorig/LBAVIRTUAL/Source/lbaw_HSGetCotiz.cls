VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_HSGetCotiz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OfVirtualLBA.lbaw_HSGetCotiz"
Const mcteStoreProc         As String = "SPSNCV_ADM_INTERNET_HOGAR_SELECT"

'Parametros XML de Entrada
Const mcteParam_EstadoOper  As String = "//ESTADOOPER"
Const mcteParam_Producto    As String = "//PRODUCTO"
Const mcteParam_Portal      As String = "//PORTAL"
Const mcteParam_FDesde      As String = "//FDESDE"
Const mcteParam_FHasta      As String = "//FHASTA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarEstadoOper      As String
    Dim wvarProducto        As String
    Dim wvarPortal          As String
    Dim wvarFDesde          As String
    Dim wvarFHasta          As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarEstadoOper = .selectSingleNode(mcteParam_EstadoOper).Text
        wvarProducto = .selectSingleNode(mcteParam_Producto).Text
        wvarPortal = .selectSingleNode(mcteParam_Portal).Text
        wvarFDesde = .selectSingleNode(mcteParam_FDesde).Text
        wvarFHasta = .selectSingleNode(mcteParam_FHasta).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADOOPER", adInteger, adParamInput, , wvarEstadoOper)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PRODUCTO", adChar, adParamInput, 4, wvarProducto)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PORTAL", adChar, adParamInput, 10, wvarPortal)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FDESDE", adVarChar, adParamInput, 10, wvarFDesde)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FHASTA", adVarChar, adParamInput, 10, wvarFHasta)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 50
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 60
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 70
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 80
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 90
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 100
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 120
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 130
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 140
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    wvarStep = 150
    Set wobjDBCmd = Nothing
    '
    wvarStep = 160
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 170
    Set wobjDBCnn = Nothing
    '
    wvarStep = 180
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 190
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 200
    Set wrstDBResult = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ROW'>"
    '
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COTIID'><xsl:value-of select='@COTIID' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FECHACOTI'><xsl:value-of select='@FECHACOTI' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADO_DESC'><xsl:value-of select='@ESTADO_DESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='APELLIDO_NOMBRE'><xsl:value-of select='@APELLIDO_NOMBRE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TELEFONO'><xsl:value-of select='@TELEFONO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='LOCALIDAD'><xsl:value-of select='@LOCALIDAD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='POSTACOD'><xsl:value-of select='@POSTACOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVIDES'><xsl:value-of select='@PROVIDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PORTAL'><xsl:value-of select='@PORTAL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PLANNCOD'><xsl:value-of select='@PLANNCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIVIVDES'><xsl:value-of select='@TIVIVDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBERCOD1'><xsl:value-of select='@COBERCOD1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMERMOD1'><xsl:value-of select='@NUMERMOD1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBERCOD2'><xsl:value-of select='@COBERCOD2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMERMOD2'><xsl:value-of select='@NUMERMOD2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBERCOD3'><xsl:value-of select='@COBERCOD3' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMERMOD3'><xsl:value-of select='@NUMERMOD3' /></xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COTIID'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FECHACOTI'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESTADO_DESC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='APELLIDO_NOMBRE'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TELEFONO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LOCALIDAD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='POSTACOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROVIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PORTAL'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='RAMOPCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PLANNCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TIVIVDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COBERCOD1'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NUMERMOD1'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COBERCOD2'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NUMERMOD2'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COBERCOD3'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NUMERMOD3'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub










