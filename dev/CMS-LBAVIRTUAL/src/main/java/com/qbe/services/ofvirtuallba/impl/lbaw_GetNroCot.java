package com.qbe.services.ofvirtuallba.impl;

import java.sql.CallableStatement;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Variant;

/**
 * Objetos del FrameWork
 */

public class lbaw_GetNroCot implements VBObjectClass {

	private static Logger logger = Logger.getLogger(lbaw_GetNroCot.class.getName());

	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_GetNroCot";
	static final String mcteStoreProc = "SPSNCV_NROCOT";
	private Object mobjCOM_Context = null;
	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLResponse = null;
		XmlDomExtended wobjXSLResponse = null;
		int wvarStep = 0;
		String wvarResult = "";
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			java.sql.Connection jdbcConn = null;
			try {
				JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
				jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
				String sp = "{call " + mcteStoreProc + "(?)}";
				CallableStatement ps = jdbcConn.prepareCall(sp);
				int posiSP = 1;

				ps.setString(posiSP++, String.valueOf("1"));

				//
				wvarStep = 50;

				boolean result = ps.execute();

				if (result) {
					// El primero es un ResultSet
				} else {
					// Salteo el "updateCount"
					int uc = ps.getUpdateCount();
					if (uc == -1) {
						// System.out.println("No hay resultados");
					}
					result = ps.getMoreResults();
				}
				//
				wvarStep = 60;
				java.sql.ResultSet cursor = ps.getResultSet();
				if (cursor.next()) {
					//
					wvarStep = 70;
					wobjXMLResponse = new XmlDomExtended();
					wobjXSLResponse = new XmlDomExtended();
					//
					wvarStep = 80;
					wobjXMLResponse.load(AdoUtils.saveResultSet(cursor));

					//
					wvarStep = 90;
					wobjXSLResponse.loadXML(p_GetXSL());
					//
					wvarStep = 100;
					wvarResult = wobjXMLResponse.transformNode(wobjXSLResponse).toString()
							.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "");
					//
					wvarStep = 120;
					wobjXMLResponse = null;
					wobjXSLResponse = null;
					//
					// wobjDBCmd = new Command();
					//
					wvarStep = 64;
					// Tuvimos que implemenar esto porque el comentado ejecuta
					// executeUpdate().
					// Consultamos a diamondedge pero no vino nada conclusivo
					// wobjDBCmd.setActiveConnection( wobjDBCnn);
					// wobjDBCmd.setCommandText(p_GetSQLQuery());
					// wobjDBCmd.setCommandType( AdoConst.adCmdText);
					Statement stmt = jdbcConn.createStatement();
					boolean stmtResult = stmt.execute(p_GetSQLQuery());
					java.sql.ResultSet rs;
					if (stmtResult) {
						rs = stmt.getResultSet();
					} else {
						throw new ComponentExecutionException("No se pudo ejecutar el p_GetSQLQuery()");
					}

					wvarStep = 70;
					wobjXMLResponse = new XmlDomExtended();
					wobjXSLResponse = new XmlDomExtended();
					//
					wvarStep = 80;
					rs.next();
					wobjXMLResponse.load(AdoUtils.saveResultSet(rs));
					stmt.close();
					//
					wvarStep = 90;
					wobjXSLResponse.loadXML(p_GetXSLDate());
					//
					wvarStep = 100;
					wvarResult = "<ROW>" + wvarResult + wobjXMLResponse.transformNode(wobjXSLResponse).toString()
							.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "") + "</ROW>";
					//
					wvarStep = 120;
					wobjXMLResponse = null;
					wobjXSLResponse = null;

					wvarStep = 130;
					Response.set("<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>");
					logger.log(Level.INFO, Response.getValue());
				} else {
					wvarStep = 140;
					Response.set("<Response><Estado resultado=\"false\" mensaje=" + String.valueOf((char) (34))
							+ "NO SE ENCONTRARON DATOS.\" /></Response>");
				}
			} finally {
				try {
					if (jdbcConn != null)
						jdbcConn.close();
				} catch (Exception e) {
					logger.log(Level.WARNING, "Exception al hacer un close", e);
				}
			}
			IAction_Execute = 0;
			return IAction_Execute;
		} catch (Exception e) {
			logger.log(Level.SEVERE, this.getClass().getName(), e);
			throw new ComponentExecutionException(e);
		}
	}

	private String p_GetXSL() throws Exception {
		String p_GetXSL = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL
				+ "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
		wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
		wvarStrXSL = wvarStrXSL + "      <xsl:element name='NROCOT'><xsl:value-of select='@NROCOT' /></xsl:element>";
		wvarStrXSL = wvarStrXSL + " </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSL = wvarStrXSL;
		return p_GetXSL;
	}

	private String p_GetXSLDate() throws Exception {
		String p_GetXSLDate = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL
				+ "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
		wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
		wvarStrXSL = wvarStrXSL
				+ "      <xsl:element name='FECHA_DIA'><xsl:value-of select='@FECHA_DIA' /></xsl:element>";
		wvarStrXSL = wvarStrXSL
				+ "      <xsl:element name='FECHA_SIGUIENTE'><xsl:value-of select='@FECHA_SIGUIENTE' /></xsl:element>";
		wvarStrXSL = wvarStrXSL + " </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSLDate = wvarStrXSL;
		return p_GetXSLDate;
	}

	private String p_GetSQLQuery() throws Exception {
		String p_GetSQLQuery = "";
		String wvarSQL = "";

		// Estos 4 van comentados para probar en DEV
		wvarSQL = "SET NOCOUNT ON" + System.getProperty("line.separator");
		wvarSQL = wvarSQL + "DECLARE @AUXDATE DATETIME" + System.getProperty("line.separator");
		wvarSQL = wvarSQL + "SELECT @AUXDATE = DATEADD(MONTH,1,GETDATE())" + System.getProperty("line.separator");
		wvarSQL = wvarSQL
				+ "SELECT CONVERT(VARCHAR,GETDATE(),112) AS FECHA_DIA, CONVERT(VARCHAR,DATEPART(YEAR,@AUXDATE)) + RIGHT('0' + CONVERT(VARCHAR,DATEPART(MONTH,@AUXDATE)),2) + '01' AS FECHA_SIGUIENTE";

		// Este va cometnado para TEST
		// wvarSQL = wvarSQL + "SELECT '20130702' AS FECHA_DIA, '20130801' AS
		// FECHA_SIGUIENTE FROM (VALUES(0))";
		p_GetSQLQuery = wvarSQL;
		return p_GetSQLQuery;
	}

	private void ObjectControl_Activate() throws Exception {
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	/**
	 * -------------------------------------------------------------------------
	 * ------------------------------------------ // Metodos del Framework
	 * -------------------------------------------------------------------------
	 * ------------------------------------------
	 */
	public void Activate() throws Exception {
		//
		/* TBD mobjCOM_Context = this.GetObjectContext() ; */
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
		mobjCOM_Context = (Object) null;
		mobjEventLog = null;
		//
	}
}
