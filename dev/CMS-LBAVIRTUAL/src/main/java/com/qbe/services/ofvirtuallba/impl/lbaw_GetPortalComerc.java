package com.qbe.services.ofvirtuallba.impl;
import java.sql.CallableStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetPortalComerc implements VBObjectClass
{
	
	private static Logger logger = Logger.getLogger(lbaw_GetPortalComerc.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_GetPortalComerc";
  static final String mcteStoreProc = "SPSNCV_REL_PORTAL_COMERC_SELECT";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_PORTAL = "//PORTAL";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarPORTAL = "";
    String wvarRAMOPCOD = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarPORTAL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PORTAL )  );
      wvarRAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      //
      wobjXMLRequest = null;
      //
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wvarStep = 40;

      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{call " + mcteStoreProc + "(?, ?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;

      ps.setString(posiSP++, String.valueOf(wvarPORTAL));
      //
      
      ps.setString(posiSP++, String.valueOf(wvarRAMOPCOD));
      wvarStep = 50;
      
      boolean result = ps.execute();
      
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }


      java.sql.ResultSet cursor = ps.getResultSet();
      if( cursor.next())
      {
        //
        wvarStep = 70;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 80;
        wobjXMLResponse.load(AdoUtils.saveResultSet(cursor));
        //
        wvarStep = 90;
        wobjXSLResponse.loadXML( p_GetXSL());
        //
        wvarStep = 100;
        wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
        //
        wvarStep = 120;
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        wvarStep = 130;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 140;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
		Response.set( "<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>" );
		return 3;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ROW'>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CAMPACOD'><xsl:value-of select='@CAMPACOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AGECOD'><xsl:value-of select='@AGECOD' /></xsl:element>";
    //MMC - 2012-11-28
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AGECLA'><xsl:value-of select='@AGECLA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TELEFONO'><xsl:value-of select='@TELNRO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CAMPACOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AGECOD'/>";
    //MMC - 2012-11-28
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AGECLA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TELNRO'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
