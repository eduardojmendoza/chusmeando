package com.qbe.integration.ovbpm.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;


@XmlType( name = "RetrieveReportListResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveReportListResponse extends BPMOVResponse {

	@XmlElementWrapper(name = "documentList")
	@XmlElement(required=true)
	protected List<DescripcionArchivo> item = new ArrayList<DescripcionArchivo>();

	public List<DescripcionArchivo> getItem() {
		return item;
	}

	public void setItem(List<DescripcionArchivo> item) {
		this.item = item;
	}
}
