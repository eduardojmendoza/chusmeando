package com.qbe.integration.ovbpm.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType( name = "PublishReportResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class PublishReportResponse extends BPMOVResponse {
}
