package com.qbe.integration.ovbpm.types;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class PublishReportRequest {

//    <element name="id" type="xsd:string"/>  //id orden de emisión
	@XmlElement(required=true)
	protected String id;
	
//    <element name="reportName" type="xsd:string"/> 
	@XmlElement(required=true)
	protected String reportName;
	
//    <element name="report" type="xsd:string"/> el PDF en base64
	@XmlElement(required=true)
	protected byte[] reportContent;
		
//	<element minOccurs="1" maxOccurs="1" name="tipoOperacion" type="xsd:integer"/>
	@XmlElement(required=true)
	protected BigInteger tipoOperacion;
	
//	<element minOccurs="1" maxOccurs="1" name="tipoDocumentoCliente" type="xsd:integer"/>
	@XmlElement(required=true)
	protected BigInteger tipoDocumentoCliente;
	
//	<element minOccurs="1" maxOccurs="1" name="numeroDocumentoCliente" type="xsd:integer"/>
	@XmlElement(required=true)
	protected BigInteger numeroDocumentoCliente;

//	<element minOccurs="1" maxOccurs="1" name="codProductor" type="xsd:integer"/>
	@XmlElement(required=true)
	protected BigInteger codigoProductor;
	
//	<element minOccurs="1" maxOccurs="1" name="claseProductor" type="xsd:string"/>
	@XmlElement(required=true)
	protected String claseProductor;
	
//	<element minOccurs="1" maxOccurs="1" name="codProducto" type="xsd:string"/>
	@XmlElement(required=true)
	protected String codigoProducto;

//	<element minOccurs="1" maxOccurs="1" name="fechaOE" type="xsd:date"/>
	@XmlElement(required=true)
	protected Date fechaOE;
	
//	<element minOccurs="0" maxOccurs="1" name="numeroDePoliza" type="xsd:integer"/>
	@XmlElement(required=false)
	protected BigInteger numeroDePoliza;

//	<element minOccurs="0" maxOccurs="1" name="tipoEndoso" type="xsd:integer"/>
	@XmlElement(required=false)
	protected BigInteger tipoEndoso;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public byte[] getReportContent() {
		return reportContent;
	}

	public void setReportContent(byte[] reportContent) {
		this.reportContent = reportContent;
	}

	public BigInteger getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(BigInteger tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public BigInteger getTipoDocumentoCliente() {
		return tipoDocumentoCliente;
	}

	public void setTipoDocumentoCliente(BigInteger tipoDocumentoCliente) {
		this.tipoDocumentoCliente = tipoDocumentoCliente;
	}

	public BigInteger getNumeroDocumentoCliente() {
		return numeroDocumentoCliente;
	}

	public void setNumeroDocumentoCliente(BigInteger numeroDocumentoCliente) {
		this.numeroDocumentoCliente = numeroDocumentoCliente;
	}

	public BigInteger getCodProductor() {
		return codigoProductor;
	}

	public void setCodProductor(BigInteger codProductor) {
		this.codigoProductor = codProductor;
	}

	public String getClaseProductor() {
		return claseProductor;
	}

	public void setClaseProductor(String claseProductor) {
		this.claseProductor = claseProductor;
	}

	public String getCodProducto() {
		return codigoProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codigoProducto = codProducto;
	}

	public Date getFechaOE() {
		return fechaOE;
	}

	public void setFechaOE(Date fechaOE) {
		this.fechaOE = fechaOE;
	}

	public BigInteger getNumeroDePoliza() {
		return numeroDePoliza;
	}

	public void setNumeroDePoliza(BigInteger numeroDePoliza) {
		this.numeroDePoliza = numeroDePoliza;
	}

	public BigInteger getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(BigInteger tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public BigInteger getCodigoProductor() {
		return codigoProductor;
	}

	public void setCodigoProductor(BigInteger codigoProductor) {
		this.codigoProductor = codigoProductor;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

}
