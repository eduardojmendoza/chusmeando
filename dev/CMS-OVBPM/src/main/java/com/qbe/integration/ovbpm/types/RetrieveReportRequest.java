package com.qbe.integration.ovbpm.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveReportRequest {

	//  <element name="id" type="xsd:string"/>  //id orden de emisión
		@XmlElement(required=true)
		protected String id;
		
//	      <element name="fileId" type="xsd:string"/>
		@XmlElement(required=true)
		protected String fileId;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getFileId() {
			return fileId;
		}

		public void setFileId(String fileId) {
			this.fileId = fileId;
		}
		
}
