/**
 * 
 */
package com.qbe.integration.ovbpm;

import java.util.Calendar;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.qbe.integration.ovbpm.types.DescripcionArchivo;
import com.qbe.integration.ovbpm.types.OVResponseMeta;
import com.qbe.integration.ovbpm.types.OrderIdRequest;
import com.qbe.integration.ovbpm.types.PublishReportRequest;
import com.qbe.integration.ovbpm.types.PublishReportResponse;
import com.qbe.integration.ovbpm.types.Retencion;
import com.qbe.integration.ovbpm.types.RetrieveHoldStatusResponse;
import com.qbe.integration.ovbpm.types.RetrieveReportListResponse;
import com.qbe.integration.ovbpm.types.RetrieveReportRequest;
import com.qbe.integration.ovbpm.types.RetrieveReportResponse;

/**
 * @author ramiro
 *
 */
@WebService(endpointInterface = "com.qbe.integration.ovbpm.BPMOVServices")
public class BPMOVServicesImpl implements BPMOVServices {

	/* (non-Javadoc)
	 * @see com.qbe.integration.ovbpm.BPMOVServices#publishReport(com.qbe.integration.ovbpm.PublishReportRequest)
	 */
	@Override
	public PublishReportResponse publishReport(@WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") PublishReportRequest request) {
		PublishReportResponse response = new PublishReportResponse();
		response.setResponse(getOKMeta());

		return response;
	}

	protected OVResponseMeta getOKMeta() {
		OVResponseMeta meta = new OVResponseMeta();
		meta.setCodigo("0");
		return meta;
	}

	/* (non-Javadoc)
	 * @see com.qbe.integration.ovbpm.BPMOVServices#retrieveReportList()
	 */
	@Override
	public RetrieveReportListResponse retrieveReportList ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") OrderIdRequest request) {
		RetrieveReportListResponse response = new RetrieveReportListResponse();
		response.setResponse(getOKMeta());
		response.getItem().add(getSampleDescripcionArchivo());
		return response;
	}

	protected DescripcionArchivo getSampleDescripcionArchivo() {
		DescripcionArchivo desc1 = new DescripcionArchivo();
		desc1.setFileId("ID_ARCHIVO");
		desc1.setNombre("NOMBRE_ARCHIVO");
		return desc1;
	}

	/* (non-Javadoc)
	 * @see com.qbe.integration.ovbpm.BPMOVServices#retrieveReport()
	 */
	@Override
	public RetrieveReportResponse retrieveReport ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") RetrieveReportRequest request) {
		RetrieveReportResponse response = new RetrieveReportResponse();
		response.setResponse(getOKMeta());
		response.setDescripcion(getSampleDescripcionArchivo());
		response.setReport(new byte[] { 1, 2, 3, 4, 5});
		return response;
	}

	/* (non-Javadoc)
	 * @see com.qbe.integration.ovbpm.BPMOVServices#retrieveHoldStatus()
	 */
	@Override
	public RetrieveHoldStatusResponse retrieveHoldStatus ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") OrderIdRequest request) {
		RetrieveHoldStatusResponse response = new RetrieveHoldStatusResponse();
		response.setResponse(getOKMeta());
		Retencion ret = new Retencion();
		ret.setEstado("Bloqueado");
		ret.setFechaAlta(Calendar.getInstance().getTime());
		ret.setMotivo("Falta fotocopia");
		ret.setSectorAsignado("Verificación");
		ret.setUltimaModificacion(Calendar.getInstance().getTime());
		response.getItem().add(ret);
		return response;
	}

}
