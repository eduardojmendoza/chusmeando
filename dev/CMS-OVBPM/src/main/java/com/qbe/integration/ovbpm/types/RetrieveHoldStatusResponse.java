package com.qbe.integration.ovbpm.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType( name = "RetrieveHoldStatusResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveHoldStatusResponse extends BPMOVResponse{

	@XmlElementWrapper(name = "retenciones")
	@XmlElement(required=true)
	private List<Retencion> item = new ArrayList<Retencion>();

	public List<Retencion> getItem() {
		return item;
	}

	public void setItem(List<Retencion> item) {
		this.item = item;
	}
}
