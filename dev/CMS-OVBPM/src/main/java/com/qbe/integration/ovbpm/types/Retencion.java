package com.qbe.integration.ovbpm.types;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Retencion {

//  <element name="estado" nillable="true" type="xsd:string"/>
	@XmlElement ( required = true)
	protected String estado;

//	<element name="motivo" nillable="true" type="xsd:string"/>
	@XmlElement ( required = true)
	protected String motivo;

//	<element name="sectorAsignado" nillable="true" type="xsd:string"/>
	@XmlElement ( required = true)
	protected String sectorAsignado;

//	<element name="fechaAlta" nillable="true" type="xsd:string"/>
	@XmlElement ( required = true)
	protected Date fechaAlta; //DD-MM-YYYY

//	<element name="ultimaModificacion" nillable="true" type="xsd:string"/>
	@XmlElement ( required = true)
	protected Date ultimaModificacion; //DD-MM-YYYY

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getSectorAsignado() {
		return sectorAsignado;
	}

	public void setSectorAsignado(String sectorAsignado) {
		this.sectorAsignado = sectorAsignado;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(Date ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

}
