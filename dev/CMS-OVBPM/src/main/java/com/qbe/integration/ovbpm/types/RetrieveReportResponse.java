package com.qbe.integration.ovbpm.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType( name = "RetrieveReportResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class RetrieveReportResponse extends BPMOVResponse{

	
//    <element name="descripcion" nillable="true" type="impl:DescArchivo"/>
	@XmlElement(required=true)
	protected DescripcionArchivo descripcion;
	
	// <element name="report" nillable="true" type="xsd:string"/> base64
	@XmlElement(required=true)
	protected byte[] report;

	public DescripcionArchivo getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(DescripcionArchivo descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getReport() {
		return report;
	}

	public void setReport(byte[] report) {
		this.report = report;
	}
 
}
