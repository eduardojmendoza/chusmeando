@javax.xml.bind.annotation.XmlSchema(
		elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
		namespace="http://ovbpm.integration.qbe.com/")
package com.qbe.integration.ovbpm.types;
