package com.qbe.integration.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VersionServlet
 */
public class VersionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VersionServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();

		// Write the response message, in an HTML document.
		try {
			out.println("<!DOCTYPE html>"); // HTML 5
			out.println("<html><head>");
			out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
			String title = "Manifest";
			out.println("<title>" + title + "</title></head>");
			out.println("<body>");
			out.println("<h1>" + title + "</h1>");

			ServletContext application = getServletConfig().getServletContext();
			InputStream inputStream = application.getResourceAsStream("/META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest(inputStream);
			Attributes attrs = manifest.getMainAttributes();
			for (Iterator<Object> it2 = attrs.keySet().iterator(); it2.hasNext();) {
				Attributes.Name attrName = (Attributes.Name) it2.next();
				String attrValue = attrs.getValue(attrName);
				out.println(attrName + ": " + attrValue);
				out.println("<br/>");
			}
			out.println("</body></html>");
		} finally {
			out.close(); // Always close the output writer
		}
	}

}
