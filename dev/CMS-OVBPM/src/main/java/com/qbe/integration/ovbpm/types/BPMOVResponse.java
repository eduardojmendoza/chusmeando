package com.qbe.integration.ovbpm.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class BPMOVResponse {

	@XmlElement(required = true)
	protected OVResponseMeta response;

	public BPMOVResponse() {
		super();
	}

	public OVResponseMeta getResponse() {
		return response;
	}

	public void setResponse(OVResponseMeta response) {
		this.response = response;
	}

}