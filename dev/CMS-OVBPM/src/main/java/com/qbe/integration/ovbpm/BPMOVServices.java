package com.qbe.integration.ovbpm;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.qbe.integration.ovbpm.types.OrderIdRequest;
import com.qbe.integration.ovbpm.types.PublishReportRequest;
import com.qbe.integration.ovbpm.types.PublishReportResponse;
import com.qbe.integration.ovbpm.types.RetrieveHoldStatusResponse;
import com.qbe.integration.ovbpm.types.RetrieveReportListResponse;
import com.qbe.integration.ovbpm.types.RetrieveReportRequest;
import com.qbe.integration.ovbpm.types.RetrieveReportResponse;

@WebService(
		name = "BPMOVServices",
		targetNamespace="http://ovbpm.integration.qbe.com/"
)
public interface BPMOVServices {

	/**
	 * Publica ( archiva ) un reporte en BPM
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public PublishReportResponse publishReport ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") PublishReportRequest request);
	

	/**
	 * Retorna la lista de archivos asociados a un Certificado de Emisión
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public RetrieveReportListResponse retrieveReportList ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") OrderIdRequest request);

	/**
	 * Retorna el contenido de un archivo
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public RetrieveReportResponse retrieveReport ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") RetrieveReportRequest request);
	
	/**
	 * Retorna el estado ( dentro del proceso ) de un ID de proceso en BPM
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public RetrieveHoldStatusResponse retrieveHoldStatus ( @WebParam(name="request", targetNamespace="http://ovbpm.integration.qbe.com/") OrderIdRequest request);
	
}
