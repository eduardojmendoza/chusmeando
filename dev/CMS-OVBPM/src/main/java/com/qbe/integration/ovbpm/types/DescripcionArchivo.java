package com.qbe.integration.ovbpm.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class DescripcionArchivo {
	
	/**
	 * ID del archivo, creado por BPM en el publishReport
	 */
	@XmlElement
	protected String fileId;
	
//    <element name="Nombre" nillable="true" type="xsd:string"/>
	@XmlElement
	protected String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	

}
