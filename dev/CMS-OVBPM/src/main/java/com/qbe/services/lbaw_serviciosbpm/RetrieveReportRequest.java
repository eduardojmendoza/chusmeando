package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RetrieveReportRequest", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class RetrieveReportRequest implements OVBPMRequest{

	/**
	 * retrieveReport
	 */
	@XmlElement(required = true, defaultValue="retrieveReport")
	protected String operation;

	/**
	 * ID del archivo, creado por BPM en el publishReport
	 */
		@XmlElement(required=true)
		protected String fileId;

		public String getFileId() {
			return fileId;
		}

		public void setFileId(String fileId) {
			this.fileId = fileId;
		}
		
}
