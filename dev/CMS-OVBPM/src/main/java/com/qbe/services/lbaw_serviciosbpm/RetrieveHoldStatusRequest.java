package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RetrieveHoldStatusRequest", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class RetrieveHoldStatusRequest implements OVBPMRequest{

	/**
	 * retrieveHoldStatus
	 */
	@XmlElement(required = true, defaultValue="retrieveHoldStatus")
	protected String operation;

	/**
	 * Id de bpm guardado en AIS 
	 */
		@XmlElement(required=true)
		protected String id;

}
