package com.qbe.services.lbaw_serviciosbpm;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "com.qbe.services.lbaw_serviciosbpm.LBAW_ServiciosBPM",
name = "LBAW_ServiciosBPM", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/"
)

public class LBAW_ServiciosBPMImpl implements LBAW_ServiciosBPM{

	public LBAW_ServiciosBPMImpl() {
	}

	@Override
	@WebMethod
	public OVBPMResponse lbaw_ServiciosBPM_publishReport(
			@WebParam(name = "Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/" ) PublishReportRequest request) {
		return null;
	}

	@Override
	@WebMethod
	public RetrieveReportListResponse lbaw_ServiciosBPM_retrieveReportList(
			@WebParam(name = "Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/") RetrieveReportListRequest request) {
		return null;
	}

	@Override
	@WebMethod
	public RetrieveReportResponse lbaw_ServiciosBPM_retrieveReport(
			@WebParam(name = "Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/") RetrieveReportRequest request) {
		return null;
	}

	@Override
	@WebMethod
	public RetrieveHoldStatusResponse lbaw_ServiciosBPM_retrieveHoldStatus(
			@WebParam(name = "Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/") RetrieveHoldStatusRequest request) {
		return null;
	}

}
