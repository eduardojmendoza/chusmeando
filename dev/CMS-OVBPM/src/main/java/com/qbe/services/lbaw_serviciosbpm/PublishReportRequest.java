package com.qbe.services.lbaw_serviciosbpm;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Request de la operación PublishReport enviado por OV
 * @author ramiro
 *
 */
@XmlType
@XmlRootElement(name = "PublishReportRequest", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
@XmlAccessorType(XmlAccessType.FIELD)
public class PublishReportRequest implements OVBPMRequest{

	/**
	 * publishReport
	 */
	@XmlElement(required = true, defaultValue="publishReport")
	protected String operation;

	/**
	 * Numero de Orden de Emision de la OV. En BPM se asocia al número de instancia generado.
	 */
	@XmlElement(required=true)
	protected String id;
	
	/**
	 * Nombre del reporte. Ejemplo SN_Autos1.PDF
	 */
	@XmlElement(required=true)
	protected String reportName;
	
	/**
	 * El contenido del reporte en base64, usualmente es un PDF
	 */ 
	@XmlElement(required=true)
	protected byte[] reportContent;
		
	/**
	 * Códigos propios de OV 
	 */
	@XmlElement(required=true)
	protected BigInteger tipoOperacion;
	
	/**
	 * Codificación de OV. Ver actionCode GetTiposDocumento
	 */
	@XmlElement(required=true)
	protected BigInteger tipoDocumentoCliente;
	
	/**
	 * Nro de documento
	 */
	@XmlElement(required=true)
	protected BigInteger numeroDocumentoCliente;

	/**
	 * 4 dígitos
	 */
	@XmlElement(required=true)
	protected BigInteger codigoProductor;
	
	/**
	 * Clase del productor. Usualmente PR, puede variar en el futuro 
	 */
	@XmlElement(required=true)
	protected String claseProductor;
	
	/**
	 * AUS1 etc
	 */
	@XmlElement(required=true)
	protected String codigoProducto;

	/**
	 * Fecha de creación de la OE. xs:date, así que formato YYYY-MM-DD
	 */
	@XmlElement(required=true)
	protected Date fechaOE;
	
	/**
	 * OV envía polizann(2) y polizsec(6) concatenados
	 */
	@XmlElement(required=false)
	protected BigInteger numeroDePoliza;

	/**
	 * Formato propio de OV
	 */
	@XmlElement(required=false)
	protected BigInteger tipoEndoso;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public byte[] getReportContent() {
		return reportContent;
	}

	public void setReportContent(byte[] reportContent) {
		this.reportContent = reportContent;
	}

	public BigInteger getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(BigInteger tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public BigInteger getTipoDocumentoCliente() {
		return tipoDocumentoCliente;
	}

	public void setTipoDocumentoCliente(BigInteger tipoDocumentoCliente) {
		this.tipoDocumentoCliente = tipoDocumentoCliente;
	}

	public BigInteger getNumeroDocumentoCliente() {
		return numeroDocumentoCliente;
	}

	public void setNumeroDocumentoCliente(BigInteger numeroDocumentoCliente) {
		this.numeroDocumentoCliente = numeroDocumentoCliente;
	}

	public BigInteger getCodProductor() {
		return codigoProductor;
	}

	public void setCodProductor(BigInteger codProductor) {
		this.codigoProductor = codProductor;
	}

	public String getClaseProductor() {
		return claseProductor;
	}

	public void setClaseProductor(String claseProductor) {
		this.claseProductor = claseProductor;
	}

	public String getCodProducto() {
		return codigoProducto;
	}

	public void setCodProducto(String codProducto) {
		this.codigoProducto = codProducto;
	}

	public Date getFechaOE() {
		return fechaOE;
	}

	public void setFechaOE(Date fechaOE) {
		this.fechaOE = fechaOE;
	}

	public BigInteger getNumeroDePoliza() {
		return numeroDePoliza;
	}

	public void setNumeroDePoliza(BigInteger numeroDePoliza) {
		this.numeroDePoliza = numeroDePoliza;
	}

	public BigInteger getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(BigInteger tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public BigInteger getCodigoProductor() {
		return codigoProductor;
	}

	public void setCodigoProductor(BigInteger codigoProductor) {
		this.codigoProductor = codigoProductor;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
