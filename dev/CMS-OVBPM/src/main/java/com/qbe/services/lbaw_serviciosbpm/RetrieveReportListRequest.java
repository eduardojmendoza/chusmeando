package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RetrieveReportListRequest", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class RetrieveReportListRequest  implements OVBPMRequest{

	/**
	 * retrieveReportList
	 */
	@XmlElement(required = true, defaultValue="retrieveReportList")
	protected String operation;

	/**
	 * Numero de Orden de Emision de la OV 
	 */
	@XmlElement(required=true)
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
