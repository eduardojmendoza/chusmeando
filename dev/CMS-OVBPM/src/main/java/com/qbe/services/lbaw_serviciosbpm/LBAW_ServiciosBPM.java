package com.qbe.services.lbaw_serviciosbpm;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * LBAW_ServiciosBPM define las operaciones del actionCode "lbaw_ServiciosBPM"
 * 
 * Como el formato de mensajes de los ActionCodes no es compatible con SOAP, usamos la siguiente convención para
 * poder implementar al menos la parte de los tipos:
 * 
 * <ul>
 * <li>El nombre de la operación se pasa como el parámetro &lt;operation&gt;
 * <li>Las operaciones se decriben acá como métodos. Por ejemplo lbaw_ServiciosBPM_publishReport representa una llamada 
 * al action code lbaw_ServiciosBPM con &lt;operation&gt;publishReport&lt;/operation&gt;
 * </ul>
 * @author ramiro
 *
 */
@WebService(
		name = "LBAW_ServiciosBPM", 
		targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/"
)
public interface LBAW_ServiciosBPM {

	/**
	 * Publica ( archiva ) un reporte en BPM
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public OVBPMResponse lbaw_ServiciosBPM_publishReport ( @WebParam(name="Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/") PublishReportRequest request);

	/**
	 * Retorna la lista de archivos asociados a un Certificado de Emisión
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public RetrieveReportListResponse lbaw_ServiciosBPM_retrieveReportList ( @WebParam(name="Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/" ) RetrieveReportListRequest request);

	/**
	 * Retorna el contenido de un archivo
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public RetrieveReportResponse lbaw_ServiciosBPM_retrieveReport ( @WebParam(name="Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/" ) RetrieveReportRequest request);

	/**
	 * Retorna el estado ( dentro del proceso ) de un ID de proceso en BPM
	 * 
	 * @param request
	 * @return
	 */
	@WebMethod	
	public RetrieveHoldStatusResponse lbaw_ServiciosBPM_retrieveHoldStatus ( @WebParam(name="Request", targetNamespace="http://lbaw_serviciosbpm.services.qbe.com/" ) RetrieveHoldStatusRequest request);
	
}
