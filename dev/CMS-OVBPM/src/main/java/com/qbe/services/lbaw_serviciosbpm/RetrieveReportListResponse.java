package com.qbe.services.lbaw_serviciosbpm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RetrieveReportListResponse", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class RetrieveReportListResponse extends OVBPMResponse {

	/**
	 * Lista de los reportes/archivos disponibles
	 */
	@XmlElementWrapper(name = "documentList")
	@XmlElement(required=true)
	protected List<DescripcionArchivo> item = new ArrayList<DescripcionArchivo>();

	public List<DescripcionArchivo> getItem() {
		return item;
	}

	public void setItem(List<DescripcionArchivo> item) {
		this.item = item;
	}
}
