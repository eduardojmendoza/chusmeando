package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "Estado", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
@XmlAccessorType(XmlAccessType.NONE)
//Configurado así para que no quiera bindear los isTrue/isFalse
public class Estado {

	public static final String FALSE = "false";

	public static final String TRUE = "true";

	/**
	 * Resultado de la ejecución, más allá de si funcionalmente la operación es o no "exitosa".
	 * 
	 * true: ejecución sin fallos
	 * false: ejecución con fallos o errores de Runtime
	 */
	@XmlAttribute
	protected String resultado;
	
	/**
	 * Desrcipción del problema en caso de que resultado == false
	 */
	@XmlAttribute
	protected String mensaje = "";

	public Estado() {
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isTrue() {
		return TRUE.equalsIgnoreCase(this.getResultado());
	}

	public boolean isFalse() {
		return FALSE.equalsIgnoreCase(this.getResultado());
	}

}
