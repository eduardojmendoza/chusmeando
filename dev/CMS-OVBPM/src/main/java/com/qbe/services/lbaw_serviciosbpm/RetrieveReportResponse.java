package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RetrieveReportResponse", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class RetrieveReportResponse extends OVBPMResponse{

	
	/**
	 * Descripción del archivo
	 */
	@XmlElement(required=true)
	protected DescripcionArchivo descripcion;
	
	/**
	 * El contenido del reporte en base64, usualmente es un PDF
	 */ 
	@XmlElement(required=true)
	protected byte[] reportContent;

	public DescripcionArchivo getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(DescripcionArchivo descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getReportContent() {
		return reportContent;
	}

	public void setReportContent(byte[] reportContent) {
		this.reportContent = reportContent;
	}

}
