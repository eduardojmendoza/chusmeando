package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "DescripcionArchivo", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class DescripcionArchivo {
	
	/**
	 * ID del archivo, creado por BPM en el publishReport
	 */
	@XmlElement
	protected String fileId;
	
	/**
	 * Nombre del archivo
	 * @see PublishReportRequest
	 */
	@XmlElement
	protected String nombre;

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


}
