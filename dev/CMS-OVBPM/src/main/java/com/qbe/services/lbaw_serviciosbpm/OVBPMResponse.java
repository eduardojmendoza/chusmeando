package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Respuesta común a OV
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Response", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class OVBPMResponse {


	/**
	 * Estado de respuesta de operación en Integración
	 */
	@XmlElement(name="Estado")
	protected Estado estado ;

	/**
	 * Código de respuesta de operación en BPM
	 */
	@XmlElement(name="responseCod")
	protected String responseCod ;

	public OVBPMResponse() {
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getResponseCod() {
		return responseCod;
	}

	public void setResponseCod(String responseCod) {
		this.responseCod = responseCod;
	}
}
