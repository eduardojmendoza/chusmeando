package com.qbe.services.lbaw_serviciosbpm;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "RetrieveHoldStatusResponse", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class RetrieveHoldStatusResponse extends OVBPMResponse{

	/**
	 * Listado de retenciones
	 */
	@XmlElementWrapper(name = "retenciones")
	@XmlElement(required=true)
	private List<Retencion> item = new ArrayList<Retencion>();

	public List<Retencion> getItem() {
		return item;
	}

	public void setItem(List<Retencion> item) {
		this.item = item;
	}
}
