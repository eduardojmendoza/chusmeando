package com.qbe.services.lbaw_serviciosbpm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Retencion", namespace="http://lbaw_serviciosbpm.services.qbe.com/")
public class Retencion {

	/**
	 * Valores posibles: 
	 * Nueva    ---->  Retención derivada
	 * Pendiente  -----> Retención en proceso de resolución 
	 * Resuelta -----> Retención resuelta por el sector correspondiente
	 * Liberada -----> Retención liberada para emitir
	 * No resuelta -----> Retención Rechazada o No Aprobada la solicitud
	 * 
	 */
	@XmlElement ( required = true)
	protected String estado;

	/**
	 * XXX Completar XXX
	 */
	@XmlElement ( required = true)
	protected String motivo;

	/**
	 * XXX Completar XXX
	 */
	@XmlElement ( required = true)
	protected String sectorAsignado;

	/**
	 * Formato DD-MM-YYY
	 */
	@XmlElement ( required = true)
	protected String fechaAlta;

	/**
	 * Formato DD-MM-YYY
	 */
	@XmlElement ( required = true)
	protected String ultimaModificacion;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getSectorAsignado() {
		return sectorAsignado;
	}

	public void setSectorAsignado(String sectorAsignado) {
		this.sectorAsignado = sectorAsignado;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(String ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}
	
}
