package com.qbe.services.coldview.estructurado;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.vbcompat.string.StringHolder;

public class GetXMLPDFTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	@Ignore
	public void testIAction_Execute() {
		GetXMLPDF comp = new GetXMLPDF();
		String request = "<Request>    "
				+ "<CATALOG>LBA - AIS COBRANZAS</CATALOG>    "
				+ "<PAGENBR/>    "
				+ "<ISSUEITEMNBR>1</ISSUEITEMNBR>    "
				+ "<DATEISSUE>2015-12-02</DATEISSUE>    "
				+ "<IDX_BUSQUEDA>751976807</IDX_BUSQUEDA>    "
				+ "<IssueDateFrom>2015-12-02</IssueDateFrom>    "
				+ "<IssueDateTo>2015-12-02</IssueDateTo>    "
				+ "<QueryClause><![CDATA[(factura = 751976807)]]></QueryClause>    "
				+ "<DocKey>LBA_CHOV</DocKey>    "
				+ "<PDFNoprint>0</PDFNoprint>    "
				+ "<PDFNomodify>-1</PDFNomodify>    "
				+ "<PDFNocopy>0</PDFNocopy>    "
				+ "<PDFNoannots>-1</PDFNoannots>    "
				+ "<ACCESSWAY>OV</ACCESSWAY>    "
				+ "<PDFUSRPWD/>"
				+ "</Request>";
		StringHolder response = new StringHolder();
		int resultCode = comp.IAction_Execute(request, response, null);
		assertEquals(0, resultCode);
	}

}
