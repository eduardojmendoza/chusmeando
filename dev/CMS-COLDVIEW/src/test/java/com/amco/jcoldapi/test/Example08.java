/**
 * 
 */
package com.amco.jcoldapi.test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.FullSearchResult;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.Page;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.test.conf.Configuration;

/**
 * Example 8 - Full text search
 * 
 * This class shows an example on how to perform a Full Text Search, retrieve the results and refer those hits to a row
 * of text on a page.
 * 
 * @author aacuna
 *
 */
public class Example08 {
	
	private static Logger logger = Logger.getLogger(Example08.class.getName());
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			final ColdViewApi api = new ColdViewApi();
			try {
				/*
				 * Connect to the Connection Service
				 */
				api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);

				/*
				 * Execute Test
				 */
				executeTest(api);
				
				/*
				 * Close API
				 */
				api.close();
				logger.info("close api");
				
			} catch (UnknownHostException e) {
				logger.log(Level.SEVERE, "No route to host.", e);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error writing to the socket", e);
			} catch (CVConnectionException e) {
				logger.log(Level.SEVERE, "Could not log into Connection Service", e);	
			}
		}
	}
	
	public static void executeTest(final ColdViewApi api) {	
		try {
			
			/*
			 * Get a list of published libraries
			 */
			final List<Library> libraries = api.struc.getLibraries();
			final Library library = libraries.iterator().next();
			
			/*
			 * Get list of tasks from a given library
			 */
			final List<Task> tasks = api.struc.getTasks(library);
			final Iterator<Task> taskIt = tasks.iterator();
			taskIt.next();
			Task task = taskIt.next();
						
			/*
			 * Get list of reports from a give library and task
			 */
			final List<Report> reports = api.struc.getReports( library, task );			
			final Iterator<Report> reportIt = reports.iterator();			
			final Report report = (Report) reportIt.next();
			
			/*
			 * Get a collection of issue info
			 */
			final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2016022520160302");
			final Iterator<IssueInfo> issueInfoIt = issueInfos.iterator();			
			final IssueInfo issueInfo = issueInfoIt.next();
			
			/*
			 * Get an issue from a given library and issue info
			 */
			Issue issue = api.struc.getIssue(library, issueInfo);
			issue.setNextPage( 3 );
			logger.info( "NextPage:" + issue.getNextPage());
											
			/*
			 * Perform a full text search on this issue
			 */
			final boolean isCompleteSearch = true;
			final boolean isAscending = false;
			final boolean isCaseSensitive = false;
			
			String toSearch = "Libertador 6350";
			FullSearchResult result = api.struc.getFullTextResult(issue, toSearch, !isCompleteSearch, !isAscending, !isCaseSensitive);
			int pageNum = result.getPageNumber();
			int lineNum = result.getLineNumber();
			
			logger.info( "Page number: " + pageNum );
			logger.info( "Line number: " + lineNum );
			
			issue.setNextPage( pageNum );
			
			Page page = api.struc.getPage(issue, Configuration.CHARSET);
			
			String[] pageLines = page.toString().split("\\r\\n");
			
			logger.info( "Page result: " + pageLines[lineNum]);
			
			/*
			 * Close issue
			 */
			api.struc.closeIssue( issue );
			
		} catch (UnknownHostException e) {
			logger.log(Level.SEVERE,  "No route to host.", e );
		} catch (IOException e) {
			logger.log(Level.SEVERE,  "Error writing to the socket", e );	
		} catch (BadServiceCallException e) {
			logger.log(Level.SEVERE,  "Invalida parameters", e );
		} catch(Exception e) {
			logger.log(Level.SEVERE,  "Connection", e );
		}
	}

}