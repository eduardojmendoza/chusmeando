/**
 * 
 */
package com.amco.jcoldapi.test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.test.conf.Configuration;

/**
 * Example 4 - Issue info retrieve 
 * 
 * An example that shows how to connect to an AcoldServer Connection Service, requests the allowed libraries, tasks, 
 * reports and issuesavailable for a user. Then prints useful data about issues for a particular user.
 * 
 * @author aacuna
 *
 */
public class Example04 {
	
	private static Logger logger = Logger.getLogger(Example04.class.getName());
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			final ColdViewApi api = new ColdViewApi();
			try {
				/*
				 * Connect to the Connection Service
				 */
				api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);

				/*
				 * Execute Test
				 */
				executeTest(api);
				
				/*
				 * Close API
				 */
				api.close();
				logger.info("close api");
				
			} catch (UnknownHostException e) {
				logger.log(Level.SEVERE, "No route to host.", e);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error writing to the socket", e);
			} catch (CVConnectionException e) {
				logger.log(Level.SEVERE, "Could not log into Connection Service", e);	
			}
		}
	}
	
	public static void executeTest(final ColdViewApi api) {
		try {
			/*
			 * Get a list of published libraries
			 */
			final List<Library> libraries = api.struc.getLibraries();
			final Iterator<Library> librariesIter = libraries.iterator(); 
			if (librariesIter.hasNext()) {
				final Library library = librariesIter.next();
				
				/*
				 * Get list of tasks from a given library
				 */
				final List<Task> tasks = api.struc.getTasks(library);
				final Iterator<Task> tasksIter = tasks.iterator();
				if (tasksIter.hasNext()) {
					final Task task = tasksIter.next();
					
					/*
					 * Get list of reports from a give library and task
					 */
					final List<Report> reports = api.struc.getReports( library, task );

					for (Iterator<Report> reportsIter = reports.iterator(); reportsIter.hasNext();) {
						final Report report = reportsIter.next();
						final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2016022520160302");

						for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
							IssueInfo info = (IssueInfo) issueInfosIter.next();
							logger.info( "Library: " + library.getName());
							logger.info( "Task: " + task.getName());
							logger.info( "Reports: " + reports );
							logger.info( "Issue name:" + info.getName() );
							logger.info( "Issue total pages: " + info.getPagesCount() );
							logger.info( "Issue publsh date:" + info.getDate() );
							logger.info( "Issue status:" + info.getStatusLevel() );
						}
					}
				}
			}
		} catch (UnknownHostException e) {
			logger.log(Level.SEVERE,  "No route to host.", e );
		} catch (IOException e) {
			logger.log(Level.SEVERE,  "Error writing to the socket", e );	
		} catch (BadServiceCallException e) {
			logger.log(Level.SEVERE,  "Invalida parameters", e );
		} catch(Exception e) {
			logger.log(Level.SEVERE,  "Connection", e );
		}
	}
	
}