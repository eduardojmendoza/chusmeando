package com.amco.jcoldapi.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.BinaryOperator;
import com.amco.jcoldapi.cvobjects.IndexedResult;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.LogicalOperator;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.cvobjects.Tuple;
import com.amco.jcoldapi.test.conf.Configuration;
import com.amco.jcoldapi.test.conf.Configuration.SEARCH;
import com.qbe.vbcompat.base64.Base64Encoding;

public class PDFStressTest implements Runnable {
	
	public static final transient Logger logger = Logger.getLogger(PDFStressTest.class.getName());
	private static int finished = 0;
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			executeTest();
		}
	}
	
	public static void executeTest() {
		long start = System.currentTimeMillis();
		List<Thread> threads = new ArrayList<Thread>();
		for (int i = 0; i < 3; i++) {
			PDFStressTest test = new PDFStressTest();
			Thread t = new Thread(test);
			threads.add(t);
		}
		
		for (Thread thread : threads) {
			thread.start();
		}
		
		while (finished < threads.size()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		long end = System.currentTimeMillis();
		System.out.println("tiempo: " + (end - start));
	}
	
	public void run() {
		/*
		 * Get a list of published libraries
		 */
		ColdViewApi api = new ColdViewApi();
		try {

			Library library = new Library("AISCOB", null);
			Task task = new Task("<Todos>");
			Report report = new Report("GRP44542", null, null, null);
			String toSearch = SEARCH.TO_SEARCH;
			
			api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);
			
			for (int i = 0; i< 10; i++ ){
				final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2016022520160302");
				for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
					final IssueInfo info = issueInfosIter.next();
					
					logger.info("info: " + info.getName());
					if (info.getName().equals("Subdiario de Cobranzas")) {	
						
						logger.info("Issue name:" + info.getName());
						logger.info("Encontrado:" + info.getName());
						Issue issue = api.struc.getIssue(library, info);
						logger.info("Issue: " + issue);
	
						final List<Tuple> tuples = new ArrayList<Tuple>();
						final Tuple tuple = new Tuple(SEARCH.SEARCH_FIELDNAME, BinaryOperator.LIKE, toSearch, LogicalOperator.NOTHING);
						tuples.add(tuple);
	
						final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);
	
						int pageNum = 1;
						for (Iterator<IndexedResult> iter = indexedResults.iterator(); iter.hasNext();) {
							IndexedResult result = iter.next();
							pageNum = result.getPageNumber();
							logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
						}
	
						issue.setNextPage(pageNum);
	
						final File folderOut = new File("output");
						if (folderOut.exists() || folderOut.mkdirs()) {
							final File output = new File(folderOut, "Example09_" + System.currentTimeMillis() + ".pdf");
							final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false, true, false, false, false, true);
							final FileOutputStream fos = new FileOutputStream(output);
							fos.write(Base64Encoding.decode(pdfBase64));
							fos.close();
							
						} else {
							logger.log(Level.SEVERE, "Error creando la carpeta "+folderOut.getAbsolutePath());
							
						}
	
						/*
						 * Close issue
						 */
						boolean closeOk = api.struc.closeIssue(issue);
						logger.info("" + closeOk);
					}
				}
			}
		} catch (UnknownHostException e) {
			logger.log(Level.SEVERE, "No route to host.", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error writing to the socket", e);
		} catch (CVConnectionException e) {
			logger.log(Level.SEVERE, "Could not log into Connection Service", e);
		} catch (BadServiceCallException e) {
			logger.log(Level.SEVERE, "Invalida parameters", e);
		} catch(Exception e) {
			logger.log(Level.SEVERE,  "Connection", e );
		} finally {
			PDFStressTest.finished ++;
			api.close();
		}
	}
}
