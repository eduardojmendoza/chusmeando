package com.amco.jcoldapi.test.conf;

public class Configuration {

	public static String HOST;
	public static int PORT;
	public static String USER;
	public static String PASS;
	public static String CHARSET;
	
	public static class SEARCH {
		public static String REPORT;
		public static String ISSUE_INFO;
		public static String LIBRARY;
		public static String TO_SEARCH;
		public static String SEARCH_FIELDNAME;
	}
	
	public static boolean loadConfiguration() {
		HOST = "10.1.10.222";
		PORT = 2944;
		USER = "all";
		PASS = "all";
		CHARSET = "ISO-8859-1";
		
		SEARCH.REPORT = "GRP44542";
		SEARCH.ISSUE_INFO = "Subdiario de Cobranzas";
		SEARCH.LIBRARY = "AISCOB";
		SEARCH.TO_SEARCH = "AER*";
		SEARCH.SEARCH_FIELDNAME = "INDEXTEXT01T";
		
		return true;
	}

	public static String HOST() {
		return HOST;
	}

	public static int PORT() {
		return PORT;
	}

	public static String USER() {
		return USER;
	}

	public static String PASS() {
		return PASS;
	}

	public static String getCHARSET() {
		return CHARSET;
	}
	
}
