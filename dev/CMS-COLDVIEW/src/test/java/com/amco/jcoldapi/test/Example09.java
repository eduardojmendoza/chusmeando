package com.amco.jcoldapi.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.BinaryOperator;
import com.amco.jcoldapi.cvobjects.IndexedResult;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.LogicalOperator;
import com.amco.jcoldapi.cvobjects.Page;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.cvobjects.Tuple;
import com.amco.jcoldapi.test.conf.Configuration;
import com.amco.jcoldapi.test.conf.Configuration.SEARCH;
import com.qbe.vbcompat.base64.Base64Encoding;

/**
 * Example 9 - Page Navigation
 * 
 * OBTENER UN SOLO PDF DETERMINADO.
 * 
 */
public class Example09 {

	private static Logger logger = Logger.getLogger(Example09.class.getName());
	
	public static void main(String[] args) {
		if (Configuration.loadConfiguration()) {
			final ColdViewApi api = new ColdViewApi();
			try {
				/*
				 * Connect to the Connection Service
				 */
				api.connect(Configuration.HOST, Configuration.PORT, Configuration.USER, Configuration.PASS);

				/*
				 * Execute Test
				 */
				executeTest(api);
				
				/*
				 * Close API
				 */
				api.close();
				logger.info("close api");
				
			} catch (UnknownHostException e) {
				logger.log(Level.SEVERE, "No route to host.", e);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error writing to the socket", e);
			} catch (CVConnectionException e) {
				logger.log(Level.SEVERE, "Could not log into Connection Service", e);	
			}
		}
	}
	
	public static void executeTest(final ColdViewApi api) {
		/*
		 * Get a list of published libraries
		 */
		try {
			Library library = null;
			Task task = null;
			Report report = null;
			IssueInfo info = null;

			final List<Library> libraries = api.struc.getLibraries();
			for (Iterator<Library>  librariesIter = libraries.iterator(); librariesIter.hasNext();) {
				library = librariesIter.next();
				if (library.getName().equals("AISCOB")) {
					logger.info("Library: " + library.getName());
					break;
				}
			}
			
			final List<Task> tasks = api.struc.getTasks(library);
			for (Iterator<Task> tasksIter = tasks.iterator(); tasksIter.hasNext();) {
				task = tasksIter.next();
				if (task.getName().equals("<Todos>")) {
					logger.info("Task: " + task.getName());
					break;
				}
			}
			
			final List<Report> reports = api.struc.getReports(library, task);
			for (Iterator<Report> reportsIter = reports.iterator(); reportsIter.hasNext();) {
				report = reportsIter.next();
				if (report.getName().equals("GRP44542")) {
					logger.info("Report name:" + report.getName());
					break;
				}
				
			}
			
			final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2016022520160302");
			for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
				info = issueInfosIter.next();
				if (info.getName().equals("Subdiario de Cobranzas")) {
					logger.info("Issue name:" + info.getName());
					logger.info("Encontrado:" + info.getName());
					Issue issue = api.struc.getIssue(library, info);
					logger.info("Issue: " + issue);

					final List<Tuple> tuples = new ArrayList<Tuple>();
					Tuple tuple = new Tuple(SEARCH.SEARCH_FIELDNAME, BinaryOperator.LIKE, SEARCH.TO_SEARCH, LogicalOperator.NOTHING);
					tuples.add(tuple);

					final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

					int pageNum = 1;
					for (Iterator<IndexedResult> iter = indexedResults.iterator(); iter.hasNext();) {
						IndexedResult result = iter.next();
						pageNum = result.getPageNumber();

						logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
					}

					issue.setNextPage(pageNum);
					Page page = api.struc.getPage(issue, Configuration.CHARSET);
					logger.info(page.toString());

					// Obtengo el pdf;
					final File folderOut = new File("output");
					if (folderOut.exists() || folderOut.mkdirs()) {
						final File output = new File(folderOut, "Example09_" + System.currentTimeMillis() + ".pdf");
						final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false, true, false, false, false, true);
						final FileOutputStream fos = new FileOutputStream(output);
						fos.write(Base64Encoding.decode(pdfBase64));
						fos.close();
						
					} else {
						logger.log(Level.SEVERE, "Error creando la carpeta "+folderOut.getAbsolutePath());
						
					}
					/*
					 * Close issue
					 */
					boolean closeOk = api.struc.closeIssue(issue);
					logger.info("" + closeOk);
				}
			}
		} catch (UnknownHostException e) {
			logger.log(Level.SEVERE, "No route to host.", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error writing to the socket", e);
		} catch (BadServiceCallException e) {
			logger.log(Level.SEVERE, "Invalida parameters", e);
		} catch(Exception e) {
			logger.log(Level.SEVERE,  "Connection", e );
		}
	}
}
