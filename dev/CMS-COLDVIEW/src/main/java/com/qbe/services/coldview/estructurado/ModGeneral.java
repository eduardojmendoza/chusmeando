package com.qbe.services.coldview.estructurado;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.DOMException;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.Exceptions.CVConnectionException;
import com.amco.jcoldapi.cvobjects.BinaryOperator;
import com.amco.jcoldapi.cvobjects.IndexedResult;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.LogicalOperator;
import com.amco.jcoldapi.cvobjects.Page;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.cvobjects.Tuple;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * ***************************************************************** COPYRIGHT.
 * HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT HAS BEEN
 * PROVIDED. NO PART OF IT IS TO BE REPRODUCED, DISASSEMBLED, TRANSMITTED,
 * STORED IN A RETRIEVAL SYSTEM NOR TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE
 * IN ANY WAY OR FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 * CONSENT OF HSBC HOLDINGS PLC.
 * ***************************************************************** Module Name
 * : ModGeneral File Name : ModGeneral.bas Creation Date: 10/10/2003 Programmer
 * : Muzzupappa - Goncalves Abstract : Declaraciones de constantes
 * ***************************************************************** API COLD
 * VIEW Public mobjCOLDview As ColdViewApi Public mobjCOLDview As Object UDL DE
 * CONEXION FIN MHC
 * ------------------------------------------------------------------------
 */

public class ModGeneral {

	private static Logger logger = Logger.getLogger(ModGeneral.class.getName());

	public static final String gcteDB = "lbaA_eCupons.udl";
	public static final String gcteDBColdView = "lbaA_ColdView.udl";
	/**
	 * PARAMETROS XML PARA COLD VIEW - NOMBRES DE ARCHIVO PARAMETROS DE CONEXION
	 */
	public static final String gcteParamFileName = "\\XMLFiles\\EColdConfig-Nuevo.xml";
	/**
	 * ESTRUCTURA XML PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE CUENTA
	 */
	public static final String gcteRetIssuesAISCOB = "\\XMLFiles\\RetIssues_AISCOB.xml";
	/**
	 * XSL PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE COMISIONES
	 */
	public static final String gcteRetIssuesAISCOB_XSL = "\\XSLFiles\\RetIssues_AISCOB.xsl";
	/**
	 * ESTRUCTURA XML PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE
	 * COMISIONES
	 */
	public static final String gcteRetIssuesAISCOM = "\\XMLFiles\\RetIssues_AISCOM.xml";
	/**
	 * XSL PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE CUENTA
	 */
	public static final String gcteRetIssuesAISCOM_XSL = "\\XSLFiles\\RetIssues_AISCOM.xsl";
	/**
	 * ESTRUCTURA XML PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE
	 * COMISIONES
	 */
	public static final String gcteRetIssuesAISOPER = "\\XMLFiles\\RetIssues_AISOPER.xml";
	/**
	 * XSL PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE CUENTA
	 */
	public static final String gcteRetIssuesAISOPER_XSL = "\\XSLFiles\\RetIssues_AISOPER.xsl";
	/**
	 * XML GENERALES ESTRUCTURA XML PARA OPEN ISSUE
	 */
	public static final String gcteOpenIssue = "\\XMLFiles\\OpenIssue.xml";
	/**
	 * ESTRUCTURA XML PARA ISSUE PAGE GOTO
	 */
	public static final String gcteIssuePageGoto = "\\XMLFiles\\IssuePageGoto.xml";
	/**
	 * ESTRUCTURA XML PARA ISSUE PROCESS PDF
	 */
	public static final String gcteIssueProcessPDF = "\\XMLFiles\\IssueProcessPDF.xml";
	/**
	 * ESTRUCTURA XML PARA PDF SEND MAIL
	 */
	public static final String gctePDFSendMail = "\\XMLFiles\\PDFSendMail.xml";
	/**
	 * QUERIES COLD VIEW
	 */
	public static final String gcteQueryParameter = "INPUT_PARAM";
	public static final String gcteQueryClause = "(factura = 'INPUT_PARAM')";
	/**
	 * 
	 * FILTROS PARA HIE
	 */
	public static final String gcteHIEFilter = "//xsl:apply-templates[@select = '/Response/Results/Row']/@select";
	public static final String gcteHIEFilterText = "/Response/Results/Row[IssueTitle=\"INPUT_PARAM\"]";
	/**
	 * 
	 * DIRECTORIO DE ARCHIVOS GENERADOS
	 */
	public static final String gcteParamFilesDir = "\\GenFiles\\";
	/**
	 * PROGRAMA PARA EJECUTAR LAS TRANSACCIONES
	 */
	public static final String gcteParamAsyncExec = "vbcA_AsyncExecution.exe";
	/**
	 * static variable for method: ColdViewConnect static variable for method:
	 * SelectUserPwd static variable for method: RetrieveIssues
	 */
	private static final String wcteFnName = "RetrieveIssues";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_CATALOG = "//CATALOG";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_DATEISSUE = "//DATEISSUE";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_IDX_SISTEMA = "//IDX_SISTEMA";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_PAGENBR = "//PAGENBR";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_TIPO_AVISO = "//TIPO_AVISO";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_DOCKEY = "//DOCKEY";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_CVDATETO = "//IssueDateTo";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_CVDOCKEY = "//DocKey";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_KEEPCONNECTED = "//KEEPCONNECTED";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
	/**
	 * static variable for method: RetrieveIssues
	 */
	private static final String mcteParam_QueryClause = "//QueryClause";

	private static final Map<String, String> indexTable = loadIndex();

	private static final String COLDVIEW_HOST = "coldview_host";

	private static final String COLDVIEW_PORT = "coldview_port";
	private static final String COLDVIEW_USER = "coldview_user";
	private static final String COLDVIEW_PASS = "coldview_pass";

	private static String propsFilename = System.getProperty("cms.config.properties");

	/**
	 * Nop, con la API Java no se necesita
	 */
	public static void ColdViewDisconnect(ColdViewApi mobjCOLDview) throws Exception {
	}

	/**
	 * *****************************************************************
	 * Function : ColdViewConnect Abstract : Crea el objeto Cold View, se
	 * conecta y valida el usuario Synopsis : ColdViewConnect() As Long
	 * *****************************************************************
	 * *****************************************************************
	 * 2008-08-15 - FJO / MC / DA Se modifica la llamada a la funci�n para que
	 * reciba el objeto por parametro para que no este definido en el m�dulo
	 * global para evitar problemas en COM+. Private Function
	 * ColdViewConnect(ByVal pvarAccessWay As String) As Long
	 */
	public static int ColdViewConnect(String pvarAccessWay, ColdViewApi mobjCOLDview) {
		int ColdViewConnect = 0;
		// *****************************************************************
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			// SELECCIONO EL USUARIO Y CLAVE DE CONEXION
			// FIXME Comentado hasta que definan como mapeo de ACCESSWAY a
			// credenciales, o si usa unas genéricas
			// if( ! (SelectUserPwd( pvarAccessWay, wvarResponseXML )) )
			// {
			// Err.raise( -1, "SelectUserPwd", "Error en la selección de usuario
			// y password de conexión a Cold View. Detalle:" + wvarResponseXML
			// );
			// }

			// FIXME Levantar parámetros de alguna config
			// mobjCOLDview.connect("10.1.10.222", 2944, "all", "all");

			String userColdView = loadConfig(COLDVIEW_USER);
			String passColdView = loadConfig(COLDVIEW_PASS);

			mobjCOLDview.connect(loadConfig(COLDVIEW_HOST), Integer.valueOf(loadConfig(COLDVIEW_PORT)).intValue(), userColdView,
					passColdView);

			// VALIDO EL USUARIO
			mobjCOLDview.validUser(userColdView, passColdView);
			ColdViewConnect = 0;
			return ColdViewConnect;
			// FIXME tirar una exception específica así el cliente sabe qué
			// falló
		} catch (UnknownHostException e) {
			logger.log(Level.SEVERE, "No route to host.", e);
			ColdViewConnect = 1;
		} catch (ConnectException e) {
			logger.log(Level.SEVERE, "Operation timed out.", e);
			ColdViewConnect = 1;
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error writing to the socket", e);
			ColdViewConnect = 1;
		} catch (CVConnectionException e) {
			logger.log(Level.SEVERE, "Could not log into Connection Service", e);
			ColdViewConnect = 1;
		}
		return ColdViewConnect;
	}

	/**
	 * *****************************************************************
	 * Function : SelectUserPwd Abstract : Determina el usuario y password de
	 * conexi�n a Cold View Synopsis : SelectUserPwd(ByVal pvarAccessWay As
	 * String, Response As String) As Boolean
	 * ***************************************************************** El
	 * formato del XML que recibe el objeto Cold View para conexi�n es el
	 * siguiente: <Request> <Server>valor</Server> <Port>valor</Port>
	 * <Timeout>valor</Timeout> <Encryptiontype>valor</Encryptiontype>
	 * <Language>valor</Language> <Securitytype>valor</Securitytype>
	 * <User>valor</User> <Pwd>valor</Pwd> </Request>
	 */
	private static boolean SelectUserPwd(String pvarAccessWay, Variant Response) {
		boolean SelectUserPwd = false;
		int wvarstep = 0;
		XmlDomExtended wobjXMLDoc = null;
		org.w3c.dom.NodeList wobjXMLConnectionsList = null;
		String[] warrConexiones = null;
		String wvarUserConnection = "";
		String wvarPwdConnection = "";
		String wvarXMLCVConnection = "";
		String wvarServer = "";
		String wvarPort = "";
		String wvarTimeout = "";
		String wvarEncryptionType = "";
		String wvarLanguage = "";
		String wvarSecuritytype = "";

		// String XML a devolver por la función
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			wvarstep = 10;
			wobjXMLDoc = new XmlDomExtended();
			wobjXMLDoc.load(System.getProperty("user.dir") + gcteParamFileName);
			wvarServer = XmlDomExtended.getText(wobjXMLDoc.selectSingleNode("Request/Server"));
			wvarPort = XmlDomExtended.getText(wobjXMLDoc.selectSingleNode("Request/Port"));
			wvarTimeout = XmlDomExtended.getText(wobjXMLDoc.selectSingleNode("Request/Timeout"));
			wvarEncryptionType = XmlDomExtended.getText(wobjXMLDoc.selectSingleNode("Request/Encryptiontype"));
			wvarLanguage = XmlDomExtended.getText(wobjXMLDoc.selectSingleNode("Request/Language"));
			wvarSecuritytype = XmlDomExtended.getText(wobjXMLDoc.selectSingleNode("Request/Securitytype"));

			wvarstep = 20;
			//
			// Busco el usuario de ColdView en base al parámetro recibido
			// (pvarAccessWay)
			if (pvarAccessWay.equals("")) {
				// Tomo el usuario por defecto
				wobjXMLConnectionsList = wobjXMLDoc.selectNodes("/Request/Connections/Connection[@Default=\"yes\"]");
			} else {
				wobjXMLConnectionsList = wobjXMLDoc
						.selectNodes("/Request/Connections/Connection[@Access=\"" + pvarAccessWay + "\"]");
			}
			//
			//
			wvarstep = 30;
			warrConexiones = Strings.split(XmlDomExtended.marshal(wobjXMLConnectionsList.item(0)), " ", -1);
			wvarUserConnection = Strings.replace(Strings.mid(warrConexiones[3],
					Strings.find(1, warrConexiones[3], "=") + 1, Strings.len(warrConexiones[3])), "\"", "");
			wvarPwdConnection = Strings.replace(Strings.mid(warrConexiones[4],
					Strings.find(1, warrConexiones[4], "=") + 1, Strings.len(warrConexiones[4])), "\"", "");
			wvarPwdConnection = Strings.replace(wvarPwdConnection, "/>", "");
			//
			// Armo el XML de conexión a ColdView con el usuario y password
			// correspondiente
			// (respetando el formato de EColdConfig.xml)
			wvarXMLCVConnection = "<Request>" + String.valueOf((char) (13)) + String.valueOf((char) (9)) + "<Server>"
					+ wvarServer + "</Server>" + String.valueOf((char) (13)) + String.valueOf((char) (9)) + "<Port>"
					+ wvarPort + "</Port>" + String.valueOf((char) (13)) + String.valueOf((char) (9)) + "<Timeout>"
					+ wvarTimeout + "</Timeout>" + String.valueOf((char) (13)) + String.valueOf((char) (9))
					+ "<Encryptiontype>" + wvarEncryptionType + "</Encryptiontype>" + String.valueOf((char) (13))
					+ String.valueOf((char) (9)) + "<Language>" + wvarLanguage + "</Language>"
					+ String.valueOf((char) (13)) + String.valueOf((char) (9)) + "<Securitytype>" + wvarSecuritytype
					+ "</Securitytype>" + String.valueOf((char) (13)) + String.valueOf((char) (9)) + "<User>"
					+ wvarUserConnection + "</User>" + String.valueOf((char) (13)) + String.valueOf((char) (9))
					+ "<Pwd>" + wvarPwdConnection + "</Pwd>" + String.valueOf((char) (13)) + "</Request>";

			wobjXMLDoc = null;
			wobjXMLConnectionsList = (org.w3c.dom.NodeList) null;

			Response.set(wvarXMLCVConnection);
			SelectUserPwd = true;
			return SelectUserPwd;

			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~
				//
				wobjXMLDoc = null;
				wobjXMLConnectionsList = (org.w3c.dom.NodeList) null;
				//
				// mobjEventLog.Log
				// EventLog_Category.evtLog_Category_Unexpected, _
				// ' mcteClassName, _
				// wcteFnName, _
				// ' wvarstep, _
				// Err.Number, _
				// ' "Error= [" & Err.Number & "] - " & Err.Description & "[XML
				// Config: No se pudo determinar el usuario y password de
				// conexi�n con Cold View para la v�a de acceso: " &
				// pvarAccessWay & "] ", _
				// vbLogEventTypeError
				//
				Response.set("Error: No se pudo determinar el usuario y password de conexi�n para la v�a de acceso "
						+ pvarAccessWay);
				SelectUserPwd = false;

				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return SelectUserPwd;
	}

	/**
	 * *****************************************************************
	 * Function : RetrieveIssues Abstract : Reemplazo de la clase RetrieveIssues
	 * a una funcion Synopsis : RetrieveIssues() As Long
	 * *****************************************************************
	 * 2008-08-15 - FJO / MC / DA Se modifica la llamada a la funci�n para que
	 * reciba el objeto por parametro para que no este definido en el m�dulo
	 * global para evitar problemas en COM+. MHC
	 * ------------------------------------------------------------------------
	 */
	public static int RetrieveIssues(String pvarRequest, Variant pvarResponse, ColdViewApi pobjCOLDview) {
		int RetrieveIssues = 0;
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLResponse = null;
		XmlDomExtended wobjXSLResponse = null;
		String wvarXMLOut = "";
		String wvarCatalog = "";
		String wvarDateIssue = "";
		String wvarIDX_Busqueda = "";
		String wvarQuerySearch = "";
		String wvarIDX_Sistema = "";
		String wvarPageNbr = "";
		String wvarTipo_Aviso = "";
		String wvarDocKey = "";
		int wvarstep = 0;
		String wvarresult = "";
		boolean wvarKeepConnected = false;
		String wvarAccessWay = "";
		// *****************************************************************
		//
		// Parametros XML de Entrada
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			// CARGO LOS PARAMETROS DE ENTRADA
			wvarstep = 10;
			wobjXMLRequest = new XmlDomExtended();
			//
			wobjXMLRequest.loadXML(pvarRequest);
			wvarCatalog = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CATALOG));
			wvarDateIssue = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DATEISSUE));
			wvarIDX_Busqueda = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IDX_BUSQUEDA));
			wvarQuerySearch = "";
			if (!(wobjXMLRequest.selectSingleNode(mcteParam_QueryClause) == (org.w3c.dom.Node) null)) {
				wvarQuerySearch = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_QueryClause));
			}
			wvarDocKey = "";
			if (!(wobjXMLRequest.selectSingleNode(mcteParam_DOCKEY) == (org.w3c.dom.Node) null)) {
				wvarDocKey = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOCKEY));
			} else if (!(wobjXMLRequest.selectSingleNode(mcteParam_CVDOCKEY) == (org.w3c.dom.Node) null)) {
				wvarDocKey = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDOCKEY));
			}
			//
			if (wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR) == (org.w3c.dom.Node) null) {
				wvarPageNbr = "";
			} else {
				wvarPageNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR));
			}
			//
			wvarstep = 20;
			//
			if (wobjXMLRequest.selectSingleNode(mcteParam_KEEPCONNECTED) == (org.w3c.dom.Node) null) {
				wvarKeepConnected = false;
			} else {
				wvarKeepConnected = true;
			}
			//
			// Verifico que exista el nodo <ACCESSWAY>
			// On Error Resume Next (optionally ignored)
			wvarAccessWay = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACCESSWAY));
			if (Err.getError().getNumber() != 0) {
				wvarAccessWay = "";
			}

			//
			wobjXMLRequest = null;
			//
			// CARGO EL ARCHIVO XML PARA EL ISSUE CORRESPONDIENTE
			wvarstep = 30;
			wobjXMLRequest = new XmlDomExtended();
			//
			wvarstep = 40;

			if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
				wobjXMLRequest.load(System.getProperty("user.dir") + gcteRetIssuesAISCOB);
			} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
				wobjXMLRequest.load(System.getProperty("user.dir") + gcteRetIssuesAISCOM);
			} else if (wvarCatalog.equals("LBA - EMISION")) {
				wobjXMLRequest.load(System.getProperty("user.dir") + gcteRetIssuesAISOPER);
			}
			// CARGO el DOCKEY
			wvarstep = 45;
			if (!wvarDocKey.equals("")) {
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVDOCKEY), wvarDocKey);
			}
			// CARGO LOS NODOS FECHA_DESDE, FECHA_HASTA Y QUERY EN EL ARCHIVO
			wvarstep = 50;
			XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATEFROM), wvarDateIssue);
			wvarstep = 51;
			XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATETO), wvarDateIssue);
			//
			wvarstep = 52;

			if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
				wvarstep = 53;
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE),
						Strings.replace(gcteQueryClause, gcteQueryParameter, wvarIDX_Busqueda));
			} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
				wvarstep = 54;
				XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE), wvarQuerySearch);
			}
			//
			// EJECUTO RETRIEVE ISSUES DE COLD VIEW
			wvarstep = 70;
			//
			if (wvarKeepConnected
					&& (Strings.len(Strings
							.trim(XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_QueryClause)))) == 0)
					&& (!wvarPageNbr.equals(""))) {
				// LA LLAMADA SE HACE DESDE GETXMLPDF, EJECUTO RETRIEVEISSUES
				// FIXME Convertir
				// pobjCOLDview.XML_RetrieveIssues(
				// wobjXMLRequest.getDocument().getDocumentElement().toString(),
				// wvarXMLOut );
			} else {
				// LA LLAMADA SE HACE DESDE LA PAGINA, EJECUTO
				// RETRIEVEISSUESWITHSEARCH
				// FIXME Convertir
				// pobjCOLDview.XML_RetrieveIssuesWithSearch(
				// wobjXMLRequest.getDocument().getDocumentElement().toString(),
				// wvarXMLOut );
			}
			//
			// VERIFICO LA RESPUESTA DE COLD VIEW
			wobjXMLResponse = new XmlDomExtended();
			wobjXMLResponse.loadXML(wvarXMLOut);
			//
			wvarstep = 80;
			//
			if (XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Status")).equals("-1")) {
				pvarResponse
						.set("<Response><Estado resultado=\"false\" mensaje=\"NO SE ENCONTRARON DATOS PARA EL RANGO DE FECHAS.\"  cv_message=\""
								+ XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Event_Message"))
								+ "\" cv_errornbr= \""
								+ XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Error_Nbr"))
								+ "\"/></Response>");
				RetrieveIssues = 1;
				return RetrieveIssues;
			}
			//
			// CARGO EL XSL PARA TRANSFORMAR LA RESPUESTA
			wvarstep = 90;
			wobjXSLResponse = new XmlDomExtended();
			//

			if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
				wobjXSLResponse.load(System.getProperty("user.dir") + gcteRetIssuesAISCOB_XSL);
			} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
				wobjXSLResponse.load(System.getProperty("user.dir") + gcteRetIssuesAISCOM_XSL);
			} else if (wvarCatalog.equals("LBA - EMISION")) {
				wobjXSLResponse.load(System.getProperty("user.dir") + gcteRetIssuesAISOPER_XSL);
			}
			//
			wvarresult = wobjXMLResponse.transformNode(wobjXSLResponse).toString()
					.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "");
			wvarstep = 100;
			wobjXMLResponse = null;
			wobjXSLResponse = null;
			wobjXMLRequest = null;
			//
			wvarstep = 110;
			pvarResponse.set("<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarresult + "</Response>");
			//
			RetrieveIssues = 0;
			return RetrieveIssues;
			//
			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~
				wobjXMLRequest = null;
				wobjXMLResponse = null;
				wobjXSLResponse = null;
				//
				pvarResponse.set("<Response><Estado resultado=\"false\"/></Response>");
				//
				RetrieveIssues = 1;
				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return RetrieveIssues;
	}

	/**
	 * Method that return a Issue object
	 * 
	 * @param api
	 * @param wvarIssueItemNbr
	 * @param queryClause
	 * @param wvarIDX_Busqueda
	 * @param wvarDateIssue
	 * @param wvarIssueItemNbr2
	 * @param wvarCatalog
	 * @param wvarDocKey
	 * @param dateTo
	 * @param dateFrom
	 * @param wvarPageNbr
	 * @return
	 * @throws IOException
	 * @throws BadServiceCallException
	 * @throws ParseException
	 */

	public static Issue obteinIssueInfo(ColdViewApi api, String wvarIssueItemNbr, String wvarCatalog,
			String wvarIssueItemNbr2, String wvarDateIssue, String wvarIDX_Busqueda, String queryClause,
			String wvarDocKey, String dateFrom, String dateTo, String wvarPageNbr)
			throws IOException, BadServiceCallException, ParseException {

		Library library = null;
		Report report = null;
		Issue issue = null;

		Task task = new Task("<Todos>");

		List<Library> libraries = api.struc.getLibraries();

		for (Library library2 : libraries) {
			if (library2.getDescription().equals(wvarCatalog)) {
				library = library2;
				break;
			}
		}
		final List<Report> reports = api.struc.getReports(library, task);

		for (Report report2 : reports) {
			if (report2.getName().equals(wvarDocKey)) {
				report = report2;
				break;
			}
		}

		StringBuffer rangoFecha = new StringBuffer();
		rangoFecha.append("R");
		rangoFecha.append(converterDateFormat(dateFrom));
		rangoFecha.append(converterDateFormat(dateTo));
		logger.info("library " + library + " task " + task + " report: " + report + "fecha " + rangoFecha.toString());
		List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
				rangoFecha.toString().trim());
		logger.info("list InfoIssue " + issueInfos.size());
		for (IssueInfo issueInfo : issueInfos) {
			logger.info("inicio de busqueda de issue");
			logger.info(library + " " + issueInfo);
			logger.info("cantidad de paginas: " + issueInfo.getPagesCount());
			// if (issueInfo.getPagesCount() >= Integer.parseInt(wvarPageNbr)) {

			issue = api.struc.getIssue(library, issueInfo);
			logger.info("queryClause " + queryClause);
			logger.info("issue: " + issue);
			int pageNum = 1;
			if (!"".equals(queryClause)) {

				List<Tuple> tuples = createTuple(obteinIndexValues(queryClause));
				logger.info("inicio de busqueda de indexResult");
				List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

				for (IndexedResult result : indexedResults) {
					pageNum = result.getPageNumber();
					//issue.setNextPage(pageNum);
					logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
					//break;
				}
				issue.setNextPage(pageNum);
			}
			// if ("".equals(wvarPageNbr)) {
			// issue.setNextPage(pageNum);
			// } else {
			// issue.setNextPage(Integer.parseInt(wvarPageNbr));
			// }
			// break;
			// }
		}
		if (issue == null) {
			throw new NullPointerException("No se han encontrado Issues.");
		}
		return issue;
	}

	/**
	 * @param dateFrom
	 * @throws ParseException
	 */
	private static String converterDateFormat(String dateFrom) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate;
		curDate = sdf.parse(dateFrom);
		logger.info(" fecha a convertir: " + dateFrom);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		return format.format(curDate);

	}

	/**
	 * 
	 * Method that create a XML with the PFDbase64 string
	 * 
	 * @param response
	 * @param wobjXMLResponse
	 * @param pdfBase64
	 * @throws DOMException
	 * @throws XmlDomExtendedException
	 */
	public static void createXML(StringHolder response, XmlDomExtended wobjXMLResponse, String pdfBase64)
			throws DOMException, XmlDomExtendedException {

		response.set("<Response><Estado resultado='true' mensaje='' /></Response>");

		wobjXMLResponse = new XmlDomExtended();
		wobjXMLResponse.loadXML(response.getValue());

		org.w3c.dom.Element wobjEle = wobjXMLResponse.getDocument().createElement("PDFSTREAM");
		wobjXMLResponse.selectSingleNode("//Response").appendChild(wobjEle);
		wobjEle.setAttribute("dt:dt", "bin.base64");
		wobjEle.setAttribute("xmlns:dt", "urn:schemas-microsoft-com:datatypes");
		XmlDomExtended.setText(wobjEle, pdfBase64);

		response.set(XmlDomExtended.prettyPrint(wobjXMLResponse.getDocument().getDocumentElement()));

	}

	public static String obteinIssuePages(ColdViewApi api, String wvarCatalog, String wvarDateIssue,
			String wvarIDX_Busqueda, String queryClause, String wvarDocKey, String dateFrom, String dateTo,
			String wvarPageNbr, String wvarCantPage) throws IOException, BadServiceCallException, ParseException {

		Library library = null;
		Report report = null;
		Issue issue = null;
		StringBuffer pdfString = new StringBuffer();
		int cantPage = Integer.parseInt((wvarCantPage.equals("")) ? "1" : wvarCantPage);
		int pageNbr = Integer.parseInt((wvarPageNbr.equals("")) ? "1" : wvarPageNbr);

		Task task = new Task("<Todos>");
		logger.info("wvarCatalog " + wvarCatalog + " wvarDateIssue" + wvarDateIssue + "wvarIDX_Busqueda "
				+ wvarIDX_Busqueda + "wvarDocKey " + wvarDocKey + "dateFrom " + dateFrom + "dateTo " + dateTo
				+ "wvarPageNbr " + wvarPageNbr + "wvarCantPage " + wvarCantPage);
		List<Library> libraries = api.struc.getLibraries();

		for (Library library2 : libraries) {
			if (library2.getDescription().equals(wvarCatalog)) {
				library = library2;
				break;
			}
		}
		final List<Report> reports = api.struc.getReports(library, task);

		for (Report report2 : reports) {
			if (report2.getName().equals(wvarDocKey)) {
				report = report2;
				break;
			}
		}

		StringBuffer rangoFecha = new StringBuffer();
		rangoFecha.append("R");
		rangoFecha.append(converterDateFormat(dateFrom));
		rangoFecha.append(converterDateFormat(dateTo));
		logger.info("library" + library + "task " + task + "report" + report);
		List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
				rangoFecha.toString().trim());

		logger.info("la cantidad de issue es: " + issueInfos.size());
		for (IssueInfo issueInfo : issueInfos) {

			issue = api.struc.getIssue(library, issueInfo);
			logger.info("finalizacion de busqueda de issue");
			logger.info("QueryClause: " + queryClause);
			logger.info("!''.equals(queryClause) " + (!"".equals(queryClause)));

			if (!"".equals(queryClause)) {
				List<Tuple> tuples = createTuple(obteinIndexValues(queryClause));
				logger.info("inicio de busqueda por tupla");
				List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);
				logger.info("la cantidad de indexResults es: " + indexedResults.size());

				int resultSize = 0;
//				if (cantPage > indexedResults.size()) {
					resultSize = indexedResults.size();
//				} else {
//					resultSize = cantPage;
//				}

				for (int i = 0; i < resultSize; i++) {
					issue.setNextPage(((IndexedResult) indexedResults.get(i)).getPageNumber());
					Page page = api.struc.getPage(issue, "ISO-8859-1");
					logger.info("fields: " + indexedResults.get(i).getFields());
					//logger.info("pageContent: " + page.getContent());
					pdfString.append(page.getContent());
					pdfString.append("\n");
				}

//			} else {
//				for (int i = 0; i < cantPage; i++) {
//					issue.setNextPage(pageNbr + i);
//					Page page = api.struc.getPage(issue, "ISO-8859-1");
//					logger.info("pageContent: " + page.getContent());
//					pdfString.append(page.getContent());
//					pdfString.append("\n");
//				}
			}

			

		}
		if (pdfString.toString().isEmpty()) {
			throw new NullPointerException("No se han encontrado Issues.");
		}
		return pdfString.toString();

	}

	/**
	 * Método que crea una lista de Tuple
	 * 
	 * @param wvarIDX_Busqueda
	 * @return
	 */
	private static List<Tuple> createTuple(Map<String, String> indexValue) {
		List<Tuple> tuples = new ArrayList<Tuple>(); // <QueryClause>
		Tuple tuple = null;
		for (Map.Entry<String, String> index : indexValue.entrySet()) {

			// if(index.getKey().substring(index.getKey().length()-1).equals("N")){
			tuple = new Tuple(index.getKey(), BinaryOperator.EQUAL, index.getValue(), LogicalOperator.AND);
			logger.info(index.getKey() + " Equals");
			// }else{
			// tuple = new Tuple(index.getKey(), BinaryOperator.EQUAL,
			// index.getValue(), LogicalOperator.AND);
			// logger.info(index.getKey() + " Equals Text");
			// }
			// tuple = new Tuple(index.getKey(), BinaryOperator.EQUAL,
			// index.getValue(), LogicalOperator.AND);
			tuples.add(tuple);
		}
		logger.info(tuples.get(tuples.size() - 1).toString());
		tuples.get(tuples.size() - 1).setLogicalOperator(LogicalOperator.NOTHING);
		logger.info(tuples.toString());
		return tuples;
	}

	/**
	 * @return
	 */
	public static Map<String, String> loadIndex() {
		Map<String, String> indexValues = new HashMap<String, String>();
		/* Comision */
		indexValues.put("Cliente Sec", "INDEXTEXT01N");
		indexValues.put("Liqui Sec", "INDEXTEXT02N");
		indexValues.put("Cant de Paginas", "INDEXTEXT03N");
		/* cobranzas */
		indexValues.put("FACTURA", "INDEXTEXT01T");
		/* Emision */
		indexValues.put("Oficina", "INDEXTEXT01T");
		indexValues.put("Productor", "INDEXTEXT02T");
		indexValues.put("Producto", "INDEXTEXT03T");
		indexValues.put("Tipo de Operacion", "INDEXTEXT04T");
		indexValues.put("N° Cliente", "INDEXTEXT05N");
		indexValues.put("Descripcion Oficina", "INDEXTEXT06T");
		indexValues.put("Numero Productor", "INDEXTEXT07T");
		return indexValues;

	}

	/**
	 * Metodo que retorna un objeto Map teniendo como clave la descripcion del
	 * indice y el valor del mismo
	 * 
	 * @param txt
	 * @return
	 */
	private static Map<String, String> parserQueryClause(String txt) {
		Map<String, String> queryClause = new HashMap<String, String>();

		String txtReturn = txt.replace("(", "");
		txtReturn = txtReturn.replace(")", "");// "ClienteSec=100223914andLiquiSec=392"
		String[] txtSplit = txtReturn.split("and"); // {"ClienteSec=100223914","LiquiSec=392"}
		for (int i = 0; i < txtSplit.length; i++) {
			String[] txtDescVal = txtSplit[i].split("="); // {"ClienteSec","100223914"}
			queryClause.put(txtDescVal[0].trim().toUpperCase(), txtDescVal[1].trim());
		}
		return queryClause;

	}

	/**
	 * Metodo que retorna un objeto Map teniendo como clave el index y el valor
	 * del mismo
	 * 
	 * 
	 * @param txt
	 * @return
	 */
	private static Map<String, String> obteinIndexValues(String txt) {
		Map<String, String> queryClause = new HashMap<String, String>();
		Map<String, String> indexValues = new HashMap<String, String>();
		queryClause = parserQueryClause(txt);
		for (Map.Entry<String, String> entry : queryClause.entrySet()) {
			for (Map.Entry<String, String> indexSearch : indexTable.entrySet()) {
				if (entry.getKey().equals(indexSearch.getKey().toUpperCase())) {
					indexValues.put(indexSearch.getValue(), entry.getValue());
					break;
				}
			}
		}
		logger.info(indexValues.toString());
		return indexValues;
	}

	/**
	 * Método que retorna el valor de la key que esta dentro del archivo de
	 * propiedades
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	private static String loadConfig(String key) throws IOException {
		Properties fileProper = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
		fileProper.load(in);
		in.close();
		return fileProper.getProperty(key);

	}

}
