package com.qbe.services.coldview.estructurado;

import java.util.logging.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.Page;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Variant;

/**
 * ***************************************************************** COPYRIGHT.
 * HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT HAS BEEN
 * PROVIDED. NO PART OF IT IS TO BE REPRODUCED, DISASSEMBLED, TRANSMITTED,
 * STORED IN A RETRIEVAL SYSTEM NOR TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE
 * IN ANY WAY OR FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 * CONSENT OF HSBC HOLDINGS PLC.
 * ***************************************************************** Module Name
 * : SendPDFMailAsync File Name : SendPDFMailAsync.cls Creation Date: 13/01/2004
 * Programmer : Muzzupappa - Goncalves Abstract : Envia un PDF por mail de la
 * base de Cold View
 * ***************************************************************** 2008-08-15
 * - FJO / MC / DA Objeto de ColdView
 */

public class SendPDFMail implements VBObjectClass {
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbaA_ECold.SendPDFMail";
	/**
	 * Parametros XML
	 */
	static final String mcteParam_CATALOG = "//CATALOG";
	static final String mcteParam_DATEISSUE = "//DATEISSUE";
	static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
	static final String mcteParam_PAGENBR = "//PAGENBR";
	static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
	static final String mcteParam_IDX_SISTEMA = "//IDX_SISTEMA";
	static final String mcteParam_MAXPAGES = "//MAXPAGES";
	static final String mcteParam_DOCKEY = "//DOCKEY";

	static final String mcteParam_SENDTO = "//SENDTO";
	static final String mcteParam_GIVENATTACHPDFNAME = "//GIVENATTACHPDFNAME";
	static final String mcteParam_SUBJECT = "//SUBJECT";
	static final String mcteParam_MSGBODY = "//MSGBODY";
	static final String mcteParam_FROM = "//FROM";
	static final String mcteParam_FROMADDRESS = "//FROMADDRESS";
	static final String mcteParam_REPLYTO = "//REPLYTO";
	static final String mcteParam_CC = "//CC";
	static final String mcteParam_BCC = "//BCC";
	static final String mcteParam_PDFUSRPWD = "//PDFUSRPWD";
	static final String mcteParam_PDFMSTRPWD = "//PDFMSTRPWD";
	static final String mcteParam_PDFKEYLENGTH = "//PDFKEYLENGTH";
	static final String mcteParam_PDFNOPRINT = "//PDFNOPRINT";
	static final String mcteParam_PDFNOMODIFY = "//PDFNOMODIFY";
	static final String mcteParam_PDFNOCOPY = "//PDFNOCOPY";
	static final String mcteParam_PDFNOANNOTS = "//PDFNOANNOTS";
	static final String mcteParam_ADDITIONALFILES = "//ADDITIONALFILES";
	static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
	static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
	private static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
	private static final String mcteParam_CVDATETO = "//IssueDateTo";
	/**
	 * Parametros Cold View
	 */
	static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
	static final String mcteParam_CVPAGENBR = "//PageNbr";
	static final String mcteParam_CVISSUEID = "//IssueID";
	static final String mcteParam_CVSENDTO = "//SendTo";
	static final String mcteParam_CVGIVENATTACHPDFNAME = "//GivenAttachPDFName";
	static final String mcteParam_CVSUBJECT = "//Subject";
	static final String mcteParam_CVMSGBODY = "//MsgBody";
	static final String mcteParam_CVFROM = "//From";
	static final String mcteParam_CVFROMADDRESS = "//FromAddress";
	static final String mcteParam_CVREPLYTO = "//ReplyTo";
	static final String mcteParam_CVPDFUSRPWD = "//PDFUsrPWD";
	static final String mcteParam_CVPDFMSTRPWD = "//PDFMstrPWD";
	static final String mcteParam_CVPDFKEYLENGTH = "//PDFKeyLength";
	static final String mcteParam_CVPDFNOPRINT = "//PDFNoprint";
	static final String mcteParam_CVPDFNOMODIFY = "//PDFNomodify";
	static final String mcteParam_CVPDFNOCOPY = "//PDFNocopy";
	static final String mcteParam_CVPDFNOANNOTS = "//PDFNoannots";
	static final String mcteParam_CVADDITIONALFILES = "//AdditionalAttachFiles";
	public ColdViewApi mobjCOLDview = new ColdViewApi();
	/**
	 * Objetos del FrameWork
	 */
	private Object mobjCOM_Context = null;
	private EventLog mobjEventLog = new EventLog();
	private static Logger logger = Logger.getLogger(ModGeneral.class.getName());
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	private static final String REPORTCOBRANZA = "LBA_CHEQ";

	private static final String REPORTCOMISION = "GR44536";

	private static final String REPORTEMISION = "GRP0012";

	private static final String OWNERPASS = "password";
	
	private static final String NAMETO = "Estimado Productor";

	/**
	 * *****************************************************************
	 * Function : IAction_Execute Abstract : Busca en la base de Cold View la
	 * emision de la fecha pasada como parametro, abre la misma y envia el PDF
	 * correspondiente por mail a la direccion especificada Synopsis :
	 * IAction_Execute(ByVal Request As String, Response As String, ByVal
	 * ContextInfo As String) As Long
	 * *****************************************************************
	 */
	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLResponse = null;
		org.w3c.dom.Element wobjXMLElement = null;
		String wvarXMLOut = "";
		String wvarCatalog = "";
		String wvarIssueItemNbr = "";
		String wvarDocKey = "";
		String wvarPageNbr = "";
		String wvarIssueId = "";
		String wvarSendTo = "";
		String wvarPDFName = "";
		String wvarSubject = "";
		String wvarMSGBody = "";
		String wvarFrom = "";
		String wvarFromAddress = "";
		String wvarReplyTo = "";
		String wvarCC = "";
		String wvarBCC = "";
		Variant wvarResponse = new Variant();
		String wvarSM_ID = "";
		int wvarCounter = 0;
		String wvarPDFUsrPWD = "";
		String wvarPDFMstrPWD = "";
		String wvarPDFKeyLength = "";
		String wvarPDFNoPrint = "";
		String wvarPDFNoModify = "";
		String wvarPDFNoCopy = "";
		String wvarPDFNoAnnots = "";
		String wvarAdditionalFiles = "";
		String wvarAccessWay = "";
		int wvarstep = 0;

		String wvarDateIssue = "";
		String wvarIdxBusqueda = "";
		String wvarIdxSistema = "";
		String wvarQueryClause = "";
		String dateFrom = "";
		String dateTo = "";

		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			// LEVANTO DE LA BASE DE DATOS LOS MAILS PARA ENVIAR
			// CARGO LOS PARAMETROS DEL MAIL
			wvarstep = 80;
			wobjXMLRequest = new XmlDomExtended();
			//
			wobjXMLRequest.loadXML(Request);
			wvarCatalog = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CATALOG));
			wvarDateIssue = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DATEISSUE));
			wvarIdxBusqueda = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IDX_BUSQUEDA));
			wvarPageNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR));
			wvarIdxSistema = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IDX_SISTEMA));
			wvarIssueItemNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ISSUEITEMNBR));
			wvarDocKey = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOCKEY));
			wvarSendTo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SENDTO));
			wvarPDFName = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_GIVENATTACHPDFNAME));
			wvarSubject = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SUBJECT));
			wvarMSGBody = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_MSGBODY));
			wvarFrom = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FROM));
			wvarFromAddress = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_FROMADDRESS));
			//
			// if (wobjXMLRequest.selectSingleNode(mcteParam_REPLYTO) ==
			// (org.w3c.dom.Node) null) {
			// wvarReplyTo = "";
			// } else {
			// wvarReplyTo =
			// XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_REPLYTO));
			// }
			//
			wvarCC = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CC));
			wvarBCC = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_BCC));
			wvarPDFUsrPWD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFUSRPWD));
			wvarPDFMstrPWD = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFMSTRPWD));
			wvarPDFKeyLength = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFKEYLENGTH));
			wvarPDFNoPrint = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOPRINT));
			wvarPDFNoModify = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOMODIFY));
			wvarPDFNoCopy = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOCOPY));
			wvarPDFNoAnnots = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOANNOTS));
			//
			// if (wobjXMLRequest.selectSingleNode(mcteParam_ADDITIONALFILES) ==
			// (org.w3c.dom.Node) null) {
			// wvarAdditionalFiles = "";
			// } else {
			// wvarAdditionalFiles = XmlDomExtended
			// .getText(wobjXMLRequest.selectSingleNode(mcteParam_ADDITIONALFILES));
			// }

			// Verifico que exista el nodo <ACCESSWAY>
			// On Error Resume Next (optionally ignored)
			wvarAccessWay = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACCESSWAY));
			wvarQueryClause = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE));
			dateFrom = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATEFROM));
			dateTo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATETO));

			if (Err.getError().getNumber() != 0) {
				wvarAccessWay = "";
			}
			//
			//
			wvarstep = 90;
			//
			// AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES
			// DEL RETRIEVEISSUES
			// wobjXMLElement =
			// wobjXMLRequest.getDocument().createElement("KEEPCONNECTED");
			// XmlDomExtended.setText(wobjXMLElement, "TRUE");
			// wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLElement);
			//
			// AGREGO EL NODO ACCESSWAY
			// wobjXMLElement =
			// wobjXMLRequest.getDocument().createElement("ACCESSWAY");
			// XmlDomExtended.setText(wobjXMLElement, wvarAccessWay);
			// wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLElement);
			//
			// MHC
			// ------------------------------------------------------------------------
			// PIDO CONEXION A COLD VIEW
			wvarstep = 100;
			mobjCOLDview = new ColdViewApi();
			wvarstep = 102;
			if (ModGeneral.ColdViewConnect(wvarAccessWay, mobjCOLDview) == Obj.toInt("1")) {
				// HUBO UN ERROR EN LA CONEXION A COLD VIEW
				wvarstep = 103;
				Response.set("<Response><Estado resultado=\"false\"/></Response>");
				mobjCOLDview = (ColdViewApi) null;
				IAction_Execute = 1;
				/* TBD mobjCOM_Context.SetAbort() ; */
				return IAction_Execute;
			}
			wvarstep = 110;
			if (wvarDocKey.equals("")) {
				if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
					wvarDocKey = REPORTCOBRANZA;
				} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
					wvarDocKey = REPORTCOMISION;
				} else if (wvarCatalog.equals("LBA - EMISION")) {
					wvarDocKey = REPORTEMISION;
				}
			}
			wvarstep = 120;
			Issue issue = ModGeneral.obteinIssueInfo(mobjCOLDview, wvarIssueItemNbr, wvarCatalog, wvarIssueItemNbr,
					wvarDateIssue, wvarIdxBusqueda, wvarQueryClause, wvarDocKey, dateFrom, dateTo, wvarPageNbr);

			wvarstep = 121;

			Page page = mobjCOLDview.getStruc().getPage(issue, "ISO-8859-1");
			// final String pdfMail = mobjCOLDview.struc.sendPDFEmail(issue, "",
			// "password", "mcendon@amco.com.ar",
			// "Mauro Cendón", issue.getNextPage(), "Mail de prueba", "Se
			// adjunta PDF", "noreply@amco.com.ar",
			// "AMCO", "reply@amco.com.ar", "mmb@amco.com.ar",
			// "jsantodomingo@amco.com.ar", false, false, false,
			// true, false, false, false, true);

			boolean isPrint = ("0".equals(wvarPDFNoPrint)) ? false : true;
			boolean isModify = ("0".equals(wvarPDFNoModify)) ? false : true;
			boolean isCopy = ("0".equals(wvarPDFNoCopy)) ? false : true;
			boolean isAnnot = ("0".equals(wvarPDFNoAnnots)) ? false : true;
			wvarstep = 130;

			logger.info("wvarSendTo" + wvarSendTo);
			//logger.info("numPage" + String.valueOf(issue.getNextPage()));
			logger.info("wvarSubject" + wvarSubject);
			logger.info("wvarMSGBody" + wvarMSGBody);
			logger.info("wvarFromAddress" + wvarFromAddress);
			logger.info("wvarFrom" + wvarFrom);
			logger.info("wvarReplyTo" + wvarReplyTo);
			logger.info("wvarCC" + wvarCC);
			logger.info("wvarBCC" + wvarBCC);
			logger.info("isPrint" + String.valueOf(isPrint));
			logger.info( "isModify" + String.valueOf(isModify));
			logger.info("isCopy" + String.valueOf(isCopy));
			logger.info("isAnnot" + String.valueOf(isAnnot));
			
			
			String pdfMail = mobjCOLDview.struc.sendPDFEmail(issue, "", OWNERPASS, wvarSendTo, wvarPDFName,
					issue.getNextPage()-1, wvarSubject, wvarMSGBody, wvarFromAddress, wvarFrom, wvarReplyTo, wvarCC,
					wvarBCC, isPrint, isModify, isCopy, Boolean.TRUE, Boolean.FALSE, isAnnot, Boolean.FALSE,
					Boolean.TRUE);
			logger.info(pdfMail);
			mobjCOLDview.struc.closeIssue(issue);
			if (!pdfMail.equals("OK")){
				throw new Exception("Error " + pdfMail);
			}
			wvarstep = 140;
			//
			// EJECUTO RETRIEVE ISSUES
			// Set wobjClass =
			// mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
			// Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
			// Set wobjClass = Nothing
			// ModGeneral.RetrieveIssues(wobjXMLRequest.getDocument().getDocumentElement().toString(),
			// wvarResponse,
			// mobjCOLDview);
			// FIN MHC
			// --------------------------------------------------------------------
			//
			// ANALIZO LA RESPUESTA
			wvarstep = 110;
			wobjXMLRequest = null;
			//
			wobjXMLRequest = new XmlDomExtended();
			wobjXMLRequest.loadXML(wvarResponse.toString());
			//
			wvarstep = 120;
			// if
			// (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode("//Response/Estado/@resultado"))
			// .equals("false")) {
			// // DESCONECTO EL OBJETO COLD VIEW
			// ModGeneral.ColdViewDisconnect(
			// mobjCOLDview/*
			// * warning: ByRef value change will be lost.
			// */ );
			// //
			// Response.set(wobjXMLRequest.getDocument().getDocumentElement().toString());
			// IAction_Execute = 0;
			// /* TBD mobjCOM_Context.SetComplete() ; */
			// return IAction_Execute;
			// } else {
			// if (wvarPageNbr.equals("")) {
			// wvarPageNbr =
			// XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR));
			// }
			// }
			//

			wvarstep = 130;
			wobjXMLRequest = null;
			//
			// CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
			wvarstep = 140;
			wobjXMLRequest = new XmlDomExtended();
			//
			wvarstep = 150;
//			wobjXMLRequest.load(System.getProperty("user.dir") + ModGeneral.gcteOpenIssue);
			//
			// CARGO EL NODO IssueItemNbr EN EL ARCHIVO
			wvarstep = 160;
//			XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVISSUEITEMNBR), wvarIssueItemNbr);
			//
			// MHC - Se pasa la conexion a CV previa a la invocacion del
			// RetrieveIssues
			// PIDO CONEXION A COLD VIEW
			// wvarstep = 161
			// Set mobjCOLDview = CreateObject("ColdViewApi")
			// wvarstep = 162
			// If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
			// ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
			// wvarstep = 163
			// Response = "<Response><Estado resultado=""false""/></Response>"
			// Set mobjCOLDview = Nothing
			// IAction_Execute = 1
			// mobjCOM_Context.SetAbort
			// Exit Function
			// End If
			//
			// EJECUTO OPEN ISSUE DE COLD VIEW
			wvarstep = 170;
			// FIXME Convertir
			// mobjCOLDview.XML_Issue_Open(
			// wobjXMLRequest.getDocument().getDocumentElement().toString(),
			// wvarXMLOut );
			//
			// VERIFICO LA RESPUESTA DE COLD VIEW
//			wobjXMLResponse = new XmlDomExtended();
//			wobjXMLResponse.loadXML(wvarXMLOut);
			//
			wvarstep = 180;
			// if
			// (XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Status")).equals("-1"))
			// {
			// // DESCONECTO EL OBJETO COLD VIEW
			// ModGeneral.ColdViewDisconnect(
			// mobjCOLDview/*
			// * warning: ByRef value change will be lost.
			// */ );
			// //
			// Response.set(
			// "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO
			// RECUPERAR EL PDF.\" cv_message=\""
			// +
			// XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Event_Message"))
			// + "\" cv_errornbr= \""
			// +
			// XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Error_Nbr"))
			// + "\"/></Response>");
			// IAction_Execute = 0;
			// /* TBD mobjCOM_Context.SetComplete() ; */
			// return IAction_Execute;
			// } else {
			// // LEVANTO EL ISSUEID
			// wvarIssueId =
			// XmlDomExtended.getText(wobjXMLResponse.selectSingleNode(mcteParam_CVISSUEID));
			// }
			//
			wobjXMLRequest = null;
			//
			// CARGO EL ARCHIVO XML PARA EL SEND MAIL
			wvarstep = 190;
			// wobjXMLRequest = new XmlDomExtended();
			//
			wvarstep = 200;
			// wobjXMLRequest.load(System.getProperty("user.dir") +
			// ModGeneral.gctePDFSendMail);
			//
			// CARGO LOS NODOS CORRESPONDIENTES EN EL ARCHIVO
			// wvarstep = 210;
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVISSUEID),
			// wvarIssueId);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPAGENBR),
			// wvarPageNbr);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVSENDTO),
			// wvarSendTo);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVGIVENATTACHPDFNAME),
			// wvarPDFName);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVSUBJECT),
			// wvarSubject);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVMSGBODY),
			// wvarMSGBody);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVFROM),
			// wvarFrom);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVFROMADDRESS),
			// wvarFromAddress);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVREPLYTO),
			// wvarReplyTo);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CC),
			// wvarCC);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_BCC),
			// wvarBCC);
			//
			// CARGO LOS NODOS CORRESPONDIENTES AL ENCRIPTADO
			wvarstep = 215;
			// SI EL KEY LENGTH ES 0 NO SE PUEDE CONFIGURAR USER PWD NI MASTER
			// PWD
			// if (wvarPDFKeyLength.equals("0")) {
			// wvarPDFUsrPWD = "";
			// wvarPDFMstrPWD = "";
			// }
			//
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFUSRPWD),
			// wvarPDFUsrPWD);
			//
			// SI EL KEY LENGTH ES <> 0 Y MASTER PWD ES "" USO LA DEL ARCHIVO
			// if ((wvarPDFKeyLength.equals("0")) ||
			// (!wvarPDFMstrPWD.equals(""))) {
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFMSTRPWD),
			// wvarPDFMstrPWD);
			// }
			// //
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFKEYLENGTH),
			// wvarPDFKeyLength);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOPRINT),
			// wvarPDFNoPrint);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOMODIFY),
			// wvarPDFNoModify);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOCOPY),
			// wvarPDFNoCopy);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOANNOTS),
			// wvarPDFNoAnnots);
			// XmlDomExtended.setText(wobjXMLRequest.selectSingleNode(mcteParam_CVADDITIONALFILES),
			// wvarAdditionalFiles);
			//
			// EJECUTO ISSUE PAGE PDFSENDMAIL DE COLD VIEW
			wvarstep = 220;
			// FIXME Convertir
			// mobjCOLDview.XML_Issue_Page_PDFSendMail(
			// wobjXMLRequest.getDocument().getDocumentElement().toString(),
			// wvarXMLOut );

			//
			// VERIFICO LA RESPUESTA DE COLD VIEW
			// wobjXMLResponse = new XmlDomExtended();
			// wobjXMLResponse.loadXML(wvarXMLOut);
			//
			// if
			// (XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Status")).equals("-1"))
			// {
			// // DESCONECTO EL OBJETO COLD VIEW
			// ModGeneral.ColdViewDisconnect(
			// mobjCOLDview/*
			// * warning: ByRef value change will be lost.
			// */ );
			// //
			// wvarstep = 240;
			// Response.set(
			// "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO
			// RECUPERAR EL PDF.\" cv_message=\""
			// +
			// XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Event_Message"))
			// + "\" cv_errornbr= \""
			// +
			// XmlDomExtended.getText(wobjXMLResponse.selectSingleNode("//Error_Nbr"))
			// + "\"/></Response>");
			// IAction_Execute = 0;
			// /* TBD mobjCOM_Context.SetComplete() ; */
			// return IAction_Execute;
			// }
			//
			wobjXMLRequest = null;
			//
			wobjXMLResponse = null;
			wobjXMLRequest = null;
			//
			// DESCONECTO EL OBJETO COLD VIEW
			wvarstep = 250;
			ModGeneral.ColdViewDisconnect(mobjCOLDview);
			//
			//
			wvarstep = 280;
			Response.set("<Response><Estado resultado=\"true\" mensaje=\"MAIL ENVIADO\" /></Response>");
			//
			IAction_Execute = 0;
			/* TBD mobjCOM_Context.SetComplete() ; */
			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~
				wobjXMLRequest = null;
				//
				ModGeneral.ColdViewDisconnect(mobjCOLDview);
				//
				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
						wcteFnName, wvarstep,
						Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - "
								+ Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]",
						vbLogEventTypeError);
				Response.set("<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO ENVIAR EL EMAIL.\" /> "
						+ Err.getError().getDescription() + "</Response>");
				IAction_Execute = 1;
				/* TBD mobjCOM_Context.SetAbort() ; */
				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return IAction_Execute;
	}

}
