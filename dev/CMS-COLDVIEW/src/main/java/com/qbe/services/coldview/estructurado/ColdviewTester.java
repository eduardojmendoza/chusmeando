package com.qbe.services.coldview.estructurado;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.Exceptions.BadServiceCallException;
import com.amco.jcoldapi.cvobjects.BinaryOperator;
import com.amco.jcoldapi.cvobjects.IndexedResult;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.IssueInfo;
import com.amco.jcoldapi.cvobjects.Library;
import com.amco.jcoldapi.cvobjects.LogicalOperator;
import com.amco.jcoldapi.cvobjects.Page;
import com.amco.jcoldapi.cvobjects.Report;
import com.amco.jcoldapi.cvobjects.Task;
import com.amco.jcoldapi.cvobjects.Tuple;
import com.qbe.vbcompat.base64.Base64Encoding;
import com.qbe.vbcompat.string.StringHolder;

public class ColdviewTester {

	protected Logger logger = Logger.getLogger(getClass().getName());

	public int IAction_Execute(String request, StringHolder sh, String string) {
		ColdViewApi api = new ColdViewApi();
		if (ModGeneral.ColdViewConnect("OV", api) == 1) {
			sh.set("<Response><Estado resultado=\"false\"/></Response>");
			return 1;
		}

		try {
			logger.info("BEGIN COLDVIEW TESTS -------------------------------------------");
			// logger.info("01 listLibraries");
			// listLibraries(api);
			// logger.info("02 listLibrariesTasks");
			// listLibrariesTasks(api);
			// logger.info("03 listLibrariesTasksReports");
			// listLibrariesTasksReports(api);
			// logger.info("04 listLibrariesTasksReportsIssues");
			// listLibrariesTasksReportsIssues(api);
			// logger.info("05 listLibrariesTasksReportsIssuesPages");
			// listLibrariesTasksReportsIssuesPages(api);
			// logger.info("07 searchIssue145");
			// searchIssue145(api);
			logger.info(" ejemplo11");
			executeTestejemplo11(api);
//			logger.info("09 example");
//			example09(api);
//			logger.info("08 searchByQuery");
//			searchByQuery(api);
			// logger.info("09 processPDFBase64");
			// listLibrariesTasksReportsIssuesPagesPDFBase64(api);
			logger.info("10 printPDFBase64withSetPage");
			printPDFBase64withSetPage(api);
			logger.info("END COLDVIEW TESTS -------------------------------------------");
			return 0;
		} catch (UnknownHostException e) {
			logger.log(Level.SEVERE, "No route to host.", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Error writing to the socket.", e);
		} catch (BadServiceCallException e) {
			logger.log(Level.SEVERE, "Invalida parameters", e);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Other error.", e);
		}
		return 1;
	}

	/**
	 * Get a list of published libraries
	 *
	 * Ejemplo01
	 */
	public void listLibraries(final ColdViewApi api) throws IOException {
		List<Library> libraries = api.struc.getLibraries();
		logger.info("Allowed libraries: " + libraries);
	}

	/**
	 * Get a list of published libraries Prints a list of tasks from each
	 * library
	 * 
	 * Ejemplo02
	 */
	public void listLibrariesTasks(final ColdViewApi api) throws IOException, BadServiceCallException {
		final List<Library> libraries = api.struc.getLibraries();

		for (Iterator<Library> iter = libraries.iterator(); iter.hasNext();) {
			final Library library = (Library) iter.next();

			List<Task> tasks = api.struc.getTasks(library);
			logger.info("Library: " + library.getName());
			logger.info("Tasks: " + tasks);
		}
	}

	/**
	 * Get a list of published libraries Get list of tasks from a given library
	 * Get list of reports from a given library and task
	 * 
	 * Ejemplo03
	 */
	public void listLibrariesTasksReports(final ColdViewApi api) throws IOException, BadServiceCallException {
		final List<Library> libraries = api.struc.getLibraries();

		for (Iterator<Library> iter = libraries.iterator(); iter.hasNext();) {
			final Library library = (Library) iter.next();

			final List<Task> tasks = api.struc.getTasks(library);

			for (Iterator<Task> iterator = tasks.iterator(); iterator.hasNext();) {
				final Task task = (Task) iterator.next();
				List<Report> reports = api.struc.getReports(library, task);
				logger.info("Library: " + library.getName());
				logger.info("Task: " + task.getName());
				logger.info("Reports: " + reports);
			}
		}

	}

	/**
	 * Get a list of published libraries Get list of tasks from a given library
	 * Get list of reports from a given library and task Look for
	 * R2016022520160302 Ejemplo04
	 */
	public void listLibrariesTasksReportsIssues(final ColdViewApi api) throws IOException, BadServiceCallException {
		final List<Library> libraries = api.struc.getLibraries();
		final Iterator<Library> librariesIter = libraries.iterator();
		if (librariesIter.hasNext()) {
			final Library library = librariesIter.next();

			final List<Task> tasks = api.struc.getTasks(library);
			final Iterator<Task> tasksIter = tasks.iterator();
			if (tasksIter.hasNext()) {
				final Task task = tasksIter.next();

				final List<Report> reports = api.struc.getReports(library, task);

				for (Iterator<Report> reportsIter = reports.iterator(); reportsIter.hasNext();) {
					final Report report = reportsIter.next();
					final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
							"R2015120120160131");

					for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
						IssueInfo info = (IssueInfo) issueInfosIter.next();
						logger.info("Library: " + library.getName());
						logger.info("Task: " + task.getName());
						logger.info("Reports: " + reports);
						logger.info("Issue name:" + info.getName());
						logger.info("Issue total pages: " + info.getPagesCount());
						logger.info("Issue publsh date:" + info.getDate());
						logger.info("Issue status:" + info.getStatusLevel());
					}
				}
			}
		}
	}

	/**
	 * Get a list of published libraries Get list of tasks from a given library
	 * Get list of reports from a given library and task Look for
	 * R2015120120160131 Print first page
	 * 
	 * Ejemplo05
	 */
	public void listLibrariesTasksReportsIssuesPages(final ColdViewApi api)
			throws IOException, BadServiceCallException {
		/*
		 * Get a list of published libraries
		 */
		final List<Library> libraries = api.struc.getLibraries();
		for (Library library : libraries) {
			/*
			 * Get list of tasks from a given library
			 */
			final List<Task> tasks = api.struc.getTasks(library);

			for (Task task : tasks) {
				/*
				 * Get list of reports from a give library and task
				 */
				final List<Report> reports = api.struc.getReports(library, task);
				for (Report report : reports) {

					final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
							"R2015120120160131");

					for (IssueInfo info : issueInfos) {

						logger.info("Library: " + library.getName());
						logger.info("Task: " + task.getName());
						logger.info("Reports: " + reports);
						logger.info("Issue name:" + info.getName());
						logger.info("Issue total pages: " + info.getPagesCount());
						logger.info("Issue publsh date:" + info.getDate());
						logger.info("Issue status:" + info.getStatusLevel());

						final Issue issue = api.struc.getIssue(library, info);
						logger.info("Issue: " + issue);

						/*
						 * Retrieve the first page of an issue
						 */
						// for (int i = 0; i <
						// issue.getIssueInfo().getPagesCount(); i++) {
						Page page = api.struc.getPage(issue, "ISO-8859-1");
						logger.info("Contenido: " + page.getContent());
						// }
						/*
						 * Close issue
						 */
						boolean closeOk = api.struc.closeIssue(issue);
						logger.info("Close Issue" + closeOk);

					}
				}
			}
		}
	}

	/**
	 * Ejemplo de búsqueda. Cuando GetXMLPDF recibe este request:
	 * 
	 * <pre>
		 <Request>
		    <CATALOG>LBA - AIS COBRANZAS</CATALOG>
		    <PAGENBR/>
		    <ISSUEITEMNBR>1</ISSUEITEMNBR>
		    <DATEISSUE>2015-12-02</DATEISSUE>
		    <IDX_BUSQUEDA>751976807</IDX_BUSQUEDA>
		    <IssueDateFrom>2015-12-02</IssueDateFrom>
		    <IssueDateTo>2015-12-02</IssueDateTo>
		    <QueryClause><![CDATA[(factura = 751976807)]]></QueryClause>
		    <DocKey>LBA_CHOV</DocKey>
		    <PDFNoprint>0</PDFNoprint>
		    <PDFNomodify>-1</PDFNomodify>
		    <PDFNocopy>0</PDFNocopy>
		    <PDFNoannots>-1</PDFNoannots>
		    <ACCESSWAY>OV</ACCESSWAY>
		    <PDFUSRPWD/>
		</Request>
	 * </pre>
	 * 
	 * llama a ModGeneral.retrieveIssues que genera otro XML antes de llamar al
	 * componente:
	 * 
	 * <pre>
	 * 	
		<Request>
		<Cat_LongName>LBA - AIS COBRANZAS</Cat_LongName>
		<TaskName><![CDATA[<Todos>]]></TaskName>
		<DocKey>LBA_CHOV</DocKey>
		<IssueRetrieveMode>R</IssueRetrieveMode>
		<IssueDateFrom>2015-12-02</IssueDateFrom>
		<IssueDateTo>2015-12-02</IssueDateTo>
		<QueryClause>
			<![CDATA[(factura = 751976807)]]>
		</QueryClause>
		<SearchLimiter>0</SearchLimiter>
		<ResultsLimit>1</ResultsLimit>
	</Request>
	 * </pre>
	 * 
	 * En este ejemplo implementamos la búsqueda de este caso
	 */

	public void searchIssue145(final ColdViewApi api) throws IOException, BadServiceCallException {
		/*
		 * Get a list of published libraries
		 */

		final List<Library> libraries = api.struc.getLibraries();

		for (Library library : libraries) {
			/*
			 * Get list of tasks from a given library
			 */
			final List<Task> tasks = api.struc.getTasks(library);
			for (Task task : tasks) {

				/*
				 * Get list of reports from a given library and task
				 */
				final List<Report> reports = api.struc.getReports(library, task);
				for (Report report : reports) {

					final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
							"R2015120220151202");

					for (IssueInfo info : issueInfos) {
						logger.info("Library: " + library.getName());
						logger.info("Task: " + task.getName());
						logger.info("Report name:" + report.getName());
						logger.info("Issue name:" + info.getName());

						if (
						// report.getName().equals(SEARCH.REPORT) &&
						// info.getName().equals(SEARCH.ISSUE_INFO) &&
						library.getName().equals("AISCOB") && task.getName().equals("<Todos>")) { // Es
																									// el
																									// nombre
																									// corto
																									// de
																									// LBA
																									// -
																									// AIS
																									// COBRANZAS
							logger.info("Encontrado:" + info.getName());

							Issue issue = api.struc.getIssue(library, info);
							logger.info("Issue: " + issue);

							/*
							 * Perform an indexed search on this issue
							 */
							final List<Tuple> tuples = new ArrayList<Tuple>();

							final Tuple tuple = new Tuple("factura", BinaryOperator.EQUAL, "751976807",
									LogicalOperator.NOTHING);
							tuples.add(tuple);

							final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

							/*
							 * Prints page number and the result fields
							 */
							int pageNum = 1;
							for (IndexedResult result : indexedResults) {
								pageNum = result.getPageNumber();
								logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
							}

							/*
							 * Go to last page searched
							 */
							issue.setNextPage(pageNum);
							final Page page = api.struc.getPage(issue, "ISO-8859-1");
							logger.info(page.toString());
							final File folderOut = new File("output");
							if (folderOut.exists() || folderOut.mkdirs()) {
								final File output = new File(folderOut,
										"Example07_" + System.currentTimeMillis() + ".pdf");
								final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false,
										false, true, false, false, false, true);
								final FileOutputStream fos = new FileOutputStream(output);
								fos.write(Base64Encoding.decode(pdfBase64));
								fos.close();

							} else {
								logger.log(Level.SEVERE, "Error creando la carpeta " + folderOut.getAbsolutePath());

							}

							/*
							 * Close issue
							 */
							boolean closeOk = api.struc.closeIssue(issue);
							logger.info("" + closeOk);
						}
					}
				}
			}
		}
	}

	public void example09(final ColdViewApi api) throws IOException, BadServiceCallException {
		/*
		 * Get a list of published libraries
		 */

		Library library = null;
		Task task = null;
		Report report = null;
		IssueInfo info = null;

		final List<Library> libraries = api.struc.getLibraries();
		for (Iterator<Library> librariesIter = libraries.iterator(); librariesIter.hasNext();) {
			library = librariesIter.next();
			if (library.getName().equals("AISCOB")) {
				logger.info("Library: " + library.getName());
				break;
			}
		}

		final List<Task> tasks = api.struc.getTasks(library);
		for (Iterator<Task> tasksIter = tasks.iterator(); tasksIter.hasNext();) {
			task = tasksIter.next();
			if (task.getName().equals("<Todos>")) {
				logger.info("Task: " + task.getName());
				break;
			}
		}

		final List<Report> reports = api.struc.getReports(library, task);
		for (Iterator<Report> reportsIter = reports.iterator(); reportsIter.hasNext();) {
			report = reportsIter.next();
			if (report.getName().equals("LBA_CHOV")) {
				logger.info("Report name:" + report.getName());
				break;
			}

		}

		final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2015110520151106");
		for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
			info = issueInfosIter.next();

			logger.info("Issue name:" + info.getName());
			logger.info("Encontrado:" + info.getName());
			Issue issue = api.struc.getIssue(library, info);
			logger.info("Issue: " + issue);

			final List<Tuple> tuples = new ArrayList<Tuple>();
			Tuple tuple = new Tuple("INDEXTEXT01T", BinaryOperator.LIKE, "751843262", LogicalOperator.NOTHING);
			tuples.add(tuple);

			final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

			int pageNum = 1;
			for (Iterator<IndexedResult> iter = indexedResults.iterator(); iter.hasNext();) {
				IndexedResult result = iter.next();
				pageNum = result.getPageNumber();

				logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
			}

			issue.setNextPage(pageNum);
			Page page = api.struc.getPage(issue, "ISO-8859-1");
			logger.info(page.toString());

			// Obtengo el pdf;

			final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false, true, false, false,
					false, true);

			logger.info("pdf base64:" + pdfBase64.substring(0,30));

			/*
			 * Close issue
			 */
			boolean closeOk = api.struc.closeIssue(issue);
			logger.info("" + closeOk);
		}

	}

	public void searchByQuery(final ColdViewApi api) throws IOException, BadServiceCallException {

		/*
		 * 
		 * Get a list of published libraries
		 * 
		 */

		Library library = new Library("AISCOB", "LBA - AIS COBRANZAS"); // <Cat_LongName>LBA
																		// -
																		// AIS
																		// COBRANZAS</Cat_LongName>

		Task task = new Task("<Todos>"); // <TaskName><![CDATA[<Todos>]]></TaskName>

		Report report = new Report("LBA_CHOV", "LBA_CHOV", "", ""); // <DocKey>LBA_CHOV</DocKey>

		final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2015110520151106"); // <IssueRetrieveMode>R</IssueRetrieveMode>

		// <IssueDateFrom>2015-12-02</IssueDateFrom>

		// <IssueDateTo>2015-12-02</IssueDateTo>
		for (IssueInfo info : issueInfos) {
			logger.info("Issue name:" + info.getName());
			// if (info.getName().equals("Subdiario de Cobranzas")) {

			logger.info("Issue name:" + info.getName());

			logger.info("Encontrado:" + info.getName());

			Issue issue = api.struc.getIssue(library, info);

			logger.info("Issue: " + issue);

			final List<Tuple> tuples = new ArrayList<Tuple>(); // <QueryClause>

			Tuple tuple = new Tuple("INDEXTEXT01T", BinaryOperator.LIKE, "751843262", LogicalOperator.NOTHING); // <![CDATA[(factura
																												// =
																												// 751976807)]]>
			tuples.add(tuple); // </QueryClause>

			final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

			int pageNum = 0;

			for (IndexedResult result : indexedResults) {
				pageNum = result.getPageNumber();
				logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());

			}
			issue.setNextPage(pageNum);
			Page page = api.struc.getPage(issue, "ISO-8859-1");
			String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false, true, false, false,
					false, true);
			logger.info("pdfBase64 deco: " + pdfBase64.substring(0, 30));
			logger.info("contenido de la pagina: " + page.getContent());
			boolean closeOk = api.struc.closeIssue(issue);

			logger.info("" + closeOk);

			// }

		}
	}

	/**
	 * Get a list of published libraries Get list of tasks from a given library
	 * Get list of reports from a given library and task Look for
	 * R2015120120160131 Print first page Create a PFDbase64 Ejemplo05
	 */
	public void listLibrariesTasksReportsIssuesPagesPDFBase64(final ColdViewApi api)
			throws IOException, BadServiceCallException {
		/*
		 * Get a list of published libraries
		 */
		final List<Library> libraries = api.struc.getLibraries();
		for (Library library : libraries) {
			/*
			 * Get list of tasks from a given library
			 */
			final List<Task> tasks = api.struc.getTasks(library);

			for (Task task : tasks) {
				/*
				 * Get list of reports from a give library and task
				 */
				final List<Report> reports = api.struc.getReports(library, task);
				for (Report report : reports) {

					final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
							"R2015120120160131");

					for (IssueInfo info : issueInfos) {

						logger.info("Library: " + library.getName());
						logger.info("Task: " + task.getName());
						logger.info("Reports: " + reports);
						logger.info("Issue name:" + info.getName());
						logger.info("Issue total pages: " + info.getPagesCount());
						logger.info("Issue publsh date:" + info.getDate());
						logger.info("Issue status:" + info.getStatusLevel());

						final Issue issue = api.struc.getIssue(library, info);
						logger.info("Issue: " + issue);

						/*
						 * Retrieve the first page of an issue
						 */
						// for (int i = 0; i <
						// issue.getIssueInfo().getPagesCount(); i++) {
						Page page = api.struc.getPage(issue, "ISO-8859-1");
						logger.info("Contenido: " + page.getContent());
						// }

						// Obtengo el pdf;
						final File folderOut = new File("output");
						if (folderOut.exists() || folderOut.mkdirs()) {
							final File output = new File(folderOut, "Example09_" + System.currentTimeMillis() + ".pdf");
							final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false,
									true, false, false, false, true);
							final FileOutputStream fos = new FileOutputStream(output);
							fos.write(Base64.decodeBase64(pdfBase64));
							fos.close();
							logger.info("pdf creado con exito " + folderOut.getAbsolutePath());
							break;
						} else {
							logger.info("Error creando la carpeta " + folderOut.getAbsolutePath());

						}

						/*
						 * Close issue
						 */
						boolean closeOk = api.struc.closeIssue(issue);
						logger.info("Close Issue" + closeOk);

					}
				}
			}
		}
	}

	/**
	 * Get a list of published libraries Get list of tasks from a given library
	 * Get list of reports from a given library and task Look for
	 * R2015120120160131 Print page Create a PFDbase64
	 * 
	 */
	public void printPDFBase64withSetPage(final ColdViewApi api) throws IOException, BadServiceCallException {

		/*
		 * Get a list of published libraries
		 */
		final List<Library> libraries = api.struc.getLibraries();
		boolean isProccess = false;
		for (Library library : libraries) {
			/*
			 * Get list of tasks from a given library
			 */

			final List<Task> tasks = api.struc.getTasks(library);

			for (Task task : tasks) {
				/*
				 * Get list of reports from a give library and task
				 */
				final List<Report> reports = api.struc.getReports(library, task);
				for (Report report : reports) {

					final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false,
							"R2015120120160131");

					for (IssueInfo info : issueInfos) {

						if (!isProccess && info.getPagesCount() > 0 && "AISCOMI".equals(library.getName())
								&& "C U E N T A S   C O R R I E N T E S".equals(info.getName())) {
							final Issue issue = api.struc.getIssue(library, info);
							logger.info("Library: " + library.getName());
							logger.info("Task: " + task.getName());
							logger.info("Reports: " + reports);
							logger.info("Report: " + report.getName());
							logger.info("Issue name:" + info.getName());
							logger.info("Issue total pages: " + info.getPagesCount());
							logger.info("Issue publsh date:" + info.getDate());
							logger.info("Issue status:" + info.getStatusLevel());
							logger.info("Issue: " + issue);

							// obtengo la pagina con el numero de la misma
							logger.info("Obtengo la página con el número de la misma");
							int pgNext = 1;
							issue.setNextPage(api.getStruc().gotoPage(issue, pgNext));
							Page page = api.getStruc().getPage(issue, "ISO-8859-1");
							logger.info("Contenido: " + page.getContent());
							// obtengo el pdfBase64

							logger.info("inicio proceso de getPDFBase64");
							final String pdfBase64 = api.struc.getPDFBase64(issue, "", "password", false, false, false,
									true, false, false, false, true);
							logger.info("pdfBase64 deco: " + pdfBase64.substring(0, 6));
							boolean closeOk = api.struc.closeIssue(issue);
							logger.info("Close Issue" + closeOk);
							isProccess = true;
							break;
						}
					}
					break;
				}
				break;
			}
		}
	}
	
	public void executeTestejemplo11(final ColdViewApi api) throws IOException, BadServiceCallException {
		/*
		 * Get a list of published libraries
		 */
		
			Library library = new Library("LBAEMI", "LBA - EMISION");
			Task task = new Task("<Todos>");
			Report report = new Report("GRP0012", "GRP0012", "", "");
			IssueInfo info = null;

			final List<IssueInfo> issueInfos = api.struc.getIssueInfos(library, task, report, false, "R2013080120130830");

			for (Iterator<IssueInfo> issueInfosIter = issueInfos.iterator(); issueInfosIter.hasNext();) {
				info = issueInfosIter.next();
				logger.info("Issue name:" + info.getName());
				logger.info("Encontrado:" + info.getName());
				Issue issue = api.struc.getIssue(library, info);
				logger.info("Issue: " + issue);

				final List<Tuple> tuples = new ArrayList<Tuple>();
				//tuples.add(new Tuple("INDEXTEXT01T", BinaryOperator.LIKE, "100223914", LogicalOperator.NOTHING));
				tuples.add(new Tuple("INDEXTEXT03T", BinaryOperator.EQUAL, "AUS1", LogicalOperator.AND));
				tuples.add(new Tuple("INDEXTEXT05N", BinaryOperator.EQUAL, "101862284", LogicalOperator.NOTHING));

				final List<IndexedResult> indexedResults = api.struc.getIndexResults(issue, tuples);

				int pageNum = 0;
				for (Iterator<IndexedResult> iter = indexedResults.iterator(); iter.hasNext();) {
					IndexedResult result = iter.next();
					pageNum = result.getPageNumber();

					logger.info("Page number '" + pageNum + "' result fields: " + result.getFields());
				}

				boolean closeOk = api.struc.closeIssue(issue);
				logger.info("" + closeOk);
			}
		
	}

}
