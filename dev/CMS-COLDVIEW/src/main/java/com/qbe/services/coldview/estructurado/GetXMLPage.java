package com.qbe.services.coldview.estructurado;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.amco.jcoldapi.ColdViewApi;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.coldview.estructurado.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;

import org.w3c.dom.DOMException;

import diamondedge.swing.*;

/**
 * ***************************************************************** COPYRIGHT.
 * HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT HAS BEEN
 * PROVIDED. NO PART OF IT IS TO BE REPRODUCED, DISASSEMBLED, TRANSMITTED,
 * STORED IN A RETRIEVAL SYSTEM NOR TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE
 * IN ANY WAY OR FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 * CONSENT OF HSBC HOLDINGS PLC.
 * ***************************************************************** Module Name
 * : GetXMLPDF File Name : GetXMLPDF.cls Creation Date: 04/11/2003 Programmer :
 * Muzzupappa - Goncalves Abstract : Obtiene un PDF en formato XML de la base de
 * Cold View *****************************************************************
 * 2008-08-15 - FJO / MC / DA Objeto de ColdView
 */

public class GetXMLPage implements VBObjectClass {
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbaA_ECold.GetXMLPage";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_CATALOG = "//CATALOG";
	static final String mcteParam_PAGENBR = "//PAGENBR";
	static final String mcteParam_CANTPAGE = "//CANTPAGE";
	static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
	static final String mcteParam_DATEISSUE = "//DATEISSUE";
	static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
	static final String mcteParam_DOCKEY = "//DocKey";
	static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
	static final String mcteParam_CVDATETO = "//IssueDateTo";
	static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
	static final String mcteParam_PDFNOPRINT = "//PDFNoprint";
	static final String mcteParam_PDFNOMODIFY = "//PDFNomodify";
	static final String mcteParam_PDFNOCOPY = "//PDFNocopy";
	static final String mcteParam_PDFNOANNOTS = "//PDFNoannots";
	static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
	/**
	 * Parametros Cold View
	 */
	static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
	static final String mcteParam_CVPAGENBR = "//PageNbr";

	static final String mcteParam_CVISSUEID = "//IssueID";
	static final String mcteParam_CVPDFUSRPWD = "//PDFUsrPWD";
	static final String mcteParam_CVPDFMSTRPWD = "//PDFMstrPWD";
	static final String mcteParam_CVPDFKEYLENGTH = "//PDFKeyLength";
	static final String mcteParam_CVPDFNOPRINT = "//PDFNoprint";
	static final String mcteParam_CVPDFNOMODIFY = "//PDFNomodify";
	static final String mcteParam_CVPDFNOCOPY = "//PDFNocopy";
	static final String mcteParam_CVPDFNOANNOTS = "//PDFNoannots";
	
	private static final String REPORTCOBRANZA = "LBA_CHEQ";

	private static final String REPORTCOMISION = "GR44536";

	private static final String REPORTEMISION = "GRP0012";
	
	public ColdViewApi mobjCOLDview = new ColdViewApi();
	/**
	 * Objetos del FrameWork
	 */
	private Object mobjCOM_Context = null;
	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	/**
	 * *****************************************************************
	 * Function : IAction_Execute Abstract : Busca en la base de Cold View la
	 * emision de la fecha pasada como parametro, abre la misma y obtiene el PDF
	 * correspondiente Synopsis : IAction_Execute(ByVal Request As String,
	 * Response As String, ByVal ContextInfo As String) As Long
	 * *****************************************************************
	 */
	@Override
	public int IAction_Execute(String request, StringHolder response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLResponse = null;
		String wvarXMLOut = "";
		String wvarCatalog = "";
		String wvarIssueItemNbr = "";
		String wvarPageNbr = "";
		String wvarIssueId = "";
		String wvarDateIssue = "";
		String wvarIDX_Busqueda = "";
		String wvarDocKey = "";
		String queryClause = "";
		String dateTo = "";
		String dateFrom = "";
		int wvarMaxPages = 0;
		Variant wvarResponse = new Variant();
		String wvarPDFUsrPWD = "";
		String wvarPDFMstrPWD = "";
		String wvarPDFKeyLength = "";
		String wvarPDFNoPrint = "";
		String wvarPDFNoModify = "";
		String wvarPDFNoCopy = "";
		String wvarPDFNoAnnots = "";
		String wvarCantPage = "";
		String wvarAccessWay = "";
		int wvarstep = 0;

		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			// CARGO LOS PARAMETROS DE ENTRADA
			wvarstep = 10;
			wobjXMLRequest = new XmlDomExtended();
			//
			wobjXMLRequest.loadXML(request);
			wvarCatalog = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CATALOG));
			wvarPageNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR));
			wvarIssueItemNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ISSUEITEMNBR));
			wvarCantPage = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CANTPAGE));
			wvarDateIssue = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DATEISSUE));
			wvarIDX_Busqueda = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IDX_BUSQUEDA));
			wvarDocKey = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOCKEY));
			dateFrom = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATEFROM));
			dateTo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATETO));
			//
			queryClause = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE));
			wvarPDFNoPrint = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOPRINT));
			wvarPDFNoModify = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOMODIFY));
			wvarPDFNoCopy = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOCOPY));
			wvarPDFNoAnnots = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PDFNOANNOTS));
			// Verifico que exista el nodo <ACCESSWAY>
			// On Error Resume Next (optionally ignored)
			wvarAccessWay = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACCESSWAY));
			// if (Err.getError().getNumber() != 0) {
			// wvarAccessWay = "";
			// }

			//
			wvarstep = 20;
			//
			
			wvarstep = 23;
			ColdViewApi mobjCOLDview = new ColdViewApi();
			wvarstep = 24;
			if (ModGeneral.ColdViewConnect(wvarAccessWay, mobjCOLDview) == 1) {
				// HUBO UN ERROR EN LA CONEXION A COLD VIEW
				// FIXME Agregar detalle del error acá
				wvarstep = 25;
				response.set("<Response><Estado resultado=\"false\"/></Response>");
				mobjCOLDview = (ColdViewApi) null;
				IAction_Execute = 1;
				return IAction_Execute;
			}
			
			if (wvarDocKey.equals("")){
				if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
					wvarDocKey = REPORTCOBRANZA;
				} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
					wvarDocKey = REPORTCOMISION;
				} else if (wvarCatalog.equals("LBA - EMISION")) {
					wvarDocKey = REPORTEMISION;
				}
			}
			wvarstep = 26;
			String pdfString = ModGeneral.obteinIssuePages(mobjCOLDview, wvarCatalog, wvarDateIssue, wvarIDX_Busqueda,
					queryClause, wvarDocKey, dateFrom, dateTo, wvarPageNbr, wvarCantPage);
			
			wvarstep = 60;
			

			//
			wvarstep = 170;
			createXML(response, wobjXMLResponse, pdfString);

			ModGeneral.ColdViewDisconnect(mobjCOLDview);
			//
			IAction_Execute = 0;
			/* TBD mobjCOM_Context.SetComplete() ; */
			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~

				//
				ModGeneral.ColdViewDisconnect(
						mobjCOLDview/*
									 * warning: ByRef value change will be lost.
									 */ );
				//
				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
						wcteFnName, wvarstep,
						Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - "
								+ Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]",
						vbLogEventTypeError);

				response.set("<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO CREAR EL XML.\" /> "
						+ Err.getError().getDescription() + "</Response>");
				IAction_Execute = 1;
				/* TBD mobjCOM_Context.SetAbort() ; */
				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return IAction_Execute;
	}

	/**
	 * 
	 * Method that create a XML with the PDF string
	 * 
	 * @param response
	 * @param wobjXMLResponse
	 * @param pdfString
	 * @throws DOMException
	 * @throws XmlDomExtendedException
	 */
	public static void createXML(StringHolder response, XmlDomExtended wobjXMLResponse, String pdfString)
			throws DOMException, XmlDomExtendedException {

		response.set("<Response><Estado resultado='true' mensaje='' /></Response>");

		wobjXMLResponse = new XmlDomExtended();
		wobjXMLResponse.loadXML(response.getValue());

		org.w3c.dom.Element wobjEle = wobjXMLResponse.getDocument().createElement("TEXTO");
		wobjXMLResponse.selectSingleNode("//Response").appendChild(wobjEle);

		XmlDomExtended.setText(wobjEle, "<![CDATA[" + pdfString + "]]>");

		response.set(XmlDomExtended.prettyPrint(wobjXMLResponse.getDocument().getDocumentElement()));

	}
}
