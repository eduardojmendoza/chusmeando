package com.qbe.services.coldview.estructurado;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.amco.jcoldapi.ColdViewApi;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.coldview.estructurado.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  *****************************************************************
 *  COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 *  THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
 *  HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
 *  DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
 *  TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
 *  FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 *  CONSENT OF HSBC HOLDINGS PLC.
 *  *****************************************************************
 *  Module Name : RetrieveIssues
 *  File Name : RetrieveIssues.cls
 *  Creation Date: 30/10/2003
 *  Programmer : Muzzupappa - Goncalves
 *  Abstract : Esta clase se llama tanto desde GetXMLPDF como desde
 *  una pagina ASP. Si existe el Nodo KEEPCONNECTED en el Request, ejecuta
 *  el metodo RetrieveIssues de ColdView, sino ejecuta RetrieveIssuesWithSearch
 *  *****************************************************************
 *  2008-08-15 - FJO / MC / DA
 * Objeto de ColdView
 */

public class RetrieveIssues implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.RetrieveIssues";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CATALOG = "//CATALOG";
  static final String mcteParam_DATEISSUE = "//DATEISSUE";
  static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
  static final String mcteParam_IDX_SISTEMA = "//IDX_SISTEMA";
  static final String mcteParam_PAGENBR = "//PAGENBR";
  static final String mcteParam_TIPO_AVISO = "//TIPO_AVISO";
  static final String mcteParam_DOCKEY = "//DOCKEY";
  static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
  static final String mcteParam_CVDATETO = "//IssueDateTo";
  static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
  static final String mcteParam_CVDOCKEY = "//DocKey";
  static final String mcteParam_KEEPCONNECTED = "//KEEPCONNECTED";
  static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
  static final String mcteParam_QueryClause = "//QueryClause";
  public ColdViewApi mobjCOLDview = new ColdViewApi();
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "RetrieveIssues";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Si existe el nodo KEEPCONNECTED ejecuta el metodo
   *  RetrieveIssues de la API de ColdView, sino ejecuta el metodo
   *  RetrieveIssuesWithSearch
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    String wvarXMLOut = "";
    String wvarCatalog = "";
    String wvarDateIssue = "";
    String wvarIDX_Busqueda = "";
    String wvarQuerySearch = "";
    String wvarIDX_Sistema = "";
    String wvarPageNbr = "";
    String wvarTipo_Aviso = "";
    String wvarDocKey = "";
    int wvarstep = 0;
    String wvarresult = "";
    boolean wvarKeepConnected = false;
    String wvarAccessWay = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // CARGO LOS PARAMETROS DE ENTRADA
      wvarstep = 10;
      wobjXMLRequest = new XmlDomExtended();
      //
      wobjXMLRequest.loadXML( Request );
      wvarCatalog = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CATALOG )  );
      wvarDateIssue = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DATEISSUE )  );
      wvarIDX_Busqueda = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IDX_BUSQUEDA )  );
      wvarQuerySearch = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_QueryClause )  == (org.w3c.dom.Node) null) )
      {
        wvarQuerySearch = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_QueryClause )  );
      }
      wvarDocKey = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_DOCKEY )  == (org.w3c.dom.Node) null) )
      {
        wvarDocKey = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOCKEY )  );
      }
      else if( ! (wobjXMLRequest.selectSingleNode( mcteParam_CVDOCKEY )  == (org.w3c.dom.Node) null) )
      {
        wvarDocKey = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CVDOCKEY )  );
      }
      //
      if( wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR )  == (org.w3c.dom.Node) null )
      {
        wvarPageNbr = "";
      }
      else
      {
        wvarPageNbr = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR )  );
      }
      //
      wvarstep = 20;
      //
      if( wobjXMLRequest.selectSingleNode( mcteParam_KEEPCONNECTED )  == (org.w3c.dom.Node) null )
      {
        wvarKeepConnected = false;
      }
      else
      {
        wvarKeepConnected = true;
      }
      //
      //Verifico que exista el nodo <ACCESSWAY>
      // On Error Resume Next (optionally ignored)
      wvarAccessWay = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ACCESSWAY )  );
      if( Err.getError().getNumber() != 0 )
      {
        wvarAccessWay = "";
      }

      //
      wobjXMLRequest = null;
      //
      // CARGO EL ARCHIVO XML PARA EL ISSUE CORRESPONDIENTE
      wvarstep = 30;
      wobjXMLRequest = new XmlDomExtended();
      //
      wvarstep = 40;
      
      if( wvarCatalog.equals( "LBA - AIS COBRANZAS" ) )
      {
        wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteRetIssuesAISCOB );
      }
      else if( wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
      {
        wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteRetIssuesAISCOM );
      }
      else if( wvarCatalog.equals( "LBA - EMISION" ) )
      {
        wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteRetIssuesAISOPER );
      }
      //CARGO el DOCKEY
      wvarstep = 45;
      if( !wvarDocKey.equals( "" ) )
      {
        XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVDOCKEY ) , wvarDocKey );
      }
      // CARGO LOS NODOS FECHA_DESDE, FECHA_HASTA Y QUERY EN EL ARCHIVO
      wvarstep = 50;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVDATEFROM ) , wvarDateIssue );
      wvarstep = 51;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVDATETO ) , wvarDateIssue );
      //
      wvarstep = 52;
      
      if( wvarCatalog.equals( "LBA - AIS COBRANZAS" ) )
      {
        wvarstep = 53;
        XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVQUERYCLAUSE ) , Strings.replace( ModGeneral.gcteQueryClause, ModGeneral.gcteQueryParameter, wvarIDX_Busqueda ) );
      }
      else if( wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
      {
        wvarstep = 54;
        XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVQUERYCLAUSE ) , wvarQuerySearch );
      }
      //
      wvarstep = 60;
      // PIDO CONEXION A COLD VIEW
      mobjCOLDview = new ColdViewApi();
      if( ModGeneral.ColdViewConnect( wvarAccessWay, mobjCOLDview ) == Obj.toInt( "1" ) )
      {
        // HUBO UN ERROR EN LA CONEXION A COLD VIEW
        Response.set( "<Response><Estado resultado=\"false\"/></Response>" );
        mobjCOLDview = (ColdViewApi) null;
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        return IAction_Execute;
      }
      //
      // EJECUTO RETRIEVE ISSUES DE COLD VIEW
      wvarstep = 70;
      //
      if( wvarKeepConnected && (Strings.len( Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_QueryClause )  ) ) ) == 0) && (!wvarPageNbr.equals( "" )) )
      {
        // LA LLAMADA SE HACE DESDE GETXMLPDF, EJECUTO RETRIEVEISSUES
          // FIXME Convertir
          //        mobjCOLDview.XML_RetrieveIssues( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      }
      else
      {
        // LA LLAMADA SE HACE DESDE LA PAGINA, EJECUTO RETRIEVEISSUESWITHSEARCH
          // FIXME Convertir
          //        mobjCOLDview.XML_RetrieveIssuesWithSearch( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      }
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      wvarstep = 80;
      //
      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Status" )  ).equals( "-1" ) )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE ENCONTRARON DATOS PARA EL RANGO DE FECHAS.\"  cv_message=\"" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Event_Message" )  ) + "\" cv_errornbr= \"" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Error_Nbr" )  ) + "\"/></Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]" + "[XML Send:" + wobjXMLRequest.getDocument().getDocumentElement().toString() + "]", vbLogEventTypeError );
        //
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        /*TBD mobjCOM_Context.SetComplete() ;*/
        IAction_Execute = 0;
        return IAction_Execute;
      }
      //
      // CARGO EL XSL PARA TRANSFORMAR LA RESPUESTA
      wvarstep = 90;
      wobjXSLResponse = new XmlDomExtended();
      //

      
      if( wvarCatalog.equals( "LBA - AIS COBRANZAS" ) )
      {
        wobjXSLResponse.load( System.getProperty("user.dir") + ModGeneral.gcteRetIssuesAISCOB_XSL );
      }
      else if( wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
      {
        wobjXSLResponse.load( System.getProperty("user.dir") + ModGeneral.gcteRetIssuesAISCOM_XSL );
      }
      else if( wvarCatalog.equals( "LBA - EMISION" ) )
      {
        wobjXSLResponse.load( System.getProperty("user.dir") + ModGeneral.gcteRetIssuesAISOPER_XSL );
      }
      //
      wvarresult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
      wvarstep = 100;
      wobjXMLResponse = null;
      wobjXSLResponse = null;
      wobjXMLRequest = null;
      //
      wvarstep = 110;
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarresult + "</Response>" );
      //
      // DESCONECTO EL OBJETO COLD VIEW SI NO EXISTE EL NODO KEEPCONNECTED
      //If Not wvarKeepConnected Then
      ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
      //End If
      //
      wvarstep = 120;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        //
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   *  *****************************************************************
   *  Function : ColdViewConnect
   *  Abstract : Crea el objeto Cold View, se conecta y valida el usuario
   *  Synopsis : ColdViewConnect() As Long
   *  *****************************************************************
   * Private Function ColdViewConnect(ByVal pvarAccessWay As String) As Long
   *     Const wcteFnName        As String = "ColdViewConnect"
   *     '
   *     Dim wobjXMLResponse     As MSXML2.DOMDocument
   *     '
   *     Dim wvarXMLOut          As String
   *     Dim wvarstep            As Long
   *     Dim wvarresult          As String
   *     Dim wvarResponseXML     As String
   *     '
   *     '~~~~~~~~~~~~~~~~~~~~~~~~~~~
   *     On Error GoTo ErrorHandler
   *     '~~~~~~~~~~~~~~~~~~~~~~~~~~~
   *     '
   * 
   *     wvarstep = 61
   *     ' SELECCIONO EL USUARIO Y CLAVE DE CONEXION
   *     If Not SelectUserPwd(pvarAccessWay, wvarResponseXML) Then
   *         Err.Raise -1, "SelectUserPwd", "Error en la selecci�n de usuario y password de conexi�n a Cold View. Detalle:" & wvarResponseXML
   *     End If
   *     '
   *     ' CREO EL OBJETO COLD VIEW
   *     wvarstep = 62
   *     Set mobjCOLDview = CreateObject("ColdViewApi")
   *     '
   * 
   *     gHilos = gHilos + 1
   * 
   *     mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, _
   * '                         mcteClassName, _
   *                          wcteFnName, _
   * '                         wvarstep, _
   *                          0, _
   * '                         CStr(gHilos), _
   *                          vbLogEventTypeInformation
   * 
   *     ' EJECUTO LA CONEXION
   *     wvarstep = 63
   *     Call mobjCOLDview.XML_Connect(wvarResponseXML, wvarXMLOut)
   *     '
   *     ' VERIFICO LA RESPUESTA DE COLD VIEW
   *     wvarstep = 64
   * 
   *     Set wobjXMLResponse = New MSXML2.DOMDocument
   *     wobjXMLResponse.async = False
   *     wobjXMLResponse.loadXML wvarXMLOut
   *     '
   *     If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
   *         Err.Raise -1, "ColdViewConnect", "Error en la conexion a ColdView. Detalle:" & wvarXMLOut
   *     End If
   *     '
   *     Set wobjXMLResponse = Nothing
   *     '
   *     ' VALIDO EL USUARIO
   *     'buscar wobjXMLParameters.xml
   *     wvarstep = 65
   *     Call mobjCOLDview.XML_ValidateUser(wvarResponseXML, wvarXMLOut)
   *     '
   *     ' VERIFICO LA RESPUESTA DE COLD VIEW
   *     wvarstep = 66
   *     Set wobjXMLResponse = New MSXML2.DOMDocument
   *     wobjXMLResponse.async = False
   *     wobjXMLResponse.loadXML wvarXMLOut
   *     '
   *     If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
   *         Err.Raise -1, "ColdViewConnect", "Error al validar el usuario ColdView. Detalle:" & wvarXMLOut
   *     End If
   *     '
   *     Set wobjXMLResponse = Nothing
   *     '
   *     wvarstep = 67
   * 
   *     ColdViewConnect = 0
   * Exit Function
   * '
   * '~~~~~~~~~~~~~~~
   * ErrorHandler:
   * '~~~~~~~~~~~~~~~
   *     '
   *     Call ColdViewDisconnect
   *     '
   *     mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
   * '                     mcteClassName, _
   *                      wcteFnName, _
   * '                     wvarstep, _
   *                      Err.Number, _
   * '                     "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]", _
   *                      vbLogEventTypeError
   *     '
   *     ColdViewConnect = 1
   * End Function
   * ' *****************************************************************
   * ' Function : SelectUserPwd
   * ' Abstract : Determina el usuario y password de conexi�n a Cold View
   * ' Synopsis : SelectUserPwd(ByVal pvarAccessWay As String, Response As String) As Boolean
   * ' *****************************************************************
   * ' El formato del XML que recibe el objeto Cold View para conexi�n es el siguiente:
   * 
   * '<Request>
   * '    <Server>valor</Server>
   * '    <Port>valor</Port>
   * '    <Timeout>valor</Timeout>
   * '    <Encryptiontype>valor</Encryptiontype>
   * '    <Language>valor</Language>
   * '    <Securitytype>valor</Securitytype>
   * '    <User>valor</User>
   * '    <Pwd>valor</Pwd>
   * '</Request>
   * 
   * Private Function SelectUserPwd(ByVal pvarAccessWay As String, Response As String) As Boolean
   *     Const wcteFnName            As String = "SelectUserPwd"
   *     Dim wvarstep                As Long
   * 
   *     Dim wobjXMLDoc              As MSXML2.DOMDocument
   *     Dim wobjXMLConnectionsList  As MSXML2.IXMLDOMNodeList
   * 
   *     Dim warrConexiones()        As String
   *     Dim wvarUserConnection      As String
   *     Dim wvarPwdConnection       As String
   * 
   *     Dim wvarXMLCVConnection     As String   'String XML a devolver por la funci�n
   *     Dim wvarServer              As String
   *     Dim wvarPort                As String
   *     Dim wvarTimeout             As String
   *     Dim wvarEncryptionType      As String
   *     Dim wvarLanguage            As String
   *     Dim wvarSecuritytype        As String
   * 
   * 
   * 
   *     '~~~~~~~~~~~~~~~~~~~~~~~~~~~
   *     On Error GoTo ErrorHandler
   *     '~~~~~~~~~~~~~~~~~~~~~~~~~~~
   * 
   *     wvarstep = 10
   *     Set wobjXMLDoc = New MSXML2.DOMDocument
   * 
   *     With wobjXMLDoc
   *         .async = False
   *         .Load App.Path & gcteParamFileName
   *         wvarServer = .selectSingleNode("Request/Server").Text
   *         wvarPort = .selectSingleNode("Request/Port").Text
   *         wvarTimeout = .selectSingleNode("Request/Timeout").Text
   *         wvarEncryptionType = .selectSingleNode("Request/Encryptiontype").Text
   *         wvarLanguage = .selectSingleNode("Request/Language").Text
   *         wvarSecuritytype = .selectSingleNode("Request/Securitytype").Text
   *     End With
   * 
   *     wvarstep = 20
   *     '
   *     'Busco el usuario de ColdView en base al par�metro recibido (pvarAccessWay)
   *     If (pvarAccessWay = "") Then
   *         'Tomo el usuario por defecto
   *         Set wobjXMLConnectionsList = wobjXMLDoc.selectNodes("/Request/Connections/Connection[@Default=""yes""]")
   *     Else
   *         Set wobjXMLConnectionsList = wobjXMLDoc.selectNodes("/Request/Connections/Connection[@Access=""" & pvarAccessWay & """]")
   *     End If
   *     '
   *     '
   *     wvarstep = 30
   *     warrConexiones = Split(wobjXMLConnectionsList(0).xml, " ")
   *     wvarUserConnection = Strings.replace(Mid(warrConexiones(3), InStr(1, warrConexiones(3), "=") + 1, Len(warrConexiones(3))), """", "")
   *     wvarPwdConnection = Strings.replace(Mid(warrConexiones(4), InStr(1, warrConexiones(4), "=") + 1, Len(warrConexiones(4))), """", "")
   *     wvarPwdConnection = Strings.replace(wvarPwdConnection, "/>", "")
   *     '
   *     'Armo el XML de conexi�n a ColdView con el usuario y password correspondiente
   *     '(respetando el formato de EColdConfig.xml)
   *     wvarXMLCVConnection = "<Request>" & Chr(13) & Chr(9) & _
   * '                            "<Server>" & wvarServer & "</Server>" & Chr(13) & Chr(9) & _
   *                             "<Port>" & wvarPort & "</Port>" & Chr(13) & Chr(9) & _
   * '                            "<Timeout>" & wvarTimeout & "</Timeout>" & Chr(13) & Chr(9) & _
   *                             "<Encryptiontype>" & wvarEncryptionType & "</Encryptiontype>" & Chr(13) & Chr(9) & _
   * '                            "<Language>" & wvarLanguage & "</Language>" & Chr(13) & Chr(9) & _
   *                             "<Securitytype>" & wvarSecuritytype & "</Securitytype>" & Chr(13) & Chr(9) & _
   * '                            "<User>" & wvarUserConnection & "</User>" & Chr(13) & Chr(9) & _
   *                             "<Pwd>" & wvarPwdConnection & "</Pwd>" & Chr(13) & _
   * '                          "</Request>"
   * 
   *     Set wobjXMLDoc = Nothing
   *     Set wobjXMLConnectionsList = Nothing
   * 
   *     Response = wvarXMLCVConnection
   *     SelectUserPwd = True
   *     Exit Function
   * 
   * '~~~~~~~~~~~~~~~
   * ErrorHandler:
   * '~~~~~~~~~~~~~~~
   *     '
   *     Set wobjXMLDoc = Nothing
   *     Set wobjXMLConnectionsList = Nothing
   *     '
   *         mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
   * '                         mcteClassName, _
   *                          wcteFnName, _
   * '                         wvarstep, _
   *                          Err.Number, _
   * '                         "Error= [" & Err.Number & "] - " & Err.Description & "[XML Config: No se pudo determinar el usuario y password de conexi�n con Cold View para la v�a de acceso: " & pvarAccessWay & "] ", _
   *                          vbLogEventTypeError
   *     '
   *     Response = "Error: No se pudo determinar el usuario y password de conexi�n para la v�a de acceso " & pvarAccessWay
   *     SelectUserPwd = False
   * 
   * End Function
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
