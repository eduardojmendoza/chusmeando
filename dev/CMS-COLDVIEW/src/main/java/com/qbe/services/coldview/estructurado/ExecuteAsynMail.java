package com.qbe.services.coldview.estructurado;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.coldview.estructurado.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class ExecuteAsynMail implements VBObjectClass
{
  /**
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.ExecuteAsynMail";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarstep = 0;
    String wvarRequest = "";
    String wvarFileName = "";
    XmlDomExtended wobjXMLRequest = null;


    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarstep = 10;

      //wvarRequest = "<Request>" & Request & "</Request>"
      wvarRequest = Request;
      //
      // CREO EL DIRECTORIO DE GENFILES SI NO EXISTE
      if( "" /*unsup this.Dir( (System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir), Constants.vbDirectory ) */.equals( "" ) )
      {
        FileSystem.mkDir( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir );
      }
      //
      wvarstep = 20;
      wobjXMLRequest = new XmlDomExtended();
      //
      wvarstep = 30;
      wobjXMLRequest.loadXML( wvarRequest );
      VB.randomize( DateTime.toInt( DateTime.now() ) );
      wvarFileName = "Request" + String.valueOf( (int)Math.rint( (((99999999 - 1) + 1) * VB.rnd()) + 1 ) ) + ".xml";
      wvarstep = 40;
      wobjXMLRequest.save( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileName );
      //
      wvarstep = 50;
      Runtime.getRuntime().exec( "\"" + System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamAsyncExec + "\"" + " " + wvarFileName );

      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\"/></Response>" );

      IAction_Execute = 0;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - ", vbLogEventTypeError );

        Response.set( "<Response><Estado resultado=\"False\" mensaje=\"No se pudo\"/></Response>" );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
