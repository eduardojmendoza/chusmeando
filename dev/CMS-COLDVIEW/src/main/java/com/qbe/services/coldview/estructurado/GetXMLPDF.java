package com.qbe.services.coldview.estructurado;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.Page;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Err;
import diamondedge.util.Variant;

/**
 * ***************************************************************** COPYRIGHT.
 * HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT HAS BEEN
 * PROVIDED. NO PART OF IT IS TO BE REPRODUCED, DISASSEMBLED, TRANSMITTED,
 * STORED IN A RETRIEVAL SYSTEM NOR TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE
 * IN ANY WAY OR FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 * CONSENT OF HSBC HOLDINGS PLC.
 * ***************************************************************** Module Name
 * : GetXMLPDF File Name : GetXMLPDF.cls Creation Date: 04/11/2003 Programmer :
 * Muzzupappa - Goncalves Abstract : Obtiene un PDF en formato XML de la base de
 * Cold View *****************************************************************
 * 2008-08-15 - FJO / MC / DA Objeto de ColdView
 */

public class GetXMLPDF implements VBObjectClass {

	protected static Logger logger = Logger.getLogger(GetXMLPDF.class.getName());

	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbaA_ECold.GetXMLPDF";
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_CATALOG = "//CATALOG";
	static final String mcteParam_PAGENBR = "//PAGENBR";
	static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
	static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
	static final String mcteParam_MAXPAGES = "//MAXPAGES";
	static final String mcteParam_PAGECOUNTER = "//PAGECOUNTER";
	static final String mcteParam_PDFUSRPWD = "//PDFUSRPWD";
	static final String mcteParam_PDFMSTRPWD = "//PDFMSTRPWD";
	static final String mcteParam_PDFKEYLENGTH = "//PDFKEYLENGTH";
	static final String mcteParam_PDFNOPRINT = "//PDFNOPRINT";
	static final String mcteParam_PDFNOMODIFY = "//PDFNOMODIFY";
	static final String mcteParam_PDFNOCOPY = "//PDFNOCOPY";
	static final String mcteParam_PDFNOANNOTS = "//PDFNOANNOTS";
	static final String mcteParam_DATEISSUE = "//DATEISSUE";
	static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
	static final String mcteParam_DOCKEY = "//DocKey";
	static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
	private static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
	private static final String mcteParam_CVDATETO = "//IssueDateTo";

	/**
	 * Parametros Cold View
	 */
	static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
	static final String mcteParam_CVPAGENBR = "//PageNbr";
	static final String mcteParam_CVISSUEID = "//IssueID";
	static final String mcteParam_CVPDFUSRPWD = "//PDFUsrPWD";
	static final String mcteParam_CVPDFMSTRPWD = "//PDFMstrPWD";
	static final String mcteParam_CVPDFKEYLENGTH = "//PDFKeyLength";
	static final String mcteParam_CVPDFNOPRINT = "//PDFNoprint";
	static final String mcteParam_CVPDFNOMODIFY = "//PDFNomodify";
	static final String mcteParam_CVPDFNOCOPY = "//PDFNocopy";
	static final String mcteParam_CVPDFNOANNOTS = "//PDFNoannots";

	private static final String REPORTCOBRANZA = "LBA_CHEQ";

	private static final String REPORTCOMISION = "GR44536";

	private static final String REPORTEMISION = "GRP0012";

	public ColdViewApi mobjCOLDview = new ColdViewApi();

	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	/**
	 * *****************************************************************
	 * Function : IAction_Execute Abstract : Busca en la base de Cold View la
	 * emision de la fecha pasada como parametro, abre la misma y obtiene el PDF
	 * correspondiente Synopsis : IAction_Execute(ByVal Request As String,
	 * Response As String, ByVal ContextInfo As String) As Long
	 * *****************************************************************
	 */
	@Override
	public int IAction_Execute(String Request, StringHolder response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLResponse = null;

		String wvarXMLOut = "";
		String wvarCatalog = "";
		String wvarIssueItemNbr = "";
		String wvarAccessWay = "";
		String wvarPageNbr = "";

		String wvarPDFNoPrint = "";
		String wvarPDFNoModify = "";
		String wvarPDFNoCopy = "";
		String wvarPDFNoAnnots = "";
		int wvarstep = 0;

		// nuevas variables

		String wvarDateIssue = "";
		String wvarDocKey = "";
		String queryClause = "";
		String wvarIDX_Busqueda = "";
		String dateTo = "";
		String dateFrom = "";
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			// CARGO LOS PARAMETROS DE ENTRADA
			wvarstep = 10;
			wobjXMLRequest = new XmlDomExtended();
			//
			wobjXMLRequest.loadXML(Request);
			wvarCatalog = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CATALOG));
			wvarPageNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR));
			wvarIssueItemNbr = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ISSUEITEMNBR));
			wvarDateIssue = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DATEISSUE));
			queryClause = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE));
			wvarDocKey = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOCKEY));
			wvarIDX_Busqueda = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IDX_BUSQUEDA));
			dateFrom = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATEFROM));
			dateTo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATETO));
			wvarPDFNoPrint = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOPRINT));
			wvarPDFNoModify = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOMODIFY));
			wvarPDFNoCopy = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOCOPY));
			wvarPDFNoAnnots = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOANNOTS));
			logger.info("query clause: " + queryClause);
			// Verifico que exista el nodo <ACCESSWAY>
			// ACCESSWAY es OV y lo usa para levantar los parámetros de conexión
			wvarAccessWay = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACCESSWAY));
			if (Err.getError().getNumber() != 0) {
				wvarAccessWay = "";
			}
			//
			wvarstep = 20;
			//
	
			// ------------------------------------------------------------------------
			// PIDO CONEXION A COLD VIEW
			wvarstep = 21;
			ColdViewApi mobjCOLDview = new ColdViewApi();
			wvarstep = 22;
			if (ModGeneral.ColdViewConnect(wvarAccessWay, mobjCOLDview) == 1) {
				// HUBO UN ERROR EN LA CONEXION A COLD VIEW
				// FIXME Agregar detalle del error acá
				wvarstep = 23;
				response.set("<Response><Estado resultado=\"false\"/></Response>");
				mobjCOLDview = (ColdViewApi) null;
				IAction_Execute = 1;
				return IAction_Execute;
			}
		
			wvarstep = 25;
			//
			// wobjXMLRequest = new XmlDomExtended();

			wvarstep = 26;

			// wobjXMLRequest.loadXML(wvarResponse.toString());

			wvarstep = 27;


			//
			// CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
			wvarstep = 30;

			//
			wvarstep = 40;

			logger.info("dateFrom " + dateFrom);
			if (wvarDocKey.equals("")){
				if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
					wvarDocKey = REPORTCOBRANZA;
				} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
					wvarDocKey = REPORTCOMISION;
				} else if (wvarCatalog.equals("LBA - EMISION")) {
					wvarDocKey = REPORTEMISION;
				}
			}
			wvarstep = 60;
			// FIXME Convertir
			Issue issue = ModGeneral.obteinIssueInfo(mobjCOLDview, wvarIssueItemNbr, wvarCatalog, wvarIssueItemNbr,
					wvarDateIssue, wvarIDX_Busqueda, queryClause, wvarDocKey, dateFrom, dateTo, wvarPageNbr);
			wvarstep = 70;
			logger.info("issue: " + issue.getName());
			Page page = mobjCOLDview.getStruc().getPage(issue, "ISO-8859-1");

	

			//
			// CARGO EL ARCHIVO XML PARA EL ISSUE PAGE GOTO
			wvarstep = 70;
			// wobjXMLRequest = new XmlDomExtended();
			//
			wvarstep = 80;
		
			wvarstep = 100;
		
			wvarstep = 110;
			// wobjXMLRequest = new XmlDomExtended();
			//
			wvarstep = 120;

			wvarstep = 130;

			wvarstep = 135;
			//

			wvarstep = 140;
			// FIXME Convertir
			boolean isPrint = ("0".equals(wvarPDFNoPrint)) ? false : true;
			boolean isModify = ("0".equals(wvarPDFNoModify)) ? false : true;
			boolean isCopy = ("0".equals(wvarPDFNoCopy)) ? false : true;
			boolean isAnnot = ("0".equals(wvarPDFNoAnnots)) ? false : true;
			isPrint = false;
			logger.info("ejecución getPDFBAse64");
			String pdfBase64 = mobjCOLDview.struc.getPDFBase64(issue, "", "password", isPrint, isModify, isCopy, true,
					false, isAnnot, false, true);

			logger.log(Level.INFO, "getPDFBase64 retornó: " + pdfBase64.substring(0, 3));


			wvarstep = 150;
			// wobjXSLResponse = new XmlDomExtended();
			ModGeneral.createXML(response, wobjXMLResponse, pdfBase64);

			wvarstep = 160;
			// wobjXMLResponse = null;

			wobjXMLRequest = null;
			//
			wvarstep = 170;

			// DESCONECTO EL OBJETO COLD VIEW
			ModGeneral.ColdViewDisconnect(mobjCOLDview);
			//
			IAction_Execute = 0;
			/* TBD mobjCOM_Context.SetComplete() ; */
			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (Exception _e_) {
			Err.set(_e_);
			try {
				// ~~~~~~~~~~~~~~~
				wobjXMLRequest = null;
				wobjXMLResponse = null;

				//
				ModGeneral.ColdViewDisconnect(
						mobjCOLDview/*
									 * warning: ByRef value change will be lost.
									 */ );
				//
				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
						wcteFnName, wvarstep,
						Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - "
								+ Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]",
						vbLogEventTypeError);
				response.set("<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO CREAR EL XML. " + Err.getError().getDescription()+"\" /> "
						+ "</Response>");
				IAction_Execute = 1;
				/* TBD mobjCOM_Context.SetAbort() ; */
				Err.clear();
			} catch (Exception _e2_) {
			}
		}
		return IAction_Execute;
	}

}
