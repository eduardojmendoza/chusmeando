package com.qbe.services.coldview.estructurado;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.amco.jcoldapi.ColdViewApi;
import com.amco.jcoldapi.cvobjects.Issue;
import com.amco.jcoldapi.cvobjects.Page;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import java.util.logging.Logger;

import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.coldview.estructurado.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  *****************************************************************
 *  COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 *  THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
 *  HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
 *  DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
 *  TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
 *  FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 *  CONSENT OF HSBC HOLDINGS PLC.
 *  *****************************************************************
 *  Module Name : SendTXTMail
 *  File Name : SendTXTMail.cls
 *  Creation Date: 15/06/2005
 *  Programmer : Ramirez
 *  Abstract : Envia un TXT por mail
 *  *****************************************************************
 *  2008-08-15 - FJO / MC / DA
 * Objeto de ColdView
 */

public class SendTXTMail implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.SendTXTMail";
  /**
   * Parametros XML
   */
  static final String mcteParam_CATALOG = "//CATALOG";
  static final String mcteParam_PAGENBR = "//PAGENBR";
  static final String mcteParam_CANTPAGE = "//CANTPAGE";
  static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
  static final String mcteParam_SUBJECT = "//SUBJECT";
  static final String mcteParam_MSGBODY = "//MSGBODY";
  static final String mcteParam_FROM = "//FROM";
  static final String mcteParam_FROMADDRESS = "//FROMADDRESS";
  static final String mcteParam_SENDTO = "//SENDTO";
  static final String mcteParam_REPLYTO = "//REPLYTO";
  static final String mcteParam_ATTACH = "//ATTACH";
  static final String mcteParam_ATTACHNAME = "//ATTACHNAME";
  static final String mcteParam_CC = "//CC";
  static final String mcteParam_BCC = "//BCC";
  static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
  
  
  
  
	static final String mcteParam_DATEISSUE = "//DATEISSUE";
	static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
	static final String mcteParam_IDX_SISTEMA = "//IDX_SISTEMA";
	static final String mcteParam_MAXPAGES = "//MAXPAGES";
	static final String mcteParam_DOCKEY = "//DOCKEY";
	static final String mcteParam_GIVENATTACHPDFNAME = "//GIVENATTACHPDFNAME";
	static final String mcteParam_PDFUSRPWD = "//PDFUSRPWD";
	static final String mcteParam_PDFMSTRPWD = "//PDFMSTRPWD";
	static final String mcteParam_PDFKEYLENGTH = "//PDFKEYLENGTH";
	static final String mcteParam_PDFNOPRINT = "//PDFNOPRINT";
	static final String mcteParam_PDFNOMODIFY = "//PDFNOMODIFY";
	static final String mcteParam_PDFNOCOPY = "//PDFNOCOPY";
	static final String mcteParam_PDFNOANNOTS = "//PDFNOANNOTS";
	static final String mcteParam_ADDITIONALFILES = "//ADDITIONALFILES";
	static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
	private static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
	private static final String mcteParam_CVDATETO = "//IssueDateTo";
  
  /**
   *  Parametros Cold View
   */
  static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
  static final String mcteParam_CVPAGENBR = "//PageNbr";
  static final String mcteParam_CVISSUEID = "//IssueID";
  public ColdViewApi mobjCOLDview = new ColdViewApi();
  /**
   * Objetos del FrameWork
   */
  @SuppressWarnings("unused")
private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  private static Logger logger = Logger.getLogger(ModGeneral.class.getName());
  private String wvarFileName = "";

  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";


	private static final String REPORTCOBRANZA = "LBA_CHEQ";

	private static final String REPORTCOMISION = "GR44536";

	private static final String REPORTEMISION = "GRP0012";

	
	private static final String NAMETO = "Estimado Productor";
  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Busca en la base de Cold View la emision de la fecha pasada
   *  como parametro, abre la misma y envia el PDF correspondiente por mail a
   *  la direccion especificada
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @SuppressWarnings("unused")
@Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    Object wobjMail = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    org.w3c.dom.Element wobjXMLElement = null;
    Object wobjFileTXT = null;
    java.io.RandomAccessFile wobjTXT = null;
    String wvarXMLOut = "";
    String wvarCatalog = "";
    String wvarIssueItemNbr = "";
    String wvarPageNbr = "";
    String wvarIssueId = "";
    String wvarSendTo = "";
    String wvarPDFName = "";
    String wvarSubject = "";
    String wvarMSGBody = "";
    String wvarFrom = "";
    String wvarFromAddress = "";
    String wvarReplyTo = "";
    String wvarCC = "";
    String wvarBCC = "";
    String wvarATTACH = "";
    String wvarATTACHNAME = "";
    Variant wvarResponse = new Variant();
    String wvarSM_ID = "";
    int wvarCounter = 0;
    String wvarCANTPAGE = "";
    String wvarAccessWay = "";
    int wvarstep = 0;
    int mvarPagAct = 0;
    String mvarTexto = "";
    String wvarDateIssue = "";
	String wvarIdxBusqueda = "";
	String wvarIdxSistema = "";
	String wvarQueryClause = "";
	String dateFrom = "";
	String dateTo = "";
	String wvarDocKey = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // LEVANTO DE LA BASE DE DATOS LOS MAILS PARA ENVIAR
      // CARGO LOS PARAMETROS DEL MAIL
      wvarstep = 80;
      wobjXMLRequest = new XmlDomExtended();
      //
      wobjXMLRequest.loadXML( Request );
      wvarCatalog = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CATALOG )  );
      wvarPageNbr = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR )  );
      wvarIssueItemNbr = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR )  );
      wvarSendTo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SENDTO )  );
      wvarSubject = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT )  );
      wvarMSGBody = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MSGBODY )  );
      wvarATTACH = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ATTACH )  );
      wvarATTACHNAME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ATTACHNAME )  );
      wvarFrom = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FROM )  );
      wvarFromAddress = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FROMADDRESS )  );
      wvarCANTPAGE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CANTPAGE )  );
      wvarDateIssue = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DATEISSUE));
      wvarDocKey = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_DOCKEY));
      wvarIdxBusqueda = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_IDX_BUSQUEDA));
      wvarQueryClause = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE));
      dateFrom = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATEFROM));
	  dateTo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_CVDATETO));
	  wvarAccessWay = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ACCESSWAY));
      //
      if( wobjXMLRequest.selectSingleNode( mcteParam_REPLYTO )  == (org.w3c.dom.Node) null )
      {
        wvarReplyTo = "";
      }
      else
      {
        wvarReplyTo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_REPLYTO )  );
      }
      //
      wvarCC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CC )  );
      wvarBCC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_BCC )  );
      //
      //Verifico que exista el nodo <ACCESSWAY>
      // On Error Resume Next (optionally ignored)
      wvarAccessWay = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ACCESSWAY )  );
      if( Err.getError().getNumber() != 0 )
      {
        wvarAccessWay = "";
      }
      //
      //
      wvarstep = 20;
      //
      // AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES DEL RETRIEVEISSUES
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "KEEPCONNECTED" );
      XmlDomExtended.setText( wobjXMLElement, "TRUE" );
      wobjXMLRequest.selectSingleNode( "//Request" ).appendChild( wobjXMLElement );
      //
      // AGREGO EL NODO ACCESSWAY
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "ACCESSWAY" );
      XmlDomExtended.setText( wobjXMLElement, wvarAccessWay );
      wobjXMLRequest.selectSingleNode( "//Request" ).appendChild( wobjXMLElement );
      //
      //MHC ------------------------------------------------------------------------
      // PIDO CONEXION A COLD VIEW
      wvarstep = 100;
      mobjCOLDview = new ColdViewApi();
      wvarstep = 102;
      if( ModGeneral.ColdViewConnect( wvarAccessWay, mobjCOLDview ) == Obj.toInt( "1" ) )
      {
        // HUBO UN ERROR EN LA CONEXION A COLD VIEW
        wvarstep = 103;
        Response.set( "<Response><Estado resultado=\"false\"/></Response>" );
        mobjCOLDview = (ColdViewApi) null;
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        return IAction_Execute;
      }
      wvarstep = 104;
      if (wvarDocKey.equals("")) {
			if (wvarCatalog.equals("LBA - AIS COBRANZAS")) {
				wvarDocKey = REPORTCOBRANZA;
			} else if (wvarCatalog.equals("AIS - LBA - COMISIONES")) {
				wvarDocKey = REPORTCOMISION;
			} else if (wvarCatalog.equals("LBA - EMISION")) {
				wvarDocKey = REPORTEMISION;
			}
		}
      wvarstep = 105;
      Issue issue = ModGeneral.obteinIssueInfo(mobjCOLDview, wvarIssueItemNbr, wvarCatalog, wvarIssueItemNbr,
				wvarDateIssue, wvarIdxBusqueda, wvarQueryClause, wvarDocKey, dateFrom, dateTo, wvarPageNbr);
      
      wvarstep = 106;

      Page page = mobjCOLDview.getStruc().getPage(issue, "ISO-8859-1");
      	logger.info("wvarSendTo" + wvarSendTo);
		//logger.info("numPage" + String.valueOf(issue.getNextPage()));
		logger.info("wvarSubject" + wvarSubject);
		logger.info("wvarMSGBody" + wvarMSGBody);
		logger.info("wvarFromAddress" + wvarFromAddress);
		logger.info("wvarFrom" + wvarFrom);
		logger.info("wvarReplyTo" + wvarReplyTo);
		logger.info("wvarCC" + wvarCC);
		logger.info("wvarBCC" + wvarBCC);
		logger.info("wvarATTACHNAME " + wvarATTACHNAME);
		
		wvarstep = 107;
      final String pdfMail = mobjCOLDview.struc.sendTXTEmail(issue,
				wvarSendTo, wvarATTACHNAME, issue.getNextPage()-1,
				wvarSubject, wvarMSGBody,
				wvarFromAddress, wvarFrom, wvarReplyTo, wvarCC, wvarBCC);
      logger.info(pdfMail);
		mobjCOLDview.struc.closeIssue(issue);
      if (!pdfMail.equals("OK")){
			throw new Exception("Error " + pdfMail);
		}
		
      	
	
//      
//      //
//      // EJECUTO RETRIEVE ISSUES
//      //Set wobjClass = mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
//      //Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
//      //Set wobjClass = Nothing
//      ModGeneral.RetrieveIssues( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, mobjCOLDview );
//      //FIN MHC --------------------------------------------------------------------
//      //
//      // ANALIZO LA RESPUESTA
//      wvarstep = 25;
//      wobjXMLRequest = null;
//      //
//      wobjXMLRequest = new XmlDomExtended();
//      wobjXMLRequest.loadXML( wvarResponse.toString() );
//
//      if( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "false" ) )
//      {
//        // DESCONECTO EL OBJETO COLD VIEW
//        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
//        //
//        Response.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
//        IAction_Execute = 0;
//        /*TBD mobjCOM_Context.SetComplete() ;*/
//        return IAction_Execute;
//      }
//      else
//      {
//        if( !wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
//        {
//          //            If wvarPageNbr = "" Then
//          //                wvarPageNbr = wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR).Text
//          //            End If
//          //
//          // CARGO EL ISSUEITEMNUMBER SI VINO VACIO EN EL REQUEST
//          if( wvarIssueItemNbr.equals( "" ) )
//          {
//            wvarIssueItemNbr = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR )  );
//          }
//        }
//        else
//        {
//          //wvarPageNbr = 1
//          //wvarIssueItemNbr = wobjXMLRequest.selectSingleNode("//ROWS/ROW[ISSUETITLE='" & wvarIssueItemNbr & "']/ISSUEITEMNBR").Text
//          wvarIssueItemNbr = "1";
//        }
//        //
//      }
//      //
//      wobjXMLRequest = null;
//      //
//      // CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
//      wvarstep = 30;
//      wobjXMLRequest = new XmlDomExtended();
//      //
//      wvarstep = 40;
//      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteOpenIssue );
//      //
//      // CARGO EL NODO IssueItemNbr EN EL ARCHIVO
//      wvarstep = 50;
//      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEITEMNBR ) , wvarIssueItemNbr );
//      //
//      //MHC - Se pasa la conexion a CV previa a la invocacion del RetrieveIssues
//      // PIDO CONEXION A COLD VIEW
//      //wvarstep = 51
//      //Set mobjCOLDview = CreateObject("ColdViewApi")
//      //wvarstep = 52
//      //If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
//      //    ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
//      //    wvarstep = 53
//      //    Response = "<Response><Estado resultado=""false""/></Response>"
//      //    Set mobjCOLDview = Nothing
//      //    IAction_Execute = 1
//      //    mobjCOM_Context.SetAbort
//      //    Exit Function
//      //End If
//      // EJECUTO OPEN ISSUE DE COLD VIEW
//      wvarstep = 60;
//      // FIXME Convertir
//      //      mobjCOLDview.XML_Issue_Open( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
//      //
//      // VERIFICO LA RESPUESTA DE COLD VIEW
//      wobjXMLResponse = new XmlDomExtended();
//      wobjXMLResponse.loadXML( wvarXMLOut );
//      //
//      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Status" )  ).equals( "-1" ) )
//      {
//        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
//        //
//        // DESCONECTO EL OBJETO COLD VIEW
//        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
//        //
//        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\" cv_message=\"" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Event_Message" )  ) + "\" cv_errornbr= \"" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Error_Nbr" )  ) + "\"/></Response>" );
//        IAction_Execute = 0;
//        /*TBD mobjCOM_Context.SetComplete() ;*/
//        return IAction_Execute;
//      }
//      else
//      {
//        // LEVANTO EL ISSUEID
//        wvarIssueId = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( mcteParam_CVISSUEID )  );
//      }
//      //
//      wobjXMLRequest = null;
//      //
//      // CARGO EL ARCHIVO XML PARA EL ISSUE PAGE GOTO
//      wvarstep = 70;
//      wobjXMLRequest = new XmlDomExtended();
//      //
//      wvarstep = 80;
//      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteIssuePageGoto );
//      //
//      // CARGO EL NODO IssueID Y PageNbr EN EL ARCHIVO
//      wvarstep = 90;
//      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEID ) , wvarIssueId );
//      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_CVPAGENBR ) , wvarPageNbr );
//      //
//      // EJECUTO ISSUE PAGE GOTO DE COLD VIEW
//      wvarstep = 100;
//      // FIXME Convertir
//      //      mobjCOLDview.XML_Issue_Page_Goto( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
//      //
//      wvarstep = 110;
//      // VERIFICO LA RESPUESTA DE COLD VIEW
//      wobjXMLResponse = new XmlDomExtended();
//      wobjXMLResponse.loadXML( wvarXMLOut );
//      //
//      wvarstep = 120;
//      if( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( "//Status" )  ).equals( "-1" ) )
//      {
//        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
//        //
//        // DESCONECTO EL OBJETO COLD VIEW
//        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
//        //
//        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\"  cv_message=\"" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Event_Message" )  ) + "\" cv_errornbr= \"" + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//Error_Nbr" )  ) + "\"/></Response>" );
//        IAction_Execute = 0;
//        /*TBD mobjCOM_Context.SetComplete() ;*/
//        return IAction_Execute;
//      }
//
//      wvarstep = 130;
//      //
//      wobjXMLRequest = null;
//      //
//      //Empiezo a tomar las paginas
//      mvarPagAct = Obj.toInt( wvarPageNbr );
//      mvarTexto = "";
//
//      wvarFileName = "TXTMAIL" + Strings.replace( DateTime.format( DateTime.now(), "######.##########" ), ",", "" );
//
//      for( mvarPagAct = Obj.toInt( wvarPageNbr ); mvarPagAct <= Obj.toDecimal( wvarPageNbr ).add( new java.math.BigDecimal( Obj.toInt( wvarCANTPAGE ) ) ).subtract( new java.math.BigDecimal( 1 ) ).intValue(); mvarPagAct++ )
//      {
//          // FIXME Convertir
//          //        mvarTexto = mvarTexto + mobjCOLDview.Issue_Page_Retrieve( wvarIssueId, 0 ) + System.getProperty("line.separator");
//          // FIXME Convertir
//          //        if( ! (mobjCOLDview.Issue_Page_Next( wvarIssueId )) )
//        {
//          break;
//        }
//      }
//
//      wvarstep = 140;
//
//      wvarFileNameTXT = wvarFileName + ".txt";
//      wobjFileTXT = new Object();
//      wobjTXT = FileSystem.openFile( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileNameTXT, "rw" );
//
//      wvarstep = 150;
//
//      wobjTXT.writeBytes( mvarTexto );
//      wobjTXT.close();
//      wobjFileTXT = (Object) null;
//
//      wvarstep = 170;
//
//      wvarFileNameXML = wvarFileName + ".xml";
//
      	Response.set("<Response><Estado resultado=\"true\" mensaje=\"MAIL ENVIADO\" /></Response>");
//      wobjXMLRequest = new XmlDomExtended();
//      //
//      wvarstep = 180;
//      wobjXMLRequest.loadXML( Response.toString() );
//      VB.randomize( DateTime.toInt( DateTime.now() ) );
//      wvarstep = 190;
//      wobjXMLRequest.save( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileNameXML );
//
//      // EJECUTO LA LLAMADA AL FRAMEWORK, LLAMO AL COMPONENTE DE ENVIO DE MAIL Y LE PASO EL XML CREADO
//      wvarstep = 200;
//      // FIXME Convertir
//      //      wobjMail = new LBAVIRTUALSendmail.SendMail();
//      wvarstep = 210;
//      //error: function 'Execute' was not found.
//      //unsup: Call wobjMail.Execute(wvarFileName, wvarResponse, "")
//      wobjMail = null;
//
//      //Borro el archivo que mande por mail y el archivo con los parametros del mail
//      FileSystem.kill( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileName + ".txt" );
//      FileSystem.kill( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileName + ".xml" );
//      //
		wvarstep = 200;
		
//      // DESCONECTO EL OBJETO COLD VIEW
      ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
      //
      IAction_Execute = 0;
      //mobjCOM_Context.SetComplete
      //Armo el archivo de texto y armo el envio de mail
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = null;
        //
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "][SM_ID = " + wvarSM_ID + "]", vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  @SuppressWarnings("unused")
private void ObjectControl_Activate() throws Exception
  {
  }

  @SuppressWarnings("unused")
private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  @SuppressWarnings("unused")
private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
