VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "SendPDFMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
' Module Name : SendPDFMailAsync
' File Name : SendPDFMailAsync.cls
' Creation Date: 13/01/2004
' Programmer : Muzzupappa - Goncalves
' Abstract : Envia un PDF por mail de la base de Cold View
' *****************************************************************
Option Explicit

' 2008-08-15 - FJO / MC / DA
'Objeto de ColdView
Public mobjCOLDview             As Object

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbaA_ECold.SendPDFMail"

'Parametros XML
Const mcteParam_CATALOG             As String = "//CATALOG"
Const mcteParam_PAGENBR             As String = "//PAGENBR"
Const mcteParam_ISSUEITEMNBR        As String = "//ISSUEITEMNBR"
Const mcteParam_SENDTO              As String = "//SENDTO"
Const mcteParam_GIVENATTACHPDFNAME  As String = "//GIVENATTACHPDFNAME"
Const mcteParam_SUBJECT             As String = "//SUBJECT"
Const mcteParam_MSGBODY             As String = "//MSGBODY"
Const mcteParam_FROM                As String = "//FROM"
Const mcteParam_FROMADDRESS         As String = "//FROMADDRESS"
Const mcteParam_REPLYTO             As String = "//REPLYTO"
Const mcteParam_CC                  As String = "//CC"
Const mcteParam_BCC                 As String = "//BCC"
Const mcteParam_PDFUSRPWD           As String = "//PDFUSRPWD"
Const mcteParam_PDFMSTRPWD          As String = "//PDFMSTRPWD"
Const mcteParam_PDFKEYLENGTH        As String = "//PDFKEYLENGTH"
Const mcteParam_PDFNOPRINT          As String = "//PDFNOPRINT"
Const mcteParam_PDFNOMODIFY         As String = "//PDFNOMODIFY"
Const mcteParam_PDFNOCOPY           As String = "//PDFNOCOPY"
Const mcteParam_PDFNOANNOTS         As String = "//PDFNOANNOTS"
Const mcteParam_ADDITIONALFILES     As String = "//ADDITIONALFILES"
Const mcteParam_ACCESSWAY           As String = "//ACCESSWAY"

' Parametros Cold View
Const mcteParam_CVISSUEITEMNBR      As String = "//IssueItemNbr"
Const mcteParam_CVPAGENBR           As String = "//PageNbr"
Const mcteParam_CVISSUEID           As String = "//IssueID"
Const mcteParam_CVSENDTO            As String = "//SendTo"
Const mcteParam_CVGIVENATTACHPDFNAME As String = "//GivenAttachPDFName"
Const mcteParam_CVSUBJECT           As String = "//Subject"
Const mcteParam_CVMSGBODY           As String = "//MsgBody"
Const mcteParam_CVFROM              As String = "//From"
Const mcteParam_CVFROMADDRESS       As String = "//FromAddress"
Const mcteParam_CVREPLYTO           As String = "//ReplyTo"
Const mcteParam_CVPDFUSRPWD         As String = "//PDFUsrPWD"
Const mcteParam_CVPDFMSTRPWD        As String = "//PDFMstrPWD"
Const mcteParam_CVPDFKEYLENGTH      As String = "//PDFKeyLength"
Const mcteParam_CVPDFNOPRINT        As String = "//PDFNoprint"
Const mcteParam_CVPDFNOMODIFY       As String = "//PDFNomodify"
Const mcteParam_CVPDFNOCOPY         As String = "//PDFNocopy"
Const mcteParam_CVPDFNOANNOTS       As String = "//PDFNoannots"
Const mcteParam_CVADDITIONALFILES   As String = "//AdditionalAttachFiles"


' *****************************************************************
' Function : IAction_Execute
' Abstract : Busca en la base de Cold View la emision de la fecha pasada
' como parametro, abre la misma y envia el PDF correspondiente por mail a
' la direccion especificada
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXMLElement      As MSXML2.IXMLDOMNode
    '
    '
    Dim wvarXMLOut          As String
    Dim wvarCatalog         As String
    Dim wvarIssueItemNbr    As String
    Dim wvarPageNbr         As String
    Dim wvarIssueId         As String
    Dim wvarSendTo          As String
    Dim wvarPDFName         As String
    Dim wvarSubject         As String
    Dim wvarMSGBody         As String
    Dim wvarFrom            As String
    Dim wvarFromAddress     As String
    Dim wvarReplyTo         As String
    Dim wvarCC              As String
    Dim wvarBCC             As String
    Dim wvarResponse        As String
    Dim wvarSM_ID           As String
    Dim wvarCounter         As Long
    Dim wvarPDFUsrPWD       As String
    Dim wvarPDFMstrPWD      As String
    Dim wvarPDFKeyLength    As String
    Dim wvarPDFNoPrint      As String
    Dim wvarPDFNoModify     As String
    Dim wvarPDFNoCopy       As String
    Dim wvarPDFNoAnnots     As String
    Dim wvarAdditionalFiles As String
    Dim wvarAccessWay       As String
    '
    Dim wvarstep            As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    ' LEVANTO DE LA BASE DE DATOS LOS MAILS PARA ENVIAR
        ' CARGO LOS PARAMETROS DEL MAIL
        wvarstep = 80
        Set wobjXMLRequest = New MSXML2.DOMDocument
        '
        With wobjXMLRequest
            wobjXMLRequest.async = False
            wobjXMLRequest.loadXML Request
            wvarCatalog = .selectSingleNode(mcteParam_CATALOG).Text
            wvarPageNbr = .selectSingleNode(mcteParam_PAGENBR).Text
            wvarIssueItemNbr = .selectSingleNode(mcteParam_ISSUEITEMNBR).Text
            wvarSendTo = .selectSingleNode(mcteParam_SENDTO).Text
            wvarPDFName = .selectSingleNode(mcteParam_GIVENATTACHPDFNAME).Text
            wvarSubject = .selectSingleNode(mcteParam_SUBJECT).Text
            wvarMSGBody = .selectSingleNode(mcteParam_MSGBODY).Text
            wvarFrom = .selectSingleNode(mcteParam_FROM).Text
            wvarFromAddress = .selectSingleNode(mcteParam_FROMADDRESS).Text
            '
            If .selectSingleNode(mcteParam_REPLYTO) Is Nothing Then
                wvarReplyTo = ""
            Else
                wvarReplyTo = .selectSingleNode(mcteParam_REPLYTO).Text
            End If
            '
            wvarCC = .selectSingleNode(mcteParam_CC).Text
            wvarBCC = .selectSingleNode(mcteParam_BCC).Text
            wvarPDFUsrPWD = .selectSingleNode(mcteParam_PDFUSRPWD).Text
            wvarPDFMstrPWD = .selectSingleNode(mcteParam_PDFMSTRPWD).Text
            wvarPDFKeyLength = .selectSingleNode(mcteParam_PDFKEYLENGTH).Text
            wvarPDFNoPrint = .selectSingleNode(mcteParam_PDFNOPRINT).Text
            wvarPDFNoModify = .selectSingleNode(mcteParam_PDFNOMODIFY).Text
            wvarPDFNoCopy = .selectSingleNode(mcteParam_PDFNOCOPY).Text
            wvarPDFNoAnnots = .selectSingleNode(mcteParam_PDFNOANNOTS).Text
            '
            If .selectSingleNode(mcteParam_ADDITIONALFILES) Is Nothing Then
                wvarAdditionalFiles = ""
            Else
                wvarAdditionalFiles = .selectSingleNode(mcteParam_ADDITIONALFILES).Text
            End If
            
            'Verifico que exista el nodo <ACCESSWAY>
            On Error Resume Next
            wvarAccessWay = .selectSingleNode(mcteParam_ACCESSWAY).Text
            If Err.Number <> 0 Then
                wvarAccessWay = ""
            End If
            On Error GoTo ErrorHandler
            '
        End With
        '
        wvarstep = 90
        '
        ' AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES DEL RETRIEVEISSUES
        Set wobjXMLElement = wobjXMLRequest.createElement("KEEPCONNECTED")
        wobjXMLElement.Text = "TRUE"
        wobjXMLRequest.selectSingleNode("//Request").appendChild wobjXMLElement
        '
        ' AGREGO EL NODO ACCESSWAY
        Set wobjXMLElement = wobjXMLRequest.createElement("ACCESSWAY")
        wobjXMLElement.Text = wvarAccessWay
        wobjXMLRequest.selectSingleNode("//Request").appendChild wobjXMLElement
        '
        'MHC ------------------------------------------------------------------------
        ' PIDO CONEXION A COLD VIEW
        wvarstep = 100
        Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
        wvarstep = 102
        If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
            ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
            wvarstep = 103
            Response = "<Response><Estado resultado=""false""/></Response>"
            Set mobjCOLDview = Nothing
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
            Exit Function
        End If
        '
        ' EJECUTO RETRIEVE ISSUES
        'Set wobjClass = mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
        'Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
        'Set wobjClass = Nothing
        RetrieveIssues wobjXMLRequest.xml, wvarResponse, mobjCOLDview
        'FIN MHC --------------------------------------------------------------------
        '
        ' ANALIZO LA RESPUESTA
        wvarstep = 110
        Set wobjXMLRequest = Nothing
        '
        Set wobjXMLRequest = New MSXML2.DOMDocument
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML wvarResponse
        '
        wvarstep = 120
        If wobjXMLRequest.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
            ' DESCONECTO EL OBJETO COLD VIEW
            Call ColdViewDisconnect(mobjCOLDview)
            '
            Response = wobjXMLRequest.xml
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
            Exit Function
        Else
            If wvarPageNbr = "" Then
                wvarPageNbr = wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR).Text
            End If
        End If
        '
        If wvarIssueItemNbr = "" Then
            wvarIssueItemNbr = 1
        End If
        wvarstep = 130
        Set wobjXMLRequest = Nothing
        '
        ' CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
        wvarstep = 140
        Set wobjXMLRequest = New MSXML2.DOMDocument
        wobjXMLRequest.async = False
        '
        wvarstep = 150
        wobjXMLRequest.Load App.Path & gcteOpenIssue
        '
        ' CARGO EL NODO IssueItemNbr EN EL ARCHIVO
        wvarstep = 160
        wobjXMLRequest.selectSingleNode(mcteParam_CVISSUEITEMNBR).Text = wvarIssueItemNbr
        '
        'MHC - Se pasa la conexion a CV previa a la invocacion del RetrieveIssues
        ' PIDO CONEXION A COLD VIEW
        'wvarstep = 161
        'Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
        'wvarstep = 162
        'If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
        '    ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
        '    wvarstep = 163
        '    Response = "<Response><Estado resultado=""false""/></Response>"
        '    Set mobjCOLDview = Nothing
        '    IAction_Execute = 1
        '    mobjCOM_Context.SetAbort
        '    Exit Function
        'End If
        '
        ' EJECUTO OPEN ISSUE DE COLD VIEW
        wvarstep = 170
        Call mobjCOLDview.XML_Issue_Open(wobjXMLRequest.xml, wvarXMLOut)
        '
        ' VERIFICO LA RESPUESTA DE COLD VIEW
        Set wobjXMLResponse = New MSXML2.DOMDocument
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML wvarXMLOut
        '
        wvarstep = 180
        If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
            ' DESCONECTO EL OBJETO COLD VIEW
            Call ColdViewDisconnect(mobjCOLDview)
            '
            Response = "<Response><Estado resultado=""false"" mensaje=""NO SE PUDO RECUPERAR EL PDF."" cv_message=""" & wobjXMLResponse.selectSingleNode("//Event_Message").Text & """ cv_errornbr= """ & wobjXMLResponse.selectSingleNode("//Error_Nbr").Text & """/></Response>"
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
            Exit Function
        Else
            ' LEVANTO EL ISSUEID
            wvarIssueId = wobjXMLResponse.selectSingleNode(mcteParam_CVISSUEID).Text
        End If
        '
        Set wobjXMLRequest = Nothing
        '
        ' CARGO EL ARCHIVO XML PARA EL SEND MAIL
        wvarstep = 190
        Set wobjXMLRequest = New MSXML2.DOMDocument
        wobjXMLRequest.async = False
        '
        wvarstep = 200
        wobjXMLRequest.Load App.Path & gctePDFSendMail
        '
        ' CARGO LOS NODOS CORRESPONDIENTES EN EL ARCHIVO
        wvarstep = 210
        wobjXMLRequest.selectSingleNode(mcteParam_CVISSUEID).Text = wvarIssueId
        wobjXMLRequest.selectSingleNode(mcteParam_CVPAGENBR).Text = wvarPageNbr
        wobjXMLRequest.selectSingleNode(mcteParam_CVSENDTO).Text = wvarSendTo
        wobjXMLRequest.selectSingleNode(mcteParam_CVGIVENATTACHPDFNAME).Text = wvarPDFName
        wobjXMLRequest.selectSingleNode(mcteParam_CVSUBJECT).Text = wvarSubject
        wobjXMLRequest.selectSingleNode(mcteParam_CVMSGBODY).Text = wvarMSGBody
        wobjXMLRequest.selectSingleNode(mcteParam_CVFROM).Text = wvarFrom
        wobjXMLRequest.selectSingleNode(mcteParam_CVFROMADDRESS).Text = wvarFromAddress
        wobjXMLRequest.selectSingleNode(mcteParam_CVREPLYTO).Text = wvarReplyTo
        wobjXMLRequest.selectSingleNode(mcteParam_CC).Text = wvarCC
        wobjXMLRequest.selectSingleNode(mcteParam_BCC).Text = wvarBCC
        '
        ' CARGO LOS NODOS CORRESPONDIENTES AL ENCRIPTADO
        wvarstep = 215
        ' SI EL KEY LENGTH ES 0 NO SE PUEDE CONFIGURAR USER PWD NI MASTER PWD
        If wvarPDFKeyLength = "0" Then
            wvarPDFUsrPWD = ""
            wvarPDFMstrPWD = ""
        End If
        '
        wobjXMLRequest.selectSingleNode(mcteParam_CVPDFUSRPWD).Text = wvarPDFUsrPWD
        '
        ' SI EL KEY LENGTH ES <> 0 Y MASTER PWD ES "" USO LA DEL ARCHIVO
        If wvarPDFKeyLength = "0" Or wvarPDFMstrPWD <> "" Then
            wobjXMLRequest.selectSingleNode(mcteParam_CVPDFMSTRPWD).Text = wvarPDFMstrPWD
        End If
        '
        wobjXMLRequest.selectSingleNode(mcteParam_CVPDFKEYLENGTH).Text = wvarPDFKeyLength
        wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOPRINT).Text = wvarPDFNoPrint
        wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOMODIFY).Text = wvarPDFNoModify
        wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOCOPY).Text = wvarPDFNoCopy
        wobjXMLRequest.selectSingleNode(mcteParam_CVPDFNOANNOTS).Text = wvarPDFNoAnnots
        wobjXMLRequest.selectSingleNode(mcteParam_CVADDITIONALFILES).Text = wvarAdditionalFiles
        '
        ' EJECUTO ISSUE PAGE PDFSENDMAIL DE COLD VIEW
        wvarstep = 220
        Call mobjCOLDview.XML_Issue_Page_PDFSendMail(wobjXMLRequest.xml, wvarXMLOut)

        '
        ' VERIFICO LA RESPUESTA DE COLD VIEW
        Set wobjXMLResponse = New MSXML2.DOMDocument
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML wvarXMLOut
        '
        If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
            ' DESCONECTO EL OBJETO COLD VIEW
            Call ColdViewDisconnect(mobjCOLDview)
            '
            wvarstep = 240
            Response = "<Response><Estado resultado=""false"" mensaje=""NO SE PUDO RECUPERAR EL PDF.""  cv_message=""" & wobjXMLResponse.selectSingleNode("//Event_Message").Text & """ cv_errornbr= """ & wobjXMLResponse.selectSingleNode("//Error_Nbr").Text & """/></Response>"
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
            Exit Function
        End If
        '
        Set wobjXMLRequest = Nothing
        '
        Set wobjXMLResponse = Nothing
        Set wobjXMLRequest = Nothing
        '
        ' DESCONECTO EL OBJETO COLD VIEW
        wvarstep = 250
        Call ColdViewDisconnect(mobjCOLDview)
        '
    '
    wvarstep = 280
    Response = "<Response><Estado resultado=""true"" mensaje=""MAIL ENVIADO"" /></Response>"
    '
    IAction_Execute = 0
    mobjCOM_Context.SetComplete
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    '
    Call ColdViewDisconnect(mobjCOLDview)
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarstep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "][SM_ID = " & wvarSM_ID & "]", _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



