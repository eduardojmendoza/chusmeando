VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "ExecuteAsynMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbaA_ECold.ExecuteAsynMail"



Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"

    Dim wvarstep As Integer
    Dim wvarRequest As String
    Dim wvarFileName As String
    Dim wobjXMLRequest As MSXML2.DOMDocument

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~

    wvarstep = 10
    
    'wvarRequest = "<Request>" & Request & "</Request>"
    wvarRequest = Request
    '
    ' CREO EL DIRECTORIO DE GENFILES SI NO EXISTE
    If Dir(App.Path & gcteParamFilesDir, vbDirectory) = "" Then
        MkDir App.Path & gcteParamFilesDir
    End If
    '
    wvarstep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        wvarstep = 30
        Call .loadXML(wvarRequest)
        Randomize Time
        wvarFileName = "Request" & CStr(CLng((99999999 - 1 + 1) * Rnd + 1)) & ".xml"
        wvarstep = 40
        .save App.Path & gcteParamFilesDir & wvarFileName
    End With
    '
    wvarstep = 50
    Shell """" & App.Path & "\" & gcteParamAsyncExec & """" & " " & wvarFileName, vbHide

    Response = "<Response><Estado resultado=""true"" mensaje=""""/></Response>"

    IAction_Execute = 0
    mobjCOM_Context.SetComplete
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarstep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - ", _
                     vbLogEventTypeError
    
    Response = "<Response><Estado resultado=""False"" mensaje=""No se pudo""/></Response>"
    IAction_Execute = 1
    mobjCOM_Context.SetAbort

End Function



'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

