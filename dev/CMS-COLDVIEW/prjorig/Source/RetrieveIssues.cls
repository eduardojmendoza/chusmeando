VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "RetrieveIssues"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
' Module Name : RetrieveIssues
' File Name : RetrieveIssues.cls
' Creation Date: 30/10/2003
' Programmer : Muzzupappa - Goncalves
' Abstract : Esta clase se llama tanto desde GetXMLPDF como desde
' una pagina ASP. Si existe el Nodo KEEPCONNECTED en el Request, ejecuta
' el metodo RetrieveIssues de ColdView, sino ejecuta RetrieveIssuesWithSearch
' *****************************************************************
Option Explicit

' 2008-08-15 - FJO / MC / DA
'Objeto de ColdView
Public mobjCOLDview             As Object

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbaA_ECold.RetrieveIssues"

'Parametros XML de Entrada
Const mcteParam_CATALOG         As String = "//CATALOG"
Const mcteParam_DATEISSUE       As String = "//DATEISSUE"
Const mcteParam_IDX_BUSQUEDA    As String = "//IDX_BUSQUEDA"
Const mcteParam_IDX_SISTEMA     As String = "//IDX_SISTEMA"
Const mcteParam_PAGENBR         As String = "//PAGENBR"
Const mcteParam_TIPO_AVISO      As String = "//TIPO_AVISO"
Const mcteParam_DOCKEY          As String = "//DOCKEY"
Const mcteParam_CVDATEFROM      As String = "//IssueDateFrom"
Const mcteParam_CVDATETO        As String = "//IssueDateTo"
Const mcteParam_CVQUERYCLAUSE   As String = "//QueryClause"
Const mcteParam_CVDOCKEY        As String = "//DocKey"
Const mcteParam_KEEPCONNECTED   As String = "//KEEPCONNECTED"
Const mcteParam_ACCESSWAY       As String = "//ACCESSWAY"
Const mcteParam_QueryClause     As String = "//QueryClause"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Si existe el nodo KEEPCONNECTED ejecuta el metodo
' RetrieveIssues de la API de ColdView, sino ejecuta el metodo
' RetrieveIssuesWithSearch
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "RetrieveIssues"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarXMLOut          As String
    Dim wvarCatalog         As String
    Dim wvarDateIssue       As String
    Dim wvarIDX_Busqueda    As String
    Dim wvarQuerySearch     As String
    Dim wvarIDX_Sistema     As String
    Dim wvarPageNbr         As String
    Dim wvarTipo_Aviso      As String
    Dim wvarDocKey          As String
    '
    Dim wvarstep            As Long
    Dim wvarresult          As String
    Dim wvarKeepConnected   As Boolean
    Dim wvarAccessWay       As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    ' CARGO LOS PARAMETROS DE ENTRADA
    wvarstep = 10
    Set wobjXMLRequest = New MSXML2.DOMDocument
    '
    With wobjXMLRequest
        wobjXMLRequest.async = False
        wobjXMLRequest.loadXML Request
        wvarCatalog = .selectSingleNode(mcteParam_CATALOG).Text
        wvarDateIssue = .selectSingleNode(mcteParam_DATEISSUE).Text
        wvarIDX_Busqueda = .selectSingleNode(mcteParam_IDX_BUSQUEDA).Text
        wvarQuerySearch = ""
        If Not .selectSingleNode(mcteParam_QueryClause) Is Nothing Then
            wvarQuerySearch = .selectSingleNode(mcteParam_QueryClause).Text
        End If
        wvarDocKey = ""
        If Not (.selectSingleNode(mcteParam_DOCKEY) Is Nothing) Then
            wvarDocKey = .selectSingleNode(mcteParam_DOCKEY).Text
        ElseIf Not (.selectSingleNode(mcteParam_CVDOCKEY) Is Nothing) Then
            wvarDocKey = .selectSingleNode(mcteParam_CVDOCKEY).Text
        End If
        '
        If .selectSingleNode(mcteParam_PAGENBR) Is Nothing Then
            wvarPageNbr = ""
        Else
            wvarPageNbr = .selectSingleNode(mcteParam_PAGENBR).Text
        End If
        '
        wvarstep = 20
        '
        If .selectSingleNode(mcteParam_KEEPCONNECTED) Is Nothing Then
            wvarKeepConnected = False
        Else
            wvarKeepConnected = True
        End If
        '
        'Verifico que exista el nodo <ACCESSWAY>
        On Error Resume Next
        wvarAccessWay = .selectSingleNode(mcteParam_ACCESSWAY).Text
        If Err.Number <> 0 Then
            wvarAccessWay = ""
        End If
        On Error GoTo ErrorHandler
        
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    ' CARGO EL ARCHIVO XML PARA EL ISSUE CORRESPONDIENTE
    wvarstep = 30
    Set wobjXMLRequest = New MSXML2.DOMDocument
    wobjXMLRequest.async = False
    '
    wvarstep = 40
    Select Case wvarCatalog
        Case "LBA - AIS COBRANZAS"
            wobjXMLRequest.Load App.Path & gcteRetIssuesAISCOB
        Case "AIS - LBA - COMISIONES"
            wobjXMLRequest.Load App.Path & gcteRetIssuesAISCOM
        Case "LBA - EMISION"
            wobjXMLRequest.Load App.Path & gcteRetIssuesAISOPER
    End Select
    'CARGO el DOCKEY
    wvarstep = 45
    If wvarDocKey <> "" Then
        wobjXMLRequest.selectSingleNode(mcteParam_CVDOCKEY).Text = wvarDocKey
    End If
    ' CARGO LOS NODOS FECHA_DESDE, FECHA_HASTA Y QUERY EN EL ARCHIVO
    wvarstep = 50
    wobjXMLRequest.selectSingleNode(mcteParam_CVDATEFROM).Text = wvarDateIssue
    wvarstep = 51
    wobjXMLRequest.selectSingleNode(mcteParam_CVDATETO).Text = wvarDateIssue
    '
    wvarstep = 52
    Select Case wvarCatalog
        Case "LBA - AIS COBRANZAS"
            wvarstep = 53
            wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE).Text = Replace(gcteQueryClause, gcteQueryParameter, wvarIDX_Busqueda)
        Case "AIS - LBA - COMISIONES"
            wvarstep = 54
            wobjXMLRequest.selectSingleNode(mcteParam_CVQUERYCLAUSE).Text = wvarQuerySearch
    End Select
    '
    wvarstep = 60
    ' PIDO CONEXION A COLD VIEW
    Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
    If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
        ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
        Response = "<Response><Estado resultado=""false""/></Response>"
        Set mobjCOLDview = Nothing
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        Exit Function
    End If
    '
    ' EJECUTO RETRIEVE ISSUES DE COLD VIEW
    wvarstep = 70
    '
    
    If (wvarKeepConnected And Len(Trim(wobjXMLRequest.selectSingleNode(mcteParam_QueryClause).Text)) = 0 And wvarPageNbr <> "") Then
        ' LA LLAMADA SE HACE DESDE GETXMLPDF, EJECUTO RETRIEVEISSUES
        Call mobjCOLDview.XML_RetrieveIssues(wobjXMLRequest.xml, wvarXMLOut)
    Else
        ' LA LLAMADA SE HACE DESDE LA PAGINA, EJECUTO RETRIEVEISSUESWITHSEARCH
        Call mobjCOLDview.XML_RetrieveIssuesWithSearch(wobjXMLRequest.xml, wvarXMLOut)
    End If
    '
    ' VERIFICO LA RESPUESTA DE COLD VIEW
    Set wobjXMLResponse = New MSXML2.DOMDocument
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML wvarXMLOut
    '
    wvarstep = 80
    '
    If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
        Response = "<Response><Estado resultado=""false"" mensaje=""NO SE ENCONTRARON DATOS PARA EL RANGO DE FECHAS.""  cv_message=""" & wobjXMLResponse.selectSingleNode("//Event_Message").Text & """ cv_errornbr= """ & wobjXMLResponse.selectSingleNode("//Error_Nbr").Text & """/></Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                 mcteClassName, _
                 wcteFnName, _
                 wvarstep, _
                 Err.Number, _
                 "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]" & "[XML Send:" & wobjXMLRequest.xml & "]", _
                 vbLogEventTypeError
        '
        Call ColdViewDisconnect(mobjCOLDview)
        mobjCOM_Context.SetComplete
        IAction_Execute = 0
        Exit Function
    End If
    '
    ' CARGO EL XSL PARA TRANSFORMAR LA RESPUESTA
    wvarstep = 90
    Set wobjXSLResponse = New MSXML2.DOMDocument
    '
    With wobjXSLResponse
        .async = False

        Select Case wvarCatalog
            Case "LBA - AIS COBRANZAS"
                .Load App.Path & gcteRetIssuesAISCOB_XSL
            Case "AIS - LBA - COMISIONES"
                .Load App.Path & gcteRetIssuesAISCOM_XSL
            Case "LBA - EMISION"
                .Load App.Path & gcteRetIssuesAISOPER_XSL
        End Select
    End With
    '
    wvarresult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
    wvarstep = 100
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    Set wobjXMLRequest = Nothing
    '
    wvarstep = 110
    Response = "<Response><Estado resultado=""true"" mensaje="""" />" & wvarresult & "</Response>"
    '
    ' DESCONECTO EL OBJETO COLD VIEW SI NO EXISTE EL NODO KEEPCONNECTED
    'If Not wvarKeepConnected Then
        Call ColdViewDisconnect(mobjCOLDview)
    'End If
    '
    wvarstep = 120
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    Call ColdViewDisconnect(mobjCOLDview)
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarstep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]", _
                     vbLogEventTypeError
    '
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

' *****************************************************************
' Function : ColdViewConnect
' Abstract : Crea el objeto Cold View, se conecta y valida el usuario
' Synopsis : ColdViewConnect() As Long
' *****************************************************************
'Private Function ColdViewConnect(ByVal pvarAccessWay As String) As Long
'    Const wcteFnName        As String = "ColdViewConnect"
'    '
'    Dim wobjXMLResponse     As MSXML2.DOMDocument
'    '
'    Dim wvarXMLOut          As String
'    Dim wvarstep            As Long
'    Dim wvarresult          As String
'    Dim wvarResponseXML     As String
'    '
'    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
'    On Error GoTo ErrorHandler
'    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
'    '
'
'    wvarstep = 61
'    ' SELECCIONO EL USUARIO Y CLAVE DE CONEXION
'    If Not SelectUserPwd(pvarAccessWay, wvarResponseXML) Then
'        Err.Raise -1, "SelectUserPwd", "Error en la selecci�n de usuario y password de conexi�n a Cold View. Detalle:" & wvarResponseXML
'    End If
'    '
'    ' CREO EL OBJETO COLD VIEW
'    wvarstep = 62
'    Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
'    '
'
'    gHilos = gHilos + 1
'
'    mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, _
'                         mcteClassName, _
'                         wcteFnName, _
'                         wvarstep, _
'                         0, _
'                         CStr(gHilos), _
'                         vbLogEventTypeInformation
'
'    ' EJECUTO LA CONEXION
'    wvarstep = 63
'    Call mobjCOLDview.XML_Connect(wvarResponseXML, wvarXMLOut)
'    '
'    ' VERIFICO LA RESPUESTA DE COLD VIEW
'    wvarstep = 64
'
'    Set wobjXMLResponse = New MSXML2.DOMDocument
'    wobjXMLResponse.async = False
'    wobjXMLResponse.loadXML wvarXMLOut
'    '
'    If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
'        Err.Raise -1, "ColdViewConnect", "Error en la conexion a ColdView. Detalle:" & wvarXMLOut
'    End If
'    '
'    Set wobjXMLResponse = Nothing
'    '
'    ' VALIDO EL USUARIO
'    'buscar wobjXMLParameters.xml
'    wvarstep = 65
'    Call mobjCOLDview.XML_ValidateUser(wvarResponseXML, wvarXMLOut)
'    '
'    ' VERIFICO LA RESPUESTA DE COLD VIEW
'    wvarstep = 66
'    Set wobjXMLResponse = New MSXML2.DOMDocument
'    wobjXMLResponse.async = False
'    wobjXMLResponse.loadXML wvarXMLOut
'    '
'    If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
'        Err.Raise -1, "ColdViewConnect", "Error al validar el usuario ColdView. Detalle:" & wvarXMLOut
'    End If
'    '
'    Set wobjXMLResponse = Nothing
'    '
'    wvarstep = 67
'
'    ColdViewConnect = 0
'Exit Function
''
''~~~~~~~~~~~~~~~
'ErrorHandler:
''~~~~~~~~~~~~~~~
'    '
'    Call ColdViewDisconnect
'    '
'    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
'                     mcteClassName, _
'                     wcteFnName, _
'                     wvarstep, _
'                     Err.Number, _
'                     "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]", _
'                     vbLogEventTypeError
'    '
'    ColdViewConnect = 1
'End Function

'' *****************************************************************
'' Function : SelectUserPwd
'' Abstract : Determina el usuario y password de conexi�n a Cold View
'' Synopsis : SelectUserPwd(ByVal pvarAccessWay As String, Response As String) As Boolean
'' *****************************************************************
'' El formato del XML que recibe el objeto Cold View para conexi�n es el siguiente:
'
''<Request>
''    <Server>valor</Server>
''    <Port>valor</Port>
''    <Timeout>valor</Timeout>
''    <Encryptiontype>valor</Encryptiontype>
''    <Language>valor</Language>
''    <Securitytype>valor</Securitytype>
''    <User>valor</User>
''    <Pwd>valor</Pwd>
''</Request>
'
'Private Function SelectUserPwd(ByVal pvarAccessWay As String, Response As String) As Boolean
'    Const wcteFnName            As String = "SelectUserPwd"
'    Dim wvarstep                As Long
'
'    Dim wobjXMLDoc              As MSXML2.DOMDocument
'    Dim wobjXMLConnectionsList  As MSXML2.IXMLDOMNodeList
'
'    Dim warrConexiones()        As String
'    Dim wvarUserConnection      As String
'    Dim wvarPwdConnection       As String
'
'    Dim wvarXMLCVConnection     As String   'String XML a devolver por la funci�n
'    Dim wvarServer              As String
'    Dim wvarPort                As String
'    Dim wvarTimeout             As String
'    Dim wvarEncryptionType      As String
'    Dim wvarLanguage            As String
'    Dim wvarSecuritytype        As String
'
'
'
'    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
'    On Error GoTo ErrorHandler
'    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'    wvarstep = 10
'    Set wobjXMLDoc = New MSXML2.DOMDocument
'
'    With wobjXMLDoc
'        .async = False
'        .Load App.Path & gcteParamFileName
'        wvarServer = .selectSingleNode("Request/Server").Text
'        wvarPort = .selectSingleNode("Request/Port").Text
'        wvarTimeout = .selectSingleNode("Request/Timeout").Text
'        wvarEncryptionType = .selectSingleNode("Request/Encryptiontype").Text
'        wvarLanguage = .selectSingleNode("Request/Language").Text
'        wvarSecuritytype = .selectSingleNode("Request/Securitytype").Text
'    End With
'
'    wvarstep = 20
'    '
'    'Busco el usuario de ColdView en base al par�metro recibido (pvarAccessWay)
'    If (pvarAccessWay = "") Then
'        'Tomo el usuario por defecto
'        Set wobjXMLConnectionsList = wobjXMLDoc.selectNodes("/Request/Connections/Connection[@Default=""yes""]")
'    Else
'        Set wobjXMLConnectionsList = wobjXMLDoc.selectNodes("/Request/Connections/Connection[@Access=""" & pvarAccessWay & """]")
'    End If
'    '
'    '
'    wvarstep = 30
'    warrConexiones = Split(wobjXMLConnectionsList(0).xml, " ")
'    wvarUserConnection = Replace(Mid(warrConexiones(3), InStr(1, warrConexiones(3), "=") + 1, Len(warrConexiones(3))), """", "")
'    wvarPwdConnection = Replace(Mid(warrConexiones(4), InStr(1, warrConexiones(4), "=") + 1, Len(warrConexiones(4))), """", "")
'    wvarPwdConnection = Replace(wvarPwdConnection, "/>", "")
'    '
'    'Armo el XML de conexi�n a ColdView con el usuario y password correspondiente
'    '(respetando el formato de EColdConfig.xml)
'    wvarXMLCVConnection = "<Request>" & Chr(13) & Chr(9) & _
'                            "<Server>" & wvarServer & "</Server>" & Chr(13) & Chr(9) & _
'                            "<Port>" & wvarPort & "</Port>" & Chr(13) & Chr(9) & _
'                            "<Timeout>" & wvarTimeout & "</Timeout>" & Chr(13) & Chr(9) & _
'                            "<Encryptiontype>" & wvarEncryptionType & "</Encryptiontype>" & Chr(13) & Chr(9) & _
'                            "<Language>" & wvarLanguage & "</Language>" & Chr(13) & Chr(9) & _
'                            "<Securitytype>" & wvarSecuritytype & "</Securitytype>" & Chr(13) & Chr(9) & _
'                            "<User>" & wvarUserConnection & "</User>" & Chr(13) & Chr(9) & _
'                            "<Pwd>" & wvarPwdConnection & "</Pwd>" & Chr(13) & _
'                          "</Request>"
'
'    Set wobjXMLDoc = Nothing
'    Set wobjXMLConnectionsList = Nothing
'
'    Response = wvarXMLCVConnection
'    SelectUserPwd = True
'    Exit Function
'
''~~~~~~~~~~~~~~~
'ErrorHandler:
''~~~~~~~~~~~~~~~
'    '
'    Set wobjXMLDoc = Nothing
'    Set wobjXMLConnectionsList = Nothing
'    '
'        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
'                         mcteClassName, _
'                         wcteFnName, _
'                         wvarstep, _
'                         Err.Number, _
'                         "Error= [" & Err.Number & "] - " & Err.Description & "[XML Config: No se pudo determinar el usuario y password de conexi�n con Cold View para la v�a de acceso: " & pvarAccessWay & "] ", _
'                         vbLogEventTypeError
'    '
'    Response = "Error: No se pudo determinar el usuario y password de conexi�n para la v�a de acceso " & pvarAccessWay
'    SelectUserPwd = False
'
'End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

