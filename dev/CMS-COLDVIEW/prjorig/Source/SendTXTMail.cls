VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "SendTXTMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
' Module Name : SendTXTMail
' File Name : SendTXTMail.cls
' Creation Date: 15/06/2005
' Programmer : Ramirez
' Abstract : Envia un TXT por mail
' *****************************************************************
Option Explicit

' 2008-08-15 - FJO / MC / DA
'Objeto de ColdView
Public mobjCOLDview             As Object

'Objetos del FrameWork
Private mobjCOM_Context             As ObjectContext
Private mobjEventLog                As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbaA_ECold.SendTXTMail"

'Parametros XML
Const mcteParam_CATALOG             As String = "//CATALOG"
Const mcteParam_PAGENBR             As String = "//PAGENBR"
Const mcteParam_CANTPAGE            As String = "//CANTPAGE"
Const mcteParam_ISSUEITEMNBR        As String = "//ISSUEITEMNBR"

Const mcteParam_SUBJECT             As String = "//SUBJECT"
Const mcteParam_MSGBODY             As String = "//MSGBODY"
Const mcteParam_FROM                As String = "//FROM"
Const mcteParam_FROMADDRESS         As String = "//FROMADDRESS"
Const mcteParam_SENDTO              As String = "//SENDTO"
Const mcteParam_REPLYTO             As String = "//REPLYTO"
Const mcteParam_ATTACH              As String = "//ATTACH"
Const mcteParam_ATTACHNAME          As String = "//ATTACHNAME"
Const mcteParam_CC                  As String = "//CC"
Const mcteParam_BCC                 As String = "//BCC"
Const mcteParam_ACCESSWAY           As String = "//ACCESSWAY"

' Parametros Cold View
Const mcteParam_CVISSUEITEMNBR      As String = "//IssueItemNbr"
Const mcteParam_CVPAGENBR           As String = "//PageNbr"
Const mcteParam_CVISSUEID           As String = "//IssueID"

Dim wvarFileName                    As String
Dim wvarFileNameTXT                 As String
Dim wvarFileNameXML                 As String


' *****************************************************************
' Function : IAction_Execute
' Abstract : Busca en la base de Cold View la emision de la fecha pasada
' como parametro, abre la misma y envia el PDF correspondiente por mail a
' la direccion especificada
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjMail            As HSBCInterfaces.IAction
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXMLElement      As MSXML2.IXMLDOMNode
    Dim wobjFileTXT         As Scripting.FileSystemObject
    Dim wobjTXT
    '
    '
    Dim wvarXMLOut          As String
    Dim wvarCatalog         As String
    Dim wvarIssueItemNbr    As String
    Dim wvarPageNbr         As String
    Dim wvarIssueId         As String
    Dim wvarSendTo          As String
    Dim wvarPDFName         As String
    Dim wvarSubject         As String
    Dim wvarMSGBody         As String
    Dim wvarFrom            As String
    Dim wvarFromAddress     As String
    Dim wvarReplyTo         As String
    Dim wvarCC              As String
    Dim wvarBCC             As String
    Dim wvarATTACH          As String
    Dim wvarATTACHNAME      As String
    Dim wvarResponse        As String
    Dim wvarSM_ID           As String
    Dim wvarCounter         As Long
    Dim wvarCANTPAGE        As String
    Dim wvarAccessWay       As String
    '
    Dim wvarstep            As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    ' LEVANTO DE LA BASE DE DATOS LOS MAILS PARA ENVIAR
        ' CARGO LOS PARAMETROS DEL MAIL
        wvarstep = 80
        Set wobjXMLRequest = New MSXML2.DOMDocument
        '
        With wobjXMLRequest
            wobjXMLRequest.async = False
            wobjXMLRequest.loadXML Request
            wvarCatalog = .selectSingleNode(mcteParam_CATALOG).Text
            wvarPageNbr = .selectSingleNode(mcteParam_PAGENBR).Text
            wvarIssueItemNbr = .selectSingleNode(mcteParam_ISSUEITEMNBR).Text
            wvarSendTo = .selectSingleNode(mcteParam_SENDTO).Text
            wvarSubject = .selectSingleNode(mcteParam_SUBJECT).Text
            wvarMSGBody = .selectSingleNode(mcteParam_MSGBODY).Text
            wvarATTACH = .selectSingleNode(mcteParam_ATTACH).Text
            wvarATTACHNAME = .selectSingleNode(mcteParam_ATTACHNAME).Text
            wvarFrom = .selectSingleNode(mcteParam_FROM).Text
            wvarFromAddress = .selectSingleNode(mcteParam_FROMADDRESS).Text
            wvarCANTPAGE = .selectSingleNode(mcteParam_CANTPAGE).Text
            '
            If .selectSingleNode(mcteParam_REPLYTO) Is Nothing Then
                wvarReplyTo = ""
            Else
                wvarReplyTo = .selectSingleNode(mcteParam_REPLYTO).Text
            End If
            '
            wvarCC = .selectSingleNode(mcteParam_CC).Text
            wvarBCC = .selectSingleNode(mcteParam_BCC).Text
            '
            'Verifico que exista el nodo <ACCESSWAY>
            On Error Resume Next
            wvarAccessWay = .selectSingleNode(mcteParam_ACCESSWAY).Text
            If Err.Number <> 0 Then
                wvarAccessWay = ""
            End If
            On Error GoTo ErrorHandler
            '
        End With
        '
    wvarstep = 20
    '
    ' AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES DEL RETRIEVEISSUES
    Set wobjXMLElement = wobjXMLRequest.createElement("KEEPCONNECTED")
    wobjXMLElement.Text = "TRUE"
    wobjXMLRequest.selectSingleNode("//Request").appendChild wobjXMLElement
    '
    ' AGREGO EL NODO ACCESSWAY
    Set wobjXMLElement = wobjXMLRequest.createElement("ACCESSWAY")
    wobjXMLElement.Text = wvarAccessWay
    wobjXMLRequest.selectSingleNode("//Request").appendChild wobjXMLElement
    '
    'MHC ------------------------------------------------------------------------
    ' PIDO CONEXION A COLD VIEW
    wvarstep = 100
    Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
    wvarstep = 102
    If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
        ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
        wvarstep = 103
        Response = "<Response><Estado resultado=""false""/></Response>"
        Set mobjCOLDview = Nothing
        IAction_Execute = 1
        mobjCOM_Context.SetAbort
        Exit Function
    End If
    '
    ' EJECUTO RETRIEVE ISSUES
    'Set wobjClass = mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
    'Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
    'Set wobjClass = Nothing
    RetrieveIssues wobjXMLRequest.xml, wvarResponse, mobjCOLDview
    'FIN MHC --------------------------------------------------------------------
    '
    ' ANALIZO LA RESPUESTA
    wvarstep = 25
    Set wobjXMLRequest = Nothing
    '
    Set wobjXMLRequest = New MSXML2.DOMDocument
    wobjXMLRequest.async = False
    wobjXMLRequest.loadXML wvarResponse
    
    If wobjXMLRequest.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
        ' DESCONECTO EL OBJETO COLD VIEW
        Call ColdViewDisconnect(mobjCOLDview)
        '
        Response = wobjXMLRequest.xml
        IAction_Execute = 0
        mobjCOM_Context.SetComplete
        Exit Function
    Else
        If wvarCatalog <> "AIS - LBA - COMISIONES" Then
'            If wvarPageNbr = "" Then
'                wvarPageNbr = wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR).Text
'            End If
            '
            ' CARGO EL ISSUEITEMNUMBER SI VINO VACIO EN EL REQUEST
            If wvarIssueItemNbr = "" Then
                wvarIssueItemNbr = wobjXMLRequest.selectSingleNode(mcteParam_ISSUEITEMNBR).Text
            End If
        Else
            'wvarPageNbr = 1
            'wvarIssueItemNbr = wobjXMLRequest.selectSingleNode("//ROWS/ROW[ISSUETITLE='" & wvarIssueItemNbr & "']/ISSUEITEMNBR").Text
            wvarIssueItemNbr = 1
        End If
        '
    End If
    '
    Set wobjXMLRequest = Nothing
    '
    ' CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
    wvarstep = 30
    Set wobjXMLRequest = New MSXML2.DOMDocument
    wobjXMLRequest.async = False
    '
    wvarstep = 40
    wobjXMLRequest.Load App.Path & gcteOpenIssue
    '
    ' CARGO EL NODO IssueItemNbr EN EL ARCHIVO
    wvarstep = 50
    wobjXMLRequest.selectSingleNode(mcteParam_CVISSUEITEMNBR).Text = wvarIssueItemNbr
    '
    'MHC - Se pasa la conexion a CV previa a la invocacion del RetrieveIssues
    ' PIDO CONEXION A COLD VIEW
    'wvarstep = 51
    'Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
    'wvarstep = 52
    'If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
    '    ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
    '    wvarstep = 53
    '    Response = "<Response><Estado resultado=""false""/></Response>"
    '    Set mobjCOLDview = Nothing
    '    IAction_Execute = 1
    '    mobjCOM_Context.SetAbort
    '    Exit Function
    'End If
    ' EJECUTO OPEN ISSUE DE COLD VIEW
    wvarstep = 60
    Call mobjCOLDview.XML_Issue_Open(wobjXMLRequest.xml, wvarXMLOut)
    '
    ' VERIFICO LA RESPUESTA DE COLD VIEW
    Set wobjXMLResponse = New MSXML2.DOMDocument
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML wvarXMLOut
    '
    If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarstep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]", _
                         vbLogEventTypeError
        '
        ' DESCONECTO EL OBJETO COLD VIEW
        Call ColdViewDisconnect(mobjCOLDview)
        '
        Response = "<Response><Estado resultado=""false"" mensaje=""NO SE PUDO RECUPERAR EL PDF."" cv_message=""" & wobjXMLResponse.selectSingleNode("//Event_Message").Text & """ cv_errornbr= """ & wobjXMLResponse.selectSingleNode("//Error_Nbr").Text & """/></Response>"
        IAction_Execute = 0
        mobjCOM_Context.SetComplete
        Exit Function
    Else
        ' LEVANTO EL ISSUEID
        wvarIssueId = wobjXMLResponse.selectSingleNode(mcteParam_CVISSUEID).Text
    End If
    '
    Set wobjXMLRequest = Nothing
    '
    ' CARGO EL ARCHIVO XML PARA EL ISSUE PAGE GOTO
    wvarstep = 70
    Set wobjXMLRequest = New MSXML2.DOMDocument
    wobjXMLRequest.async = False
    '
    wvarstep = 80
    wobjXMLRequest.Load App.Path & gcteIssuePageGoto
    '
    ' CARGO EL NODO IssueID Y PageNbr EN EL ARCHIVO
    wvarstep = 90
    wobjXMLRequest.selectSingleNode(mcteParam_CVISSUEID).Text = wvarIssueId
    wobjXMLRequest.selectSingleNode(mcteParam_CVPAGENBR).Text = wvarPageNbr
    '
    ' EJECUTO ISSUE PAGE GOTO DE COLD VIEW
    wvarstep = 100
    Call mobjCOLDview.XML_Issue_Page_Goto(wobjXMLRequest.xml, wvarXMLOut)
    '
    wvarstep = 110
    ' VERIFICO LA RESPUESTA DE COLD VIEW
    Set wobjXMLResponse = New MSXML2.DOMDocument
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML wvarXMLOut
    '
    wvarstep = 120
    If wobjXMLResponse.selectSingleNode("//Status").Text = "-1" Then
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarstep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]", _
                         vbLogEventTypeError
        '
        ' DESCONECTO EL OBJETO COLD VIEW
        Call ColdViewDisconnect(mobjCOLDview)
        '
        Response = "<Response><Estado resultado=""false"" mensaje=""NO SE PUDO RECUPERAR EL PDF.""  cv_message=""" & wobjXMLResponse.selectSingleNode("//Event_Message").Text & """ cv_errornbr= """ & wobjXMLResponse.selectSingleNode("//Error_Nbr").Text & """/></Response>"
        IAction_Execute = 0
        mobjCOM_Context.SetComplete
        Exit Function
    End If
    
    wvarstep = 130
    '
    Set wobjXMLRequest = Nothing
    '
    'Empiezo a tomar las paginas
    Dim mvarPagAct As Long
    Dim mvarTexto As String
    
    mvarPagAct = wvarPageNbr
    mvarTexto = ""
    
    wvarFileName = "TXTMAIL" & Replace(Format(Now(), "######.##########"), ",", "")
    
    For mvarPagAct = wvarPageNbr To CCur(wvarPageNbr) + CInt(wvarCANTPAGE) - 1
        mvarTexto = mvarTexto & mobjCOLDview.Issue_Page_Retrieve(wvarIssueId, 0) & vbCrLf
        If Not mobjCOLDview.Issue_Page_Next(wvarIssueId) Then
            Exit For
        End If
    Next
    
    wvarstep = 140
    
    wvarFileNameTXT = wvarFileName & ".txt"
    Set wobjFileTXT = CreateObject("Scripting.FileSystemObject")
    Set wobjTXT = wobjFileTXT.CreateTextFile(App.Path & gcteParamFilesDir & wvarFileNameTXT)
    
    wvarstep = 150
    
    wobjTXT.Write mvarTexto
    wobjTXT.Close
    Set wobjFileTXT = Nothing
    
    wvarstep = 170
    
    wvarFileNameXML = wvarFileName & ".xml"

    Response = "<Response><Estado resultado=""true"" mensaje="""" />" & _
    "<TO>" & wvarSendTo & "</TO><FROM>" & wvarFrom & "</FROM><CC>" & wvarCC & "</CC><BCC>" & wvarBCC & "</BCC>" & _
    "<REPLYTO>" & wvarReplyTo & "</REPLYTO><BODY>" & wvarMSGBody & "</BODY>" & _
    "<ATTACH><![CDATA[" & App.Path & gcteParamFilesDir & wvarFileName & ".txt]]></ATTACH>" & _
    "<ATTACHNAME><![CDATA[" & wvarATTACHNAME & "]]></ATTACHNAME>" & _
    "<SUBJECT><![CDATA[" & wvarSubject & "]]></SUBJECT>" & _
    "</Response>"

    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        wvarstep = 180
        Call .loadXML(Response)
        Randomize Time
        wvarstep = 190
        .Save App.Path & gcteParamFilesDir & wvarFileNameXML
    End With
    
    ' EJECUTO LA LLAMADA AL FRAMEWORK, LLAMO AL COMPONENTE DE ENVIO DE MAIL Y LE PASO EL XML CREADO
    wvarstep = 200
    Set wobjMail = CreateObject("LBAVIRTUALSendmail.SendMail")
    wvarstep = 210
    Call wobjMail.Execute(wvarFileName, wvarResponse, "")
    Set wobjMail = Nothing
    
    'Borro el archivo que mande por mail y el archivo con los parametros del mail
    Kill App.Path & gcteParamFilesDir & wvarFileName & ".txt"
    Kill App.Path & gcteParamFilesDir & wvarFileName & ".xml"
    '
    ' DESCONECTO EL OBJETO COLD VIEW
    Call ColdViewDisconnect(mobjCOLDview)
    '
    'IAction_Execute = 0
    'mobjCOM_Context.SetComplete

    'Armo el archivo de texto y armo el envio de mail
    
       
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    '
    Call ColdViewDisconnect(mobjCOLDview)
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarstep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "][SM_ID = " & wvarSM_ID & "]", _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub





