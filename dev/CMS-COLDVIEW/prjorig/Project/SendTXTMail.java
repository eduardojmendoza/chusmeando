import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  *****************************************************************
 *  COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 *  THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
 *  HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
 *  DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
 *  TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
 *  FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 *  CONSENT OF HSBC HOLDINGS PLC.
 *  *****************************************************************
 *  Module Name : SendTXTMail
 *  File Name : SendTXTMail.cls
 *  Creation Date: 15/06/2005
 *  Programmer : Ramirez
 *  Abstract : Envia un TXT por mail
 *  *****************************************************************
 *  2008-08-15 - FJO / MC / DA
 * Objeto de ColdView
 */

public class SendTXTMail implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.SendTXTMail";
  /**
   * Parametros XML
   */
  static final String mcteParam_CATALOG = "//CATALOG";
  static final String mcteParam_PAGENBR = "//PAGENBR";
  static final String mcteParam_CANTPAGE = "//CANTPAGE";
  static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
  static final String mcteParam_SUBJECT = "//SUBJECT";
  static final String mcteParam_MSGBODY = "//MSGBODY";
  static final String mcteParam_FROM = "//FROM";
  static final String mcteParam_FROMADDRESS = "//FROMADDRESS";
  static final String mcteParam_SENDTO = "//SENDTO";
  static final String mcteParam_REPLYTO = "//REPLYTO";
  static final String mcteParam_ATTACH = "//ATTACH";
  static final String mcteParam_ATTACHNAME = "//ATTACHNAME";
  static final String mcteParam_CC = "//CC";
  static final String mcteParam_BCC = "//BCC";
  static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
  /**
   *  Parametros Cold View
   */
  static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
  static final String mcteParam_CVPAGENBR = "//PageNbr";
  static final String mcteParam_CVISSUEID = "//IssueID";
  public CVCLFC.CVCL mobjCOLDview = new CVCLFC.CVCL();
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private String wvarFileName = "";
  private String wvarFileNameTXT = "";
  private String wvarFileNameXML = "";
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Busca en la base de Cold View la emision de la fecha pasada
   *  como parametro, abre la misma y envia el PDF correspondiente por mail a
   *  la direccion especificada
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjMail = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.Element wobjXMLElement = null;
    Object wobjFileTXT = null;
    java.io.RandomAccessFile wobjTXT = null;
    String wvarXMLOut = "";
    String wvarCatalog = "";
    String wvarIssueItemNbr = "";
    String wvarPageNbr = "";
    String wvarIssueId = "";
    String wvarSendTo = "";
    String wvarPDFName = "";
    String wvarSubject = "";
    String wvarMSGBody = "";
    String wvarFrom = "";
    String wvarFromAddress = "";
    String wvarReplyTo = "";
    String wvarCC = "";
    String wvarBCC = "";
    String wvarATTACH = "";
    String wvarATTACHNAME = "";
    Variant wvarResponse = new Variant();
    String wvarSM_ID = "";
    int wvarCounter = 0;
    String wvarCANTPAGE = "";
    String wvarAccessWay = "";
    int wvarstep = 0;
    int mvarPagAct = 0;
    String mvarTexto = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // LEVANTO DE LA BASE DE DATOS LOS MAILS PARA ENVIAR
      // CARGO LOS PARAMETROS DEL MAIL
      wvarstep = 80;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCatalog = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CATALOG ) */ );
      wvarPageNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ );
      wvarIssueItemNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR ) */ );
      wvarSendTo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SENDTO ) */ );
      wvarSubject = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT ) */ );
      wvarMSGBody = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MSGBODY ) */ );
      wvarATTACH = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ATTACH ) */ );
      wvarATTACHNAME = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ATTACHNAME ) */ );
      wvarFrom = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FROM ) */ );
      wvarFromAddress = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FROMADDRESS ) */ );
      wvarCANTPAGE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CANTPAGE ) */ );
      //
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REPLYTO ) */ == (org.w3c.dom.Node) null )
      {
        wvarReplyTo = "";
      }
      else
      {
        wvarReplyTo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REPLYTO ) */ );
      }
      //
      wvarCC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CC ) */ );
      wvarBCC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BCC ) */ );
      //
      //Verifico que exista el nodo <ACCESSWAY>
      // On Error Resume Next (optionally ignored)
      wvarAccessWay = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACCESSWAY ) */ );
      if( Err.getError().getNumber() != 0 )
      {
        wvarAccessWay = "";
      }
      //
      //
      wvarstep = 20;
      //
      // AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES DEL RETRIEVEISSUES
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "KEEPCONNECTED" );
      diamondedge.util.XmlDom.setText( wobjXMLElement, "TRUE" );
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( wobjXMLElement );
      //
      // AGREGO EL NODO ACCESSWAY
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "ACCESSWAY" );
      diamondedge.util.XmlDom.setText( wobjXMLElement, wvarAccessWay );
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( wobjXMLElement );
      //
      //MHC ------------------------------------------------------------------------
      // PIDO CONEXION A COLD VIEW
      wvarstep = 100;
      mobjCOLDview = new CVCLFC.CVCL();
      wvarstep = 102;
      if( ModGeneral.ColdViewConnect( wvarAccessWay, mobjCOLDview ) == Obj.toInt( "1" ) )
      {
        // HUBO UN ERROR EN LA CONEXION A COLD VIEW
        wvarstep = 103;
        Response.set( "<Response><Estado resultado=\"false\"/></Response>" );
        mobjCOLDview = (CVCLFC.CVCL) null;
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        return IAction_Execute;
      }
      //
      // EJECUTO RETRIEVE ISSUES
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
      //Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      //Set wobjClass = Nothing
      ModGeneral.RetrieveIssues( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, mobjCOLDview );
      //FIN MHC --------------------------------------------------------------------
      //
      // ANALIZO LA RESPUESTA
      wvarstep = 25;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarResponse.toString() );

      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      else
      {
        if( !wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
        {
          //            If wvarPageNbr = "" Then
          //                wvarPageNbr = wobjXMLRequest.selectSingleNode(mcteParam_PAGENBR).Text
          //            End If
          //
          // CARGO EL ISSUEITEMNUMBER SI VINO VACIO EN EL REQUEST
          if( wvarIssueItemNbr.equals( "" ) )
          {
            wvarIssueItemNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR ) */ );
          }
        }
        else
        {
          //wvarPageNbr = 1
          //wvarIssueItemNbr = wobjXMLRequest.selectSingleNode("//ROWS/ROW[ISSUETITLE='" & wvarIssueItemNbr & "']/ISSUEITEMNBR").Text
          wvarIssueItemNbr = "1";
        }
        //
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
      wvarstep = 30;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 40;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteOpenIssue );
      //
      // CARGO EL NODO IssueItemNbr EN EL ARCHIVO
      wvarstep = 50;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEITEMNBR ) */, wvarIssueItemNbr );
      //
      //MHC - Se pasa la conexion a CV previa a la invocacion del RetrieveIssues
      // PIDO CONEXION A COLD VIEW
      //wvarstep = 51
      //Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
      //wvarstep = 52
      //If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
      //    ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
      //    wvarstep = 53
      //    Response = "<Response><Estado resultado=""false""/></Response>"
      //    Set mobjCOLDview = Nothing
      //    IAction_Execute = 1
      //    mobjCOM_Context.SetAbort
      //    Exit Function
      //End If
      // EJECUTO OPEN ISSUE DE COLD VIEW
      wvarstep = 60;
      mobjCOLDview.XML_Issue_Open( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        //
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\" cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      else
      {
        // LEVANTO EL ISSUEID
        wvarIssueId = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( mcteParam_CVISSUEID ) */ );
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL ISSUE PAGE GOTO
      wvarstep = 70;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 80;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteIssuePageGoto );
      //
      // CARGO EL NODO IssueID Y PageNbr EN EL ARCHIVO
      wvarstep = 90;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEID ) */, wvarIssueId );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPAGENBR ) */, wvarPageNbr );
      //
      // EJECUTO ISSUE PAGE GOTO DE COLD VIEW
      wvarstep = 100;
      mobjCOLDview.XML_Issue_Page_Goto( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      //
      wvarstep = 110;
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      wvarstep = 120;
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        //
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\"  cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }

      wvarstep = 130;
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      //Empiezo a tomar las paginas
      mvarPagAct = Obj.toInt( wvarPageNbr );
      mvarTexto = "";

      wvarFileName = "TXTMAIL" + Strings.replace( DateTime.format( DateTime.now(), "######.##########" ), ",", "" );

      for( mvarPagAct = Obj.toInt( wvarPageNbr ); mvarPagAct <= Obj.toDecimal( wvarPageNbr ).add( new java.math.BigDecimal( Obj.toInt( wvarCANTPAGE ) ) ).subtract( new java.math.BigDecimal( 1 ) ).intValue(); mvarPagAct++ )
      {
        mvarTexto = mvarTexto + mobjCOLDview.Issue_Page_Retrieve( wvarIssueId, 0 ) + System.getProperty("line.separator");
        if( ! (mobjCOLDview.Issue_Page_Next( wvarIssueId )) )
        {
          break;
        }
      }

      wvarstep = 140;

      wvarFileNameTXT = wvarFileName + ".txt";
      wobjFileTXT = new Object();
      wobjTXT = FileSystem.openFile( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileNameTXT, "rw" );

      wvarstep = 150;

      wobjTXT.writeBytes( mvarTexto );
      wobjTXT.close();
      wobjFileTXT = (Object) null;

      wvarstep = 170;

      wvarFileNameXML = wvarFileName + ".xml";

      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + "<TO>" + wvarSendTo + "</TO><FROM>" + wvarFrom + "</FROM><CC>" + wvarCC + "</CC><BCC>" + wvarBCC + "</BCC>" + "<REPLYTO>" + wvarReplyTo + "</REPLYTO><BODY>" + wvarMSGBody + "</BODY>" + "<ATTACH><![CDATA[" + System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileName + ".txt]]></ATTACH>" + "<ATTACHNAME><![CDATA[" + wvarATTACHNAME + "]]></ATTACHNAME>" + "<SUBJECT><![CDATA[" + wvarSubject + "]]></SUBJECT>" + "</Response>" );

      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wvarstep = 180;
      wobjXMLRequest.loadXML( Response.toString() );
      VB.randomize( DateTime.toInt( DateTime.now() ) );
      wvarstep = 190;
      wobjXMLRequest.save( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileNameXML );

      // EJECUTO LA LLAMADA AL FRAMEWORK, LLAMO AL COMPONENTE DE ENVIO DE MAIL Y LE PASO EL XML CREADO
      wvarstep = 200;
      wobjMail = new LBAVIRTUALSendmail.SendMail();
      wvarstep = 210;
      //error: function 'Execute' was not found.
      //unsup: Call wobjMail.Execute(wvarFileName, wvarResponse, "")
      wobjMail = null;

      //Borro el archivo que mande por mail y el archivo con los parametros del mail
      FileSystem.kill( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileName + ".txt" );
      FileSystem.kill( System.getProperty("user.dir") + ModGeneral.gcteParamFilesDir + wvarFileName + ".xml" );
      //
      // DESCONECTO EL OBJETO COLD VIEW
      ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
      //
      //IAction_Execute = 0
      //mobjCOM_Context.SetComplete
      //Armo el archivo de texto y armo el envio de mail
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        //
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "][SM_ID = " + wvarSM_ID + "]", vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
