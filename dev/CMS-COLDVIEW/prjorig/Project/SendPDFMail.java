import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  *****************************************************************
 *  COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 *  THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
 *  HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
 *  DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
 *  TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
 *  FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 *  CONSENT OF HSBC HOLDINGS PLC.
 *  *****************************************************************
 *  Module Name : SendPDFMailAsync
 *  File Name : SendPDFMailAsync.cls
 *  Creation Date: 13/01/2004
 *  Programmer : Muzzupappa - Goncalves
 *  Abstract : Envia un PDF por mail de la base de Cold View
 *  *****************************************************************
 *  2008-08-15 - FJO / MC / DA
 * Objeto de ColdView
 */

public class SendPDFMail implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.SendPDFMail";
  /**
   * Parametros XML
   */
  static final String mcteParam_CATALOG = "//CATALOG";
  static final String mcteParam_PAGENBR = "//PAGENBR";
  static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
  static final String mcteParam_SENDTO = "//SENDTO";
  static final String mcteParam_GIVENATTACHPDFNAME = "//GIVENATTACHPDFNAME";
  static final String mcteParam_SUBJECT = "//SUBJECT";
  static final String mcteParam_MSGBODY = "//MSGBODY";
  static final String mcteParam_FROM = "//FROM";
  static final String mcteParam_FROMADDRESS = "//FROMADDRESS";
  static final String mcteParam_REPLYTO = "//REPLYTO";
  static final String mcteParam_CC = "//CC";
  static final String mcteParam_BCC = "//BCC";
  static final String mcteParam_PDFUSRPWD = "//PDFUSRPWD";
  static final String mcteParam_PDFMSTRPWD = "//PDFMSTRPWD";
  static final String mcteParam_PDFKEYLENGTH = "//PDFKEYLENGTH";
  static final String mcteParam_PDFNOPRINT = "//PDFNOPRINT";
  static final String mcteParam_PDFNOMODIFY = "//PDFNOMODIFY";
  static final String mcteParam_PDFNOCOPY = "//PDFNOCOPY";
  static final String mcteParam_PDFNOANNOTS = "//PDFNOANNOTS";
  static final String mcteParam_ADDITIONALFILES = "//ADDITIONALFILES";
  static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
  /**
   *  Parametros Cold View
   */
  static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
  static final String mcteParam_CVPAGENBR = "//PageNbr";
  static final String mcteParam_CVISSUEID = "//IssueID";
  static final String mcteParam_CVSENDTO = "//SendTo";
  static final String mcteParam_CVGIVENATTACHPDFNAME = "//GivenAttachPDFName";
  static final String mcteParam_CVSUBJECT = "//Subject";
  static final String mcteParam_CVMSGBODY = "//MsgBody";
  static final String mcteParam_CVFROM = "//From";
  static final String mcteParam_CVFROMADDRESS = "//FromAddress";
  static final String mcteParam_CVREPLYTO = "//ReplyTo";
  static final String mcteParam_CVPDFUSRPWD = "//PDFUsrPWD";
  static final String mcteParam_CVPDFMSTRPWD = "//PDFMstrPWD";
  static final String mcteParam_CVPDFKEYLENGTH = "//PDFKeyLength";
  static final String mcteParam_CVPDFNOPRINT = "//PDFNoprint";
  static final String mcteParam_CVPDFNOMODIFY = "//PDFNomodify";
  static final String mcteParam_CVPDFNOCOPY = "//PDFNocopy";
  static final String mcteParam_CVPDFNOANNOTS = "//PDFNoannots";
  static final String mcteParam_CVADDITIONALFILES = "//AdditionalAttachFiles";
  public CVCLFC.CVCL mobjCOLDview = new CVCLFC.CVCL();
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Busca en la base de Cold View la emision de la fecha pasada
   *  como parametro, abre la misma y envia el PDF correspondiente por mail a
   *  la direccion especificada
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    org.w3c.dom.Element wobjXMLElement = null;
    String wvarXMLOut = "";
    String wvarCatalog = "";
    String wvarIssueItemNbr = "";
    String wvarPageNbr = "";
    String wvarIssueId = "";
    String wvarSendTo = "";
    String wvarPDFName = "";
    String wvarSubject = "";
    String wvarMSGBody = "";
    String wvarFrom = "";
    String wvarFromAddress = "";
    String wvarReplyTo = "";
    String wvarCC = "";
    String wvarBCC = "";
    Variant wvarResponse = new Variant();
    String wvarSM_ID = "";
    int wvarCounter = 0;
    String wvarPDFUsrPWD = "";
    String wvarPDFMstrPWD = "";
    String wvarPDFKeyLength = "";
    String wvarPDFNoPrint = "";
    String wvarPDFNoModify = "";
    String wvarPDFNoCopy = "";
    String wvarPDFNoAnnots = "";
    String wvarAdditionalFiles = "";
    String wvarAccessWay = "";
    int wvarstep = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // LEVANTO DE LA BASE DE DATOS LOS MAILS PARA ENVIAR
      // CARGO LOS PARAMETROS DEL MAIL
      wvarstep = 80;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCatalog = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CATALOG ) */ );
      wvarPageNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ );
      wvarIssueItemNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR ) */ );
      wvarSendTo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SENDTO ) */ );
      wvarPDFName = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GIVENATTACHPDFNAME ) */ );
      wvarSubject = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT ) */ );
      wvarMSGBody = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MSGBODY ) */ );
      wvarFrom = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FROM ) */ );
      wvarFromAddress = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FROMADDRESS ) */ );
      //
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REPLYTO ) */ == (org.w3c.dom.Node) null )
      {
        wvarReplyTo = "";
      }
      else
      {
        wvarReplyTo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REPLYTO ) */ );
      }
      //
      wvarCC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CC ) */ );
      wvarBCC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BCC ) */ );
      wvarPDFUsrPWD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFUSRPWD ) */ );
      wvarPDFMstrPWD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFMSTRPWD ) */ );
      wvarPDFKeyLength = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFKEYLENGTH ) */ );
      wvarPDFNoPrint = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFNOPRINT ) */ );
      wvarPDFNoModify = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFNOMODIFY ) */ );
      wvarPDFNoCopy = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFNOCOPY ) */ );
      wvarPDFNoAnnots = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PDFNOANNOTS ) */ );
      //
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ADDITIONALFILES ) */ == (org.w3c.dom.Node) null )
      {
        wvarAdditionalFiles = "";
      }
      else
      {
        wvarAdditionalFiles = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ADDITIONALFILES ) */ );
      }

      //Verifico que exista el nodo <ACCESSWAY>
      // On Error Resume Next (optionally ignored)
      wvarAccessWay = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACCESSWAY ) */ );
      if( Err.getError().getNumber() != 0 )
      {
        wvarAccessWay = "";
      }
      //
      //
      wvarstep = 90;
      //
      // AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES DEL RETRIEVEISSUES
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "KEEPCONNECTED" );
      diamondedge.util.XmlDom.setText( wobjXMLElement, "TRUE" );
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( wobjXMLElement );
      //
      // AGREGO EL NODO ACCESSWAY
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "ACCESSWAY" );
      diamondedge.util.XmlDom.setText( wobjXMLElement, wvarAccessWay );
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( wobjXMLElement );
      //
      //MHC ------------------------------------------------------------------------
      // PIDO CONEXION A COLD VIEW
      wvarstep = 100;
      mobjCOLDview = new CVCLFC.CVCL();
      wvarstep = 102;
      if( ModGeneral.ColdViewConnect( wvarAccessWay, mobjCOLDview ) == Obj.toInt( "1" ) )
      {
        // HUBO UN ERROR EN LA CONEXION A COLD VIEW
        wvarstep = 103;
        Response.set( "<Response><Estado resultado=\"false\"/></Response>" );
        mobjCOLDview = (CVCLFC.CVCL) null;
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        return IAction_Execute;
      }
      //
      // EJECUTO RETRIEVE ISSUES
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
      //Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      //Set wobjClass = Nothing
      ModGeneral.RetrieveIssues( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, mobjCOLDview );
      //FIN MHC --------------------------------------------------------------------
      //
      // ANALIZO LA RESPUESTA
      wvarstep = 110;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarResponse.toString() );
      //
      wvarstep = 120;
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      else
      {
        if( wvarPageNbr.equals( "" ) )
        {
          wvarPageNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ );
        }
      }
      //
      if( wvarIssueItemNbr.equals( "" ) )
      {
        wvarIssueItemNbr = "1";
      }
      wvarstep = 130;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
      wvarstep = 140;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 150;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteOpenIssue );
      //
      // CARGO EL NODO IssueItemNbr EN EL ARCHIVO
      wvarstep = 160;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEITEMNBR ) */, wvarIssueItemNbr );
      //
      //MHC - Se pasa la conexion a CV previa a la invocacion del RetrieveIssues
      // PIDO CONEXION A COLD VIEW
      //wvarstep = 161
      //Set mobjCOLDview = CreateObject("CVCLFC.CVCL")
      //wvarstep = 162
      //If ColdViewConnect(wvarAccessWay, mobjCOLDview) = "1" Then
      //    ' HUBO UN ERROR EN LA CONEXION A COLD VIEW
      //    wvarstep = 163
      //    Response = "<Response><Estado resultado=""false""/></Response>"
      //    Set mobjCOLDview = Nothing
      //    IAction_Execute = 1
      //    mobjCOM_Context.SetAbort
      //    Exit Function
      //End If
      //
      // EJECUTO OPEN ISSUE DE COLD VIEW
      wvarstep = 170;
      mobjCOLDview.XML_Issue_Open( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      wvarstep = 180;
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\" cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      else
      {
        // LEVANTO EL ISSUEID
        wvarIssueId = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( mcteParam_CVISSUEID ) */ );
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL SEND MAIL
      wvarstep = 190;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 200;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gctePDFSendMail );
      //
      // CARGO LOS NODOS CORRESPONDIENTES EN EL ARCHIVO
      wvarstep = 210;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEID ) */, wvarIssueId );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPAGENBR ) */, wvarPageNbr );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVSENDTO ) */, wvarSendTo );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVGIVENATTACHPDFNAME ) */, wvarPDFName );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVSUBJECT ) */, wvarSubject );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVMSGBODY ) */, wvarMSGBody );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVFROM ) */, wvarFrom );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVFROMADDRESS ) */, wvarFromAddress );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVREPLYTO ) */, wvarReplyTo );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CC ) */, wvarCC );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BCC ) */, wvarBCC );
      //
      // CARGO LOS NODOS CORRESPONDIENTES AL ENCRIPTADO
      wvarstep = 215;
      // SI EL KEY LENGTH ES 0 NO SE PUEDE CONFIGURAR USER PWD NI MASTER PWD
      if( wvarPDFKeyLength.equals( "0" ) )
      {
        wvarPDFUsrPWD = "";
        wvarPDFMstrPWD = "";
      }
      //
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFUSRPWD ) */, wvarPDFUsrPWD );
      //
      // SI EL KEY LENGTH ES <> 0 Y MASTER PWD ES "" USO LA DEL ARCHIVO
      if( (wvarPDFKeyLength.equals( "0" )) || (!wvarPDFMstrPWD.equals( "" )) )
      {
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFMSTRPWD ) */, wvarPDFMstrPWD );
      }
      //
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFKEYLENGTH ) */, wvarPDFKeyLength );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOPRINT ) */, wvarPDFNoPrint );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOMODIFY ) */, wvarPDFNoModify );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOCOPY ) */, wvarPDFNoCopy );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOANNOTS ) */, wvarPDFNoAnnots );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVADDITIONALFILES ) */, wvarAdditionalFiles );
      //
      // EJECUTO ISSUE PAGE PDFSENDMAIL DE COLD VIEW
      wvarstep = 220;
      mobjCOLDview.XML_Issue_Page_PDFSendMail( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );

      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        wvarstep = 240;
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\"  cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // DESCONECTO EL OBJETO COLD VIEW
      wvarstep = 250;
      ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
      //
      //
      wvarstep = 280;
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"MAIL ENVIADO\" /></Response>" );
      //
      IAction_Execute = 0;
      /*unsup mobjCOM_Context.SetComplete() */;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        //
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "][SM_ID = " + wvarSM_ID + "]", vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
