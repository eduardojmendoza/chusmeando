import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
import java.applet.*;

public class lbaA_ECold extends JApplet
{
  static {
    try {
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (Exception e) { System.out.println(e); }
  }
  public static String Title = "lbaA_ECold";
  public static String ProductName = "";
  public static int MajorVersion = 2;
  public static int MinorVersion = 0;
  public static int Revision = 9;
  public static String HelpFile = "";
  public static String Comments = "";
  public static String FileDescription = "";
  public static String CompanyName = "HSBC";
  public static String LegalCopyright = "";
  public static String LegalTrademarks = "";

  public lbaA_ECold()
  {
  }

  // called only when running as an applet
  public void init()
  {
    getContentPane().setLayout( new java.awt.BorderLayout() );
    Application app = new Application( "lbaA_ECold" );
  }

  public String getAppletInfo()
  {
    return "lbaA_ECold" + " " + LegalCopyright;
  }

  // called only when running as a stand-alone application
  public static void main( String args[] )
  {
    final Application app = new Application( "lbaA_ECold" );
    app.setApplication( new lbaA_ECold(), args );
    try
    {
    }
    catch(Exception e) { Err.set(e); }
    app.endApplication();
    javax.swing.SwingUtilities.invokeLater( new Runnable() {
      public void run() {
        Application.setApplication( app );
      }
    });
  }
}
