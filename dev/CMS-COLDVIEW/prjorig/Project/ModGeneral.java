import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  *****************************************************************
 *  COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 *  THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
 *  HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
 *  DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
 *  TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
 *  FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 *  CONSENT OF HSBC HOLDINGS PLC.
 *  *****************************************************************
 *  Module Name : ModGeneral
 *  File Name : ModGeneral.bas
 *  Creation Date: 10/10/2003
 *  Programmer : Muzzupappa - Goncalves
 *  Abstract : Declaraciones de constantes
 *  *****************************************************************
 *  API COLD VIEW
 * Public mobjCOLDview             As CVCLFC.CVCL
 * Public mobjCOLDview             As Object
 *  UDL DE CONEXION
 * FIN MHC ------------------------------------------------------------------------
 */

public class ModGeneral
{
  public static final String gcteDB = "lbaA_eCupons.udl";
  public static final String gcteDBColdView = "lbaA_ColdView.udl";
  /**
   *  PARAMETROS XML PARA COLD VIEW - NOMBRES DE ARCHIVO
   *  PARAMETROS DE CONEXION
   */
  public static final String gcteParamFileName = "\\XMLFiles\\EColdConfig-Nuevo.xml";
  /**
   *  ESTRUCTURA XML PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE CUENTA
   */
  public static final String gcteRetIssuesAISCOB = "\\XMLFiles\\RetIssues_AISCOB.xml";
  /**
   *  XSL PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE COMISIONES
   */
  public static final String gcteRetIssuesAISCOB_XSL = "\\XSLFiles\\RetIssues_AISCOB.xsl";
  /**
   *  ESTRUCTURA XML PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE COMISIONES
   */
  public static final String gcteRetIssuesAISCOM = "\\XMLFiles\\RetIssues_AISCOM.xml";
  /**
   *  XSL PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE CUENTA
   */
  public static final String gcteRetIssuesAISCOM_XSL = "\\XSLFiles\\RetIssues_AISCOM.xsl";
  /**
   *  ESTRUCTURA XML PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE COMISIONES
   */
  public static final String gcteRetIssuesAISOPER = "\\XMLFiles\\RetIssues_AISOPER.xml";
  /**
   *  XSL PARA RETRIEVE ISSUES WITH SEARCH DE RESUMENES DE CUENTA
   */
  public static final String gcteRetIssuesAISOPER_XSL = "\\XSLFiles\\RetIssues_AISOPER.xsl";
  /**
   *  XML GENERALES
   *  ESTRUCTURA XML PARA OPEN ISSUE
   */
  public static final String gcteOpenIssue = "\\XMLFiles\\OpenIssue.xml";
  /**
   *  ESTRUCTURA XML PARA ISSUE PAGE GOTO
   */
  public static final String gcteIssuePageGoto = "\\XMLFiles\\IssuePageGoto.xml";
  /**
   *  ESTRUCTURA XML PARA ISSUE PROCESS PDF
   */
  public static final String gcteIssueProcessPDF = "\\XMLFiles\\IssueProcessPDF.xml";
  /**
   *  ESTRUCTURA XML PARA PDF SEND MAIL
   */
  public static final String gctePDFSendMail = "\\XMLFiles\\PDFSendMail.xml";
  /**
   *  QUERIES COLD VIEW
   */
  public static final String gcteQueryParameter = "INPUT_PARAM";
  public static final String gcteQueryClause = "(factura = 'INPUT_PARAM')";
  /**
   * 
   *  FILTROS PARA HIE
   */
  public static final String gcteHIEFilter = "//xsl:apply-templates[@select = '/Response/Results/Row']/@select";
  public static final String gcteHIEFilterText = "/Response/Results/Row[IssueTitle=\"INPUT_PARAM\"]";
  /**
   * 
   *  DIRECTORIO DE ARCHIVOS GENERADOS
   */
  public static final String gcteParamFilesDir = "\\GenFiles\\";
  /**
   *  PROGRAMA PARA EJECUTAR LAS TRANSACCIONES
   */
  public static final String gcteParamAsyncExec = "vbcA_AsyncExecution.exe";
  /**
   * static variable for method: ColdViewConnect
   * static variable for method: SelectUserPwd
   * static variable for method: RetrieveIssues
   */
  private static final String wcteFnName = "RetrieveIssues";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_CATALOG = "//CATALOG";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_DATEISSUE = "//DATEISSUE";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_IDX_BUSQUEDA = "//IDX_BUSQUEDA";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_IDX_SISTEMA = "//IDX_SISTEMA";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_PAGENBR = "//PAGENBR";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_TIPO_AVISO = "//TIPO_AVISO";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_DOCKEY = "//DOCKEY";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_CVDATEFROM = "//IssueDateFrom";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_CVDATETO = "//IssueDateTo";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_CVQUERYCLAUSE = "//QueryClause";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_CVDOCKEY = "//DocKey";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_KEEPCONNECTED = "//KEEPCONNECTED";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
  /**
   * static variable for method: RetrieveIssues
   */
  private static final String mcteParam_QueryClause = "//QueryClause";

  /**
   *  *****************************************************************
   *  Function : ColdViewDisconnect
   *  Abstract : Desconecta y destruye el objeto ColdView
   *  Synopsis : ColdViewDisconnect() As Long
   *  *****************************************************************
   *  *****************************************************************
   *  2008-08-15 - FJO / MC / DA
   *  Se modifica la llamada a la funci�n para que reciba el objeto por parametro para que no
   *  este definido en el m�dulo global para evitar problemas en COM+.
   * Public Sub ColdViewDisconnect()
   */
  public static void ColdViewDisconnect( CVCLFC.CVCL mobjCOLDview ) throws Exception
  {
    // *****************************************************************
    if( ! (mobjCOLDview == (CVCLFC.CVCL) null) )
    {
      // *****************************************************************
      // 2008-08-11 - FJO - A instancias de IT Arquitectura.
      // Se comenta el IF para evitar que un mal uso del flag por parte de la API del proveedor afecte en la performance de la aplicaci�n.
      //If mobjCOLDview.Connected Then
      //    mobjCOLDview.Disconnect
      //End If
      mobjCOLDview.Disconnect();
      // *****************************************************************
      //
      //warning: modifying ByRef argument 'mobjCOLDview'. It must be a Variant.
      mobjCOLDview = (CVCLFC.CVCL) null;
    }
  }

  /**
   *  *****************************************************************
   *  Function : ColdViewConnect
   *  Abstract : Crea el objeto Cold View, se conecta y valida el usuario
   *  Synopsis : ColdViewConnect() As Long
   *  *****************************************************************
   *  *****************************************************************
   *  2008-08-15 - FJO / MC / DA
   *  Se modifica la llamada a la funci�n para que reciba el objeto por parametro para que no
   *  este definido en el m�dulo global para evitar problemas en COM+.
   * Private Function ColdViewConnect(ByVal pvarAccessWay As String) As Long
   */
  public static int ColdViewConnect( String pvarAccessWay, CVCLFC.CVCL mobjCOLDview )
  {
    int ColdViewConnect = 0;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    String wvarXMLOut = "";
    int wvarstep = 0;
    String wvarresult = "";
    Variant wvarResponseXML = new Variant();
    // *****************************************************************
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarstep = 61;
      // SELECCIONO EL USUARIO Y CLAVE DE CONEXION
      if( ! (SelectUserPwd( pvarAccessWay, wvarResponseXML )) )
      {
        Err.raise( -1, "SelectUserPwd", "Error en la selecci�n de usuario y password de conexi�n a Cold View. Detalle:" + wvarResponseXML );
      }
      //
      // EJECUTO LA CONEXION
      wvarstep = 63;
      mobjCOLDview.XML_Connect( wvarResponseXML, wvarXMLOut );
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wvarstep = 64;
      //
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        Err.raise( -1, "ColdViewConnect", "Error en la conexion a ColdView. Detalle:" + wvarXMLOut );
      }
      //
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      //
      // VALIDO EL USUARIO
      //buscar wobjXMLParameters.xml
      wvarstep = 65;
      mobjCOLDview.XML_ValidateUser( wvarResponseXML, wvarXMLOut );
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wvarstep = 66;
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        Err.raise( -1, "ColdViewConnect", "Error al validar el usuario ColdView. Detalle:" + wvarXMLOut );
      }
      //
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      //
      wvarstep = 67;
      //
      ColdViewConnect = 0;
      return ColdViewConnect;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        //mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
        //     '                 mcteClassName, _
        //                 wcteFnName, _
        //     '                 wvarstep, _
        //                 Err.Number, _
        //     '                 "Error= [" & Err.Number & "] - " & Err.Description & "[XML Cold View: " & wvarXMLOut & "]", _
        //                 vbLogEventTypeError
        //
        ColdViewConnect = 1;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return ColdViewConnect;
  }

  /**
   *  *****************************************************************
   *  Function : SelectUserPwd
   *  Abstract : Determina el usuario y password de conexi�n a Cold View
   *  Synopsis : SelectUserPwd(ByVal pvarAccessWay As String, Response As String) As Boolean
   *  *****************************************************************
   *  El formato del XML que recibe el objeto Cold View para conexi�n es el siguiente:
   * <Request>
   *     <Server>valor</Server>
   *     <Port>valor</Port>
   *     <Timeout>valor</Timeout>
   *     <Encryptiontype>valor</Encryptiontype>
   *     <Language>valor</Language>
   *     <Securitytype>valor</Securitytype>
   *     <User>valor</User>
   *     <Pwd>valor</Pwd>
   * </Request>
   */
  private static boolean SelectUserPwd( String pvarAccessWay, Variant Response )
  {
    boolean SelectUserPwd = false;
    int wvarstep = 0;
    diamondedge.util.XmlDom wobjXMLDoc = null;
    org.w3c.dom.NodeList wobjXMLConnectionsList = null;
    String[] warrConexiones = null;
    String wvarUserConnection = "";
    String wvarPwdConnection = "";
    String wvarXMLCVConnection = "";
    String wvarServer = "";
    String wvarPort = "";
    String wvarTimeout = "";
    String wvarEncryptionType = "";
    String wvarLanguage = "";
    String wvarSecuritytype = "";



    //String XML a devolver por la funci�n
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarstep = 10;
      wobjXMLDoc = new diamondedge.util.XmlDom();

      //unsup wobjXMLDoc.async = false;
      wobjXMLDoc.load( System.getProperty("user.dir") + gcteParamFileName );
      wvarServer = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDoc.selectSingleNode( "Request/Server" ) */ );
      wvarPort = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDoc.selectSingleNode( "Request/Port" ) */ );
      wvarTimeout = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDoc.selectSingleNode( "Request/Timeout" ) */ );
      wvarEncryptionType = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDoc.selectSingleNode( "Request/Encryptiontype" ) */ );
      wvarLanguage = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDoc.selectSingleNode( "Request/Language" ) */ );
      wvarSecuritytype = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLDoc.selectSingleNode( "Request/Securitytype" ) */ );

      wvarstep = 20;
      //
      //Busco el usuario de ColdView en base al par�metro recibido (pvarAccessWay)
      if( pvarAccessWay.equals( "" ) )
      {
        //Tomo el usuario por defecto
        wobjXMLConnectionsList = null /*unsup wobjXMLDoc.selectNodes( "/Request/Connections/Connection[@Default=\"yes\"]" ) */;
      }
      else
      {
        wobjXMLConnectionsList = null /*unsup wobjXMLDoc.selectNodes( "/Request/Connections/Connection[@Access=\"" + pvarAccessWay + "\"]" ) */;
      }
      //
      //
      wvarstep = 30;
      warrConexiones = Strings.split( wobjXMLConnectionsList.item( 0 ).toString(), " ", -1 );
      wvarUserConnection = Strings.replace( Strings.mid( warrConexiones[3], Strings.find( 1, warrConexiones[3], "=" ) + 1, Strings.len( warrConexiones[3] ) ), "\"", "" );
      wvarPwdConnection = Strings.replace( Strings.mid( warrConexiones[4], Strings.find( 1, warrConexiones[4], "=" ) + 1, Strings.len( warrConexiones[4] ) ), "\"", "" );
      wvarPwdConnection = Strings.replace( wvarPwdConnection, "/>", "" );
      //
      //Armo el XML de conexi�n a ColdView con el usuario y password correspondiente
      //(respetando el formato de EColdConfig.xml)
      wvarXMLCVConnection = "<Request>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Server>" + wvarServer + "</Server>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Port>" + wvarPort + "</Port>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Timeout>" + wvarTimeout + "</Timeout>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Encryptiontype>" + wvarEncryptionType + "</Encryptiontype>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Language>" + wvarLanguage + "</Language>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Securitytype>" + wvarSecuritytype + "</Securitytype>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<User>" + wvarUserConnection + "</User>" + String.valueOf( (char)(13) ) + String.valueOf( (char)(9) ) + "<Pwd>" + wvarPwdConnection + "</Pwd>" + String.valueOf( (char)(13) ) + "</Request>";

      wobjXMLDoc = (diamondedge.util.XmlDom) null;
      wobjXMLConnectionsList = (org.w3c.dom.NodeList) null;

      Response.set( wvarXMLCVConnection );
      SelectUserPwd = true;
      return SelectUserPwd;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLDoc = (diamondedge.util.XmlDom) null;
        wobjXMLConnectionsList = (org.w3c.dom.NodeList) null;
        //
        //    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
        // '        mcteClassName, _
        //        wcteFnName, _
        // '        wvarstep, _
        //        Err.Number, _
        // '        "Error= [" & Err.Number & "] - " & Err.Description & "[XML Config: No se pudo determinar el usuario y password de conexi�n con Cold View para la v�a de acceso: " & pvarAccessWay & "] ", _
        //        vbLogEventTypeError
        //
        Response.set( "Error: No se pudo determinar el usuario y password de conexi�n para la v�a de acceso " + pvarAccessWay );
        SelectUserPwd = false;

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return SelectUserPwd;
  }

  /**
   *  *****************************************************************
   *  Function : RetrieveIssues
   *  Abstract : Reemplazo de la clase RetrieveIssues a una funcion
   *  Synopsis : RetrieveIssues() As Long
   *  *****************************************************************
   *  2008-08-15 - FJO / MC / DA
   *  Se modifica la llamada a la funci�n para que reciba el objeto por parametro para que no
   *  este definido en el m�dulo global para evitar problemas en COM+.
   * MHC ------------------------------------------------------------------------
   */
  public static int RetrieveIssues( String pvarRequest, Variant pvarResponse, CVCLFC.CVCL pobjCOLDview )
  {
    int RetrieveIssues = 0;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    String wvarXMLOut = "";
    String wvarCatalog = "";
    String wvarDateIssue = "";
    String wvarIDX_Busqueda = "";
    String wvarQuerySearch = "";
    String wvarIDX_Sistema = "";
    String wvarPageNbr = "";
    String wvarTipo_Aviso = "";
    String wvarDocKey = "";
    int wvarstep = 0;
    String wvarresult = "";
    boolean wvarKeepConnected = false;
    String wvarAccessWay = "";
    // *****************************************************************
    //
    //Parametros XML de Entrada
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // CARGO LOS PARAMETROS DE ENTRADA
      wvarstep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      wvarCatalog = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CATALOG ) */ );
      wvarDateIssue = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DATEISSUE ) */ );
      wvarIDX_Busqueda = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_IDX_BUSQUEDA ) */ );
      wvarQuerySearch = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_QueryClause ) */ == (org.w3c.dom.Node) null) )
      {
        wvarQuerySearch = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_QueryClause ) */ );
      }
      wvarDocKey = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOCKEY ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDocKey = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOCKEY ) */ );
      }
      else if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVDOCKEY ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDocKey = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVDOCKEY ) */ );
      }
      //
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ == (org.w3c.dom.Node) null )
      {
        wvarPageNbr = "";
      }
      else
      {
        wvarPageNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ );
      }
      //
      wvarstep = 20;
      //
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_KEEPCONNECTED ) */ == (org.w3c.dom.Node) null )
      {
        wvarKeepConnected = false;
      }
      else
      {
        wvarKeepConnected = true;
      }
      //
      //Verifico que exista el nodo <ACCESSWAY>
      // On Error Resume Next (optionally ignored)
      wvarAccessWay = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACCESSWAY ) */ );
      if( Err.getError().getNumber() != 0 )
      {
        wvarAccessWay = "";
      }

      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL ISSUE CORRESPONDIENTE
      wvarstep = 30;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 40;
      
      if( wvarCatalog.equals( "LBA - AIS COBRANZAS" ) )
      {
        wobjXMLRequest.load( System.getProperty("user.dir") + gcteRetIssuesAISCOB );
      }
      else if( wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
      {
        wobjXMLRequest.load( System.getProperty("user.dir") + gcteRetIssuesAISCOM );
      }
      else if( wvarCatalog.equals( "LBA - EMISION" ) )
      {
        wobjXMLRequest.load( System.getProperty("user.dir") + gcteRetIssuesAISOPER );
      }
      //CARGO el DOCKEY
      wvarstep = 45;
      if( !wvarDocKey.equals( "" ) )
      {
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVDOCKEY ) */, wvarDocKey );
      }
      // CARGO LOS NODOS FECHA_DESDE, FECHA_HASTA Y QUERY EN EL ARCHIVO
      wvarstep = 50;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVDATEFROM ) */, wvarDateIssue );
      wvarstep = 51;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVDATETO ) */, wvarDateIssue );
      //
      wvarstep = 52;
      
      if( wvarCatalog.equals( "LBA - AIS COBRANZAS" ) )
      {
        wvarstep = 53;
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVQUERYCLAUSE ) */, Strings.replace( gcteQueryClause, gcteQueryParameter, wvarIDX_Busqueda ) );
      }
      else if( wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
      {
        wvarstep = 54;
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVQUERYCLAUSE ) */, wvarQuerySearch );
      }
      //
      // EJECUTO RETRIEVE ISSUES DE COLD VIEW
      wvarstep = 70;
      //
      if( wvarKeepConnected && (Strings.len( Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_QueryClause ) */ ) ) ) == 0) && (!wvarPageNbr.equals( "" )) )
      {
        // LA LLAMADA SE HACE DESDE GETXMLPDF, EJECUTO RETRIEVEISSUES
        pobjCOLDview.XML_RetrieveIssues( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      }
      else
      {
        // LA LLAMADA SE HACE DESDE LA PAGINA, EJECUTO RETRIEVEISSUESWITHSEARCH
        pobjCOLDview.XML_RetrieveIssuesWithSearch( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      }
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      wvarstep = 80;
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE ENCONTRARON DATOS PARA EL RANGO DE FECHAS.\"  cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        RetrieveIssues = 1;
        return RetrieveIssues;
      }
      //
      // CARGO EL XSL PARA TRANSFORMAR LA RESPUESTA
      wvarstep = 90;
      wobjXSLResponse = new diamondedge.util.XmlDom();
      //
      //unsup wobjXSLResponse.async = false;

      
      if( wvarCatalog.equals( "LBA - AIS COBRANZAS" ) )
      {
        wobjXSLResponse.load( System.getProperty("user.dir") + gcteRetIssuesAISCOB_XSL );
      }
      else if( wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
      {
        wobjXSLResponse.load( System.getProperty("user.dir") + gcteRetIssuesAISCOM_XSL );
      }
      else if( wvarCatalog.equals( "LBA - EMISION" ) )
      {
        wobjXSLResponse.load( System.getProperty("user.dir") + gcteRetIssuesAISOPER_XSL );
      }
      //
      wvarresult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
      wvarstep = 100;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      wobjXSLResponse = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarstep = 110;
      pvarResponse.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarresult + "</Response>" );
      //
      RetrieveIssues = 0;
      return RetrieveIssues;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        pvarResponse.set( "<Response><Estado resultado=\"false\"/></Response>" );
        //
        RetrieveIssues = 1;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return RetrieveIssues;
  }
}
