<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>
	<xsl:template match='/Response/SubSrvRet'> 
		<xsl:apply-templates select='SubSrvRet'/>
	</xsl:template>
	<xsl:template match='/Response/Results/Rows'> 
		<xsl:apply-templates select='Rows'/>
	</xsl:template>
	<xsl:template match='/Response/Results/IndexCols'> 
		<xsl:apply-templates select='IndexCols'/>
	</xsl:template>
   	<xsl:template match='/Response/Results'>
		<xsl:element name='ROWS'>
			<xsl:apply-templates select='Row'/>
		</xsl:element>
	</xsl:template>
	<xsl:template match='/Response/Results/Row'>
		<xsl:element name='ROW'>
			<xsl:element name='ISSUEITEMNBR'><xsl:value-of select='IssueItemNbr' /></xsl:element>
			<xsl:element name='ISSUETITLE'><xsl:value-of select='IssueTitle' /></xsl:element>
			<xsl:element name='ISSUEASOFDATE'><xsl:value-of select='IssueAsOfDate' /></xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
