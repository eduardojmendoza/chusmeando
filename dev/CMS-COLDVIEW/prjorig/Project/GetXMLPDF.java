import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  *****************************************************************
 *  COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
 * 
 *  THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
 *  HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
 *  DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
 *  TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
 *  FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
 *  CONSENT OF HSBC HOLDINGS PLC.
 *  *****************************************************************
 *  Module Name : GetXMLPDF
 *  File Name : GetXMLPDF.cls
 *  Creation Date: 04/11/2003
 *  Programmer : Muzzupappa - Goncalves
 *  Abstract : Obtiene un PDF en formato XML de la base de Cold View
 *  *****************************************************************
 *  2008-08-15 - FJO / MC / DA
 * Objeto de ColdView
 */

public class GetXMLPDF implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbaA_ECold.GetXMLPDF";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CATALOG = "//CATALOG";
  static final String mcteParam_PAGENBR = "//PAGENBR";
  static final String mcteParam_ISSUEITEMNBR = "//ISSUEITEMNBR";
  static final String mcteParam_ACCESSWAY = "//ACCESSWAY";
  static final String mcteParam_MAXPAGES = "//MAXPAGES";
  static final String mcteParam_PAGECOUNTER = "//PAGECOUNTER";
  static final String mcteParam_PDFUSRPWD = "//PDFUSRPWD";
  static final String mcteParam_PDFMSTRPWD = "//PDFMSTRPWD";
  static final String mcteParam_PDFKEYLENGTH = "//PDFKEYLENGTH";
  static final String mcteParam_PDFNOPRINT = "//PDFNOPRINT";
  static final String mcteParam_PDFNOMODIFY = "//PDFNOMODIFY";
  static final String mcteParam_PDFNOCOPY = "//PDFNOCOPY";
  static final String mcteParam_PDFNOANNOTS = "//PDFNOANNOTS";
  /**
   *  Parametros Cold View
   */
  static final String mcteParam_CVISSUEITEMNBR = "//IssueItemNbr";
  static final String mcteParam_CVPAGENBR = "//PageNbr";
  static final String mcteParam_CVISSUEID = "//IssueID";
  static final String mcteParam_CVPDFUSRPWD = "//PDFUsrPWD";
  static final String mcteParam_CVPDFMSTRPWD = "//PDFMstrPWD";
  static final String mcteParam_CVPDFKEYLENGTH = "//PDFKeyLength";
  static final String mcteParam_CVPDFNOPRINT = "//PDFNoprint";
  static final String mcteParam_CVPDFNOMODIFY = "//PDFNomodify";
  static final String mcteParam_CVPDFNOCOPY = "//PDFNocopy";
  static final String mcteParam_CVPDFNOANNOTS = "//PDFNoannots";
  public CVCLFC.CVCL mobjCOLDview = new CVCLFC.CVCL();
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Busca en la base de Cold View la emision de la fecha pasada
   *  como parametro, abre la misma y obtiene el PDF correspondiente
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.Element wobjXMLElement = null;
    String wvarXMLOut = "";
    String wvarCatalog = "";
    String wvarIssueItemNbr = "";
    String wvarAccessWay = "";
    String wvarPageNbr = "";
    String wvarIssueId = "";
    int wvarMaxPages = 0;
    Variant wvarResponse = new Variant();
    String wvarPDFUsrPWD = "";
    String wvarPDFMstrPWD = "";
    String wvarPDFKeyLength = "";
    String wvarPDFNoPrint = "";
    String wvarPDFNoModify = "";
    String wvarPDFNoCopy = "";
    String wvarPDFNoAnnots = "";
    int wvarstep = 0;
    String wvarresult = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // CARGO LOS PARAMETROS DE ENTRADA
      wvarstep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCatalog = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CATALOG ) */ );
      wvarPageNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ );
      wvarIssueItemNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR ) */ );

      //Verifico que exista el nodo <ACCESSWAY>
      // On Error Resume Next (optionally ignored)
      wvarAccessWay = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ACCESSWAY ) */ );
      if( Err.getError().getNumber() != 0 )
      {
        wvarAccessWay = "";
      }
      //
      //wvarPDFUsrPWD = .selectSingleNode(mcteParam_PDFUSRPWD).Text
      //wvarPDFMstrPWD = .selectSingleNode(mcteParam_PDFMSTRPWD).Text
      //wvarPDFKeyLength = .selectSingleNode(mcteParam_PDFKEYLENGTH).Text
      //wvarPDFNoPrint = .selectSingleNode(mcteParam_PDFNOPRINT).Text
      //wvarPDFNoModify = .selectSingleNode(mcteParam_PDFNOMODIFY).Text
      //wvarPDFNoCopy = .selectSingleNode(mcteParam_PDFNOCOPY).Text
      //wvarPDFNoAnnots = .selectSingleNode(mcteParam_PDFNOANNOTS).Text
      //
      wvarstep = 20;
      //
      // AGREGO EL NODO KEEPCONNECTED PARA QUE NO SE DESCONECTE DESPUES DEL RETRIEVEISSUES
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "KEEPCONNECTED" );
      diamondedge.util.XmlDom.setText( wobjXMLElement, "TRUE" );
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( wobjXMLElement );
      //
      // AGREGO EL NODO ACCESSWAY
      wobjXMLElement = wobjXMLRequest.getDocument().createElement( "ACCESSWAY" );
      diamondedge.util.XmlDom.setText( wobjXMLElement, wvarAccessWay );
      /*unsup wobjXMLRequest.selectSingleNode( "//Request" ) */.appendChild( wobjXMLElement );
      //
      //MHC ------------------------------------------------------------------------
      // PIDO CONEXION A COLD VIEW
      wvarstep = 21;
      mobjCOLDview = new CVCLFC.CVCL();
      wvarstep = 22;
      if( ModGeneral.ColdViewConnect( wvarAccessWay, mobjCOLDview ) == Obj.toInt( "1" ) )
      {
        // HUBO UN ERROR EN LA CONEXION A COLD VIEW
        wvarstep = 23;
        Response.set( "<Response><Estado resultado=\"false\"/></Response>" );
        mobjCOLDview = (CVCLFC.CVCL) null;
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        return IAction_Execute;
      }
      //
      // EJECUTO RETRIEVE ISSUES
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbaA_ECold.RetrieveIssues")
      //Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      //Set wobjClass = Nothing
      ModGeneral.RetrieveIssues( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarResponse, mobjCOLDview );
      //FIN MHC --------------------------------------------------------------------
      //
      // ANALIZO LA RESPUESTA
      wvarstep = 25;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;

      wvarstep = 26;

      wobjXMLRequest.loadXML( wvarResponse.toString() );

      wvarstep = 27;

      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//Response/Estado/@resultado" ) */ ).equals( "false" ) )
      {
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( wobjXMLRequest.getDocument().getDocumentElement().toString() );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      else
      {
        if( !wvarCatalog.equals( "AIS - LBA - COMISIONES" ) )
        {
          if( wvarPageNbr.equals( "" ) )
          {
            wvarPageNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PAGENBR ) */ );
          }
          //
          // CARGO EL ISSUEITEMNUMBER SI VINO VACIO EN EL REQUEST
          if( wvarIssueItemNbr.equals( "" ) )
          {
            wvarIssueItemNbr = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ISSUEITEMNBR ) */ );
          }
        }
        else
        {
          //wvarPageNbr = 1
          //wvarIssueItemNbr = wobjXMLRequest.selectSingleNode("//ROWS/ROW[ISSUETITLE='" & wvarIssueItemNbr & "']/ISSUEITEMNBR").Text
          wvarIssueItemNbr = "1";
        }
        //
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL OPEN ISSUE
      wvarstep = 30;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 40;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteOpenIssue );
      //
      // CARGO EL NODO IssueItemNbr EN EL ARCHIVO
      wvarstep = 50;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEITEMNBR ) */, wvarIssueItemNbr );
      //
      // EJECUTO OPEN ISSUE DE COLD VIEW
      wvarstep = 60;
      mobjCOLDview.XML_Issue_Open( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        //
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\" cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      else
      {
        // LEVANTO EL ISSUEID
        wvarIssueId = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( mcteParam_CVISSUEID ) */ );
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL ISSUE PAGE GOTO
      wvarstep = 70;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 80;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteIssuePageGoto );
      //
      // CARGO EL NODO IssueID Y PageNbr EN EL ARCHIVO
      wvarstep = 90;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEID ) */, wvarIssueId );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPAGENBR ) */, wvarPageNbr );
      //
      // EJECUTO ISSUE PAGE GOTO DE COLD VIEW
      wvarstep = 100;
      mobjCOLDview.XML_Issue_Page_Goto( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );
      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        //
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\"  cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      // CARGO EL ARCHIVO XML PARA EL ISSUE PROCESS PDF
      wvarstep = 110;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      //
      wvarstep = 120;
      wobjXMLRequest.load( System.getProperty("user.dir") + ModGeneral.gcteIssueProcessPDF );
      //
      // CARGO EL NODO IssueID EN EL ARCHIVO
      wvarstep = 130;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVISSUEID ) */, wvarIssueId );
      //
      // CARGO LOS NODOS CORRESPONDIENTES AL ENCRIPTADO
      wvarstep = 135;
      //
      // SI EL KEY LENGTH ES 0 NO SE PUEDE CONFIGURAR USER PWD NI MASTER PWD
      if( wvarPDFKeyLength.equals( "0" ) )
      {
        wvarPDFUsrPWD = "";
        wvarPDFMstrPWD = "";
      }
      //
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFUSRPWD ) */, wvarPDFUsrPWD );
      //
      // SI EL KEY LENGTH ES <> 0 Y MASTER PWD ES "" USO LA DEL ARCHIVO
      if( (wvarPDFKeyLength.equals( "0" )) || (!wvarPDFMstrPWD.equals( "" )) )
      {
        diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFMSTRPWD ) */, wvarPDFMstrPWD );
      }
      //
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFKEYLENGTH ) */, wvarPDFKeyLength );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOPRINT ) */, wvarPDFNoPrint );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOMODIFY ) */, wvarPDFNoModify );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOCOPY ) */, wvarPDFNoCopy );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CVPDFNOANNOTS ) */, wvarPDFNoAnnots );
      //
      // EJECUTO ISSUE PAGE PROCESS PDF DE COLD VIEW
      wvarstep = 140;
      mobjCOLDview.XML_Issue_Page_ProcessPDF( wobjXMLRequest.getDocument().getDocumentElement().toString(), wvarXMLOut );

      //
      // VERIFICO LA RESPUESTA DE COLD VIEW
      wobjXMLResponse = new diamondedge.util.XmlDom();
      //unsup wobjXMLResponse.async = false;
      wobjXMLResponse.loadXML( wvarXMLOut );
      //
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Status" ) */ ).equals( "-1" ) )
      {
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        //
        // DESCONECTO EL OBJETO COLD VIEW
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO RECUPERAR EL PDF.\"  cv_message=\"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Event_Message" ) */ ) + "\" cv_errornbr= \"" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//Error_Nbr" ) */ ) + "\"/></Response>" );
        IAction_Execute = 0;
        /*unsup mobjCOM_Context.SetComplete() */;
        return IAction_Execute;
      }
      //
      // CARGO EL XSL PARA TRANSFORMAR LA RESPUESTA
      wvarstep = 150;
      wobjXSLResponse = new diamondedge.util.XmlDom();
      //
      //unsup wobjXSLResponse.async = false;
      wobjXSLResponse.loadXML( invoke( "p_GetPDFXSL", new Variant[] {} ) );

      wvarresult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
      wvarstep = 160;
      wobjXMLResponse = (diamondedge.util.XmlDom) null;
      wobjXSLResponse = (diamondedge.util.XmlDom) null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarstep = 170;
      Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarresult + "</Response>" );
      //
      // DESCONECTO EL OBJETO COLD VIEW
      ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
      //
      IAction_Execute = 0;
      /*unsup mobjCOM_Context.SetComplete() */;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLRequest = (diamondedge.util.XmlDom) null;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        ModGeneral.ColdViewDisconnect( mobjCOLDview/*warning: ByRef value change will be lost.*/ );
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarstep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + "[XML Cold View: " + wvarXMLOut + "]", vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   *  *****************************************************************
   *  Function : p_GetPDFXSL
   *  Abstract : Genera el XSL para la transformacion del XML con el PDF
   *  Synopsis : p_GetPDFXSL() As String
   *  *****************************************************************
   */
  private String p_GetPDFXSL() throws Exception
  {
    String p_GetPDFXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:dt='urn:schemas-microsoft-com:datatypes'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='/Response/SubSrvRet'> ";
    wvarStrXSL = wvarStrXSL + "      <xsl:apply-templates select='SubSrvRet'/>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='Results'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='PDFSTREAM'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='dt:dt'>bin.base64</xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "      <xsl:value-of select='PDFStream' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetPDFXSL = wvarStrXSL;
    return p_GetPDFXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
