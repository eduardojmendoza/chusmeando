XML_RetrieveIssuesWithSearch -

El componente java no posee esta funcionalidad. Se puede replicar utilizando los métodos ColdViewApi.struc.getIssueInfos y luego ColdViewApi.struc.getIndexResults (ver Ejemplo07.java)

XML_RetrieveIssues - ColdViewApi.struc.getIssuesInfos (ver Ejemplo04.java)

XML_Issue_Open - ColdViewApi.struc.getIssue (ver Ejemplo05.java)

XML_Issue_Page_PDFSendMail - El componente java no posee esta funcionalidad.

XML_Issue_Page_Goto - ColdViewApi.struc.gotoPage

XML_Issue_Page_ProcessPDF - ColdViewApi.struc.getPDFBase64 (ver Ejemplo05.java)

issue #145



<Request>
    <CATALOG>LBA - AIS COBRANZAS</CATALOG>
    <PAGENBR/>
    <ISSUEITEMNBR>1</ISSUEITEMNBR>
    <DATEISSUE>2015-12-02</DATEISSUE>
    <IDX_BUSQUEDA>751976807</IDX_BUSQUEDA>
    <IssueDateFrom>2015-12-02</IssueDateFrom>
    <IssueDateTo>2015-12-02</IssueDateTo>
    <QueryClause><![CDATA[(factura = 751976807)]]></QueryClause>
    <DocKey>LBA_CHOV</DocKey>
    <PDFNoprint>0</PDFNoprint>
    <PDFNomodify>-1</PDFNomodify>
    <PDFNocopy>0</PDFNocopy>
    <PDFNoannots>-1</PDFNoannots>
    <ACCESSWAY>OV</ACCESSWAY>
    <PDFUSRPWD/>
</Request>

Esto el HDP de VB lo convierte así:
como es el de cobranzas, levanta el archivo RetIssues_AISCOB.xml

<Request>
	<Cat_LongName>LBA - AIS COBRANZAS</Cat_LongName>
	<TaskName><![CDATA[<Todos>]]></TaskName>
	<DocKey>LBA_CHEQ</DocKey>
	<IssueRetrieveMode>R</IssueRetrieveMode>
	<IssueDateFrom></IssueDateFrom>
	<IssueDateTo></IssueDateTo>
	<QueryClause>
		<![CDATA[]]>
	</QueryClause>
	<SearchLimiter>0</SearchLimiter>
	<ResultsLimit>1</ResultsLimit>
</Request>

que es un default, y después le va pisando ( si vinieron ):
dockey
IssueDateFrom
IssueDateTo
QueryClause


entonces para este caso quedaría:

<Request>
	<Cat_LongName>LBA - AIS COBRANZAS</Cat_LongName>
	<TaskName><![CDATA[<Todos>]]></TaskName>
	<DocKey>LBA_CHOV</DocKey>
	<IssueRetrieveMode>R</IssueRetrieveMode>
	<IssueDateFrom>2015-12-02</IssueDateFrom>
	<IssueDateTo>2015-12-02</IssueDateTo>
	<QueryClause>
		<![CDATA[(factura = 751976807)]]>
	</QueryClause>
	<SearchLimiter>0</SearchLimiter>
	<ResultsLimit>1</ResultsLimit>
</Request>
