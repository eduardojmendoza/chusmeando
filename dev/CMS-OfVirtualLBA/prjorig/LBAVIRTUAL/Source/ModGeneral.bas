Attribute VB_Name = "ModGeneral"
Option Explicit

Public Const gcteDB As String = "lbawA_OfVirtualLBA.udl"

' Parametros XML del Cotizador
Public Const gcteParamFileName      As String = "ParametrosCotizador.xml"
Public Const gcteNodosHogar         As String = "//HOGAR"
Public Const gcteNodosAutoScoring   As String = "//AUTOSCORING"
Public Const gcteRAMOPCOD           As String = "/RAMOPCOD"
Public Const gctePLANNCOD           As String = "/PLANNCOD"
Public Const gctePOLIZSEC           As String = "/POLIZSEC"
Public Const gctePOLIZANN           As String = "/POLIZANN"
Public Const gcteBANCOCOD           As String = "/BANCOCOD"
Public Const gcteSUCURCOD           As String = "/SUCURCOD"


