VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetCotizItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OfVirtualLBA.lbaw_GetCotizItem"
Const mcteStoreProc         As String = "SPSNCV_PROD_COTI_SELECT_ITEM"
Const mcteStoreProcHijos    As String = "SPSNCV_PROD_COTI_HIJOS_SELECT"
Const mcteStoreProcAcc      As String = "SPSNCV_PROD_COTI_ACC_SELECT"
Const mcteStoreProcAseg     As String = "SPSNCV_PROD_COTI_ASEGADIC_SELECT"

'Parametros XML de Entrada
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_SUPLENUM    As String = "//SUPLENUM"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument

    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wrstDBResultHijos   As ADODB.Recordset
    Dim wrstDBResultAcc     As ADODB.Recordset
    Dim wrstDBResultAseg    As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarRAMOPCOD        As String
    Dim wvarPOLIZANN        As String
    Dim wvarPOLIZSEC        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    Dim wvarSUPLENUM        As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarPOLIZANN = .selectSingleNode(mcteParam_POLIZANN).Text
        wvarPOLIZSEC = .selectSingleNode(mcteParam_POLIZSEC).Text
        wvarCERTIPOL = .selectSingleNode(mcteParam_CERTIPOL).Text
        wvarCERTIANN = .selectSingleNode(mcteParam_CERTIANN).Text
        wvarCERTISEC = .selectSingleNode(mcteParam_CERTISEC).Text
        wvarSUPLENUM = .selectSingleNode(mcteParam_SUPLENUM).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '''''''''''''''''''''''''''
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , wvarPOLIZANN)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , wvarPOLIZSEC)
    wobjDBParm.Precision = 6
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , wvarCERTIPOL)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , wvarCERTIANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , wvarCERTISEC)
    wobjDBParm.Precision = 6
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , wvarSUPLENUM)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 130
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 140
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 150
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 160
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 170
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 180
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 190
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        'A partir de aca Hijos
        '
        wvarStep = 200
        Set wobjDBCmd = CreateObject("ADODB.Command")
        '
        With wobjDBCmd
            Set .ActiveConnection = wobjDBCnn
            .CommandText = mcteStoreProcHijos
            .CommandType = adCmdStoredProc
        End With
        '
        wvarStep = 210
        Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 220
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , wvarPOLIZANN)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 230
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , wvarPOLIZSEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 240
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , wvarCERTIPOL)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 250
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , wvarCERTIANN)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 260
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , wvarCERTISEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 270
        Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , wvarSUPLENUM)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 280
        Set wrstDBResultHijos = wobjDBCmd.Execute
        Set wrstDBResultHijos.ActiveConnection = Nothing
        '
        wvarStep = 290
        If Not wrstDBResultHijos.EOF Then
            '
            wvarStep = 300
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 310
            wrstDBResultHijos.Save wobjXMLResponse, adPersistXML
            '
            wvarStep = 320
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSLHijos())
            '
            wvarStep = 330
            wvarResult = wvarResult & "<HIJOS>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</HIJOS>"
            '
            wvarStep = 340
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        Else
            wvarStep = 350
            wvarResult = wvarResult & "<HIJOS></HIJOS>"
        End If
        '
        'Hasta aca hijos
        '
        'Desde aca accesorios
        '
        wvarStep = 360
        Set wobjDBCmd = CreateObject("ADODB.Command")
        '
        With wobjDBCmd
            Set .ActiveConnection = wobjDBCnn
            .CommandText = mcteStoreProcAcc
            .CommandType = adCmdStoredProc
        End With
        '
        wvarStep = 370
        Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 380
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , wvarPOLIZANN)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 390
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , wvarPOLIZSEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 400
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , wvarCERTIPOL)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 410
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , wvarCERTIANN)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 420
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , wvarCERTISEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 430
        Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , wvarSUPLENUM)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 440
        Set wrstDBResultAcc = wobjDBCmd.Execute
        Set wrstDBResultAcc.ActiveConnection = Nothing
        '
        wvarStep = 450
        If Not wrstDBResultAcc.EOF Then
            '
            wvarStep = 460
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 470
            wrstDBResultAcc.Save wobjXMLResponse, adPersistXML
            '
            wvarStep = 480
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSLAcc())
            '
            wvarStep = 490
            wvarResult = wvarResult & "<ACCESORIOS>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</ACCESORIOS>"
            '
            wvarStep = 500
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        Else
            wvarStep = 510
            wvarResult = wvarResult & "<ACCESORIOS></ACCESORIOS>"
        End If
        '
        'Hasta aca accesorios
        '
        'Desde aca Asegurados Adicionales
        '
        wvarStep = 520
        Set wobjDBCmd = CreateObject("ADODB.Command")
        '
        With wobjDBCmd
            Set .ActiveConnection = wobjDBCnn
            .CommandText = mcteStoreProcAseg
            .CommandType = adCmdStoredProc
        End With
        '
        wvarStep = 530
        Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 540
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , wvarPOLIZANN)
        wobjDBParm.Precision = 2
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 550
        Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , wvarPOLIZSEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 560
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , wvarCERTIPOL)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 570
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , wvarCERTIANN)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 580
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , wvarCERTISEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        
        wvarStep = 590
        Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , wvarSUPLENUM)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 600
        Set wrstDBResultAseg = wobjDBCmd.Execute
        Set wrstDBResultAseg.ActiveConnection = Nothing
        '
        wvarStep = 610
        If Not wrstDBResultAseg.EOF Then
            '
            wvarStep = 620
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 630
            wrstDBResultAseg.Save wobjXMLResponse, adPersistXML
            '
            wvarStep = 640
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSLAseg())
            '
            wvarStep = 650
            wvarResult = wvarResult & "<ASEGURADOS>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</ASEGURADOS>"
            '
            wvarStep = 660
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
        Else
            wvarStep = 670
            wvarResult = wvarResult & "<ASEGURADOS></ASEGURADOS>"
        End If
        '
        'Hasta aca Asegurados Adicionales
        '
        wvarStep = 680
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 690
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 700
    Set wobjDBCmd = Nothing
    '
    wvarStep = 710
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 720
    Set wobjDBCnn = Nothing
    '
    wvarStep = 730
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 740
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 750
    Set wrstDBResult = Nothing
    '
    wvarStep = 780
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='COTIZACION'>"
    '
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='POLIZANN'><xsl:value-of select='@POLIZANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='POLIZSEC'><xsl:value-of select='@POLIZSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CERTIPOL'><xsl:value-of select='@CERTIPOL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CERTIANN'><xsl:value-of select='@CERTIANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CERTISEC'><xsl:value-of select='@CERTISEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUPLENUM'><xsl:value-of select='@SUPLENUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FRANQCOD'><xsl:value-of select='@FRANQCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PQTDES'><xsl:value-of select='@PQTDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='HIJOS1416'><xsl:value-of select='@HIJOS1416' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='HIJOS1729'><xsl:value-of select='@HIJOS1729' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ZONA'><xsl:value-of select='@ZONA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CODPROV'><xsl:value-of select='@CODPROV' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUMALBA'><xsl:value-of select='@SUMALBA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENIVA'><xsl:value-of select='@CLIENIVA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUMAASEG'><xsl:value-of select='@SUMAASEG' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLUB_LBA'><xsl:value-of select='@CLUB_LBA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CPAANO'><xsl:value-of select='@CPAANO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CTAKMS'><xsl:value-of select='@CTAKMS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESCERO'><xsl:value-of select='@ESCERO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIENEPLAN'><xsl:value-of select='@TIENEPLAN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COND_ADIC'><xsl:value-of select='@COND_ADIC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ASEG_ADIC'><xsl:value-of select='@ASEG_ADIC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FH_NAC'><xsl:value-of select='@FH_NAC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SEXO'><xsl:value-of select='@SEXO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTCIV'><xsl:value-of select='@ESTCIV' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SIFMVEHI_DES'><xsl:value-of select='@SIFMVEHI_DES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROFECOD'><xsl:value-of select='@PROFECOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ACCESORIOS'><xsl:value-of select='@ACCESORIOS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='REFERIDO'><xsl:value-of select='@REFERIDO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='MOTORNUM'><xsl:value-of select='@MOTORNUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CHASINUM'><xsl:value-of select='@CHASINUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PATENNUM'><xsl:value-of select='@PATENNUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMARCOD'><xsl:value-of select='@AUMARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMODCOD'><xsl:value-of select='@AUMODCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUSUBCOD'><xsl:value-of select='@AUSUBCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUADICOD'><xsl:value-of select='@AUADICOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMODORI'><xsl:value-of select='@AUMODORI' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUUSOCOD'><xsl:value-of select='@AUUSOCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVCOD'><xsl:value-of select='@AUVTVCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVDIA'><xsl:value-of select='@AUVTVDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVMES'><xsl:value-of select='@AUVTVMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVANN'><xsl:value-of select='@AUVTVANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='VEHCLRCOD'><xsl:value-of select='@VEHCLRCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUKLMNUM'><xsl:value-of select='@AUKLMNUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FABRICAN'><xsl:value-of select='@FABRICAN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FABRICMES'><xsl:value-of select='@FABRICMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='GUGARAGE'><xsl:value-of select='@GUGARAGE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='GUDOMICI'><xsl:value-of select='@GUDOMICI' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUCATCOD'><xsl:value-of select='@AUCATCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUTIPCOD'><xsl:value-of select='@AUTIPCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUCIASAN'><xsl:value-of select='@AUCIASAN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUANTANN'><xsl:value-of select='@AUANTANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUNUMSIN'><xsl:value-of select='@AUNUMSIN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUNUMKMT'><xsl:value-of select='@AUNUMKMT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUUSOGNC'><xsl:value-of select='@AUUSOGNC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMISIANN'><xsl:value-of select='@EMISIANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMISIMES'><xsl:value-of select='@EMISIMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMISIDIA'><xsl:value-of select='@EMISIDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SITUCPOL'><xsl:value-of select='@SITUCPOL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBROFOR'><xsl:value-of select='@COBROFOR' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EDADACTU'><xsl:value-of select='@EDADACTU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CAMP_CODIGO'><xsl:value-of select='@CAMP_CODIGO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='LEGAJO_GTE'><xsl:value-of select='@LEGAJO_GTE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NRO_PROD'><xsl:value-of select='@NRO_PROD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUCURSAL_CODIGO'><xsl:value-of select='@SUCURSAL_CODIGO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='LEGAJO_VEND'><xsl:value-of select='@LEGAJO_VEND' /></xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='RAMOPCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FRANQCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PQTDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENIVA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLUB_LBA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESCERO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TIENEPLAN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COND_ADIC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ASEG_ADIC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SEXO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESTCIV'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SIFMVEHI_DES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROFECOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ACCESORIOS'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MOTORNUM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CHASINUM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PATENNUM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUMODORI'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUVTVCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='GUGARAGE'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='GUDOMICI'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUCIASAN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUUSOGNC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='USUARCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SITUCPOL'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NUMEDOCU'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICSEC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUENTSEC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LEGAJO_GTE'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LEGAJO_VEND'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function


Private Function p_GetXSLHijos() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='HIJO'>"
    '
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CONDUAPE'><xsl:value-of select='@CONDUAPE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CONDUNOM'><xsl:value-of select='@CONDUNOM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CONDUFEC'><xsl:value-of select='@CONDUFEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CONDUSEX'><xsl:value-of select='@CONDUSEX' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CONDUEST'><xsl:value-of select='@CONDUEST' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CONDUEXC'><xsl:value-of select='@CONDUEXC' /></xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CONDUAPE'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CONDUNOM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CONDUSEX'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CONDUEST'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CONDUEXC'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLHijos = wvarStrXSL
End Function

Private Function p_GetXSLAcc() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ACCESORIO'>"
    '
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUACCCOD1'><xsl:value-of select='@AUACCCOD1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVEASUM1'><xsl:value-of select='@AUVEASUM1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVEADES1'><xsl:value-of select='@AUVEADES1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVEADEP1'><xsl:value-of select='@AUVEADEP1' /></xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUVEADES1'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUVEADEP1'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLAcc = wvarStrXSL
End Function

Private Function p_GetXSLAseg() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ASEGURADO'>"
    '
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOCUMDAT'><xsl:value-of select='@DOCUMDAT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NOMBREAS'><xsl:value-of select='@NOMBREAS' /></xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NOMBREAS'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLAseg = wvarStrXSL
End Function



'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub












