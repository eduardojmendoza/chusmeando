VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_PutCotizHogar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OfVirtualLBA.lbaw_PutCotizHogar"
Const mcteStoreProc             As String = "SPSNCV_ADM_INTERNET_HOGAR_INSERT"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_ESTADO_ID       As String = "//ESTADO_ID"
Const mcteParam_TELEFONO        As String = "//TELEFONO"
Const mcteParam_APELLIDO_NOMBRE As String = "//APELLIDO_NOMBRE"
Const mcteParam_LOCALIDAD       As String = "//LOCALIDAD"
Const mcteParam_POSTACOD        As String = "//POSTACOD"
Const mcteParam_PROVINCIAID     As String = "//PROVINCIAID"
Const mcteParam_PORTAL          As String = "//PORTAL"
Const mcteParam_TIVIVCOD        As String = "//TIVIVCOD"
Const mcteParam_COBERCOD1       As String = "//COBERCOD1"
Const mcteParam_NUMERMOD1       As String = "//NUMERMOD1"
Const mcteParam_COBERCOD2       As String = "//COBERCOD2"
Const mcteParam_NUMERMOD2       As String = "//NUMERMOD2"
Const mcteParam_COBERCOD3       As String = "//COBERCOD3"
Const mcteParam_NUMERMOD3       As String = "//NUMERMOD3"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
    Dim wvarESTADO_ID       As String
    Dim wvarTELEFONO        As String
    Dim wvarAPELLIDO_NOMBRE As String
    Dim wvarLOCALIDAD       As String
    Dim wvarPOSTACOD        As String
    Dim wvarPROVINCIAID     As String
    Dim wvarPORTAL          As String
    Dim wvarRAMOPCOD        As String
    Dim wvarPLANNCOD        As String
    Dim wvarTIVIVCOD        As String
    Dim wvarCOBERCOD1       As String
    Dim wvarNUMERMOD1       As String
    Dim wvarCOBERCOD2       As String
    Dim wvarNUMERMOD2       As String
    Dim wvarCOBERCOD3       As String
    Dim wvarNUMERMOD3       As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarESTADO_ID = .selectSingleNode(mcteParam_ESTADO_ID).Text
        wvarTELEFONO = .selectSingleNode(mcteParam_TELEFONO).Text
        wvarAPELLIDO_NOMBRE = .selectSingleNode(mcteParam_APELLIDO_NOMBRE).Text
        wvarLOCALIDAD = .selectSingleNode(mcteParam_LOCALIDAD).Text
        wvarPOSTACOD = .selectSingleNode(mcteParam_POSTACOD).Text
        wvarPROVINCIAID = .selectSingleNode(mcteParam_PROVINCIAID).Text
        wvarPORTAL = .selectSingleNode(mcteParam_PORTAL).Text
        wvarTIVIVCOD = .selectSingleNode(mcteParam_TIVIVCOD).Text
        wvarCOBERCOD1 = .selectSingleNode(mcteParam_COBERCOD1).Text
        wvarNUMERMOD1 = .selectSingleNode(mcteParam_NUMERMOD1).Text
        wvarCOBERCOD2 = .selectSingleNode(mcteParam_COBERCOD2).Text
        wvarNUMERMOD2 = .selectSingleNode(mcteParam_NUMERMOD2).Text
        wvarCOBERCOD3 = .selectSingleNode(mcteParam_COBERCOD3).Text
        wvarNUMERMOD3 = .selectSingleNode(mcteParam_NUMERMOD3).Text
    End With
    
    ' LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
    wvarStep = 24
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarRAMOPCOD = .selectSingleNode(gcteNodosHogar & gcteRAMOPCOD).Text
        wvarPLANNCOD = .selectSingleNode(gcteNodosHogar & gctePLANNCOD).Text
    End With
    '
    wvarStep = 28
    Set wobjXMLParametros = Nothing
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 60
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ESTADO_ID", adInteger, adParamInput, , wvarESTADO_ID)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@APELLIDO_NOMBRE", adVarChar, adParamInput, 50, wvarAPELLIDO_NOMBRE)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TELEFONO", adVarChar, adParamInput, 50, wvarTELEFONO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@LOCALIDAD", adVarChar, adParamInput, 50, wvarLOCALIDAD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@POSTACOD", adNumeric, adParamInput, , wvarPOSTACOD)
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PROVINCIAID", adNumeric, adParamInput, , wvarPROVINCIAID)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@PORTAL", adChar, adParamInput, 10, wvarPORTAL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@PLANNCOD", adNumeric, adParamInput, , wvarPLANNCOD)
    wobjDBParm.Precision = 3
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIVIVCOD", adNumeric, adParamInput, , wvarTIVIVCOD)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    Set wobjDBParm = wobjDBCmd.CreateParameter("@COBERCOD1", adNumeric, adParamInput, , wvarCOBERCOD1)
    wobjDBParm.Precision = 3
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMERMOD1", adNumeric, adParamInput, , wvarNUMERMOD1)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    Set wobjDBParm = wobjDBCmd.CreateParameter("@COBERCOD2", adNumeric, adParamInput, , wvarCOBERCOD2)
    wobjDBParm.Precision = 3
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMERMOD2", adNumeric, adParamInput, , wvarNUMERMOD2)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing


    Set wobjDBParm = wobjDBCmd.CreateParameter("@COBERCOD3", adNumeric, adParamInput, , wvarCOBERCOD3)
    wobjDBParm.Precision = 3
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMERMOD3", adNumeric, adParamInput, , wvarNUMERMOD3)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
  
    wvarStep = 140
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 150
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value > 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><COTIID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</COTIID></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "PARAMETROS INCORRECTOS"
            Case -2
                wvarMensaje = "ERROR AL INSERTAR"
            Case -3
                wvarMensaje = "ERROR AL ACTUALIZAR"
            Case -4
                wvarMensaje = "ERROR AL ELIMINAR"
        End Select
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 180
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub









