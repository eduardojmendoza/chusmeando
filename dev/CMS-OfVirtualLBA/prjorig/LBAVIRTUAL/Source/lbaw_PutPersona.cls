VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_PutPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OfVirtualLBA.lbaw_PutPersona"
Const mcteStoreProc             As String = "SPSNCV_PROD_SIFMPERS_INSERT"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_USUARCOD    As String = "//USUARCOD"
Const mcteParam_NUMEDOCU    As String = "//NUMEDOCU"
Const mcteParam_TIPODOCU    As String = "//TIPODOCU"
Const mcteParam_CLIENAP1    As String = "//CLIENAP1"
Const mcteParam_CLIENAP2    As String = "//CLIENAP2"
Const mcteParam_CLIENNOM    As String = "//CLIENNOM"
Const mcteParam_NACIMANN    As String = "//NACIMANN"
Const mcteParam_NACIMMES    As String = "//NACIMMES"
Const mcteParam_NACIMDIA    As String = "//NACIMDIA"
Const mcteParam_CLIENSEX    As String = "//CLIENSEX"
Const mcteParam_CLIENEST    As String = "//CLIENEST"
Const mcteParam_PAISSCOD    As String = "//PAISSCOD"
Const mcteParam_IDIOMCOD    As String = "//IDIOMCOD"
Const mcteParam_NUMHIJOS    As String = "//NUMHIJOS"
Const mcteParam_EFECTANN    As String = "//EFECTANN"
Const mcteParam_EFECTMES    As String = "//EFECTMES"
Const mcteParam_EFECTDIA    As String = "//EFECTDIA"
Const mcteParam_CLIENTIP    As String = "//CLIENTIP"
Const mcteParam_CLIENFUM    As String = "//CLIENFUM"
Const mcteParam_CLIENDOZ    As String = "//CLIENDOZ"
Const mcteParam_ABRIDTIP    As String = "//ABRIDTIP"
Const mcteParam_ABRIDNUM    As String = "//ABRIDNUM"
Const mcteParam_CLIENORG    As String = "//CLIENORG"
Const mcteParam_PERSOTIP    As String = "//PERSOTIP"
Const mcteParam_DOMICSEC    As String = "//DOMICSEC"
Const mcteParam_CLIENCLA    As String = "//CLIENCLA"
Const mcteParam_FALLEANN    As String = "//FALLEANN"
Const mcteParam_FALLEMES    As String = "//FALLEMES"
Const mcteParam_FALLEDIA    As String = "//FALLEDIA"
Const mcteParam_FBAJAANN    As String = "//FBAJAANN"
Const mcteParam_FBAJAMES    As String = "//FBAJAMES"
Const mcteParam_FBAJADIA    As String = "//FBAJADIA"
Const mcteParam_EMAIL    As String = "//EMAIL"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
     Dim wvarUSUARCOD    As String
     Dim wvarNUMEDOCU    As String
     Dim wvarTIPODOCU    As String
     Dim wvarCLIENAP1    As String
     Dim wvarCLIENAP2    As String
     Dim wvarCLIENNOM    As String
     Dim wvarNACIMANN    As String
     Dim wvarNACIMMES    As String
     Dim wvarNACIMDIA    As String
     Dim wvarCLIENSEX    As String
     Dim wvarCLIENEST    As String
     Dim wvarPAISSCOD    As String
     Dim wvarIDIOMCOD    As String
     Dim wvarNUMHIJOS    As String
     Dim wvarEFECTANN    As String
     Dim wvarEFECTMES    As String
     Dim wvarEFECTDIA    As String
     Dim wvarCLIENTIP    As String
     Dim wvarCLIENFUM    As String
     Dim wvarCLIENDOZ    As String
     Dim wvarABRIDTIP    As String
     Dim wvarABRIDNUM    As String
     Dim wvarCLIENORG    As String
     Dim wvarPERSOTIP    As String
     Dim wvarDOMICSEC    As String
     Dim wvarCLIENCLA    As String
     Dim wvarFALLEANN    As String
     Dim wvarFALLEMES    As String
     Dim wvarFALLEDIA    As String
     Dim wvarFBAJAANN    As String
     Dim wvarFBAJAMES    As String
     Dim wvarFBAJADIA    As String
     Dim wvarEMAIL       As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarUSUARCOD = .selectSingleNode(mcteParam_USUARCOD).Text
        wvarNUMEDOCU = .selectSingleNode(mcteParam_NUMEDOCU).Text
        wvarTIPODOCU = .selectSingleNode(mcteParam_TIPODOCU).Text
        wvarCLIENAP1 = .selectSingleNode(mcteParam_CLIENAP1).Text
        wvarCLIENAP2 = .selectSingleNode(mcteParam_CLIENAP2).Text
        wvarCLIENNOM = .selectSingleNode(mcteParam_CLIENNOM).Text
        wvarNACIMANN = .selectSingleNode(mcteParam_NACIMANN).Text
        wvarNACIMMES = .selectSingleNode(mcteParam_NACIMMES).Text
        wvarNACIMDIA = .selectSingleNode(mcteParam_NACIMDIA).Text
        wvarCLIENSEX = .selectSingleNode(mcteParam_CLIENSEX).Text
        wvarCLIENEST = .selectSingleNode(mcteParam_CLIENEST).Text
        wvarPAISSCOD = .selectSingleNode(mcteParam_PAISSCOD).Text
        wvarIDIOMCOD = .selectSingleNode(mcteParam_IDIOMCOD).Text
        wvarNUMHIJOS = .selectSingleNode(mcteParam_NUMHIJOS).Text
        wvarEFECTANN = .selectSingleNode(mcteParam_EFECTANN).Text
        wvarEFECTMES = .selectSingleNode(mcteParam_EFECTMES).Text
        wvarEFECTDIA = .selectSingleNode(mcteParam_EFECTDIA).Text
        wvarCLIENTIP = .selectSingleNode(mcteParam_CLIENTIP).Text
        wvarCLIENFUM = .selectSingleNode(mcteParam_CLIENFUM).Text
        wvarCLIENDOZ = .selectSingleNode(mcteParam_CLIENDOZ).Text
        wvarABRIDTIP = .selectSingleNode(mcteParam_ABRIDTIP).Text
        wvarABRIDNUM = .selectSingleNode(mcteParam_ABRIDNUM).Text
        wvarCLIENORG = .selectSingleNode(mcteParam_CLIENORG).Text
        wvarPERSOTIP = .selectSingleNode(mcteParam_PERSOTIP).Text
        wvarDOMICSEC = .selectSingleNode(mcteParam_DOMICSEC).Text
        wvarCLIENCLA = .selectSingleNode(mcteParam_CLIENCLA).Text
        wvarFALLEANN = .selectSingleNode(mcteParam_FALLEANN).Text
        wvarFALLEMES = .selectSingleNode(mcteParam_FALLEMES).Text
        wvarFALLEDIA = .selectSingleNode(mcteParam_FALLEDIA).Text
        wvarFBAJAANN = .selectSingleNode(mcteParam_FBAJAANN).Text
        wvarFBAJAMES = .selectSingleNode(mcteParam_FBAJAMES).Text
        wvarFBAJADIA = .selectSingleNode(mcteParam_FBAJADIA).Text
        wvarEMAIL = .selectSingleNode(mcteParam_EMAIL).Text
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 60
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adChar, adParamInput, 10, wvarUSUARCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMEDOCU", adChar, adParamInput, 11, wvarNUMEDOCU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPODOCU", adNumeric, adParamInput, , wvarTIPODOCU)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP1", adChar, adParamInput, 20, wvarCLIENAP1)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP2", adChar, adParamInput, 20, wvarCLIENAP2)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENNOM", adChar, adParamInput, 20, wvarCLIENNOM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMANN", adNumeric, adParamInput, , wvarNACIMANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMMES", adNumeric, adParamInput, , wvarNACIMMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMDIA", adNumeric, adParamInput, , wvarNACIMDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEX", adChar, adParamInput, 1, wvarCLIENSEX)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 180
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENEST", adChar, adParamInput, 1, wvarCLIENEST)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 190
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PAISSCOD", adChar, adParamInput, 2, wvarPAISSCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 200
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDIOMCOD", adChar, adParamInput, 3, wvarIDIOMCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 210
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMHIJOS", adNumeric, adParamInput, , wvarNUMHIJOS)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 220
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTANN", adNumeric, adParamInput, , wvarEFECTANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 230
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTMES", adNumeric, adParamInput, , wvarEFECTMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 240
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTDIA", adNumeric, adParamInput, , wvarEFECTDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 250
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, wvarCLIENTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 260
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENFUM", adChar, adParamInput, 1, wvarCLIENFUM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 270
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENDOZ", adChar, adParamInput, 1, wvarCLIENDOZ)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 280
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ABRIDTIP", adChar, adParamInput, 2, wvarABRIDTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 290
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ABRIDNUM", adChar, adParamInput, 10, wvarABRIDNUM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 300
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENORG", adChar, adParamInput, 3, wvarCLIENORG)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 310
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PERSOTIP", adChar, adParamInput, 1, wvarPERSOTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 320
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICSEC", adChar, adParamInput, 3, wvarDOMICSEC)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 330
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENCLA", adChar, adParamInput, 9, wvarCLIENCLA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 340
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEANN", adNumeric, adParamInput, , wvarFALLEANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 350
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEMES", adNumeric, adParamInput, , wvarFALLEMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 360
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEDIA", adNumeric, adParamInput, , wvarFALLEDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 370
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJAANN", adNumeric, adParamInput, , wvarFBAJAANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 380
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJAMES", adNumeric, adParamInput, , wvarFBAJAMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 390
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJADIA", adNumeric, adParamInput, , wvarFBAJADIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 395
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAIL", adChar, adParamInput, 60, wvarEMAIL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 400
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 410
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value > 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><PERSONAID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</PERSONAID></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "ERROR AL INSERTAR"
        End Select
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 420
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub









