VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_DelSolicProd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OfVirtualLBA.lbaw_DelSolicProd"
Const mcteStoreProc             As String = "SPSNCV_PROD_SOLI_DELETE"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_RAMOPCOD   As String = "//RAMOPCOD"
Const mcteParam_POLIZANN   As String = "//POLIZANN"
Const mcteParam_POLIZSEC   As String = "//POLIZSEC"
Const mcteParam_CERTIPOL   As String = "//CERTIPOL"
Const mcteParam_CERTIANN   As String = "//CERTIANN"
Const mcteParam_CERTISEC   As String = "//CERTISEC"
Const mcteParam_SUPLENUM   As String = "//SUPLENUM"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
    Dim wvarRAMOPCOD      As String
    Dim wvarPOLIZANN      As String
    Dim wvarPOLIZSEC      As String
    Dim wvarCERTIPOL      As String
    Dim wvarCERTIANN      As String
    Dim wvarCERTISEC      As String
    Dim wvarSUPLENUM      As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarPOLIZANN = .selectSingleNode(mcteParam_POLIZANN).Text
        wvarPOLIZSEC = .selectSingleNode(mcteParam_POLIZSEC).Text
        wvarCERTIPOL = .selectSingleNode(mcteParam_CERTIPOL).Text
        wvarCERTIANN = .selectSingleNode(mcteParam_CERTIANN).Text
        wvarCERTISEC = .selectSingleNode(mcteParam_CERTISEC).Text
        wvarSUPLENUM = .selectSingleNode(mcteParam_SUPLENUM).Text
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 60
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZANN", adNumeric, adParamInput, , wvarPOLIZANN)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@POLIZSEC", adNumeric, adParamInput, , wvarPOLIZSEC)
    wobjDBParm.Precision = 6
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , wvarCERTIPOL)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , wvarCERTIANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , wvarCERTISEC)
    wobjDBParm.Precision = 6
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SUPLENUM", adNumeric, adParamInput, , wvarSUPLENUM)
    wobjDBParm.Precision = 6
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 400
    wobjDBCmd.Execute adExecuteNoRecords

    wvarStep = 410
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><PERSONAID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</PERSONAID></Response>"
    Else
        Select Case wobjDBCmd.Parameters("@RETURN_VALUE").Value
            Case -1
                wvarMensaje = "error al eliminar SIFSPOLI"
            Case -2
                wvarMensaje = "error al eliminar SEFMDECA"
            Case -3
                wvarMensaje = "error al eliminar SEFMDERI_ASEGADIC"
            Case -4
                wvarMensaje = "error al eliminar SIFSMOVI"
            Case -5
                wvarMensaje = "error al eliminar SIFSFIMA_TARI"
            Case -6
                wvarMensaje = "error al eliminar SIFSFIMA_VEHI"
            Case -7
                wvarMensaje = "error al eliminar SIFSPECO"
            Case -8
                wvarMensaje = "error al eliminar SIFSDERI_CAMP"
            Case -9
                wvarMensaje = "error al eliminar SIFSDERI_GTE"
            Case -10
                wvarMensaje = "error al eliminar SIFSDERI_PROD"
            Case -11
                wvarMensaje = "error al eliminar SIFSDERI_SUCU"
            Case -12
                wvarMensaje = "error al eliminar SIFSDERI_VEND"
            Case -13
                wvarMensaje = "error al eliminar SIFMVECO"
            Case -14
                wvarMensaje = "error al eliminar SIFMVEAC"
            Case -15
                wvarMensaje = "error al eliminar SIFWINSW"
        End Select
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 420
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub















