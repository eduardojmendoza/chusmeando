VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetDetalleCotiz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OfVirtualLBA.lbaw_GetDetalleCotiz"
Const mcteStoreProc         As String = "SPSNCV_ADM_INTERNET_COTIID_SELECT"

'Parametros XML de Entrada
Const mcteParam_CotiID      As String = "//COTIID"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCotiID          As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCotiID = .selectSingleNode(mcteParam_CotiID).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    
    Set wobjDBParm = wobjDBCmd.CreateParameter("@COTIID", adNumeric, adParamInput, 0, wvarCotiID)
    wobjDBParm.Precision = 10
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 50
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 60
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 70
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 80
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 90
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 100
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 120
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 130
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 140
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    wvarStep = 150
    Set wobjDBCmd = Nothing
    '
    wvarStep = 160
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 170
    Set wobjDBCnn = Nothing
    '
    wvarStep = 180
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 190
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 200
    Set wrstDBResult = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ROW'>"
    '
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='COTIID'><xsl:value-of select='@COTIID' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FECHACOTI'><xsl:value-of select='@FECHACOTI' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMDIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMMES'><xsl:value-of select='@NACIMMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMANN'><xsl:value-of select='@NACIMANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADO_DESC'><xsl:value-of select='@ESTADO_DESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='APELLIDO_NOMBRE'><xsl:value-of select='@APELLIDO_NOMBRE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TELEFONO'><xsl:value-of select='@TELEFONO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADO_CIVIL_DESC'><xsl:value-of select='@ESTADO_CIVIL_DESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='LOCALIDAD'><xsl:value-of select='@LOCALIDAD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='POSTACOD'><xsl:value-of select='@POSTACOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SEXO'><xsl:value-of select='@SEXO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVIDES'><xsl:value-of select='@PROVIDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVINCIAID'><xsl:value-of select='@PROVINCIAID' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PORTAL'><xsl:value-of select='@PORTAL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='MARCADES'><xsl:value-of select='@MARCADES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='VEHICULO'><xsl:value-of select='@VEHICULO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='FABRICAN'><xsl:value-of select='@FABRICAN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUKLMNUM'><xsl:value-of select='@AUKLMNUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUNUMSIN'><xsl:value-of select='@AUNUMSIN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESCERO'><xsl:value-of select='@ESCERO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUMAASEG'><xsl:value-of select='@SUMAASEG' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CBITVRTA'><xsl:value-of select='@CBITVRTA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ITVLSDIA'><xsl:value-of select='@ITVLSDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='ITVLSMES'><xsl:value-of select='@ITVLSMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CBITVANN'><xsl:value-of select='@CBITVANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='HIJOS1729'><xsl:value-of select='@HIJOS1729' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='INCLUYEHIJOS'><xsl:value-of select='@INCLUYEHIJOS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMARCOD'><xsl:value-of select='@AUMARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMODCOD'><xsl:value-of select='@AUMODCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUSUBCOD'><xsl:value-of select='@AUSUBCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUADICOD'><xsl:value-of select='@AUADICOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMODORI'><xsl:value-of select='@AUMODORI' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='VEHCLRCOD'><xsl:value-of select='@VEHCLRCOD' /></xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COTIID'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FECHACOTI'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NACIMDIA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NACIMMES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NACIMANN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESTADO_DESC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='APELLIDO_NOMBRE'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TELEFONO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESTADO_CIVIL_DESC'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LOCALIDAD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='POSTACOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SEXO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROVIDES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROVINCIAID'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PORTAL'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='RAMOPCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MARCADES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='VEHICULO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FABRICAN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUKLMNUM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUNUMSIN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESCERO'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SUMAASEG'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CBITVRTA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ITVLSDIA'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ITVLSMES'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CBITVANN'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='HIJOS1729'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INCLUYEHIJOS'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUMARCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUMODCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUSUBCOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUADICOD'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUMODORI'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='VEHCLRCOD'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub










