package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCotizaciones implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_GetCotizaciones";
  static final String mcteStoreProc = "SPSNCV_ADM_INTERNET_COTI_SELECT";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_EstadoOper = "//ESTADOOPER";
  static final String mcteParam_Producto = "//PRODUCTO";
  static final String mcteParam_Portal = "//PORTAL";
  static final String mcteParam_FDesde = "//FDESDE";
  static final String mcteParam_FHasta = "//FHASTA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarEstadoOper = "";
    String wvarProducto = "";
    String wvarPortal = "";
    String wvarFDesde = "";
    String wvarFHasta = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarEstadoOper = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EstadoOper ) */ );
      wvarProducto = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Producto ) */ );
      wvarPortal = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Portal ) */ );
      wvarFDesde = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FDesde ) */ );
      wvarFHasta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FHasta ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wobjDBParm = new Parameter( "@ESTADOOPER", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarEstadoOper ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@PRODUCTO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarProducto ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@PORTAL", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarPortal ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@FDESDE", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( wvarFDesde ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@FHASTA", AdoConst.adVarChar, AdoConst.adParamInput, 10, new Variant( wvarFHasta ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 60;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 70;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 80;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 90;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 100;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 120;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wvarStep = 130;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 140;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      wvarStep = 150;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 160;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 170;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 180;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 190;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 200;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ROW'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='COTIID'><xsl:value-of select='@COTIID' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FECHACOTI'><xsl:value-of select='@FECHACOTI' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='MARCADES'><xsl:value-of select='@MARCADES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='VEHICULO'><xsl:value-of select='@VEHICULO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='FABRICAN'><xsl:value-of select='@FABRICAN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMDIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMMES'><xsl:value-of select='@NACIMMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NACIMANN'><xsl:value-of select='@NACIMANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADO_DESC'><xsl:value-of select='@ESTADO_DESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUKLMNUM'><xsl:value-of select='@AUKLMNUM' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='AUNUMSIN'><xsl:value-of select='@AUNUMSIN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='APELLIDO_NOMBRE'><xsl:value-of select='@APELLIDO_NOMBRE' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TELEFONO'><xsl:value-of select='@TELEFONO' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='ESTADO_CIVIL_DESC'><xsl:value-of select='@ESTADO_CIVIL_DESC' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='LOCALIDAD'><xsl:value-of select='@LOCALIDAD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='POSTACOD'><xsl:value-of select='@POSTACOD' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='COTIID'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FECHACOTI'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='MARCADES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='VEHICULO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='FABRICAN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NACIMDIA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NACIMMES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='NACIMANN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ESTADO_DESC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUKLMNUM'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='AUNUMSIN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='APELLIDO_NOMBRE'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TELEFONO'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='ESTADO_CIVIL_DESC'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='LOCALIDAD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='POSTACOD'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
