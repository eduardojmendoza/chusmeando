package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutSolHogar implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_PutSolHogar";
  static final String mcteStoreProc = "SPSNCV_HOGAR_INSERT_SOLICITUD";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_WDB_ALTURNUM = "//WDB_ALTURNUM";
  static final String mcteParam_WDB_CALLESEL = "//WDB_CALLESEL";
  static final String mcteParam_WDB_CLIENAP1 = "//WDB_CLIENAP1";
  static final String mcteParam_WDB_CLIENAP2 = "//WDB_CLIENAP2";
  static final String mcteParam_WDB_CLIENNOM = "//WDB_CLIENNOM";
  static final String mcteParam_WDB_CO_TITULAR = "//WDB_CO_TITULAR";
  static final String mcteParam_WDB_CODBANCO = "//WDB_CODBANCO";
  static final String mcteParam_WDB_CODI_ZONA = "//WDB_CODI_ZONA";
  static final String mcteParam_WDB_CODIGO_COBRO = "//WDB_CODIGO_COBRO";
  static final String mcteParam_WDB_CODINST = "//WDB_CODINST";
  static final String mcteParam_WDB_CODPOST_RGO = "//WDB_CODPOST_RGO";
  static final String mcteParam_WDB_CODTRANS = "//WDB_CODTRANS";
  static final String mcteParam_WDB_DOCUMDAT = "//WDB_DOCUMDAT";
  static final String mcteParam_WDB_DOCUMTIP = "//WDB_DOCUMTIP";
  static final String mcteParam_WDB_DOMICCPOCO = "//WDB_DOMICCPOCO";
  static final String mcteParam_WDB_DOMICDNU = "//WDB_DOMICDNU";
  static final String mcteParam_WDB_DOMICDNUCO = "//WDB_DOMICDNUCO";
  static final String mcteParam_WDB_DOMICDOMCOM = "//WDB_DOMICDOMCOM";
  static final String mcteParam_WDB_DOMICPIS = "//WDB_DOMICPIS";
  static final String mcteParam_WDB_DOMICPISCO = "//WDB_DOMICPISCO";
  static final String mcteParam_WDB_DOMICPOBCO = "//WDB_DOMICPOBCO";
  static final String mcteParam_WDB_DOMICPRECO = "//WDB_DOMICPRECO";
  static final String mcteParam_WDB_DOMICPTA = "//WDB_DOMICPTA";
  static final String mcteParam_WDB_DOMICPTACO = "//WDB_DOMICPTACO";
  static final String mcteParam_WDB_DOMICTLFCO = "//WDB_DOMICTLFCO";
  static final String mcteParam_WDB_EDADCLI = "//WDB_EDADCLI";
  static final String mcteParam_WDB_ESTADO = "//WDB_ESTADO";
  static final String mcteParam_WDB_ESTCIVCLI = "//WDB_ESTCIVCLI";
  static final String mcteParam_WDB_FECCIECOT_ANO = "//WDB_FECCIECOT_ANO";
  static final String mcteParam_WDB_FECCIECOT_DIA = "//WDB_FECCIECOT_DIA";
  static final String mcteParam_WDB_FECCIECOT_MES = "//WDB_FECCIECOT_MES";
  static final String mcteParam_WDB_FECHAVTOCTA_ANO = "//WDB_FECHAVTOCTA_ANO";
  static final String mcteParam_WDB_FECHAVTOCTA_DIA = "//WDB_FECHAVTOCTA_DIA";
  static final String mcteParam_WDB_FECHAVTOCTA_MES = "//WDB_FECHAVTOCTA_MES";
  static final String mcteParam_WDB_FECNTO_AAAA = "//WDB_FECNTO_AAAA";
  static final String mcteParam_WDB_FECNTO_DD = "//WDB_FECNTO_DD";
  static final String mcteParam_WDB_FECNTO_MM = "//WDB_FECNTO_MM";
  static final String mcteParam_WDB_IVASITCLI = "//WDB_IVASITCLI";
  static final String mcteParam_WDB_LEGAJO = "//WDB_LEGAJO";
  static final String mcteParam_WDB_LOCALIDAD = "//WDB_LOCALIDAD";
  static final String mcteParam_WDB_NROCTA = "//WDB_NROCTA";
  static final String mcteParam_WDB_PRECIO_MENSUAL = "//WDB_PRECIO_MENSUAL";
  static final String mcteParam_WDB_PREFIJO = "//WDB_PREFIJO";
  static final String mcteParam_WDB_PRODUCTO = "//WDB_PRODUCTO";
  static final String mcteParam_WDB_PROVICODCO = "//WDB_PROVICODCO";
  static final String mcteParam_WDB_SEXOCLI = "//WDB_SEXOCLI";
  static final String mcteParam_WDB_SUCCTA = "//WDB_SUCCTA";
  static final String mcteParam_WDB_SWASCENS = "//WDB_SWASCENS";
  static final String mcteParam_WDB_SWCALDER = "//WDB_SWCALDER";
  static final String mcteParam_WDB_SWCREJAS = "//WDB_SWCREJAS";
  static final String mcteParam_WDB_SWDISYUN = "//WDB_SWDISYUN";
  static final String mcteParam_WDB_SWPBLIND = "//WDB_SWPBLIND";
  static final String mcteParam_WDB_SWPROPIE = "//WDB_SWPROPIE";
  static final String mcteParam_WDB_TELEFONO = "//WDB_TELEFONO";
  static final String mcteParam_WBD_CODPROV_RGO = "//WBD_CODPROV_RGO";
  static final String mcteParam_WDB_TIPO_COBRO = "//WDB_TIPO_COBRO";
  static final String mcteParam_WDB_TIVIVCOD = "//WDB_TIVIVCOD";
  static final String mcteParam_WDB_USOTIPOS = "//WDB_USOTIPOS";
  static final String mcteParam_WDB_ALARMTIP = "//WDB_ALARMTIP";
  static final String mcteParam_WDB_USRINI = "//WDB_USRINI";
  static final String mcteParam_WDB_USRSPV = "//WDB_USRSPV";
  static final String mcteParam_WDB_USUARIO = "//WDB_USUARIO";
  static final String mcteParam_WDB_VIGENANN = "//WDB_VIGENANN";
  static final String mcteParam_WDB_VIGENDIA = "//WDB_VIGENDIA";
  static final String mcteParam_WDB_VIGENMES = "//WDB_VIGENMES";
  static final String mcteParam_WDB_CODUSUA = "//WDB_CODUSUA";
  /**
   *  Coberturas
   */
  static final String mcteParam_COBERTURAS = "//COBERTURAS/COBERTURA";
  static final String mcteParam_WDB_COBERCOD = "COBERCOD";
  static final String mcteParam_WDB_COBERORD = "COBERORD";
  static final String mcteParam_WDB_CONTRMOD = "CONTRMOD";
  static final String mcteParam_WDB_CAPITASG = "CAPITASG";
  static final String mcteParam_WDB_CAPITIMP = "CAPITIMP";
  /**
   *  Accesorios de la casa
   */
  static final String mcteParam_WDB_TIPO1 = "//WDB_TIPO1";
  static final String mcteParam_WDB_MARCA1 = "//WDB_MARCA1";
  static final String mcteParam_WDB_MODELO1 = "//WDB_MODELO1";
  static final String mcteParam_WDB_TIPO2 = "//WDB_TIPO2";
  static final String mcteParam_WDB_MARCA2 = "//WDB_MARCA2";
  static final String mcteParam_WDB_MODELO2 = "//WDB_MODELO2";
  static final String mcteParam_WDB_TIPO3 = "//WDB_TIPO3";
  static final String mcteParam_WDB_MARCA3 = "//WDB_MARCA3";
  static final String mcteParam_WDB_MODELO3 = "//WDB_MODELO3";
  static final String mcteParam_WDB_TIPO4 = "//WDB_TIPO4";
  static final String mcteParam_WDB_MARCA4 = "//WDB_MARCA4";
  static final String mcteParam_WDB_MODELO4 = "//WDB_MODELO4";
  static final String mcteParam_WDB_TIPO5 = "//WDB_TIPO5";
  static final String mcteParam_WDB_MARCA5 = "//WDB_MARCA5";
  static final String mcteParam_WDB_MODELO5 = "//WDB_MODELO5";
  static final String mcteParam_WDB_TIPO6 = "//WDB_TIPO6";
  static final String mcteParam_WDB_MARCA6 = "//WDB_MARCA6";
  static final String mcteParam_WDB_MODELO6 = "//WDB_MODELO6";
  static final String mcteParam_WDB_TIPO7 = "//WDB_TIPO7";
  static final String mcteParam_WDB_MARCA7 = "//WDB_MARCA7";
  static final String mcteParam_WDB_MODELO7 = "//WDB_MODELO7";
  static final String mcteParam_WDB_TIPO8 = "//WDB_TIPO8";
  static final String mcteParam_WDB_MARCA8 = "//WDB_MARCA8";
  static final String mcteParam_WDB_MODELO8 = "//WDB_MODELO8";
  static final String mcteParam_WDB_TIPO9 = "//WDB_TIPO9";
  static final String mcteParam_WDB_MARCA9 = "//WDB_MARCA9";
  static final String mcteParam_WDB_MODELO9 = "//WDB_MODELO9";
  static final String mcteParam_WDB_TIPO10 = "//WDB_TIPO10";
  static final String mcteParam_WDB_MARCA10 = "//WDB_MARCA10";
  static final String mcteParam_WDB_MODELO10 = "//WDB_MODELO10";
  static final String mcteParam_ZONA = "//CODIZONA";
  static final String mcteParam_CampaTel = "//TELEFONO";
  static final String mcteParam_AgeCod = "//AGECOD";
  /**
   * Datos exigidos por la UIF
   */
  static final String mcteParam_WDB_OCUPACODCLIE = "//OCUPACODCLIE";
  static final String mcteParam_WDB_UIFCUIT = "//UIFCUIT";
  static final String mcteParam_WDB_SWAPODER = "//SWAPODER";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Object wobjClass = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarWDB_ALTURNUM = "";
    String mvarWDB_CALLESEL = "";
    String mvarWDB_CLIENAP1 = "";
    String mvarWDB_CLIENAP2 = "";
    String mvarWDB_CLIENNOM = "";
    String mvarWDB_CO_TITULAR = "";
    String mvarWDB_CODBANCO = "";
    String mvarWDB_CODI_ZONA = "";
    String mvarWDB_CODIGO_COBRO = "";
    String mvarWDB_CODINST = "";
    String mvarWDB_CODPOST_RGO = "";
    String mvarWDB_CODTRANS = "";
    String mvarWDB_DOCUMDAT = "";
    String mvarWDB_DOCUMTIP = "";
    String mvarWDB_DOMICCPOCO = "";
    String mvarWDB_DOMICDNU = "";
    String mvarWDB_DOMICDNUCO = "";
    String mvarWDB_DOMICDOMCOM = "";
    String mvarWDB_DOMICPIS = "";
    String mvarWDB_DOMICPISCO = "";
    String mvarWDB_DOMICPOBCO = "";
    String mvarWDB_DOMICPRECO = "";
    String mvarWDB_DOMICPTA = "";
    String mvarWDB_DOMICPTACO = "";
    String mvarWDB_DOMICTLFCO = "";
    String mvarWDB_EDADCLI = "";
    String mvarWDB_ESTADO = "";
    String mvarWDB_ESTCIVCLI = "";
    String mvarWDB_FECCIECOT_ANO = "";
    String mvarWDB_FECCIECOT_DIA = "";
    String mvarWDB_FECCIECOT_MES = "";
    String mvarWDB_FECHAVTOCTA_ANO = "";
    String mvarWDB_FECHAVTOCTA_DIA = "";
    String mvarWDB_FECHAVTOCTA_MES = "";
    String mvarWDB_FECNTO_AAAA = "";
    String mvarWDB_FECNTO_DD = "";
    String mvarWDB_FECNTO_MM = "";
    String mvarWDB_IVASITCLI = "";
    String mvarWDB_LEGAJO = "";
    String mvarWDB_LOCALIDAD = "";
    String mvarWDB_NROCTA = "";
    String mvarWDB_PRECIO_MENSUAL = "";
    String mvarWDB_PREFIJO = "";
    String mvarWDB_PRODUCTO = "";
    String mvarWDB_PROVICODCO = "";
    String mvarWDB_SEXOCLI = "";
    String mvarWDB_SUCCTA = "";
    String mvarWDB_SWASCENS = "";
    String mvarWDB_SWCALDER = "";
    String mvarWDB_SWCREJAS = "";
    String mvarWDB_SWDISYUN = "";
    String mvarWDB_SWPBLIND = "";
    String mvarWDB_SWPROPIE = "";
    String mvarWDB_TELEFONO = "";
    String mvarWDB_TELNRO = "";
    String mvarWDB_TIPO_COBRO = "";
    String mvarWDB_TIVIVCOD = "";
    String mvarWDB_USOTIPOS = "";
    String mvarWDB_ALARMTIP = "";
    String mvarWDB_USRINI = "";
    String mvarWDB_USRSPV = "";
    String mvarWDB_USUARIO = "";
    String mvarWBD_CODPROV_RGO = "";
    String mvarWDB_VIGENANN = "";
    String mvarWDB_VIGENDIA = "";
    String mvarWDB_VIGENMES = "";
    String mvarWDB_CODUSUA = "";
    String mvarWDB_COBERCOD = "";
    String mvarWDB_COBERORD = "";
    String mvarWDB_CAPITASG = "";
    String mvarWDB_CAPITIMP = "";
    String mvarWDB_MOD_COBERCOD = "";
    String mvarWDB_MOD_CONTRMOD = "";
    String mvarWDB_MOD_CAPITASG = "";
    String mvarWDB_TIPO1 = "";
    String mvarWDB_MARCA1 = "";
    String mvarWDB_MODELO1 = "";
    String mvarWDB_TIPO2 = "";
    String mvarWDB_MARCA2 = "";
    String mvarWDB_MODELO2 = "";
    String mvarWDB_TIPO3 = "";
    String mvarWDB_MARCA3 = "";
    String mvarWDB_MODELO3 = "";
    String mvarWDB_TIPO4 = "";
    String mvarWDB_MARCA4 = "";
    String mvarWDB_MODELO4 = "";
    String mvarWDB_TIPO5 = "";
    String mvarWDB_MARCA5 = "";
    String mvarWDB_MODELO5 = "";
    String mvarWDB_TIPO6 = "";
    String mvarWDB_MARCA6 = "";
    String mvarWDB_MODELO6 = "";
    String mvarWDB_TIPO7 = "";
    String mvarWDB_MARCA7 = "";
    String mvarWDB_MODELO7 = "";
    String mvarWDB_TIPO8 = "";
    String mvarWDB_MARCA8 = "";
    String mvarWDB_MODELO8 = "";
    String mvarWDB_TIPO9 = "";
    String mvarWDB_MARCA9 = "";
    String mvarWDB_MODELO9 = "";
    String mvarWDB_TIPO10 = "";
    String mvarWDB_MARCA10 = "";
    String mvarWDB_MODELO10 = "";
    String mvarRamoPCod = "";
    String mvarPlanNCod = "";
    String mvarPolizSec = "";
    String mvarPolizAnn = "";
    String mvarBancoCod = "";
    String mvarSucurCod = "";
    String mvarAgentCla = "";
    String wvarWDB_OCUPACODCLIE = "";
    String wvarWDB_UIFCUIT = "";
    String wvarWDB_SWAPODER = "";
    //
    //
    // DATOS GENERALES
    //
    //
    //
    //Datos exigidos por la UIF
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      // DATOS GENERALES
      mvarWDB_ALTURNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ALTURNUM ) */ );
      mvarWDB_CALLESEL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CALLESEL ) */ );
      mvarWDB_CLIENAP1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENAP1 ) */ );
      mvarWDB_CLIENAP2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENAP2 ) */ );
      mvarWDB_CLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENNOM ) */ );
      mvarWDB_CO_TITULAR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CO_TITULAR ) */ );
      mvarWDB_CODBANCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODBANCO ) */ );
      mvarWDB_CODIGO_COBRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODIGO_COBRO ) */ );
      mvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODINST ) */ );
      mvarWDB_CODPOST_RGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODPOST_RGO ) */ );
      mvarWDB_CODTRANS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODTRANS ) */ );
      mvarWDB_DOCUMDAT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOCUMDAT ) */ );
      mvarWDB_DOCUMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOCUMTIP ) */ );
      mvarWDB_DOMICCPOCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICCPOCO ) */ );
      mvarWDB_DOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDNU ) */ );
      mvarWDB_DOMICDNUCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDNUCO ) */ );
      mvarWDB_DOMICDOMCOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDOMCOM ) */ );
      mvarWDB_DOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPIS ) */ );
      mvarWDB_DOMICPISCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPISCO ) */ );
      mvarWDB_DOMICPOBCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPOBCO ) */ );
      mvarWDB_DOMICPRECO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPRECO ) */ );
      mvarWDB_DOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPTA ) */ );
      mvarWDB_DOMICPTACO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPTACO ) */ );
      mvarWDB_DOMICTLFCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICTLFCO ) */ );
      mvarWDB_EDADCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_EDADCLI ) */ );
      mvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ESTADO ) */ );
      mvarWDB_ESTCIVCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ESTCIVCLI ) */ );
      mvarWDB_FECCIECOT_ANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_ANO ) */ );
      mvarWDB_FECCIECOT_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_DIA ) */ );
      mvarWDB_FECCIECOT_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_MES ) */ );
      mvarWDB_FECHAVTOCTA_ANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_ANO ) */ );
      mvarWDB_FECHAVTOCTA_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_DIA ) */ );
      mvarWDB_FECHAVTOCTA_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_MES ) */ );
      mvarWDB_FECNTO_AAAA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_AAAA ) */ );
      mvarWDB_FECNTO_DD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_DD ) */ );
      mvarWDB_FECNTO_MM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_MM ) */ );
      mvarWDB_IVASITCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_IVASITCLI ) */ );
      mvarWDB_LEGAJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_LEGAJO ) */ );
      mvarWDB_LOCALIDAD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_LOCALIDAD ) */ );
      mvarWDB_NROCTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_NROCTA ) */ );
      mvarWDB_PRECIO_MENSUAL = Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRECIO_MENSUAL ) */ ), ",", "." );
      mvarWDB_PREFIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PREFIJO ) */ );
      mvarWDB_PRODUCTO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRODUCTO ) */ );
      mvarWDB_PROVICODCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PROVICODCO ) */ );
      mvarWDB_SEXOCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SEXOCLI ) */ );
      mvarWDB_SUCCTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUCCTA ) */ );
      mvarWBD_CODPROV_RGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WBD_CODPROV_RGO ) */ );
      mvarWDB_SWASCENS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWASCENS ) */ );
      mvarWDB_SWCALDER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWCALDER ) */ );
      mvarWDB_SWCREJAS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWCREJAS ) */ );
      mvarWDB_SWDISYUN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWDISYUN ) */ );
      mvarWDB_SWPBLIND = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWPBLIND ) */ );
      mvarWDB_SWPROPIE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWPROPIE ) */ );
      mvarWDB_TELEFONO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELEFONO ) */ );
      mvarWDB_TIPO_COBRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_COBRO ) */ );
      mvarWDB_TIVIVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIVIVCOD ) */ );
      mvarWDB_USOTIPOS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USOTIPOS ) */ );
      mvarWDB_ALARMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ALARMTIP ) */ );
      mvarWDB_USRINI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRINI ) */ );
      mvarWDB_USRSPV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRSPV ) */ );
      mvarWDB_USUARIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USUARIO ) */ );
      mvarWDB_VIGENANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENANN ) */ );
      mvarWDB_VIGENDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENDIA ) */ );
      mvarWDB_VIGENMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENMES ) */ );
      mvarWDB_TIPO1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO1 ) */ );
      mvarWDB_MARCA1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA1 ) */ );
      mvarWDB_MODELO1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO1 ) */ );
      mvarWDB_TIPO2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO2 ) */ );
      mvarWDB_MARCA2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA2 ) */ );
      mvarWDB_MODELO2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO2 ) */ );
      mvarWDB_TIPO3 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO3 ) */ );
      mvarWDB_MARCA3 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA3 ) */ );
      mvarWDB_MODELO3 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO3 ) */ );
      mvarWDB_TIPO4 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO4 ) */ );
      mvarWDB_MARCA4 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA4 ) */ );
      mvarWDB_MODELO4 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO4 ) */ );
      mvarWDB_TIPO5 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO5 ) */ );
      mvarWDB_MARCA5 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA5 ) */ );
      mvarWDB_MODELO5 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO5 ) */ );
      mvarWDB_TIPO6 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO6 ) */ );
      mvarWDB_MARCA6 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA6 ) */ );
      mvarWDB_MODELO6 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO6 ) */ );
      mvarWDB_TIPO7 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO7 ) */ );
      mvarWDB_MARCA7 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA7 ) */ );
      mvarWDB_MODELO7 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO7 ) */ );
      mvarWDB_TIPO8 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO8 ) */ );
      mvarWDB_MARCA8 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA8 ) */ );
      mvarWDB_MODELO8 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO8 ) */ );
      mvarWDB_TIPO9 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO9 ) */ );
      mvarWDB_MARCA9 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA9 ) */ );
      mvarWDB_MODELO9 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO9 ) */ );
      mvarWDB_TIPO10 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO10 ) */ );
      mvarWDB_MARCA10 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MARCA10 ) */ );
      mvarWDB_MODELO10 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MODELO10 ) */ );
      mvarWDB_CODUSUA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODUSUA ) */ );

      //Datos exigidos por la UIF
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_OCUPACODCLIE ) */ == (org.w3c.dom.Node) null) )
      {
        wvarWDB_OCUPACODCLIE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_OCUPACODCLIE ) */ );
      }
      else
      {
        wvarWDB_OCUPACODCLIE = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_UIFCUIT ) */ == (org.w3c.dom.Node) null) )
      {
        wvarWDB_UIFCUIT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_UIFCUIT ) */ );
      }
      else
      {
        wvarWDB_UIFCUIT = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWAPODER ) */ == (org.w3c.dom.Node) null) )
      {
        wvarWDB_SWAPODER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWAPODER ) */ );
      }
      else
      {
        wvarWDB_SWAPODER = "";
      }
      //FIN: Datos exigidos por la UIF
      // LEVANTO LOS PARAMETROS PARA HOGAR (ParametrosCotizador.xml)
      wvarStep = 32;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      mvarRamoPCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteRAMOPCOD ) */ );
      mvarPlanNCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePLANNCOD ) */ );
      mvarPolizSec = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZSEC ) */ );
      mvarPolizAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZANN ) */ );
      mvarBancoCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteBANCOCOD ) */ );
      mvarSucurCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteSUCURCOD ) */ );
      //
      wvarStep = 30;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;

      // LEVANTO LA ZONA CON MSG MQ
      wvarStep = 40;
      wvarRequest = "<Request><RAMOPCOD>" + mvarRamoPCod + "</RAMOPCOD><PROVICOD>" + mvarWBD_CODPROV_RGO + "</PROVICOD><CPACODPO>" + mvarWDB_CODPOST_RGO + "</CPACODPO></Request>";
      wobjClass = new lbawA_OVLBAMQ.lbaw_GetZona();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      //
      // LEVANTO EL VALOR DE LA ZONA
      wvarStep = 12;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.loadXML( wvarResponse );
      mvarWDB_CODI_ZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( mcteParam_ZONA ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      // BUSCO LOS DATOS DE PRODUCTOR Y TELEFONO
      wvarStep = 22;
      wvarRequest = "<Request><PORTAL>LBA</PORTAL><RAMOPCOD>" + mvarRamoPCod + "</RAMOPCOD></Request>";
      wobjClass = new Variant( new com.qbe.services.lbaw_GetPortalComerc() )new com.qbe.services.lbaw_GetPortalComerc().toObject();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      //
      wvarStep = 24;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.loadXML( wvarResponse );
      mvarAgentCla = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( mcteParam_AgeCod ) */ ) );
      mvarWDB_TELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( mcteParam_CampaTel ) */ );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      wobjClass = null;
      //
      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 60;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 70;

      // Obligatorios
      wvarStep = 1430;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1440;
      wobjDBParm = new Parameter( "@WDB_CDNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1450;
      wobjDBParm = new Parameter( "@WDB_ESLNRO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1460;
      wobjDBParm = new Parameter( "@WDB_CONTINUACION", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1470;
      wobjDBParm = new Parameter( "@WDB_CO_TITULAR", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( mvarWDB_CO_TITULAR ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1480;

      wobjDBParm = new Parameter( "@WDB_USUARIO", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_USUARIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1490;

      wobjDBParm = new Parameter( "@WDB_HORAINI", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1500;

      wobjDBParm = new Parameter( "@WDB_HORAFIN", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1510;

      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( mvarWDB_ESTADO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1520;

      wobjDBParm = new Parameter( "@WDB_USRINI", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_USRINI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1530;

      wobjDBParm = new Parameter( "@WDB_USRSPV", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_USRSPV ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1540;

      wobjDBParm = new Parameter( "@WDB_FECCIECOT_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECCIECOT_DIA.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_FECCIECOT_DIA)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1550;

      wobjDBParm = new Parameter( "@WDB_FECCIECOT_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECCIECOT_MES.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_FECCIECOT_MES)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 15560;

      wobjDBParm = new Parameter( "@WDB_FECCIECOT_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECCIECOT_ANO.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_FECCIECOT_ANO)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1570;

      wobjDBParm = new Parameter( "@WDB_CTICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1580;

      wobjDBParm = new Parameter( "@WDB_TELNRO", AdoConst.adChar, AdoConst.adParamInput, 12, new Variant( (mvarWDB_TELNRO.equals( "" ) ? " " : mvarWDB_TELNRO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1590;

      wobjDBParm = new Parameter( "@WDB_DMS_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarRamoPCod ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1600;

      wobjDBParm = new Parameter( "@WDB_DMS_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPolizAnn ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1610;

      wobjDBParm = new Parameter( "@WDB_DMS_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPolizSec ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1620;

      wobjDBParm = new Parameter( "@WDB_DMS_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1630;

      wobjDBParm = new Parameter( "@WDB_DMS_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1640;

      wobjDBParm = new Parameter( "@WDB_DMS_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1650;

      wobjDBParm = new Parameter( "@WDB_FECENVIO_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1660;

      wobjDBParm = new Parameter( "@WDB_FECENVIO_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1670;

      wobjDBParm = new Parameter( "@WDB_FECENVIO_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_CODUSUA", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant( mvarWDB_CODUSUA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1680;

      wvarStep = 1680;
      wobjDBParm = new Parameter( "@WDB_FECORIA_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1690;

      wobjDBParm = new Parameter( "@WDB_FECORIA_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1700;

      wobjDBParm = new Parameter( "@WDB_FECORIA_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1710;

      wobjDBParm = new Parameter( "@WDB_FECDST_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1720;

      wobjDBParm = new Parameter( "@WDB_FECDST_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1730;

      wobjDBParm = new Parameter( "@WDB_FECDST_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1740;

      wobjDBParm = new Parameter( "@WDB_FECDST_HORA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1750;

      wobjDBParm = new Parameter( "@WDB_FECDST_MINUTO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1760;

      wobjDBParm = new Parameter( "@WDB_AGENDA_RECH_OBS", AdoConst.adChar, AdoConst.adParamInput, 150, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1780;

      wobjDBParm = new Parameter( "@WDB_RCOCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1790;

      wobjDBParm = new Parameter( "@WDB_FECORI_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1800;

      wobjDBParm = new Parameter( "@WDB_FECORI_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1810;

      wobjDBParm = new Parameter( "@WDB_FECORI_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1820;

      for( wvarCounter = 1; wvarCounter <= 10; wvarCounter++ )
      {
        wobjDBParm = new Parameter( "@WDB_DOCUMDATCOTIT" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_APENOMCOTIT" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wobjDBParm = new Parameter( "@WDB_CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1830;

      wobjDBParm = new Parameter( "@WDB_CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENAP1.equals( "" ) ? " " : mvarWDB_CLIENAP1) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1840;

      wobjDBParm = new Parameter( "@WDB_CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_CLIENAP2.equals( "" ) ? " " : mvarWDB_CLIENAP2) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1850;

      wobjDBParm = new Parameter( "@WDB_CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_CLIENNOM.equals( "" ) ? " " : mvarWDB_CLIENNOM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1860;

      wobjDBParm = new Parameter( "@WDB_DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( (mvarWDB_DOCUMTIP.equals( "" ) ? "0" : mvarWDB_DOCUMTIP) ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1870;

      wobjDBParm = new Parameter( "@WDB_DOCUMDAT", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( (mvarWDB_DOCUMDAT.equals( "" ) ? " " : mvarWDB_DOCUMDAT) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1880;

      wobjDBParm = new Parameter( "@WDB_FECNTO_DD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECNTO_DD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_FECNTO_DD)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1890;

      wobjDBParm = new Parameter( "@WDB_FECNTO_MM", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECNTO_MM.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_FECNTO_MM)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1900;

      wobjDBParm = new Parameter( "@WDB_FECNTO_AAAA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECNTO_AAAA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_FECNTO_AAAA)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1910;

      wobjDBParm = new Parameter( "@WDB_EDADCLI", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_EDADCLI.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_EDADCLI)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1920;

      wobjDBParm = new Parameter( "@WDB_ESTCIVCLI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_ESTCIVCLI.equals( "" ) ? " " : mvarWDB_ESTCIVCLI) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1930;

      wobjDBParm = new Parameter( "@WDB_SEXOCLI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SEXOCLI.equals( "" ) ? " " : mvarWDB_SEXOCLI) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1940;

      wobjDBParm = new Parameter( "@WDB_LEGAJO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_LEGAJO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_LEGAJO)) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1950;

      wobjDBParm = new Parameter( "@WDB_IVASITCLI", AdoConst.adChar, AdoConst.adParamInput, 1, (mvarWDB_IVASITCLI.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_IVASITCLI)) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1960;

      wobjDBParm = new Parameter( "@WDB_PREFIJO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_PREFIJO.equals( "" ) ? " " : mvarWDB_PREFIJO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1970;

      wobjDBParm = new Parameter( "@WDB_TELEFONO", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( (mvarWDB_TELEFONO.equals( "" ) ? " " : mvarWDB_TELEFONO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1980;

      wobjDBParm = new Parameter( "@WDB_FECALTA_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 1990;

      wobjDBParm = new Parameter( "@WDB_FECALTA_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2000;

      wobjDBParm = new Parameter( "@WDB_FECALTA_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2010;

      wobjDBParm = new Parameter( "@WDB_USUARIO_ALTA", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2020;

      wobjDBParm = new Parameter( "@WDB_EMAIL", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2030;

      wobjDBParm = new Parameter( "@WDB_CALLESEL", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( (mvarWDB_CALLESEL.equals( "" ) ? " " : mvarWDB_CALLESEL) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2030;

      wobjDBParm = new Parameter( "@WDB_DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICDNU.equals( "" ) ? " " : mvarWDB_DOMICDNU) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2040;

      wobjDBParm = new Parameter( "@WDB_DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPIS.equals( "" ) ? " " : mvarWDB_DOMICPIS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2050;

      wobjDBParm = new Parameter( "@WDB_DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPTA.equals( "" ) ? " " : mvarWDB_DOMICPTA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_CODPOST_RGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODPOST_RGO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODPOST_RGO)) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_LOCALIDAD", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( (mvarWDB_LOCALIDAD.equals( "" ) ? " " : mvarWDB_LOCALIDAD) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WBD_CODPROV_RGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWBD_CODPROV_RGO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWBD_CODPROV_RGO)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_DOMICDOMCOM", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( (mvarWDB_DOMICDOMCOM.equals( "" ) ? " " : mvarWDB_DOMICDOMCOM) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_DOMICDNUCO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICDNUCO.equals( "" ) ? " " : mvarWDB_DOMICDNUCO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_DOMICPISCO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPISCO.equals( "" ) ? " " : mvarWDB_DOMICPISCO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_DOMICPTACO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_DOMICPTACO.equals( "" ) ? " " : mvarWDB_DOMICPTACO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_DOMICPOBCO", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( (mvarWDB_DOMICPOBCO.equals( "" ) ? " " : mvarWDB_DOMICPOBCO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2060;

      wobjDBParm = new Parameter( "@WDB_DOMICCPOCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_DOMICCPOCO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_DOMICCPOCO)) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2070;

      wobjDBParm = new Parameter( "@WDB_PROVICODCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PROVICODCO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PROVICODCO)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2080;

      wobjDBParm = new Parameter( "@WDB_DOMICPRECO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( (mvarWDB_DOMICPRECO.equals( "" ) ? " " : mvarWDB_DOMICPRECO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2090;

      wobjDBParm = new Parameter( "@WDB_DOMICTLFCO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( (mvarWDB_DOMICTLFCO.equals( "" ) ? " " : mvarWDB_DOMICTLFCO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2100;

      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODINST.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODINST)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2110;

      wobjDBParm = new Parameter( "@WDB_MAQUINA", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2120;

      wobjDBParm = new Parameter( "@WDB_CODTRANS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_CODTRANS.equals( "" ) ? " " : mvarWDB_CODTRANS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2130;

      wobjDBParm = new Parameter( "@WDB_PRODUCTO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( (mvarWDB_PRODUCTO.equals( "" ) ? " " : mvarWDB_PRODUCTO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2140;

      wobjDBParm = new Parameter( "@WDB_VIGENDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VIGENDIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VIGENDIA)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2150;

      wobjDBParm = new Parameter( "@WDB_VIGENMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VIGENMES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VIGENMES)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2160;

      wobjDBParm = new Parameter( "@WDB_VIGENANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_VIGENANN.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_VIGENANN)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2170;

      wobjDBParm = new Parameter( "@WDB_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2180;

      wobjDBParm = new Parameter( "@WDB_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2190;

      wobjDBParm = new Parameter( "@WDB_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2200;

      wobjDBParm = new Parameter( "@WDB_CODI_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODI_ZONA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODI_ZONA)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2210;

      wobjDBParm = new Parameter( "@WDB_CODMONE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2220;

      wobjDBParm = new Parameter( "@WDB_PRECIO_MENSUAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_PRECIO_MENSUAL.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_PRECIO_MENSUAL)) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2230;

      wobjDBParm = new Parameter( "@WDB_PRECIO_PROPORCIONAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2240;

      wobjDBParm = new Parameter( "@WDB_BONIF_MEMBRECIA_POR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2250;

      wobjDBParm = new Parameter( "@WDB_CUOTA_MENBRECIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2260;

      wobjDBParm = new Parameter( "@WDB_FECVENCI_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2270;

      wobjDBParm = new Parameter( "@WDB_FECVENCI_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2280;

      wobjDBParm = new Parameter( "@WDB_FECVENCI_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2290;

      wobjDBParm = new Parameter( "@WDB_PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarPlanNCod ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2300;

      wobjDBParm = new Parameter( "@WDB_EMISDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2310;

      wobjDBParm = new Parameter( "@WDB_EMISMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2320;

      wobjDBParm = new Parameter( "@WDB_EMISANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2330;

      wobjDBParm = new Parameter( "@WDB_CAMPANA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2340;

      wobjDBParm = new Parameter( "@WDB_PRODUCTOR", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( mvarAgentCla ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2350;

      wobjDBParm = new Parameter( "@WDB_BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarBancoCod ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2360;

      wobjDBParm = new Parameter( "@WDB_SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarSucurCod ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2370;

      wobjDBParm = new Parameter( "@WDB_VENDEDOR", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2380;

      wobjDBParm = new Parameter( "@WDB_NROLEGGTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2390;

      wobjDBParm = new Parameter( "@WDB_NROCOTIZACION", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2400;

      wobjDBParm = new Parameter( "@WDB_CODIGO_COBRO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODIGO_COBRO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODIGO_COBRO)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2410;

      wobjDBParm = new Parameter( "@WDB_TIPO_COBRO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( (mvarWDB_TIPO_COBRO.equals( "" ) ? " " : mvarWDB_TIPO_COBRO) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2420;

      wobjDBParm = new Parameter( "@WDB_NROCTA", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( (mvarWDB_NROCTA.equals( "" ) ? " " : mvarWDB_NROCTA) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2430;

      wobjDBParm = new Parameter( "@WDB_CODBANCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CODBANCO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_CODBANCO)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2440;

      wobjDBParm = new Parameter( "@WDB_SUCCTA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_SUCCTA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_SUCCTA)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2450;

      wobjDBParm = new Parameter( "@WDB_FECHAVTOCTA_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECHAVTOCTA_DIA.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_FECHAVTOCTA_DIA)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2460;

      wobjDBParm = new Parameter( "@WDB_FECHAVTOCTA_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECHAVTOCTA_MES.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_FECHAVTOCTA_MES)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2470;

      wobjDBParm = new Parameter( "@WDB_FECHAVTOCTA_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_FECHAVTOCTA_ANO.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_FECHAVTOCTA_ANO)) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2480;

      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_COBERTURAS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        mvarWDB_COBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_COBERCOD ) */ );
        mvarWDB_COBERORD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_COBERORD ) */ );
        mvarWDB_CAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_CAPITASG ) */ );
        mvarWDB_CAPITIMP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_CAPITIMP ) */ );

        wobjDBParm = new Parameter( "@WDB_COBERCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBERCOD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBERCOD)) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_COBERORD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_COBERORD.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_COBERORD)) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_CAPITASG" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CAPITASG.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CAPITASG)) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_CAPITIMP" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_CAPITIMP.equals( "" ) ? new Variant((Object)null) : new Variant(mvarWDB_CAPITIMP)) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 2500;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 20; wvarCounter++ )
      {

        wobjDBParm = new Parameter( "@WDB_COBERCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_COBERORD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_CAPITASG" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_CAPITIMP" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;


      }

      wobjXMLList = (org.w3c.dom.NodeList) null;

      wvarStep = 2510;
      wobjDBParm = new Parameter( "@WDB_TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_TIVIVCOD.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_TIVIVCOD)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2520;

      wobjDBParm = new Parameter( "@WDB_ALTURNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_ALTURNUM.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_ALTURNUM)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2530;

      wobjDBParm = new Parameter( "@WDB_USOTIPOS", AdoConst.adNumeric, AdoConst.adParamInput, 0, (mvarWDB_USOTIPOS.equals( "" ) ? new Variant((Object)new Integer( 0 )) : new Variant(mvarWDB_USOTIPOS)) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2540;

      wobjDBParm = new Parameter( "@WDB_SWCALDER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SWCALDER.equals( "" ) ? " " : mvarWDB_SWCALDER) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2550;

      wobjDBParm = new Parameter( "@WDB_SWASCENS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SWASCENS.equals( "" ) ? " " : mvarWDB_SWASCENS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2560;

      wobjDBParm = new Parameter( "@WDB_ALARMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_ALARMTIP ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2570;

      wobjDBParm = new Parameter( "@WDB_GUARDTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2580;

      wobjDBParm = new Parameter( "@WDB_SWCREJAS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SWCREJAS.equals( "" ) ? " " : mvarWDB_SWCREJAS) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2590;

      wobjDBParm = new Parameter( "@WDB_SWPBLIND", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SWPBLIND.equals( "" ) ? " " : mvarWDB_SWPBLIND) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2600;

      wobjDBParm = new Parameter( "@WDB_SWDISYUN", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SWDISYUN.equals( "" ) ? " " : mvarWDB_SWDISYUN) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2610;

      wobjDBParm = new Parameter( "@WDB_ZORIECOD", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2620;

      wobjDBParm = new Parameter( "@WDB_SWPROPIE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( (mvarWDB_SWPROPIE.equals( "" ) ? " " : mvarWDB_SWPROPIE) ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2630;

      wobjDBParm = new Parameter( "@WDB_MASCACOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      wvarStep = 2640;

      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_COBERTURAS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        mvarWDB_MOD_COBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_COBERCOD ) */ );
        mvarWDB_MOD_CONTRMOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_CONTRMOD ) */ );
        mvarWDB_MOD_CAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_CAPITASG ) */ );

        wobjDBParm = new Parameter( "@WDB_MOD_COBERCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_MOD_COBERCOD ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_MOD_CONTRMOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_MOD_CONTRMOD ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_MOD_CAPITASG" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( mvarWDB_MOD_CAPITASG ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 2500;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 20; wvarCounter++ )
      {

        wobjDBParm = new Parameter( "@WDB_MOD_COBERCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_MOD_CONTRMOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@WDB_MOD_CAPITASG" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      wvarStep = 2650;

      wobjDBParm = new Parameter( "@WDB_TIPO1", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA1", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO1", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO2", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA2", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO2", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO3", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA3", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO3", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO3 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO4", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA4", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO4", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO4 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO5", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA5", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO5", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO5 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO6", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA6", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO6", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO6 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO7", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA7", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO7", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO7 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO8", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA8", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO8", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO8 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO9", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA9", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO9", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO9 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_TIPO10", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( mvarWDB_TIPO10 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MARCA10", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MARCA10 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_MODELO10", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( mvarWDB_MODELO10 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 26660;

      wobjDBParm = new Parameter( "@WDB_ERRORMSG", AdoConst.adChar, AdoConst.adParamInput, 70, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //11/12/2006 - DA: Datos exigidos por la UIF
      wvarStep = 26661;
      wobjDBParm = new Parameter( "@WDB_OCUPACODCLIE", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( wvarWDB_OCUPACODCLIE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 26662;
      wobjDBParm = new Parameter( "@WDB_UIFCUIT", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarWDB_UIFCUIT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 26663;
      wobjDBParm = new Parameter( "@WDB_SWAPODER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_SWAPODER ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //FIN: Datos exigidos por la UIF
      wvarStep = 140;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 150;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CADENA>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</CADENA></Response>" );
        //pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><COTIID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</COTIID></Response>"
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -10 )
        {
          wvarMensaje = "ERROR AL OBTENER LA CLAVE-ESLABON";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -11 )
        {
          wvarMensaje = "DEBE INGRESAR UN NOMBRE DE USUARIO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -12 )
        {
          wvarMensaje = "DEBE INGRESAR UN ESTADO VALIDO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -13 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA REL_CENTRAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -14 )
        {
          wvarMensaje = "ERROR AL ACTUALIZAR LA MARCA DE ULTIMO REGISTRO EN LA TABLA REL_CENTRAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -15 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA ADM_AGENDA";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -16 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA ADM_RECAHZO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -17 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -18 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -19 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -20 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -21 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -22 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -23 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -24 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -25 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -26 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -27 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CLIENTE";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -28 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_DOMICILIOS";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -29 )
        {
          wvarMensaje = "ERROR AL ERROR EN EL PARAMETRO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -30 )
        {
          wvarMensaje = "ERROR AL SE DEBE INGRESAR UN CODIGO DE PRODUCTO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -31 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_GENERAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -32 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CUENTA";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -33 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -34 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -35 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -36 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -37 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -38 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -39 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -40 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -41 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -42 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -43 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 11";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -44 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 12";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -45 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 13";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -46 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 14";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -47 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 15";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -48 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 16";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -49 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 17";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -50 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 18";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -51 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 19";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -52 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS 20";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -53 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_VIVIENDA";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -54 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -55 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -56 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -57 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -58 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -59 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -60 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -61 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -62 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -63 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -64 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 11";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -65 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 12";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -66 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 13";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -67 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 14";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -68 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 15";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -69 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 16";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -70 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 17";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -71 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 18";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -72 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 19";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -73 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_MODULOS 20";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -74 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -75 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -76 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -77 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -78 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -79 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -80 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -81 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -82 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -83 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_HOGAR_ELECTRO 10";
        }

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 180;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
