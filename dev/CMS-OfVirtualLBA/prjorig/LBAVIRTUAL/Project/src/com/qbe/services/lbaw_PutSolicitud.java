package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutSolicitud implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_PutSolicitud";
  static final String mcteStoreProc = "SPSNCV_INSERT_SOLICITUD";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_AUPROCOD = "//AUPROCOD";
  static final String mcteParam_WDB_ACCESORIOS_FLG = "//WDB_ACCESORIOS_FLG";
  static final String mcteParam_WDB_PRENDA = "//WDB_PRENDA";
  static final String mcteParam_WDB_CO_TITULAR = "//WDB_CO_TITULAR";
  static final String mcteParam_WDB_HIJOS1416_FLG = "//WDB_HIJOS1416_FLG";
  static final String mcteParam_WDB_HIJOS1729_FLG = "//WDB_HIJOS1729_FLG";
  static final String mcteParam_WDB_CONSEJO_FLG = "//WDB_CONSEJO_FLG ";
  static final String mcteParam_WDB_REFERIDO_FLG = "//WDB_REFERIDO_FLG";
  static final String mcteParam_WDB_USUARIO = "//WDB_USUARIO";
  static final String mcteParam_WDB_ESTADO = "//WDB_ESTADO";
  static final String mcteParam_WDB_USRINI = "//WDB_USRINI";
  static final String mcteParam_WDB_USRSPV = "//WDB_USRSPV";
  static final String mcteParam_WDB_FECCIECOT_DIA = "//WDB_FECCIECOT_DIA";
  static final String mcteParam_WDB_FECCIECOT_MES = "//WDB_FECCIECOT_MES";
  static final String mcteParam_WDB_FECCIECOT_ANO = "//WDB_FECCIECOT_ANO";
  static final String mcteParam_WDB_TELNRO = "//WDB_TELNRO";
  /**
   *  CAMPANIA, PRODUCTOR Y TELEFONO
   */
  static final String mcteParam_CAMPA_TEL = "//CAMPA_TEL";
  static final String mcteParam_CAMPA_COD = "//CAMPA_COD";
  static final String mcteParam_AGE_COD = "//AGE_COD";
  /**
   *  Accesorios
   */
  static final String mcteParam_ACCESORIOS = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_WDB_AC_AUACCCOD = "CODIGOACC";
  static final String mcteParam_WDB_AC_SUMA = "PRECIOACC";
  static final String mcteParam_WDB_AC_DESCRI = "DESCRIPCIONACC";
  static final String mcteParam_WDB_AUMARCOD = "//WDB_AUMARCOD";
  static final String mcteParam_WDB_AUMODCOD = "//WDB_AUMODCOD";
  static final String mcteParam_WDB_AUSUBCOD = "//WDB_AUSUBCOD";
  static final String mcteParam_WDB_AUADICOD = "//WDB_AUADICOD";
  static final String mcteParam_WDB_AUMODORI = "//WDB_AUMODORI";
  static final String mcteParam_WDB_FABRICAN = "//WDB_FABRICAN";
  static final String mcteParam_WDB_AUTIPCOD = "//WDB_AUTIPCOD";
  static final String mcteParam_WDB_AUKLMNUM = "//WDB_AUKLMNUM";
  static final String mcteParam_WDB_SUMAASEG = "//WDB_SUMAASEG";
  static final String mcteParam_WDB_SUMALBA = "//WDB_SUMALBA";
  static final String mcteParam_WDB_AUUSOCOD = "//WDB_AUUSOCOD";
  static final String mcteParam_WDB_AUTIPCOM = "//WDB_AUTIPCOM";
  static final String mcteParam_WDB_CHASISNUM = "//WDB_CHASISNUM";
  static final String mcteParam_WDB_MOTORNUM = "//WDB_MOTORNUM";
  static final String mcteParam_WDB_PATENNUM = "//WDB_PATENNUM";
  static final String mcteParam_WDB_ANOCOMP = "//WDB_ANOCOMP";
  static final String mcteParam_WDB_GARAJE_FLG = "//WDB_GARAJE_FLG";
  static final String mcteParam_WDB_GARAJE_PART = "//WDB_GARAJE_PART";
  static final String mcteParam_WDB_CEROKM_FLG = "//WDB_CEROKM_FLG";
  static final String mcteParam_WDB_AUCATCOD = "//WDB_AUCATCOD";
  static final String mcteParam_WDB_AUUSOGNC = "//WDB_AUUSOGNC";
  static final String mcteParam_WDB_COLOR = "//WDB_COLOR";
  static final String mcteParam_WDB_CLIENAP1 = "//WDB_CLIENAP1";
  static final String mcteParam_WDB_CLIENAP2 = "//WDB_CLIENAP2";
  static final String mcteParam_WDB_CLIENNOM = "//WDB_CLIENNOM";
  static final String mcteParam_WDB_DOCUMTIP = "//WDB_DOCUMTIP";
  static final String mcteParam_WDB_DOCUMDAT = "//WDB_DOCUMDAT";
  static final String mcteParam_WDB_FECNTO_DD = "//WDB_FECNTO_DD";
  static final String mcteParam_WDB_FECNTO_MM = "//WDB_FECNTO_MM";
  static final String mcteParam_WDB_FECNTO_AAAA = "//WDB_FECNTO_AAAA";
  static final String mcteParam_WDB_EDADCLI = "//WDB_EDADCLI";
  static final String mcteParam_WDB_ESTCIVCLI = "//WDB_ESTCIVCLI";
  static final String mcteParam_WDB_SEXOCLI = "//WDB_SEXOCLI";
  static final String mcteParam_WDB_LEGAJO = "//WDB_LEGAJO";
  static final String mcteParam_WDB_IVASITCLI = "//WDB_IVASITCLI";
  static final String mcteParam_WDB_PREFIJO = "//WDB_PREFIJO";
  static final String mcteParam_WDB_TELEFXXX = "//WDB_TELEFXXX";
  static final String mcteParam_WDB_FECALTA_DIA = "//WDB_FECALTA_DIA";
  static final String mcteParam_WDB_FECALTA_MES = "//WDB_FECALTA_MES";
  static final String mcteParam_WDB_FECALTA_ANO = "//WDB_FECALTA_ANO";
  static final String mcteParam_WDB_ANOLICEN = "//WDB_ANOLICEN";
  /**
   * Hijos
   */
  static final String mcteParam_HIJOS = "//HIJOS/HIJO";
  static final String mcteParam_WDB_APELLIDO_HIJO = "WDB_APELLIDO_HIJO";
  static final String mcteParam_WDB_NOMBRE_HIJO = "WDB_NOMBRE_HIJO";
  static final String mcteParam_WDB_SEXO_HIJO = "WDB_SEXO_HIJO";
  static final String mcteParam_WDB_NACIM_FEC_HIJO_DIA = "WDB_NACIM_FEC_HIJO_DIA";
  static final String mcteParam_WDB_NACIM_FEC_HIJO_MES = "WDB_NACIM_FEC_HIJO_MES";
  static final String mcteParam_WDB_NACIM_FEC_HIJO_ANO = "WDB_NACIM_FEC_HIJO_ANO";
  static final String mcteParam_WDB_EDAD_HIJO = "WDB_EDAD_HIJO";
  static final String mcteParam_WDB_ESTCIV_HIJO = "WDB_ESTCIV_HIJO";
  static final String mcteParam_WDB_INCLUIDO_HIJO = "WDB_INCLUIDO_HIJO";
  /**
   * 
   */
  static final String mcteParam_WDB_CALLESEL = "//WDB_CALLESEL";
  static final String mcteParam_WDB_DOMICDNU = "//WDB_DOMICDNU";
  static final String mcteParam_WDB_DOMICPIS = "//WDB_DOMICPIS";
  static final String mcteParam_WDB_DOMICPTA = "//WDB_DOMICPTA";
  static final String mcteParam_WDB_CODPOST_RGO = "//WDB_CODPOST_RGO";
  static final String mcteParam_WDB_LOCALIDAD = "//WDB_LOCALIDAD";
  static final String mcteParam_WBD_CODPROV_RGO = "//WBD_CODPROV_RGO";
  static final String mcteParam_WDB_DOMICDOMCOM = "//WDB_DOMICDOMCOM";
  static final String mcteParam_WDB_DOMICDNUCO = "//WDB_DOMICDNUCO";
  static final String mcteParam_WDB_DOMICPISCO = "//WDB_DOMICPISCO";
  static final String mcteParam_WDB_DOMICPTACO = "//WDB_DOMICPTACO";
  static final String mcteParam_WDB_DOMICPOBCO = "//WDB_DOMICPOBCO";
  static final String mcteParam_WDB_DOMICCPOCO = "//WDB_DOMICCPOCO";
  static final String mcteParam_WDB_PROVICODCO = "//WDB_PROVICODCO";
  static final String mcteParam_WDB_DOMICPRECO = "//WDB_DOMICPRECO";
  static final String mcteParam_WDB_DOMICTLFCO = "//WDB_DOMICTLFCO";
  static final String mcteParam_WDB_CODINST = "//WDB_CODINST";
  static final String mcteParam_WDB_CODTRANS = "//WDB_CODTRANS";
  static final String mcteParam_WDB_RAMOPCOD = "//WDB_RAMOPCOD";
  static final String mcteParam_WDB_VIGENDIA = "//WDB_VIGENDIA";
  static final String mcteParam_WDB_VIGENMES = "//WDB_VIGENMES";
  static final String mcteParam_WDB_VIGENANN = "//WDB_VIGENANN";
  static final String mcteParam_WDB_CODI_ZONA = "//WDB_CODI_ZONA";
  static final String mcteParam_WDB_PRECIO_MENSUAL = "//WDB_PRECIO_MENSUAL";
  static final String mcteParam_WDB_SINIESTROS = "//WDB_SINIESTROS";
  static final String mcteParam_WDB_PLANCOD = "//WDB_PLANCOD";
  static final String mcteParam_WDB_FRANQCOD = "//WDB_FRANQCOD";
  static final String mcteParam_WDB_CLUBLBA = "//WDB_CLUBLBA";
  static final String mcteParam_WDB_LUNETA = "//WDB_LUNETA";
  static final String mcteParam_WDB_EMISDIA = "//WDB_EMISDIA";
  static final String mcteParam_WDB_EMISMES = "//WDB_EMISMES";
  static final String mcteParam_WDB_EMISANN = "//WDB_EMISANN";
  static final String mcteParam_WDB_CAMPANA = "//WDB_CAMPANA";
  static final String mcteParam_WDB_PRODUCTOR = "//WDB_PRODUCTOR";
  static final String mcteParam_WDB_BANCOCOD = "//WDB_BANCOCOD";
  static final String mcteParam_WDB_SUCURCOD = "//WDB_SUCURCOD";
  static final String mcteParam_WDB_CODIGO_COBRO = "//WDB_CODIGO_COBRO";
  static final String mcteParam_WDB_TIPO_COBRO = "//WDB_TIPO_COBRO";
  static final String mcteParam_WDB_VEHDES = "//WDB_VEHDES";
  static final String mcteParam_WDB_NROCTA = "//WDB_NROCTA";
  static final String mcteParam_WDB_CODBANCO = "//WDB_CODBANCO";
  static final String mcteParam_WDB_SUCCTA = "//WDB_SUCCTA";
  static final String mcteParam_WDB_FECHAVTOCTA_DIA = "//WDB_FECHAVTOCTA_DIA";
  static final String mcteParam_WDB_FECHAVTOCTA_MES = "//WDB_FECHAVTOCTA_MES";
  static final String mcteParam_WDB_FECHAVTOCTA_ANN = "//WDB_FECHAVTOCTA_ANN";
  static final String mcteParam_WDB_TELEFONO = "//WDB_TELEFONO";
  static final String mcteParam_WDB_EMAIL = "//WDB_EMAIL";
  static final String mcteParam_WDB_USUARIO_RED = "//WDB_USUARIO_RED";
  static final String mcteParam_WDB_ALARMA = "//WDB_ALARMA";
  /**
   *  Coberturas
   */
  static final String mcteParam_COT_NRO = "//COT_NRO";
  static final String mcteParam_WDB_COBERCOD = "//COBERCOD";
  static final String mcteParam_WDB_COBERORD = "//COBERORD";
  static final String mcteParam_WDB_CAPITASG = "//CAPITASG";
  static final String mcteParam_WDB_CAPITIMP = "//CAPITIMP";
  /**
   * Datos exigidos por la UIF
   */
  static final String mcteParam_WDB_OCUPACODCLIE = "//OCUPACODCLIE";
  static final String mcteParam_WDB_UIFCUIT = "//UIFCUIT";
  static final String mcteParam_WDB_SWAPODER = "//SWAPODER";
  /**
   * Green Products
   */
  static final String mcteParam_WDB_GREEN = "//WDB_GREEN";
  static final String mcteParam_WDB_GRANIZO = "//WDB_GRANIZO";
  static final String mcteParam_WDB_ROBOCONT = "//WDB_ROBOCONT";
  /**
   * AGENTCLA
   *  MMC 2012-11-29
   */
  static final String mcteParam_WDB_AGECLA = "//AGECLA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  DATOS DE CONDUCTORES ADICIONALES
   */
  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    Object wobjClass = null;
    String wvarResponse = "";
    String wvarRequest = "";
    String wvarSumAseg = "";
    String wvarSumaLBA = "";
    String wvarWDB_ACCESORIOS_FLG = "";
    String wvarWDB_PRENDA = "";
    String wvarWDB_CO_TITULAR = "";
    String wvarWDB_HIJOS1416_FLG = "";
    String wvarWDB_HIJOS1729_FLG = "";
    String wvarWDB_CONSEJO_FLG = "";
    String wvarWDB_REFERIDO_FLG = "";
    String wvarWDB_USUARIO = "";
    String wvarWDB_ESTADO = "";
    String wvarWDB_USRINI = "";
    String wvarWDB_USRSPV = "";
    String wvarWDB_FECCIECOT_DIA = "";
    String wvarWDB_FECCIECOT_MES = "";
    String wvarWDB_FECCIECOT_ANO = "";
    String wvarWDB_TELNRO = "";
    String wvarCAMPA_TEL = "";
    String wvarCAMPA_COD = "";
    String wvarAGE_COD = "";
    String wvarWDB_AC_AUACCCOD = "";
    String wvarWDB_AC_SUMA = "";
    String wvarWDB_AC_DESCRI = "";
    String wvarWDB_AC_DEPRE = "";
    String wvarWDB_AUMARCOD = "";
    String wvarWDB_AUMODCOD = "";
    String wvarWDB_AUSUBCOD = "";
    String wvarWDB_AUADICOD = "";
    String wvarWDB_AUMODORI = "";
    String wvarWDB_FABRICAN = "";
    String wvarWDB_AUTIPCOD = "";
    String wvarWDB_AUKLMNUM = "";
    String wvarWDB_SUMAASEG = "";
    String wvarWDB_SUMALBA = "";
    String wvarWDB_AUUSOCOD = "";
    String wvarWDB_AUTIPCOM = "";
    String wvarWDB_CHASISNUM = "";
    String wvarWDB_MOTORNUM = "";
    String wvarWDB_PATENNUM = "";
    String wvarWDB_ANOCOMP = "";
    String wvarWDB_GARAJE_FLG = "";
    String wvarWDB_GARAJE_PART = "";
    String wvarWDB_CEROKM_FLG = "";
    String wvarWDB_AUCATCOD = "";
    String wvarWDB_AUUSOGNC = "";
    String wvarWDB_COLOR = "";
    String wvarWDB_CLIENAP1 = "";
    String wvarWDB_CLIENAP2 = "";
    String wvarWDB_CLIENNOM = "";
    String wvarWDB_DOCUMTIP = "";
    String wvarWDB_DOCUMDAT = "";
    String wvarWDB_FECNTO_DD = "";
    String wvarWDB_FECNTO_MM = "";
    String wvarWDB_FECNTO_AAAA = "";
    String wvarWDB_EDADCLI = "";
    String wvarWDB_ESTCIVCLI = "";
    String wvarWDB_SEXOCLI = "";
    String wvarWDB_LEGAJO = "";
    String wvarWDB_IVASITCLI = "";
    String wvarWDB_PREFIJO = "";
    String wvarWDB_TELEFXXX = "";
    String wvarWDB_FECALTA_DIA = "";
    String wvarWDB_FECALTA_MES = "";
    String wvarWDB_FECALTA_ANO = "";
    String wvarWDB_ANOLICEN = "";
    String wvarWDB_APELLIDO_HIJO = "";
    String wvarWDB_NOMBRE_HIJO = "";
    String wvarWDB_SEXO_HIJO = "";
    String wvarWDB_NACIM_FEC_HIJO_DIA = "";
    String wvarWDB_NACIM_FEC_HIJO_MES = "";
    String wvarWDB_NACIM_FEC_HIJO_ANO = "";
    String wvarWDB_EDAD_HIJO = "";
    String wvarWDB_ESTCIV_HIJO = "";
    String wvarWDB_INCLUIDO_HIJO = "";
    String wvarWDB_CALLESEL = "";
    String wvarWDB_DOMICDNU = "";
    String wvarWDB_DOMICPIS = "";
    String wvarWDB_DOMICPTA = "";
    String wvarWDB_CODPOST_RGO = "";
    String wvarWDB_LOCALIDAD = "";
    String wvarWBD_CODPROV_RGO = "";
    String wvarWDB_DOMICDOMCOM = "";
    String wvarWDB_DOMICDNUCO = "";
    String wvarWDB_DOMICPISCO = "";
    String wvarWDB_DOMICPTACO = "";
    String wvarWDB_DOMICPOBCO = "";
    String wvarWDB_DOMICCPOCO = "";
    String wvarWDB_PROVICODCO = "";
    String wvarWDB_DOMICPRECO = "";
    String wvarWDB_DOMICTLFCO = "";
    String wvarWDB_CODINST = "";
    String wvarWDB_CODTRANS = "";
    String wvarWDB_RAMOPCOD = "";
    String wvarWDB_VIGENDIA = "";
    String wvarWDB_VIGENMES = "";
    String wvarWDB_VIGENANN = "";
    String wvarWDB_CODI_ZONA = "";
    String wvarWDB_PRECIO_MENSUAL = "";
    String wvarWDB_SINIESTROS = "";
    String wvarWDB_PLANCOD = "";
    String wvarWDB_FRANQCOD = "";
    String wvarWDB_CLUBLBA = "";
    String wvarWDB_LUNETA = "";
    String wvarWDB_EMISDIA = "";
    String wvarWDB_EMISMES = "";
    String wvarWDB_EMISANN = "";
    String wvarWDB_CAMPANA = "";
    String wvarWDB_PRODUCTOR = "";
    String wvarWDB_BANCOCOD = "";
    String wvarWDB_SUCURCOD = "";
    String wvarWDB_CODIGO_COBRO = "";
    String wvarWDB_TIPO_COBRO = "";
    String wvarWDB_VEHDES = "";
    String wvarWDB_NROCTA = "";
    String wvarWDB_CODBANCO = "";
    String wvarWDB_SUCCTA = "";
    String wvarWDB_FECHAVTOCTA_DIA = "";
    String wvarWDB_FECHAVTOCTA_MES = "";
    String wvarWDB_FECHAVTOCTA_ANN = "";
    String wvarWDB_TELEFONO = "";
    String wvarWDB_EMAIL = "";
    String wvarWDB_USUARIO_RED = "";
    String wvarWDB_ALARMA = "";
    String wvarCot_Nro = "";
    String wvarWDB_COBERCOD = "";
    String wvarWDB_COBERORD = "";
    String wvarWDB_CAPITASG = "";
    String wvarWDB_CAPITIMP = "";
    String wvarWDB_OCUPACODCLIE = "";
    String wvarWDB_UIFCUIT = "";
    String wvarWDB_SWAPODER = "";
    String wvarWDB_GREEN = "";
    String wvarWDB_GRANIZO = "";
    String wvarWDB_ROBOCONT = "";
    String wvarWDB_AGECLA = "";
    //
    //
    // DATOS GENERALES
    // CAMPANIA, PRODUCTOR Y TELEFONO
    // ACCESORIOS
    //
    // Datos de los hijos adicionales
    //
    // COBERTURAS
    //Datos exigidos por la UIF
    //Green Products
    //AGENTCLA
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // CARGO EL XML DE ENTRADA
      wvarStep = 5;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // VUELVO A COTIZAR PARA EL PLAN SELECCIONADO
      wvarStep = 20;
      //
      //Cotizamos con el mismo mensaje que la OV
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaw_GetCotizacion")
      //
      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      wobjClass = null;
      //
      // GUARDO LAS COBERTURAS, LAS SUMAS ASEGURADAS, CAMPANIA, PRODUCTOR Y TELEFONO
      wvarStep = 25;
      wobjXMLCoberturas = new diamondedge.util.XmlDom();
      //unsup wobjXMLCoberturas.async = false;
      wobjXMLCoberturas.loadXML( wvarResponse );
      //
      wvarCot_Nro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_COT_NRO ) */ );
      //
      wvarStep = 26;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMAASEG ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_SUMASEG ) */ ) );
      //
      wvarStep = 27;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMALBA ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_SUMALBA ) */ ) );
      //
      wvarStep = 28;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELNRO ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_CAMPA_TEL ) */ ) );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CAMPANA ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_CAMPA_COD ) */ ) );
      //
      wvarStep = 29;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRINI ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_AGE_COD ) */ ) );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRSPV ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_AGE_COD ) */ ) );
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRODUCTOR ) */, diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_AGE_COD ) */ ) );
      //
      wvarStep = 30;
      //
      // DATOS GENERALES
      wvarStep = 40;
      wvarWDB_ACCESORIOS_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ACCESORIOS_FLG ) */ );
      wvarWDB_PRENDA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRENDA ) */ );
      wvarWDB_CO_TITULAR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CO_TITULAR ) */ );
      wvarWDB_HIJOS1416_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_HIJOS1416_FLG ) */ );
      wvarWDB_HIJOS1729_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_HIJOS1729_FLG ) */ );

      wvarStep = 50;
      wvarWDB_CONSEJO_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CONSEJO_FLG ) */ );
      wvarWDB_REFERIDO_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_REFERIDO_FLG ) */ );
      wvarWDB_USUARIO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USUARIO ) */ );
      wvarWDB_ESTADO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ESTADO ) */ );
      wvarWDB_USRINI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRINI ) */ );

      wvarStep = 60;
      wvarWDB_USRSPV = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRSPV ) */ );
      wvarWDB_FECCIECOT_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_DIA ) */ );
      wvarWDB_FECCIECOT_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_MES ) */ );
      wvarWDB_FECCIECOT_ANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_ANO ) */ );
      wvarWDB_TELNRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELNRO ) */ );

      wvarStep = 70;
      wvarWDB_AUMARCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUMARCOD ) */ );
      wvarWDB_AUMODCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUMODCOD ) */ );
      wvarWDB_AUSUBCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUSUBCOD ) */ );
      wvarWDB_AUADICOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUADICOD ) */ );
      wvarWDB_AUMODORI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUMODORI ) */ );

      wvarStep = 80;
      wvarWDB_FABRICAN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FABRICAN ) */ );
      wvarWDB_AUTIPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUTIPCOD ) */ );
      wvarWDB_AUKLMNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUKLMNUM ) */ );
      wvarWDB_SUMAASEG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMAASEG ) */ );
      wvarWDB_SUMALBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMALBA ) */ );

      wvarStep = 90;
      wvarWDB_AUUSOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUUSOCOD ) */ );
      wvarWDB_AUTIPCOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUTIPCOM ) */ );
      wvarWDB_CHASISNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CHASISNUM ) */ );
      wvarWDB_MOTORNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_MOTORNUM ) */ );
      wvarWDB_PATENNUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PATENNUM ) */ );

      wvarStep = 100;
      wvarWDB_ANOCOMP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ANOCOMP ) */ );
      wvarWDB_GARAJE_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_GARAJE_FLG ) */ );
      wvarWDB_GARAJE_PART = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_GARAJE_PART ) */ );
      wvarWDB_CEROKM_FLG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CEROKM_FLG ) */ );
      wvarWDB_AUCATCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUCATCOD ) */ );
      wvarWDB_AUUSOGNC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUUSOGNC ) */ );
      wvarStep = 110;
      wvarWDB_COLOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_COLOR ) */ );

      wvarWDB_CLIENAP1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENAP1 ) */ );
      wvarWDB_CLIENAP2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENAP2 ) */ );
      wvarWDB_CLIENNOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENNOM ) */ );
      wvarWDB_DOCUMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOCUMTIP ) */ );

      wvarStep = 120;
      wvarWDB_DOCUMDAT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOCUMDAT ) */ );
      wvarWDB_FECNTO_DD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_DD ) */ );
      wvarWDB_FECNTO_MM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_MM ) */ );
      wvarWDB_FECNTO_AAAA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_AAAA ) */ );
      wvarWDB_EDADCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_EDADCLI ) */ );

      wvarStep = 130;
      wvarWDB_ESTCIVCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ESTCIVCLI ) */ );
      wvarWDB_SEXOCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SEXOCLI ) */ );
      wvarWDB_LEGAJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_LEGAJO ) */ );
      wvarWDB_IVASITCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_IVASITCLI ) */ );
      wvarWDB_PREFIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PREFIJO ) */ );

      wvarStep = 140;
      wvarWDB_TELEFXXX = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELEFXXX ) */ );
      wvarWDB_FECALTA_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECALTA_DIA ) */ );
      wvarWDB_FECALTA_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECALTA_MES ) */ );
      wvarWDB_FECALTA_ANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECALTA_ANO ) */ );
      wvarWDB_ANOLICEN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ANOLICEN ) */ );

      wvarStep = 150;
      wvarWDB_CALLESEL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CALLESEL ) */ );
      wvarWDB_DOMICDNU = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDNU ) */ );
      wvarWDB_DOMICPIS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPIS ) */ );
      wvarWDB_DOMICPTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPTA ) */ );
      wvarWDB_CODPOST_RGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODPOST_RGO ) */ );

      wvarStep = 160;
      wvarWDB_LOCALIDAD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_LOCALIDAD ) */ );
      wvarWBD_CODPROV_RGO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WBD_CODPROV_RGO ) */ );
      wvarWDB_DOMICDOMCOM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDOMCOM ) */ );
      wvarWDB_DOMICDNUCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDNUCO ) */ );
      wvarWDB_DOMICPISCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPISCO ) */ );

      wvarStep = 170;
      wvarWDB_DOMICPTACO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPTACO ) */ );
      wvarWDB_DOMICPOBCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPOBCO ) */ );
      wvarWDB_DOMICCPOCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICCPOCO ) */ );
      wvarWDB_PROVICODCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PROVICODCO ) */ );
      wvarWDB_DOMICPRECO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPRECO ) */ );

      wvarStep = 180;
      wvarWDB_DOMICTLFCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICTLFCO ) */ );
      wvarWDB_CODINST = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODINST ) */ );
      wvarWDB_CODTRANS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODTRANS ) */ );
      wvarWDB_RAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_RAMOPCOD ) */ );
      wvarWDB_VIGENDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENDIA ) */ );

      wvarStep = 190;
      wvarWDB_VIGENMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENMES ) */ );
      wvarWDB_VIGENANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENANN ) */ );
      wvarWDB_CODI_ZONA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODI_ZONA ) */ );
      wvarWDB_PRECIO_MENSUAL = Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRECIO_MENSUAL ) */ ), ",", "." );
      wvarWDB_SINIESTROS = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SINIESTROS ) */ );

      wvarStep = 200;
      wvarWDB_PLANCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PLANCOD ) */ );
      wvarWDB_FRANQCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FRANQCOD ) */ );
      wvarWDB_CLUBLBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLUBLBA ) */ );
      wvarWDB_LUNETA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_LUNETA ) */ );
      wvarWDB_EMISDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMISDIA ) */ );
      wvarWDB_EMISMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMISMES ) */ );

      wvarStep = 205;
      wvarWDB_GREEN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_GREEN ) */ );
      wvarWDB_GRANIZO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_GRANIZO ) */ );
      wvarWDB_ROBOCONT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ROBOCONT ) */ );

      wvarStep = 210;
      wvarWDB_EMISANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMISANN ) */ );
      wvarWDB_CAMPANA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CAMPANA ) */ );
      wvarWDB_PRODUCTOR = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRODUCTOR ) */ );
      wvarWDB_BANCOCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_BANCOCOD ) */ );
      wvarWDB_SUCURCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUCURCOD ) */ );
      //MMC 2012-11-29
      wvarWDB_AGECLA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_AGECLA ) */ );

      wvarStep = 220;
      wvarWDB_CODIGO_COBRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODIGO_COBRO ) */ );
      wvarWDB_TIPO_COBRO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_COBRO ) */ );
      wvarWDB_VEHDES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_VEHDES ) */ );
      wvarWDB_NROCTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_NROCTA ) */ );
      wvarWDB_CODBANCO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODBANCO ) */ );

      wvarStep = 230;
      wvarWDB_SUCCTA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUCCTA ) */ );
      wvarWDB_FECHAVTOCTA_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_DIA ) */ );
      wvarWDB_FECHAVTOCTA_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_MES ) */ );
      wvarWDB_FECHAVTOCTA_ANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_ANN ) */ );
      wvarWDB_TELEFONO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELEFONO ) */ );

      wvarStep = 240;
      wvarWDB_EMAIL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMAIL ) */ );
      wvarWDB_USUARIO_RED = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_USUARIO_RED ) */ );
      wvarWDB_ALARMA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_ALARMA ) */ );

      wvarStep = 241;
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_OCUPACODCLIE ) */ == (org.w3c.dom.Node) null) )
      {
        wvarWDB_OCUPACODCLIE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_OCUPACODCLIE ) */ );
      }
      else
      {
        wvarWDB_OCUPACODCLIE = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_UIFCUIT ) */ == (org.w3c.dom.Node) null) )
      {
        wvarWDB_UIFCUIT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_UIFCUIT ) */ );
      }
      else
      {
        wvarWDB_UIFCUIT = "";
      }
      //
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWAPODER ) */ == (org.w3c.dom.Node) null) )
      {
        wvarWDB_SWAPODER = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWAPODER ) */ );
      }
      else
      {
        wvarWDB_SWAPODER = "";
      }
      //
      wvarStep = 250;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 260;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 280;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 290;
      wobjDBParm = new Parameter( "@WDB_CDNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 300;
      wobjDBParm = new Parameter( "@WDB_ESLNRO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 310;
      wobjDBParm = new Parameter( "@WDB_CONTINUACION", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 320;
      wobjDBParm = new Parameter( "@WDB_ACCESORIOS_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_ACCESORIOS_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 330;
      wobjDBParm = new Parameter( "@WDB_PRENDA", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_PRENDA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 340;
      wobjDBParm = new Parameter( "@WDB_CO_TITULAR", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_CO_TITULAR ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 350;
      wobjDBParm = new Parameter( "@WDB_HIJOS1416_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_HIJOS1416_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 360;
      wobjDBParm = new Parameter( "@WDB_HIJOS1729_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_HIJOS1729_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 370;
      wobjDBParm = new Parameter( "@WDB_CONSEJO_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_CONSEJO_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 380;
      wobjDBParm = new Parameter( "@WDB_REFERIDO_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_REFERIDO_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 390;
      wobjDBParm = new Parameter( "@WDB_USUARIO", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarWDB_USUARIO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 400;
      wobjDBParm = new Parameter( "@WDB_HORAINI", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 410;
      wobjDBParm = new Parameter( "@WDB_HORAFIN", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 420;
      wobjDBParm = new Parameter( "@WDB_ESTADO", AdoConst.adChar, AdoConst.adParamInput, 8, new Variant( wvarWDB_ESTADO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 430;
      wobjDBParm = new Parameter( "@WDB_USRINI", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarWDB_USRINI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 450;
      wobjDBParm = new Parameter( "@WDB_USRSPV", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarWDB_USRSPV ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 460;
      wobjDBParm = new Parameter( "@WDB_FECCIECOT_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECCIECOT_DIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 470;
      wobjDBParm = new Parameter( "@WDB_FECCIECOT_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECCIECOT_MES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 480;
      wobjDBParm = new Parameter( "@WDB_FECCIECOT_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECCIECOT_ANO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 500;
      wobjDBParm = new Parameter( "@WDB_CTICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 510;
      wobjDBParm = new Parameter( "@WDB_TELNRO", AdoConst.adChar, AdoConst.adParamInput, 12, new Variant( wvarWDB_TELNRO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 520;
      wobjDBParm = new Parameter( "@WDB_DMS_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 530;
      wobjDBParm = new Parameter( "@WDB_DMS_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 540;
      wobjDBParm = new Parameter( "@WDB_DMS_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 550;
      wobjDBParm = new Parameter( "@WDB_DMS_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 560;
      wobjDBParm = new Parameter( "@WDB_DMS_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 570;
      wobjDBParm = new Parameter( "@WDB_DMS_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 580;
      wobjDBParm = new Parameter( "@WDB_FECENVIO_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 590;
      wobjDBParm = new Parameter( "@WDB_FECENVIO_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 592;
      wobjDBParm = new Parameter( "@WDB_FECENVIO_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 594;
      wobjDBParm = new Parameter( "@WDB_FECORIA_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 598;
      wobjDBParm = new Parameter( "@WDB_FECORIA_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 600;
      wobjDBParm = new Parameter( "@WDB_FECORIA_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 610;
      wobjDBParm = new Parameter( "@WDB_FECDST_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 620;
      wobjDBParm = new Parameter( "@WDB_FECDST_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 630;
      wobjDBParm = new Parameter( "@WDB_FECDST_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 640;
      wobjDBParm = new Parameter( "@WDB_FECDST_HORA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 650;
      wobjDBParm = new Parameter( "@WDB_FECDST_MINUTO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 660;
      wobjDBParm = new Parameter( "@WDB_AGENDA_RECH_OBS", AdoConst.adChar, AdoConst.adParamInput, 150, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 670;
      wobjDBParm = new Parameter( "@WDB_RCOCOD", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 680;
      wobjDBParm = new Parameter( "@WDB_FECORI_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 690;
      wobjDBParm = new Parameter( "@WDB_FECORI_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 700;
      wobjDBParm = new Parameter( "@WDB_FECORI_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // aca vienen los accesorios
      wvarStep = 710;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarWDB_AC_AUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_AC_AUACCCOD ) */ );
        wvarWDB_AC_SUMA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_AC_SUMA ) */ );
        wvarWDB_AC_DESCRI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_AC_DESCRI ) */ );
        wvarWDB_AC_DEPRE = "SI";

        wvarStep = 720;
        wobjDBParm = new Parameter( "@WDB_AC_AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AC_AUACCCOD ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 740;
        wobjDBParm = new Parameter( "@WDB_AC_SUMA" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AC_SUMA ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 750;
        wobjDBParm = new Parameter( "@WDB_AC_DESCRI" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarWDB_AC_DESCRI ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 760;
        wobjDBParm = new Parameter( "@WDB_AC_DEPRE" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_AC_DEPRE ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

      }

      wvarStep = 780;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 790;
        wobjDBParm = new Parameter( "@WDB_AC_AUACCCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 840;
        wobjDBParm = new Parameter( "@WDB_AC_SUMA" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 850;
        wobjDBParm = new Parameter( "@WDB_AC_DESCRI" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 860;
        wobjDBParm = new Parameter( "@WDB_AC_DEPRE" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;

      // hasta aca accesorios
      wvarStep = 870;
      wobjDBParm = new Parameter( "@WDB_APEACRE", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 880;
      wobjDBParm = new Parameter( "@WDB_NOMACRE", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 890;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIPACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 900;
      wobjDBParm = new Parameter( "@WDB_DOCUMDATACARE", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 910;
      wobjDBParm = new Parameter( "@WDB_DOMICDOMACRE", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 920;
      wobjDBParm = new Parameter( "@WDB_DOMICNUACRE", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 930;
      wobjDBParm = new Parameter( "@WDB_DOMICPISACRE", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 940;
      wobjDBParm = new Parameter( "@WDB_DOMICPTAACRE", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 950;
      wobjDBParm = new Parameter( "@WDB_DOMICCPOACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 960;
      wobjDBParm = new Parameter( "@WDB_DOMICPOBACRE", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 970;
      wobjDBParm = new Parameter( "@WDB_PROVICODACRE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //   cotitulares
      for( wvarCounter = 1; wvarCounter <= 10; wvarCounter++ )
      {
        wvarStep = 980;
        wobjDBParm = new Parameter( "@WDB_DOCUMDATCOTIT" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 990;
        wobjDBParm = new Parameter( "@WDB_APENOMCOTIT" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      //   hasta aca cotitulares
      wvarStep = 1000;
      wobjDBParm = new Parameter( "@WDB_AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUMARCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1010;
      wobjDBParm = new Parameter( "@WDB_AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUMODCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1020;
      wobjDBParm = new Parameter( "@WDB_AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUSUBCOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1030;
      wobjDBParm = new Parameter( "@WDB_AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUADICOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1040;
      wobjDBParm = new Parameter( "@WDB_AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_AUMODORI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1050;
      wobjDBParm = new Parameter( "@WDB_FABRICAN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FABRICAN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1060;
      wobjDBParm = new Parameter( "@WDB_AUTIPCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUTIPCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1070;
      wobjDBParm = new Parameter( "@WDB_AUKLMNUM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUKLMNUM ) );
      wobjDBParm.setPrecision( (byte)( 7 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1080;
      wobjDBParm = new Parameter( "@WDB_SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toDouble( wvarWDB_SUMAASEG ) / 100 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1090;
      wobjDBParm = new Parameter( "@WDB_SUMALBA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toDouble( wvarWDB_SUMALBA ) / 100 ) );
      wobjDBParm.setPrecision( (byte)( 15 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1100;
      wobjDBParm = new Parameter( "@WDB_AUUSOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_AUUSOCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1110;
      wobjDBParm = new Parameter( "@WDB_AUTIPCOM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_AUTIPCOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1120;
      wobjDBParm = new Parameter( "@WDB_CHASISNUM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarWDB_CHASISNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1130;
      wobjDBParm = new Parameter( "@WDB_MOTORNUM", AdoConst.adChar, AdoConst.adParamInput, 25, new Variant( wvarWDB_MOTORNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1140;
      wobjDBParm = new Parameter( "@WDB_PATENNUM", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarWDB_PATENNUM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1150;
      wobjDBParm = new Parameter( "@WDB_ANOCOMP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_ANOCOMP ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1160;
      wobjDBParm = new Parameter( "@WDB_GARAJE_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_GARAJE_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1170;
      wobjDBParm = new Parameter( "@WDB_GARAJE_PART", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_GARAJE_PART ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1180;
      wobjDBParm = new Parameter( "@WDB_CEROKM_FLG", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_CEROKM_FLG ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@WDB_AUCATCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_AUCATCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1190;
      wobjDBParm = new Parameter( "@WDB_AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_AUUSOGNC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1200;
      wobjDBParm = new Parameter( "@WDB_COLOR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_COLOR ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 1210;
      wobjDBParm = new Parameter( "@WDB_CLIENSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 9 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1220;
      wobjDBParm = new Parameter( "@WDB_CLIENAP1", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarWDB_CLIENAP1 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1230;
      wobjDBParm = new Parameter( "@WDB_CLIENAP2", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarWDB_CLIENAP2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1240;
      wobjDBParm = new Parameter( "@WDB_CLIENNOM", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarWDB_CLIENNOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1250;
      wobjDBParm = new Parameter( "@WDB_DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_DOCUMTIP ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1260;
      wobjDBParm = new Parameter( "@WDB_DOCUMDAT", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarWDB_DOCUMDAT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1270;
      wobjDBParm = new Parameter( "@WDB_FECNTO_DD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECNTO_DD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1280;
      wobjDBParm = new Parameter( "@WDB_FECNTO_MM", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECNTO_MM ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1290;
      wobjDBParm = new Parameter( "@WDB_FECNTO_AAAA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECNTO_AAAA ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1300;
      wobjDBParm = new Parameter( "@WDB_EDADCLI", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_EDADCLI ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1310;
      wobjDBParm = new Parameter( "@WDB_ESTCIVCLI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_ESTCIVCLI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1320;
      wobjDBParm = new Parameter( "@WDB_SEXOCLI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_SEXOCLI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1330;
      wobjDBParm = new Parameter( "@WDB_LEGAJO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_LEGAJO ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1340;
      wobjDBParm = new Parameter( "@WDB_IVASITCLI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_IVASITCLI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1350;
      wobjDBParm = new Parameter( "@WDB_PREFIJO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarWDB_PREFIJO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 1360;
      wobjDBParm = new Parameter( "@WDB_TELEFONO", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarWDB_TELEFXXX ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1370;
      wobjDBParm = new Parameter( "@WDB_FECALTA_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECALTA_DIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1380;
      wobjDBParm = new Parameter( "@WDB_FECALTA_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECALTA_MES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1390;
      wobjDBParm = new Parameter( "@WDB_FECALTA_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECALTA_ANO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1400;
      wobjDBParm = new Parameter( "@WDB_USUARIO_ALTA", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1410;
      wobjDBParm = new Parameter( "@WDB_PROFCOD", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 1420;
      wobjDBParm = new Parameter( "@WDB_ANOLICEN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_ANOLICEN ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // aca vienen los hijos
      // DATOS DE CONDUCTORES ADICIONALES
      wvarStep = 1430;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_HIJOS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarWDB_APELLIDO_HIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_APELLIDO_HIJO ) */ );
        wvarWDB_NOMBRE_HIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_NOMBRE_HIJO ) */ );
        wvarWDB_SEXO_HIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_SEXO_HIJO ) */ );
        wvarWDB_NACIM_FEC_HIJO_DIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_NACIM_FEC_HIJO_DIA ) */ );
        wvarWDB_NACIM_FEC_HIJO_MES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_NACIM_FEC_HIJO_MES ) */ );
        wvarWDB_NACIM_FEC_HIJO_ANO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_NACIM_FEC_HIJO_ANO ) */ );
        wvarWDB_EDAD_HIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_EDAD_HIJO ) */ );
        wvarWDB_ESTCIV_HIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_ESTCIV_HIJO ) */ );
        wvarWDB_INCLUIDO_HIJO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_WDB_INCLUIDO_HIJO ) */ );

        wvarStep = 1440;
        wobjDBParm = new Parameter( "@WDB_APELLIDO_HIJO" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarWDB_APELLIDO_HIJO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1450;
        wobjDBParm = new Parameter( "@WDB_NOMBRE_HIJO" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarWDB_NOMBRE_HIJO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1460;
        wobjDBParm = new Parameter( "@WDB_SEXO_HIJO" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_SEXO_HIJO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1470;
        wobjDBParm = new Parameter( "@WDB_NACIM_FEC_HIJO_DIA" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_NACIM_FEC_HIJO_DIA ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1480;
        wobjDBParm = new Parameter( "@WDB_NACIM_FEC_HIJO_MES" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_NACIM_FEC_HIJO_MES ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1490;
        wobjDBParm = new Parameter( "@WDB_NACIM_FEC_HIJO_ANO" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_NACIM_FEC_HIJO_ANO ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1500;
        wobjDBParm = new Parameter( "@WDB_EDAD_HIJO" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_EDAD_HIJO ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1510;
        wobjDBParm = new Parameter( "@WDB_ESTCIV_HIJO" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_ESTCIV_HIJO ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1520;
        wobjDBParm = new Parameter( "@WDB_INCLUIDO_HIJO" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_INCLUIDO_HIJO ) );
        wobjDBParm.setPrecision( (byte)( 1 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 1530;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {
        wvarStep = 1540;
        wobjDBParm = new Parameter( "@WDB_APELLIDO_HIJO" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1550;
        wobjDBParm = new Parameter( "@WDB_NOMBRE_HIJO" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1560;
        wobjDBParm = new Parameter( "@WDB_SEXO_HIJO" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1570;
        wobjDBParm = new Parameter( "@WDB_NACIM_FEC_HIJO_DIA" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1580;
        wobjDBParm = new Parameter( "@WDB_NACIM_FEC_HIJO_MES" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1590;
        wobjDBParm = new Parameter( "@WDB_NACIM_FEC_HIJO_ANO" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1600;
        wobjDBParm = new Parameter( "@WDB_EDAD_HIJO" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1610;
        wobjDBParm = new Parameter( "@WDB_ESTCIV_HIJO" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 1620;
        wobjDBParm = new Parameter( "@WDB_INCLUIDO_HIJO" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
        wobjDBParm.setPrecision( (byte)( 1 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      //   hasta aca hijitos
      wvarStep = 1630;
      wobjDBParm = new Parameter( "@WDB_CPCFOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1640;
      wobjDBParm = new Parameter( "@WDB_CPCTMO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1650;
      wobjDBParm = new Parameter( "@WDB_CPCMATTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 1 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1660;
      wobjDBParm = new Parameter( "@WDB_CALLESEL", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( wvarWDB_CALLESEL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1670;
      wobjDBParm = new Parameter( "@WDB_DOMICDNU", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarWDB_DOMICDNU ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1680;
      wobjDBParm = new Parameter( "@WDB_DOMICPIS", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_DOMICPIS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1690;
      wobjDBParm = new Parameter( "@WDB_DOMICPTA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_DOMICPTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1700;
      wobjDBParm = new Parameter( "@WDB_CODPOST_RGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CODPOST_RGO ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1750;
      wobjDBParm = new Parameter( "@WDB_LOCALIDAD", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( wvarWDB_LOCALIDAD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1760;
      wobjDBParm = new Parameter( "@WBD_CODPROV_RGO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWBD_CODPROV_RGO ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1770;
      wobjDBParm = new Parameter( "@WDB_DOMICDOMCOM", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( wvarWDB_DOMICDOMCOM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1780;
      wobjDBParm = new Parameter( "@WDB_DOMICDNUCO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarWDB_DOMICDNUCO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1790;
      wobjDBParm = new Parameter( "@WDB_DOMICPISCO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_DOMICPISCO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1800;
      wobjDBParm = new Parameter( "@WDB_DOMICPTACO", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_DOMICPTACO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1820;
      wobjDBParm = new Parameter( "@WDB_DOMICPOBCO", AdoConst.adChar, AdoConst.adParamInput, 40, new Variant( wvarWDB_DOMICPOBCO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1830;
      wobjDBParm = new Parameter( "@WDB_DOMICCPOCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_DOMICCPOCO ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1840;
      wobjDBParm = new Parameter( "@WDB_PROVICODCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_PROVICODCO ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1850;
      wobjDBParm = new Parameter( "@WDB_DOMICPRECO", AdoConst.adChar, AdoConst.adParamInput, 5, new Variant( wvarWDB_DOMICPRECO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1860;
      wobjDBParm = new Parameter( "@WDB_DOMICTLFCO", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarWDB_DOMICTLFCO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1870;
      wobjDBParm = new Parameter( "@WDB_CODINST", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CODINST ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1900;
      wobjDBParm = new Parameter( "@WDB_MAQUINA", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1910;
      wobjDBParm = new Parameter( "@WDB_CODTRANS", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_CODTRANS ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1920;
      wobjDBParm = new Parameter( "@WDB_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_RAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1930;
      wobjDBParm = new Parameter( "@WDB_VIGENDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_VIGENDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1940;
      wobjDBParm = new Parameter( "@WDB_VIGENMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_VIGENMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1950;
      wobjDBParm = new Parameter( "@WDB_VIGENANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_VIGENANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2960;
      wobjDBParm = new Parameter( "@WDB_EFECTDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2970;
      wobjDBParm = new Parameter( "@WDB_EFECTMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2980;
      wobjDBParm = new Parameter( "@WDB_EFECTANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 1990;
      wobjDBParm = new Parameter( "@WDB_CODI_ZONA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CODI_ZONA ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2000;
      wobjDBParm = new Parameter( "@WDB_CODMONE", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2010;
      wobjDBParm = new Parameter( "@WDB_PRECIO_MENSUAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_PRECIO_MENSUAL ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2020;
      wobjDBParm = new Parameter( "@WDB_PRECIO_PROPORCIONAL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2030;
      wobjDBParm = new Parameter( "@WDB_BONIF_MEMBRECIA_POR", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2040;
      wobjDBParm = new Parameter( "@WDB_CUOTA_MENBRECIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2050;
      wobjDBParm = new Parameter( "@WDB_FECVENCI_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2060;
      wobjDBParm = new Parameter( "@WDB_FECVENCI_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2070;
      wobjDBParm = new Parameter( "@WDB_FECVENCI_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2080;
      wobjDBParm = new Parameter( "@WDB_CIASEGCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2090;
      wobjDBParm = new Parameter( "@WDB_PRECIO_SEGANT", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 13 ) );
      wobjDBParm.setScale( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2100;
      wobjDBParm = new Parameter( "@WDB_SINIESTROS", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_SINIESTROS ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2110;
      wobjDBParm = new Parameter( "@WDB_PLANCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_PLANCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2120;
      wobjDBParm = new Parameter( "@WDB_FRANQCOD", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_FRANQCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2125;
      wobjDBParm = new Parameter( "@WDB_CLUBLBA", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_CLUBLBA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2126;
      wobjDBParm = new Parameter( "@WDB_LUNETA", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_LUNETA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2130;
      wobjDBParm = new Parameter( "@WDB_EMISDIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_EMISDIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2140;
      wobjDBParm = new Parameter( "@WDB_EMISMES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_EMISMES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2150;
      wobjDBParm = new Parameter( "@WDB_EMISANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_EMISANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 2190;
      wobjDBParm = new Parameter( "@WDB_CAMPANA", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_CAMPANA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2200;
      wobjDBParm = new Parameter( "@WDB_PRODUCTOR", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarWDB_PRODUCTOR ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2210;
      wobjDBParm = new Parameter( "@WDB_BANCOCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_BANCOCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2220;
      wobjDBParm = new Parameter( "@WDB_SUCURCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_SUCURCOD ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2260;
      wobjDBParm = new Parameter( "@WDB_VENDEDOR", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2270;
      wobjDBParm = new Parameter( "@WDB_NROLEGGTE", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2280;
      wobjDBParm = new Parameter( "@WDB_NROCOTIZACION", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCot_Nro ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2290;
      wobjDBParm = new Parameter( "@WDB_CODIGO_COBRO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CODIGO_COBRO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2310;
      wobjDBParm = new Parameter( "@WDB_TIPO_COBRO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_TIPO_COBRO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2320;
      wobjDBParm = new Parameter( "@WDB_INSPECCION_FLG", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2330;
      wobjDBParm = new Parameter( "@WDB_CENTROCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2340;
      wobjDBParm = new Parameter( "@WDB_REF_CIAASCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2350;
      wobjDBParm = new Parameter( "@WDB_REF_RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2360;
      wobjDBParm = new Parameter( "@WDB_REF_POLIZANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2370;
      wobjDBParm = new Parameter( "@WDB_REF_POLIZSEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2380;
      wobjDBParm = new Parameter( "@WDB_REF_CERTIPOL", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2390;
      wobjDBParm = new Parameter( "@WDB_REF_CERTIANN", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2400;
      wobjDBParm = new Parameter( "@WDB_REF_CERTISEC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 6 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 2410;
      wobjDBParm = new Parameter( "@WDB_VEHDES", AdoConst.adChar, AdoConst.adParamInput, 35, new Variant( wvarWDB_VEHDES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2420;
      wobjDBParm = new Parameter( "@WDB_NROCTA", AdoConst.adChar, AdoConst.adParamInput, 20, new Variant( wvarWDB_NROCTA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2430;
      wobjDBParm = new Parameter( "@WDB_CODBANCO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CODBANCO ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2440;
      wobjDBParm = new Parameter( "@WDB_SUCCTA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_SUCCTA ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2450;
      wobjDBParm = new Parameter( "@WDB_FECHAVTOCTA_DIA", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECHAVTOCTA_DIA ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2460;
      wobjDBParm = new Parameter( "@WDB_FECHAVTOCTA_MES", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECHAVTOCTA_MES ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2470;
      wobjDBParm = new Parameter( "@WDB_FECHAVTOCTA_ANO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_FECHAVTOCTA_ANN ) );
      wobjDBParm.setPrecision( (byte)( 4 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2480;
      wobjDBParm = new Parameter( "@WDB_TELEFONO1", AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarWDB_TELEFONO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2490;
      // COBERTURAS
      for( wvarCounter = 1; wvarCounter <= 30; wvarCounter++ )
      {
        wvarWDB_COBERCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_COBERCOD + wvarCounter ) */ );
        wvarWDB_COBERORD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_COBERORD + wvarCounter ) */ );
        wvarWDB_CAPITASG = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_CAPITASG + wvarCounter ) */ );
        wvarWDB_CAPITIMP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_CAPITIMP + wvarCounter ) */ );

        wvarStep = 2500;
        wobjDBParm = new Parameter( "@WDB_COBERCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_COBERCOD ) );
        wobjDBParm.setPrecision( (byte)( 3 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 2510;
        wobjDBParm = new Parameter( "@WDB_COBERORD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_COBERORD ) );
        wobjDBParm.setPrecision( (byte)( 2 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 2520;
        wobjDBParm = new Parameter( "@WDB_CAPITASG" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CAPITASG ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wvarStep = 2530;
        wobjDBParm = new Parameter( "@WDB_CAPITIMP" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarWDB_CAPITIMP ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 2540;
      wobjDBParm = new Parameter( "@WDB_EMAIL", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( wvarWDB_EMAIL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2550;
      wobjDBParm = new Parameter( "@WDB_USUARIO_RED", AdoConst.adChar, AdoConst.adParamInput, 15, new Variant( wvarWDB_USUARIO_RED ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2560;
      wobjDBParm = new Parameter( "@WDB_ALARMA", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_ALARMA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2565;
      wobjDBParm = new Parameter( "@WDB_INTERNO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2566;
      wobjDBParm = new Parameter( "@WDB_INSPECC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2567;
      wobjDBParm = new Parameter( "@WDB_RASTREO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2568;
      wobjDBParm = new Parameter( "@WDB_ALARMCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( "0" ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2570;
      wobjDBParm = new Parameter( "@WDB_ERRORMSG", AdoConst.adChar, AdoConst.adParamOutput, 70, new Variant( " " ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //11/12/2006 - DA: Datos exigidos por la UIF
      wvarStep = 2571;
      wobjDBParm = new Parameter( "@WDB_OCUPACODCLIE", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( wvarWDB_OCUPACODCLIE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2572;
      wobjDBParm = new Parameter( "@WDB_UIFCUIT", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarWDB_UIFCUIT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2573;
      wobjDBParm = new Parameter( "@WDB_SWAPODER", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarWDB_SWAPODER ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //FIN: Datos exigidos por la UIF
      //Agregado por Green Products
      wvarStep = 2574;
      wobjDBParm = new Parameter( "@WDB_GREEN", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_GREEN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2575;
      wobjDBParm = new Parameter( "@WDB_GRANIZO", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_GRANIZO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 2576;
      wobjDBParm = new Parameter( "@WDB_ROBOCONT", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_ROBOCONT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //-- MMC 2012-11-29
      wobjDBParm = new Parameter( "@WDB_PRODUCTIP", AdoConst.adChar, AdoConst.adParamInput, 2, new Variant( wvarWDB_AGECLA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //-- --
      //FIN: Green Products
      wvarStep = 2580;
      //unsup wobjDBCmd.NamedParameters = true;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 2590;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><COT_NRO>" + wvarCot_Nro + "</COT_NRO></Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -10 )
        {
          wvarMensaje = "ERROR AL OBTENER CADENA-ESLABON";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -11 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA REL_CENTRAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -12 )
        {
          wvarMensaje = "ERROR AL ACTUALIZAR LA MARCA DE ULTIMO REGISTR EN LA TABLA REL_CENTRAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -13 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA ADM_AGENDA";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -14 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA ADM_RECAHZO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -15 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -16 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -17 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -18 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -19 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -20 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -21 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -22 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -23 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -24 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -25 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_ACREEDOR";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -26 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 1";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -27 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 2";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -28 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 3";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -29 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 4";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -30 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 5";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -31 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 6";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -32 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 7";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -33 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 8";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -34 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 9";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -35 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 10";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -36 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_AUTO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -37 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CLIENTE";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -38 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CONDUCTORES";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -39 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CONSEJO_PROFESIONAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -40 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_DOMICILIOS";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -41 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_GENERAL";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -42 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_INSPECCION";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -43 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_REFERIDO";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -44 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CUENTA";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -45 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS";
        }
        wvarStep = 2600;
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      wvarStep = 2610;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 2620;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
