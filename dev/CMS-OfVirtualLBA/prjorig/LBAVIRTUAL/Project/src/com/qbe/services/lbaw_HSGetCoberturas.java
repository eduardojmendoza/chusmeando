package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_HSGetCoberturas implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_HSGetCoberturas";
  static final String mcteStoreProc = "SPSNCV_SELECT_COBERTURAS_HOGAR";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    String wvarPolizSec = "";
    String wvarPolizAnn = "";
    String wvarRAMOPCOD = "";
    String wvarPLANNCOD = "";
    int wvarStep = 0;
    String wvarResult = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
      wvarStep = 4;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteRAMOPCOD ) */ );
      wvarPLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePLANNCOD ) */ );
      wvarPolizSec = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZSEC ) */ );
      wvarPolizAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZANN ) */ );
      //
      wvarStep = 8;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 10;
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //
      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      //
      wvarStep = 40;
      wobjDBCmd = new Command();
      //
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adVarWChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@POLIZANN", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarPolizAnn ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@POLIZSEC", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarPolizSec ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@PLANNCOD", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarPLANNCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 60;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 70;
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 80;
        /*unsup wrstDBResult.Save( wobjXMLResponse.toString(), AdoConst.adUnsupported ) */;
        //
        wvarStep = 90;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 100;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 120;
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        wvarStep = 130;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 140;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      wvarStep = 150;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 160;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 170;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 180;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 190;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 200;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        if( ! (wrstDBResult == (Recordset) null) )
        {
          if( 0 /*unsup wrstDBResult.State */ == 0/*unsup adStateOpen*/ )
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          if( 0 /*unsup wobjDBCnn.State */ == 0/*unsup adStateOpen*/ )
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "<xsl:key name='coberturas' match='z:row' use='@COBEBCOD' />";

    wvarStrXSL = wvarStrXSL + " <xsl:template match='/'>";

    wvarStrXSL = wvarStrXSL + "     <xsl:for-each select=\"//z:row[@COBEBCOD = '0']\">";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='ROW'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERCOD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"@COBERCOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERDAB'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"@COBERDAB\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAMINMOD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"@CAMINMOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAMAXMOD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"@CAMAXMOD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAPITASG'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"@CAPITASG\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CAPITASGDSP'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"format-number(number(@CAPITASG), '###.###,00', 'european')\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='COBERORD'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select=\"@COBERORD\"/>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";


    wvarStrXSL = wvarStrXSL + "             <xsl:element name='SECUNDARIAS'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:for-each select=\"key('coberturas', @COBERCOD)\">";

    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='SECUNDARIA'>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:element name='COBERCOD'>";
    wvarStrXSL = wvarStrXSL + "                             <xsl:value-of select=\"@COBERCOD\"/>";
    wvarStrXSL = wvarStrXSL + "                         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:element name='COBERDAB'>";
    wvarStrXSL = wvarStrXSL + "                             <xsl:value-of select=\"@COBERDAB\"/>";
    wvarStrXSL = wvarStrXSL + "                         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:element name='CAPITASG'>";
    wvarStrXSL = wvarStrXSL + "                             <xsl:value-of select=\"@CAPITASG\"/>";
    wvarStrXSL = wvarStrXSL + "                         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:element name='CAPITASGDSP'>";
    wvarStrXSL = wvarStrXSL + "                             <xsl:value-of select=\"format-number(number(@CAPITASG), '###.###,00', 'european')\"/>";
    wvarStrXSL = wvarStrXSL + "                         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:element name='COBERORD'>";
    wvarStrXSL = wvarStrXSL + "                             <xsl:value-of select=\"@COBERORD\"/>";
    wvarStrXSL = wvarStrXSL + "                         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";

    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:for-each>";

    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
