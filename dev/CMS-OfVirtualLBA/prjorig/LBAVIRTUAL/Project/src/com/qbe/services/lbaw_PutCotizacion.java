package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutCotizacion implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_PutCotizacion";
  static final String mcteStoreProc = "SPSNCV_ADM_INTERNET_COTI_INSERT";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_FECHACOTI = "//FECHACOTI";
  static final String mcteParam_NACIMDIA = "//NACIMDIA";
  static final String mcteParam_NACIMMES = "//NACIMMES";
  static final String mcteParam_NACIMANN = "//NACIMANN";
  static final String mcteParam_ESTADO_OP = "//ESTADO_OP";
  static final String mcteParam_ESTADO = "//ESTADO";
  static final String mcteParam_LOCALIDAD = "//LOCALIDAD";
  static final String mcteParam_LOCALIDADCOD = "//LOCALIDADCOD";
  static final String mcteParam_SEXO = "//SEXO";
  static final String mcteParam_PROVI = "//PROVI";
  static final String mcteParam_PORTAL = "//PORTAL";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  /**
   *  DATOS DEL VEHICULO
   */
  static final String mcteParam_MARCAAUT = "//MARCAAUT";
  static final String mcteParam_MODEAUT = "//MODEAUT";
  static final String mcteParam_EFECTANN = "//EFECTANN";
  static final String mcteParam_KMSRNGCOD = "//KMSRNGCOD";
  static final String mcteParam_SINIESTROS = "//SINIESTROS";
  static final String mcteParam_RBOKM = "//RBOKM";
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_HIJOS1729 = "//HIJOS1729";
  static final String mcteParam_CONDADIC = "//CONDADIC";
  static final String mcteParam_MODEAUTCOD = "//MODEAUTCOD";
  static final String mcteParam_GAS = "//GAS";
  /**
   *  DATOS DE CONDUCTORES ADICIONALES
   */
  static final String mcteParam_HIJOS = "//HIJOS/HIJO";
  static final String mcteParam_EDADHIJO = "EDADHIJO";
  static final String mcteParam_ESTADOHIJO = "ESTADOHIJO";
  static final String mcteParam_SEXOHIJO = "SEXOHIJO";
  /**
   *  --- CAMPOS AGREGADOS 03-12-2005. ---
   *  Modificación Realizada: 12-12-2005. Fernando Osores
   */
  static final String mcteParam_TELEFONO = "//TELEFONO";
  static final String mcteParam_NOMBRE = "//NOMBRE";
  static final String mcteParam_APELLIDO = "//APELLIDO";
  static final String mcteParam_GARAGE = "//GARAGE";
  static final String mcteParam_POSEEACC = "//POSEEACC";
  static final String mcteParam_DOCUMTIP = "//DOCUMTIP";
  static final String mcteParam_DOCUMDAT = "//DOCUMDAT";
  static final String mcteParam_EMAILP = "//EMAILP";
  static final String mcteParam_EMAILL = "//EMAILL";
  static final String mcteParam_CLIENTE = "//CLIENTE";
  static final String mcteParam_CONFORM = "//CONFORM";
  static final String mcteParam_CAMCMLCOD = "//CAMCMLCOD";
  static final String mcteParam_TIPOCLI = "//TIPOCLI";
  /**
   *  DATOS DE ACCESORIOS ADICIONALES
   */
  static final String mcteParam_AU = "//ACCESORIOS/ACCESORIO";
  static final String mcteParam_AUACCCOD = "CODIGOACC";
  static final String mcteParam_AUVEASUM = "PRECIOACC";
  static final String mcteParam_AUVEADES = "DESCRIPCIONACC";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  --- FIN MODIFICACION 12-12-2005 ---
   */
  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarFechaCoti = "";
    String wvarNACIMDIA = "";
    String wvarNACIMMES = "";
    String wvarNACIMANN = "";
    String wvarEstado_OP = "";
    String wvarEstado = "";
    String wvarLOCALIDAD = "";
    String wvarLocalidadCod = "";
    String wvarSexo = "";
    String wvarProvi = "";
    String wvarPORTAL = "";
    String wvarRAMOPCOD = "";
    String wvarMarcaAut = "";
    String wvarModeAut = "";
    String wvarEFECTANN = "";
    String wvarKmsRngCod = "";
    String wvarSiniestros = "";
    String wvarRbOkm = "";
    String wvarSumAseg = "";
    String wvarHijos1729 = "";
    String wvarCondAdic = "";
    String wvarMarcaAutCod = "";
    String wvarModeAutCod = "";
    String wvarAuSubCod = "";
    String wvarAuAdiCod = "";
    String wvarAuModOri = "";
    String wvarGas = "";
    String wvarHijos = "";
    String wvarEdadHijo = "";
    String wvarEstadoHijo = "";
    String wvarSexoHijo = "";
    String wvarTELEFONO = "";
    String wvarNOMBRE = "";
    String wvarAPELLIDO = "";
    String wvarAPELLIDO_NOMBRE = "";
    String wvarGARAGE = "";
    String wvarPOSEEACC = "";
    String wvarDOCUMTIP = "";
    String wvarDOCUMDAT = "";
    String wvarEMAILP = "";
    String wvarEMAILL = "";
    String wvarCLIENTE = "";
    String wvarCONFORM = "";
    String wvarCAMCMLCOD = "";
    String wvarAUACCCOD = "";
    String wvarAUVEASUM = "";
    String wvarAUVEADES = "";
    String wvarTIPOCLI = "";
    //
    //
    // DATOS GENERALES
    //
    // DATOS DEL VEHICULO
    //
    // DATOS DE CONDUCTORES ADICIONALES
    //
    // --- CAMPOS AGREGADOS 03-12-2005. ---
    // Modificación Realizada: 12-12-2005. Fernando Osores
    // --- FIN MODIFICACION 12-12-2005 ---
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      // DATOS GENERALES
      wvarFechaCoti = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHACOTI ) */ );
      wvarNACIMDIA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NACIMDIA ) */ );
      wvarNACIMMES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NACIMMES ) */ );
      wvarNACIMANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NACIMANN ) */ );
      wvarEstado_OP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTADO_OP ) */ );
      wvarEstado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTADO ) */ );
      wvarLOCALIDAD = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LOCALIDAD ) */ ), 50 );
      wvarLocalidadCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LOCALIDADCOD ) */ );
      wvarSexo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SEXO ) */ );
      wvarProvi = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVI ) */ );
      wvarPORTAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PORTAL ) */ );
      wvarRAMOPCOD = "AUS1";
      // DATOS DEL VEHICULO
      wvarMarcaAut = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MARCAAUT ) */ );
      wvarModeAut = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MODEAUT ) */ );
      wvarEFECTANN = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EFECTANN ) */ );
      wvarKmsRngCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_KMSRNGCOD ) */ );
      wvarSiniestros = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SINIESTROS ) */ );
      wvarRbOkm = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RBOKM ) */ );
      wvarSumAseg = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUMASEG ) */ );
      wvarHijos1729 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_HIJOS1729 ) */ );
      wvarCondAdic = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CONDADIC ) */ );
      wvarMarcaAutCod = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MODEAUTCOD ) */ ), 5 );
      wvarModeAutCod = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MODEAUTCOD ) */ ), 6, 5 );
      wvarAuSubCod = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MODEAUTCOD ) */ ), 11, 5 );
      wvarAuAdiCod = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MODEAUTCOD ) */ ), 16, 5 );
      wvarGas = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GAS ) */ );
      //
      // --- CAMPOS AGREGADOS 03-12-2005. ---
      // Modificación Realizada: 12-12-2005. Fernando Osores
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRE ) */ == (org.w3c.dom.Node) null) )
      {
        wvarNOMBRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NOMBRE ) */ );
      }
      else
      {
        wvarNOMBRE = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO ) */ == (org.w3c.dom.Node) null) )
      {
        wvarAPELLIDO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO ) */ );
      }
      else
      {
        wvarAPELLIDO = "";
      }
      wvarAPELLIDO_NOMBRE = wvarAPELLIDO + ", " + wvarNOMBRE;
      if( Strings.len( wvarAPELLIDO_NOMBRE ) > 50 )
      {
        wvarAPELLIDO_NOMBRE = Strings.mid( wvarAPELLIDO_NOMBRE, 1, 50 );
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELEFONO ) */ == (org.w3c.dom.Node) null) )
      {
        wvarTELEFONO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELEFONO ) */ );
      }
      else
      {
        wvarTELEFONO = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GARAGE ) */ == (org.w3c.dom.Node) null) )
      {
        wvarGARAGE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GARAGE ) */ );
      }
      else
      {
        wvarGARAGE = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POSEEACC ) */ == (org.w3c.dom.Node) null) )
      {
        wvarPOSEEACC = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POSEEACC ) */ );
      }
      else
      {
        wvarPOSEEACC = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDOCUMTIP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOCUMTIP ) */ );
      }
      else
      {
        wvarDOCUMTIP = "0";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT ) */ == (org.w3c.dom.Node) null) )
      {
        wvarDOCUMDAT = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOCUMDAT ) */ );
      }
      else
      {
        wvarDOCUMDAT = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAILP ) */ == (org.w3c.dom.Node) null) )
      {
        wvarEMAILP = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAILP ) */ );
      }
      else
      {
        wvarEMAILP = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAILL ) */ == (org.w3c.dom.Node) null) )
      {
        wvarEMAILL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EMAILL ) */ );
      }
      else
      {
        wvarEMAILL = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENTE ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCLIENTE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENTE ) */ );
      }
      else
      {
        wvarCLIENTE = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CONFORM ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCONFORM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CONFORM ) */ );
      }
      else
      {
        wvarCONFORM = "";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMCMLCOD ) */ == (org.w3c.dom.Node) null) )
      {
        wvarCAMCMLCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CAMCMLCOD ) */ );
      }
      else
      {
        wvarCAMCMLCOD = "";
      }

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPOCLI ) */ == (org.w3c.dom.Node) null) )
      {
        wvarTIPOCLI = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIPOCLI ) */ );
      }
      else
      {
        wvarTIPOCLI = "";
      }

      // --- FIN MODIFICACION 12-12-2005 ---
      //
      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 60;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 70;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@FECHACOTI", AdoConst.adVarChar, AdoConst.adParamInput, 8, new Variant( wvarFechaCoti ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NACIMDIA", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarNACIMDIA ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NACIMMES", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarNACIMMES ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NACIMANN", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarNACIMANN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@ESTADO_ID", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarEstado_OP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //Set wobjDBParm = wobjDBCmd.CreateParameter("@APELLIDO_NOMBRE", adVarChar, adParamInput, 50, "")
      wobjDBParm = new Parameter( "@APELLIDO_NOMBRE", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarAPELLIDO_NOMBRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //Set wobjDBParm = wobjDBCmd.CreateParameter("@TELEFONO", adVarChar, adParamInput, 50, "")
      wobjDBParm = new Parameter( "@TELEFONO", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarTELEFONO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@ESTADO_CIVIL_ID", AdoConst.adVarChar, AdoConst.adParamInput, 1, new Variant( wvarEstado ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@LOCALIDAD", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarLOCALIDAD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@POSTACOD", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarLocalidadCod ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@SEXO", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarSexo ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@PROVINCIAID", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarProvi ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@PORTAL", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarPORTAL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@MARCADES", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarMarcaAut ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      //Set wobjDBParm = wobjDBCmd.CreateParameter("@VEHÍCULO", adVarChar, adParamInput, 50, wvarModeAut) ' MQ 0093
      // MQ 0051
      wobjDBParm = new Parameter( "@VEHÍCULO", AdoConst.adVarChar, AdoConst.adParamInput, 80, new Variant( wvarModeAut ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@FABRICAN", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarEFECTANN ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUKLMNUM", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarKmsRngCod ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUNUMSIN", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarSiniestros ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@ESCERO", AdoConst.adBoolean, AdoConst.adParamInput, 0, new Variant( wvarRbOkm ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@SUMAASEG", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarSumAseg ) );
      wobjDBParm.setPrecision( (byte)( 18 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@CBITVRTA", AdoConst.adBoolean, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@ITVLSDIA", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@ITVLSMES", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@CBITVANN", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@HIJOS1729", AdoConst.adBoolean, AdoConst.adParamInput, 0, new Variant( wvarHijos1729 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@INCLUYEHIJOS", AdoConst.adBoolean, AdoConst.adParamInput, 0, new Variant( wvarCondAdic ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUMARCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarMarcaAutCod ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUMODCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarModeAutCod ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUSUBCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAuSubCod ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUADICOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAuAdiCod ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUMODORI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarAuModOri ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NUMECOLO", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@AUUSOGNC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarGas ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      // DATOS DE CONDUCTORES ADICIONALES
      wvarStep = 70;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_HIJOS ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarEdadHijo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_EDADHIJO ) */ );
        wvarEstadoHijo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_ESTADOHIJO ) */ );
        wvarSexoHijo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_SEXOHIJO ) */ );

        wobjDBParm = new Parameter( "@CAEDAD" + String.valueOf( wvarCounter + 1 ), AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarEdadHijo ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CAESTADO_CIVIL" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarEstadoHijo ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CASEXO" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarSexoHijo ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }

      wvarStep = 80;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {
        wobjDBParm = new Parameter( "@CAEDAD" + String.valueOf( wvarCounter ), AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CAESTADO_CIVIL" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@CASEXO" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( "" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;

      // --- CAMPOS AGREGADOS 03-12-2005. ---
      // Modificación Realizada: 12-12-2005. Fernando Osores
      wvarStep = 90;
      wobjDBParm = new Parameter( "@GARAGE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarGARAGE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@POSEEACC", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarPOSEEACC ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@DOCUMTIP", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( "0" + wvarDOCUMTIP ) ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@DOCUMDAT", AdoConst.adChar, AdoConst.adParamInput, 11, new Variant( wvarDOCUMDAT ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@EMAILP", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( wvarEMAILP ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@EMAILL", AdoConst.adChar, AdoConst.adParamInput, 80, new Variant( wvarEMAILL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@CLIENTE", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCLIENTE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@CONFORM", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarCONFORM ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@CAMCMLCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarCAMCMLCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wobjDBParm = new Parameter( "@TIPOCLI", AdoConst.adChar, AdoConst.adParamInput, 1, new Variant( wvarTIPOCLI ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wvarStep = 100;
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteParam_AU ) */;
      wvarStep = 110;
      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarAUACCCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUACCCOD ) */ );
        wvarAUVEASUM = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEASUM ) */ );
        wvarAUVEADES = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_AUVEADES ) */ );

        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( Obj.toInt( wvarAUACCCOD ) ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter + 1 ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarAUVEASUM ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter + 1 ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( wvarAUVEADES ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {
        wobjDBParm = new Parameter( "@AUACCCOD" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 4 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEASUM" + String.valueOf( wvarCounter ), AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( 0 ) );
        wobjDBParm.setPrecision( (byte)( 15 ) );
        wobjDBParm.setScale( (byte)( 0 ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;

        wobjDBParm = new Parameter( "@AUVEADES" + String.valueOf( wvarCounter ), AdoConst.adChar, AdoConst.adParamInput, 30, new Variant( "" ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      wobjXMLList = (org.w3c.dom.NodeList) null;
      // --- FIN MODIFICACION 12-12-2005 ---
      wvarStep = 140;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 150;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() > 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><COTIID>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</COTIID></Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
        {
          wvarMensaje = "PARAMETROS INCORRECTOS";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -2 )
        {
          wvarMensaje = "ERROR AL INSERTAR";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -3 )
        {
          wvarMensaje = "ERROR AL ACTUALIZAR";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -4 )
        {
          wvarMensaje = "ERROR AL ELIMINAR";
        }

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 180;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
