package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutCotizHogar implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_PutCotizHogar";
  static final String mcteStoreProc = "SPSNCV_ADM_INTERNET_HOGAR_INSERT";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_ESTADO_ID = "//ESTADO_ID";
  static final String mcteParam_TELEFONO = "//TELEFONO";
  static final String mcteParam_APELLIDO_NOMBRE = "//APELLIDO_NOMBRE";
  static final String mcteParam_LOCALIDAD = "//LOCALIDAD";
  static final String mcteParam_POSTACOD = "//POSTACOD";
  static final String mcteParam_PROVINCIAID = "//PROVINCIAID";
  static final String mcteParam_PORTAL = "//PORTAL";
  static final String mcteParam_TIVIVCOD = "//TIVIVCOD";
  static final String mcteParam_COBERCOD1 = "//COBERCOD1";
  static final String mcteParam_NUMERMOD1 = "//NUMERMOD1";
  static final String mcteParam_COBERCOD2 = "//COBERCOD2";
  static final String mcteParam_NUMERMOD2 = "//NUMERMOD2";
  static final String mcteParam_COBERCOD3 = "//COBERCOD3";
  static final String mcteParam_NUMERMOD3 = "//NUMERMOD3";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  private com.qbe.services.HSBCInterfaces.IDBConnection mobjHSBC_DBCnn = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    String wvarESTADO_ID = "";
    String wvarTELEFONO = "";
    String wvarAPELLIDO_NOMBRE = "";
    String wvarLOCALIDAD = "";
    String wvarPOSTACOD = "";
    String wvarPROVINCIAID = "";
    String wvarPORTAL = "";
    String wvarRAMOPCOD = "";
    String wvarPLANNCOD = "";
    String wvarTIVIVCOD = "";
    String wvarCOBERCOD1 = "";
    String wvarNUMERMOD1 = "";
    String wvarCOBERCOD2 = "";
    String wvarNUMERMOD2 = "";
    String wvarCOBERCOD3 = "";
    String wvarNUMERMOD3 = "";
    //
    //
    // DATOS GENERALES
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      // DATOS GENERALES
      wvarESTADO_ID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ESTADO_ID ) */ );
      wvarTELEFONO = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TELEFONO ) */ );
      wvarAPELLIDO_NOMBRE = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_APELLIDO_NOMBRE ) */ );
      wvarLOCALIDAD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LOCALIDAD ) */ );
      wvarPOSTACOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POSTACOD ) */ );
      wvarPROVINCIAID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PROVINCIAID ) */ );
      wvarPORTAL = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PORTAL ) */ );
      wvarTIVIVCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TIVIVCOD ) */ );
      wvarCOBERCOD1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBERCOD1 ) */ );
      wvarNUMERMOD1 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMERMOD1 ) */ );
      wvarCOBERCOD2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBERCOD2 ) */ );
      wvarNUMERMOD2 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMERMOD2 ) */ );
      wvarCOBERCOD3 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_COBERCOD3 ) */ );
      wvarNUMERMOD3 = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NUMERMOD3 ) */ );

      // LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
      wvarStep = 24;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarRAMOPCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteRAMOPCOD ) */ );
      wvarPLANNCOD = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePLANNCOD ) */ );
      //
      wvarStep = 28;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;

      wvarStep = 30;
      wobjHSBC_DBCnn = new HSBC.DBConnectionTrx();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
      wobjDBCmd = new Command();

      wvarStep = 60;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 70;
      wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@ESTADO_ID", AdoConst.adInteger, AdoConst.adParamInput, 0, new Variant( wvarESTADO_ID ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@APELLIDO_NOMBRE", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarAPELLIDO_NOMBRE ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@TELEFONO", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarTELEFONO ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@LOCALIDAD", AdoConst.adVarChar, AdoConst.adParamInput, 50, new Variant( wvarLOCALIDAD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@POSTACOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPOSTACOD ) );
      wobjDBParm.setPrecision( (byte)( 5 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@PROVINCIAID", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPROVINCIAID ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@PORTAL", AdoConst.adChar, AdoConst.adParamInput, 10, new Variant( wvarPORTAL ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@RAMOPCOD", AdoConst.adChar, AdoConst.adParamInput, 4, new Variant( wvarRAMOPCOD ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@PLANNCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarPLANNCOD ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@TIVIVCOD", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarTIVIVCOD ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wobjDBParm = new Parameter( "@COBERCOD1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCOBERCOD1 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NUMERMOD1", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNUMERMOD1 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wobjDBParm = new Parameter( "@COBERCOD2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCOBERCOD2 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NUMERMOD2", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNUMERMOD2 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;


      wobjDBParm = new Parameter( "@COBERCOD3", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarCOBERCOD3 ) );
      wobjDBParm.setPrecision( (byte)( 3 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wobjDBParm = new Parameter( "@NUMERMOD3", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNUMERMOD3 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;

      wvarStep = 140;
      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );

      wvarStep = 150;
      if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() > 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><COTIID>" + wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue() + "</COTIID></Response>" );
      }
      else
      {
        
        if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -1 )
        {
          wvarMensaje = "PARAMETROS INCORRECTOS";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -2 )
        {
          wvarMensaje = "ERROR AL INSERTAR";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -3 )
        {
          wvarMensaje = "ERROR AL ACTUALIZAR";
        }
        else if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() == -4 )
        {
          wvarMensaje = "ERROR AL ELIMINAR";
        }

        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      wvarStep = 180;
      wobjDBCnn = (Connection) null;
      wobjDBCmd = (Command) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        wobjDBCnn = (Connection) null;
        wobjDBCmd = (Command) null;
        wobjHSBC_DBCnn = null;
        wobjXMLRequest = (diamondedge.util.XmlDom) null;

        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
