package com.qbe.services.ofVirtualLBA.impl;
import java.sql.CallableStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetZonaIngBrut implements VBObjectClass
{
	
	  protected static Logger logger = Logger.getLogger(lbaw_GetZonaIngBrut.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_GetZonaIngBrut";
  static final String mcteStoreProc = "SPSNCV_SEFTTAIB_SELECT_ITEM";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_PAISSCOD = "//PAISSCOD";
  static final String mcteParam_V_PROVICOD = "//V_PROVICOD";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_CLIENIVA = "//CLIENIVA";
  static final String mcteParam_LISTA = "//LISTA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarPAISSCOD = "";
    String wvarV_PROVICOD = "";
    String wvarRAMOPCOD = "";
    String wvarCLIENIVA = "";
    String wvarLISTA = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );

      wvarPAISSCOD = StringUtils.trim(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PAISSCOD )));
      wvarV_PROVICOD = StringUtils.trim(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_V_PROVICOD )));
      wvarRAMOPCOD = StringUtils.trim(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )));
      wvarCLIENIVA = StringUtils.trim(XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENIVA )));

      wobjXMLRequest = null;

      wvarStep = 20;
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{call " + mcteStoreProc + "(?,?,?,?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSp = 1;
      //
      wvarStep = 55;
      ps.setString(posiSp++, wvarPAISSCOD);
      //
      wvarStep = 56;
      ps.setString(posiSp++, wvarV_PROVICOD);
      //
      wvarStep = 57;
      ps.setString(posiSp++, wvarRAMOPCOD);
      //
      wvarStep = 58;
      ps.setString(posiSp++, wvarCLIENIVA);
      //
      wvarStep = 60;
      boolean result = ps.execute();

      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }
      
      java.sql.ResultSet cursor = ps.getResultSet();
      //
      wvarStep = 70;
      if(cursor.next())
      {
        //
        wvarStep = 80;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 85;
        wobjXMLResponse.load(AdoUtils.saveResultSet(cursor));
        //
        wvarStep = 90;
        wobjXSLResponse.loadXML( p_GetXSL());

        wvarStep = 100;
        wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );

        wvarStep = 120;
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        wvarStep = 130;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 140;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ROW'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='PROVICOD'><xsl:value-of select='@PROVICOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTANN'><xsl:value-of select='@EFECTANN' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTMES'><xsl:value-of select='@EFECTMES' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EFECTDIA'><xsl:value-of select='@EFECTDIA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENIVA'><xsl:value-of select='@CLIENIVA' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIEIBTP'><xsl:value-of select='@CLIEIBTP' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='TASASPOR'><xsl:value-of select='@TASASPOR' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IMPMINIB'><xsl:value-of select='@IMPMINIB' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PAISSCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PROVICOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='RAMOPCOD'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EFECTANN'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EFECTMES'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='EFECTDIA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIENIVA'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CLIEIBTP'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='TASASPOR'/>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='IMPMINIB'/>";
    //
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
