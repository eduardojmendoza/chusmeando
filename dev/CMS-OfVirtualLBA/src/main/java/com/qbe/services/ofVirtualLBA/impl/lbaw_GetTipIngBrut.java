package com.qbe.services.ofVirtualLBA.impl;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetTipIngBrut implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_GetTipIngBrut";
  static final String mcteStoreProc = "SPSNCV_SIFTCIBB_SELECT";
  protected static Logger logger = Logger.getLogger(lbaw_GetTipIngBrut.class.getName());
  
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CodIB = "//NUMERO";
  static final String mcteParam_LISTA = "//LISTA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCodIB = "";
    String wvarLISTA = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarCodIB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CodIB )  );
      if( wobjXMLRequest.selectNodes( mcteParam_LISTA ) .getLength() == 0 )
      {
        wvarLISTA = "N";
      }
      else
      {
        wvarLISTA = "S";
      }
      //
      wobjXMLRequest = null;

      wvarStep = 20;
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{call " + mcteStoreProc + "(?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;
      //
      wvarStep = 40;
      //
      wvarStep = 55;

      if (wvarCodIB.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else { 
    	  ps.setInt(posiSP++, Integer.parseInt(wvarCodIB)); 
      }

      //
      wvarStep = 60;
      boolean result = ps.execute();
      
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }


      java.sql.ResultSet cursor = ps.getResultSet();
      if( cursor.next())
      {
        //
        wvarStep = 80;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 85;
        wobjXMLResponse.load(AdoUtils.saveResultSet(cursor));
        //
        if( wvarLISTA.equals( "S" ) )
        {
          wvarStep = 95;
          wobjXSLResponse.loadXML( p_GetXSLLista());
          //
          wvarStep = 100;
          wvarResult = "<TIPOIB>" + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ) + "</TIPOIB>";
          //
        }
        else
        {
          wvarStep = 90;
          wobjXSLResponse.loadXML( p_GetXSL());
          //
          wvarStep = 100;
          wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
          //
        }

        wvarStep = 120;
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        wvarStep = 130;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 140;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    //
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='OPTION'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:attribute name='value'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:value-of select='@CLIENIBB' />";
    wvarStrXSL = wvarStrXSL + "     </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "  <xsl:value-of select='@CIBBDESC' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private String p_GetXSLLista() throws Exception
  {
    String p_GetXSLLista = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='TIPOIB'>";
    //
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENIBB'><xsl:value-of select='@CLIENIBB' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CIBBDESC'><xsl:value-of select='@CIBBDESC' /></xsl:element>";
    //
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='CIBBDESC'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLLista = wvarStrXSL;
    return p_GetXSLLista;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
