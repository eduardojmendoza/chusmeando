package com.qbe.services.ofVirtualLBA.impl;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCotiAUS;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_PutSolicitud implements VBObjectClass
{
	
	private static Logger logger = Logger.getLogger(lbaw_PutSolicitud.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OfVirtualLBA.lbaw_PutSolicitud";
  static final String mcteStoreProc = "SPSNCV_INSERT_SOLICITUD";
  /**
   *  Parametros XML de Entrada
   *  DATOS GENERALES
   */
  static final String mcteParam_SUMASEG = "//SUMASEG";
  static final String mcteParam_SUMALBA = "//SUMALBA";
  static final String mcteParam_AUPROCOD = "//AUPROCOD";
  static final String mcteParam_WDB_ACCESORIOS_FLG = "//WDB_ACCESORIOS_FLG";
  static final String mcteParam_WDB_PRENDA = "//WDB_PRENDA";
  static final String mcteParam_WDB_CO_TITULAR = "//WDB_CO_TITULAR";
  static final String mcteParam_WDB_HIJOS1416_FLG = "//WDB_HIJOS1416_FLG";
  static final String mcteParam_WDB_HIJOS1729_FLG = "//WDB_HIJOS1729_FLG";
  static final String mcteParam_WDB_CONSEJO_FLG = "//WDB_CONSEJO_FLG ";
  static final String mcteParam_WDB_REFERIDO_FLG = "//WDB_REFERIDO_FLG";
  static final String mcteParam_WDB_USUARIO = "//WDB_USUARIO";
  static final String mcteParam_WDB_ESTADO = "//WDB_ESTADO";
  static final String mcteParam_WDB_USRINI = "//WDB_USRINI";
  static final String mcteParam_WDB_USRSPV = "//WDB_USRSPV";
  static final String mcteParam_WDB_FECCIECOT_DIA = "//WDB_FECCIECOT_DIA";
  static final String mcteParam_WDB_FECCIECOT_MES = "//WDB_FECCIECOT_MES";
  static final String mcteParam_WDB_FECCIECOT_ANO = "//WDB_FECCIECOT_ANO";
  static final String mcteParam_WDB_TELNRO = "//WDB_TELNRO";
  /**
   *  CAMPANIA, PRODUCTOR Y TELEFONO
   */
  static final String mcteParam_CAMPA_TEL = "//CAMPA_TEL";
  static final String mcteParam_CAMPA_COD = "//CAMPA_COD";
  static final String mcteParam_AGE_COD = "//AGE_COD";
  /**
   *  Accesorios
   */
  static final String mcteParam_ACCESORIOS = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_WDB_AC_AUACCCOD = "CODIGOACC";
  static final String mcteParam_WDB_AC_SUMA = "PRECIOACC";
  static final String mcteParam_WDB_AC_DESCRI = "DESCRIPCIONACC";
  static final String mcteParam_WDB_AUMARCOD = "//WDB_AUMARCOD";
  static final String mcteParam_WDB_AUMODCOD = "//WDB_AUMODCOD";
  static final String mcteParam_WDB_AUSUBCOD = "//WDB_AUSUBCOD";
  static final String mcteParam_WDB_AUADICOD = "//WDB_AUADICOD";
  static final String mcteParam_WDB_AUMODORI = "//WDB_AUMODORI";
  static final String mcteParam_WDB_FABRICAN = "//WDB_FABRICAN";
  static final String mcteParam_WDB_AUTIPCOD = "//WDB_AUTIPCOD";
  static final String mcteParam_WDB_AUKLMNUM = "//WDB_AUKLMNUM";
  static final String mcteParam_WDB_SUMAASEG = "//WDB_SUMAASEG";
  static final String mcteParam_WDB_SUMALBA = "//WDB_SUMALBA";
  static final String mcteParam_WDB_AUUSOCOD = "//WDB_AUUSOCOD";
  static final String mcteParam_WDB_AUTIPCOM = "//WDB_AUTIPCOM";
  static final String mcteParam_WDB_CHASISNUM = "//WDB_CHASISNUM";
  static final String mcteParam_WDB_MOTORNUM = "//WDB_MOTORNUM";
  static final String mcteParam_WDB_PATENNUM = "//WDB_PATENNUM";
  static final String mcteParam_WDB_ANOCOMP = "//WDB_ANOCOMP";
  static final String mcteParam_WDB_GARAJE_FLG = "//WDB_GARAJE_FLG";
  static final String mcteParam_WDB_GARAJE_PART = "//WDB_GARAJE_PART";
  static final String mcteParam_WDB_CEROKM_FLG = "//WDB_CEROKM_FLG";
  static final String mcteParam_WDB_AUCATCOD = "//WDB_AUCATCOD";
  static final String mcteParam_WDB_AUUSOGNC = "//WDB_AUUSOGNC";
  static final String mcteParam_WDB_COLOR = "//WDB_COLOR";
  static final String mcteParam_WDB_CLIENAP1 = "//WDB_CLIENAP1";
  static final String mcteParam_WDB_CLIENAP2 = "//WDB_CLIENAP2";
  static final String mcteParam_WDB_CLIENNOM = "//WDB_CLIENNOM";
  static final String mcteParam_WDB_DOCUMTIP = "//WDB_DOCUMTIP";
  static final String mcteParam_WDB_DOCUMDAT = "//WDB_DOCUMDAT";
  static final String mcteParam_WDB_FECNTO_DD = "//WDB_FECNTO_DD";
  static final String mcteParam_WDB_FECNTO_MM = "//WDB_FECNTO_MM";
  static final String mcteParam_WDB_FECNTO_AAAA = "//WDB_FECNTO_AAAA";
  static final String mcteParam_WDB_EDADCLI = "//WDB_EDADCLI";
  static final String mcteParam_WDB_ESTCIVCLI = "//WDB_ESTCIVCLI";
  static final String mcteParam_WDB_SEXOCLI = "//WDB_SEXOCLI";
  static final String mcteParam_WDB_LEGAJO = "//WDB_LEGAJO";
  static final String mcteParam_WDB_IVASITCLI = "//WDB_IVASITCLI";
  static final String mcteParam_WDB_PREFIJO = "//WDB_PREFIJO";
  static final String mcteParam_WDB_TELEFXXX = "//WDB_TELEFXXX";
  static final String mcteParam_WDB_FECALTA_DIA = "//WDB_FECALTA_DIA";
  static final String mcteParam_WDB_FECALTA_MES = "//WDB_FECALTA_MES";
  static final String mcteParam_WDB_FECALTA_ANO = "//WDB_FECALTA_ANO";
  static final String mcteParam_WDB_ANOLICEN = "//WDB_ANOLICEN";
  /**
   * Hijos
   */
  static final String mcteParam_HIJOS = "//HIJOS/HIJO";
  static final String mcteParam_WDB_APELLIDO_HIJO = "WDB_APELLIDO_HIJO";
  static final String mcteParam_WDB_NOMBRE_HIJO = "WDB_NOMBRE_HIJO";
  static final String mcteParam_WDB_SEXO_HIJO = "WDB_SEXO_HIJO";
  static final String mcteParam_WDB_NACIM_FEC_HIJO_DIA = "WDB_NACIM_FEC_HIJO_DIA";
  static final String mcteParam_WDB_NACIM_FEC_HIJO_MES = "WDB_NACIM_FEC_HIJO_MES";
  static final String mcteParam_WDB_NACIM_FEC_HIJO_ANO = "WDB_NACIM_FEC_HIJO_ANO";
  static final String mcteParam_WDB_EDAD_HIJO = "WDB_EDAD_HIJO";
  static final String mcteParam_WDB_ESTCIV_HIJO = "WDB_ESTCIV_HIJO";
  static final String mcteParam_WDB_INCLUIDO_HIJO = "WDB_INCLUIDO_HIJO";
  /**
   * 
   */
  static final String mcteParam_WDB_CALLESEL = "//WDB_CALLESEL";
  static final String mcteParam_WDB_DOMICDNU = "//WDB_DOMICDNU";
  static final String mcteParam_WDB_DOMICPIS = "//WDB_DOMICPIS";
  static final String mcteParam_WDB_DOMICPTA = "//WDB_DOMICPTA";
  static final String mcteParam_WDB_CODPOST_RGO = "//WDB_CODPOST_RGO";
  static final String mcteParam_WDB_LOCALIDAD = "//WDB_LOCALIDAD";
  static final String mcteParam_WBD_CODPROV_RGO = "//WBD_CODPROV_RGO";
  static final String mcteParam_WDB_DOMICDOMCOM = "//WDB_DOMICDOMCOM";
  static final String mcteParam_WDB_DOMICDNUCO = "//WDB_DOMICDNUCO";
  static final String mcteParam_WDB_DOMICPISCO = "//WDB_DOMICPISCO";
  static final String mcteParam_WDB_DOMICPTACO = "//WDB_DOMICPTACO";
  static final String mcteParam_WDB_DOMICPOBCO = "//WDB_DOMICPOBCO";
  static final String mcteParam_WDB_DOMICCPOCO = "//WDB_DOMICCPOCO";
  static final String mcteParam_WDB_PROVICODCO = "//WDB_PROVICODCO";
  static final String mcteParam_WDB_DOMICPRECO = "//WDB_DOMICPRECO";
  static final String mcteParam_WDB_DOMICTLFCO = "//WDB_DOMICTLFCO";
  static final String mcteParam_WDB_CODINST = "//WDB_CODINST";
  static final String mcteParam_WDB_CODTRANS = "//WDB_CODTRANS";
  static final String mcteParam_WDB_RAMOPCOD = "//WDB_RAMOPCOD";
  static final String mcteParam_WDB_VIGENDIA = "//WDB_VIGENDIA";
  static final String mcteParam_WDB_VIGENMES = "//WDB_VIGENMES";
  static final String mcteParam_WDB_VIGENANN = "//WDB_VIGENANN";
  static final String mcteParam_WDB_CODI_ZONA = "//WDB_CODI_ZONA";
  static final String mcteParam_WDB_PRECIO_MENSUAL = "//WDB_PRECIO_MENSUAL";
  static final String mcteParam_WDB_SINIESTROS = "//WDB_SINIESTROS";
  static final String mcteParam_WDB_PLANCOD = "//WDB_PLANCOD";
  static final String mcteParam_WDB_FRANQCOD = "//WDB_FRANQCOD";
  static final String mcteParam_WDB_CLUBLBA = "//WDB_CLUBLBA";
  static final String mcteParam_WDB_LUNETA = "//WDB_LUNETA";
  static final String mcteParam_WDB_EMISDIA = "//WDB_EMISDIA";
  static final String mcteParam_WDB_EMISMES = "//WDB_EMISMES";
  static final String mcteParam_WDB_EMISANN = "//WDB_EMISANN";
  static final String mcteParam_WDB_CAMPANA = "//WDB_CAMPANA";
  static final String mcteParam_WDB_PRODUCTOR = "//WDB_PRODUCTOR";
  static final String mcteParam_WDB_BANCOCOD = "//WDB_BANCOCOD";
  static final String mcteParam_WDB_SUCURCOD = "//WDB_SUCURCOD";
  static final String mcteParam_WDB_CODIGO_COBRO = "//WDB_CODIGO_COBRO";
  static final String mcteParam_WDB_TIPO_COBRO = "//WDB_TIPO_COBRO";
  static final String mcteParam_WDB_VEHDES = "//WDB_VEHDES";
  static final String mcteParam_WDB_NROCTA = "//WDB_NROCTA";
  static final String mcteParam_WDB_CODBANCO = "//WDB_CODBANCO";
  static final String mcteParam_WDB_SUCCTA = "//WDB_SUCCTA";
  static final String mcteParam_WDB_FECHAVTOCTA_DIA = "//WDB_FECHAVTOCTA_DIA";
  static final String mcteParam_WDB_FECHAVTOCTA_MES = "//WDB_FECHAVTOCTA_MES";
  static final String mcteParam_WDB_FECHAVTOCTA_ANN = "//WDB_FECHAVTOCTA_ANN";
  static final String mcteParam_WDB_TELEFONO = "//WDB_TELEFONO";
  static final String mcteParam_WDB_EMAIL = "//WDB_EMAIL";
  static final String mcteParam_WDB_USUARIO_RED = "//WDB_USUARIO_RED";
  static final String mcteParam_WDB_ALARMA = "//WDB_ALARMA";
  /**
   *  Coberturas
   */
  static final String mcteParam_COT_NRO = "//COT_NRO";
  static final String mcteParam_WDB_COBERCOD = "//COBERCOD";
  static final String mcteParam_WDB_COBERORD = "//COBERORD";
  static final String mcteParam_WDB_CAPITASG = "//CAPITASG";
  static final String mcteParam_WDB_CAPITIMP = "//CAPITIMP";
  /**
   * Datos exigidos por la UIF
   */
  static final String mcteParam_WDB_OCUPACODCLIE = "//OCUPACODCLIE";
  static final String mcteParam_WDB_UIFCUIT = "//UIFCUIT";
  static final String mcteParam_WDB_SWAPODER = "//SWAPODER";
  /**
   * Green Products
   */
  static final String mcteParam_WDB_GREEN = "//WDB_GREEN";
  static final String mcteParam_WDB_GRANIZO = "//WDB_GRANIZO";
  static final String mcteParam_WDB_ROBOCONT = "//WDB_ROBOCONT";
  /**
   * AGENTCLA
   *  MMC 2012-11-29
   */
  static final String mcteParam_WDB_AGECLA = "//AGECLA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   * 
   *  DATOS DE CONDUCTORES ADICIONALES
   */
  public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLCoberturas = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    int wvarStep = 0;
    int wvarCounter = 0;
    String wvarMensaje = "";
    Object wobjClass = null;
    String wvarResponse = "";
    String wvarRequest = "";
    String wvarSumAseg = "";
    String wvarSumaLBA = "";
    String wvarWDB_ACCESORIOS_FLG = "";
    String wvarWDB_PRENDA = "";
    String wvarWDB_CO_TITULAR = "";
    String wvarWDB_HIJOS1416_FLG = "";
    String wvarWDB_HIJOS1729_FLG = "";
    String wvarWDB_CONSEJO_FLG = "";
    String wvarWDB_REFERIDO_FLG = "";
    String wvarWDB_USUARIO = "";
    String wvarWDB_ESTADO = "";
    String wvarWDB_USRINI = "";
    String wvarWDB_USRSPV = "";
    String wvarWDB_FECCIECOT_DIA = "";
    String wvarWDB_FECCIECOT_MES = "";
    String wvarWDB_FECCIECOT_ANO = "";
    String wvarWDB_TELNRO = "";
    String wvarCAMPA_TEL = "";
    String wvarCAMPA_COD = "";
    String wvarAGE_COD = "";
    String wvarWDB_AC_AUACCCOD = "";
    String wvarWDB_AC_SUMA = "";
    String wvarWDB_AC_DESCRI = "";
    String wvarWDB_AC_DEPRE = "";
    String wvarWDB_AUMARCOD = "";
    String wvarWDB_AUMODCOD = "";
    String wvarWDB_AUSUBCOD = "";
    String wvarWDB_AUADICOD = "";
    String wvarWDB_AUMODORI = "";
    String wvarWDB_FABRICAN = "";
    String wvarWDB_AUTIPCOD = "";
    String wvarWDB_AUKLMNUM = "";
    String wvarWDB_SUMAASEG = "";
    String wvarWDB_SUMALBA = "";
    String wvarWDB_AUUSOCOD = "";
    String wvarWDB_AUTIPCOM = "";
    String wvarWDB_CHASISNUM = "";
    String wvarWDB_MOTORNUM = "";
    String wvarWDB_PATENNUM = "";
    String wvarWDB_ANOCOMP = "";
    String wvarWDB_GARAJE_FLG = "";
    String wvarWDB_GARAJE_PART = "";
    String wvarWDB_CEROKM_FLG = "";
    String wvarWDB_AUCATCOD = "";
    String wvarWDB_AUUSOGNC = "";
    String wvarWDB_COLOR = "";
    String wvarWDB_CLIENAP1 = "";
    String wvarWDB_CLIENAP2 = "";
    String wvarWDB_CLIENNOM = "";
    String wvarWDB_DOCUMTIP = "";
    String wvarWDB_DOCUMDAT = "";
    String wvarWDB_FECNTO_DD = "";
    String wvarWDB_FECNTO_MM = "";
    String wvarWDB_FECNTO_AAAA = "";
    String wvarWDB_EDADCLI = "";
    String wvarWDB_ESTCIVCLI = "";
    String wvarWDB_SEXOCLI = "";
    String wvarWDB_LEGAJO = "";
    String wvarWDB_IVASITCLI = "";
    String wvarWDB_PREFIJO = "";
    String wvarWDB_TELEFXXX = "";
    String wvarWDB_FECALTA_DIA = "";
    String wvarWDB_FECALTA_MES = "";
    String wvarWDB_FECALTA_ANO = "";
    String wvarWDB_ANOLICEN = "";
    String wvarWDB_APELLIDO_HIJO = "";
    String wvarWDB_NOMBRE_HIJO = "";
    String wvarWDB_SEXO_HIJO = "";
    String wvarWDB_NACIM_FEC_HIJO_DIA = "";
    String wvarWDB_NACIM_FEC_HIJO_MES = "";
    String wvarWDB_NACIM_FEC_HIJO_ANO = "";
    String wvarWDB_EDAD_HIJO = "";
    String wvarWDB_ESTCIV_HIJO = "";
    String wvarWDB_INCLUIDO_HIJO = "";
    String wvarWDB_CALLESEL = "";
    String wvarWDB_DOMICDNU = "";
    String wvarWDB_DOMICPIS = "";
    String wvarWDB_DOMICPTA = "";
    String wvarWDB_CODPOST_RGO = "";
    String wvarWDB_LOCALIDAD = "";
    String wvarWBD_CODPROV_RGO = "";
    String wvarWDB_DOMICDOMCOM = "";
    String wvarWDB_DOMICDNUCO = "";
    String wvarWDB_DOMICPISCO = "";
    String wvarWDB_DOMICPTACO = "";
    String wvarWDB_DOMICPOBCO = "";
    String wvarWDB_DOMICCPOCO = "";
    String wvarWDB_PROVICODCO = "";
    String wvarWDB_DOMICPRECO = "";
    String wvarWDB_DOMICTLFCO = "";
    String wvarWDB_CODINST = "";
    String wvarWDB_CODTRANS = "";
    String wvarWDB_RAMOPCOD = "";
    String wvarWDB_VIGENDIA = "";
    String wvarWDB_VIGENMES = "";
    String wvarWDB_VIGENANN = "";
    String wvarWDB_CODI_ZONA = "";
    String wvarWDB_PRECIO_MENSUAL = "";
    String wvarWDB_SINIESTROS = "";
    String wvarWDB_PLANCOD = "";
    String wvarWDB_FRANQCOD = "";
    String wvarWDB_CLUBLBA = "";
    String wvarWDB_LUNETA = "";
    String wvarWDB_EMISDIA = "";
    String wvarWDB_EMISMES = "";
    String wvarWDB_EMISANN = "";
    String wvarWDB_CAMPANA = "";
    String wvarWDB_PRODUCTOR = "";
    String wvarWDB_BANCOCOD = "";
    String wvarWDB_SUCURCOD = "";
    String wvarWDB_CODIGO_COBRO = "";
    String wvarWDB_TIPO_COBRO = "";
    String wvarWDB_VEHDES = "";
    String wvarWDB_NROCTA = "";
    String wvarWDB_CODBANCO = "";
    String wvarWDB_SUCCTA = "";
    String wvarWDB_FECHAVTOCTA_DIA = "";
    String wvarWDB_FECHAVTOCTA_MES = "";
    String wvarWDB_FECHAVTOCTA_ANN = "";
    String wvarWDB_TELEFONO = "";
    String wvarWDB_EMAIL = "";
    String wvarWDB_USUARIO_RED = "";
    String wvarWDB_ALARMA = "";
    String wvarCot_Nro = "";
    String wvarWDB_COBERCOD = "";
    String wvarWDB_COBERORD = "";
    String wvarWDB_CAPITASG = "";
    String wvarWDB_CAPITIMP = "";
    String wvarWDB_OCUPACODCLIE = "";
    String wvarWDB_UIFCUIT = "";
    String wvarWDB_SWAPODER = "";
    String wvarWDB_GREEN = "";
    String wvarWDB_GRANIZO = "";
    String wvarWDB_ROBOCONT = "";
    String wvarWDB_AGECLA = "";
    //
    //
    // DATOS GENERALES
    // CAMPANIA, PRODUCTOR Y TELEFONO
    // ACCESORIOS
    //
    // Datos de los hijos adicionales
    //
    // COBERTURAS
    //Datos exigidos por la UIF
    //Green Products
    //AGENTCLA
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // CARGO EL XML DE ENTRADA
      wvarStep = 5;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      // VUELVO A COTIZAR PARA EL PLAN SELECCIONADO
      wvarStep = 20;
      //
      //Cotizamos con el mismo mensaje que la OV
      //Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaw_GetCotizacion")
      //
//      wobjClass = new lbawA_OVMQCotizar.lbaw_OVGetCotiAUS();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
      //Begin convert
      lbaw_OVGetCotiAUS getCotiAus = new lbaw_OVGetCotiAUS();
      StringHolder sh = new StringHolder();
      int resultCode = getCotiAus.IAction_Execute(XmlDomExtended.marshal(wobjXMLRequest.getDocument().getDocumentElement()), sh, "");
      wvarResponse = sh.getValue();
      //End convert
      wobjClass = null;
      //
      // GUARDO LAS COBERTURAS, LAS SUMAS ASEGURADAS, CAMPANIA, PRODUCTOR Y TELEFONO
      wvarStep = 25;
      wobjXMLCoberturas = new XmlDomExtended();
      wobjXMLCoberturas.loadXML( wvarResponse );
      //
      wvarCot_Nro = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_COT_NRO )  );
      //
      wvarStep = 26;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMAASEG ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_SUMASEG )  ) );
      //
      wvarStep = 27;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMALBA ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_SUMALBA )  ) );
      //
      wvarStep = 28;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELNRO ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAMPA_TEL )  ) );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CAMPANA ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_CAMPA_COD )  ) );
      //
      wvarStep = 29;
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRINI ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_AGE_COD )  ) );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRSPV ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_AGE_COD )  ) );
      XmlDomExtended.setText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRODUCTOR ) , XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_AGE_COD )  ) );
      //
      wvarStep = 30;
      //
      // DATOS GENERALES
      wvarStep = 40;
      wvarWDB_ACCESORIOS_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ACCESORIOS_FLG )  );
      wvarWDB_PRENDA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRENDA )  );
      wvarWDB_CO_TITULAR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CO_TITULAR )  );
      wvarWDB_HIJOS1416_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_HIJOS1416_FLG )  );
      wvarWDB_HIJOS1729_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_HIJOS1729_FLG )  );

      wvarStep = 50;
      wvarWDB_CONSEJO_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CONSEJO_FLG )  );
      wvarWDB_REFERIDO_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_REFERIDO_FLG )  );
      wvarWDB_USUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_USUARIO )  );
      wvarWDB_ESTADO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ESTADO )  );
      wvarWDB_USRINI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRINI )  );

      wvarStep = 60;
      wvarWDB_USRSPV = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_USRSPV )  );
      wvarWDB_FECCIECOT_DIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_DIA )  );
      wvarWDB_FECCIECOT_MES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_MES )  );
      wvarWDB_FECCIECOT_ANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECCIECOT_ANO )  );
      wvarWDB_TELNRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELNRO )  );

      wvarStep = 70;
      wvarWDB_AUMARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUMARCOD )  );
      wvarWDB_AUMODCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUMODCOD )  );
      wvarWDB_AUSUBCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUSUBCOD )  );
      wvarWDB_AUADICOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUADICOD )  );
      wvarWDB_AUMODORI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUMODORI )  );

      wvarStep = 80;
      wvarWDB_FABRICAN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FABRICAN )  );
      wvarWDB_AUTIPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUTIPCOD )  );
      wvarWDB_AUKLMNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUKLMNUM )  );
      wvarWDB_SUMAASEG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMAASEG )  );
      wvarWDB_SUMALBA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUMALBA )  );

      wvarStep = 90;
      wvarWDB_AUUSOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUUSOCOD )  );
      wvarWDB_AUTIPCOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUTIPCOM )  );
      wvarWDB_CHASISNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CHASISNUM )  );
      wvarWDB_MOTORNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_MOTORNUM )  );
      wvarWDB_PATENNUM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PATENNUM )  );

      wvarStep = 100;
      wvarWDB_ANOCOMP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ANOCOMP )  );
      wvarWDB_GARAJE_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_GARAJE_FLG )  );
      wvarWDB_GARAJE_PART = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_GARAJE_PART )  );
      wvarWDB_CEROKM_FLG = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CEROKM_FLG )  );
      wvarWDB_AUCATCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUCATCOD )  );
      wvarWDB_AUUSOGNC = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AUUSOGNC )  );
      wvarStep = 110;
      wvarWDB_COLOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_COLOR )  );

      wvarWDB_CLIENAP1 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENAP1 )  );
      wvarWDB_CLIENAP2 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENAP2 )  );
      wvarWDB_CLIENNOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLIENNOM )  );
      wvarWDB_DOCUMTIP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOCUMTIP )  );

      wvarStep = 120;
      wvarWDB_DOCUMDAT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOCUMDAT )  );
      wvarWDB_FECNTO_DD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_DD )  );
      wvarWDB_FECNTO_MM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_MM )  );
      wvarWDB_FECNTO_AAAA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECNTO_AAAA )  );
      wvarWDB_EDADCLI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_EDADCLI )  );

      wvarStep = 130;
      wvarWDB_ESTCIVCLI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ESTCIVCLI )  );
      wvarWDB_SEXOCLI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SEXOCLI )  );
      wvarWDB_LEGAJO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_LEGAJO )  );
      wvarWDB_IVASITCLI = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_IVASITCLI )  );
      wvarWDB_PREFIJO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PREFIJO )  );

      wvarStep = 140;
      wvarWDB_TELEFXXX = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELEFXXX )  );
      wvarWDB_FECALTA_DIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECALTA_DIA )  );
      wvarWDB_FECALTA_MES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECALTA_MES )  );
      wvarWDB_FECALTA_ANO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECALTA_ANO )  );
      wvarWDB_ANOLICEN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ANOLICEN )  );

      wvarStep = 150;
      wvarWDB_CALLESEL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CALLESEL )  );
      wvarWDB_DOMICDNU = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDNU )  );
      wvarWDB_DOMICPIS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPIS )  );
      wvarWDB_DOMICPTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPTA )  );
      wvarWDB_CODPOST_RGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODPOST_RGO )  );

      wvarStep = 160;
      wvarWDB_LOCALIDAD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_LOCALIDAD )  );
      wvarWBD_CODPROV_RGO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WBD_CODPROV_RGO )  );
      wvarWDB_DOMICDOMCOM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDOMCOM )  );
      wvarWDB_DOMICDNUCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICDNUCO )  );
      wvarWDB_DOMICPISCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPISCO )  );

      wvarStep = 170;
      wvarWDB_DOMICPTACO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPTACO )  );
      wvarWDB_DOMICPOBCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPOBCO )  );
      wvarWDB_DOMICCPOCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICCPOCO )  );
      wvarWDB_PROVICODCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PROVICODCO )  );
      wvarWDB_DOMICPRECO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICPRECO )  );

      wvarStep = 180;
      wvarWDB_DOMICTLFCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_DOMICTLFCO )  );
      wvarWDB_CODINST = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODINST )  );
      wvarWDB_CODTRANS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODTRANS )  );
      wvarWDB_RAMOPCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_RAMOPCOD )  );
      wvarWDB_VIGENDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENDIA )  );

      wvarStep = 190;
      wvarWDB_VIGENMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENMES )  );
      wvarWDB_VIGENANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_VIGENANN )  );
      wvarWDB_CODI_ZONA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODI_ZONA )  );
      wvarWDB_PRECIO_MENSUAL = Strings.replace( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRECIO_MENSUAL )  ), ",", "." );
      wvarWDB_SINIESTROS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SINIESTROS )  );

      wvarStep = 200;
      wvarWDB_PLANCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PLANCOD )  );
      wvarWDB_FRANQCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FRANQCOD )  );
      wvarWDB_CLUBLBA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CLUBLBA )  );
      wvarWDB_LUNETA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_LUNETA )  );
      wvarWDB_EMISDIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMISDIA )  );
      wvarWDB_EMISMES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMISMES )  );

      wvarStep = 205;
      wvarWDB_GREEN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_GREEN )  );
      wvarWDB_GRANIZO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_GRANIZO )  );
      wvarWDB_ROBOCONT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ROBOCONT )  );

      wvarStep = 210;
      wvarWDB_EMISANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMISANN )  );
      wvarWDB_CAMPANA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CAMPANA )  );
      wvarWDB_PRODUCTOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_PRODUCTOR )  );
      wvarWDB_BANCOCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_BANCOCOD )  );
      wvarWDB_SUCURCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUCURCOD )  );
      //MMC 2012-11-29
      wvarWDB_AGECLA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_AGECLA )  );

      wvarStep = 220;
      wvarWDB_CODIGO_COBRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODIGO_COBRO )  );
      wvarWDB_TIPO_COBRO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_COBRO )  );
      wvarWDB_VEHDES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_VEHDES )  );
      wvarWDB_NROCTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_NROCTA )  );
      wvarWDB_CODBANCO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_CODBANCO )  );

      wvarStep = 230;
      wvarWDB_SUCCTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SUCCTA )  );
      wvarWDB_FECHAVTOCTA_DIA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_DIA )  );
      wvarWDB_FECHAVTOCTA_MES = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_MES )  );
      wvarWDB_FECHAVTOCTA_ANN = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_FECHAVTOCTA_ANN )  );
      wvarWDB_TELEFONO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_TELEFONO )  );

      wvarStep = 240;
      wvarWDB_EMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_EMAIL )  );
      wvarWDB_USUARIO_RED = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_USUARIO_RED )  );
      wvarWDB_ALARMA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_ALARMA )  );

      wvarStep = 241;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_WDB_OCUPACODCLIE )  == (org.w3c.dom.Node) null) )
      {
        wvarWDB_OCUPACODCLIE = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_OCUPACODCLIE )  );
      }
      else
      {
        wvarWDB_OCUPACODCLIE = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_WDB_UIFCUIT )  == (org.w3c.dom.Node) null) )
      {
        wvarWDB_UIFCUIT = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_UIFCUIT )  );
      }
      else
      {
        wvarWDB_UIFCUIT = "";
      }
      //
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWAPODER )  == (org.w3c.dom.Node) null) )
      {
        wvarWDB_SWAPODER = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_WDB_SWAPODER )  );
      }
      else
      {
        wvarWDB_SWAPODER = "";
      }
      //
      wvarStep = 250;
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
      String sp = "{? = call " + mcteStoreProc + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
      CallableStatement ps = jdbcConn.prepareCall(sp);
      int posiSP = 1;
      ps.registerOutParameter(posiSP++, Types.INTEGER);

      wvarStep = 280;

      wvarStep = 290;
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }

      wvarStep = 300;
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }

      wvarStep = 310;
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 320;
      ps.setString(posiSP++, String.valueOf(wvarWDB_ACCESORIOS_FLG));

      wvarStep = 330;
      ps.setString(posiSP++, String.valueOf(wvarWDB_PRENDA));

      wvarStep = 340;
      ps.setString(posiSP++, String.valueOf(wvarWDB_CO_TITULAR));

      wvarStep = 350;
      ps.setString(posiSP++, String.valueOf(wvarWDB_HIJOS1416_FLG));

      wvarStep = 360;
      ps.setString(posiSP++, String.valueOf(wvarWDB_HIJOS1729_FLG));

      wvarStep = 370;
      ps.setString(posiSP++, String.valueOf(wvarWDB_CONSEJO_FLG));

      wvarStep = 380;
      ps.setString(posiSP++, String.valueOf(wvarWDB_REFERIDO_FLG));

      wvarStep = 390;
      ps.setString(posiSP++, String.valueOf(wvarWDB_USUARIO));

      wvarStep = 400;
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 410;
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 420;
      ps.setString(posiSP++, String.valueOf(wvarWDB_ESTADO));

      wvarStep = 430;
      ps.setString(posiSP++, String.valueOf(wvarWDB_USRINI));

      wvarStep = 450;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_USRSPV));
	      wvarStep = 460;
      
      if (wvarWDB_FECCIECOT_DIA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECCIECOT_DIA));
      }
      
	      wvarStep = 470;
      
      if (wvarWDB_FECCIECOT_MES.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECCIECOT_MES));
      }
      
	      wvarStep = 480;
      
      if (wvarWDB_FECCIECOT_ANO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECCIECOT_ANO));
      }
      
	      wvarStep = 500;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 510;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_TELNRO));
	      wvarStep = 520;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 530;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 540;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 550;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 560;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 570;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 580;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 590;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 592;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 594;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 598;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 600;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 610;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 620;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 630;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 640;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 650;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 660;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 670;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 680;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 690;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 700;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      // aca vienen los accesorios
      wvarStep = 710;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_ACCESORIOS ) ;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarWDB_AC_AUACCCOD = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_AC_AUACCCOD )  );
        wvarWDB_AC_SUMA = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_AC_SUMA )  );
        wvarWDB_AC_DESCRI = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_AC_DESCRI )  );
        wvarWDB_AC_DEPRE = "SI";

        wvarStep = 720;
        
        if (wvarWDB_AC_AUACCCOD.isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AC_AUACCCOD));
        }
        
        wvarStep = 740;
        
        if (wvarWDB_AC_SUMA.isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AC_SUMA));
        }
        
        wvarStep = 750;
        
        
        ps.setString(posiSP++, String.valueOf(wvarWDB_AC_DESCRI));

        wvarStep = 760;
        
        
        ps.setString(posiSP++, String.valueOf(wvarWDB_AC_DEPRE));

      }

      wvarStep = 780;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {

        wvarStep = 790;
        if ("0".isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt("0"));
        }
        
        wvarStep = 840;
        if ("0".isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt("0"));
        }
        
        wvarStep = 850;
        ps.setString(posiSP++, String.valueOf(" "));

        wvarStep = 860;
        ps.setString(posiSP++, String.valueOf(" "));
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;

      // hasta aca accesorios
      wvarStep = 870;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 880;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 890;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 900;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 910;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 920;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 930;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 940;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 950;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 960;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 970;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      //   cotitulares
      for( wvarCounter = 1; wvarCounter <= 10; wvarCounter++ )
      {
    	  wvarStep = 980;
    	  ps.setString(posiSP++, String.valueOf(" "));

    	  wvarStep = 990;
    	  ps.setString(posiSP++, String.valueOf(" "));
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      //   hasta aca cotitulares
      wvarStep = 1000;
      
      if (wvarWDB_AUMARCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUMARCOD));
      }
      
	      wvarStep = 1010;
      
      if (wvarWDB_AUMODCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUMODCOD));
      }
      
	      wvarStep = 1020;
      
      if (wvarWDB_AUSUBCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUSUBCOD));
      }
      
	      wvarStep = 1030;
      
      if (wvarWDB_AUADICOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUADICOD));
      }
      
	      wvarStep = 1040;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_AUMODORI));
	      wvarStep = 1050;
      
      if (wvarWDB_FABRICAN.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FABRICAN));
      }
      
	      wvarStep = 1060;
      
      if (wvarWDB_AUTIPCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUTIPCOD));
      }
      
	      wvarStep = 1070;
      
      if (wvarWDB_AUKLMNUM.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUKLMNUM));
      }
      
	      wvarStep = 1080;
      // Inicio parche
      String sumaAseg = String.valueOf( Obj.toDouble( wvarWDB_SUMAASEG ) / 100 );

      String sumaAsegConvertido;
      if (sumaAseg.indexOf(".") != -1) {
    	  sumaAsegConvertido = sumaAseg.substring(0, sumaAseg.indexOf("."));
      } else {
    	  sumaAsegConvertido = sumaAseg;
      }
      // Fin parche
      if (sumaAsegConvertido.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(sumaAsegConvertido));
      }
      
	      wvarStep = 1090;
      // Inicio parche
      String sumaLba = String.valueOf( Obj.toDouble( wvarWDB_SUMALBA ) / 100 );

      String sumaLbaConvertido;
      if (sumaLba.indexOf(".") != -1) {
        sumaLbaConvertido = sumaLba.substring(0, sumaLba.indexOf("."));
      } else {
        sumaLbaConvertido = sumaLba;
      }
      // Fin parche
      
      if (sumaLbaConvertido.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(sumaLbaConvertido));
      }
      
      wvarStep = 1100;
      
      if (wvarWDB_AUUSOCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUUSOCOD));
      }
      
	      wvarStep = 1110;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_AUTIPCOM));
	      wvarStep = 1120;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CHASISNUM));
	      wvarStep = 1130;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_MOTORNUM));
	      wvarStep = 1140;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_PATENNUM));
	      wvarStep = 1150;
      
      if (wvarWDB_ANOCOMP.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_ANOCOMP));
      }
      
	      wvarStep = 1160;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_GARAJE_FLG));
	      wvarStep = 1170;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_GARAJE_PART));
	      wvarStep = 1180;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CEROKM_FLG));
	      
      if (wvarWDB_AUCATCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_AUCATCOD));
      }
	      wvarStep = 1190;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_AUUSOGNC));
	      wvarStep = 1200;
      
      if (wvarWDB_COLOR.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_COLOR));
      }
      

      wvarStep = 1210;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 1220;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CLIENAP1));
	      wvarStep = 1230;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CLIENAP2));
	      wvarStep = 1240;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CLIENNOM));
	      wvarStep = 1250;
      
      if (wvarWDB_DOCUMTIP.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_DOCUMTIP));
      }
      
	      wvarStep = 1260;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOCUMDAT));
	      wvarStep = 1270;
      
      if (wvarWDB_FECNTO_DD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECNTO_DD));
      }
      
	      wvarStep = 1280;
      
      if (wvarWDB_FECNTO_MM.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECNTO_MM));
      }
      
	      wvarStep = 1290;
      
      if (wvarWDB_FECNTO_AAAA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECNTO_AAAA));
      }
      
	      wvarStep = 1300;
      
      if (wvarWDB_EDADCLI.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_EDADCLI));
      }
      
	      wvarStep = 1310;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_ESTCIVCLI));
	      wvarStep = 1320;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_SEXOCLI));
	      wvarStep = 1330;
      
      if (wvarWDB_LEGAJO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_LEGAJO));
      }
      
	      wvarStep = 1340;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_IVASITCLI));
	      wvarStep = 1350;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_PREFIJO));

      wvarStep = 1360;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_TELEFXXX));
	      wvarStep = 1370;
      
      if (wvarWDB_FECALTA_DIA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECALTA_DIA));
      }
      
	      wvarStep = 1380;
      
      if (wvarWDB_FECALTA_MES.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECALTA_MES));
      }
      
	      wvarStep = 1390;
      
      if (wvarWDB_FECALTA_ANO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECALTA_ANO));
      }
      
	      wvarStep = 1400;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 1410;
      
      ps.setString(posiSP++, String.valueOf(" "));

      wvarStep = 1420;
      
      if (wvarWDB_ANOLICEN.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_ANOLICEN));
      }
      
	      // aca vienen los hijos
      // DATOS DE CONDUCTORES ADICIONALES
      wvarStep = 1430;
      wobjXMLList = wobjXMLRequest.selectNodes( mcteParam_HIJOS ) ;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarWDB_APELLIDO_HIJO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_APELLIDO_HIJO )  );
        wvarWDB_NOMBRE_HIJO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_NOMBRE_HIJO )  );
        wvarWDB_SEXO_HIJO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_SEXO_HIJO )  );
        wvarWDB_NACIM_FEC_HIJO_DIA = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_NACIM_FEC_HIJO_DIA )  );
        wvarWDB_NACIM_FEC_HIJO_MES = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_NACIM_FEC_HIJO_MES )  );
        wvarWDB_NACIM_FEC_HIJO_ANO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_NACIM_FEC_HIJO_ANO )  );
        wvarWDB_EDAD_HIJO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_EDAD_HIJO )  );
        wvarWDB_ESTCIV_HIJO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_ESTCIV_HIJO )  );
        wvarWDB_INCLUIDO_HIJO = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvarCounter), mcteParam_WDB_INCLUIDO_HIJO )  );

        wvarStep = 1440;
        
        
        ps.setString(posiSP++, String.valueOf(wvarWDB_APELLIDO_HIJO));

        wvarStep = 1450;
        
        
        ps.setString(posiSP++, String.valueOf(wvarWDB_NOMBRE_HIJO));

        wvarStep = 1460;
        
        
        ps.setString(posiSP++, String.valueOf(wvarWDB_SEXO_HIJO));

        wvarStep = 1470;
        
        
        
        if (wvarWDB_NACIM_FEC_HIJO_DIA.isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_NACIM_FEC_HIJO_DIA));
        }

        wvarStep = 1480;
        
        if (wvarWDB_NACIM_FEC_HIJO_MES.isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_NACIM_FEC_HIJO_MES));
        }
        
        
        wvarStep = 1490;
        
        if (wvarWDB_NACIM_FEC_HIJO_ANO.isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_NACIM_FEC_HIJO_ANO));
        }
        
        wvarStep = 1500;
        
        if (wvarWDB_EDAD_HIJO.isEmpty()) {
      	  ps.setNull(posiSP++, Types.NUMERIC);
        } else {
      	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_EDAD_HIJO));
        }
        
        wvarStep = 1510;
        
        
        ps.setString(posiSP++, String.valueOf(" "));

        wvarStep = 1520;
        
        ps.setString(posiSP++, String.valueOf(wvarWDB_INCLUIDO_HIJO));
        
        
      }

      wvarStep = 1530;
      for( wvarCounter = wobjXMLList.getLength() + 1; wvarCounter <= 10; wvarCounter++ )
      {
        wvarStep = 1540;
        ps.setString(posiSP++, String.valueOf(" "));
        wvarStep = 1550;
        ps.setString(posiSP++, String.valueOf(" "));
        wvarStep = 1560;
        ps.setString(posiSP++, String.valueOf(" "));
        wvarStep = 1570;
        ps.setInt(posiSP++, 0);
        
        wvarStep = 1580;
        ps.setInt(posiSP++, 0);
        
        wvarStep = 1590;
        ps.setInt(posiSP++, 0);
        
        wvarStep = 1600;
        ps.setInt(posiSP++, 0);
        
        wvarStep = 1610;
        ps.setString(posiSP++, String.valueOf(" "));
        wvarStep = 1620;
        ps.setInt(posiSP++, 0);
        
        
      }

      wobjXMLList = (org.w3c.dom.NodeList) null;
      //   hasta aca hijitos
      wvarStep = 1630;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 1640;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 1650;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 1660;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CALLESEL));
	      wvarStep = 1670;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICDNU));
	      wvarStep = 1680;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICPIS));
	      wvarStep = 1690;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICPTA));
	      wvarStep = 1700;
      
      if (wvarWDB_CODPOST_RGO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_CODPOST_RGO));
      }
      
	      wvarStep = 1750;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_LOCALIDAD));
	      wvarStep = 1760;
      
      if (wvarWBD_CODPROV_RGO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWBD_CODPROV_RGO));
      }
      
	      wvarStep = 1770;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICDOMCOM));
	      wvarStep = 1780;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICDNUCO));
	      wvarStep = 1790;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICPISCO));
	      wvarStep = 1800;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICPTACO));
	      wvarStep = 1820;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICPOBCO));
	      wvarStep = 1830;
      
      if (wvarWDB_DOMICCPOCO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_DOMICCPOCO));
      }
      
	      wvarStep = 1840;
      
      if (wvarWDB_PROVICODCO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_PROVICODCO));
      }
      
	      wvarStep = 1850;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICPRECO));
	      wvarStep = 1860;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_DOMICTLFCO));
	      wvarStep = 1870;
      
      if (wvarWDB_CODINST.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_CODINST));
      }
      
	      wvarStep = 1900;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 1910;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CODTRANS));
	      wvarStep = 1920;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_RAMOPCOD));
	      wvarStep = 1930;
      
      if (wvarWDB_VIGENDIA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_VIGENDIA));
      }
      
	      wvarStep = 1940;
      
      if (wvarWDB_VIGENMES.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_VIGENMES));
      }
      
	      wvarStep = 1950;
      
      if (wvarWDB_VIGENANN.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_VIGENANN));
      }
      
	      wvarStep = 2960;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2970;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2980;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 1990;
      
      if (wvarWDB_CODI_ZONA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_CODI_ZONA));
      }
      
	      wvarStep = 2000;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2010;
      
      ps.setDouble(posiSP++, Double.parseDouble(wvarWDB_PRECIO_MENSUAL));
      
	      wvarStep = 2020;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2030;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2040;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2050;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2060;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2070;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2080;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2090;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2100;
      
      if (wvarWDB_SINIESTROS.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_SINIESTROS));
      }
      
	      wvarStep = 2110;
      
      if (wvarWDB_PLANCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_PLANCOD));
      }
      
	      wvarStep = 2120;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_FRANQCOD));
	      wvarStep = 2125;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CLUBLBA));
	      wvarStep = 2130;
      
      if (wvarWDB_EMISDIA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_EMISDIA));
      }
      
	      wvarStep = 2140;
      
      if (wvarWDB_EMISMES.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_EMISMES));
      }
      
	      wvarStep = 2150;
      
      if (wvarWDB_EMISANN.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_EMISANN));
      }
      

      wvarStep = 2190;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_CAMPANA));
	      wvarStep = 2200;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_PRODUCTOR));
	      wvarStep = 2210;
      
      if (wvarWDB_BANCOCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_BANCOCOD));
      }
      
	      wvarStep = 2220;
      
      if (wvarWDB_SUCURCOD.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_SUCURCOD));
      }
      
	      wvarStep = 2260;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2270;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2280;
      
      if (wvarCot_Nro.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarCot_Nro));
      }
      
	      wvarStep = 2290;
      
      if (wvarWDB_CODIGO_COBRO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_CODIGO_COBRO));
      }
      
	      wvarStep = 2310;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_TIPO_COBRO));
	      wvarStep = 2320;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2330;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2340;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2350;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      wvarStep = 2360;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2370;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2380;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2390;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2400;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      

      wvarStep = 2410;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_VEHDES));
	      wvarStep = 2420;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_NROCTA));
	      wvarStep = 2430;
      
      if (wvarWDB_CODBANCO.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_CODBANCO));
      }
      
	      wvarStep = 2440;
      
      if (wvarWDB_SUCCTA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_SUCCTA));
      }
      
	      wvarStep = 2450;
      
      if (wvarWDB_FECHAVTOCTA_DIA.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECHAVTOCTA_DIA));
      }
      
	      wvarStep = 2460;
      
      if (wvarWDB_FECHAVTOCTA_MES.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECHAVTOCTA_MES));
      }
      
	      wvarStep = 2470;
      
      if (wvarWDB_FECHAVTOCTA_ANN.isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_FECHAVTOCTA_ANN));
      }
      
	      wvarStep = 2480;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_TELEFONO));
	      wvarStep = 2490;
      // COBERTURAS
      for( wvarCounter = 1; wvarCounter <= 30; wvarCounter++ )
      {
    	  wvarWDB_COBERCOD = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_COBERCOD + wvarCounter )  );
    	  wvarWDB_COBERORD = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_COBERORD + wvarCounter )  );
    	  wvarWDB_CAPITASG = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_CAPITASG + wvarCounter )  );
    	  wvarWDB_CAPITIMP = XmlDomExtended.getText( wobjXMLCoberturas.selectSingleNode( mcteParam_WDB_CAPITIMP + wvarCounter )  );

    	  wvarStep = 2500;
    	  
    	  if (wvarWDB_COBERCOD.isEmpty()) {
        	  ps.setNull(posiSP++, Types.NUMERIC);
          } else {
        	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_COBERCOD));
          }
    	  
    	  

    	  wvarStep = 2510;
    	  
    	  if (wvarWDB_COBERORD.isEmpty()) {
        	  ps.setNull(posiSP++, Types.NUMERIC);
          } else {
        	  ps.setInt(posiSP++, Integer.parseInt(wvarWDB_COBERORD));
          }
    	  
    	  

    	  wvarStep = 2520;
    	  
    	  double capitaSg = Double.parseDouble(wvarWDB_CAPITASG.replace(",", "."));
          ps.setDouble(posiSP++, capitaSg);
    	  
          
    	  

    	  wvarStep = 2530;
    	  
    	  double capiTimp = Double.parseDouble(wvarWDB_CAPITIMP.replace(",", "."));
          ps.setDouble(posiSP++, capiTimp);
          
    	  
    	  
      }

      wvarStep = 2540;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_EMAIL));
	      wvarStep = 2550;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_USUARIO_RED));
	      wvarStep = 2560;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_ALARMA));
	      wvarStep = 2565;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2566;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2567;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2568;
      
      if ("0".isEmpty()) {
    	  ps.setNull(posiSP++, Types.NUMERIC);
      } else {
    	  ps.setInt(posiSP++, Integer.parseInt("0"));
      }
      
	      wvarStep = 2570;
      
      ps.setString(posiSP++, String.valueOf(" "));
	      //11/12/2006 - DA: Datos exigidos por la UIF
      wvarStep = 2571;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_OCUPACODCLIE));
	      wvarStep = 2572;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_UIFCUIT));
	      wvarStep = 2573;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_SWAPODER));
      
      //FIN: Datos exigidos por la UIF
      //Agregado por Green Products
      wvarStep = 2574;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_GREEN));
	      wvarStep = 2575;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_GRANIZO));
	      wvarStep = 2576;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_ROBOCONT));
      
      //-- MMC 2012-11-29
      
      //Se setea en null campos que no se utilizan actualmente.
      for (int posiNull = 0; posiNull < 21; posiNull++) {
    	  ps.setNull(posiSP++, Types.CHAR);
      }
      
      //Movido por orden de los campos en el SP.
      wvarStep = 2126;
      
      ps.setString(posiSP++, String.valueOf(wvarWDB_LUNETA));
      
      
      //Se setea en null campos que no se utilizan actualmente.
      ps.setNull(posiSP++, Types.NUMERIC);
      
      for (int posiNull = 0; posiNull < 2; posiNull++) {
    	  ps.setNull(posiSP++, Types.CHAR);
      }
      ps.setString(posiSP++, String.valueOf(wvarWDB_AGECLA));
      
      //-- --
      //FIN: Green Products
      wvarStep = 2580;
      //unsup wobjDBCmd.NamedParameters = true;
//      wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdStoredProc );
      boolean result = ps.execute();
      int returnValue = ps.getInt(1);
      
      if (result) {
    	  //El primero es un ResultSet
      } else {
    	  //Salteo el "updateCount"
    	  int uc = ps.getUpdateCount();
    	  if (uc  == -1) {
//    		  System.out.println("No hay resultados");
    	  }
    	  result = ps.getMoreResults();
      }
      wvarStep = 2590;
      if(returnValue >= 0 )
      {
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><COT_NRO>" + wvarCot_Nro + "</COT_NRO></Response>" );
      }
      else
      {
        
        if(returnValue == -10 )
        {
          wvarMensaje = "ERROR AL OBTENER CADENA-ESLABON";
        }
        else if(returnValue == -11 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA REL_CENTRAL";
        }
        else if(returnValue == -12 )
        {
          wvarMensaje = "ERROR AL ACTUALIZAR LA MARCA DE ULTIMO REGISTR EN LA TABLA REL_CENTRAL";
        }
        else if(returnValue == -13 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA ADM_AGENDA";
        }
        else if(returnValue == -14 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA ADM_RECAHZO";
        }
        else if(returnValue == -15 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 1";
        }
        else if(returnValue == -16 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 2";
        }
        else if(returnValue == -17 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 3";
        }
        else if(returnValue == -18 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 4";
        }
        else if(returnValue == -19 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 5";
        }
        else if(returnValue == -20 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 6";
        }
        else if(returnValue == -21 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 7";
        }
        else if(returnValue == -22 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 8";
        }
        else if(returnValue == -23 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 9";
        }
        else if(returnValue == -24 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ACCESORIO NRO 10";
        }
        else if(returnValue == -25 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_ACREEDOR";
        }
        else if(returnValue == -26 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 1";
        }
        else if(returnValue == -27 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 2";
        }
        else if(returnValue == -28 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 3";
        }
        else if(returnValue == -29 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 4";
        }
        else if(returnValue == -30 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 5";
        }
        else if(returnValue == -31 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 6";
        }
        else if(returnValue == -32 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 7";
        }
        else if(returnValue == -33 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 8";
        }
        else if(returnValue == -34 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 9";
        }
        else if(returnValue == -35 )
        {
          wvarMensaje = "ERROR AL INGRESAR EL ADICIONAL NRO 10";
        }
        else if(returnValue == -36 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_AUTO";
        }
        else if(returnValue == -37 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CLIENTE";
        }
        else if(returnValue == -38 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CONDUCTORES";
        }
        else if(returnValue == -39 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CONSEJO_PROFESIONAL";
        }
        else if(returnValue == -40 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_DOMICILIOS";
        }
        else if(returnValue == -41 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_GENERAL";
        }
        else if(returnValue == -42 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_INSPECCION";
        }
        else if(returnValue == -43 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_REFERIDO";
        }
        else if(returnValue == -44 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_CUENTA";
        }
        else if(returnValue == -45 )
        {
          wvarMensaje = "ERROR AL INGRESAR DATOS EN LA TABLA OPE_COBERTURAS";
        }
        wvarStep = 2600;
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + wvarMensaje + " /></Response>" );
      }

      wvarStep = 2610;
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
