package com.qbe.services.ofVirtualLBA.impl;

public class ModGeneral
{
  public static final String gcteDB = "lbawA_OfVirtualLBA.udl";
  /**
   *  Parametros XML del Cotizador
   */
  public static final String gcteParamFileName = "ParametrosCotizador.xml";
  public static final String gcteNodosHogar = "//HOGAR";
  public static final String gcteNodosAutoScoring = "//AUTOSCORING";
  public static final String gcteRAMOPCOD = "/RAMOPCOD";
  public static final String gctePLANNCOD = "/PLANNCOD";
  public static final String gctePOLIZSEC = "/POLIZSEC";
  public static final String gctePOLIZANN = "/POLIZANN";
  public static final String gcteBANCOCOD = "/BANCOCOD";
  public static final String gcteSUCURCOD = "/SUCURCOD";
}
