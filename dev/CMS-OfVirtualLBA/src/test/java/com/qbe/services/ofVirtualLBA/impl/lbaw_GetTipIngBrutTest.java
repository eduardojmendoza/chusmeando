package com.qbe.services.ofVirtualLBA.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;
import com.qbe.vbcompat.string.StringHolder;

public class lbaw_GetTipIngBrutTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParametrosNoTrimeados() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		StringHolder sh =  new StringHolder();
		lbaw_GetTipIngBrut gti = new lbaw_GetTipIngBrut();
		gti.IAction_Execute("<Request> <PAISSCOD>   00</PAISSCOD>    <V_PROVICOD>    2</V_PROVICOD>    <RAMOPCOD>    AUS1       </RAMOPCOD>    <CLIENIVA>    1</CLIENIVA>    </Request>", sh , "");
//		System.out.println(sh);
		assertTrue(sh.getValue().contains("<Estado resultado=\"true\""));
	}

	@Test
	public void testParametrosTrimeados() throws CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		StringHolder sh =  new StringHolder();
		lbaw_GetTipIngBrut gti = new lbaw_GetTipIngBrut();
		gti.IAction_Execute("<Request> <PAISSCOD>00</PAISSCOD><V_PROVICOD>2</V_PROVICOD><RAMOPCOD>AUS1</RAMOPCOD><CLIENIVA>1</CLIENIVA></Request>", sh , "");
//		System.out.println(sh);
	}
}
