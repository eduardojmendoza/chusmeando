package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_BlanquearClave
 *  File Name : camA_BlanquearClave.cls
 *  Creation Date: 14/12/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Blanquea la clave NT del usuario, utilizando el componente de PORINI
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_BlanquearClave implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_BlanquearClave";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_EMAIL = "//Email";
  static final String mcteParam_DOMINIO = "//Dominio";
  static final String mcteParam_RESULTADO = "//@RESULTADO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Genera un nuevo identificador y envia mitad por mail mitad a pantalla.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    Object wobjClass = null;
    HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    boolean wvarModificarIdentificador = false;
    int wvarStep = 0;
    boolean wvarPwdReset = false;
    boolean wvarEmailOK = false;
    String wvarClave = "";
    String wvarMitadASP = "";
    String wvarMitadEmail = "";
    String wvarRequestRESET = "";
    String wvarResponseRESET = "";
    String wvarUSUARIO = "";
    String wvarMailAEnviar = "";
    String wvarResultASP = "";
    String wvarResultEnviarMail = "";
    String wvarEMAIL = "";
    String wvarDomain = "";




    //wvarDomain = "externo\"
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      System.out.println( wvarUSUARIO );
      wvarDomain = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DOMINIO )  );
      System.out.println( wvarDomain );
      wobjXMLRequest = null;

      wvarStep = 20;
      wvarClave = ModRandom.generar_RNDSTR( new Variant( 8 )/*warning: ByRef value change will be lost.*/ );
      System.out.println( wvarClave );

      wvarStep = 30;
      //    wvarRequestRESET = "<HSBC_MSG>"
      //    wvarRequestRESET = wvarRequestRESET & "<HEADER>"
      //    wvarRequestRESET = wvarRequestRESET & "<APPLICATION_CODE>AuthenticateUser</APPLICATION_CODE>"
      //    wvarRequestRESET = wvarRequestRESET & "<ACTION_CODE>AuthenticateUser</ACTION_CODE>"
      //wvarRequestRESET = wvarRequestRESET & "<ACTION_CODE>NTUserManager</ACTION_CODE>"
      //    wvarRequestRESET = wvarRequestRESET & "</HEADER>"
      //    wvarRequestRESET = wvarRequestRESET & "<BODY>"
      wvarRequestRESET = wvarRequestRESET + "<Request>";
      wvarRequestRESET = wvarRequestRESET + "<ActionCode>AuthenticateUser</ActionCode>";
      //Produccion
      wvarRequestRESET = wvarRequestRESET + "<Command>ResetUserPassword</Command>";
      //wvarRequestRESET = wvarRequestRESET & "<Command>GetUser</Command>"
      //wvarRequestRESET = wvarRequestRESET & "<ActionCode>ResetUserPassword</ActionCode>"
      wvarRequestRESET = wvarRequestRESET + "<record>";
      wvarRequestRESET = wvarRequestRESET + "<name>" + wvarUSUARIO + "</name>";
      wvarRequestRESET = wvarRequestRESET + "<password>" + wvarClave + "</password>";
      wvarRequestRESET = wvarRequestRESET + "</record>";
      wvarRequestRESET = wvarRequestRESET + "</Request>";
      //    wvarRequestRESET = wvarRequestRESET & "</BODY>"
      //    wvarRequestRESET = wvarRequestRESET & "</HSBC_MSG>"
      System.out.println( wvarRequestRESET );


      //   App.LogEvent wvarRequestRESET
      wvarStep = 40;
      wobjClass = new WD.AuthenticateUser();
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //unsup: Call wobjClass.Execute(wvarRequestRESET, wvarResponseRESET, "")
      wobjClass = null;

      //  App.LogEvent wvarResponseRESET
      //    wvarResponseRESET = "<resultado><error>2245</error><GENERAL><ESTADO RESULTADO=" & Chr(34) & "FALSE" & Chr(34) & " MENSAJE=" & Chr(34) & "(2245 0x8C5) The password does not meet the password policy requirements. Check the minimum password length, password complexity and password history requirements." & Chr(34) & " CODIGO=" & Chr(34) & "2245" & Chr(34) & "/><Response/></GENERAL></resultado>"
      //    wvarResponseRESET = "<resultado><error>0</error><GENERAL><ESTADO RESULTADO=" & Chr(34) & "TRUE" & Chr(34) & " MENSAJE=" & Chr(34) & Chr(34) & " CODIGO=" & Chr(34) & "0" & Chr(34) & "/><Response><record><user_id>2637</user_id><name>rappoll0114654936</name><full_name>rappoll0114654936</full_name><priv>1</priv><group>USER</group><password_expired>1</password_expired></record></Response></GENERAL></resultado>"
      //    wvarResponseRESET = "<Response><Estado resultado="" mensaje=""  />" & wvarResponseRESET & "</Response>"
      wvarStep = 50;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarResponseRESET );
      wvarPwdReset = Boolean.valueOf( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( mcteParam_RESULTADO )  ) ).booleanValue();
      System.out.println( wvarPwdReset );

      wvarStep = 60;
      if( wvarPwdReset )
      {

        wvarStep = 70;
        wvarMitadASP = wvarClave.substring( 0, 0 + 4 );
        System.out.println( wvarMitadASP );

        wvarStep = 80;
        wvarMitadEmail = wvarClave.substring( 4, wvarClave.length() );
        System.out.println( wvarMitadEmail );

        wvarStep = 90;
        wvarResultASP = "<Response><Estado resultado=" + String.valueOf( (char)(39) ) + "TRUE" + String.valueOf( (char)(39) ) + " mensaje=" + String.valueOf( (char)(39) ) + "Password Blanqueado. 1ra Mitad. La segunda fue por mail." + String.valueOf( (char)(39) ) + "  /><MediaClave>" + wvarMitadASP + "</MediaClave></Response>";

        wvarStep = 100;
        wobjClass = new Variant( new camA_ObtenerEmail() )new camA_ObtenerEmail().toObject();

        wvarStep = 110;
        //error: function 'Execute' was not found.
        //unsup: Call wobjClass.Execute(Strings.replace(Request, wvarDomain, ""), wvarEMAIL, "")
        wvarStep = 120;
        wobjClass = null;

        wvarStep = 130;
        wobjXMLResponse = new XmlDomExtended();
        wobjXMLResponse.loadXML( wvarEMAIL );
        wvarEmailOK = Boolean.valueOf( XmlDomExtended .getText( wobjXMLResponse.selectSingleNode( mcteParam_RESULTADO.toLowerCase() )  ) ).booleanValue();
        if( wvarEmailOK )
        {
          wvarEMAIL = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( mcteParam_EMAIL )  );
        }


        if( wvarEmailOK )
        {
          wvarStep = 140;
          wvarMailAEnviar = "";
          wvarMailAEnviar = "<Request>";
          wvarMailAEnviar = wvarMailAEnviar + "<FORMAT_FILE>BlanqueoClave</FORMAT_FILE>";
          wvarMailAEnviar = wvarMailAEnviar + "<TO><![CDATA[" + wvarEMAIL + "]]></TO>";
          wvarMailAEnviar = wvarMailAEnviar + "<SUBJECT><![CDATA[Blanqueo de Clave]]></SUBJECT>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAMETROS>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAMETRO>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAM_NOMBRE><![CDATA[%%USUARIO%%]]></PARAM_NOMBRE>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAM_VALOR><![CDATA[" + wvarUSUARIO/*args should be chars wvarUSUARIO.replace(wvarDomain, "") */ + "]]></PARAM_VALOR>";
          wvarMailAEnviar = wvarMailAEnviar + "</PARAMETRO>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAMETRO>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAM_NOMBRE><![CDATA[%%MEDIACLAVE%%]]></PARAM_NOMBRE>";
          wvarMailAEnviar = wvarMailAEnviar + "<PARAM_VALOR>" + wvarMitadEmail + "</PARAM_VALOR>";
          wvarMailAEnviar = wvarMailAEnviar + "</PARAMETRO>";
          wvarMailAEnviar = wvarMailAEnviar + "</PARAMETROS>";
          wvarMailAEnviar = wvarMailAEnviar + "</Request>";

          wvarStep = 150;
          wobjClass = new Variant( new camA_EnviarMail() )new camA_EnviarMail().toObject();
          //error: function 'Execute' was not found.
          //unsup: Call wobjClass.Execute(wvarMailAEnviar, wvarResultEnviarMail, "")
          wobjClass = null;
        }
        else
        {

          wvarStep = 160;
          wvarResultASP = "<resultado>" + wvarEMAIL + "</resultado>";

        }
      }
      else
      {

        wvarStep = 170;
        wvarResultASP = wvarResponseRESET/*args should be chars wvarResponseRESET.replace("<ESTADO RESULTADO=" + String.valueOf( (char)(39) ) + String.valueOf( (char)(39) ) + " MENSAJE=" + String.valueOf( (char)(39) ) + String.valueOf( (char)(39) ) + "  />", "<Estado resultado=" + String.valueOf( (char)(39) ) + "FALSE" + String.valueOf( (char)(39) ) + " mensaje=" + String.valueOf( (char)(39) ) + "El Password no fue Blanqueado." + String.valueOf( (char)(39) ) + "  />") */;
        wvarResultASP = wvarResultASP/*args should be chars wvarResultASP.replace("<ESTADO RESULTADO=", "<Estado resultado=") */;
        wvarResultASP = wvarResultASP/*args should be chars wvarResultASP.replace(" MENSAJE=", " mensaje=") */;

      }

      wvarStep = 180;
      Response.set( wvarResultASP );

      wvarStep = 190;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 200;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 210;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 220;
      wobjHSBC_DBCnn = (HSBCInterfaces.IDBConnection) null;
      //
      wvarStep = 230;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 240;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 250;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }

        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = (HSBCInterfaces.IDBConnection) null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
