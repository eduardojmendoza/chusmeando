package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * Private Declare Function timeGetTime Lib "winmm.dll" () As Long
 */

public class ModXMLLogging
{

  public static Variant SaveStepToLog( int strStepNumber, String strDescripcion, String strDestination ) throws Exception
  {
    Variant SaveStepToLog = new Variant();
    XmlDomExtended objXMLDoc = new XmlDomExtended();
    org.w3c.dom.Element objXMLElem = null;


    if( ! (objXMLDoc.load( strDestination )) )
    {
      objXMLDoc.loadXML( "<CORRIDA></CORRIDA>" );
    }

    objXMLElem = objXMLDoc.getDocument().createElement( "PASO" );
    objXMLElem.setAttribute( "numero", String.valueOf( strStepNumber ) );
    objXMLElem.appendChild( objXMLDoc.getDocument().createTextNode( strDescripcion ) );
    objXMLDoc.getDocument().getDocumentElement().appendChild( objXMLElem );

    objXMLDoc.save( strDestination );

    return SaveStepToLog;
  }

  public static String GetLogFileName() throws Exception
  {
    String GetLogFileName = "";
    SYSTEMTIME myTime = new SYSTEMTIME();

    //Randomize
    GetLocalTime( myTime );
    //+ "_" + CStr(CLng(Rnd(1057850) * 10000000)) + ".xml"
    GetLogFileName = System.getProperty("user.dir") + "\\LOG\\camA_CAM30-Log-" + String.valueOf( 0 /*unsup null (*unsup vbGlobals.App *).ThreadID */ ) + "-" + java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.MEDIUM).format( new java.util.Date() )/*args should be chars java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.MEDIUM).format( new java.util.Date() ).replace("/", "-") */ + "_" + String.valueOf( myTime.wHour ) + "-" + String.valueOf( myTime.wMinute ) + "-" + String.valueOf( myTime.wSecond ) + "-" + String.valueOf( myTime.wMilliseconds ) + ".xml";

    return GetLogFileName;
  }

  public static Variant DeleteLogFileName( String strFile ) throws Exception
  {
    Variant DeleteLogFileName = new Variant();
    Object fso = null;

    fso = new Object();
    new java.io.File( strFile ).delete();
    fso = (Object) null;

    return DeleteLogFileName;
  }
}
