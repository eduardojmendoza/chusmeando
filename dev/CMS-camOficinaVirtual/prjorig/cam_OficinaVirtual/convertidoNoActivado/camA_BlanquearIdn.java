package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_BlanquearIdn
 *  File Name : camA_BlanquearIdn.cls
 *  Creation Date: 07/12/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Blanquea el identificador del usuario.
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_BlanquearIdn implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_BlanquearIdn";
  static final String mcteSPResetearIdentificador = "SPCAM_CAM_CAMBIAR_IDENTIFICADOR";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_EMAIL = "//Email";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Genera un nuevo identificador y envia mitad por mail mitad a pantalla.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    HSBCInterfaces.IAction wobjClass = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    boolean wvarModificarIdentificador = false;
    int wvarStep = 0;
    String wvarIDENTIFICADOR = "";
    String wvarMitadASP = "";
    String wvarMitadEmail = "";
    String wvarUSUARIO = "";
    String wvarMailAEnviar = "";
    String wvarResultASP = "";
    String wvarResultEnviarMail = "";
    String wvarEMAIL = "";




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );

      //Set wobjXMLRequest = Nothing
      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPResetearIdentificador );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      wvarIDENTIFICADOR = ModRandom.generar_RNDSTR( new Variant( 8 )/*warning: ByRef value change will be lost.*/ ).toLowerCase();
      // el LCASE() est� puesto porque eChannels solicit� que el identificador sea
      // CASE-INSENSITIVE. De la misma forma solicit� que la longitud del mismo sea
      // de 8 caracteres
      wvarStep = 80;
      wvarMitadASP = wvarIDENTIFICADOR.substring( 0, 0 + 4 );
      System.out.println( wvarMitadASP );

      wvarStep = 90;
      wvarMitadEmail = wvarIDENTIFICADOR.substring( 4, wvarIDENTIFICADOR.length() );
      System.out.println( wvarMitadEmail );

      wvarStep = 100;
      wvarIDENTIFICADOR = ModEncryptDecrypt.CapicomEncrypt( wvarIDENTIFICADOR );

      wvarStep = 110;
      
      

      wvarStep = 120;
      
      

      wvarStep = 130;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 140;
      wobjXMLResponse = new XmlDomExtended();

      if( ! (wrstDBResult.isEOF()) )
      {
        wvarStep = 150;
        wobjXMLResponse.load(AdoUtils.saveRecordset(wrstDBResult));
      }

      wvarStep = 160;
      wvarResultASP = "<Response><Estado resultado=";

      wvarStep = 170;
      wvarResultASP = wvarResultASP + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//z:row/@resultado" )  ) + String.valueOf( (char)(34) );

      wvarStep = 180;
      wvarResultASP = wvarResultASP + " mensaje=";

      wvarStep = 190;
      wvarResultASP = wvarResultASP + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//z:row/@mensaje" )  ) + String.valueOf( (char)(34) );
      wvarResultASP = wvarResultASP + " /><MediaClave>" + wvarMitadASP + "</MediaClave></Response>";

      wvarStep = 200;
      wobjClass = new Variant( new camA_ObtenerEmail() )((HSBCInterfaces.IAction) new camA_ObtenerEmail().toObject());

      wvarStep = 210;
      wobjClass.Execute( Request, wvarEMAIL, "" );

      wvarStep = 220;
      wobjClass = (HSBCInterfaces.IAction) null;

      wvarStep = 230;
      wobjXMLResponse = new XmlDomExtended();
      wobjXMLResponse.loadXML( wvarEMAIL );
      wvarEMAIL = XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( mcteParam_EMAIL )  );

      wvarStep = 240;
      wvarMailAEnviar = "";
      wvarMailAEnviar = "<Request>";
      wvarMailAEnviar = wvarMailAEnviar + "<FORMAT_FILE>BlanqueoIdentificador</FORMAT_FILE>";
      wvarMailAEnviar = wvarMailAEnviar + "<TO><![CDATA[" + wvarEMAIL + "]]></TO>";
      wvarMailAEnviar = wvarMailAEnviar + "<SUBJECT><![CDATA[Blanqueo de Identificador]]></SUBJECT>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAMETROS>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAMETRO>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAM_NOMBRE><![CDATA[%%USUARIO%%]]></PARAM_NOMBRE>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAM_VALOR><![CDATA[" + wvarUSUARIO + "]]></PARAM_VALOR>";
      wvarMailAEnviar = wvarMailAEnviar + "</PARAMETRO>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAMETRO>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAM_NOMBRE><![CDATA[%%MEDIACLAVE%%]]></PARAM_NOMBRE>";
      wvarMailAEnviar = wvarMailAEnviar + "<PARAM_VALOR>" + wvarMitadEmail + "</PARAM_VALOR>";
      wvarMailAEnviar = wvarMailAEnviar + "</PARAMETRO>";
      wvarMailAEnviar = wvarMailAEnviar + "</PARAMETROS>";
      wvarMailAEnviar = wvarMailAEnviar + "</Request>";

      wvarStep = 250;
      wobjClass = new Variant( new camA_EnviarMail() )((HSBCInterfaces.IAction) new camA_EnviarMail().toObject());
      wobjClass.Execute( wvarMailAEnviar, wvarResultEnviarMail, "" );
      wobjClass = (HSBCInterfaces.IAction) null;

      wvarStep = 260;
      Response.set( wvarResultASP );

      wvarStep = 270;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 280;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 290;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 300;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 310;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 320;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 330;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
