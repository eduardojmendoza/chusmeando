package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_ValidarRCC
 *  File Name : camA_ValidarRCC.cls
 *  Creation Date: 29/11/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Valida que los caracteres ingresados para las 3 posiciones del RCC
 *                sean correctos.
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_ValidarRCC implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_ValidarRCC";
  static final String mcteSPObtenerIdentificador = "SPCAM_CAM_OBTENER_IDENTIFICADOR";
  static final String mcteSPGuardarUltimoRCC = "SPCAM_CAM_GUARDAR_ULTIMORCC";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_RCCs = "//rcc";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Esta funcion devuelve TRUE / FALSE seg�n si los tres caracteres recibidos
   *        en el REQUEST coinciden con las 3 posiciones indicadas en el ultimo RCC generado.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLUltimoRCC = null;
    org.w3c.dom.Node wobjXMLRequestNode = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    int wvarStep = 0;
    String wvarIDENTIFICADOR = "";
    String wvarUltimoRCC = "";
    String wvarUSUARIO = "";
    String wvarResult = "";
    int wvarRCC = 0;




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // Leer parametro de entrada
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );

      //Set wobjXMLRequest = Nothing
      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      // Se obtiene el identificador encriptado de la base.
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPObtenerIdentificador );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 80;
      if( ! (wrstDBResult.isEOF()) )
      {
        // Aqui se obtiene el ultimo RCC pedido.
        wvarStep = 90;
        wvarUltimoRCC = wrstDBResult.getField("ultimoRCC").getValue().toString();

        wvarStep = 100;
        if( wvarUltimoRCC.length() > 0 )
        {

          //se desencripta el identificador para poder hacer la comparacion con lo recibido.
          wvarStep = 110;
          wvarIDENTIFICADOR = ModEncryptDecrypt.CapicomDecrypt( wrstDBResult.getField("identificador").getValue().toString() );

          wvarStep = 120;
          wobjXMLUltimoRCC = new XmlDomExtended();
          wobjXMLUltimoRCC.loadXML( wvarUltimoRCC );

          wvarError = false;

          wvarStep = 130;
          for( int nwobjXMLRequestNode = 0; nwobjXMLRequestNode < wobjXMLRequest.selectNodes( mcteParam_RCCs ) .getLength(); nwobjXMLRequestNode++ )
          {
            wobjXMLRequestNode = wobjXMLRequest.selectNodes( mcteParam_RCCs ) .item( nwobjXMLRequestNode );

            wvarStep = 140;
            wvarRCC = Integer.parseInt( XmlDomExtended .getText( wobjXMLUltimoRCC.selectSingleNode( "//rcc[@id=" + XmlDomExtended.getText( wobjXMLRequestNode.getAttributes().item( 0 ) ) + "]" )  ) );

            // comparacion de lo ingresado con el caracter correspondiente
            wvarStep = 150;
            if( !wvarIDENTIFICADOR.substring( wvarRCC-1, wvarRCC-1 + 1 ).equals( XmlDomExtended .getText( wobjXMLRequestNode ) ) )
            {
              wvarError = true;
              break;
            }

          }

          if( ! (wvarError) )
          {
            wvarStep = 160;
            wobjDBCmd = (Command) null;

            wvarStep = 170;
            wobjDBCmd = new Command();

            // Limpio el ultimoRCC porque acert� el actual
            wvarStep = 180;
            wobjDBCmd.setActiveConnection( wobjDBCnn );
            wobjDBCmd.setCommandText( mcteSPGuardarUltimoRCC );
            wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

            wvarStep = 190;
            
            

            wvarStep = 180;
            
            

            wvarStep = 190;
            wobjDBCmd.execute();

            wvarStep = 200;
            wvarResult = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Identificador Validado." + String.valueOf( (char)(34) ) + " /></Response>";
          }
          else
          {
            wvarStep = 210;
            wvarResult = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Identificador no validado." + String.valueOf( (char)(34) ) + " /></Response>";
          }

        }
        else
        {
          wvarStep = 220;
          wvarResult = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No hay un ultimoRCC previo." + String.valueOf( (char)(34) ) + " /></Response>";
        }

        wvarStep = 230;
        Response.set( wvarResult );

      }
      else
      {
        wvarStep = 240;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 250;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 260;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 270;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 280;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 290;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 300;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 310;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
