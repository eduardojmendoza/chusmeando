package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_RegRCCFallido
 *  File Name : camA_RegRCCFallido.cls
 *  Creation Date: 29/11/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Registra un acceso ERRONEO a RCC. Y lo tiene en cuenta para el bloqueo
 *        temporal.
 * 
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_RegRCCFallido implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_RegRCCFallido";
  static final String mcteSPRegistrarRCCFallido = "SPCAM_CAM_REGISTRAR_RCC_FALLIDO";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Registra los intentos fallidos en el RCC. Utilizada dentro del circuito
   *        de bloqueo temporal.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    int wvarStep = 0;
    String wvarUSUARIO = "";
    int wvarTIPOACCESO = 0;
    int wvarIDSISTEMA = 0;
    String wvarIPADDRESS = "";
    String wvarResult = "";




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // lectura de la entrada
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );

      wobjXMLRequest = null;

      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      // hubo error en el rcc ingresado. Se registra el mismo.
      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPRegistrarRCCFallido );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      wobjDBCmd.execute();

      wvarStep = 80;
      wvarResult = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Intento Fallido Registrado." + String.valueOf( (char)(34) ) + " /></Response>";

      wvarStep = 90;
      Response.set( wvarResult );

      wvarStep = 100;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 110;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 120;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 130;
      wobjHSBC_DBCnn = null;

      wvarStep = 140;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
