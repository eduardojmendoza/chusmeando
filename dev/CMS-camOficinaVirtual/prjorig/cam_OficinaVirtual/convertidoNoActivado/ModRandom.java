package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : modRandom
 *  File Name : modRandom.bas
 *  Creation Date: 01/12/2005
 *  Programmer : Fernando Osores / Lucas De Merlier
 *  Abstract :    Genera un string random, de longitud indicada por parametro.
 *  *****************************************************************
 */

public class ModRandom
{

  public static String generar_RNDSTR( Variant strLength )
  {
    String generar_RNDSTR = "";
    String strCode = "";
    int nroAux = 0;
    // Esta funci�n genera un string de longitud strLength aleatorio.
    // Si la longitud no se especifica, se asume igual a 4.
    try 
    {


      strCode = "";

      Random randomNumberGenerator = new Random();

      if( strLength.toInt() == 0 )
      {
        strLength.set( 4 );
      }

      while( strCode.length() < strLength.toInt() )
      {
        nroAux = (int)Math.rint( Math.floor( (92 * VB.rnd()) + 33 ) );
        if( ((nroAux >= 48) && (nroAux <= 57)) || ((nroAux >= 65) && (nroAux <= 90)) || ((nroAux >= 97) && (nroAux <= 122)) )
        {
          // 48:57 --> 0..9
          // 65:90 --> a..z
          // 97:122 -> A..Z
          strCode = strCode + String.valueOf( (char)(nroAux) );
        }
      }

      generar_RNDSTR = strCode;

      return generar_RNDSTR;

    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        generar_RNDSTR = new Variant().toString();

        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return generar_RNDSTR;
  }
}
