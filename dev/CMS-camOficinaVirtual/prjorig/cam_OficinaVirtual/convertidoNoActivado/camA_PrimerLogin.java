package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_PrimerLogin
 *  File Name : camA_PrimerLogin.cls
 *  Creation Date: 29/11/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Guarda la informaci�n del Primer Login del Usuario. Ya sea la primera vez,
 *        o el reseteo desde eChannels.
 * 
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_PrimerLogin implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_PrimerLogin";
  static final String mcteSPPrimerLogin = "SPCAM_CAM_PRIMER_LOGIN";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_IDENTIFICADOR = "//Identificador";
  static final String mcteParam_EMAIL = "//Email";
  static final String mcteParam_PREGUNTA = "//Pregunta";
  static final String mcteParam_RESPUESTA = "//Respuesta";
  static final String mcteParam_IDSISTEMA = "//IdSistema";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Graba en la base la informaci�n del primer login del usuario.
   *        email, identificador, pregunta y respuesta secreta.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    int wvarStep = 0;
    String wvarUSUARIO = "";
    String wvarIDENTIFICADOR = "";
    String wvarEMAIL = "";
    String wvarPREGUNTA = "";
    String wvarRESPUESTA = "";
    int wvarIDSISTEMA = 0;
    String wvarResult = "";




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarIDENTIFICADOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IDENTIFICADOR )  );
      wvarEMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL )  );
      wvarPREGUNTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PREGUNTA )  );
      wvarRESPUESTA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RESPUESTA )  );
      wvarIDSISTEMA = Integer.parseInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_IDSISTEMA )  ) );

      wobjXMLRequest = null;

      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPPrimerLogin );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      wvarIDENTIFICADOR = ModEncryptDecrypt.CapicomEncrypt( wvarIDENTIFICADOR );

      wvarStep = 80;
      
      

      wvarStep = 90;
      wvarPREGUNTA = ModEncryptDecrypt.CapicomEncrypt( wvarPREGUNTA );

      wvarStep = 100;
      
      

      wvarStep = 110;
      wvarRESPUESTA = ModEncryptDecrypt.CapicomEncrypt( wvarRESPUESTA );

      wvarStep = 120;
      
      

      wvarStep = 130;
      
      

      wvarStep = 140;
      
      

      wvarStep = 150;
      wobjDBCmd.execute();

      wvarStep = 160;
      wvarResult = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Usuario '" + wvarUSUARIO + "' Registrado." + String.valueOf( (char)(34) ) + " /><Usuario>" + wvarUSUARIO + "</Usuario></Response>";

      wvarStep = 170;
      Response.set( wvarResult );

      wvarStep = 180;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 190;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 200;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 210;
      wobjHSBC_DBCnn = null;

      wvarStep = 220;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
