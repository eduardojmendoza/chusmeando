package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_CambiarIdn
 *  File Name : camA_CambiarIdn.cls
 *  Creation Date: 05/12/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Modifica el identificador del usuario, siempre y cuando el identificador
 *        viejo sea ingresado. Caso contrario no se puede modificar el identificador.
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_CambiarIdn implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_CambiarIdn";
  static final String mcteSPCambiarIdentificador = "SPCAM_CAM_CAMBIAR_IDENTIFICADOR";
  static final String mcteSPObtenerIdentificador = "SPCAM_CAM_OBTENER_IDENTIFICADOR";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_IDENTIFICADOR = "//Identificador";
  static final String mcteParam_NEW_IDENTIFICADOR = "//New_Identificador";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Actualiza el identificador previamente solicitando el identificador viejo.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    boolean wvarModificarIdentificador = false;
    int wvarStep = 0;
    String wvarIDENTIFICADOR = "";
    String wvarNEWIDENTIFICADOR = "";
    String wvarUSUARIO = "";
    String wvarResult = "";




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarIDENTIFICADOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IDENTIFICADOR )  );
      wvarNEWIDENTIFICADOR = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NEW_IDENTIFICADOR )  );

      //Set wobjXMLRequest = Nothing
      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPObtenerIdentificador );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      wvarStep = 80;
      if( ! (wrstDBResult.isEOF()) )
      {

        wvarStep = 90;
        if( wvarIDENTIFICADOR.equals( ModEncryptDecrypt.CapicomDecrypt( wrstDBResult.getField("Identificador").getValue().toString() ) ) )
        {
          wvarModificarIdentificador = true;
        }
        else
        {
          wvarModificarIdentificador = false;
        }

        wvarStep = 100;
        wrstDBResult.close();

      }

      wvarStep = 110;
      wrstDBResult = (Recordset) null;

      wvarStep = 120;
      wobjDBCmd = (Command) null;

      wvarStep = 130;
      if( wvarModificarIdentificador )
      {

        wvarStep = 140;
        wobjDBCmd = new Command();

        wvarStep = 150;
        wobjDBCmd.setActiveConnection( wobjDBCnn );
        wobjDBCmd.setCommandText( mcteSPCambiarIdentificador );
        wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

        wvarStep = 160;
        
        

        wvarStep = 170;
        wvarNEWIDENTIFICADOR = ModEncryptDecrypt.CapicomEncrypt( wvarNEWIDENTIFICADOR );

        wvarStep = 180;
        
        

        wvarStep = 190;
        wrstDBResult = wobjDBCmd.execute();
        wrstDBResult.setActiveConnection( (Connection) null );
        //
        wvarStep = 200;
        wobjXMLResponse = new XmlDomExtended();

        if( ! (wrstDBResult.isEOF()) )
        {
          wvarStep = 210;
          wobjXMLResponse.load(AdoUtils.saveRecordset(wrstDBResult));
        }

        wvarStep = 220;
        wvarResult = "<Response><Estado resultado=";

        wvarStep = 230;
        wvarResult = wvarResult + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//z:row/@resultado" )  ) + String.valueOf( (char)(34) );

        wvarStep = 240;
        wvarResult = wvarResult + " mensaje=";

        wvarStep = 250;
        wvarResult = wvarResult + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//z:row/@mensaje" )  ) + String.valueOf( (char)(34) );
        wvarResult = wvarResult + " /></Response>";

      }
      else
      {
        wvarStep = 260;
        wvarResult = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) );
        wvarResult = wvarResult + " mensaje=" + String.valueOf( (char)(34) ) + "El identificador no coincide. El cambio no se ha realizado." + String.valueOf( (char)(34) ) + " /></Response>";
      }
      //
      wvarStep = 270;
      Response.set( wvarResult );

      wvarStep = 280;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 290;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 300;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 310;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 320;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 330;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 340;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
