package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.Err;
import diamondedge.util.Variant;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_CambiarEmail
 *  File Name : camA_CambiarEmail.cls
 *  Creation Date: 06/12/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Actualiza el mail del usuario.
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_CambiarEmail implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_CambiarEmail";
  static final String mcteSPCambiarEmail = "SPCAM_CAM_CAMBIAR_EMAIL";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  static final String mcteParam_EMAIL = "//Email";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Actualiza el email del usuario.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    boolean wvarError = false;
    int wvarStep = 0;
    String wvarEMAIL = "";
    String wvarUSUARIO = "";
    String wvarResult = "";




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );
      wvarEMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL )  );

      //Set wobjXMLRequest = Nothing
      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPCambiarEmail );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      
      

      wvarStep = 80;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );

      wvarStep = 90;
      wobjXMLResponse = new XmlDomExtended();

      if( ! (wrstDBResult.isEOF()) )
      {
        wvarStep = 100;
        wobjXMLResponse.load(AdoUtils.saveRecordset(wrstDBResult));
      }

      wvarStep = 110;
      wvarResult = "<Response><Estado resultado=";

      wvarStep = 120;
      wvarResult = wvarResult + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//z:row/@resultado" )  ) + String.valueOf( (char)(34) );

      wvarStep = 130;
      wvarResult = wvarResult + " mensaje=";

      wvarStep = 140;
      wvarResult = wvarResult + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wobjXMLResponse.selectSingleNode( "//z:row/@mensaje" )  ) + String.valueOf( (char)(34) );
      wvarResult = wvarResult + " /></Response>";

      wvarStep = 150;
      Response.set( wvarResult );

      wvarStep = 160;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 170;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 180;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 190;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 200;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 210;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 220;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
