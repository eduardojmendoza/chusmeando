package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_ObtenerRCC
 *  File Name : camA_ObtenerRCC.cls
 *  Creation Date: 28/11/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Obtiene la longitud del identificador de la DB y determina
 *                3 caracteres para el RCC.
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_ObtenerRCC implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_ObtenerRCC";
  static final String mcteSPObtenerIdentificador = "SPCAM_CAM_OBTENER_IDENTIFICADOR";
  static final String mcteSPGuardarUltimoRCC = "SPCAM_CAM_GUARDAR_ULTIMORCC";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_USUARIO = "//Usuario";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Recibe el usuario como parametro de entrada, y devuelve 3 posiciones del identificador
   *        y la longitud del mismo.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
    int wvarStep = 0;
    String wvarIDENTIFICADOR = "";
    String wvarUltimoRCC = "";
    String wvarUSUARIO = "";
    String wvarResult = "";
    int wvarRCC1 = 0;
    int wvarRCC2 = 0;
    int wvarRCC3 = 0;




    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarUSUARIO = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARIO )  );

      wobjXMLRequest = null;

      wvarStep = 20;
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();

      wvarStep = 30;
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
      wvarStep = 40;
      wobjDBCmd = new Command();

      wvarStep = 50;
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteSPObtenerIdentificador );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

      wvarStep = 60;
      
      

      wvarStep = 70;
      wrstDBResult = wobjDBCmd.execute();
      wrstDBResult.setActiveConnection( (Connection) null );
      //
      wvarStep = 80;
      if( ! (wrstDBResult.isEOF()) )
      {
        //
        wvarStep = 90;
        wvarUltimoRCC = wrstDBResult.getField("ultimoRCC").getValue().toString();

        wvarStep = 100;
        if( wvarUltimoRCC.length() > 0 )
        {
          // La ultima vez que ingres� no acert�.
          wvarResult = wvarUltimoRCC;
        }
        else
        {
          wvarIDENTIFICADOR = ModEncryptDecrypt.CapicomDecrypt( wrstDBResult.getField("identificador").getValue().toString() );

          wvarStep = 110;
          if( wvarIDENTIFICADOR.length() > 3 )
          {

            wvarStep = 120;
            Random randomNumberGenerator = new Random();


            wvarStep = 130;
            wvarRCC1 = (int)Math.rint( Math.floor( ((wvarIDENTIFICADOR.length() - 1) * VB.rnd()) + 1 ) );

            wvarStep = 140;
            wvarRCC2 = (int)Math.rint( Math.floor( ((wvarIDENTIFICADOR.length() - 1) * VB.rnd()) + 1 ) );
            while( wvarRCC2 == wvarRCC1 )
            {
              wvarRCC2 = (int)Math.rint( Math.floor( ((wvarIDENTIFICADOR.length() - 1) * VB.rnd()) + 1 ) );
            }

            wvarStep = 150;
            wvarRCC3 = (int)Math.rint( Math.floor( ((wvarIDENTIFICADOR.length() - 1) * VB.rnd()) + 1 ) );
            while( (wvarRCC3 == wvarRCC1) || (wvarRCC3 == wvarRCC2) )
            {
              wvarRCC3 = (int)Math.rint( Math.floor( ((wvarIDENTIFICADOR.length() - 1) * VB.rnd()) + 1 ) );
            }

          }
          else
          {
            // El identificador no tiene la longitud suficiente
            wvarStep = 160;
            wvarRCC1 = -1;
            wvarRCC2 = -1;
            wvarRCC3 = -1;
          }

          wvarStep = 170;
          wvarResult = "<RCCs longitud=" + String.valueOf( (char)(34) ) + wvarIDENTIFICADOR.length() + String.valueOf( (char)(34) ) + ">";
          wvarResult = wvarResult + "<rcc id=" + String.valueOf( (char)(34) ) + "1" + String.valueOf( (char)(34) ) + ">" + wvarRCC1 + "</rcc>";
          wvarResult = wvarResult + "<rcc id=" + String.valueOf( (char)(34) ) + "2" + String.valueOf( (char)(34) ) + ">" + wvarRCC2 + "</rcc>";
          wvarResult = wvarResult + "<rcc id=" + String.valueOf( (char)(34) ) + "3" + String.valueOf( (char)(34) ) + ">" + wvarRCC3 + "</rcc>";
          wvarResult = wvarResult + "</RCCs>";

          wvarStep = 180;
          wobjDBCmd = (Command) null;

          wvarStep = 190;
          wobjDBCmd = new Command();

          wvarStep = 200;
          wobjDBCmd.setActiveConnection( wobjDBCnn );
          wobjDBCmd.setCommandText( mcteSPGuardarUltimoRCC );
          wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

          wvarStep = 210;
          
          

          wvarStep = 220;
          
          

          wvarStep = 230;
          wobjDBCmd.execute();

        }

        wvarStep = 240;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );

      }
      else
      {
        wvarStep = 250;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "NO SE ENCONTRARON DATOS." + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 260;
      wobjDBCmd = (Command) null;
      //
      wvarStep = 270;
      if( ! (wobjDBCnn == (Connection) null) )
      {
        
        {
          wobjDBCnn.close();
        }
      }
      //
      wvarStep = 280;
      wobjDBCnn = (Connection) null;
      //
      wvarStep = 290;
      wobjHSBC_DBCnn = null;
      //
      wvarStep = 300;
      if( ! (wrstDBResult == (Recordset) null) )
      {
        
        {
          wrstDBResult.close();
        }
      }
      //
      wvarStep = 310;
      wrstDBResult = (Recordset) null;
      //
      wvarStep = 320;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( ! (wrstDBResult == (Recordset) null) )
        {
          
          {
            wrstDBResult.close();
          }
        }
        wrstDBResult = (Recordset) null;
        //
        wobjDBCmd = (Command) null;
        //
        if( ! (wobjDBCnn == (Connection) null) )
        {
          
          {
            wobjDBCnn.close();
          }
        }
        wobjDBCnn = (Connection) null;
        //
        wobjHSBC_DBCnn = null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * Private Function p_GetXSL() As String
   *     Dim wvarStrXSL  As String
   *     '
   *     wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
   *     wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
   *     wvarStrXSL = wvarStrXSL & "  <xsl:element name='SOLICITUD'>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='RAMOPCOD'><xsl:value-of select='@RAMOPCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='POLIZANN'><xsl:value-of select='@POLIZANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='POLIZSEC'><xsl:value-of select='@POLIZSEC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CERTIPOL'><xsl:value-of select='@CERTIPOL' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CERTIANN'><xsl:value-of select='@CERTIANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CERTISEC'><xsl:value-of select='@CERTISEC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUPLENUM'><xsl:value-of select='@SUPLENUM' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FRANQCOD'><xsl:value-of select='@FRANQCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='HIJOS1416'><xsl:value-of select='@HIJOS1416' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='HIJOS1729'><xsl:value-of select='@HIJOS1729' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='HIJOS1729ND'><xsl:value-of select='@HIJOS1729_EXC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ZONA'><xsl:value-of select='@ZONA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CODPROV'><xsl:value-of select='@CODPROV' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUMALBA'><xsl:value-of select='@SUMALBA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLUB_LBA'><xsl:value-of select='@CLUB_LBA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CPAANO'><xsl:value-of select='@CPAANO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CTAKMS'><xsl:value-of select='@CTAKMS' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESCERO'><xsl:value-of select='@ESCERO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIENEPLAN'><xsl:value-of select='@TIENEPLAN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='COND_ADIC'><xsl:value-of select='@COND_ADIC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ASEG_ADIC'><xsl:value-of select='@ASEG_ADIC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FH_NAC'><xsl:value-of select='@FH_NAC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SEXO'><xsl:value-of select='@SEXO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTCIV'><xsl:value-of select='@ESTCIV' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SIFMVEHI_DES'><xsl:value-of select='@SIFMVEHI_DES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROFECOD'><xsl:value-of select='@PROFECOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SIACCESORIOS'><xsl:value-of select='@ACCESORIOS' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='REFERIDO'><xsl:value-of select='@REFERIDO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MOTORNUM'><xsl:value-of select='normalize-space(@MOTORNUM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CHASINUM'><xsl:value-of select='normalize-space(@CHASINUM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PATENNUM'><xsl:value-of select='normalize-space(@PATENNUM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMARCOD'><xsl:value-of select='@AUMARCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMODCOD'><xsl:value-of select='@AUMODCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUSUBCOD'><xsl:value-of select='@AUSUBCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUADICOD'><xsl:value-of select='@AUADICOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMODORI'><xsl:value-of select='@AUMODORI' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUUSOCOD'><xsl:value-of select='@AUUSOCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVCOD'><xsl:value-of select='@AUVTVCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVDIA'><xsl:value-of select='@AUVTVDIA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVMES'><xsl:value-of select='@AUVTVMES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUVTVANN'><xsl:value-of select='@AUVTVANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='VEHCLRCOD'><xsl:value-of select='@VEHCLRCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUKLMNUM'><xsl:value-of select='@AUKLMNUM' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FABRICAN'><xsl:value-of select='@FABRICAN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FABRICMES'><xsl:value-of select='@FABRICMES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='GUGARAGE'><xsl:value-of select='@GUGARAGE' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='GUDOMICI'><xsl:value-of select='@GUDOMICI' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUCATCOD'><xsl:value-of select='@AUCATCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUTIPCOD'><xsl:value-of select='@AUTIPCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUCIASAN'><xsl:value-of select='normalize-space(@AUCIASAN)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUANTANN'><xsl:value-of select='@AUANTANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUNUMSIN'><xsl:value-of select='@AUNUMSIN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUNUMKMT'><xsl:value-of select='@AUNUMKMT' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUUSOGNC'><xsl:value-of select='@AUUSOGNC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='normalize-space(@USUARCOD)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SITUCPOL'><xsl:value-of select='@SITUCPOL' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMEDOCU'><xsl:value-of select='normalize-space(@NUMEDOCU)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBROFOR'><xsl:value-of select='@COBROFOR' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EDADACTU'><xsl:value-of select='@EDADACTU' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CAMP_CODIGO'><xsl:value-of select='@CAMP_CODIGO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CAMP_DESC'><xsl:value-of select='@CAMP_DESC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LEGAJO_GTE'><xsl:value-of select='normalize-space(@LEGAJO_GTE)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NRO_PROD'><xsl:value-of select='@NRO_PROD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUCURSAL_CODIGO'><xsl:value-of select='@SUCURSAL_CODIGO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LEGAJO_VEND'><xsl:value-of select='normalize-space(@LEGAJO_VEND)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTAINSP'><xsl:value-of select='@ESTAINSP' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPEANN'><xsl:value-of select='@INSPEANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPEMES'><xsl:value-of select='@INSPEMES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPEDIA'><xsl:value-of select='@INSPEDIA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPECTOR'><xsl:value-of select='normalize-space(@INSPEDES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPECOD'><xsl:value-of select='@INSPECOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPEADOM'><xsl:value-of select='@INSPADOM' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='OBSERTXT'><xsl:value-of select='normalize-space(@INSPEOBS)' /></xsl:element>"
   *     'wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPENUM'><xsl:value-of select='@INSPENUM' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CENTRCOD'><xsl:value-of select='normalize-space(@CENSICOD)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CENTRDES'><xsl:value-of select='normalize-space(@CENSIDES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPEKMS'><xsl:value-of select='@INSPEKMS' /></xsl:element>"
   *     'wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSPECC'><xsl:value-of select='@INSPECC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='RASTREO'><xsl:value-of select='@DISPOCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ALARMCOD'><xsl:value-of select='normalize-space(@PRESTCOD)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INS_DOMICDOM'><xsl:value-of select='normalize-space(@INS_DOMICDOM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INS_CPACODPO'><xsl:value-of select='normalize-space(@INS_CPACODPO)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INS_DOMICPOB'><xsl:value-of select='normalize-space(@INS_DOMICPOB)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INS_PROVICOD'><xsl:value-of select='@INS_PROVICOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INS_DOMICTLF'><xsl:value-of select='normalize-space(@INS_DOMICTLF)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INS_RANGOHOR'><xsl:value-of select='@INS_RANGOHOR' /></xsl:element>"
   * 
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMEDOCU_ACRE'><xsl:value-of select='normalize-space(@NUMEDOCU_ACRE)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPODOCU_ACRE'><xsl:value-of select='@TIPODOCU_ACRE' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='APELLIDO'><xsl:value-of select='normalize-space(@APELLIDO)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NOMBRES'><xsl:value-of select='normalize-space(@NOMBRES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICDOM'><xsl:value-of select='normalize-space(@DOMICDOM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICDNU'><xsl:value-of select='normalize-space(@DOMICDNU)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICESC'><xsl:value-of select='normalize-space(@DOMICESC)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPIS'><xsl:value-of select='normalize-space(@DOMICPIS)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPTA'><xsl:value-of select='normalize-space(@DOMICPTA)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPOB'><xsl:value-of select='normalize-space(@DOMICPOB)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICCPO'><xsl:value-of select='@DOMICCPO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVICOD'><xsl:value-of select='@PROVICOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PAISSCOD'><xsl:value-of select='normalize-space(@PAISSCOD)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PQTDES'><xsl:value-of select='normalize-space(@PQTDES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PLANCOD'><xsl:value-of select='@PLANCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENNOM'><xsl:value-of select='normalize-space(@CLIENNOM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENAP1'><xsl:value-of select='normalize-space(@CLIENAP1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENAP2'><xsl:value-of select='normalize-space(@CLIENAP2)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVIDES'><xsl:value-of select='@PROVIDES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICPOB_SOLI'><xsl:value-of select='normalize-space(@DOMICPOB_SOLI)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICCPO_SOLI'><xsl:value-of select='@DOMICCPO_SOLI' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROFEDES'><xsl:value-of select='normalize-space(@PROFEDAB)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DESCCOLO'><xsl:value-of select='@DESCCOLO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='BANCOCOD'><xsl:value-of select='normalize-space(@BANCOCOD)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBROCOD'><xsl:value-of select='@COBROCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EFECTDIA'><xsl:value-of select='@EFECTDIA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EFECTMES'><xsl:value-of select='@EFECTMES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EFECTANN'><xsl:value-of select='@EFECTANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBRODES'><xsl:value-of select='normalize-space(@COBRODES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ENTDOMSC'><xsl:value-of select='@ENTDOMSC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ANOTACIO'><xsl:value-of select='normalize-space(@ANOTACIO)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SUMAASEG'><xsl:value-of select='@SUMAASEG' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PRECIO_MENSUAL'><xsl:value-of select='@PRECIO_MENSUAL' /></xsl:element>"
   *     'wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOCUMDAB'><xsl:value-of select='@DOCUMDAB' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOCUMDAB_ACRE'><xsl:value-of select='@DOCUMDAB_ACRE' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUMARDES'><xsl:value-of select='@AUMARDES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PROVIDES_ACRE'><xsl:value-of select='@PROVIDES_ACRE' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUITNUME'><xsl:value-of select='normalize-space(@CUITNUME)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='RAZONSOC'><xsl:value-of select='normalize-space(@RAZONSOC)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIEIBTP'><xsl:value-of select='@CLIEIBTP' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NROIIBB'><xsl:value-of select='normalize-space(@NROIIBB)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUNETA'><xsl:value-of select='@LUNETA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIIBBDES'><xsl:value-of select='normalize-space(@CIBBDESC)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='IVADES'><xsl:value-of select='normalize-space(@CIVADESC)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMAIL'><xsl:value-of select='normalize-space(@EMAIL)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENIVA'><xsl:value-of select='@CLIENIVA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DERIVACI'><xsl:value-of select='@DERIVACI' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ALEATORIO'><xsl:value-of select='@ALEATORIO' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NROFACTU'><xsl:value-of select='normalize-space(@NROFACTU)' /></xsl:element>"
   *     'DDJJ
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADO'><xsl:value-of select='substring(@DDJJ,1,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='USO'><xsl:value-of select='substring(@DDJJ,2,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ABOLLADO'><xsl:value-of select='substring(@DDJJ,3,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PICADO'><xsl:value-of select='substring(@DDJJ,4,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MASILLADO'><xsl:value-of select='substring(@DDJJ,5,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROTO'><xsl:value-of select='substring(@DDJJ,6,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FALTANTES'><xsl:value-of select='substring(@DDJJ,7,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUCES'><xsl:value-of select='substring(@DDJJ,8,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ODOMETRO'><xsl:value-of select='normalize-space(substring(@DDJJ,9,10))' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PARAGOLPE'><xsl:value-of select='substring(@DDJJ,19,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PAR_DEL'><xsl:value-of select='substring(@DDJJ,20,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PAR_TRA'><xsl:value-of select='substring(@DDJJ,21,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_CAPOT'><xsl:value-of select='substring(@DDJJ,22,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_TECHO'><xsl:value-of select='substring(@DDJJ,23,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_TBAUL'><xsl:value-of select='substring(@DDJJ,24,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUARDABARRO'><xsl:value-of select='substring(@DDJJ,25,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUAR_DEL'><xsl:value-of select='substring(@DDJJ,26,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUAR_DEL_DER'><xsl:value-of select='substring(@DDJJ,27,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUAR_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,28,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUAR_TRA'><xsl:value-of select='substring(@DDJJ,29,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUAR_TRA_DER'><xsl:value-of select='substring(@DDJJ,30,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_GUAR_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,31,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUERTA'><xsl:value-of select='substring(@DDJJ,32,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUER_DEL'><xsl:value-of select='substring(@DDJJ,33,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUER_DEL_DER'><xsl:value-of select='substring(@DDJJ,34,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUER_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,35,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUER_TRA'><xsl:value-of select='substring(@DDJJ,36,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUER_TRA_DER'><xsl:value-of select='substring(@DDJJ,37,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AB_PUER_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,38,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_CAPOT'><xsl:value-of select='substring(@DDJJ,39,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_TECHO'><xsl:value-of select='substring(@DDJJ,40,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_TBAUL'><xsl:value-of select='substring(@DDJJ,41,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUARDABARRO'><xsl:value-of select='substring(@DDJJ,42,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUAR_DEL'><xsl:value-of select='substring(@DDJJ,43,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUAR_DEL_DER'><xsl:value-of select='substring(@DDJJ,44,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUAR_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,45,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUAR_TRA'><xsl:value-of select='substring(@DDJJ,46,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUAR_TRA_DER'><xsl:value-of select='substring(@DDJJ,47,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_GUAR_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,48,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUERTA'><xsl:value-of select='substring(@DDJJ,49,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUER_DEL'><xsl:value-of select='substring(@DDJJ,50,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUER_DEL_DER'><xsl:value-of select='substring(@DDJJ,51,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUER_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,52,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUER_TRA'><xsl:value-of select='substring(@DDJJ,53,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUER_TRA_DER'><xsl:value-of select='substring(@DDJJ,54,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_PUER_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,55,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_ZOCALO'><xsl:value-of select='substring(@DDJJ,56,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_ZOC_DER'><xsl:value-of select='substring(@DDJJ,57,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PIC_ZOC_IZQ'><xsl:value-of select='substring(@DDJJ,58,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_CAPOT'><xsl:value-of select='substring(@DDJJ,59,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_TECHO'><xsl:value-of select='substring(@DDJJ,60,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_TBAUL'><xsl:value-of select='substring(@DDJJ,61,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUARDABARRO'><xsl:value-of select='substring(@DDJJ,62,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUAR_DEL'><xsl:value-of select='substring(@DDJJ,63,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUAR_DEL_DER'><xsl:value-of select='substring(@DDJJ,64,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUAR_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,65,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUAR_TRA'><xsl:value-of select='substring(@DDJJ,66,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUAR_TRA_DER'><xsl:value-of select='substring(@DDJJ,67,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_GUAR_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,68,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUERTA'><xsl:value-of select='substring(@DDJJ,69,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUER_DEL'><xsl:value-of select='substring(@DDJJ,70,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUER_DEL_DER'><xsl:value-of select='substring(@DDJJ,71,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUER_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,72,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUER_TRA'><xsl:value-of select='substring(@DDJJ,73,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUER_TRA_DER'><xsl:value-of select='substring(@DDJJ,74,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_PUER_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,75,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_ZOCALO'><xsl:value-of select='substring(@DDJJ,76,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_ZOC_DER'><xsl:value-of select='substring(@DDJJ,77,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='MAS_ZOC_IZQ'><xsl:value-of select='substring(@DDJJ,78,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_PARAGOLPE'><xsl:value-of select='substring(@DDJJ,79,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_PAR_DEL'><xsl:value-of select='substring(@DDJJ,80,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_PAR_TRA'><xsl:value-of select='substring(@DDJJ,81,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_REJILLA'><xsl:value-of select='substring(@DDJJ,82,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_OPTICA'><xsl:value-of select='substring(@DDJJ,83,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_OPT_DER'><xsl:value-of select='substring(@DDJJ,84,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_OPT_IZQ'><xsl:value-of select='substring(@DDJJ,85,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FAROGIRO'><xsl:value-of select='substring(@DDJJ,86,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FG_DEL'><xsl:value-of select='substring(@DDJJ,87,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FG_DEL_DER'><xsl:value-of select='substring(@DDJJ,88,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FG_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,89,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FG_TRA'><xsl:value-of select='substring(@DDJJ,90,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FG_TRA_DER'><xsl:value-of select='substring(@DDJJ,91,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_FG_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,92,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_LUNETA'><xsl:value-of select='substring(@DDJJ,93,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ROT_PARABRISAS'><xsl:value-of select='substring(@DDJJ,94,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_PARAGOLPE'><xsl:value-of select='substring(@DDJJ,95,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_PAR_DEL'><xsl:value-of select='substring(@DDJJ,96,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_PAR_TRA'><xsl:value-of select='substring(@DDJJ,97,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_REJILLA'><xsl:value-of select='substring(@DDJJ,98,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_OPTICA'><xsl:value-of select='substring(@DDJJ,99,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_OPT_DER'><xsl:value-of select='substring(@DDJJ,100,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_OPT_IZQ'><xsl:value-of select='substring(@DDJJ,101,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FAROGIRO'><xsl:value-of select='substring(@DDJJ,102,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FG_DEL'><xsl:value-of select='substring(@DDJJ,103,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FG_DEL_DER'><xsl:value-of select='substring(@DDJJ,104,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FG_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,105,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FG_TRA'><xsl:value-of select='substring(@DDJJ,106,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FG_TRA_DER'><xsl:value-of select='substring(@DDJJ,107,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FAL_FG_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,108,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_ALTA'><xsl:value-of select='substring(@DDJJ,109,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_BAJA'><xsl:value-of select='substring(@DDJJ,110,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIRO'><xsl:value-of select='substring(@DDJJ,111,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIR_DEL'><xsl:value-of select='substring(@DDJJ,112,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIR_DEL_DER'><xsl:value-of select='substring(@DDJJ,113,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIR_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,114,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIR_TRA'><xsl:value-of select='substring(@DDJJ,115,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIR_TRA_DER'><xsl:value-of select='substring(@DDJJ,116,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_GIR_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,117,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POSICION'><xsl:value-of select='substring(@DDJJ,118,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POS_DEL'><xsl:value-of select='substring(@DDJJ,119,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POS_DEL_DER'><xsl:value-of select='substring(@DDJJ,120,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POS_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,121,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POS_TRA'><xsl:value-of select='substring(@DDJJ,122,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POS_TRA_DER'><xsl:value-of select='substring(@DDJJ,123,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_POS_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,124,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='LUC_STOP'><xsl:value-of select='substring(@DDJJ,125,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NEU_EST_DEL_DER'><xsl:value-of select='substring(@DDJJ,126,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NEU_EST_DEL_IZQ'><xsl:value-of select='substring(@DDJJ,127,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NEU_EST_TRA_DER'><xsl:value-of select='substring(@DDJJ,128,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NEU_EST_TRA_IZQ'><xsl:value-of select='substring(@DDJJ,129,1)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FUNCKIL'><xsl:value-of select='substring(@DDJJ,130,1)' /></xsl:element>"
   *     'DDJJ
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMISIDIA'><xsl:value-of select='@EMISIDIA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMISIMES'><xsl:value-of select='@EMISIMES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMISIANN'><xsl:value-of select='@EMISIANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTAIDES'><xsl:value-of select='normalize-space(@ESTAIDES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DISPODES'><xsl:value-of select='normalize-space(@DISPODES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INSTALADP'><xsl:value-of select='normalize-space(@INSTALADP)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='POSEEDISP'><xsl:value-of select='normalize-space(@POSEEDISP)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='AUTIPGAMA'><xsl:value-of select='normalize-space(@AUTIPGAMA)' /></xsl:element>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "  </xsl:element>"
   *     wvarStrXSL = wvarStrXSL & " </xsl:template>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='RAMOPCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='FRANQCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SIFMVEHI_DES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROFECOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='MOTORNUM'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CHASINUM'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PATENNUM'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUMODORI'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUCIASAN'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='USUARCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SITUCPOL'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NUMEDOCU'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICSEC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUENTSEC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CAMP_DESC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LEGAJO_GTE'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='LEGAJO_VEND'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NUMEDOCU_ACRE'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='APELLIDO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NOMBRES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICDOM'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICDNU'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICESC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPIS'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPTA'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPOB'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PAISSCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PQTDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENNOM'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENAP1'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENAP2'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROVIDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOMICPOB_SOLI'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROFEDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DESCCOLO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='BANCOCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COBRODES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ANOTACIO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DOCUMDAB_ACRE'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PROVIDES_ACRE'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUMARDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUITNUME'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='RAZONSOC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIEIBTP'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NROIIBB'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIIBBDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='IVADES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='EMAIL'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INSPECTOR'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CENTRCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CENTRDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='OBSERTXT'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INS_DOMICDOM'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INS_CPACODPO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INS_DOMICPOB'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INS_DOMICTLF'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NROFACTU'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DDJJ_STRING'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESTAIDES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DISPODES'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INSTALADP'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='POSEEDISP'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='AUTIPGAMA'/>"
   *     wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
   *     '
   *     p_GetXSL = wvarStrXSL
   * End Function
   * 
   * 
   * Private Function p_GetXSLHijos() As String
   *     Dim wvarStrXSL  As String
   *     '
   *     wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
   *     wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
   *     wvarStrXSL = wvarStrXSL & "  <xsl:element name='HIJO'>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='APELLIDOHIJO'><xsl:value-of select='normalize-space(@CONDUAPE)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NOMBREHIJO'><xsl:value-of select='normalize-space(@CONDUNOM)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMHIJO'><xsl:value-of select=""substring(concat('00000000',string(@CONDUFEC)),string-length(string(@CONDUFEC)) + 1)""/></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='SEXOHIJO'><xsl:value-of select='@CONDUSEX' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='ESTADOHIJO'><xsl:value-of select='@CONDUEST' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='INCLUIDO'><xsl:value-of select='@CONDUEXC' /></xsl:element>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "  </xsl:element>"
   *     wvarStrXSL = wvarStrXSL & " </xsl:template>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='APELLIDOHIJO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NOMBREHIJO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='SEXOHIJO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='ESTADOHIJO'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='INCLUIDO'/>"
   *     wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
   *     '
   *     p_GetXSLHijos = wvarStrXSL
   * End Function
   * 
   * Private Function p_GetXSLAcc() As String
   *     Dim wvarStrXSL  As String
   *     '
   *     wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
   *     wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
   *     wvarStrXSL = wvarStrXSL & "  <xsl:element name='ACCESORIO'>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CODIGOACC'><xsl:value-of select='@AUACCCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='PRECIOACC'><xsl:value-of select='@AUVEASUM' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DESCRIPCIONACC'><xsl:value-of select='normalize-space(@AUVEADES)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DEPRECIA'><xsl:value-of select='@AUVEADEP' /></xsl:element>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "  </xsl:element>"
   *     wvarStrXSL = wvarStrXSL & " </xsl:template>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DESCRIPCIONACC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='DEPRECIA'/>"
   *     wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
   *     '
   *     p_GetXSLAcc = wvarStrXSL
   * End Function
   * 
   * Private Function p_GetXSLAseg() As String
   *     Dim wvarStrXSL  As String
   *     '
   *     wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
   *     wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
   *     wvarStrXSL = wvarStrXSL & "  <xsl:element name='ASEGURADO'>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOCUMDAT'><xsl:value-of select='@DOCUMDAT' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='NOMBREAS'><xsl:value-of select='normalize-space(@NOMBREAS)' /></xsl:element>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "  </xsl:element>"
   *     wvarStrXSL = wvarStrXSL & " </xsl:template>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='NOMBREAS'/>"
   *     wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
   *     '
   *     p_GetXSLAseg = wvarStrXSL
   * End Function
   * 
   * Private Function p_GetXSLCuentas(pCUENTSEC) As String
   *     Dim wvarStrXSL  As String
   *     '
   *     wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
   *     wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row[@CUENTSEC = """ & pCUENTSEC & """]'>"
   *     '
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENTSEC'><xsl:value-of select='@CUENTSEC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPOCUEN'><xsl:value-of select='@TIPOCUEN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='COBROTIP'><xsl:value-of select='@COBROTIP' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FPBANCOCOD'><xsl:value-of select='@BANCOCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='FPSUCURCOD'><xsl:value-of select='@SUCURCOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENTDC'><xsl:value-of select='@CUENTDC' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='CUENNUME'><xsl:value-of select='normalize-space(@CUENNUME)' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='TARJECOD'><xsl:value-of select='@TARJECOD' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='VENCIANN'><xsl:value-of select='@VENCIANN' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='VENCIMES'><xsl:value-of select='@VENCIMES' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='VENCIDIA'><xsl:value-of select='@VENCIDIA' /></xsl:element>"
   *     wvarStrXSL = wvarStrXSL & "      <xsl:element name='TARJEDES'><xsl:value-of select='normalize-space(@COBRODES)' /></xsl:element>"
   *     '
   *     wvarStrXSL = wvarStrXSL & " </xsl:template>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='USUARCOD'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TIPOCUEN'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='COBROTIP'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUENTDC'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CUENNUME'/>"
   *     wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='TARJEDES'/>"
   *     wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
   *     '
   *     p_GetXSLCuentas = wvarStrXSL
   * End Function
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
