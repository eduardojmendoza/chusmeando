package com.qbe.services.cam30;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.camOficinaVirtual.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_CAM30
 *  File Name : camA_CAM30.cls
 *  Creation Date: 27/12/2005
 *  Programmer : Fernando Osores
 *  Abstract :    Hace de interface con el componente de PORINI
 *  *****************************************************************
 * Objetos del FrameWork
 */

public class camA_CAM30 implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "cam_OficinaVirtual.camA_CAM30";
  /**
   * Parametros XML de Entrada
   * Const mcteParam_NTUSERMANAGER      As String = "WD.NTUserManager"
   */
  static final String mcteParam_COMMAND = "//Command";
  static final String mcteParam_SYSTEM = "//System";
  static final String mcteParam_NAME = "//name";
  static final String mcteParam_PASSWORD = "//password";
  static final String mcteParam_SQLUSERMANAGER = "WD.SQLUserManager";
  static final String mcteParam_AUTHENTICATEUSER = "WD.AuthenticateUser";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Genera un nuevo identificador y envia mitad por mail mitad a pantalla.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    Object wobjClass = null;
    HSBCInterfaces.IDBConnection wobjHSBC_DBCnn = null;
    boolean wvarError = false;
    int wvarStep = 0;
    String wvarSYSTEM = "";
    String wvarPASSWORD = "";
    String wvarNAME = "";
    String wvarNAME_new = "";
    String wvarCOMMAND = "";
    String wvarRequestAU = "";
    String wvarResponseAU = "";
    String wvarComponente = "";
    String strFileName = "";

    //    Dim wobjDBCnn           As ADODB.Connection
    //    Dim wobjDBCmd           As ADODB.Command
    //    Dim wrstDBResult        As ADODB.Recordset
    //    Dim wobjDBParm          As ADODB.Parameter
    //------------------------------------------
    //Agregado para LOG Adicional de ERROR
    //FJO. 15/06/2006
    //------------------------------------------
    strFileName = ModXMLLogging.GetLogFileName();
    //------------------------------------------
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( 0, "INICIO " + new java.util.Date().toString() + " " + new java.util.Date().toString(), strFileName );
      //------------------------------------------
      wvarStep = 10;
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "Instancio MSXML2.DOMDocument", strFileName );
      //------------------------------------------
      wobjXMLRequest = new XmlDomExtended();
      wvarStep = 20;
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "Cargo el Request en el objeto MSXML2.DOMDocument", strFileName );
      //------------------------------------------
      wobjXMLRequest.loadXML( Request );

      wvarStep = 30;
      wvarCOMMAND = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_COMMAND )  );
      System.out.println( wvarCOMMAND );
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "wvarCOMMAND=" + wvarCOMMAND, strFileName );
      //------------------------------------------
      wvarStep = 40;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_SYSTEM )  == (org.w3c.dom.Node) null) )
      {
        wvarSYSTEM = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SYSTEM )  );
      }
      else
      {
        wvarSYSTEM = "";
      }
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "wvarSYSTEM=" + wvarSYSTEM, strFileName );
      //------------------------------------------
      wvarStep = 50;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_PASSWORD )  == (org.w3c.dom.Node) null) )
      {
        wvarPASSWORD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PASSWORD )  );
      }
      else
      {
        wvarPASSWORD = "";
      }
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "wvarPASSWORD=(***************)", strFileName );
      //------------------------------------------
      wvarStep = 60;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_NAME )  == (org.w3c.dom.Node) null) )
      {
        wvarNAME = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NAME )  );
      }
      else
      {
        wvarNAME = "";
      }
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "wvarNAME=" + wvarNAME, strFileName );
      //------------------------------------------
      wobjXMLRequest = null;
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep + 1, "Libero MSXML2.DOMDocument", strFileName );
      //------------------------------------------
      wvarStep = 70;
      if( wvarSYSTEM.toUpperCase().equals( "OVCAM" ) )
      {
        wvarComponente = mcteParam_SQLUSERMANAGER;
        if( (wvarNAME.indexOf( "\\", 0 ) + 1) > 0 )
        {
          wvarNAME_new = wvarNAME.substring( (wvarNAME-1.indexOf( "\\", 0 ) + 1) + 1, wvarNAME.length() );
          Request = Request/*args should be chars Request.replace(wvarNAME, wvarNAME_new) */;
        }
        if( (wvarCOMMAND.toUpperCase().equals( "SETUSERPASSWORD" )) || (wvarCOMMAND.toUpperCase().equals( "RESETUSERPASSWORD" )) )
        {
          Request = Request/*args should be chars Request.replace(wvarPASSWORD, ModEncryptDecrypt.CapicomEncrypt( wvarPASSWORD )) */;
        }
      }
      else
      {
        if( (wvarCOMMAND.toUpperCase().equals( "SETUSERPASSWORD" )) || (wvarCOMMAND.toUpperCase().equals( "RESETUSERPASSWORD" )) )
        {
          wvarComponente = mcteParam_AUTHENTICATEUSER;
          //            wvarComponente = mcteParam_NTUSERMANAGER
        }
        else
        {
          wvarComponente = mcteParam_AUTHENTICATEUSER;
        }
      }
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "Determino el componente a invocar", strFileName );
      //------------------------------------------
      wvarStep = 80;
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "Instancio " + wvarComponente, strFileName );
      //------------------------------------------
      //mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, mcteClassName, wcteFnName, wvarStep, 1, "Se inicia el llamado a componente " & wvarComponente, vbLogEventTypeInformation
      wobjClass = new wvarComponente();
      //Set wobjClass = CreateObject(wvarComponente)
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep + 1, "Voy a ejecutar " + wvarComponente, strFileName );
      //------------------------------------------
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(Request, wvarResponseAU, "")
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep + 2, "ejecute " + wvarComponente, strFileName );
      //------------------------------------------
      wobjClass = null;

      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep + 3, "Libero " + wvarComponente, strFileName );
      //------------------------------------------
      //mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, mcteClassName, wcteFnName, wvarStep, 1, "Ha terminado el llamado a componente " & wvarComponente, vbLogEventTypeInformation
      wvarStep = 90;
      Response.set( wvarResponseAU );
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "Devuelve respuesta" + wvarComponente, strFileName );
      //------------------------------------------
      wvarStep = 100;
      //------------------------------------------
      //Agregado para LOG Adicional de ERROR
      //FJO. 15/06/2006
      //------------------------------------------
      ModXMLLogging.SaveStepToLog( wvarStep, "FIN " + new java.util.Date().toString() + " " + new java.util.Date().toString(), strFileName );
      ModXMLLogging.DeleteLogFileName( strFileName );
      //------------------------------------------
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjHSBC_DBCnn = (HSBCInterfaces.IDBConnection) null;

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 0 /*unsup Err.getError().Number */, "Error= [" + 0 /*unsup Err.getError().Number */ + "] - " + Err.getError().toString(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
