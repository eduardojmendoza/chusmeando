Attribute VB_Name = "ModXMLLogging"
Option Explicit
'Private Declare Function timeGetTime Lib "winmm.dll" () As Long

Private Declare Sub GetLocalTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)

Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Function SaveStepToLog(strStepNumber As Long, strDescripcion As String, strDestination As String)

    Dim objXMLDoc As New MSXML2.DOMDocument
    Dim objXMLElem As MSXML2.IXMLDOMElement
    
    If Not objXMLDoc.Load(strDestination) Then
        objXMLDoc.loadXML ("<CORRIDA></CORRIDA>")
    End If
    
    Set objXMLElem = objXMLDoc.createElement("PASO")
    objXMLElem.setAttribute "numero", CStr(strStepNumber)
    objXMLElem.appendChild objXMLDoc.createTextNode(strDescripcion)
    objXMLDoc.documentElement.appendChild objXMLElem
    
    objXMLDoc.Save strDestination

End Function

Public Function GetLogFileName() As String

    'Randomize
    Dim myTime As SYSTEMTIME
    GetLocalTime myTime
    GetLogFileName = App.Path + "\LOG\camA_CAM30-Log-" + CStr(App.ThreadID) + "-" + Replace(Format(Date), "/", "-") + "_" + CStr(myTime.wHour) + "-" + CStr(myTime.wMinute) + "-" + CStr(myTime.wSecond) + "-" + CStr(myTime.wMilliseconds) + ".xml" '+ "_" + CStr(CLng(Rnd(1057850) * 10000000)) + ".xml"

End Function


Public Function DeleteLogFileName(strFile As String)

    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    fso.DeleteFile strFile, True
    Set fso = Nothing

End Function



