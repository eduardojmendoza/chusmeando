VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_PrimerLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_PrimerLogin
' File Name : camA_PrimerLogin.cls
' Creation Date: 29/11/2005
' Programmer : Fernando Osores
' Abstract :    Guarda la información del Primer Login del Usuario. Ya sea la primera vez,
'       o el reseteo desde eChannels.
'
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "cam_OficinaVirtual.camA_PrimerLogin"
Const mcteSPPrimerLogin             As String = "SPCAM_CAM_PRIMER_LOGIN"

'Parametros XML de Entrada
Const mcteParam_USUARIO         As String = "//Usuario"
Const mcteParam_IDENTIFICADOR   As String = "//Identificador"
Const mcteParam_EMAIL           As String = "//Email"
Const mcteParam_PREGUNTA        As String = "//Pregunta"
Const mcteParam_RESPUESTA       As String = "//Respuesta"
Const mcteParam_IDSISTEMA       As String = "//IdSistema"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Graba en la base la información del primer login del usuario.
'       email, identificador, pregunta y respuesta secreta.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"

    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter

    Dim wvarError           As Boolean

    Dim wvarStep            As Long
    Dim wvarUSUARIO         As String
    Dim wvarIDENTIFICADOR   As String
    Dim wvarEMAIL           As String
    Dim wvarPREGUNTA        As String
    Dim wvarRESPUESTA       As String
    Dim wvarIDSISTEMA       As Integer
    Dim wvarResult          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~

    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
        wvarIDENTIFICADOR = .selectSingleNode(mcteParam_IDENTIFICADOR).Text
        wvarEMAIL = .selectSingleNode(mcteParam_EMAIL).Text
        wvarPREGUNTA = .selectSingleNode(mcteParam_PREGUNTA).Text
        wvarRESPUESTA = .selectSingleNode(mcteParam_RESPUESTA).Text
        wvarIDSISTEMA = CInt(.selectSingleNode(mcteParam_IDSISTEMA).Text)
    End With

    Set wobjXMLRequest = Nothing

    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")

    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)

    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")

    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteSPPrimerLogin
        .CommandType = adCmdStoredProc
    End With

    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    wvarIDENTIFICADOR = CapicomEncrypt(wvarIDENTIFICADOR)
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDENTIFICADOR", adVarChar, adParamInput, 300, wvarIDENTIFICADOR)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    wvarPREGUNTA = CapicomEncrypt(wvarPREGUNTA)
    
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PREGUNTA_SECRETA", adVarChar, adParamInput, 600, wvarPREGUNTA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    wvarRESPUESTA = CapicomEncrypt(wvarRESPUESTA)
    
    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RESPUESTA_SECRETA", adVarChar, adParamInput, 600, wvarRESPUESTA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAILSMS", adVarChar, adParamInput, 255, wvarEMAIL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDSISTEMA", adInteger, adParamInput, 4, wvarIDSISTEMA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 150
    wobjDBCmd.Execute

    wvarStep = 160
    wvarResult = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & "Usuario '" & wvarUSUARIO & "' Registrado." & Chr(34) & " /><Usuario>" & wvarUSUARIO & "</Usuario></Response>"

    wvarStep = 170
    Response = wvarResult

    wvarStep = 180
    Set wobjDBCmd = Nothing
    '
    wvarStep = 190
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 200
    Set wobjDBCnn = Nothing
    '
    wvarStep = 210
    Set wobjHSBC_DBCnn = Nothing

    wvarStep = 220
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub





