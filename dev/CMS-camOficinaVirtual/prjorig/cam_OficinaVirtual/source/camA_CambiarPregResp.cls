VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_CambiarPregResp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_CambiarPregResp
' File Name : camA_CambiarPregResp.cls
' Creation Date: 06/12/2005
' Programmer : Fernando Osores
' Abstract : Cambia la pregunta y respuesta secretas ingresadas por el usuario.
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName            As String = "cam_OficinaVirtual.camA_CambiarPregResp"
Const mcteSPCambiarPregResp    As String = "SPCAM_CAM_CAMBIAR_PREGRESP"

'Parametros XML de Entrada
Const mcteParam_USUARIO        As String = "//Usuario"
Const mcteParam_PREGUNTA       As String = "//Pregunta"
Const mcteParam_RESPUESTA      As String = "//Respuesta"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Graba la pregunta y respuesta secretas ingresadas.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError           As Boolean
    
    Dim wvarStep            As Long
    Dim wvarPREGUNTA        As String
    Dim wvarRESPUESTA       As String
    Dim wvarUSUARIO         As String
    Dim wvarResult          As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Debug.Print Request
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
        wvarPREGUNTA = .selectSingleNode(mcteParam_PREGUNTA).Text
        wvarRESPUESTA = .selectSingleNode(mcteParam_RESPUESTA).Text
    End With
    
    'Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteSPCambiarPregResp
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    wvarPREGUNTA = CapicomEncrypt(wvarPREGUNTA)
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PREGUNTA", adVarChar, adParamInput, 600, wvarPREGUNTA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    wvarRESPUESTA = CapicomEncrypt(wvarRESPUESTA)
    
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RESPUESTA", adVarChar, adParamInput, 600, wvarRESPUESTA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 110
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 120
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    
    If Not wrstDBResult.EOF Then
        wvarStep = 130
        wrstDBResult.Save wobjXMLResponse, adPersistXML
    End If

    wvarStep = 140
    wvarResult = "<Response><Estado resultado="
    
    wvarStep = 150
    wvarResult = wvarResult & Chr(34) & wobjXMLResponse.selectSingleNode("//z:row/@resultado").Text & Chr(34)
    
    wvarStep = 160
    wvarResult = wvarResult & " mensaje="
    
    wvarStep = 170
    wvarResult = wvarResult & Chr(34) & wobjXMLResponse.selectSingleNode("//z:row/@mensaje").Text & Chr(34)
    wvarResult = wvarResult & " /></Response>"

    wvarStep = 180
    Response = wvarResult
    
    wvarStep = 190
    Set wobjDBCmd = Nothing
    '
    wvarStep = 200
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 210
    Set wobjDBCnn = Nothing
    '
    wvarStep = 220
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 230
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 240
    Set wrstDBResult = Nothing
    '
    wvarStep = 250
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub




