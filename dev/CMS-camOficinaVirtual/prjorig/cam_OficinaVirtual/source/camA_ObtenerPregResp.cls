VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_ObtenerPregResp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_ObtenerPregResp
' File Name : camA_ObtenerPregResp.cls
' Creation Date: 05/12/2005
' Programmer : Fernando Osores
' Abstract :    Devuelve la pregunta y respuesta secretas.
'
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "cam_OficinaVirtual.camA_ObtenerPregResp"
Const mcteSPObtenerPregResp    As String = "SPCAM_CAM_OBTENER_PREGRESP"

'Parametros XML de Entrada
Const mcteParam_USUARIO     As String = "//Usuario"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Recibe como parametro el nombre del usuario, y devuelve la pregunta y respuesta secreta.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError           As Boolean
    
    Dim wvarStep            As Long
    Dim wvarPREGUNTA        As String
    Dim wvarUSUARIO         As String
    Dim wvarRESPUESTA       As String
    Dim wvarResult          As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
    End With
    
    'Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteSPObtenerPregResp
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 80
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    
    wvarStep = 90
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    
    If Not wrstDBResult.EOF Then
        
        wvarStep = 100
        wvarResult = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje="
    
        wvarStep = 110
        wvarResult = wvarResult & Chr(34) & "Pregunta y Respuesta Secretas" & Chr(34) & " />"
        
        wvarStep = 120
        wvarResult = wvarResult & "<Pregunta>" & CapicomDecrypt(wrstDBResult("PreguntaSecreta").Value) & "</Pregunta>"
    
        wvarStep = 130
        wvarResult = wvarResult & "<Respuesta>" & CapicomDecrypt(wrstDBResult("RespuestaSecreta").Value) & "</Respuesta>"
        wvarResult = wvarResult & "</Response>"
        
    Else
    
        wvarStep = 140
        wvarResult = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje="
        wvarResult = wvarResult & Chr(34) & "No se ha podido obtener la pregunta y la respuesta." & Chr(34) & " />"
        wvarResult = wvarResult & "</Response>"
        
    End If
    
    wvarStep = 150
    Response = wvarResult
    
    wvarStep = 160
    Set wobjDBCmd = Nothing
    '
    wvarStep = 170
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 180
    Set wobjDBCnn = Nothing
    '
    wvarStep = 190
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 200
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 210
    Set wrstDBResult = Nothing
    '
    wvarStep = 220
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub






