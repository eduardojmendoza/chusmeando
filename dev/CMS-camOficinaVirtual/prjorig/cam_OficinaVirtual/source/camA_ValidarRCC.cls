VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_ValidarRCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_ValidarRCC
' File Name : camA_ValidarRCC.cls
' Creation Date: 29/11/2005
' Programmer : Fernando Osores
' Abstract :    Valida que los caracteres ingresados para las 3 posiciones del RCC
'               sean correctos.
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "cam_OficinaVirtual.camA_ValidarRCC"
Const mcteSPObtenerIdentificador    As String = "SPCAM_CAM_OBTENER_IDENTIFICADOR"
Const mcteSPGuardarUltimoRCC        As String = "SPCAM_CAM_GUARDAR_ULTIMORCC"

'Parametros XML de Entrada
Const mcteParam_USUARIO     As String = "//Usuario"
Const mcteParam_RCCs        As String = "//rcc"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Esta funcion devuelve TRUE / FALSE seg�n si los tres caracteres recibidos
'       en el REQUEST coinciden con las 3 posiciones indicadas en el ultimo RCC generado.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLUltimoRCC    As MSXML2.DOMDocument
    Dim wobjXMLRequestNode  As MSXML2.IXMLDOMNode
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError           As Boolean
    
    Dim wvarStep            As Long
    Dim wvarIDENTIFICADOR   As String
    Dim wvarUltimoRCC       As String
    Dim wvarUSUARIO         As String
    Dim wvarResult          As String
    Dim wvarRCC             As Integer
   
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ' Leer parametro de entrada
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
    End With
    
    'Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    ' Se obtiene el identificador encriptado de la base.
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteSPObtenerIdentificador
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 80
    If Not wrstDBResult.EOF Then
        ' Aqui se obtiene el ultimo RCC pedido.
        wvarStep = 90
        wvarUltimoRCC = wrstDBResult("ultimoRCC").Value
        
        wvarStep = 100
        If Len(wvarUltimoRCC) > 0 Then
            
            'se desencripta el identificador para poder hacer la comparacion con lo recibido.
            wvarStep = 110
            wvarIDENTIFICADOR = CapicomDecrypt(wrstDBResult("identificador").Value)

            wvarStep = 120
            Set wobjXMLUltimoRCC = CreateObject("MSXML2.DOMDocument")
            With wobjXMLUltimoRCC
                .async = False
                Call .loadXML(wvarUltimoRCC)
            End With
            
            wvarError = False
                        
            wvarStep = 130
            For Each wobjXMLRequestNode In wobjXMLRequest.selectNodes(mcteParam_RCCs)
                
                wvarStep = 140
                wvarRCC = wobjXMLUltimoRCC.selectSingleNode("//rcc[@id=" & wobjXMLRequestNode.Attributes(0).Text & "]").Text
                
                ' comparacion de lo ingresado con el caracter correspondiente
                wvarStep = 150
                If Mid(wvarIDENTIFICADOR, wvarRCC, 1) <> wobjXMLRequestNode.Text Then
                    wvarError = True
                    Exit For
                End If
            
            Next
            
            If Not wvarError Then
                wvarStep = 160
                Set wobjDBCmd = Nothing
    
                wvarStep = 170
                Set wobjDBCmd = CreateObject("ADODB.Command")
    
                ' Limpio el ultimoRCC porque acert� el actual
                wvarStep = 180
                With wobjDBCmd
                    Set .ActiveConnection = wobjDBCnn
                    .CommandText = mcteSPGuardarUltimoRCC
                    .CommandType = adCmdStoredProc
                End With
    
                wvarStep = 190
                Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
                wobjDBCmd.Parameters.Append wobjDBParm
                Set wobjDBParm = Nothing
    
                wvarStep = 180
                Set wobjDBParm = wobjDBCmd.CreateParameter("@ULTIMORCC", adVarChar, adParamInput, 100, "")
                wobjDBCmd.Parameters.Append wobjDBParm
                Set wobjDBParm = Nothing
    
                wvarStep = 190
                wobjDBCmd.Execute
                
                wvarStep = 200
                wvarResult = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & "Identificador Validado." & Chr(34) & " /></Response>"
            Else
                wvarStep = 210
                wvarResult = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Identificador no validado." & Chr(34) & " /></Response>"
            End If
        
        Else
            wvarStep = 220
            wvarResult = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "No hay un ultimoRCC previo." & Chr(34) & " /></Response>"
        End If
        
        wvarStep = 230
        Response = wvarResult
        
    Else
        wvarStep = 240
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 250
    Set wobjDBCmd = Nothing
    '
    wvarStep = 260
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 270
    Set wobjDBCnn = Nothing
    '
    wvarStep = 280
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 290
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 300
    Set wrstDBResult = Nothing
    '
    wvarStep = 310
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

