VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_CambiarIdn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_CambiarIdn
' File Name : camA_CambiarIdn.cls
' Creation Date: 05/12/2005
' Programmer : Fernando Osores
' Abstract :    Modifica el identificador del usuario, siempre y cuando el identificador
'       viejo sea ingresado. Caso contrario no se puede modificar el identificador.
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "cam_OficinaVirtual.camA_CambiarIdn"
Const mcteSPCambiarIdentificador    As String = "SPCAM_CAM_CAMBIAR_IDENTIFICADOR"
Const mcteSPObtenerIdentificador    As String = "SPCAM_CAM_OBTENER_IDENTIFICADOR"

'Parametros XML de Entrada
Const mcteParam_USUARIO             As String = "//Usuario"
Const mcteParam_IDENTIFICADOR       As String = "//Identificador"
Const mcteParam_NEW_IDENTIFICADOR   As String = "//New_Identificador"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Actualiza el identificador previamente solicitando el identificador viejo.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError                   As Boolean
    Dim wvarModificarIdentificador  As Boolean
    
    Dim wvarStep                As Long
    Dim wvarIDENTIFICADOR       As String
    Dim wvarNEWIDENTIFICADOR    As String
    Dim wvarUSUARIO             As String
    Dim wvarResult              As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
        wvarIDENTIFICADOR = .selectSingleNode(mcteParam_IDENTIFICADOR).Text
        wvarNEWIDENTIFICADOR = .selectSingleNode(mcteParam_NEW_IDENTIFICADOR).Text
    End With
    
    'Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteSPObtenerIdentificador
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    
    wvarStep = 80
    If Not wrstDBResult.EOF Then
    
        wvarStep = 90
        If wvarIDENTIFICADOR = CapicomDecrypt(wrstDBResult("Identificador").Value) Then
            wvarModificarIdentificador = True
        Else
            wvarModificarIdentificador = False
        End If
    
        wvarStep = 100
        wrstDBResult.Close
    
    End If
  
    wvarStep = 110
    Set wrstDBResult = Nothing
    
    wvarStep = 120
    Set wobjDBCmd = Nothing
  
    wvarStep = 130
    If wvarModificarIdentificador Then
    
        wvarStep = 140
        Set wobjDBCmd = CreateObject("ADODB.Command")
    
        wvarStep = 150
        With wobjDBCmd
            Set .ActiveConnection = wobjDBCnn
            .CommandText = mcteSPCambiarIdentificador
            .CommandType = adCmdStoredProc
        End With
        
        wvarStep = 160
        Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 170
        wvarNEWIDENTIFICADOR = CapicomEncrypt(wvarNEWIDENTIFICADOR)
        
        wvarStep = 180
        Set wobjDBParm = wobjDBCmd.CreateParameter("@IDENTIFICADOR", adVarChar, adParamInput, 300, wvarNEWIDENTIFICADOR)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    
        wvarStep = 190
        Set wrstDBResult = wobjDBCmd.Execute
        Set wrstDBResult.ActiveConnection = Nothing
        '
        wvarStep = 200
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        
        If Not wrstDBResult.EOF Then
            wvarStep = 210
            wrstDBResult.Save wobjXMLResponse, adPersistXML
        End If
        
        wvarStep = 220
        wvarResult = "<Response><Estado resultado="
        
        wvarStep = 230
        wvarResult = wvarResult & Chr(34) & wobjXMLResponse.selectSingleNode("//z:row/@resultado").Text & Chr(34)
        
        wvarStep = 240
        wvarResult = wvarResult & " mensaje="
        
        wvarStep = 250
        wvarResult = wvarResult & Chr(34) & wobjXMLResponse.selectSingleNode("//z:row/@mensaje").Text & Chr(34)
        wvarResult = wvarResult & " /></Response>"
        
    Else
        wvarStep = 260
        wvarResult = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34)
        wvarResult = wvarResult & " mensaje=" & Chr(34) & "El identificador no coincide. El cambio no se ha realizado." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 270
    Response = wvarResult
    
    wvarStep = 280
    Set wobjDBCmd = Nothing
    '
    wvarStep = 290
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 300
    Set wobjDBCnn = Nothing
    '
    wvarStep = 310
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 320
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 330
    Set wrstDBResult = Nothing
    '
    wvarStep = 340
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


