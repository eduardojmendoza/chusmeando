Attribute VB_Name = "ModRandom"
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : modRandom
' File Name : modRandom.bas
' Creation Date: 01/12/2005
' Programmer : Fernando Osores / Lucas De Merlier
' Abstract :    Genera un string random, de longitud indicada por parametro.
' *****************************************************************
Option Explicit

Public Function generar_RNDSTR(Optional strLength As Integer) As String
' Esta funci�n genera un string de longitud strLength aleatorio.
' Si la longitud no se especifica, se asume igual a 4.
    On Error GoTo ErrorHandler
    
    Dim strCode     As String
    Dim nroAux      As Integer
    
    strCode = ""
    
    Randomize
    
    If strLength = 0 Then
        strLength = 4
    End If

    While Len(strCode) < strLength
        nroAux = Int((92 * Rnd) + 33)
        If (nroAux >= 48 And nroAux <= 57) _
            Or (nroAux >= 65 And nroAux <= 90) _
            Or (nroAux >= 97 And nroAux <= 122) _
            Then
            ' 48:57 --> 0..9
            ' 65:90 --> a..z
            ' 97:122 -> A..Z
            
            strCode = strCode & Chr(nroAux)
        End If
    Wend
    
    generar_RNDSTR = strCode
    
    Exit Function

ErrorHandler:
    generar_RNDSTR = Empty

End Function

