VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_BlanquearClave"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_BlanquearClave
' File Name : camA_BlanquearClave.cls
' Creation Date: 14/12/2005
' Programmer : Fernando Osores
' Abstract :    Blanquea la clave NT del usuario, utilizando el componente de PORINI
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "cam_OficinaVirtual.camA_BlanquearClave"

'Parametros XML de Entrada
Const mcteParam_USUARIO             As String = "//Usuario"
Const mcteParam_EMAIL               As String = "//Email"
Const mcteParam_DOMINIO               As String = "//Dominio"
Const mcteParam_RESULTADO           As String = "//@RESULTADO"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Genera un nuevo identificador y envia mitad por mail mitad a pantalla.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError                   As Boolean
    Dim wvarModificarIdentificador  As Boolean
    
    Dim wvarStep                As Long
    Dim wvarPwdReset            As Boolean
    Dim wvarEmailOK             As Boolean
    Dim wvarClave               As String
    Dim wvarMitadASP            As String
    Dim wvarMitadEmail          As String
    Dim wvarRequestRESET        As String
    Dim wvarResponseRESET       As String
    Dim wvarUSUARIO             As String
    Dim wvarMailAEnviar         As String
    Dim wvarResultASP           As String
    Dim wvarResultEnviarMail    As String
    Dim wvarEMAIL               As String
    Dim wvarDomain              As String
    
    'wvarDomain = "externo\"
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
        Debug.Print wvarUSUARIO
        wvarDomain = .selectSingleNode(mcteParam_DOMINIO).Text
        Debug.Print wvarDomain
    End With
    Set wobjXMLRequest = Nothing

    wvarStep = 20
    wvarClave = generar_RNDSTR(8)
    Debug.Print wvarClave

    wvarStep = 30
'    wvarRequestRESET = "<HSBC_MSG>"
'    wvarRequestRESET = wvarRequestRESET & "<HEADER>"
'    wvarRequestRESET = wvarRequestRESET & "<APPLICATION_CODE>AuthenticateUser</APPLICATION_CODE>"
'    wvarRequestRESET = wvarRequestRESET & "<ACTION_CODE>AuthenticateUser</ACTION_CODE>"
    'wvarRequestRESET = wvarRequestRESET & "<ACTION_CODE>NTUserManager</ACTION_CODE>"
'    wvarRequestRESET = wvarRequestRESET & "</HEADER>"
'    wvarRequestRESET = wvarRequestRESET & "<BODY>"
        wvarRequestRESET = wvarRequestRESET & "<Request>"
        wvarRequestRESET = wvarRequestRESET & "<ActionCode>AuthenticateUser</ActionCode>"
        wvarRequestRESET = wvarRequestRESET & "<Command>ResetUserPassword</Command>" 'Produccion
        'wvarRequestRESET = wvarRequestRESET & "<Command>GetUser</Command>"
        'wvarRequestRESET = wvarRequestRESET & "<ActionCode>ResetUserPassword</ActionCode>"
        wvarRequestRESET = wvarRequestRESET & "<record>"
        wvarRequestRESET = wvarRequestRESET & "<name>" & wvarUSUARIO & "</name>"
        wvarRequestRESET = wvarRequestRESET & "<password>" & wvarClave & "</password>"
        wvarRequestRESET = wvarRequestRESET & "</record>"
        wvarRequestRESET = wvarRequestRESET & "</Request>"
'    wvarRequestRESET = wvarRequestRESET & "</BODY>"
'    wvarRequestRESET = wvarRequestRESET & "</HSBC_MSG>"
    Debug.Print wvarRequestRESET
    
    
 '   App.LogEvent wvarRequestRESET
    
    wvarStep = 40
    Set wobjClass = mobjCOM_Context.CreateInstance("WD.AuthenticateUser")
    'Set wobjClass = mobjCOM_Context.CreateInstance("HSBC.CmdProcessor")
    Call wobjClass.Execute(wvarRequestRESET, wvarResponseRESET, "")
    Set wobjClass = Nothing

  '  App.LogEvent wvarResponseRESET

'    wvarResponseRESET = "<resultado><error>2245</error><GENERAL><ESTADO RESULTADO=" & Chr(34) & "FALSE" & Chr(34) & " MENSAJE=" & Chr(34) & "(2245 0x8C5) The password does not meet the password policy requirements. Check the minimum password length, password complexity and password history requirements." & Chr(34) & " CODIGO=" & Chr(34) & "2245" & Chr(34) & "/><Response/></GENERAL></resultado>"
'    wvarResponseRESET = "<resultado><error>0</error><GENERAL><ESTADO RESULTADO=" & Chr(34) & "TRUE" & Chr(34) & " MENSAJE=" & Chr(34) & Chr(34) & " CODIGO=" & Chr(34) & "0" & Chr(34) & "/><Response><record><user_id>2637</user_id><name>rappoll0114654936</name><full_name>rappoll0114654936</full_name><priv>1</priv><group>USER</group><password_expired>1</password_expired></record></Response></GENERAL></resultado>"
'    wvarResponseRESET = "<Response><Estado resultado="" mensaje=""  />" & wvarResponseRESET & "</Response>"

    wvarStep = 50
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    With wobjXMLResponse
        .async = False
        Call .loadXML(wvarResponseRESET)
        wvarPwdReset = CBool(.selectSingleNode(mcteParam_RESULTADO).Text)
        Debug.Print wvarPwdReset
    End With
    
    wvarStep = 60
    If wvarPwdReset Then

        wvarStep = 70
        wvarMitadASP = Mid(wvarClave, 1, 4)
        Debug.Print wvarMitadASP
    
        wvarStep = 80
        wvarMitadEmail = Mid(wvarClave, 5)
        Debug.Print wvarMitadEmail
        
        wvarStep = 90
        wvarResultASP = "<Response><Estado resultado=" & Chr(39) & "TRUE" & Chr(39) & " mensaje=" & Chr(39) & "Password Blanqueado. 1ra Mitad. La segunda fue por mail." & Chr(39) & "  /><MediaClave>" & wvarMitadASP & "</MediaClave></Response>"
            
        wvarStep = 100
        Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_ObtenerEmail")
        
        wvarStep = 110
        Call wobjClass.Execute(Replace(Request, wvarDomain, ""), wvarEMAIL, "")
    
        wvarStep = 120
        Set wobjClass = Nothing
            
        wvarStep = 130
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        With wobjXMLResponse
            .async = False
            Call .loadXML(wvarEMAIL)
            wvarEmailOK = CBool(.selectSingleNode(LCase(mcteParam_RESULTADO)).Text)
            If wvarEmailOK Then
                wvarEMAIL = .selectSingleNode(mcteParam_EMAIL).Text
            End If
            
        End With

        If wvarEmailOK Then
            wvarStep = 140
            wvarMailAEnviar = ""
            wvarMailAEnviar = "<Request>"
            wvarMailAEnviar = wvarMailAEnviar & "<FORMAT_FILE>BlanqueoClave</FORMAT_FILE>"
            wvarMailAEnviar = wvarMailAEnviar & "<TO><![CDATA[" & wvarEMAIL & "]]></TO>"
            wvarMailAEnviar = wvarMailAEnviar & "<SUBJECT><![CDATA[Blanqueo de Clave]]></SUBJECT>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAMETROS>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAMETRO>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAM_NOMBRE><![CDATA[%%USUARIO%%]]></PARAM_NOMBRE>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAM_VALOR><![CDATA[" & Replace(wvarUSUARIO, wvarDomain, "") & "]]></PARAM_VALOR>"
            wvarMailAEnviar = wvarMailAEnviar & "</PARAMETRO>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAMETRO>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAM_NOMBRE><![CDATA[%%MEDIACLAVE%%]]></PARAM_NOMBRE>"
            wvarMailAEnviar = wvarMailAEnviar & "<PARAM_VALOR>" & wvarMitadEmail & "</PARAM_VALOR>"
            wvarMailAEnviar = wvarMailAEnviar & "</PARAMETRO>"
            wvarMailAEnviar = wvarMailAEnviar & "</PARAMETROS>"
            wvarMailAEnviar = wvarMailAEnviar & "</Request>"
        
            wvarStep = 150
            Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_EnviarMail")
            Call wobjClass.Execute(wvarMailAEnviar, wvarResultEnviarMail, "")
            Set wobjClass = Nothing
        Else
        
            wvarStep = 160
            wvarResultASP = "<resultado>" & wvarEMAIL & "</resultado>"
        
        End If
    Else
    
        wvarStep = 170
        wvarResultASP = Replace(wvarResponseRESET, "<ESTADO RESULTADO=" & Chr(39) & Chr(39) & " MENSAJE=" & Chr(39) & Chr(39) & "  />", "<Estado resultado=" & Chr(39) & "FALSE" & Chr(39) & " mensaje=" & Chr(39) & "El Password no fue Blanqueado." & Chr(39) & "  />")
        wvarResultASP = Replace(wvarResultASP, "<ESTADO RESULTADO=", "<Estado resultado=")
        wvarResultASP = Replace(wvarResultASP, " MENSAJE=", " mensaje=")
    
    End If
    
    wvarStep = 180
    Response = wvarResultASP
    
    wvarStep = 190
    Set wobjDBCmd = Nothing
    '
    wvarStep = 200
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 210
    Set wobjDBCnn = Nothing
    '
    wvarStep = 220
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 230
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 240
    Set wrstDBResult = Nothing
    '
    wvarStep = 250
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

