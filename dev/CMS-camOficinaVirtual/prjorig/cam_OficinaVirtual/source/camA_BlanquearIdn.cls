VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_BlanquearIdn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_BlanquearIdn
' File Name : camA_BlanquearIdn.cls
' Creation Date: 07/12/2005
' Programmer : Fernando Osores
' Abstract :    Blanquea el identificador del usuario.
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "cam_OficinaVirtual.camA_BlanquearIdn"
Const mcteSPResetearIdentificador    As String = "SPCAM_CAM_CAMBIAR_IDENTIFICADOR"

'Parametros XML de Entrada
Const mcteParam_USUARIO             As String = "//Usuario"
Const mcteParam_EMAIL               As String = "//Email"

' *****************************************************************
' Function : IAction_Execute
' Abstract : Genera un nuevo identificador y envia mitad por mail mitad a pantalla.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError                   As Boolean
    Dim wvarModificarIdentificador  As Boolean
    
    Dim wvarStep                As Long
    Dim wvarIDENTIFICADOR       As String
    Dim wvarMitadASP            As String
    Dim wvarMitadEmail          As String
    Dim wvarUSUARIO             As String
    Dim wvarMailAEnviar         As String
    Dim wvarResultASP           As String
    Dim wvarResultEnviarMail    As String
    Dim wvarEMAIL               As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARIO = .selectSingleNode(mcteParam_USUARIO).Text
    End With
    
    'Set wobjXMLRequest = Nothing
    
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteSPResetearIdentificador
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIO", adVarChar, adParamInput, 50, wvarUSUARIO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    wvarIDENTIFICADOR = LCase(generar_RNDSTR(8))
    ' el LCASE() est� puesto porque eChannels solicit� que el identificador sea
    ' CASE-INSENSITIVE. De la misma forma solicit� que la longitud del mismo sea
    ' de 8 caracteres

    wvarStep = 80
    wvarMitadASP = Mid(wvarIDENTIFICADOR, 1, 4)
    Debug.Print wvarMitadASP
    
    wvarStep = 90
    wvarMitadEmail = Mid(wvarIDENTIFICADOR, 5)
    Debug.Print wvarMitadEmail
    
    wvarStep = 100
    wvarIDENTIFICADOR = CapicomEncrypt(wvarIDENTIFICADOR)
    
    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDENTIFICADOR", adVarChar, adParamInput, 300, wvarIDENTIFICADOR)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EXPIRADO", adInteger, adParamInput, 4, 1)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 130
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
        '
    wvarStep = 140
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    
    If Not wrstDBResult.EOF Then
        wvarStep = 150
        wrstDBResult.Save wobjXMLResponse, adPersistXML
    End If
    
    wvarStep = 160
    wvarResultASP = "<Response><Estado resultado="
    
    wvarStep = 170
    wvarResultASP = wvarResultASP & Chr(34) & wobjXMLResponse.selectSingleNode("//z:row/@resultado").Text & Chr(34)
    
    wvarStep = 180
    wvarResultASP = wvarResultASP & " mensaje="
    
    wvarStep = 190
    wvarResultASP = wvarResultASP & Chr(34) & wobjXMLResponse.selectSingleNode("//z:row/@mensaje").Text & Chr(34)
    wvarResultASP = wvarResultASP & " /><MediaClave>" & wvarMitadASP & "</MediaClave></Response>"
        
    wvarStep = 200
    Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_ObtenerEmail")
    
    wvarStep = 210
    Call wobjClass.Execute(Request, wvarEMAIL, "")
    
    wvarStep = 220
    Set wobjClass = Nothing
        
    wvarStep = 230
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    With wobjXMLResponse
        .async = False
        Call .loadXML(wvarEMAIL)
        wvarEMAIL = .selectSingleNode(mcteParam_EMAIL).Text
    End With

    wvarStep = 240
    wvarMailAEnviar = ""
    wvarMailAEnviar = "<Request>"
    wvarMailAEnviar = wvarMailAEnviar & "<FORMAT_FILE>BlanqueoIdentificador</FORMAT_FILE>"
    wvarMailAEnviar = wvarMailAEnviar & "<TO><![CDATA[" & wvarEMAIL & "]]></TO>"
    wvarMailAEnviar = wvarMailAEnviar & "<SUBJECT><![CDATA[Blanqueo de Identificador]]></SUBJECT>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAMETROS>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAMETRO>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAM_NOMBRE><![CDATA[%%USUARIO%%]]></PARAM_NOMBRE>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAM_VALOR><![CDATA[" & wvarUSUARIO & "]]></PARAM_VALOR>"
    wvarMailAEnviar = wvarMailAEnviar & "</PARAMETRO>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAMETRO>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAM_NOMBRE><![CDATA[%%MEDIACLAVE%%]]></PARAM_NOMBRE>"
    wvarMailAEnviar = wvarMailAEnviar & "<PARAM_VALOR>" & wvarMitadEmail & "</PARAM_VALOR>"
    wvarMailAEnviar = wvarMailAEnviar & "</PARAMETRO>"
    wvarMailAEnviar = wvarMailAEnviar & "</PARAMETROS>"
    wvarMailAEnviar = wvarMailAEnviar & "</Request>"
        
    wvarStep = 250
    Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_EnviarMail")
    Call wobjClass.Execute(wvarMailAEnviar, wvarResultEnviarMail, "")
    Set wobjClass = Nothing
        
    wvarStep = 260
    Response = wvarResultASP
    
    wvarStep = 270
    Set wobjDBCmd = Nothing
    '
    wvarStep = 280
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 290
    Set wobjDBCnn = Nothing
    '
    wvarStep = 300
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 310
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 320
    Set wrstDBResult = Nothing
    '
    wvarStep = 330
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub




