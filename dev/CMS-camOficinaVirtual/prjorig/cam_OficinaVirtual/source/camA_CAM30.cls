VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "camA_CAM30"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : camA_CAM30
' File Name : camA_CAM30.cls
' Creation Date: 27/12/2005
' Programmer : Fernando Osores
' Abstract :    Hace de interface con el componente de PORINI
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "cam_OficinaVirtual.camA_CAM30"

'Parametros XML de Entrada
'Const mcteParam_NTUSERMANAGER      As String = "WD.NTUserManager"
Const mcteParam_COMMAND            As String = "//Command"
Const mcteParam_SYSTEM             As String = "//System"
Const mcteParam_NAME               As String = "//name"
Const mcteParam_PASSWORD           As String = "//password"
Const mcteParam_SQLUSERMANAGER     As String = "WD.SQLUserManager"
Const mcteParam_AUTHENTICATEUSER   As String = "WD.AuthenticateUser"


' *****************************************************************
' Function : IAction_Execute
' Abstract : Genera un nuevo identificador y envia mitad por mail mitad a pantalla.
' Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
' *****************************************************************
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
'    Dim wobjDBCnn           As ADODB.Connection
'    Dim wobjDBCmd           As ADODB.Command
'    Dim wrstDBResult        As ADODB.Recordset
'    Dim wobjDBParm          As ADODB.Parameter
    
    Dim wvarError              As Boolean
    
    Dim wvarStep               As Long
    Dim wvarSYSTEM             As String
    Dim wvarPASSWORD           As String
    Dim wvarNAME               As String
    Dim wvarNAME_new           As String
    Dim wvarCOMMAND            As String
    Dim wvarRequestAU          As String
    Dim wvarResponseAU         As String
    Dim wvarComponente         As String
    
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    Dim strFileName As String
    strFileName = GetLogFileName()
    '------------------------------------------
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog 0, "INICIO " + CStr(Date) + " " + CStr(Time), strFileName
    '------------------------------------------
    
    wvarStep = 10
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep, "Instancio MSXML2.DOMDocument", strFileName
    '------------------------------------------
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
    wvarStep = 20
        '------------------------------------------
        'Agregado para LOG Adicional de ERROR
        'FJO. 15/06/2006
        '------------------------------------------
        SaveStepToLog wvarStep, "Cargo el Request en el objeto MSXML2.DOMDocument", strFileName
        '------------------------------------------
        Call .loadXML(Request)
        
    wvarStep = 30
        wvarCOMMAND = .selectSingleNode(mcteParam_COMMAND).Text
        Debug.Print wvarCOMMAND
        '------------------------------------------
        'Agregado para LOG Adicional de ERROR
        'FJO. 15/06/2006
        '------------------------------------------
        SaveStepToLog wvarStep, "wvarCOMMAND=" + wvarCOMMAND, strFileName
        '------------------------------------------
        
    wvarStep = 40
        If Not .selectSingleNode(mcteParam_SYSTEM) Is Nothing Then
            wvarSYSTEM = .selectSingleNode(mcteParam_SYSTEM).Text
        Else
            wvarSYSTEM = ""
        End If
        '------------------------------------------
        'Agregado para LOG Adicional de ERROR
        'FJO. 15/06/2006
        '------------------------------------------
        SaveStepToLog wvarStep, "wvarSYSTEM=" + wvarSYSTEM, strFileName
        '------------------------------------------
    
    wvarStep = 50
        If Not .selectSingleNode(mcteParam_PASSWORD) Is Nothing Then
            wvarPASSWORD = .selectSingleNode(mcteParam_PASSWORD).Text
        Else
            wvarPASSWORD = ""
        End If
        '------------------------------------------
        'Agregado para LOG Adicional de ERROR
        'FJO. 15/06/2006
        '------------------------------------------
        SaveStepToLog wvarStep, "wvarPASSWORD=(***************)", strFileName
        '------------------------------------------
    
    wvarStep = 60
        If Not .selectSingleNode(mcteParam_NAME) Is Nothing Then
            wvarNAME = .selectSingleNode(mcteParam_NAME).Text
        Else
            wvarNAME = ""
        End If
        '------------------------------------------
        'Agregado para LOG Adicional de ERROR
        'FJO. 15/06/2006
        '------------------------------------------
        SaveStepToLog wvarStep, "wvarNAME=" + wvarNAME, strFileName
        '------------------------------------------
    
    End With
    Set wobjXMLRequest = Nothing
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep + 1, "Libero MSXML2.DOMDocument", strFileName
    '------------------------------------------
   
    wvarStep = 70
    If UCase(wvarSYSTEM) = "OVCAM" Then
        wvarComponente = mcteParam_SQLUSERMANAGER
        If InStr(1, wvarNAME, "\") > 0 Then
            wvarNAME_new = Mid(wvarNAME, InStr(1, wvarNAME, "\") + 1)
            Request = Replace(Request, wvarNAME, wvarNAME_new)
        End If
        If UCase(wvarCOMMAND) = "SETUSERPASSWORD" Or UCase(wvarCOMMAND) = "RESETUSERPASSWORD" Then
            Request = Replace(Request, wvarPASSWORD, CapicomEncrypt(wvarPASSWORD))
        End If
    Else
        If UCase(wvarCOMMAND) = "SETUSERPASSWORD" Or UCase(wvarCOMMAND) = "RESETUSERPASSWORD" Then
            wvarComponente = mcteParam_AUTHENTICATEUSER
'            wvarComponente = mcteParam_NTUSERMANAGER
        Else
            wvarComponente = mcteParam_AUTHENTICATEUSER
        End If
    End If
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep, "Determino el componente a invocar", strFileName
    '------------------------------------------
    
    wvarStep = 80
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep, "Instancio " + wvarComponente, strFileName
    '------------------------------------------
    'mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, mcteClassName, wcteFnName, wvarStep, 1, "Se inicia el llamado a componente " & wvarComponente, vbLogEventTypeInformation
    
    Set wobjClass = mobjCOM_Context.CreateInstance(wvarComponente)
    'Set wobjClass = CreateObject(wvarComponente)
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep + 1, "Voy a ejecutar " + wvarComponente, strFileName
    '------------------------------------------
    Call wobjClass.Execute(Request, wvarResponseAU, "")
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep + 2, "ejecute " + wvarComponente, strFileName
    '------------------------------------------
    Set wobjClass = Nothing
    
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep + 3, "Libero " + wvarComponente, strFileName
    '------------------------------------------
    'mobjEventLog.Log EventLog_Category.evtLog_Category_Logical, mcteClassName, wcteFnName, wvarStep, 1, "Ha terminado el llamado a componente " & wvarComponente, vbLogEventTypeInformation
    
    wvarStep = 90
    Response = wvarResponseAU
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep, "Devuelve respuesta" + wvarComponente, strFileName
    '------------------------------------------
    
    wvarStep = 100
    '------------------------------------------
    'Agregado para LOG Adicional de ERROR
    'FJO. 15/06/2006
    '------------------------------------------
    SaveStepToLog wvarStep, "FIN " + CStr(Date) + " " + CStr(Time), strFileName
    DeleteLogFileName strFileName
    '------------------------------------------
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



