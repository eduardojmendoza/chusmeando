Attribute VB_Name = "ModSendMail"
Option Explicit

'Parametros XML de Entrada
Const mcteParam_PARAMETROS      As String = "//PARAMETROS/PARAMETRO"
Const mcteParam_PARAM_NOMBRE    As String = "PARAM_NOMBRE"
Const mcteParam_PARAM_VALOR     As String = "PARAM_VALOR"
Const mcteParam_FORMAT_FILE     As String = "//FORMAT_FILE"
Const mcteParam_FROM            As String = "//FROM"
Const mcteParam_SUBJECT         As String = "//SUBJECT"
Const mcteParam_TO              As String = "//TO"
Const mcteParam_REPLYTO         As String = "//REPLYTO"
Const mcteParam_IMPORTANCE      As String = "//IMPORTANCE"
Const mcteParam_BODYFORMAT      As String = "//BODYFORMAT"
Const mcteParam_MAILFORMAT      As String = "//MAILFORMAT"
Const mcteParam_BODY            As String = "//BODY"


' *****************************************************************
' Function : EnviarMail
' Abstract :
' un combo
' Synopsis :
' *****************************************************************
Private Function EnviarMail(ByVal Request As String) As Boolean

    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLFormat       As MSXML2.DOMDocument
    Dim wobjElement         As MSXML2.IXMLDOMElement
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarFormatFile      As String
    Dim wvarParamNombre     As String
    Dim wvarParamValor      As String
    Dim wvarFrom            As String
    Dim wvarTo              As String
    Dim wvarSubject         As String
    Dim wvarReplyTo         As String
    Dim wvarImportance      As String
    Dim wvarBodyFormat      As String
    Dim wvarMailFormat      As String
    Dim wvarBody            As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    ' CARGO EL XML DE ENTRADA
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        '
        wvarStep = 20
        wvarFormatFile = .selectSingleNode(mcteParam_FORMAT_FILE).Text
        '
        ' CARGO EL XML CON EL FORMATO DEL MAIL
        '
        wvarStep = 30
        Set wobjXMLFormat = CreateObject("MSXML2.DOMDocument")
        wobjXMLFormat.async = False
        Call wobjXMLFormat.Load(App.Path & "\cam_OficinaVirtual\" & wvarFormatFile & ".xml")
        '
        ' CARGO EL RESTO DE LOS PARAMETROS
        '
        wvarFrom = wobjXMLFormat.selectSingleNode(mcteParam_FROM).Text
        wvarTo = .selectSingleNode(mcteParam_TO).Text
        
        ' SI MANDA UN NODO SUBJECT USO ESE, SINO USO EL DEFAULT
        If .selectNodes(mcteParam_SUBJECT).length = 0 Then
            wvarSubject = wobjXMLFormat.selectSingleNode(mcteParam_SUBJECT).Text
        Else
            If .selectSingleNode(mcteParam_SUBJECT).Text = "" Then
                wvarSubject = wobjXMLFormat.selectSingleNode(mcteParam_SUBJECT).Text
            Else
                wvarSubject = .selectSingleNode(mcteParam_SUBJECT).Text
            End If
        End If
        '
        wvarReplyTo = wobjXMLFormat.selectSingleNode(mcteParam_REPLYTO).Text
        wvarImportance = wobjXMLFormat.selectSingleNode(mcteParam_IMPORTANCE).Text
        wvarBodyFormat = wobjXMLFormat.selectSingleNode(mcteParam_BODYFORMAT).Text
        wvarMailFormat = wobjXMLFormat.selectSingleNode(mcteParam_MAILFORMAT).Text
        wvarBody = wobjXMLFormat.selectSingleNode(mcteParam_BODY).Text
        '
        wvarStep = 40
        '
        ' RECORRO LA LISTA DE PARAMETROS Y VALORES
        For Each wobjElement In .selectNodes(mcteParam_PARAMETROS)
            wvarParamNombre = wobjElement.selectSingleNode(mcteParam_PARAM_NOMBRE).Text
            wvarParamValor = wobjElement.selectSingleNode(mcteParam_PARAM_VALOR).Text
            '
            wvarBody = Replace(wvarBody, wvarParamNombre, wvarParamValor)
        Next
    End With
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLFormat = Nothing
    Set wobjElement = Nothing
    '
    ' EJECUTO EL ENVIO DE MAILS
'    wvarStep = 50
'    If p_Send_EMail(wvarFrom, wvarTo, wvarSubject, wvarImportance, wvarBodyFormat, wvarMailFormat, wvarBody, wvarReplyTo) Then
'        wvarStep = 60
        Response = "<Response><Estado resultado=""true"" mensaje=""EMAIL ENVIADO"" /></Response>"
'    Else
'        wvarStep = 70
'        Response = "<Response><Estado resultado=""false"" mensaje=""NO SE PUDO ENVIAR EL EMAIL."" /></Response>"
'    End If
    '
    wvarStep = 80
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLRequest = Nothing
    Set wobjXMLFormat = Nothing
    Set wobjElement = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

' *****************************************************************
' Function : SendMail
' Abstract : Realiza el envio de mails utilizando CDONTS
' Synopsis : Private Function p_Send_EMail(ByVal pFrom As String, ByVal pTo As String,
'                            ByVal pSubject As String, ByVal pImportance As String,
'                            ByVal pBodyFormat As String, ByVal pMailFormat As String,
'                            ByVal pBody As String) As Boolean
' *****************************************************************
Public Function SendMail(ByVal pvarFrom As String, ByVal pvarTo As String, _
                            ByVal pvarSubject As String, ByVal pvarImportance As String, _
                            ByVal pvarBodyFormat As String, ByVal pvarMailFormat As String, _
                            ByVal pvarBody As String, ByVal pvarReplyTo As String) As Boolean
    
    Dim wobjMail As CDONTS.NewMail
    '
    Set wobjMail = CreateObject("CDONTS.NewMail")
    '
    On Error GoTo ErrorCreateMail
    '
    wobjMail.From = pvarFrom
    wobjMail.To = pvarTo
    wobjMail.Value("Reply-To") = pvarReplyTo
    wobjMail.Subject = pvarSubject
    wobjMail.Importance = pvarImportance
    wobjMail.BodyFormat = pvarBodyFormat
    wobjMail.MailFormat = pvarMailFormat
    wobjMail.Body = pvarBody
    wobjMail.send
    '
    Set wobjMail = Nothing
    '
    EnviarMail = True
    Exit Function
    
ErrorCreateMail:
    EnviarMail = False
End Function
