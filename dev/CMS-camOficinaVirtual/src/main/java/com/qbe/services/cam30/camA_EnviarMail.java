package com.qbe.services.cam30;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;

import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import diamondedge.util.Err;
import diamondedge.util.Variant;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : camA_EnviarMail
 *  File Name : camA_EnviarMail.cls
 *  Creation Date: 07/12/2005
 *  Programmer : Fernando Osores / Rodrigo Goncalves.
 *  Abstract :    Envia un mail en funcion de los parametros de entrada y de un archivo
 *        XML de configuracion. Este debe estar dentro del directorio de ejecucion en
 *        /cam_OficinaVirtual.
 *  *****************************************************************
 * Objetos del FrameWork
 */

@Service
public class camA_EnviarMail implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "camA_OficinaVirtual.camA_EnviarMail";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_PARAMETROS = "//PARAMETROS/PARAMETRO";
  static final String mcteParam_PARAM_NOMBRE = "PARAM_NOMBRE";
  static final String mcteParam_PARAM_VALOR = "PARAM_VALOR";
  static final String mcteParam_FORMAT_FILE = "//FORMAT_FILE";
  static final String mcteParam_FROM = "//FROM";
  static final String mcteParam_SUBJECT = "//SUBJECT";
  static final String mcteParam_TO = "//TO";
  static final String mcteParam_REPLYTO = "//REPLYTO";
  static final String mcteParam_IMPORTANCE = "//IMPORTANCE";
  static final String mcteParam_BODYFORMAT = "//BODYFORMAT";
  static final String mcteParam_MAILFORMAT = "//MAILFORMAT";
  static final String mcteParam_BODY = "//BODY";
  
  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "IAction_Execute";
  
	private static Logger logger = Logger.getLogger(camA_EnviarMail.class.getName());

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:beans.xml");
		}
		return context;
	}


  /**
   *  *****************************************************************
   *  Function : IAction_Execute
   *  Abstract : Envia un mail on DEMAND.
   *  Synopsis : IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
   *  *****************************************************************
   */
  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLFormat = null;
    org.w3c.dom.Node wobjElement = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarFormatFile = "";
    String wvarParamNombre = "";
    String wvarParamValor = "";
    String wvarFrom = "";
    String wvarTo = "";
    String wvarSubject = "";
    String wvarReplyTo = "";
    String wvarImportance = "";
    String wvarBodyFormat = "";
    String wvarMailFormat = "";
    String wvarBody = "";
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      // CARGO EL XML DE ENTRADA
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      wvarStep = 20;
      wvarFormatFile = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FORMAT_FILE )  );
      //
      // CARGO EL XML CON EL FORMATO DEL MAIL
      //
      wvarStep = 30;
      wobjXMLFormat = new XmlDomExtended();
      wobjXMLFormat.loadXML( Request );

      wobjXMLFormat.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(wvarFormatFile + ".xml" ));
      //
      // CARGO EL RESTO DE LOS PARAMETROS
      //
      wvarFrom = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_FROM )  );
      wvarTo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TO )  );

      // SI MANDA UN NODO SUBJECT USO ESE, SINO USO EL DEFAULT
      if( wobjXMLRequest.selectNodes( mcteParam_SUBJECT ) .getLength() == 0 )
      {
        wvarSubject = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_SUBJECT )  );
      }
      else
      {
        if( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT )  ).equals( "" ) )
        {
          wvarSubject = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_SUBJECT )  );
        }
        else
        {
          wvarSubject = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SUBJECT )  );
        }
      }
      //
      wvarReplyTo = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_REPLYTO )  );
      wvarImportance = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_IMPORTANCE )  );
      wvarBodyFormat = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_BODYFORMAT )  );
      wvarMailFormat = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_MAILFORMAT )  );
      wvarBody = XmlDomExtended.getText( wobjXMLFormat.selectSingleNode( mcteParam_BODY )  );
      //
      wvarStep = 40;
      //
      // RECORRO LA LISTA DE PARAMETROS Y VALORES
      for( int nwobjElement = 0; nwobjElement < wobjXMLRequest.selectNodes( mcteParam_PARAMETROS ) .getLength(); nwobjElement++ )
      {
        wobjElement = wobjXMLRequest.selectNodes( mcteParam_PARAMETROS ) .item( nwobjElement );
        wvarParamNombre = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjElement, mcteParam_PARAM_NOMBRE ));
        wvarParamValor = XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjElement, mcteParam_PARAM_VALOR ));
        //
        //             wvarBody = Replace(wvarBody, wvarParamNombre, wvarParamValor)
//Expression: Required. String expression containing substring to replace.
//        Find: Required. Substring being searched for.
//        Replacement: Required. Replacement substring.
        wvarBody = StringUtils.replace(wvarBody, wvarParamNombre, wvarParamValor);
//        Así lo había convertido el VBC 
        //wvarBody = wvarBody/*args should be chars wvarBody.replace(wvarParamNombre, wvarParamValor) */;
      }
      //
      wobjXMLRequest = null;
      wobjXMLFormat = null;
      wobjElement = (org.w3c.dom.Node) null;
      //
      // EJECUTO EL ENVIO DE MAILS
      wvarStep = 50;
			try {
				if (p_Send_EMail(wvarFrom, wvarTo, wvarSubject, wvarImportance,
						wvarBodyFormat, wvarMailFormat, wvarBody, wvarReplyTo)) {
					wvarStep = 60;
					Response.set("<Response><Estado resultado=\"true\" mensaje=\"EMAIL ENVIADO\" /></Response>");
				}
			} catch (Cam30MailException e) {
				Response.set("<Response><Estado resultado=\"false\" mensaje=\"NO SE PUDO ENVIAR EL EMAIL.\" /> "
						+ e.getMessage() + "</Response>");
			}
      //
      wvarStep = 80;
      IAction_Execute = 0;
      //
      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + Request, null );
        Err.clear();
        throw new ComponentExecutionException(_e_);
    }
  }

  /**
   *  *****************************************************************
   *  Function : p_Send_EMail
   *  Abstract : Realiza el envio de mails utilizando CDONTS
   *  Synopsis : Private Function p_Send_EMail(ByVal pFrom As String, ByVal pTo As String,
   *                             ByVal pSubject As String, ByVal pImportance As String,
   *                             ByVal pBodyFormat As String, ByVal pMailFormat As String,
   *                             ByVal pBody As String) As Boolean
   *  *
 * @throws Cam30MailException ****************************************************************
   */
  private boolean p_Send_EMail( String pvarFrom, String pvarTo, String pvarSubject, String pvarImportance, String pvarBodyFormat, String pvarMailFormat, String pvarBody, String pvarReplyTo ) throws Cam30MailException
  {
//    http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/mail.html
    
	// Si quiero cableralo y ejecutar en un test
	//	JavaMailSenderImpl sender = new JavaMailSenderImpl();
//	sender.setHost("mail.host.com");

	try {
		JavaMailSender mailSender = (JavaMailSender) getContext().getBean("mailSender");
		if ( mailSender == null) {
			throw new ComponentExecutionException("No está configurado el mailSender en Scheduler Engine. Revisar la configuración de Spring");
		}

		MimeMessage message = mailSender.createMimeMessage();

		// use the true flag to indicate you need a multipart message
		// No lo usamos porque solamente mandamos un html, no hay attachments ni multipart txt
		MimeMessageHelper helper = new MimeMessageHelper(message, false);
		helper.setFrom(pvarFrom);
		helper.setTo(pvarTo);
		helper.setReplyTo(pvarReplyTo);
		helper.setSubject(pvarSubject);
		//http://docs.spring.io/spring/docs/2.0.x/api/org/springframework/mail/javamail/MimeMessageHelper.html#setPriority(int)
		//	priority - the priority value; typically between 1 (highest) and 5 (lowest)
		int priority = pvarImportance.equalsIgnoreCase("1") ? 1 : 3;
		helper.setPriority(priority);
		boolean htmlFormat = pvarBodyFormat.equalsIgnoreCase("0");
		// use the true flag to indicate the text included is HTML
		helper.setText(pvarBody, htmlFormat);
		mailSender.send(message);
		return true;
	} catch (MailException e) {
		logger.log(Level.SEVERE, "Al enviar mail", e);
		throw new Cam30MailException(e);
	} catch (MessagingException e) {
		logger.log(Level.SEVERE, "Al enviar mail", e);
		throw new Cam30MailException(e);
	}
    //
      // Del original
//    wobjMail = new CDONTS.NewMail();
//      wobjMail.From.set( pvarFrom );
//      wobjMail.To.set( pvarTo );
//      wobjMail.Value( "Reply-To" ) = pvarReplyTo;
//      wobjMail.Subject.set( pvarSubject );
//      wobjMail.Importance.set( pvarImportance );
//      wobjMail.BodyFormat.set( CdoBodyFormatHTML );
//      wobjMail.MailFormat.set( CdoMailFormatMime );
//      wobjMail.Body.set( pvarBody );
//      wobjMail.send();
//      p_Send_EMail = true;
//      return p_Send_EMail;
//    }
//    catch( Exception _e_ )
//    {
//        p_Send_EMail = false;
//    }
//    return p_Send_EMail;
}
}
