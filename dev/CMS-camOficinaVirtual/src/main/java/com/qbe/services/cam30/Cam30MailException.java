package com.qbe.services.cam30;

public class Cam30MailException extends Exception {

	public Cam30MailException() {
		// TODO Auto-generated constructor stub
	}

	public Cam30MailException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public Cam30MailException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public Cam30MailException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
