package com.qbe.services.cam30;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.cam30.Cam30MailException;
import com.qbe.services.cam30.camA_EnviarMail;
import com.qbe.vbcompat.string.StringHolder;

public class camA_EnviarMailTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIAction_Execute() {
		String request = "<Request>" + 
		"<FORMAT_FILE>AlertasxMailCtrl</FORMAT_FILE>" +
		"<TO><![CDATA[ramiro@snoopconsulting.com]]></TO>" +
		"<IMPORTANCE>1</IMPORTANCE>" +
		"<REPLYTO><![CDATA[replytoer.request@zurich.com.ar]]></REPLYTO>" +
		"<SUBJECT><![CDATA[(OV) Envío de Alertas por Email - Informe de Ejecución]]></SUBJECT>" +
		"<PARAMETROS>" +
		" <PARAMETRO>" +
		"   <PARAM_NOMBRE><![CDATA[%%USUARIO%%]]></PARAM_NOMBRE>" +
		"   <PARAM_VALOR><![CDATA[Joan Carles]]></PARAM_VALOR>" +
		" </PARAMETRO>" +
		"</PARAMETROS>" +
	"</Request>";

	
//				"<Request>" + 
//"	<FROM><![CDATA[Cotizacion ICO Modular - Mercado Abierto <HSBC>]]></FROM>" +
//"	<REPLYTO><![CDATA[none]]></REPLYTO>" +
//"	<IMPORTANCE>2</IMPORTANCE>" +
//"	<BODYFORMAT>0</BODYFORMAT>" +
//"	<MAILFORMAT>0</MAILFORMAT>" +
//"	<SUBJECT>Nueva cotización de ICO Modular desde Mercado Abierto</SUBJECT>" +
//"	<BODY><![CDATA[" +
//"			<font size='2' face='Arial, Helvetica, sans-serif'>" +
//"				Le enviamos la cotización que realizó un cliente que desea ser contactado." + 
//"				<BR/>" +
//"				</font>" +
//"		]]></BODY>" +
//"</Request>";

		camA_EnviarMail mailer = new camA_EnviarMail();
		StringHolder response = new StringHolder();
		mailer.IAction_Execute(request, response, null);
		assertTrue(response.getValue().contains("EMAIL ENVIADO") || response.getValue().contains("NO SE PUDO ENVIAR EL EMAIL"));
	}

}
