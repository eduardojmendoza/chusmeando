package com.qbe.services.mobile;

import static org.junit.Assert.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

public class SegurosMobileServiceImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	//Nonsense porque la impl es dummy! hay que mandarlo via OSB
	public void testIngreso() throws SegurosMobileServiceException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		SegurosMobileServiceImpl service = new SegurosMobileServiceImpl();
		String usuario = "ramiro@snoopconsulting.com";
		String password = "SNOOP123";
		String ipOrigen = "1.1.1.1";
		TipoDocumento dni = TipoDocumento.DNI;
		
		DataIngresoPaso1 dataPaso1 = service.ingresarPaso1(usuario, password, ipOrigen);
		System.out.println("dataPaso1: " + dataPaso1);
		assertNotNull(dataPaso1);
		
		DocumentoIncompleto di = dataPaso1.getDocumentoIncompleto();
		List<DigitoFaltante> dfaltantes = di.getDigitos();
		
		String nrodni = "23626965";

		Digitos digitos = new Digitos();
		Digito d1 = new Digito();
		d1.setId(dfaltantes.get(0).getId());
		d1.setValor(Integer.parseInt(String.valueOf(nrodni.charAt(dfaltantes.get(0).getPos() - 1))));
		
		Digito d2 = new Digito();
		d2.setId(dfaltantes.get(1).getId());
		d2.setValor(Integer.parseInt(String.valueOf(nrodni.charAt(dfaltantes.get(1).getPos() - 1))));
		
		Digito d3 = new Digito();
		d3.setId(dfaltantes.get(2).getId());
		d3.setValor(Integer.parseInt(String.valueOf(nrodni.charAt(dfaltantes.get(2).getPos() - 1))));
		
		
		digitos.getDigitos().add(d1);
		digitos.getDigitos().add(d2);
		digitos.getDigitos().add(d3);
		System.out.println("Paso2. enviando digitos: " + digitos);
		DataIngresoPaso2 dataPaso2 = service.ingresarPaso2(usuario, password, ipOrigen, dni, digitos);
		System.out.println("respuesta dataPaso2: " + dataPaso2);
				
		String ingresoType = dataPaso2.getType();
		
		assertNotNull(ingresoType);
	}

	@Test
	public void testGetSucursales() throws SegurosMobileServiceException {
		SegurosMobileServiceImpl service = new SegurosMobileServiceImpl();
		Sucursales sucs = service.getSucursales();
		assertNotNull(sucs);
	}

	@Test
	public void testGetTiposDocumento() throws SegurosMobileServiceException {
		SegurosMobileServiceImpl service = new SegurosMobileServiceImpl();
		TiposDocumento tdocs = service.getTiposDocumento();
		assertNotNull(tdocs);
	}
	
	@Test
	@Ignore //FIXME Este test no funciona porque con estos datos trae una póliza de Insis, y al correrlo en una maquina distinta a la 98 no existe el PDF
	public void testEnviarCertificados() throws SegurosMobileServiceException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		SegurosMobileServiceImpl service = new SegurosMobileServiceImpl();
		IdPoliza idpol = new IdPoliza();
		idpol.ramopcod = "AUS1";
		idpol.polizann = 0;
		idpol.polizsec = 1;
		idpol.certipol = 0;
		idpol.certiann = 1;
		idpol.certisec = 559115;
		
		EnviarCertificadosPorMailLog log = service.enviarCertificadosPorMail(idpol, TipoCertificado.CertificadoMercosur, "ramiro@snoop.com.ar");
		assertNotNull(log);
	}

	@Test
	public void testSendReminders() throws SegurosMobileServiceException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		SegurosMobileServiceImpl service = new SegurosMobileServiceImpl();
		service.sendVencimientoRegistroMails();
	}

	@Test
	public void testGetPreguntas() throws SegurosMobileServiceException {
		SegurosMobileServiceImpl service = new SegurosMobileServiceImpl();
		PreguntasSeguridad tdocs = service.getPreguntas();
		assertNotNull(tdocs);
	}
	
	@Test
	public void testParsefechas() {
		Pattern p = Pattern.compile("([0-9]{2}/[0-9]{2}/[0-9]{4}) - ([0-9]{2}/[0-9]{2}/[0-9]{4})");
		Matcher m = p.matcher("07/04/2012 - 07/04/2013");

		if (m.find()) {
//		    System.out.println(m.group(1));
//		    System.out.println(m.group(2));
		}		
		
	}
	
}
