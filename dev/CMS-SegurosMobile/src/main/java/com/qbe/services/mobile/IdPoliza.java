package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "IdPoliza")
public class IdPoliza {

	@XmlElement(nillable=true, required=true)
	public String ramopcod;

	@XmlElement(nillable=true, required=true)
	public int polizann;

	@XmlElement(nillable=true, required=true)
	public int polizsec;

	@XmlElement(nillable=true, required=true)
	public int certipol;

	@XmlElement(nillable=true, required=true)
	public int certiann;

	@XmlElement(nillable=true, required=true)
	public int certisec;

	@XmlElement(nillable=true, required=false)
	public int suplenum = 0;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	
}
