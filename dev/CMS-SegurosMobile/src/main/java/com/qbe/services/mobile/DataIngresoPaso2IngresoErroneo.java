package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso2IngresoErroneo", propOrder = {
	"type",
    "codError",
    "codResultado",
    "msgResultado"
})
@XmlRootElement(name = "IngresoErroneo")
public class DataIngresoPaso2IngresoErroneo extends DataIngresoPaso2 {

	@XmlElement(nillable=true, required=true, defaultValue = "IngresoErroneo")
	private final String type = "IngresoErroneo"; 

	@XmlElement(nillable=true, required=true)
	private String codError;

	@XmlElement(nillable=true, required=true)
	private String codResultado;
	
	@XmlElement(nillable=true, required=true)
	private String msgResultado;
	
	public DataIngresoPaso2IngresoErroneo() {
	}

	public String getCodError() {
		return codError;
	}

	public void setCodError(String codError) {
		this.codError = codError;
	}

	public String getCodResultado() {
		return codResultado;
	}

	public void setCodResultado(String codResultado) {
		this.codResultado = codResultado;
	}

	public String getMsgResultado() {
		return msgResultado;
	}

	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
