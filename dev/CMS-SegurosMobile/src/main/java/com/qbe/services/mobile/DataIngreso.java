package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "DataIngreso")
public class DataIngreso {
	
	@XmlElement(nillable=true, required=true)
	private RespuestaBasica resultadoIngreso;
	
	@XmlElement(nillable=true, required=true)
	private DataInicio dataInicio;

	public RespuestaBasica getResultadoIngreso() {
		return resultadoIngreso;
	}

	public void setResultadoIngreso(RespuestaBasica resultadoIngreso) {
		this.resultadoIngreso = resultadoIngreso;
	}

	public DataInicio getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(DataInicio dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
