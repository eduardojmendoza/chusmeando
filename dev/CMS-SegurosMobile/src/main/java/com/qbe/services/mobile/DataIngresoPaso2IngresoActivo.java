package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso2IngresoActivo", propOrder = {
	"type",
    "resultado",
    "cliente",
    "lastLogon",
    "documento"
})
@XmlRootElement(name = "IngresoActivo")
public class DataIngresoPaso2IngresoActivo extends DataIngresoPaso2 {

	@XmlElement(nillable=true, required=true, defaultValue = "IngresoActivo")
	private final String type = "IngresoActivo"; 

	@XmlElement(nillable=true, required=true)
	private String resultado; // OK o ...?? //INGRESO

	@XmlElement(nillable=true, required=true)
	private Cliente cliente;
	
	@XmlElement(nillable=true, required=true)
	private String lastLogon;
	
	@XmlElement(nillable=true, required=true)
	private Documento documento;

	public DataIngresoPaso2IngresoActivo() {
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getLastLogon() {
		return lastLogon;
	}

	public void setLastLogon(String lastLogon) {
		this.lastLogon = lastLogon;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public String getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
