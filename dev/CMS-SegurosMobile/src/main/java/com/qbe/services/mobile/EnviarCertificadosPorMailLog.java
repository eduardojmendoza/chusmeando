package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnviarCertificadosPorMailLog", propOrder = {
    "codResultado",
    "imprimirPolizaRequest",
    "imprimirPolizaResponse",
    "filespec"
})
@XmlRootElement(name = "EnviarCertificadosPorMailLog")
public class EnviarCertificadosPorMailLog {

	@XmlElement(nillable=false, required=true)
	protected String codResultado;
	
	@XmlElement(nillable=false, required=true)
	protected String imprimirPolizaRequest;
	
	@XmlElement(nillable=false, required=true)
	protected String imprimirPolizaResponse;

	@XmlElement(nillable=false, required=true)
	protected String filespec;

	public String getCodResultado() {
		return codResultado;
	}

	public void setCodResultado(String codResultado) {
		this.codResultado = codResultado;
	}

	public String getImprimirPolizaRequest() {
		return imprimirPolizaRequest;
	}

	public void setImprimirPolizaRequest(String imprimirPolizaRequest) {
		this.imprimirPolizaRequest = imprimirPolizaRequest;
	}

	public String getImprimirPolizaResponse() {
		return imprimirPolizaResponse;
	}

	public void setImprimirPolizaResponse(String imprimirPolizaResponse) {
		this.imprimirPolizaResponse = imprimirPolizaResponse;
	}

	public String getFilespec() {
		return filespec;
	}

	public void setFilespec(String filespec) {
		this.filespec = filespec;
	}
}
