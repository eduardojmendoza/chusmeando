package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PreguntasSeguridad", propOrder = { "pregunta" })
public class PreguntasSeguridad {

	@XmlElementWrapper(name = "preguntas")
	@XmlElement(nillable = true, required = true)
	private List<PreguntaSeguridad> pregunta = new ArrayList<PreguntaSeguridad>();

	public List<PreguntaSeguridad> getPregunta() {
		return pregunta;
	}

	public void setPregunta(List<PreguntaSeguridad> pregunta) {
		this.pregunta = pregunta;
	}

	public void addSucursal(PreguntaSeguridad preguntaSeguridad) {
		getPregunta().add(preguntaSeguridad);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
