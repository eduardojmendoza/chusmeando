package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoIncompleto", propOrder = {
    "numero",
    "longitud",
    "digito"
})
public class DocumentoIncompleto {
	
	@XmlElement(nillable=false, required=true)
	private String numero;

	@XmlElement(nillable=false, required=true)
	private int longitud;
	
	@XmlElementWrapper(name = "digitos")
	@XmlElement(nillable=true, required=true)
	private List<DigitoFaltante> digito = new ArrayList<DigitoFaltante>();

	public DocumentoIncompleto() {
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public List<DigitoFaltante> getDigitos() {
		return digito;
	}

	public void setDigitos(List<DigitoFaltante> digito) {
		this.digito = digito;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
