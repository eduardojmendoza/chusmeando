package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaBasica", propOrder = {
    "codResultado",
    "codError",
    "msgResultado"})
public class RespuestaBasica {

	@XmlElement(nillable = false, required = true)
	private String codResultado;
	
	@XmlElement(nillable = true, required = true)
	private String codError;
	
	@XmlElement(nillable = true, required = true)
	private String msgResultado;

	public RespuestaBasica() {
		super();
	}

	public String getCodResultado() {
		return codResultado;
	}

	public void setCodResultado(String codResultado) {
		this.codResultado = codResultado;
	}

	public String getCodError() {
		return codError;
	}

	public void setCodError(String codError) {
		this.codError = codError;
	}

	public String getMsgResultado() {
		return msgResultado;
	}

	public void setMsgResultado(String msgResultado) {
		this.msgResultado = msgResultado;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}