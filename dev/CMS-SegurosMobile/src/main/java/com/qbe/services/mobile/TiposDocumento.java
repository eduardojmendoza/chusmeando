package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TiposDocumento", propOrder = {
    "tipoDocumento"
})
public class TiposDocumento {

	@XmlElementWrapper(name = "tiposDocumento")
	@XmlElement(nillable=true, required=true)
	private List<TipoDocumento> tipoDocumento = new ArrayList<TipoDocumento>();

	public void addTipoDocumento(TipoDocumento s) {
		this.tipoDocumento.add(s);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
