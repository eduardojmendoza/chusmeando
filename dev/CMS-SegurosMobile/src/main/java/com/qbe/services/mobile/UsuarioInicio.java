package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Wrap a 
 * 
				<CLIENSEC>101879610</CLIENSEC>
				<TIPODOCU>1</TIPODOCU>
				<NUMEDOCU>18069158</NUMEDOCU>
				<CLIENAP1>ARMATI</CLIENAP1>
				<CLIENAP2/>
				<CLIENNOM>MARIA ADRIANA</CLIENNOM>
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UsuarioInicio", propOrder = {
	"cliensec",
    "documento",
    "apellido",
    "nombre"
})
@XmlRootElement(name = "UsuarioInicio")
public class UsuarioInicio {

	@XmlElement(nillable=true, required=true)
	public int cliensec;

	@XmlElement(nillable=true, required=true)
	public Documento documento;
	
	@XmlElement(nillable=true, required=true)
	public String apellido;
	
	@XmlElement(nillable=true, required=true)
	public String nombre;

	public int getCliensec() {
		return cliensec;
	}

	public void setCliensec(int cliensec) {
		this.cliensec = cliensec;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
