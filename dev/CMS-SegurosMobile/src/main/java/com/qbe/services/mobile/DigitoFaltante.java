package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DigitoFaltante", propOrder = {
    "id",
    "pos"
})
public class DigitoFaltante {

	@XmlElement(nillable=false, required=true)
	private int id;

	@XmlElement(nillable=false, required=true)
	private int pos;

	public DigitoFaltante() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
