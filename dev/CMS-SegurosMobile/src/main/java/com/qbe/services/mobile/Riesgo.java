package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "Riesgo")
/**
 * Mapeado desde el 1202
 * 
 * 	<MOTORNUM><![CDATA[BAH326286]]></MOTORNUM>
	<CHASINUM><![CDATA[98WKB452574043291]]></CHASINUM>
	<AUMARDES><![CDATA[VOLKSWAGEN]]></AUMARDES>
	<AUMODDES><![CDATA[FOX 1.6]]></AUMODDES>
	<AUSMODES><![CDATA[TRENDLINE PLUS]]></AUSMODES>	
	<AUADIDES><![CDATA[5 PTAS]]></AUADIDES>	
	<AUUSODES><![CDATA[PARTICULAR]]></AUUSODES>
	<AUCOLDES><![CDATA[NEGRO]]></AUCOLDES>
	

 * @author ramiro
 *
 */
public class Riesgo {

	/**
	 * MOTORNUM
	 */
	@XmlElement
	private String numeroMotor;

	/**
	 * CHASINUM
	 */
	@XmlElement
	private String numeroChasis;

	/**
	 * AUMARDES
	 */
	@XmlElement
	private String marca;

	/**
	 * AUMODDES + AUSMODES + AUADIDES
	 */
	@XmlElement
	private String modelo;

	/**
	 * AUUSODES
	 */
	@XmlElement
	private String uso;

	/**
	 * AUCOLDES
	 */
	@XmlElement
	private String color;

	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
