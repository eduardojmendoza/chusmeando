package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataInicio", propOrder = {
	"producto",
    "usuarioInicio"
})
@XmlRootElement(name = "DataInicio")
public class DataInicio {

	@XmlElementWrapper(name = "productos")
	@XmlElement(nillable=true, required=true)
	private List<Producto> producto = new ArrayList<Producto>();

	@XmlElement(nillable=true, required=true)
	private UsuarioInicio usuarioInicio; 
	
	public List<Producto> getProducto() {
		return producto;
	}

	public void setProducto(List<Producto> producto) {
		this.producto = producto;
	}

	public UsuarioInicio getUsuarioInicio() {
		return usuarioInicio;
	}

	public void setUsuarioInicio(UsuarioInicio usuarioInicio) {
		this.usuarioInicio = usuarioInicio;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
