package com.qbe.services.mobile.dao;

	import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

	import javax.persistence.NonUniqueResultException;

	import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

	/**
	 * 
	 */
	@Component
	public class SegurosMobileDAO extends HibernateDaoSupport {

		protected static Logger logger = Logger.getLogger(SegurosMobileDAO.class.getName());

		/**
		 * Timeout para las queries, en segundos
		 */
		private static final int TIMEOUT = 30;
		
		public SegurosMobileDAO() {
		}

		@Autowired
		public SegurosMobileDAO(SessionFactory sessionFactory) {
			super.setSessionFactory(sessionFactory);
		}

		@Transactional
		public VencimientoRegistroReminder findVencimientoByMail(String mail) throws SegurosMobilePersistenceException {
			try {
				VencimientoRegistroReminder found = (VencimientoRegistroReminder)this.getSessionFactory().getCurrentSession()
					.createQuery("from VencimientoRegistroReminder where mail= :mail ")
					.setString("mail", mail)
					.setTimeout(TIMEOUT)
					.uniqueResult();
				return found;
			} catch (NonUniqueResultException e) {
				throw new SegurosMobilePersistenceException(String.format("Hay más de una entrada para el mail %s",mail));
			}
		}

		@Transactional
		public List<VencimientoRegistroReminder> findAllVencimientos() throws SegurosMobilePersistenceException {
			@SuppressWarnings("unchecked")
			List<VencimientoRegistroReminder> list = (List<VencimientoRegistroReminder>)this.getSessionFactory().getCurrentSession()
				.createQuery("from VencimientoRegistroReminder")
				.setTimeout(TIMEOUT)
				.list();
			return list;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @seeorg.springframework.orm.hibernate3.support.HibernateDaoSupport#
		 * createHibernateTemplate(org.hibernate.SessionFactory)
		 */
		@Override
		protected HibernateTemplate createHibernateTemplate(
				SessionFactory sessionFactory) {
			HibernateTemplate hibernateTemplate = super
					.createHibernateTemplate(sessionFactory);
			hibernateTemplate.setFlushMode(10);  // Esto de 10 de dónde salió?
			return hibernateTemplate;
		}

		@Transactional(readOnly = false)
		public void delete(Collection entitys) {
			getHibernateTemplate().deleteAll(entitys);
		}

		@Transactional(readOnly = false)
		public void delete(Object entity) {
			getHibernateTemplate().delete(entity);
		}

		// @Transactional(readOnly = true)
		public <T> List<T> find(Class<T> entityClass) {
			final List<T> entities = getHibernateTemplate().loadAll(entityClass);
			return entities;
		}

		// @Transactional(readOnly = true)
		@SuppressWarnings("unchecked")
		public <T> List<T> find(String hql) {
			final List<T> entities = getHibernateTemplate().find(hql);
			return entities;
		}

		//@Transactional(readOnly = true)
		public <T> T load(Class<T> entityClass, Serializable id) {
			final T entity = getHibernateTemplate().load(entityClass, id);
			return entity;
		}

		@Transactional(readOnly = false)
		public void persist(Object entity) {
			getHibernateTemplate().saveOrUpdate(entity);
		}

		@Transactional
		public void persist(Object[] entities) {
			for (Object entitie : entities) {
				persist(entitie);
			}
		}

		@Transactional(readOnly = true)
		public void refresh(Object entity) {
			getHibernateTemplate().refresh(entity);
		}

		@Transactional
		public void doWork(Work work) {
			this.getSessionFactory().getCurrentSession().doWork(work);
		}

	}
