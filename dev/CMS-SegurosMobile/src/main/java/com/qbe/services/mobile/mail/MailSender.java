package com.qbe.services.mobile.mail;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StreamUtils;

import com.qbe.services.mobile.FileAttachment;
import com.qbe.services.mobile.SegurosMobileServiceImpl;
import com.qbe.vbcompat.framework.ComponentExecutionException;

public class MailSender {

	private static final String HEADER_CLIENTES_GIF = "mails/header-clientes.gif";

	public static final String TEMPLATE_EMAIL_1_MES = "mails/mail1mes.html";

	public static final String TEMPLATE_EMAIL_2_SEMANAS = "mails/mail2semanas.html";

	private static final String GIF_MIME_TYPE = "image/gif";

	public static final String MAIL_CERTIFICADOS = "mails/mail_certificados.html";
	
	public static final String LOGO_ZURICH_PNG = "mails/ZURICH_Logos_horizontal-COLOR.png";

	public static final String PNG_MIME_TYPE = "image/png";
		
	private static Logger logger = Logger.getLogger(MailSender.class.getName());

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans-seguros-mobile.xml");
		}
		return context;
	}


	/**
	 * Envía mail de aviso de vencimiento de registros
	 * 
	 * @param email
	 * @param viaInscripcion
	 * @param claveTemporal
	 * @return
	 */
	public boolean sendMailVencimiento(String template, String fecha, String destinatario) {
		String subject = "Aviso de vencimiento de tu registro de conducir";
		String fromAddress = "Mis Seguros <misseguros@zurich.com>";
		String replyToAddress = "Contacto Zurich <contactenos.patrimoniales@zurich.com>";

		try {
			JavaMailSender mailSender = (JavaMailSender) getContext().getBean("mailSender");
			if (mailSender == null) {
				throw new ComponentExecutionException(
						"No está configurado el mailSender en Seguros Mobile. Revisar la configuración de Spring");
			}

			MimeMessage message = mailSender.createMimeMessage();

			// use the true flag to indicate you need a multipart message
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(fromAddress);
			helper.setTo(destinatario);
			helper.setReplyTo(replyToAddress);
			helper.setSubject(subject);
			// http://docs.spring.io/spring/docs/2.0.x/api/org/springframework/mail/javamail/MimeMessageHelper.html#setPriority(int)
			// priority - the priority value; typically between 1 (highest) and
			// 5 (lowest)
			helper.setPriority(3);
			boolean htmlFormat = true;
			// use the true flag to indicate the text included is HTML
			String body = null;
			try {
					body = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream(template), Charset.forName("UTF-8"));
			} catch (IOException e) {
				body = "<p><strong>Estimado Cliente:</strong></p>" +
						"<p>De acuerdo a la registración que realizaste desde la aplicación móvil, tu registro vencerá el día <strong>%%FECHA%%</strong>.</p>" +
						"<p>Te recordamos que solo faltan dos semanas para dicho evento, recordá renovar el mismo a la brevedad.</p>" +
						"<p>Cuando cuentes con el nuevo registro, actualizá su fecha de vencimiento en la aplicación para que volvamos a recordarte en unos años.</p>" +
						"<p>Muchas gracias.</p>";
			}
			body = StringUtils.replace(body, "%%FECHA%%", fecha);
			helper.setText(body, htmlFormat);
			
			try {
				helper.addInline("header-clientes", new ByteArrayDataSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(HEADER_CLIENTES_GIF), GIF_MIME_TYPE));
			} catch (IOException e) {
				logger.log(Level.SEVERE, "No puedo incrustar el gif en el mail", e);
			}
			mailSender.send(message);
			return true;
		} catch (MailException e) {
			logger.log(Level.SEVERE, "Al enviar mail", e);
			throw new ComponentExecutionException("Al enviar mail", e);
		} catch (MessagingException e) {
			logger.log(Level.SEVERE, "Al enviar mail", e);
			throw new ComponentExecutionException("Al enviar mail", e);
		}
	}


	public static void sendCertificadosByMail(List<FileAttachment> attachments, String mailAddress) throws MessagingException {
			String subject = "Documentación de su Póliza de Autoscoring";
			String fromAddress = "Zurich <noreply@zurich.com>";
			String replyToAddress = "contactenos.patrimoniales@zurich.com";
	
			// http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/mail.html
	
			// Si quiero cableralo y ejecutar en un test
			// JavaMailSenderImpl sender = new JavaMailSenderImpl();
			// sender.setHost("mail.host.com");
	
			try {
				JavaMailSender mailSender = (JavaMailSender) getContext().getBean("mailSender");
				if (mailSender == null) {
					throw new ComponentExecutionException(
							"No está configurado el mailSender en Scheduler Engine. Revisar la configuración de Spring");
				}
	
				MimeMessage message = mailSender.createMimeMessage();
	
				// use the true flag to indicate you need a multipart message
				MimeMessageHelper helper = new MimeMessageHelper(message, true);
				helper.setFrom(fromAddress);
				helper.setTo(mailAddress);
				helper.setReplyTo(replyToAddress);
				helper.setSubject(subject);
				// http://docs.spring.io/spring/docs/2.0.x/api/org/springframework/mail/javamail/MimeMessageHelper.html#setPriority(int)
				// priority - the priority value; typically between 1 (highest) and 5 (lowest)
				helper.setPriority(3);
				boolean htmlFormat = true;
				// use the true flag to indicate the text included is HTML
				
				//Primero agregar el texto y después los resurces incluídos, si no, no anda
				String body = null;
				try {
						body = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream(MAIL_CERTIFICADOS), Charset.forName("UTF-8"));
				} catch (IOException e) {
					body = "<p><strong>Estimado Cliente:</strong></p>" + 
				"<br>Le enviamos adjunto la documentación de su Póliza solicitada desde nuestra aplicación móvil.</p>" +
	"<p>Le recordamos que para poder visualizar el documento, deberá tener instalado el programa Adobe Acrobat Reader. Si aún no lo tiene, podrá descargarlo desde www.latinamerica.adobe.com.</p>" +
	"<p>Muchas Gracias.</p><br><br>" +
	"<img src=\"cid:ZURICH_Logos_horizontal-COLOR.png\"" +
	"<i>" +
	"<p>No responda este mail, por consultas comuníquese con nuestro Centro de Atención al Cliente al 0810-999-2424 o enviando un correo a contactenos.patrimoniales@zurich.com </p>" +
	"<p>Casa Central: Av. del Libertador 6350 C.A.B.A., C1428ART.</p>" +
	"<p>AVISO DE CONFIDENCIALIDAD. Este mensaje y la información incluida en él es confidencial y está dirigida únicamente al destinatario. Puede contener información privilegiada, confidencial, amparada por el secreto profesional y/o que no deba ser revelada. Si Usted ha recibido este mail por error, por favor comuníquenoslo inmediatamente vía e-mail y tenga la amabilidad de eliminarlo de su sistema, quedando notificado que no deberá copiar este mensaje ni utilizar, divulgar, publicar o distribuir su contenido de modo alguno. La casilla de correo empleada para el envío de este mail es de propiedad de Zurich Aseguradora de Argentina S.A., anteriormente denominada QBE Seguros La Buenos Aires S.A., en trámite de cambio de denominación. Todo mensaje enviado desde o hacia esta dirección de correo electrónico puede ser monitoreado por Zurich Aseguradora de Argentina S.A., anteriormente denominada QBE Seguros La Buenos Aires S.A., en trámite de cambio de denominación. La transmisión de mails no garantiza que la información sea libre de error, ni se garantiza que las comunicaciones por Internet sean enviadas en el plazo correspondiente o que no contengan virus.</p>" +
	"<p>RECORDATORIO DE SEGURIDAD. Zurich Aseguradora de Argentina S.A., anteriormente denominada QBE Seguros La Buenos Aires S.A., en trámite de cambio de denominación, mantiene estrictas normas de seguridad y procedimientos para prevenir el acceso no autorizado a toda información relacionada con Ud. Fuera del proceso standard de acceso a nuestro sitio, le hacemos saber que Zurich Aseguradora de Argentina S.A., anteriormente denominada QBE Seguros La Buenos Aires S.A., en trámite de cambio de denominación, no lo contactará por sí ni por medio de terceros para validar información personal tal como: identificación de cliente, password o número de cuenta. Si Ud. recibiera un requerimiento de este tipo, agradeceremos se comunique de inmediato a nuestro Centro de Atención al Cliente al 0810-999-2424 para un conveniente asesoramiento.</p>" +
	"</i>";
				}
				helper.setText(body, htmlFormat);
				
				try {
					helper.addInline("ZURICH_Logos_horizontal-COLOR.png", new ByteArrayDataSource(Thread.currentThread().getContextClassLoader().getResourceAsStream(LOGO_ZURICH_PNG), PNG_MIME_TYPE));
				} catch (IOException e) {
					logger.log(Level.SEVERE, "No puedo incrustar el PNG en el mail, igualmente lo enviamos", e);
				}
	
				for (FileAttachment fileAttachment : attachments) {
					String attachmentName = fileAttachment.getNombre();
					if ( !attachmentName.endsWith(".pdf")) {
						attachmentName = attachmentName + ".pdf";
					}
					helper.addAttachment(attachmentName, new ByteArrayDataSource(fileAttachment.getContent(), SegurosMobileServiceImpl.PDF_MIME_TYPE));
				}
				mailSender.send(message);
			} catch (MailException e) {
				logger.log(Level.SEVERE, "Al enviar mail", e);
				throw e;
			} catch (MessagingException e) {
				logger.log(Level.SEVERE, "Al enviar mail", e);
				throw e;
			}
		}

}
