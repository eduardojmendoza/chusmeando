package com.qbe.services.mobile;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Tomado de http://stackoverflow.com/questions/7966506/marshalling-enum-attributes-into-xml-using-jaxb-and-jaxws-annotations
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public enum TipoCertificado {
	
	PolizaCompleta("PC", "Póliza Completa", "CO"),
	CertificadoMercosur("CM", "Certificado Mercosur", "OR");
	
    @XmlElement(nillable=false, required=true)
    private String codigo = null;

    @XmlElement(nillable=false, required=true)
    private String nombre = null;

    @XmlElement(nillable=false, required=true)
    private String tipoImpreso = null;

    private TipoCertificado(String codigo, String nombre, String tipoImpreso) {
    	this.codigo = codigo;
    	this.nombre = nombre;
    	this.tipoImpreso = tipoImpreso;
    }

    public String getNombre() {
        return nombre;
    }
    
	public String getCodigo() {
		return codigo;
	}

	public String getTipoImpreso() {
		return tipoImpreso;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
