package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Digitos", propOrder = {
    "digito"
})
public class Digitos {

	private List<Digito> digito = new ArrayList<Digito>();

	public Digitos() {

	}

	public List<Digito> getDigitos() {
		return digito;
	}

	public void setDigitos(List<Digito> digitos) {
		this.digito = digitos;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
