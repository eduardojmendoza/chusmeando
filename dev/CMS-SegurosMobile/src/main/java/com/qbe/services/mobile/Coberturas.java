package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "Coberturas")
/**
 * Mapeado desde el 1117
 * 
 * <PLANNCOD>4TERCCOMPL</PLANNCOD>
<PLANNDAB>Terceros Completos</PLANNDAB>
<SUMALBA>150500</SUMALBA>
<SUMAASEG>150500</SUMAASEG>
 *
 */ 
public class Coberturas {
	
	/**
	 * PLANNCOD
	 */
	@XmlElement
	private String codigoPlan;

	/**
	 * PLANNDAB
	 */
	@XmlElement
	private String descripcionPlan;

	/**
	 * SUMALBA
	 */
	@XmlElement
	private String sumaLBA;

	/**
	 * SUMAASEG
	 */
	@XmlElement
	private String sumaAsegurada;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
}
