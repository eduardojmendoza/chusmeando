package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "Producto")
public class Producto {

	@XmlElement(nillable=true, required=true)
	public String ciaascod;

	@XmlElement(nillable=true, required=true)
	public String ramopcod;

	@XmlElement(nillable=true, required=true)
	public int polizann;

	@XmlElement(nillable=true, required=true)
	public int polizsec;

	@XmlElement(nillable=true, required=true)
	public int certipol;

	@XmlElement(nillable=true, required=true)
	public int certiann;

	@XmlElement(nillable=true, required=true)
	public int certisec;

	@XmlElement(nillable=true, required=true)
	public int suplenum;

	@XmlElement(nillable=true, required=true)
	public String ramopdes;

	@XmlElement(nillable=true, required=true)
	public int cliensecv;

	@XmlElement(nillable=true, required=true)
	public String clienap1v;

	@XmlElement(nillable=true, required=true)
	public String clienap2v;

	@XmlElement(nillable=true, required=true)
	public String cliennomv;

	@XmlElement(nillable=true, required=true)
	public int agentcod;

	@XmlElement(nillable=true, required=true)
	public String agentcla;

	@XmlElement(nillable=true, required=true)
	public String tomaries;

	@XmlElement(nillable=true, required=true)
	public String situcpol;

	@XmlElement(nillable=true, required=true)
	public int emisiann;

	@XmlElement(nillable=true, required=true)
	public int emisimes;

	@XmlElement(nillable=true, required=true)
	public int emisidia;

	@XmlElement(nillable=true, required=true)
	public String tipoprod;

	@XmlElement(nillable=true, required=true)
	public String patente;

	@XmlElement(nillable=true, required=true)
	public String dato1;

	@XmlElement(nillable=true, required=true)
	public String dato2;

	@XmlElement(nillable=true, required=true)
	public String dato3;

	@XmlElement(nillable=true, required=true)
	public String dato4;

	@XmlElement(nillable=true, required=true)
	public String cobrodes;

	@XmlElement(nillable=true, required=true)
	public String swsuscri;

	@XmlElement(nillable=true, required=true)
	public String mail;

	@XmlElement(nillable=true, required=true)
	public String clave;

	@XmlElement(nillable=true, required=true)
	public String swclave;

	@XmlElement(nillable=true, required=true)
	public String mensasin;

	@XmlElement(nillable=true, required=true)
	public String mensasus;

	@XmlElement(nillable=true, required=true)
	public String swimprim;

	@XmlElement(nillable=true, required=true)
	public String cobranza;

	@XmlElement(nillable=true, required=true)
	public String siniestro;

	@XmlElement(nillable=true, required=true)
	public String constancia;
	
	@XmlElement(nillable=true, required=true)
	public String habilitadoNBWS;

	@XmlElement(nillable=true, required=true)
	public String positiveid;

	@XmlElement(nillable=true, required=true)
	public String polizaExcluida;

	@XmlElement(nillable=true, required=true)
	public String habilitadoNavegacion;

	@XmlElement(nillable=true, required=true)
	public String masDeCincoCert;

	@XmlElement(nillable=true, required=true)
	public String habilitadoEPoliza;
	
	@XmlElement(nillable=true, required=true)
	public String vigenciaDesde;

	@XmlElement(nillable=true, required=true)
	public String vigenciaHasta;

	@XmlElement
	public DetallePoliza detallePoliza;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
