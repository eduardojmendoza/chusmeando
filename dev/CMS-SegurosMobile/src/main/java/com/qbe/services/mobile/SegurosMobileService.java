package com.qbe.services.mobile;

import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.qbe.services.mobile.dao.VencimientoRegistroReminder;

/**
 * Este servicio exporta operaciones que son usadas por la aplicación "SegurosMobile".
 * 
 * Está pensado para ser a su vez wrappeado desde OSB, usando el mismo WSDL que se genera de esta interface via JAXB.
 * 
 * @author ramiro
 *
 */
@WebService(
		name = "SegurosMobileService" 
		,targetNamespace = "http://cms.services.qbe.com/segurosmobile"
)
public interface SegurosMobileService {
	
	@WebMethod(operationName = "registrar", action = "registrar")
	public RespuestaBasica registrar ( 
			@WebParam(name = "documento" ) Documento documento, 
			@WebParam(name = "email" ) String email,
			@WebParam(name = "conformidad" ) Boolean conformidad) throws SegurosMobileServiceException;

	@WebMethod(operationName = "ingresarPaso1", action = "ingresarPaso1")
	public DataIngresoPaso1 ingresarPaso1 ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "password" ) String password, 
			@WebParam(name = "ipOrigen" ) String ipOrigen);

	@WebMethod(operationName = "ingresarPaso2", action = "ingresarPaso2")
	public DataIngresoPaso2 ingresarPaso2 ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "password" ) String password, 
			@WebParam(name = "ipOrigen" ) String ipOrigen, 
			@WebParam(name = "tipoDocumento" ) TipoDocumento tipoDocumento, 
			@WebParam(name = "digitos" ) Digitos digitos);
	
	@WebMethod(operationName = "getPreguntas", action = "getPreguntas")
	public PreguntasSeguridad getPreguntas() throws SegurosMobileServiceException;

	@WebMethod(operationName = "primerIngreso", action = "primerIngreso")
	public DataIngreso primerIngreso ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "pregunta" ) String pregunta, 
			@WebParam(name = "respuesta" ) String respuesta, 
			@WebParam(name = "nuevaPassword" ) String nuevaPassword) throws SegurosMobileServiceException;
	
	@WebMethod(operationName = "productosCliente", action = "productosCliente")
	public DataInicio productosCliente ( 
			@WebParam(name = "documento" ) Documento documento);
	
	@WebMethod(operationName = "enviarCertificadosPorMail", action = "enviarCertificadosPorMail")
	public EnviarCertificadosPorMailLog enviarCertificadosPorMail( 
			@WebParam(name = "poliza" ) IdPoliza poliza, 
			@WebParam(name = "certificado" ) TipoCertificado certificado, 
			@WebParam(name = "mail" ) String mail ) throws SegurosMobileServiceException;

	@WebMethod(operationName = "getSucursales", action = "getSucursales")
	public Sucursales getSucursales() throws SegurosMobileServiceException;

	@WebMethod(operationName = "getTiposDocumento", action = "getTiposDocumento")
	public TiposDocumento getTiposDocumento() throws SegurosMobileServiceException;

	@WebMethod(operationName = "getDetallesPoliza", action = "getDetallesPoliza")
	public DetallePoliza getDetallesPoliza(IdPoliza polizaId);

	@WebMethod(operationName = "setVencimientoRegistroReminder", action = "setVencimientoRegistroReminder")
	public boolean setVencimientoRegistroReminder ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "vencimiento" ) Vencimiento vencimiento) throws SegurosMobileServiceException;

	@WebMethod(operationName = "getVencimientoRegistroReminder", action = "getVencimientoRegistroReminder")
	public VencimientoRegistroReminder getVencimientoRegistroReminder ( 
			@WebParam(name = "usuario" ) String usuario) throws SegurosMobileServiceException;

	@WebMethod(operationName = "sendVencimientoRegistroMails", action = "sendVencimientoRegistroMails")
	public void sendVencimientoRegistroMails () throws SegurosMobileServiceException;
}
