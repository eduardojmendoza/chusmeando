package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlEnum
@XmlType ( name = "TipoDocumento")
public enum TipoDocumento {
	
	@XmlEnumValue("DNI")
	DNI("DNI"),
	
	@XmlEnumValue("LE")
	LE("LE"),

	@XmlEnumValue("LC")
	LC("LC"),
	
	@XmlEnumValue("CUIT")
	CUIT("CUIT"),
	
	@XmlEnumValue("CUIL")
	CUIL("CUIL"),

	@XmlEnumValue("CI")
	CI("CI"),
	
	@XmlEnumValue("PASAPORTE")
	PASAPORTE("PASAPORTE"),

	@XmlEnumValue("OTRO")
	OTRO("OTRO");

	private final String nombre;

	private TipoDocumento(String nombre) {
		this.nombre = nombre;
	}
	
	public static TipoDocumento fromValue(String v) {
        for (TipoDocumento c: TipoDocumento.values()) {
            if (c.nombre.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
	/**
	 * Mapea los nombres de documentos a códigos usados por AIS en "DOCUMTIP" et al.
	 * 
	 * @param td
	 * @return
	 */
	public static String nombreToCodigo(TipoDocumento td) {
//        <TIPODOC codigo="01" nombre="DNI"/>
//        <TIPODOC codigo="02" nombre="LE"/>
//        <TIPODOC codigo="03" nombre="LC"/>
//        <TIPODOC codigo="04" nombre="CUIT"/>
//        <TIPODOC codigo="05" nombre="CUIL"/>
//        <TIPODOC codigo="06" nombre="CI"/>
//        <TIPODOC codigo="47" nombre="PASAPORTE"/>
//        <TIPODOC codigo="99" nombre="OTRO"/>

		switch (td) {
		case DNI:
			return "01";
		case LE:
			return "02";
		case LC:
			return "03";
		case CUIT:
			return "04";
		case CUIL:
			return "05";
		case CI:
			return "06";
		case PASAPORTE:
			return "47";
		case OTRO:
			return "99";
		default:
			throw new IllegalArgumentException("Tipo de documento desconocido: " + td);
		} 
	}
	
	/**
	 * Mapea los nombres de documentos a códigos usados por AIS en "DOCUMTIP" et al.
	 * 
	 * @param td
	 * @return
	 * @throws SegurosMobileServiceException 
	 */
	public static TipoDocumento fromCodigo(String codigo) throws SegurosMobileServiceException {
//        <TIPODOC codigo="01" nombre="DNI"/>
//        <TIPODOC codigo="02" nombre="LE"/>
//        <TIPODOC codigo="03" nombre="LC"/>
//        <TIPODOC codigo="04" nombre="CUIT"/>
//        <TIPODOC codigo="05" nombre="CUIL"/>
//        <TIPODOC codigo="06" nombre="CI"/>
//        <TIPODOC codigo="47" nombre="PASAPORTE"/>
//        <TIPODOC codigo="99" nombre="OTRO"/>

		try {
			int codigoDoc = Integer.parseInt(codigo);
			
			switch (codigoDoc) {
			case 1:
				return DNI;
			case 2:
				return LE;
			case 3:
				return LC;
			case 4:
				return CUIT;
			case 5:
				return CUIL;
			case 6:
				return CI;
			case 47:
				return PASAPORTE;
			case 99:
				return OTRO;
			default:
				throw new IllegalArgumentException("Código de documento desconocido: " + codigo);
			} 
		} catch (NumberFormatException e) {
			String msg = "No pude convertir el codigo de doc a TipoDocumento. Vino " + codigo + " y generó " + e.getMessage();
			throw new SegurosMobileServiceException(msg,e);
		}
	}
	public String getNombre() {
		return nombre;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
