package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso ( value = { DataIngresoPaso2PrimerIngreso.class, DataIngresoPaso2IngresoActivo.class, DataIngresoPaso2IngresoErroneo.class })
public abstract class DataIngresoPaso2 {
	
	public abstract String getType();
	

}
