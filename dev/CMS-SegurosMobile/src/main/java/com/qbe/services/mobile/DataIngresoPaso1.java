package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso1", propOrder = {
    "documentoIncompleto"
})
@XmlRootElement(name = "DataIngresoPaso1")
public class DataIngresoPaso1 extends RespuestaBasica {

	@XmlElement(nillable=false, required=true)
	private DocumentoIncompleto documentoIncompleto;

	public DataIngresoPaso1() {
	}

	public DocumentoIncompleto getDocumentoIncompleto() {
		return documentoIncompleto;
	}

	public void setDocumentoIncompleto(DocumentoIncompleto documentoIncompleto) {
		this.documentoIncompleto = documentoIncompleto;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
