package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataIngresoPaso2PrimerIngreso", propOrder = {
	"type",
    "medioIngreso",
    "accion",
    "viaInscripcion",
    "poliza"
})
@XmlRootElement(name = "PrimerIngreso")
public class DataIngresoPaso2PrimerIngreso extends DataIngresoPaso2 {

	@XmlElement(nillable=true, required=true, defaultValue = "PrimerIngreso")
	private final String type = "PrimerIngreso"; 

	@XmlElement(nillable=true, required=true)
	private String medioIngreso; // CAI es "1er ingreso"

	@XmlElement(nillable=true, required=true)
	private int accion;
	
	@XmlElement(nillable=true, required=true)
	private String viaInscripcion;
	
	@XmlElementWrapper(name = "polizas")
	@XmlElement(nillable=true, required=true)
	private List<PolizaPrimerIngreso> poliza = new ArrayList<PolizaPrimerIngreso>();
	
	public DataIngresoPaso2PrimerIngreso() {
	}

	public String getMedioIngreso() {
		return medioIngreso;
	}

	public void setMedioIngreso(String medioIngreso) {
		this.medioIngreso = medioIngreso;
	}

	public int getAccion() {
		return accion;
	}

	public void setAccion(int accion) {
		this.accion = accion;
	}

	public String getViaInscripcion() {
		return viaInscripcion;
	}

	public void setViaInscripcion(String viaInscripcion) {
		this.viaInscripcion = viaInscripcion;
	}

	public String getType() {
		return type;
	}

	public List<PolizaPrimerIngreso> getPoliza() {
		return poliza;
	}

	public void setPoliza(List<PolizaPrimerIngreso> poliza) {
		this.poliza = poliza;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
