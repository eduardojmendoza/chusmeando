package com.qbe.services.mobile.dao;

public class SegurosMobilePersistenceException extends Exception {

	public SegurosMobilePersistenceException() {
	}

	public SegurosMobilePersistenceException(String message) {
		super(message);
	}

	public SegurosMobilePersistenceException(Throwable cause) {
		super(cause);
	}

	public SegurosMobilePersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

}
