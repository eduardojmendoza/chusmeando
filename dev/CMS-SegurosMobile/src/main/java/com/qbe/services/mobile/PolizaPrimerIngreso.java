package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Datos de la póliza usados para validar la identidad del usuario en el primer ingreso
 * 
 * Tomado de la respuesta del sIngresar:
 * <OPTION 
 * 	COBRODES="VI" 
 * 	DATO2="T85068417" 
 * 	DATO3="JMD293" 
 * 	PID="A" 
 * 	value="AUS1|00|000001|0000|0001|559139">AUTOSCORING (AUS1-00-000001/0000-0001-559139)</OPTION>
 *
 * 
 * @author ramiro
 *
 */

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "Poliza")
public class PolizaPrimerIngreso {

	@XmlElement(nillable=true, required=true)
	public String cobrodes;

	@XmlElement(nillable=true, required=true)
	public String numeroMotor;

	@XmlElement(nillable=true, required=true)
	public String patente;

	@XmlElement(nillable=true, required=true)
	public String pid;

	@XmlElement(nillable=true, required=true)
	public String codigoPoliza;

	@XmlElement(nillable=true, required=true)
	public String textoPoliza;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	
}
