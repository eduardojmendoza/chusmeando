package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.builder.ToStringBuilder;

public enum Estados {

	OK("OK"),
	ERROR("Error");
	
    @XmlElement(nillable=false, required=true)
    private String nombre = null;

    private Estados(String nombre) {
    	this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
