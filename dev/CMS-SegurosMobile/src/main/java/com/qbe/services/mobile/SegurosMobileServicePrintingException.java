package com.qbe.services.mobile;

public class SegurosMobileServicePrintingException extends SegurosMobileServiceException {

	private int codigoError;
	
	private String detalle;
	
	public SegurosMobileServicePrintingException() {
	}

	public SegurosMobileServicePrintingException(String message) {
		super(message);
	}

	public SegurosMobileServicePrintingException(Throwable cause) {
		super(cause);
	}

	public SegurosMobileServicePrintingException(String message, Throwable cause) {
		super(message, cause);
	}

	public SegurosMobileServicePrintingException(String message, int code, String detail) {
		super(message);
		this.codigoError = code;
		this.detalle = detail;
	}

	public int getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(int codigoError) {
		this.codigoError = codigoError;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

}
