package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Productos", propOrder = {
    "producto"
})
public class Productos {

	@XmlElementWrapper(name = "productos")
	@XmlElement(nillable=true, required=true)
	private List<Producto> producto = new ArrayList<Producto>();

	public List<Producto> getProducto() {
		return producto;
	}

	public void setProducto(List<Producto> producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
