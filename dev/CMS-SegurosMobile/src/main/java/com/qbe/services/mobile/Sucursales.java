package com.qbe.services.mobile;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sucursales", propOrder = {
    "sucursal"
})
public class Sucursales {

	@XmlElementWrapper(name = "sucursales")
	@XmlElement(nillable=true, required=true)
	private List<Sucursal> sucursal = new ArrayList<Sucursal>();

	public List<Sucursal> getSucursal() {
		return sucursal;
	}

	public void setSucursal(List<Sucursal> sucursal) {
		this.sucursal = sucursal;
	}

	public void addSucursal(Sucursal s) {
		this.sucursal.add(s);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
