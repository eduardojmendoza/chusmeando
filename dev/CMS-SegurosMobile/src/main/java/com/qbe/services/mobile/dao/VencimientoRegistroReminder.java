package com.qbe.services.mobile.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Una entrada de vencimiento de registro para un usuario ( email )
 */
@Entity
@Table(name = "SEGMOB_VENCIREG", uniqueConstraints=
@UniqueConstraint(columnNames={"MAIL"}))
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VencimientoRegistroReminder")
public class VencimientoRegistroReminder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4094813786759437567L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@XmlElement(required=true)
	private Integer id;

	@Column(name = "MAIL", length = 256)
	@XmlElement(required=true)
	private String mail;

	@Column(name = "VENCIMIENTO")
	@XmlSchemaType(name = "date")
	@XmlElement(required=true)
	private Date vencimiento;

	@Column(name = "AVISO_2S_ENVIADO")
	@XmlElement(required=true)
	private Boolean avisoDosSemanasEnviado;
	
	@Column(name = "AVISO_1M_ENVIADO")
	@XmlElement(required=true)
	private Boolean avisoUnMesEnviado;
	
	
	public VencimientoRegistroReminder() {
	}

	public VencimientoRegistroReminder(String mail, Date vencimiento) {
		this.mail = mail;
		this.vencimiento = vencimiento;
	}

	public Date getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}

	/**
	 * Compara todas las properties de ambos objetos
	 */
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof VencimientoRegistroReminder))
			return false;
		VencimientoRegistroReminder castOther = (VencimientoRegistroReminder) other;

		return (this.getId() == castOther.getId())
				&& ((this.getVencimiento() == castOther.getVencimiento()) || (this.getVencimiento() != null
						&& castOther.getVencimiento() != null && this.getVencimiento().equals(
						castOther.getVencimiento())))
				&& ((this.getMail() == castOther.getMail()) || (this.getMail() != null && castOther.getMail() != null && this
						.getMail().equals(castOther.getMail())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getId();
		result = 37 * result + (getVencimiento() == null ? 0 : this.getVencimiento().hashCode());
		result = 37 * result + (getMail() == null ? 0 : this.getMail().hashCode());
		return result;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isAvisoDosSemanasEnviado() {
		return avisoDosSemanasEnviado == null ? false : avisoDosSemanasEnviado;
	}

	public void setAvisoDosSemanasEnviado(Boolean avisoDosSemanasEnviado) {
		this.avisoDosSemanasEnviado = avisoDosSemanasEnviado;
	}

	public boolean isAvisoUnMesEnviado() {
		return avisoUnMesEnviado == null ? false : avisoUnMesEnviado ;
	}

	public void setAvisoUnMesEnviado(Boolean avisoUnMesEnviado) {
		this.avisoUnMesEnviado = avisoUnMesEnviado;
	}

	public Boolean getAvisoDosSemanasEnviado() {
		return avisoDosSemanasEnviado;
	}

	public Boolean getAvisoUnMesEnviado() {
		return avisoUnMesEnviado;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
