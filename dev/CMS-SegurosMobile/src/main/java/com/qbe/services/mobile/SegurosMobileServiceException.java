package com.qbe.services.mobile;

public class SegurosMobileServiceException extends Exception {

	public SegurosMobileServiceException() {
	}

	public SegurosMobileServiceException(String message) {
		super(message);
	}

	public SegurosMobileServiceException(Throwable cause) {
		super(cause);
	}

	public SegurosMobileServiceException(String message, Throwable cause) {
		super(message, cause);
	}

}
