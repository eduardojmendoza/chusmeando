package com.qbe.services.mobile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "DetallePoliza")
public class DetallePoliza {
	
	@XmlElement(nillable=true, required=true)
	private String operpte;

	@XmlElement(nillable=true, required=true)
	private String cliensec;

	@XmlElement(nillable=true, required=true)
	private String cliendes;

	@XmlElement(nillable=true, required=true)
	private String domicsec;

	@XmlElement(nillable=true, required=true)
	private String cuentsec;

	@XmlElement(nillable=true, required=true)
	private String situcpol;

	@XmlElement(nillable=true, required=true)
	private String acreedor;

	@XmlElement(nillable=true, required=true)
	private String fecini;

	@XmlElement(nillable=true, required=true)
	private String fecultre;

	@XmlElement(nillable=true, required=true)
	private String vigenciaDesde;

	@XmlElement(nillable=true, required=true)
	private String vigenciaHasta;

	@XmlElement(nillable=true, required=true)
	private String cobrocod;

	@XmlElement(nillable=true, required=true)
	private String cobrodab;

	@XmlElement(nillable=true, required=true)
	private String cobrotip;

	@XmlElement(nillable=true, required=true)
	private String cobrodes;

	@XmlElement(nillable=true, required=true)
	private String cuentnum;

	@XmlElement(nillable=true, required=true)
	private String campacod;

	@XmlElement(nillable=true, required=true)
	private String campades;

	@XmlElement(nillable=true, required=true)
	private String grupoase;

	@XmlElement(nillable=true, required=true)
	private String clubeco;

	@XmlElement(nillable=true, required=true)
	private String tarjecod;

	@XmlElement(nillable=true, required=true)
	private String optp6511;

	@XmlElement
	private Riesgo riesgo;

	@XmlElement
	private Coberturas coberturas;
	
	
	public String getOperpte() {
		return operpte;
	}


	public void setOperpte(String operpte) {
		this.operpte = operpte;
	}


	public String getCliensec() {
		return cliensec;
	}


	public void setCliensec(String cliensec) {
		this.cliensec = cliensec;
	}


	public String getCliendes() {
		return cliendes;
	}


	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}


	public String getDomicsec() {
		return domicsec;
	}


	public void setDomicsec(String domicsec) {
		this.domicsec = domicsec;
	}


	public String getCuentsec() {
		return cuentsec;
	}


	public void setCuentsec(String cuentsec) {
		this.cuentsec = cuentsec;
	}


	public String getSitucpol() {
		return situcpol;
	}


	public void setSitucpol(String situcpol) {
		this.situcpol = situcpol;
	}


	public String getAcreedor() {
		return acreedor;
	}


	public void setAcreedor(String acreedor) {
		this.acreedor = acreedor;
	}


	public String getFecini() {
		return fecini;
	}


	public void setFecini(String fecini) {
		this.fecini = fecini;
	}


	public String getFecultre() {
		return fecultre;
	}


	/**
	 * Formato: <FECULTRE>07/04/2012 - 07/04/2013</FECULTRE>
	 * 
	 * @param fecultre
	 */
	public void setFecultre(String fecultre) {
		this.fecultre = fecultre;
		Pattern p = Pattern.compile("([0-9]{2}/[0-9]{2}/[0-9]{4}) - ([0-9]{2}/[0-9]{2}/[0-9]{4})");
		Matcher m = p.matcher(fecultre);

		if (m.find()) {
			this.setVigenciaDesde(m.group(1));
		    this.setVigenciaHasta(m.group(2));
		}		
	}


	public String getVigenciaDesde() {
		return vigenciaDesde;
	}


	public void setVigenciaDesde(String vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}


	public String getVigenciaHasta() {
		return vigenciaHasta;
	}


	public void setVigenciaHasta(String vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}


	public String getCobrocod() {
		return cobrocod;
	}


	public void setCobrocod(String cobrocod) {
		this.cobrocod = cobrocod;
	}


	public String getCobrodab() {
		return cobrodab;
	}


	public void setCobrodab(String cobrodab) {
		this.cobrodab = cobrodab;
	}


	public String getCobrotip() {
		return cobrotip;
	}


	public void setCobrotip(String cobrotip) {
		this.cobrotip = cobrotip;
	}


	public String getCobrodes() {
		return cobrodes;
	}


	public void setCobrodes(String cobrodes) {
		this.cobrodes = cobrodes;
	}


	public String getCuentnum() {
		return cuentnum;
	}


	public void setCuentnum(String cuentnum) {
		this.cuentnum = cuentnum;
	}


	public String getCampacod() {
		return campacod;
	}


	public void setCampacod(String campacod) {
		this.campacod = campacod;
	}


	public String getCampades() {
		return campades;
	}


	public void setCampades(String campades) {
		this.campades = campades;
	}


	public String getGrupoase() {
		return grupoase;
	}


	public void setGrupoase(String grupoase) {
		this.grupoase = grupoase;
	}


	public String getClubeco() {
		return clubeco;
	}


	public void setClubeco(String clubeco) {
		this.clubeco = clubeco;
	}


	public String getTarjecod() {
		return tarjecod;
	}


	public void setTarjecod(String tarjecod) {
		this.tarjecod = tarjecod;
	}


	public String getOptp6511() {
		return optp6511;
	}


	public void setOptp6511(String optp6511) {
		this.optp6511 = optp6511;
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
