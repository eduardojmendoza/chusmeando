package com.qbe.services.mobile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;
import com.qbe.services.mobile.dao.SegurosMobileDAO;
import com.qbe.services.mobile.dao.SegurosMobilePersistenceException;
import com.qbe.services.mobile.dao.VencimientoRegistroReminder;
import com.qbe.services.mobile.mail.MailSender;
import com.qbe.services.ovmqemision.impl.FileSpec;
import com.qbe.services.ovmqemision.impl.UnsupportedFileSpecException;
import com.qbe.services.ovmqemision.impl.lbaw_OVGetBinaryFile;
import com.qbe.services.segurosOnline.impl.CommonFunctions;
import com.qbe.services.segurosOnline.impl.nbwsA_setPassword;
import com.qbe.services.segurosOnline.impl.nbwsA_setPreguntas;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

/**
 * Implementación de SegurosMovileService, que implementa dos tipos de operaciones:
 *  
 * En algunas operaciones se comporta como un wrapper de SegurosOnline con alguna lógica de conversión. Estas operaciones 
 * están implementadas en OSB, y en esta clase se definen como dummies para facilitar la generación del WSDL via JAXB y
 * y poder hacer algunas pruebas sin involucrar al OSB.
 * 
 * Otras operaciones incluyen lógica que resultó más cómodo y simple implementar acá; en este caso las operaciones en el
 * OSB son un pasamanos.
 * 
 * @author ramiro
 *
 */
public class SegurosMobileServiceImpl implements SegurosMobileService {

	private static final String NBWS_A_SOLICITUD_WEB = "nbwsA_SolicitudWeb";

	public static final String PDF_MIME_TYPE = "application/pdf";

	private static final String LBAW_OV_IMPRIMIR_POLIZA_ACTION_CODE = "lbaw_OVImprimirPoliza";

	private static final int timeoutSegundos = 120;

	private static Logger logger = Logger.getLogger(SegurosMobileServiceImpl.class.getName());

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans-seguros-mobile.xml");
		}
		return context;
	}

	private OSBConnector getOSBConnector() {
		OSBConnector eng = (OSBConnector)getContext().getBean(OSBConnector.class);
		return eng;
	}

	private SegurosMobileDAO getSegurosMobileDAO() {
		SegurosMobileDAO dao = (SegurosMobileDAO)getContext().getBean("segurosMobileDAO");
		return dao;
	}


	/**
	 * Implementado en OSB, esta es una implementación dummy
	 */
	@Override
	@WebMethod(operationName = "ingresarPaso1", action = "ingresarPaso1")
	public DataIngresoPaso1 ingresarPaso1 ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "password" ) String password, 
			@WebParam(name = "ipOrigen" ) String ipOrigen) {
		
		DataIngresoPaso1 di1 = new DataIngresoPaso1();
		di1.setCodResultado("OK");
		di1.setCodError("0");
		di1.setMsgResultado("OK");

		DocumentoIncompleto doc = new DocumentoIncompleto();
		doc.setNumero("23XXX965");
		doc.setLongitud(8);
		DigitoFaltante rcc1 = new DigitoFaltante();
		rcc1.setId(1);
		rcc1.setPos(3);
		doc.getDigitos().add(rcc1);
		
		DigitoFaltante rcc2 = new DigitoFaltante();
		rcc2.setId(2);
		rcc2.setPos(5);
		doc.getDigitos().add(rcc2);
		
		DigitoFaltante rcc3 = new DigitoFaltante();
		rcc3.setId(3);
		rcc3.setPos(4);
		doc.getDigitos().add(rcc3);
		
		di1.setDocumentoIncompleto(doc);
		return di1;
	}


	/**
	 * Implementado en OSB, esta es una implementación dummy
	 */
	@Override
	@WebMethod(operationName = "ingresarPaso2", action = "ingresarPaso2")
	public DataIngresoPaso2 ingresarPaso2 ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "password" ) String password, 
			@WebParam(name = "ipOrigen" ) String ipOrigen, 
			@WebParam(name = "tipoDocumento" ) TipoDocumento tipoDocumento, 
			@WebParam(name = "digitos" ) Digitos digitos) {
		
		
	DataIngresoPaso2IngresoActivo segundo = new DataIngresoPaso2IngresoActivo();
	segundo.setLastLogon("09/12/2013, a las 18:19");
	segundo.setResultado("OK");
	Cliente c = new Cliente();
	c.setNombre("Ramiro");
	c.setApellido("González Maciel");
	segundo.setCliente(c);
	Documento d = new Documento();
	d.setNumero("23626965");
	d.setTipo(TipoDocumento.DNI);
	segundo.setDocumento(d);
	
	return segundo;
	}


	/**
	 * Retorna datos del cliente y sus productos. Wrapea a sInicio
	 * 
	 * Implementado en OSB, esta es una implementación dummy.
	 * 
	 */
	@Override
	@WebMethod(operationName = "productosCliente", action = "productosCliente")
	public DataInicio productosCliente(
			@WebParam(name = "documento") Documento documento) {

		DataInicio di = new DataInicio();
		di.getProducto().add(getDummyProduct());
		UsuarioInicio dui = new UsuarioInicio();
		dui.setApellido("CMS");
		dui.setNombre("Dummy");
		dui.setCliensec(12345678);
		dui.setDocumento(documento);
		di.setUsuarioInicio(dui);
		return di;
	}

	private Producto getDummyProduct() {
		Producto p = new Producto();
		p.ciaascod = "0001";
		p.ramopcod = "AUS1";
		p.polizann = 0;
		p.polizsec = 1;
		p.certipol = 0;
		p.certiann = 1;
		p.certisec = 411665;
		p.suplenum = 0;
		p.ramopdes = "AUTOSCORING";
		p.cliensecv = 101555163;
		p.clienap1v = "QBE SEGUROS LA BUENO";
		p.clienap2v = "S AIRES";
		p.cliennomv = null;
		p.agentcod = 9243;
		p.agentcla = "PR";
		p.tomaries = "CITROEN C3 10DBS40005787";
		p.situcpol = "PROX.RENOV";
		p.emisiann = 0;
		p.emisimes = 0;
		p.emisidia = 0;
		p.tipoprod = "M";
		p.patente = "JCB046";
		p.dato1 = "00010";
		p.dato2 = "10DBS40005787";
		p.dato3 = "JCB046";
		p.dato4 = "2010";
		p.cobrodes = "MC";
		p.swsuscri = null;
		p.mail = "ramiro@snoopconsulting.com";
		p.clave = null;
		p.swclave = null;
		p.mensasin = null;
		p.mensasus = null;
		p.swimprim = "S";
		p.cobranza = "S";
		p.siniestro = "S";
		p.constancia = "S";
		p.habilitadoNBWS = "S";
		p.positiveid = "A";
		p.polizaExcluida = "N";
		p.habilitadoNavegacion = "S";
		p.masDeCincoCert = "N";
		p.habilitadoEPoliza = "S";
		return p;
	}


	/**
	 * Registra al usuario y le envía un mail con la clave temporal
	 */
	@Override
	@WebMethod(operationName = "registrar", action = "registrar")
	public RespuestaBasica registrar(@WebParam(name = "documento") Documento documento,
			@WebParam(name = "email") String email, @WebParam(name = "conformidad") Boolean conformidad) throws SegurosMobileServiceException {

		logger.log(Level.FINE, String.format("documento [%s], email [%s], conformidad [%s]", documento, email, conformidad));
		
		RespuestaBasica resp = new RespuestaBasica();
		
		String codigoDoc = TipoDocumento.nombreToCodigo(documento.getTipo());
		
		String requestTemplate = "<DOCUMTIP>%s</DOCUMTIP>" +
				"<DOCUMDAT>%s</DOCUMDAT>" +
				"<MAIL>%s</MAIL>" +
				"<CONFORMIDAD>%s</CONFORMIDAD>";
		String request = String.format(requestTemplate,codigoDoc, documento.getNumero(), email, conformidad ? "S" : "N");
	
		try {
			String response = sendRequest(NBWS_A_SOLICITUD_WEB, request);
			XmlDomExtended xmlResp = new XmlDomExtended();
			xmlResp.loadXML(response);
			Node resultadoNode;
			try {
				resultadoNode = xmlResp.selectSingleNode("//Response/Estado/@resultado");
				if ( resultadoNode == null ) {
					throw new SegurosMobileServiceException("Exception al generar el impreso. No puedo leer el Estado/@Resultado. Response: " + response);
				}
				String resultado = XmlDomExtended.getText(resultadoNode);
				if ( "false".equalsIgnoreCase(resultado)) {
					throw new SegurosMobileServiceException("Exception al generar el impreso. Response: " + response);
				}
				
				Node codRes = xmlResp.selectSingleNode("//Response/CODRESULTADO");
				if ( codRes == null ) {
					throw new SegurosMobileServiceException("Exception al generar el impreso. No puedo leer el CODRESULTADO. Response: " + response);
				}
				resp.setCodResultado(XmlDomExtended.getText(codRes));
				
				Node codError = xmlResp.selectSingleNode("//Response/CODERROR");
				if ( codError == null ) {
					throw new SegurosMobileServiceException("Exception al generar el impreso. No puedo leer el CODERROR. Response: " + response);
				}
				resp.setCodError(XmlDomExtended.getText(codError));
				
				Node msgResultado = xmlResp.selectSingleNode("//Response/MSGRESULTADO");
				if ( msgResultado == null ) {
					throw new SegurosMobileServiceException("Exception al generar el impreso. No puedo leer el MSGRESULTADO. Response: " + response);
				}
				resp.setMsgResultado(XmlDomExtended.getText(msgResultado));
				
				return resp;
				
			} catch (XmlDomExtendedException e) {
				String msg = "Exception al procesar la respuesta de la generación del impreso. Response: " + response;
				logger.log(Level.SEVERE,msg,e);
				throw new SegurosMobileServiceException(msg,e);
			}
					
			
		} catch (OSBConnectorException e) {
			String msg = "Al ejecutar " + NBWS_A_SOLICITUD_WEB;
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}
	}

	/**
	 * PrimerIngreso: setea la pregunta y respuesta de seguridad, y después las password. 
	 * Como último paso obtiene los datos de productosCliente
	 * 
	 * Este método NO es atómico, por ejemplo puede setear las preg/resp de seguridad y fallar al setear la password.
	 * 
	 * @return 
	 * @throws SegurosMobileServiceException "EX01: Exception al intentar obtener los datos del ingreso.La password y pregunta sí se establecieron"
	 */
	@Override
	@WebMethod(operationName = "primerIngreso", action = "primerIngreso")
	public DataIngreso primerIngreso(@WebParam(name = "usuario") String usuario,
			@WebParam(name = "pregunta") String pregunta, @WebParam(name = "respuesta") String respuesta,
			@WebParam(name = "nuevaPassword") String nuevaPassword) throws SegurosMobileServiceException {

		DataIngreso di = new DataIngreso();
		RespuestaBasica resp = new RespuestaBasica();
		di.setResultadoIngreso(resp);

		String setPreguntasReqTemplate = "<Request><USUARIO>%s</USUARIO><PREGUNTA>%s</PREGUNTA><RESPUESTA>%s</RESPUESTA></Request>";
		String setPasswdReqTemplate = "<Request><USUARIO>%s</USUARIO><PASSWORD>%s</PASSWORD></Request>";
		String setPreguntasReq = String.format(setPreguntasReqTemplate, usuario, pregunta, respuesta);
		String setPasswdReq = String.format(setPasswdReqTemplate, usuario,nuevaPassword);
		
			try {
				nbwsA_setPreguntas setPreg = new nbwsA_setPreguntas();
				StringHolder respH = new StringHolder();
				int codResultado = setPreg.IAction_Execute(setPreguntasReq, respH, null);
				String response = respH.getValue();
				XmlDomExtended xmlResp = new XmlDomExtended();
				xmlResp.loadXML(response);
				Node resultadoNode;

				resultadoNode = xmlResp.selectSingleNode("//Response/Estado/@resultado");
				if ( resultadoNode == null ) {
					throw new SegurosMobileServiceException("Exception al setear pregunta/respuesta de seguridad. No puedo leer el Estado/@Resultado. Response: " + response);
				}
				String resultado = XmlDomExtended.getText(resultadoNode);
				if ( "false".equalsIgnoreCase(resultado)) {
					throw new SegurosMobileServiceException("Exception al setear pregunta/respuesta de seguridad. Response: " + response);
				}
				
				Node codRes = xmlResp.selectSingleNode("//Response/CODRESULTADO");
				if ( codRes == null ) {
					throw new SegurosMobileServiceException("Exception al setear pregunta/respuesta de seguridad. No puedo leer el CODRESULTADO. Response: " + response);
				}
				resp.setCodResultado(XmlDomExtended.getText(codRes));
				
				Node codError = xmlResp.selectSingleNode("//Response/CODERROR");
				if ( codError == null ) {
					throw new SegurosMobileServiceException("Exception al setear pregunta/respuesta de seguridad. No puedo leer el CODERROR. Response: " + response);
				}
				resp.setCodError(XmlDomExtended.getText(codError));
				
				Node msgResultado = xmlResp.selectSingleNode("//Response/MSGRESULTADO");
				if ( msgResultado == null ) {
					throw new SegurosMobileServiceException("Exception al setear pregunta/respuesta de seguridad. No puedo leer el MSGRESULTADO. Response: " + response);
				}
				resp.setMsgResultado(XmlDomExtended.getText(msgResultado));
								
			} catch (XmlDomExtendedException e) {
				String msg = "Exception al procesar la respuesta de seteo de pregunta/respuesta.";
				logger.log(Level.SEVERE,msg,e);
				throw new SegurosMobileServiceException(msg,e);
			}
					

		if ( "OK".equalsIgnoreCase(resp.getCodResultado())) {
			// Seguimos
				try {
					nbwsA_setPassword setPasswd = new nbwsA_setPassword();
					StringHolder respH = new StringHolder();
					int codResultado = setPasswd.IAction_Execute(setPasswdReq, respH, null);
					String response = respH.getValue();

					XmlDomExtended xmlResp = new XmlDomExtended();
					xmlResp.loadXML(response);
					Node resultadoNode;
					resultadoNode = xmlResp.selectSingleNode("//Response/Estado/@resultado");
					if ( resultadoNode == null ) {
						throw new SegurosMobileServiceException("Exception al setear la password. No puedo leer el Estado/@Resultado. Response: " + response);
					}
					String resultado = XmlDomExtended.getText(resultadoNode);
					if ( "false".equalsIgnoreCase(resultado)) {
						throw new SegurosMobileServiceException("Exception al setear la password. Response: " + response);
					}
					
					Node codRes = xmlResp.selectSingleNode("//Response/CODRESULTADO");
					if ( codRes == null ) {
						throw new SegurosMobileServiceException("Exception al setear la password. No puedo leer el CODRESULTADO. Response: " + response);
					}
					resp.setCodResultado(XmlDomExtended.getText(codRes));
					
					Node codError = xmlResp.selectSingleNode("//Response/CODERROR");
					if ( codError == null ) {
						throw new SegurosMobileServiceException("Exception al setear la password. No puedo leer el CODERROR. Response: " + response);
					}
					resp.setCodError(XmlDomExtended.getText(codError));
					
					Node msgResultado = xmlResp.selectSingleNode("//Response/MSGRESULTADO");
					if ( msgResultado == null ) {
						throw new SegurosMobileServiceException("Exception al setear la password. No puedo leer el MSGRESULTADO. Response: " + response);
					}
					resp.setMsgResultado(XmlDomExtended.getText(msgResultado));
										
				} catch (XmlDomExtendedException e) {
					String msg = "Exception al procesar la respuesta de seteo de la password.";
					logger.log(Level.SEVERE,msg,e);
					throw new SegurosMobileServiceException(msg,e);
				}
		}
		
		// Si pasaron los pasos del ingreso, acá completo el tipo y nro de documento para que en el OSB pueda completar los
		// datos del paso2. No lo puedo hacer acá porque el productosCliente es un dummy.
		// Le seteo acá el tipo y nro de documento porque productosCliente espera eso como parámetro
		if ( "OK".equalsIgnoreCase(resp.getCodResultado())) {
			logger.log(Level.FINE,"Por generar los datos para el primerIngreso");
			
			StringHolder documTip = new StringHolder();
			StringHolder documDat = new StringHolder();
			StringHolder rccPrevio = new StringHolder();
			try {
				String msg = String.format("CommonFunctions.fncObtenerIdentificador(%s,%s,%s,%s",usuario, documTip, documDat, rccPrevio);
//				System.out.println(msg);
				logger.log(Level.FINE,msg);
				CommonFunctions.fncObtenerIdentificador(usuario, documTip, documDat, rccPrevio);
				String respMsg = String.format("CommonFunctions.fncObtenerIdentificador(%s,%s,%s,%s",usuario, documTip.getValue(), documDat.getValue(), rccPrevio.getValue());
//				System.out.println(respMsg);
				logger.log(Level.FINE,respMsg);
				DataInicio dini = new DataInicio();
				di.setDataInicio(dini);
				UsuarioInicio uini = new UsuarioInicio();
				dini.setUsuarioInicio(uini);
				Documento doc = new Documento();
				doc.setNumero(documDat.getValue());
				doc.setTipo(TipoDocumento.fromCodigo(documTip.getValue()));
				uini.setDocumento(doc);
//				System.out.println(di.toString());
			} catch (Exception e) {
				String msg = "EX01: Exception al intentar obtener los datos del ingreso.La password y pregunta sí se establecieron. " + e.getMessage();
				logger.log(Level.SEVERE,msg,e);
				throw new SegurosMobileServiceException(msg,e);
			}

		} else {
			String msg = "Resultado inesperado al setear la password: " + resp.getCodResultado();
			logger.log(Level.SEVERE,msg);
			throw new SegurosMobileServiceException(msg);
			
		}
		return di;
	}

	
	/**
	 * Retorna la lista de preguntas de seguridad
	 * 
	 * TODO Hardwired, no llama al actionCode que las trae de la base por ahora sino que las levanta de un CSV
	 * 
	 * @return
	 * @throws SegurosMobileServiceException 
	 */
	@Override
	@WebMethod(operationName = "getPreguntas", action = "getPreguntas")
	public PreguntasSeguridad getPreguntas() throws SegurosMobileServiceException {
		try {
			PreguntasSeguridad pregs = new PreguntasSeguridad();

			String preguntasCSV = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("Preguntas.csv"), Charset.forName("UTF-8"));
			StringReader sr = new StringReader(preguntasCSV);
			BufferedReader reader = new BufferedReader(sr);
			StrTokenizer csvTokenizer = StrTokenizer.getCSVInstance();
			csvTokenizer.setDelimiterChar(';');
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				csvTokenizer.reset(line);
				String[] preguntaPrim = csvTokenizer.getTokenArray();
				pregs.addSucursal(new PreguntaSeguridad(preguntaPrim));
			}

			return pregs;
		} catch (IOException e) {
			String msg = "Al leer las preguntas";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}
	}
	
	/**
	 * Envía por mail los certificados solicitados por el usuario
	 */
	@Override
	@WebMethod(operationName = "enviarCertificadosPorMail", action = "enviarCertificadosPorMail")
	public EnviarCertificadosPorMailLog enviarCertificadosPorMail( IdPoliza poliza, TipoCertificado certificado, String mailAddress ) throws SegurosMobileServiceException {
		logger.log(Level.FINE, String.format("poliza [%s], certificado [%s], mail [%s]", poliza, certificado, mailAddress));
		
		EnviarCertificadosPorMailLog responseLog = new EnviarCertificadosPorMailLog();
		
		String requestTemplate = "<USUARIO>EX009005L</USUARIO>" +
				"<RAMOPCOD>%s</RAMOPCOD>" +
				"<POLIZANN>%s</POLIZANN>" +
				"<POLIZSEC>%s</POLIZSEC>" +
				"<CERTIPOL>%s</CERTIPOL>" +
				"<CERTIANN>%s</CERTIANN>" +
				"<CERTISEC>%s</CERTISEC>" +
				"<SUPLENUM>%s</SUPLENUM>" +
				"<OPERAPOL>0</OPERAPOL>" +
				"<TIPODOCU>%s</TIPODOCU>" +
				"<TIPOIMPR>%s</TIPOIMPR>";
		if ( certificado == null ) {
			String msg = "Nombre de certificado inválido";
			logger.log(Level.SEVERE,msg);
			throw new SegurosMobileServiceException(msg);
		}
		if ( poliza == null ) {
			String msg = "ID de póliza inválido ( null )";
			logger.log(Level.SEVERE,msg);
			throw new SegurosMobileServiceException(msg);
		}
		String request = String.format(requestTemplate,poliza.ramopcod, poliza.polizann, poliza.polizsec, 
				poliza.certipol, poliza.certiann, poliza.certisec, poliza.suplenum, 
				certificado.getCodigo(), certificado.getTipoImpreso());
		responseLog.imprimirPolizaRequest = request;
		
		try {
			String response = sendRequest(LBAW_OV_IMPRIMIR_POLIZA_ACTION_CODE, request);
			responseLog.imprimirPolizaResponse = response;
			
			XmlDomExtended xmlResp = new XmlDomExtended();
			xmlResp.loadXML(response);
			Node resultadoNode;
			try {
				resultadoNode = xmlResp.selectSingleNode("//Response/Estado/@resultado");
				String resultado = XmlDomExtended.getText(resultadoNode);
				if ( "false".equalsIgnoreCase(resultado)) {
					SegurosMobileServicePrintingException ex = getPrintErrorException(xmlResp); 
					logger.log(Level.WARNING, "Error en la generación de los impresos. Devuelvo SegurosMobileServicePrintingException", ex);
					throw ex;
				}
			} catch (XmlDomExtendedException e) {
				String msg = "Exception al procesar la respuesta de la generación del impreso. Response: " + response;
				logger.log(Level.SEVERE,msg,e);
				throw new SegurosMobileServiceException(msg,e);
			}
					
			try { 
				NodeList fileNodes = xmlResp.selectNodes("//Response/FILES/*");
//				<FILE>
//			      <TIPOHOJA>A4</TIPOHOJA>
//			      <DESCRIPCION>Cláusulas Generales</DESCRIPCION>
//			      <RUTA>insis:///impresiones/oficinavirtual/cst_nl_clausulas_generales_0001AUS10000000100000001559115.pdf</RUTA>
//			   </FILE>

				List<FileAttachment> attachments = new ArrayList<FileAttachment>();
				for (int i = 0; i < fileNodes.getLength(); i++) {
					Node node = fileNodes.item(i);
					FileAttachment fa = new FileAttachment();
					NodeList children = node.getChildNodes();
					for (int j = 0; j < children.getLength(); j++) {
						Node child = children.item(j);
						if ( ! (child instanceof Element) )  {
							continue;
						}
						Element childElem = (Element) child;
						if ( "DESCRIPCION".equals(childElem.getNodeName())) {
							fa.setNombre("" + i + "_" + XmlDomExtended.getText(childElem) + ".pdf");
						} else if ( "RUTA".equals(childElem.getNodeName())) {
							fa.setRuta(XmlDomExtended.getText(childElem));
						}
					}
					
					try {
						FileSpec fs = FileSpec.initFileSpecWaitUpTo(fa.getRuta(), timeoutSegundos,
								lbaw_OVGetBinaryFile.getConfigPlanetPressURL());
						if ( fs == null) {
							String msg = "Al intentar cargar el archivo del impreso ";
							logger.log(Level.SEVERE,msg);
							throw new SegurosMobileServiceException(msg);
						}
						responseLog.filespec = fs.toString();
						byte[] fileContent = fs.getBytes();
						fa.setContent(fileContent);
						attachments.add(fa);
					} catch (UnsupportedFileSpecException e) {
						String msg = "Al intentar cargar el archivo del impreso ";
						logger.log(Level.SEVERE,msg,e);
						throw new SegurosMobileServiceException(msg,e);
					} catch (IOException e) {
						String msg = "Al intentar cargar el archivo del impreso ";
						logger.log(Level.SEVERE,msg,e);
						throw new SegurosMobileServiceException(msg,e);
					}					
				}

				try {
					MailSender.sendCertificadosByMail(attachments, mailAddress);
					responseLog.codResultado = Estados.OK.getNombre();
					return responseLog;
				} catch (MessagingException e) {
					String msg = "Al enviar el impreso ";
					logger.log(Level.SEVERE,msg,e);
					throw new SegurosMobileServiceException(msg,e);
				}

			} catch (XmlDomExtendedException e) {
				String msg = "Error interno al procesar el XML del request " + request;
				logger.log(Level.SEVERE,msg,e);
				throw new SegurosMobileServiceException(msg,e);
			}			
			
		} catch (OSBConnectorException e) {
			String msg = "Al ejecutar " + LBAW_OV_IMPRIMIR_POLIZA_ACTION_CODE;
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}
		
	}
	
	private SegurosMobileServicePrintingException getPrintErrorException(XmlDomExtended xmlResp) {

		
		String mensaje = "En este momento no se pueden reimprimir los documentos.";
		String detalle = "";
		int codigoError = -1;
		
		Node codigoErrorNode;
		try {
			codigoErrorNode = xmlResp.selectSingleNode("//CODIGOERROR");
			if ( codigoErrorNode != null ) {
				String codigoErrorTexto = XmlDomExtended.getText(codigoErrorNode);
				try {
					codigoError = Integer.parseInt(codigoErrorTexto);
					switch (codigoError) {
					case 30:
						detalle = "Motivo: El original aun no ha sido impreso.";
						break;
					case 31:
						detalle = "Motivo: No se encontró la póliza.";
						break;
					case 32:
						detalle = "Motivo: Motivo: Impresión de Operación no permitida (VALIDACIÓN GRO1581A).";
						break;
					case 39:
						detalle = "La póliza seleccionada no posee las condiciones necesarias para la impresión del Libre Deuda";
						break;
					case 40:
						detalle = "Motivo: Error en llamada al GR16000P.";
						break;
					case 50:
						detalle = "Motivo: Póliza dada de Baja.";
						break;
					case 51:
						detalle = "Motivo: Certificado Anulado.";
						break;
					case 52:
						detalle = "Motivo: No se pudo generar el documento.";
						break;
					case 77:
						detalle = "Motivo: Registro bloqueado.";
						break;
					case 88:
						detalle = "Motivo: Esta operación no puede imprimirse en este momento.";
						break;
					case 97:
						detalle = "Motivo: El servicio de impresión no está disponible en este momento por procesos batch.";
						break;
					default:
						detalle = "Código de error: " + codigoError;
						break;
					}
				} catch (NumberFormatException e) {
					logger.log(Level.WARNING, "No pude convertir el código de error a int: " + codigoErrorTexto);
				}
			}
		} catch (XmlDomExtendedException e1) {
			logger.log(Level.WARNING, "No puedo obtener el CODIGOERROR", e1);
		}
    
    return new SegurosMobileServicePrintingException(mensaje + " (" + codigoError + ") " + detalle,codigoError,detalle);
	}

	/**
	 * Ejecuta el action code
	 * @param name
	 * @param actionCode
	 * @param dataRequestXml
	 * @throws MalformedURLException 
	 * @throws OSBConnectorException 
	 * @throws SegurosMobileServiceException 
	 */
	private String sendRequest(String actionCode,
			String dataRequestXml) throws OSBConnectorException, SegurosMobileServiceException {
		String requestXml = "<Request>" + dataRequestXml + "</Request>";
		try {
			String response = getOSBConnector().executeRequest(actionCode, requestXml);
			return response;
		} catch (MalformedURLException e) {
			String msg = "Al leer las preguntas";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		} catch (OSBConnectorException e) {
			throw e;
		}
		
	}

	/**
	 * Retorna la lista de sucursales de QBE. Los datos los obtiene de un archivo local, dado que no hay servicio en QBE de donde obtener estos datos
	 */
	@Override
	@WebMethod(operationName = "getSucursales", action = "getSucursales")
	public Sucursales getSucursales() throws SegurosMobileServiceException {
		try {
			Sucursales sucursales = new Sucursales();
			
			String sucursalesCSV = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("Sucursales.csv"), Charset.forName("UTF-8"));
			StringReader sr = new StringReader(sucursalesCSV);
			BufferedReader reader =
	                new BufferedReader(sr);
			StrTokenizer csvTokenizer = StrTokenizer.getCSVInstance();
			csvTokenizer.setDelimiterChar(';');
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				csvTokenizer.reset(line);
				String[] sucursalPrim =  csvTokenizer.getTokenArray();
				sucursales.addSucursal(new Sucursal(sucursalPrim));
			}
			return sucursales;
		} catch (IOException e) {
			String msg = "Al leer las sucursales";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		} catch (ParseException e) {
			String msg = "Al leer las sucursales";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}
	}
	
	/**
	 * Retorna la lista de tipos de documentos
	 * Es la misma data que se genera en el XSD
	 * 
	 * TODO Hardwired
	 * 
	 */
	@Override
	@WebMethod(operationName = "getTiposDocumento", action = "getTiposDocumento")
	public TiposDocumento getTiposDocumento() throws SegurosMobileServiceException {
		TiposDocumento tiposDocs = new TiposDocumento();
		for (TipoDocumento tipodoc : TipoDocumento.values()) {
			tiposDocs.addTipoDocumento(tipodoc);
		}
		return tiposDocs;
	}
	
	/**
	 * Retorna datos de una póliza ( obtenidos del service 1010 ).
	 * 
	 * Implementado en OSB, esta es una implementación dummy.
	 * 
	 */
	@Override
	@WebMethod(operationName = "getDetallesPoliza", action = "getDetallesPoliza")
	public DetallePoliza getDetallesPoliza(IdPoliza polizaId) {
		DetallePoliza dp = new DetallePoliza();
		dp.setOperpte("");
		dp.setCliensec("1762676");
		dp.setCliendes("VIVIANA PATRICIA EREJOMOVICH");
		dp.setDomicsec("002");
		dp.setCliensec("002");
		dp.setSitucpol("VIGENTE");
		dp.setAcreedor("NO");
		dp.setFecini("07/04/2012");
		dp.setFecultre("07/04/2012 - 07/04/2013");
		dp.setCobrocod("4");
/*
 * 		<COBRODAB>TARJETA</COBRODAB>
<COBROTIP>VI</COBROTIP>
<COBRODES>VISA</COBRODES>
<CUENTNUM>4766060000017525</CUENTNUM>
<CAMPACOD/>
<CAMPADES/>
<GRUPOASE>EREJOMOVICH VIVIANA PATRICIA</GRUPOASE>
<CLUBECO>N</CLUBECO>
<TARJECOD>7</TARJECOD>
<OPTP6511>SIRT06VI</OPTP6511>
 */
		return dp;
	}

	
	/**
	 * Setea el recordatorio para el vencimiento del registro del usuario.
	 * @throws SegurosMobileServiceException 
	 * 
	 */
	@Override
	@WebMethod(operationName = "setVencimientoRegistroReminder", action = "setVencimientoRegistroReminder")
	public boolean setVencimientoRegistroReminder ( 
			@WebParam(name = "usuario" ) String usuario, 
			@WebParam(name = "vencimiento" ) Vencimiento vencimiento) throws SegurosMobileServiceException {
		
		SegurosMobileDAO dao = getSegurosMobileDAO();
		try {
			VencimientoRegistroReminder venciReg =  dao.findVencimientoByMail(usuario);
			if ( venciReg != null) {
				venciReg.setVencimiento(vencimiento.getFecha());
			} else {
				venciReg = new VencimientoRegistroReminder(usuario, vencimiento.getFecha());
			}
			venciReg.setAvisoDosSemanasEnviado(false);
			venciReg.setAvisoUnMesEnviado(false);
			dao.persist(venciReg);
		} catch (SegurosMobilePersistenceException e) {
			String msg = "Al grabar";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}
		
		
		return true;
	}	
	
	/**
	 * Retorna el registro asociado con el usuario.
	 * Si no lo encuentra retorna null
	 */
	@Override
	@WebMethod(operationName = "getVencimientoRegistroReminder", action = "getVencimientoRegistroReminder")
	public VencimientoRegistroReminder getVencimientoRegistroReminder ( 
			@WebParam(name = "usuario" ) String usuario) throws SegurosMobileServiceException {
		SegurosMobileDAO dao = getSegurosMobileDAO();
		try {
			VencimientoRegistroReminder venciReg =  dao.findVencimientoByMail(usuario);
			return venciReg;
		} catch (SegurosMobilePersistenceException e) {
			String msg = "Al buscar";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}
		
	}

	/**
	 * Obtiene las fechas de vencimiento, para los que están a 2 semanas o 1 mes les manda un reminder.
	 * No manda mails a los que se vencieron
	 */
	@Override
	@WebMethod(operationName = "sendVencimientoRegistroMails", action = "sendVencimientoRegistroMails")
	public void sendVencimientoRegistroMails () throws SegurosMobileServiceException {
		SegurosMobileDAO dao = getSegurosMobileDAO();
		
		Date hoy = Calendar.getInstance().getTime();
		
		Calendar cal2s = Calendar.getInstance();
		cal2s.add(Calendar.DAY_OF_MONTH, 14);
		Date hoyMas2Semanas = cal2s.getTime();
		
		Calendar cal1m = Calendar.getInstance();
		cal1m.add(Calendar.MONTH, 1);
		Date hoyMas1Mes = cal1m.getTime();

		try {
			List<VencimientoRegistroReminder> vtos = dao.findAllVencimientos();
			for (VencimientoRegistroReminder vto : vtos) {
				logger.log(Level.FINE, String.format("Por procesar: %s", vto));
				if ( hoy.before(vto.getVencimiento())) {
					if ( hoyMas1Mes.after(vto.getVencimiento()) && !vto.isAvisoUnMesEnviado()) {
						logger.log(Level.FINE, String.format("Enviando mail de 1 mes por: %s", vto));
						sendAviso1Mes(vto);
					} else {
						//Está con un else para evitar mandar dos avisos en caso de que nos hayamos salteado el de 1 mes por algún error
						if ( hoyMas2Semanas.after(vto.getVencimiento()) && !vto.isAvisoDosSemanasEnviado()) {
							logger.log(Level.FINE, String.format("Enviando mail de 2 semanas por: %s", vto));
							sendAviso2Semanas(vto);
						}
					}
				}
				
			}
		} catch (SegurosMobilePersistenceException e) {
			String msg = "Al enviar mails";
			logger.log(Level.SEVERE,msg,e);
			throw new SegurosMobileServiceException(msg,e);
		}		
	}

	private void sendAviso2Semanas(VencimientoRegistroReminder vto) {
		MailSender sender = new MailSender();
		sender.sendMailVencimiento(MailSender.TEMPLATE_EMAIL_2_SEMANAS, getDateFormatter().format(vto.getVencimiento()), vto.getMail());
		vto.setAvisoDosSemanasEnviado(true);
		save(vto);
	}

	private void sendAviso1Mes(VencimientoRegistroReminder vto) {
		MailSender sender = new MailSender();
		sender.sendMailVencimiento(MailSender.TEMPLATE_EMAIL_1_MES, getDateFormatter().format(vto.getVencimiento()), vto.getMail());
		vto.setAvisoUnMesEnviado(true);
		save(vto);
	}
	

	private void save(VencimientoRegistroReminder vto) {
		SegurosMobileDAO dao = getSegurosMobileDAO();
		dao.persist(vto);
	}

	private DateFormat getDateFormatter() {
//		DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		return df;
	}

	
}
