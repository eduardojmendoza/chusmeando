package com.qbe.services.mobile;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "Sucursal")
public class Sucursal {
	
	@XmlElement(nillable=true, required=true)
	public String nombre;

	@XmlElement(nillable=true, required=true)
	public String direccion;

	@XmlElement(nillable=true, required=true)
	public String localidad;

	@XmlElement(nillable=true, required=true)
	public String provincia;

	@XmlElement(nillable=true, required=true)
	public String telefono;

	@XmlElement(nillable=true, required=true)
	public String horario;

	@XmlElement(nillable=true, required=true)
	public String latitudSexa;

	@XmlElement(nillable=true, required=true)
	public String longitudSexa;
	
	@XmlElement(nillable=true, required=true)
	public float latitud;

	@XmlElement(nillable=true, required=true)
	public float longitud;

	public Sucursal() {
	}
	
	public Sucursal(String [] elems) throws ParseException {
		this.nombre = elems[0];
		this.direccion = elems[1];
		this.localidad = elems[2];
		this.provincia = elems[3];
		this.telefono = elems[4];
		this.horario = elems[5];
		this.latitudSexa = elems[6];
		this.longitudSexa = elems[7];
		
		NumberFormat formatter = DecimalFormat.getNumberInstance(Locale.ITALIAN);
		
		this.latitud = formatter.parse(elems[8]).floatValue();
		this.longitud = formatter.parse(elems[9]).floatValue();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getLatitudSexa() {
		return latitudSexa;
	}

	public void setLatitudSexa(String latitudSexa) {
		this.latitudSexa = latitudSexa;
	}

	public String getLongitudSexa() {
		return longitudSexa;
	}

	public void setLongitudSexa(String longitudSexa) {
		this.longitudSexa = longitudSexa;
	}

	public float getLatitud() {
		return latitud;
	}

	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}

	public float getLongitud() {
		return longitud;
	}

	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
