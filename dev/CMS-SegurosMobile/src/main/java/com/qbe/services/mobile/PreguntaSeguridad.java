package com.qbe.services.mobile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.ToStringBuilder;

@XmlAccessorType ( XmlAccessType.FIELD)
@XmlType ( name = "Pregunta")
public class PreguntaSeguridad {
	
	@XmlElement(nillable=true, required=true)
	public String id;

	@XmlElement(nillable=true, required=true)
	public String texto;

	public PreguntaSeguridad() {
	}

	public PreguntaSeguridad(String[] preguntaPrim) {
		this.id = preguntaPrim[0];
		this.texto = preguntaPrim[1];
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}


}
