VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVTipoInspeccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVTipoInspeccion"
Const mcteOpID              As String = "1511"

'Parametros XML de Entrada
Const mcteParam_Usuario         As String = "//USUARIO"
Const mcteParam_EsCero          As String = "//ESCERO"
Const mcteParam_Rastreo         As String = "//RASTREO"
Const mcteParam_EstDispo        As String = "//ESTDISPO"
Const mcteParam_Agentcod        As String = "//AGENTCOD"
Const mcteParam_AgentCla        As String = "//AGENTCLA"
Const mcteParam_Provi           As String = "//PROVI"
Const mcteParam_LocalidadCod    As String = "//LOCALIDADCOD"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarResponse        As String
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarEsCero          As String
    Dim wvarRastreo         As String
    Dim wvarEstDispo        As String
    Dim wvarAgentcod        As String
    Dim wvarAgentCla        As String
    Dim wvarProvi           As String
    Dim wvarLocalidadCod    As String
    Dim wvarRamopcod        As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    Dim wvarPos             As Long
    Dim wvarEstado          As String
    Dim wvarAleatorio       As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarEsCero = .selectSingleNode(mcteParam_EsCero).Text
        wvarRastreo = .selectSingleNode(mcteParam_Rastreo).Text
        wvarEstDispo = Right("0000" & .selectSingleNode(mcteParam_EstDispo).Text, 4)
        wvarAgentcod = Right("0000" & .selectSingleNode(mcteParam_Agentcod).Text, 4)
        If .selectNodes(mcteParam_AgentCla).length = 0 Then
            wvarAgentCla = "PR"
        Else
            wvarAgentCla = Left(.selectSingleNode(mcteParam_AgentCla).Text & Space(2), 2)
        End If
        wvarProvi = Right("00" & .selectSingleNode(mcteParam_Provi).Text, 2)
        wvarLocalidadCod = .selectSingleNode(mcteParam_LocalidadCod).Text
        '
    End With
    '
    wvarStep = 20
    'Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        wvarRamopcod = .selectSingleNode(gcteNodosAutoScoring & gcteRAMOPCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarRamopcod & wvarEsCero & wvarRastreo & wvarEstDispo & wvarAgentcod & wvarAgentCla & wvarProvi & wvarLocalidadCod
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 180
    '
    wvarPos = 89 'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStep = 190
    If Mid(strParseString, 19, 2) = "ER" Then
        '
        wvarStep = 200
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 210
        wvarResult = ""
        wvarEstado = Mid(strParseString, wvarPos, 1)
        '
        If wvarEstado = "O" Then
            'Corro el SP de Aleatorio
            wvarStep = 220
            Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVSQLCotizar.lbaw_OVAleatorio")
            Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
            Set wobjClass = Nothing
            '
            wvarStep = 230
            Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
            With wobjXMLParametros
                .async = False
                Call .loadXML(wvarResponse)
                wvarAleatorio = .selectSingleNode("//ESTADO").Text
            End With
            '
            Set wobjXMLParametros = Nothing
            '
        End If
        '
        wvarStep = 240
        If wvarEstado = "I" Or wvarEstado = "O" Then
            wvarResult = wvarResult & ParseoMensajeEstado(wvarPos, strParseString)
            '
            wvarStep = 250
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 260
            wobjXMLResponse.async = False
            wobjXMLResponse.loadXML (wvarResult)
            '
            If wobjXMLResponse.selectNodes("//R").length <> 0 Then
                wvarStep = 270
                wobjXSLResponse.async = False
                Call wobjXSLResponse.loadXML(p_GetXSLEstados())
                '
                wvarStep = 280
                wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
                '
                wvarStep = 290
                Set wobjXMLResponse = Nothing
                Set wobjXSLResponse = Nothing
                '
            End If
            '
        End If
        '
        wvarStep = 300
        wvarEstado = "<RTA>" & wvarEstado & "</RTA>"
        wvarAleatorio = "<ALEATORIO>" & wvarAleatorio & "</ALEATORIO>"
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarEstado & wvarAleatorio & "<COMBO>" & wvarResult & "</COMBO>" & "</Response>"
    End If
    '
    wvarStep = 310
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function p_GetXSLEstados() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='OPTION'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='value'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='number(substring(.,1,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='flag'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:value-of select='normalize-space(substring(.,3,30))' />"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLEstados = wvarStrXSL
End Function
Private Function ParseoMensajeEstado(wvarPos As Long, strParseString As String) As String
Dim wvarResult As String
Dim wvarCant As Integer
Dim i As Integer
    '
    wvarResult = wvarResult & "<RS>"
    '
    wvarPos = wvarPos + 1
    If Trim(Mid(strParseString, wvarPos, 3)) <> "000" And Trim(Mid(strParseString, wvarPos, 3)) <> "" Then
        wvarCant = CInt(Trim(Mid(strParseString, wvarPos, 3)))
        wvarPos = wvarPos + 3
        For i = 1 To wvarCant
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 33) & "]]></R>"
            wvarPos = wvarPos + 33
        Next
    End If
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensajeEstado = wvarResult
    '
End Function












