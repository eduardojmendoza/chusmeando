VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetDispRastreo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVGetDispRastreo"
Const mcteOpID              As String = "1510"

'Parametros XML de Entrada
Const mcteParam_Usuario         As String = "//USUARIO"
Const mcteParam_ModeAutCod      As String = "//MODEAUTCOD"
Const mcteParam_SumAseg         As String = "//SUMASEG"
Const mcteParam_Provi           As String = "//PROVI"
Const mcteParam_LocalidadCod    As String = "//LOCALIDADCOD"
Const mcteParam_Opcion          As String = "//OPCION"
Const mcteParam_Operacion       As String = "//OPERACION"
Const mcteParam_PresCod         As String = "//PRESCOD"
Const mcteParam_Instala         As String = "//INSTALA"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarResultParcial   As String
    Dim wvarCiaAsCod        As String
    Dim wvarRamopcod        As String
    Dim wvarUsuario         As String
    Dim wvarModeAutCod      As String
    Dim wvarSumAseg         As String
    Dim wvarProvi           As String
    Dim wvarLocalidadCod    As String
    Dim wvarOpcion          As String
    Dim wvarOperacion       As String
    Dim wvarPresCod         As String
    Dim wvarInstala         As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    Dim wvarPos             As Integer
    
    Dim wvarAuxReq          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        wvarModeAutCod = .selectSingleNode(mcteParam_ModeAutCod).Text
        wvarSumAseg = Right(String(15, "0") & Replace(Replace(.selectSingleNode(mcteParam_SumAseg).Text, ",", ""), ".", ""), 15)
        wvarProvi = Right("00" & .selectSingleNode(mcteParam_Provi).Text, 2)
        wvarLocalidadCod = .selectSingleNode(mcteParam_LocalidadCod).Text
        If .selectSingleNode(mcteParam_Opcion).Text = "" Then
            wvarOpcion = "N"
        Else
            wvarOpcion = .selectSingleNode(mcteParam_Opcion).Text
        End If
        wvarOperacion = .selectSingleNode(mcteParam_Operacion).Text
        wvarPresCod = Right$(String(4, " ") & .selectSingleNode(mcteParam_PresCod).Text, 4)
        wvarInstala = .selectSingleNode(mcteParam_Instala).Text
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
        wvarRamopcod = .selectSingleNode(gcteNodosAutoScoring & gcteRAMOPCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarRamopcod & wvarModeAutCod & wvarSumAseg & wvarProvi & wvarLocalidadCod & wvarOpcion & wvarOperacion & wvarPresCod & wvarInstala
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarPos = 120 'cantidad de caracteres ocupados por parámetros de entrada
    '
    wvarStep = 190
    If Mid(strParseString, 19, 2) = "ER" Then
        '
        wvarStep = 200
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarStep = 210
        wvarResult = ""
        wvarAuxReq = Mid(strParseString, wvarPos, 1)
        wvarResult = "<REQ>" & wvarAuxReq & "</REQ>"
        wvarPos = wvarPos + 1
        wvarResult = wvarResult & "<GAMA>" & Mid(strParseString, wvarPos, 1) & "</GAMA>"
        wvarPos = wvarPos + 1
        '
        If wvarOpcion = "S" And (wvarAuxReq = "S" Or wvarAuxReq = "O") Then
            '
            wvarStep = 220
            wvarResultParcial = wvarResultParcial & ParseoMensaje(Mid(strParseString, wvarPos, 6603), "E")
            '
            wvarStep = 230
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 240
            wobjXMLResponse.async = False
            wobjXMLResponse.loadXML (wvarResultParcial)
            '
            If wobjXMLResponse.selectNodes("//R").length <> 0 Then
                wvarStep = 250
                wobjXSLResponse.async = False
                Call wobjXSLResponse.loadXML(p_GetXSL())
                '
                wvarStep = 260
                wvarResultParcial = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
                wvarResult = wvarResult & "<ESTADOS>" & wvarResultParcial & "</ESTADOS>"
                '
                wvarStep = 270
                Set wobjXMLResponse = Nothing
                Set wobjXSLResponse = Nothing
                '
            Else
                '
                wvarStep = 280
                wvarResult = wvarResult & "<ESTADOS></ESTADOS>"
                '
            End If
            '
            wvarStep = 290
            '
            wvarResultParcial = ""
            wvarResultParcial = wvarResultParcial & ParseoMensaje(Mid(strParseString, wvarPos + 6603, 7803), "P")
            '
            wvarStep = 300
            Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
            Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
            '
            wvarStep = 310
            wobjXMLResponse.async = False
            wobjXMLResponse.loadXML (wvarResultParcial)
            '
            If wobjXMLResponse.selectNodes("//R").length <> 0 Then
                wvarStep = 320
                wobjXSLResponse.async = False
                Call wobjXSLResponse.loadXML(p_GetXSLProve())
                '
                wvarStep = 330
                wvarResultParcial = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
                wvarResult = wvarResult & "<PROVEE>" & wvarResultParcial & "</PROVEE>"
                '
                wvarStep = 340
                Set wobjXMLResponse = Nothing
                Set wobjXSLResponse = Nothing
                '
            Else
                '
                wvarStep = 350
                wvarResult = wvarResult & "<PROVEE></PROVEE>"
                '
            End If
            '
        End If
        '
        wvarStep = 355
        wvarPos = wvarPos + 6603 + 7803
        wvarResult = wvarResult & "<DISPGEN>" & Mid(strParseString, wvarPos, 4) & "</DISPGEN>"
        wvarPos = wvarPos + 4
        wvarResult = wvarResult & "<DISPGENDESC>" & Mid(strParseString, wvarPos, 30) & "</DISPGENDESC>"
        wvarStep = 360
        Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        '
    End If
    '
    wvarStep = 370
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarRamopcod & wvarModeAutCod & wvarSumAseg & wvarProvi & wvarLocalidadCod & wvarOpcion & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='OPTION'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='value'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,2))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='flag'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:value-of select='normalize-space(substring(.,3,30))' />"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function
Private Function p_GetXSLProve() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='OPTION'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='value'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,4))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='acccod'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,35,4))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='disvip'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,39,1))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:value-of select='normalize-space(substring(.,5,30))' />"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLProve = wvarStrXSL
End Function

Private Function ParseoMensaje(strParseString As String, strTipo As String) As String
Dim wvarResult As String
Dim wvarPos As Integer
Dim wvarCantLin As Integer
Dim wvarCounter As Integer
    '
    wvarResult = wvarResult & "<RS>"
    '
    wvarPos = 1
    If strTipo = "E" Then
        If Trim(Mid(strParseString, wvarPos, 3)) <> "000" And Trim(Mid(strParseString, wvarPos, 3)) <> "" Then
            wvarCantLin = Mid(strParseString, wvarPos, 3)
            wvarPos = wvarPos + 3
            '
            For wvarCounter = 0 To wvarCantLin - 1
                    wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 33) & "]]></R>"
                    wvarPos = wvarPos + 33
            Next
        End If
    Else
        If Trim(Mid(strParseString, wvarPos, 3)) <> "000" And Trim(Mid(strParseString, wvarPos, 3)) <> "" Then
            wvarCantLin = Mid(strParseString, wvarPos, 3)
            wvarPos = wvarPos + 3
            '
            For wvarCounter = 0 To wvarCantLin - 1
                wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 39) & "]]></R>"
                wvarPos = wvarPos + 39
            Next
        End If
    End If
    '
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function


