VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVValidaCuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVValidaCuentas"
Const mcteOpID              As String = "1008"

'Parametros XML de Entrada
Const mcteParam_Usuario    As String = "//USUARIO"
Const mcteParam_BancoCod   As String = "//BCOCOD"
Const mcteParam_SucurCod   As String = "//SUCURCOD"
Const mcteParam_CobroTip   As String = "//COBROTIP"
Const mcteParam_CuentNum   As String = "//CTANUM"
Const mcteParam_VenciAnn   As String = "//VENANN"
Const mcteParam_VenciMes   As String = "//VENMES"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarBancoCod        As String
    Dim wvarSucurCod        As String
    Dim wvarCobroTip        As String
    Dim wvarCuentNum        As String
    Dim wvarVenciAnn        As String
    Dim wvarVenciMes        As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deber� venir desde la p�gina
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        If .selectNodes(mcteParam_BancoCod).length = 0 Then
            wvarBancoCod = "0000"
        Else
            wvarBancoCod = Right(String(4, "0") & .selectSingleNode(mcteParam_BancoCod).Text, 4)
        End If
        If .selectNodes(mcteParam_SucurCod).length = 0 Then
            wvarSucurCod = "0000"
        Else
            wvarSucurCod = Right(String(4, "0") & .selectSingleNode(mcteParam_SucurCod).Text, 4)
        End If
        wvarCobroTip = Left(.selectSingleNode(mcteParam_CobroTip).Text & Space(2), 2)
        wvarCuentNum = Left(.selectSingleNode(mcteParam_CuentNum).Text & Space(20), 20)
        wvarVenciAnn = Right(String(4, "0") & .selectSingleNode(mcteParam_VenciAnn).Text, 4)
        wvarVenciMes = Right(String(2, "0") & .selectSingleNode(mcteParam_VenciMes).Text, 2)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los par�metros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarBancoCod & wvarSucurCod & wvarCobroTip & wvarCuentNum & wvarVenciAnn & wvarVenciMes
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 190
    If Mid(strParseString, 19, 2) = "OK" Then
       '
       wvarStep = 230
       Response = "<Response><Estado resultado='true' mensaje='' /></Response>"
       '
    Else
        wvarError = Mid(strParseString, 21, 2)
        Select Case wvarError
            Case "01"
                Response = "<Response><Estado resultado='false' mensaje='Los datos ingresados del banco no son correctos.' /></Response>"
            Case "02"
                Response = "<Response><Estado resultado='false' mensaje='Los datos ingresados de la sucusal del banco no son correctos.' /></Response>"
            Case "03"
                Response = "<Response><Estado resultado='false' mensaje='Los datos ingresados de la tarjeta de cr�dito no son correctos.' /></Response>"
            Case "05"
                Response = "<Response><Estado resultado='false' mensaje='La fecha de vencimiento de la tarjeta de cr�dito no es v�lida.' /></Response>"
            Case Else
                Response = "<Response><Estado resultado='false' mensaje='Alguno de los datos no son los correctos.' /></Response>"
        End Select
    End If
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & wvarBancoCod & wvarSucurCod & wvarCobroTip & wvarCuentNum & wvarVenciAnn & wvarVenciMes & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
