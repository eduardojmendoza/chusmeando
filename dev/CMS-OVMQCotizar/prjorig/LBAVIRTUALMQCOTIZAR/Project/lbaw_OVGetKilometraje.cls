VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetKilometraje"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVGetKilometraje"
Const mcteOpID              As String = "0058"

'Parametros XML de Entrada
Const mcteParam_Ramopcod    As String = "//RAMOPCOD"
Const mcteParam_Campacod    As String = "//CAMPACOD"
Const mcteParam_Agentcod    As String = "//AGENTCOD"
Const mcteParam_AgentCla    As String = "//AGENTCLA"
Const mcteParam_Codizona    As String = "//CODIZONA"
Const mcteParam_Fecinici    As String = "//FECINICI"
Const mcteParam_Fecinvig    As String = "//FECINVIG"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarQueueManager    As String
    Dim wvarPutQueue        As String
    Dim wvarGetQueue        As String
    Dim wvarGMOWaitInterval As Long
    '
    Dim wobjQueueManager    As MQAX200.MQQueueManager
    Dim wobjPutQueue        As MQAX200.MQQueue
    Dim wobjGetQueue        As MQAX200.MQQueue
    Dim wobjMessage         As MQAX200.MQMessage
    Dim wobjPMO             As MQAX200.MQPutMessageOptions
    Dim wobjGMO             As MQAX200.MQGetMessageOptions
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String




    Dim wvarRamopcod        As String
    Dim wvarCampacod        As String
    Dim wvarAgentcod        As String
    Dim wvarAgentCla        As String
    Dim wvarCodiZona        As String
    Dim wvarFecInici        As String
    Dim wvarFecInVig        As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarCampacod = Left(.selectSingleNode(mcteParam_Campacod).Text & Space(4), 4)
        wvarAgentcod = Right(String(4, "0") & .selectSingleNode(mcteParam_Agentcod).Text, 4)
        wvarAgentCla = Left(.selectSingleNode(mcteParam_AgentCla).Text & Space(2), 2)
        wvarCodiZona = Right(String(4, "0") & .selectSingleNode(mcteParam_Codizona).Text, 4)
        wvarFecInici = Right(String(8, "0") & .selectSingleNode(mcteParam_Fecinici).Text, 8)
        wvarFecInVig = Right(String(8, "0") & .selectSingleNode(mcteParam_Fecinvig).Text, 8)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    With wobjXMLConfig
        .async = False
        Call .Load(App.Path & "\" & gcteConfFileName)
        wvarStep = 40
        wvarQueueManager = .selectSingleNode(gcteQueueManager).Text
        wvarPutQueue = .selectSingleNode(gctePutQueue).Text
        wvarGetQueue = .selectSingleNode(gcteGetQueue).Text
        wvarGMOWaitInterval = CLng(.selectSingleNode(gcteGMOWaitInterval).Text)
    End With
    '
    wvarStep = 50
    Set wobjXMLConfig = Nothing
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarRamopcod = .selectSingleNode(gcteNodosAutoScoring & gcteRAMOPCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    'Genera y conecta el manager MQ
    Set wobjQueueManager = New MQAX200.MQQueueManager
    wobjQueueManager.Name = wvarQueueManager
    '
    wobjQueueManager.Connect
    '
    wvarStep = 90
    If Not wobjQueueManager.IsConnected Then
        Err.Raise -1, mcteClassName & "." & wcteFnName, "No se pudo realizar la conexion a " & wvarQueueManager
    Else
        Set wobjPutQueue = New MQAX200.MQQueue
        Set wobjGetQueue = New MQAX200.MQQueue
        '
        wvarStep = 100
        'Si se pudo crear el manager, crea las colas de put y get
        On Error Resume Next
        With wobjPutQueue
            Set .ConnectionReference = wobjQueueManager
            .Name = wvarPutQueue
            .OpenOptions = MQOO_OUTPUT + MQOO_FAIL_IF_QUIESCING
            .Open
        End With
        '
        wvarStep = 110
        With wobjGetQueue
            Set .ConnectionReference = wobjQueueManager
            .Name = wvarGetQueue
            .OpenOptions = MQOO_INPUT_SHARED + MQOO_FAIL_IF_QUIESCING
            .Open
        End With
        '
        wvarStep = 120
        On Error GoTo ErrorHandler
        If wobjPutQueue.ReasonCode <> MQCC_OK Then
            Err.Raise -1, mcteClassName & "." & wcteFnName, "Error al abrir la cola " & wvarPutQueue & ". Motivo: " & wobjPutQueue.ReasonName
        End If
        '
        If wobjGetQueue.ReasonCode <> MQCC_OK Then
            Err.Raise -1, mcteClassName & "." & wcteFnName, "Error al abrir la cola " & wvarGetQueue & ". Motivo: " & wobjGetQueue.ReasonName
        End If
        '
        wvarStep = 130
        Set wobjMessage = New MQAX200.MQMessage
        '
        'Genero el mensaje con sus propiedades y los parámetros de entrada
        With wobjMessage
            .Format = "MQSTR"
            .MessageData = mcteOpID & wvarRamopcod & wvarCampacod & wvarAgentcod & wvarAgentCla & wvarCodiZona & wvarFecInici & wvarFecInVig
            .Expiry = 2000
            .Report = MQRO_EXPIRATION_WITH_DATA + MQRO_COPY_MSG_ID_TO_CORREL_ID
            .ReplyToQueueManagerName = wvarQueueManager
            .ReplyToQueueName = wvarGetQueue
        End With
        '
        Set wobjPMO = New MQAX200.MQPutMessageOptions
        wobjPMO.Options = MQPMO_NEW_MSG_ID
        '
        wvarStep = 140
        'Coloca el mensaje en la cola
        wobjPutQueue.Put wobjMessage, wobjPMO
        '
        Set wobjGMO = New MQAX200.MQGetMessageOptions
        wobjGMO.MatchOptions = MQMO_MATCH_MSG_ID
        wobjGMO.Options = MQGMO_WAIT
        wobjGMO.WaitInterval = wvarGMOWaitInterval * 2
        '
        wvarStep = 150
        On Error Resume Next
        wobjGetQueue.Get wobjMessage, wobjGMO
        '
        If wobjGetQueue.CompletionCode = MQCC_FAILED Then
            wobjGMO.WaitInterval = wvarGMOWaitInterval * 4
            wobjGetQueue.Get wobjMessage, wobjGMO
        End If
        '
        wvarStep = 160
        If wobjGetQueue.CompletionCode = MQCC_FAILED Then
            ' NO HAY SERVICIO DE MQ
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & wvarResult & "</Response>"
            wvarStep = 170
            mobjCOM_Context.SetComplete
            IAction_Execute = 0
            
            mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [-1] - EL SERVICIO DE CONSULTA NO SE ENCUENTRA DISPONIBLE - Timeout Expired", _
                     vbLogEventTypeError
                     
            GoTo ClearObjects
        End If
        '
        On Error GoTo ErrorHandler
        '
        wvarStep = 180
        '
        strParseString = wobjMessage.MessageData
        
        If Trim(strParseString) <> "" Then
            wvarStep = 190
            '
            wvarResult = ParseoMensaje(strParseString, 35)
            '
            wvarStep = 200
            Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
        
        Else
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
        End If
        '
        wvarStep = 220
    End If
    '
    wvarStep = 230
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS MQ
    Set wobjPMO = Nothing
    Set wobjGMO = Nothing
    Set wobjMessage = Nothing
        
    If Not wobjGetQueue Is Nothing Then
        If wobjGetQueue.IsOpen Then wobjGetQueue.Close
    End If
    
    Set wobjGetQueue = Nothing
    
    If Not wobjPutQueue Is Nothing Then
        If wobjPutQueue.IsOpen Then wobjPutQueue.Close
    End If
    
    Set wobjPutQueue = Nothing
    
    If Not wobjQueueManager Is Nothing Then
        If wobjQueueManager.IsConnected Then wobjQueueManager.Disconnect
    End If
    
    Set wobjQueueManager = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarRamopcod & wvarCampacod & wvarAgentcod & wvarAgentCla & wvarCodiZona & wvarFecInici & wvarFecInVig & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
    
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(ByVal strParseString As String, ByVal plPos As Long) As String

Dim wvarResult As String
Dim i As Integer
Dim wvarDesde As String
Dim wvarHasta As String
    
    '
    If Trim(strParseString) <> "" Then
        For i = 1 To 50
            wvarDesde = Right(Space(7) & CStr(CLng("0" & Mid(strParseString, plPos, 7))), 7)
            If Trim(wvarDesde) <> "0" Then
                wvarHasta = Right(Space(7) & CStr(CLng("0" & Mid(strParseString, plPos + 7, 7))), 7)
                wvarResult = wvarResult & "<OPTION value='" & Trim(wvarDesde) & "' >" & wvarDesde & " - " & wvarHasta & "</OPTION>"
            End If
            plPos = plPos + 14
        Next
        
    End If

    '
    ParseoMensaje = wvarResult
    '
 
End Function

