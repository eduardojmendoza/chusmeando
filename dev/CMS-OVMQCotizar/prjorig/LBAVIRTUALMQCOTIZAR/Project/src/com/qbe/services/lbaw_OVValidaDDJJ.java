package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVValidaDDJJ implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVValidaDDJJ";
  static final String mcteOpID = "1515";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_Estado = "//ESTADO";
  static final String mcteParam_Uso = "//USO";
  static final String mcteParam_Abollado = "//ABOLLADO";
  static final String mcteParam_Picado = "//PICADO";
  static final String mcteParam_Masillado = "//MASILLADO";
  static final String mcteParam_Roto = "//ROTO";
  static final String mcteParam_Faltantes = "//FALTANTES";
  static final String mcteParam_Luces = "//LUCES";
  static final String mcteParam_Odometro = "//ODOMETRO";
  static final String mcteParam_AB_Paragolpe = "//AB_PARAGOLPE";
  static final String mcteParam_AB_Par_Del = "//AB_PAR_DEL";
  static final String mcteParam_AB_Par_Tra = "//AB_PAR_TRA";
  static final String mcteParam_AB_Capot = "//AB_CAPOT";
  static final String mcteParam_AB_Techo = "//AB_TECHO";
  static final String mcteParam_AB_TBaul = "//AB_TBAUL";
  static final String mcteParam_AB_Guardabarro = "//AB_GUARDABARRO";
  static final String mcteParam_AB_Guar_Del = "//AB_GUAR_DEL";
  static final String mcteParam_AB_Guar_Del_Der = "//AB_GUAR_DEL_DER";
  static final String mcteParam_AB_Guar_Del_Izq = "//AB_GUAR_DEL_IZQ";
  static final String mcteParam_AB_Guar_Tra = "//AB_GUAR_TRA";
  static final String mcteParam_AB_Guar_Tra_Der = "//AB_GUAR_TRA_DER";
  static final String mcteParam_AB_Guar_Tra_Izq = "//AB_GUAR_TRA_IZQ";
  static final String mcteParam_AB_Puerta = "//AB_PUERTA";
  static final String mcteParam_AB_Puer_Del = "//AB_PUER_DEL";
  static final String mcteParam_AB_Puer_Del_Der = "//AB_PUER_DEL_DER";
  static final String mcteParam_AB_Puer_Del_Izq = "//AB_PUER_DEL_IZQ";
  static final String mcteParam_AB_Puer_Tra = "//AB_PUER_TRA";
  static final String mcteParam_AB_Puer_Tra_Der = "//AB_PUER_TRA_DER";
  static final String mcteParam_AB_Puer_Tra_Izq = "//AB_PUER_TRA_IZQ";
  static final String mcteParam_PIC_Capot = "//PIC_CAPOT";
  static final String mcteParam_PIC_Techo = "//PIC_TECHO";
  static final String mcteParam_PIC_TBaul = "//PIC_TBAUL";
  static final String mcteParam_PIC_Guardabarro = "//PIC_GUARDABARRO";
  static final String mcteParam_PIC_Guar_Del = "//PIC_GUAR_DEL";
  static final String mcteParam_PIC_Guar_Del_Der = "//PIC_GUAR_DEL_DER";
  static final String mcteParam_PIC_Guar_Del_Izq = "//PIC_GUAR_DEL_IZQ";
  static final String mcteParam_PIC_Guar_Tra = "//PIC_GUAR_TRA";
  static final String mcteParam_PIC_Guar_Tra_Der = "//PIC_GUAR_TRA_DER";
  static final String mcteParam_PIC_Guar_Tra_Izq = "//PIC_GUAR_TRA_IZQ";
  static final String mcteParam_PIC_Puerta = "//PIC_PUERTA";
  static final String mcteParam_PIC_Puer_Del = "//PIC_PUER_DEL";
  static final String mcteParam_PIC_Puer_Del_Der = "//PIC_PUER_DEL_DER";
  static final String mcteParam_PIC_Puer_Del_Izq = "//PIC_PUER_DEL_IZQ";
  static final String mcteParam_PIC_Puer_Tra = "//PIC_PUER_TRA";
  static final String mcteParam_PIC_Puer_Tra_Der = "//PIC_PUER_TRA_DER";
  static final String mcteParam_PIC_Puer_Tra_Izq = "//PIC_PUER_TRA_IZQ";
  static final String mcteParam_PIC_Zocalo = "//PIC_ZOCALO";
  static final String mcteParam_PIC_Zoc_Der = "//PIC_ZOC_DER";
  static final String mcteParam_PIC_Zoc_Izq = "//PIC_ZOC_IZQ";
  static final String mcteParam_MAS_Capot = "//MAS_CAPOT";
  static final String mcteParam_MAS_Techo = "//MAS_TECHO";
  static final String mcteParam_MAS_TBaul = "//MAS_TBAUL";
  static final String mcteParam_MAS_Guardabarro = "//MAS_GUARDABARRO";
  static final String mcteParam_MAS_Guar_Del = "//MAS_GUAR_DEL";
  static final String mcteParam_MAS_Guar_Del_Der = "//MAS_GUAR_DEL_DER";
  static final String mcteParam_MAS_Guar_Del_Izq = "//MAS_GUAR_DEL_IZQ";
  static final String mcteParam_MAS_Guar_Tra = "//MAS_GUAR_TRA";
  static final String mcteParam_MAS_Guar_Tra_Der = "//MAS_GUAR_TRA_DER";
  static final String mcteParam_MAS_Guar_Tra_Izq = "//MAS_GUAR_TRA_IZQ";
  static final String mcteParam_MAS_Puerta = "//MAS_PUERTA";
  static final String mcteParam_MAS_Puer_Del = "//MAS_PUER_DEL";
  static final String mcteParam_MAS_Puer_Del_Der = "//MAS_PUER_DEL_DER";
  static final String mcteParam_MAS_Puer_Del_Izq = "//MAS_PUER_DEL_IZQ";
  static final String mcteParam_MAS_Puer_Tra = "//MAS_PUER_TRA";
  static final String mcteParam_MAS_Puer_Tra_Der = "//MAS_PUER_TRA_DER";
  static final String mcteParam_MAS_Puer_Tra_Izq = "//MAS_PUER_TRA_IZQ";
  static final String mcteParam_MAS_Zocalo = "//MAS_ZOCALO";
  static final String mcteParam_MAS_Zoc_Der = "//MAS_ZOC_DER";
  static final String mcteParam_MAS_Zoc_Izq = "//MAS_ZOC_IZQ";
  static final String mcteParam_ROT_Paragolpe = "//ROT_PARAGOLPE";
  static final String mcteParam_ROT_Par_Del = "//ROT_PAR_DEL";
  static final String mcteParam_ROT_Par_Tra = "//ROT_PAR_TRA";
  static final String mcteParam_ROT_Rejilla = "//ROT_REJILLA";
  static final String mcteParam_ROT_Optica = "//ROT_OPTICA";
  static final String mcteParam_ROT_Opt_Der = "//ROT_OPT_DER";
  static final String mcteParam_ROT_Opt_Izq = "//ROT_OPT_IZQ";
  static final String mcteParam_ROT_FaroGiro = "//ROT_FAROGIRO";
  static final String mcteParam_ROT_FG_Del = "//ROT_FG_DEL";
  static final String mcteParam_ROT_FG_Del_Der = "//ROT_FG_DEL_DER";
  static final String mcteParam_ROT_FG_Del_Izq = "//ROT_FG_DEL_IZQ";
  static final String mcteParam_ROT_FG_Tra = "//ROT_FG_TRA";
  static final String mcteParam_ROT_FG_Tra_Der = "//ROT_FG_TRA_DER";
  static final String mcteParam_ROT_FG_Tra_Izq = "//ROT_FG_TRA_IZQ";
  static final String mcteParam_ROT_Luneta = "//ROT_LUNETA";
  static final String mcteParam_ROT_Parabrisas = "//ROT_PARABRISAS";
  static final String mcteParam_FAL_Paragolpe = "//FAL_PARAGOLPE";
  static final String mcteParam_FAL_Par_Del = "//FAL_PAR_DEL";
  static final String mcteParam_FAL_Par_Tra = "//FAL_PAR_TRA";
  static final String mcteParam_FAL_Rejilla = "//FAL_REJILLA";
  static final String mcteParam_FAL_Optica = "//FAL_OPTICA";
  static final String mcteParam_FAL_Opt_Der = "//FAL_OPT_DER";
  static final String mcteParam_FAL_Opt_Izq = "//FAL_OPT_IZQ";
  static final String mcteParam_FAL_FaroGiro = "//FAL_FAROGIRO";
  static final String mcteParam_FAL_FG_Del = "//FAL_FG_DEL";
  static final String mcteParam_FAL_FG_Del_Der = "//FAL_FG_DEL_DER";
  static final String mcteParam_FAL_FG_Del_Izq = "//FAL_FG_DEL_IZQ";
  static final String mcteParam_FAL_FG_Tra = "//FAL_FG_TRA";
  static final String mcteParam_FAL_FG_Tra_Der = "//FAL_FG_TRA_DER";
  static final String mcteParam_FAL_FG_Tra_Izq = "//FAL_FG_TRA_IZQ";
  static final String mcteParam_LUC_Alta = "//LUC_ALTA";
  static final String mcteParam_LUC_Baja = "//LUC_BAJA";
  static final String mcteParam_LUC_Giro = "//LUC_GIRO";
  static final String mcteParam_LUC_Gir_Del = "//LUC_GIR_DEL";
  static final String mcteParam_LUC_Gir_Del_Der = "//LUC_GIR_DEL_DER";
  static final String mcteParam_LUC_Gir_Del_Izq = "//LUC_GIR_DEL_IZQ";
  static final String mcteParam_LUC_Gir_Tra = "//LUC_GIR_TRA";
  static final String mcteParam_LUC_Gir_Tra_Der = "//LUC_GIR_TRA_DER";
  static final String mcteParam_LUC_Gir_Tra_Izq = "//LUC_GIR_TRA_IZQ";
  static final String mcteParam_LUC_Posicion = "//LUC_POSICION";
  static final String mcteParam_LUC_Pos_Del = "//LUC_POS_DEL";
  static final String mcteParam_LUC_Pos_Del_Der = "//LUC_POS_DEL_DER";
  static final String mcteParam_LUC_Pos_Del_Izq = "//LUC_POS_DEL_IZQ";
  static final String mcteParam_LUC_Pos_Tra = "//LUC_POS_TRA";
  static final String mcteParam_LUC_Pos_Tra_Der = "//LUC_POS_TRA_DER";
  static final String mcteParam_LUC_Pos_Tra_Izq = "//LUC_POS_TRA_IZQ";
  static final String mcteParam_LUC_Stop = "//LUC_STOP";
  static final String mcteParam_NEU_Est_Del_Der = "//NEU_EST_DEL_DER";
  static final String mcteParam_NEU_Est_Del_Izq = "//NEU_EST_DEL_IZQ";
  static final String mcteParam_NEU_Est_Tra_Der = "//NEU_EST_TRA_DER";
  static final String mcteParam_NEU_Est_Tra_Izq = "//NEU_EST_TRA_IZQ";
  static final String mcteParam_FuncKil = "//FUNCKIL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarEstado = "";
    String wvarUso = "";
    String wvarAbollado = "";
    String wvarPicado = "";
    String wvarMasillado = "";
    String wvarRoto = "";
    String wvarFaltantes = "";
    String wvarLuces = "";
    String wvarOdometro = "";
    String wvarAB_Paragolpe = "";
    String wvarAB_Par_Del = "";
    String wvarAB_Par_Tra = "";
    String wvarAB_Capot = "";
    String wvarAB_Techo = "";
    String wvarAB_TBaul = "";
    String wvarAB_Guardabarro = "";
    String wvarAB_Guar_Del = "";
    String wvarAB_Guar_Del_Der = "";
    String wvarAB_Guar_Del_Izq = "";
    String wvarAB_Guar_Tra = "";
    String wvarAB_Guar_Tra_Der = "";
    String wvarAB_Guar_Tra_Izq = "";
    String wvarAB_Puerta = "";
    String wvarAB_Puer_Del = "";
    String wvarAB_Puer_Del_Der = "";
    String wvarAB_Puer_Del_Izq = "";
    String wvarAB_Puer_Tra = "";
    String wvarAB_Puer_Tra_Der = "";
    String wvarAB_Puer_Tra_Izq = "";
    String wvarPIC_Capot = "";
    String wvarPIC_Techo = "";
    String wvarPIC_TBaul = "";
    String wvarPIC_Guardabarro = "";
    String wvarPIC_Guar_Del = "";
    String wvarPIC_Guar_Del_Der = "";
    String wvarPIC_Guar_Del_Izq = "";
    String wvarPIC_Guar_Tra = "";
    String wvarPIC_Guar_Tra_Der = "";
    String wvarPIC_Guar_Tra_Izq = "";
    String wvarPIC_Puerta = "";
    String wvarPIC_Puer_Del = "";
    String wvarPIC_Puer_Del_Der = "";
    String wvarPIC_Puer_Del_Izq = "";
    String wvarPIC_Puer_Tra = "";
    String wvarPIC_Puer_Tra_Der = "";
    String wvarPIC_Puer_Tra_Izq = "";
    String wvarPIC_Zocalo = "";
    String wvarPIC_Zoc_Der = "";
    String wvarPIC_Zoc_Izq = "";
    String wvarMAS_Capot = "";
    String wvarMAS_Techo = "";
    String wvarMAS_TBaul = "";
    String wvarMAS_Guardabarro = "";
    String wvarMAS_Guar_Del = "";
    String wvarMAS_Guar_Del_Der = "";
    String wvarMAS_Guar_Del_Izq = "";
    String wvarMAS_Guar_Tra = "";
    String wvarMAS_Guar_Tra_Der = "";
    String wvarMAS_Guar_Tra_Izq = "";
    String wvarMAS_Puerta = "";
    String wvarMAS_Puer_Del = "";
    String wvarMAS_Puer_Del_Der = "";
    String wvarMAS_Puer_Del_Izq = "";
    String wvarMAS_Puer_Tra = "";
    String wvarMAS_Puer_Tra_Der = "";
    String wvarMAS_Puer_Tra_Izq = "";
    String wvarMAS_Zocalo = "";
    String wvarMAS_Zoc_Der = "";
    String wvarMAS_Zoc_Izq = "";
    String wvarROT_Paragolpe = "";
    String wvarROT_Par_Del = "";
    String wvarROT_Par_Tra = "";
    String wvarROT_Rejilla = "";
    String wvarROT_Optica = "";
    String wvarROT_Opt_Der = "";
    String wvarROT_Opt_Izq = "";
    String wvarROT_FaroGiro = "";
    String wvarROT_FG_Del = "";
    String wvarROT_FG_Del_Der = "";
    String wvarROT_FG_Del_Izq = "";
    String wvarROT_FG_Tra = "";
    String wvarROT_FG_Tra_Der = "";
    String wvarROT_FG_Tra_Izq = "";
    String wvarROT_Luneta = "";
    String wvarROT_Parabrisas = "";
    String wvarFAL_Paragolpe = "";
    String wvarFAL_Par_Del = "";
    String wvarFAL_Par_Tra = "";
    String wvarFAL_Rejilla = "";
    String wvarFAL_Optica = "";
    String wvarFAL_Opt_Der = "";
    String wvarFAL_Opt_Izq = "";
    String wvarFAL_FaroGiro = "";
    String wvarFAL_FG_Del = "";
    String wvarFAL_FG_Del_Der = "";
    String wvarFAL_FG_Del_Izq = "";
    String wvarFAL_FG_Tra = "";
    String wvarFAL_FG_Tra_Der = "";
    String wvarFAL_FG_Tra_Izq = "";
    String wvarLUC_Alta = "";
    String wvarLUC_Baja = "";
    String wvarLUC_Giro = "";
    String wvarLUC_Gir_Del = "";
    String wvarLUC_Gir_Del_Der = "";
    String wvarLUC_Gir_Del_Izq = "";
    String wvarLUC_Gir_Tra = "";
    String wvarLUC_Gir_Tra_Der = "";
    String wvarLUC_Gir_Tra_Izq = "";
    String wvarLUC_Posicion = "";
    String wvarLUC_Pos_Del = "";
    String wvarLUC_Pos_Del_Der = "";
    String wvarLUC_Pos_Del_Izq = "";
    String wvarLUC_Pos_Tra = "";
    String wvarLUC_Pos_Tra_Der = "";
    String wvarLUC_Pos_Tra_Izq = "";
    String wvarLUC_Stop = "";
    String wvarNEU_Est_Del_Der = "";
    String wvarNEU_Est_Del_Izq = "";
    String wvarNEU_Est_Tra_Der = "";
    String wvarNEU_Est_Tra_Izq = "";
    String wvarFuncKil = "";
    String strParseString = "";
    String wvarError = "";
    int wvarPos = 0;
    String wvarString = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarEstado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Estado ) */ );
      wvarUso = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Uso ) */ );
      wvarStep = 20;
      wvarAbollado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Abollado ) */ );
      wvarPicado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Picado ) */ );
      wvarMasillado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Masillado ) */ );
      wvarRoto = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Roto ) */ );
      wvarFaltantes = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Faltantes ) */ );
      wvarLuces = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Luces ) */ );
      wvarStep = 30;
      wvarOdometro = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Odometro ) */ ) + Strings.space( 10 ), 10 );
      wvarAB_Paragolpe = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Paragolpe ) */ );
      wvarAB_Par_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Par_Del ) */ );
      wvarAB_Par_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Par_Tra ) */ );
      wvarAB_Capot = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Capot ) */ );
      wvarAB_Techo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Techo ) */ );
      wvarStep = 40;
      wvarAB_TBaul = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_TBaul ) */ );
      wvarAB_Guardabarro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guardabarro ) */ );
      wvarAB_Guar_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guar_Del ) */ );
      wvarAB_Guar_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guar_Del_Der ) */ );
      wvarAB_Guar_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guar_Del_Izq ) */ );
      wvarAB_Guar_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guar_Tra ) */ );
      wvarStep = 50;
      wvarAB_Guar_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guar_Tra_Der ) */ );
      wvarAB_Guar_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Guar_Tra_Izq ) */ );
      wvarAB_Puerta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puerta ) */ );
      wvarAB_Puer_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puer_Del ) */ );
      wvarAB_Puer_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puer_Del_Der ) */ );
      wvarAB_Puer_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puer_Del_Izq ) */ );
      wvarStep = 60;
      wvarAB_Puer_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puer_Tra ) */ );
      wvarAB_Puer_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puer_Tra_Der ) */ );
      wvarAB_Puer_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AB_Puer_Tra_Izq ) */ );
      wvarPIC_Capot = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Capot ) */ );
      wvarPIC_Techo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Techo ) */ );
      wvarPIC_TBaul = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_TBaul ) */ );
      wvarStep = 70;
      wvarPIC_Guardabarro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guardabarro ) */ );
      wvarPIC_Guar_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guar_Del ) */ );
      wvarPIC_Guar_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guar_Del_Der ) */ );
      wvarPIC_Guar_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guar_Del_Izq ) */ );
      wvarPIC_Guar_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guar_Tra ) */ );
      wvarPIC_Guar_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guar_Tra_Der ) */ );
      wvarStep = 80;
      wvarPIC_Guar_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Guar_Tra_Izq ) */ );
      wvarPIC_Puerta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puerta ) */ );
      wvarPIC_Puer_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puer_Del ) */ );
      wvarPIC_Puer_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puer_Del_Der ) */ );
      wvarPIC_Puer_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puer_Del_Izq ) */ );
      wvarPIC_Puer_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puer_Tra ) */ );
      wvarStep = 90;
      wvarPIC_Puer_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puer_Tra_Der ) */ );
      wvarPIC_Puer_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Puer_Tra_Izq ) */ );
      wvarPIC_Zocalo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Zocalo ) */ );
      wvarPIC_Zoc_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Zoc_Der ) */ );
      wvarPIC_Zoc_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PIC_Zoc_Izq ) */ );
      wvarMAS_Capot = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Capot ) */ );
      wvarStep = 100;
      wvarMAS_Techo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Techo ) */ );
      wvarMAS_TBaul = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_TBaul ) */ );
      wvarMAS_Guardabarro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guardabarro ) */ );
      wvarMAS_Guar_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guar_Del ) */ );
      wvarMAS_Guar_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guar_Del_Der ) */ );
      wvarMAS_Guar_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guar_Del_Izq ) */ );
      wvarStep = 110;
      wvarMAS_Guar_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guar_Tra ) */ );
      wvarMAS_Guar_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guar_Tra_Der ) */ );
      wvarMAS_Guar_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Guar_Tra_Izq ) */ );
      wvarMAS_Puerta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puerta ) */ );
      wvarMAS_Puer_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puer_Del ) */ );
      wvarMAS_Puer_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puer_Del_Der ) */ );
      wvarStep = 120;
      wvarMAS_Puer_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puer_Del_Izq ) */ );
      wvarMAS_Puer_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puer_Tra ) */ );
      wvarMAS_Puer_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puer_Tra_Der ) */ );
      wvarMAS_Puer_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Puer_Tra_Izq ) */ );
      wvarMAS_Zocalo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Zocalo ) */ );
      wvarMAS_Zoc_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Zoc_Der ) */ );
      wvarStep = 130;
      wvarMAS_Zoc_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_MAS_Zoc_Izq ) */ );
      wvarROT_Paragolpe = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Paragolpe ) */ );
      wvarROT_Par_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Par_Del ) */ );
      wvarROT_Par_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Par_Tra ) */ );
      wvarROT_Rejilla = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Rejilla ) */ );
      wvarROT_Optica = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Optica ) */ );
      wvarStep = 140;
      wvarROT_Opt_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Opt_Der ) */ );
      wvarROT_Opt_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Opt_Izq ) */ );
      wvarROT_FaroGiro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FaroGiro ) */ );
      wvarROT_FG_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FG_Del ) */ );
      wvarROT_FG_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FG_Del_Der ) */ );
      wvarROT_FG_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FG_Del_Izq ) */ );
      wvarStep = 150;
      wvarROT_FG_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FG_Tra ) */ );
      wvarROT_FG_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FG_Tra_Der ) */ );
      wvarROT_FG_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_FG_Tra_Izq ) */ );
      wvarROT_Luneta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Luneta ) */ );
      wvarROT_Parabrisas = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ROT_Parabrisas ) */ );
      wvarFAL_Paragolpe = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Paragolpe ) */ );
      wvarStep = 160;
      wvarFAL_Par_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Par_Del ) */ );
      wvarFAL_Par_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Par_Tra ) */ );
      wvarFAL_Rejilla = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Rejilla ) */ );
      wvarFAL_Optica = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Optica ) */ );
      wvarFAL_Opt_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Opt_Der ) */ );
      wvarFAL_Opt_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_Opt_Izq ) */ );
      wvarStep = 170;
      wvarFAL_FaroGiro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FaroGiro ) */ );
      wvarFAL_FG_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FG_Del ) */ );
      wvarFAL_FG_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FG_Del_Der ) */ );
      wvarFAL_FG_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FG_Del_Izq ) */ );
      wvarFAL_FG_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FG_Tra ) */ );
      wvarFAL_FG_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FG_Tra_Der ) */ );
      wvarStep = 180;
      wvarFAL_FG_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FAL_FG_Tra_Izq ) */ );
      wvarLUC_Alta = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Alta ) */ );
      wvarLUC_Baja = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Baja ) */ );
      wvarLUC_Giro = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Giro ) */ );
      wvarLUC_Gir_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Gir_Del ) */ );
      wvarLUC_Gir_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Gir_Del_Der ) */ );
      wvarStep = 190;
      wvarLUC_Gir_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Gir_Del_Izq ) */ );
      wvarLUC_Gir_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Gir_Tra ) */ );
      wvarLUC_Gir_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Gir_Tra_Der ) */ );
      wvarLUC_Gir_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Gir_Tra_Izq ) */ );
      wvarLUC_Posicion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Posicion ) */ );
      wvarLUC_Pos_Del = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Pos_Del ) */ );
      wvarStep = 200;
      wvarLUC_Pos_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Pos_Del_Der ) */ );
      wvarLUC_Pos_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Pos_Del_Izq ) */ );
      wvarLUC_Pos_Tra = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Pos_Tra ) */ );
      wvarLUC_Pos_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Pos_Tra_Der ) */ );
      wvarLUC_Pos_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Pos_Tra_Izq ) */ );
      wvarLUC_Stop = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LUC_Stop ) */ );
      wvarStep = 210;
      wvarNEU_Est_Del_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NEU_Est_Del_Der ) */ );
      wvarNEU_Est_Del_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NEU_Est_Del_Izq ) */ );
      wvarNEU_Est_Tra_Der = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NEU_Est_Tra_Der ) */ );
      wvarNEU_Est_Tra_Izq = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NEU_Est_Tra_Izq ) */ );
      wvarFuncKil = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FuncKil ) */ );
      //
      //
      wvarStep = 230;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 240;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 270;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 280;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 290;
      wvarString = wvarEstado + wvarUso + wvarAbollado + wvarPicado + wvarMasillado + wvarRoto + wvarFaltantes + wvarLuces + wvarOdometro + wvarAB_Paragolpe;
      wvarString = wvarString + wvarAB_Par_Del + wvarAB_Par_Tra + wvarAB_Capot + wvarAB_Techo + wvarAB_TBaul + wvarAB_Guardabarro + wvarAB_Guar_Del + wvarAB_Guar_Del_Der + wvarAB_Guar_Del_Izq;
      wvarString = wvarString + wvarAB_Guar_Tra + wvarAB_Guar_Tra_Der + wvarAB_Guar_Tra_Izq + wvarAB_Puerta + wvarAB_Puer_Del + wvarAB_Puer_Del_Der + wvarAB_Puer_Del_Izq + wvarAB_Puer_Tra + wvarAB_Puer_Tra_Der;
      wvarString = wvarString + wvarAB_Puer_Tra_Izq + wvarPIC_Capot + wvarPIC_Techo + wvarPIC_TBaul + wvarPIC_Guardabarro + wvarPIC_Guar_Del + wvarPIC_Guar_Del_Der + wvarPIC_Guar_Del_Izq + wvarPIC_Guar_Tra;
      wvarString = wvarString + wvarPIC_Guar_Tra_Der + wvarPIC_Guar_Tra_Izq + wvarPIC_Puerta + wvarPIC_Puer_Del + wvarPIC_Puer_Del_Der + wvarPIC_Puer_Del_Izq + wvarPIC_Puer_Tra + wvarPIC_Puer_Tra_Der + wvarPIC_Puer_Tra_Izq;
      wvarString = wvarString + wvarPIC_Zocalo + wvarPIC_Zoc_Der + wvarPIC_Zoc_Izq + wvarMAS_Capot + wvarMAS_Techo + wvarMAS_TBaul + wvarMAS_Guardabarro + wvarMAS_Guar_Del + wvarMAS_Guar_Del_Der + wvarMAS_Guar_Del_Izq;
      wvarString = wvarString + wvarMAS_Guar_Tra + wvarMAS_Guar_Tra_Der + wvarMAS_Guar_Tra_Izq + wvarMAS_Puerta + wvarMAS_Puer_Del + wvarMAS_Puer_Del_Der + wvarMAS_Puer_Del_Izq + wvarMAS_Puer_Tra + wvarMAS_Puer_Tra_Der;
      wvarString = wvarString + wvarMAS_Puer_Tra_Izq + wvarMAS_Zocalo + wvarMAS_Zoc_Der + wvarMAS_Zoc_Izq + wvarROT_Paragolpe + wvarROT_Par_Del + wvarROT_Par_Tra + wvarROT_Rejilla + wvarROT_Optica + wvarROT_Opt_Der + wvarROT_Opt_Izq;
      wvarString = wvarString + wvarROT_FaroGiro + wvarROT_FG_Del + wvarROT_FG_Del_Der + wvarROT_FG_Del_Izq + wvarROT_FG_Tra + wvarROT_FG_Tra_Der + wvarROT_FG_Tra_Izq + wvarROT_Luneta + wvarROT_Parabrisas + wvarFAL_Paragolpe + wvarFAL_Par_Del;
      wvarString = wvarString + wvarFAL_Par_Tra + wvarFAL_Rejilla + wvarFAL_Optica + wvarFAL_Opt_Der + wvarFAL_Opt_Izq + wvarFAL_FaroGiro + wvarFAL_FG_Del + wvarFAL_FG_Del_Der + wvarFAL_FG_Del_Izq + wvarFAL_FG_Tra + wvarFAL_FG_Tra_Der;
      wvarString = wvarString + wvarFAL_FG_Tra_Izq + wvarLUC_Alta + wvarLUC_Baja + wvarLUC_Giro + wvarLUC_Gir_Del + wvarLUC_Gir_Del_Der + wvarLUC_Gir_Del_Izq + wvarLUC_Gir_Tra + wvarLUC_Gir_Tra_Der + wvarLUC_Gir_Tra_Izq + wvarLUC_Posicion;
      wvarString = wvarString + wvarLUC_Pos_Del + wvarLUC_Pos_Del_Der + wvarLUC_Pos_Del_Izq + wvarLUC_Pos_Tra + wvarLUC_Pos_Tra_Der + wvarLUC_Pos_Tra_Izq + wvarLUC_Stop + wvarNEU_Est_Del_Der + wvarNEU_Est_Del_Izq + wvarNEU_Est_Tra_Der + wvarNEU_Est_Tra_Izq + wvarFuncKil;

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarString;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos = 197;
      //
      wvarStep = 420;
      if( Strings.mid( strParseString, 19, 2 ).equals( "ER" ) )
      {
        //
        wvarStep = 430;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 440;
        wvarResult = "";
        wvarResult = "<RTA>" + Strings.mid( strParseString, wvarPos, 1 ) + "</RTA>";
        wvarResult = wvarResult + "<MSGERR><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 1), 50 ) ) + "]]></MSGERR>";
        wvarResult = wvarResult + "<DDJJSTRING>" + wvarString + "</DDJJSTRING>";
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 450;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String p_GetXSLEstados() throws Exception
  {
    String p_GetXSLEstados = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,2))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='flag'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,3,30))' />";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLEstados = wvarStrXSL;
    return p_GetXSLEstados;
  }

  private String ParseoMensajeEstado( Variant wvarPos, String strParseString ) throws Exception
  {
    String ParseoMensajeEstado = "";
    String wvarResult = "";
    int wvarCant = 0;
    int i = 0;
    //
    wvarResult = wvarResult + "<RS>";
    //
    wvarPos.set( wvarPos.add( new Variant( 1 ) ) );
    if( (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "000" )) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "" )) )
    {
      wvarCant = Obj.toInt( Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ) );
      wvarPos.set( wvarPos.add( new Variant( 3 ) ) );
      for( i = 1; i <= wvarCant; i++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 33 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 33 ) ) );
      }
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensajeEstado = wvarResult;
    //
    return ParseoMensajeEstado;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
