package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVTipoInspeccion implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVTipoInspeccion";
  static final String mcteOpID = "1511";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_EsCero = "//ESCERO";
  static final String mcteParam_Rastreo = "//RASTREO";
  static final String mcteParam_EstDispo = "//ESTDISPO";
  static final String mcteParam_Agentcod = "//AGENTCOD";
  static final String mcteParam_AgentCla = "//AGENTCLA";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    Object wobjClass = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    String wvarResponse = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarEsCero = "";
    String wvarRastreo = "";
    String wvarEstDispo = "";
    String wvarAgentcod = "";
    String wvarAgentCla = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarRamopcod = "";
    String strParseString = "";
    String wvarError = "";
    Variant wvarPos = new Variant();
    String wvarEstado = "";
    String wvarAleatorio = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarEsCero = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EsCero ) */ );
      wvarRastreo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Rastreo ) */ );
      wvarEstDispo = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EstDispo ) */ ), 4 );
      wvarAgentcod = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Agentcod ) */ ), 4 );
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_AgentCla ) */.getLength() == 0 )
      {
        wvarAgentCla = "PR";
      }
      else
      {
        wvarAgentCla = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AgentCla ) */ ) + Strings.space( 2 ), 2 );
      }
      wvarProvi = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Provi ) */ ), 2 );
      wvarLocalidadCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod ) */ );
      //
      //
      wvarStep = 20;
      //Set wobjXMLRequest = Nothing
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wvarRamopcod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosAutoScoring + ModGeneral.gcteRAMOPCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamopcod + wvarEsCero + wvarRastreo + wvarEstDispo + wvarAgentcod + wvarAgentCla + wvarProvi + wvarLocalidadCod;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 180;
      //
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos.set( 89 );
      //
      wvarStep = 190;
      if( Strings.mid( strParseString, 19, 2 ).equals( "ER" ) )
      {
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 210;
        wvarResult = "";
        wvarEstado = Strings.mid( strParseString, wvarPos.toInt(), 1 );
        //
        if( wvarEstado.equals( "O" ) )
        {
          //Corro el SP de Aleatorio
          wvarStep = 220;
          wobjClass = new lbawA_OVSQLCotizar.lbaw_OVAleatorio();
          //error: function 'Execute' was not found.
          //unsup: Call wobjClass.Execute(wobjXMLRequest.xml, wvarResponse, "")
          wobjClass = null;
          //
          wvarStep = 230;
          wobjXMLParametros = new diamondedge.util.XmlDom();
          //unsup wobjXMLParametros.async = false;
          wobjXMLParametros.loadXML( wvarResponse );
          wvarAleatorio = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( "//ESTADO" ) */ );
          //
          wobjXMLParametros = (diamondedge.util.XmlDom) null;
          //
        }
        //
        wvarStep = 240;
        if( (wvarEstado.equals( "I" )) || (wvarEstado.equals( "O" )) )
        {
          wvarResult = wvarResult + invoke( "ParseoMensajeEstado", new Variant[] { new Variant(wvarPos), new Variant(strParseString) } );
          //
          wvarStep = 250;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 260;
          //unsup wobjXMLResponse.async = false;
          wobjXMLResponse.loadXML( wvarResult );
          //
          if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
          {
            wvarStep = 270;
            //unsup wobjXSLResponse.async = false;
            wobjXSLResponse.loadXML( invoke( "p_GetXSLEstados", new Variant[] {} ) );
            //
            wvarStep = 280;
            wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
            //
            wvarStep = 290;
            wobjXMLResponse = (diamondedge.util.XmlDom) null;
            wobjXSLResponse = (diamondedge.util.XmlDom) null;
            //
          }
          //
        }
        //
        wvarStep = 300;
        wvarEstado = "<RTA>" + wvarEstado + "</RTA>";
        wvarAleatorio = "<ALEATORIO>" + wvarAleatorio + "</ALEATORIO>";
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarEstado + wvarAleatorio + "<COMBO>" + wvarResult + "</COMBO>" + "</Response>" );
      }
      //
      wvarStep = 310;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String p_GetXSLEstados() throws Exception
  {
    String p_GetXSLEstados = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='number(substring(.,1,2))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='flag'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,3,30))' />";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLEstados = wvarStrXSL;
    return p_GetXSLEstados;
  }

  private String ParseoMensajeEstado( Variant wvarPos, String strParseString ) throws Exception
  {
    String ParseoMensajeEstado = "";
    String wvarResult = "";
    int wvarCant = 0;
    int i = 0;
    //
    wvarResult = wvarResult + "<RS>";
    //
    wvarPos.set( wvarPos.add( new Variant( 1 ) ) );
    if( (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "000" )) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ).equals( "" )) )
    {
      wvarCant = Obj.toInt( Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 3 ) ) );
      wvarPos.set( wvarPos.add( new Variant( 3 ) ) );
      for( i = 1; i <= wvarCant; i++ )
      {
        wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos.toInt(), 33 ) + "]]></R>";
        wvarPos.set( wvarPos.add( new Variant( 33 ) ) );
      }
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensajeEstado = wvarResult;
    //
    return ParseoMensajeEstado;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
