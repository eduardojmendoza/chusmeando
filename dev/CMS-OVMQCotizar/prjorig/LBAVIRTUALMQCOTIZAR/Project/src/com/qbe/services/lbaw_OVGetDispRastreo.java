package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetDispRastreo implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetDispRastreo";
  static final String mcteOpID = "1510";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_SumAseg = "//SUMASEG";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  static final String mcteParam_Opcion = "//OPCION";
  static final String mcteParam_Operacion = "//OPERACION";
  static final String mcteParam_PresCod = "//PRESCOD";
  static final String mcteParam_Instala = "//INSTALA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarResultParcial = "";
    String wvarCiaAsCod = "";
    String wvarRamopcod = "";
    String wvarUsuario = "";
    String wvarModeAutCod = "";
    String wvarSumAseg = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarOpcion = "";
    String wvarOperacion = "";
    String wvarPresCod = "";
    String wvarInstala = "";
    String strParseString = "";
    String wvarError = "";
    int wvarPos = 0;
    String wvarAuxReq = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      //Levanto los parámetros que llegan desde la página
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      //Deberá venir desde la página
      wvarUsuario = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Usuario ) */ ) + Strings.space( 10 ), 10 );
      wvarModeAutCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) */ );
      wvarSumAseg = Strings.right( Strings.fill( 15, "0" ) + Strings.replace( Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SumAseg ) */ ), ",", "" ), ".", "" ), 15 );
      wvarProvi = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Provi ) */ ), 2 );
      wvarLocalidadCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod ) */ );
      if( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Opcion ) */ ).equals( "" ) )
      {
        wvarOpcion = "N";
      }
      else
      {
        wvarOpcion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Opcion ) */ );
      }
      wvarOperacion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Operacion ) */ );
      wvarPresCod = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PresCod ) */ ), 4 );
      wvarInstala = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Instala ) */ );
      //
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //Levanto los parámetros generales (ParametrosMQ.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD ) */ );
      wvarRamopcod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosAutoScoring + ModGeneral.gcteRAMOPCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarRamopcod + wvarModeAutCod + wvarSumAseg + wvarProvi + wvarLocalidadCod + wvarOpcion + wvarOperacion + wvarPresCod + wvarInstala;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      //cantidad de caracteres ocupados por parámetros de entrada
      wvarPos = 120;
      //
      wvarStep = 190;
      if( Strings.mid( strParseString, 19, 2 ).equals( "ER" ) )
      {
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 210;
        wvarResult = "";
        wvarAuxReq = Strings.mid( strParseString, wvarPos, 1 );
        wvarResult = "<REQ>" + wvarAuxReq + "</REQ>";
        wvarPos = wvarPos + 1;
        wvarResult = wvarResult + "<GAMA>" + Strings.mid( strParseString, wvarPos, 1 ) + "</GAMA>";
        wvarPos = wvarPos + 1;
        //
        if( (wvarOpcion.equals( "S" )) && ((wvarAuxReq.equals( "S" )) || (wvarAuxReq.equals( "O" ))) )
        {
          //
          wvarStep = 220;
          wvarResultParcial = wvarResultParcial + invoke( "ParseoMensaje", new Variant[] { new Variant(Strings.mid( new Variant(strParseString), new Variant(wvarPos), new Variant(6603) )), new Variant("E") } );
          //
          wvarStep = 230;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 240;
          //unsup wobjXMLResponse.async = false;
          wobjXMLResponse.loadXML( wvarResultParcial );
          //
          if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
          {
            wvarStep = 250;
            //unsup wobjXSLResponse.async = false;
            wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
            //
            wvarStep = 260;
            wvarResultParcial = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
            wvarResult = wvarResult + "<ESTADOS>" + wvarResultParcial + "</ESTADOS>";
            //
            wvarStep = 270;
            wobjXMLResponse = (diamondedge.util.XmlDom) null;
            wobjXSLResponse = (diamondedge.util.XmlDom) null;
            //
          }
          else
          {
            //
            wvarStep = 280;
            wvarResult = wvarResult + "<ESTADOS></ESTADOS>";
            //
          }
          //
          wvarStep = 290;
          //
          wvarResultParcial = "";
          wvarResultParcial = wvarResultParcial + invoke( "ParseoMensaje", new Variant[] { new Variant(Strings.mid( new Variant(strParseString), new Variant(wvarPos + 6603), new Variant(7803) )), new Variant("P") } );
          //
          wvarStep = 300;
          wobjXMLResponse = new diamondedge.util.XmlDom();
          wobjXSLResponse = new diamondedge.util.XmlDom();
          //
          wvarStep = 310;
          //unsup wobjXMLResponse.async = false;
          wobjXMLResponse.loadXML( wvarResultParcial );
          //
          if( null /*unsup wobjXMLResponse.selectNodes( "//R" ) */.getLength() != 0 )
          {
            wvarStep = 320;
            //unsup wobjXSLResponse.async = false;
            wobjXSLResponse.loadXML( invoke( "p_GetXSLProve", new Variant[] {} ) );
            //
            wvarStep = 330;
            wvarResultParcial = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
            wvarResult = wvarResult + "<PROVEE>" + wvarResultParcial + "</PROVEE>";
            //
            wvarStep = 340;
            wobjXMLResponse = (diamondedge.util.XmlDom) null;
            wobjXSLResponse = (diamondedge.util.XmlDom) null;
            //
          }
          else
          {
            //
            wvarStep = 350;
            wvarResult = wvarResult + "<PROVEE></PROVEE>";
            //
          }
          //
        }
        //
        wvarStep = 355;
        wvarPos = wvarPos + 6603 + 7803;
        wvarResult = wvarResult + "<DISPGEN>" + Strings.mid( strParseString, wvarPos, 4 ) + "</DISPGEN>";
        wvarPos = wvarPos + 4;
        wvarResult = wvarResult + "<DISPGENDESC>" + Strings.mid( strParseString, wvarPos, 30 ) + "</DISPGENDESC>";
        wvarStep = 360;
        Response.set( "<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>" );
        //
      }
      //
      wvarStep = 370;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      //~~~~~~~~~~~~~~~
      ClearObjects: 
      //~~~~~~~~~~~~~~~
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarRamopcod + wvarModeAutCod + wvarSumAseg + wvarProvi + wvarLocalidadCod + wvarOpcion + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,2))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='flag'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,3,30))' />";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private String p_GetXSLProve() throws Exception
  {
    String p_GetXSLProve = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
    wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,4))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='acccod'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,35,4))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='disvip'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,39,1))' />";
    wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
    wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,5,30))' />";
    wvarStrXSL = wvarStrXSL + "         </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSLProve = wvarStrXSL;
    return p_GetXSLProve;
  }

  private String ParseoMensaje( String strParseString, String strTipo ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";
    int wvarPos = 0;
    int wvarCantLin = 0;
    int wvarCounter = 0;
    //
    wvarResult = wvarResult + "<RS>";
    //
    wvarPos = 1;
    if( strTipo.equals( "E" ) )
    {
      if( (!Strings.trim( Strings.mid( strParseString, wvarPos, 3 ) ).equals( "000" )) && (!Strings.trim( Strings.mid( strParseString, wvarPos, 3 ) ).equals( "" )) )
      {
        wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos, 3 ) );
        wvarPos = wvarPos + 3;
        //
        for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
        {
          wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos, 33 ) + "]]></R>";
          wvarPos = wvarPos + 33;
        }
      }
    }
    else
    {
      if( (!Strings.trim( Strings.mid( strParseString, wvarPos, 3 ) ).equals( "000" )) && (!Strings.trim( Strings.mid( strParseString, wvarPos, 3 ) ).equals( "" )) )
      {
        wvarCantLin = Obj.toInt( Strings.mid( strParseString, wvarPos, 3 ) );
        wvarPos = wvarPos + 3;
        //
        for( wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++ )
        {
          wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid( strParseString, wvarPos, 39 ) + "]]></R>";
          wvarPos = wvarPos + 39;
        }
      }
    }
    //
    wvarResult = wvarResult + "</RS>";
    //
    ParseoMensaje = wvarResult;
    //
    return ParseoMensaje;
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
