package com.qbe.services.ovmqcotizar.impl;
import java.sql.CallableStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.Parameter;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVAleatorio implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVSQLCotizar.lbaw_OVAleatorio";
  static final String mcteStoreProc = "SPSNCV_DDJJ_VER_ALEATORIO";
  protected static Logger logger = Logger.getLogger(lbaw_OVAleatorio.class.getName());
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_PatenNum = "//PATENNUM";
  static final String mcteParam_MotorNum = "//MOTORNUM";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  static final String mcteParam_CampaCod = "//CAMPACOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarStep = 0;
    String wvarResult = "";
    String mvarEfectAnn = "";
    String mvarPatenNum = "";
    String mvarMotorNum = "";
    String mvarNacimAnn = "";
    String mvarNacimMes = "";
    String mvarNacimDia = "";
    String mvarLocalidadCod = "";
    String mvarCampaCod = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      logger.log(Level.INFO, "||||||"+Request+"||||||");
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      mvarEfectAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  );
      mvarPatenNum = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_PatenNum )  );
      mvarMotorNum = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_MotorNum )  );
      mvarNacimAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimAnn )  );
      mvarNacimMes = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimMes )  );
      mvarNacimDia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimDia )  );
      mvarLocalidadCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod )  );
      mvarCampaCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CampaCod )  );
      //
      //
      wobjXMLRequest = null;
      //
      wvarStep = 20;
      java.sql.Connection jdbcConn = null;
      try {
          JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
          jdbcConn = connFactory.getJDBCConnection(ModGeneral.gcteDB);
          //String sp = "{call " + mcteStoreProc + "()}";
          String sp = "{call " + mcteStoreProc + "(?,?,?,?,?,?,?,?)}";
          CallableStatement ps = jdbcConn.prepareCall(sp);
          ps.setInt("ANIO", Integer.parseInt(mvarEfectAnn) );
          ps.setString("PATENTE",  mvarPatenNum);
          ps.setString("MOTOR",  mvarMotorNum);
          ps.setInt("ANIO_NAC", Integer.parseInt(mvarNacimAnn) );
          ps.setInt("MES_NAC", Integer.parseInt(mvarNacimMes) );
          ps.setInt("DIA_NAC", Integer.parseInt(mvarNacimDia) );
          ps.setInt("CODPOS", Integer.parseInt(mvarLocalidadCod) );
          ps.setString("CAMPACOD",  mvarCampaCod);
         
          boolean result = ps.execute();
          
          if (result) {
        	  //El primero es un ResultSet
          } else {
        	  //Salteo el "updateCount"
        	  int uc = ps.getUpdateCount();
        	  if (uc  == -1) {
//        		  System.out.println("No hay resultados");
        	  }
        	  result = ps.getMoreResults();
          }


          java.sql.ResultSet cursor = ps.getResultSet();
          if( cursor.next())
      {
        //
        wvarStep = 160;
        wobjXMLResponse = new XmlDomExtended();
        wobjXSLResponse = new XmlDomExtended();
        //
        wvarStep = 170;
        wobjXMLResponse.load(AdoUtils.saveResultSet(cursor));
        //
        wvarStep = 180;
        wobjXSLResponse.loadXML( p_GetXSL());
        //
        wvarStep = 190;
        wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
        //
        wvarStep = 200;
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        wvarStep = 210;
        Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarResult + "</Response>" );
      }
      else
      {
        wvarStep = 220;
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"NO SE ENCONTRARON DATOS.\" /></Response>" );
      }
      //
      } finally {
          try {
              if (jdbcConn != null)
                  jdbcConn.close();
          } catch (Exception e) {
              logger.log(Level.WARNING, "Exception al hacer un close", e);
          }
      }

      
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception e ) {
        logger.log(Level.SEVERE, this.getClass().getName(), e);
        throw new ComponentExecutionException(e);
    }
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='ESTADO'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:value-of select='@RES' />";
    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
