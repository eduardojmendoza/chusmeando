package com.qbe.services.ovmqcotizar.impl;

import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetCentrosIns implements VBObjectClass {
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetCentrosIns";
	static final String mcteOpID = "1512";

	protected static Logger logger = Logger.getLogger(lbaw_OVGetCentrosIns.class.getName());
	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_Usuario = "//USUARIO";
	static final String mcteParam_Agentcod = "//AGENTCOD";
	static final String mcteParam_AgentCla = "//AGENTCLA";
	private Object mobjCOM_Context = null;
	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLConfig = null;
		XmlDomExtended wobjXMLParametros = null;
		XmlDomExtended wobjXMLResponse = null;
		XmlDomExtended wobjXSLResponse = null;
		int wvarMQError = 0;
		String wvarArea = "";
		MQProxy wobjFrame2MQ = null;
		int wvarStep = 0;
		String wvarResult = "";
		String wvarCiaAsCod = "";
		String wvarUsuario = "";
		String wvarAgentcod = "";
		String wvarAgentCla = "";
		String strParseString = "";
		String wvarError = "";
		Variant wvarPos = new Variant();
		//
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			// Levanto los par�metros que llegan desde la p�gina
			wobjXMLRequest = new XmlDomExtended();
			wobjXMLRequest.loadXML(Request);
			wvarStep = 20;
			// Deber� venir desde la p�gina
			wvarUsuario = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Usuario)) + Strings.space(10), 10);
			wvarAgentcod = Strings.right(
					Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Agentcod)),
					4);
			if (wobjXMLRequest.selectNodes(mcteParam_AgentCla).getLength() > 0) {
				wvarAgentCla = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AgentCla));
			} else {
				if (!wvarAgentcod.equals("0000")) {
					wvarAgentCla = "PR";
				} else {
					wvarAgentCla = "  ";
				}
			}
			//
			//
			wvarStep = 30;
			wobjXMLRequest = null;
			//
			wvarStep = 40;
			// Levanto los datos de la cola de MQ del archivo de configuraci�n
			wobjXMLConfig = new XmlDomExtended();
			wobjXMLConfig.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
			//
			// Levanto los par�metros generales (ParametrosMQ.xml)
			wvarStep = 70;
			wobjXMLParametros = new XmlDomExtended();
			wobjXMLParametros.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
			wvarCiaAsCod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD));
			//
			wvarStep = 80;
			wobjXMLParametros = null;
			//
			wvarStep = 90;
			wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000"
					+ wvarAgentcod + wvarAgentCla;
			XmlDomExtended
					.setText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL"),
							String.valueOf(VB
									.val(XmlDomExtended
											.getText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL")))
									* 40));

			wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
			StringHolder strParseStringHolder = new StringHolder(strParseString);
			wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
			strParseString = strParseStringHolder.getValue();

			//
			wvarStep = 150;
			if (wvarMQError != 0) {
				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible" + String.valueOf((char) (34)) + " />"
						+ "Codigo Error:" + wvarMQError + "</Response>");
				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
						mcteClassName + "--" + mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - "
								+ strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(),
						vbLogEventTypeError);
				return IAction_Execute;
			}
			//
			//
			// cantidad de caracteres ocupados por par�metros de entrada
			wvarPos.set(73);
			//
			wvarStep = 200;
			if (Strings.mid(strParseString, 19, 2).equals("ER")) {
				//
				wvarStep = 210;
				Response.set(
						"<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>");
				//
			} else {
				//
				wvarStep = 220;
				wvarResult = "";
				wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString);
				//
				wvarStep = 230;
				wobjXMLResponse = new XmlDomExtended();
				wobjXSLResponse = new XmlDomExtended();
				//
				wvarStep = 240;
				wobjXMLResponse.loadXML(wvarResult);
				//
				if (wobjXMLResponse.selectNodes("//R").getLength() != 0) {
					wvarStep = 250;
					wobjXSLResponse.loadXML(p_GetXSL());
					//
					wvarStep = 260;
					wvarResult = wobjXMLResponse.transformNode(wobjXSLResponse).toString()
							.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "");
					//
					wvarStep = 270;
					wobjXMLResponse = null;
					wobjXSLResponse = null;
					//
					wvarStep = 280;
					Response.set("<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>");
				} else {
					wvarStep = 285;
					Response.set("<Response><Estado resultado='false' mensaje='No se encontraron datos' /></Response>");
				}
				//
				//
			}
			//
			wvarStep = 290;
			/* TBD mobjCOM_Context.SetComplete() ; */
			IAction_Execute = 0;
			//
			// ~~~~~~~~~~~~~~~
			ClearObjects:
			// ~~~~~~~~~~~~~~~
			// LIBERO LOS OBJETOS
			wobjXMLConfig = null;
			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (ComponentExecutionException e) {
			// La propagamos, ya contiene el detalle del error
			throw e;
		} catch (Exception _e_) {
			logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
			Err.set(_e_);
			mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
					wcteFnName, wvarStep,
					Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - "
							+ Err.getError().getDescription() + " Mensaje:" + "" + " Hora:" + DateTime.now(),
					vbLogEventTypeError);
			Err.clear();
			throw new ComponentExecutionException(_e_);
		}
	}

	private void ObjectControl_Activate() throws Exception {
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	private String p_GetXSL() throws Exception {
		String p_GetXSL = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
		wvarStrXSL = wvarStrXSL
				+ "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
		wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
		wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='substring(.,1,4)' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='domic'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,35,40))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='nro'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,75,5))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='codpos'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,80,8))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='local'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,88,40))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='prov'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,130,20))' />";
		wvarStrXSL = wvarStrXSL + "                </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='tel'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,152,30))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='hora'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,182,30))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,5,30))' />";
		wvarStrXSL = wvarStrXSL + "         </xsl:element>";
		wvarStrXSL = wvarStrXSL + "     </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSL = wvarStrXSL;
		return p_GetXSL;
	}

	private String ParseoMensaje(Variant wvarPos, String strParseString) throws Exception {
		String ParseoMensaje = "";
		String wvarResult = "";
		int wvarCant = 0;
		int i = 0;

		//
		wvarResult = wvarResult + "<RS>";
		//
		if ((!Strings.trim(Strings.mid(strParseString, wvarPos.toInt(), 3)).equals("000"))
				&& (!Strings.trim(Strings.mid(strParseString, wvarPos.toInt(), 3)).equals(""))) {
			wvarCant = Obj.toInt(Strings.trim(Strings.mid(strParseString, wvarPos.toInt(), 3)));
			wvarPos.set(wvarPos.add(new Variant(3)));
			for (i = 1; i <= wvarCant; i++) {
				wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid(strParseString, wvarPos.toInt(), 211)
						+ "]]></R>";
				wvarPos.set(wvarPos.add(new Variant(211)));
			}
		}

		//
		wvarResult = wvarResult + "</RS>";
		//
		ParseoMensaje = wvarResult;
		//
		return ParseoMensaje;
	}

	public void Activate() throws Exception {
		//
		/* TBD mobjCOM_Context = this.GetObjectContext() ; */
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
		mobjCOM_Context = (Object) null;
		mobjEventLog = null;
		//
	}
}
