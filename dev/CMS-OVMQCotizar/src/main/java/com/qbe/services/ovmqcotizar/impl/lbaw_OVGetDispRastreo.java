package com.qbe.services.ovmqcotizar.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * Invoca al mensaje 1510 de AIS
 * 
 * @author ramiro
 *
 */
public class lbaw_OVGetDispRastreo implements VBObjectClass {

	protected static Logger logger = Logger.getLogger(lbaw_OVGetDispRastreo.class.getName());

	static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetDispRastreo";
	static final String mcteOpID = "1510";

	static final String mcteParam_Usuario = "//USUARIO";
	static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
	static final String mcteParam_SumAseg = "//SUMASEG";
	static final String mcteParam_Provi = "//PROVI";
	static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
	static final String mcteParam_Opcion = "//OPCION";
	static final String mcteParam_Operacion = "//OPERACION";
	static final String mcteParam_PresCod = "//PRESCOD";
	static final String mcteParam_Instala = "//INSTALA";

	private EventLog mobjEventLog = new EventLog();

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLParametros = null;
		XmlDomExtended wobjXMLResponse = null;
		XmlDomExtended wobjXSLResponse = null;
		int wvarMQError = 0;
		String wvarArea = "";
		MQProxy wobjFrame2MQ = null;
		int wvarStep = 0;
		String wvarResult = "";
		String wvarResultParcial = "";
		String wvarCiaAsCod = "";
		String wvarRamopcod = "";
		String wvarUsuario = "";
		String wvarModeAutCod = "";
		String wvarSumAseg = "";
		String wvarProvi = "";
		String wvarLocalidadCod = "";
		String wvarOpcion = "";
		String wvarOperacion = "";
		String wvarPresCod = "";
		String wvarInstala = "";
		String strParseString = "";
		String wvarError = "";
		int wvarPos = 0;
		String wvarAuxReq = "";

		try {

			wvarStep = 10;

			wobjXMLRequest = new XmlDomExtended();
			wobjXMLRequest.loadXML(Request);

			wvarUsuario = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Usuario)) + Strings.space(10), 10);
			wvarModeAutCod = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_ModeAutCod));
			wvarSumAseg = Strings.right(Strings.fill(15, "0") + Strings.replace(Strings.replace(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_SumAseg)), ",", ""), ".", ""), 15);
			wvarProvi = Strings.right("00" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Provi)),
					2);
			wvarLocalidadCod = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_LocalidadCod));
			if (XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Opcion)).equals("")) {
				wvarOpcion = "N";
			} else {
				wvarOpcion = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Opcion));
			}
			wvarOperacion = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Operacion));
			wvarPresCod = Strings.right(
					Strings.fill(4, " ") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_PresCod)),
					4);
			wvarInstala = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Instala));

			wobjXMLParametros = new XmlDomExtended();
			wobjXMLParametros.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
			logger.fine(String.format("wobjXMLParametros = %s",
					wobjXMLParametros == null ? "NULL!!!" : wobjXMLParametros.marshal()));
			wvarCiaAsCod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD));
			wvarRamopcod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosAutoScoring + ModGeneral.gcteRAMOPCOD));
			logger.fine(String.format("wvarRamopcod = %s", wvarRamopcod));
			//
			wvarStep = 70;
			wobjXMLParametros = null;
			//
			wvarStep = 80;
			wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000"
					+ wvarRamopcod + wvarModeAutCod + wvarSumAseg + wvarProvi + wvarLocalidadCod + wvarOpcion
					+ wvarOperacion + wvarPresCod + wvarInstala;
			wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
			StringHolder strParseStringHolder = new StringHolder(strParseString);
			wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
			strParseString = strParseStringHolder.getValue();

			//
			wvarStep = 150;
			if (wvarMQError != 0) {
				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible" + String.valueOf((char) (34)) + " />"
						+ "Codigo Error:" + wvarMQError + "</Response>");
				mobjEventLog
						.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
								mcteClassName + "--" + mcteClassName,
								"", wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - "
										+ strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(),
						vbLogEventTypeError);
				return IAction_Execute;
			}
			// cantidad de caracteres ocupados por parámetros de entrada
			wvarPos = 120;

			if (Strings.mid(strParseString, 19, 2).equals("ER")) {
				//
				wvarStep = 200;
				Response.set(
						"<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>");
				//
			} else {
				//
				wvarStep = 210;
				wvarResult = "";
				wvarAuxReq = Strings.mid(strParseString, wvarPos, 1);
				wvarResult = "<REQ>" + wvarAuxReq + "</REQ>";
				wvarPos = wvarPos + 1;
				wvarResult = wvarResult + "<GAMA>" + Strings.mid(strParseString, wvarPos, 1) + "</GAMA>";
				wvarPos = wvarPos + 1;
				//
				if ((wvarOpcion.equals("S")) && ((wvarAuxReq.equals("S")) || (wvarAuxReq.equals("O")))) {
					//
					wvarStep = 220;
					wvarResultParcial = wvarResultParcial
							+ ParseoMensaje(Strings.mid(strParseString, wvarPos, 6603), "E");
					//
					wvarStep = 230;
					wobjXMLResponse = new XmlDomExtended();
					wobjXSLResponse = new XmlDomExtended();
					//
					wvarStep = 240;
					wobjXMLResponse.loadXML(wvarResultParcial);
					//
					if (wobjXMLResponse.selectNodes("//R").getLength() != 0) {
						wvarStep = 250;
						wobjXSLResponse.loadXML(p_GetXSL());
						//
						wvarStep = 260;
						wvarResultParcial = wobjXMLResponse.transformNode(wobjXSLResponse).toString()
								.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "");
						wvarResult = wvarResult + "<ESTADOS>" + wvarResultParcial + "</ESTADOS>";
						//
						wvarStep = 270;
						wobjXMLResponse = null;
						wobjXSLResponse = null;
						//
					} else {
						//
						wvarStep = 280;
						wvarResult = wvarResult + "<ESTADOS></ESTADOS>";
						//
					}
					//
					wvarStep = 290;
					//
					wvarResultParcial = "";
					wvarResultParcial = wvarResultParcial
							+ ParseoMensaje(Strings.mid(strParseString, wvarPos + 6603, 7803), "P");
					//
					wvarStep = 300;
					wobjXMLResponse = new XmlDomExtended();
					wobjXSLResponse = new XmlDomExtended();
					//
					wvarStep = 310;
					wobjXMLResponse.loadXML(wvarResultParcial);
					//
					if (wobjXMLResponse.selectNodes("//R").getLength() != 0) {
						wvarStep = 320;
						wobjXSLResponse.loadXML(p_GetXSLProve());
						//
						wvarStep = 330;
						wvarResultParcial = wobjXMLResponse.transformNode(wobjXSLResponse).toString()
								.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "");
						wvarResult = wvarResult + "<PROVEE>" + wvarResultParcial + "</PROVEE>";
						//
						wvarStep = 340;
						wobjXMLResponse = null;
						wobjXSLResponse = null;
						//
					} else {
						//
						wvarStep = 350;
						wvarResult = wvarResult + "<PROVEE></PROVEE>";
						//
					}
					//
				}
				//
				wvarStep = 355;
				wvarPos = wvarPos + 6603 + 7803;
				wvarResult = wvarResult + "<DISPGEN>" + Strings.mid(strParseString, wvarPos, 4) + "</DISPGEN>";
				wvarPos = wvarPos + 4;
				wvarResult = wvarResult + "<DISPGENDESC>" + Strings.mid(strParseString, wvarPos, 30) + "</DISPGENDESC>";
				wvarStep = 360;
				Response.set("<Response><Estado resultado='true' mensaje='' />" + wvarResult + "</Response>");
				//
			}
			//
			wvarStep = 370;
			IAction_Execute = 0;

			return IAction_Execute;
		} catch (com.qbe.connector.mq.MQProxyTimeoutException toe) {
			Response.set(
					"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>");
			return 3;
		} catch (Exception _e_) {
			logger.log(Level.SEVERE, this.getClass().getName(), _e_);
			throw new ComponentExecutionException(_e_);
		}
	}

	private String p_GetXSL() throws Exception {
		String p_GetXSL = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
		wvarStrXSL = wvarStrXSL
				+ "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
		wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
		wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,2))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='flag'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,3,30))' />";
		wvarStrXSL = wvarStrXSL + "         </xsl:element>";
		wvarStrXSL = wvarStrXSL + "     </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSL = wvarStrXSL;
		return p_GetXSL;
	}

	private String p_GetXSLProve() throws Exception {
		String p_GetXSLProve = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
		wvarStrXSL = wvarStrXSL
				+ "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
		wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
		wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,1,4))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='acccod'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,35,4))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='disvip'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,39,1))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,5,30))' />";
		wvarStrXSL = wvarStrXSL + "         </xsl:element>";
		wvarStrXSL = wvarStrXSL + "     </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSLProve = wvarStrXSL;
		return p_GetXSLProve;
	}

	private String ParseoMensaje(String strParseString, String strTipo) throws Exception {
		String ParseoMensaje = "";
		String wvarResult = "";
		int wvarPos = 0;
		int wvarCantLin = 0;
		int wvarCounter = 0;
		//
		wvarResult = wvarResult + "<RS>";
		//
		wvarPos = 1;
		if (strTipo.equals("E")) {
			if ((!Strings.trim(Strings.mid(strParseString, wvarPos, 3)).equals("000"))
					&& (!Strings.trim(Strings.mid(strParseString, wvarPos, 3)).equals(""))) {
				wvarCantLin = Obj.toInt(Strings.mid(strParseString, wvarPos, 3));
				wvarPos = wvarPos + 3;
				//
				for (wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++) {
					wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid(strParseString, wvarPos, 33) + "]]></R>";
					wvarPos = wvarPos + 33;
				}
			}
		} else {
			if ((!Strings.trim(Strings.mid(strParseString, wvarPos, 3)).equals("000"))
					&& (!Strings.trim(Strings.mid(strParseString, wvarPos, 3)).equals(""))) {
				wvarCantLin = Obj.toInt(Strings.mid(strParseString, wvarPos, 3));
				wvarPos = wvarPos + 3;
				//
				for (wvarCounter = 0; wvarCounter <= (wvarCantLin - 1); wvarCounter++) {
					wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid(strParseString, wvarPos, 39) + "]]></R>";
					wvarPos = wvarPos + 39;
				}
			}
		}
		//
		wvarResult = wvarResult + "</RS>";
		//
		ParseoMensaje = wvarResult;
		//
		return ParseoMensaje;
	}

	public void Activate() throws Exception {
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
	}
}
