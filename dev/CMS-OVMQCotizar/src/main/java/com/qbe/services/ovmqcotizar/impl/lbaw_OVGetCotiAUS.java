package com.qbe.services.ovmqcotizar.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.util.StringUtils;
import org.w3c.dom.Node;

import com.qbe.connector.mq.MQProxy;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.ofvirtuallba.impl.lbaw_GetNroCot;
import com.qbe.services.ofvirtuallba.impl.lbaw_GetParamGral;
import com.qbe.services.ofvirtuallba.impl.lbaw_GetPortalComerc;
import com.qbe.services.webmq.impl.lbaw_GetSumAseg;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;

public class lbaw_OVGetCotiAUS implements VBObjectClass
{
  /**
   * 
   * Implementacion de los objetos
   * 
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetCotiAUS";
  static final String mcteOpID = "0047";
  /**
   * 
   * Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_Sexo = "//SEXO";
  static final String mcteParam_Estado = "//ESTADO";
  static final String mcteParam_IVA = "//IVA";
  static final String mcteParam_IBB = "//IBB";
  /**
   * Fjo 2009-02-13
   */
  static final String mcteParam_CLIENTIP = "//CLIENTIP";
  static final String mcteParam_CobroTip = "//COBROTIP";
  static final String mcteParam_CobroCod = "//COBROCOD";
  static final String mcteParam_AgeCod = "//AGECOD";
  /**
   * MMC 2012-11-28
   */
  static final String mcteParam_AgeCla = "//AGECLA";
  static final String mcteParam_DatosPlan = "//DATOSPLAN";
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "//KMSRNGCOD";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_SiGarage = "//SIGARAGE";
  static final String mcteParam_Siniestros = "//SINIESTROS";
  static final String mcteParam_Gas = "//GAS";
  static final String mcteParam_ClubLBA = "//CLUBLBA";
  static final String mcteParam_Luneta = "//LUNETA";
  static final String mcteParam_Green = "//CLUBECO";
  static final String mcteParam_Granizo = "//GRANIZO";
  static final String mcteParam_RoboCont = "//ROBOCONT";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  static final String mcteParam_Campacod = "//CAMPACOD";
  static final String mcteParam_EsCero = "//ESCERO";
  static final String mcteParam_Portal = "//PORTAL";
  /**
   * 23/01/2006
   */
  static final String mcteParam_DEST80 = "//DESTRUCCION_80";
  /**
   * Datos de los Hijos
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  /**
   * Datos de los Accesorios
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Para Mercado Abierto
   */
  static final String mcteParam_CampaTel = "//TELEFONO";
  /**
   * Incios y Tamaños
   */
  static final int mcteInicioCHijos = 3 - 1;
  static final int mcteInicioSHijos = 2323 - 1;
  static final int mcteInicioOtrosD = 6689 - 1;
  static final int mcteTamanoCHijos = 116;
  static final int mcteTamanoSHijos = 165;
  static final int mcteTamanoOtrosD = 129;
  static final int mcteInicioTbDECA = 5633 - 1;
  static final int mcteTamanoTbDECA = 35;
  static final int mcteInicioMxDECA = 6686 - 1;
  
  
  protected static Logger logger = Logger.getLogger(lbaw_OVGetCotiAUS.class.getName());

  private EventLog mobjEventLog = new EventLog();

  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParams = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    lbaw_GetPortalComerc wobjClassGetPortalComerc = new lbaw_GetPortalComerc();
    String wvarRequest = "";
    String wvarResponse = "";
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarMensaje = "";
    String wvarNacimAnn = "";
    String wvarNacimMes = "";
    String wvarNacimDia = "";
    String wvarSexo = "";
    String wvarEstado = "";
    String wvarIVA = "";
    String wvarIBB = "";
    String wvarCLIENTIP = "";
    String wvarCobroCod = "";
    String wvarCobroTip = "";
    String wvarAgeCod = "";
    String wvarAgeCla = "";
    String wvarDatosPlan = "";
    String wvarModeAutCod = "";
    String wvarKMsrngCod = "";
    String wvarEfectAnn = "";
    String wvarSiGarage = "";
    String wvarSiniestros = "";
    String wvarGas = "";
    String wvarClubLBA = "";
    String wvarLuneta = "";
    String wvarGreen = "";
    String wvarGranizo = "";
    String wvarRoboCont = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarHijos = "";
    String wvarConHijos = "";
    String wvarAccesorios = "";
    String wvarCampacod = "";
    String wvarEsCero = "";
    String wvarDEST80 = "";
    String wvarSumaMin = "";
    String wvarSumAseg = "";
    String wvarSumaLBA = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaSig = "";
    String wvarCampaTelForm = "";
    String wvarCampaTel = "";
    String strParseString = "";
    int wvariCounter = 0;
    String wvarAuxCodAcc = "";
    String wvarAuxPlanCicle = "";
    @SuppressWarnings("unused")
	String wvarAuxCobeCicle = "";
    int wvarContadorPlan = 0;
    int wvarContadorCobs = 0;

    //
    //Fjo 2009-02-13
    //MMC 2012-11-28
    //Agregado para promos de los 0Km - 07/12/2005
    //Agregado para la Destrucción al 80% - 23/01/2006
    //

    //~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Cargar el XML de Entrada
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      //
      // Obtener el Numero de Cotización
      wvarStep = 10;
      wvarRequest = "<Request></Request>";
      lbaw_GetNroCot wobjClassGetNroCot = new lbaw_GetNroCot();
      StringHolder strHoldGetNroCot = new StringHolder(wvarResponse);
      wobjClassGetNroCot.IAction_Execute(wvarRequest, strHoldGetNroCot, "");
      wvarResponse = strHoldGetNroCot.getValue(); 

      //
      wvarStep = 20;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.loadXML( wvarResponse );
      wvarCotiID = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//NROCOT" )  );
      wvarFechaDia = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//FECHA_DIA" )  );
      wvarFechaSig = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//FECHA_DIA" )  );
      //
      wobjXMLParams = null;
      //
      //Obtener la Mínima Suma
      wvarStep = 30;
      wvarRequest = "<Request><CONCEPTO>SA-SCO-MIN</CONCEPTO></Request>";
      lbaw_GetParamGral wobjClassGetParamGral = new lbaw_GetParamGral();
      StringHolder strHoldGetParamGra = new StringHolder(wvarResponse);
      wobjClassGetParamGral.IAction_Execute(wvarRequest, strHoldGetParamGra, "" );
      wvarResponse = strHoldGetParamGra.getValue();

      //
      wvarStep = 40;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.loadXML( wvarResponse );
      //Antes: wvarSumaMin = Strings.right( Strings.fill( 11, "0" ) + String.valueOf( Obj.toDecimal( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//PARAMNUM" )  ) ).multiply( new java.math.BigDecimal( 100 ) ).doubleValue() ), 11 );
    
      // Inicio parche
      String paramNumOriginal = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( "//PARAMNUM" )  );

      String numeroConvertido;
       if (paramNumOriginal.indexOf(".") != -1) {
    	  numeroConvertido = paramNumOriginal.substring(0, paramNumOriginal.indexOf("."));
      } else {
    	  numeroConvertido = paramNumOriginal;
      }
       // Fin parche
      wvarSumaMin = Strings.right( Strings.fill( 11, "0" ) + numeroConvertido.concat("00") , 11 );
      //
      wobjXMLParams = null;
      //
      //Buscar la Suma Asegurada para AUPROCOD = 02
      wvarStep = 50;
      
      //Parche para tierra del fuego, Bug 1418
//    If CInt(Right("00" & wobjXMLRequest.selectSingleNode(mcteParam_Provi).Text,2)) = 25 Then 'Tierra del Fuego 19/06/2014
      String provincia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Provi ));
      String auprocod = "02"; //default
      if ( provincia != null && provincia.equals("25")) {
          auprocod = "33";
      }
      wvarRequest = "<Request>" +
        		"<AUMODCOD>" + 
        			XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod )  ) + 
        		"</AUMODCOD>" +
        		"<AUPROCOD>" + 
        		auprocod + 
        		"</AUPROCOD>" +
        		"</Request>";
      lbaw_GetSumAseg wobjClassGetSumAseg = new lbaw_GetSumAseg();
      StringHolder strHoldGetSumAseg1 = new StringHolder(wvarResponse);
      wobjClassGetSumAseg.IAction_Execute( wvarRequest, strHoldGetSumAseg1, "" );
      wvarResponse = strHoldGetSumAseg1.getValue();
      //
      //Valor de SUMASEG
      wvarStep = 60;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.loadXML( wvarResponse );
      
      Node nodeEfectAnn = wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn );
      String nodeEffectAnnValue = XmlDomExtended.getText( nodeEfectAnn  );
      if ( StringUtils.isEmpty(nodeEffectAnnValue)) {
          throw new ComponentExecutionException("No especificó el parámetro " + mcteParam_EfectAnn);
      }
      String xpathEffectAnn = "//COMBO/OPTION[@value=" + nodeEffectAnnValue + "]/@sumaseg";
      if( wobjXMLParams.selectSingleNode( xpathEffectAnn  ) == (org.w3c.dom.Node) null )
      {
        wvarMensaje = "No se pueden cotizar autos para el año " + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  );
        //FIXEDunsup GoTo ErrorHandler
        throw new Exception(wvarMensaje);
      }
      else
      {
        wvarSumAseg = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( XmlDomExtended.getText(wobjXMLParams.selectSingleNode( "//COMBO/OPTION[@value=" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  ) + "]/@sumaseg" )), ",", "" ), 11 );
      }
      wobjXMLParams = null;
      //
      //Buscar la Suma Asegurada para AUPROCOD = 03
      wvarStep = 70;
      wvarRequest = "<Request><AUMODCOD>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod )  ) + "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>";
      StringHolder strHoldGetSumAseg_2 = new StringHolder(wvarResponse);
      wobjClassGetSumAseg.IAction_Execute( wvarRequest, strHoldGetSumAseg_2, "" );
      wvarResponse = strHoldGetSumAseg_2.getValue();
      //
      //Valor de SUMALBA
      wvarStep = 80;
      wobjXMLParams = new XmlDomExtended();
      wobjXMLParams.loadXML( wvarResponse );
      wvarSumaLBA = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( XmlDomExtended.getText(wobjXMLParams.selectSingleNode( "//COMBO/OPTION[@value=" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) ) + "]/@sumaseg" ) ), ",", "" ), 11 );
      wobjXMLParams = null;
      wobjClassGetSumAseg = null;
      //
      wvarStep = 90;
      if( wobjXMLRequest.selectNodes( mcteParam_Cliensec ) .getLength() == 0 )
      {
        wvarCliensec = Strings.fill( 9, "0" );
      }
      else
      {
        wvarCliensec = Strings.right( Strings.fill( 9, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Cliensec )  ), 9 );
      }
      wvarNacimAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimAnn )  );
      wvarNacimMes = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimMes )  );
      wvarNacimDia = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NacimDia )  );
      wvarSexo = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Sexo )  );
      wvarEstado = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Estado )  );
      wvarIVA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IVA )  );
      wvarIBB = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_IBB )  );
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CLIENTIP ) .getLength() == 0 )
      {
        //Para forzar la persona física si no informa el nodo
        wvarCLIENTIP = "00";
      }
      else
      {
        wvarCLIENTIP = Strings.right( "00" + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENTIP )  ) ), 2 );
      }
      //
      //Mercado Abierto siempre Cotiza con COBROCOD = 4 y COBROTIP = VI
      if( wobjXMLRequest.selectNodes( mcteParam_CobroCod ) .getLength() == 0 )
      {
        wvarCobroCod = "4";
      }
      else
      {
        wvarCobroCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CobroCod )  );
      }
      //
      if( wobjXMLRequest.selectNodes( mcteParam_CobroTip ) .getLength() == 0 )
      {
        wvarCobroTip = "VI";
      }
      else
      {
        wvarCobroTip = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CobroTip )  );
      }
      //
      wvarAgeCod = Strings.right( Strings.fill( 4, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AgeCod )  ) ), 4 );
      //
      if( wobjXMLRequest.selectNodes( mcteParam_AgeCla ) .getLength() == 0 )
      {
        wvarAgeCla = "PR";
      }
      else
      {
        //2012-11-28
        wvarAgeCla = Strings.right( Strings.fill( 2, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AgeCla )  ) ), 2 );
      }
      //Plan y Franquicia
      if( wobjXMLRequest.selectNodes( mcteParam_DatosPlan ) .getLength() == 0 )
      {
        //Cotiza todos los Planes
        wvarDatosPlan = "00000";
      }
      else
      {
        wvarDatosPlan = Strings.right( Strings.fill( 5, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DatosPlan )  ), 5 );
      }
      //
      wvarStep = 100;
      wvarModeAutCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod )  );
      wvarKMsrngCod = Strings.right( Strings.fill( 7, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_KMsrngCod )  ), 7 );
      wvarEfectAnn = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn )  );
      wvarSiGarage = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SiGarage )  );
      wvarSiniestros = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Siniestros )  ), 2 );
      wvarGas = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Gas )  );
      wvarClubLBA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ClubLBA )  );
      wvarLuneta = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Luneta )  );
      wvarGreen = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Green )  ), 1 );
      wvarGranizo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Granizo )  ), 1 );
      wvarRoboCont = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RoboCont )  ), 1 );
      wvarProvi = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Provi )  ), 2 );
      wvarLocalidadCod = Strings.right( "0000" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod )  ), 4 );
      // Marca de OKm Se agregó para las promos - 07/12/2005
      wvarEsCero = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EsCero )  );
      // Destrucción al 80%  --- 23/01/2006
      if( wobjXMLRequest.selectNodes( mcteParam_DEST80 ) .getLength() > 0 )
      {
        wvarDEST80 = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DEST80 )  );
      }
      if( wvarDEST80.equals( "" ) )
      {
        wvarDEST80 = "N";
      }
      //
      //~~~~~~~~~~~
      //Incio Hijos
      //~~~~~~~~~~~
      wvarStep = 110;
      wvarHijos = "";
      wvarConHijos = "N";
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Hijos ) ;
      //
      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarConHijos = "S";
        wvarHijos = wvarHijos + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_NacimHijo )  );
        wvarHijos = wvarHijos + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_SexoHijo )  );
        wvarHijos = wvarHijos + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_EstadoHijo )  );
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarHijos = wvarHijos + Strings.fill( 8, "0" ) + "  ";
      }
      //~~~~~~~~~
      //Fin Hijos
      //~~~~~~~~~
      //
      //~~~~~~~~~~~~~~~~~
      //Inicio Accesorios
      //~~~~~~~~~~~~~~~~~
      wvarStep = 120;
      wvarAccesorios = "";
      wobjXMLList = wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) ;
      //
      wvarAuxCodAcc = "";
      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarAuxCodAcc = wvarAuxCodAcc + Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_CodigoAcc )  ), 4 );
        wvarAccesorios = wvarAccesorios + Strings.right( Strings.fill( 14, "0" ) + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_PrecioAcc )  ), 14 );
        wvarAccesorios = wvarAccesorios + Strings.left( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLList.item(wvariCounter), mcteParam_DescripcionAcc )  ) + Strings.fill( 30, " " ), 30 );
        wvarAccesorios = wvarAccesorios + "S";
      }
      //
      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarAccesorios = wvarAccesorios + Strings.fill( 14, "0" ) + Strings.fill( 31, " " );
      }
      //~~~~~~~~~~~~~~
      //Fin Accesorios
      //~~~~~~~~~~~~~~
      wvarCampacod = Strings.right( Strings.fill( 4, "0" ) + Strings.trim( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Campacod )  ) ), 4 );
      //
      //Buscar los datos de Campaña, Productor (Solo para Mercado Abierto)
      if( wobjXMLRequest.selectNodes( mcteParam_Portal ) .getLength() != 0 )
      {
        if( ! (XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Portal )  ).equals( "LBA_PRODUCTORES" )) )
        {
          wvarStep = 130;
          wvarRequest = "<Request><PORTAL>" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Portal )  ) + "</PORTAL><RAMOPCOD>AUS1</RAMOPCOD></Request>";
          wobjClassGetPortalComerc = new lbaw_GetPortalComerc();
          StringHolder strHoldGetPortalComerc = new StringHolder(wvarResponse);
          wobjClassGetPortalComerc.IAction_Execute( wvarRequest, strHoldGetPortalComerc, "");
          wvarResponse = strHoldGetPortalComerc.getValue();          
          //
          wvarStep = 140;
          wobjXMLParams = new XmlDomExtended();
          wobjXMLParams.loadXML( wvarResponse );
          wvarCampacod = Strings.left( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_Campacod )  ) + Strings.fill( 4, " " ), 4 );
          wvarAgeCod = Strings.trim( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_AgeCod )  ) );
          //MMC 2012-11-28
          wvarAgeCla = Strings.trim( XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_AgeCla )  ) );
          wvarCampaTel = XmlDomExtended.getText( wobjXMLParams.selectSingleNode( mcteParam_CampaTel )  );
          wvarCampaTelForm = Formateo_Telefono(XmlDomExtended.getText( wobjXMLParams.selectSingleNode(mcteParam_CampaTel) )  );
          wobjXMLParams = null;
          wobjClassGetPortalComerc =  null;
        }
      }
      //
      wobjXMLRequest = null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 150;

      wvarMensaje = mcteOpID + wvarCliensec + wvarNacimAnn + wvarNacimMes + wvarNacimDia;
      wvarMensaje = wvarMensaje + wvarSexo + wvarEstado;
      wvarMensaje = wvarMensaje + wvarIVA + wvarIBB;
      //Fjo 2009-02-13
      wvarMensaje = wvarMensaje + wvarCLIENTIP;
      wvarMensaje = wvarMensaje + wvarCobroTip + wvarCobroCod + wvarFechaDia + wvarFechaSig + wvarAgeCod + wvarDatosPlan + wvarSumAseg + wvarSumaLBA;
      wvarMensaje = wvarMensaje + wvarModeAutCod + "001" + wvarKMsrngCod + wvarEfectAnn + wvarSiGarage;
      wvarMensaje = wvarMensaje + wvarSiniestros + wvarGas + wvarClubLBA + wvarLuneta + wvarGreen + wvarGranizo + wvarRoboCont + "00" + wvarProvi + wvarLocalidadCod;
      wvarMensaje = wvarMensaje + wvarHijos;
      wvarMensaje = wvarMensaje + wvarAccesorios;
      wvarMensaje = wvarMensaje + Strings.right( Strings.fill( 9, "0" ) + wvarCotiID, 9 ) + wvarSumaMin + wvarCampacod + Strings.fill( 36, " " );
      wvarMensaje = wvarMensaje + "000000000000000000000000NNNSSSN" + Strings.left( (wvarAuxCodAcc + Strings.fill( 40, "0" )), 40 );
      wvarMensaje = wvarMensaje + wvarEsCero + wvarDEST80 + wvarAgeCla;
      //
      wvarArea = wvarMensaje;
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
      StringHolder sHstrParseString = new StringHolder(strParseString);
      try {
		wvarMQError = wobjFrame2MQ.executePrim( wvarArea, sHstrParseString);
	} catch ( MQProxyTimeoutException te) {
		logger.log(Level.SEVERE, "Timeout al enviar mensaje MQ via conector MQProxy.AISPROXY ( error " + wvarMQError + " )",te);
		throw new ComponentExecutionException(3, "Ocurrió un timeout al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", te);
	} catch (MQProxyException e) {
		String msg = "Exception [ Msg:"+ e.getMessage() + ", causa:" + e.getCause() + "] al enviar mensaje: [" + wvarRequest + "]";
		logger.log(Level.SEVERE, msg,e);
		throw new ComponentExecutionException(1, "Ocurrió un error al comunicarse con el cotizador AIS ( error " + wvarMQError + " )", e);
	}
      strParseString = sHstrParseString.getValue();          

      //
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //HRF Anexo I (Nueva Respuesta, Planes y Coberturas Variables)
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //~~~~~~~~~~~~~~~~~~~~~~~~~~
      //Incio Response del Mensaje
      //~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 200;
      wvarResult = "";
      //
      if( Strings.toUpperCase( Strings.left( strParseString, 2 ) ).equals( "OK" ) )
      {
        wvarResult = wvarResult + "<COT_NRO>" + wvarCotiID + "</COT_NRO>";
        wvarResult = wvarResult + "<CAMPA_COD>" + wvarCampacod + "</CAMPA_COD>";
        wvarResult = wvarResult + "<AGE_COD>" + wvarAgeCod + "</AGE_COD>";
        wvarResult = wvarResult + "<AGE_CLA>" + wvarAgeCla + "</AGE_CLA>";
        //Solo para Mercado Abierto
        wvarResult = wvarResult + "<CAMPA_TEL>" + wvarCampaTel + "</CAMPA_TEL>";
        wvarResult = wvarResult + "<CAMPA_TEL_FORM>" + wvarCampaTelForm + "</CAMPA_TEL_FORM>";
        //
        wvarResult = wvarResult + "<SUMASEG>" + wvarSumAseg + "</SUMASEG>";
        wvarResult = wvarResult + "<SUMALBA>" + wvarSumaLBA + "</SUMALBA>";
        wvarResult = wvarResult + "<SILUNETA>" + Strings.trim( Strings.mid( strParseString, (mcteInicioMxDECA + 1 + 1), 1 ) ) + "</SILUNETA>";
        wvarResult = wvarResult + "<TIENEHIJOS>" + wvarConHijos + "</TIENEHIJOS>";
        //
        //~~~~~~~~~~~~
        //Incio Planes
        //~~~~~~~~~~~~
        wvarStep = 210;
        wvarResult = wvarResult + "<PLANES>";
        //
        wvarAuxPlanCicle = "1";
        wvarContadorPlan = 0;
        //Todos los planes
        while( (VBFixesUtil.val( wvarAuxPlanCicle ) > 0) && (wvarContadorPlan < 20) )
        {
          //Plan
          wvarResult = wvarResult + "<PLAN>";
          //Datos
          wvarResult = wvarResult + "<PLANNCOD>" + Strings.trim( Strings.mid( strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 0 + 1), 5 ) ) + "</PLANNCOD>";
          wvarResult = wvarResult + "<PLANNDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 0 + 1), 121 ) ) + "]]></PLANNDES>";
          wvarResult = wvarResult + "<ORDEN>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 125 + 1), 4 ) ) + "</ORDEN>";
          wvarResult = wvarResult + "<LUNPAR>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 121 + 1), 1 ) ) + "</LUNPAR>";
          wvarResult = wvarResult + "<GRANIZO>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 122 + 1), 1 ) ) + "</GRANIZO>";
          wvarResult = wvarResult + "<ROBOCON>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 123 + 1), 1 ) ) + "</ROBOCON>";
          wvarResult = wvarResult + "<ESRC>" + Strings.trim( Strings.mid( strParseString, (mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 124 + 1), 1 ) ) + "</ESRC>";
          //Precio c/Hijos
          wvarResult = wvarResult + "<CON_HIJOS>";
          //LR 11/10/2011 La PRIMA de CON_HIJOS pasa a ser "05 PLANNDES PIC X(1)"
          wvarResult = wvarResult + "<PRIMA>" + FormatoDec(Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 6 + 1), 11 )) + "</PRIMA>";
          wvarResult = wvarResult + "<RECARGOS>" + FormatoDec (Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 17 + 1), 11 )) + "</RECARGOS>";
          wvarResult = wvarResult + "<IVAIMPOR>" + FormatoDec (Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 28 + 1), 11) ) + "</IVAIMPOR>";
          wvarResult = wvarResult + "<IVAIMPOA>" + FormatoDec(Strings.mid( strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 39 + 1), 11 )) + "</IVAIMPOA>";
          wvarResult = wvarResult + "<IVARETEN>" + FormatoDec(Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 50 + 1), 11 ) ) + "</IVARETEN>";
          wvarResult = wvarResult + "<DEREMI>" + FormatoDec (Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 61 + 1), 11) ) + "</DEREMI>";
          wvarResult = wvarResult + "<SELLADO>" + FormatoDec (Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 72 + 1), 11 ) ) + "</SELLADO>";
          wvarResult = wvarResult + "<INGBRU>" + FormatoDec(Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 83 + 1), 11) ) + "</INGBRU>";
          wvarResult = wvarResult + "<IMPUES>" + FormatoDec(Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 94 + 1), 11 ) ) + "</IMPUES>";
          wvarResult = wvarResult + "<PRECIO>" + FormatoDec(Strings.mid(strParseString, (mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 105 + 1), 11) )   + "</PRECIO>";
          wvarResult = wvarResult + "</CON_HIJOS>";
          //Precio s/Hijos
          wvarResult = wvarResult + "<SIN_HIJOS>";
          wvarResult = wvarResult + "<PRIMA>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 55 + 1), 11) ) + "</PRIMA>";
          wvarResult = wvarResult + "<RECARGOS>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 66 + 1), 11) ) + "</RECARGOS>";
          wvarResult = wvarResult + "<IVAIMPOR>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 77 + 1), 11 )) + "</IVAIMPOR>";
          wvarResult = wvarResult + "<IVAIMPOA>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 88 + 1), 11))  + "</IVAIMPOA>";
          wvarResult = wvarResult + "<IVARETEN>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 99 + 1), 11) ) + "</IVARETEN>";
          wvarResult = wvarResult + "<DEREMI>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 110 + 1), 11 )) + "</DEREMI>";
          wvarResult = wvarResult + "<SELLADO>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 121 + 1), 11 )) + "</SELLADO>";
          wvarResult = wvarResult + "<INGBRU>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 132 + 1), 11) ) + "</INGBRU>";
          wvarResult = wvarResult + "<IMPUES>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 143 + 1), 11)  ) + "</IMPUES>";
          wvarResult = wvarResult + "<PRECIO>" + FormatoDec(Strings.mid(strParseString, (mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 154 + 1), 11 )) + "</PRECIO>";
          wvarResult = wvarResult + "</SIN_HIJOS>";
          wvarResult = wvarResult + "</PLAN>";
          //
          wvarContadorPlan = wvarContadorPlan + 1;
          //
          //Seleccionar el próximo plan (si es cero cortar el ciclo)
          wvarAuxPlanCicle = Strings.trim( Strings.mid( strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 0 + 1, 5 ) );
        }
        wvarResult = wvarResult + "</PLANES>";
        //~~~~~~~~~~
        //Fin Planes
        //~~~~~~~~~~
        //
        //~~~~~~~~~~~~~~~~~
        //Inicio Coberturas
        //~~~~~~~~~~~~~~~~~
        wvarStep = 220;
        //Coberturas solo si le pasa un plan
        if( !wvarDatosPlan.equals( "00000" ) )
        {
          //Coberturas
          //wvarResult = wvarResult & "<COBERTURAS>"
          //
          wvarAuxCobeCicle = "1";
          wvarContadorCobs = 0;
          //Todos las coberturas -- Código comentado de VBasic
          //Val(wvarAuxCobeCicle) > 0 And
          while( wvarContadorCobs < 30 )
          {
            //Cobertura
            //wvarResult = wvarResult & "<COBERTURA>"
            wvarResult = wvarResult + "<COBERCOD" + (wvarContadorCobs + 1) + ">" + Strings.trim( Strings.mid( strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 0 + 1), 3 ) ) + "</COBERCOD" + (wvarContadorCobs + 1) + ">";
            wvarResult = wvarResult + "<COBERORD" + (wvarContadorCobs + 1) + ">" + Strings.trim( Strings.mid( strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 3 + 1), 2 ) ) + "</COBERORD" + (wvarContadorCobs + 1) + ">";
            wvarResult = wvarResult + "<CAPITASG" + (wvarContadorCobs + 1) + ">" + FormatoDec(Strings.mid(strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 5 + 1), 15 ) ) + "</CAPITASG" + (wvarContadorCobs + 1) + ">";
            wvarResult = wvarResult + "<CAPITIMP" + (wvarContadorCobs + 1) + ">" + FormatoDec(Strings.mid(strParseString, (mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 20 + 1), 15)  ) + "</CAPITIMP" + (wvarContadorCobs + 1) + ">";
            //wvarResult = wvarResult & "</COBERTURA>"
            //
            wvarContadorCobs = wvarContadorCobs + 1;
            //
            //Seleccionar la próxima cobertura (si es cero cortar el ciclo)
            wvarAuxCobeCicle = Strings.trim( Strings.mid( strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 0 + 1, 3 ) );
          }
          //wvarResult = wvarResult & "</COBERTURAS>"
        }
        //~~~~~~~~~~~~~~
        //Fin Coberturas
        //~~~~~~~~~~~~~~
        //
        wvarStep = 230;
        Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"El servicio de consulta no se encuentra disponible\" />" + wvarResult + "</Response>" );
      }
      //~~~~~~~~~~~~~~~~~~~~~~~~
      //Fin Response del Mensaje
      //~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 240;
      IAction_Execute = 0;
      return IAction_Execute;
    }
    catch (ComponentExecutionException e) {
    	//La propagamos, ya contiene el detalle del error
		throw e;
	}
	catch( Exception _e_ )
	{
		logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
    	Err.set( _e_ );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        Err.clear();
        throw new ComponentExecutionException(_e_);
      }
  }

  private String Formateo_Telefono( String pvarTelefono ) throws Exception
  {
    String Formateo_Telefono = "";
    String wvarNewTelef = "";
    //
    if( Strings.trim( Strings.left( pvarTelefono, 4 ) ).equals( "0800" ) )
    {
      wvarNewTelef = Strings.trim( Strings.left( pvarTelefono, 4 ) ) + "-" + Strings.mid( pvarTelefono, 5, 3 ) + "-" + Strings.mid( pvarTelefono, 8, Strings.len( pvarTelefono ) );
    }
    else
    {
      wvarNewTelef = pvarTelefono;
    }
    //
    Formateo_Telefono = wvarNewTelef;
    return Formateo_Telefono;
  }

  private String FormatoDec( String pvarNumero ) throws Exception
  {
    String FormatoDec = "";
//    Antes: FormatoDec = VBFixesUtil.val( Strings.left( pvarNumero, (Strings.len( pvarNumero ) - 2) ) ) + "," + Strings.right( pvarNumero, 2 );
    Integer parteEntera = new Integer(Strings.left( pvarNumero, (Strings.len( pvarNumero ) - 2) ) );
    FormatoDec = parteEntera + "," + Strings.right( pvarNumero, 2 );
    return FormatoDec;
  }

  public void Activate() throws Exception {
  }

  public boolean CanBePooled() throws Exception {
    boolean ObjectControl_CanBePooled = false;
    ObjectControl_CanBePooled = true;
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception {
  }
}
