package com.qbe.services.ovmqcotizar.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * Invoca al mensaje 1006 de AIS
 * 
 * @author ramiro
 *
 */
public class lbaw_OVValidaNumDoc implements VBObjectClass
{
	  protected static Logger logger = Logger.getLogger( lbaw_OVValidaNumDoc.class.getName());

  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVValidaNumDoc";
  static final String mcteOpID = "1006";

  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_DocumTip = "//DOCUMTIP";
  static final String mcteParam_DocumNro = "//DOCUMNRO";

  private EventLog mobjEventLog = new EventLog();

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarDocumTip = "";
    String wvarDocumNum = "";
    String strParseString = "";

    try 
    {

      wvarStep = 10;

      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      wvarDocumTip = Strings.right( "00" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DocumTip )  ), 2 );
      wvarDocumNum = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_DocumNro )  ) + Strings.space( 11 ), 11 );


      wvarStep = 60;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );


      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarDocumTip + wvarDocumNum;

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();


      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }

      wvarStep = 190;
      if( Strings.mid( strParseString, 19, 2 ).equals( "OK" ) )
      {
        //
        wvarStep = 230;
        Response.set( "<Response><Estado resultado='true' mensaje='' /></Response>" );
        //
      }
      else
      {
        //
        wvarStep = 240;
        Response.set( "<Response><Estado resultado='false' mensaje='Los datos ingresados no son correctos.' /></Response>" );
      }
      //
      wvarStep = 250;
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception _e_ ) {
    	logger.log(Level.SEVERE, this.getClass().getName(), _e_);
        throw new ComponentExecutionException(_e_);
    }
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
  }
}
