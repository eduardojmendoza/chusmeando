package com.qbe.services.ovmqcotizar.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.Variant;

/**
 * Invoca al mensaje 1008 de AIS
 * 
 * @author ramiro
 *
 */
public class lbaw_OVValidaCuentas implements VBObjectClass
{

	  protected static Logger logger = Logger.getLogger( lbaw_OVValidaCuentas.class.getName());

  static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVValidaCuentas";
  static final String mcteOpID = "1008";

  static final String mcteParam_Usuario = "//USUARIO";
  static final String mcteParam_BancoCod = "//BCOCOD";
  static final String mcteParam_SucurCod = "//SUCURCOD";
  static final String mcteParam_CobroTip = "//COBROTIP";
  static final String mcteParam_CuentNum = "//CTANUM";
  static final String mcteParam_VenciAnn = "//VENANN";
  static final String mcteParam_VenciMes = "//VENMES";

  private EventLog mobjEventLog = new EventLog();

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;

    XmlDomExtended wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCiaAsCod = "";
    String wvarUsuario = "";
    String wvarBancoCod = "";
    String wvarSucurCod = "";
    String wvarCobroTip = "";
    String wvarCuentNum = "";
    String wvarVenciAnn = "";
    String wvarVenciMes = "";
    String strParseString = "";
    String wvarError = "";

    try 
    {

      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );

      wvarUsuario = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Usuario )  ) + Strings.space( 10 ), 10 );
      if( wobjXMLRequest.selectNodes( mcteParam_BancoCod ) .getLength() == 0 )
      {
        wvarBancoCod = "0000";
      }
      else
      {
        wvarBancoCod = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_BancoCod )  ), 4 );
      }
      if( wobjXMLRequest.selectNodes( mcteParam_SucurCod ) .getLength() == 0 )
      {
        wvarSucurCod = "0000";
      }
      else
      {
        wvarSucurCod = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_SucurCod )  ), 4 );
      }
      wvarCobroTip = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CobroTip )  ) + Strings.space( 2 ), 2 );
      wvarCuentNum = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CuentNum )  ) + Strings.space( 20 ), 20 );
      wvarVenciAnn = Strings.right( Strings.fill( 4, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VenciAnn )  ), 4 );
      wvarVenciMes = Strings.right( Strings.fill( 2, "0" ) + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_VenciMes )  ), 2 );

      wvarStep = 20;
      wobjXMLRequest = null;

      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarCiaAsCod = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD )  );

      wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000" + wvarBancoCod + wvarSucurCod + wvarCobroTip + wvarCuentNum + wvarVenciAnn + wvarVenciMes;

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, "", wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }

      if( Strings.mid( strParseString, 19, 2 ).equals( "OK" ) )
      {

        Response.set( "<Response><Estado resultado='true' mensaje='' /></Response>" );

      }
      else
      {
        wvarError = Strings.mid( strParseString, 21, 2 );
        
        if( wvarError.equals( "01" ) )
        {
          Response.set( "<Response><Estado resultado='false' mensaje='Los datos ingresados del banco no son correctos.' /></Response>" );
        }
        else if( wvarError.equals( "02" ) )
        {
          Response.set( "<Response><Estado resultado='false' mensaje='Los datos ingresados de la sucusal del banco no son correctos.' /></Response>" );
        }
        else if( wvarError.equals( "03" ) )
        {
          Response.set( "<Response><Estado resultado='false' mensaje='Los datos ingresados de la tarjeta de crédito no son correctos.' /></Response>" );
        }
        else if( wvarError.equals( "05" ) )
        {
          Response.set( "<Response><Estado resultado='false' mensaje='La fecha de vencimiento de la tarjeta de crédito no es válida.' /></Response>" );
        }
        else
        {
          Response.set( "<Response><Estado resultado='false' mensaje='Alguno de los datos no son los correctos.' /></Response>" );
        }
      }
      IAction_Execute = 0;
      return IAction_Execute;
    } catch( Exception _e_ ) {
    	logger.log(Level.SEVERE, this.getClass().getName(), _e_);
        throw new ComponentExecutionException(_e_);
    }
  }

  public void Activate() throws Exception
  {

  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {

  }
}
