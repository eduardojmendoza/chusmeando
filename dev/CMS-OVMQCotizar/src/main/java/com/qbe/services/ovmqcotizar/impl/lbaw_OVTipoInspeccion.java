package com.qbe.services.ovmqcotizar.impl;

import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

/**
 * Objetos del FrameWork
 */

public class lbaw_OVTipoInspeccion implements VBObjectClass {
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVTipoInspeccion";
	static final String mcteOpID = "1511";

	protected static Logger logger = Logger.getLogger(lbaw_OVTipoInspeccion.class.getName());

	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_Usuario = "//USUARIO";
	static final String mcteParam_EsCero = "//ESCERO";
	static final String mcteParam_Rastreo = "//RASTREO";
	static final String mcteParam_EstDispo = "//ESTDISPO";
	static final String mcteParam_Agentcod = "//AGENTCOD";
	static final String mcteParam_AgentCla = "//AGENTCLA";
	static final String mcteParam_Provi = "//PROVI";
	static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
	private Object mobjCOM_Context = null;
	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLConfig = null;
		XmlDomExtended wobjXMLParametros = null;
		XmlDomExtended wobjXMLResponse = null;
		XmlDomExtended wobjXSLResponse = null;
		Object wobjClass = null;
		int wvarMQError = 0;
		String wvarArea = "";
		MQProxy wobjFrame2MQ = null;
		String wvarResponse = "";
		int wvarStep = 0;
		String wvarResult = "";
		String wvarCiaAsCod = "";
		String wvarUsuario = "";
		String wvarEsCero = "";
		String wvarRastreo = "";
		String wvarEstDispo = "";
		String wvarAgentcod = "";
		String wvarAgentCla = "";
		String wvarProvi = "";
		String wvarLocalidadCod = "";
		String wvarRamopcod = "";
		String strParseString = "";
		String wvarError = "";
		Variant wvarPos = new Variant();
		String wvarEstado = "";
		String wvarAleatorio = "";
		//
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			// Levanto los par�metros que llegan desde la p�gina
			wobjXMLRequest = new XmlDomExtended();
			wobjXMLRequest.loadXML(Request);
			// Deber� venir desde la p�gina
			wvarUsuario = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Usuario)) + Strings.space(10), 10);
			wvarEsCero = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_EsCero));
			wvarRastreo = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Rastreo));
			wvarEstDispo = Strings
					.right("0000" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_EstDispo)), 4);
			wvarAgentcod = Strings
					.right("0000" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Agentcod)), 4);
			if (wobjXMLRequest.selectNodes(mcteParam_AgentCla).getLength() == 0) {
				wvarAgentCla = "PR";
			} else {
				wvarAgentCla = Strings.left(
						XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AgentCla)) + Strings.space(2),
						2);
			}
			wvarProvi = Strings.right("00" + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Provi)),
					2);
			wvarLocalidadCod = XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_LocalidadCod));
			//
			//
			wvarStep = 20;
			// Set wobjXMLRequest = Nothing
			//
			wvarStep = 30;
			// Levanto los datos de la cola de MQ del archivo de configuraci�n
			wobjXMLConfig = new XmlDomExtended();
			wobjXMLConfig.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
			//
			// Levanto los par�metros generales (ParametrosMQ.xml)
			wvarStep = 60;
			wobjXMLParametros = new XmlDomExtended();
			wobjXMLParametros.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
			wvarCiaAsCod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosGenerales + ModGeneral.gcteCIAASCOD));
			wvarRamopcod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosAutoScoring + ModGeneral.gcteRAMOPCOD));
			//
			wvarStep = 70;
			wobjXMLParametros = null;
			//
			wvarStep = 80;
			wvarArea = mcteOpID + wvarCiaAsCod + wvarUsuario + "    000000000    000000000  000000000  000000000"
					+ wvarRamopcod + wvarEsCero + wvarRastreo + wvarEstDispo + wvarAgentcod + wvarAgentCla + wvarProvi
					+ wvarLocalidadCod;
			XmlDomExtended
					.setText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL"),
							String.valueOf(VB
									.val(XmlDomExtended
											.getText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL")))
									* 40));

			wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
			StringHolder strParseStringHolder = new StringHolder(strParseString);
			wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
			strParseString = strParseStringHolder.getValue();

			//
			wvarStep = 150;
			if (wvarMQError != 0) {
				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible" + String.valueOf((char) (34)) + " />"
						+ "Codigo Error:" + wvarMQError + "</Response>");
				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
						mcteClassName + "--" + mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - "
								+ strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(),
						vbLogEventTypeError);
				return IAction_Execute;
			}
			//
			//
			wvarStep = 180;
			//
			// cantidad de caracteres ocupados por par�metros de entrada
			wvarPos.set(89);
			//
			wvarStep = 190;
			if (Strings.mid(strParseString, 19, 2).equals("ER")) {
				//
				wvarStep = 200;
				Response.set(
						"<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>");
				//
			} else {
				//
				wvarStep = 210;
				wvarResult = "";
				wvarEstado = Strings.mid(strParseString, wvarPos.toInt(), 1);
				//
				if (wvarEstado.equals("O")) {
					// Corro el SP de Aleatorio
					wvarStep = 220;

					// Le pasa el request que viene, total OVAleatorio lo ignora
					// y llama a un SP que no lleva parámetros
					lbaw_OVAleatorio comp = new lbaw_OVAleatorio();
					StringHolder shAleatorio = new StringHolder(wvarResponse);
					comp.IAction_Execute(Request, shAleatorio, "");
					wvarResponse = shAleatorio.getValue();

					//
					wvarStep = 20;
					wobjXMLParametros = new XmlDomExtended();
					wobjXMLParametros.loadXML(wvarResponse);

					wvarAleatorio = XmlDomExtended.getText(wobjXMLParametros.selectSingleNode("//ESTADO"));
					//
					wobjXMLParametros = null;
					//
				}
				//
				wvarStep = 240;
				if ((wvarEstado.equals("I")) || (wvarEstado.equals("O"))) {
					wvarResult = wvarResult + ParseoMensajeEstado(wvarPos, strParseString);
					//
					wvarStep = 250;
					wobjXMLResponse = new XmlDomExtended();
					wobjXSLResponse = new XmlDomExtended();
					//
					wvarStep = 260;
					wobjXMLResponse.loadXML(wvarResult);
					//
					if (wobjXMLResponse.selectNodes("//R").getLength() != 0) {
						wvarStep = 270;
						wobjXSLResponse.loadXML(p_GetXSLEstados());
						//
						wvarStep = 280;
						wvarResult = wobjXMLResponse.transformNode(wobjXSLResponse).toString()
								.replaceAll("<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "");
						//
						wvarStep = 290;
						wobjXMLResponse = null;
						wobjXSLResponse = null;
						//
					}
					//
				}
				//
				wvarStep = 300;
				wvarEstado = "<RTA>" + wvarEstado + "</RTA>";
				wvarAleatorio = "<ALEATORIO>" + wvarAleatorio + "</ALEATORIO>";
				Response.set("<Response><Estado resultado='true' mensaje='' />" + wvarEstado + wvarAleatorio + "<COMBO>"
						+ wvarResult + "</COMBO>" + "</Response>");
			}
			//
			wvarStep = 310;
			IAction_Execute = 0;

			return IAction_Execute;
			//
			// ~~~~~~~~~~~~~~~
		} catch (ComponentExecutionException e) {
			// La propagamos, ya contiene el detalle del error
			throw e;
		} catch (Exception _e_) {
			logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
			Err.set(_e_);
			mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
					wcteFnName, wvarStep,
					Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - "
							+ Err.getError().getDescription() + " Mensaje:" + "" + " Hora:" + DateTime.now(),
					vbLogEventTypeError);
			Err.clear();
			throw new ComponentExecutionException(_e_);
		}
	}

	private String p_GetXSLEstados() throws Exception {
		String p_GetXSLEstados = "";
		String wvarStrXSL = "";
		//
		wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
		wvarStrXSL = wvarStrXSL
				+ "<xsl:decimal-format name=\"european\" decimal-separator=\",\" grouping-separator=\".\"/>";
		wvarStrXSL = wvarStrXSL + "     <xsl:template match='/RS/R'>";
		wvarStrXSL = wvarStrXSL + "         <xsl:element name='OPTION'>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='value'>";
		wvarStrXSL = wvarStrXSL
				+ "                 <xsl:value-of select='format-number(number(substring(.,1,2)), \"0\")' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:attribute name='flag'>";
		wvarStrXSL = wvarStrXSL + "                 <xsl:value-of select='normalize-space(substring(.,33,1))' />";
		wvarStrXSL = wvarStrXSL + "             </xsl:attribute>";
		wvarStrXSL = wvarStrXSL + "             <xsl:value-of select='normalize-space(substring(.,3,30))' />";
		wvarStrXSL = wvarStrXSL + "         </xsl:element>";
		wvarStrXSL = wvarStrXSL + "     </xsl:template>";
		wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
		//
		p_GetXSLEstados = wvarStrXSL;
		return p_GetXSLEstados;
	}

	private String ParseoMensajeEstado(Variant wvarPos, String strParseString) throws Exception {
		String ParseoMensajeEstado = "";
		String wvarResult = "";
		int wvarCant = 0;
		int i = 0;
		//
		wvarResult = wvarResult + "<RS>";
		//
		wvarPos.set(wvarPos.add(new Variant(1)));
		if ((!Strings.trim(Strings.mid(strParseString, wvarPos.toInt(), 3)).equals("000"))
				&& (!Strings.trim(Strings.mid(strParseString, wvarPos.toInt(), 3)).equals(""))) {
			wvarCant = Obj.toInt(Strings.trim(Strings.mid(strParseString, wvarPos.toInt(), 3)));
			wvarPos.set(wvarPos.add(new Variant(3)));
			for (i = 1; i <= wvarCant; i++) {
				wvarResult = wvarResult + "<R><![CDATA[" + Strings.mid(strParseString, wvarPos.toInt(), 33) + "]]></R>";
				wvarPos.set(wvarPos.add(new Variant(33)));
			}
		}
		//
		wvarResult = wvarResult + "</RS>";
		//
		ParseoMensajeEstado = wvarResult;
		//
		return ParseoMensajeEstado;
	}

	public void Activate() throws Exception {
		//
		/* TBD mobjCOM_Context = this.GetObjectContext() ; */
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
		mobjCOM_Context = (Object) null;
		mobjEventLog = null;
		//
	}
}
