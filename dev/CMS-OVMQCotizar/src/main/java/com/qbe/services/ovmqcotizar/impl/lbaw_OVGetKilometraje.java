package com.qbe.services.ovmqcotizar.impl;

import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;

/**
 * Objetos del FrameWork
 */

public class lbaw_OVGetKilometraje implements VBObjectClass {
	/**
	 * Implementacion de los objetos Datos de la accion
	 */
	static final String mcteClassName = "lbawA_OVMQCotizar.lbaw_OVGetKilometraje";
	static final String mcteOpID = "0058";

	protected static Logger logger = Logger.getLogger(lbaw_OVGetKilometraje.class.getName());

	/**
	 * Parametros XML de Entrada
	 */
	static final String mcteParam_Ramopcod = "//RAMOPCOD";
	static final String mcteParam_Campacod = "//CAMPACOD";
	static final String mcteParam_Agentcod = "//AGENTCOD";
	static final String mcteParam_AgentCla = "//AGENTCLA";
	static final String mcteParam_Codizona = "//CODIZONA";
	static final String mcteParam_Fecinici = "//FECINICI";
	static final String mcteParam_Fecinvig = "//FECINVIG";
	private Object mobjCOM_Context = null;
	private EventLog mobjEventLog = new EventLog();
	/**
	 * static variable for method: IAction_Execute
	 */
	private final String wcteFnName = "IAction_Execute";

	@Override
	public int IAction_Execute(String Request, StringHolder Response, String ContextInfo) {
		int IAction_Execute = 0;
		Variant vbLogEventTypeError = new Variant();
		XmlDomExtended wobjXMLRequest = null;
		XmlDomExtended wobjXMLConfig = null;
		XmlDomExtended wobjXMLParametros = null;
		int wvarMQError = 0;
		String wvarArea = "";
		MQProxy wobjFrame2MQ = null;
		int wvarStep = 0;
		String wvarResult = "";
		String wvarRamopcod = "";
		String wvarCampacod = "";
		String wvarAgentcod = "";
		String wvarAgentCla = "";
		String wvarCodiZona = "";
		String wvarFecInici = "";
		String wvarFecInVig = "";
		String strParseString = "";
		String wvarError = "";
		//
		//
		//
		//
		//
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
		try {
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//
			wvarStep = 10;
			// Levanto los par�metros que llegan desde la p�gina
			wobjXMLRequest = new XmlDomExtended();
			wobjXMLRequest.loadXML(Request);
			// Deber� venir desde la p�gina
			wvarCampacod = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Campacod)) + Strings.space(4), 4);
			wvarAgentcod = Strings.right(
					Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Agentcod)),
					4);
			wvarAgentCla = Strings.left(
					XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_AgentCla)) + Strings.space(2), 2);
			wvarCodiZona = Strings.right(
					Strings.fill(4, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Codizona)),
					4);
			wvarFecInici = Strings.right(
					Strings.fill(8, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Fecinici)),
					8);
			wvarFecInVig = Strings.right(
					Strings.fill(8, "0") + XmlDomExtended.getText(wobjXMLRequest.selectSingleNode(mcteParam_Fecinvig)),
					8);
			//
			//
			wvarStep = 20;
			wobjXMLRequest = null;
			//
			wvarStep = 30;
			// Levanto los datos de la cola de MQ del archivo de configuraci�n
			wobjXMLConfig = new XmlDomExtended();
			wobjXMLConfig.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
			//
			// Levanto los par�metros generales (ParametrosMQ.xml)
			wvarStep = 60;
			wobjXMLParametros = new XmlDomExtended();
			wobjXMLParametros.load(
					Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
			wvarRamopcod = XmlDomExtended.getText(
					wobjXMLParametros.selectSingleNode(ModGeneral.gcteNodosAutoScoring + ModGeneral.gcteRAMOPCOD));
			//
			wvarStep = 70;
			wobjXMLParametros = null;
			//
			wvarStep = 80;
			wvarArea = mcteOpID + wvarRamopcod + wvarCampacod + wvarAgentcod + wvarAgentCla + wvarCodiZona
					+ wvarFecInici + wvarFecInVig;
			XmlDomExtended
					.setText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL"),
							String.valueOf(VB
									.val(XmlDomExtended
											.getText(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL")))
									* 40));

			wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
			StringHolder strParseStringHolder = new StringHolder(strParseString);
			wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
			strParseString = strParseStringHolder.getValue();

			//
			wvarStep = 150;
			if (wvarMQError != 0) {
				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "El servicio de consulta no se encuentra disponible" + String.valueOf((char) (34)) + " />"
						+ "Codigo Error:" + wvarMQError + "</Response>");
				mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"),
						mcteClassName + "--" + mcteClassName,
						wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - "
								+ strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(),
						vbLogEventTypeError);
				return IAction_Execute;
			}
			//
			//
			wvarStep = 180;

			if (!Strings.trim(strParseString).equals("")) {
				wvarStep = 190;
				//
				wvarResult = ParseoMensaje(strParseString, 35);
				//
				wvarStep = 200;
				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "true"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ String.valueOf((char) (34)) + " />" + wvarResult + "</Response>");

			} else {
				Response.set("<Response><Estado resultado=" + String.valueOf((char) (34)) + "false"
						+ String.valueOf((char) (34)) + " mensaje=" + String.valueOf((char) (34))
						+ "NO SE ENCONTRARON DATOS." + String.valueOf((char) (34)) + " /></Response>");
			}
			//
			wvarStep = 220;
			//
			wvarStep = 230;
			/* TBD mobjCOM_Context.SetComplete() ; */
			IAction_Execute = 0;
			//
			// ~~~~~~~~~~~~~~~
			ClearObjects:
			// ~~~~~~~~~~~~~~~
			// LIBERO LOS OBJETOS
			wobjXMLConfig = null;
			return IAction_Execute;
		} catch (ComponentExecutionException e) {
			// La propagamos, ya contiene el detalle del error
			throw e;
		} catch (Exception _e_) {
			logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
			Err.set(_e_);
			mobjEventLog.Log(ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName,
					wcteFnName, wvarStep,
					Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - "
							+ Err.getError().getDescription() + " Mensaje:" + "" + " Hora:" + DateTime.now(),
					vbLogEventTypeError);
			Err.clear();
			throw new ComponentExecutionException(_e_);
		}
	}

	private void ObjectControl_Activate() throws Exception {
	}

	private boolean ObjectControl_CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		return ObjectControl_CanBePooled;
	}

	private void ObjectControl_Deactivate() throws Exception {
	}

	private String ParseoMensaje(String strParseString, int plPos) throws Exception {
		String ParseoMensaje = "";
		String wvarResult = "";
		int i = 0;
		String wvarDesde = "";
		String wvarHasta = "";

		//
		if (!Strings.trim(strParseString).equals("")) {
			for (i = 1; i <= 50; i++) {
				wvarDesde = Strings.right(
						Strings.space(7) + String.valueOf(Obj.toInt("0" + Strings.mid(strParseString, plPos, 7))), 7);
				if (!Strings.trim(wvarDesde).equals("0")) {
					wvarHasta = Strings.right(Strings.space(7)
							+ String.valueOf(Obj.toInt("0" + Strings.mid(strParseString, plPos + 7, 7))), 7);
					wvarResult = wvarResult + "<OPTION value='" + Strings.trim(wvarHasta) + "' >" + wvarDesde + " - "
							+ wvarHasta + "</OPTION>";
				}
				plPos = plPos + 14;
			}

		}

		//
		ParseoMensaje = wvarResult;
		//
		return ParseoMensaje;
	}

	public void Activate() throws Exception {
		//
		/* TBD mobjCOM_Context = this.GetObjectContext() ; */
		//
	}

	public boolean CanBePooled() throws Exception {
		boolean ObjectControl_CanBePooled = false;
		//
		ObjectControl_CanBePooled = true;
		//
		return ObjectControl_CanBePooled;
	}

	public void Deactivate() throws Exception {
		//
		mobjCOM_Context = (Object) null;
		mobjEventLog = null;
		//
	}
}
