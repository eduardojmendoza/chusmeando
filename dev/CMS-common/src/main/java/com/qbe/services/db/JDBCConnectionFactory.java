package com.qbe.services.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import com.mchange.v2.c3p0.PooledDataSource;

/**
 * Factory para java.sql.Connection a partir de nombres de UDL que se 
 * usan en el código migrado
 * 
 * @author ramiro
 *
 */
public class JDBCConnectionFactory {

	protected static ApplicationContext context; 

	private static Logger logger = Logger.getLogger(JDBCConnectionFactory.class.getName());

	protected static boolean driverLoaded = false;
	
	static final String URL_DB ="jtds.url";
	static final String USER_DB="jtds.user";
	static final String PASS_DB="jtds.pass";
	
	private static String propsFilename = System.getProperty("cms.config.properties");

	public JDBCConnectionFactory() {
		loadDrivers();
	}
	
	protected synchronized void loadDrivers() {
		if (!driverLoaded) {
			String dbDriver = "TBD";
		    dbDriver = "org.hsqldb.jdbcDriver";
			logger.log(Level.INFO, "Loading driver: " + dbDriver );
		    try {
				Class.forName(dbDriver);
			} catch (ClassNotFoundException e1) {
			    logger.log(Level.SEVERE, "org.hsqldb.jdbcDriver: ", e1);
			}
		    
			try {

			    Class.forName("net.sourceforge.jtds.jdbc.Driver");

			    Enumeration<Driver> enu = DriverManager.getDrivers();
			    while ( enu.hasMoreElements()) {
			    	Driver d = enu.nextElement();
				    logger.log(Level.INFO, "DRIVER: " + d.getClass().getName() );
			    }
	
//				Connection conn = DriverManager.getConnection("jdbc:jtds:sqlserver://ARD808VWNCDB/SNCV", "SIS_SNCV1", "RHVC7673");
			    Connection conn = DriverManager.getConnection(loadValueOfConfig(URL_DB), loadValueOfConfig(USER_DB), loadValueOfConfig(PASS_DB));
				DatabaseMetaData dbm = conn.getMetaData();
	            ResultSet rs = dbm.getTables(null, null, "%", new String[] { "TABLE" });
	            while (rs.next()) { 
	            	logger.log(Level.INFO, "JTDS table check: " + rs.getString("TABLE_NAME")); 
	            }		

			    driverLoaded = true;
			} catch (SQLException e) {
				logger.log(Level.SEVERE, "ERROR EN JTDS ", e);
			} catch (ClassNotFoundException e) {
				logger.log(Level.SEVERE, "ERROR EN JTDS ", e);
			} catch (IOException e) {
				logger.log(Level.SEVERE, "ERROR AL CARGAR PARAMETROS ", e);
			}
		}
	}

	protected synchronized static ApplicationContext getContext() {
		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:db-beans.xml");
		}
		return context;
	}


	/**
	 * Crea y retorna una nueva java.sql.Connection que apunta a la base definida por gctedb.
	 * 
	 * @param udlName hace referencia a un nombre UDL, como en lbawA_OfVirtualLBA.udl
	 * @return
	 * @throws SQLException 
	 * @throws Exception 
	 */
	public java.sql.Connection getJDBCConnection(String udlName) throws SQLException {
		logger.log(Level.FINE, String.format("Devolviendo connection para : %s", udlName));
		DataSource ds = getDataSourceForUDLName(udlName);
		logger.log(Level.FINE, String.format("Datasource : %s", ds));

		try {
			return ds.getConnection();
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "SQLException al sacar una conexión para udlName: " + udlName, e);
			throw e;
		}
	}

	public static DataSource getDataSourceForUDLName(String gctedb) {
		return (DataSource)getContext().getBean(gctedb + "DS");	
	}

	public static int getDataSourceBusyConnections(String udl) {
		return getDataSourceBusyConnections(getDataSourceForUDLName(udl));
	}

	public static int getDataSourceBusyConnections(DataSource ds) {
		// make sure it's a c3p0 PooledDataSource 
		try {
			if ( ds instanceof PooledDataSource) { 
				PooledDataSource pds = (PooledDataSource) ds; 
				return pds.getNumBusyConnectionsDefaultUser(); 
			} else {
				logger.log(Level.WARNING, String.format("No es un c3p0 Datasource : %s", ds));
				return -1; 		
			}
		} catch (SQLException e) {
			logger.log(Level.WARNING, "Al intentar obtener datos del pool", e);
			return -1;
		}
	}

	public static void logDataSourceState(String udl) {
		logger.log(Level.INFO, MessageFormat.format("DataSource state para UDL: {0}",udl));
		logDataSourceState(getDataSourceForUDLName(udl));
	}
	
	public static void logDataSourceState(DataSource ds) {
		try {
			if ( ds instanceof PooledDataSource) { 
				PooledDataSource pds = (PooledDataSource) ds;
				logger.log(Level.INFO, MessageFormat.format("DataSource state: [name={0}; connections={1}; busy={2}; idle={3}]",
						pds.getDataSourceName(), pds.getNumConnectionsDefaultUser(), pds.getNumBusyConnectionsDefaultUser(),pds.getNumIdleConnectionsDefaultUser()));
			} else {
				logger.log(Level.SEVERE, "No es un pool c3p0(?)");		
			}
		} catch (SQLException e) {
			logger.log(Level.SEVERE, "SQLException al querer imprimir data del DS " + ds );		
		}
		
	}
	
	/**
	 * Método que retorna el valor de la key que esta dentro del archivo de
	 * propiedades
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	private static String loadValueOfConfig(String key) throws IOException {
		Properties fileProper = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
		fileProper.load(in);
		in.close();
		return fileProper.getProperty(key);

	}

}
