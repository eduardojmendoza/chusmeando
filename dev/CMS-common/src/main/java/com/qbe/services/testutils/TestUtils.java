package com.qbe.services.testutils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TestUtils {

	/**
	 * Extrae el valor de mensaje de:
	 * <Response>		<Estado mensaje="" resultado="true"/>
	 * o
	 * <Response><Estado resultado="false" mensaje="El servicio de consulta no se encuentra disponible" />Codigo Error:-1</Response>
	 * 
	 * @param value
	 * @return
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 * @throws TransformerFactoryConfigurationError 
	 * @throws IllegalArgumentException 
	 * @throws TransformerConfigurationException 
	 * @throws XPathExpressionException 
	 */
	public static String parseMensaje(String value) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, TransformerConfigurationException, IllegalArgumentException, TransformerFactoryConfigurationError, TransformerException {
		Document document = loadDocumentFromString(value);
		String xpathExpr = "Response/Estado/@mensaje";
		return getExpressionValue(document, xpathExpr);
	}

	/**
	 * Retorna el valor del nodo que se obtiene al evaluad xpathExpr en el document
	 * 
	 * @param document
	 * @param xpathExpr
	 * @return
	 * @throws XPathExpressionException
	 */
	public static String getExpressionValue(Document document,
			String xpathExpr) throws XPathExpressionException {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = xpath.compile(xpathExpr);
		Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
		return node.getNodeValue();
	}

	/**
	 * Extrae el valor de resultado de:
	 * <Response>		<Estado mensaje="" resultado="true"/>
	 * o
	 * <Response><Estado resultado="false" mensaje="El servicio de consulta no se encuentra disponible" />Codigo Error:-1</Response>
	 * 
	 * @param value
	 * @return
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 * @throws TransformerFactoryConfigurationError 
	 * @throws IllegalArgumentException 
	 * @throws TransformerConfigurationException 
	 * @throws XPathExpressionException 
	 */
	public static boolean parseResultado(String value) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, TransformerConfigurationException, IllegalArgumentException, TransformerFactoryConfigurationError, TransformerException {
		Document document = loadDocumentFromString(value);
		String xpathExpr = "Response/Estado/@resultado";
		String boolString = getExpressionValue(document, xpathExpr);
		return Boolean.parseBoolean(boolString);
	}

	public static String loadRequest(String msgCode) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerException {
		Document document = loadLogDocument(msgCode);
		String xpathExpr = "LOGS/LOG/Request";
		return getXmlString(document, xpathExpr);
	}

	public static String loadResponse(String msgCode) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerException {
		Document document = loadLogDocument(msgCode);
		String xpathExpr = "LOGS/LOG/Response";
		return getXmlString(document, xpathExpr);
	}
	
	public static String loadActionCode(String msgCode) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerException {
		Document document = loadLogDocument(msgCode);
		String xpathExpr = "Logs/actionCode";
		return getNodeTextContent(document, xpathExpr);

		}

	/**
	 * @param document
	 * @param xpathExpr
	 * @return
	 * @throws XPathExpressionException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerConfigurationException
	 * @throws IllegalArgumentException
	 * @throws TransformerException
	 */
	public static String getXmlString(Document document, String xpathExpr)
			throws XPathExpressionException,
			TransformerFactoryConfigurationError,
			TransformerConfigurationException, IllegalArgumentException,
			TransformerException {
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = xpath.compile(xpathExpr);
		Node requestNode = (Node) expr.evaluate(document, XPathConstants.NODE);
	
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
	
		StringWriter stringWriter = new StringWriter();
		StreamResult xmlOutput = new StreamResult(stringWriter);
		transformer.transform(new DOMSource(requestNode),xmlOutput);
		return xmlOutput.getWriter().toString();
	}

	/**
	 * @param msgCode 
	 * @return
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	public static Document loadLogDocument(String msgCode) throws ParserConfigurationException,
			SAXException, IOException {
		InputStream xmlLogStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("Log"+msgCode+".xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(xmlLogStream);
		return document;
	}

	public static Document loadDocumentFromString(String msgCode)
			throws ParserConfigurationException, SAXException, IOException {
		InputSource is = new InputSource(new StringReader(msgCode));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(is);
		return document;
	}
	
	/**
	* Retorna el text content (@see org.w3c.dom.Node.getTextContent() ) del nodo representado por xpathExpr en document.
	* NO INCLUYE el tag del nodo, ni los children. Por ejemplo si pedimos /padre/hijo, retorna el valor del tag <hijo>
	* 
	* @param document
	* @param xpathExpr
	* @return
	* @throws XPathExpressionException
	* @throws TransformerFactoryConfigurationError
	* @throws TransformerConfigurationException
	* @throws IllegalArgumentException
	* @throws TransformerException
	*/

	public static String getNodeTextContent(Document document, String xpathExpr)
		throws XPathExpressionException,
		TransformerFactoryConfigurationError,
		TransformerConfigurationException, IllegalArgumentException,
		TransformerException {
		
	XPathFactory xPathfactory = XPathFactory.newInstance();
	XPath xpath = xPathfactory.newXPath();
	XPathExpression expr = xpath.compile(xpathExpr);
	Node requestNode = (Node) expr.evaluate(document, XPathConstants.NODE);
	return requestNode.getTextContent();
	}
	
	/**
	* Retorna la serialización del nodo representado por xpathExpr en document.
	* INCLUYE el tag del nodo. Por ejemplo si pedimos /padre/hijo, retorna <hijo>...etc
	* 
	* @param document
	* @param xpathExpr
	* @return
	* @throws XPathExpressionException
	* @throws TransformerFactoryConfigurationError
	* @throws TransformerConfigurationException
	* @throws IllegalArgumentException
	* @throws TransformerException
	*/

	public static String getNodeAsXml(Document document, String xpathExpr)
			throws XPathExpressionException,
			TransformerFactoryConfigurationError,
			TransformerConfigurationException, IllegalArgumentException,
			TransformerException {
	
	XPathFactory xPathfactory = XPathFactory.newInstance();
	XPath xpath = xPathfactory.newXPath();
	XPathExpression expr = xpath.compile(xpathExpr);
	Node requestNode = (Node) expr.evaluate(document, XPathConstants.NODE);

	TransformerFactory tf = TransformerFactory.newInstance();
	Transformer transformer = tf.newTransformer();
	transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	StringWriter stringWriter = new StringWriter();
	StreamResult xmlOutput = new StreamResult(stringWriter);
	transformer.transform(new DOMSource(requestNode),xmlOutput);
	return xmlOutput.getWriter().toString();
	}

}
