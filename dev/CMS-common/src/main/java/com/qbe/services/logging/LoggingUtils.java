package com.qbe.services.logging;

import java.util.*;
import java.util.logging.*;
import java.io.File;

/**
 <h3>Debugging Logging Settings</h3>
 * For debugging your logging settings you can use 
 * <pre><code>   System.out.println({@link #currentConfiguration() Logging.currentConfiguration()});
 *</pre></code>which will dump the current logging setting in your log file.<p>
 *
 *@author     Matthew Ford
 *@created    October 9, 2003
 */
public class LoggingUtils {

  /**
   * The version number  1.3 
	 */
	public static final String VERSION = "1.3";
	
	/** The version date  23nd Nov. 2004
	 *
	 */
	public static final String VERSION_DATE = "23nd Nov. 2004";
	
	/**
	 * private constructor as all methods are static
	 */
	private LoggingUtils() {
	}

  /**
   *  Returns the current logging configuration for debugging. <p>
	 *  Loggers created in classes are not visable in the current configuration until their class
	 *  is loaded.  This means the best place to call<br>
   * &nbsp; &nbsp; System.out.println(Logging.currentConfiguration());<br>
	 * is at the end of the application main() or in the exit code after all the classes have
   * been loaded.
   *
   *@return    a string describing the current logging configuration
   */
  public static String currentConfiguration() {
    try {
      StringBuffer rtn = new StringBuffer(1024);
      rtn.append("-- Logging configuration --\n");
			
      String classPath = System.getProperty("java.class.path");
      rtn.append(" The classpath is '"+classPath+"'\n");
			
      String loggingConfigFile = System.getProperty("java.util.logging.config.file");
      if (loggingConfigFile == null) {
        rtn.append(" No java.util.logging.config.file file is set.\n");
      } else {
        rtn.append("java.util.logging.config.file is set to '" + loggingConfigFile + "'\n");
        File loggingFile = new File(loggingConfigFile);
        rtn.append("      '"+loggingFile.getAbsolutePath() + "'\n");
        if (loggingFile.exists() && loggingFile.isFile()) {
          rtn.append("   and this file exists.\n");
        } else {
          rtn.append("   and this file does NOT EXIST.\n");
        }
      }
      System.out.println(rtn);
      
      Handler[] handlers = Logger.getLogger("").getHandlers();
      rtn.append("\n    There are " + handlers.length + " logging handlers :-\n");
      listHandlers(handlers,rtn,"");
      rtn.append("\n    Currently known loggers are:-\n");
      LogManager logManager = LogManager.getLogManager(); 
      Enumeration en = logManager.getLoggerNames();
      for (; en.hasMoreElements(); ) {
        String loggerName = (String)en.nextElement();
        Logger logger = logManager.getLogger(loggerName);
        Filter filter = logger.getFilter();
        
        handlers = logger.getHandlers();
        Logger handlerParent = null;
        if (handlers.length == 0) {
          handlerParent = logger;
          while ((handlerParent != null) && 
               ((handlers = handlerParent.getHandlers()).length==0) ) {
            handlerParent = handlerParent.getParent();
          }
        } 
        
        Level level = logger.getLevel();
        Logger levelParent = null;
        if (level == null) {
          levelParent = logger;
          while ((levelParent != null) && 
               ((level=levelParent.getLevel())== null) ) {
            levelParent = levelParent.getParent();
          }
        }
        
        String resourceBundleName = logger.getResourceBundleName();
        Logger resourceParent = null;
        if (resourceBundleName == null) {
          resourceParent = logger;
          while ((resourceParent != null) && 
               ((resourceBundleName=resourceParent.getResourceBundleName())== null) ) {
            resourceParent = resourceParent.getParent();
          }
        } 
        
        rtn.append("LoggerName:'"+loggerName+"'\n");
        rtn.append("    Level:"+level+"  "
                   +((levelParent==null)?"":"set by parent:'"+levelParent.getName()+"'")+"\n");
        rtn.append("    Filter:"+((filter==null)?"none set":"'"+filter.getClass().getName()+"'")+"\n");
        rtn.append("    ResourceBundleName:"
                   +((resourceBundleName==null)?" none set ":"'"+resourceBundleName+"'  ")
                   +((resourceParent==null)?"":"set by parent:'"+resourceParent.getName()+"'")+"\n");
        rtn.append("    Handlers: "
                   +((handlerParent==null)?"":"set by parent:'"+handlerParent.getName()+"'")+"\n");
        listHandlers(handlers,rtn,"             ");
        rtn.append("\n");
      }
      rtn.append("NOTE: More loggers may be added as classes are loaded by the JVM.\n");
      return rtn.toString();
    } catch (Throwable t) {
    	t.printStackTrace();
      return "Error getting current logging configuration\n"+ t;
    } 
  }

  
  private static StringBuffer listHandlers(Handler[] handlers, StringBuffer buffer, String indent) {
    if (indent == null) {
      indent = "";
    }
    for (int index = 0; index < handlers.length; index++) {
      buffer.append(indent+"Handler:" + handlers[index].getClass().getName() +
          " Level:" + handlers[index].getLevel().toString() +
          " Formatter:" + handlers[index].getFormatter().getClass().getName() + "\n");
    }
    return buffer;
  }

}

