package com.qbe.services.fileutils;

public class UnsupportedProtocolException extends Exception {

	public UnsupportedProtocolException() {
	}

	public UnsupportedProtocolException(String message) {
		super(message);
	}

	public UnsupportedProtocolException(Throwable cause) {
		super(cause);
	}

	public UnsupportedProtocolException(String message, Throwable cause) {
		super(message, cause);
	}

}
