package com.qbe.services.common.merge;

public class MergeException extends Exception {

	private static final long serialVersionUID = 9077263418985037386L;

	public MergeException() {
	}

	public MergeException(String message) {
		super(message);
	}

	public MergeException(Throwable cause) {
		super(cause);
	}

	public MergeException(String message, Throwable cause) {
		super(message, cause);
	}

}
