package com.qbe.services.mqgeneric.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import diamondedge.util.Variant;

public class EventLog {
	
	protected static Logger logger = Logger.getLogger(EventLog.class.getName());

	public void Log(String string, String string2, String wcteFnName,
			int wvarStep, int number, String string22,
			Variant vbLogEventTypeError) {
	StringBuffer msg = new StringBuffer();
	msg.append("[");
	msg.append("[msg:");
	msg.append(string);
	msg.append("],");
	msg.append("[msg2:");
	msg.append(string2);
	msg.append("],");
	msg.append("[wcteFnName:");
	msg.append(wcteFnName);
	msg.append("],");
	msg.append("[wvarStep:");
	msg.append(wvarStep);
	msg.append("],");
	msg.append("[number:");
	msg.append(number);
	msg.append("],");
	msg.append("[string22:");
	msg.append(string22);
	msg.append("],");
	msg.append("[vbLogEventTypeError:");
	msg.append(vbLogEventTypeError);
	msg.append("]");
	logger.log(Level.SEVERE, msg.toString());
		
	}
}
