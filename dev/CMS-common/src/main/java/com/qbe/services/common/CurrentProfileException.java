package com.qbe.services.common;

public class CurrentProfileException extends Exception {

	public CurrentProfileException() {
	}

	public CurrentProfileException(String message) {
		super(message);
	}

	public CurrentProfileException(Throwable cause) {
		super(cause);
	}

	public CurrentProfileException(String message, Throwable cause) {
		super(message, cause);
	}

}
