package com.qbe.services.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class QBESchema {

	public static final String JDBC_URL = "jdbc:log4jdbc:hsqldb:mem:testdb";

	/**
	 * @throws SQLException
	 */
	public static void createQBESchema() throws SQLException {
		try {
	    	//http://stackoverflow.com/questions/160611/cause-of-no-suitable-driver-found-for
			Class.forName("org.hsqldb.jdbcDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    Connection con = DriverManager.getConnection(JDBC_URL, "SA", "");
    	createREL_PORTAL_ESTRUCTURA_COMERCIAL(con);
        createSPSNCV_REL_PORTAL_COMERC_SELECT(con);
        createTAB_SIFTGRAL(con);
        createSPSNCV_SIFTGRAL(con);
        createTAB_NUMERADORES(con);
        createSPSNCV_NROCOT(con);
        createUsers(con);
        con.close();
	}

	private static void createUsers(Connection con) throws SQLException {
		String grant = 
				"GRANT EXECUTE ON PROCEDURE SPSNCV_NROCOT TO PUBLIC";
		PreparedStatement spps = con.prepareStatement(grant);
		spps.executeUpdate();
		spps.close();

		grant = 
				"GRANT EXECUTE ON PROCEDURE SPSNCV_REL_PORTAL_COMERC_SELECT TO PUBLIC";
		spps = con.prepareStatement(grant);
		spps.executeUpdate();
		spps.close();
		
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private static void createSPSNCV_NROCOT(Connection con)
			throws SQLException {
		//@see http://hsqldb.org/doc/guide/sqlroutines-chapt.html
		// Sección "Cursors"
		String createSP = 
				"CREATE PROCEDURE SPSNCV_NROCOT(pPARAMETR VARCHAR(1))" +
				"   READS SQL DATA DYNAMIC RESULT SETS 1" +
				"   BEGIN ATOMIC" +
				"     DECLARE result CURSOR FOR " +
				"SELECT NROCOT " +
				"FROM TAB_NUMERADORES " +
				"FOR READ ONLY; " +
				"OPEN result; " +
				"   END";
		
		PreparedStatement spps = con.prepareStatement(createSP);
		spps.executeUpdate();
		spps.close();
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private static void createTAB_NUMERADORES(Connection con)
			throws SQLException {
		PreparedStatement ps = con.prepareStatement("CREATE TABLE TAB_NUMERADORES(" +
				"NROCOT INTEGER)");
		ps.executeUpdate();
		
		ps = con.prepareStatement("INSERT INTO TAB_NUMERADORES VALUES (2049089)");
		ps.executeUpdate();
		ps.close();
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private static void createSPSNCV_SIFTGRAL(Connection con)
			throws SQLException {
		//@see http://hsqldb.org/doc/guide/sqlroutines-chapt.html
		// Sección "Cursors"
		String createSP = 
				"CREATE PROCEDURE SPSNCV_SIFTGRAL(pPARAMETR VARCHAR(20))" +
				"   READS SQL DATA DYNAMIC RESULT SETS 1" +
				"   BEGIN ATOMIC" +
				"     DECLARE result CURSOR FOR " +
				"SELECT ISNULL(PARAMNUM,0) AS PARAMNUM," +
				"ISNULL(PARAMSTR,'') AS PARAMSTR " +
				"FROM TAB_SIFTGRAL " +
				"WHERE PARAMETR = pPARAMETR "+ 
				"FOR READ ONLY; " +
				"OPEN result; " +
				"   END";
		
		PreparedStatement spps = con.prepareStatement(createSP);
		spps.executeUpdate();
		spps.close();
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private static void createTAB_SIFTGRAL(Connection con)
			throws SQLException {
		PreparedStatement ps = con.prepareStatement("CREATE TABLE TAB_SIFTGRAL(" +
				"PARAMNUM VARCHAR(13), PARAMSTR VARCHAR(10), PARAMETR VARCHAR(10))");
		ps.executeUpdate();
		
		ps = con.prepareStatement("INSERT INTO TAB_SIFTGRAL VALUES (9, 'SEVERE', 'LOGLEVEL')");
		ps.executeUpdate();
		ps.close();

		//Antes:
		ps = con.prepareStatement("INSERT INTO TAB_SIFTGRAL VALUES ('29999.000000', '                                                                                ', 'SA-SCO-MIN')");
		ps.executeUpdate();
		ps.close();
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private static void createSPSNCV_REL_PORTAL_COMERC_SELECT(Connection con)
			throws SQLException {
		//@see http://hsqldb.org/doc/guide/sqlroutines-chapt.html
		// Sección "Cursors"
		String createSP = 
				"CREATE PROCEDURE SPSNCV_REL_PORTAL_COMERC_SELECT(pPORTAL VARCHAR(10), pRAMOPCOD VARCHAR(4))" +
				"   READS SQL DATA DYNAMIC RESULT SETS 1" +
				"   BEGIN ATOMIC" +
				"     DECLARE result CURSOR FOR SELECT " +
				"REL_PORTAL_ESTRUCTURA_COMERCIAL.PORTAL, " +
				"REL_PORTAL_ESTRUCTURA_COMERCIAL.RAMOPCOD, " +
				"REL_PORTAL_ESTRUCTURA_COMERCIAL.CAMPACOD, " +
				"REL_PORTAL_ESTRUCTURA_COMERCIAL.AGECOD, " +
				"REL_PORTAL_ESTRUCTURA_COMERCIAL.TELNRO, " +
				"isnull(REL_PORTAL_ESTRUCTURA_COMERCIAL.AGECLA,'PR') AS AGECLA " +
				"from REL_PORTAL_ESTRUCTURA_COMERCIAL " +
				"WHERE REL_PORTAL_ESTRUCTURA_COMERCIAL.RAMOPCOD = pRAMOPCOD AND REL_PORTAL_ESTRUCTURA_COMERCIAL.PORTAL = pPORTAL " +
				"FOR READ ONLY;" +
				"     OPEN result;    " +
				"   END";
		
		PreparedStatement spps = con.prepareStatement(createSP);
		spps.executeUpdate();
		spps.close();
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private static void createREL_PORTAL_ESTRUCTURA_COMERCIAL(Connection con)
			throws SQLException {
		PreparedStatement ps = con.prepareStatement("CREATE TABLE REL_PORTAL_ESTRUCTURA_COMERCIAL(" +
				"PORTAL VARCHAR(10), RAMOPCOD VARCHAR(4), CAMPACOD VARCHAR(10), AGECOD VARCHAR(10), TELNRO VARCHAR(10), AGECLA VARCHAR(10))");
		ps.executeUpdate();
		
		ps = con.prepareStatement("INSERT INTO REL_PORTAL_ESTRUCTURA_COMERCIAL VALUES ('ARMA', 'AUS1', 'CAMPACOD1', 'AGECOD1', 'TELNRO1', 'AGECLA1')");
		ps.executeUpdate();
		ps.close();
	}

	/**
	 * @throws SQLException
	 */
	public static void shutdownQBESchema() throws SQLException {
		Connection con = DriverManager.getConnection(JDBC_URL, "SA", "");
		
		PreparedStatement ps;
		ps = con.prepareStatement("DROP SCHEMA PUBLIC CASCADE");
		ps.executeUpdate();
		ps.close();
		con.close();
	}


}
