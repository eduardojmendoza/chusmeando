package com.qbe.services.common;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CurrentProfile {

	private static Logger logger = Logger.getLogger(CurrentProfile.class.getName());

	/**
	 * Retorna el nombre del profile en el que estamos corriendo. Lo saca de la
	 * propiedad profile_name del archivo profile.properties. Para cada profile
	 * se define uno distinto y maven elige el que corresponde al hacer el build
	 * 
	 * @return
	 * @throws CurrentProfileException
	 */
	public static String getProfileName() throws CurrentProfileException {
		try {
			String propsFilename = System.getProperty("cms.config.properties");
			logger.log(Level.INFO, "CurrentProfile: propsFilename = " + propsFilename);
			// En el 10.1.10.30 está definido como
			// -Dcms.config.properties=nov-test.properties

			if (propsFilename == null)
				return "dev";

			if (propsFilename.startsWith("/")) {
				URL propUrl = new URL(propsFilename);
				logger.log(Level.INFO, "propUrl: " + propUrl.toString());
				InputStream in = propUrl.openStream();
				Properties p = new Properties();
				p.load(in);
				in.close();
				return p.getProperty("profile_name", "???");

			} else {
				// Supongo que es un path local al war, aunque debería resolver también el caso de arriba
				InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
				Properties p = new Properties();
				p.load(in);
				in.close();
				return p.getProperty("profile_name", "???");
			}

		} catch (IOException e) {
			String msg = "No pude leer el archivo de properties de profile";
			logger.log(Level.SEVERE, msg);
			throw new CurrentProfileException(msg);
		}

	}
}
