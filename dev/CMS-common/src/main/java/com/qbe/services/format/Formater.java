package com.qbe.services.format;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import diamondedge.util.Obj;

/**
 * Clase que formatea valores con DecimalFormat.
 * @author leandrocohn
 *
 */
public class Formater {
	
	/**
	 * @param valueFormat Valor double a formatear.
	 * @param format Formato a convertir.
	 * @return
	 */
	public static String doubleFormat(String valueFormat, String format) {
		DecimalFormat formatter = new DecimalFormat(format,  new DecimalFormatSymbols(Locale.ENGLISH));
      	double toBeFormatted = Double.valueOf(valueFormat);
      	String formateado = formatter.format(toBeFormatted);
      	return formateado;
	}
	
	/**
	 * Convierte los doubles con .0 en Strings sin los 2 últimos dígitos
	 * @param floatNumber
	 * @return
	 */
	public static String convertsDouble(String floatNumber) {
		String parteEntera = floatNumber;
		String parteEnteraConvertida = String
				.valueOf(Obj.toDouble(parteEntera));
		if (parteEnteraConvertida.endsWith(".0"))
			parteEnteraConvertida = parteEnteraConvertida.substring(0,
					parteEnteraConvertida.length() - 2);
		return parteEnteraConvertida;
	}
}
