package com.qbe.services.fewebservices.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class ComposedResponse {
	@XmlElement( name = "code", required = true)
	private int code;
	
	@XmlElement( name = "Response", required = true)
	private AnyXmlElement response;

	public ComposedResponse() {
	}

	public ComposedResponse(int code, AnyXmlElement response) {
		this.response = response;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int resultCode) {
		this.code = resultCode;
	}

	public AnyXmlElement getResponse() {
		return response;
	}

	public void setResponse(AnyXmlElement response) {
		this.response = response;
	}

}
