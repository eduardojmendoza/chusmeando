package com.qbe.services.fileutils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

public class CMSFileUtils {

	public static Logger logger = Logger
			.getLogger(CMSFileUtils.class.getName());

	/**
	 * Retorna true si el URL es un recurso válido
	 * 
	 * @param resourceURL
	 * @return
	 * @throws UnsupportedProtocolException Si no soporta el protocolo especificado por el URL
	 * @throws IOException 
	 */
	public static boolean resourceExists(URL resourceURL) throws UnsupportedProtocolException, IOException {
		logger.log(Level.FINE, String.format("resourceExists: URL [%s]", resourceURL));
		if ( resourceURL.getProtocol().equalsIgnoreCase("http")) {
			HttpURLConnection huc = (HttpURLConnection) resourceURL.openConnection();
			huc.setRequestMethod("HEAD");
			huc.setConnectTimeout(5000);
			return (huc.getResponseCode() / 100 == 2);
		} else if ( resourceURL.getProtocol().equalsIgnoreCase("file")) {
			try {
				InputStream is = resourceURL.openStream();
				is.close();				
				return true;
			} catch (FileNotFoundException e) {
				logger.log(Level.INFO, String.format("FILE NOT FOUND  [%s]", resourceURL ));
				return false;
			}
		}
		throw new UnsupportedProtocolException("Protocolo no soportado:" + resourceURL.getProtocol());
	}

	/**
	 * Carga un resource desde el URL
	 * 
	 * @param resourceURL
	 * @return
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public static byte[] loadResource(URL resourceURL)
			throws MalformedURLException, IOException {
		logger.log(Level.FINE, String.format("loadResource: Load URL: [%s]", resourceURL));
		InputStream in;
		if ( resourceURL.getProtocol().equalsIgnoreCase("http")) {
			HttpURLConnection huc = (HttpURLConnection) resourceURL.openConnection();
			huc.setConnectTimeout(5000);
			in = huc.getInputStream();
		} else {
			in = resourceURL.openStream();
		}		
		try {
			byte[] bytes = IOUtils.toByteArray(in);
			return bytes;
		} finally {
			IOUtils.closeQuietly(in);
		}
	}
	
	
	/**
	 * @param location
	 * @param nameSearch
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */
	public static String[] getFiles(String location, final String nameSearch) throws MalformedURLException, URISyntaxException {
		File dir = new File(new URL(location).toURI());
	      FilenameFilter filter = new FilenameFilter() {
	         public boolean accept (File dir, String name) { 
	            return name.contains(nameSearch) && !name.endsWith(".zip");
	         } 
	      }; 
	      String[] children = dir.list(filter);
	      if (children == null) {
	         System.out.println("Either dir does not exist or is not a directory"); 
	      } else { 
	         for (int i=0; i< children.length; i++) {
	            String filename = children[i];
	            System.out.println(filename);
	         } 
	      }
	      
	      return children;
		
	}

}
