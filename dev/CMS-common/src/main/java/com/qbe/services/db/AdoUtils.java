package com.qbe.services.db;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import diamondedge.ado.AdoError;
import diamondedge.ado.Field;
import diamondedge.ado.Recordset;

/**
 * 
 * @author ramiro
 *
 */
public class AdoUtils {
	
	private static Logger logger = Logger.getLogger(AdoUtils.class.getName());
	
	/**
	 * Genera una representación XML del recordset que recibe, de forma equivalente/similar a lo que hace ADO.
	 * @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms681501(v=vs.85).aspx
	 * @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms676547(v=vs.85).aspx
	 * 
	 * Cierra el recordSet una vez que termina
	 * 
	 * FALTA Revisar si necesita la metadata el implementarla. Se puede ir armando cuando se recorre, porque los Fields lo tienen
	 * FALTA Declarar los NS en el root que ahora, aunque legal, queda incómodo.
	 * FALTA Probar con valores de tipos que pueden tener problema en la conversión como fechas y números decimales
	 * @param recordSet
	 * @return
	 * @throws ParserConfigurationException
	 * @throws TransformerException
	 * @throws AdoError
	 */
	public static Document saveRecordset(Recordset recordSet) throws ParserConfigurationException, XmlDomExtendedException, AdoError {
	    Document doc = createEmptyDocument();
	    Element dataNode = createDataNodeInDoc(doc);
	    
		while ((!recordSet.getEOF())) {
			Element rowNode = doc.createElementNS("#RowsetSchema", "z:row");
			dataNode.appendChild(rowNode);
			for (int ct = 0; ct < recordSet.getFields().getCount(); ct++) {
				if (recordSet.getFields().getField(ct).getValue().isNull() == false) {
					Field f = recordSet.getFields().getField(ct);
					rowNode.setAttribute(f.getName(), f.getValue().toString());
				}
			}
			recordSet.moveNext();
		}

		recordSet.close();
	    
	    //DEBUG
	    String result = XmlDomExtended.prettyPrintComplete(doc);
	    logger.fine("Document generado desde el recordSet: " + result);
	    
	    return doc;
	}

	/**
	 * @return
	 * @throws ParserConfigurationException
	 */
	private static Document createEmptyDocument()
			throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.newDocument();
		return doc;
	}

	/**
	 * Equivalente a saveRecordset, pero para java.sql.ResultSet
	 * 
	 * @param rs
	 * @return
	 * @throws ParserConfigurationException 
	 * @throws SQLException 
	 * @throws DOMException 
	 * @throws TransformerException 
	 * @throws XmlDomExtendedException 
	 */
	public static Document saveResultSet(ResultSet rs) throws ParserConfigurationException, DOMException, SQLException, XmlDomExtendedException {
	    Document doc = createEmptyDocument();
	    Element dataNode = createDataNodeInDoc(doc);
	    
		do {
			Element rowNode = doc.createElementNS("#RowsetSchema", "z:row");
			dataNode.appendChild(rowNode);
			for (int ct = 1; ct <= rs.getMetaData().getColumnCount(); ct++) {
				String columnName = rs.getMetaData().getColumnName(ct);
				String value = rs.getString(ct);
				rowNode.setAttribute(columnName, value);
			}
		} while (rs.next());

		rs.close();
	    //DEBUG
	    String result = XmlDomExtended.prettyPrintComplete(doc);
	    logger.fine("Document generado desde el ResultSet: " + result);
	    return doc;
	}

	/**
	 * @param doc
	 * @return
	 * @throws DOMException
	 */
	private static Element createDataNodeInDoc(Document doc)
			throws DOMException {
		Element dataNode = doc.createElementNS("urn:schemas-microsoft-com:rowset", "rs:data");
	    doc.appendChild(dataNode);
		return dataNode;
	}

}
