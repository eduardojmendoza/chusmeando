package com.qbe.services.decimal;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import diamondedge.util.Obj;

/**
 * @author leandrocohn
 *
 */
public class DecimalFormater {
	/**
	 * @param parteEntera Es lo que actualmente se está mandando como parámetro al Obj.toDouble. 
	 * @return
	 */
	public static String executePuntoCero(String parteEntera) {
		String parteEnteraConvertida = String.valueOf( Obj.toDouble( parteEntera ));
      	if ( parteEnteraConvertida.endsWith(".0")) parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length()-2);
      	return parteEnteraConvertida;
	}
	
	/**
	 * @param parteEntera Es lo que actualmente se está mandando como parámetro al Obj.toDouble. 
	 * @param formato Es lo que actualmente se pasa como segundo parámentro en el Strings.format.
	 * @return
	 */
	public static String executePuntoCeroFormat(String parteEntera, String formato) {
		String parteEnteraConvertida = executePuntoCero(parteEntera);
		DecimalFormat formatter = new DecimalFormat(formato,  new DecimalFormatSymbols(Locale.ENGLISH));
      	double toBeFormatted = Double.valueOf(parteEnteraConvertida);
      String formateado = formatter.format(toBeFormatted);
      	return formateado;
	}
	
	
	/**
	 * @param parteEntera Es lo que actualmente se está mandando como parámetro al Obj.toDouble. 
	 * @param formato Es lo que actualmente se pasa como segundo parámentro en el Strings.format.
	 * @param division Es la división que se le aplica al Obj.toDouble.
	 * @return
	 */
	public static String executePuntoCeroFormatDividido(String parteEntera, String formato, Integer division) {
      String parteEnteraConvertida = String.valueOf( Obj.toDouble( parteEntera )  / division );
      	if ( parteEnteraConvertida.endsWith(".0")) parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length()-2);
		DecimalFormat formatter = new DecimalFormat(formato,  new DecimalFormatSymbols(Locale.ENGLISH));
      	double toBeFormatted = Double.valueOf(parteEnteraConvertida);
      String formateado = formatter.format(toBeFormatted);
      	return formateado;
	}
	
}
