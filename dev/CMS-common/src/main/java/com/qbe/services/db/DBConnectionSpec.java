package com.qbe.services.db;

/**
 * Bean simple usado para almacenar las propiedades de conexión a una DB, usado por JDBCConnectionFactory
 * 
 * @author ramiro
 *
 */
public class DBConnectionSpec {

	private String jdbcurl;
	private String user;
	private String password;
	
	public DBConnectionSpec() {
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJdbcurl() {
		return jdbcurl;
	}

	public void setJdbcurl(String jdbcurl) {
		this.jdbcurl = jdbcurl;
	}

}
