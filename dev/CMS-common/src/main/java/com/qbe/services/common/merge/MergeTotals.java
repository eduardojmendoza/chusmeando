package com.qbe.services.common.merge;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class MergeTotals {

	private static NumberFormat numeroDecimal = DecimalFormat.getInstance(Locale.ENGLISH);
	private static NumberFormat numeroDecimal2 = DecimalFormat.getInstance(Locale.ENGLISH);
	private static NumberFormat numeroDecimalF = DecimalFormat.getInstance(Locale.FRANCE);
	static {
		numeroDecimal.setMaximumFractionDigits(2);
		numeroDecimal.setMinimumFractionDigits(2);
		numeroDecimal2.setMaximumFractionDigits(2);
		numeroDecimal2.setMinimumFractionDigits(0);
		numeroDecimalF.setMaximumFractionDigits(2);
		numeroDecimalF.setMinimumFractionDigits(2);
	}
	
	protected static Logger logger = Logger.getLogger(MergeTotals.class.getName());
	private static final String MERGE_ERROR = "Exception al intentar hacer el merge";

	public static Node mergeTotals(Node left, Node right) throws MergeException, XPathExpressionException,
			ParseException {

		if (logger.isLoggable(Level.FINEST)) {
			logger.log(Level.FINEST, String.format("mergeTotals: left = [%s], right = [%s]", left, right));
		}

		Node merged = left.cloneNode(false); // Se toma como referencia leftNode(AIS)
		if (merged.getNodeType() == Node.ELEMENT_NODE) {

			mergeAttributes(merged, right);

			List<Node> leftChildrenList = getChildElementList(left);
			List<Node> rightChildrenList = getChildElementList(right);

			XPath xpath = XPathFactory.newInstance().newXPath();
			for (int i = 0; i < leftChildrenList.size(); i++) {
				Node leftChild = leftChildrenList.get(i);
				Node rightChild = getRightMatchingNode(leftChild, right, rightChildrenList, xpath, i);

				if (rightChild != null) {
					merged.appendChild(mergeTotals(leftChild, rightChild));
				} else {
					merged.appendChild(leftChild);
				}
			}
		}
		logger.log(Level.FINEST, String.format("Entra al orden del merge a partir del siguiente nodo: ", merged.getNodeName()));
		sortCommercialStructureByNOM(merged);
		
		
		return merged;
	}
	
	/**
	 * Ordena cada nivel recursivamente por el atributo NOM
	 * @param parent
	 */
	public static void sortCommercialStructureByNOM(Node parent) {
			logger.log(Level.INFO, String.format("Orden del merge:", parent.getNodeName()));

		if ( "EST".equals(parent.getNodeName()) || "GO".equals(parent.getNodeName()) || "OR".equals(parent.getNodeName()) || "PR".equals(parent.getNodeName())) {
			NodeList childNodes = parent.getChildNodes();
			int childNodesLength = childNodes.getLength();
			
			List<Node> childrenCopy = new ArrayList<Node>();
			
			for (int i = 0; i < childNodesLength; i++) {
				Node childNode = childNodes.item(i);
				if (childNode.getNodeType() == Node.ELEMENT_NODE) {
					if (childNode.hasChildNodes()){
						sortCommercialStructureByNOM(childNode);
						childrenCopy.add(childNode);
					}else
						childrenCopy.add(childNode);
				}
			}
			sortComNodeByNOM(childrenCopy);

			while (parent.hasChildNodes()) {
				parent.removeChild(parent.getFirstChild());			
			}
			for (Node node : childrenCopy) {
				parent.appendChild(node);
			}
		}
			
	}

	private static void sortComNodeByNOM(List<Node> estComNodes) {
		Collections.sort(estComNodes, new Comparator<Node>() {

			@Override
			public int compare(Node o1, Node o2) {
				return o1.getAttributes().getNamedItem("NOM").getNodeValue().compareTo(o2.getAttributes().getNamedItem("NOM").getNodeValue());
			}
		});
		
	}

	public static Node mergeTotalsBalanced(Node ais, Node insis) throws MergeException {

		try {
			try {
				verificarInconsistencias(ais, insis);
			}catch (Exception e) {
				ais = normalizarSegundoSegunElPrimero(insis, ais); 
			}

			Node merged = mergeTotals(ais, insis);
			return merged;
			
		} catch (Exception e) {
			// TODO Falta log
			throw new MergeException(MERGE_ERROR + e.getMessage(), e);
		}
	}

	private static void mergeAttributes(Node left, Node right) throws MergeException {
		NamedNodeMap attrs = left.getAttributes();
		// Attrs puede ser null en algunos casos como TextNodes. Si esta vacio
		// no necesito hacer anda con sus atributos
		for (int i = 0; attrs != null && i < attrs.getLength(); i++) {
			Node attr = attrs.item(i);
			if (attr.getNodeType() != Node.ATTRIBUTE_NODE)
				throw new MergeException("Los atributos no son atributos(??)");
			String attrName = attr.getNodeName();
			if (attrName.startsWith("TOT")) {
				// El nodo de la izquierda es el que define en qué formato
				// tenemos que retornar el total.
				// Está así porque por convención mandamos el valor que retorna
				// CMS/AIS en el de la izquierda, y OV maneja el formato de
				// CMS/AIS.
				String leftValue = "";
				String rightValue = "";
				try {
					leftValue = left.getAttributes().getNamedItem(attrName).getNodeValue();
					rightValue = right.getAttributes().getNamedItem(attrName).getNodeValue();
				} catch (Exception e) {
					throw new MergeException("ERROR: Difieren los esquemas. ", e);
				}

				String mergeValue = "???";
				// El formato que viene de INSIS siempre va a ser el mismo
				// ###########.##
				// Se evalua estos formatos: 123,456.78 y 123.45
				if (leftValue.matches("\\d{1,3}(,\\d{3})*\\.\\d*\\d")) {
					mergeValue = sumadorFormato1(leftValue, rightValue);
				}
				// Se evalua estos formatos: 123
				else if (leftValue.matches("\\d{1,3}")) {
					mergeValue = sumadorFormato2(leftValue, rightValue);
				}
				// Se evalua estos formatos: 12345.67
				else if (leftValue.matches("\\d{4,}(\\d{3})*\\.\\d*\\d")) {
					mergeValue = sumadorFormato3(leftValue, rightValue);
				}
				// Se evalua estos formatos: 123.456,78 y 123,45
				else if (leftValue.matches("\\d{1,3}(.\\d{3})*\\,\\d*\\d")) {
					mergeValue = sumadorFormato4(leftValue, rightValue);
				} else {
					int leftNumber = Integer.parseInt(leftValue);
					int rightNumber = Integer.parseInt(rightValue);
					mergeValue = String.valueOf(leftNumber + rightNumber);
				}
				attr.setNodeValue(mergeValue);
			}
		}
	}

	/**
	 * Se crea este método porque la implementación de DOM de Weblogic hace cosas inesperadas cuando se recorre el arbol
	 * de DOM en diferentes niveles al mismo tiempo y deja inconsistentes las NodeList
	 * 
	 * @param Nodo
	 *            del que necesito los elementos hijos
	 * @return Lista de elementos hijos de primer nivel
	 */
	private static List<Node> getChildElementList(Node node) {
		NodeList childNodes = node.getChildNodes();
		int childNodesLength = childNodes.getLength();

		List<Node> nodeList = new ArrayList<Node>();

		for (int i = 0; i < childNodesLength; i++) {
			Node childNode = childNodes.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				nodeList.add(childNode);
			}
		}
		return nodeList;
	}

	/**
	 * Retorno un nodo hijo de right correspondiente a mergear con "nodo"
	 * 
	 * @param node con atributos a buscar
	 * @param right nodo en donde se ba a buscar
	 * @param rightChildrenList lista de hijos del nodo right
	 * @param xpath instancia de xpathNormalizador
	 * @param leftPosition posicion actual del "nodo" 
	 * @return nodo hijo de right que matchea con los atributos del "nodo" o rightChildrenList[leftPosition] si "nodo" no tiene atributos TOT
	 * @throws MergeException si falla el xquery o si retorna más de un resultado.
	 */
	private static Node getRightMatchingNode(Node node, Node right, List<Node> rightChildrenList, XPath xpath,
			int leftPosition) throws MergeException {

		String nodeName = node.getNodeName();
		NamedNodeMap nodeAttrs = node.getAttributes();
		Node matchingRightnode = null;
		if (nodeAttrs != null) {
			String queryString = "";
			String queryAttrs = "";

			// Recorremos los Atributos que no son TOT y generamos una query para buscarlo del lado derecho.
			queryAttrs = generateQueryAttrs(nodeAttrs);

			// En caso que solo se encuentren atributos TOT la query va a ser = ""
			if (!("".equals(queryAttrs))) {
				// Acá genero la query que me traera el mismo childNode en la lista de los de la derecha.
				// Tomando como referencia Todos los atributos menos los TOT.
				queryString = ("./" + nodeName + queryAttrs);
				NodeList rightChildSelected = null;
				try {
					// Ejecuto la query.
					rightChildSelected = (NodeList) xpath.evaluate(queryString, right.getChildNodes(),
							XPathConstants.NODESET);
				} catch (XPathExpressionException e) {
					throw new MergeException("Error al buscar el nodo en el arbol derecho", e);
				}

				// Verifico que no traiga más de uno.
				if (rightChildSelected.getLength() > 1) {
					throw new MergeException(String.format("Difiere estructura: Hay más de un nodo %s en el derecho.",
							queryString));
				}

				matchingRightnode = rightChildSelected.item(0);

			} else {
				// Al no tener atributos que no sean "TOT*" Asumimos que las estructuras son iguales.
				Node rightChild = rightChildrenList.get(leftPosition);

				if (!node.getNodeName().equalsIgnoreCase(rightChild.getNodeName()))
					throw new MergeException(
							"Difiere estructura, No se puede procesar porque no tienen la misma estructura ni atributos ademas del TOT* para buscarlo.");
				matchingRightnode = rightChild;
			}
		}
		return matchingRightnode;

	}

	/**
	 * Crea un nuevo doc XML sumando los valores de los atributos TOT* en los tags de ambos nodos.
	 * 
	 * Usa DOM internamente, la versión con Node es más directa
	 * 
	 * @param args
	 * @throws MergeException
	 *             Si las dos estructuras a mergear no coinciden
	 */
	public static String mergeTotals(String leftXml, String rightXml) throws MergeException {

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document leftDoc = db.parse(new InputSource(new StringReader(leftXml)));
			Node left = leftDoc.getFirstChild();

			Document rightDoc = db.parse(new InputSource(new StringReader(rightXml)));
			Node right = rightDoc.getFirstChild();

			verificarInconsistencias(left, right);

			Node merged = mergeTotals(left, right);

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(merged), new StreamResult(writer));
			String mergedStr = writer.getBuffer().toString();

			// String mergedStr = XmlDomExtended.marshal(merged);
			return mergedStr;
		} catch (Exception e) {
			// TODO Falta log
			throw new MergeException(MERGE_ERROR + e.getMessage(), e);
		}

	}
	
	public static String mergeTotalsBalancedTest(String aisXml, String insisXml) throws MergeException {
		try {
			Node merged = mergeTotalsBalanced(aisXml, insisXml);
			
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(merged), new StreamResult(writer));
			String mergedStr = writer.getBuffer().toString();

			// String mergedStr = XmlDomExtended.marshal(merged);
			return mergedStr;
		} catch (Exception e) {
			// TODO Falta log
			throw new MergeException(MERGE_ERROR + e.getMessage(), e);
		}

	}
	
	public static Node mergeTotalsBalanced(String aisXml, String insisXml) throws MergeException {
		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document leftDoc = db.parse(new InputSource(new StringReader(aisXml)));
			Node ais = leftDoc.getFirstChild();
			
			Document rightDoc = db.parse(new InputSource(new StringReader(insisXml)));
			Node insis = rightDoc.getFirstChild();

			Node merged = mergeTotalsBalanced(ais, insis);

			return merged;
		} catch (Exception e) {
			// TODO Falta log
			throw new MergeException(MERGE_ERROR + e.getMessage(), e);
		}
	}	
	
	public static void verificarInconsistencias(Node left, Node right) throws XPathExpressionException, MergeException {
		NodeList leftChildrenList = left.getChildNodes();
		NodeList rightChildrenList = right.getChildNodes();// Se utiliza INSIS
															// como indice para
															// verificar que no
															// existan nodos que
															// no esten en AIS.

		for (int i = 0; i < rightChildrenList.getLength(); i++) {

			Node rightChild = rightChildrenList.item(i);
			String rightChildnodeName = rightChild.getNodeName();
			NamedNodeMap rightAttrs = rightChild.getAttributes();

			if (rightAttrs != null) {
				String queryString = "";
				String queryAttrs = "";
				XPath queryXPath = XPathFactory.newInstance().newXPath();

				// Recorremos los Atributos que no son TOT y generamos una query
				// para buscarlo del lado derecho.
				for (int t = 0; t < rightAttrs.getLength(); t++) {
					Node rightAttrNode = rightAttrs.item(t);
					String rightAttrName = rightAttrNode.getNodeName();
					String rightAttrValue = rightAttrNode.getNodeValue();
					if ((rightAttrName.startsWith("CLI") || rightAttrName.startsWith("NIV"))) {
						if (queryAttrs == "") {
							queryAttrs += "[";
						}
						queryAttrs += "@" + rightAttrName + "='" + rightAttrValue + "' ";
						if (!(rightAttrs.getLength() == t + 1)) {
							queryAttrs += "and ";
						}
					}
					if (rightAttrs.getLength() == (t + 1)) {
						if (queryAttrs.endsWith("and ")) {
							queryAttrs = queryAttrs.substring(0, (queryAttrs.length() - 4));
							queryAttrs += "]";
						}
					}
				}
				// En caso que solo se encuentren atributos TOT la query va a
				// ser = ""
				if (queryAttrs != "") {
					// Acá genero la query que me traera el mismo childNode en
					// la lista de los de la derecha. Tomando como referencia
					// Todos los atributos menos los TOT.
					queryString = ("./" + rightChildnodeName + queryAttrs);
					// Ejecuto la query.
					NodeList leftChildSelected = (NodeList) queryXPath.evaluate(queryString, leftChildrenList,
							XPathConstants.NODESET);

					// Verifico que no sea Null.
					if (leftChildSelected.getLength() > 1) {
						throw new MergeException(String.format(
								"Difiere estructura: Hay más de un nodo %s en el derecho.", queryString));
					}
					Node leftChild = leftChildSelected.item(0);
					if (leftChild == null) {
						throw new MergeException("ERROR: Insis tiene mas nodos que AIS.");
					} else {
						verificarInconsistencias(leftChild, rightChild);
					}
				}
			}
		}
	}

	private static String generateQueryAttrs(NamedNodeMap leftAttrs) {
		String queryAttrs = "";

		for (int t = 0; t < leftAttrs.getLength(); t++) {
			Node leftAttrNode = leftAttrs.item(t);
			String leftAttrName = leftAttrNode.getNodeName();
			String leftAttrValue = leftAttrNode.getNodeValue();
			if (leftAttrName.startsWith("CLI") || leftAttrName.startsWith("NIV")) {
				if (queryAttrs == "") {
					queryAttrs += "[";
				}
				queryAttrs += "@" + leftAttrName + "='" + leftAttrValue + "' ";
				if (!(leftAttrs.getLength() == t + 1)) {
					queryAttrs += "and ";
				}
			}
			if (leftAttrs.getLength() == (t + 1)) {
				if (queryAttrs.endsWith("and ")) {
					queryAttrs = queryAttrs.substring(0, (queryAttrs.length() - 4));
					queryAttrs += "]";
				}
			}
		}
		return queryAttrs;
	}

	/**
	 * @param leftValue
	 * @param rightValue
	 * @return
	 * @throws MergeException
	 */
	private static String sumadorFormato1(String leftValue, String rightValue) throws MergeException {

		String mergeValue = "???";
		leftValue = leftValue.replace(",", "");

		try {
			Number leftNumber = numeroDecimal.parse(leftValue);
			Number rightNumber = numeroDecimal.parse(rightValue);
			double newValue = leftNumber.doubleValue() + rightNumber.doubleValue();
			mergeValue = numeroDecimal.format(newValue);
		} catch (ParseException e) {
			throw new MergeException("No puedo parsear los valores", e);
		}
		return mergeValue;
	}

	/**
	 * @param leftValue
	 * @param rightValue
	 * @return
	 * @throws MergeException
	 */
	private static String sumadorFormato2(String leftValue, String rightValue) throws MergeException {

		String mergeValue = "???";

		try {
			Number leftNumber = numeroDecimal2.parse(leftValue);
			Number rightNumber = numeroDecimal2.parse(rightValue);
			double newValue = leftNumber.doubleValue() + rightNumber.doubleValue();
			mergeValue = numeroDecimal2.format(newValue);
		} catch (ParseException e) {
			throw new MergeException("No puedo parsear los valores", e);
		}
		return mergeValue;
	}

	/**
	 * Realiza el parseo con los formatos:
	 * 
	 * @param leftValue
	 * @param rightValue
	 * @return
	 * @throws MergeException
	 */
	private static String sumadorFormato3(String leftValue, String rightValue) throws MergeException {

		String mergeValue = "???";

		try {
			Number leftNumber = numeroDecimal.parse(leftValue);
			Number rightNumber = numeroDecimal.parse(rightValue);
			double newValue = leftNumber.doubleValue() + rightNumber.doubleValue();
			mergeValue = numeroDecimal.format(newValue);
			mergeValue = mergeValue.replace(",", "");
		} catch (ParseException e) {
			throw new MergeException("No puedo parsear los valores", e);
		}
		return mergeValue;
	}

	/**
	 * Realiza el parseo con los formatos: 123.456,78 y 123,45
	 * 
	 * @param leftValue
	 * @param rightValue
	 * @return suma formateada
	 * @throws MergeException
	 */
	private static String sumadorFormato4(String leftValue, String rightValue) throws MergeException {

		String mergeValue = "???";
		
		leftValue = leftValue.replace(".", "");
		leftValue = leftValue.replace(",", ".");

		try {
			Number leftNumber = numeroDecimal.parse(leftValue);
			Number rightNumber = numeroDecimal.parse(rightValue);
			double newValue = leftNumber.doubleValue() + rightNumber.doubleValue();
			mergeValue = numeroDecimalF.format(newValue);
			mergeValue = mergeValue.replace(" ", ".");
			
		} catch (ParseException e) {
			throw new MergeException("No puedo parsear los valores", e);
		}
		return mergeValue;
	}

	
	
	
	public static Node normalizarSegundoSegunElPrimero(Node referente, Node aNormalizar) throws MergeException, XPathExpressionException, ParseException {
			
		if (logger.isLoggable(Level.FINEST)) {
			logger.log(Level.FINEST, String.format("mergeTotals: left = [%s], right = [%s]", referente, aNormalizar));
		}
		
		Node normalizado = aNormalizar.cloneNode(false);
		
		if (normalizado.getNodeType() == Node.ELEMENT_NODE) {
		
			List<Node> referenteChildrenList = getChildElementList(referente);
			List<Node> aNormalizarChildrenList = getChildElementList(aNormalizar);
			XPath xpath = XPathFactory.newInstance().newXPath();
			
			for (int i = 0; i < referenteChildrenList.size(); i++) {
				Node referenteChild = referenteChildrenList.get(i);
				Node aNormalizarChild = getRightMatchingNode(referenteChild, aNormalizar, aNormalizarChildrenList, xpath, i);
		
				if (aNormalizarChild != null) {
					normalizado.appendChild(normalizado.getOwnerDocument().importNode(normalizarSegundoSegunElPrimero(referenteChild, aNormalizarChild), true));
				} else {
					Node referenteClone = referenteChild.cloneNode(true);
					setAttributeDummy(referenteClone);
					normalizado.appendChild(normalizado.getOwnerDocument().importNode(referenteClone, true));
				}
			}
			
			for (int i = 0; i < aNormalizarChildrenList.size(); i++) {
				Node normalizarChild = aNormalizarChildrenList.get(i);
				Node referenteChild = getRightMatchingNode(normalizarChild, referente, referenteChildrenList, xpath, i);
		
				if (referenteChild != null) {
//					normalizado.appendChild(normalizado.getOwnerDocument().importNode(normalizarSegundoSegunElPrimero(normalizarChild, referenteChild), true));
				} else {
					Node referenteClone = normalizarChild.cloneNode(true);
					normalizado.appendChild(normalizado.getOwnerDocument().importNode(referenteClone, true));
				}
			}
		}
		return normalizado;
	}
	
	private static void setAttributeDummy(Node node) throws MergeException {
		NamedNodeMap attrs = node.getAttributes();
		
		for (int i = 0; attrs != null && i < attrs.getLength(); i++) {
			
			Node attr = attrs.item(i);
			String attrName = attr.getNodeName();
			
			if (attrName.startsWith("TOT")) {
				attr.setNodeValue("0");
			}
		}
		
		List<Node> childs = getChildElementList(node); 
		for (Node child : childs) {
			setAttributeDummy(child);
		}
	}
	
}

