package com.qbe.services.exutils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * FIXME Analizar cambiar por ExceptionUtils de commons lang
 * @author ramiro
 *
 */
public class ExceptionUtils {

	public static String fullStackTrace(Throwable t) {
		StringWriter errors = new StringWriter();
		t.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public static String getCauses(Exception _e_) {
		StringBuffer causes = new StringBuffer();
		for (Object o : org.apache.commons.lang.exception.ExceptionUtils.getThrowableList(_e_)) {
			Throwable t = ( Throwable)o;
			causes.append(t.getClass().getName());
			causes.append(": ");
			causes.append(t.getMessage());
			causes.append(".\n");
		}
		return causes.toString();
	}
}
