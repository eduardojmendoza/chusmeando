package com.qbe.integration;

import java.io.IOException;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

/**
 * Convierte un XML de la estructura comercial que devuelve INSIS ( ver QbeTotals.xsd )
 * a la que espera la OV ( ver OVTotals.xsd )
 * 
 * @author ramiro
 *
 */
public class CommercialStructureConverter {

	public static String convert(String sourceXML)
			throws IOException, XmlDomExtendedException {
		
		XmlDomExtended rawInsis = new XmlDomExtended();
		rawInsis.loadXML(sourceXML);

		XmlDomExtended fase1 = new XmlDomExtended();
		fase1.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		rawInsis.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("pullUpTotals.xsl"), fase1.getDocument(),true);

		XmlDomExtended fase2 = new XmlDomExtended();
		fase2.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase1.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("fixQBETotalsTagNames.xsl"), fase2.getDocument(),true);

		XmlDomExtended fase3 = new XmlDomExtended();
		fase3.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase2.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("groupByCLs.xsl"), fase3.getDocument(),true);

		XmlDomExtended fase4 = new XmlDomExtended();
		fase4.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase3.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("OpePendTotalesGroup.xsl"), fase4.getDocument(),true);
		return XmlDomExtended.marshal(fase4.getDocument().getDocumentElement());
	}

}
