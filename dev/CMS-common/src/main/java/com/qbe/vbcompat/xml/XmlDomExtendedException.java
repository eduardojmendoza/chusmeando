/**
 * 
 */
package com.qbe.vbcompat.xml;

/**
 * @author ramiro
 *
 */
public class XmlDomExtendedException extends Exception {

	/**
	 * 
	 */
	public XmlDomExtendedException() {
	}

	/**
	 * @param message
	 */
	public XmlDomExtendedException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public XmlDomExtendedException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public XmlDomExtendedException(String message, Throwable cause) {
		super(message, cause);
	}

}
