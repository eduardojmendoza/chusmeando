package com.qbe.vbcompat.framework.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.oxm.annotations.XmlCDATA;

@XmlRootElement(name = "Estado")
@XmlAccessorType(XmlAccessType.NONE)
//Configurado así para que no quiera bindear los isTrue/isFalse
public class Estado {

	public static final String FALSE = "false";

	public static final String TRUE = "true";

	@XmlAttribute
	protected String resultado;
	
	@XmlAttribute
	protected String mensaje = "";

	@XmlCDATA 
	@XmlValue
	protected String texto;
	
	public Estado() {
	}

	public static Estado newWithResultadoTrue() {
		Estado e = new Estado();
		e.setResultado(TRUE);
		return e;
	}

	public static Estado newWithResultadoFalse() {
		Estado e = new Estado();
		e.setResultado(FALSE);
		return e;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTexto() {
		return texto;
	}

	/**
	 * Tengo que limpiar los posibles CDATAs porque el XMLCData no maneja bien strings que ya tengan un CDATA.
	 * Ver https://bugs.eclipse.org/bugs/show_bug.cgi?id=322358
	 * Esto claramente puede generar un XML inválido, porque debería escapear todo lo que está dentro de las actuales secciones CDATA
	 * Por eso usamos el Response para respuestas de error y no como solución general.
	 * @param texto
	 */
	public void setTexto(String texto) {
		texto = StringUtils.remove(texto, "<![CDATA[");
		texto = StringUtils.remove(texto, "]]>");
		this.texto = texto;
	}

	public boolean isTrue() {
		return TRUE.equalsIgnoreCase(this.getResultado());
	}

	public boolean isFalse() {
		return FALSE.equalsIgnoreCase(this.getResultado());
	}

}
