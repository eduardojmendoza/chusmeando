package com.qbe.vbcompat.framework;

import com.qbe.vbcompat.string.StringHolder;


public interface VBObjectClass {

	public abstract int IAction_Execute(String wvarRequest, StringHolder wvarResponse, String extra);
}
