package com.qbe.vbcompat.string;

public class StringHolder {

	private String value;

	public StringHolder() {
	}

	public StringHolder(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void set(String value) {
		this.value = value;
	}

	public boolean isNumeric() {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	@Override
	public String toString() {
		return value;
	}
}
