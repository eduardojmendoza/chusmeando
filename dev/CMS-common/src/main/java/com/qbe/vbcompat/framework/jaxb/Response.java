package com.qbe.vbcompat.framework.jaxb;

import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.eclipse.persistence.oxm.annotations.XmlCDATA;

/**
 * Bean de respuesta usado por el framework de invocación de action codes.
 * 
 * @author ramiro
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Response")
public class Response {

	private static Logger logger = Logger.getLogger(Response.class.getName());

	@XmlElement(name="Estado")
	protected Estado estado ;

	public static String marshaled(String resultado, String mensaje, String texto) {
		Response r = new Response(resultado, mensaje, texto);
		return r.marshal();
	}
	
	public static String marshaledResultadoFalse(String mensaje, String texto) {
		Response r = new Response(Estado.FALSE, mensaje, texto);
		return r.marshal();
	}

	public static String marshaledResultadoTrue(String mensaje, String texto) {
		Response r = new Response(Estado.TRUE, mensaje, texto);
		return r.marshal();
	}

	public Response() {
	}

	public Response(String resultado, String mensaje, String texto) {
		this();
		this.setMensajeEstado(mensaje);
		this.setTextoEstado(texto);
		this.setResultadoEstado(resultado);
	}
	
	public static Response newWithEstadoFalse() {
		Response r = new Response();
		r.setEstado(Estado.newWithResultadoFalse());
		return r;
	}

	public String marshal() {
		StringWriter sw = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(this.getClass());
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, true);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			m.marshal(this, sw);
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("Exception %s [ %s ] al marshalear un Response. StackTrace: %s", e.getClass().getName(), e.getMessage(),ExceptionUtils.getStackTrace(e)));
			//Intento algo más básico
			return String
					.format("<Response><Estado resultado='%s' mensaje='%s' /></Response>",
							this.estado == null ? "N/A" : this.estado
									.getResultado(),
							this.estado == null ? "N/A" : this.estado
									.getMensaje());
		}
		return sw.toString();
	}

	public Estado getEstado() {
		if ( this.estado == null) {
			this.estado = new Estado();
		}
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void setMensajeEstado(String msj) {
		getEstado().setMensaje(msj);
	}

	public void setTextoEstado(String msj) {
		getEstado().setTexto(msj);
	}

	public void setResultadoEstado(String msj) {
		getEstado().setResultado(msj);
	}

	public static String marshalWithException(Exception _e_) {
		return Response.marshaledResultadoFalse(ExceptionUtils.getMessage(_e_), 
				ExceptionUtils.getFullStackTrace(_e_));
	}

	public static String marshalWithException(String mensaje, Exception _e_) {
		return Response.marshaledResultadoFalse(mensaje, 
				ExceptionUtils.getFullStackTrace(_e_));
	}

	public static String cannedTimeoutResponse() {
		return Response.marshaledResultadoFalse("El servicio de consulta no se encuentra disponible", "Codigo Error:3");
	}
}
