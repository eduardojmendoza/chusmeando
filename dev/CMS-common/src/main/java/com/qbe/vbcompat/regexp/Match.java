package com.qbe.vbcompat.regexp;

import java.util.regex.Matcher;

public class Match {
	private String[] capturingGroups;

	private String textoEncontrado;
	
	/**
	 * @param matcherEntrada
	 */
	public Match(Matcher matcherEntrada) {
		this.textoEncontrado = matcherEntrada.group();
		//Se crean los SubMatches
		capturingGroups = new String[matcherEntrada.groupCount()];
		
		//Se realiza este tratamiento porque en VB, los submatcher comienzan en 0 y en java en 1.
		for (int posiGrupo = 1; posiGrupo <= matcherEntrada.groupCount(); posiGrupo++) {
			capturingGroups[posiGrupo - 1] = matcherEntrada.group(posiGrupo);
		}
	}
	
	/**
	 * Método que devuelve el subgrupo con la posición solicitada. 
	 * 
	 * @param wvarCounter - Es el índice del grupo capturado, siendo 0, la primer posición. En java era la 1.
	 * @return Es el string con la posición del grupo capturado, solicitado.
	 */
	public String SubMatches(int wvarCounter) {
		return capturingGroups[wvarCounter];
	}
	
	/**
	 * @return Devuelve la cantidad de caracteres que contiene el match.
	 */
	public int Length() {
		return textoEncontrado.length();
	}
	
	/**
	 * @return Devuelve el texto encontrado.
	 */
	public String Value() {
		return textoEncontrado;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Match[");
		sb.append("textoEncontrado: ");
		sb.append(textoEncontrado);
		sb.append(",");
		sb.append("capturingGropus: [");
		for (String cg : capturingGroups) {
			sb.append(cg);
			sb.append(",");
		}
		sb.append("]]");
		return sb.toString();
	}
}
