package com.qbe.vbcompat.base64;

import javax.xml.bind.DatatypeConverter;

/**
 * Métodos utilitarios para pasar de byte[] a un String codificado como Base64
 * 
 * 
 * @author ramiro
 *
 */
public class Base64Encoding {
	
	public static String encode(byte[] byteArray) {
		return DatatypeConverter.printBase64Binary(byteArray);
	}

	public static byte[] decode(String base64) {
		return DatatypeConverter.parseBase64Binary(base64);
	}
}
