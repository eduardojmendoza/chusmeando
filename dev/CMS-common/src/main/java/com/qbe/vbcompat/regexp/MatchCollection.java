package com.qbe.vbcompat.regexp;

import java.util.List;

/**
 * @author leandrocohn
 *
 */
public class MatchCollection {

	private List<Match> collection;
	
	private int position;
	
	/**
	 * Constructor por parámetros en donde se le informan la colección de matches que contendrá la clase.
	 * @param items
	 */
	public MatchCollection(List<Match> items) {
		this.collection = items;
		position = 0;
	}
	
	/**
	 * 
	 * @return Devuelve el próximo Match que contiene la lista de items.
	 */
	public Match next() {
		if (position < collection.size()) {
			Match retorno = collection.get(position);
			position++;
			return retorno;
		} else {
			return null;
		}
	}

	/**
	 * @return Retorna la cantidad de Matches, que contiene la coleccion.
	 */
	public int getCount() {
		return collection.size();
	}

	/**
	 * @param posicion
	 * @return Retorna el Match de la coleccion, cuya posición, se corresponde con el parámetro de entrada. NOTA: Comienza con la posición 0.
	 */
	public Match Item(int posicion) {
		if (posicion < collection.size()) {
			return collection.get(posicion);
		} else {
			return null;
		}
	}
}
