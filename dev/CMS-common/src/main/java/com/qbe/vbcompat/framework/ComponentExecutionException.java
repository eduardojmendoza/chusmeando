package com.qbe.vbcompat.framework;

public class ComponentExecutionException extends RuntimeException {

	private int code = 1;
	
	public ComponentExecutionException() {
	}

	public ComponentExecutionException(String arg0) {
		super(arg0);
	}

	public ComponentExecutionException(Throwable arg0) {
		super(arg0);
	}

	public ComponentExecutionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ComponentExecutionException(int code, String arg0, Throwable arg1) {
		super(arg0, arg1);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
