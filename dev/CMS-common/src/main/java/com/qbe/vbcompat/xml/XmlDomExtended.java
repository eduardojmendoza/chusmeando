package com.qbe.vbcompat.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import diamondedge.util.XmlDom;

/**
 * XmlDomExtended agrega funcionalidad a la provista por XmlDom de VBC, y básicamente wrappea un org.w3c.dom.Document
 *  
 * XmlDom está basada en Microsoft.XMLDOM
 * @see http://msdn.microsoft.com/en-us/library/windows/desktop/ms764730(v=vs.85).aspx
 * 
 * 
 * @author ramiro
 *
 */
public class XmlDomExtended extends XmlDom{

	protected static Logger logger = Logger.getLogger(XmlDomExtended.class.getName());

	public XmlDomExtended() {
		super();
	}
	
	public XmlDomExtended(Node node) throws XmlDomExtendedException {
		super();
		this.load(node);
	}

	/**
	 * Retona una NodeList de los nodos que matchean la expresión partiendo del org.w3c.dom.Document del receptor
	 * 
	 * @param xpathExpr
	 * @return
	 * @throws XmlDomExtendedException
	 */
	public NodeList selectNodes( String xpathExpr) throws XmlDomExtendedException {

		try {
			Document doc = this.getDocument(); 
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			//Para obtener un conjunto de nodos:
			NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			return nl;
		} catch (ClassCastException e) {
			String msg = "El resultado de la expresión no era un NodeList";
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	/**
	 * Retona una NodeList de los subnodos de rootNode de que matchean la expresión
	 * @param rootNode
	 * @param xpathExpr
	 * @return
	 * @throws XmlDomExtendedException
	 */
	public static NodeList Node_selectNodes(Element rootNode, String xpathExpr) throws XmlDomExtendedException {
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			NodeList nl = (NodeList) expr.evaluate(rootNode, XPathConstants.NODESET);
			return nl;
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + XmlDomExtended.class.getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}
	
	/**
	 * Retona el Node que matchea la expresión, a partir del org.w3c.dom.Document del receptor 
	 * 
	 * @param xpathExpr debe devolver un Node
	 * @return
	 * @throws XmlDomExtendedException
	 */
	public Node selectSingleNode(String xpathExpr) throws XmlDomExtendedException {
		try {
			Document doc = this.getDocument(); 
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			Node nl = (Node) expr.evaluate(doc, XPathConstants.NODE);
			return nl;
		} catch (ClassCastException e) {
			String msg = "El resultado de la expresión no era un Node";
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	/**
	 * Retona el Node que matchea la expresión, a partir del org.w3c.dom.Document del receptor.
	 * La expresión puede tener referencias a Namespaces que se resuelven via el nc
	 * 
	 * @param xpathExpr debe devolver un Node
	 * @return
	 * @throws XmlDomExtendedException
	 */
	public Node selectSingleNode(String xpathExpr, NamespaceContext nc) throws XmlDomExtendedException {
		try {
			Document doc = this.getDocument(); 
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			xpath.setNamespaceContext(nc);
			XPathExpression expr = xpath.compile(xpathExpr);
			Node nl = (Node) expr.evaluate(doc, XPathConstants.NODE);
			return nl;
		} catch (ClassCastException e) {
			String msg = "El resultado de la expresión no era un Node";
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	/**
	 * Retona que matchea la expresión, a partir del rootNode
	 * 
	 * @param rootNode
	 * @param xpathExpr Debe retornar un solo Node
	 * @return
	 * @throws XmlDomExtendedException 
	 */
	public static Node Node_selectSingleNode(Node rootNode, String xpathExpr) throws XmlDomExtendedException {
		try {
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			XPathExpression expr = xpath.compile(xpathExpr);
			Node nl = (Node) expr.evaluate(rootNode, XPathConstants.NODE);
			return nl;
		} catch (ClassCastException e) {
			String msg = "El resultado de la expresión no era un Node";
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + XmlDomExtended.class.getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	/**
	 * Aplica el XSL contenido en el xslFileName sobre el org.w3c.dom.Document del receptor y retorna el resultado
	 * 
	 * @param xslFileName
	 * @return
	 * @throws XmlDomExtendedException que encapsula las causas del error ( FileNotFoundException, TransformerConfigurationException, TransformerException )
	 */
	public String transformNode(String xslFileName) throws XmlDomExtendedException {
	    TransformerFactory tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
	    try {
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(xslFileName);

			Transformer transformer = tFactory.newTransformer(new StreamSource(
					is));
			StringWriter sw = new StringWriter();
			transformer.transform(new DOMSource(this.getDocument()),
					new StreamResult((sw)));
			return sw.toString();
		} catch (Exception e) {
			String msg = "No se pudo aplicar el XSL: " + e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	/**
	 * Aplica el XSL contenido en el inputStream sobre el org.w3c.dom.Document del receptor y mete el resultado en el node
	 *
	 * Usa saxon 
	 * @return
	 * @throws XmlDomExtendedException que encapsula las causas del error ( FileNotFoundException, TransformerConfigurationException, TransformerException )
	 */
	public void transformNode(InputStream is, Node node) throws XmlDomExtendedException { 
		transformNode(is, node, true);
	}
	

		/**
	 * Aplica el XSL contenido en el inputStream sobre el org.w3c.dom.Document del receptor y mete el resultado en el node
	 *
	 * 
	 * @param saxon Si usa la implementación de saxon o la embedida. Algunos XSLTs, dan problema con uno y andan con el otro
	 * @return
	 * @throws XmlDomExtendedException que encapsula las causas del error ( FileNotFoundException, TransformerConfigurationException, TransformerException )
	 */
	public void transformNode(InputStream is, Node node, boolean saxon) throws XmlDomExtendedException {
		String factoryName = saxon ? "net.sf.saxon.TransformerFactoryImpl" : "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl";
	    TransformerFactory tFactory = TransformerFactory.newInstance(factoryName,null);
	    try {

			Transformer transformer = tFactory.newTransformer(new StreamSource(
					is));
			transformer.transform(new DOMSource(this.getDocument()),
					new DOMResult(node));
		} catch (Exception e) {
			String msg = "No se pudo aplicar el XSL: " + e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	/**
 	 * Aplica el XSL contenido en el xslDom sobre el org.w3c.dom.Document del receptor
	 * 
	 * @param xslDom
	 * @return
	 * @throws XmlDomExtendedException
	 */
	public String transformNode(XmlDomExtended xslDom) throws XmlDomExtendedException {
	    TransformerFactory tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
	    try {
	    	//Algunos XSLs vienen con <xsl:output method=\"html\" version=\"1.0\"  y a Saxon no le gusta el version="1.0" ( está mal, HTML no es v 1.0 )
	    	Document doc = xslDom.getDocument();
	    	NodeList nl = doc.getElementsByTagName("xsl:output");
	    	for (int i = 0; i < nl.getLength(); i++) {
				Element element = (Element)nl.item(i);
				if ( element.hasAttribute("version")) {
					element.removeAttribute("version");
				}
			}
	    	
			Transformer transformer = tFactory.newTransformer(new DOMSource(doc));
			StringWriter xmlResultWriter = new StringWriter();
			transformer.transform(new StreamSource(new StringReader(this.toString())),
					new StreamResult((xmlResultWriter)));
			return xmlResultWriter.toString();
		} catch (Exception e) {
			String msg = e.getClass().getName() + " en " + this.getClass().getName();
			logger.log(Level.SEVERE, msg,e);
			throw new XmlDomExtendedException(msg, e);
		}
	}
	
	/**
	 * Agrega una CDATASection conteniendo "text" como child del nodo
	 * 
	 * @param nodo
	 * @param text
	 */
	public static void setCDATAText(Element nodo, String text) {
		Document document = nodo.getOwnerDocument();
		Node cdata = document.createCDATASection(text);
		nodo.appendChild(cdata);
	}
	
	/**
	 * Retorna un String con el XML del receptor, si falla la serialización retorna Object.toString()
	 * @see prettyPrint(Node)
	 * 
	 */
	@Override
	public String toString() {
		try {
			return prettyPrint(this.getDocument()); //FALTA cambiar esto por marshal PREVIA revisada que el no rompo nada que llame a este toString. 
		} catch (Exception e) {
			logger.log(Level.WARNING,"Exception al imprimir un XmlDomExtended",e);
			logger.log(Level.WARNING,"super.toString() es: " + super.toString());
			return super.toString();
		}
	}
	
	/**
	 * Retorna un String con el XML legible que representa al node ( indentado, sin declaración XML, UTF-8 )
	 * Implementado via JAXB, para que serialice los CDATASection
	 * FIXME revisar si no puedo usar el q esta en fewebservices para esto
	 * 
	 * @param node
	 * @return 
	 * @throws XmlDomExtendedException que encapsula las causas del error ( PropertyException, JAXBException )
	 */
	public static String marshal(Node node) throws XmlDomExtendedException {

		AnyXmlElement rootAny = new AnyXmlElement();
		rootAny.getAny().add(node);

		StringWriter writer = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(AnyXmlElement.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, true);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			m.marshal(rootAny, writer);
			String xml = writer.getBuffer().toString().trim();
			return xml;
		} catch (PropertyException e) {
			String msg = "Exception al hacer el marshal del nodo";
			logger.log(Level.SEVERE, msg, e);
			throw new XmlDomExtendedException(msg, e);
		} catch (JAXBException e) {
			String msg = "Exception al hacer el marshal del nodo";
			logger.log(Level.SEVERE, msg, e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	
	/**
	 * Retorna un String con el XML legible que representa al node ( indentado, full ( con declaración XML ), UTF-8 )
	 * Implementado via JAXB, para que serialice los CDATASection
	 * 
	 * @param node
	 * @return
	 * @throws XmlDomExtendedException que encapsula las causas del error ( PropertyException, JAXBException )
	 */
	public static String marshalAsDocument(Element element) throws XmlDomExtendedException {

		AnyXmlElement rootAny = new AnyXmlElement();
		rootAny.getAny().add(element);

		StringWriter writer = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(AnyXmlElement.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, false);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			m.marshal(rootAny, writer);
			String xml = writer.getBuffer().toString().trim();
			return xml;
		} catch (PropertyException e) {
			String msg = "Exception al hacer el marshal del nodo";
			logger.log(Level.SEVERE, msg, e);
			throw new XmlDomExtendedException(msg, e);
		} catch (JAXBException e) {
			String msg = "Exception al hacer el marshal del nodo";
			logger.log(Level.SEVERE, msg, e);
			throw new XmlDomExtendedException(msg, e);
		}
	}

	
	/**
	 * Retorna un String con el XML legible que representa al node ( indentado, sin declaración XML, UTF-8 )
	 * Este método NO genera CDATA Sections
	 * 
	 * @param node
	 * @return
	 * @throws XmlDomExtendedException 
	 */
	public static String prettyPrint(Node node) throws XmlDomExtendedException {

		try {
			TransformerFactory tf = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl",null);
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			transformer.transform(new DOMSource(node),xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (TransformerConfigurationException e) {
			throw new XmlDomExtendedException(e);
		} catch (IllegalArgumentException e) {
			throw new XmlDomExtendedException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new XmlDomExtendedException(e);
		} catch (TransformerException e) {
			throw new XmlDomExtendedException(e);
		}
	}

	/**
	 * Retorna un String con el XML legible que representa al node ( indentado, CON declaración XML, UTF-8 )
	 * 
	 * @param node
	 * @return
	 * @throws TransformerException
	 */
	public static String prettyPrintComplete(Node node) throws XmlDomExtendedException {

		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			transformer.transform(new DOMSource(node),xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (TransformerConfigurationException e) {
			throw new XmlDomExtendedException(e);
		} catch (IllegalArgumentException e) {
			throw new XmlDomExtendedException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new XmlDomExtendedException(e);
		} catch (TransformerException e) {
			throw new XmlDomExtendedException(e);
		}
	}

	/**
	 * Graba el Document del receptor en el archivo en path
	 * Sobreescribe el archivo si existe.
	 * Crea los directorios padre del archivo si no existen
	 */
	@Override
	public void save(String path) {
		logger.log(Level.FINE, "Por grabar XmlDomExtended en path: " + path);
		String content = "";
		try {
			content = marshal(this.getDocument());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception al serializar un XmlDomExtended para grabarlo. Grabo XML de error.",e);
			content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<NoPudoGenerarPrintDelXmlDomExtended/>\n";
		}
		logger.log(Level.FINE, "Grabando contenido: " + StringUtils.abbreviate(content, 140));
		try {
			File out = new File(path);
			FileUtils.writeStringToFile(out,content ,Charset.forName("UTF-8"));
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Exception al grabar. No grabo nada.",e);
		}
	}

	/**
	 * Carga el receptor desde el file.
	 * Retorna true solamente si lo pudo cargar
	 * 
	 */
	@Override
	public boolean load(File file) {
		return super.load(file);
	}

	/**
	 * Carga el receptor desde el file con path "filePath"
	 * Retorna true solamente si lo pudo cargar
	 * 
	 */
	@Override
	public boolean load(String filePath) throws XmlDomExtendedException  {
		try {
			return super.load(filePath);
		} catch (Exception e) {
			// la e la tira cuando el string está en NULL
			throw new XmlDomExtendedException("el path es null");
		}
	}

	/**
	 * Carga el receptor con un Document parseando el xml
	 */
	@Override
	public boolean loadXML(String xml) {
		return super.loadXML(xml);
	}

	/**
	 * Carga el receptor parseando el contenido del resourceAsStream
	 * Este método lo agregamos porque los métodos load() que se heredan no levantan correctamente resources que están dentro de un jar, 
	 * porque esperan paths simples. Esto puede ocurrir en una webapp donde los paths obtenidos de "getResource()" vienen del tipo:
	 * weblogic_domains/domain_10.3.6/servers/myserver/tmp/_WL_user/_appsdir_CMS-fewebservices-0.0.1-SNAPSHOT_war/1hhp1f/war/WEB-INF/lib/CMS-mqgen-0.0.1-SNAPSHOT.jar!/DefinicionesXML/DefinicionesCotizador/CotizarICOModular.xml
	 * 
	 * @param resourceAsStream
	 * @return
	 * @throws IOException
	 * @throws XmlDomExtendedException si el stream es null
	 */
	public boolean load(InputStream resourceAsStream) throws IOException, XmlDomExtendedException {
		if ( resourceAsStream == null) {
			throw new XmlDomExtendedException("El recurso a cargar es null. ¿Archivo no encontrado?");
		}
		//el IOUtils no es demasiado fiable en algunos casos y devuelve mal la interpretacion del archivo.
		//Por ej: archivo que comienza por <?xml lo puede interpretar como ?xml
		String xml = IOUtils.toString(resourceAsStream,"UTF-8");
		if ( (xml.startsWith("<MENSAJE")) ){
			/*TODO	
			 * añadir el encabezado <?xml version="1.0" encoding="UTF-8"> y 
			 * mitigar la falla en lugar de tirar el error e interrumpir el correcto funcionamiento
			 */
			throw new XmlDomExtendedException("El recurso a cargar no se puede interpretar porque falta cabecera de XML");	
		}if (xml.startsWith("?xml")){
			xml = "<" + xml;
		}
			
		return this.loadXML(xml);
	}

	/**
	 * Carga el receptor con el doc que recibe como parámetro
	 * 
	 * @param doc
	 * @throws XmlDomExtendedException
	 */
	public void load(Document doc) throws XmlDomExtendedException {
			this.loadXML(prettyPrintComplete(doc)); //FALTA cambiar por marshall, solamente lo uso desde los que llaman a SPs
	}

	/**
	 * Carga el receptor con el doc que recibe como parámetro
	 * 
	 * @param doc
	 * @throws XmlDomExtendedException
	 */
	public void load(Node node) throws XmlDomExtendedException {
			this.loadXML(marshal(node)); 
	}

	/**
	 * Este método espera una NodeList de un elemento. El código de los componentes migrados
	 * usa selectNodes en vez de selectNode ( a veces ), por lo que este método resuelve esta
	 * interface
	 * 
	 * El comportamiento de getText para una NodeList no está definido, por lo tanto si no recibe
	 * un NodeList de un solo elemento tira una Exception
	 * 
	 * @param selectNodes
	 * @param valueOf
	 * @throws XmlDomExtendedException 
	 */
	public static String getText(NodeList selectNodes) throws XmlDomExtendedException {
		if ( selectNodes.getLength() != 1) {
			throw new XmlDomExtendedException("setText espera NodeList de 1 elemento");
		}
		return XmlDomExtended.getText(selectNodes.item(0));
	}

	/**
	 * Este método espera una NodeList de un elemento.
	 * @see getText(NodeList)
	 * 
	 * @param selectNodes
	 * @param valueOf
	 */
	public static void setText(NodeList selectNodes, String value) throws XmlDomExtendedException{
		if ( selectNodes.getLength() != 1) {
			throw new XmlDomExtendedException("setText espera NodeList de 1 elemento");
		}
		Node n = selectNodes.item(0);
		XmlDomExtended.setText(n, value);
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	
	
	@Override
	public Document getDocument() throws XmlDomExtendedException {
		try {
			return super.getDocument();
		} catch (Exception e) {
			logger.log(Level.WARNING, "Convirtiendo Exception a XmlDomExtendedException.",e);
			throw new XmlDomExtendedException(e);
		}
	}

	@Override
	public diamondedge.util.XmlDom.a getParseError() {
		return super.getParseError();
	}

	@Override
	public String getUrl() {
		return super.getUrl();
	}

	@Override
	public boolean isPreservingWhiteSpace() {
		return super.isPreservingWhiteSpace();
	}

	@Override
	public boolean isValidating() {
		return super.isValidating();
	}

	@Override
	public void setPreservingWhiteSpace(boolean arg0) {
		super.setPreservingWhiteSpace(arg0);
	}

	@Override
	public void setValidating(boolean arg0) {
		super.setValidating(arg0);
	}

	/**
	 * Método que devuelve el nodo creado según su tipo.
	 * @param type short.
	 * @param name String.
	 * @param namespaceURI String.
	 * @return Devuelve el nodo creado.
	 * @throws XmlDomExtendedException 
	 */
	public Node createNode(short type, String name, String namespaceURI) throws XmlDomExtendedException {
		
		Node nodoCreado = null;
		switch (type) {
		case 1: 
			//Se crea un ELEMENT_NODE
			nodoCreado = this.getDocument().createElement(name);
			break;
		case 2: 
			//Se crea un ATTRIBUTE_NODE
			nodoCreado = this.getDocument().createAttribute(name);
			break;
		case 3: 
			//Se crea un TEXT_NODE
			nodoCreado = this.getDocument().createTextNode(name);
			break;
		default:
			break;
		}
		return nodoCreado;
		
	}

	/**
	 * Retorna el resultado de marshal() sobre el documento del receptor
	 * 
	 * @see XmlDomExtended.marshal()
	 * @return "" si el DocumentElement del Document es null, el marshaleo del Document si no
	 * @throws XmlDomExtendedException
	 */
	public String marshal() throws XmlDomExtendedException {
		if ( this.getDocument() == null || this.getDocument().getDocumentElement() == null ) {
			return "";
		} else {
			return XmlDomExtended.marshal(this.getDocument());
		}
	}

	/**
	 * Agrega un clon del original como nuevo child del parent
	 * El original viene de otro Document, por lo que hay que importarlo en el del source
	 * 
	 * @param selectSingleNode
	 * @param selectSingleNode2
	 * @throws XmlDomExtendedException Si alguno de los dos es null
	 */
	public static void nodeImportAsChildNode(Node parent,
			Node original) throws XmlDomExtendedException {
		if ( parent == null) throw new XmlDomExtendedException("parent node is null");
		if ( original == null) throw new XmlDomExtendedException("child node is null");
		Node clone = parent.getOwnerDocument().importNode(original, true);
		parent.appendChild (clone);
	}

	/**
	 * Agrega los children como hijos de parentNode.
	 * Método utilitario, para no repetir la iteración dado que no hay un addChildren(NodeList)
	 * @see Node org.w3c.dom.Node.appendChild(Node newChild)
	 * 
	 * @param parentNode
	 * @param children
	 */
	public static void addChildrenToParent(Node parentNode,
			NodeList children) {
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			parentNode.appendChild(child);
		}
	}

}
