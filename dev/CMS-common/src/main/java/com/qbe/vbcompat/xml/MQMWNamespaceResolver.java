package com.qbe.vbcompat.xml;

import java.util.Iterator;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

public class MQMWNamespaceResolver implements NamespaceContext {

    /**
     * This method returns the uri for all prefixes needed. Wherever possible
     * it uses XMLConstants.
     * 
     * @param prefix
     * @return uri
     */
    public String getNamespaceURI(String prefix) {
        if (prefix == null) {
            throw new IllegalArgumentException("No prefix provided!");
//Para manejar un default
            //        } else if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
//            return "";
        } else if (prefix.equals("share")) {
            return "http://ejbs.mdw.hbar.hsbc.com/SharedServicesSessionFacadeService";
        } else {
            return XMLConstants.NULL_NS_URI;
        }
    }

    public String getPrefix(String namespaceURI) {
        // Not needed in this context.
        return null;
    }

    public Iterator<?> getPrefixes(String namespaceURI) {
        // Not needed in this context.
        return null;
    }
}
