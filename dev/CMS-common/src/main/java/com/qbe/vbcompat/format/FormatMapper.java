package com.qbe.vbcompat.format;

import org.apache.commons.lang.StringUtils;

/**
 * Esta clase mapea los formatos usados por VisualBasic en la función "Format" a los usados por Java en DecimalFormat,
 * y resuelve mapeos no implementados directamente por Java como los de Strings
 * 
 * Documentación del Format en VB:
 * http://msdn.microsoft.com/en-us/library/office/gg251755.aspx
 * http://msdn.microsoft.com/en-us/library/4fb56f4y(v=vs.71).aspx
 * 
 * La documentación no es formal.
 * Para probar como funciona un formato en VB hacer una app como en http://www.vb6.us/tutorials/understanding-vb-format-function-custom-numeric-formats
 * 
 * Uno de los mapeos definidos es de "#,###0.00" a "#,##0.00". Si usamos el formato VB directamente en Java, 50000 lo formatea como 5,0000.00 (!!??!@#!@?!?#).
 * Esto es porque en VB la "," especifica "usá decimal separator ( del Locale )", mientras que en java quiere decir "usá decimal separator en ESA posición".
 *  
 * 
 * @author ramiro
 *
 */
public class FormatMapper {

	/**
	 * Implementa todos los mapeos que figuran en las definiciones de los MQGenéricos
	 * Si no implementa el mapeo, devuelve el formato que recibe
	 * 
	 * @param format
	 * @return
	 */
	public static String mapVBNumberFormat(String format) {
		if ( "#,###0.00".equals(format)) return "#,##0.00";
		if ( "###0.00".equals(format)) return "###0.00";
		return format;
	}

	/**
	 * Formatea el string de acuerdo al formato VB
	 * 
	 * @	Character placeholder. Display a character or a space. 
	 * If the string has a character in the position where the 
	 * at symbol (@) appears in the format string, display it; 
	 * otherwise, display a space in that position. 
	 * Placeholders are filled from right to left unless there 
	 * is an exclamation point character (!) in the format string.
	 * 
	 * @param input
	 * @param vbFormat
	 * @return
	 * @throws FormatterException si no puede manejar el formato
	 */
	public static String formatStringWithVBFormat(String input, String vbFormat) throws FormatterException {
		if ( "@@-@@@@".equals(vbFormat)) return valueToString(input, vbFormat);
		throw new FormatterException("No puedo formatear con el pattern desconocido: " + vbFormat);
	}
	
	/**
	 * Convierte el value a un String aplicando la máscara
     * Por ahora, el único caracter interpretado es @, que significa copiar literalmente el caracter, y si no hay poner un ' '
     * 
     * No soporta caracteres escapeados
	 * @param value
	 * @param vbFormat
	 * @return
	 * @throws FormatterException
	 */
    public static String valueToString(String value, String vbFormat) throws FormatterException
    {
      String string = value != null ? value.toString() : "";
      return convertValueToString(string,vbFormat);
    }
  
    /**
     * @see valueToString(String,String)
     * 
     * @param value
     * @param mask
     * @return
     */
	private static String convertValueToString(String value, String mask)
			{
		//Se agregan espacios a la izquierda en base al tamaño de la máscara
		String valuePad = StringUtils.leftPad(value, mask.length() - 1);
		char placeHolderChar = ' ';
		StringBuffer result = new StringBuffer();
		char valueChar;
		boolean isPlaceHolder;

		int length = mask.length();
		// j recorre la máscara
		// i recorre value
		for (int i = 0, j = 0; j < length; j++) {
			char maskChar = mask.charAt(j);
			if (i < valuePad.length()) {
				isPlaceHolder = false;
				valueChar = valuePad.charAt(i);
			} else {
				isPlaceHolder = true;
				valueChar = placeHolderChar;
			}
			switch (maskChar) {
			case '@':
				if (isPlaceHolder) {
					result.append(placeHolderChar);
				} else {
					result.append(valueChar);
					i++;
				}
				break;
			default:
				result.append(maskChar);
			}
		}
		return result.toString();
	}

}
