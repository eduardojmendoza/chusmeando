package com.qbe.vbcompat.format;

import java.text.DecimalFormat;
import java.text.ParsePosition;

public class VBFixesUtil {

	public static double val(java.lang.String s) {
		java.lang.StringBuffer stringbuffer;
		double d;
		stringbuffer = new StringBuffer();
		int i = s.length();
		for (int j = 0; j < i; j++) {
			char c = s.charAt(j);
			if (c != ' ')
				stringbuffer.append(s.charAt(j));
		}

		d = 0.0D;
		DecimalFormat df = new DecimalFormat();
		java.lang.Number number = df.parse(stringbuffer.toString(), new ParsePosition(0));
		if (number != null) {
			return number.doubleValue();
		} else {
			return d;
		}
	}
}
