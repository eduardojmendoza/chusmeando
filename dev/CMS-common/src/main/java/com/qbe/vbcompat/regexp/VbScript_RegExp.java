package com.qbe.vbcompat.regexp;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author leandrocohn
 *
 */
public class VbScript_RegExp {
	
	protected static Logger logger = Logger.getLogger(VbScript_RegExp.class.getName());

	/**
	 * Atributo para saber si busca la primer coincidencia o todas las que encuentre.
	 */
	private boolean global;

	/**
	 * Atributo para ignorar las mayúsculas
	 */
	private boolean ignoreCase;

	/**
	 * Patrón a buscar.
	 */
	private String pattern;

	public boolean isGlobal() {
		return global;
	}

	public void setGlobal(boolean global) {
		this.global = global;
	}

	public boolean isIgnoreCase() {
		return ignoreCase;
	}

	public void setIgnoreCase(boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * Aplica el patrón que ya tiene cargado con setPattern al string que recibe como parámetro
	 * 
	 * @param wvarParseEvalString - Texto en donde se buscará la coincidencia marcada por el pattern.
	 * @return Una lista de encuentros. Si el global es false, solo devolverá un, en cambio Si está informada con true, devolverá en el MatchCollection, todas las expresiones encontradas. 
	 */
	public MatchCollection Execute(String wvarParseEvalString) {
		//Se valida que el patrón haya sido previamente informado.
		if (pattern == null) {
			return null;
		}
		
		Pattern patternExecute = null;
		if(ignoreCase) {
			patternExecute = Pattern.compile(this.pattern, Pattern.CASE_INSENSITIVE);
		} else {
			patternExecute = Pattern.compile(this.pattern);
		}

		//Se ejecuta
		Matcher matcher = patternExecute.matcher(wvarParseEvalString);
		Match match = null;
		List<Match> arrayMatches = new ArrayList<Match>();
		
		// Se agregan todos los matches en la lista de MatchCollection
		while (matcher.find()) {
			match = new Match(matcher);
			arrayMatches.add(match);
		}
		MatchCollection mColleSalida = new MatchCollection(arrayMatches);
		return mColleSalida;
	}

	/**
	 * @param wvarParseEvalString
	 * @param replacement
	 * @return Devuelve un String con el primer reemplazo, si el global es false. 
	 * o con todos los reemplazos, en caso que el global sea true. 
	 */
	public String Replace(String wvarParseEvalString, String replacement) {
		Pattern replace = null;
		if (ignoreCase) {
			replace = Pattern.compile(this.pattern, Pattern.CASE_INSENSITIVE);
		} else {
			replace = Pattern.compile(this.pattern);
		}

		Matcher matcher = replace.matcher(wvarParseEvalString);

		if (global) {
			return matcher.replaceAll(replacement);
		} else {
			return matcher.replaceFirst(replacement);
		}
	}

	/**
	 * @param wvarParseEvalString
	 * @return Devuelve true en caso que el patrón se haya encontrado por lo menos una vez.
	 */
	public boolean Test(String wvarParseEvalString) {
		if (Execute(wvarParseEvalString).getCount() == 0) {
			return false;
		} else return true;
	}
}
