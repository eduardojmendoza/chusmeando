<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>

<xsl:strip-space elements="Totals"/>

	<xsl:template match="/">
		<xsl:element name="QBETotalsRs">
			<xsl:apply-templates select="*[local-name() = 'QBETotalsRs']/*[local-name() = 'Totals']" />
		</xsl:element>
	</xsl:template>

<xsl:template match="node() | @*">
    <xsl:copy>
        <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
</xsl:template>

<xsl:template match="*[local-name() = 'Totals']">
	<xsl:element name="{local-name()}">
		<xsl:copy-of select="@*"/>
		<xsl:apply-templates select="child::node()[not(self::*[local-name() = 'Totals'])] | @* " />
	</xsl:element>
	<xsl:apply-templates select="*[local-name() = 'Totals']" />
</xsl:template>

</xsl:stylesheet>