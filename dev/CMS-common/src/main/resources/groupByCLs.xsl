<xsl:stylesheet version="2.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:xs="http://www.w3.org/2001/XMLSchema"
 exclude-result-prefixes="xs"
 >

<!-- Ideas de http://stackoverflow.com/questions/2768712/how-to-group-and-sum-values-in-xslt -->

 <xsl:output omit-xml-declaration="no" indent="yes"/>
 <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>

 <xsl:template match="node()|@*">
   <xsl:copy>
    <xsl:apply-templates select="node()|@*"/>
   </xsl:copy>
 </xsl:template>

 <xsl:template match="REGS">
  <xsl:element name="REGS">
   <xsl:for-each-group select="REG" group-by=
    "concat(@NI1 , ',' , @CL1 , ',' , @NI2 , ',' , @CL2 , ',' , @NI3 , ',' , @CL3)">
   <xsl:element name="REG">
			<xsl:element name="NI1"><xsl:value-of select="./@NI1"/></xsl:element>
			<xsl:element name="CL1"><xsl:value-of select="./@CL1"/></xsl:element> 
			<xsl:element name="NI2"><xsl:value-of select="./@NI2"/></xsl:element>
			<xsl:element name="CL2"><xsl:value-of select="./@CL2"/></xsl:element>
			<xsl:element name="NI3"><xsl:value-of select="./@NI3"/></xsl:element>
			<xsl:element name="CL3"><xsl:value-of select="./@CL3"/></xsl:element>
			<xsl:element name="agentID"><xsl:value-of select="./@agentID"/></xsl:element>
			<xsl:element name="agentNo"><xsl:value-of select="./@agentNo"/></xsl:element>
			<xsl:element name="ESTSTR"><xsl:value-of select="./@ESTSTR"/></xsl:element>
			<xsl:element name="NOM"><xsl:value-of select="./@NOM"/></xsl:element>
			<xsl:for-each select="./@*[starts-with(local-name(), 'TOT')]">
				<xsl:variable name="TOT" select="local-name(.)" />
				<xsl:element name="{$TOT}"><xsl:value-of select="sum(current-group()/@*[local-name(.) = $TOT])"/></xsl:element>
			</xsl:for-each>
    </xsl:element>
   </xsl:for-each-group>
  </xsl:element>
 </xsl:template>
</xsl:stylesheet>