<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8" indent="yes" />
	
	<!-- Para evitar que devuelva un NaN cuando en el format-number no está el cliensec* -->
	<xsl:decimal-format name="noNaN" NaN=""/>
	
	<xsl:strip-space elements="Totals"/>
	<xsl:template match="Totals" >
		<xsl:element name="REG">
			<xsl:if test="./@nivel1 !=''">
				<xsl:attribute name="NI1"><xsl:value-of select="./@nivel1"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="./@cliensec1 !=''">
				<xsl:attribute name="CL1"><xsl:value-of select="format-number(./@cliensec1,'000000000','noNaN')"/></xsl:attribute> 
			</xsl:if>
			<xsl:if test="./@nivel2 !=''">
				<xsl:attribute name="NI2"><xsl:value-of select="./@nivel2"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="./@cliensec2 !=''">
				<xsl:attribute name="CL2"><xsl:value-of select="format-number(./@cliensec2,'000000000','noNaN')"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="./@nivel3 !=''">
				<xsl:attribute name="NI3"><xsl:value-of select="./@nivel3"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="./@cliensec3 !=''">
				<xsl:attribute name="CL3"><xsl:value-of select="format-number(./@cliensec3,'000000000','noNaN')"/></xsl:attribute>
			</xsl:if>
			<xsl:attribute name="agentID"><xsl:value-of select="./@agentID"/></xsl:attribute>
			<xsl:attribute name="agentNo"><xsl:value-of select="./@agentNo"/></xsl:attribute>
			<xsl:attribute name="ESTSTR"><xsl:value-of select="./@eststr"/></xsl:attribute>
			<xsl:attribute name="NOM"><xsl:value-of select="./@name"/></xsl:attribute>
			<xsl:for-each select="./@*[starts-with(local-name(), 'TOT')]">
				<xsl:variable name="TOT" select="local-name(.)" />
				<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
			</xsl:for-each>
		
			<xsl:apply-templates select="Totals" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="/">
		<xsl:element name="REGS">
			<xsl:apply-templates select="//Totals" />
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
