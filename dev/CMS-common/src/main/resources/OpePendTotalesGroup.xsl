<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

 <xsl:output omit-xml-declaration="no" indent="yes"/>
 <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>

	<xsl:key name="contacts-by-CL1CL2" match="REG" use="concat(CL1,'_',CL2)"/>
	<xsl:key name="contacts-by-CL1" match="//REGS/REG" use="CL1"/>
	<xsl:template match="/">
		<xsl:element name="EST">
			<xsl:for-each select="//REGS/REG[count(. | key('contacts-by-CL1', CL1)[1]) = 1]">
				<xsl:if test="NI1 !=''">
					<xsl:element name="GO">
						<xsl:attribute name="CLI"><xsl:value-of select="CL1"/></xsl:attribute>
						<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
						<xsl:attribute name="NIV"><xsl:value-of select="NI1"/></xsl:attribute>
 						<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
						<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
							<xsl:variable name="TOT" select="local-name(.)" />
							<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
						</xsl:for-each>
						<xsl:for-each select="key('contacts-by-CL1',CL1)">
							<xsl:for-each select="(current())[count(. | key('contacts-by-CL1CL2', concat(CL1,'_',CL2))[1]) = 1]">
								<xsl:if test="NI2 !=''">
									<xsl:element name="OR">
										<xsl:attribute name="CLI"><xsl:value-of select="CL2"/></xsl:attribute>
										<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
										<xsl:attribute name="NIV"><xsl:value-of select="NI2"/></xsl:attribute>
										<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
										<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
											<xsl:variable name="TOT" select="local-name(.)" />
											<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
										</xsl:for-each>
										<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
											<xsl:if test="NI3 !=''">
												<xsl:element name="PR">
													<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
													<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
													<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
													<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
													<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
														<xsl:variable name="TOT" select="local-name(.)" />
														<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
													</xsl:for-each>
												</xsl:element>
											</xsl:if>
										</xsl:for-each>
									</xsl:element>
								</xsl:if>
								<xsl:if test="NI2 =''">
									<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
										<xsl:if test="NI3 !=''">
											<xsl:element name="PR">
												<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
												<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
												<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
 												<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
												<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
													<xsl:variable name="TOT" select="local-name(.)" />
													<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
												</xsl:for-each>
											</xsl:element>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:element>
				</xsl:if>
				<xsl:if test="(NI1 ='')">
					<xsl:for-each select="key('contacts-by-CL1',CL1)">
						<xsl:for-each select="(current())[count(. | key('contacts-by-CL1CL2', concat(CL1,'_',CL2))[1]) = 1]">
							<xsl:if test="NI2 !=''">
								<xsl:element name="OR">
									<xsl:attribute name="CLI"><xsl:value-of select="CL2"/></xsl:attribute>
									<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
									<xsl:attribute name="NIV"><xsl:value-of select="NI2"/></xsl:attribute>
									<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
									<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
										<xsl:variable name="TOT" select="local-name(.)" />
										<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
									</xsl:for-each>
									 <xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
										<xsl:if test="NI3 !=''">
											<xsl:element name="PR">
												<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
												<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
												<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
												<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
												<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
													<xsl:variable name="TOT" select="local-name(.)" />
													<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
												</xsl:for-each>
											</xsl:element>
										</xsl:if>
									</xsl:for-each>
								</xsl:element>
							</xsl:if>
							<xsl:if test="NI2 =''">
								<xsl:for-each select="key('contacts-by-CL1CL2',concat(CL1,'_',CL2))">
									<xsl:if test="NI3 !=''">
										<xsl:element name="PR">
											<xsl:attribute name="CLI"><xsl:value-of select="CL3"/></xsl:attribute>
											<xsl:attribute name="NOM"><xsl:value-of select="NOM"/></xsl:attribute>
											<xsl:attribute name="NIV"><xsl:value-of select="NI3"/></xsl:attribute>
											<xsl:attribute name="ESTSTR"><xsl:value-of select="ESTSTR"/></xsl:attribute>
											<xsl:for-each select="./*[starts-with(local-name(), 'TOT')]">
												<xsl:variable name="TOT" select="local-name(.)" />
												<xsl:attribute name="{$TOT}"><xsl:value-of select="."/></xsl:attribute>
											</xsl:for-each>
										</xsl:element>
									</xsl:if>
								</xsl:for-each>
							</xsl:if>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:if>
			</xsl:for-each>
        </xsl:element>
	</xsl:template>
</xsl:stylesheet>
