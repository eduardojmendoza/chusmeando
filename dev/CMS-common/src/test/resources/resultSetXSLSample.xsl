    <xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>
     <xsl:template match='z:row'>
      <xsl:element name='ROW'>
          <xsl:element name='CAMPACOD'><xsl:value-of select='@CAMPACOD' /></xsl:element>
          <xsl:element name='AGECOD'><xsl:value-of select='@AGECOD' /></xsl:element>
          <xsl:element name='TELEFONO'><xsl:value-of select='@TELNRO' /></xsl:element>
      </xsl:element>
     </xsl:template>
    <xsl:output cdata-section-elements='CAMPACOD'/>
    <xsl:output cdata-section-elements='AGECOD'/>
    <xsl:output cdata-section-elements='TELNRO'/>
    </xsl:stylesheet>
  