package com.qbe.integration;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class TestQBETotalsFixWithXSL {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	

	@Test
	public void test1106InsisNoNS() throws IOException, XmlDomExtendedException {
		String sourceFileName = "1106INSISActual.xml";
		
		XmlDomExtended fase4 = processXMLFilename(sourceFileName);
		
		XmlDomExtended expected = new XmlDomExtended();
		expected.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("1106INSISActualFIXED.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expected.getDocument(), fase4.getDocument()));
		List<?> allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", expected.getDocument(), fase4.getDocument());

	}

	
	@Test
	public void test1106InsisConNS() throws IOException, XmlDomExtendedException {
		String sourceFileName = "1106QbeTotalsConNS.xml";
		
		XmlDomExtended fase4 = processXMLFilename(sourceFileName);
		
		XmlDomExtended expected = new XmlDomExtended();
		expected.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("1106INSISActualFIXED.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expected.getDocument(), fase4.getDocument()));
		List<?> allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", expected.getDocument(), fase4.getDocument());

	}

	@Test
	public void testArreglaCLIs() throws IOException, XmlDomExtendedException {
		String sourceFileName = "1106CLIsCortos.xml";
		
		XmlDomExtended fase4 = processXMLFilename(sourceFileName);
		
		XmlDomExtended expected = new XmlDomExtended();
		expected.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("1106CLIsCortos_Fixed.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expected.getDocument(), fase4.getDocument()));
		List<?> allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", expected.getDocument(), fase4.getDocument());

	}
	
	@Test
	public void testParaVerResultado() throws IOException, XmlDomExtendedException {
		String sourceFileName = "1106INSIS2daVersion.xml";
		
		XmlDomExtended fase4 = processXMLFilename(sourceFileName);
		
//		XmlDomExtended expected = new XmlDomExtended();
//		expected.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("1106CLIsCortos_Fixed.xml"));
//		XMLUnit.setIgnoreWhitespace(true);
//		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expected.getDocument(), fase4.getDocument()));
//		List<?> allDifferences = myDiff.getAllDifferences();
//		assertEquals(myDiff.toString(), 0, allDifferences.size());
//		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", expected.getDocument(), fase4.getDocument());

	}

	@Test
	@Ignore //FIXME No está andando OK ahora
	public void test1426_01() throws IOException, XmlDomExtendedException {
		String sourceFileName = "1426_01.xml";
		
		XmlDomExtended fase4 = processXMLFilename(sourceFileName);
		
		XmlDomExtended expected = new XmlDomExtended();
		expected.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("1426_01_Fixed.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expected.getDocument(), fase4.getDocument()));
		List<?> allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", expected.getDocument(), fase4.getDocument());

	}
	
	@Test
	@Ignore //FIXME No está andando OK ahora
	public void test1426InsisNoNS() throws IOException, XmlDomExtendedException {
		String sourceFileName = "1426_01.xml";
		
		XmlDomExtended fase4 = processXMLFilename(sourceFileName);
		
		XmlDomExtended expected = new XmlDomExtended();
		expected.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("1426_01_Fixed.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expected.getDocument(), fase4.getDocument()));
		List<?> allDifferences = myDiff.getAllDifferences();
		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", expected.getDocument(), fase4.getDocument());

	}
	
	
	@Test
	public void testPaso1() throws IOException, XmlDomExtendedException {
		String sourceXML = convertStreamToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("1426_01.xml"));
		
		XmlDomExtended rawInsis = new XmlDomExtended();
		rawInsis.loadXML(sourceXML);
		
		XmlDomExtended fase1 = new XmlDomExtended();
		fase1.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		rawInsis.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("pullUpTotals.xsl"), fase1.getDocument(),true);
		
//		System.out.println(XmlDomExtended.marshal(fase1.getDocument().getDocumentElement()));
	}
	
	@Test
	public void testPaso2() throws IOException, XmlDomExtendedException {
		String sourceXML = convertStreamToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("1426_01.xml"));
		
		XmlDomExtended rawInsis = new XmlDomExtended();
		rawInsis.loadXML(sourceXML);
		
		XmlDomExtended fase1 = new XmlDomExtended();
		fase1.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		rawInsis.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("pullUpTotals.xsl"), fase1.getDocument(),true);
		
		XmlDomExtended fase2 = new XmlDomExtended();
		fase2.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase1.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("fixQBETotalsTagNames.xsl"), fase2.getDocument(),true);

//		System.out.println(XmlDomExtended.marshal(fase2.getDocument().getDocumentElement()));
	}
	
	@Test
	public void testPaso3() throws IOException, XmlDomExtendedException {
		String sourceXML = convertStreamToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("1426_01.xml"));
		
		XmlDomExtended rawInsis = new XmlDomExtended();
		rawInsis.loadXML(sourceXML);
		
		XmlDomExtended fase1 = new XmlDomExtended();
		fase1.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		rawInsis.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("pullUpTotals.xsl"), fase1.getDocument(),true);
		
		XmlDomExtended fase2 = new XmlDomExtended();
		fase2.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase1.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("fixQBETotalsTagNames.xsl"), fase2.getDocument(),true);
		
		XmlDomExtended fase3 = new XmlDomExtended();
		fase3.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase2.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("groupByCLs.xsl"), fase3.getDocument(),true);

//		System.out.println(XmlDomExtended.marshal(fase3.getDocument().getDocumentElement()));
	}
	
	@Test
	public void testPaso4() throws IOException, XmlDomExtendedException {
		String sourceXML = convertStreamToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("1426_01.xml"));
		
		XmlDomExtended rawInsis = new XmlDomExtended();
		rawInsis.loadXML(sourceXML);
		
		XmlDomExtended fase1 = new XmlDomExtended();
		fase1.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		rawInsis.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("pullUpTotals.xsl"), fase1.getDocument(),true);
		
		XmlDomExtended fase2 = new XmlDomExtended();
		fase2.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase1.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("fixQBETotalsTagNames.xsl"), fase2.getDocument(),true);
		
		XmlDomExtended fase3 = new XmlDomExtended();
		fase3.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase2.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("groupByCLs.xsl"), fase3.getDocument(),true);
		
		XmlDomExtended fase4 = new XmlDomExtended();
		fase4.loadXML("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		fase3.transformNode(Thread.currentThread().getContextClassLoader().getResourceAsStream("OpePendTotalesGroup.xsl"), fase4.getDocument(),true);

//		System.out.println(XmlDomExtended.marshal(fase4.getDocument().getDocumentElement()));
	}
	
	private XmlDomExtended processXMLFilename(String sourceFileName)
			throws IOException, XmlDomExtendedException {

		String sourceXML = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream(sourceFileName));
		String converted = CommercialStructureConverter.convert(sourceXML);

		XmlDomExtended result = new XmlDomExtended();
		result.loadXML(converted);
		return result;
	}
	
    public String convertStreamToString(InputStream is)
            throws IOException {
        //
        // To convert the InputStream to String we use the
        // Reader.read(char[] buffer) method. We iterate until the
        // Reader return -1 which means there's no more data to
        // read. We use the StringWriter class to produce the string.
        //
        if (is != null) {
            Writer writer = new StringWriter();
 
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {        
            return "";
        }
    }

}
