package com.qbe.services.common.merge;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.CharConversionException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UTFDataFormatException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class MergeTotalsNew_Test {

	/**
	 * @throws XmlException
	 * @throws IOException
	 * @throws SAXException
	 */

	@Before
	public void setUp() throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
	}

	
	@Test
	public void sortCOMTest() throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException {
		
		String xml =	
				"<EST>" +
						"<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"22\">" + 
							"<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\">" + 
								"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
								"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
								"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
							"</OR>" + 
						"</GO>" +
						"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
							"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
							"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
						"</OR>" +
					"</EST>";
		
		String expected = 
					"<EST>" +
						"<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"22\">" + 
							"<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\">" + 
								"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
								"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
								"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
							"</OR>" + 
						"</GO>" +
						"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
							"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
							"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
						"</OR>" +
					"</EST>";
		
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document leftDoc = db.parse(new InputSource(new StringReader(xml)));
		Node estNode = leftDoc.getFirstChild();
		
		MergeTotals.sortCommercialStructureByNOM(estNode);
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(estNode), new StreamResult(writer));
		String sortedXML = writer.getBuffer().toString();

		System.out.println(sortedXML);

		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 expected,
				 //Test
				 sortedXML);

	}
	
	@Ignore
	@Test
	public void sortESTTest() throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException {
		
		String xml =
				"          <EST>" +
				"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTPOL=\"0\"/>" +
				"              <GO CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTPOL=\"0\"/>" +
				"              <G CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\"/>" +
				"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LIDER S.A.\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICE\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
				"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
				"          </EST>";
		
		String expected = "<EST>" +
						"   <PR CLI=\"100223914\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"AON RISK SERVICES AR GENTINA S\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"101863661\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"BUONO OSVALDO HECTOR\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"103628629\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"CANTABRIA S.A.\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"100217481\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"DERKA Y VARGAS S.A.\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"101560963\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"FANTIN RICARDO LUIS\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"101560385\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"FERNANDEZ MARIANA DEL CARMEN\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"000003387\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"GRECCO PAULO ANTONIO\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"100072343\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"HSBC BANK ARGENTINA S.A.\"" +
						"       TOTPOL=\"101\"/>" +
						"   <PR CLI=\"100176735\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"LIDER S.A.\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"101356049\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"NESPECA JULIO HUMBERTO\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"101561120\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"PESADO CASTRO MOTORS S.A.\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"101886076\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"PREDOVIC JORGE ANDRES\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"000002540\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"SINERGIAS S.R.L.\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"103628627\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"SOCIEDAD ITALIANA DE BENEFICE\"" +
						"       TOTPOL=\"0\"/>" +
						"   <PR CLI=\"100365104\"" +
						"       ESTSTR=\"0\"" +
						"       NIV=\"PR\"" +
						"       NOM=\"STALLOCCA GUSTAVO TOMAS\"" +
						"       TOTPOL=\"0\"/>" +
						"</EST>";
		
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document leftDoc = db.parse(new InputSource(new StringReader(xml)));
		Node estNode = leftDoc.getFirstChild();
		
		MergeTotals.sortCommercialStructureByNOM(estNode);
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(estNode), new StreamResult(writer));
		String sortedXML = writer.getBuffer().toString();

		System.out.println(sortedXML);

		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 expected,
				 //Test
				 sortedXML);

	}
	
	
	
	@Test
	public void testMergeTotals0() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
				"</OR>" +
				"<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"22\">" + 
					"<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\">" + 
						"<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
					"</OR>" + 
				"</GO>" + 
			"</EST>";
		 String insis =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</OR>" +
				"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";
		 String resultNode =
			"<EST>" + 
			"	<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"10\">" + 
			"		<PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTPOL=\"10\"/>" + 
			"		<PR CLI=\"000099590\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" TOTPOL=\"0\"/>" + 
			"	</OR>" + 
			"	<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"22\">" + 
			"		<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"	</GO>" + 
			"	<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"9\"/>" + 
			"</EST>";

		 XMLAssert.assertXMLEqual(
			 //Error
			 "No volvió la respuesta esperada",
			 //Control
			 resultNode,
			 //Test
			 MergeTotals.mergeTotalsBalancedTest(ais,insis));
			System.out.println(MergeTotals.mergeTotalsBalancedTest(ais,insis));

	}
	
	@Test
	public void testMergeTotals() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
			"</EST>";
		 String insis =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
				"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";
		 String resultNode =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
				"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";

		 XMLAssert.assertXMLEqual(
			 //Error
			 "No volvió la respuesta esperada",
			 //Control
			 resultNode,
			 //Test
			 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}
	
	@Test
	public void testMergeTotals2() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
				"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
			"</EST>";
		 String resultNode =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
				"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";

		 XMLAssert.assertXMLEqual(
			 //Error
			 "No volvió la respuesta esperada",
			 //Control
			 resultNode,
			 //Test
			 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}


	@Test
	public void testSumImportes() throws MergeException,
			SAXException, IOException {
		//FIXME Consultar a Martín como debe resultar esto. SIG1 qué es, el signo del TOTIMP1? Idem SIG2
		String ais = "<EST><PR TOTPOL=\"587\" MON1=\"$\" SIG1=\"-\" TOTIMP1=\"470.410,88\" MON2=\"\" SIG2=\"\" TOTIMP2=\"0,00\" /></EST>";
		String insis = "<EST><PR TOTPOL=\"-587\" MON1=\"$\" SIG1=\"-\" TOTIMP1=\"-470410.88\" MON2=\"\" SIG2=\"\" TOTIMP2=\"0,00\" /></EST>";
		String expected = "<EST><PR TOTPOL=\"0\" MON1=\"$\" SIG1=\"-\" TOTIMP1=\"0,00\" MON2=\"\" SIG2=\"\" TOTIMP2=\"0,00\" /></EST>";
		String actual = MergeTotals.mergeTotalsBalancedTest(ais,insis);
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expected,
				actual);
	}

	@Test
	public void testMergeTotalsDesordenadoYOrdenadoSegunAIS() throws Exception {
	
		String ais =
				"<EST>" +
					"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
						"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
						"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
					"</OR>" +
					"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</EST>";
	
		String insis =
				"<EST>" +
					"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
					"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
						"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
						"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
					"</OR>" +
				"</EST>";
	
		 String resultNode =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"12\" />" +
			"</EST>";
	
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(ais,insis));

			 //FIXME Cuando esté el ordenamiento, el siguiente tiene que pasar
			 //queda comentado para no subir algo roto
			 
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(insis,ais));
			 System.out.println(MergeTotals.mergeTotalsBalancedTest(insis,ais));
			 
	}

	@Test
	public void testMergeTotalsAttrDesordenados() throws Exception {
	
		String ais =
				"<EST>" +
					"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
						"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
						"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
					"</OR>" +
					"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</EST>";
	
		 String insis =
				"<EST>" +
					"<OR CLI=\"100029438\" NIV=\"OR\" ESTSTR=\"0\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"1\" >" +
						"<PR NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" CLI=\"000099590\" />" +
						"<PR NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" CLI=\"100138267\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
					"</OR>" +
				"</EST>";
	
		 String resultNode =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";
	
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}

	@Test
	public void testNodosRepetidos() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";
		try {
			MergeTotals.mergeTotalsBalancedTest(ais,insis);
			fail("Debería haber saltado una Exception porque no puede haber nodos repetidos.");
		} catch (MergeException e) {
		}
	}

	@Test
	public void testNodoVacio() throws SAXException, IOException, MergeException {
		String ais =
			"<EST />";
		 String insis =
			"<EST />";
		 String resultNode =
			"<EST />";
	
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}

	@Test
	public void testAisTieneMasNodosQueInsis() throws SAXException, IOException, MergeException {
		 String ais =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"4\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</OR>" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";
	
		String insis =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
			"</EST>";
	
		 String resultNode =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"7\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</OR>" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";
	
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}

	@Test(expected=UTFDataFormatException.class)
	@Ignore
	public void testEncodingBug() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
	
		String xml = "<pepe>ó</pepe>";
	
		CharBuffer cb = CharBuffer.wrap(xml);
		CharsetEncoder ce = Charset.forName("ISO-8859-1").newEncoder();
		ByteBuffer bbuffer = ce.encode(cb);
	
		try {
			db.parse(new ByteArrayInputStream(bbuffer.array()));
		} catch (CharConversionException e) {
			//OK, esperado
		}
	
		CharBuffer cb2 = CharBuffer.wrap(xml);
		CharsetEncoder ce2 = Charset.forName("UTF-8").newEncoder();
		ByteBuffer bbuffer2 = ce2.encode(cb2);
	
		try {
			db.parse(new ByteArrayInputStream(bbuffer2.array()));
		} catch (CharConversionException e) {
			fail("Acá no debería haber ocurrido, si el default es UTF-8");
		}
	
	}

	public void testMsg1106() throws SAXException, IOException, MergeException {
		String ais =
			"          <EST>" +
			"            <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\">" +
			"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\"/>" +
			"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LIDER S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICE\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"102\"/>" +
			"          </EST>";
		 String insis =
			"  <EST xmlns:ns2=\"http://cms.services.qbe.com/convertcommstructure\" xmlns:ns3=\"http://www.fadata.bg/Insurance_Messages/v3.0/xml/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
			"    <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\">" +
			"      <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"    </OR>" +
			"    <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"  </EST>";
	
			try {
				MergeTotals.mergeTotalsBalancedTest( ais, insis);
			} catch (MergeException e) {
				fail("No debería haber Errores.");
			}
	}

	@Test
	public void testMsg1106Encoding() throws SAXException, IOException, MergeException {
		String ais =
			"          <EST>" +
			"            <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\">" +
			"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\"/>" +
			"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICE\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"102\"/>" +
			"          </EST>";
		 String insis =
			"  <EST xmlns:ns2=\"http://cms.services.qbe.com/convertcommstructure\" xmlns:ns3=\"http://www.fadata.bg/Insurance_Messages/v3.0/xml/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
			"    <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\">" +
			"      <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"    </OR>" +
			"    <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"  </EST>";
	
			try {
//			 	System.out.println("Default Charset=" + Charset.defaultCharset());
//		    	System.out.println("file.encoding=" + System.getProperty("file.encoding"));
	
				MergeTotals.mergeTotalsBalancedTest( ais, insis );
				//MergeTotals.mergeTotalsNuevo( new String(ais.getBytes(), Charset.forName("ISO-8859-1")), new String(insis.getBytes(), Charset.forName("ISO-8859-1")));
				//MergeTotals.mergeTotalsNuevo( new String(ais.getBytes(), Charset.forName("UTF-8")), new String(insis.getBytes(), Charset.forName("UTF-8")));
			} catch (MergeException e) {
				e.printStackTrace();
				fail("No debería haber Errores.");
			}
	}

	@Test
	public void testMSG1426() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
			"            <OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA  RUBEN NARCISO\" TOTIMP1=\"70147.24\" TOTIMP2=\"3162\">" +
			"              <PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD  ANONIMA\" TOTIMP1=\"61690.66\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"101557888\" ESTSTR=\"1\" NIV=\"PR\" NOM=\"CAMINITI  JUAN CARLOS\" TOTIMP1=\"338.43\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"100749771\" ESTSTR=\"1\" NIV=\"PR\" NOM=\"CIANCIBELLO  ROSA ANA MARIA\" TOTIMP1=\"104.73\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"90959\" ESTSTR=\"1\" NIV=\"PR\" NOM=\"FAELLA  EZEQUIEL MATIAS\" TOTIMP1=\"6465.74\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  RUBEN NARCISO\" TOTIMP1=\"1304.21\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG  CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"3162\"/>" +
			"              <PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENO S AIRES\" TOTIMP1=\"243.47\" TOTIMP2=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  RUBEN NARCISO\" TOTIMP1=\"1027285.89\" TOTIMP2=\"3065.22\"/>" +
			"          </EST>";
		 String insis =
			"	<EST>" +
			"<OR 	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\">" +
			"<PR 	CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENS MARIA DEL PILAR\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENO S AIRES\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\"/>" +
			"</OR>" +
			"<PR 	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\"/>" +
			"</EST>" +
			"";
	
			try {
				MergeTotals.mergeTotalsBalancedTest(ais,insis);
				fail("Debería haber saltado una Exception porque AIS no puede tener menos nodos que INSIS.");
			} catch (MergeException e) {
			}
	}

	@Test
	public void testMSG1309() throws SAXException, IOException, MergeException {
		String ais =
			"          <EST>" +
			"            <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTABI=\"283\" TOTCER=\"537\">" +
			"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTABI=\"283\" TOTCER=\"537\"/>" +
			"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LIDER S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTABI=\"283\" TOTCER=\"537\"/>" +
			"          </EST>";
		 String insis =
			"<EST>" +
			"	<OR 	CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\">" +
			"		<PR 	CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"	</OR>" +
			"	<PR 	CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"</EST>";
	
			try {
				MergeTotals.mergeTotalsBalancedTest(ais,insis);
				fail("Difieren los esquemas, Tendria que saltar un Error.");
			} catch (MergeException e) {
			}
	}

	@Test
	public void testFormato1() throws SAXException, IOException, MergeException {
		 String ais =
			"<EST>" +
					"<PR CLI=\"100138267\" TOTPOL=\"12345.01\" />" +
			"</EST>";
	
		String insis =
			"<EST>" +
					"<PR CLI=\"100138267\" TOTPOL=\"1\" />" +
			"</EST>";
	
		 String resultNode =
			 "<EST>" +
					 "<PR CLI=\"100138267\" TOTPOL=\"12346.01\" />" +
			"</EST>";
	
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}

	@Test
	public void error1403() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
					"<PR      CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0,00\" TOTIMP2=\"0,00\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
					"<PR      CLI=\"101560048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"6450.97\" TOTIMP2=\"0\" />" +
			"</EST>";
		 
		 String resultNode =
			"<EST>" +
					"<PR	  CLI=\"101560048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"6,450.97\" TOTIMP2=\"0\" />" +
					"<PR      CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0,00\" TOTIMP2=\"0,00\" />" +
			"</EST>";
	
		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 resultNode,
				 //Test
				 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}
	
	@Test
	public void aisVacio() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
			"</EST>";
		 String insis =
			"<EST>" +
					"<PR      CLI=\"101560048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"6450.97\" TOTIMP2=\"0\" />" +
			"</EST>";
		 
		 String resultNode =
			"<EST>" +
					"<PR	  CLI=\"101560048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"6,450.97\" TOTIMP2=\"0\" />" +
			"</EST>";
	
		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 resultNode,
				 //Test
				 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}
	
	@Test
	public void error1426Bug755() throws SAXException, IOException, MergeException {
		String ais =
			"<EST/>";
		 String insis =
			"<EST>" + 
			"        <GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"          <OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BERESTOVOY LEON\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BERESTOVOY LEON\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MONTES  MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MONTES  MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"001753654\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RECAITE ANGEL ARTURO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"102498120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OLIJAVETSKY MAURICIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRITOS JUAN JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"050007553\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAILHADEYOLANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRITOS JUAN JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"OSES S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OSES S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MBO S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MBO S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASARES HERNAN GERVASIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASARES HERNAN GERVASIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA CARLOS GASTON\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA CARLOS GASTON\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001755795\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR COMPANY SOCIEDAD ANONIMA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI  MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO  LILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  FERNANDA SOLEDAD\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO  ROSA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HERRERA EDUARDO JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HERRERA EDUARDO JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CARDOSO CLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CARDOSO CLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RAJMILEVICH  MARTA OFELIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAJMILEVICH  MARTA OFELIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PATIÑO  HEBE BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO  HEBE BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRAVIACLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRAVIACLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CAGNONI HERNAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAGNONI HERNAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"000943488\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZAJAVIER MARCELO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"KOCH JUAN JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"KOCH JUAN JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERRARO  DOMINGA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERRARO  DOMINGA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"NOTO SERGIO EMILIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NOTO SERGIO EMILIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"000261007\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MALAMUDLILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BUSSO ROBERTO JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUSSO ROBERTO JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASINELLI MARIO OSCAR\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASINELLI MARIO OSCAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERROL  MARIA FLORENCIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERROL  MARIA FLORENCIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001351485\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR SECURITY S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"103460780\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PERAZZOANALIA  RITA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI  MARIANA INES\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI  MARIANA INES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DIAZ JAVIER\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DIAZ JAVIER\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCHMIDTEDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCHMIDTEDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LUPIS WALTER ORLANDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LUPIS WALTER ORLANDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"004087345\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ROSALESJUAN CARLOS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FOLCO  ROMY ELIZABETH\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FOLCO  ROMY ELIZABETH\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LAJE PILAR FERNANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LAJE PILAR FERNANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOTTA DIEGO GONZALO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOTTA DIEGO GONZALO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ  MIRTA BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ  MIRTA BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GOMEZ BARBERO  CRISTINA DE LA PAZ\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GOMEZ BARBERO  CRISTINA DE LA PAZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DUDKEVICHFEDERICO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DUDKEVICHFEDERICO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIGLIANO  SUSANA SILVIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIGLIANO  SUSANA SILVIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"          <OR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"            <PR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"            <PR CLI=\"005464145\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QUINTAS VICCIA  ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          </OR>" + 
			"        </GO>" + 
			"        <OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"          <PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          <PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          <PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          <PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          <PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"          <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"        </OR>" + 
			"        <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"      </EST>";
		 
		 String expectedNode =
				 "<EST> " + 
						 "<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI  MARIANA INES\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI  MARIANA INES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BERESTOVOY LEON\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BERESTOVOY LEON\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRITOS JUAN JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRITOS JUAN JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"050007553\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAILHADEYOLANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BUSSO ROBERTO JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUSSO ROBERTO JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CAGNONI HERNAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAGNONI HERNAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CARDOSO CLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CARDOSO CLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASARES HERNAN GERVASIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASARES HERNAN GERVASIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASINELLI MARIO OSCAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASINELLI MARIO OSCAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO  ROSA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  FERNANDA SOLEDAD\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI  MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO  LILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001755795\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR COMPANY SOCIEDAD ANONIMA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DIAZ JAVIER\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DIAZ JAVIER\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ  MIRTA BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ  MIRTA BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOTTA DIEGO GONZALO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOTTA DIEGO GONZALO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DUDKEVICHFEDERICO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DUDKEVICHFEDERICO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"102498120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OLIJAVETSKY MAURICIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001753654\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RECAITE ANGEL ARTURO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERRARO  DOMINGA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERRARO  DOMINGA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERROL  MARIA FLORENCIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERROL  MARIA FLORENCIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FOLCO  ROMY ELIZABETH\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FOLCO  ROMY ELIZABETH\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GOMEZ BARBERO  CRISTINA DE LA PAZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GOMEZ BARBERO  CRISTINA DE LA PAZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001351485\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR SECURITY S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"103460780\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PERAZZOANALIA  RITA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HERRERA EDUARDO JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HERRERA EDUARDO JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"KOCH JUAN JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"KOCH JUAN JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LAJE PILAR FERNANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LAJE PILAR FERNANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LUPIS WALTER ORLANDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LUPIS WALTER ORLANDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004087345\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ROSALESJUAN CARLOS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MBO S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MBO S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MONTES  MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MONTES  MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"NOTO SERGIO EMILIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NOTO SERGIO EMILIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"OSES S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OSES S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PATIÑO  HEBE BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO  HEBE BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RAJMILEVICH  MARTA OFELIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAJMILEVICH  MARTA OFELIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCHMIDTEDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCHMIDTEDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIGLIANO  SUSANA SILVIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIGLIANO  SUSANA SILVIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000943488\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZAJAVIER MARCELO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA CARLOS GASTON\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA CARLOS GASTON\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRAVIACLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRAVIACLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"005464145\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QUINTAS VICCIA  ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<OR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"000261007\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MALAMUDLILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "</GO> " + 
						 "<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</EST> ";
	String resultNode = MergeTotals.mergeTotalsBalancedTest(ais,insis);
		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 expectedNode,
				 //Test
				 resultNode
				 );
		 
	}
	@Ignore
	@Test
	public void error1426Bug7552() throws SAXException, IOException, MergeException {
		String ais =
			"<EST/>";
		 String insis =
			"<EST>" + 
			"	<GO  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<OR  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"100221518\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BERESTOVOY LEON\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"100221518\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BERESTOVOY LEON\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101559851\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MONTES MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101559851\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MONTES MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"100045255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"100045255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"001753654\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RECAITE ANGEL ARTURO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"102498120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OLIJAVETSKY MAURICIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101560459\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101560459\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101555497\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRITOS JUAN JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"050007553\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAILHADEYOLANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"101555497\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRITOS JUAN JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101862468\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"OSES S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101862468\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OSES S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101560095\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101560095\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101998829\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MBO S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101998829\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MBO S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000010289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASARES HERNAN GERVASIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000010289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASARES HERNAN GERVASIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000696872\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA CARLOS GASTON\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000696872\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA CARLOS GASTON\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101862843\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001755795\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR COMPANY SOCIEDAD ANONIMA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"101862843\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO LILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA FERNANDA SOLEDAD\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO ROSA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"100040133\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HERRERA EDUARDO JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"100040133\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HERRERA EDUARDO JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101913653\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CARDOSO CLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101913653\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CARDOSO CLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000223246\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RAJMILEVICH MARTA OFELIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000223246\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAJMILEVICH MARTA OFELIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"001293587\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PATIÑO HEBE BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001293587\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO HEBE BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000860917\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRAVIACLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000860917\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRAVIACLAUDIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"001433174\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CAGNONI HERNAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001433174\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAGNONI HERNAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"001861169\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001861169\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"000943488\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZAJAVIER MARCELO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"103304313\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"103304313\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000144361\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"KOCH JUAN JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000144361\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"KOCH JUAN JORGE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"002175904\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERRARO DOMINGA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"002175904\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERRARO DOMINGA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000755289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"NOTO SERGIO EMILIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000755289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NOTO SERGIO EMILIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"050007704\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"050007704\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"000261007\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MALAMUDLILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000417072\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000417072\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"102755393\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BUSSO ROBERTO JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"102755393\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUSSO ROBERTO JOSE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003032331\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASINELLI MARIO OSCAR\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003032331\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASINELLI MARIO OSCAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"104024567\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"104024567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003073255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERROL MARIA FLORENCIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003073255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERROL MARIA FLORENCIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003117082\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001351485\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR SECURITY S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"103460780\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PERAZZOANALIA RITA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"003117082\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003189665\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003189665\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000595947\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000595947\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"002496605\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"002496605\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"001958318\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001958318\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"002682185\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI MARIANA INES\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"002682185\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI MARIANA INES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003607178\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003607178\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003766114\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DIAZ JAVIER\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003766114\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DIAZ JAVIER\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"000201946\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"000201946\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003917568\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCHMIDTEDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003917568\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCHMIDTEDUARDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"004342898\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LUPIS WALTER ORLANDO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"004342898\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LUPIS WALTER ORLANDO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"004087345\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ROSALESJUAN CARLOS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101862630\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101862630\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003963452\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"001078480\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FOLCO ROMY ELIZABETH\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"001078480\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FOLCO ROMY ELIZABETH\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"004685167\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LAJE PILAR FERNANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"004685167\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LAJE PILAR FERNANDA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003729132\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOTTA DIEGO GONZALO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003729132\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOTTA DIEGO GONZALO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"005076713\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"005076713\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"003428237\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ MIRTA BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003428237\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ MIRTA BEATRIZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"101987943\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GOMEZ BARBERO CRISTINA DE LA PAZ\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"101987943\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GOMEZ BARBERO CRISTINA DE LA PAZ\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"005357408\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"005357408\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"005103484\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DUDKEVICHFEDERICO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"005103484\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DUDKEVICHFEDERICO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"005449290\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"005449290\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"005457790\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIGLIANO SUSANA SILVIA\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"005457790\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIGLIANO SUSANA SILVIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<OR  	CLI=\"005464136\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"005464136\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"005464145\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QUINTAS VICCIA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	</GO>" + 
			"	<OR  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
			"	<PR  	CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	<PR  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</OR>" + 
			"	<PR  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
			"	</EST>";
		 
		 String expectedNode =
				 "<EST> " + 
						 "<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<	PR CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO ROSA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA FERNANDA SOLEDAD\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO LILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "</EST> " 
;
		 
	String resultNode = MergeTotals.mergeTotalsBalancedTest(ais,insis);
		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 expectedNode,
				 //Test
				 resultNode);
	}
	
	@Test
	public void testError1426Numero3() throws Exception {
	
		String ais =
				"<EST>" +
				"</EST>";
	
		 String insis =
				"<EST>" + 
				"	<OR  	CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\">" + 
				"	<PR  	CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	k<PR  	CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	k<PR  	CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	k<PR  	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO LILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA FERNANDA SOLEDAD\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO ROSA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	<PR  	CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/>" + 
				"	</OR>" + 
				"	</EST>";
	
		 String expectedNode =
				 "<EST> " + 
						 "<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"> " + 
						 "<PR CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO ROSA ANA MARIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA FERNANDA SOLEDAD\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI MARIA SUSANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO LILIANA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "<PR CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTIMP1=\"0\" TOTIMP2=\"0\"/> " + 
						 "</OR> " + 
						 "</EST> " ;
		 String resultNode = MergeTotals.mergeTotalsBalancedTest(ais,insis);		 
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 expectedNode,
					 //Test
					 resultNode);
	}

	@Test
	public void testError1426Numero4() throws Exception {
	
		String ais =
				"<EST>" +
				"</EST>";
	
		 String insis =
				"<EST>" + 
				"	<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"6.60952287E7\" TOTIMP2=\"0\">" + 
				"		<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"6.60952287E7\" TOTIMP2=\"0\">" + 
				"		</OR>" + 
				"	</GO>" + 
				"</EST>";
	
		 String resultNode =
					"<EST>" + 
							"	<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTIMP1=\"66,095,228.7\" TOTIMP2=\"0\">" + 
							"		<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTIMP1=\"66,095,228.7\" TOTIMP2=\"0\">" + 
							"		</OR>" + 
							"	</GO>" + 
							"</EST>";
	
			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotalsBalancedTest(ais,insis));
	}
}
