package com.qbe.services.fileutils;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class CMSFileUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testResourceExists() throws MalformedURLException, Exception {
		assertFalse("Error: este archivo no existe",CMSFileUtils.resourceExists(new URL("file:///NO_EXISTE_ESTE_ARCHIVO")));
	}

	@Test
	@Ignore
	public void testLoadFile() {
	}

}
