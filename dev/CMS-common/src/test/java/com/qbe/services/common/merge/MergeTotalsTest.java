package com.qbe.services.common.merge;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.CharConversionException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.StreamUtils;
import org.xml.sax.SAXException;


public class MergeTotalsTest {

	/**
	 * @throws XmlException
	 * @throws IOException
	 * @throws SAXException
	 */

	@Before
	public void setUp() throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
	}

	@Test
	public void testMergeTotals() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";
		 String resultNode =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"12\" />" +
			"</EST>";

		 XMLAssert.assertXMLEqual(
			 //Error
			 "No volvió la respuesta esperada",
			 //Control
			 resultNode,
			 //Test
			 MergeTotals.mergeTotals(ais,insis));
	}

	@Test
	public void testSumaTOTOtros() throws MergeException, SAXException, IOException {

		String ais = "<EST><PR TOTABI=\"11\" TOTCER=\"3\"/></EST>";
		String insis = "<EST><PR TOTABI=\"11\" TOTCER=\"4\"/></EST>";
		String expected = "<EST><PR TOTABI=\"22\" TOTCER=\"7\"/></EST>";
		String actual = MergeTotals.mergeTotals(ais,insis);
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expected,
				actual);
	}

	@Test
	public void testSumImportes() throws MergeException,
			SAXException, IOException {
		//FIXME Consultar a Martín como debe resultar esto. SIG1 qué es, el signo del TOTIMP1? Idem SIG2
		String ais = "<EST><PR TOTPOL=\"587\" MON1=\"$\" SIG1=\"-\" TOTIMP1=\"470.410,88\" MON2=\"\" SIG2=\"\" TOTIMP2=\"0,00\" /></EST>";
		String insis = "<EST><PR TOTPOL=\"-587\" MON1=\"$\" SIG1=\"-\" TOTIMP1=\"-470410.88\" MON2=\"\" SIG2=\"\" TOTIMP2=\"0,00\" /></EST>";
		String expected = "<EST><PR TOTPOL=\"0\" MON1=\"$\" SIG1=\"-\" TOTIMP1=\"0,00\" MON2=\"\" SIG2=\"\" TOTIMP2=\"0,00\" /></EST>";
		String actual = MergeTotals.mergeTotals(ais,insis);
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expected,
				actual);
	}
	
	@Ignore
	@Test
	public void testMultilevel() throws MergeException,SAXException, IOException {
	String expected = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("merge1expected.xml"), Charset.forName("UTF-8"));
	String actual = MergeTotals.mergeTotals(
			StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("merge1left.xml"), Charset.forName("UTF-8")),
			StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("merge1right.xml"), Charset.forName("UTF-8")));
	XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expected,
			actual);
	}

	@Test
	public void testMergeTotalsDesordenadoYOrdenadoSegunAIS() throws Exception {

		String ais =
				"<EST>" +
					"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
						"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
						"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
					"</OR>" +
					"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</EST>";

		String insis =
				"<EST>" +
					"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
					"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
						"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
						"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
					"</OR>" +
				"</EST>";

		 String resultNode =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"12\" />" +
			"</EST>";

			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotals(ais,insis));

	}

	@Test
	public void testMergeTotalsAttrDesordenados() throws Exception {

		String ais =
				"<EST>" +
					"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
						"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
						"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
					"</OR>" +
					"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</EST>";

		 String insis =
				"<EST>" +
					"<PR NOM=\"FAELLA RUBEN NARCISO\" CLI=\"100029438\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
					"<OR CLI=\"100029438\" NIV=\"OR\" ESTSTR=\"0\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"1\" >" +
						"<PR NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" CLI=\"000099590\" />" +
						"<PR NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" CLI=\"100138267\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
					"</OR>" +
				"</EST>";

		 String resultNode =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"10\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"10\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"12\" />" +
			"</EST>";

			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotals(ais,insis));
	}

	@Test
	public void testNodosRepetidos() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";
		try {
			MergeTotals.mergeTotals(ais,insis);
			fail("Debería haber saltado una Exception porque no puede haber nodos repetidos.");
		} catch (MergeException e) {
		}
	}

	@Test
	public void testNodoVacio() throws SAXException, IOException, MergeException {
		String ais =
			"<EST />";
		 String insis =
			"<EST />";
		 String resultNode =
			"<EST />";

			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotals(ais,insis));
	}

	@Test
	public void testMergeParecidos() throws SAXException, IOException, MergeException {
		String ais =
				"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
				"</OR>" +
				"</EST>";
			 String insis =
				"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
					"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
				"</OR>" +
				"</EST>";
		try {
			MergeTotals.mergeTotals(ais,insis);
			fail("Debería haber saltado una Exception porque las estructuras difieren.");
		} catch (MergeException e) {
		}
	}

	@Test
	public void testAisTieneMasNodosQueInsis() throws SAXException, IOException, MergeException {
		 String ais =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"4\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</OR>" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";

		String insis =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
			"</EST>";

		 String resultNode =
			"<EST>" +
				"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"7\">" +
					"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"</OR>" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</EST>";

			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotals(ais,insis));
	}

	@Test
	public void testAisTieneMenosNodosQueInsis() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"1\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";

			try {
				MergeTotals.mergeTotals(ais,insis);
				fail("Debería haber saltado una Exception porque AIS no puede tener menos nodos que INSIS.");
			} catch (MergeException e) {
			}
	}

	@Test
	public void testInsisTieneNodosQueAis() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"1\">" +
				"<PR CLI=\"000099590\" NOM=\"DISTRIBUCIONES RIZZI S.A.\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"0\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"3\" />" +
			"</EST>";
		 String insis =
			"<EST>" +
			"<OR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"OR\" ESTSTR=\"0\" TOTPOL=\"9\">" +
				"<PR CLI=\"100138267\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</OR>" +
			"<PR CLI=\"100029438\" NOM=\"FAELLA RUBEN NARCISO\" NIV=\"PR\" ESTSTR=\"0\" TOTPOL=\"9\" />" +
			"</EST>";

			try {
				MergeTotals.mergeTotals(ais,insis);
				fail("Debería haber saltado una Exception porque INSIS no puede tener nodos que AIS no tenga.");
			} catch (MergeException e) {
			}
	}

	@Test
	public void testEncodingBug() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		String xml = "<pepe>ó</pepe>";

		CharBuffer cb = CharBuffer.wrap(xml);
		CharsetEncoder ce = Charset.forName("ISO-8859-1").newEncoder();
		ByteBuffer bbuffer = ce.encode(cb);

		try {
			db.parse(new ByteArrayInputStream(bbuffer.array()));
		} catch (CharConversionException e) {
			//OK, esperado
		}

		CharBuffer cb2 = CharBuffer.wrap(xml);
		CharsetEncoder ce2 = Charset.forName("UTF-8").newEncoder();
		ByteBuffer bbuffer2 = ce2.encode(cb2);

		try {
			db.parse(new ByteArrayInputStream(bbuffer2.array()));
		} catch (CharConversionException e) {
			fail("Acá no debería haber ocurrido, si el default es UTF-8");
		}

	}

	public void testMsg1106() throws SAXException, IOException, MergeException {
		String ais =
			"          <EST>" +
			"            <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\">" +
			"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\"/>" +
			"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LIDER S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICE\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"102\"/>" +
			"          </EST>";
		 String insis =
			"  <EST xmlns:ns2=\"http://cms.services.qbe.com/convertcommstructure\" xmlns:ns3=\"http://www.fadata.bg/Insurance_Messages/v3.0/xml/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
			"    <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\">" +
			"      <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"    </OR>" +
			"    <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"  </EST>";

			try {
				MergeTotals.mergeTotals( ais, insis);
			} catch (MergeException e) {
				fail("No debería haber Errores.");
			}
	}


	@Test
	public void testMsg1106Encoding() throws SAXException, IOException, MergeException {
		String ais =
			"          <EST>" +
			"            <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\">" +
			"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"101\"/>" +
			"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICE\" TOTPOL=\"0\"/>" +
			"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"102\"/>" +
			"          </EST>";
		 String insis =
			"  <EST xmlns:ns2=\"http://cms.services.qbe.com/convertcommstructure\" xmlns:ns3=\"http://www.fadata.bg/Insurance_Messages/v3.0/xml/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
			"    <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\">" +
			"      <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"      <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"    </OR>" +
			"    <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"  </EST>";

			try {
//			 	System.out.println("Default Charset=" + Charset.defaultCharset());
//		    	System.out.println("file.encoding=" + System.getProperty("file.encoding"));

				MergeTotals.mergeTotals( ais, insis );
				//MergeTotals.mergeTotals( new String(ais.getBytes(), Charset.forName("ISO-8859-1")), new String(insis.getBytes(), Charset.forName("ISO-8859-1")));
				//MergeTotals.mergeTotals( new String(ais.getBytes(), Charset.forName("UTF-8")), new String(insis.getBytes(), Charset.forName("UTF-8")));
			} catch (MergeException e) {
				e.printStackTrace();
				fail("No debería haber Errores.");
			}
	}

	@Test
	public void testMSG1426() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
			"            <OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA  RUBEN NARCISO\" TOTIMP1=\"70147.24\" TOTIMP2=\"3162\">" +
			"              <PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD  ANONIMA\" TOTIMP1=\"61690.66\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"101557888\" ESTSTR=\"1\" NIV=\"PR\" NOM=\"CAMINITI  JUAN CARLOS\" TOTIMP1=\"338.43\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"100749771\" ESTSTR=\"1\" NIV=\"PR\" NOM=\"CIANCIBELLO  ROSA ANA MARIA\" TOTIMP1=\"104.73\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"90959\" ESTSTR=\"1\" NIV=\"PR\" NOM=\"FAELLA  EZEQUIEL MATIAS\" TOTIMP1=\"6465.74\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  RUBEN NARCISO\" TOTIMP1=\"1304.21\" TOTIMP2=\"0\"/>" +
			"              <PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG  CARLOS ENRIQUE\" TOTIMP1=\"0\" TOTIMP2=\"3162\"/>" +
			"              <PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENO S AIRES\" TOTIMP1=\"243.47\" TOTIMP2=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  RUBEN NARCISO\" TOTIMP1=\"1027285.89\" TOTIMP2=\"3065.22\"/>" +
			"          </EST>";
		 String insis =
			"	<EST>" +
			"<OR 	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\">" +
			"<PR 	CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENS MARIA DEL PILAR\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENO S AIRES\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTPOL=\"0\"/>" +
			"<PR 	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\"/>" +
			"</OR>" +
			"<PR 	CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\"/>" +
			"</EST>" +
			"";

			try {
				MergeTotals.mergeTotals(ais,insis);
				fail("Debería haber saltado una Exception porque AIS no puede tener menos nodos que INSIS.");
			} catch (MergeException e) {
			}
	}

	@Test
	public void testMSG1309() throws SAXException, IOException, MergeException {
		String ais =
			"          <EST>" +
			"            <OR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTABI=\"283\" TOTCER=\"537\">" +
			"              <PR CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"103628629\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTABRIA S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"100217481\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKA Y VARGAS S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101560963\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FANTIN RICARDO LUIS\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101560385\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MARIANA DEL CARMEN\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTABI=\"283\" TOTCER=\"537\"/>" +
			"              <PR CLI=\"100176735\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LIDER S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101356049\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NESPECA JULIO HUMBERTO\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101561120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PESADO CASTRO MOTORS S.A.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"              <PR CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTABI=\"0\" TOTCER=\"0\"/>" +
			"            </OR>" +
			"            <PR CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTABI=\"283\" TOTCER=\"537\"/>" +
			"          </EST>";
		 String insis =
			"<EST>" +
			"	<OR 	CLI=\"100072343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\">" +
			"		<PR 	CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"103628627\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ITALIANA DE BENEFICENCIA EN BUE NOS AIRES\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"100223914\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"AON RISK SERVICES AR GENTINA S.A.\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"101863661\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUONO OSVALDO HECTOR\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"100365104\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"STALLOCCA GUSTAVO TOMAS\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"101886076\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PREDOVIC JORGE ANDRES\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"000002540\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SINERGIAS S.R.L.\" TOTPOL=\"0\"/>" +
			"		<PR 	CLI=\"000003387\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GRECCO PAULO ANTONIO\" TOTPOL=\"0\"/>" +
			"	</OR>" +
			"	<PR 	CLI=\"100072343\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HSBC BANK ARGENTINA S.A.\" TOTPOL=\"0\"/>" +
			"</EST>";

			try {
				MergeTotals.mergeTotals(ais,insis);
				fail("Difieren los esquemas, Tendria que saltar un Error.");
			} catch (MergeException e) {
			}
	}

	@Test
	public void testFormato1() throws SAXException, IOException, MergeException {
		 String ais =
			"<EST>" +
					"<PR CLI=\"100138267\" TOTPOL=\"12345.01\" />" +
			"</EST>";

		String insis =
			"<EST>" +
					"<PR CLI=\"100138267\" TOTPOL=\"1\" />" +
			"</EST>";

		 String resultNode =
			 "<EST>" +
					 "<PR CLI=\"100138267\" TOTPOL=\"12346.01\" />" +
			"</EST>";

			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotals(ais,insis));
	}

	@Test
	@Ignore //FIXME Revisar si el expected está bien
	public void testFormato2() throws SAXException, IOException, MergeException {
		 String ais =
			"<EST>" + 
			"	<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"22\">" + 
			"		<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ASESORES SEGUROENLIN EA S.A.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ASESORES SEGUROENLIN EA S.A.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI MARIANA INES\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI MARIANA INES\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"102796154\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BENCARDINO MONICA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"102796154\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BENCARDINO MONICA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BERESTOVOY LEON\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BERESTOVOY LEON\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001644427\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BLASCO ALEJO MIGUEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001644427\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BLASCO ALEJO MIGUEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRITOS JUAN JOSE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRITOS JUAN JOSE\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"050007553\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAILHADE YOLANDA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BUSSO ROBERTO JOSE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUSSO ROBERTO JOSE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CAGNONI HERNAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAGNONI HERNAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CARDOSO CLAUDIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CARDOSO CLAUDIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASARES HERNAN GERVASIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASARES HERNAN GERVASIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASINELLI MARIO OSCAR\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASINELLI MARIO OSCAR\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100168919\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASTRO ERNESTO ALFREDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100168919\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASTRO ERNESTO ALFREDO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUR\" TOTPOL=\"21\">" + 
			"			<PR CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO ROSA ANA MARIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUR\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA FERNANDA SOLEDAD\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"21\"/>" + 
			"			<PR CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI MARIA SUSANA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO LILIANA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"102042987\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PODESTA SANTIAGO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRI JAQUELINE NATALIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA D E CARDIOL\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001755795\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR COMPANY SOCIEDAD ANONIMA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DIAZ JAVIER\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DIAZ JAVIER\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ MIRTA BEATRIZ\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ MIRTA BEATRIZ\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOTTA DIEGO GONZALO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOTTA DIEGO GONZALO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DUDKEVICH FEDERICO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DUDKEVICH FEDERICO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENS MARIA DEL PILAR\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENO S AIRES\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERNANDEZ MANERO CARLOS ALBER\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MANERO CARLOS ALBER\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"102498120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OLIJAVETSKY MAURICIO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"001753654\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RECAITE ANGEL ARTURO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERRARO DOMINGA SUSANA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERRARO DOMINGA SUSANA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERROL MARIA FLORENCIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERROL MARIA FLORENCIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FOLCO ROMY ELIZABETH\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FOLCO ROMY ELIZABETH\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GOMEZ BARBERO CRISTINA DE LA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GOMEZ BARBERO CRISTINA DE LA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001351485\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR SECURITY S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"103460780\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PERAZZO ANALIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HERRERA EDUARDO JORGE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HERRERA EDUARDO JORGE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000032343\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"IÑARRA IRAEGUI MACARENA\" TOTPOL=\"0\"/>" + 
			"		<OR CLI=\"001097297\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"KEPPES LEONARDO NICOLAS\" TOTPOL=\"0\"/>" + 
			"		<OR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"KOCH JUAN JORGE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"KOCH JUAN JORGE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LAJE PILAR FERNANDA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LAJE PILAR FERNANDA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LUPIS WALTER ORLANDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LUPIS WALTER ORLANDO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004087345\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ROSALES JUAN CARLOS\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MARTORELLLI ALEJANDRO FRANCIS\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MARTORELLLI ALEJANDRO FRANCIS\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MBO S.A.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MBO S.A.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MONTES MARIA SUSANA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MONTES MARIA SUSANA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"NOTO SERGIO EMILIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NOTO SERGIO EMILIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000295647\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ORONA HUGO ALBERTO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000295647\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORONA HUGO ALBERTO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"OSES S.R.L.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OSES S.R.L.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PATIÑO HEBE BEATRIZ\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO HEBE BEATRIZ\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PLATE ASESORES DE SE GUROS S.A\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PLATE ASESORES DE SE GUROS S.A\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RAJMILEVICH MARTA OFELIA\" TOTPOL=\"1\">" + 
			"			<PR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAJMILEVICH MARTA OFELIA\" TOTPOL=\"1\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001198878\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SALVATTO CARLOS DANIEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001198878\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SALVATTO CARLOS DANIEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCHMIDT EDUARDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCHMIDT EDUARDO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIGLIANO SUSANA SILVIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIGLIANO SUSANA SILVIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SIRI JAQUELINE NATALIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRI JAQUELINE NATALIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000943488\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA JAVIER MARCELO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA CARLOS GASTON\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA CARLOS GASTON\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRAVIA CLAUDIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRAVIA CLAUDIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRUCCO GUILLERMO ROBERTO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRUCCO GUILLERMO ROBERTO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"UNO CORREDORES DE SE GUROS SRL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005464145\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QUINTAS VICCIA ANA MARIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"UNO CORREDORES DE SE GUROS SRL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"VAN ZANDWEGHE CARLOS ALBERTO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000261007\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MALAMUD LILIANA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"VAN ZANDWEGHE CARLOS ALBERTO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"	</GO>" + 
			"	<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\">" + 
			"		<PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTPOL=\"0\"/>" + 
			"		<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"0\"/>" + 
			"		<PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTPOL=\"0\"/>" + 
			"		<PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENS MARIA DEL PILAR\" TOTPOL=\"0\"/>" + 
			"		<PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTPOL=\"0\"/>" + 
			"		<PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENO S AIRES\" TOTPOL=\"0\"/>" + 
			"	</OR>" + 
			"	<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA RUBEN NARCISO\" TOTPOL=\"21\"/>" + 
			"</EST>";

		String insis =
			"<EST>" + 
			"	<GO CLI=\"100029438\" ESTSTR=\"0\" NIV=\"GO\" NOM=\"RUBEN NARCISO FAELLA\" TOTPOL=\"7\">" + 
			"		<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RUBEN NARCISO FAELLA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003783374\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NECALOJ S.R.L\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003883708\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MIGUENSMARIA DEL PILAR\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"101555163\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QBE SEGUROS LA BUENOS AIRES\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BERESTOVOY LEON\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100221518\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BERESTOVOY LEON\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MONTES  MARIA SUSANA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101559851\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MONTES  MARIA SUSANA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100045255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERNANDEZ MANERO CARLOS ALBERTO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"001753654\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RECAITE ANGEL ARTURO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"102498120\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OLIJAVETSKY MAURICIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101560459\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIARRETTA LUCIO EDUARDO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRITOS JUAN JOSE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"050007553\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAILHADEYOLANDA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"101555497\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRITOS JUAN JOSE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"OSES S.R.L.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101862468\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"OSES S.R.L.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101560095\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PLATE ASESORES DE SEGUROS S.A.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MBO S.A.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101998829\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MBO S.A.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASARES HERNAN GERVASIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000010289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASARES HERNAN GERVASIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA CARLOS GASTON\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000696872\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA CARLOS GASTON\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001755795\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR COMPANY SOCIEDAD ANONIMA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"101862843\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DERKACZ JUAN NICOLAS\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTPOL=\"7\">" + 
			"			<PR CLI=\"004084048\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TAISHAN MOTORS S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004084045\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MAXNA S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004084035\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FENG TAI S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004136897\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ORGANIZACIÓN SUR S.A\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"102465732\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SOCIEDAD ARGENTINA DE CARDIOLOGIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004241476\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FRE-BAL S.R.L.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004251628\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAMOS MEJIA MOTOR SA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"002914758\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUR-CAM S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004496243\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GARGIULO NATALIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004363796\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CUCCIOLETTA RICARDO FABIAN\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RUBEN NARCISO FAELLA\" TOTPOL=\"7\"/>" + 
			"			<PR CLI=\"000814415\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"COVER GROUP ASESORES DE SEGUROS S.A\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000292567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GIUSTI  MARIA SUSANA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"001511024\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEZCANO  LILIANA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"001214356\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA  FERNANDA SOLEDAD\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"100749771\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CIANCIBELLO  ROSA ANA MARIA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"002631373\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LICASTRO FRANCISCO ALEJANDRO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000002857\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"IANZITO FRANCO DARIO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000090959\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FAELLA EZEQUIEL MATIAS\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"HERRERA EDUARDO JORGE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"100040133\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"HERRERA EDUARDO JORGE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CARDOSO CLAUDIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101913653\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CARDOSO CLAUDIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RAJMILEVICH  MARTA OFELIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000223246\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RAJMILEVICH  MARTA OFELIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"PATIÑO  HEBE BEATRIZ\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001293587\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PATIÑO  HEBE BEATRIZ\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRAVIACLAUDIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000860917\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRAVIACLAUDIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CAGNONI HERNAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001433174\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAGNONI HERNAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001861169\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZA ALEJANDRO PABLO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000943488\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SPERANZAJAVIER MARCELO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"103304313\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"TRUCCOGUILLERMO ROBERTO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"KOCH JUAN JORGE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000144361\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"KOCH JUAN JORGE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERRARO  DOMINGA SUSANA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"002175904\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERRARO  DOMINGA SUSANA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"NOTO SERGIO EMILIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000755289\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"NOTO SERGIO EMILIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"050007704\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"VAN ZANDWEGHECARLOS ALBERTO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"000261007\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MALAMUDLILIANA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101878267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ABELENDA GUSTAVO DANIEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000417072\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LOPEZ HECTOR RUBEN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BUSSO ROBERTO JOSE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"102755393\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BUSSO ROBERTO JOSE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CASINELLI MARIO OSCAR\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003032331\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CASINELLI MARIO OSCAR\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"104024567\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LEIVA JUAN ENRIQUE\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FERROL  MARIA FLORENCIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003073255\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FERROL  MARIA FLORENCIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001351485\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CAR SECURITY S.A.\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"103460780\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"PERAZZOANALIA  RITA\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"003117082\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUEVARA TRAVERSO ALFREDO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003189665\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ ERNESTO FABIAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000595947\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"CANTAMESSA RUBEN DARIO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"002496605\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI NORBERTO OMAR\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001958318\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"MARTORELLLI ALEJANDRO FRANCISCO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BELUARDI  MARIANA INES\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"002682185\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BELUARDI  MARIANA INES\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003607178\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BRUNO MANASLISKI RAUL FABIAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DIAZ JAVIER\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003766114\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DIAZ JAVIER\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"000201946\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SUDROT CARLOS ESTEBAN\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCHMIDTEDUARDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003917568\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCHMIDTEDUARDO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LUPIS WALTER ORLANDO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"004342898\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LUPIS WALTER ORLANDO\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"004087345\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ROSALESJUAN CARLOS\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101862630\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SANCHEZ SERGIO DANIEL\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003963452\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SIRIJAQUELINE NATALIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"FOLCO  ROMY ELIZABETH\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"001078480\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FOLCO  ROMY ELIZABETH\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"LAJE PILAR FERNANDA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"004685167\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"LAJE PILAR FERNANDA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOTTA DIEGO GONZALO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003729132\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOTTA DIEGO GONZALO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005076713\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ANIDO PABLO ALEJANDRO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DOMINGUEZ  MIRTA BEATRIZ\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"003428237\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DOMINGUEZ  MIRTA BEATRIZ\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GOMEZ BARBERO  CRISTINA DE LA PAZ\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"101987943\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GOMEZ BARBERO  CRISTINA DE LA PAZ\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005357408\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"ASESORES SEGUROENLINEA S.A.\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"DUDKEVICHFEDERICO\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005103484\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"DUDKEVICHFEDERICO\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005449290\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"GUIDOBONO FEDERICO ANDRES\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"SCIGLIANO  SUSANA SILVIA\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005457790\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"SCIGLIANO  SUSANA SILVIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"		<OR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTPOL=\"0\">" + 
			"			<PR CLI=\"005464136\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"UNO CORREDORES DE SEGUROS SRL\" TOTPOL=\"0\"/>" + 
			"			<PR CLI=\"005464145\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"QUINTAS VICCIA  ANA MARIA\" TOTPOL=\"0\"/>" + 
			"		</OR>" + 
			"	</GO>" + 
			"	<OR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"OR\" NOM=\"RUBEN NARCISO FAELLA\" TOTPOL=\"0\">" + 
			"		<PR CLI=\"100138267\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"BANCO MACRO SOCIEDAD ANONIMA\" TOTPOL=\"0\"/>" + 
			"		<PR CLI=\"100029438\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"RUBEN NARCISO FAELLA\" TOTPOL=\"0\"/>" + 
			"	</OR>" + 
			"</EST>";

		 String resultNode =
			 "<EST>" +
					 "<PR CLI=\"100138267\" TOTPOL=\"234,567.02\" />" +
			"</EST>";

			 XMLAssert.assertXMLEqual(
					 //Error
					 "No volvió la respuesta esperada",
					 //Control
					 resultNode,
					 //Test
					 MergeTotals.mergeTotals(ais,insis));
	}
	@Ignore
	@Test
	public void testErrorOSB() throws MergeException, SAXException, IOException {
	String expected = StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("mergeRESULTADO.xml"), Charset.forName("UTF-8"));
	String actual = MergeTotals.mergeTotals(
			StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("mergeAIS.xml"), Charset.forName("UTF-8")),
			StreamUtils.copyToString(Thread.currentThread().getContextClassLoader().getResourceAsStream("mergeINSIS.xml"), Charset.forName("UTF-8")));
	XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expected,
			actual);
	}
	
	@Test
	public void error1403() throws SAXException, IOException, MergeException {
		String ais =
			"<EST>" +
					"<PR      CLI=\"101560047\" ESTSTR=\"0\" MON1=\"\" MON2=\"\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" SIG1=\"\" SIG2=\"\" TOTIMP1=\"0,00\" TOTIMP2=\"0,00\"/>" +
			"</EST>";
		 String insis =
			"<EST>" +
					"<PR      CLI=\"101560047\" ESTSTR=\"0\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" TOTIMP1=\"6450.97\" TOTIMP2=\"0\"/>" +
			"</EST>";
		 
		 String resultNode =
			"<EST>" +
					"<PR CLI=\"101560047\" ESTSTR=\"0\" MON1=\"\" MON2=\"\" NIV=\"PR\" NOM=\"FREIDENBERG CARLOS ENRIQUE\" SIG1=\"\" SIG2=\"\" TOTIMP1=\"6.450,97\" TOTIMP2=\"0,00\"/>" +
			"</EST>";

		 XMLAssert.assertXMLEqual(
				 //Error
				 "No volvió la respuesta esperada",
				 //Control
				 resultNode,
				 //Test
				 MergeTotals.mergeTotals(ais,insis));
	}

}
