package com.qbe.services.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

import diamondedge.ado.AdoConst;
import diamondedge.ado.AdoError;
import diamondedge.ado.AdoUtil;
import diamondedge.ado.Parameter;
import diamondedge.util.Variant;

/**
 * 
 * http://www.cubrid.org/analyzing_jdbc_logs
 * 
 */
public class InvokeSPExampleTest {

	//Cualquiera sirve xq en dev todos los "UDLs" apuntan a la HSQLDB
	final String udlName = "camA_OficinaVirtual.udl";

	@Before
	public void setUp() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			QBESchema.createQBESchema();
		}
	}

	@After
	public void tearDown() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			QBESchema.shutdownQBESchema();
		}
	}

	@Test
	@Ignore
	public void testJDBC() throws SQLException, CurrentProfileException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		System.out.println("En profile: " + CurrentProfile.getProfileName());
		JDBCConnectionFactory cf = new JDBCConnectionFactory();
		
		Connection con = cf.getJDBCConnection(udlName); 
		con.setAutoCommit(true); //Lo mismo que hace el diamodedge ADO
		testSPSNCV_REL_PORTAL_COMERC_SELECT(con);
		testSPSNCV_SIFTGRAL(con);
		testSPSNCV_NROCOT(con);
		con.close();
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private void testSPSNCV_REL_PORTAL_COMERC_SELECT(Connection con)
			throws SQLException {
		CallableStatement ps = con.prepareCall("{call SPSNCV_REL_PORTAL_COMERC_SELECT(?,?)}");
        ps.setString(1, "ARMA");
        ps.setString(2, "AUS1");
        ps.setMaxRows(0);
        boolean result = ps.execute();
        if (result) {
        } else {
        	int uc = ps.getUpdateCount();
        	result = ps.getMoreResults();
        }
        ResultSet cursor = ps.getResultSet();
        boolean habia = false;
        while ( cursor.next()) {
			String portal = cursor.getString("PORTAL");
			String ramopcod = cursor.getString("RAMOPCOD");
			assertEquals("ARMA", portal);
			assertEquals("AUS1", ramopcod);
			habia = true;
        }
        assertTrue(habia);
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private void testSPSNCV_NROCOT(Connection con)
			throws SQLException {
		CallableStatement ps = con.prepareCall("{call SPSNCV_NROCOT(?)}");
        ps.setString(1, "S");
        ps.setMaxRows(0);
        boolean result = ps.execute();
        if (result) {
        } else {
        	int uc = ps.getUpdateCount();
        	result = ps.getMoreResults();
        }
        ResultSet cursor = ps.getResultSet();
        boolean habia = false;
        while ( cursor.next()) {
			int nrocot = cursor.getInt("NROCOT");
			assertEquals(2049089, nrocot);
			habia = true;
        }
        assertTrue(habia);
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	private void testSPSNCV_SIFTGRAL(Connection con)
			throws SQLException {
		CallableStatement ps = con.prepareCall("{call SPSNCV_SIFTGRAL(?)}");
        ps.setString(1, "LOGLEVEL");
        ps.setMaxRows(0);
        boolean result = ps.execute();
        if (result) {
        } else {
        	int uc = ps.getUpdateCount();
        	result = ps.getMoreResults();
        }
        ResultSet cursor = ps.getResultSet();
        boolean habia = false;
        while ( cursor.next()) {
			String paramStr = cursor.getString("PARAMSTR");
			int paramnum = cursor.getInt("PARAMNUM");
			assertEquals("SEVERE", paramStr);
			assertEquals(9, paramnum);
			habia = true;
        }
        assertTrue(habia);
	}


}
