package com.qbe.services.db;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.mchange.v2.c3p0.PooledDataSource;
import com.qbe.services.common.CurrentProfile;
import com.qbe.services.common.CurrentProfileException;

public class JDBCConnectionFactoryTest {

	@Before
	public void setUp() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			QBESchema.createQBESchema();
		}
	}

	@After
	public void tearDown() throws Exception {
		if ( CurrentProfile.getProfileName().equals("dev")) {
			QBESchema.shutdownQBESchema();
		}
	}

	@Test
	@Ignore
	public void testgetDBConnection() throws Exception {
		System.out.println("En profile: " + CurrentProfile.getProfileName());
		JDBCConnectionFactory cf = new JDBCConnectionFactory();
		String udlName = "camA_OficinaVirtual.udl";
		
		JDBCConnectionFactory.logDataSourceState(udlName);
		// Debe ser maxPoolSize + 1 para probar el caso de pool exhausted
		for (int i = 0; i < 51; i++) {
			Connection con = cf.getJDBCConnection(udlName); 
			//http://stackoverflow.com/questions/3668506/efficient-sql-test-query-or-validation-query-that-will-work-across-all-or-most
			//http://stackoverflow.com/questions/591518/how-to-see-all-the-tables-in-an-hsqldb-database
			PreparedStatement ps  = con.prepareStatement("SELECT * FROM INFORMATION_SCHEMA.TABLES");
			ps.execute();
			ResultSet rs = ps.getResultSet();
			while ( rs.next() ) {
				rs.getString("TABLE_NAME");
			}
			ps.close();
			con.close();
		}
		System.out.println("OK");
		//1 seg xq a veces tarda el devolver la última conn al pool
		Thread.sleep(1*1000);
		JDBCConnectionFactory.logDataSourceState(udlName);
		int busyCon = JDBCConnectionFactory.getDataSourceBusyConnections(udlName);
		assertTrue("Quedaron conexiones colgadas fuera del pool", busyCon == 0);
	}

	@Test
	public void testJdbcTemplateConMgmt() throws Exception {
		System.out.println("En profile: " + CurrentProfile.getProfileName());
		String udlName = "camA_OficinaVirtual.udl";
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCConnectionFactory.getDataSourceForUDLName(udlName));
		
		// Debe ser maxPoolSize + 1 para probar el caso de pool exhausted
		for (int i = 0; i < 51; i++) {
			List<Map<String,Object>> results = jdbcTemplate.queryForList("SELECT * FROM INFORMATION_SCHEMA.TABLES"); 
			//http://stackoverflow.com/questions/3668506/efficient-sql-test-query-or-validation-query-that-will-work-across-all-or-most
			//http://stackoverflow.com/questions/591518/how-to-see-all-the-tables-in-an-hsqldb-database
			for (Map<String, Object> row : results) {
				row.get("TABLE_NAME");
			}
		}
		System.out.println("OK");
		//1 seg xq a veces tarda el devolver la última conn al pool
		Thread.sleep(1*1000);
		JDBCConnectionFactory.logDataSourceState(udlName);
		int busyCon = JDBCConnectionFactory.getDataSourceBusyConnections(udlName);
		assertTrue("Quedaron conexiones colgadas fuera del pool", busyCon == 0);
	}


	/**
	 * Prueba un insert via jdbcTemplate
	 * FIXME Acomodarlo para que testee sobre SQLServer
	 * 
	 * @throws Exception
	 */
	@Test
	public void testJdbcTemplateInsert() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("dev"));
		System.out.println("En profile: " + CurrentProfile.getProfileName());
		String udlName = "camA_OficinaVirtual.udl";
		final String parannum = "99";
		final String paramstr = "abc";
		final String parametr = "def";
		
		JdbcTemplate insertjdbcTemplate = new JdbcTemplate(JDBCConnectionFactory.getDataSourceForUDLName(udlName));
		int count = insertjdbcTemplate.update("INSERT INTO TAB_SIFTGRAL VALUES (?, ?, ?)", parannum, paramstr, parametr);
		assertTrue(count == 1);
		
		JdbcTemplate selectjdbcTemplate = new JdbcTemplate(JDBCConnectionFactory.getDataSourceForUDLName(udlName));
		List<Map<String,Object>> results = selectjdbcTemplate.queryForList("SELECT * FROM TAB_SIFTGRAL where PARAMNUM = ? and PARAMSTR = ? and PARAMETR = ?", parannum, paramstr, parametr);
		assertTrue(results.size() == 1);
	}
		
	/**
	 * Prueba un insert via jdbc pelado
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSimpleInsert() throws Exception {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		System.out.println("En profile: " + CurrentProfile.getProfileName());
		final String udlName = "camA_OficinaVirtual.udl";
		final String parannum = "99";
		final String paramstr = "abc";
		final String parametr = "def";
		
        java.sql.Connection jdbcConn = null;

        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(udlName);
			PreparedStatement ps = jdbcConn.prepareStatement("IF OBJECT_ID('TEST_COMMIT_TABLE', 'U') IS NOT NULL DROP TABLE TEST_COMMIT_TABLE");
			ps.executeUpdate();
			ps.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }

        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(udlName);
            System.out.println("getAutoCommit = " + jdbcConn.getAutoCommit());
            System.out.println("getHoldability = " + jdbcConn.getHoldability());
			PreparedStatement ps = jdbcConn.prepareStatement("CREATE TABLE TEST_COMMIT_TABLE(" +
					"PARAMNUM VARCHAR(13), PARAMSTR VARCHAR(10), PARAMETR VARCHAR(10))");
			ps.executeUpdate();
			ps.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }
		
        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(udlName);

			
			PreparedStatement ps = jdbcConn.prepareStatement("SELECT * FROM TEST_COMMIT_TABLE where PARAMNUM = ? and PARAMSTR = ? and PARAMETR = ?");
			ps.setString(1, parannum);
			ps.setString(2, paramstr);
			ps.setString(3, parametr);
			ResultSet rs = ps.executeQuery();
			assertFalse(rs.next());
			ps.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }
        
        
        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(udlName);

			
			PreparedStatement ps = jdbcConn.prepareStatement("INSERT INTO TEST_COMMIT_TABLE VALUES (?, ?, ?)");
			ps.setString(1, parannum);
			ps.setString(2, paramstr);
			ps.setString(3, parametr);
			int count = ps.executeUpdate();
			assertTrue(count == 1);
			ps.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }
		
		
        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(udlName);

			
			PreparedStatement ps = jdbcConn.prepareStatement("SELECT * FROM TEST_COMMIT_TABLE where PARAMNUM = ? and PARAMSTR = ? and PARAMETR = ?");
			ps.setString(1, parannum);
			ps.setString(2, paramstr);
			ps.setString(3, parametr);
			ResultSet rs = ps.executeQuery();
			assertTrue(rs.next());
			do {
				rs.getString("PARAMNUM");
			} while (rs.next());
			ps.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }

        try {
            JDBCConnectionFactory connFactory = new JDBCConnectionFactory();
            jdbcConn = connFactory.getJDBCConnection(udlName);
			PreparedStatement ps = jdbcConn.prepareStatement("DROP TABLE TEST_COMMIT_TABLE");
			ps.executeUpdate();
			ps.close();
        } finally {
                if (jdbcConn != null)
                    jdbcConn.close();
        }
	}

	
	/**
	 * Prueba llamando a 
	 * PROCEDURE P_NBWS_INSERT_ENVIOEPOLIZAS_LOG
	 * @throws CurrentProfileException 
	 * @throws InterruptedException 

	@CODOP       VARCHAR(6),
	@DESCRIPCION VARCHAR(100),
	@CLIENTE     VARCHAR(70),
	@EMAIL       VARCHAR(50),
	@ESTADO      VARCHAR(3),
	@DOCUMTIP    NUMERIC(2) = 0,
	@DOCUMDAT    NUMERIC(11) = 0
	
	que hace un
	INSERT INTO NBWS_ENVIOEPOLIZAS_LOG
	(FECHA, CODOP, DESCRIPCION, CLIENTE, EMAIL, ESTADO, DOCUMTIP, DOCUMDAT)
	VALUES
	(@FECHA, @CODOP, @DESCRIPCION, @CLIENTE, @EMAIL, @ESTADO, @DOCUMTIP, @DOCUMDAT)
	
	 */
	@Test
	public void testCallSP() throws CurrentProfileException, InterruptedException {
		Assume.assumeTrue(CurrentProfile.getProfileName().equals("test"));
		
		//Tomado de nbwsA_FillScheduler
		final String gcteDBLOG = "lbawA_OfVirtualLBA.udl";
		final String mcteStoreProcInsLog = "P_NBWS_INSERT_ENVIOEPOLIZAS_LOG";

	    boolean debugToBD = false;
	    //
	    //
	    final MapSqlParameterSource params = new MapSqlParameterSource();
	     
	    SimpleJdbcCall call = new SimpleJdbcCall(JDBCConnectionFactory.getDataSourceForUDLName(gcteDBLOG))
//		    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
//		    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
	    .withProcedureName(mcteStoreProcInsLog)
//		    .withoutProcedureColumnMetaDataAccess() //No sé si es necesario ponerlo, probar
//		    wobjDBParm = new Parameter( "@RETURN_VALUE", AdoConst.adInteger, AdoConst.adParamReturnValue, 0, new Variant( "0" ) );
//		    wobjDBCmd.getParameters().append( wobjDBParm );
//		    wobjDBParm = (Parameter) null;
	    .declareParameters(new SqlOutParameter("RETURN_VALUE", Types.INTEGER))
	    .withReturnValue()

//		    wobjDBParm = new Parameter( "@CODOP", AdoConst.adChar, AdoConst.adParamInput, 6, new Variant( Strings.left( pvarCODOP, 6 ) ) );
//		    wobjDBCmd.getParameters().append( wobjDBParm );
//		    wobjDBParm = (Parameter) null;
	    .declareParameters(new SqlParameter("CODOP", Types.CHAR, 6))

//		    wobjDBParm = new Parameter( "@DESCRIPCION", AdoConst.adChar, AdoConst.adParamInput, 100, new Variant( Strings.left( pvarDescripcion, 100 ) ) );
//		    wobjDBCmd.getParameters().append( wobjDBParm );
//		    wobjDBParm = (Parameter) null;
	    .declareParameters(new SqlParameter("DESCRIPCION", Types.CHAR, 6))
	    .declareParameters(new SqlParameter("CLIENTE", Types.CHAR, 70))
	    .declareParameters(new SqlParameter("EMAIL", Types.CHAR, 50))
	    .declareParameters(new SqlParameter("ESTADO", Types.CHAR, 3))
	    .declareParameters(new SqlParameter("DOCUMTIP", Types.NUMERIC, 2))
	    .declareParameters(new SqlParameter("DOCUMDAT", Types.NUMERIC, 11));

	    String uniqueId = UUID.randomUUID().toString();
	    params.addValue("CODOP", 1);
	    params.addValue("DESCRIPCION", uniqueId);
	    params.addValue("CLIENTE", "Ramiro");
	    params.addValue("EMAIL", "ramiro@snoopconsulting.com");
	    params.addValue("ESTADO", "OK-");
	    params.addValue("DOCUMTIP", 1);
	    params.addValue("DOCUMDAT", 22000000);

//		    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
	    Map<String, Object> out = call.execute(params);

	    Object returnValue = out.get("RETURN_VALUE");
	    
	    //Controlamos la respuesta del SQL
	    debugToBD = Integer.parseInt(returnValue.toString()) >= 0;

	    assertTrue(debugToBD);
	    
	    // Ahora valido que lo haya insertado
	    JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCConnectionFactory.getDataSourceForUDLName(gcteDBLOG));
	    Integer founds = jdbcTemplate.queryForObject(
	            "select count(*) from NBWS_ENVIOEPOLIZAS_LOG where DESCRIPCION = ?", Integer.class, uniqueId);
	    assertTrue(founds == 1);
	 
	    //Clean up
	    int borrados = jdbcTemplate.update(
	            "delete from NBWS_ENVIOEPOLIZAS_LOG where DESCRIPCION = ?",
	            uniqueId);
	    assertTrue(borrados == 1);
	    
		//1 seg xq a veces tarda el devolver la última conn al pool
		Thread.sleep(1*1000);
		JDBCConnectionFactory.logDataSourceState(gcteDBLOG);
		int busyCon = JDBCConnectionFactory.getDataSourceBusyConnections(gcteDBLOG);
		assertTrue("Quedaron conexiones colgadas fuera del pool", busyCon == 0);

	}

}
