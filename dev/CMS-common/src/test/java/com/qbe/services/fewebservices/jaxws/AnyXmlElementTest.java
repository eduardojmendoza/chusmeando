package com.qbe.services.fewebservices.jaxws;

import static org.junit.Assert.*;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.vbcompat.framework.ComponentExecutionException;

public class AnyXmlElementTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNewForEstado() throws Exception {
		try {
			throw new NullPointerException("NPE en linea 0");
		} catch (Exception ex) {
			ComponentExecutionException e = new ComponentExecutionException("Error en el componente", new RuntimeException("Runtime error", ex));
			AnyXmlElement any = AnyXmlElement.newForEstado("false", "Ha ocurrido un error", ExceptionUtils.getFullStackTrace(e));
			String anyString = any.wrapWithTagName("Any");
			assertTrue("No serializó bien el Any", anyString.contains("NullPointerException"));
		}
	}

}
