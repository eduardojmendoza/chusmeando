package com.qbe.vbcompat.xml;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import diamondedge.util.Strings;

public class XmlDomExtendedTest {

	private static final String JAXB_CONTEXT_IMPL = "org.eclipse.persistence.jaxb.JAXBContext";

	@Before
	public void setUp() throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testMarshalNullContent() throws XmlDomExtendedException {
		XmlDomExtended wobjXmlDOMResp = new XmlDomExtended();
		wobjXmlDOMResp.setPreservingWhiteSpace( true );
		wobjXmlDOMResp.marshal();
		
	}
	/**
	 * Testea que prettyPrint no elimine el CDATA
	 * @throws XmlDomExtendedException 
	 * @throws TransformerException 
	 * @throws JAXBException 
	 * 
	 */
	@Test
	public void testMarshalConservesCDATA() throws XmlDomExtendedException, TransformerException, JAXBException  {
		String expectedAddressXML = "<address><![CDATA[3+4]]></address>";
		XmlDomExtended wobjXMLDOM = new XmlDomExtended();
		wobjXMLDOM.setPreservingWhiteSpace( true );
		wobjXMLDOM.loadXML( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><address/>");
		Node addressNode = wobjXMLDOM.selectSingleNode( "//address");
		CDATASection addressCDATA = wobjXMLDOM.getDocument().createCDATASection("3+4");
		addressNode.appendChild(addressCDATA);
		
		String addressPP = XmlDomExtended.marshal(addressNode);
		assertTrue("El prettyPrint no serializa el nodo con CDATA section como esperaba. Hizo: " + addressPP, expectedAddressXML.equalsIgnoreCase(addressPP));

	}

	/**
	 * Testea que el XmlDomExtended use MOXI
	 * @param context
	 * @throws JAXBException 
	 */
	@Test
	public void checkJAXBImplementation() throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(XmlDomExtended.class);
//		System.out.println(context.getClass().getName());
		assertTrue("No está usando EclipseLink", context.getClass().getName().contains(JAXB_CONTEXT_IMPL));
	}
	
	
	/**
	 * Testea el funcionamiento del Strings.Replace y del prettyPrint
	 * Basado en lo que ocurre en VisualBasic
	 * @see https://sites.google.com/a/snoopconsulting.com/migracioncomplus/notas/loadxml-vs-xml-vs-replace
	 * 
	 * @throws TransformerException
	 * @throws Exception
	 */
	@Test
	public void testPrettyPrintEscapesXmlEntities() throws TransformerException, Exception {
		XmlDomExtended wobjXMLPrueba = new XmlDomExtended();
		wobjXMLPrueba.loadXML("<math><expr>x + y > z</expr></math>");
		String out = XmlDomExtended.prettyPrint(wobjXMLPrueba.getDocument());
//		assertTrue(out.equals("<math>\n    <expr>x + y &gt; z</expr>\n</math>\n"));
//		Se cambia el assertTrue por XMLAssert.
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", out, "<math>\n    <expr>x + y &gt; z</expr>\n</math>\n");
		

		XmlDomExtended wobjXmlDOMResp = new XmlDomExtended();
		wobjXmlDOMResp.loadXML (Strings.replace(Strings.replace(XmlDomExtended.prettyPrint(wobjXMLPrueba.getDocument()), "&gt;", ">"), "&lt;", "<"));
		out = XmlDomExtended.prettyPrint(wobjXmlDOMResp.getDocument());
//		assertTrue(out.equals("<math>\n    <expr>x + y &gt; z</expr>\n</math>\n"));	
//		Se cambia el assertTrue por XMLAssert.
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", out, "<math>\n    <expr>x + y &gt; z</expr>\n</math>\n");
		
		wobjXmlDOMResp = new XmlDomExtended();
		wobjXmlDOMResp.loadXML (XmlDomExtended.prettyPrint(wobjXMLPrueba.getDocument()));
		out = XmlDomExtended.prettyPrint(wobjXmlDOMResp.getDocument());
//		assertTrue(out.equals("<math>\n    <expr>x + y &gt; z</expr>\n</math>\n"));	
//		Se cambia el assertTrue por XMLAssert.
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", out, "<math>\n    <expr>x + y &gt; z</expr>\n</math>\n");
		
		wobjXMLPrueba = new XmlDomExtended();
		wobjXMLPrueba.loadXML("<math><expr>><</expr></math>");
		out = XmlDomExtended.prettyPrint(wobjXMLPrueba.getDocument());
//		El XML que intenta levantar no es valido por lo cual devuelve Vacio.
		assertTrue(out.trim().equals(""));	
		

		wobjXMLPrueba = new XmlDomExtended();
		wobjXMLPrueba.loadXML("<math><expr>&gt;&lt;</expr></math>");
		out = XmlDomExtended.prettyPrint(wobjXMLPrueba.getDocument());
//		assertTrue(out.equals("<math>\n    <expr>&gt;&lt;</expr>\n</math>\n"));
//		Se cambia el assertTrue por XMLAssert.
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", out, "<math>\n    <expr>&gt;&lt;</expr>\n</math>\n");
		
	}
	
	@Test
	public void testSelectNodesString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testSelectNodesElementString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testSelectSingleNode() throws XmlDomExtendedException {
		XmlDomExtended resultadoEmisionXml = new XmlDomExtended();
		resultadoEmisionXml.loadXML("<EMISION><Estpol>R</Estpol><PolizaNo>0001AUS100000001900020166593030000000000</PolizaNo><TipoCerti>3</TipoCerti><TipoInspe>1</TipoInspe><TipoRastreo>0</TipoRastreo></EMISION>");
		Node nodeEmision = resultadoEmisionXml.selectSingleNode("//EMISION");
		assertNotNull(nodeEmision);
	}
	

	@Test
	public void testNode_selectSingleNode() {
		// fail("Not yet implemented");
	}

	@Test
	public void testTransformNodeString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testTransformNodeXmlDomExtended() throws SAXException, IOException, XmlDomExtendedException {

		String xml = 		
"		<CAMPOS>" + 
"	    <T-PAGOS-SAL>" +
"	        <FORMADEPAGO>" +
"	            <COBROCOD>1</COBROCOD>" +
"	            <COBROTIP>ES</COBROTIP>" +
"	            <COBRODAB>CAJA SEGUROS</COBRODAB>" +
"	            <COBRODES>EFECTIVO SCORING INT</COBRODES>" +
"	            <TARJECOD>0</TARJECOD>" +
"	            <OPTPGM1/>" +
"	        </FORMADEPAGO>" +
"	        <FORMADEPAGO>" +
"	            <COBROCOD>1</COBROCOD>" +
"	            <COBROTIP>PF</COBROTIP>" +
"	            <COBRODAB>CAJA SEGUROS</COBRODAB>" +
"	            <COBRODES>PAGO FACIL SC C2</COBRODES>" +
"	            <TARJECOD>0</TARJECOD>" +
"	            <OPTPGM1/>" +
"	        </FORMADEPAGO>" +
"	        <FORMADEPAGO>" +
"	            <COBROCOD>4</COBROCOD>" +
"	            <COBROTIP>VI</COBROTIP>" +
"	            <COBRODAB>TARJETA</COBRODAB>" +
"	            <COBRODES>TARJETA DE CREDITO</COBRODES>" +
"	            <TARJECOD>0</TARJECOD>" +
"	            <OPTPGM1/>" +
"	        </FORMADEPAGO>" +
"	    </T-PAGOS-SAL>" +
"	</CAMPOS>";
	        
		String xsl = "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">"+
    "<xsl:output encoding=\"UTF-8\" indent=\"yes\" method=\"html\" version=\"1.0\" />"+
    "<xsl:template match=\"/\">"+
    "    <xsl:for-each select=\"//FORMADEPAGO\">"+
    "       <xsl:sort select=\"./COBRODES\"/>"+
    "        <option>"+
    "            <xsl:attribute name=\"value\">"+
    "                <xsl:value-of select=\"COBROCOD\"/>"+
    "            </xsl:attribute>"+
    "            <xsl:attribute name=\"cobrotip\">"+
    "                <xsl:value-of select=\"COBROTIP\"/>"+
    "            </xsl:attribute>"+
    "            <xsl:attribute name=\"cobrodab\">"+
    "                <xsl:value-of select=\"COBRODAB\"/>"+
    "            </xsl:attribute>"+
    "            <xsl:attribute name=\"optpgm1\">"+
    "                <xsl:value-of select=\"OPTPGM1\"/>"+
    "            </xsl:attribute>"+
    "            <xsl:value-of select=\"COBRODES\"/>"+
    "        </option>"+
    "    </xsl:for-each>"+
    "</xsl:template>"+
"</xsl:stylesheet>"+
"";
		XmlDomExtended domXml = new XmlDomExtended();
		domXml.loadXML(xml);
		XmlDomExtended domXsl = new XmlDomExtended();
		domXsl.loadXML(xsl);
		String result = domXml.transformNode(domXsl);
		assertNotNull(result);
		result = "<root>" + result + "</root>";
		String expected = "<root>" + "<option value=\"1\" cobrotip=\"ES\" cobrodab=\"CAJA SEGUROS\" optpgm1=\"\">EFECTIVO SCORING INT</option><option value=\"1\" cobrotip=\"PF\" cobrodab=\"CAJA SEGUROS\" optpgm1=\"\">PAGO FACIL SC C2</option><option value=\"4\" cobrotip=\"VI\" cobrodab=\"TARJETA\" optpgm1=\"\">TARJETA DE CREDITO</option>" + "</root>";
		
		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expected, result);
		
//		assertTrue(expected.trim().equals(result.trim()));
	}

	@Test
	public void testNode_selectNodes() {
		// fail("Not yet implemented");
	}

	@Test
	public void testToString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testPrettyPrint() {
		// fail("Not yet implemented");
	}

	@Test
	public void testSaveString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testLoadFile() {
		// fail("Not yet implemented");
	}

	@Test
	public void testLoadString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testLoadXMLString() {
		// fail("Not yet implemented");
	}

	@Test
	public void testLoadInputStream() {
		// fail("Not yet implemented");
	}

}
