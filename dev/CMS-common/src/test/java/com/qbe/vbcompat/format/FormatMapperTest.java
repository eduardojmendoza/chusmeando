package com.qbe.vbcompat.format;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FormatMapperTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFormatStringWithVBFormat() throws Exception {
		String input = "PR2389";
		String expected = "PR-2389";
		String vbFormat = "@@-@@@@";
		String actual = FormatMapper.formatStringWithVBFormat(input,vbFormat);
		assertTrue(actual.equals(expected));
	}

	@Test
	public void testFormatStringWithVBFormatShortValue() throws Exception {
		String input = "";
		String expected = "  -    ";
		String vbFormat = "@@-@@@@";
		String actual = FormatMapper.formatStringWithVBFormat(input,vbFormat);
		assertTrue(actual.equals(expected));
	}
	
	@Test
	public void testFormatStringWithVBFormatRightToLeft () throws Exception {
		String input = "0000";
		String expected = "  -0000";
		String vbFormat = "@@-@@@@";
		String actual = FormatMapper.formatStringWithVBFormat(input,vbFormat);
		assertTrue(actual.equals(expected));
	}
}
