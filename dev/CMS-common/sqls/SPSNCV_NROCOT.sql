SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  Stored Procedure dbo.SPSNCV_NROCOT    Script Date: 22/03/2003 20:02:25 ******/




ALTER    PROCEDURE SPSNCV_NROCOT (@ORIGEN CHAR(1))
AS


SET NOCOUNT ON

BEGIN
      BEGIN TRANSACTION
         UPDATE TAB_NUMERADORES
	 SET TAB_NUMERADORES.NROCOT = TAB_NUMERADORES.NROCOT + 1
	 IF @@ERROR <> 0
	 BEGIN
		RAISERROR 30000 'ERROR UPDATE' 
	 END

         SELECT NROCOT FROM TAB_NUMERADORES
      COMMIT TRANSACTION


IF @ORIGEN = 'S'
   BEGIN
   COMMIT
   END

END






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

