package com.qbe.sac.server.jaxb;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import org.apache.commons.lang.exception.ExceptionUtils;


/**
 * Bean de respuesta usado por el framework de invocación de action codes.
 * 
 * @author ramiro
 *
 */
public abstract class Response {

	private static Logger logger = Logger.getLogger(Response.class.getName());

	@XmlElement(name="Estado")
	protected Estado estado ;

	public Response() {
	}

	public Response(String resultado, String mensaje, String texto) {
		this();
		this.setMensajeEstado(mensaje);
		this.setTextoEstado(texto);
		this.setResultadoEstado(resultado);
	}
	
	/**
	 * 
	 * @param clazz Esta o alguna subclase concreta
	 * @param xml
	 * @return
	 */
	public static Response unmarshal(Class clazz, String xml) {
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller um = context.createUnmarshaller();
			Object r = um.unmarshal(new StringReader(xml));
			return (Response) r;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String marshal() {
		StringWriter sw = new StringWriter();
		try {
			JAXBContext context = JAXBContext.newInstance(this.getClass());
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FRAGMENT, true);
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

			m.marshal(this, sw);
		} catch (Exception e) {
			logger.log(Level.SEVERE, String.format("Exception %s [ %s ] al marshalear un Response. StackTrace: %s", e.getClass().getName(), e.getMessage(),ExceptionUtils.getStackTrace(e)));
			//Intento algo más básico
			return String
					.format("<Response><Estado resultado='%s' mensaje='%s' /></Response>",
							this.estado == null ? "N/A" : this.estado
									.getResultado(),
							this.estado == null ? "N/A" : this.estado
									.getMensaje());
		}
		return sw.toString();
	}

	public Estado getEstado() {
		if ( this.estado == null) {
			this.estado = new Estado();
		}
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void setMensajeEstado(String msj) {
		getEstado().setMensaje(msj);
	}

	public void setTextoEstado(String msj) {
		getEstado().setTexto(msj);
	}

	public void setResultadoEstado(String msj) {
		getEstado().setResultado(msj);
	}

}
