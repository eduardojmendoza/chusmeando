package com.qbe.sac.server.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Response1184")
public class Response1184 extends Response {
	
	@XmlElement(name="CAMPOS")
	protected Campos1184 campos = new Campos1184();

	public Response1184() {
	}

	public Campos1184 getCampos() {
		return campos;
	}

	public void setCampos(Campos1184 campos) {
		this.campos = campos;
	}

	

}
