package com.qbe.sac.server.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.sac.shared.PolizaDTO;
import com.qbe.sac.shared.StringCompat;

@XmlRootElement(name = "PRODUCTO")
public class Item1184 extends Item implements Serializable {

	@XmlElement(name = "CIAASCOD")
	public String ciaascod;

	@XmlElement(name = "RAMOPDES")
	public String ramopdes;

	@XmlElement(name = "AGENTCOD")
	public String agentcod;

	@XmlElement(name = "AGENTCLA")
	public String agentcla;

	@XmlElement(name = "SITUCPOL")
	public String situcpol;

	@XmlElement(name = "EMISIANN")
	public String emisiann;

	@XmlElement(name = "EMISIMES")
	public String emisimes;

	@XmlElement(name = "EMISIDIA")
	public String emisidia;

	@XmlElement(name = "EFEVGANN")
	public String efevgann;

	@XmlElement(name = "EFEVGMES")
	public String efevgmes;

	@XmlElement(name = "EFEVGDIA")
	public String efevgdia;

	@XmlElement(name = "VENFIANN")
	public String venfiann;

	@XmlElement(name = "VENFIMES")
	public String venfimes;

	@XmlElement(name = "VENFIDIA")
	public String venfidia;

	@XmlElement(name = "CLIENSECV")
	public String cliensecv;

	@XmlElement(name = "CLIENAP1V")
	public String clienap1v;

	@XmlElement(name = "CLIENAP2V")
	public String clienap2v;

	@XmlElement(name = "CLIENNOMV")
	public String cliennomv;

	@XmlElement(name = "TOMARIES")
	public String tomaries;

	@XmlElement(name = "TIPOPROD")
	public String tipoprod;

	@XmlElement(name = "PATENTE")
	public String patente;

	@XmlElement(name = "DATO1")
	public String dato1;

	@XmlElement(name = "DATO2")
	public String dato2;

	@XmlElement(name = "DATO3")
	public String dato3;

	@XmlElement(name = "DATO4")
	public String dato4;

	@XmlElement(name = "COBRODES")
	public String cobrodes;

	@XmlElement(name = "SWSUSCRI")
	public String swsuscri;

	@XmlElement(name = "MAIL")
	public String mail;

	@XmlElement(name = "CLAVE")
	public String clave;

	@XmlElement(name = "SWCLAVE")
	public String swclave;

	@XmlElement(name = "MENSASIN")
	public String mensasin;

	@XmlElement(name = "MENSASUS")
	public String mensasus;

	@XmlElement(name = "SWIMPRIM")
	public String swimprim;

	@XmlElement(name = "COBRANZA")
	public String cobranza;

	@XmlElement(name = "SINIESTRO")
	public String siniestro;

	@XmlElement(name = "CONSTANCIA")
	public String constancia;

	public String getCiaascod() {
		return ciaascod;
	}

	public void setCiaascod(String ciaascod) {
		this.ciaascod = ciaascod;
	}

	public String getRamopcod() {
		return ramopcod;
	}

	public void setRamopcod(String ramopcod) {
		this.ramopcod = ramopcod;
	}

	public String getPolizann() {
		return polizann;
	}

	public void setPolizann(String polizann) {
		this.polizann = polizann;
	}

	public String getPolizsec() {
		return polizsec;
	}

	public void setPolizsec(String polizsec) {
		this.polizsec = polizsec;
	}

	public String getCertipol() {
		return certipol;
	}

	public void setCertipol(String certipol) {
		this.certipol = certipol;
	}

	public String getCertiann() {
		return certiann;
	}

	public void setCertiann(String certiann) {
		this.certiann = certiann;
	}

	public String getCertisec() {
		return certisec;
	}

	public void setCertisec(String certisec) {
		this.certisec = certisec;
	}

	public String getSuplenum() {
		return suplenum;
	}

	public void setSuplenum(String suplenum) {
		this.suplenum = suplenum;
	}

	public String getRamopdes() {
		return ramopdes;
	}

	public void setRamopdes(String ramopdes) {
		this.ramopdes = ramopdes;
	}

	public String getAgentcod() {
		return agentcod;
	}

	public void setAgentcod(String agentcod) {
		this.agentcod = agentcod;
	}

	public String getAgentcla() {
		return agentcla;
	}

	public void setAgentcla(String agentcla) {
		this.agentcla = agentcla;
	}

	public String getSitucpol() {
		return situcpol;
	}

	public void setSitucpol(String situcpol) {
		this.situcpol = situcpol;
	}

	public String getEmisiann() {
		return emisiann;
	}

	public void setEmisiann(String emisiann) {
		this.emisiann = emisiann;
	}

	public String getEmisimes() {
		return emisimes;
	}

	public void setEmisimes(String emisimes) {
		this.emisimes = emisimes;
	}

	public String getEmisidia() {
		return emisidia;
	}

	public void setEmisidia(String emisidia) {
		this.emisidia = emisidia;
	}

	public String getEfevgann() {
		return efevgann;
	}

	public void setEfevgann(String efevgann) {
		this.efevgann = efevgann;
	}

	public String getEfevgmes() {
		return efevgmes;
	}

	public void setEfevgmes(String efevgmes) {
		this.efevgmes = efevgmes;
	}

	public String getEfevgdia() {
		return efevgdia;
	}

	public void setEfevgdia(String efevgdia) {
		this.efevgdia = efevgdia;
	}

	public String getVenfiann() {
		return venfiann;
	}

	public void setVenfiann(String venfiann) {
		this.venfiann = venfiann;
	}

	public String getVenfimes() {
		return venfimes;
	}

	public void setVenfimes(String venfimes) {
		this.venfimes = venfimes;
	}

	public String getVenfidia() {
		return venfidia;
	}

	public void setVenfidia(String venfidia) {
		this.venfidia = venfidia;
	}

	public String getCliensecv() {
		return cliensecv;
	}

	public void setCliensecv(String cliensecv) {
		this.cliensecv = cliensecv;
	}

	public String getClienap1v() {
		return clienap1v;
	}

	public void setClienap1v(String clienap1v) {
		this.clienap1v = clienap1v;
	}

	public String getClienap2v() {
		return clienap2v;
	}

	public void setClienap2v(String clienap2v) {
		this.clienap2v = clienap2v;
	}

	public String getCliennomv() {
		return cliennomv;
	}

	public void setCliennomv(String cliennomv) {
		this.cliennomv = cliennomv;
	}

	public String getTomaries() {
		return tomaries;
	}

	public void setTomaries(String tomaries) {
		this.tomaries = tomaries;
	}

	public String getTipoprod() {
		return tipoprod;
	}

	public void setTipoprod(String tipoprod) {
		this.tipoprod = tipoprod;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getDato1() {
		return dato1;
	}

	public void setDato1(String dato1) {
		this.dato1 = dato1;
	}

	public String getDato2() {
		return dato2;
	}

	public void setDato2(String dato2) {
		this.dato2 = dato2;
	}

	public String getDato3() {
		return dato3;
	}

	public void setDato3(String dato3) {
		this.dato3 = dato3;
	}

	public String getDato4() {
		return dato4;
	}

	public void setDato4(String dato4) {
		this.dato4 = dato4;
	}

	public String getCobrodes() {
		return cobrodes;
	}

	public void setCobrodes(String cobrodes) {
		this.cobrodes = cobrodes;
	}

	public String getSwsuscri() {
		return swsuscri;
	}

	public void setSwsuscri(String swsuscri) {
		this.swsuscri = swsuscri;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getSwclave() {
		return swclave;
	}

	public void setSwclave(String swclave) {
		this.swclave = swclave;
	}

	public String getMensasin() {
		return mensasin;
	}

	public void setMensasin(String mensasin) {
		this.mensasin = mensasin;
	}

	public String getMensasus() {
		return mensasus;
	}

	public void setMensasus(String mensasus) {
		this.mensasus = mensasus;
	}

	public String getSwimprim() {
		return swimprim;
	}

	public void setSwimprim(String swimprim) {
		this.swimprim = swimprim;
	}

	public String getCobranza() {
		return cobranza;
	}

	public void setCobranza(String cobranza) {
		this.cobranza = cobranza;
	}

	public String getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	public String getConstancia() {
		return constancia;
	}

	public void setConstancia(String constancia) {
		this.constancia = constancia;
	}

	@Override
	public PolizaDTO getPolizaDTO() {
		PolizaDTO dto = new PolizaDTO();
		dto.setNumeroPoliza(this.getNumeroPoliza());
		dto.setNombreCliente(StringCompat.format("%s %s %s", this.clienap1v, this.clienap2v, this.cliennomv));
		dto.setPatente(this.patente);
		dto.setSituacion(this.situcpol);
		dto.setUltimoSiniestro(this.siniestro);
		dto.setTomaries(this.tomaries);
		dto.setVigenteDesde(StringCompat.format("%s/%s/%s", this.efevgdia, this.efevgmes, this.efevgann ));
		dto.setVigenteHasta(StringCompat.format("%s/%s/%s", this.venfidia, this.venfimes, this.venfiann ));
		return dto;
	}

}
