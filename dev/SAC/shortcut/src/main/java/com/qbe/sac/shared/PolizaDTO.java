package com.qbe.sac.shared;

import java.io.Serializable;


public class PolizaDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6397575809296002073L;
	
	public String nombreCliente; 
	public String numeroPoliza;
	public String tomaries;
	public String patente;
	public String situacion;
	public String sistema;
	public String vigenteDesde;
	public String vigenteHasta;
	public String ultimoSiniestro;
	
	public PolizaDTO() {
		
	}
	
	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getTomaries() {
		return tomaries;
	}

	public void setTomaries(String tomaries) {
		this.tomaries = tomaries;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getVigenteDesde() {
		return vigenteDesde;
	}

	public void setVigenteDesde(String vigenteDesde) {
		this.vigenteDesde = vigenteDesde;
	}

	public String getVigenteHasta() {
		return vigenteHasta;
	}

	public void setVigenteHasta(String vigenteHasta) {
		this.vigenteHasta = vigenteHasta;
	}

	public String getUltimoSiniestro() {
		return ultimoSiniestro;
	}

	public void setUltimoSiniestro(String ultimoSiniestro) {
		this.ultimoSiniestro = ultimoSiniestro;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	
	public String getSistema() {
		return sistema;
	}
}
