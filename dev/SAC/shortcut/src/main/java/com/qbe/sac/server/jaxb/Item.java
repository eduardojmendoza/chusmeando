package com.qbe.sac.server.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.sac.shared.PolizaDTO;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PRODUCTO")
public abstract class Item implements Serializable{

	@XmlElement(name = "RAMOPCOD")
	public String ramopcod;

	@XmlElement(name = "POLIZANN")
	public String polizann;

	@XmlElement(name = "POLIZSEC")
	public String polizsec;

	@XmlElement(name = "CERTIPOL")
	public String certipol;

	@XmlElement(name = "CERTIANN")
	public String certiann;

	@XmlElement(name = "CERTISEC")
	public String certisec;

	@XmlElement(name = "SUPLENUM")
	public String suplenum;


	public abstract PolizaDTO getPolizaDTO();
	
	public String getNumeroPoliza() {
		return this.ramopcod + this.polizann + this.polizsec + this.certipol + this.certiann + this.certisec;
	}

	public String getRamopcod() {
		return ramopcod;
	}

	public void setRamopcod(String ramopcod) {
		this.ramopcod = ramopcod;
	}

	public String getPolizann() {
		return polizann;
	}

	public void setPolizann(String polizann) {
		this.polizann = polizann;
	}

	public String getPolizsec() {
		return polizsec;
	}

	public void setPolizsec(String polizsec) {
		this.polizsec = polizsec;
	}

	public String getCertipol() {
		return certipol;
	}

	public void setCertipol(String certipol) {
		this.certipol = certipol;
	}

	public String getCertiann() {
		return certiann;
	}

	public void setCertiann(String certiann) {
		this.certiann = certiann;
	}

	public String getCertisec() {
		return certisec;
	}

	public void setCertisec(String certisec) {
		this.certisec = certisec;
	}

	public String getSuplenum() {
		return suplenum;
	}

	public void setSuplenum(String suplenum) {
		this.suplenum = suplenum;
	}
	
	
}
