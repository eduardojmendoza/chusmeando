package com.qbe.sac.server.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Response1901")
public class Response1901 extends Response {
	
	@XmlElement(name="CAMPOS")
	protected Campos1901 campos = new Campos1901();

	public Response1901() {
	}

	public Campos1901 getCampos() {
		return campos;
	}

	public void setCampos(Campos1901 campos) {
		this.campos = campos;
	}

	

}
