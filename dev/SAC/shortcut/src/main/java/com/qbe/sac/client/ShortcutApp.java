package com.qbe.sac.client;

import com.qbe.sac.shared.FieldVerifier;
import com.qbe.sac.shared.ClienteDTO;
import com.qbe.sac.shared.StringCompat;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class ShortcutApp implements EntryPoint {
	
  /**
   * The message displayed to the user when the server cannot be reached or
   * returns an error.
   */
  private static final String SERVER_ERROR = "An error occurred while "
      + "attempting to contact the server. Please check your network "
      + "connection and try again.";

  /**
   * Create a remote service proxy to talk to the server-side AIS service.
   */
  private final AISServiceAsync aisService = GWT.create(AISService.class);

  private final Messages messages = GWT.create(Messages.class);

  final RequestQueue rq = new RequestQueue();
  final Button sendButton = new Button( messages.sendButton() );
  
  final Label dniLabel = new Label("DNI");
  final TextBox dniField = new TextBox();
  final Label apellidoLabel = new Label("Apellido");
  final TextBox apellidoField = new TextBox();
  final Label patenteLabel = new Label("Patente");
  final TextBox patenteField = new TextBox();
  final Label errorLabel = new Label();
  final Label opsStatusLabel = new Label();
  final Label statusTitleLabel = new Label("Estado");
  final Label statusLabel = new Label();
  
  final ProductoTable prodTable = new ProductoTable();
  
  final VerticalPanel resultsVPanel = new VerticalPanel();

  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
//    dniField.setText( messages.dniField() );
    dniField.setText("18069158");

//    nombreYApellidoField.setText( messages.nombreYApellidoField() );
    apellidoField.setText("ARMATI" );

    patenteField.setText("BHV229");


    // We can add style names to widgets
    sendButton.addStyleName("sendButton");

    // Add the nameField and sendButton to the RootPanel
    // Use RootPanel.get() to get the entire body element
//    RootPanel.get("nameFieldContainer").add(nameField);
    RootPanel.get("dniFieldContainer").add(dniLabel);
    RootPanel.get("dniFieldContainer").add(dniField);
    RootPanel.get("apellidoFieldContainer").add(apellidoLabel);
    RootPanel.get("apellidoFieldContainer").add(apellidoField);
    RootPanel.get("patenteFieldContainer").add(patenteLabel);
    RootPanel.get("patenteFieldContainer").add(patenteField);
    RootPanel.get("sendButtonContainer").add(sendButton);
    RootPanel.get("errorLabelContainer").add(errorLabel);
    RootPanel.get("statusContainer").add(statusTitleLabel);
    RootPanel.get("statusContainer").add(statusLabel);
    RootPanel.get("datosCliente").add(opsStatusLabel);

    dniField.setFocus(true);
    dniField.selectAll();

    // Create the popup dialog box

    RootPanel resultsPanel = RootPanel.get("resultsContainer");
//    final Button closeButton = new Button("Close");
//    // We can set the id of a widget by accessing its Element
//    closeButton.getElement().setId("closeButton");
    resultsVPanel.addStyleName("dialogVPanel"); //FIXME
    resultsVPanel.add(prodTable);
    resultsVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
    resultsPanel.add(resultsVPanel);

    // Create a handler for the sendButton and nameField
    class MyHandler implements ClickHandler, KeyUpHandler {
      /**
       * Fired when the user clicks on the sendButton.
       */
      public void onClick(ClickEvent event) {
        sendRequestToServer();
      }

      /**
       * Fired when the user types in the field.
       */
      public void onKeyUp(KeyUpEvent event) {
        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
          sendRequestToServer();
        }
      }

      /**
       * Send request to the server and wait for a response.
       */
      private void sendRequestToServer() {
        errorLabel.setText("");

        String dni = dniField.getText();
        String apellido = apellidoField.getText();
        String patente = patenteField.getText();
        
        boolean patenteVacio = "".equals(patente);
        boolean dniVacio = "".equals(dni);
        boolean apellidoVacio = "".equals(apellido);
        if ( patenteVacio && dniVacio && apellidoVacio) {
        	errorLabel.setText("Ingrese algún criterio para realizar la búsqueda");
        	return;
        }
        if ( ((patenteVacio ? 0 : 1) + (dniVacio ? 0 : 1) + (apellidoVacio ? 0 : 1)) > 1 ) {
        	errorLabel.setText("Ingrese sólo un criterio para realizar la búsqueda");
        	return;
        }
        
        //sendButton.setEnabled(false);
        statusLabel.setText("Esperando respuesta del servidor");
        prodTable.cleanRows();
        opsStatusLabel.setText("");
        
        final AsyncCallback<ClienteDTO> callback = new AsyncCallback<ClienteDTO>() {
          public void onFailure(Throwable caught) {
              requestFinished();
        	  errorLabel.setText("Ocurrió un error al consultar al servidor");
//            serverResponseLabel.setHTML(SERVER_ERROR);
        	  statusLabel.setText("Última operación fallida");
              sendButton.setEnabled(true);
          }

          public void onSuccess(ClienteDTO result) {
            requestFinished();
            errorLabel.setText("");
            statusLabel.setText("");
            prodTable.addRows(result.getPolizas());
            sendButton.setEnabled(true);
          }
        };
        if ( !"".equals(patente)) {
        	initRequests(2);
    		aisService.getPolizasPorPatente(patente, AISService.AIS, callback);
    		aisService.getPolizasPorPatente(patente, AISService.INSIS, callback);
        	
        } else if ( !"".equals(dni)) {
        	initRequests(2);
    		aisService.getPolizasPorDocumento("1", dni, AISService.AIS, callback);
    		aisService.getPolizasPorDocumento("1", dni, AISService.INSIS, callback);
        } else if ( !"".equals(apellido)) {
        	initRequests(2);
    		aisService.getPolizasPorApellido(apellido, AISService.AIS, callback);
    		aisService.getPolizasPorApellido(apellido, AISService.INSIS, callback);
        } else {
        	throw new RuntimeException("Sin criterio para consultar - Falló la validación");
        }
      }
    }

    // Add a handler to send the name to the server
    MyHandler handler = new MyHandler();
    sendButton.addClickHandler(handler);
    dniField.addKeyUpHandler(handler);
    patenteField.addKeyUpHandler(handler);
    apellidoField.addKeyUpHandler(handler);
  }

private void requestFinished() {
	int cant = rq.getOngoingRequests() - 1;
	opsStatusLabel.setText("Operaciones pendientes: " + cant);
	rq.setOngoingRequests(cant);
}

private void initRequests(int increment) {
	int cant = rq.getOngoingRequests() + increment;
	opsStatusLabel.setText("Operaciones pendientes: " + cant);
	rq.setOngoingRequests(cant);
}
}
