package com.qbe.sac.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.qbe.sac.shared.PolizaDTO;
import com.qbe.sac.shared.ClienteDTO;

public class ProductoTable extends FlexTable {

	public ProductoTable() {
		super();
		this.setCellPadding(3);
		this.setCellSpacing(0);
		this.addStyleName("productoTable");
		this.getColumnFormatter().setStyleName(0, "productoTabletd");
		this.getColumnFormatter().setStyleName(1, "productoTabletd");
		this.getColumnFormatter().setStyleName(2, "productoTabletd");
		this.getColumnFormatter().setStyleName(3, "productoTabletd");
		this.getColumnFormatter().setStyleName(4, "productoTabletd");
		FlexCellFormatter cellFormatter = this.getFlexCellFormatter();
	    cellFormatter.setHorizontalAlignment(0, 1, HasHorizontalAlignment.ALIGN_LEFT);
	}

	public void setInput(List<PolizaDTO> rows) {
		cleanRows();
		if (rows == null) {
			return;
		}
		addRows(rows);
	}

	public void addRows(List<PolizaDTO> rows) {
		int i = this.getRowCount();
		for (PolizaDTO pol : rows) {
			this.setText(i, 0, pol.getNumeroPoliza());
			this.setText(i, 1, pol.getNombreCliente());
			this.setText(i, 2, pol.getSituacion());
			this.setText(i, 3, pol.getTomaries() + ( "".equals(pol.getPatente()) ? "": "-" + pol.getPatente()));
			this.setText(i, 4, pol.getSistema());
			i++;
		}
	}

	public void cleanRows() {
		for (int i = this.getRowCount(); i > 0; i--) {
			this.removeRow(0);
		}
		int row = 0;
		List<String> headers = getTableHeader();
		if (headers != null) {
			int i = 0;
			for (String string : headers) {
				this.setText(row, i, string);
				i++;
			}
			row++;
		}


	}

	private List<String> getTableHeader() {
		List<String> headers = new ArrayList<String>();
		headers.add("Nro Póliza");
		headers.add("Cliente");
		headers.add("Estado");
		headers.add("Riesgo");
		headers.add("Sistema");
		return headers;
	}
}
