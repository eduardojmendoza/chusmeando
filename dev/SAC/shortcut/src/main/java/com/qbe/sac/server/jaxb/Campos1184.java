package com.qbe.sac.server.jaxb;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.sac.shared.ClienteDTO;

@XmlRootElement(name = "CAMPOS")
public class Campos1184 implements Serializable {

	@XmlElement(name="CLIENSEC")
	private String cliensec;

	@XmlElement(name="TIPODOCU")
	private String tipodocu;

	@XmlElement(name="NUMEDOCU")
	private String numedocu;

	@XmlElement(name="CLIENAP1")
	private String clienap1;

	@XmlElement(name="CLIENAP2")
	private String clienap2;

	@XmlElement(name="CLIENNOM")
	private String cliennom;

	@XmlElementWrapper(name="PRODUCTOS")
	@XmlElement(name="PRODUCTO")
	private List<Item1184> productos;

	public String getCliensec() {
		return cliensec;
	}

	public void setCliensec(String cliensec) {
		this.cliensec = cliensec;
	}

	public String getTipodocu() {
		return tipodocu;
	}

	public void setTipodocu(String tipodocu) {
		this.tipodocu = tipodocu;
	}

	public String getNumedocu() {
		return numedocu;
	}

	public void setNumedocu(String numedocu) {
		this.numedocu = numedocu;
	}

	public String getClienap1() {
		return clienap1;
	}

	public void setClienap1(String clienap1) {
		this.clienap1 = clienap1;
	}

	public String getClienap2() {
		return clienap2;
	}

	public void setClienap2(String clienap2) {
		this.clienap2 = clienap2;
	}

	public String getCliennom() {
		return cliennom;
	}

	public void setCliennom(String cliennom) {
		this.cliennom = cliennom;
	}

	public List<Item1184> getProductos() {
		return productos;
	}

	public void setProductos(List<Item1184> productos) {
		this.productos = productos;
	}

	public ClienteDTO getProductosPorCuitDTO() {
		ClienteDTO dtoProds = new ClienteDTO();
		dtoProds.setClienap1(this.getClienap1());
		dtoProds.setClienap2(this.getClienap2());
		dtoProds.setCliennom(this.getCliennom());
		dtoProds.setCliensec(this.getCliensec());
		dtoProds.setNumedocu(this.getNumedocu());
		dtoProds.setTipodocu(this.getTipodocu());
		for (Item p : this.getProductos()) {
			dtoProds.getPolizas().add(p.getPolizaDTO());
		}
		return dtoProds;
	}

}
