package com.qbe.sac.client;

public class RequestQueue {
	
	private int ongoingRequests = 0;

	public int getOngoingRequests() {
		return ongoingRequests;
	}

	public void setOngoingRequests(int ongoingRequests) {
		this.ongoingRequests = ongoingRequests;
	}
	
	

}
