package com.qbe.sac.server;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.qbe.sac.client.AISService;
import com.qbe.sac.server.jaxb.Response;
import com.qbe.sac.server.jaxb.Response1184;
import com.qbe.sac.server.jaxb.Response1901;
import com.qbe.sac.shared.PolizaDTO;
import com.qbe.sac.shared.ClienteDTO;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.osbconnector.OSBConnectorException;

@SuppressWarnings("serial")
public class AISServiceImpl extends RemoteServiceServlet implements AISService {

	private static final String INSIS_SERVICE_URL = "http://10.1.10.98:8011/OV/Proxy/INSISSvc?wsdl";
	private static final String CMS_SERVICE_URL = "http://10.1.10.98:7200/CMS-fewebservices/MigratedComponentService?wsdl";

	public AISServiceImpl() {
	}

	public ClienteDTO getPolizasPorNroPoliza(String nroPoliza, int sistema) {
		if ( sistema == AISService.AIS) {
			ClienteDTO dto = call1901AIS(nroPoliza, "", "");
			return dto;
		} else {
			ClienteDTO dto = call1901INSIS(nroPoliza, "", "");
			return dto;
		}
	}
	
	public ClienteDTO getPolizasPorPatente(String patente, int sistema) {
		if ( sistema == AISService.AIS) {
			ClienteDTO dto = call1901AIS("", patente, "");
			return dto;
		} else {
			ClienteDTO dto = call1901INSIS("", patente, "");
			return dto;
		}
	}
	
	public ClienteDTO getPolizasPorApellido(String apellido, int sistema) {
		if ( sistema == AISService.AIS) {
			ClienteDTO dto = call1901AIS("", "", apellido);
			return dto;
		} else {
			ClienteDTO dto = call1901INSIS("", "", apellido);
			return dto;
		}
	}
	
	public ClienteDTO getPolizasPorDocumento(String tipodocu,
			String numedocu, int sistema) {

		if ( sistema == AISService.AIS) {
			ClienteDTO dto = call1184AIS(tipodocu, numedocu);
			return dto;
		} else {
			ClienteDTO dto = call1184INSIS(tipodocu, numedocu);
			return dto;
		}
	}

	public ClienteDTO call1901AIS(String nroPoliza, String patente, String apellido) {
		ClienteDTO prods = call1901(nroPoliza, patente, apellido, CMS_SERVICE_URL);
		prods.setSistema("AIS");
		return prods;
	}
	
	public ClienteDTO call1901INSIS(String nroPoliza, String patente, String apellido) {
		ClienteDTO prods = call1901(nroPoliza, patente, apellido, INSIS_SERVICE_URL);
		prods.setSistema("INSIS");
		return prods;
	}
	
	public ClienteDTO call1901(String nroPoliza, String patente, String apellido, String serviceURL) {
		OSBConnector osbconn = new OSBConnector(serviceURL);
		String request = String.format("<Request>" + 
    "<DEFINICION>1901_BusquedaConstanciasPago.xml</DEFINICION>" + 
    "<USUARCOD/>" + 
    "<NIVELCLAS/>" + 
    "<CLIENSECAS/>" + 
    "<NIVELCLA1/>" + 
    "<CLIENSEC1/>" + 
    "<NIVELCLA2/>" + 
    "<CLIENSEC2/>" + 
    "<NIVELCLA3/>" + 
    "<CLIENSEC3/>" + 
    "<RAMO><![CDATA[]]></RAMO>" + 
    "<POLIZA><![CDATA[%s]]></POLIZA>" + 
    "<CERTIF><![CDATA[]]></CERTIF>" + 
    "<PATENTE><![CDATA[%s]]></PATENTE>" + 
    "<APELLIDO><![CDATA[%s]]></APELLIDO>" + 
    "<DOCUMTIP/>" + 
    "<DOCUMDAT/>" + 
    "<FECHASINIE></FECHASINIE>" + 
    "<TIPOSINIE><![CDATA[]]></TIPOSINIE>" + 
    "<SWBUSCA>N</SWBUSCA>" + 
    "<ORDEN>A</ORDEN>" + 
    "<COLUMNA>1</COLUMNA>" + 
    "<NROQRY>0</NROQRY>" + 
    "<RETOMA/>" + 
    "<RETORNARREQUEST/>" + 
    "</Request>", nroPoliza, patente, apellido); 
		try {
			String result = osbconn.executeRequest("lbaw_GetConsultaMQGestion", request);
			System.out.println(result); //FIXME if null...
			result = result.replace("<Response>", "<Response1901>");
			result = result.replace("</Response>", "</Response1901>");
			Response1901 resp = (Response1901)Response.unmarshal(Response1901.class, result);
			if ( resp.getEstado().getResultado().equalsIgnoreCase("false") && !resp.getEstado().getMensaje().startsWith("No se han encontrado resultados para la ")) {
				throw new RuntimeException("Error en Backend: " + resp.getEstado().getMensaje());
			}
			return resp.getCampos().getProductosPorCuitDTO();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (OSBConnectorException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public ClienteDTO call1184AIS(String tipodocu, String numedocu) {
		ClienteDTO prods = call1184(numedocu, CMS_SERVICE_URL);
		prods.setSistema("AIS");
		return prods;
	}

	public ClienteDTO call1184INSIS(String tipodocu, String numedocu) {
		ClienteDTO prods = call1184(numedocu, INSIS_SERVICE_URL);
		prods.setSistema("INSIS");
		return prods;
	}

	private ClienteDTO call1184(String numedocu,
			final String serviceURL) {
		OSBConnector osbconn = new OSBConnector(serviceURL);
		String request = String.format("<Request>" +
				"<DEFINICION>LBA_1184_ProductosClientes.xml</DEFINICION>" +
				"<AplicarXSL>LBA_1184_ProductosClientes.xsl</AplicarXSL>" +
				"<TIPODOCU>%s</TIPODOCU>" +
				"<NUMEDOCU>%s</NUMEDOCU>" +
				"</Request>", "01", numedocu); 
		try {
			String result = osbconn.executeRequest("nbwsA_MQGenericoAIS", request);
			System.out.println(result); //FIXME if null...
			result = result.replace("<Response>", "<Response1184>");
			result = result.replace("</Response>", "</Response1184>");
			Response1184 resp = (Response1184)Response.unmarshal(Response1184.class, result);
			if ( resp.getEstado().getResultado().equalsIgnoreCase("false")) {
				throw new RuntimeException("Error en INSIS: " + resp.getEstado().getMensaje());
			}
			return resp.getCampos().getProductosPorCuitDTO();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (OSBConnectorException e) {
			e.printStackTrace();
		}
		return null;
	}
}
