package com.qbe.sac.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.qbe.sac.shared.ClienteDTO;

@RemoteServiceRelativePath("ais")
public interface AISService extends RemoteService {

	public static int INSIS = 1;
	public static int AIS = 2;
	
	public ClienteDTO getPolizasPorDocumento(String tipodocu, String numedocu, int sistema);
	public ClienteDTO getPolizasPorNroPoliza(String nroPoliza, int sistema);
	public ClienteDTO getPolizasPorPatente(String patente, int sistema);
	public ClienteDTO getPolizasPorApellido(String apellido, int sistema);
	
}
