package com.qbe.sac.server.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.sac.shared.ClienteDTO;

/**
 * 
 * 
 * <CAMPOS>			
	<CANT>44</CANT>		
	<ITEMS>		
		<ITEM>	
			<RAMOPCOD><![CDATA[AUS1]]></RAMOPCOD>
			<POLIZANN>00</POLIZANN>
			<POLIZSEC>000001</POLIZSEC>
			<CERTIPOL>0000</CERTIPOL>
			<CERTIANN>0000</CERTIANN>
			<CERTISEC>171345</CERTISEC>
			<SUPLENUM>0</SUPLENUM>
			<CLIENDES><![CDATA[ARMATI , MARIA ADRIANA]]></CLIENDES>
			<TOMARIES><![CDATA[FORD KA J4HVU00611]]></TOMARIES>
			<PATENNUM><![CDATA[BHV229]]></PATENNUM>
			<SITUCPOL><![CDATA[ANULADA]]></SITUCPOL>
			<SWDENHAB><![CDATA[S]]></SWDENHAB>
			<SWSINIES><![CDATA[N]]></SWSINIES>
			<SWEXIGIB><![CDATA[0]]></SWEXIGIB>
			<RAMO>1</RAMO>
			<VIGDESDE><![CDATA[20001227]]></VIGDESDE>
			<VIGHASTA><![CDATA[99991231]]></VIGHASTA>
			<FULTSTRO><![CDATA[20010727]]></FULTSTRO>
		</ITEM>	
		usw
 * @author ramiro
 *
 */
@XmlRootElement(name = "CAMPOS")
public class Campos1901 implements Serializable{

	@XmlElement(name="CANT")
	private int cant;

	@XmlElementWrapper(name="ITEMS")
	@XmlElement(name="ITEM")
	private List<Item1901> productos = new ArrayList<Item1901>();

	public int getCant() {
		return cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}

	public List<Item1901> getProductos() {
		return productos;
	}

	public void setProductos(List<Item1901> productos) {
		this.productos = productos;
	}

	public ClienteDTO getProductosPorCuitDTO() {
		ClienteDTO dtoProds = new ClienteDTO();
		for (Item1901 p : this.getProductos()) {
			dtoProds.getPolizas().add(p.getPolizaDTO());
			//Le seteo el primero, se *debería* repetir en todos según spec
			if ( !"".equals(dtoProds.getCliennom()) ) {
				dtoProds.setCliennom(p.getCliendes());
			}
		}
		return dtoProds;
	}

}
