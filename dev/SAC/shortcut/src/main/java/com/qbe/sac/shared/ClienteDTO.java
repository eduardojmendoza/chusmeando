package com.qbe.sac.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ClienteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5825471210317283463L;
	
	private String cliensec;
	private String tipodocu;
	private String numedocu;
	private String clienap1;
	private String clienap2;
	private String cliennom;
	private List<PolizaDTO> polizas;
	
	public ClienteDTO() {
	}

	public String getCliensec() {
		return cliensec;
	}

	public void setCliensec(String cliensec) {
		this.cliensec = cliensec;
	}

	public String getTipodocu() {
		return tipodocu;
	}

	public void setTipodocu(String tipodocu) {
		this.tipodocu = tipodocu;
	}

	public String getNumedocu() {
		return numedocu;
	}

	public void setNumedocu(String numedocu) {
		this.numedocu = numedocu;
	}

	public String getClienap1() {
		return clienap1;
	}

	public void setClienap1(String clienap1) {
		this.clienap1 = clienap1;
	}

	public String getClienap2() {
		return clienap2;
	}

	public void setClienap2(String clienap2) {
		this.clienap2 = clienap2;
	}

	public String getCliennom() {
		return cliennom;
	}

	public void setCliennom(String cliennom) {
		this.cliennom = cliennom;
	}

	public List<PolizaDTO> getPolizas() {
		if ( polizas == null) {
			polizas = new ArrayList<PolizaDTO>();
		}
		return polizas;
	}

	public void setPolizas(List<PolizaDTO> productos) {
		this.polizas = productos;
	}

	public String getNombreCompleto() {
		return StringCompat.format("%s %s, %s", getClienap1(), getClienap2(), getCliennom());
	}

	public ClienteDTO setSistema(String sistema) {
		for (PolizaDTO p : getPolizas()) {
			p.setSistema(sistema);
		}
		return null;
	}


}
