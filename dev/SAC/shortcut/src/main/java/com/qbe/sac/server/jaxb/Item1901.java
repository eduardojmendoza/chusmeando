package com.qbe.sac.server.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.qbe.sac.shared.PolizaDTO;
import com.qbe.sac.shared.StringCompat;

@XmlRootElement(name = "ITEM")
public class Item1901 extends Item implements Serializable{

	@XmlElement(name = "CLIENDES")
	public String cliendes;

	@XmlElement(name = "TOMARIES")
	public String tomaries;

	@XmlElement(name = "PATENNUM")
	public String patennum;

	@XmlElement(name = "SITUCPOL")
	public String situcpol;

	@XmlElement(name = "SWDENHAB")
	public String swdenhab;

	@XmlElement(name = "SWSINIES")
	public String swsinies;

	@XmlElement(name = "SWEXIGIB")
	public String swexigib;

	@XmlElement(name = "RAMO")
	public String ramo;

	@XmlElement(name = "VIGDESDE")
	public String vigdesde;

	@XmlElement(name = "VIGHASTA")
	public String vighasta;

	@XmlElement(name = "FULTSTRO")
	public String fultstro;
	
	@Override
	public PolizaDTO getPolizaDTO() {
		PolizaDTO dto = new PolizaDTO();
		dto.setNumeroPoliza(this.getNumeroPoliza());//Heredada. TODO Mandarla arriba
		dto.setSituacion(this.situcpol);//Heredada TODO Mandarla arriba
		dto.setTomaries(this.tomaries);//Heredada TODO Mandarla arriba
		dto.setNombreCliente(this.cliendes);
		dto.setPatente(this.patennum);
		dto.setUltimoSiniestro(this.fultstro);
		dto.setVigenteDesde(this.vigdesde);
		dto.setVigenteHasta(this.vighasta);
		return dto;
	}

	public String getCliendes() {
		return cliendes;
	}

	public void setCliendes(String cliendes) {
		this.cliendes = cliendes;
	}

	public String getTomaries() {
		return tomaries;
	}

	public void setTomaries(String tomaries) {
		this.tomaries = tomaries;
	}

	public String getPatennum() {
		return patennum;
	}

	public void setPatennum(String patennum) {
		this.patennum = patennum;
	}

	public String getSitucpol() {
		return situcpol;
	}

	public void setSitucpol(String situcpol) {
		this.situcpol = situcpol;
	}

	public String getSwdenhab() {
		return swdenhab;
	}

	public void setSwdenhab(String swdenhab) {
		this.swdenhab = swdenhab;
	}

	public String getSwsinies() {
		return swsinies;
	}

	public void setSwsinies(String swsinies) {
		this.swsinies = swsinies;
	}

	public String getSwexigib() {
		return swexigib;
	}

	public void setSwexigib(String swexigib) {
		this.swexigib = swexigib;
	}

	public String getRamo() {
		return ramo;
	}

	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	public String getVigdesde() {
		return vigdesde;
	}

	public void setVigdesde(String vigdesde) {
		this.vigdesde = vigdesde;
	}

	public String getVighasta() {
		return vighasta;
	}

	public void setVighasta(String vighasta) {
		this.vighasta = vighasta;
	}

	public String getFultstro() {
		return fultstro;
	}

	public void setFultstro(String fultstro) {
		this.fultstro = fultstro;
	}

	
}
