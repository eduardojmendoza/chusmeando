package com.qbe.services.alertasxmail.impl;
import com.qbe.services.cam30.camA_EnviarMail;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.FileSystem;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVProcAlertas implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_AlertasxMail.lbaw_OVProcAlertas";
  /**
   * Stored Procedures
   */
  static final String mcteStoreProcListarSuscriptos = "P_ALERTAS_LISTAR_SUSCRIPTOS";
  static final String mcteStoreProcListarPendientes = "P_ALERTAS_LISTAR_PENDIENTES";
  static final String mcteStoreProcUpdControlEnv = "P_ALERTAS_UPDATE_CONTROL_ENVIADO";
  static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
  static final String mcteStoreProcInsLog = "P_ALERTAS_INSERT_LOG";
  static final String mcteSPObtenerIdentificador = "SPCAM_CAM_OBTENER_IDENTIFICADOR";
  /**
   * 
   */
  static final String mcteParam_CODOP = "//CODOP";
  static final String mcteParam_FECHA = "//FECHA";
  /**
   * 
   */
  static final String mcteOperacion = "PROCALERTAS";
  
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarCODOP = "";
    String mvarFECHA = "";
    XmlDomExtended wobjXMLResponseAIS = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLListadoProd = null;
    org.w3c.dom.Node wobjXMLNode = null;
    String mvarSPListarProd = "";
    String mvarREPROCESO = "";
    int mvarIDCONTROL = 0;
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    //
    //
    //Parámetros de entrada
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicialización de variables
      mvarProcesar = true;
      mvarFECHA = "";
      mvarPendientes = 0;
      mvarProcConErrores = false;
      //
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarCODOP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CODOP )  );
      wvarStep = 31;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_FECHA )  == (org.w3c.dom.Node) null) )
      {
        mvarFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FECHA )  );
      }
      //
      //Registración en Log de inicio de ejecución
      wvarStep = 30;
      insertLog(mcteOperacion,"1- Nueva ejecución - Validación de parámetros");
      //
      //Si parametro FECHA definido, significa que hay que reprocesar los pendientes
      //para una fecha y código de operacion particular
      //Si FECHA definido, se marca REPROCESO en S y se selecciona el SP que
      //recupera los casos en los que no se pudo generar/enviar el email para
      //esa fecha
      //Sino, se marca REPROCESO en N y se selecciona el SP que recupera todos
      //los productores con CODOP en S
      wvarStep = 40;
      if( !mvarFECHA.equals( "" ) )
      {
        mvarREPROCESO = "S";
        mvarSPListarProd = mcteStoreProcListarPendientes;
      }
      else
      {
        mvarFECHA = Strings.right( "00" + DateTime.month( DateTime.now() ), 2 ) + "-" + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + "-" + DateTime.year( DateTime.now() );
        mvarREPROCESO = "N";
        mvarSPListarProd = mcteStoreProcListarSuscriptos;
        //
        //Verifica la fecha de ejecución para el impreso seleccionado
        mvarProcesar = analizarFechaEjecucion(mvarCODOP);
      }
      //
      //Obtención de listado de productores a procesar...
      wvarStep = 60;
      wobjXMLListadoProd = new XmlDomExtended();
      wobjXMLListadoProd.loadXML( getListadoProductores(mvarSPListarProd,mvarCODOP,mvarFECHA,mvarREPROCESO));
      //
      //Registración en Log de inicio de proceso
      wvarStep = 50;
      insertLog(mcteOperacion,"2- Validación Ok - Inicio de proceso (CODOP:" + mvarCODOP + " REPRO:" + 
    		  	mvarREPROCESO + " Regs:" +  wobjXMLListadoProd.selectNodes( "//PRODUCTOR") .getLength() + ")");
      //
      if( mvarProcesar )
      {
        //Mientras queden productores sin procesar en el listado...
        wvarStep = 70;
        mvarPendientes = wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .getLength();
        //
        //En caso de error se resume para evitar el corte del proceso (se realiza control de err.Number)
        //FIXME OJO implementar este comportamiento anterior!!!!!!
        
        // On Error Resume Next (optionally ignored)
        //
        for( int nwobjXMLNode = 0; nwobjXMLNode < wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .getLength(); nwobjXMLNode++ )
        {
          wobjXMLNode = wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .item( nwobjXMLNode );
          //
          //Es reproceso?
          wvarStep = 80;
          if( mvarREPROCESO.equals( "N" ) )
          {
            //No - Se insertan los datos de la operacion en la tabla de control
            wvarStep = 90;
            mvarIDCONTROL = agregarControl(XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"IDSUSCRIPTOS")), mvarCODOP);
          }
          else
          {
            mvarIDCONTROL = Obj.toInt( XmlDomExtended .getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"IDCONTROL" )  ) );
          }
          //
          //Registraci�n en Log de inicio de caso a procesar.
          insertLog(mcteOperacion,"a- Procesando operación... (IDCONTROL: " + mvarIDCONTROL + ")");
          //
          wvarStep = 100;
          wvarRequest = "<Request>" + "<CODOP>" + mvarCODOP + "</CODOP>" + "<USUARCOD>" + 
          XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"USUARCOD" )  ) + "</USUARCOD>" + 
        		  "<NIVELAS>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"NIVELAS" )  ) + "</NIVELAS>" + 
        		  "<CLIENSECAS>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"CLIENSECAS" )  ) + "</CLIENSECAS>" + 
        		  "<EMAIL>" + XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode,"EMAIL" )  ) + "</EMAIL>" + 
        		  "<FECHAEXEC>" + mvarFECHA + "</FECHAEXEC>" + "<IDCONTROL>" + mvarIDCONTROL + "</IDCONTROL>" + "</Request>";
          //
          wvarStep = 110;
          lbaw_OVExeSch exeSch = new lbaw_OVExeSch();
          StringHolder exeResponse = new StringHolder();
          exeSch.IAction_Execute(wvarRequest, exeResponse, "" );
          //Agregado esta linea para que setear wobjXMLResponseAIS a partir de lbaw_OVExeSch
          wobjXMLResponseAIS.loadXML(exeResponse.getValue());
          //
          wvarStep = 120;
          if( ! ((wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null)) && (Err.getError().getNumber() == 0) ) //FIXME acá es donde controla que no haya habido error
        	  // para poder continuar. Yo lo debo traducir a un control de exceptions
          {
            if( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
            {
              mvarPendientes = mvarPendientes - 1;
              wvarStep = 130;
              insertLog(mcteOperacion, "b- Fin Oper. con EXITO. (IDCONTROL: " + mvarIDCONTROL + ")");
            }
            else
            {
              wvarStep = 140;
              insertLog(mcteOperacion, "b- Fin Oper. con ERROR. (IDCONTROL: " + mvarIDCONTROL + ")");
              mvarProcConErrores = true;
            }
          }
          else
          {
            wvarStep = 150;
            insertLog(mcteOperacion, "b- Fin Oper. con ERROR. (IDCONTROL: " + mvarIDCONTROL + ")");
            mvarProcConErrores = true;
          }
          //
          //Loop
          wvarStep = 230;
        }
        //
        //Reestablece el estado de On Error
        //
        //Quedaron alertas en estado pendiente y REPROCESO = N?
        wvarStep = 250;
        if( mvarProcConErrores && (mvarREPROCESO.equals( "N" )) )
        {
          //Envio de email informando la existencia de operaciones pendientes en la fecha
          wvarStep = 260;
          enviarEMail(mvarCODOP, mvarPendientes, "ALGUNAS OPERACIONES QUEDARON PENDIENTES",DateTime.format( DateTime.now() ));
        }
      }
      //
      //Registración en Log de fin de proceso
      wvarStep = 240;
      if( mvarProcesar == false )
      {
        insertLog(mcteOperacion, "3- Fin de procesamiento. Estado: NO EJECUTADO POR PARAMETRO");
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else if( mvarProcConErrores )
      {
        insertLog(mcteOperacion, "3- Fin de procesamiento. Estado: ERROR - Pendientes: " + mvarPendientes);
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        insertLog(mcteOperacion, "3- Fin de procesamiento. Estado: OK - Pendientes: " + mvarPendientes);
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      //FIN
      //
      wvarStep = 250;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 260;
      wobjXMLResponseAIS = null;
      wobjXMLRequest = null;
      wobjXMLListadoProd = null;
      wobjXMLNode = (org.w3c.dom.Node) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        insertLog(mcteOperacion, mcteClassName + " - " + wcteFnName + " - " + wvarStep + " - " + Err.getError().getNumber() + " - " + "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + vbLogEventTypeError);

        insertLog(mcteOperacion, "99- Fin de procesamiento. Estado: ABORTADO POR ERROR");

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      debugToBD(pvarCODOP, pvarDescripcion);
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      debugToFile("REPROCESO", pvarDescripcion);
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    boolean debugToBD = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    // FIXME Pasar a JDBC
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    XmlDomExtended wobjXMLConfig = null;
    String wvarText = "";
    String wvarFilename = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion;

    wvarFilename = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    wvarNroArch = FileSystem.getFreeFile();
    //FIXME
    FileSystem.openAppend( System.getProperty("user.dir") + "\\DEBUG\\" + wvarFilename, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private String getListadoProductores( String pvarSPListarProd, String pvarCODOP, String pvarFECHA, String pvarREPROCESO ) throws Exception
  {
    String getListadoProductores = "";
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstDBResult = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    //FIXME pasar a JDBC
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( pvarSPListarProd );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );

    if( pvarREPROCESO.equals( "S" ) )
    {
      
      
    }
    //
    wrstDBResult = wobjDBCmd.execute();
    wrstDBResult.setActiveConnection( (Connection) null );
    //
    //Controlamos la respuesta del SQL
    if( (wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0) && ! (wrstDBResult.isEOF()) )
    {
      //
      wobjXMLResponse = new XmlDomExtended();
      wobjXSLResponse = new XmlDomExtended();
      //
      wobjXSLResponse.loadXML( p_GetXSL());

      wobjXMLResponse.load(AdoUtils.saveRecordset(wrstDBResult));
      //
      getListadoProductores = "<PRODUCTORES>" + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ) + "</PRODUCTORES>";

      wobjXMLResponse = null;
      wobjXSLResponse = null;
    }
    else
    {
      getListadoProductores = "<PRODUCTORES/>";
    }

    return getListadoProductores;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='PRODUCTOR'>";

    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDSUSCRIPTOS'><xsl:value-of select='@IDSUSCRIPTOS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDCONTROL'><xsl:value-of select='@IDCONTROL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NIVELAS'><xsl:value-of select='@NIVELAS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSECAS'><xsl:value-of select='@CLIENSECAS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>";

    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;

    return p_GetXSL;
  }

  private int agregarControl( String pvarIDSUSCRIPTOS, String pvarCODOP ) throws Exception
  {
    int agregarControl = 0;
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    //FIXME JDBC
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsControl );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      agregarControl = wobjDBCmd.getParameters().getParameter("@IDCONTROL").getValue().toInt();
    }
    else
    {
      agregarControl = 0;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return agregarControl;
  }

  private boolean analizarFechaEjecucion( String pvarCODOP ) throws Exception
  {
    boolean analizarFechaEjecucion = false;
    XmlDomExtended wobjXMLParametro = null;
    int wvarSemana = 0;
    int wvarDia = 0;
    java.util.Date wvarDateAux = DateTime.EmptyDate;
    int wvarDayCount = 0;
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msg" + pvarCODOP + ".xml" );
    //
    //Solo analiza el caso en que la ejecución es MENSUAL, ya que para el SEMANAL esta
    //configurado el scheduler.
    if( XmlDomExtended .getText( wobjXMLParametro.selectSingleNode( "//PARAMETROS/EJECUCION" )  ).equals( "MENSUAL" ) )
    {
      wvarSemana = Obj.toInt( XmlDomExtended .getText( wobjXMLParametro.selectSingleNode( "//PARAMETROS/EJECUCION/@SEMANA" )  ) );
      wvarDia = Obj.toInt( XmlDomExtended .getText( wobjXMLParametro.selectSingleNode( "//PARAMETROS/EJECUCION/@DIA" )  ) );
      //
      //Calcula la semana actual
      wvarDateAux = DateTime.toDate( "01" + "-" + DateTime.month( DateTime.now() ) + "-" + DateTime.year( DateTime.now() ) );
      wvarDayCount = 1;
      while( ! ((DateTime.weekday( wvarDateAux, 0 ) == wvarDia) && (wvarDayCount == wvarSemana)) )
      {
        wvarDateAux = DateTime.add( wvarDateAux, 1 );
        if( DateTime.weekday( wvarDateAux, 0 ) == 1 )
        {
          wvarDayCount = wvarDayCount + 1;
        }
      }
      if( DateTime.diff( "d", wvarDateAux, DateTime.now() ) == 0 )
      {
        analizarFechaEjecucion = true;
      }
      else
      {
        analizarFechaEjecucion = false;
      }

    }
    else
    {
      analizarFechaEjecucion = true;
    }
    //
    return analizarFechaEjecucion;
  }

  /**
   * 
   */
  private boolean enviarEMail( String pvarCODOP, int pvarPendientes, String pvarEstado, String pvarFECHA ) throws Exception
  {
    boolean enviarEMail = false;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wobjXMLParametro = null;
    XmlDomExtended wobjXMLCODOPParam = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    String wvarCODOPDesc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgEnvioMail.xml" );
    wobjXMLCODOPParam = new XmlDomExtended();
    wobjXMLCODOPParam.load( System.getProperty("user.dir") + "\\XML\\msg" + pvarCODOP + ".xml" );
    //
    wvarCODOPDesc = XmlDomExtended.marshal(wobjXMLCODOPParam.selectSingleNode( "//DESCRIPCION" ));
    //
    agregarParametro(wobjXMLParametro, "%%FECHA%%", pvarFECHA);
    agregarParametro(wobjXMLParametro, "%%FORMULARIO%%", wvarCODOPDesc);
    agregarParametro(wobjXMLParametro, "%%ESTADO%%", pvarEstado);
    agregarParametro(wobjXMLParametro, "%%PENDIENTES%%", String.valueOf( pvarPendientes));
    //
    wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
    //
	camA_EnviarMail mailer = new camA_EnviarMail();
	StringHolder response = new StringHolder();
	mailer.IAction_Execute(wvarRequest, response, null);
	wvarResponse = response.getValue();
	
    //
    wvarXMLResponse = new XmlDomExtended();
    wvarXMLResponse.loadXML( wvarResponse );
    //
    if( ! (wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        enviarEMail = true;
      }
      else
      {
        enviarEMail = false;
      }
    }
    else
    {
      enviarEMail = false;
    }
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    return enviarEMail;
  }

  private void agregarParametro( XmlDomExtended pobjXMLParametro, String pvarParam, String pvarValor ) throws Exception
  {
    org.w3c.dom.Element wobjXMLElement = null;
    org.w3c.dom.Element wobjXMLElementAux = null;
    org.w3c.dom.CDATASection wobjXMLElementCda = null;

    wobjXMLElement = pobjXMLParametro.getDocument().createElement( "PARAMETRO" );
    //
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_NOMBRE" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarParam );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    wobjXMLElement.appendChild( wobjXMLElementAux );
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_VALOR" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarValor );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    //
    wobjXMLElement.appendChild( wobjXMLElementAux );
    XmlDomExtended.nodeImportAsChildNode(pobjXMLParametro.selectSingleNode( "//PARAMETROS" ), wobjXMLElement);
  }

}
