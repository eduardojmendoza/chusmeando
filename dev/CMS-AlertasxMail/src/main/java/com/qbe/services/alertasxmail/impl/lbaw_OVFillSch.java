package com.qbe.services.alertasxmail.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.ado.AdoConst;
import diamondedge.ado.Command;
import diamondedge.ado.Connection;
import diamondedge.ado.Parameter;
import diamondedge.ado.Recordset;
import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.FileSystem;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVFillSch implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_AlertasxMail.lbaw_OVFillSch";
  /**
   * Stored Procedures
   */
  //FIXME cambiarlo al de OSB
  static final String mcteStoreProcInsBandejaSCH = "P_CP_SA_BANDEJA_INSERT";
  
  static final String mcteStoreProcListarSuscriptos = "P_ALERTAS_LISTAR_SUSCRIPTOS";
  static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
  static final String mcteStoreProcInsLog = "P_ALERTAS_INSERT_LOG";
  /**
   * 
   */
  static final String mcteParam_INSCTRL = "//INSCTRL";
  /**
   * 
   */
  static final String mcteOperacion = "FILLSCH";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    XmlDomExtended wobjXMLListadoProd = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.Node wobjXMLConfigNode = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLRequest = null;
    int mvarIDCONTROL = 0;
    String mvarINSCTRL = "";
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    String mvarFECHA = "";
    String mvarCODOP = "";
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicialización de variables
      wvarStep = 10;
      mvarFECHA = Strings.right( "00" + DateTime.month( DateTime.now() ), 2 ) + "-" + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + "-" + DateTime.year( DateTime.now() );
      mvarINSCTRL = "N";
      //
      //Lectura de parámetros
      wvarStep = 20;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 25;
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_INSCTRL )  == (org.w3c.dom.Node) null) )
      {
        mvarINSCTRL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INSCTRL )  );
      }
      //
      //Registración en Log de inicio de ejecución
      wvarStep = 30;
      insertLog(mcteOperacion, "I-OVFILLSCH - Nueva ejecución");
      //
      wvarStep = 40;
      //Se obtienen los CODOP a procesar
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
      //
      wvarStep = 50;
      for( int nwobjXMLConfigNode = 0; nwobjXMLConfigNode < wobjXMLConfig.selectNodes( "//CODOP" ) .getLength(); nwobjXMLConfigNode++ )
      {
        wobjXMLConfigNode = wobjXMLConfig.selectNodes( "//CODOP" ) .item( nwobjXMLConfigNode );
        //
        wvarStep = 60;
        mvarCODOP = XmlDomExtended.getText( wobjXMLConfigNode );
        //
        //Verifica la fecha de ejecución para el impreso seleccionado
        wvarStep = 70;
        mvarProcesar = analizarFechaEjecucion(mvarCODOP);
        //
        wvarStep = 80;
        if( mvarProcesar )
        {
          //
          //Obtención de listado de productores a procesar...
          wvarStep = 90;
          wobjXMLListadoProd = new XmlDomExtended();
          wobjXMLListadoProd.loadXML( getListadoProductores(mcteStoreProcListarSuscriptos, mvarCODOP, mvarFECHA, "N"));
          //
          //Mientras queden productores sin procesar en el listado...
          wvarStep = 100;
          mvarPendientes = wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .getLength();
          //
          for( int nwobjXMLNode = 0; nwobjXMLNode < wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .getLength(); nwobjXMLNode++ )
          {
            wobjXMLNode = wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .item( nwobjXMLNode );
            //
            //Se insertan los datos de la operacion en la tabla de control
            wvarStep = 110;
            mvarIDCONTROL = agregarControl(XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "IDSUSCRIPTOS")), mvarCODOP);
            //
            wvarStep = 120;
            insertAlertasSCH( XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "USUARCOD")), 
            		XmlDomExtended.getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "NIVELAS") ), 
            		XmlDomExtended .getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "CLIENSECAS") ),
            		XmlDomExtended .getText( XmlDomExtended.Node_selectSingleNode(wobjXMLNode, "EMAIL") ),
            		mvarCODOP, mvarIDCONTROL, mvarFECHA);
            //
            //Loop
            wvarStep = 140;
          }
          //
          wvarStep = 150;
        }
        //
        //Loop
        wvarStep = 160;
      }
      //
      //Registración en Log de fin de proceso
      wvarStep = 170;
      if( mvarProcConErrores )
      {
        insertLog(mcteOperacion, "E- Fin OVFILLSCH. Estado: ERROR");
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        //insertControlSCH
        if( mvarINSCTRL.equals( "S" ) )
        {
          insertControlSCH(mvarFECHA);
        }
        insertLog(mcteOperacion, "0- Fin OVFILLSCH. Estado: OK");
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      //FIN
      //
      wvarStep = 180;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 190;
      wobjXMLListadoProd = null;
      wobjXMLNode = (org.w3c.dom.Node) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        insertLog(mcteOperacion, mcteClassName + " - " + wcteFnName + " - " + wvarStep + " - " + Err.getError().getNumber() + " - " + "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + vbLogEventTypeError);
        insertLog(mcteOperacion, "99- Fin OVFILLSCH. Estado: ABORTADO POR ERROR");

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private boolean insertAlertasSCH( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarEMAIL, String pvarCODOP, int pvarIDCONTROL, String pvarFECHA ) throws Exception
  {
    boolean insertAlertasSCH = false;
    String wvarCodusu = "";
    String wvarAplicacion = "";
    String wvarDataIn = "";
    //
    //
    wvarAplicacion = "OV_AlertaxMail";
    wvarCodusu = "OVFILLSCH";
    wvarDataIn = "<CODOP>" + pvarCODOP + "</CODOP>" + "<USUARCOD>" + pvarUSUARCOD + "</USUARCOD>" + "<NIVELAS>" + pvarNIVELAS + "</NIVELAS>" + "<CLIENSECAS>" + pvarCLIENSECAS + "</CLIENSECAS>" + "<EMAIL>" + pvarEMAIL + "</EMAIL>" + "<FECHAEXEC>" + pvarFECHA + "</FECHAEXEC>" + "<IDCONTROL>" + pvarIDCONTROL + "</IDCONTROL>";
    //
    insertAlertasSCH = insertBandejaSCH(wvarAplicacion, wvarCodusu, wvarDataIn, pvarFECHA);
    //
    return insertAlertasSCH;
  }

  private boolean insertControlSCH( String pvarFECHA ) throws Exception
  {
    boolean insertControlSCH = false;
    String wvarCodusu = "";
    String wvarAplicacion = "";
    String wvarDataIn = "";
    String wvarFECHA = "";
    java.util.Date wvarDateAux = DateTime.EmptyDate;
    //
    //
    wvarAplicacion = "OV_AlertasxMail_Ctrl";
    wvarCodusu = "OVFILLSCH";
    wvarDataIn = "<FECHA>" + pvarFECHA + "</FECHA>";
    //
    //Calculo de rango de fechas
    wvarDateAux = DateTime.add( DateTime.toDate( Strings.mid( pvarFECHA, 4, 2 ) + "-" + Strings.mid( pvarFECHA, 1, 2 ) + "-" + Strings.mid( pvarFECHA, 7, 4 ) ), 1 );
    wvarFECHA = Strings.right( "00" + DateTime.month( wvarDateAux ), 2 ) + "-" + Strings.right( ("00" + DateTime.day( wvarDateAux )), 2 ) + "-" + DateTime.year( wvarDateAux );
    //
    insertControlSCH = insertBandejaSCH(wvarAplicacion, wvarCodusu, wvarDataIn, wvarFECHA);
    //
    return insertControlSCH;
  }

  private boolean insertBandejaSCH( String pvarAplicacion, String pvarCODUSU, String pvarDataIn, String pvarFECHA ) throws Exception
  {
    boolean insertBandejaSCH = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    String wvarCodusu = "";
    String wvarAplicacion = "";
    String wvarDataIn = "";
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    //FIXME JDBC
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDBACTIONS);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsBandejaSCH );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      insertBandejaSCH = true;
    }
    else
    {
      insertBandejaSCH = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return insertBandejaSCH;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      debugToBD(pvarCODOP, pvarDescripcion);
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      debugToFile(pvarCODOP, pvarDescripcion);
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    boolean debugToBD = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    //FIXME JDBC
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    XmlDomExtended wobjXMLConfig = null;
    String wvarText = "";
    String wvarFilename = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion;

    wvarFilename = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend( System.getProperty("user.dir") + "\\DEBUG\\" + wvarFilename, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private String getListadoProductores( String pvarSPListarProd, String pvarCODOP, String pvarFECHA, String pvarREPROCESO ) throws Exception
  {
    String getListadoProductores = "";
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstDBResult = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    //FIXME JDBC
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( pvarSPListarProd );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    if( pvarREPROCESO.equals( "S" ) )
    {
    }
    //
    wrstDBResult = wobjDBCmd.execute();
    wrstDBResult.setActiveConnection( (Connection) null );
    //
    //Controlamos la respuesta del SQL
    if( (wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0) && ! (wrstDBResult.isEOF()) )
    {
      //
      wobjXMLResponse = new XmlDomExtended();
      wobjXSLResponse = new XmlDomExtended();
      //
      wobjXSLResponse.loadXML( p_GetXSL());

      wobjXMLResponse.load(AdoUtils.saveRecordset(wrstDBResult));
      //
      getListadoProductores = "<PRODUCTORES>" + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ) + "</PRODUCTORES>";

      wobjXMLResponse = null;
      wobjXSLResponse = null;
    }
    else
    {
      getListadoProductores = "<PRODUCTORES/>";
    }

    return getListadoProductores;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='PRODUCTOR'>";

    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDSUSCRIPTOS'><xsl:value-of select='@IDSUSCRIPTOS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDCONTROL'><xsl:value-of select='@IDCONTROL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NIVELAS'><xsl:value-of select='@NIVELAS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSECAS'><xsl:value-of select='@CLIENSECAS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>";

    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;

    return p_GetXSL;
  }

  private int agregarControl( String pvarIDSUSCRIPTOS, String pvarCODOP ) throws Exception
  {
    int agregarControl = 0;
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //FIXME JDBC
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsControl );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      agregarControl = wobjDBCmd.getParameters().getParameter("@IDCONTROL").getValue().toInt();
    }
    else
    {
      agregarControl = 0;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return agregarControl;
  }

  private boolean analizarFechaEjecucion( String pvarCODOP ) throws Exception
  {
    boolean analizarFechaEjecución = false;
    XmlDomExtended wobjXMLParametro = null;
    int wvarSemana = 0;
    int wvarDia = 0;
    java.util.Date wvarDateAux = DateTime.EmptyDate;
    int wvarDayCount = 0;
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msg" + pvarCODOP + ".xml" );
    //
    //Solo analiza el caso en que la ejecución es MENSUAL, ya que para el SEMANAL esta
    //configurado el scheduler.
    if( XmlDomExtended .getText( wobjXMLParametro.selectSingleNode( "//PARAMETROS/EJECUCION" )  ).equals( "MENSUAL" ) )
    {
      wvarSemana = Obj.toInt( XmlDomExtended .getText( wobjXMLParametro.selectSingleNode( "//PARAMETROS/EJECUCION/@SEMANA" )  ) );
      wvarDia = Obj.toInt( XmlDomExtended .getText( wobjXMLParametro.selectSingleNode( "//PARAMETROS/EJECUCION/@DIA" )  ) );
      //
      //Calcula la semana actual
      wvarDateAux = DateTime.dateSerial( DateTime.year( DateTime.now() ), DateTime.month( DateTime.now() ), 1 );
      wvarDayCount = 1;
      while( ! ((DateTime.weekday( wvarDateAux, 0 ) == wvarDia) && (wvarDayCount == wvarSemana)) )
      {
        wvarDateAux = DateTime.add( wvarDateAux, 1 );
        if( DateTime.weekday( wvarDateAux, 0 ) == 1 )
        {
          wvarDayCount = wvarDayCount + 1;
        }
      }

      if( DateTime.diff( "d", wvarDateAux, DateTime.now() ) == 0 )
      {
        analizarFechaEjecución = true;
      }
      else
      {
        analizarFechaEjecución = false;
      }

    }
    else
    {
      analizarFechaEjecución = true;
    }

    insertLog("ANALIZARFECHA", String.valueOf( new Variant(wvarSemana) ) + " - " + String.valueOf( new Variant(wvarDia) ));
    //
    return analizarFechaEjecución;
  }

}
