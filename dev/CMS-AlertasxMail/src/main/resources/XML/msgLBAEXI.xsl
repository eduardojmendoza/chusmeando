<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: msgLBAEXI.xsl   
	Fecha de Creación: 19/03/2009   
	PPCR: 2008-00791
	Desarrollador: Matias Coaker
	Descripción: Este archivo XSL se crea para configurar la ejecución y request
				 del código de operacion LBAEXI.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<Request>
			<EstadosMsgAIS>
				<xsl:copy-of select="/ROOT/Response/Estado[1]"/>
			</EstadosMsgAIS>
			<xmlDataSource>
				<Response>
					<DATOSTRUNCADOS>
						<xsl:value-of select="//DATOSTRUNCADOS"/>
					</DATOSTRUNCADOS>
					<FECHADES>
						<xsl:value-of select="//FECHADES"/>
					</FECHADES>
					<FECHAHAS>
						<xsl:value-of select="//FECHAHAS"/>
					</FECHAHAS>
					<CANTREGS>
						<xsl:value-of select="count(//REG)"/>
					</CANTREGS>
					<REGS>
						<xsl:for-each select="//REG">
							<REG>
								<AGE>
									<xsl:value-of select="AGE"/>
								</AGE>
								<SINI>
									<xsl:value-of select="SINI"/>
								</SINI>
								<RAMO>
									<xsl:value-of select="RAMO"/>
								</RAMO>
								<CLIDES>
									<xsl:value-of select="CLIDES"/>
								</CLIDES>
								<PROD>
									<xsl:value-of select="PROD"/>
								</PROD>
								<POL>
									<xsl:value-of select="concat(substring(POL,1,2),'-',substring(POL,3,4))"/>
								</POL>
								<CERPOL>
									<xsl:value-of select="CERPOL"/>
								</CERPOL>
								<CERANN>
									<xsl:value-of select="CERANN"/>
								</CERANN>
								<CERSEC>
									<xsl:value-of select="CERSEC"/>
								</CERSEC>
								<MON>
									<xsl:value-of select="MON"/>
								</MON>
								<IMPTO>
									<xsl:value-of select="IMPTO"/>
								</IMPTO>
								<I30>
									<xsl:value-of select="I_30"/>
								</I30>
								<I60>
									<xsl:value-of select="I_60"/>
								</I60>
								<I90>
									<xsl:value-of select="I_90"/>
								</I90>
								<IM90>
									<xsl:value-of select="I_M90"/>
								</IM90>
								<EST>
									<xsl:value-of select="EST"/>
								</EST>
								<COB>
									<xsl:value-of select="COB"/>
								</COB>
							</REG>
						</xsl:for-each>
					</REGS>
					<TOTS>
						<xsl:for-each select="//TOT">
							<xsl:copy-of select="."/>
						</xsl:for-each>
					</TOTS>
				</Response>
			</xmlDataSource>
			<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>
			<Raiz>share:generateAndSendPdfReport</Raiz>
			<recipientTO/>
			<recipientsCC/>
			<recipientsBCC/>
			<templateFileName>LBAEmailOVDeudaExigiblePDF</templateFileName>
			<parametersTemplate/>
			<attachFileName>ListadoExigible.pdf</attachFileName>
			<from>ayuda.ovirtual@hsbc.com.ar</from>
			<replyTO>ayuda.ovirtual@hsbc.com.ar</replyTO>
			<bodyText/>
			<subject/>
			<importance>1</importance>
			<reportId>AlertaxMail_Exigible_v1.0</reportId>
			<attachPassword/>
			<imagePathFile>/web/BannerLBA.GIF</imagePathFile>
		</Request>
	</xsl:template>
</xsl:stylesheet>
