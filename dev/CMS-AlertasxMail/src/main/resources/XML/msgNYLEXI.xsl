<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: msgNYLEXI.xsl   
	Fecha de Creación: 19/03/2009   
	PPCR: 2008-00791
	Desarrollador: Matias Coaker
	Descripción: Este archivo XSL se crea para configurar la ejecución y request
				 del código de operacion NYLEXI.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<Request>
			<EstadosMsgAIS>
				<xsl:copy-of select="/ROOT/Response/Estado[1]"/>
			</EstadosMsgAIS>
			<xmlDataSource>
				<Response>
					<DATOSTRUNCADOS>
						<xsl:value-of select="//DATOSTRUNCADOS"/>
					</DATOSTRUNCADOS>				
					<FECHADES>
						<xsl:value-of select="//FECHADES"/>
					</FECHADES>
					<FECHAHAS>
						<xsl:value-of select="//FECHAHAS"/>
					</FECHAHAS>
					<CANTREGS>
						<xsl:value-of select="count(//REG)"/>
					</CANTREGS>
					<REGS>
						<xsl:for-each select="//REG">
							<REG>
								<AGE>
									<xsl:value-of select="AGENTCLA"/>-<xsl:value-of select="AGENTCOD"/>
								</AGE>
								<SINI>
									<xsl:value-of select="SINIESTRO"/>
								</SINI>
								<RAMO>
									<xsl:value-of select="MASIVO"/>
								</RAMO>
								<CLIDES>
									<xsl:value-of select="ASEGURADO"/>
								</CLIDES>
								<PROD>
									<xsl:value-of select="RAMOPCOD"/> 
								</PROD>
								<POL>
									<xsl:value-of select="POLIZANN"/>-<xsl:value-of select="POLIZSEC"/>
								</POL>
								<CERPOL>
									<xsl:value-of select="CERTIPOL"/>
								</CERPOL>
								<CERANN>
									<xsl:value-of select="CERTIANN"/>
								</CERANN>
								<CERSEC>
									<xsl:value-of select="CERTISEC"/>
								</CERSEC>
								<MON>
									<xsl:value-of select="MONEDA"/>
								</MON>
								<IMPTO>
									<xsl:value-of select="TOTAL"/>
								</IMPTO>
								<I30>
									<xsl:value-of select="IMPORTE30"/>
								</I30>
								<I60>
									<xsl:value-of select="IMPORTE60"/>
								</I60>
								<I90>
									<xsl:value-of select="IMPORTE90"/>
								</I90>
								<IM90>
									<xsl:value-of select="IMPOMAS90"/>
								</IM90>
								<EST>
									<xsl:value-of select="ESTADO"/>
								</EST>
								<COB>
									<xsl:value-of select="CANALCOBR"/>
								</COB>
							</REG>
						</xsl:for-each>
					</REGS>
					<TOTS>
						<xsl:for-each select="//TOT">
							<xsl:copy-of select="."/>
						</xsl:for-each>
					</TOTS>
				</Response>
			</xmlDataSource>
			<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>
			<Raiz>share:generateAndSendPdfReport</Raiz>
			<recipientTO/>
			<recipientsCC/>
			<recipientsBCC/>
			<templateFileName>NYLEmailOVDeudaExigiblePDF</templateFileName>
			<parametersTemplate/>
			<attachFileName>ListadoExigible.pdf</attachFileName>
			<from>ayuda.ovirtual@hsbc.com.ar</from>
			<replyTO>ayuda.ovirtual@hsbc.com.ar</replyTO>
			<bodyText/>
			<subject/>
			<importance>1</importance>
			<reportId>AlertaxMail_Exigible_v1.0</reportId>
			<attachPassword/>
			<imagePathFile>/web/BannerNYL.GIF</imagePathFile>
		</Request>
	</xsl:template>
</xsl:stylesheet>
