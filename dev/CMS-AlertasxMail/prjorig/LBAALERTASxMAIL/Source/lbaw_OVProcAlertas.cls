VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVProcAlertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_AlertasxMail.lbaw_OVProcAlertas"

'Stored Procedures
Const mcteStoreProcListarSuscriptos As String = "P_ALERTAS_LISTAR_SUSCRIPTOS"
Const mcteStoreProcListarPendientes As String = "P_ALERTAS_LISTAR_PENDIENTES"
Const mcteStoreProcUpdControlEnv    As String = "P_ALERTAS_UPDATE_CONTROL_ENVIADO"
Const mcteStoreProcInsControl       As String = "P_ALERTAS_INSERT_CONTROL"
Const mcteStoreProcInsLog           As String = "P_ALERTAS_INSERT_LOG"
Const mcteSPObtenerIdentificador    As String = "SPCAM_CAM_OBTENER_IDENTIFICADOR"
'
Const mcteParam_CODOP               As String = "//CODOP"
Const mcteParam_FECHA               As String = "//FECHA"
'
Const mcteOperacion                 As String = "PROCALERTAS"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarStep            As Long
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    'Parámetros de entrada
    Dim mvarCODOP           As String
    Dim mvarFECHA           As String
    '
    Dim wobjXMLResponseAIS  As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLListadoProd  As MSXML2.DOMDocument
    Dim wobjXMLNode         As MSXML2.IXMLDOMNode
    '
    Dim mvarSPListarProd    As String
    Dim mvarREPROCESO       As String
    Dim mvarIDCONTROL       As Long
    Dim mvarResponseAIS     As String
    Dim mvarEnvioMDW        As Boolean
    Dim mvarProcConErrores  As Boolean
    Dim mvarProcesar        As Boolean
    Dim mvarPendientes      As Integer
    Dim mvarMSGError        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicialización de variables
    mvarProcesar = True
    mvarFECHA = ""
    mvarPendientes = 0
    mvarProcConErrores = False
    '
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    With wobjXMLRequest
        mvarCODOP = .selectSingleNode(mcteParam_CODOP).Text
        wvarStep = 31
        If Not .selectSingleNode(mcteParam_FECHA) Is Nothing Then
            mvarFECHA = .selectSingleNode(mcteParam_FECHA).Text
        End If
    End With
    '
    'Registración en Log de inicio de ejecución
    wvarStep = 30
    insertLog mcteOperacion, "1- Nueva ejecución - Validación de parámetros"
    '
    'Si parametro FECHA definido, significa que hay que reprocesar los pendientes
    'para una fecha y código de operacion particular
    'Si FECHA definido, se marca REPROCESO en S y se selecciona el SP que
    'recupera los casos en los que no se pudo generar/enviar el email para
    'esa fecha
    'Sino, se marca REPROCESO en N y se selecciona el SP que recupera todos
    'los productores con CODOP en S
    wvarStep = 40
    If mvarFECHA <> "" Then
        mvarREPROCESO = "S"
        mvarSPListarProd = mcteStoreProcListarPendientes
    Else
        mvarFECHA = Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "-" & Year(Now())
        mvarREPROCESO = "N"
        mvarSPListarProd = mcteStoreProcListarSuscriptos
        '
        'Verifica la fecha de ejecución para el impreso seleccionado
        mvarProcesar = analizarFechaEjecución(mvarCODOP)
    End If
    '
    'Obtención de listado de productores a procesar...
    wvarStep = 60
     Set wobjXMLListadoProd = CreateObject("MSXML2.DOMDocument")
    wobjXMLListadoProd.loadXML getListadoProductores(mvarSPListarProd, mvarCODOP, mvarFECHA, mvarREPROCESO)
    '
    'Registración en Log de inicio de proceso
    wvarStep = 50
    insertLog mcteOperacion, "2- Validación Ok - Inicio de proceso (CODOP:" & mvarCODOP & " REPRO:" & mvarREPROCESO & " Regs:" & wobjXMLListadoProd.selectNodes("//PRODUCTOR").length & ")"
    '
    If mvarProcesar Then
        'Mientras queden productores sin procesar en el listado...
        wvarStep = 70
        mvarPendientes = wobjXMLListadoProd.selectNodes("//PRODUCTOR").length
        '
        'En caso de error se resume para evitar el corte del proceso (se realiza control de err.Number)
        On Error Resume Next
        '
        For Each wobjXMLNode In wobjXMLListadoProd.selectNodes("//PRODUCTOR")
            '
            'Es reproceso?
            wvarStep = 80
            If mvarREPROCESO = "N" Then
                'No - Se insertan los datos de la operacion en la tabla de control
                wvarStep = 90
                mvarIDCONTROL = agregarControl(wobjXMLNode.selectSingleNode("IDSUSCRIPTOS").Text, mvarCODOP)
            Else
                mvarIDCONTROL = wobjXMLNode.selectSingleNode("IDCONTROL").Text
            End If
            '
            'Registración en Log de inicio de caso a procesar.
            insertLog mcteOperacion, "a- Procesando operación... (IDCONTROL: " & mvarIDCONTROL & ")"
            '
            wvarStep = 100
            wvarRequest = "<Request>" & _
                            "<CODOP>" & mvarCODOP & "</CODOP>" & _
                            "<USUARCOD>" & wobjXMLNode.selectSingleNode("USUARCOD").Text & "</USUARCOD>" & _
                            "<NIVELAS>" & wobjXMLNode.selectSingleNode("NIVELAS").Text & "</NIVELAS>" & _
                            "<CLIENSECAS>" & wobjXMLNode.selectSingleNode("CLIENSECAS").Text & "</CLIENSECAS>" & _
                            "<EMAIL>" & wobjXMLNode.selectSingleNode("EMAIL").Text & "</EMAIL>" & _
                            "<FECHAEXEC>" & mvarFECHA & "</FECHAEXEC>" & _
                            "<IDCONTROL>" & mvarIDCONTROL & "</IDCONTROL>" & _
                          "</Request>"
            '
            wvarStep = 110
            Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_AlertasxMail.lbaw_OVExeSch")
            Call wobjClass.Execute(wvarRequest, wvarResponse, "")
            Set wobjClass = Nothing
            '
            wvarStep = 120
            If Not wobjXMLResponseAIS.selectSingleNode("//Response/Estado/@resultado") Is Nothing And Err.Number = 0 Then
                If wobjXMLResponseAIS.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
                    mvarPendientes = mvarPendientes - 1
                    wvarStep = 130
                    insertLog mcteOperacion, "b- Fin Oper. con EXITO. (IDCONTROL: " & mvarIDCONTROL & ")"
                Else
                    wvarStep = 140
                    insertLog mcteOperacion, "b- Fin Oper. con ERROR. (IDCONTROL: " & mvarIDCONTROL & ")"
                    mvarProcConErrores = True
                End If
            Else
                wvarStep = 150
                insertLog mcteOperacion, "b- Fin Oper. con ERROR. (IDCONTROL: " & mvarIDCONTROL & ")"
                mvarProcConErrores = True
            End If
            '
            'Loop
            wvarStep = 230
        Next
        '
        'Reestablece el estado de On Error
        On Error GoTo ErrorHandler
        '
        'Quedaron alertas en estado pendiente y REPROCESO = N?
        wvarStep = 250
        If (mvarProcConErrores And mvarREPROCESO = "N") Then
            'Envio de email informando la existencia de operaciones pendientes en la fecha
            wvarStep = 260
            enviarEMail mvarCODOP, mvarPendientes, "ALGUNAS OPERACIONES QUEDARON PENDIENTES", Now()
        End If
    End If
    '
    'Registración en Log de fin de proceso
    wvarStep = 240
    If mvarProcesar = False Then
        insertLog mcteOperacion, "3- Fin de procesamiento. Estado: NO EJECUTADO POR PARAMETRO"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    ElseIf mvarProcConErrores Then
        insertLog mcteOperacion, "3- Fin de procesamiento. Estado: ERROR - Pendientes: " & mvarPendientes
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    Else
        insertLog mcteOperacion, "3- Fin de procesamiento. Estado: OK - Pendientes: " & mvarPendientes
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    End If
    '
    'FIN
    '
    wvarStep = 250
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    wvarStep = 260
    Set wobjXMLResponseAIS = Nothing
    Set wobjXMLRequest = Nothing
    Set wobjXMLListadoProd = Nothing
    Set wobjXMLNode = Nothing
    Exit Function
    
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    'Inserta error en Log
    insertLog mcteOperacion, mcteClassName & " - " & _
                     wcteFnName & " - " & _
                     wvarStep & " - " & _
                     Err.Number & " - " & _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & _
                     vbLogEventTypeError
    
    insertLog mcteOperacion, "99- Fin de procesamiento. Estado: ABORTADO POR ERROR"
          
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function insertLog(ByVal pvarCODOP As String, ByVal pvarDescripcion As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarReqRespInTxt    As Boolean
    Dim mvarDebugCode       As Integer
    '
    
    wvarReqRespInTxt = False
    '
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\XML\Config.xml"
    '1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = CInt(wobjXMLConfig.selectSingleNode("//DEBUG").Text)
    '
    If InStr(1, UCase(pvarDescripcion), "<REQUEST>", vbTextCompare) > 0 Or InStr(1, UCase(pvarDescripcion), "<RESPONSE>", vbTextCompare) > 0 Then
        wvarReqRespInTxt = True
    End If
    '
    If mvarDebugCode >= 1 And Not wvarReqRespInTxt Then
        debugToBD pvarCODOP, pvarDescripcion
    End If
    If mvarDebugCode = 3 Or (mvarDebugCode = 2 And Not wvarReqRespInTxt) Then
        debugToFile "REPROCESO", pvarDescripcion
    End If
    '
End Function

Private Function debugToBD(ByVal pvarCODOP As String, ByVal pvarDescripción As String) As Boolean
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsLog
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, Left(pvarCODOP, 6))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DESCRIPCION", adChar, adParamInput, 100, Left(pvarDescripción, 100))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        debugToBD = True
    Else
        debugToBD = False
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Sub debugToFile(ByVal pvarCODOP As String, ByVal pvarDescripcion As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarText            As String
    Dim wvarFilename        As String
    Dim wvarNroArch         As Long
    '
    wvarText = "(" & Date & "-" & Time & "):" & pvarDescripcion
        
    wvarFilename = "debug-" & pvarCODOP & "-" & Year(Date) & Right("00" & Month(Date), 2) & Right("00" & Day(Date), 2) & ".log"
    wvarNroArch = FreeFile()
    Open App.Path & "\DEBUG\" & wvarFilename For Append As #1
    Write #1, wvarText
    Close #1
    '
End Sub

Private Function getListadoProductores(pvarSPListarProd As String, pvarCODOP As String, pvarFECHA As String, pvarREPROCESO As String) As String
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstDBResult        As ADODB.Recordset
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = pvarSPListarProd
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, pvarCODOP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    If pvarREPROCESO = "S" Then
        Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHA", adVarChar, adParamInput, 10, Left(pvarFECHA, 10))
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    End If
    '
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 And Not wrstDBResult.EOF Then
        '
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        getListadoProductores = "<PRODUCTORES>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</PRODUCTORES>"
        
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
    Else
        getListadoProductores = "<PRODUCTORES/>"
    End If
    
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='PRODUCTOR'>"
        
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='IDSUSCRIPTOS'><xsl:value-of select='@IDSUSCRIPTOS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='IDCONTROL'><xsl:value-of select='@IDCONTROL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NIVELAS'><xsl:value-of select='@NIVELAS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSECAS'><xsl:value-of select='@CLIENSECAS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>"

    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    
    p_GetXSL = wvarStrXSL
    
End Function

Private Function agregarControl(ByVal pvarIDSUSCRIPTOS As String, ByVal pvarCODOP As String) As Long
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsControl
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDSUSCRIPTOS", adInteger, adParamInput, , pvarIDSUSCRIPTOS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, Left(pvarCODOP, 6))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDCONTROL", adInteger, adParamInputOutput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        agregarControl = wobjDBCmd.Parameters("@IDCONTROL").Value
    Else
        agregarControl = 0
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Function analizarFechaEjecución(pvarCODOP As String) As Boolean
    '
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    Dim wvarSemana          As Integer
    Dim wvarDia             As Integer
    Dim wvarDateAux         As Date
    Dim wvarDayCount        As Integer
    '
    Set wobjXMLParametro = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametro.Load App.Path & "\XML\msg" & pvarCODOP & ".xml"
    '
    'Solo analiza el caso en que la ejecución es MENSUAL, ya que para el SEMANAL esta
    'configurado el scheduler.
    If wobjXMLParametro.selectSingleNode("//PARAMETROS/EJECUCION").Text = "MENSUAL" Then
        wvarSemana = CInt(wobjXMLParametro.selectSingleNode("//PARAMETROS/EJECUCION/@SEMANA").Text)
        wvarDia = CInt(wobjXMLParametro.selectSingleNode("//PARAMETROS/EJECUCION/@DIA").Text)
        '
        'Calcula la semana actual
        wvarDateAux = DateValue("01" & "-" & Month(Now) & "-" & Year(Now))
        wvarDayCount = 1
        While Not (Weekday(wvarDateAux) = wvarDia And wvarDayCount = wvarSemana)
            wvarDateAux = wvarDateAux + 1
            If Weekday(wvarDateAux) = 1 Then
                wvarDayCount = wvarDayCount + 1
            End If
        Wend
        If DateDiff("d", wvarDateAux, Date) = 0 Then
            analizarFechaEjecución = True
        Else
            analizarFechaEjecución = False
        End If

    Else
        analizarFechaEjecución = True
    End If
    '
End Function
'
Private Function enviarEMail(pvarCODOP As String, pvarPendientes As Integer, pvarEstado As String, pvarFECHA As String)
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarXMLRequest      As MSXML2.DOMDocument
    Dim wvarXMLResponse     As MSXML2.DOMDocument
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    Dim wobjXMLCODOPParam   As MSXML2.DOMDocument
    Dim wvarRellamar        As Boolean
    Dim wvarResponseAIS     As String
    Dim wvarCODOPDesc       As String
    '
    Set wobjXMLParametro = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametro.Load App.Path & "\XML\msgEnvioMail.xml"
    Set wobjXMLCODOPParam = CreateObject("MSXML2.DOMDocument")
    wobjXMLCODOPParam.Load App.Path & "\XML\msg" & pvarCODOP & ".xml"
    '
    wvarCODOPDesc = wobjXMLCODOPParam.selectSingleNode("//DESCRIPCION").xml
    '
    agregarParametro wobjXMLParametro, "%%FECHA%%", pvarFECHA
    agregarParametro wobjXMLParametro, "%%FORMULARIO%%", wvarCODOPDesc
    agregarParametro wobjXMLParametro, "%%ESTADO%%", pvarEstado
    agregarParametro wobjXMLParametro, "%%PENDIENTES%%", CStr(pvarPendientes)
    '
    wvarRequest = wobjXMLParametro.selectSingleNode("//Request").xml
    '
    Set wobjClass = mobjCOM_Context.CreateInstance("cam_OficinaVirtual.camA_EnviarMail")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    Set wvarXMLResponse = CreateObject("MSXML2.DOMDocument")
        wvarXMLResponse.async = False
        wvarXMLResponse.loadXML (wvarResponse)
    '
    If Not wvarXMLResponse.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If wvarXMLResponse.selectSingleNode("//Response/Estado/@resultado").Text = "true" Then
            enviarEMail = True
        Else
            enviarEMail = False
        End If
    Else
        enviarEMail = False
    End If
    '
    Set wvarXMLResponse = Nothing
    Set wobjClass = Nothing
    Set wobjXMLParametro = Nothing
End Function

Private Sub agregarParametro(ByRef pobjXMLParametro As MSXML2.DOMDocument, pvarParam As String, pvarValor As String)
    Dim wobjXMLElement      As MSXML2.IXMLDOMElement
    Dim wobjXMLElementAux  As MSXML2.IXMLDOMElement
    Dim wobjXMLElementCda  As MSXML2.IXMLDOMCDATASection
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    
    Set wobjXMLElement = pobjXMLParametro.createElement("PARAMETRO")
    '
    Set wobjXMLElementAux = pobjXMLParametro.createElement("PARAM_NOMBRE")
    Set wobjXMLElementCda = pobjXMLParametro.createCDATASection(pvarParam)
        wobjXMLElementAux.appendChild wobjXMLElementCda
        wobjXMLElement.appendChild wobjXMLElementAux
    Set wobjXMLElementAux = pobjXMLParametro.createElement("PARAM_VALOR")
    Set wobjXMLElementCda = pobjXMLParametro.createCDATASection(pvarValor)
        wobjXMLElementAux.appendChild wobjXMLElementCda
    '
    wobjXMLElement.appendChild wobjXMLElementAux
    '
    pobjXMLParametro.selectSingleNode("//PARAMETROS").appendChild wobjXMLElement
    '
End Sub

Private Sub ObjectControl_Activate()

    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub








