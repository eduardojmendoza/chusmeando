VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVFillSch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName                 As String = "lbawA_AlertasxMail.lbaw_OVFillSch"

'Stored Procedures
Const mcteStoreProcInsBandejaSCH    As String = "P_CP_SA_BANDEJA_INSERT"
Const mcteStoreProcListarSuscriptos As String = "P_ALERTAS_LISTAR_SUSCRIPTOS"
Const mcteStoreProcInsControl       As String = "P_ALERTAS_INSERT_CONTROL"
Const mcteStoreProcInsLog           As String = "P_ALERTAS_INSERT_LOG"
'
Const mcteParam_INSCTRL             As String = "//INSCTRL"
'
Const mcteOperacion                 As String = "FILLSCH"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarStep            As Long
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    Dim wobjXMLListadoProd  As MSXML2.DOMDocument
    Dim wobjXMLNode         As MSXML2.IXMLDOMNode
    Dim wobjXMLConfigNode   As MSXML2.IXMLDOMNode
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    '
    Dim mvarIDCONTROL       As Long
    Dim mvarINSCTRL         As String
    Dim mvarResponseAIS     As String
    Dim mvarEnvioMDW        As Boolean
    Dim mvarProcConErrores  As Boolean
    Dim mvarProcesar        As Boolean
    Dim mvarPendientes      As Integer
    Dim mvarMSGError        As String
    Dim mvarFECHA           As String
    Dim mvarCODOP           As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Inicialización de variables
    wvarStep = 10
    mvarFECHA = Right("00" & Month(Now()), 2) & "-" & Right("00" & Day(Now()), 2) & "-" & Year(Now())
    mvarINSCTRL = "N"
    '
    'Lectura de parámetros
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 25
    With wobjXMLRequest
        If Not .selectSingleNode(mcteParam_INSCTRL) Is Nothing Then
            mvarINSCTRL = .selectSingleNode(mcteParam_INSCTRL).Text
        End If
    End With
    '
    'Registración en Log de inicio de ejecución
    wvarStep = 30
    insertLog mcteOperacion, "I-OVFILLSCH - Nueva ejecución"
    '
    wvarStep = 40
    'Se obtienen los CODOP a procesar
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\XML\Config.xml"
    '
    wvarStep = 50
    For Each wobjXMLConfigNode In wobjXMLConfig.selectNodes("//CODOP")
        '
        wvarStep = 60
        mvarCODOP = wobjXMLConfigNode.Text
        '
        'Verifica la fecha de ejecución para el impreso seleccionado
        wvarStep = 70
        mvarProcesar = analizarFechaEjecución(mvarCODOP)
        '
        wvarStep = 80
        If mvarProcesar Then
            '
            'Obtención de listado de productores a procesar...
            wvarStep = 90
            Set wobjXMLListadoProd = CreateObject("MSXML2.DOMDocument")
            wobjXMLListadoProd.loadXML getListadoProductores(mcteStoreProcListarSuscriptos, mvarCODOP, mvarFECHA, "N")
            '
            'Mientras queden productores sin procesar en el listado...
            wvarStep = 100
            mvarPendientes = wobjXMLListadoProd.selectNodes("//PRODUCTOR").length
            '
            For Each wobjXMLNode In wobjXMLListadoProd.selectNodes("//PRODUCTOR")
                '
                'Se insertan los datos de la operacion en la tabla de control
                wvarStep = 110
                mvarIDCONTROL = agregarControl(wobjXMLNode.selectSingleNode("IDSUSCRIPTOS").Text, mvarCODOP)
                '
                wvarStep = 120
                insertAlertasSCH wobjXMLNode.selectSingleNode("USUARCOD").Text, wobjXMLNode.selectSingleNode("NIVELAS").Text, wobjXMLNode.selectSingleNode("CLIENSECAS").Text, wobjXMLNode.selectSingleNode("EMAIL").Text, mvarCODOP, mvarIDCONTROL, mvarFECHA
                '
            'Loop
            wvarStep = 140
            Next
            '
        wvarStep = 150
        End If
        '
    'Loop
    wvarStep = 160
    Next
    '
    'Registración en Log de fin de proceso
    wvarStep = 170
    If mvarProcConErrores Then
        insertLog mcteOperacion, "E- Fin OVFILLSCH. Estado: ERROR"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    Else
        'insertControlSCH
        If mvarINSCTRL = "S" Then
            insertControlSCH mvarFECHA
        End If
        insertLog mcteOperacion, "0- Fin OVFILLSCH. Estado: OK"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /></Response>"
    End If
    '
    'FIN
    '
    wvarStep = 180
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    wvarStep = 190
    Set wobjXMLListadoProd = Nothing
    Set wobjXMLNode = Nothing
    Exit Function
    
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    'Inserta Error en EventViewer
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    'Inserta error en Log
    insertLog mcteOperacion, mcteClassName & " - " & _
                     wcteFnName & " - " & _
                     wvarStep & " - " & _
                     Err.Number & " - " & _
                     "Error= [" & Err.Number & "] - " & Err.Description & " - " & _
                     vbLogEventTypeError
    
    insertLog mcteOperacion, "99- Fin OVFILLSCH. Estado: ABORTADO POR ERROR"
          
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function insertAlertasSCH(pvarUSUARCOD As String, pvarNIVELAS As String, pvarCLIENSECAS As String, pvarEMAIL As String, ByVal pvarCODOP As String, ByVal pvarIDCONTROL As Long, ByVal pvarFECHA As String) As Boolean
    '
    Dim wvarCodusu          As String
    Dim wvarAplicacion      As String
    Dim wvarDataIn          As String
    '
    wvarAplicacion = "OV_AlertaxMail"
    wvarCodusu = "OVFILLSCH"
    wvarDataIn = "<CODOP>" & pvarCODOP & "</CODOP>" & _
                 "<USUARCOD>" & pvarUSUARCOD & "</USUARCOD>" & _
                 "<NIVELAS>" & pvarNIVELAS & "</NIVELAS>" & _
                 "<CLIENSECAS>" & pvarCLIENSECAS & "</CLIENSECAS>" & _
                 "<EMAIL>" & pvarEMAIL & "</EMAIL>" & _
                 "<FECHAEXEC>" & pvarFECHA & "</FECHAEXEC>" & _
                 "<IDCONTROL>" & pvarIDCONTROL & "</IDCONTROL>"
    '
    insertAlertasSCH = insertBandejaSCH(wvarAplicacion, wvarCodusu, wvarDataIn, pvarFECHA)
    '
End Function

Private Function insertControlSCH(ByVal pvarFECHA As String) As Boolean
    '
    Dim wvarCodusu          As String
    Dim wvarAplicacion      As String
    Dim wvarDataIn          As String
    Dim wvarFECHA           As String
    Dim wvarDateAux         As Date
    '
    wvarAplicacion = "OV_AlertasxMail_Ctrl"
    wvarCodusu = "OVFILLSCH"
    wvarDataIn = "<FECHA>" & pvarFECHA & "</FECHA>"
    '
    'Calculo de rango de fechas
    wvarDateAux = DateValue(Mid(pvarFECHA, 4, 2) & "-" & Mid(pvarFECHA, 1, 2) & "-" & Mid(pvarFECHA, 7, 4)) + 1
    wvarFECHA = Right("00" & Month(wvarDateAux), 2) & "-" & Right("00" & Day(wvarDateAux), 2) & "-" & Year(wvarDateAux)
    '
    insertControlSCH = insertBandejaSCH(wvarAplicacion, wvarCodusu, wvarDataIn, wvarFECHA)
    '
End Function

Private Function insertBandejaSCH(pvarAplicacion As String, pvarCODUSU As String, pvarDataIn As String, pvarFECHA As String) As Boolean
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarCodusu          As String
    Dim wvarAplicacion      As String
    Dim wvarDataIn          As String
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDBACTIONS)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsBandejaSCH
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@APLIC", adVarChar, adParamInput, 20, pvarAplicacion)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@SOLICITUD", adInteger, adParamInput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PASO", adInteger, adParamInput, , 100)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHAEXEC", adChar, adParamInput, 10, pvarFECHA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DATAIN", adChar, adParamInput, 1000, Left(pvarDataIn, 1000))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@COD_USU", adChar, adParamInput, 20, pvarCODUSU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        insertBandejaSCH = True
    Else
        insertBandejaSCH = False
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Function insertLog(ByVal pvarCODOP As String, ByVal pvarDescripcion As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarReqRespInTxt    As Boolean
    Dim mvarDebugCode       As Integer
    '
    
    wvarReqRespInTxt = False
    '
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.Load App.Path & "\XML\Config.xml"
    '1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = CInt(wobjXMLConfig.selectSingleNode("//DEBUG").Text)
    '
    If InStr(1, UCase(pvarDescripcion), "<REQUEST>", vbTextCompare) > 0 Or InStr(1, UCase(pvarDescripcion), "<RESPONSE>", vbTextCompare) > 0 Then
        wvarReqRespInTxt = True
    End If
    '
    If mvarDebugCode >= 1 And Not wvarReqRespInTxt Then
        debugToBD pvarCODOP, pvarDescripcion
    End If
    If mvarDebugCode = 3 Or (mvarDebugCode = 2 And Not wvarReqRespInTxt) Then
        debugToFile pvarCODOP, pvarDescripcion
    End If
    '
End Function

Private Function debugToBD(ByVal pvarCODOP As String, ByVal pvarDescripción As String) As Boolean
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsLog
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, Left(pvarCODOP, 6))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DESCRIPCION", adChar, adParamInput, 100, Left(pvarDescripción, 100))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        debugToBD = True
    Else
        debugToBD = False
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Sub debugToFile(ByVal pvarCODOP As String, ByVal pvarDescripcion As String)
    '
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wvarText            As String
    Dim wvarFilename        As String
    Dim wvarNroArch         As Long
    '
    wvarText = "(" & Date & "-" & Time & "):" & pvarDescripcion
        
    wvarFilename = "debug-" & pvarCODOP & "-" & Year(Date) & Right("00" & Month(Date), 2) & Right("00" & Day(Date), 2) & ".log"
    wvarNroArch = FreeFile()
    Open App.Path & "\DEBUG\" & wvarFilename For Append As #1
    Write #1, wvarText
    Close #1
    '
End Sub

Private Function getListadoProductores(pvarSPListarProd As String, pvarCODOP As String, pvarFECHA As String, pvarREPROCESO As String) As String
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstDBResult        As ADODB.Recordset
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = pvarSPListarProd
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, pvarCODOP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    If pvarREPROCESO = "S" Then
        Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHA", adVarChar, adParamInput, 10, Left(pvarFECHA, 10))
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    End If
    '
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 And Not wrstDBResult.EOF Then
        '
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        getListadoProductores = "<PRODUCTORES>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</PRODUCTORES>"
        
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
    Else
        getListadoProductores = "<PRODUCTORES/>"
    End If
    
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='PRODUCTOR'>"
        
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='IDSUSCRIPTOS'><xsl:value-of select='@IDSUSCRIPTOS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='IDCONTROL'><xsl:value-of select='@IDCONTROL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NIVELAS'><xsl:value-of select='@NIVELAS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSECAS'><xsl:value-of select='@CLIENSECAS' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>"

    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    
    p_GetXSL = wvarStrXSL
    
End Function

Private Function agregarControl(ByVal pvarIDSUSCRIPTOS As String, ByVal pvarCODOP As String) As Long
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    'Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProcInsControl
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , "0")
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDSUSCRIPTOS", adInteger, adParamInput, , pvarIDSUSCRIPTOS)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CODOP", adChar, adParamInput, 6, Left(pvarCODOP, 6))
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDCONTROL", adInteger, adParamInputOutput, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wobjDBCmd.Execute adExecuteNoRecords
    '
    'Controlamos la respuesta del SQL
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value >= 0 Then
        agregarControl = wobjDBCmd.Parameters("@IDCONTROL").Value
    Else
        agregarControl = 0
    End If
    '
    Set wobjHSBC_DBCnn = Nothing
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    '
End Function

Private Function analizarFechaEjecución(pvarCODOP As String) As Boolean
    '
    Dim wobjXMLParametro    As MSXML2.DOMDocument
    Dim wvarSemana          As Integer
    Dim wvarDia             As Integer
    Dim wvarDateAux         As Date
    Dim wvarDayCount        As Integer
    '
    Set wobjXMLParametro = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametro.Load App.Path & "\XML\msg" & pvarCODOP & ".xml"
    '
    'Solo analiza el caso en que la ejecución es MENSUAL, ya que para el SEMANAL esta
    'configurado el scheduler.
    If wobjXMLParametro.selectSingleNode("//PARAMETROS/EJECUCION").Text = "MENSUAL" Then
        wvarSemana = CInt(wobjXMLParametro.selectSingleNode("//PARAMETROS/EJECUCION/@SEMANA").Text)
        wvarDia = CInt(wobjXMLParametro.selectSingleNode("//PARAMETROS/EJECUCION/@DIA").Text)
        '
        'Calcula la semana actual
        wvarDateAux = DateSerial(Year(Now), Month(Now), 1)
        wvarDayCount = 1
        While Not (Weekday(wvarDateAux) = wvarDia And wvarDayCount = wvarSemana)
            wvarDateAux = wvarDateAux + 1
            If Weekday(wvarDateAux) = 1 Then
                wvarDayCount = wvarDayCount + 1
            End If
        Wend

        If DateDiff("d", wvarDateAux, Date) = 0 Then
            analizarFechaEjecución = True
        Else
            analizarFechaEjecución = False
        End If

    Else
        analizarFechaEjecución = True
    End If
   
    insertLog "ANALIZARFECHA", CStr(wvarSemana) & " - " & CStr(wvarDia)
    '
End Function

Private Sub ObjectControl_Activate()

    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter

    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub










