package com.qbe.services.alertasxmail.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.alertasxmail.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVExeSch implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_AlertasxMail.lbaw_OVExeSch";
  /**
   * Stored Procedures
   */
  static final String mcteStoreProcUpdControlEnv = "P_ALERTAS_UPDATE_CONTROL_ENVIADO";
  static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
  static final String mcteStoreProcInsLog = "P_ALERTAS_INSERT_LOG";
  static final String mcteSPObtenerIdentificador = "SPCAM_CAM_OBTENER_IDENTIFICADOR";
  /**
   * 
   */
  static final String mcteParam_CODOP = "//CODOP";
  static final String mcteParam_IDCONTROL = "//IDCONTROL";
  static final String mcteParam_USUARCOD = "//USUARCOD";
  static final String mcteParam_CLIENSECAS = "//CLIENSECAS";
  static final String mcteParam_NIVELAS = "//NIVELAS";
  static final String mcteParam_EMAIL = "//EMAIL";
  static final String mcteParam_FECHA = "//FECHAEXEC";
  /**
   * 
   */
  static final String mcteOperacion = "EXESCH";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarCODOP = "";
    int mvarIDCONTROL = 0;
    String mvarUSUARCOD = "";
    String mvarNIVELAS = "";
    String mvarCLIENSECAS = "";
    String mvarEMAIL = "";
    String mvarFECHA = "";
    XmlDomExtended wobjXMLResponseAIS = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLListadoProd = null;
    org.w3c.dom.Node wobjXMLNode = null;
    XmlDomExtended wobjXMLConfig = null;
    String mvarResponseAIS = "";
    boolean mvarEnvioMDW = false;
    boolean mvarProcConErrores = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    int mvarCantRellamadosMAX = 0;
    String mvarCodError = "";
    //
    //
    //
    //Par�metros de entrada
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializaci�n de variables
      mvarProcConErrores = false;
      mvarCodError = "";
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      //
      wvarStep = 15;
      //Se obtienen los par�metros desde XML
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
      //
      wvarStep = 50;
      mvarCantRellamadosMAX = Obj.toInt( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//RELLAMADOSMAX" )  ) );
      //
      wvarStep = 20;
      mvarCODOP = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CODOP )  );
      wvarStep = 21;
      mvarIDCONTROL = Obj.toInt( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_IDCONTROL )  ) );
      wvarStep = 22;
      mvarUSUARCOD = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_USUARCOD )  );
      wvarStep = 23;
      mvarNIVELAS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NIVELAS )  );
      wvarStep = 24;
      mvarCLIENSECAS = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSECAS )  );
      wvarStep = 25;
      mvarEMAIL = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EMAIL )  );
      wvarStep = 26;
      mvarFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FECHA )  );
      //
      //Registraci�n en Log de inicio de ejecuci�n
      wvarStep = 40;
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("I-OVEXESCH - Nueva ejecuci�n - (CODOP:" + mvarCODOP + " - IDCONTROL: " + mvarIDCONTROL + ")") } );
      //
      //En caso de error se resume para evitar el corte del proceso (se realiza control de err.Number)
      // On Error Resume Next (optionally ignored)
      //
      //Recuperaci�n de datos de AIS para la operacion indicada
      wvarStep = 100;
      
      if( mvarCODOP.equals( "LBAANU" ) )
      {
        wvarStep = 110;
        mvarResponseAIS = invoke( "obtenerLBAAnulacion", new Variant[] { new Variant(mvarUSUARCOD), new Variant(mvarNIVELAS), new Variant(mvarCLIENSECAS), new Variant(mvarFECHA) } );
      }
      else if( mvarCODOP.equals( "LBAEXI" ) )
      {
        wvarStep = 120;
        mvarResponseAIS = invoke( "obtenerLBAExigible", new Variant[] { new Variant(mvarUSUARCOD), new Variant(mvarNIVELAS), new Variant(mvarCLIENSECAS), new Variant(mvarFECHA), new Variant(mvarCantRellamadosMAX) } );
      }
      else if( mvarCODOP.equals( "LBASIN" ) )
      {
        wvarStep = 130;
        mvarResponseAIS = invoke( "obtenerLBASiniestros", new Variant[] { new Variant(mvarUSUARCOD), new Variant(mvarNIVELAS), new Variant(mvarCLIENSECAS), new Variant(mvarFECHA) } );
      }
      else if( mvarCODOP.equals( "NYLANU" ) )
      {
        wvarStep = 140;
        mvarResponseAIS = invoke( "obtenerNYLAnulacion", new Variant[] { new Variant(mvarUSUARCOD), new Variant(mvarNIVELAS), new Variant(mvarCLIENSECAS), new Variant(mvarFECHA) } );
      }
      else if( mvarCODOP.equals( "NYLEXI" ) )
      {
        wvarStep = 150;
        mvarResponseAIS = invoke( "obtenerNYLExigible", new Variant[] { new Variant(mvarUSUARCOD), new Variant(mvarNIVELAS), new Variant(mvarCLIENSECAS), new Variant(mvarFECHA), new Variant(mvarCantRellamadosMAX) } );
      }
      else if( mvarCODOP.equals( "NYLSIN" ) )
      {
        wvarStep = 160;
        mvarResponseAIS = invoke( "obtenerNYLSiniestros", new Variant[] { new Variant(mvarUSUARCOD), new Variant(mvarNIVELAS), new Variant(mvarCLIENSECAS), new Variant(mvarFECHA) } );
      }
      //
      //Resultado OK?
      wvarStep = 170;
      wobjXMLResponseAIS = new XmlDomExtended();
      wobjXMLResponseAIS.loadXML( mvarResponseAIS );
      //
      wvarStep = 175;
      if( ! (wobjXMLResponseAIS.selectSingleNode( "//Request/EstadosMsgAIS/Estado/@codigo" )  == (org.w3c.dom.Node) null) )
      {
        mvarCodError = XmlDomExtended.getText( wobjXMLResponseAIS.selectSingleNode( "//Request/EstadosMsgAIS/Estado/@codigo" )  );
      }
      //
      wvarStep = 180;
      if( ! ((wobjXMLResponseAIS.selectSingleNode( "//Request/EstadosMsgAIS/Estado/@resultado" )  == (org.w3c.dom.Node) null)) && (Err.getError().getNumber() == 0) )
      {
        if( ( XmlDomExtended .getText( wobjXMLResponseAIS.selectSingleNode( "//Request/EstadosMsgAIS/Estado/@resultado" )  ).equals( "true" )) && (mvarCodError.equals( "" )) )
        {
          //
          //Si - Generar XML para env�o a middleware
          XmlDomExtended.setText( wobjXMLResponseAIS.selectSingleNode( "//recipientTO" ) , mvarEMAIL );
          wvarStep = 185;
          //A pedido de AP/LG se reemplaza la clave del identificador por el usuario de OV
          //wobjXMLResponseAIS.selectSingleNode("//attachPassword").Text = obtenerIdentificador(mvarUSUARCOD)
          XmlDomExtended.setText( wobjXMLResponseAIS.selectSingleNode( "//attachPassword" ) , mvarUSUARCOD );
          //Env�o de XML a MDW
          wvarStep = 190;
          mvarEnvioMDW = false;
          if( Err.getError().getNumber() == 0 )
          {
            mvarEnvioMDW = invoke( "enviarXMLaMDW", new Variant[] { new Variant(mvarCODOP), new Variant(wobjXMLResponseAIS.getDocument().getDocumentElement().toString()), new Variant(new Variant( mvarMSGError ))/*warning: ByRef value change will be lost.*/ } );
          }
          else
          {
            mvarMSGError = Err.getError().getDescription();
          }
          //
          //Enviado con �xito?
          wvarStep = 200;
          if( ! (mvarEnvioMDW) || (Err.getError().getNumber() != 0) )
          {
            //No (o si no se envi� con �xito)
            //Registraci�n en Log de fin de proceso con fallas
            wvarStep = 220;
            mvarProcConErrores = true;
          }
          else
          {
            invoke( "marcarControlEnviado", new Variant[] { new Variant(mvarIDCONTROL) } );
          }
        }
        else
        {
          mvarProcConErrores = true;
        }
      }
      else
      {
        mvarProcConErrores = true;
      }
      //
      //Reestablece el estado de On Error
      //
      //Registraci�n en Log de fin de proceso
      wvarStep = 240;
      if( mvarProcConErrores )
      {
        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("E-Fin OVEXESCH. (IDCONTROL: " + mvarIDCONTROL + ") Estado: ERROR - " + mvarMSGError) } );
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + mvarMSGError + "</Response>" );
      }
      else
      {
        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("0-Fin OVEXESCH. (IDCONTROL: " + mvarIDCONTROL + ") Estado: OK") } );
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //FIN
      //
      wvarStep = 250;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 260;
      wobjXMLResponseAIS = null;
      wobjXMLRequest = null;
      wobjXMLListadoProd = null;
      wobjXMLNode = (org.w3c.dom.Node) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant(mcteClassName + " - " + wcteFnName + " - " + wvarStep + " - " + Err.getError().getNumber() + " - " + "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + vbLogEventTypeError) } );

        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("99-Fin OVEXESCH. (IDCONTROL: " + mvarIDCONTROL + ") Estado: ABORTADO POR ERROR") } );

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      invoke( "debugToBD", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion) } );
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      invoke( "debugToFile", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion) } );
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripci�n ) throws Exception
  {
    boolean debugToBD = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    
    
    //
    
    
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    XmlDomExtended wobjXMLConfig = null;
    String wvarText = "";
    String wvarFilename = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion;

    wvarFilename = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend( System.getProperty("user.dir") + "\\DEBUG\\" + wvarFilename, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private String obtenerLBAAnulacion( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarFECHA ) throws Exception
  {
    String obtenerLBAAnulacion = "";
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseAIS = null;
    XmlDomExtended wobjXMLParametro = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    java.util.Date wvarDateDde = DateTime.EmptyDate;
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgLBAANU.xml" );
    //
    wvarResponse = "";
    wvarRellamar = true;
    while( wvarRellamar )
    {
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/USUARIO" ) , pvarUSUARCOD );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NIVELAS" ) , pvarNIVELAS );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CLIENSECAS" ) , pvarCLIENSECAS );
      wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
      //
      wobjClass = new XmlDomExtended.getText( wobjXMLParametro.selectSingleNode( "//CLASE" )  )();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponseAIS, "")
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("LBAANU RequestAIS: " + wvarRequest) } );
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("LBAANU ResponseAIS: " + wvarResponseAIS) } );
      //
      wobjClass = null;
      wvarRellamar = false;
      //
      wvarXMLResponseAIS = new XmlDomExtended();
      wvarXMLResponseAIS.loadXML( wvarResponseAIS );
      //
      if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          if( ! (wvarXMLResponseAIS.selectSingleNode( "//MSGEST" )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//MSGEST" )  ).equals( "TR" ) )
            {
              //Si vuelve con marca de rellamado, se completan los campos necesarios y se
              //concatena la salida actual a wvarResponse para su posterior an�lisis
              wvarResponse = wvarResponse + wvarResponseAIS;
              wvarRellamar = true;
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CONTINUAR" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//CONTINUAR" )  ) );
            }
            else
            {
              wvarResponse = wvarResponse + wvarResponseAIS;
            }
          }
          else
          {
            wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ) + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response" )  ) + String.valueOf( (char)(34) ) + " /></Response>";
          }
        }
        else
        {
          wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ) + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response" )  ) + String.valueOf( (char)(34) ) + " /></Response>";
        }
      }
      else
      {
        wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL IACTION" + String.valueOf( (char)(34) ) + " /></Response>";
      }
    }
    //
    //Aplica el xls para formatear la salida del mensaje
    wobjXMLResponse = new XmlDomExtended();
    wobjXSLResponse = new XmlDomExtended();
    //
    //
    wobjXMLResponse.loadXML( "<ROOT><FECHADDE/><FECHADES/><FECHAHAS/>" + wvarResponse + "</ROOT>" );
    wobjXSLResponse.load( System.getProperty("user.dir") + "\\XML\\msgLBAANU.xsl" );
    //
    //Calculo de rango de fechas
    //wvarDateDde = DateValue(Mid(pvarFECHA, 4, 2) & "-" & Mid(pvarFECHA, 1, 2) & "-" & Mid(pvarFECHA, 7, 4)) - 6
    wvarDateDde = DateTime.subtract( DateTime.toDate( pvarFECHA ), 7 );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADDE" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADES" ) , Strings.right( "00" + DateTime.day( wvarDateDde ), 2 ) + "/" + Strings.right( ("00" + DateTime.month( wvarDateDde )), 2 ) + "/" + DateTime.year( wvarDateDde ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHAHAS" ) , Strings.mid( pvarFECHA, 4, 2 ) + "/" + Strings.mid( pvarFECHA, 1, 2 ) + "/" + Strings.mid( pvarFECHA, 7, 4 ) );
    //
    obtenerLBAAnulacion = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    wobjXSLResponse = null;
    wobjXMLResponse = null;
    return obtenerLBAAnulacion;
  }

  private String obtenerLBASiniestros( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarFECHA ) throws Exception
  {
    String obtenerLBASiniestros = "";
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseAIS = null;
    XmlDomExtended wobjXMLParametro = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    java.util.Date wvarDateDde = DateTime.EmptyDate;
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgLBASIN.xml" );
    //
    wvarResponse = "";
    wvarRellamar = true;
    while( wvarRellamar )
    {
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/USUARIO" ) , pvarUSUARCOD );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NIVELAS" ) , pvarNIVELAS );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CLIENSECAS" ) , pvarCLIENSECAS );
      //
      //wvarDateDde = DateValue(Mid(pvarFECHA, 4, 2) & "-" & Mid(pvarFECHA, 1, 2) & "-" & Mid(pvarFECHA, 7, 4)) - 6
      wvarDateDde = DateTime.subtract( DateTime.toDate( pvarFECHA ), 7 );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/FECDES" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/FECHAS" ) , Strings.mid( pvarFECHA, 7, 4 ) + Strings.mid( pvarFECHA, 1, 2 ) + Strings.mid( pvarFECHA, 4, 2 ) );
      wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
      //
      wobjClass = new XmlDomExtended.getText( wobjXMLParametro.selectSingleNode( "//CLASE" )  )();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponseAIS, "")
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("LBASIN RequestAIS: " + wvarRequest) } );
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("LBASIN ResponseAIS: " + wvarResponseAIS) } );
      //
      wobjClass = null;
      wvarRellamar = false;
      //
      wvarXMLResponseAIS = new XmlDomExtended();
      wvarXMLResponseAIS.loadXML( wvarResponseAIS );
      //
      if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          if( ! (wvarXMLResponseAIS.selectSingleNode( "//MSGEST" )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//MSGEST" )  ).equals( "TR" ) )
            {
              //Si vuelve con marca de rellamado, se completan los campos necesarios y se
              //concatena la salida actual a wvarResponse para su posterior an�lisis
              wvarResponse = wvarResponse + wvarResponseAIS;
              wvarRellamar = true;
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CONTINUAR" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//CONTINUAR" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/MSGEST" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//MSGEST" )  ) );
            }
            else
            {
              wvarResponse = wvarResponse + wvarResponseAIS;
            }
          }
          else
          {
            wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ) + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response" )  ) + String.valueOf( (char)(34) ) + " /></Response>";
          }
        }
        else
        {
          wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ) + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response" )  ) + String.valueOf( (char)(34) ) + " /></Response>";
        }
      }
      else
      {
        wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL IACTION" + String.valueOf( (char)(34) ) + " /></Response>";
      }
    }
    //
    //Aplica el xls para formatear la salida del mensaje
    wobjXMLResponse = new XmlDomExtended();
    wobjXSLResponse = new XmlDomExtended();
    //
    //
    wobjXMLResponse.loadXML( "<ROOT><FECHADDE/><FECHADES/><FECHAHAS/>" + wvarResponse + "</ROOT>" );
    wobjXSLResponse.load( System.getProperty("user.dir") + "\\XML\\msgLBASIN.xsl" );
    //
    //C�lculo de rango de fechas
    //wvarDateDde = DateValue(Mid(pvarFECHA, 4, 2) & "-" & Mid(pvarFECHA, 1, 2) & "-" & Mid(pvarFECHA, 7, 4)) - 6
    wvarDateDde = DateTime.subtract( DateTime.toDate( pvarFECHA ), 7 );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADDE" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADES" ) , Strings.right( "00" + DateTime.day( wvarDateDde ), 2 ) + "/" + Strings.right( ("00" + DateTime.month( wvarDateDde )), 2 ) + "/" + DateTime.year( wvarDateDde ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHAHAS" ) , Strings.mid( pvarFECHA, 4, 2 ) + "/" + Strings.mid( pvarFECHA, 1, 2 ) + "/" + Strings.mid( pvarFECHA, 7, 4 ) );
    //
    obtenerLBASiniestros = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    wobjXSLResponse = null;
    wobjXMLResponse = null;
    return obtenerLBASiniestros;
  }

  private String obtenerLBAExigible( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarFECHA, int pvarRellamadosMAX ) throws Exception
  {
    String obtenerLBAExigible = "";
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseAIS = null;
    XmlDomExtended wobjXMLParametro = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    int wvarRellamadoCount = 0;
    String wvarTrunc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgLBAEXI.xml" );
    //
    wvarResponse = "";
    wvarRellamar = true;
    wvarTrunc = "N";
    wvarRellamadoCount = 0;
    while( wvarRellamar )
    {
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/USUARIO" ) , pvarUSUARCOD );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NIVELAS" ) , pvarNIVELAS );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CLIENSECAS" ) , pvarCLIENSECAS );
      wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
      //
      wobjClass = new XmlDomExtended.getText( wobjXMLParametro.selectSingleNode( "//CLASE" )  )();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponseAIS, "")
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("LBAEXI RequestAIS: " + wvarRequest) } );
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("LBAEXI ResponseAIS: " + wvarResponseAIS) } );
      //
      wobjClass = null;
      wvarRellamar = false;
      //
      wvarXMLResponseAIS = new XmlDomExtended();
      wvarXMLResponseAIS.loadXML( wvarResponseAIS );
      //
      if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/MSGEST" )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/MSGEST" )  ).equals( "TR" ) )
            {
              //Si vuelve con marca de rellamado, se completan los campos necesarios y se
              //concatena la salida actual a wvarResponse para su posterior an�lisis
              wvarResponse = wvarResponse + wvarResponseAIS;
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/PRODUCTO" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/PRODUCTO" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/POLIZA" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/POLIZA" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NROCONS" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/NROCONS" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NROORDEN" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/NROORDEN" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/TFILTRO" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/TFILTRO" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/VFILTRO" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/VFILTRO" )  ) );
              wvarRellamadoCount = wvarRellamadoCount + 1;
              //Limita el rellamado a pvarRellamadosMAX
              if( wvarRellamadoCount < pvarRellamadosMAX )
              {
                wvarRellamar = true;
              }
              else
              {
                wvarTrunc = "S";
              }
            }
            else
            {
              wvarResponse = wvarResponse + wvarResponseAIS;
            }
          }
          else
          {
            wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ) + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response" )  ) + String.valueOf( (char)(34) ) + " /></Response>";
          }
        }
        else
        {
          wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@mensaje" )  ) + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Response" )  ) + String.valueOf( (char)(34) ) + " /></Response>";
        }
      }
      else
      {
        wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL IACTION" + String.valueOf( (char)(34) ) + " /></Response>";
      }
    }
    //
    //Aplica el xls para formatear la salida del mensaje
    wobjXMLResponse = new XmlDomExtended();
    wobjXSLResponse = new XmlDomExtended();
    //
    wobjXMLResponse.loadXML( "<ROOT><DATOSTRUNCADOS>" + wvarTrunc + "</DATOSTRUNCADOS><FECHADES/><FECHAHAS/>" + wvarResponse + "</ROOT>" );
    wobjXSLResponse.load( System.getProperty("user.dir") + "\\XML\\msgLBAEXI.xsl" );
    //
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHAHAS" ) , Strings.mid( pvarFECHA, 4, 2 ) + "/" + Strings.mid( pvarFECHA, 1, 2 ) + "/" + Strings.mid( pvarFECHA, 7, 4 ) );
    //
    obtenerLBAExigible = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    wobjXSLResponse = null;
    wobjXMLResponse = null;
    return obtenerLBAExigible;
  }

  private String obtenerNYLAnulacion( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarFECHA ) throws Exception
  {
    String obtenerNYLAnulacion = "";
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseAIS = null;
    XmlDomExtended wobjXMLParametro = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    java.util.Date wvarDateDde = DateTime.EmptyDate;
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgNYLANU.xml" );
    //
    wvarResponse = "";
    wvarRellamar = true;
    while( wvarRellamar )
    {
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/USUARCOD" ) , Strings.left( pvarUSUARCOD, 10 ) );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NIVELAS" ) , pvarNIVELAS );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CLIENAS" ) , pvarCLIENSECAS );
      wvarDateDde = DateTime.subtract( DateTime.toDate( Strings.mid( pvarFECHA, 4, 2 ) + "-" + Strings.mid( pvarFECHA, 1, 2 ) + "-" + Strings.mid( pvarFECHA, 7, 4 ) ), 6 );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/FECHADDE" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/FECHAHTA" ) , Strings.mid( pvarFECHA, 7, 4 ) + Strings.mid( pvarFECHA, 1, 2 ) + Strings.mid( pvarFECHA, 4, 2 ) );
      wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
      //
      wobjClass = new XmlDomExtended.getText( wobjXMLParametro.selectSingleNode( "//CLASE" )  )();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponseAIS, "")
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("NYLANU RequestAIS: " + wvarRequest) } );
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("NYLANU ResponseAIS: " + wvarResponseAIS) } );
      //
      wobjClass = null;
      wvarRellamar = false;
      //
      wvarXMLResponseAIS = new XmlDomExtended();
      wvarXMLResponseAIS.loadXML( wvarResponseAIS );
      //
      if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( (( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" )) && ( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ERRORMSG" )  ).equals( "" ))) || ( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ERRORMSG" )  ).equals( "ND" )) )
        {
          if( ! (wvarXMLResponseAIS.selectSingleNode( "//ESTADOMSG " )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ESTADOMSG " )  ).equals( "TR" ) )
            {
              //Si vuelve con marca de rellamado, se completan los campos necesarios y se
              //concatena la salida actual a wvarResponse para su posterior an�lisis
              wvarResponse = wvarResponse + wvarResponseAIS;
              wvarRellamar = true;
              wobjXMLParametro.loadXML( /*unsup wvarXMLResponseAIS.selectSingleNode( "//Request" ) */.toString() );
            }
            else if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ESTADOMSG" )  ).equals( "OK" ) )
            {
              wvarResponse = wvarResponse + wvarResponseAIS;
            }
            else
            {
              wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
            }
          }
          else
          {
            wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
          }
        }
        else
        {
          wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
        }
      }
      else
      {
        wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL IACTION" + String.valueOf( (char)(34) ) + " /></Response>";
      }
    }
    //
    //Aplica el xls para formatear la salida del mensaje
    wobjXMLResponse = new XmlDomExtended();
    wobjXSLResponse = new XmlDomExtended();
    //
    //
    wobjXMLResponse.loadXML( "<ROOT><FECHADDE/><FECHADES/><FECHAHAS/>" + wvarResponse + "</ROOT>" );
    wobjXSLResponse.load( System.getProperty("user.dir") + "\\XML\\msgNYLANU.xsl" );
    //
    //alculo de rango de fechas
    wvarDateDde = DateTime.subtract( DateTime.toDate( Strings.mid( pvarFECHA, 4, 2 ) + "-" + Strings.mid( pvarFECHA, 1, 2 ) + "-" + Strings.mid( pvarFECHA, 7, 4 ) ), 6 );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADDE" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADES" ) , Strings.right( "00" + DateTime.day( wvarDateDde ), 2 ) + "/" + Strings.right( ("00" + DateTime.month( wvarDateDde )), 2 ) + "/" + DateTime.year( wvarDateDde ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHAHAS" ) , Strings.mid( pvarFECHA, 4, 2 ) + "/" + Strings.mid( pvarFECHA, 1, 2 ) + "/" + Strings.mid( pvarFECHA, 7, 4 ) );
    //
    obtenerNYLAnulacion = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    wobjXSLResponse = null;
    wobjXMLResponse = null;
    return obtenerNYLAnulacion;
  }

  private String obtenerNYLSiniestros( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarFECHA ) throws Exception
  {
    String obtenerNYLSiniestros = "";
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseAIS = null;
    XmlDomExtended wobjXMLParametro = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    java.util.Date wvarDateDde = DateTime.EmptyDate;
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgNYLSIN.xml" );
    //
    wvarResponse = "";
    wvarRellamar = true;
    while( wvarRellamar )
    {
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/USUARCOD" ) , Strings.left( pvarUSUARCOD, 10 ) );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NIVELAS" ) , pvarNIVELAS );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CLIENSECAS" ) , pvarCLIENSECAS );
      //
      wvarDateDde = DateTime.subtract( DateTime.toDate( Strings.mid( pvarFECHA, 4, 2 ) + "-" + Strings.mid( pvarFECHA, 1, 2 ) + "-" + Strings.mid( pvarFECHA, 7, 4 ) ), 6 );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/FECDESDE" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/FECHASTA" ) , Strings.mid( pvarFECHA, 7, 4 ) + Strings.mid( pvarFECHA, 1, 2 ) + Strings.mid( pvarFECHA, 4, 2 ) );
      wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
      //
      wobjClass = new XmlDomExtended.getText( wobjXMLParametro.selectSingleNode( "//CLASE" )  )();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponseAIS, "")
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("NYLSIN RequestAIS: " + wvarRequest) } );
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("NYLSIN ResponseAIS: " + wvarResponseAIS) } );
      //
      wobjClass = null;
      wvarRellamar = false;
      //
      wvarXMLResponseAIS = new XmlDomExtended();
      wvarXMLResponseAIS.loadXML( wvarResponseAIS );
      //
      if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
        {
          if( ! (wvarXMLResponseAIS.selectSingleNode( "//ESTADO" )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ESTADO" )  ).equals( "TR" ) )
            {
              //Si vuelve con marca de rellamado, se completan los campos necesarios y se
              //concatena la salida actual a wvarResponse para su posterior an�lisis
              wvarResponse = wvarResponse + wvarResponseAIS;
              wvarRellamar = true;
              wobjXMLParametro.loadXML( /*unsup wvarXMLResponseAIS.selectSingleNode( "//Request" ) */.toString() );
            }
            else if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ESTADO" )  ).equals( "OK" ) )
            {
              wvarResponse = wvarResponse + wvarResponseAIS;
            }
            else
            {
              wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
            }
          }
          else
          {
            wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
          }
        }
        else
        {
          wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
        }
      }
      else
      {
        wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL IACTION" + String.valueOf( (char)(34) ) + " /></Response>";
      }
    }
    //
    //Aplica el xls para formatear la salida del mensaje
    wobjXMLResponse = new XmlDomExtended();
    wobjXSLResponse = new XmlDomExtended();
    //
    //
    wobjXMLResponse.loadXML( "<ROOT><FECHADDE/><FECHADES/><FECHAHAS/>" + wvarResponse + "</ROOT>" );
    wobjXSLResponse.load( System.getProperty("user.dir") + "\\XML\\msgNYLSIN.xsl" );
    //
    //alculo de rango de fechas
    wvarDateDde = DateTime.subtract( DateTime.toDate( Strings.mid( pvarFECHA, 4, 2 ) + "-" + Strings.mid( pvarFECHA, 1, 2 ) + "-" + Strings.mid( pvarFECHA, 7, 4 ) ), 6 );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADDE" ) , DateTime.year( wvarDateDde ) + Strings.right( "00" + DateTime.month( wvarDateDde ), 2 ) + Strings.right( ("00" + DateTime.day( wvarDateDde )), 2 ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHADES" ) , Strings.right( "00" + DateTime.day( wvarDateDde ), 2 ) + "/" + Strings.right( ("00" + DateTime.month( wvarDateDde )), 2 ) + "/" + DateTime.year( wvarDateDde ) );
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHAHAS" ) , Strings.mid( pvarFECHA, 4, 2 ) + "/" + Strings.mid( pvarFECHA, 1, 2 ) + "/" + Strings.mid( pvarFECHA, 7, 4 ) );
    //
    obtenerNYLSiniestros = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    wobjXSLResponse = null;
    wobjXMLResponse = null;
    return obtenerNYLSiniestros;
  }

  private String obtenerNYLExigible( String pvarUSUARCOD, String pvarNIVELAS, String pvarCLIENSECAS, String pvarFECHA, int pvarRellamadosMAX ) throws Exception
  {
    String obtenerNYLExigible = "";
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseAIS = null;
    XmlDomExtended wobjXMLParametro = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    int wvarRellamadoCount = 0;
    String wvarTrunc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgNYLEXI.xml" );
    //
    wvarResponse = "";
    wvarRellamar = true;
    wvarTrunc = "N";
    wvarRellamadoCount = 0;
    while( wvarRellamar )
    {
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/USUARCOD" ) , Strings.left( pvarUSUARCOD, 10 ) );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NIVELAS" ) , pvarNIVELAS );
      XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CLIENAS" ) , pvarCLIENSECAS );
      wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
      //
      wobjClass = new XmlDomExtended.getText( wobjXMLParametro.selectSingleNode( "//CLASE" )  )();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponseAIS, "")
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("NYLEXI RequestAIS: " + wvarRequest) } );
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("NYLEXI ResponseAIS: " + wvarResponseAIS) } );
      //
      wobjClass = null;
      wvarRellamar = false;
      //
      wvarXMLResponseAIS = new XmlDomExtended();
      wvarXMLResponseAIS.loadXML( wvarResponseAIS );
      //
      if( ! (wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
      {
        if( (( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" )) && ( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ERRORMSG" )  ).equals( "" ))) || ( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ERRORMSG" )  ).equals( "ND" )) )
        {
          if( ! (wvarXMLResponseAIS.selectSingleNode( "//ESTADOMSG" )  == (org.w3c.dom.Node) null) )
          {
            if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ESTADOMSG" )  ).equals( "TR" ) )
            {
              //Si vuelve con marca de rellamado, se completan los campos necesarios y se
              //concatena la salida actual a wvarResponse para su posterior an�lisis
              wvarResponse = wvarResponse + wvarResponseAIS;
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/NROQRY" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/NROQRY" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/ORDENIDX" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/ORDENIDX" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/ASCDESC" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/ASCDESC" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/PDTODDE" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/PDTODDE" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/POLIZANNDDE" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/POLIZANNDDE" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/POLIZSECDDE" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/POLIZSECDDE" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CERTIPOLDDE" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/CERTIPOLDDE" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CERTIANNDDE" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/CERTIANNDDE" )  ) );
              XmlDomExtended.setText( wobjXMLParametro.selectSingleNode( "//Request/CERTISECDDE" ) , XmlDomExtended.getText( wvarXMLResponseAIS.selectSingleNode( "//Request/CERTISECDDE" )  ) );
              wvarRellamadoCount = wvarRellamadoCount + 1;
              //Limita el rellamado a pvarRellamadosMAX
              if( wvarRellamadoCount < pvarRellamadosMAX )
              {
                wvarRellamar = true;
              }
              else
              {
                wvarTrunc = "S";
              }
            }
            else if( XmlDomExtended .getText( wvarXMLResponseAIS.selectSingleNode( "//ESTADOMSG" )  ).equals( "OK" ) )
            {
              wvarResponse = wvarResponse + wvarResponseAIS;
            }
            else
            {
              wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
            }
          }
          else
          {
            wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
          }
        }
        else
        {
          wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL MENSAJE" + String.valueOf( (char)(34) ) + " codigo=" + String.valueOf( (char)(34) ) + "ERROR" + String.valueOf( (char)(34) ) + "/></Response>";
        }
      }
      else
      {
        wvarResponse = "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "ERROR AL EJECUTAR EL IACTION" + String.valueOf( (char)(34) ) + " /></Response>";
      }
    }
    //
    //Aplica el xls para formatear la salida del mensaje
    wobjXMLResponse = new XmlDomExtended();
    wobjXSLResponse = new XmlDomExtended();
    //
    wobjXMLResponse.loadXML( "<ROOT><DATOSTRUNCADOS>" + wvarTrunc + "</DATOSTRUNCADOS><FECHADES/><FECHAHAS/>" + wvarResponse + "</ROOT>" );
    wobjXSLResponse.load( System.getProperty("user.dir") + "\\XML\\msgNYLEXI.xsl" );
    //
    XmlDomExtended.setText( wobjXMLResponse.selectSingleNode( "//FECHAHAS" ) , Strings.mid( pvarFECHA, 4, 2 ) + "/" + Strings.mid( pvarFECHA, 1, 2 ) + "/" + Strings.mid( pvarFECHA, 7, 4 ) );
    //
    obtenerNYLExigible = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" );
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    wobjXSLResponse = null;
    wobjXMLResponse = null;
    return obtenerNYLExigible;
  }

  private boolean marcarControlEnviado( int pvarIDCONTROL ) throws Exception
  {
    boolean marcarControlEnviado = false;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcUpdControlEnv );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    
    
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      marcarControlEnviado = true;
    }
    else
    {
      marcarControlEnviado = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return marcarControlEnviado;
  }

  private boolean enviarXMLaMDW( String pvarCODOP, String pvarRequestMdw, Variant pvarMsgError ) throws Exception
  {
    boolean enviarXMLaMDW = false;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wvarXMLResponseMdw = null;
    XmlDomExtended wobjXMLInMdw = null;
    boolean wvarRellamar = false;
    String wvarResponseMdw = "";
    //
    //
    invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("RequestMDW: " + pvarRequestMdw) } );
    wobjClass = new LBAA_MWGenerico.lbaw_MQMW();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(pvarRequestMdw, wvarResponseMdw, "")
    invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("ResponseMDW: " + wvarResponseMdw) } );
    //
    wobjClass = null;
    //
    wvarXMLResponseMdw = new XmlDomExtended();
    wvarXMLResponseMdw.loadXML( wvarResponseMdw );
    //
    if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponseMdw.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        if( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null )
        {
          enviarXMLaMDW = true;
        }
        else
        {
          enviarXMLaMDW = false;
          pvarMsgError.set( XmlDomExtended .getText( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  ) );
        }
      }
      else
      {
        if( ! (wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  == (org.w3c.dom.Node) null) )
        {
          pvarMsgError.set( XmlDomExtended .getText( wvarXMLResponseMdw.selectSingleNode( "//Response/faultstring" )  ) );
        }
        else
        {
          pvarMsgError.set( "Error en la ejecuci�n de LBAA_MWGenerico.lbaw_MQMW" );
        }
        enviarXMLaMDW = false;
      }
    }
    else
    {
      pvarMsgError.set( "Error en la ejecuci�n de LBAA_MWGenerico.lbaw_MQMW" );
      enviarXMLaMDW = false;
    }
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLInMdw = null;
    wobjXSLResponse = null;
    wvarXMLResponseMdw = null;
    return enviarXMLaMDW;
  }

  private String obtenerIdentificador( String pvarUSUARCOD ) throws Exception
  {
    String obtenerIdentificador = "";
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstDBResult = null;
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDBCAM);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteSPObtenerIdentificador );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    wrstDBResult = wobjDBCmd.execute();
    wrstDBResult.setActiveConnection( (Connection) null );
    //
    obtenerIdentificador = ModEncryptDecrypt.CapicomDecrypt( wrstDBResult.getField("identificador").getValue().toString() );
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    wrstDBResult = (Recordset) null;
    //
    return obtenerIdentificador;
  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;


    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
