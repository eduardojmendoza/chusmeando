package com.qbe.services.alertasxmail.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.alertasxmail.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  UDL a la base de datos.
 */

public class ModGeneral
{
  public static final String gcteDB = "lbawA_OfVirtualLBA.udl";
  public static final String gcteDBCAM = "camA_OficinaVirtual.udl";
  public static final String gcteDBACTIONS = "lbaw_SCH2Fwk.udl";
  /**
   *  Parametros XML de Configuracion
   */
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";

  public static String MidAsString( String pvarStringCompleto, Variant pvarActualCounter, int pvarLongitud ) throws Exception
  {
    String MidAsString = "";
    MidAsString = Strings.mid( pvarStringCompleto, pvarActualCounter.toInt(), pvarLongitud );
    pvarActualCounter.set( pvarActualCounter.add( new Variant( pvarLongitud ) ) );
    return MidAsString;
  }

  public static String CompleteZero( String pvarString, int pvarLongitud ) throws Exception
  {
    String CompleteZero = "";
    int wvarCounter = 0;
    String wvarstrTemp = "";
    for( wvarCounter = 1; wvarCounter <= pvarLongitud; wvarCounter++ )
    {
      wvarstrTemp = wvarstrTemp + "0";
    }
    CompleteZero = Strings.right( wvarstrTemp + pvarString, pvarLongitud );
    return CompleteZero;
  }

  public static String GetErrorInformacionDato( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, String pvarTipoDato, org.w3c.dom.Node pobjLongitud, org.w3c.dom.Node pobjDecimales, org.w3c.dom.Node pobjDefault, org.w3c.dom.Node pobjObligatorio ) throws Exception
  {
    String GetErrorInformacionDato = "";
    org.w3c.dom.Node wobjNodoValor = null;
    String wvarDatoValue = "";
    int wvarCounter = 0;
    double wvarCampoNumerico = 0.0;
    boolean wvarIsDatoObligatorio = false;

    if( ! (pobjObligatorio == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( pobjObligatorio ).equals( "SI" ) )
      {
        //Es un Dato Obligatorio
        wvarIsDatoObligatorio = true;
        wobjNodoValor = pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) ;
        if( wobjNodoValor == (org.w3c.dom.Node) null )
        {
          GetErrorInformacionDato = "Nodo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " NO INFORMADO";
          return IAction_Execute;
        }
      }
    }

    wobjNodoValor = pobjXMLContenedor.selectSingleNode( "./" + Strings.mid( pvarPathDato, 3 ) ) ;
    if( wobjNodoValor == (org.w3c.dom.Node) null )
    {
      if( ! (pobjDefault == (org.w3c.dom.Node) null) )
      {
        wvarDatoValue = XmlDomExtended.getText( pobjDefault );
      }
    }
    else
    {
      wvarDatoValue = XmlDomExtended.getText( wobjNodoValor );
    }
    //
    
    if( pvarTipoDato.equals( "TEXTO" ) || pvarTipoDato.equals( "TEXTOIZQUIERDA" ) )
    {
      //Dato del Tipo String
      if( wvarIsDatoObligatorio && (Strings.trim( wvarDatoValue ).equals( "" )) )
      {
        GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " SIN VALOR INGRESADO";
        return IAction_Execute;
      }
    }
    else if( pvarTipoDato.equals( "ENTERO" ) || pvarTipoDato.equals( "DECIMAL" ) )
    {
      //Dato del Tipo Numerico
      if( wvarDatoValue.equals( "" ) )
      {
        wvarDatoValue = "0";
      }
      if( ! (new Variant( wvarDatoValue ).isNumeric()) )
      {
        GetErrorInformacionDato = "Campo " + Strings.mid( pvarPathDato, 3 ) + " CON FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: Numerico)";
        return IAction_Execute;
      }
      else
      {
        if( wvarIsDatoObligatorio && (Obj.toDouble( wvarDatoValue ) == 0) )
        {
          GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " SIN VALOR INGRESADO";
          return IAction_Execute;
        }
      }
    }
    else if( pvarTipoDato.equals( "FECHA" ) )
    {
      //Dato del Tipo "Fecha" Debe venir del tipo dd/mm/yyyy devuelve YYYYMMDD
      if( wvarIsDatoObligatorio && ! (wvarDatoValue.matches( "*/*/*" )) )
      {
        GetErrorInformacionDato = "Campo Obligatorio " + Strings.mid( pvarPathDato, 3 ) + " VALOR INGRESADO EN FORMATO INVALIDO (Valor Informado: " + wvarDatoValue + ". Requerido: dd/mm/yyyy)";
      }
    }
    //
    ClearObjects: 
    wobjNodoValor = (org.w3c.dom.Node) null;
    return GetErrorInformacionDato;
  }

  public static org.w3c.dom.Node GetRequestRetornado( XmlDomExtended pobjXMLRequest, org.w3c.dom.Node pobjXMLRequestDef, Variant pvarStrRetorno ) throws Exception
  {
    org.w3c.dom.Node GetRequestRetornado = null;
    org.w3c.dom.Node wobjNodoRequestDef = null;
    org.w3c.dom.Node wobjNodoVectorDef = null;
    org.w3c.dom.Node wobjNewNodo = null;
    int pvarStartCount = 0;
    String wvarLastValue = "";
    int wvarCount = 0;

    //Desestimo el Numero de mensaje
    pvarStartCount = 5;
    //
    for( int nwobjNodoRequestDef = 0; nwobjNodoRequestDef < pobjXMLRequestDef.getChildNodes().getLength(); nwobjNodoRequestDef++ )
    {
      wobjNodoRequestDef = pobjXMLRequestDef.getChildNodes().item( nwobjNodoRequestDef );
      //
      if( wobjNodoRequestDef.getAttributes().getNamedItem( "Cantidad" ) == (org.w3c.dom.Node) null )
      {
        //No proceso los vectores del Request
        if( pobjXMLRequest.selectSingleNode( ("//" + XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) )) )  == (org.w3c.dom.Node) null )
        {
          wobjNewNodo = pobjXMLRequest.getDocument().createElement( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ) );
          pobjXMLRequest.getDocument().getChildNodes().item( 0 ).appendChild( wobjNewNodo );
        }
        else
        {
          wobjNewNodo = pobjXMLRequest.selectSingleNode( "//" + XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Nombre" ) ) ) ;
        }
        //
        if( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
        {
          wvarLastValue = Strings.mid( pvarStrRetorno.toString(), pvarStartCount, Obj.toInt( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) ) );
        }
        else
        {
          wvarLastValue = Strings.mid( pvarStrRetorno.toString(), pvarStartCount, Obj.toInt( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) + XmlDomExtended.getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) ) );
        }
        //
        
        if( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "ENTERO" ) )
        {
          XmlDomExtended.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue ) ) );
        }
        else if( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "DECIMAL" ) )
        {
          XmlDomExtended.setText( wobjNewNodo, String.valueOf( VB.val( wvarLastValue ) / Math.pow( 10, VB.val( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) ) ) ) );
        }
        else if( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "TipoDato" ) ).equals( "FECHA" ) )
        {
          XmlDomExtended.setText( wobjNewNodo, Strings.right( wvarLastValue, 2 ) + "/" + Strings.mid( wvarLastValue, 5, 2 ) + "/" + Strings.left( wvarLastValue, 4 ) );
        }
        else
        {
          XmlDomExtended.setText( wobjNewNodo, Strings.trim( wvarLastValue ) );
        }
        if( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
        {
          pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) );
        }
        else
        {
          pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Enteros" ) ) ) + Obj.toInt( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Decimales" ) ) );
        }
      }
      else
      {
        //Salteo todos los registros del vector
        for( wvarCount = 1; wvarCount <= Obj.toInt( XmlDomExtended .getText( wobjNodoRequestDef.getAttributes().getNamedItem( "Cantidad" ) ) ); wvarCount++ )
        {
          for( int nwobjNodoVectorDef = 0; nwobjNodoVectorDef < wobjNodoRequestDef.getChildNodes().getLength(); nwobjNodoVectorDef++ )
          {
            wobjNodoVectorDef = wobjNodoRequestDef.getChildNodes().item( nwobjNodoVectorDef );
            if( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) == (org.w3c.dom.Node) null )
            {
              pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended .getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) );
            }
            else
            {
              pvarStartCount = pvarStartCount + Obj.toInt( XmlDomExtended .getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Enteros" ) ) ) + Obj.toInt( XmlDomExtended .getText( wobjNodoVectorDef.getAttributes().getNamedItem( "Decimales" ) ) );
            }
          }
        }
      }
      //
    }
    //
    GetRequestRetornado = pobjXMLRequest.getDocument().getChildNodes().item( 0 );
    wobjNewNodo = (org.w3c.dom.Node) null;
    wobjNodoRequestDef = (org.w3c.dom.Node) null;
    return GetRequestRetornado;
  }
}
