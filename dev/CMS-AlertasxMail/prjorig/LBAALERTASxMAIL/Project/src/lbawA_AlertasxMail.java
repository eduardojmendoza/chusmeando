package com.qbe.services.alertasxmail.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.alertasxmail.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
import java.applet.*;

public class lbawA_AlertasxMail extends JApplet
{
  static {
    try {
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (Exception e) { System.out.println(e); }
  }
  public static String Title = "lbawA_AlertasxMail";
  public static String ProductName = "";
  public static int MajorVersion = 1;
  public static int MinorVersion = 0;
  public static int Revision = 82;
  public static String HelpFile = "";
  public static String Comments = "";
  public static String FileDescription = "";
  public static String CompanyName = "HSBC";
  public static String LegalCopyright = "";
  public static String LegalTrademarks = "";

  public lbawA_AlertasxMail()
  {
  }

  // called only when running as an applet
  public void init()
  {
    getContentPane().setLayout( new java.awt.BorderLayout() );
    Application app = new Application( "lbawA_AlertasxMail" );
  }

  public String getAppletInfo()
  {
    return "lbawA_AlertasxMail" + " " + LegalCopyright;
  }

  // called only when running as a stand-alone application
  public static void main( String args[] )
  {
    final Application app = new Application( "lbawA_AlertasxMail" );
    app.setApplication( new lbawA_AlertasxMail(), args );
    try
    {
    }
    catch(Exception e) { Err.set(e); }
    app.endApplication();
    javax.swing.SwingUtilities.invokeLater( new Runnable() {
      public void run() {
        Application.setApplication( app );
      }
    });
  }
}
