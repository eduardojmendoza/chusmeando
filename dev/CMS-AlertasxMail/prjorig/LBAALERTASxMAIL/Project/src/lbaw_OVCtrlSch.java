package com.qbe.services.alertasxmail.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.services.mq.connector.MQConnectionConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.alertasxmail.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_OVCtrlSch implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_AlertasxMail.lbaw_OVCtrlSch";
  /**
   * Stored Procedures
   */
  static final String mcteStoreProcUpdControlEnv = "P_ALERTAS_UPDATE_CONTROL_ENVIADO";
  static final String mcteStoreProcInsControl = "P_ALERTAS_INSERT_CONTROL";
  static final String mcteStoreProcInsLog = "P_ALERTAS_INSERT_LOG";
  static final String mcteStoreProcListarPendientes = "P_ALERTAS_LISTAR_PENDIENTES";
  /**
   * 
   */
  static final String mcteParam_FECHA = "//FECHA";
  /**
   * 
   */
  static final String mcteOperacion = "CTRLSCH";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    HSBCInterfaces.IAction wobjClass = null;
    int wvarStep = 0;
    String wvarRequest = "";
    String wvarResponse = "";
    String mvarFECHA = "";
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLListadoProd = null;
    org.w3c.dom.Node wobjXMLNode = null;
    org.w3c.dom.Node wobjXMLConfigNode = null;
    String mvarCODOP = "";
    boolean mvarProcConErrores = false;
    boolean mvarProcesar = false;
    int mvarPendientes = 0;
    String mvarMSGError = "";
    //
    //
    //Par�metros de entrada
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      //Inicializaci�n de variables
      mvarPendientes = 0;
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      mvarFECHA = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FECHA )  );
      //
      //Registraci�n en Log de inicio de ejecuci�n
      wvarStep = 30;
      invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("I-OVCTRLSCH - Nueva ejecuci�n") } );
      //
      //Se obtienen los CODOP a procesar
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
      //
      wvarStep = 50;
      for( int nwobjXMLConfigNode = 0; nwobjXMLConfigNode < wobjXMLConfig.selectNodes( "//CODOP" ) .getLength(); nwobjXMLConfigNode++ )
      {
        wobjXMLConfigNode = wobjXMLConfig.selectNodes( "//CODOP" ) .item( nwobjXMLConfigNode );
        //Obtenci�n de listado de productores pendientes...
        wvarStep = 80;
        mvarCODOP = XmlDomExtended.getText( wobjXMLConfigNode );
        //
        wvarStep = 90;
        wobjXMLListadoProd = new XmlDomExtended();
        wobjXMLListadoProd.loadXML( invoke( "getListadoProductores", new Variant[] { new Variant(mcteStoreProcListarPendientes), new Variant(mvarCODOP), new Variant(mvarFECHA), new Variant("S") } ) );
        //
        //Mientras queden productores sin procesar en el listado...
        wvarStep = 100;
        mvarPendientes = mvarPendientes + wobjXMLListadoProd.selectNodes( "//PRODUCTOR" ) .getLength();
      }
      //
      //Quedaron alertas en estado pendiente y REPROCESO = N?
      wvarStep = 150;
      if( mvarPendientes > 0 )
      {
        //Envio de email informando la existencia de operaciones pendientes en la fecha
        wvarStep = 160;
        invoke( "enviarEMailCtrl", new Variant[] { new Variant(mvarPendientes), new Variant("ALGUNAS OPERACIONES QUEDARON PENDIENTES"), new Variant(DateTime.format( DateTime.now() )) } );
      }
      //
      //Registraci�n en Log de fin de proceso
      wvarStep = 170;
      if( mvarProcConErrores )
      {
        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("E- Fin OVCTRLSCH. Estado: ERROR") } );
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      else
      {
        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("0- Fin OVCTRLSCH. Estado: OK") } );
        pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }

      //FIN
      //
      wvarStep = 180;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;
      //
      wvarStep = 190;
      wobjXMLRequest = null;
      wobjXMLListadoProd = null;
      wobjXMLNode = (org.w3c.dom.Node) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //Inserta Error en EventViewer
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        //Inserta error en Log
        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant(mcteClassName + " - " + wcteFnName + " - " + wvarStep + " - " + Err.getError().getNumber() + " - " + "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " - " + vbLogEventTypeError) } );

        invoke( "insertLog", new Variant[] { new Variant(mcteOperacion), new Variant("99- Fin OVCTRLSCH. Estado: ABORTADO POR ERROR") } );

        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private Variant insertLog( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    Variant insertLog = new Variant();
    XmlDomExtended wobjXMLConfig = null;
    boolean wvarReqRespInTxt = false;
    int mvarDebugCode = 0;
    //
    //
    wvarReqRespInTxt = false;
    //
    wobjXMLConfig = new XmlDomExtended();
    wobjXMLConfig.load( System.getProperty("user.dir") + "\\XML\\Config.xml" );
    //1=Debug to BD - 2=Debug to BD and File sin request/Responses - 3=Debug to BD andFile completo
    mvarDebugCode = Obj.toInt( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//DEBUG" )  ) );
    //
    if( (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<REQUEST>", true ) > 0) || (Strings.find( 1, Strings.toUpperCase( pvarDescripcion ), "<RESPONSE>", true ) > 0) )
    {
      wvarReqRespInTxt = true;
    }
    //
    if( (mvarDebugCode >= 1) && ! (wvarReqRespInTxt) )
    {
      invoke( "debugToBD", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion) } );
    }
    if( (mvarDebugCode == 3) || ((mvarDebugCode == 2) && ! (wvarReqRespInTxt)) )
    {
      invoke( "debugToFile", new Variant[] { new Variant(pvarCODOP), new Variant(pvarDescripcion) } );
    }
    //
    return insertLog;
  }

  private boolean debugToBD( String pvarCODOP, String pvarDescripci�n ) throws Exception
  {
    boolean debugToBD = false;
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( mcteStoreProcInsLog );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    
    
    //
    
    
    //
    wobjDBCmd.execute( new Variant( AdoConst.adUnsupported ), null, AdoConst.adCmdText );
    //
    //Controlamos la respuesta del SQL
    if( wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0 )
    {
      debugToBD = true;
    }
    else
    {
      debugToBD = false;
    }
    //
    wobjHSBC_DBCnn = null;
    wobjDBCnn = (Connection) null;
    wobjDBCmd = (Command) null;
    //
    return debugToBD;
  }

  private void debugToFile( String pvarCODOP, String pvarDescripcion ) throws Exception
  {
    XmlDomExtended wobjXMLConfig = null;
    String wvarText = "";
    String wvarFilename = "";
    int wvarNroArch = 0;
    //
    //
    wvarText = "(" + DateTime.now() + "-" + DateTime.now() + "):" + pvarDescripcion;

    wvarFilename = "debug-" + pvarCODOP + "-" + DateTime.year( DateTime.now() ) + Strings.right( ("00" + DateTime.month( DateTime.now() )), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 ) + ".log";
    wvarNroArch = FileSystem.getFreeFile();
    FileSystem.openAppend( System.getProperty("user.dir") + "\\DEBUG\\" + wvarFilename, 1 );
    FileSystem.out(1).writeValue( wvarText );
    FileSystem.out(1).println();
    FileSystem.close( 1 );
    //
  }

  private String getListadoProductores( String pvarSPListarProd, String pvarCODOP, String pvarFECHA, String pvarREPROCESO ) throws Exception
  {
    String getListadoProductores = "";
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstDBResult = null;
    //
    //
    com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
    //error: function 'GetDBConnection' was not found.
    wobjDBCnn = connFactory.getDBConnection(ModGeneral.gcteDB);
    wobjDBCmd = new Command();
    //
    wobjDBCmd.setActiveConnection( wobjDBCnn );
    wobjDBCmd.setCommandText( pvarSPListarProd );
    wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
    //
    
    
    //
    
    
    //
    if( pvarREPROCESO.equals( "S" ) )
    {
      
      
    }
    //
    wrstDBResult = wobjDBCmd.execute();
    wrstDBResult.setActiveConnection( (Connection) null );
    //
    //Controlamos la respuesta del SQL
    if( (wobjDBCmd.getParameters().getParameter("@RETURN_VALUE").getValue().toInt() >= 0) && ! (wrstDBResult.isEOF()) )
    {
      //
      wobjXMLResponse = new XmlDomExtended();
      wobjXSLResponse = new XmlDomExtended();
      //
      wobjXSLResponse.loadXML( p_GetXSL());

      wobjXMLResponse.load(AdoUtils.saveRecordset(wrstDBResult));
      //
      getListadoProductores = "<PRODUCTORES>" + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>", "" ) + "</PRODUCTORES>";

      wobjXMLResponse = null;
      wobjXSLResponse = null;
    }
    else
    {
      getListadoProductores = "<PRODUCTORES/>";
    }

    return getListadoProductores;
  }

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";

    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>";
    wvarStrXSL = wvarStrXSL + " <xsl:template match='z:row'>";
    wvarStrXSL = wvarStrXSL + "  <xsl:element name='PRODUCTOR'>";

    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDSUSCRIPTOS'><xsl:value-of select='@IDSUSCRIPTOS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='IDCONTROL'><xsl:value-of select='@IDCONTROL' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='USUARCOD'><xsl:value-of select='@USUARCOD' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='NIVELAS'><xsl:value-of select='@NIVELAS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='CLIENSECAS'><xsl:value-of select='@CLIENSECAS' /></xsl:element>";
    wvarStrXSL = wvarStrXSL + "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>";

    wvarStrXSL = wvarStrXSL + "  </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";

    p_GetXSL = wvarStrXSL;

    return p_GetXSL;
  }

  private boolean enviarEMailCtrl( int pvarPendientes, String pvarEstado, String pvarFECHA ) throws Exception
  {
    boolean enviarEMailCtrl = false;
    String wvarRequest = "";
    String wvarResponse = "";
    Object wobjClass = null;
    XmlDomExtended wvarXMLRequest = null;
    XmlDomExtended wvarXMLResponse = null;
    XmlDomExtended wobjXMLParametro = null;
    XmlDomExtended wobjXMLCODOPParam = null;
    boolean wvarRellamar = false;
    String wvarResponseAIS = "";
    String wvarCODOPDesc = "";
    //
    //
    wobjXMLParametro = new XmlDomExtended();
    wobjXMLParametro.load( System.getProperty("user.dir") + "\\XML\\msgEnvioMailCtrl.xml" );
    //
    invoke( "agregarParametro", new Variant[] { new Variant(wobjXMLParametro), new Variant("%%HORA%%"), new Variant(DateTime.format( DateTime.now() )) } );
    invoke( "agregarParametro", new Variant[] { new Variant(wobjXMLParametro), new Variant("%%FECHA%%"), new Variant(pvarFECHA) } );
    invoke( "agregarParametro", new Variant[] { new Variant(wobjXMLParametro), new Variant("%%ESTADO%%"), new Variant(pvarEstado) } );
    invoke( "agregarParametro", new Variant[] { new Variant(wobjXMLParametro), new Variant("%%PENDIENTES%%"), new Variant(String.valueOf( new Variant(pvarPendientes) )) } );
    //
    wvarRequest = XmlDomExtended.marshal(wobjXMLParametro.selectSingleNode( "//Request" ));
    //
    wobjClass = new cam_OficinaVirtual.camA_EnviarMail();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    wobjClass = null;
    //
    wvarXMLResponse = new XmlDomExtended();
    wvarXMLResponse.loadXML( wvarResponse );
    //
    if( ! (wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  == (org.w3c.dom.Node) null) )
    {
      if( XmlDomExtended .getText( wvarXMLResponse.selectSingleNode( "//Response/Estado/@resultado" )  ).equals( "true" ) )
      {
        enviarEMailCtrl = true;
      }
      else
      {
        enviarEMailCtrl = false;
      }
    }
    else
    {
      enviarEMailCtrl = false;
    }
    //
    wvarXMLResponse = null;
    wobjClass = null;
    wobjXMLParametro = null;
    return enviarEMailCtrl;
  }

  private void agregarParametro( XmlDomExtended pobjXMLParametro, String pvarParam, String pvarValor ) throws Exception
  {
    org.w3c.dom.Element wobjXMLElement = null;
    org.w3c.dom.Element wobjXMLElementAux = null;
    org.w3c.dom.CDATASection wobjXMLElementCda = null;
    XmlDomExtended wobjXMLParametro = null;

    wobjXMLElement = pobjXMLParametro.getDocument().createElement( "PARAMETRO" );
    //
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_NOMBRE" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarParam );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    wobjXMLElement.appendChild( wobjXMLElementAux );
    wobjXMLElementAux = pobjXMLParametro.getDocument().createElement( "PARAM_VALOR" );
    wobjXMLElementCda = pobjXMLParametro.getDocument().createCDATASection( pvarValor );
    wobjXMLElementAux.appendChild( wobjXMLElementCda );
    //
    wobjXMLElement.appendChild( wobjXMLElementAux );
    //
    /*unsup pobjXMLParametro.selectSingleNode( "//PARAMETROS" ) */.appendChild( wobjXMLElement );
    //
  }

  private void ObjectControl_Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Recordset wrstDBResult = null;
    Parameter wobjDBParm = null;


    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
