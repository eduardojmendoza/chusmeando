<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: msgLBAANU.xsl   
	Fecha de Creación: 19/03/2009   
	PPCR: 2008-00791
	Desarrollador: Matias Coaker
	Descripción: Este archivo XSL se crea para configurar la ejecución y request
				 del código de operacion LBAANU.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<Request>
			<EstadosMsgAIS>
				<xsl:copy-of select="/ROOT/Response/Estado[1]"/>
			</EstadosMsgAIS>
			<xmlDataSource>
				<Response>
					<FECHADES>
						<xsl:value-of select="//FECHADES"/>
					</FECHADES>					
					<FECHAHAS>
						<xsl:value-of select="//FECHAHAS"/>
					</FECHAHAS>	
					<CANTREGS>
						<xsl:value-of select="count(//REG[concat(substring(FECANU,7,4),substring(FECANU,4,2),substring(FECANU,1,2)) > //FECHADDE])"/>
					</CANTREGS>
					<REGS>
						<xsl:for-each select="//REG[concat(substring(FECANU,7,4),substring(FECANU,4,2),substring(FECANU,1,2)) > //FECHADDE]">
							<REG>
							<RAMOPCOD><xsl:value-of select="PROD"/></RAMOPCOD>
							<POLIZANN><xsl:value-of select="substring(POL,1,2)"/></POLIZANN>							
							<POLIZSEC><xsl:value-of select="substring(POL,3,6)"/></POLIZSEC>
							<CERTIPOL><xsl:value-of select="CERPOL"/></CERTIPOL>
							<CERTIANN><xsl:value-of select="CERANN"/></CERTIANN>
							<CERTISEC><xsl:value-of select="CERSEC"/></CERTISEC>
							<TOMADOR><xsl:value-of select="CLIDES"/></TOMADOR>
							<FECANUL><xsl:value-of select="FECANU"/></FECANUL>
							<MOTIVO>ANUL. FALTA DE PAGO</MOTIVO>							
							<AGENCLA><xsl:value-of select="substring(COD,1,2)"/></AGENCLA>
							<AGENCOD><xsl:value-of select="substring(COD,4,4)"/></AGENCOD>
							<SINIESTRO><xsl:value-of select="SINI"/></SINIESTRO>
							</REG>
						</xsl:for-each>
					</REGS>
				</Response>
			</xmlDataSource>
			<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>
			<Raiz>share:generateAndSendPdfReport</Raiz>
			<recipientTO></recipientTO>
			<recipientsCC/>
			<recipientsBCC/>
			<templateFileName>LBAEmailOVPolizasAnuladasPDF</templateFileName>
			<parametersTemplate/>
			<attachFileName>ListadoAnulaciones.pdf</attachFileName>
			<from>ayuda.ovirtual@hsbc.com.ar</from>
			<replyTO>ayuda.ovirtual@hsbc.com.ar</replyTO>
			<bodyText/>
			<subject/>
			<importance>1</importance>
			<reportId>AlertaxMail_Anuladas_v1.0</reportId>
			<attachPassword></attachPassword>
			<imagePathFile>/web/BannerLBA.GIF</imagePathFile>
		</Request>
	</xsl:template>
</xsl:stylesheet>
