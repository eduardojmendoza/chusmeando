<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: msgNYLSIN.xsl   
	Fecha de Creación: 19/03/2009   
	PPCR: 2008-00791
	Desarrollador: Matias Coaker
	Descripción: Este archivo XSL se crea para configurar la ejecución y request
				 del código de operacion NYLSIN.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<Request>
			<EstadosMsgAIS>
				<xsl:copy-of select="/ROOT/Response/Estado[1]"/>
			</EstadosMsgAIS>
			<xmlDataSource>
				<Response>
					<FECHADES>
						<xsl:value-of select="//FECHADES"/>
					</FECHADES>
					<FECHAHAS>
						<xsl:value-of select="//FECHAHAS"/>
					</FECHAHAS>
					<CANTREGS>
						<xsl:value-of select="count(//REG)"/>
					</CANTREGS>
					<REGS>
						<xsl:for-each select="//REG">
							<REG>
								<PROD>
									<xsl:value-of select="RAMOPCOD"/>
								</PROD>
								<SINIAN>
									<xsl:value-of select="SINIEANN"/>
								</SINIAN>
								<SININUM>
									<xsl:value-of select="SINIENUM"/>
								</SININUM>
								<CLIDES>
									<xsl:value-of select="CLIENTE"/>
								</CLIDES>
								<RAMO>
									<xsl:value-of select="TIPOPOLIZA"/>
								</RAMO>
								<POL>
									<xsl:value-of select="POLIZANN"/>
									<xsl:value-of select="POLIZSEC"/>
								</POL>
								<CERPOL>
									<xsl:value-of select="CERTIPOL"/>
								</CERPOL>
								<CERANN>
									<xsl:value-of select="CERTIANN"/>
								</CERANN>
								<CERSEC>
									<xsl:value-of select="CERTISEC"/>
								</CERSEC>
								<EST>
									<xsl:value-of select="ESTADOSINIESTROS"/>
								</EST>
								<FECSINI>
									<xsl:value-of select="substring(FECHASINIESTRO,7,2)"/>/<xsl:value-of select="substring(FECHASINIESTRO,5,2)"/>/<xsl:value-of select="substring(FECHASINIESTRO,1,4)"/>
								</FECSINI>
								<AGE>
									<xsl:value-of select="AGENTECLASE"/>
									<xsl:value-of select="AGENTECODIGO"/>
								</AGE>
							</REG>
						</xsl:for-each>
					</REGS>
				</Response>
			</xmlDataSource>
			<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>
			<Raiz>share:generateAndSendPdfReport</Raiz>
			<recipientTO/>
			<recipientsCC/>
			<recipientsBCC/>
			<templateFileName>NYLEmailOVSiniestroAbiertoPDF</templateFileName>
			<parametersTemplate/>
			<attachFileName>ListadoSiniestros.pdf</attachFileName>
			<from>ayuda.ovirtual@hsbc.com.ar</from>
			<replyTO>ayuda.ovirtual@hsbc.com.ar</replyTO>
			<bodyText/>
			<subject/>
			<importance>1</importance>
			<reportId>AlertaxMail_Siniestros_v1.0</reportId>
			<attachPassword/>
			<imagePathFile>/web/BannerNYL.GIF</imagePathFile>
		</Request>
	</xsl:template>
</xsl:stylesheet>
