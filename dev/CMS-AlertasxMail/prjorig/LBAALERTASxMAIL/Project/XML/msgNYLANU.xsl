<!--
	COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
	LIMITED 2009. ALL RIGHTS RESERVED
	
	This software is only to be used for the purpose for which it has been provided.
	No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
	system or translated in any human or computer language in any way or for any other
	purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
	Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
	offence, which can result in heavy fines and payment of substantial damages.
	
	Nombre del Fuente: msgNYLANU.xsl   
	Fecha de Creación: 19/03/2009   
	PPCR: 2008-00791
	Desarrollador: Matias Coaker
	Descripción: Este archivo XSL se crea para configurar la ejecución y request
				 del código de operacion NYLANU.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
	<xsl:output method="xml"/>
	<xsl:template match="/">
		<Request>
			<EstadosMsgAIS>
				<xsl:copy-of select="/ROOT/Response/Estado[1]"/>
			</EstadosMsgAIS>
			<xmlDataSource>
				<Response>
					<FECHADES>
						<xsl:value-of select="//FECHADES"/>
					</FECHADES>					
					<FECHAHAS>
						<xsl:value-of select="//FECHAHAS"/>
					</FECHAHAS>		
					<CANTREGS>
						<xsl:value-of select="count(//POLI[RAMOPCOD!=''])"/>
					</CANTREGS>
					<REGS>
						<xsl:for-each select="//POLI[RAMOPCOD!='']">
							<REG>
							<RAMOPCOD><xsl:value-of select="RAMOPCOD"/></RAMOPCOD>
							<POLIZANN><xsl:value-of select="POLIZANN"/></POLIZANN>							
							<POLIZSEC><xsl:value-of select="POLIZSEC"/></POLIZSEC>
							<CERTIPOL><xsl:value-of select="CERTIPOL"/></CERTIPOL>
							<CERTIANN><xsl:value-of select="CERTIANN"/></CERTIANN>
							<CERTISEC><xsl:value-of select="CERTISEC"/></CERTISEC>
							<TOMADOR><xsl:value-of select="TOMADOR"/></TOMADOR>
							<FECANUL><xsl:value-of select="FECANUL"/></FECANUL>
							<MOTIVO><xsl:value-of select="MOTIVO"/></MOTIVO>							
							<AGENCLA><xsl:value-of select="AGENCLA"/></AGENCLA>
							<AGENCOD><xsl:value-of select="AGENCOD"/></AGENCOD>
							<SINIESTRO><xsl:value-of select="SINIESTRO"/></SINIESTRO>
							</REG>
						</xsl:for-each>
					</REGS>
				</Response>
			</xmlDataSource>
			<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>
			<Raiz>share:generateAndSendPdfReport</Raiz>
			<recipientTO></recipientTO>
			<recipientsCC/>
			<recipientsBCC/>
			<templateFileName>NYLEmailOVPolizasAnuladasPDF</templateFileName>
			<parametersTemplate/>
			<attachFileName>ListadoAnulaciones.pdf</attachFileName>
			<from>ayuda.ovirtual@hsbc.com.ar</from>
			<replyTO>ayuda.ovirtual@hsbc.com.ar</replyTO>
			<bodyText/>
			<subject/>
			<importance>1</importance>
			<reportId>AlertaxMail_Anuladas_v1.0</reportId>
			<attachPassword></attachPassword>
			<imagePathFile>/web/BannerNYL.GIF</imagePathFile>
		</Request>
	</xsl:template>
</xsl:stylesheet>
