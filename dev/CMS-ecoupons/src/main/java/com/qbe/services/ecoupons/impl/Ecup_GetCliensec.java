package com.qbe.services.ecoupons.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class Ecup_GetCliensec implements VBObjectClass {
	
	private static Logger logger = Logger.getLogger(Ecup_GetCliensec.class.getName());
	
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.ECup_GetCliensec";
  static final String mcteOpID = "0001";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_TipoDocu = "//TIPODOCU";
  static final String mcteParam_NumeDocu = "//NUMEDOCU";
  static final String mcteParam_FechaNac = "//FECHANAC";
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    String wvarMensaje = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarTipoDocu = "";
    String wvarNumeDocu = "";
    String wvarFechaNac = "";
    int wvarPos = 0;
    String strParseString = "";
    boolean wvarControls = false;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      //
      wobjXMLRequest.loadXML( Request );
      wvarTipoDocu = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TipoDocu )  );
      wvarNumeDocu = Strings.left( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_NumeDocu )  ) + Strings.fill( 11, " " ), 11 );
      wvarFechaNac = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaNac )  );
      //
      wobjXMLRequest = null;
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
//      wobjXMLConfig = new XmlDomExtended();
//      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      wvarArea = mcteOpID + wvarTipoDocu + wvarNumeDocu + wvarFechaNac;
      logger.fine("wvarArea: " + wvarArea);

//      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.ECOUPONS_PROXY); //TODO actualizar en el resto de los componentes de ecoupons

      logger.info("MQProxy.getInstance retornó wobjFrame2MQ: " + wobjFrame2MQ);
      if ( wobjFrame2MQ == null ) {
    	  throw new Exception("El MQProxy.getInstance retornó wobjFrame2MQ null. Revisar la configuración de los beans de conectores");
      }
      StringHolder strParseStringHolder = new StringHolder(strParseString);
      wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
      logger.fine("wvarMQError: " + wvarMQError);
      strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=\"false\" mensaje=\"El servicio de consulta no se encuentra disponible\" />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, 1, "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = 22;
      wvarControls = true;

      if( Obj.toInt( Strings.mid( strParseString, wvarPos, 1 ) ) == 1 )
      {
        //NO EXISTE EL CLIENTE
        Response.set( "<Response><Estado resultado=\"true\" mensaje=\"Los datos ingresados no concuerdan con los datos registrados en la compañia.  Ante cualquier duda  comuníquese con el centro de Atención al Cliente al 0-810-999-2424. Muchas Gracias\" /></Response>" );
        wvarControls = false;
      }
      else if( (Strings.mid( strParseString, (wvarPos + 1), 9 ).equals( "000000000" )) || (Strings.trim( Strings.mid( strParseString, (wvarPos + 1), 9 ) ).equals( "" )) )
      {
        // NO DEVOLVIO CLIENTSEC
        Response.set( "<Response><Estado resultado=\"true\" mensaje=\"Los datos ingresados no concuerdan con los datos registrados en la compañia.  Ante cualquier duda  comuníquese con el centro de Atención al Cliente al 0-810-999-2424. Muchas Gracias\" /></Response>" );
        wvarControls = false;
      }

      if( wvarControls )
      {
        // ARMA LA RESPUESTA
        wvarResult = "<CLIENSEC>" + Strings.mid( strParseString, wvarPos + 1, 9 ) + "</CLIENSEC><NOMBRE>" +  Strings.trim( Strings.mid( strParseString, (wvarPos + 10), 60 ) ) + "</NOMBRE>";
        Response.set( "<Response><Estado resultado=\"true\" mensaje=\"\" />" + wvarResult + "</Response>" );
      }
      //
      wvarStep = 170;
      IAction_Execute = 0;

      return IAction_Execute;
    }
    catch( Exception _e_ )
    {
		logger.log(Level.SEVERE, "Exception al ejecutar el request", _e_);

        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, 1, "Error= [" +1 + "] - " + 1 + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        return IAction_Execute;
    }
  }

}
