package com.qbe.services.ecoupons.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ecoupons.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class Ecup_GetPolHOM implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.ECup_GetPolHOM";
  static final String mcteOpID = "0003";
  /**
   * MENSAJE 0003 (HOGAR).
   * 
   * Entrada.
   * �   Numero de cliente       NUMERICO  09
   * �   C�digo de producto      CAR�CTER  04
   * �   Numero de certificado   NUMERICO  10
   * 
   * Respuesta.
   * �   Status                  CAR�CTER  01 (0=OK/1=NOK/2=Canal cobro no efectivo)
   * �   P�liza ingresada        NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
   * �   Canal de cobro          NUMERICO  04 ( 9999 - la p�liza esta anulada )
   * �   Tipo de vivienda        CAR�CTER  40
   * �   Domicilio del Riesgo    CAR�CTER  130 (CALLE/NUMERO/ESCALERA/PISO/PUERTA/LOCALIDAD)
   * �   Direcci�n e-mail        CAR�CTER  60 ( si la p�liza envia cupones por e-mail )
   * �   Clave de encriptamiento CAR�CTER 264 ( si la p�liza envia cupones por e-mail )
   * �   Marca de propaganda   CAR�CTER  01 (S=acepta recibir propaganda por mail
   *  N=no acepta recibir )
   * �   P�liza renovaci�n       NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
   * �   Marca renovaci�n        NUMERICO  01 (0=no renovada/1=Posterior/2=Anterior)
   * Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_TipoSeg = "//TIPOSEG";
  static final String mcteParam_Poliza = "//POLIZA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    String wvarMensaje = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarTipoSeg = "";
    String wvarPoliza = "";
    String wvarCliensec = "";
    int wvarPos = 0;
    String strParseString = "";
    boolean wvarControls = false;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      //
      wobjXMLRequest.loadXML( Request );
      wvarCliensec = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      wvarTipoSeg = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TipoSeg )  );
      wvarPoliza = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  );
      //
      wobjXMLRequest = null;
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      wvarArea = mcteOpID + wvarCliensec + wvarTipoSeg + wvarPoliza;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.ECOUPONS_PROXY);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 160;
      wvarResult = "";
      //wvarPos = 20
      wvarPos = 24;

      
      if( Obj.toInt( Strings.mid( strParseString, wvarPos, 1 ) ) == 0 )
      {
        //POLIZA OK
        //Chequeo si el Canal de Cobro = 9999 es Poliza Anulada
        if( Strings.mid( strParseString, (wvarPos + 27), 4 ).equals( "9999" ) )
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "La póliza que ha ingresado es inexistente" + String.valueOf( (char)(34) ) + "/>" + Strings.replace( Request, "Request", "PARAMETROS" ) + wvarResult + "</Response>" );
        }
        else
        {
          //ARMA LA RESPUESTA
          wvarResult = "<POLIZANN>" + Strings.mid( strParseString, wvarPos + 1, 2 ) + "</POLIZANN>";
          wvarResult = wvarResult + "<POLIZSEC>" + Strings.mid( strParseString, (wvarPos + 3), 6 ) + "</POLIZSEC>";
          wvarResult = wvarResult + "<CERTIPOL>" + Strings.mid( strParseString, (wvarPos + 9), 4 ) + "</CERTIPOL>";
          wvarResult = wvarResult + "<CERTIANN>" + Strings.mid( strParseString, (wvarPos + 13), 4 ) + "</CERTIANN>";
          wvarResult = wvarResult + "<CERTISEC>" + Strings.mid( strParseString, (wvarPos + 17), 6 ) + "</CERTISEC>";
          wvarResult = wvarResult + "<SUPLENUM>" + Strings.mid( strParseString, (wvarPos + 23), 4 ) + "</SUPLENUM>";
          wvarResult = wvarResult + "<CCOBRO>" + Strings.mid( strParseString, (wvarPos + 27), 4 ) + "</CCOBRO>";
          wvarResult = wvarResult + "<BIEN><![CDATA[" + ModCaracterAscii.CaracteresAsciiValidos( Strings.mid( strParseString, (wvarPos + 31), 40 ) ) + "]]></BIEN>";
          //wvarResult = wvarResult & "<DIRBIEN>" & Mid(strParseString, wvarPos + 71, 82) & "</DIRBIEN>"
          wvarResult = wvarResult + "<DIRBIEN><![CDATA[" + ModCaracterAscii.CaracteresAsciiValidos( Strings.mid( strParseString, (wvarPos + 71), 130 ) ) + "]]></DIRBIEN>";

          if( Strings.trim( Strings.mid( strParseString, (wvarPos + 261), 264 ) ).equals( "" ) )
          {
            wvarResult = wvarResult + "<SUSCRIPTO>No</SUSCRIPTO>";
          }
          else
          {
            wvarResult = wvarResult + "<SUSCRIPTO>Si</SUSCRIPTO>";
            wvarResult = wvarResult + "<EMAIL>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 201), 60 ) ) + "</EMAIL>";
            wvarResult = wvarResult + "<CLAVE>" + ModEncryptDecrypt.CapicomDecrypt( Strings.trim( Strings.mid( strParseString, (wvarPos + 261), 264 ) ) ) + "</CLAVE>";
            if( Strings.toUpperCase( Strings.trim( Strings.mid( strParseString, (wvarPos + 525), 1 ) ) ).equals( "S" ) )
            {
              wvarResult = wvarResult + "<INFOADICIONAL>SI</INFOADICIONAL>";
            }
            else
            {
              wvarResult = wvarResult + "<INFOADICIONAL>NO</INFOADICIONAL>";
            }
          }

          wvarResult = wvarResult + "<POLIZA_RENOVACION>" + (Strings.mid( strParseString, wvarPos + 526, 4 ) + "-" + Strings.mid( strParseString, (wvarPos + 526 + 4), 2 ) + "-" + Strings.mid( strParseString, (wvarPos + 526 + 6), 6 ) + "/" + Strings.mid( strParseString, (wvarPos + 526 + 12), 4 ) + "-" + Strings.mid( strParseString, (wvarPos + 526 + 16), 4 ) + "-" + Strings.mid( strParseString, (wvarPos + 526 + 20), 6 )) + "</POLIZA_RENOVACION>";

          if( Obj.toInt( Strings.trimRight( Strings.mid( strParseString, (wvarPos + 552), 1 ) ) ) == 1 )
          {
            wvarResult = wvarResult + "<MARCA_RENOVACION desc='Poliza Posterior'>" + Strings.trimRight( Strings.mid( strParseString, (wvarPos + 552), 1 ) ) + "</MARCA_RENOVACION>";
          }
          else if( Obj.toInt( Strings.trimRight( Strings.mid( strParseString, (wvarPos + 552), 1 ) ) ) == 2 )
          {
            wvarResult = wvarResult + "<MARCA_RENOVACION desc='Poliza Anterior'>" + Strings.trimRight( Strings.mid( strParseString, (wvarPos + 552), 1 ) ) + "</MARCA_RENOVACION>";
          }
          else
          {
            wvarResult = wvarResult + "<MARCA_RENOVACION desc='Poliza No Renovada'>" + Strings.trimRight( Strings.mid( strParseString, (wvarPos + 552), 1 ) ) + "</MARCA_RENOVACION>";
          }


          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + Strings.replace( Request, "Request", "PARAMETROS" ) + wvarResult + "</Response>" );
        }
      }
      else if( Obj.toInt( Strings.mid( strParseString, wvarPos, 1 ) ) == 1 )
      {
        //NO EXISTE LA POLIZA
        //Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Los datos ingresados no concuerdan con los datos registrados en la compa��a.  Ante cualquier duda  comun�quese con el centro de Atenci�n al Cliente al 0-810-999-2424. Muchas Gracias" & Chr(34) & " />" & Strings.replace(Request, "Request", "PARAMETROS") & "</Response>"
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "La póliza que ha ingresado es inexistente." + String.valueOf( (char)(34) ) + " />" + Strings.replace( Request, "Request", "PARAMETROS" ) + "</Response>" );
      }
      else if( Obj.toInt( Strings.mid( strParseString, wvarPos, 1 ) ) == 2 )
      {
        //LA FORMA DE PAGO ES DISTINTA DE LA PERMITIDA
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El pago de su seguro está adherido a débito automático. Ante cualquier duda  comuníquese con el centro de Atención al Cliente al 0-810-999-2424. Muchas Gracias" + String.valueOf( (char)(34) ) + " />" + Strings.replace( Request, "Request", "PARAMETROS" ) + "</Response>" );
      }
      //
      wvarStep = 170;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
