package com.qbe.services.ecoupons.impl;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;

import org.junit.Test;

import com.qbe.vbcompat.string.StringHolder;

public class ECup_GetCliensecTest {
 
	@Test
	public void testIAction_Execute() {
		Ecup_GetCliensec comp = new Ecup_GetCliensec();
		String request ="<Request><TIPODOCU>01</TIPODOCU><NUMEDOCU>13373141</NUMEDOCU><FECHANAC></FECHANAC></Request>";
		StringHolder responseHolder = new StringHolder();
		int response = comp.IAction_Execute(request, responseHolder, null);
		String responseTest ="<Response><Estado resultado='true' mensaje=''/><CLIENSEC>101976143</CLIENSEC><NOMBRE>ACEVEDO DE SAUCEDO  MIRIAM</NOMBRE></Response>";
	    assertEquals(StringUtils.deleteWhitespace(responseTest), StringUtils.deleteWhitespace(responseHolder.getValue())); 	
	}
}
