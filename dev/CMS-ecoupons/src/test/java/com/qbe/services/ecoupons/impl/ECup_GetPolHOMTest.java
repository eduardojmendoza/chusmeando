package com.qbe.services.ecoupons.impl;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;

import org.junit.Test;

import com.qbe.vbcompat.string.StringHolder;

public class ECup_GetPolHOMTest {
 
	@Test
	public void testIAction_Execute() {
		Ecup_GetPolHOM comp = new Ecup_GetPolHOM();
		String request ="<REQUEST><CLIENSEC>101976143</CLIENSEC><TIPOSEG>HOM1</TIPOSEG><POLIZA>0000004773</POLIZA></REQUEST>";
		StringHolder responseHolder = new StringHolder();
		int response = comp.IAction_Execute(request, responseHolder, null);
		String responseTest ="<Response>"
        +"<Estado resultado='true' mensaje=''/>"
        +"<REQUEST>"
        +"<CLIENSEC>101976143</CLIENSEC>"
        +"<TIPOSEG>HOM1</TIPOSEG>"
	    +"<POLIZA>0000004773</POLIZA>"
        +"</REQUEST>"
        +"<POLIZANN>00</POLIZANN>"
        +"<POLIZSEC>157838</POLIZSEC>"
        +"<CERTIPOL>0000</CERTIPOL>"
        +"<CERTIANN>0000</CERTIANN>"
        +"<CERTISEC>004773</CERTISEC>"
        +"<SUPLENUM>0002</SUPLENUM>"
        +"<CCOBRO>0001</CCOBRO>"
        +"<BIEN><![CDATA[CASA/PB/1ER.PISO                        ]]></BIEN>"
        +"<DIRBIEN><![CDATA[LA RIOJA                           04514         ALBERDI                                                                          ]]></DIRBIEN>"
        +"<SUSCRIPTO>No</SUSCRIPTO>"
        +"<POLIZA_RENOVACION>HOM1-00-157838/0000-0000-005228</POLIZA_RENOVACION>"
        +"<MARCA_RENOVACION desc='Poliza Anterior'>2</MARCA_RENOVACION>"
        +"</Response>";
	    assertEquals(StringUtils.deleteWhitespace(responseTest), StringUtils.deleteWhitespace(responseHolder.getValue())); 	
	}
}
