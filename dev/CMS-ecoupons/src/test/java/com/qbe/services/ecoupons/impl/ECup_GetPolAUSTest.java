package com.qbe.services.ecoupons.impl;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;

import org.junit.Test;

import com.qbe.vbcompat.string.StringHolder;

public class ECup_GetPolAUSTest {

	@Test
	public void testIAction_Execute() {
		ECup_GetPolAUS comp = new ECup_GetPolAUS();
		String request ="<REQUEST>"
		+"<CLIENSEC>000412335</CLIENSEC>"
		+"<TIPOSEG>AUS1</TIPOSEG>"
		+"<POLIZA>0001856577</POLIZA>"
		+"</REQUEST>";
		StringHolder responseHolder = new StringHolder();
		int response = comp.IAction_Execute(request, responseHolder, null);
		String responseTest ="<Response>"
		+"<Estado resultado='true' mensaje=''/>"
        +"<REQUEST>"
        +"<CLIENSEC>000412335</CLIENSEC>"
        +"<TIPOSEG>AUS1</TIPOSEG>"
        +"<POLIZA>0001856577</POLIZA>"
        +"</REQUEST>"
        +"<POLIZANN>00</POLIZANN>"
        +"<POLIZSEC>000001</POLIZSEC>"
        +"<CERTIPOL>0000</CERTIPOL>"
        +"<CERTIANN>0001</CERTIANN>"
        +"<CERTISEC>856577</CERTISEC>"
        +"<SUPLENUM>0000</SUPLENUM>"
        +"<CCOBRO>0001</CCOBRO>"
        +"<BIEN><![CDATA[PEUGEOT                       207]]></BIEN>"
        +"<SUSCRIPTO>No</SUSCRIPTO>"
        +"<POLIZA_RENOVACION>AUS1-00-000001/0000-0001-981402</POLIZA_RENOVACION>"
        +"<MARCA_RENOVACION desc='Poliza Anterior'>2</MARCA_RENOVACION>"
        +"</Response>";
		assertEquals(StringUtils.deleteWhitespace(responseTest), StringUtils.deleteWhitespace(responseHolder.getValue())); 
		
	}
}
