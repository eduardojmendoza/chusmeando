package com.qbe.services.ecoupons.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ecoupons.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class ECup_PutDesbloqUsua implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.ECup_PutDesbloqUsua";
  static final String mcteStoreProc = "SPSNCV_ECOUPON_DESBLOQUEAR_USUARIO";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_TipoDocu = "//TIPODOCU";
  static final String mcteParam_NumeDocu = "//NUMEDOCU";
  static final String mcteParam_FechaNac = "//FECHANAC";
  static final String mcteParam_Empresa = "//EMPRESA";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String pvarRequest, StringHolder pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarTipoDocu = "";
    String wvarNumeDocu = "";
    String wvarFechaNac = "";
    String wvarEmpresa = "";
    XmlDomExtended wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wvarTipoDocu = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_TipoDocu )  == (org.w3c.dom.Node) null) )
      {
        wvarTipoDocu = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_TipoDocu )  );
      }
      //
      wvarStep = 30;
      wvarNumeDocu = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_NumeDocu )  == (org.w3c.dom.Node) null) )
      {
        wvarNumeDocu = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_NumeDocu )  );
      }
      //
      wvarStep = 40;
      wvarFechaNac = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_FechaNac )  == (org.w3c.dom.Node) null) )
      {
        //Armo la fecha en el formato yyyymmdd
        wvarFechaNac = Strings.mid( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaNac )  ), 5, 4 ) + "-" + Strings.mid( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaNac )  ), 3, 2 ) + "-" + Strings.mid( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_FechaNac )  ), 1, 2 );
      }
      //
      wvarStep = 50;
      wvarEmpresa = "";
      if( ! (wobjXMLRequest.selectSingleNode( mcteParam_Empresa )  == (org.w3c.dom.Node) null) )
      {
        wvarEmpresa = Strings.trim( XmlDomExtended .getText( wobjXMLRequest.selectSingleNode( mcteParam_Empresa )  ) );
      }
      //
      wvarStep = 55;
      //
      com.qbe.services.db.JDBCConnectionFactory connFactory = new com.qbe.services.db.JDBCConnectionFactory();
      //error: function 'GetDBConnection' was not found.
      wobjDBCnn = connFactory.getDBConnection(ModGeneral.mcteDB);
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 60;
      
      
      //
      wvarStep = 70;

      
      
      //
      wvarStep = 80;
      
      
      //
      wvarStep = 85;
      if( !wvarEmpresa.equals( "" ) )
      {
        
        
      }
      //
      wvarStep = 90;

      wrstRes = wobjDBCmd.execute();
      wrstRes.setActiveConnection( (Connection) null );

      wvarStep = 100;
      //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
      if( ! (wrstRes.isEOF()) )
      {
        if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "0" ) )
        {
          wvarStep = 110;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio ha sido reestablecido satisfactoriamente" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "1" ) )
        {
          //error
          wvarStep = 120;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Los datos ingresados no concuerdan con los datos registrados en la compa��a.  Ante cualquier duda  comun�quese con el centro de Atenci�n al Cliente al 0-810-999-2424. Muchas Gracias" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "2" ) )
        {
          //error
          wvarStep = 120;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio se encuentra habilitado" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else
        {
          wvarStep = 130;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
      }
      else
      {
        //error
      }
      //
      wvarStep = 290;
      //
      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        /*TBD mobjCOM_Context.SetComplete() ;*/
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
