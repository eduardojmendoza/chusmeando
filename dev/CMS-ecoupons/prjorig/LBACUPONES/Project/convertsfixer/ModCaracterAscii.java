package com.qbe.services.ecoupons.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ecoupons.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : ModCaracterAscii
 *  File Name : ModCaracterAscii.bas
 *  Creation Date: 17/10/2006
 *  Programmer : Fernando Osores
 *  Abstract :    Dado un STRING devuelve un STRING que tiene �nicamente caracteres ascii v�lidos. Mayor o igual a 32.
 *  *****************************************************************
 *  *****************************************************************
 *  Function : CaracteresAsciiValidos
 *  Abstract : EDado un STRING devuelve un STRING que tiene �nicamente caracteres ascii v�lidos. Mayor o igual a 32.
 *  Synopsis : CaracteresAsciiValidos(str As String) As String
 *  *****************************************************************
 */

public class ModCaracterAscii
{

  public static String CaracteresAsciiValidos( String str ) throws Exception
  {
    String CaracteresAsciiValidos = "";
    String strAux = "";
    int i = 0;


    strAux = "";

    for( i = 1; i <= Strings.len( str ); i++ )
    {
      if( Strings.asc( Strings.mid( str, i, 1 ) ) >= 32 )
      {
        strAux = strAux + Strings.mid( str, i, 1 );
      }
      else
      {
        strAux = strAux + " ";
      }
    }
    CaracteresAsciiValidos = strAux;

    return CaracteresAsciiValidos;
  }
}
