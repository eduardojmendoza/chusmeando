package com.qbe.services.ecoupons.impl;
import com.qbe.services.db.AdoUtils;
import com.qbe.services.db.JDBCConnectionFactory;
import com.qbe.vbcompat.regexp.Match;
import com.qbe.vbcompat.regexp.MatchCollection;
import com.qbe.vbcompat.regexp.VbScript_RegExp;
import com.qbe.connector.mq.MQProxyException;
import com.qbe.connector.mq.MQProxyTimeoutException;
import com.qbe.connector.mq.MQProxy;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.vbcompat.string.StringHolder;
import java.io.File;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.services.ecoupons.impl.ModGeneral;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class Ecup_PutSuscripcion implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.Ecup_PutSuscripcion";
  static final String mcteOpID = "0009";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_Clave = "//CLAVE";
  static final String mcteParam_EsHexa = "//HEX";
  static final String mcteParam_Email = "//EMAIL";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_OPERACION = "//OPERACION";
  static final String mcteParam_INFOADICIONAL = "//INFOADICIONAL";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    String wvarMensaje = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarClave = "";
    String wvarEsHexa = "";
    String wvarPoliza = "";
    String wvarEmail = "";
    String wvarRamopcod = "";
    String wvarOperacion = "";
    String wvarInfoAdicional = "";
    String wvarCliensec = "";
    int wvarPos = 0;
    String strParseString = "";
    boolean wvarControls = false;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      //
      wobjXMLRequest.loadXML( Request );
      wvarEmail = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Email )  );
      wvarCliensec = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC )  );
      wvarClave = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Clave )  );
      if( wobjXMLRequest.selectSingleNode( mcteParam_EsHexa )  == (org.w3c.dom.Node) null )
      {
        wvarEsHexa = "0";
      }
      else
      {
        wvarEsHexa = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_EsHexa )  );
      }
      wvarRamopcod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarOperacion = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_OPERACION )  );
      wvarPoliza = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_Poliza )  );
      wvarInfoAdicional = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_INFOADICIONAL )  );
      //
      wobjXMLRequest = null;
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      //quiere decir que tengo la cadena HEXA en la variable!
      if( String.valueOf( wvarEsHexa ).equals( "0" ) )
      {
        wvarClave = ModEncryptDecrypt.CapicomEncrypt( mobjCOM_Context, wvarClave );
      }

      wvarStep = 111;
      if( Strings.len( wvarClave ) < 264 )
      {
        wvarClave = wvarClave + Strings.space( (264 - Strings.len( wvarClave )) );
      }
      if( Strings.len( wvarEmail ) < 60 )
      {
        wvarEmail = wvarEmail + Strings.space( (60 - Strings.len( wvarEmail )) );
      }
      if( Strings.len( wvarEmail ) > 60 )
      {
        wvarEmail = Strings.left( wvarEmail, 60 );
      }

      wvarStep = 115;
      if( Strings.toUpperCase( wvarInfoAdicional ).equals( "SI" ) )
      {
        wvarInfoAdicional = "S";
      }
      else
      {
        wvarInfoAdicional = "N";
      }

      wvarStep = 116;
      wvarArea = mcteOpID + wvarCliensec + wvarRamopcod + wvarPoliza + wvarEmail + wvarClave + wvarOperacion + wvarInfoAdicional;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VB.val( XmlDomExtended .getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = Strings.len( wvarCliensec + wvarRamopcod + wvarPoliza + wvarEmail + wvarClave + wvarOperacion + wvarInfoAdicional );

      //Respuesta.
      //Status                  CAR�CTER  01 (0=OK/1=NOK)
      
      if( Obj.toInt( Strings.mid( strParseString, (wvarPos + 1), 1 ) ) == 0 )
      {
        //Operaci�n Exitosa.
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + Strings.replace( Request, "Request", "PARAMETROS" ) + "</Response>" );
      }
      else if( Obj.toInt( Strings.mid( strParseString, (wvarPos + 1), 1 ) ) == 1 )
      {
        //Operaci�n No Realizada
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No se ha podido realizar la operaci�n. Por favor intente nuevamente m�s tarde. Si el problema persiste comun�quese telef�nicamente con el Call Center." + String.valueOf( (char)(34) ) + " />" + Strings.replace( Request, "Request", "PARAMETROS" ) + "</Response>" );
      }
      //
      wvarStep = 170;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
