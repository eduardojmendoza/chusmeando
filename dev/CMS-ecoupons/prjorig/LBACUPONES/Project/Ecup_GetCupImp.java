import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class Ecup_GetCupImp implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.ECup_GetCupImp";
  static final String mcteOpID = "0004";
  /**
   * MENSAJE 4#
   * 
   * Entrada.
   * �   Numero de cliente       NUMERICO  09
   * �   C�digo de producto      CAR�CTER  04
   * �   P�liza completa         NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
   * 
   * Respuesta.
   * �   Numero de recibo1       NUMERICO  09
   * �   Fecha inicio vigencia1  NUMERICO  08 (ddmmaaaa)
   * �   Fecha fin vigencia1     NUMERICO  08 (ddmmaaaa)
   * �   Fecha vencimiento1      NUMERICO  08 (ddmmaaaa)
   * �   Importe1                NUMERICO  13 2
   * �   P�liza completa1        NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
   * �   Numero de recibo2       NUMERICO  09
   * �   Fecha inicio vigencia2  NUMERICO  08 (ddmmaaaa)
   * �   Fecha fin vigencia2     NUMERICO  08 (ddmmaaaa)
   * �   Fecha vencimiento2      NUMERICO  08 (ddmmaaaa)
   * �   Importe2                NUMERICO  13 2
   * �   P�liza completa2        NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
   * �   Hay mas recibos         CARACTER  01   ( Valores: S / N / V )
   * �   Hay Deuda P�liza Renov. CARACTER  01   ( Valores: S / N )
   * Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_TipoSeg = "//TIPOSEG";
  static final String mcteParam_Poliza = "//POLIZA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    String wvarMensaje = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarTipoSeg = "";
    String wvarPoliza = "";
    int wvarPos = 0;
    String strParseString = "";
    boolean wvarControls = false;
    int wvarCount = 0;
    String wvarFDesde = "";
    String wvarFHasta = "";
    String wvarFVto = "";
    String wvarRecibo = "";
    String wvarImporte = "";
    String wvarPolizaNew = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCliensec = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      wvarTipoSeg = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TipoSeg ) */ );
      wvarPoliza = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      wvarArea = mcteOpID + wvarCliensec + wvarTipoSeg + wvarPoliza;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = 40;
      wvarControls = true;

      System.out.println( Strings.trim( strParseString ) );

      //Eval�a que la p�liza no tenga recibos disponibles
      if( Strings.mid( strParseString, wvarPos, 64 ).equals( "0000000000000000000000000000000000000000000000000000000000000000" ) )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No existen cupones para publicar" + String.valueOf( (char)(34) ) + " /></Response>" );
        wvarControls = false;
      }

      if( wvarControls )
      {
        //ARMA LA RESPUESTA
        //Chequeo si tiene mas de 2 cupones
        
        if( Strings.toUpperCase( Strings.mid( strParseString, (wvarPos + 144), 1 ) ).equals( "N" ) )
        {
          //No tiene mas de dos CUPONES
          //Parseo de los recibos
          wvarResult = wvarResult + "<RECIBOS>";
          for( wvarCount = wvarPos; wvarCount <= (wvarPos + 72); wvarCount += 72 )
          {
            wvarRecibo = Strings.mid( strParseString, wvarCount, 9 );
            wvarFDesde = invoke( "FormatFecha", new Variant[] { new Variant(Strings.mid( new Variant(strParseString), new Variant(wvarCount + 9), new Variant(8) )) } );
            wvarFHasta = invoke( "FormatFecha", new Variant[] { new Variant(Strings.mid( new Variant(strParseString), new Variant(wvarCount + 17), new Variant(8) )) } );
            wvarFVto = invoke( "FormatFecha", new Variant[] { new Variant(Strings.mid( new Variant(strParseString), new Variant(wvarCount + 25), new Variant(8) )) } );
            wvarImporte = Strings.format( String.valueOf( Obj.toInt( Strings.mid( strParseString, (wvarCount + 33), 13 ) ) / 100 ), "0.00" );
            wvarPolizaNew = Strings.mid( strParseString, wvarCount + 46, 4 ) + "-" + Strings.mid( strParseString, (wvarCount + 46 + 4), 2 ) + "-" + Strings.mid( strParseString, (wvarCount + 46 + 6), 6 ) + "/" + Strings.mid( strParseString, (wvarCount + 46 + 12), 4 ) + "-" + Strings.mid( strParseString, (wvarCount + 46 + 16), 4 ) + "-" + Strings.mid( strParseString, (wvarCount + 46 + 20), 6 );
            //Elimina recibos que vienen en 0
            if( (!wvarFDesde.equals( "00/00/0000" )) && (!wvarFHasta.equals( "00/00/0000" )) )
            {
              wvarResult = wvarResult + "<RECIBO>";
              wvarResult = wvarResult + "<POLIZA>" + wvarPolizaNew + "</POLIZA><DESDE>" + wvarFDesde + "</DESDE><HASTA>" + wvarFHasta + "</HASTA><CODIGO>" + wvarRecibo + "</CODIGO><VENCIMIENTO>" + wvarFVto + "</VENCIMIENTO><IMPORTE>" + wvarImporte + "</IMPORTE>";
              wvarResult = wvarResult + "</RECIBO>";
            }
          }
          wvarResult = wvarResult + "</RECIBOS>";
          wvarResult = wvarResult + "<HAY_DATOS_POLIZA_RENOVACION>" + Strings.mid( strParseString, wvarPos + 145, 1 ) + "</HAY_DATOS_POLIZA_RENOVACION>";

          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + wvarResult + "</Response>" );
        }
        else if( Strings.toUpperCase( Strings.mid( strParseString, (wvarPos + 144), 1 ) ).equals( "S" ) )
        {
          //Tiene mas de dos CUPONES
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Su deuda excede lo permitido publicar. Si Ud. reside en Capital Federal o Gran Buenos Aires, por favor comun�quese al 4338-6950; si Usted reside en el Interior del pa�s, por favor comu�quese al 0-800-222-8575. Muchas gracias" + String.valueOf( (char)(34) ) + "/>" + wvarResult + "</Response>" );
        }
        else if( Strings.toUpperCase( Strings.mid( strParseString, (wvarPos + 144), 1 ) ).equals( "V" ) )
        {
          //La deuda excede los 65 dias de plazo, con lo cual no debe mostrarse.
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Su deuda excede lo permitido publicar. Si Ud. reside en Capital Federal o Gran Buenos Aires, por favor comun�quese al 4338-6950; si Usted reside en el Interior del pa�s, por favor comu�quese al 0-800-222-8575. Muchas gracias" + String.valueOf( (char)(34) ) + "/>" + wvarResult + "</Response>" );
        }
        else
        {
          Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + "/>" + wvarResult + "</Response>" );
        }
      }

      wvarStep = 170;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String FormatFecha( String fecha ) throws Exception
  {
    String FormatFecha = "";
    FormatFecha = Strings.mid( fecha, 1, 2 ) + "/" + Strings.mid( fecha, 3, 2 ) + "/" + Strings.mid( fecha, 5, 4 );
    return FormatFecha;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
