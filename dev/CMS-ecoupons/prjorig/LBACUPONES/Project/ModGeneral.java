import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 *  Parametros XML de Configuracion
 */

public class ModGeneral
{
  public static final String gcteConfFileName = "MQConfig.xml";
  public static final String gcteQueueManager = "//QUEUEMANAGER";
  public static final String gctePutQueue = "//PUTQUEUE";
  public static final String gcteGetQueue = "//GETQUEUE";
  public static final String gcteGMOWaitInterval = "//GMO_WAITINTERVAL";
  public static final String mcteDB = "lbawA_Ecupons.udl";
  public static final String mcteDBCV = "lbawA_EcuponsCV.udl";
  public static final String gcteClassMQConnection = "WD.Frame2MQ";
}
