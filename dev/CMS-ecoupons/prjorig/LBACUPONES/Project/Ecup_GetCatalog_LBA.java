import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class Ecup_GetCatalog_LBA implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.Ecup_GetCatalog_LBA";
  static final String mcteStoreProc = "SP_INDICES_CUPONES_FACTURA";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Valor_Clave = "//VALOR_CLAVE";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarValor_Clave = "";
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wvarValor_Clave = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Valor_Clave ) */ == (org.w3c.dom.Node) null) )
      {
        wvarValor_Clave = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Valor_Clave ) */ );
      }
      //
      wvarStep = 30;
      //
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDBCV)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 40;
      wobjDBParm = new Parameter( "@VALOR_CLAVE", AdoConst.adChar, AdoConst.adParamInput, 255, new Variant( wvarValor_Clave ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 50;
      wrstRes = new Recordset();
      wrstRes.open( wobjDBCmd, null, AdoConst.adOpenForwardOnly, AdoConst.adLockReadOnly );


      wrstRes.setActiveConnection( (Connection) null );
      //
      wvarStep = 60;
      //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
      if( ! (wrstRes.isEOF()) )
      {
        if( ! (wrstRes.getFields().getField(0).getValue().isNull()) )
        {
          pvarResponse.set( "<Response><Estado resultado=\"true\" mensaje=\"\"/>" + "<FEC_CATALOGO>" + wrstRes.getFields().getField(0).getValue() + "</FEC_CATALOGO>" + "</Response>" );
        }
        else
        {
          //error
          wvarStep = 65;
          pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"En este momento no existen cupones de pago para publicar\"/></Response>" );
        }
      }
      else
      {
        //error
        wvarStep = 70;
        pvarResponse.set( "<Response><Estado resultado=\"false\" mensaje=\"En este momento no existen cupones de pago para publicar\"/></Response>" );
      }
      //
      wvarStep = 290;
      //
      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
