import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * -----------------------------------------------------------------------------------------------------------------------------------
 *  COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
 *            CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
 * 
 * THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
 * IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
 * DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
 * TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
 * FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
 * OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
 * INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
 * OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
 * SUBSTANTIAL DAMAGES.
 * -------------------------------------------------------------------------------------------------------------------------------------
 *  Module Name : ModEncryptDecrypt
 *  File Name : ModEncryptDecrypt.bas
 *  Creation Date: 29/11/2005
 *  Programmer : Fernando Osores / Lucas De Merlier
 *  Abstract :    Encripta y Desencripta un string utilizando el secreto indicado.
 *  *****************************************************************
 *  Secreto. Debe ser fijo. No puede ser modificado.
 */

public class ModEncryptDecrypt
{
  public static final String mcteClave = "Rg-Sv+Fo$Dg!2006=CuponesLBAżnyl#HSBC";
  /**
   * static variable for method: CapicomEncrypt
   */
  private static final String wcteComponente = "embA_CVEncrypt.Process";

  /**
   *  *****************************************************************
   *  Function : CapicomEncrypt
   *  Abstract : Encripta un string.
   *  Synopsis : CapicomEncrypt(ByVal strText As String) As String
   *  *****************************************************************
   */
  public static String CapicomEncrypt( Object mobjCOM_Context, String strText ) throws Exception
  {
    String CapicomEncrypt = "";
    Object wobjClass = null;
    String wvarRequest = "";
    String wvarResponse = "";
    diamondedge.util.XmlDom wobjXMLResponse = null;



    wvarRequest = "<Request>";
    wvarRequest = wvarRequest + "<STRCLAVE>";
    wvarRequest = wvarRequest + strText;
    wvarRequest = wvarRequest + "</STRCLAVE>";
    //Devolver el resultado en HEXA
    wvarRequest = wvarRequest + "<HEX>1</HEX>";
    //wvarRequest = wvarRequest & "<HEX>0</HEX>" 'Devolver el resultado en DECIMAL
    wvarRequest = wvarRequest + "</Request>";

    wobjClass = new wcteComponente();
    //error: function 'Execute' was not found.
    //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    wobjClass = null;

    wobjXMLResponse = new diamondedge.util.XmlDom();
    if( wobjXMLResponse.loadXML( wvarResponse ) )
    {
      CapicomEncrypt = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLResponse.selectSingleNode( "//STRENCRYPT" ) */ );
    }
    else
    {
      CapicomEncrypt = "";
    }

    //On Error GoTo ErrorHandler
    //    Dim objCapicom As CAPICOM.EncryptedData
    //    Set objCapicom = New CAPICOM.EncryptedData
    //
    //    If strText <> "" Then
    //
    //        objCapicom.Algorithm = CAPICOM_ENCRYPTION_ALGORITHM_RC4
    //        objCapicom.SetSecret mcteClave
    //        objCapicom.Content = strText
    //        CapicomEncrypt = StrToHex(objCapicom.Encrypt())
    //
    //    Else
    //
    //        CapicomEncrypt = ""
    //
    //    End If
    //
    //    Set objCapicom = Nothing
    //
    //    Exit Function
    //ErrorHandler:
    //    Set objCapicom = Nothing
    //    CapicomEncrypt = Err.Description & ": " & strText
    return CapicomEncrypt;
  }

  /**
   *  *****************************************************************
   *  Function : CapicomDecrypt
   *  Abstract : Desencripta un string
   *  Synopsis : CapicomDecrypt(ByVal strText As String) As String
   *  *****************************************************************
   */
  public static String CapicomDecrypt( String strText ) throws Exception
  {
    String CapicomDecrypt = "";

    CapicomDecrypt = strText;

    //On Error GoTo ErrorHandler
    //    Dim objCapicom As CAPICOM.EncryptedData
    //    Set objCapicom = New CAPICOM.EncryptedData
    //
    //    If strText <> "" Then
    //
    //        objCapicom.Algorithm = CAPICOM_ENCRYPTION_ALGORITHM_RC4
    //        objCapicom.SetSecret mcteClave
    //        objCapicom.Decrypt (HexToStr(strText))
    //        CapicomDecrypt = objCapicom.Content
    //
    //    Else
    //
    //        CapicomDecrypt = ""
    //
    //    End If
    //
    //    Set objCapicom = Nothing
    //    Exit Function
    //ErrorHandler:
    //    Set objCapicom = Nothing
    //    CapicomDecrypt = Err.Description & ": " & strText
    return CapicomDecrypt;
  }

  public static String HexToStr( String strHex ) throws Exception
  {
    String HexToStr = "";
    String strAux = "";
    int i = 0;


    strAux = "";

    for( i = 1; i <= Strings.len( strHex ); i += 2 )
    {
      strAux = strAux + String.valueOf( (char)(Obj.toInt( "&H" + Strings.mid( strHex, i, 2 ) )) );
    }

    HexToStr = strAux;

    return HexToStr;
  }

  public static String StrToHex( String str ) throws Exception
  {
    String StrToHex = "";
    String strAux = "";
    int i = 0;


    strAux = "";

    for( i = 1; i <= Strings.len( str ); i++ )
    {
      if( Strings.len( Integer.toHexString( Strings.asc( Strings.mid( str, i, 1 ) ) ) ) == 1 )
      {
        strAux = strAux + "0";
      }
      strAux = strAux + Integer.toHexString( Strings.asc( Strings.mid( str, i, 1 ) ) );
    }
    StrToHex = strAux;

    return StrToHex;
  }
}
