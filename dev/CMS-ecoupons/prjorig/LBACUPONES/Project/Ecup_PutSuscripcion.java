import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class Ecup_PutSuscripcion implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.Ecup_PutSuscripcion";
  static final String mcteOpID = "0009";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_CLIENSEC = "//CLIENSEC";
  static final String mcteParam_Clave = "//CLAVE";
  static final String mcteParam_EsHexa = "//HEX";
  static final String mcteParam_Email = "//EMAIL";
  static final String mcteParam_Poliza = "//POLIZA";
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_OPERACION = "//OPERACION";
  static final String mcteParam_INFOADICIONAL = "//INFOADICIONAL";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    String wvarMensaje = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarClave = "";
    String wvarEsHexa = "";
    String wvarPoliza = "";
    String wvarEmail = "";
    String wvarRamopcod = "";
    String wvarOperacion = "";
    String wvarInfoAdicional = "";
    String wvarCliensec = "";
    int wvarPos = 0;
    String strParseString = "";
    boolean wvarControls = false;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarEmail = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Email ) */ );
      wvarCliensec = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CLIENSEC ) */ );
      wvarClave = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Clave ) */ );
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EsHexa ) */ == (org.w3c.dom.Node) null )
      {
        wvarEsHexa = "0";
      }
      else
      {
        wvarEsHexa = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EsHexa ) */ );
      }
      wvarRamopcod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ );
      wvarOperacion = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_OPERACION ) */ );
      wvarPoliza = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ );
      wvarInfoAdicional = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_INFOADICIONAL ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //quiere decir que tengo la cadena HEXA en la variable!
      if( String.valueOf( wvarEsHexa ).equals( "0" ) )
      {
        wvarClave = ModEncryptDecrypt.CapicomEncrypt( mobjCOM_Context, wvarClave );
      }

      wvarStep = 111;
      if( Strings.len( wvarClave ) < 264 )
      {
        wvarClave = wvarClave + Strings.space( (264 - Strings.len( wvarClave )) );
      }
      if( Strings.len( wvarEmail ) < 60 )
      {
        wvarEmail = wvarEmail + Strings.space( (60 - Strings.len( wvarEmail )) );
      }
      if( Strings.len( wvarEmail ) > 60 )
      {
        wvarEmail = Strings.left( wvarEmail, 60 );
      }

      wvarStep = 115;
      if( Strings.toUpperCase( wvarInfoAdicional ).equals( "SI" ) )
      {
        wvarInfoAdicional = "S";
      }
      else
      {
        wvarInfoAdicional = "N";
      }

      wvarStep = 116;
      wvarArea = mcteOpID + wvarCliensec + wvarRamopcod + wvarPoliza + wvarEmail + wvarClave + wvarOperacion + wvarInfoAdicional;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = Strings.len( wvarCliensec + wvarRamopcod + wvarPoliza + wvarEmail + wvarClave + wvarOperacion + wvarInfoAdicional );

      //Respuesta.
      //Status                  CAR�CTER  01 (0=OK/1=NOK)
      
      if( Obj.toInt( Strings.mid( strParseString, (wvarPos + 1), 1 ) ) == 0 )
      {
        //Operaci�n Exitosa.
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + "/>" + Strings.replace( Request, "Request", "PARAMETROS" ) + "</Response>" );
      }
      else if( Obj.toInt( Strings.mid( strParseString, (wvarPos + 1), 1 ) ) == 1 )
      {
        //Operaci�n No Realizada
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "No se ha podido realizar la operaci�n. Por favor intente nuevamente m�s tarde. Si el problema persiste comun�quese telef�nicamente con el Call Center." + String.valueOf( (char)(34) ) + " />" + Strings.replace( Request, "Request", "PARAMETROS" ) + "</Response>" );
      }
      //
      wvarStep = 170;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
