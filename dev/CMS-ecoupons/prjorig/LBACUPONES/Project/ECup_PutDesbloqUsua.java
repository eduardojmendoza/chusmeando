import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class ECup_PutDesbloqUsua implements Variant, ObjectControl, HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_EcuponsMQ.ECup_PutDesbloqUsua";
  static final String mcteStoreProc = "SPSNCV_ECOUPON_DESBLOQUEAR_USUARIO";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_TipoDocu = "//TIPODOCU";
  static final String mcteParam_NumeDocu = "//NUMEDOCU";
  static final String mcteParam_FechaNac = "//FECHANAC";
  static final String mcteParam_Empresa = "//EMPRESA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String pvarRequest, Variant pvarResponse, String pvarContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    int wvarStep = 0;
    String wvarTipoDocu = "";
    String wvarNumeDocu = "";
    String wvarFechaNac = "";
    String wvarEmpresa = "";
    diamondedge.util.XmlDom wobjXMLRequest = null;
    Object wobjHSBC_DBCnn = null;
    Connection wobjDBCnn = null;
    Command wobjDBCmd = null;
    Parameter wobjDBParm = null;
    Recordset wrstRes = null;
    //
    //
    //Declaracion de variables
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( pvarRequest );
      //
      wvarStep = 20;
      wvarTipoDocu = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TipoDocu ) */ == (org.w3c.dom.Node) null) )
      {
        wvarTipoDocu = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TipoDocu ) */ );
      }
      //
      wvarStep = 30;
      wvarNumeDocu = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NumeDocu ) */ == (org.w3c.dom.Node) null) )
      {
        wvarNumeDocu = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NumeDocu ) */ );
      }
      //
      wvarStep = 40;
      wvarFechaNac = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FechaNac ) */ == (org.w3c.dom.Node) null) )
      {
        //Armo la fecha en el formato yyyymmdd
        wvarFechaNac = Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FechaNac ) */ ), 5, 4 ) + "-" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FechaNac ) */ ), 3, 2 ) + "-" + Strings.mid( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FechaNac ) */ ), 1, 2 );
      }
      //
      wvarStep = 50;
      wvarEmpresa = "";
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Empresa ) */ == (org.w3c.dom.Node) null) )
      {
        wvarEmpresa = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Empresa ) */ ) );
      }
      //
      wvarStep = 55;
      //
      wobjHSBC_DBCnn = new HSBC.DBConnection();
      //error: function 'GetDBConnection' was not found.
      //unsup: Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
      wobjDBCmd = new Command();
      wobjDBCmd.setActiveConnection( wobjDBCnn );
      wobjDBCmd.setCommandText( mcteStoreProc );
      wobjDBCmd.setCommandType( AdoConst.adCmdStoredProc );
      //
      wvarStep = 60;
      wobjDBParm = new Parameter( "@TIPODOC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarTipoDocu ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 2 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 70;

      wobjDBParm = new Parameter( "@NRODOC", AdoConst.adNumeric, AdoConst.adParamInput, 0, new Variant( wvarNumeDocu ) );
      wobjDBParm.setScale( (byte)( 0 ) );
      wobjDBParm.setPrecision( (byte)( 11 ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 80;
      wobjDBParm = new Parameter( "@FECHANAC", AdoConst.adDate, AdoConst.adParamInput, 0, new Variant( wvarFechaNac ) );
      wobjDBCmd.getParameters().append( wobjDBParm );
      wobjDBParm = (Parameter) null;
      //
      wvarStep = 85;
      if( !wvarEmpresa.equals( "" ) )
      {
        wobjDBParm = new Parameter( "@EMPRESA", AdoConst.adBSTR, AdoConst.adParamInput, 3, new Variant( wvarEmpresa ) );
        wobjDBCmd.getParameters().append( wobjDBParm );
        wobjDBParm = (Parameter) null;
      }
      //
      wvarStep = 90;

      wrstRes = wobjDBCmd.execute();
      wrstRes.setActiveConnection( (Connection) null );

      wvarStep = 100;
      //SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
      if( ! (wrstRes.isEOF()) )
      {
        if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "0" ) )
        {
          wvarStep = 110;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio ha sido reestablecido satisfactoriamente" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "1" ) )
        {
          //error
          wvarStep = 120;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "Los datos ingresados no concuerdan con los datos registrados en la compa��a.  Ante cualquier duda  comun�quese con el centro de Atenci�n al Cliente al 0-810-999-2424. Muchas Gracias" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else if( Strings.toUpperCase( Strings.trim( wrstRes.getFields().getField(0).getValue().toString() ) ).equals( "2" ) )
        {
          //error
          wvarStep = 120;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio se encuentra habilitado" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
        else
        {
          wvarStep = 130;
          pvarResponse.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " /></Response>" );
        }
      }
      else
      {
        //error
      }
      //
      wvarStep = 290;
      //
      wrstRes.close();
      wobjDBCnn.close();
      fin: 
      //libero los objectos
      wrstRes = (Recordset) null;
      wobjDBCmd = (Command) null;
      wobjDBCnn = (Connection) null;
      wobjHSBC_DBCnn = null;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      return IAction_Execute;

      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription(), vbLogEventTypeError );

        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup GoTo fin
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
