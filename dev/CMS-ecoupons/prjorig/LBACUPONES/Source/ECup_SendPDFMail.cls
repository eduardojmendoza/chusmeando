VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "ECup_SendPDFMail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_EcuponsMQ.ECup_SendPDFMail"

'Parametros XML de Entrada
Const mcteParam_CLAVENOENCRIPTADA   As String = "//CLAVENOENCRIPTADA"
Const mcteParam_PDFUSRPWD           As String = "//PDFUSRPWD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wvarStep            As Long
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    Dim wobjClass           As HSBCInterfaces.IAction
    Const wcteComponente    As String = "embA_CVEncrypt.SendMail" ' Actioncode="Emb_CVSendMail"
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        If CInt(.selectSingleNode(mcteParam_CLAVENOENCRIPTADA).Text) = 1 Then
            .selectSingleNode(mcteParam_PDFUSRPWD).Text = CapicomEncrypt(mobjCOM_Context, .selectSingleNode(mcteParam_PDFUSRPWD).Text)
        End If

        Set wobjClass = mobjCOM_Context.CreateInstance(wcteComponente)
        Call wobjClass.Execute(.xml, Response, "")
        Set wobjClass = Nothing
    
    End With

    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & Request & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
