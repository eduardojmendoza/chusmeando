VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Ecup_GetPolHOM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_EcuponsMQ.ECup_GetPolHOM"
Const mcteOpID              As String = "0003"
'MENSAJE 0003 (HOGAR).
'
'Entrada.
'�   Numero de cliente       NUMERICO  09
'�   C�digo de producto      CAR�CTER  04
'�   Numero de certificado   NUMERICO  10
'
'Respuesta.
'�   Status                  CAR�CTER  01 (0=OK/1=NOK/2=Canal cobro no efectivo)
'�   P�liza ingresada        NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
'�   Canal de cobro          NUMERICO  04 ( 9999 - la p�liza esta anulada )
'�   Tipo de vivienda        CAR�CTER  40
'�   Domicilio del Riesgo    CAR�CTER  130 (CALLE/NUMERO/ESCALERA/PISO/PUERTA/LOCALIDAD)
'�   Direcci�n e-mail        CAR�CTER  60 ( si la p�liza envia cupones por e-mail )
'�   Clave de encriptamiento CAR�CTER 264 ( si la p�liza envia cupones por e-mail )
'�   Marca de propaganda   CAR�CTER  01 (S=acepta recibir propaganda por mail
' N=no acepta recibir )
'�   P�liza renovaci�n       NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
'�   Marca renovaci�n        NUMERICO  01 (0=no renovada/1=Posterior/2=Anterior)


'Parametros XML de Entrada
Const mcteParam_CLIENSEC    As String = "//CLIENSEC"
Const mcteParam_TipoSeg    As String = "//TIPOSEG"
Const mcteParam_Poliza    As String = "//POLIZA"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument

    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarTipoSeg         As String
    Dim wvarPoliza          As String
    Dim wvarCliensec        As String
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarControls        As Boolean
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCliensec = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarTipoSeg = .selectSingleNode(mcteParam_TipoSeg).Text
        wvarPoliza = .selectSingleNode(mcteParam_Poliza).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarArea = mcteOpID & wvarCliensec & wvarTipoSeg & wvarPoliza
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
        wvarStep = 160
        wvarResult = ""
        'wvarPos = 20
        wvarPos = 24
      
        Select Case Mid(strParseString, wvarPos, 1)
            Case 0
            'POLIZA OK
            'Chequeo si el Canal de Cobro = 9999 es Poliza Anulada
                If Mid(strParseString, wvarPos + 27, 4) = "9999" Then
                    Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "La p�liza que ha ingresado es inexistente" & Chr(34) & "/>" & Replace(Request, "Request", "PARAMETROS") & wvarResult & "</Response>"
                Else
                'ARMA LA RESPUESTA
                    wvarResult = "<POLIZANN>" & Mid(strParseString, wvarPos + 1, 2) & "</POLIZANN>"
                    wvarResult = wvarResult & "<POLIZSEC>" & Mid(strParseString, wvarPos + 3, 6) & "</POLIZSEC>"
                    wvarResult = wvarResult & "<CERTIPOL>" & Mid(strParseString, wvarPos + 9, 4) & "</CERTIPOL>"
                    wvarResult = wvarResult & "<CERTIANN>" & Mid(strParseString, wvarPos + 13, 4) & "</CERTIANN>"
                    wvarResult = wvarResult & "<CERTISEC>" & Mid(strParseString, wvarPos + 17, 6) & "</CERTISEC>"
                    wvarResult = wvarResult & "<SUPLENUM>" & Mid(strParseString, wvarPos + 23, 4) & "</SUPLENUM>"
                    wvarResult = wvarResult & "<CCOBRO>" & Mid(strParseString, wvarPos + 27, 4) & "</CCOBRO>"
                    wvarResult = wvarResult & "<BIEN><![CDATA[" & CaracteresAsciiValidos(Mid(strParseString, wvarPos + 31, 40)) & "]]></BIEN>"
                    'wvarResult = wvarResult & "<DIRBIEN>" & Mid(strParseString, wvarPos + 71, 82) & "</DIRBIEN>"
                    wvarResult = wvarResult & "<DIRBIEN><![CDATA[" & CaracteresAsciiValidos(Mid(strParseString, wvarPos + 71, 130)) & "]]></DIRBIEN>"
                    
                    If Trim(Mid(strParseString, wvarPos + 261, 264)) = "" Then
                        wvarResult = wvarResult & "<SUSCRIPTO>No</SUSCRIPTO>"
                    Else
                        wvarResult = wvarResult & "<SUSCRIPTO>Si</SUSCRIPTO>"
                        wvarResult = wvarResult & "<EMAIL>" & Trim(Mid(strParseString, wvarPos + 201, 60)) & "</EMAIL>"
                        wvarResult = wvarResult & "<CLAVE>" & CapicomDecrypt(Trim(Mid(strParseString, wvarPos + 261, 264))) & "</CLAVE>"
                        If UCase(Trim(Mid(strParseString, wvarPos + 525, 1))) = "S" Then
                            wvarResult = wvarResult & "<INFOADICIONAL>SI</INFOADICIONAL>"
                        Else
                            wvarResult = wvarResult & "<INFOADICIONAL>NO</INFOADICIONAL>"
                        End If
                    End If
                    
                    wvarResult = wvarResult & "<POLIZA_RENOVACION>" & Mid(strParseString, wvarPos + 526, 4) + "-" + Mid(strParseString, wvarPos + 526 + 4, 2) + "-" + Mid(strParseString, wvarPos + 526 + 6, 6) + "/" + Mid(strParseString, wvarPos + 526 + 12, 4) + "-" + Mid(strParseString, wvarPos + 526 + 16, 4) + "-" + Mid(strParseString, wvarPos + 526 + 20, 6) & "</POLIZA_RENOVACION>"
                    
                    If CInt(RTrim(Mid(strParseString, wvarPos + 552, 1))) = 1 Then
                        wvarResult = wvarResult & "<MARCA_RENOVACION desc='Poliza Posterior'>" & RTrim(Mid(strParseString, wvarPos + 552, 1)) & "</MARCA_RENOVACION>"
                    ElseIf CInt(RTrim(Mid(strParseString, wvarPos + 552, 1))) = 2 Then
                        wvarResult = wvarResult & "<MARCA_RENOVACION desc='Poliza Anterior'>" & RTrim(Mid(strParseString, wvarPos + 552, 1)) & "</MARCA_RENOVACION>"
                    Else
                        wvarResult = wvarResult & "<MARCA_RENOVACION desc='Poliza No Renovada'>" & RTrim(Mid(strParseString, wvarPos + 552, 1)) & "</MARCA_RENOVACION>"
                    End If
                    
                    
                    Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & Replace(Request, "Request", "PARAMETROS") & wvarResult & "</Response>"
                End If
            Case 1
            'NO EXISTE LA POLIZA
                'Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Los datos ingresados no concuerdan con los datos registrados en la compa��a.  Ante cualquier duda  comun�quese con el centro de Atenci�n al Cliente al 0-810-999-2424. Muchas Gracias" & Chr(34) & " />" & Replace(Request, "Request", "PARAMETROS") & "</Response>"
                Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "La p�liza que ha ingresado es inexistente." & Chr(34) & " />" & Replace(Request, "Request", "PARAMETROS") & "</Response>"
            Case 2
            'LA FORMA DE PAGO ES DISTINTA DE LA PERMITIDA
                Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El pago de su seguro est� adherido a d�bito autom�tico. Ante cualquier duda  comun�quese con el centro de Atenci�n al Cliente al 0-810-999-2424. Muchas Gracias" & Chr(34) & " />" & Replace(Request, "Request", "PARAMETROS") & "</Response>"
        End Select
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



