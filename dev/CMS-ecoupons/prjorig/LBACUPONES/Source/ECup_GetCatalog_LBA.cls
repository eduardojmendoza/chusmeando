VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Ecup_GetCatalog_LBA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_EcuponsMQ.Ecup_GetCatalog_LBA"
Const mcteStoreProc             As String = "SP_INDICES_CUPONES_FACTURA"

'Parametros XML de Entrada
Const mcteParam_Valor_Clave    As String = "//VALOR_CLAVE"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    
    'Declaracion de variables
    Dim wvarValor_Clave         As String
    Dim wobjXMLRequest          As MSXML2.DOMDocument
    
    Dim wobjHSBC_DBCnn        As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn             As ADODB.Connection
    Dim wobjDBCmd             As ADODB.Command
    Dim wobjDBParm            As ADODB.Parameter
    Dim wrstRes               As ADODB.Recordset

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(pvarRequest)
    '
    wvarStep = 20
    wvarValor_Clave = ""
    If Not wobjXMLRequest.selectSingleNode(mcteParam_Valor_Clave) Is Nothing Then
         wvarValor_Clave = wobjXMLRequest.selectSingleNode(mcteParam_Valor_Clave).Text
    End If
    '
    wvarStep = 30
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDBCV)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = wobjDBCmd.CreateParameter("@VALOR_CLAVE", adChar, adParamInput, 255, wvarValor_Clave)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 50
    Set wrstRes = CreateObject("ADODB.Recordset")
    wrstRes.Open wobjDBCmd
    
    
    Set wrstRes.ActiveConnection = Nothing
    '
   
    wvarStep = 60
    'SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
    If Not wrstRes.EOF Then
        If Not IsNull(wrstRes.Fields(0).Value) Then
            pvarResponse = "<Response><Estado resultado=""true"" mensaje=""""/>" & _
                     "<FEC_CATALOGO>" & wrstRes.Fields(0).Value & "</FEC_CATALOGO>" & _
                     "</Response>"
        Else
            'error
            wvarStep = 65
            pvarResponse = "<Response><Estado resultado=""false"" mensaje=""En este momento no existen cupones de pago para publicar""/></Response>"
        End If
    Else
       'error
       wvarStep = 70
       pvarResponse = "<Response><Estado resultado=""false"" mensaje=""En este momento no existen cupones de pago para publicar""/></Response>"
    End If
    '
    wvarStep = 290
    '
    wrstRes.Close
    wobjDBCnn.Close
fin:
    'libero los objectos
    Set wrstRes = Nothing
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

  
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  mobjCOM_Context.SetComplete
  GoTo fin
End Function



'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

