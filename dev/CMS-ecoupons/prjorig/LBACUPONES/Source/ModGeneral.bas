Attribute VB_Name = "ModGeneral"
Option Explicit

' Parametros XML de Configuracion
Public Const gcteConfFileName       As String = "MQConfig.xml"
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"

Public Const mcteDB                 As String = "lbawA_Ecupons.udl"
Public Const mcteDBCV               As String = "lbawA_EcuponsCV.udl"

Public Const gcteClassMQConnection As String = "WD.Frame2MQ"
