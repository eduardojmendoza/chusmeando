VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Ecup_GetClientStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_EcuponsMQ.Ecup_GetClientStatus"
Const mcteStoreProc         As String = "SPSNCV_ECOUPON_ESTADO_CLIENTE"

'Parametros XML de Entrada
Const mcteParam_TipoDocu    As String = "//TIPODOCU"
Const mcteParam_NumeDocu    As String = "//NUMEDOCU"
Const mcteParam_FechaNac    As String = "//FECHANAC"
Const mcteParam_Empresa     As String = "//EMPRESA"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    
    'Declaracion de variables
    Dim wvarTipoDocu            As String
    Dim wvarNumeDocu            As String
    Dim wvarFechaNac            As String
    Dim wvarEmpresa             As String
    Dim wobjXMLRequest          As MSXML2.DOMDocument
    
    Dim wobjHSBC_DBCnn        As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn             As ADODB.Connection
    Dim wobjDBCmd             As ADODB.Command
    Dim wobjDBParm            As ADODB.Parameter
    Dim wrstRes               As ADODB.Recordset

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(pvarRequest)
    '
    wvarStep = 20
    wvarTipoDocu = ""
    If Not wobjXMLRequest.selectSingleNode(mcteParam_TipoDocu) Is Nothing Then
         wvarTipoDocu = wobjXMLRequest.selectSingleNode(mcteParam_TipoDocu).Text
    End If
    '
    wvarStep = 30
    wvarNumeDocu = ""
    If Not wobjXMLRequest.selectSingleNode(mcteParam_NumeDocu) Is Nothing Then
         wvarNumeDocu = wobjXMLRequest.selectSingleNode(mcteParam_NumeDocu).Text
    End If
    '
    wvarStep = 40
    wvarFechaNac = ""
    If Not wobjXMLRequest.selectSingleNode(mcteParam_FechaNac) Is Nothing Then
         'Armo la fecha en el formato yyyymmdd
         wvarFechaNac = Mid(wobjXMLRequest.selectSingleNode(mcteParam_FechaNac).Text, 5, 4) & "-" & _
                        Mid(wobjXMLRequest.selectSingleNode(mcteParam_FechaNac).Text, 3, 2) & "-" & _
                        Mid(wobjXMLRequest.selectSingleNode(mcteParam_FechaNac).Text, 1, 2)
    End If
    '
    wvarStep = 50
    '
    
    wvarEmpresa = ""
    If Not wobjXMLRequest.selectSingleNode(mcteParam_Empresa) Is Nothing Then
         wvarEmpresa = Trim(wobjXMLRequest.selectSingleNode(mcteParam_Empresa).Text)
    End If
    '
    wvarStep = 55
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPODOC", adNumeric, adParamInput, , wvarTipoDocu)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 2
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 70
    '
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NRODOC", adNumeric, adParamInput, , wvarNumeDocu)
    wobjDBParm.NumericScale = 0
    wobjDBParm.Precision = 11
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FECHANAC", adDate, adParamInput, , wvarFechaNac)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 85
    If wvarEmpresa <> "" Then
        Set wobjDBParm = wobjDBCmd.CreateParameter("@EMPRESA", adBSTR, adParamInput, 3, wvarEmpresa)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    End If
    '
    wvarStep = 90
    'Set wrstRes = CreateObject("ADODB.Recordset")
    
    Set wrstRes = wobjDBCmd.Execute
    Set wrstRes.ActiveConnection = Nothing
      
    wvarStep = 100
    'SI NO HUBO ERROR AGREGO LOS DATOS AL XML DE ENTRADA PARA LA RESPUESTA
    If Not wrstRes.EOF Then
      If UCase(Trim(wrstRes.Fields(0).Value)) = "D" Then
        'desbloqueado
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & "El servicio se encuentra habilitado" & Chr(34) & " /></Response>"
        wvarStep = 275
      Else
        'bloqueado
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Por razones de Seguridad el servicio ha sido deshabilitado" & Chr(34) & " /></Response>"
        wvarStep = 280
        '
        Exit Function
      End If
    Else
      'error
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " /></Response>"
        wvarStep = 285
    End If
    
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
    wvarStep = 290
    '
    wrstRes.Close
    wobjDBCnn.Close
fin:
    'libero los objectos
    Set wrstRes = Nothing
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

  
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  mobjCOM_Context.SetComplete
  GoTo fin
End Function



'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



