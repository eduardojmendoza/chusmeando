VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "Ecup_PutSuscripcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_EcuponsMQ.Ecup_PutSuscripcion"
Const mcteOpID              As String = "0009"

'Parametros XML de Entrada
Const mcteParam_CLIENSEC  As String = "//CLIENSEC"
Const mcteParam_Clave     As String = "//CLAVE"
Const mcteParam_EsHexa    As String = "//HEX"
Const mcteParam_Email     As String = "//EMAIL"
Const mcteParam_Poliza    As String = "//POLIZA"
Const mcteParam_RAMOPCOD  As String = "//RAMOPCOD"
Const mcteParam_OPERACION As String = "//OPERACION"
Const mcteParam_INFOADICIONAL As String = "//INFOADICIONAL"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarClave           As String
    Dim wvarEsHexa          As String
    Dim wvarPoliza          As String
    Dim wvarEmail           As String
    Dim wvarRamopcod        As String
    Dim wvarOperacion       As String
    Dim wvarInfoAdicional   As String
    Dim wvarCliensec        As String
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarControls        As Boolean
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarEmail = .selectSingleNode(mcteParam_Email).Text
        wvarCliensec = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarClave = .selectSingleNode(mcteParam_Clave).Text
        If .selectSingleNode(mcteParam_EsHexa) Is Nothing Then
            wvarEsHexa = 0
        Else
            wvarEsHexa = .selectSingleNode(mcteParam_EsHexa).Text
        End If
        wvarRamopcod = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarOperacion = .selectSingleNode(mcteParam_OPERACION).Text
        wvarPoliza = .selectSingleNode(mcteParam_Poliza).Text
        wvarInfoAdicional = .selectSingleNode(mcteParam_INFOADICIONAL).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    If CStr(wvarEsHexa) = "0" Then 'quiere decir que tengo la cadena HEXA en la variable!
        wvarClave = CapicomEncrypt(mobjCOM_Context, wvarClave)
    End If
    
    wvarStep = 111
    If Len(wvarClave) < 264 Then
        wvarClave = wvarClave + Space(264 - Len(wvarClave))
    End If
    If Len(wvarEmail) < 60 Then
        wvarEmail = wvarEmail + Space(60 - Len(wvarEmail))
    End If
    If Len(wvarEmail) > 60 Then
        wvarEmail = Left(wvarEmail, 60)
    End If
    
    wvarStep = 115
    If UCase(wvarInfoAdicional) = "SI" Then
        wvarInfoAdicional = "S"
    Else
        wvarInfoAdicional = "N"
    End If
    
    wvarStep = 116
    wvarArea = mcteOpID & wvarCliensec & wvarRamopcod & wvarPoliza & wvarEmail & wvarClave & wvarOperacion & wvarInfoAdicional
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
  wvarStep = 160
  wvarResult = ""
  wvarPos = Len(wvarCliensec & wvarRamopcod & wvarPoliza & wvarEmail & wvarClave & wvarOperacion & wvarInfoAdicional)

  'Respuesta.
  'Status                  CAR�CTER  01 (0=OK/1=NOK)

  Select Case Mid(strParseString, wvarPos + 1, 1)
      Case 0
      'Operaci�n Exitosa.
          Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & Replace(Request, "Request", "PARAMETROS") & "</Response>"
      Case 1
      'Operaci�n No Realizada
          Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "No se ha podido realizar la operaci�n. Por favor intente nuevamente m�s tarde. Si el problema persiste comun�quese telef�nicamente con el Call Center." & Chr(34) & " />" & Replace(Request, "Request", "PARAMETROS") & "</Response>"
  End Select
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


