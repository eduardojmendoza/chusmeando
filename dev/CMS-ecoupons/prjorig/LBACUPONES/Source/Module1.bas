Attribute VB_Name = "Module1"
Option Explicit

' Parametros XML de Configuracion
Public Const gcteConfFileName       As String = "MQConfig.xml"
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"
