VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "ECup_GetUnlockAUS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_EcuponsMQ.ECup_GetUnlockAUS"
Const mcteOpID              As String = "0006"

'Parametros XML de Entrada
Const mcteParam_TipoDoc     As String = "//TIPODOC"
Const mcteParam_Doc         As String = "//DOCUMENTO"
Const mcteParam_FechaNac    As String = "//FECHANAC"
Const mcteParam_TipoSeg     As String = "//TIPOSEG"
Const mcteParam_Patente     As String = "//PATENTE"
Const mcteParam_Anio        As String = "//ANIO"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument

    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As Long
    Dim wvarResult          As String
    
    Dim wvarTipoDoc         As String
    Dim wvarDoc             As String
    Dim wvarFechaNac        As String
    Dim wvarTipoSeg         As String
    Dim wvarPatente         As String
    Dim wvarAnio            As String
    
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarControls        As Boolean
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarTipoSeg = .selectSingleNode(mcteParam_TipoSeg).Text
        wvarTipoDoc = .selectSingleNode(mcteParam_TipoDoc).Text
        wvarDoc = Left(.selectSingleNode(mcteParam_Doc).Text & String(11, " "), 11)
        wvarFechaNac = .selectSingleNode(mcteParam_FechaNac).Text
        wvarPatente = .selectSingleNode(mcteParam_Patente).Text
        wvarAnio = .selectSingleNode(mcteParam_Anio).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarArea = mcteOpID & wvarTipoDoc & wvarDoc & wvarFechaNac & wvarTipoSeg & wvarPatente & wvarAnio
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 160
    wvarResult = ""
    wvarPos = 36
    
    If Mid(strParseString, wvarPos, 1) = 0 Then
        'DATOS OK
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje="""" /></Response>"
    Else
        'NO COINCIDE LA INFO
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Los datos ingresados no concuerdan con los datos registrados en la compa��a.  Ante cualquier duda  comun�quese con el centro de Atenci�n al Cliente al 0-810-999-2424. Muchas Gracias" & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub



