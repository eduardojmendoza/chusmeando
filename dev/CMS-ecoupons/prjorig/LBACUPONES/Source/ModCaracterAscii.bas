Attribute VB_Name = "ModCaracterAscii"
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : ModCaracterAscii
' File Name : ModCaracterAscii.bas
' Creation Date: 17/10/2006
' Programmer : Fernando Osores
' Abstract :    Dado un STRING devuelve un STRING que tiene únicamente caracteres ascii válidos. Mayor o igual a 32.
' *****************************************************************
Option Explicit

' *****************************************************************
' Function : CaracteresAsciiValidos
' Abstract : EDado un STRING devuelve un STRING que tiene únicamente caracteres ascii válidos. Mayor o igual a 32.
' Synopsis : CaracteresAsciiValidos(str As String) As String
' *****************************************************************
Public Function CaracteresAsciiValidos(str As String) As String

    Dim strAux As String
    Dim i As Integer
    
    strAux = ""
    
    For i = 1 To Len(str)
        If Asc(Mid(str, i, 1)) >= 32 Then
            strAux = strAux + Mid(str, i, 1)
        Else
            strAux = strAux + " "
        End If
    Next
    CaracteresAsciiValidos = strAux

End Function

