VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Ecup_GetCupImp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_EcuponsMQ.ECup_GetCupImp"
Const mcteOpID              As String = "0004"
'MENSAJE 4#
'
'Entrada.
'�   Numero de cliente       NUMERICO  09
'�   C�digo de producto      CAR�CTER  04
'�   P�liza completa         NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
'
'Respuesta.
'�   Numero de recibo1       NUMERICO  09
'�   Fecha inicio vigencia1  NUMERICO  08 (ddmmaaaa)
'�   Fecha fin vigencia1     NUMERICO  08 (ddmmaaaa)
'�   Fecha vencimiento1      NUMERICO  08 (ddmmaaaa)
'�   Importe1                NUMERICO  13 2
'�   P�liza completa1        NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
'�   Numero de recibo2       NUMERICO  09
'�   Fecha inicio vigencia2  NUMERICO  08 (ddmmaaaa)
'�   Fecha fin vigencia2     NUMERICO  08 (ddmmaaaa)
'�   Fecha vencimiento2      NUMERICO  08 (ddmmaaaa)
'�   Importe2                NUMERICO  13 2
'�   P�liza completa2        NUMERICO  26 (8=P�liza/14=Certificado/4=Endoso)
'�   Hay mas recibos         CARACTER  01   ( Valores: S / N / V )
'�   Hay Deuda P�liza Renov. CARACTER  01   ( Valores: S / N )

'Parametros XML de Entrada
Const mcteParam_CLIENSEC   As String = "//CLIENSEC"
Const mcteParam_TipoSeg    As String = "//TIPOSEG"
Const mcteParam_Poliza     As String = "//POLIZA"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument

    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    Dim wvarTipoSeg         As String
    Dim wvarPoliza          As String
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarControls        As Boolean
    Dim wvarCount           As Integer
    Dim wvarFDesde          As String
    Dim wvarFHasta          As String
    Dim wvarFVto            As String
    Dim wvarRecibo          As String
    Dim wvarImporte         As String
    Dim wvarPolizaNew       As String
    
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    '
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCliensec = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarTipoSeg = .selectSingleNode(mcteParam_TipoSeg).Text
        wvarPoliza = .selectSingleNode(mcteParam_Poliza).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarArea = mcteOpID & wvarCliensec & wvarTipoSeg & wvarPoliza
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarStep = 160
    wvarResult = ""
    wvarPos = 40
    wvarControls = True
    
    Debug.Print Trim(strParseString)
    
    'Eval�a que la p�liza no tenga recibos disponibles
    If Mid(strParseString, wvarPos, 64) = "0000000000000000000000000000000000000000000000000000000000000000" Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "No existen cupones para publicar" & Chr(34) & " /></Response>"
        wvarControls = False
    End If
    
    If wvarControls Then
        'ARMA LA RESPUESTA
        'Chequeo si tiene mas de 2 cupones
        Select Case UCase(Mid(strParseString, wvarPos + 144, 1))
        Case "N" 'No tiene mas de dos CUPONES
            'Parseo de los recibos
            wvarResult = wvarResult & "<RECIBOS>"
            For wvarCount = (wvarPos) To (wvarPos + 72) Step 72
                wvarRecibo = Mid(strParseString, wvarCount, 9)
                wvarFDesde = FormatFecha(Mid(strParseString, wvarCount + 9, 8))
                wvarFHasta = FormatFecha(Mid(strParseString, wvarCount + 17, 8))
                wvarFVto = FormatFecha(Mid(strParseString, wvarCount + 25, 8))
                wvarImporte = Format(CStr(CLng(Mid(strParseString, wvarCount + 33, 13)) / 100), "0.00")
                wvarPolizaNew = Mid(strParseString, wvarCount + 46, 4) + "-" + Mid(strParseString, wvarCount + 46 + 4, 2) + "-" + Mid(strParseString, wvarCount + 46 + 6, 6) + "/" + Mid(strParseString, wvarCount + 46 + 12, 4) + "-" + Mid(strParseString, wvarCount + 46 + 16, 4) + "-" + Mid(strParseString, wvarCount + 46 + 20, 6)
                'Elimina recibos que vienen en 0
                If (wvarFDesde <> "00/00/0000" And wvarFHasta <> "00/00/0000") Then
                    wvarResult = wvarResult & "<RECIBO>"
                    wvarResult = wvarResult & "<POLIZA>" & wvarPolizaNew & "</POLIZA><DESDE>" & wvarFDesde & "</DESDE><HASTA>" & wvarFHasta & "</HASTA><CODIGO>" & wvarRecibo & "</CODIGO><VENCIMIENTO>" & wvarFVto & "</VENCIMIENTO><IMPORTE>" & wvarImporte & "</IMPORTE>"
                    wvarResult = wvarResult & "</RECIBO>"
                End If
            Next wvarCount
            wvarResult = wvarResult & "</RECIBOS>"
            wvarResult = wvarResult & "<HAY_DATOS_POLIZA_RENOVACION>" + Mid(strParseString, wvarPos + 145, 1) + "</HAY_DATOS_POLIZA_RENOVACION>"
        
            Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & "/>" & wvarResult & "</Response>"
        Case "S" 'Tiene mas de dos CUPONES
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Su deuda excede lo permitido publicar. Si Ud. reside en Capital Federal o Gran Buenos Aires, por favor comun�quese al 4338-6950; si Usted reside en el Interior del pa�s, por favor comu�quese al 0-800-222-8575. Muchas gracias" & Chr(34) & "/>" & wvarResult & "</Response>"
        Case "V" 'La deuda excede los 65 dias de plazo, con lo cual no debe mostrarse.
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "Su deuda excede lo permitido publicar. Si Ud. reside en Capital Federal o Gran Buenos Aires, por favor comun�quese al 4338-6950; si Usted reside en el Interior del pa�s, por favor comu�quese al 0-800-222-8575. Muchas gracias" & Chr(34) & "/>" & wvarResult & "</Response>"
        Case Else
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & "/>" & wvarResult & "</Response>"
        End Select
    End If

    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function FormatFecha(fecha)
    FormatFecha = Mid(fecha, 1, 2) & "/" & Mid(fecha, 3, 2) & "/" & Mid(fecha, 5, 4)
End Function
