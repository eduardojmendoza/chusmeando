Attribute VB_Name = "ModEncryptDecrypt"
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2004. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : ModEncryptDecrypt
' File Name : ModEncryptDecrypt.bas
' Creation Date: 29/11/2005
' Programmer : Fernando Osores / Lucas De Merlier
' Abstract :    Encripta y Desencripta un string utilizando el secreto indicado.
' *****************************************************************
Option Explicit

' Secreto. Debe ser fijo. No puede ser modificado.
Public Const mcteClave = "Rg-Sv+Fo$Dg!2006=CuponesLBAżnyl#HSBC"

' *****************************************************************
' Function : CapicomEncrypt
' Abstract : Encripta un string.
' Synopsis : CapicomEncrypt(ByVal strText As String) As String
' *****************************************************************
Public Function CapicomEncrypt(mobjCOM_Context As ObjectContext, ByVal strText As String) As String

    Const wcteComponente    As String = "embA_CVEncrypt.Process"
    
    Dim wobjClass           As HSBCInterfaces.IAction
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    
    wvarRequest = "<Request>"
    wvarRequest = wvarRequest & "<STRCLAVE>"
    wvarRequest = wvarRequest & strText
    wvarRequest = wvarRequest & "</STRCLAVE>"
    wvarRequest = wvarRequest & "<HEX>1</HEX>" 'Devolver el resultado en HEXA
    'wvarRequest = wvarRequest & "<HEX>0</HEX>" 'Devolver el resultado en DECIMAL
    wvarRequest = wvarRequest & "</Request>"
    
    Set wobjClass = mobjCOM_Context.CreateInstance(wcteComponente)
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    If wobjXMLResponse.loadXML(wvarResponse) Then
        CapicomEncrypt = wobjXMLResponse.selectSingleNode("//STRENCRYPT").Text
    Else
        CapicomEncrypt = ""
    End If

'On Error GoTo ErrorHandler
'    Dim objCapicom As CAPICOM.EncryptedData
'    Set objCapicom = New CAPICOM.EncryptedData
'
'    If strText <> "" Then
'
'        objCapicom.Algorithm = CAPICOM_ENCRYPTION_ALGORITHM_RC4
'        objCapicom.SetSecret mcteClave
'        objCapicom.Content = strText
'        CapicomEncrypt = StrToHex(objCapicom.Encrypt())
'
'    Else
'
'        CapicomEncrypt = ""
'
'    End If
'
'    Set objCapicom = Nothing
'
'    Exit Function
'ErrorHandler:
'    Set objCapicom = Nothing
'    CapicomEncrypt = Err.Description & ": " & strText
End Function

' *****************************************************************
' Function : CapicomDecrypt
' Abstract : Desencripta un string
' Synopsis : CapicomDecrypt(ByVal strText As String) As String
' *****************************************************************
Public Function CapicomDecrypt(ByVal strText As String) As String

    CapicomDecrypt = strText

'On Error GoTo ErrorHandler
'    Dim objCapicom As CAPICOM.EncryptedData
'    Set objCapicom = New CAPICOM.EncryptedData
'
'    If strText <> "" Then
'
'        objCapicom.Algorithm = CAPICOM_ENCRYPTION_ALGORITHM_RC4
'        objCapicom.SetSecret mcteClave
'        objCapicom.Decrypt (HexToStr(strText))
'        CapicomDecrypt = objCapicom.Content
'
'    Else
'
'        CapicomDecrypt = ""
'
'    End If
'
'    Set objCapicom = Nothing
'    Exit Function
'ErrorHandler:
'    Set objCapicom = Nothing
'    CapicomDecrypt = Err.Description & ": " & strText
End Function

Public Function HexToStr(strHex As String) As String

    Dim strAux As String
    Dim i As Integer
    
    strAux = ""
    
    For i = 1 To Len(strHex) Step 2
        strAux = strAux + Chr("&H" + Mid(strHex, i, 2))
    Next

    HexToStr = strAux

End Function

Public Function StrToHex(str As String) As String

    Dim strAux As String
    Dim i As Integer
    
    strAux = ""
    
    For i = 1 To Len(str)
        If Len(Hex(Asc(Mid(str, i, 1)))) = 1 Then
            strAux = strAux + "0"
        End If
        strAux = strAux + Hex(Asc(Mid(str, i, 1)))
    Next
    StrToHex = strAux

End Function
