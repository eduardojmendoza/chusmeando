
package com.qbe.services.cms.wsclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.qbe.services.cms.wsclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExecuteRequest_QNAME = new QName("http://cms.services.qbe.com/", "executeRequest");
    private final static QName _ExecuteRequestResponse_QNAME = new QName("http://cms.services.qbe.com/", "executeRequestResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.qbe.services.cms.wsclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteRequestResponse }
     * 
     */
    public ExecuteRequestResponse createExecuteRequestResponse() {
        return new ExecuteRequestResponse();
    }

    /**
     * Create an instance of {@link ExecuteRequest }
     * 
     */
    public ExecuteRequest createExecuteRequest() {
        return new ExecuteRequest();
    }

    /**
     * Create an instance of {@link ComposedResponse }
     * 
     */
    public ComposedResponse createComposedResponse() {
        return new ComposedResponse();
    }

    /**
     * Create an instance of {@link AnyXmlElement }
     * 
     */
    public AnyXmlElement createAnyXmlElement() {
        return new AnyXmlElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/", name = "executeRequest")
    public JAXBElement<ExecuteRequest> createExecuteRequest(ExecuteRequest value) {
        return new JAXBElement<ExecuteRequest>(_ExecuteRequest_QNAME, ExecuteRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cms.services.qbe.com/", name = "executeRequestResponse")
    public JAXBElement<ExecuteRequestResponse> createExecuteRequestResponse(ExecuteRequestResponse value) {
        return new JAXBElement<ExecuteRequestResponse>(_ExecuteRequestResponse_QNAME, ExecuteRequestResponse.class, null, value);
    }

}
