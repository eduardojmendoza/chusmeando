package com.qbe.services.cms.wsclient;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.cms.test.TestUtils;
import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.services.fewebservices.jaxws.ComposedResponse;

public class Test1308 {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore //para correr esto necesitamos un Weblogic específico
	public void test1308() throws Exception {
		String msgCode = "1308";
		String actionCode = TestUtils.loadActionCode(msgCode );
		String request = TestUtils.loadRequest(msgCode );
		String expectedResponse = TestUtils.loadResponse(msgCode );
		OSBConnector conn = new OSBConnector();
		String actualResponse = conn.executeRequest(actionCode, request);

//		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(expectedResponse, actualResponse));
//		List allDifferences = myDiff.getAllDifferences();
//		assertEquals(myDiff.toString(), 0, allDifferences.size());
//		XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, actualResponse);
	}

}
