package com.qbe.services.cms.wsclient;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.qbe.services.cms.osbconnector.OSBConnector;

public class TestCMSClient {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@Ignore //para correr esto necesitamos un Weblogic específico
	public void testClientConnect() {
		MigratedComponentService_Service service = new MigratedComponentService_Service();
		MigratedComponentService client = service.getMigratedComponentServicePort();
		AnyXmlElement request = new AnyXmlElement();
		ComposedResponse cr = client.executeRequest("lbaw_OVOpePendTotales", request, "");
		assertNotNull(cr);
		assertTrue("no vino el código de respuesta esperado ( -1 )", cr.getCode() == -1);
	}
	
	@Test
	@Ignore //Solo para pruebas manuales en ambiente específico
	public void testMQExecute() {
		OSBConnector connector =  new OSBConnector();
		String response = connector.executeQueueRequest("lbaw_OVSiniListadoDetalles", "<Request><USUARIO>EX009011L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100098857</CLIENSECAS><NIVEL1>GO</NIVEL1><CLIENSEC1>100098857</CLIENSEC1><NIVEL2></NIVEL2><CLIENSEC2></CLIENSEC2><NIVEL3></NIVEL3><CLIENSEC3></CLIENSEC3><FECDES>20131101</FECDES><FECHAS>20131130</FECHAS><MSGEST></MSGEST><CONTINUAR></CONTINUAR></Request>");
//		System.out.println(response);
	}

	@Test
	public void testGetResponse() {

		OSBConnector connector =  new OSBConnector();
		try{
			String response = "<Request><USUARIO>EX009011L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100098857</CLIENSECAS><NIVEL1>GO</NIVEL1><CLIENSEC1>100098857</CLIENSEC1><NIVEL2></NIVEL2><CLIENSEC2></CLIENSEC2><NIVEL3></NIVEL3><CLIENSEC3></CLIENSEC3><FECDES>20131101</FECDES><FECHAS>20131130</FECHAS><MSGEST></MSGEST><Response>\n" + 
					"    <Estado resultado=\"true\" mensaje=\"\" xmlns:ns0=\"http://cms.services.qbe.com/\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"/>\n" + 
					"</Response></Request>";
		String responsexml = connector.getXMLResponse(response);
		assertTrue("No empieza con Respone", responsexml.startsWith("<Response>"));
		assertTrue("No termina con Respone", responsexml.endsWith("</Response>"));
		} catch (Exception e) {
			assertTrue(e.getMessage(), false);
		}
	}

	@Test
	public void testGetResponseSingle() {

		OSBConnector connector =  new OSBConnector();
		try{
			String response = "<Request><USUARIO>EX009011L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100098857</CLIENSECAS><NIVEL1>GO</NIVEL1><CLIENSEC1>100098857</CLIENSEC1><NIVEL2></NIVEL2><CLIENSEC2></CLIENSEC2><NIVEL3></NIVEL3><CLIENSEC3></CLIENSEC3><FECDES>20131101</FECDES><FECHAS>20131130</FECHAS><MSGEST></MSGEST><Response /></Request>";
		String responsexml = connector.getXMLResponse(response);
		assertTrue("No termina con Respone", responsexml.matches("<Response\\s?/>"));
		} catch (Exception e) {
			assertTrue(e.getMessage(), false);
		}
	}

	
}
