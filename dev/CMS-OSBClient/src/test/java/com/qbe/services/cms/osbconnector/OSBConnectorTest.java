package com.qbe.services.cms.osbconnector;

import static org.junit.Assert.fail;

import java.net.MalformedURLException;

import org.junit.Ignore;
import org.junit.Test;

public class OSBConnectorTest {

	@Test
	@Ignore //Usado temporalmente para debuggear logging de Faults
	public void testLogFault() throws MalformedURLException, OSBConnectorException {
		
		// Usado para poder marshalear los parámetros, si no sale una Ex
		// Gracias a http://stackoverflow.com/a/17408912/70238
		System.setProperty("javax.xml.bind.JAXBContext", 
                "com.sun.xml.internal.bind.v2.ContextFactory"); 
		
//		Uso este que todavía falla :)
		String serviceURL = "http://10.1.10.202:8011/OV-MQ/Proxy/Paginated_1010_A_Insis";
		String actionCode = "lbaw_OVClientesConsulta";
		String request = "<Request>        <USUARIO>EX009002L</USUARIO>        <NIVELAS>PR</NIVELAS>        <CLIENSECAS>100034738</CLIENSECAS>        <DOCUMTIP/>        <DOCUMNRO/>        <CLIENDES>ARMAND</CLIENDES>        <PRODUCTO/>        <POLIZA/>        <CERTI/>        <PATENTE/>        <MOTOR/>        <ESTPOL>TODAS</ESTPOL>        <QRYCONT/>        <CLICONT/>        <PRODUCONT/>        <POLIZACONT/>        <SUPLENUMS/>        <CLIENSECS/>        <AGENTCLAS/>        <AGENTCODS/>        <NROITEMS/>      </Request>";

		OSBConnector con = new OSBConnector(serviceURL);
		try {
			String response = con.executeRequest(actionCode, request);
			fail("Debería haber saltado una OSBConnectorException");
		} catch (OSBConnectorException e) {
			
		}
	}

}
