package com.qbe.services.cms.osbconnector;

public class OSBConnectorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OSBConnectorException() {
	}

	public OSBConnectorException(String message) {
		super(message);
	}

	public OSBConnectorException(Throwable cause) {
		super(cause);
	}

	public OSBConnectorException(String message, Throwable cause) {
		super(message, cause);
	}

}
