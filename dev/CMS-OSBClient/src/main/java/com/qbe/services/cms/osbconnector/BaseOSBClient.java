package com.qbe.services.cms.osbconnector;


public abstract class BaseOSBClient {

	private OSBConnector osbConnector;
	  
	public OSBConnector getOsbConnector() throws OSBConnectorException {
		if ( osbConnector == null) {
			throw new OSBConnectorException("Connector no definido");
		}
		return osbConnector;
	}

	public void setOsbConnector(OSBConnector osbConnector) {
		this.osbConnector = osbConnector;
	}

  

}
