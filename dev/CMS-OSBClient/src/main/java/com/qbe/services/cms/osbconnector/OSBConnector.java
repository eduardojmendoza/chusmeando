package com.qbe.services.cms.osbconnector;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;

import org.w3c.dom.Text;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.cms.wsclient.ExecuteRequest;
import com.qbe.services.cms.wsclient.ExecuteRequestResponse;
import com.qbe.services.cms.wsclient.MigratedComponentService;
import com.qbe.services.cms.wsclient.MigratedComponentService_Service;
import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

public class OSBConnector {

	//protected static Logger logger = Logger.getLogger(OSBConnector.class.getName());
	protected Logger logger = Logger.getLogger(getClass().getName());

	private final static String CMS_NAMESPACE = "http://cms.services.qbe.com/";

	private final static QName EXECUTE_REQUEST_QNAME = new QName(CMS_NAMESPACE, "executeRequest");

	private String serviceURL = "NO_DEFINIDO";
	
	/**
	 * Tiempo máximo de espera para recibir respuesta en MQ. Por default es -1 = no espera ni bloquea
	 */
	private int osbMQTimeout = -1;

	public OSBConnector() {
	}

	public OSBConnector(String serviceURL) {
		this();
		this.serviceURL = serviceURL;
	}

	/**
	 * Recibe un <Request> y un action code, y con estos parámetros ejecuta el MigratedComponentService en el OSB
	 * Se comunica con el OSB via SOAP
	 * Retorna un <Response>
	 * 
	 * @param actionCode
	 * @param request
	 * @return
	 * @throws MalformedURLException
	 * @throws OSBConnectorException
	 *             cuando la respuesta es null
	 */
	public String executeRequest(String actionCode, String request) throws OSBConnectorException, MalformedURLException {
		logger.log(Level.FINE, "OSBConnector.executeRequest serviceURL = " + serviceURL);
		logger.log(Level.FINE, "OSBConnector.executeRequest actionCode = " + actionCode);
		logger.log(Level.FINE, "OSBConnector.executeRequest request = " + request);
		logger.info("OSBConnector.executeRequest serviceURL = " + serviceURL);

		try {
			logger.info("paso linea 73");
			MigratedComponentService_Service service = new MigratedComponentService_Service(new URL(serviceURL));
			logger.info("paso linea 75 " + service);
			MigratedComponentService client = service.getMigratedComponentServicePort();
			logger.info("paso linea 76");
			AnyXmlElement requestAny = AnyXmlElement.newWithRootChildrenNodes(request);

			com.qbe.services.cms.wsclient.AnyXmlElement anyReqParam = new com.qbe.services.cms.wsclient.AnyXmlElement();
			/*
			 * Esto se hace con el parche del for en vez de con: anyReqParam.getContent().addAll(requestAny.getAny());
			 * porque si quiero insertarlo directamente salta una Excepcion: javax.xml.ws.WebServiceException:
			 * javax.xml.bind.MarshalException - with linked exception: [javax.xml.bind.JAXBException: class
			 * com.sun.org.apache.xerces.internal.dom.DeferredTextImpl nor any of its super class is known to this
			 * context.]
			 * 
			 * Esto es porque el AnyXmlElement que genera en wsclient NO soporta este tipo de nodos. Ver que dice que
			 * soporta solamente Objects of the following type(s) are allowed in the list {@link Element } {@link String
			 * } {@link Object }
			 * 
			 * Acá kohsuke explica un problema similar, aunque en una versión anterior:
			 * https://www.java.net//node/659395 Ver #5
			 */
			for (Object node : requestAny.getAny()) {
				if (node instanceof Text) {
					Text textNode = (Text) node;
					anyReqParam.getContent().add(textNode.getTextContent());
				} else {
					anyReqParam.getContent().add(node);
				}
			}
			logger.info("actionCode: " + actionCode + " anyREq: "+ anyReqParam);
			com.qbe.services.cms.wsclient.ComposedResponse cr = client.executeRequest(actionCode, anyReqParam, "");
			if (cr == null)
				throw new OSBConnectorException("ComposedResponse recibido es null");

			// Lo paso del AnyXmlElement del cliente al de fewebservice para poder hacer el marshal
			AnyXmlElement responseAny = new AnyXmlElement();
			responseAny.getAny().addAll(cr.getResponse().getContent());
			String responseString = responseAny.wrapWithTagName("Response");
			logger.log(Level.FINE, "OSBConnector.executeRequest response = " + responseString);
			return responseString;
		} catch (SOAPFaultException e) {
			logFault(e.getFault());
			logger.log(Level.WARNING,e.getMessage(),e);
			throw new OSBConnectorException("SOAPFaultException: " + e.getMessage(), e);
		} catch (WebServiceException e) {
			throw new OSBConnectorException(e);
		} catch (Exception e) {
			throw new OSBConnectorException(e);
		}
	}
	
	public void logFault(SOAPFault f ) {

		try {
			String fSerializado = XmlDomExtended.prettyPrint(f);
			logger.log(Level.SEVERE, fSerializado);
		} catch (XmlDomExtendedException e) {
			logger.log(Level.SEVERE, "No pude serializad el SOAPFault recibido, imprimo los atributos básicos", f);

			logger.log(Level.SEVERE,"SOAPFault [ code = {0}, faultString = {1}, faultActor = {2}", 
					new Object[] { f.getFaultCode(), f.getFaultString(), f.getFaultActor()});
		}
	}
	
	/**
	 * Ejecuta un request a OSB enviándolo via MQ, con un timeout máximo de osbTimeout
	 * Se envía con prefijo "CMS_" para que en OSB puedan distinguir el formato
	 * 
	 * @param actionCode
	 * @param request
	 * @return
	 */
	public String executeQueueRequest(String actionCode, String request) {
		try {
			StringBuilder executeRequest = new StringBuilder("CMS_");
			executeRequest.append(createExecuteRequest(actionCode, request));

//			MQProxy mqProxy = MQProxy.getInstance(MQProxy.OSBPROXY);
			MQProxy mqProxy = MQProxy.getInstance("osbproxy");
			StringHolder executeRequestResponse = new StringHolder();
			mqProxy.executePrim(executeRequest.toString(), executeRequestResponse, osbMQTimeout);

			String response = getXMLResponse(executeRequestResponse.getValue());
			return response;

		} catch (Exception e) {
			throw new ComponentExecutionException("No se pudo enviar mensaje al OSB via MQ", e);
		}
	}

	public static String createExecuteRequest(String actionCode, String request) throws Exception {
		ExecuteRequest executeRequest = new ExecuteRequest();
		executeRequest.setActionCode(actionCode);
		com.qbe.services.cms.wsclient.AnyXmlElement body = new com.qbe.services.cms.wsclient.AnyXmlElement();
		body.getContent().add("REPLACE_TOKEN");
		executeRequest.setRequest(body);

		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(ExecuteRequestResponse.class);
		JAXBElement<ExecuteRequest> requestElement = new JAXBElement<ExecuteRequest>(EXECUTE_REQUEST_QNAME,
				ExecuteRequest.class, executeRequest);
		Marshaller m = context.createMarshaller();
		m.marshal(requestElement, writer);
		String requestXML = writer.toString().replace("<Request>REPLACE_TOKEN</Request>", request);

		return requestXML;
	}

	/**
	 * Pesudo-parsea el String con RegEx y devuelve el <Response>
	 * FIXME No parsear con RegEx sino con JAXB o un parser de XML real
	 * 
	 * @param soapResponse
	 * @return
	 * @throws Exception
	 */
	public static String getXMLResponse(String soapResponse) throws Exception {
		Pattern pattern = Pattern.compile(".*(<Response>.*?</Response>).*|.*(<Response\\s?/>).*", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(soapResponse);
		if(!matcher.matches()){
			throw new ComponentExecutionException("No se encontró nodo Response en respuesta SOAP: " + soapResponse);
		}
		return matcher.group(1) != null ? matcher.group(1) : matcher.group(2);
	}

	public String getServiceURL() {
		return serviceURL;
	}

	public void setServiceURL(String serviceURL) {
		this.serviceURL = serviceURL;
	}

	public void setOsbMQTimeout(int osbMQTimeout) {
		this.osbMQTimeout = osbMQTimeout;
	}
	
	@Override
	public String toString() {
		return String.format("OSBConnector service: {%s}", getServiceURL());
	}
}
