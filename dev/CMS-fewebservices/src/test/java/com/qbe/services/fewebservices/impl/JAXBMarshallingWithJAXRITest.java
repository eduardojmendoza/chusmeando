package com.qbe.services.fewebservices.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.services.fewebservices.jaxws.ComposedResponse;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

/**
 * Tests de JAXBMarshalling usando la implementación JAX-RI que viene por default con el JDK
 * 
 * @author ramiro
 *
 */
public class JAXBMarshallingWithJAXRITest {

	private static final String JAXB_CONTEXT_IMPL = "com.sun.xml.bind.v2.runtime.JAXBContextImpl";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Hace el marshalling de la respuesta como la hace el WebService, a partir de un ComposedResponse
	 * que tiene un AnyXmlElement
	 * Usa JAX-RI 
	 * El marshalling NO debe generar el CDATA, lo elimina directamente, escapeando el contenido
	 * Y debe escapear el <xml> como corresponde, dado que le sacó el CDATA que lo wrapeaba
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalAnyXmlElement() throws JAXBException {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><BENEF><![CDATA[Esto es un <xml>]]></BENEF><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		AnyXmlElement anyResp = AnyXmlElement.newWithRootChildrenNodes(cdataResp);
		ComposedResponse cr = new ComposedResponse(0, anyResp);
		com.qbe.services.fewebservices.jaxri.ExecuteRequestResponse err = new com.qbe.services.fewebservices.jaxri.ExecuteRequestResponse();
		err.setReturn(cr);
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.jaxri.ExecuteRequestResponse.class);
		assertTrue("No está usando JAX-RI!", context.getClass().getName().contains(JAXB_CONTEXT_IMPL));
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(err, writer);
		String xml = writer.getBuffer().toString();
		assertTrue("Vino el CDATA!", xml.contains("<BENEF>ARMATI MARIA ADRIANA</BENEF>"));
		assertTrue("No escapeo el tag <xml> como esperaba", xml.contains("<BENEF>Esto es un &lt;xml&gt;</BENEF>"));
	}

	/**
	 * Hace la serialización de la respuesta como la hace el WebService, a partir de un ComposedResponse
	 * que tiene un AnyXmlElement
	 * Usa JAX-RI 
	 * A diferencia del testMarshalAnyXmlElementWithJAXRI, este tiene un parche para que el marshaller
	 * no escapee el CDATA, pero el parche NO funciona bien, porque el CDATASection ya elimina el !<CDATA y
	 * el parche lo toma después. Además, al no escapear, genera un xml inválido porque no escapea el <xml>
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalAnyXmlElementPatched() throws JAXBException {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><BENEF><![CDATA[Esto es un <xml>]]></BENEF><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		AnyXmlElement anyResp = AnyXmlElement.newWithRootChildrenNodes(cdataResp);
		ComposedResponse cr = new ComposedResponse(0, anyResp);
		com.qbe.services.fewebservices.jaxri.ExecuteRequestResponse err = new com.qbe.services.fewebservices.jaxri.ExecuteRequestResponse();
		err.setReturn(cr);
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.jaxri.ExecuteRequestResponse.class);
		assertTrue("No está usando JAX-RI!", context.getClass().getName().contains(JAXB_CONTEXT_IMPL));
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty( "com.sun.xml.bind.characterEscapeHandler", new CharacterEscapeHandler() {
		    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
		        String s = new String(ch, start, length);
		        out.write(s);
		    }
		});
		m.marshal(err, writer);
		String xml = writer.getBuffer().toString();
		assertTrue("Vino el CDATA!", xml.contains("<BENEF>ARMATI MARIA ADRIANA</BENEF>"));
		assertTrue("Escapeo el tag <xml>, aunque tiene el parche de no escapeo", xml.contains("<BENEF>Esto es un <xml></BENEF>"));
	}

	/**
	 * Prueba la serialización de un String con forma de XML, sin pasar por DOM. 
	 * Uso ComposedStringResponse en vez de ComposedResponse
	 * Usa JAX-RI 
	 * Este tiene un parche para que el marshaller no escapee los CDATAs. El parche depende de usar jax-ri de sun
	 * 
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalStringWithXMLInsidePatched() throws JAXBException {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><BENEF><![CDATA[Esto es un <xml>]]></BENEF><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		com.qbe.services.fewebservices.jaxri.ComposedStringResponse cr = new com.qbe.services.fewebservices.jaxri.ComposedStringResponse(0, cdataResp);
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.jaxri.ComposedStringResponse.class);
		assertTrue("No está usando JAX-RI!", context.getClass().getName().contains(JAXB_CONTEXT_IMPL));
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty( "com.sun.xml.bind.characterEscapeHandler", new CharacterEscapeHandler() {
		    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
		        String s = new String(ch, start, length);
		        out.write(s);
		    }
		});
		m.marshal(cr, writer);
		String xml = writer.getBuffer().toString();
//		System.out.println(xml);
		assertTrue("No vino el CDATA como esperábamos", xml.contains("<BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF>"));
		assertTrue("No vino el CDATA <xml> como esperábamos", xml.contains("<BENEF><![CDATA[Esto es un <xml>]]></BENEF>"));
	}

	/**
	 * Prueba la serialización de un String con forma de XML, sin pasar por DOM. 
	 * Uso ComposedStringResponse en vez de ComposedResponse
	 * Usa JAX-RI 
	 * Este NO tiene el parche, por lo que genera el string escapeado
	 * 
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalStringWithXMLInsideUnPatched() throws JAXBException {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><BENEF><![CDATA[Esto es un <xml>]]></BENEF><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		com.qbe.services.fewebservices.jaxri.ComposedStringResponse cr = new com.qbe.services.fewebservices.jaxri.ComposedStringResponse(0, cdataResp);
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.jaxri.ComposedStringResponse.class);
		assertTrue("No está usando JAX-RI!", context.getClass().getName().contains(JAXB_CONTEXT_IMPL));
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(cr, writer);
		String xml = writer.getBuffer().toString();
		assertTrue("Vino sin escapear", xml.contains("&lt;Response&gt;&lt;Estado resultado='true' mensaje='' /&gt;&lt;BENEF&gt;&lt;![CDATA[Esto es un &lt;xml&gt;]]&gt;&lt;/BENEF&gt;&lt;MSGEST&gt;OK&lt;/MSGEST&gt;&lt;REGS&gt;&lt;REG&gt;&lt;NROP&gt;11240641&lt;/NROP&gt;&lt;BENEF&gt;&lt;![CDATA[ARMATI MARIA ADRIANA]]&gt;&lt;/BENEF&gt;&lt;EST&gt;R&lt;/EST&gt;&lt;FEC&gt;&lt;![CDATA[19/11/2012]]&gt;&lt;/FEC&gt;&lt;MON&gt;&lt;![CDATA[$]]&gt;&lt;/MON&gt;&lt;SIG&gt; &lt;/SIG&gt;&lt;IMP&gt;12845,00&lt;/IMP&gt;&lt;/REG&gt;&lt;/REGS&gt;&lt;/Response&gt;</Response>"));
	}
}
