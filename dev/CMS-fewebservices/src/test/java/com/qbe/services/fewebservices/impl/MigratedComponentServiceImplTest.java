package com.qbe.services.fewebservices.impl;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.qbe.services.fewebservices.jaxws.AnyXmlElement;

public class MigratedComponentServiceImplTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFormatRequestAsString() throws DOMException, SAXException, IOException, ParserConfigurationException, Exception {

		String xml1 = "<Request><Estado resultado='true' mensaje='' /><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Request>";
		String xml1Formatted = primTestFormatRequestAsString(xml1);
		assertTrue("No incluyó el CDATA en el formateado",xml1Formatted.contains("<![CDATA[ARMATI MARIA ADRIANA]]"));
		
		String xml2 = "<Request><USUARIO>EX009005L</USUARIO>				<NIVELAS>GO</NIVELAS>		<CLIENSECAS>100029438</CLIENSECAS>				<NIVEL1 />				<CLIENSEC1 />				<NIVEL2 />			<CLIENSEC2 />				<NIVEL3>PR</NIVEL3>				<CLIENSEC3>100029438</CLIENSEC3>				<TIPO_DISPLAY>2</TIPO_DISPLAY>				<ORIGEN>E</ORIGEN></Request>";
		primTestFormatRequestAsString(xml2);
		
		String xml3 = "<Request><Estado resultado='true' mensaje='' />CODIGO,NOMBRE,PROD,POLIZA,CERTIF,ESTADO,ENDOSO,RECIBO,MONEDA,IMPORTE,CANAL,FEC.VTO.,DIAS,MOTIVO\n"+
"PR3233,FRANCO PAREDES  BENICIO ARNALD,AUS1,00000001,00000001323588,SUS,000011,631968120,$  ,+491.45,DEBI,2012-12-18,+164,SALDO INSUFICIENTEPR3233,RAMOS CHOQUE  RAMON           ,AUS1,00000001,00000001327102,SUS,000011,631910768,$  ,+487.24,TARJ,2012-12-18,+164,DESCONOCIMIENTO</Request>";
		primTestFormatRequestAsString(xml3);
	}

	private String primTestFormatRequestAsString(
			String requestXML) throws SAXException, IOException,
			ParserConfigurationException, Exception {
		AnyXmlElement anyResp1 = AnyXmlElement.newWithRootChildrenNodes(requestXML);
		String formatted = anyResp1.formatAsRequestString();

		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(requestXML, formatted));
		List allDifferences = myDiff.getAllDifferences();
		Assert.assertEquals(myDiff.toString(), 0, allDifferences.size());
		return formatted;
	}
	
}
