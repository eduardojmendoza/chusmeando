package com.qbe.services.fewebservices.jaxws;

import static org.junit.Assert.*;

import java.util.logging.Level;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AnyXmlElementTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFormatAsRequestString() throws Exception {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		builder = f.newDocumentBuilder();
		Document doc = builder.newDocument();
		Element dataElement = (Element)doc.createElement("Data");
		AnyXmlElement any = new AnyXmlElement();
		any.getAny().add(dataElement);
		String result = any.formatAsRequestString();
		assertTrue("No lo marshaleo como esperaba", "<Request><Data/></Request>".equals(result));
	}

	private void checkTagNameAndNoNSStuff(Element regs, String tagName) {
		assertTrue(regs.getLocalName() == null);
		assertTrue(regs.getTagName().equals(tagName));
		assertTrue(regs.getNamespaceURI() == null);
		assertTrue(regs.getPrefix() == null);
	}

	@Test
	public void testNewForResponseXML_Namespaces() {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		AnyXmlElement anyResp = AnyXmlElement.newWithRootChildrenNodes(cdataResp);
		Element estado = (Element)anyResp.getAny().get(0);
		checkTagNameAndNoNSStuff(estado, "Estado");
		
		Element msgtest = (Element)anyResp.getAny().get(1);
		checkTagNameAndNoNSStuff(msgtest, "MSGEST");
		Element regs = (Element)anyResp.getAny().get(2);
		checkTagNameAndNoNSStuff(regs, "REGS");
		
		Element reg = (Element) regs.getFirstChild();
		NodeList regChildren = reg.getChildNodes();
		for (int i = 0; i < regChildren.getLength(); i++) {
			Node child = regChildren.item(i);
			assertTrue(child.getLocalName() == null);
			assertTrue(child.getNamespaceURI() == null);
			assertTrue(child.getPrefix() == null);
		}
	}

	@Test
	public void testNewForResponseXML_WithCDATA() {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		AnyXmlElement anyResp = AnyXmlElement.newWithRootChildrenNodes(cdataResp);
		Element regs = (Element)anyResp.getAny().get(2);
		Element reg = (Element) regs.getFirstChild();
		NodeList regChildren = reg.getChildNodes();
		Element benef = (Element)regChildren.item(1);
		CDATASection benefContent = (CDATASection) benef.getFirstChild();
		assertTrue("No encontré el dato esperado en un CDATA", benefContent.getData().equals("ARMATI MARIA ADRIANA"));
	}

	@Test
	public void testNewForResponseXML_WithNoElements() {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' />CODIGO,NOMBRE,PROD,POLIZA,CERTIF,ESTADO,ENDOSO,RECIBO,MONEDA,IMPORTE,CANAL,FEC.VTO.,DIAS,MOTIVO\n"+
"PR3233,FRANCO PAREDES  BENICIO ARNALD,AUS1,00000001,00000001323588,SUS,000011,631968120,$  ,+491.45,DEBI,2012-12-18,+164,SALDO INSUFICIENTEPR3233,RAMOS CHOQUE  RAMON           ,AUS1,00000001,00000001327102,SUS,000011,631910768,$  ,+487.24,TARJ,2012-12-18,+164,DESCONOCIMIENTO</Response>";
		AnyXmlElement anyResp = AnyXmlElement.newWithRootChildrenNodes(cdataResp);
		Node text = (Node)anyResp.getAny().get(1);
		assertTrue(text.getNodeType() == Node.TEXT_NODE);
	}

	@Test
	public void testNewForEstadoWithMessage() {
		final String attrValue = "NADA";
		AnyXmlElement result = AnyXmlElement.newForEstadoWithMessage(attrValue);
		Element estado = (Element)result.getAny().get(0);
		NamedNodeMap attrs = estado.getAttributes();
		Node mensaje = attrs.getNamedItem("mensaje");
		String nada = mensaje.getNodeValue();
		assertTrue(nada.equals(attrValue));
	}
}
