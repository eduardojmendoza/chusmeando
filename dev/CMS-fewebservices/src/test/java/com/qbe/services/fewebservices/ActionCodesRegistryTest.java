package com.qbe.services.fewebservices;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.vbcompat.framework.VBObjectClass;

public class ActionCodesRegistryTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void testGetImplForActionCodeForOVCotEndosos() throws Exception {
		ActionCodesRegistry reg = ActionCodesRegistry.instance();
		Class<? extends VBObjectClass> clazz = reg.getImplForActionCode("lbaw_CotEndosos");
		assertNotNull(clazz);
	}
	
	@Test
	public void testGetImplForActionCodeForSiniestroDenuncia() throws Exception {
		ActionCodesRegistry reg = ActionCodesRegistry.instance();
		Class<? extends VBObjectClass> clazz = reg.getImplForActionCode("lbaw_siniMQGenerico");
		assertNotNull(clazz);
	}
	
	@Test
	public void testGetImplForActionCodeForlbaw_GetConsultaMQGestion_Paginado() throws Exception {
		ActionCodesRegistry reg = ActionCodesRegistry.instance();
		Class<? extends VBObjectClass> clazz = reg.getImplForActionCode("lbaw_GetConsultaMQGestion_Paginado");
		assertNotNull(clazz);
		assertTrue(clazz.getName().equals("com.qbe.services.paginate.mqgestion.GetConsultaMQGestion"));
	}
	

}
