package com.qbe.services.fewebservices.jaxri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "executeRequestResponseFORALL", namespace = "http://fewebservices.services.qbe.com/")
@XmlAccessorType(XmlAccessType.FIELD)
public class ComposedStringResponse {
	private int code;
	
	@XmlElement( name = "Response")
	private String response;

	public ComposedStringResponse() {
	}

	public ComposedStringResponse(int code, String response) {
		this.response = response;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int resultCode) {
		this.code = resultCode;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

}
