package com.qbe.services.fewebservices.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.services.fewebservices.jaxws.ComposedResponse;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

/**
 * Tests de JAXB marshalling usando la implementación de EclipseLink Moxy. Para esto debemos configurar un jaxb.properties.
 * Ver referencias:
 * http://blog.bdoughan.com/2011/05/specifying-eclipselink-moxy-as-your.html
 * 
 * @author ramiro
 *
 */
public class JAXBMarshallingWithEclipseLinkTest {

	private static final String JAXB_CONTEXT_IMPL = "org.eclipse.persistence.jaxb.JAXBContext";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Hace el marshalling de la respuesta como la hace el WebService, a partir de un ComposedResponse
	 * que tiene un AnyXmlElement
	 * El marshalling no debe escapear el CDATA
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalAnyXmlElement() throws JAXBException {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><BENEF><![CDATA[Esto es un <xml>]]></BENEF><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		AnyXmlElement anyResp = AnyXmlElement.newWithRootChildrenNodes(cdataResp);
		ComposedResponse cr = new ComposedResponse(0, anyResp);
		com.qbe.services.fewebservices.moxy.ExecuteRequestResponse err = new com.qbe.services.fewebservices.moxy.ExecuteRequestResponse();
		err.setReturn(cr);
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.moxy.ExecuteRequestResponse.class);
		checkJAXBImplementation(context);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(err, writer);
		String xml = writer.getBuffer().toString();
		assertTrue("No vino el CDATA", xml.contains("<BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF>"));
		assertTrue("No vino el CDATA del <xml>", xml.contains("<BENEF><![CDATA[Esto es un <xml>]]></BENEF>"));
	}

	/**
	 * @param context
	 */
	private void checkJAXBImplementation(JAXBContext context) {
		assertTrue("No está usando EclipseLink", context.getClass().getName().contains(JAXB_CONTEXT_IMPL));
	}

	/**
	 * Hace la serialización de la respuesta como la hace el WebService, a partir de un ComposedResponse
	 * que tiene un AnyXmlElement
	 * Muestra que setear el parche de JAX-RI en EclipseLink no funciona
	 *  
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalAnyXmlElementPatched() throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.moxy.ExecuteRequestResponse.class);
		checkJAXBImplementation(context);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		try {
			m.setProperty( "com.sun.xml.bind.characterEscapeHandler", new CharacterEscapeHandler() {
			    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
			        String s = new String(ch, start, length);
			        out.write(s);
			    }
			});
		} catch (PropertyException e) {
			return;
		}
		fail("No saltó PropertyException, debería haber saltado usando Eclipse Link Moxy");
	}

	/**
	 * Prueba la serialización de un String con forma de XML, sin pasar por DOM. 
	 * Uso ComposedStringResponse en vez de ComposedResponse
	 * EclipseLink escapea todo el string
	 * 
	 * @throws JAXBException
	 */
	@Test
	public void testMarshalStringWithXMLInside() throws JAXBException {
		String cdataResp = "<Response><Estado resultado='true' mensaje='' /><BENEF><![CDATA[Esto es un <xml>]]></BENEF><MSGEST>OK</MSGEST><REGS><REG><NROP>11240641</NROP><BENEF><![CDATA[ARMATI MARIA ADRIANA]]></BENEF><EST>R</EST><FEC><![CDATA[19/11/2012]]></FEC><MON><![CDATA[$]]></MON><SIG> </SIG><IMP>12845,00</IMP></REG></REGS></Response>";
		com.qbe.services.fewebservices.moxy.ComposedStringResponse cr = new com.qbe.services.fewebservices.moxy.ComposedStringResponse(0, cdataResp);
		StringWriter writer = new StringWriter();
		JAXBContext context = JAXBContext.newInstance(com.qbe.services.fewebservices.moxy.ComposedStringResponse.class);
		checkJAXBImplementation(context);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.marshal(cr, writer);
		String xml = writer.getBuffer().toString();
		assertTrue("Vino sin escapear", xml.contains("&lt;Response>&lt;Estado resultado='true' mensaje='' />&lt;BENEF>&lt;![CDATA[Esto es un &lt;xml>]]>&lt;/BENEF>&lt;"));
	}

}
