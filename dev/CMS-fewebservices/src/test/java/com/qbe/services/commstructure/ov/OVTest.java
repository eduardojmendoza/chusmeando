package com.qbe.services.commstructure.ov;

import static org.junit.Assert.*;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OVTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBack2Back() throws Exception {
		JAXBContext context = JAXBContext.newInstance(EST.class);
		Unmarshaller um = context.createUnmarshaller();
		Object o = um.unmarshal(Thread.currentThread().getContextClassLoader().getResourceAsStream("OV_EST.xml"));
		if (! (o instanceof EST)) throw new Exception("No pude unmarshalear");
		EST est = (EST) o;
		String nom0_4 = est.getOR().get(0).getPR().get(4).getNOM();
		assertTrue(nom0_4.equalsIgnoreCase("BANCO MACRO SOCIEDAD ANONIMA "));

		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

		StringWriter sw = new StringWriter();
		m.marshal(est, sw);
//		System.out.println(sw);
		
		String control = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("OV_EST.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(control, sw.toString()));
		List<?> allDifferences = myDiff.getAllDifferences();
		//Ignorado xq marca las diferencias de falta de nanespace prefix
//		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", control, sw.toString());
		
		
	}

}
