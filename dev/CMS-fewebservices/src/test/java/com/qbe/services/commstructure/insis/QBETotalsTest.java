package com.qbe.services.commstructure.insis;

import static org.junit.Assert.*;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class QBETotalsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBack2Back() throws Exception {
		JAXBContext context = JAXBContext.newInstance(QBETotalsRs.class);
		Unmarshaller um = context.createUnmarshaller();
		Object o = um.unmarshal(Thread.currentThread().getContextClassLoader().getResourceAsStream("1106QbeTotalsConNS-localXSD.xml"));
		if (! (o instanceof QBETotalsRs)) throw new Exception("No pude unmarshalear");
		QBETotalsRs tot = (QBETotalsRs) o;
		String name0_0 = tot.getTotals().get(0).getTotals().get(0).getName();
		assertTrue(name0_0.equalsIgnoreCase("NECALOJ S.R.L "));

		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

		StringWriter sw = new StringWriter();
		m.marshal(tot, sw);
//		System.out.println(sw);
		
		String control = IOUtils.toString(Thread.currentThread().getContextClassLoader().getResourceAsStream("1106QbeTotalsConNS-localXSD.xml"));
		XMLUnit.setIgnoreWhitespace(true);
		DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(control, sw.toString()));
		List<?> allDifferences = myDiff.getAllDifferences();
		//Ignorado xq marca las diferencias de falta de nanespace prefix
//		assertEquals(myDiff.toString(), 0, allDifferences.size());
		XMLAssert.assertXMLEqual("No lo procesó como esperábamos", control, sw.toString());
		
		
	}

}
