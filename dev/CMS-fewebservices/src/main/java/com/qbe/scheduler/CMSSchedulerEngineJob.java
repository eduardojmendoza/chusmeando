package com.qbe.scheduler;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.qbe.services.scheduler.SchedulerServiceImpl;

public class CMSSchedulerEngineJob implements Job {
	 
	/**
	 * FIXME parametrizar el nombre de usuario
	 */
	private static final String SCHEDULER_USER = "SCHEDULER_INT";
	
	private static Logger logger = Logger.getLogger(CMSSchedulerEngineJob.class.getName());
	
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
         
        logger.log(Level.FINE,"Disparo el SchedulerEngine");
        SchedulerServiceImpl service = new SchedulerServiceImpl();
        try {
			String resultado = service.procesarBandeja(SCHEDULER_USER);
			logger.log(Level.FINE, "Resultado de ejecutar CMSScheduler: " + resultado);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error al ejecutar SchedulerEngine para usuario " + SCHEDULER_USER, e);
		}
        
    }
}