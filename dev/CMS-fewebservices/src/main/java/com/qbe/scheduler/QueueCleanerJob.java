package com.qbe.scheduler;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qbe.mq.utils.QueueCleaner;
import com.qbe.mq.utils.QueueCleanerException;

public class QueueCleanerJob implements Job {

	private static Logger logger = Logger.getLogger(QueueCleanerJob.class.getName());
	
	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:quartz-scheduler-beans.xml");
		}
		return context;
	}

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
         
        logger.log(Level.FINE,"Starting queue cleaner");
        runCleaner("aisRespQueueCleaner");
        runCleaner("mwRespQueueCleaner");
        runCleaner("osbRespQueueCleaner");
        runCleaner("cuponesRespQueueCleaner");
    }

	protected void runCleaner(String queueBeanName) {
		try {
        	QueueSpec mqSpec = (QueueSpec) getContext().getBean(queueBeanName);
			QueueCleaner.cleanOldMessages(mqSpec.hostname, mqSpec.port, mqSpec.channel, mqSpec.queueManager, mqSpec.queueName);
		} catch (QueueCleanerException e) {
	        logger.log(Level.SEVERE,"En queue cleaner job para bean: " + queueBeanName, e);
		}
	}

    
}