package com.qbe.scheduler;
import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
 
public class QuartzScheduler extends GenericServlet {
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1850041051774247265L;

	public void init(ServletConfig config) throws ServletException {
 
        super.init(config);
 
        Scheduler sched;
        try {
             //Recupero el scheduler que levantó la init servlet de Quartz
            sched = StdSchedulerFactory.getDefaultScheduler();
            sched.start();
 
            scheduleQueueCleaner(sched);
            scheduleCMSScheduler(sched);
             
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

	private void scheduleQueueCleaner(Scheduler sched) throws SchedulerException {
		JobDetail job = JobBuilder.newJob(QueueCleanerJob.class).withIdentity("QueueCleanerJob").build();
		Trigger trigger = TriggerBuilder.newTrigger()
		                    .withIdentity("queueCleanerTrigger")
		                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/10 * * * ?"))
		                    .startNow()
		                    .build();
		sched.scheduleJob(job, trigger);
	}

	/**
	 * FIXME Parametrizar la programación
	 * 
	 * @param sched
	 * @throws SchedulerException
	 */
	private void scheduleCMSScheduler(Scheduler sched) throws SchedulerException {
		JobDetail job = JobBuilder.newJob(CMSSchedulerEngineJob.class).withIdentity("CMSSchedulerEngineJob").build();
		Trigger trigger = TriggerBuilder.newTrigger()
		                    .withIdentity("CMSSchedulerEngineTrigger")
		                    .withSchedule(CronScheduleBuilder.cronSchedule("0 0/20 * * * ?"))
		                    .startNow()
		                    .build();
		sched.scheduleJob(job, trigger);
	}
 
	@Override
	public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {

	}
}