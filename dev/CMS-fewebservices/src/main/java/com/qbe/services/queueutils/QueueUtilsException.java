package com.qbe.services.queueutils;

public class QueueUtilsException extends Exception {

	public QueueUtilsException() {
	}

	public QueueUtilsException(String message) {
		super(message);
	}

	public QueueUtilsException(Throwable cause) {
		super(cause);
	}

	public QueueUtilsException(String message, Throwable cause) {
		super(message, cause);
	}

}
