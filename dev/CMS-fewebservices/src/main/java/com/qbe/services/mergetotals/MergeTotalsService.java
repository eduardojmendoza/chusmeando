package com.qbe.services.mergetotals;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.w3c.dom.Node;

@WebService(
		name = "MergeTotalsService" 
		,targetNamespace = "http://cms.services.qbe.com/mergetotals"
)

public interface MergeTotalsService {

	@WebMethod(operationName = "mergeTotals", action = "mergeTotals")
	@WebResult( name = "merged")
	public Object mergeTotals(
			@WebParam(name = "left" )Object left, 
			@WebParam(name = "right" )Object right) throws Exception;
	
	@WebMethod(operationName = "mergeTotalsBalanced", action = "mergeTotalsBalanced")
	@WebResult( name = "merged")
	public Object mergeTotalsBalanced(
			@WebParam(name = "left" )Object left, 
			@WebParam(name = "right" )Object right) throws Exception;

}




