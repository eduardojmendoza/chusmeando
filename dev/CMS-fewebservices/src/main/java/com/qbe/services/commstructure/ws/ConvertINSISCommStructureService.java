package com.qbe.services.commstructure.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.qbe.services.commstructure.insis.QBETotalsRs;
import com.qbe.services.commstructure.ov.EST;

@WebService(
		name = "ConvertINSISCommStructureService" 
		,targetNamespace = "http://cms.services.qbe.com/convertcommstructure"
)
public interface ConvertINSISCommStructureService {
	
	@WebMethod(operationName = "convert", action = "convert")
//	@RequestWrapper(className = "com.qbe.services.mergetotals.jaxws.MergeTotalsRequest", 
//					localName = "mergeTotalsRequest",targetNamespace = "http://cms.services.qbe.com/mergetotals")
//	@ResponseWrapper(className = "com.qbe.services.mergetotals.jaxws.MergeTotalsResponse", 
//					localName = "mergeTotalsResponse",targetNamespace = "http://cms.services.qbe.com/mergetotals")

	@WebResult( name = "EST")
	public EST convert(
			@WebParam(name = "QBETotalsRs", targetNamespace = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/") QBETotalsRs QBETotalsRs) throws Exception;
	
}
