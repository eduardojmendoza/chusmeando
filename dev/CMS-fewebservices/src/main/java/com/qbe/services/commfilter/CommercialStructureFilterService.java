package com.qbe.services.commfilter;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.qbe.services.commfilter.types.CommercialStructureFilter;
import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.services.fewebservices.jaxws.ComposedResponse;

@WebService(
		name = "CommercialStructureFilterService" 
		,targetNamespace = "http://cms.services.qbe.com/commFilter"
)
public interface CommercialStructureFilterService  {

	@WebMethod(operationName = "fetchUserFilter", action = "urn:fetchUserFilter")
	@RequestWrapper(className = "com.qbe.services.commfilter.jaxws.FetchUserFilterRequest", 
					localName = "fetchUserFilterRequest",targetNamespace = "http://cms.services.qbe.com/")
	@ResponseWrapper(className = "com.qbe.services.commfilter.jaxws.FetchUserFilterResponse", 
					localName = "fetchUserFilterResponse",targetNamespace = "http://cms.services.qbe.com/")
	public CommercialStructureFilter fetchUserFilter(
			   @WebParam(name = "CIAASCOD"    ) String ciaAsCod,//          PIC X(04).
			   @WebParam(name = "USUARCOD"    ) String usuarCod,//          PIC X(10).
			   @WebParam(name = "ESTADOMSG"   ) String estadoMsg,//          PIC X(02).
			   @WebParam(name = "ERRORMSG"    ) String errorMsg,//          PIC X(02).
			   @WebParam(name = "CLIENAS"     ) String clienAs,//          PIC 9(09).
			   @WebParam(name = "NIVELAS"     ) String nivelAs,//          PIC X(02).
			   @WebParam(name = "NIVELCLA1"   ) String nivelCla1,//          PIC X(02).
			   @WebParam(name = "CLIENSEC1"   ) String clienSec1,//          PIC 9(09).
			   @WebParam(name = "NIVELCLA2"   ) String nivelCla2,//          PIC X(02).
			   @WebParam(name = "CLIENSEC2"   ) String clienSec2,//          PIC 9(09).
			   @WebParam(name = "NIVELCLA3"   ) String nivelCla3,//          PIC X(02).
			   @WebParam(name = "CLIENSEC3"   ) String clienSec3 //          PIC 9(09).
			 ) ;

}
