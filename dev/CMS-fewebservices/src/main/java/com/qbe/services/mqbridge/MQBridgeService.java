package com.qbe.services.mqbridge;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(
		name = "MQBridgeService" 
		,targetNamespace = "http://cms.services.qbe.com/mqbridge"
)
public interface MQBridgeService {
	
	@WebMethod(operationName = "sendAndReceive", action = "sendAndReceive")
	@WebResult( name = "response")
	public String sendAndReceive(
			@WebParam(name = "config" ) String config,
			@WebParam(name = "request" ) String request) throws Exception;

	public String sendAndReceiveSub(@WebParam(name = "len" ) Integer len,@WebParam(name = "request") String request)
			throws Exception;

	
}
