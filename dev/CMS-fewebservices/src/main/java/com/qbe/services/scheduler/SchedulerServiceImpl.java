package com.qbe.services.scheduler;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import com.qbe.scheduler.SchedulerEngine;

public class SchedulerServiceImpl implements SchedulerService {

	protected static ApplicationContext context; 

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:scheduler-beans.xml");
		}
		return context;
	}

	private SchedulerEngine getSchedulerEngine() {
		SchedulerEngine eng = (SchedulerEngine)getContext().getBean(SchedulerEngine.class);
		return eng;
	}


	@Override
	@WebMethod(operationName = "procesarBandeja", action = "procesarBandeja")
	@WebResult(name = "Estado")
	public String procesarBandeja(
			@WebParam(name = "usuario") String usuario) throws Exception {
		SchedulerEngine engine = getSchedulerEngine();
		String resultado = engine.processUserInbox(usuario);
		return resultado;
	}

	@Override
	@WebMethod(operationName = "listarBandeja", action = "listarBandeja")
	@WebResult( name = "Listado")
	public String listarBandeja(
			@WebParam(name = "usuario" ) String usuario,
			@WebParam(name = "terminados" ) Boolean terminados) throws Exception {
		SchedulerEngine engine = getSchedulerEngine();
		String resultado = engine.listUserInbox(usuario, terminados);
		return resultado;
		
	}
	
}
