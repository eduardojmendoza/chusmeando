package com.qbe.services.fewebservices;

import java.util.HashMap;
import java.util.Map;

import com.qbe.vbcompat.framework.VBObjectClass;

/**
 * Mapea ActionCodes a la VbObjectClass que la implementa  
 *  
 * @author ramiro
 *
 */
public class ActionCodesRegistry {

	/**
	 * Singleton
	 */
	private static ActionCodesRegistry instance;
	
	/**
	 * Mapeo entre actionCodes y la clase que lo implementa
	 * 
	 * VER HSBC_Actions.xls como referencia
	 */
	private Map<String, Class<? extends VBObjectClass>> actionImplMap = new HashMap<String, Class<? extends VBObjectClass>>();
	
	private ActionCodesRegistry() throws ClassNotFoundException {

		//Clases del lbawA_SiniestroDenuncia
		actionImplMap.put("lbaw_siniMQGenerico", com.qbe.services.siniestros.impl.lbaw_siniMQGenerico.class);
		
		actionImplMap.put("lbaw_GetConsultaMQ", com.qbe.services.mqgeneric.impl.LBAW_MQMensaje.class);
		actionImplMap.put("lbaw_GetConsultaMQGestion", com.qbe.services.mqgeneric.impl.LBAW_MQMensajeGestion.class);
		actionImplMap.put("nbwsA_MQGenericoAIS", com.qbe.services.segurosOnline.impl.NBWSA_MQGenericoAIS.class);
		
		//Clases de lbawA_OVCotEndosos
		actionImplMap.put("lbaw_CotEndosos", com.qbe.services.cotizadorEndosos.impl.LBAWA_OVCotEndosos.class);
		
		//Clases de OVMQUsuarios
		actionImplMap.put("lbaw_OVAnulxPagoDetalles", com.qbe.services.ovmqusuario.impl.lbaw_OVAnulxPagoDet.class);
		actionImplMap.put("lbaw_OVCliensecxUsu", com.qbe.services.ovmqusuario.impl.lbaw_OVCliensecxUsu.class);
		actionImplMap.put("lbaw_OVDatosGralPoliza", com.qbe.services.ovmqusuario.impl.lbaw_OVDatosGralPol.class);
		actionImplMap.put("lbaw_OVRolesAdministrativa", com.qbe.services.ovmqusuario.impl.lbaw_OVRolesAdmin.class);
		
		//Clases de OVSiniestros
		actionImplMap.put("lbaw_OVSiniConsPago", com.qbe.services.ovmq.impl.lbaw_OVSiniConsPagos.class);
		

		
		//Clases del OVMQEmision
		actionImplMap.put("lbaw_GetListaRetenciones", com.qbe.services.ovmqemision.impl.lbaw_OVGetRetenc.class);
		actionImplMap.put("lbaw_BajaPropuesta", com.qbe.services.ovmqemision.impl.lbaw_OVBajaPropuestas.class);
		actionImplMap.put("lbaw_EmitirPropuesta", com.qbe.services.ovmqemision.impl.lbaw_OVEmisPropuesta.class);
		actionImplMap.put("lbaw_AisPutSolicAUS", com.qbe.services.ovmqemision.impl.lbaw_OVAisPutSolicAUS.class);
		actionImplMap.put("lbaw_OVVerifReimpresion", com.qbe.services.ovmqemision.impl.lbaw_OVVerifImpresion.class);
		actionImplMap.put("lbaw_OVEndososDetallesImpre", com.qbe.services.ovmqemision.impl.lbaw_OVEndososImpres.class);
		actionImplMap.put("lbaw_GetCertMercosur", com.qbe.services.ovmqemision.impl.lbaw_GetCertMercosur.class);
		actionImplMap.put("lbaw_OVRiesgosListadoImpre", com.qbe.services.ovmqemision.impl.lbaw_OVRiesgosImpres.class);
		actionImplMap.put("lbaw_OVImprimirPoliza", com.qbe.services.ovmqemision.impl.lbaw_OVImprimirPoliza.class);
		actionImplMap.put("lbaw_OVGetBinaryFile", com.qbe.services.ovmqemision.impl.lbaw_OVGetBinaryFile.class);
		
		//Clases del OVMQCotizar
		actionImplMap.put("lbaw_OVGetCotiAUS", com.qbe.services.ovmqcotizar.impl.lbaw_OVGetCotiAUS.class);
               
        //Clases del OVMQRiesgos
        actionImplMap.put("lbaw_OVRiesAutos", com.qbe.services.ovmqriesgos.impl.lbaw_OVRiesAutos.class);
        
        //Clases para Acceder a Base de Datos del GetCotiAus
        actionImplMap.put("lbaw_GetNroCot", com.qbe.services.ofvirtuallba.impl.lbaw_GetNroCot.class);
        actionImplMap.put("lbaw_GetParamGral", com.qbe.services.ofvirtuallba.impl.lbaw_GetParamGral.class);
        actionImplMap.put("lbaw_GetSumAseg", com.qbe.services.webmq.impl.lbaw_GetSumAseg.class);
        actionImplMap.put("lbaw_GetPortalComerc", com.qbe.services.ofvirtuallba.impl.lbaw_GetPortalComerc.class);
        
      //Clases de los Componentes Derivados
        actionImplMap.put("GetValidacionSolAU", com.qbe.services.interWSBrok.impl.GetValidacionSolAU.class);
        actionImplMap.put("GetCotizacion_broker", com.qbe.services.interWSBrok.impl.GetCotizacionAU.class);// Especificado por QBE
        actionImplMap.put("GetValidacionCotAU", com.qbe.services.interWSBrok.impl.GetValidacionCotAU.class); //En QBE, no tiene ActionCode
        actionImplMap.put("GetImpreSolAU", com.qbe.services.interWSBrok.impl.GetImpreSolAU.class);
        actionImplMap.put("GetSolicitudAU_broker", com.qbe.services.interWSBrok.impl.GetSolicitudAU.class);

        actionImplMap.put("lbaw_GetTiposIVA", com.qbe.services.ofVirtualLBA.impl.lbaw_GetTiposIVA.class);// Especificado por QBE
        actionImplMap.put("lbaw_GetZonaIngBrut", com.qbe.services.ofVirtualLBA.impl.lbaw_GetZonaIngBrut.class);// Especificado por QBE
        actionImplMap.put("lbaw_GetTipIngBrut", com.qbe.services.ofVirtualLBA.impl.lbaw_GetTipIngBrut.class);// Especificado por QBE
        actionImplMap.put("lbaw_UpdSolicProd", com.qbe.services.productor.impl.lbaw_UpdSolicProd.class);
        actionImplMap.put("lbaw_PutSolicProd", com.qbe.services.productor.impl.lbaw_PutSolicProd.class);
        actionImplMap.put("lbaw_PutSolicitud", com.qbe.services.ofVirtualLBA.impl.lbaw_PutSolicitud.class);
        actionImplMap.put("lbaw_OVPutSolicAUS", com.qbe.services.lbawA_OVSQLCotizar.impl.lbaw_OVPutSolicAUS.class);
        actionImplMap.put("lbaw_OVUpdSolicAUS", com.qbe.services.lbawA_OVSQLCotizar.impl.lbaw_OVUpdSolicAUS.class);
        
        actionImplMap.put("lbaw_GetZona", com.qbe.services.webmq.impl.lbaw_GetZona.class);
        actionImplMap.put("lbaw_OVValidaCuentas", com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaCuentas.class);
        actionImplMap.put("lbaw_OVGetDispRastreo", com.qbe.services.ovmqcotizar.impl.lbaw_OVGetDispRastreo.class);
        actionImplMap.put("lbaw_OVValidaNumDoc", com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaNumDoc.class);
        actionImplMap.put("lbaw_OVValidaIIBB", com.qbe.services.ovmqcotizar.impl.lbaw_OVValidaIIBB.class);
        actionImplMap.put("lbaw_GetVehiculos", com.qbe.services.webmq.impl.lbaw_GetVehiculos.class);

        actionImplMap.put("lbaw_OVMWGen", com.qbe.services.mwGenerico.impl.lbaw_MQMW.class);

        //CMS-Siniestros
        actionImplMap.put("lbaw_siniAppEnvio", com.qbe.services.siniestros.impl.lbaw_siniAppEnvio.class);
        actionImplMap.put("lbaw_siniGetBinPDF", com.qbe.services.siniestros.impl.lbaw_siniGetBinPDF.class);
        actionImplMap.put("lbaw_siniGetItem", com.qbe.services.siniestros.impl.lbaw_siniGetItem.class);
        actionImplMap.put("lbaw_siniGetLista", com.qbe.services.siniestros.impl.lbaw_siniGetLista.class);
        actionImplMap.put("lbaw_siniInsertAIS", com.qbe.services.siniestros.impl.lbaw_siniInsertAIS.class);
        actionImplMap.put("lbaw_siniInsertMID", com.qbe.services.siniestros.impl.lbaw_siniInsertMID.class);
        actionImplMap.put("lbaw_siniInsLogEnvio", com.qbe.services.siniestros.impl.lbaw_siniInsLogEnvio.class);
        actionImplMap.put("lbaw_siniMQGenerico", com.qbe.services.siniestros.impl.lbaw_siniMQGenerico.class);
        actionImplMap.put("lbaw_siniMQMiddle", com.qbe.services.siniestros.impl.lbaw_siniMQMiddle.class);
        actionImplMap.put("lbaw_siniUpdMenvAIS", com.qbe.services.siniestros.impl.lbaw_siniUpdMenvAIS .class);
        actionImplMap.put("lbaw_siniUpdMenvAISX", com.qbe.services.siniestros.impl.lbaw_siniUpdMenvAISX.class);
        actionImplMap.put("lbaw_siniUpdMenvMID", com.qbe.services.siniestros.impl.lbaw_siniUpdMenvMID.class);
        actionImplMap.put("lbaw_XMLtoJPG", com.qbe.services.siniestros.impl.lbaw_XMLtoJPG.class);
        //CMS-SegurosOnline
        actionImplMap.put("nbwsA_Alta", com.qbe.services.segurosOnline.impl.nbwsA_Alta.class);
        actionImplMap.put("nbwsA_ccBaja", com.qbe.services.segurosOnline.impl.nbwsA_ccBaja.class);
        actionImplMap.put("nbwsA_ePolizaImpact", com.qbe.services.segurosOnline.impl.nbwsA_ePolizaImpact.class);
        actionImplMap.put("nbwsA_ExeScheduler", com.qbe.services.segurosOnline.impl.nbwsA_ExeScheduler.class);
        actionImplMap.put("nbwsA_FillScheduler", com.qbe.services.segurosOnline.impl.nbwsA_FillScheduler.class);
        actionImplMap.put("nbwsA_sCobranzas", com.qbe.services.segurosOnline.impl.nbwsA_sCobranzas.class);
        actionImplMap.put("nbwsA_setMisDatos", com.qbe.services.segurosOnline.impl.nbwsA_setMisDatos.class);
        actionImplMap.put("nbwsA_sInicio", com.qbe.services.segurosOnline.impl.nbwsA_sInicio.class);
        actionImplMap.put("nbwsA_SQLGenerico", com.qbe.services.segurosOnline.impl.nbwsA_SQLGenerico.class);
        actionImplMap.put("nbwsA_sSiniestros", com.qbe.services.segurosOnline.impl.nbwsA_sSiniestros.class);
        actionImplMap.put("nbwsA_sImpresos", com.qbe.services.segurosOnline.impl.nbwsA_sImpresos.class);
        actionImplMap.put("nbwsA_ccBuscaSuscr", com.qbe.services.segurosOnline.impl.nbwsA_ccBuscaSuscr.class);
        actionImplMap.put("nbwsA_SolicitudWeb", com.qbe.services.segurosOnline.impl.nbwsA_SolicitudWeb.class);
        actionImplMap.put("nbwsA_Ingresar", com.qbe.services.segurosOnline.impl.nbwsA_Ingresar.class);
        actionImplMap.put("nbwsA_chkPositiveId", com.qbe.services.segurosOnline.impl.nbwsA_chkPositiveId.class);
        
        //CAM
        actionImplMap.put("camA_EnviarMail", com.qbe.services.cam30.camA_EnviarMail.class);
        
		//Tester del Scheduler
		actionImplMap.put("TestScheduler", com.qbe.scheduler.TestSchedulerAction.class);

		
		actionImplMap.put("lbaw_OVOpeEmiDetalles_Paginado", com.qbe.services.paginate.service1109.OVOpeEmiDetallePaginada.class);
		actionImplMap.put("lbaw_OVOpeEmiDetalles_PaginadoSecuencial", com.qbe.services.paginate.service1109.OVOpeEmiDetallePaginadaSecuencial.class);
		actionImplMap.put("lbaw_OVOpePendDetalles_Paginado", com.qbe.services.paginate.service1107.OVOpePendDetallesPaginada.class);
		actionImplMap.put("lbaw_OVClientesConsulta_Paginado", com.qbe.services.paginate.service1010.OVClientesConsulta.class);
		actionImplMap.put("lbaw_OVExigibleDetalles_Paginado", com.qbe.services.paginate.service1401.OVExigibleDetalles.class);
		actionImplMap.put("lbaw_OVDeudaVceDetalles_Paginado", com.qbe.services.paginate.service1404.OVDeudaVceDetalles.class);
		actionImplMap.put("lbaw_OVCartasReclaDetalle_Paginado", com.qbe.services.paginate.service1406.OVCartasReclaDetalle.class);
		actionImplMap.put("lbaw_OVSiniListadoDetalles_Paginado", com.qbe.services.paginate.service1301.OVSiniListadoDetalles.class);
		actionImplMap.put("lbaw_GetDeudaCobradaCanalesDetalle_Paginado", com.qbe.services.paginate.mqgestion.GetDeudaCobradaCanalesDetalle.class);
		actionImplMap.put("lbaw_GetCuponesPendientes_Paginado", com.qbe.services.paginate.mqgestion.GetCuponesPendientes.class);
		actionImplMap.put("lbaw_GetConsultaMQGestion_Paginado", com.qbe.services.paginate.mqgestion.GetConsultaMQGestion.class);
		
		actionImplMap.put("lbaw_OVAnulxPagoDetalles_Paginado", com.qbe.services.paginate.service1410.OVAnulxPagoDetalles.class);
		actionImplMap.put("lbaw_OVSiniConsulta_Paginado",com.qbe.services.paginate.service1302.OVSiniConsulta.class);
		actionImplMap.put("lbaw_OVLOGetCantRecibos_Paginado",com.qbe.services.paginate.service1415.OVLOGetCantRecibos.class);
		actionImplMap.put("lbaw_OVLOGetRecibos_Paginado",com.qbe.services.paginate.service1416.OVLOGetRecibos.class);
        
		// paginado 1340
		actionImplMap.put("lbaw_siniMQGenerico_Paginado", com.qbe.services.paginate.siniMQgenerico.SiniMQGenerico.class);
		 
		//Recupero de PDFs de ColdView
		actionImplMap.put("nbwsA_getDenuItem", com.qbe.services.segurosOnline.impl.nbwsA_getDenuItem.class);
	}
	
	public static ActionCodesRegistry instance() throws ClassNotFoundException {
		if ( instance == null ) instance = new ActionCodesRegistry();
		return instance;
	}
	
	/**
	 * Retorna la VBObjectClass que implementa el actionCode
	 * 
	 * @param actionCode
	 * @return
	 * @throws Exception
	 */
	public Class<? extends VBObjectClass> getImplForActionCode(String actionCode) throws Exception {
		
		if(actionImplMap.get(actionCode) != null){
			return actionImplMap.get(actionCode);
		}
		
		//Clases del OVMQ
		if ( actionCode.startsWith("lbaw_OV")) {
			String packagePrefix = "com.qbe.services.ovmq.impl.";
			try {
				Class<?> loadedClass = Thread.currentThread().getContextClassLoader().loadClass(packagePrefix + actionCode);
				Class<? extends VBObjectClass> ovmqClass = loadedClass.asSubclass(VBObjectClass.class);
				actionImplMap.put(actionCode, ovmqClass);
				return ovmqClass;
			} catch (ClassNotFoundException e) {
				throw new Exception("No existe clase correspondiente al actionCode " + actionCode + ".",e);
			} catch (ClassCastException e) {
				// Ahora en este caso es un error, porque levantó una clase que no respeta la interface
				throw new Exception("La clase correspondiente al actionCode " + actionCode + " no es una VBObjectClass.",e);
			}
			
		}
		return null;
	}
}
