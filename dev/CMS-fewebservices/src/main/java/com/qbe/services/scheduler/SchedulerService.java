package com.qbe.services.scheduler;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(
		name = "SchedulerService" 
		,targetNamespace = "http://cms.services.qbe.com/scheduler"
)
public interface SchedulerService {
	
	@WebMethod(operationName = "procesarBandeja", action = "procesarBandeja")
	@WebResult( name = "Estado")
	public String procesarBandeja(
			@WebParam(name = "usuario" ) String usuario) throws Exception;
	
	@WebMethod(operationName = "listarBandeja", action = "listarBandeja")
	@WebResult( name = "Listado")
	public String listarBandeja(
			@WebParam(name = "usuario" ) String usuario,
			@WebParam(name = "terminados" ) Boolean terminados) throws Exception;
}
