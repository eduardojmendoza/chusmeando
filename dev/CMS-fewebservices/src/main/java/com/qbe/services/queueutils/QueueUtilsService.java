package com.qbe.services.queueutils;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(
		name = "QueueUtilsService" 
		,targetNamespace = "http://cms.services.qbe.com/queueutils")
public interface QueueUtilsService {

	@WebMethod(operationName = "cleanUp", action = "cleanUp")
	public abstract void cleanUp(
			@WebParam( name = "hostname") String hostname, 
			@WebParam( name = "port") int port, 
			@WebParam( name = "channel") String channel,
			@WebParam( name = "queueManager") String queueManager, 
			@WebParam( name = "queueName") String queueName) throws QueueUtilsException;

}