package com.qbe.services.mergetotals;

import java.io.StringWriter;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;

import com.qbe.services.common.merge.MergeException;
import com.qbe.services.common.merge.MergeTotals;

public class MergeTotalsServiceImpl implements MergeTotalsService {

	protected static Logger logger = Logger.getLogger(MergeTotalsServiceImpl.class.getName());
	private static final String ERROR = "Exception al procesar el merge totals";

	public MergeTotalsServiceImpl() {
	}

	@Override
	@WebMethod(operationName = "mergeTotals", action = "mergeTotals")
	@WebResult(name = "merged")
	public Object mergeTotals(@WebParam(name = "left") Object left, @WebParam(name = "right") Object right)
			throws Exception {
		try {
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, String.format("mergeTotals: left = [%s], right = [%s]", left, right));
			}
			Node response = MergeTotals.mergeTotals(((Node) left).getFirstChild(), ((Node) right).getFirstChild());
			logger.log(Level.FINE, String.format("mergeTotals result: response = [%s]", response));
			return response;
		} catch (XPathExpressionException e) {
			logger.log(Level.SEVERE, ERROR, e);
			throw new Exception(ERROR + e.getMessage(), e);
		} catch (MergeException e) {
			logger.log(Level.SEVERE, ERROR, e);
			throw new Exception(ERROR + e.getMessage(), e);
		} catch (ParseException e) {
			logger.log(Level.SEVERE, ERROR, e);
			throw new Exception(ERROR + e.getMessage(), e);
		}
	}
	
	@Override
	@WebMethod(operationName = "mergeTotalsBalanced", action = "mergeTotalsBalanced")
	@WebResult(name = "merged")
	public Object mergeTotalsBalanced(@WebParam(name = "left") Object left, @WebParam(name = "right") Object right)
			throws Exception {
		try {
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, String.format("mergeTotalsBalanced: left = [%s], right = [%s]", left, right));
			}
			

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			StringWriter writer2 = new StringWriter();
			transformer.transform(new DOMSource( ((Node) left).getFirstChild()), new StreamResult(writer));
			String leftString = writer.getBuffer().toString();
			transformer.transform(new DOMSource( ((Node) right).getFirstChild()), new StreamResult(writer2));
			String rightString = writer2.getBuffer().toString();
			
			Node response = MergeTotals.mergeTotalsBalanced( leftString, rightString);
			Node responseWraper = response.getOwnerDocument().createElement("merged");
			responseWraper.appendChild(response);
			
			return responseWraper;
		} catch (MergeException e) {
			logger.log(Level.SEVERE, ERROR, e);
			throw new Exception(ERROR + e.getMessage(), e);
		} 
	}

}
