package com.qbe.services.fewebservices.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import com.qbe.services.cms.osbconnector.BaseOSBClient;
import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.services.exutils.ExceptionUtils;
import com.qbe.services.fewebservices.ActionCodesRegistry;
import com.qbe.services.fewebservices.MigratedComponentService;
import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.services.fewebservices.jaxws.ComposedResponse;
import com.qbe.services.logging.LoggingUtils;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;

@WebService(
	targetNamespace = "http://cms.services.qbe.com/", 
	endpointInterface = "com.qbe.services.fewebservices.MigratedComponentService", 
	portName = "MigratedComponentServicePort", 
	serviceName = "MigratedComponentService")
public class MigratedComponentServiceImpl implements MigratedComponentService{

	public static final String OSB_CONNECTOR_BEAN = "osbConnector";

	protected static Logger logger = Logger.getLogger(MigratedComponentServiceImpl.class.getName());

	protected static ApplicationContext context; 
	/**
	 * Ejecuta el actionCode que recibe, delegando en la VBObjectClass que corresponde
	 */
	@Override
	@WebMethod(operationName = "executeRequest", action = "urn:ExecuteRequest")
	@RequestWrapper(className = "com.qbe.services.fewebservices.jaxws.ExecuteRequest", 
					localName = "executeRequest",targetNamespace = "http://cms.services.qbe.com/")
	@ResponseWrapper(className = "com.qbe.services.fewebservices.jaxws.ExecuteRequestResponse", 
					localName = "executeRequestResponse",targetNamespace = "http://cms.services.qbe.com/")
	public ComposedResponse executeRequest(
			@WebParam(name = "actionCode") String actionCode, 
			@WebParam(name = "Request") AnyXmlElement request,
			@WebParam(name = "contextInfo") String contextInfo)  {

//		System.out.println(LoggingUtils.currentConfiguration());
		
		logger.log(Level.FINE, "Recibido actionCode: " + actionCode);
		try {
			Class<? extends VBObjectClass> clazz = ActionCodesRegistry.instance().getImplForActionCode(actionCode);
			if ( clazz == null ) {
				String msg = "No existe implementación del ActionCode: " + actionCode;
				logger.log(Level.SEVERE, msg);
				return new ComposedResponse(0, AnyXmlElement.newForEstadoWithMessage(msg));
			}
			VBObjectClass vbObject = clazz.newInstance();
			if ( vbObject instanceof BaseOSBClient ) {
				BaseOSBClient osbc = (BaseOSBClient) vbObject;
				OSBConnector conn = (OSBConnector)getContext().getBean(OSB_CONNECTOR_BEAN);
				osbc.setOsbConnector(conn);
				logger.log(Level.FINE, "Usando OSBConnector: " + conn);

			}
			StringHolder sh = new StringHolder();
			String reqXML = request.formatAsRequestString();
			logger.log(Level.FINE, "Recibido request: " + reqXML);
			int resultCode = vbObject.IAction_Execute(reqXML, sh, contextInfo);
			logger.log(Level.FINE, "Resultado de ejecución de actionCode: " + actionCode + ": [code: " + resultCode + ", texto: " + sh.getValue() + "]" );
			if (sh.getValue() == null ) throw new Exception("Respuesta en NULL del componente");
			return new ComposedResponse(resultCode, AnyXmlElement.newWithRootChildrenNodes(sh.getValue()));
		} catch ( ComponentExecutionException e) {
			String msg = e.getMessage();
			if (  msg == null || msg == "" ) {
				msg = getMessageForException(actionCode, e);
			}
			return new ComposedResponse(e.getCode(),AnyXmlElement.newForEstado("false",msg, ExceptionUtils.fullStackTrace(e)));
		} catch ( Exception e) {
			String msg = getMessageForException(actionCode, e);
			logger.log(Level.SEVERE, getMessageForException(actionCode, e), e);
			return new ComposedResponse(1,AnyXmlElement.newForEstado("false",msg, ExceptionUtils.fullStackTrace(e)));
		}
	}

	private String getMessageForException(String actionCode,
			Exception e) {
		return String.format("Exception %s [ %s ] al ejecutar el actionCode %s.", e.getClass().getName(), e.getMessage(), actionCode);
	}

	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:spring-beans.xml");
		}
		return context;
	}

}
