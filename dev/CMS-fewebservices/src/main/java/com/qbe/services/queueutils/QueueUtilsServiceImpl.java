package com.qbe.services.queueutils;

import javax.jws.WebMethod;
import javax.jws.WebParam;

import com.qbe.mq.utils.QueueCleaner;
import com.qbe.mq.utils.QueueCleanerException;

public class QueueUtilsServiceImpl implements QueueUtilsService {

	public QueueUtilsServiceImpl() {
	}

	/* (non-Javadoc)
	 * @see com.qbe.services.queueutils.QueueUtilsService#cleanUp(java.lang.String, int, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@WebMethod(operationName = "cleanUp", action = "cleanUp")
	public void cleanUp(
			@WebParam( name = "hostname") String hostname, 
			@WebParam( name = "port") int port, 
			@WebParam( name = "channel") String channel,
			@WebParam( name = "queueManager") String queueManager, 
			@WebParam( name = "queueName") String queueName) throws QueueUtilsException {
		try {
			QueueCleaner.cleanOldMessages(hostname, port, channel, queueManager, queueName);
		} catch (QueueCleanerException e) {
			throw new QueueUtilsException(e);
		}
	}
}
