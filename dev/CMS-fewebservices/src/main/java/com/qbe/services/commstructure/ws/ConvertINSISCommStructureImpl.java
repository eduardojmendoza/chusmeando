package com.qbe.services.commstructure.ws;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.qbe.integration.CommercialStructureConverter;
import com.qbe.services.commstructure.insis.QBETotalsRs;
import com.qbe.services.commstructure.ov.EST;


/**
 * 
 * FIXME performance para no pasar e JAXB a string marshalleando, puedo usar un JAXB source:
 * 
 *  MyObject o = // get JAXB content tree
       
       // jaxbContext is a JAXBContext object from which 'o' is created.
       JAXBSource source = new JAXBSource( jaxbContext, o );
       
       // set up XSLT transformation
       TransformerFactory tf = TransformerFactory.newInstance();
       Transformer t = tf.newTransformer(new StreamSource("test.xsl"));
       
       // run transformation
       t.transform(source,new StreamResult(System.out));
       
 * @author ramiro
 *
 */
@WebService(
		name = "ConvertINSISCommStructureService" 
		,targetNamespace = "http://cms.services.qbe.com/convertcommstructure"
)
public class ConvertINSISCommStructureImpl implements
		ConvertINSISCommStructureService {
	
	public static Logger logger = Logger.getLogger(ConvertINSISCommStructureImpl.class.getName());

	@Override
	@WebMethod(operationName = "convert", action = "convert")
	@WebResult( name = "EST")
	public EST convert(
			@WebParam(name = "QBETotalsRs", targetNamespace = "http://www.fadata.bg/Insurance_Messages/v3.0/xml/") QBETotalsRs qbeTotalsRS) throws Exception {
		
		logger.log(Level.FINE, String.format("ConvertINSISCommStructureImpl.convert(%s)", qbeTotalsRS));
		JAXBContext insisContext = JAXBContext.newInstance(QBETotalsRs.class);

		Marshaller m = insisContext.createMarshaller();
		m.setProperty(Marshaller.JAXB_FRAGMENT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

		StringWriter sw = new StringWriter();
		m.marshal(qbeTotalsRS, sw);
		logger.log(Level.FINE, String.format("Marshaleado es [%s]", sw.toString()));

		String converted = CommercialStructureConverter.convert(sw.toString());
		logger.log(Level.FINE, String.format("Convertido es [%s]", converted));
		
		JAXBContext ovContext = JAXBContext.newInstance(EST.class);
		Unmarshaller um = ovContext.createUnmarshaller();
		StringReader sr = new StringReader(converted);
		Object o = um.unmarshal (sr);
		if (! (o instanceof EST)) throw new Exception("No pude unmarshalear a un EST, el resultado es de otra clase");
		EST est = (EST) o;
		logger.log(Level.FINE, String.format("Devolviendo [%s]", est));
		return est;
	}

}
