package com.qbe.services.commfilter;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.qbe.services.commfilter.types.CommercialStructureFilter;

public class CommercialStructureFilterServiceImpl implements
		CommercialStructureFilterService {

	@Override
	@WebMethod(operationName = "fetchUserFilter", action = "urn:fetchUserFilter")
	@RequestWrapper(className = "com.qbe.services.commfilter.jaxws.FetchUserFilterRequest", localName = "fetchUserFilterRequest", targetNamespace = "http://cms.services.qbe.com/")
	@ResponseWrapper(className = "com.qbe.services.commfilter.jaxws.FetchUserFilterResponse", localName = "fetchUserFilterResponse", targetNamespace = "http://cms.services.qbe.com/")
	public CommercialStructureFilter fetchUserFilter(
			@WebParam(name = "CIAASCOD") String ciaAsCod,
			@WebParam(name = "USUARCOD") String usuarCod,
			@WebParam(name = "ESTADOMSG") String estadoMsg,
			@WebParam(name = "ERRORMSG") String errorMsg,
			@WebParam(name = "CLIENAS") String clienAs,
			@WebParam(name = "NIVELAS") String nivelAs,
			@WebParam(name = "NIVELCLA1") String nivelCla1,
			@WebParam(name = "CLIENSEC1") String clienSec1,
			@WebParam(name = "NIVELCLA2") String nivelCla2,
			@WebParam(name = "CLIENSEC2") String clienSec2,
			@WebParam(name = "NIVELCLA3") String nivelCla3,
			@WebParam(name = "CLIENSEC3") String clienSec3) {
		// FALTA implementar la lógica. Llamar a un genérico MQ que nos van a pasar
		return new CommercialStructureFilter();
	}

}
