package com.qbe.services.mqbridge;

import javax.annotation.PostConstruct;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.apache.commons.lang.StringUtils;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.qbe.connector.mq.impl.SpringMQConnector;

@WebService(
		name = "MQBridgeService" 
		,targetNamespace = "http://cms.services.qbe.com/mqbridge"
)
public class MQBridgeServiceImpl implements MQBridgeService {

	private static final String AISMQ_BEAN = "aismq";

	protected static ApplicationContext context; 

//	@Autowired ( required=true )
//	@Qualifier ( "aismq")
	private SpringMQConnector aismq;
	
	
	public MQBridgeServiceImpl() {
	}

	@Override
	@WebMethod(operationName = "sendAndReceive", action = "sendAndReceive")
	@WebResult(name = "response")
	public String sendAndReceive(@WebParam(name = "config" ) String config,@WebParam(name = "request") String request)
			throws Exception {

		String response = getAismq().sendMessage(request);
		return response;
	}

	@Override
	@WebMethod(operationName = "sendAndReceiveSub", action = "sendAndReceiveSub")
	@WebResult(name = "response")
	public String sendAndReceiveSub(@WebParam(name = "len" ) Integer len,@WebParam(name = "request") String request)
			throws Exception {

		String response = getAismq().sendMessage(request);
		return StringUtils.left(response, len);
	}

//	@PostConstruct
//	public void init() {
//	    SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
//	}

	public SpringMQConnector getAismq() {
		if ( aismq == null) {
			aismq = (SpringMQConnector) getContext().getBean(AISMQ_BEAN);
		}
		return aismq;
	}

	public void setAismq(SpringMQConnector aismq) {
		this.aismq = aismq;
	}
	
	protected synchronized static ApplicationContext getContext() {

		if ( context == null) {
			context = new ClassPathXmlApplicationContext("classpath*:beansmq.xml");
		}
		return context;
	}

	
}
