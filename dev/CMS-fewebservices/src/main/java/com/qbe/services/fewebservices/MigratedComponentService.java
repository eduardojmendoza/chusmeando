package com.qbe.services.fewebservices;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import com.qbe.services.fewebservices.jaxws.AnyXmlElement;
import com.qbe.services.fewebservices.jaxws.ComposedResponse;

@WebService(
		name = "MigratedComponentService" 
		,targetNamespace = "http://cms.services.qbe.com/"
)
public interface MigratedComponentService  {

	@WebMethod(operationName = "executeRequest", action = "urn:ExecuteRequest")
	@RequestWrapper(className = "com.qbe.services.fewebservices.jaxws.ExecuteRequest", 
					localName = "executeRequest",targetNamespace = "http://cms.services.qbe.com/")
	@ResponseWrapper(className = "com.qbe.services.fewebservices.jaxws.ExecuteRequestResponse", 
					localName = "executeRequestResponse",targetNamespace = "http://cms.services.qbe.com/")
	public ComposedResponse executeRequest(
			@WebParam(name = "actionCode") String actionCode, 
			@WebParam(name = "Request") AnyXmlElement request, 
			@WebParam(name = "contextInfo") String contextInfo) ;

}
