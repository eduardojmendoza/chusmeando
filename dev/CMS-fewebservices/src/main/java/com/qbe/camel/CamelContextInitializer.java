package com.qbe.camel;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.slf4j.LoggerFactory;
 
/**
 * 
 * 
 * 
 * // http://java.dzone.com/articles/exploring-apache-camel-core-2
 * // http://www.javaworld.com/article/2074705/core-java/hello-camel--automatic-file-transfer.html
 * @author ramiro
 *
 */
public class CamelContextInitializer extends GenericServlet {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static Logger logger = Logger.getLogger(CamelContextInitializer.class.getName());

	public void init(ServletConfig config) throws ServletException {
 
		logger.log(Level.INFO, "Inicializando camel routes");
		System.out.println("Inicializando camel routes");
        super.init(config);
 
        final CamelContext camelContext = new DefaultCamelContext();
        try {
			camelContext.addRoutes(createRouteBuilder());
			camelContext.start();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception al inicializar camel", e);
		}
	}

	protected RouteBuilder createRouteBuilder() {
		return new RouteBuilder() {
			public void configure() {
				from("file:/tmp/renovacion/lotes?delay=1000").process(new Processor() {
					public void process(Exchange msg) {
						String content = msg.getIn().getBody(String.class);
						logger.info("Processing file contents: " + content);
						// Acá armar el request soap si queremos mandarlo así
					}
				})
				.setHeader(Exchange.HTTP_METHOD, constant("POST"))
				.to("http://10.1.10.98:8011/proxyRenovacion");
			}
		};
	}
	
        		
	@Override
	public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {
		logger.log(Level.WARNING, "Request al CamelContextInitializer??");
	}
}