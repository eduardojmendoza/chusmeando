package com.qbe.camel;

import java.io.File;
import java.util.logging.Logger;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;


public class FileRouteBuilder extends RouteBuilder {

	private static Logger logger = Logger.getLogger(FileRouteBuilder.class.getName());

	public void configure() {
		from("file:/tmp/renovacion/lotes?delay=1000").process(new Processor() {
			public void process(Exchange msg) {
				File file = msg.getIn().getBody(File.class);
				logger.info("Processing file: " + file);
			}
		});
	}

}
