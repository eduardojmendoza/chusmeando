package com.qbe.services.webmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetZona implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetZona";
  static final String mcteOpID = "0001";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_ProviCod = "//PROVICOD";
  static final String mcteParam_CPACodPo = "//CPACODPO";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLParametros = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    String wvarMensaje = "";
    int wvarStep = 0;
    String wvarResult = "";
    String wvarRamoPCod = "";
    String wvarProviCod = "";
    String wvarCPACodPo = "";
    int wvarPos = 0;
    String wvarPolizSec = "";
    String wvarPolizAnn = "";
    String strParseString = "";
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarRamoPCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD )  );
      wvarProviCod = Strings.right( "0" + XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_ProviCod )  ), 2 );
      wvarCPACodPo = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_CPACodPo )  ) + Strings.fill( 4, " " ), 8 );
      //
      wobjXMLRequest = null;
      //
      // LEVANTO LOS PARAMETROS PARA HOGAR (ParametrosCotizador.xml)
      wvarStep = 20;
      wobjXMLParametros = new XmlDomExtended();
      wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteParamFileName));
      wvarPolizSec = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ((wvarRamoPCod.equals( "HOM1" )) ? ModGeneral.gcteNodosHogar : ModGeneral.gcteNodosAutoScoring) + ModGeneral.gctePOLIZSEC )  );
      wvarPolizAnn = XmlDomExtended.getText( wobjXMLParametros.selectSingleNode( ((wvarRamoPCod.equals( "HOM1" )) ? ModGeneral.gcteNodosHogar : ModGeneral.gcteNodosAutoScoring) + ModGeneral.gctePOLIZANN )  );
      //
      wvarStep = 30;
      wobjXMLParametros = null;
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      wvarMensaje = mcteOpID;
      wvarMensaje = wvarMensaje + "0001";
      wvarMensaje = wvarMensaje + wvarRamoPCod;
      wvarMensaje = wvarMensaje + wvarPolizAnn;
      wvarMensaje = wvarMensaje + wvarPolizSec;
      wvarMensaje = wvarMensaje + "000";
      wvarMensaje = wvarMensaje + "00";
      wvarMensaje = wvarMensaje + wvarProviCod;
      wvarMensaje = wvarMensaje + "01";
      wvarMensaje = wvarMensaje + wvarCPACodPo;

      wvarArea = wvarMensaje;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = 34;


      if( (Strings.mid( strParseString, wvarPos, 4 ).equals( "0000" )) || (Strings.trim( Strings.mid( strParseString, wvarPos, 4 ) ).equals( "" )) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><CODIZONA>0000</CODIZONA><ZONASDES></ZONASDES></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA
        wvarResult = "<CODIZONA>" + Strings.mid( strParseString, wvarPos, 4 ) + "</CODIZONA>";
        wvarResult = wvarResult + "<ZONASDES>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 4), 30 ) ) + "</ZONASDES>";
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      //
      wvarStep = 170;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
