package com.qbe.services.webmq.impl;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.ComponentExecutionException;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetVehiculos implements VBObjectClass
{

	  protected static Logger logger = Logger.getLogger(lbaw_GetVehiculos.class.getName());

	  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetVehiculos";
  /**
   * Const mcteOpID              As String = "0093"
   */
  static final String mcteOpID = "0051";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_AuMarCod = "//AUMARCOD";
  static final String mcteParam_AuVehDes = "//AUVEHDES";
  static final String mcteParam_AuFecVig = "//AUFECVIG";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarAuMarCod = "";
    String wvarAuVehDes = "";
    String wvarAuFecVig = "";
    String wvarNow = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarAuMarCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AuMarCod )  );
      //error: syntax error: near "Not":
      //unsup: If (.selectSingleNode(mcteParam_AuFecVig) Is Not Nothing) Then
      wvarAuFecVig = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AuFecVig )  );
      //error: syntax error: near "Else":
      //unsup: Else
      wvarAuFecVig = DateTime.year( DateTime.now() ) + Strings.right( "00" + DateTime.month( DateTime.now() ), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 );
      //error: syntax error: near "End If":
      //unsup: End If
      //wvarAuVehDes = Left(.selectSingleNode(mcteParam_AuVehDes).Text & String(35, " "), 35) 'MQ 0093
      //MQ 0051
      wvarAuVehDes = Strings.left( XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AuVehDes )  ) + Strings.fill( 65, " " ), 65 );
      //
      // Quitamos esto y ponemos una nueva variable wvarAuVehDes en donde la fecha de vigencia se puede
      // especificar
      //wvarNow = Year(Now) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2)
      //
      wobjXMLRequest = null;
      //
      wvarStep = 20;
      //
      //wvarArea = mcteOpID & wvarAuMarCod & wvarAuVehDes & wvarNow
      wvarArea = mcteOpID + wvarAuMarCod + wvarAuVehDes + wvarAuFecVig;

      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
      StringHolder sHstrParseString = new StringHolder(strParseString);
      wvarMQError = wobjFrame2MQ.execute( wvarArea, sHstrParseString);
      strParseString = sHstrParseString.getValue();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarResult = "";
      //wvarPos = 49 'MQ 0093
      //MQ0051
      wvarPos = 79;

      wvarstrLen = Strings.len( strParseString );

      if( Strings.mid( strParseString, wvarPos, 5 ).equals( "00000" ) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><option value='0'>NO SE ENCONTRARON DATOS</option></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA
        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, wvarPos, 5 ).equals( "00000" )) )
        {
          wvarResult = wvarResult + "<OPTION value='" + Strings.mid( strParseString, wvarPos, 21 ) + "'>";
          //wvarResult = wvarResult & "<![CDATA[" & Trim(Mid(strParseString, wvarPos + 21, 50)) & "]]></OPTION>" 'MQ 0093
          //MQ 0051
          wvarResult = wvarResult + "<![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 21), 80 ) ) + "]]></OPTION>";
          //wvarPos = wvarPos + 71 'MQ 0093
          //MQ 0051
          wvarPos = wvarPos + 101;
        }

        wvarStep = 120;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      //
      wvarStep = 130;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 

      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
			Err.set(_e_);
			logger.log(Level.SEVERE, "en lbaw_GetVehiculos", _e_);

			mobjEventLog.Log(ErrorConstants
					.getValue("EventLog_Category.evtLog_Category_Unexpected"),
					mcteClassName, wcteFnName, wvarStep, Err.getError()
							.getNumber(), "Error= ["
							+ Err.getError().getNumber() + "] - "
							+ Err.getError().getDescription() + " Mensaje:"
							+ mcteOpID + wvarAuMarCod + wvarAuVehDes + wvarNow
							+ " Hora:" + DateTime.now(), vbLogEventTypeError);
			IAction_Execute = 1;

			Err.clear();
			throw new ComponentExecutionException(_e_);
    }
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
