package com.qbe.services.webmq.impl;
import com.qbe.connector.mq.MQProxy;
import com.qbe.services.mqgeneric.impl.ErrorConstants;
import com.qbe.services.mqgeneric.impl.EventLog;
import com.qbe.vbcompat.framework.VBObjectClass;
import com.qbe.vbcompat.string.StringHolder;
import com.qbe.vbcompat.xml.XmlDomExtended;

import diamondedge.util.DateTime;
import diamondedge.util.Err;
import diamondedge.util.Obj;
import diamondedge.util.Strings;
import diamondedge.util.VB;
import diamondedge.util.Variant;
import com.qbe.vbcompat.format.VBFixesUtil;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetSumAseg implements VBObjectClass
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetSumAseg";
  static final String mcteOpID = "0004";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_AuModCod = "//AUMODCOD";
  static final String mcteParam_AuProCod = "//AUPROCOD";
  private Object mobjCOM_Context = null;
  private EventLog mobjEventLog = new EventLog();
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  @Override
	public int IAction_Execute( String Request, StringHolder Response, String ContextInfo ) {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    XmlDomExtended wobjXMLRequest = null;
    XmlDomExtended wobjXMLConfig = null;
    XmlDomExtended wobjXMLResponse = null;
    XmlDomExtended wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    MQProxy wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarAuModCod = "";
    String wvarAuProCod = "";
    int wvarPos = 0;
    int wvarPosCat = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new XmlDomExtended();
      wobjXMLRequest.loadXML( Request );
      wvarAuModCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AuModCod )  );
      wvarAuProCod = XmlDomExtended.getText( wobjXMLRequest.selectSingleNode( mcteParam_AuProCod )  );
      //
      wobjXMLRequest = null;
      //
      wvarStep = 20;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new XmlDomExtended();
      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.gcteConfFileName));
      //
      wvarArea = mcteOpID + wvarAuModCod + wvarAuProCod;
      XmlDomExtended.setText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) , String.valueOf( VBFixesUtil.val( XmlDomExtended.getText( wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" )  ) ) * 40 ) );
      wobjFrame2MQ = MQProxy.getInstance(ModGeneral.gcteConfFileName);
	StringHolder strParseStringHolder = new StringHolder(strParseString);
	wvarMQError = wobjFrame2MQ.execute(wvarArea, strParseStringHolder);
	strParseString = strParseStringHolder.getValue();

      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        return IAction_Execute;
      }
      //
      wvarResult = "";
      wvarPos = 49;

      wvarstrLen = Strings.len( strParseString );

      if( !Strings.mid( strParseString, (wvarPos - 10), 4 ).equals( "0000" ) )
      {
        wvarResult = wvarResult + "<AUTIPCOD>" + Strings.mid( strParseString, wvarPos, 4 ) + "</AUTIPCOD>";
        wvarResult = wvarResult + "<AUTIPCOM>" + Strings.mid( strParseString, (wvarPos + 4), 1 ) + "</AUTIPCOM>";
        wvarResult = wvarResult + "<AUTIPDES>" + Strings.mid( strParseString, (wvarPos + 5), 30 ) + "</AUTIPDES>";
        wvarResult = wvarResult + "<AUCOMDES>" + Strings.mid( strParseString, (wvarPos + 30), 60 ) + "</AUCOMDES>";
        wvarPosCat = wvarPos + 2395;
        wvarResult = wvarResult + "<AUCATCOD>" + Strings.mid( strParseString, (wvarPosCat + 35), 2 ) + "</AUCATCOD>";

        wvarPos = wvarPos + 95;
        wvarResult = wvarResult + "<COMBO>";

        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, wvarPos, 4 ).equals( "0000" )) )
        {
          wvarResult = wvarResult + "<OPTION value='" + Strings.mid( strParseString, wvarPos, 4 ) + "' ";
          String parteEntera = Strings.mid( strParseString, (wvarPos + 4), 13 );
          String parteEnteraConvertida = String.valueOf( Obj.toDouble( parteEntera ) );//ESTO es lo que hay que arreglar. Ver en MQGEN
		  //Si el número no tiene decimales, VB lo formatea SIN .0 al final, mientras que en Java como String.valueOf recibe un double lo escribe con .0
		  if ( parteEnteraConvertida.endsWith(".0")) parteEnteraConvertida = parteEnteraConvertida.substring(0, parteEnteraConvertida.length()-2);

          wvarResult = wvarResult + "sumaseg='" + parteEnteraConvertida + "," + Strings.mid( strParseString, (wvarPos + 4 + 13), 2 ) + "'>";
          wvarResult = wvarResult + Strings.mid( strParseString, wvarPos, 4 ) + "</OPTION>";
          wvarPos = wvarPos + 23;
        }

        wvarResult = wvarResult + "</COMBO>";
        wvarStep = 120;
        //
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=NO SE ENCONTRARON DATOS DEL VEHICULO " + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 130;
      /*TBD mobjCOM_Context.SetComplete() ;*/
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }

	catch( Exception _e_ )
	{
		Err.set( _e_ );
		java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
      logger.log(java.util.logging.Level.SEVERE, "Exception al ejecutar el request", _e_);
      try 
       {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarAuModCod + wvarAuProCod + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    /* TBD mobjCOM_Context = this.GetObjectContext() ;*/
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
