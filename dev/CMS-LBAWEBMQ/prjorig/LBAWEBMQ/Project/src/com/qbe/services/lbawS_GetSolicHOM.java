package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Esto archivo es unico solo para las pruebas UAT de Brokers
 */

public class lbawS_GetSolicHOM implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String gcteConfFileName2 = "MQConfig2.xml";
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbawS_GetSolicHOM";
  static final String mcteOpID = "0045";
  /**
   * Archivo de Config. del MQ
   * Const gcteConfFileName2          As String = "MQConfig.XML"
   * Parametros XML de Entrada
   */
  static final String mcteParam_REQUESTID = "//REQUESTID";
  static final String mcteParam_CODINST = "//CODINST";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CobroCod = "//COBROCOD";
  static final String mcteParam_CobroTip = "//COBROTIP";
  static final String mcteParam_TipoHogar = "//TIPOHOGAR";
  static final String mcteParam_PLANNCOD = "//PLANNCOD";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  static final String mcteRAMOPCOD = "HOM1";
  static final String mcteParam_ClienIVA = "//CLIENIVA";
  static final String mcteParam_NroCot = "//COT_NRO";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_DOMICDOM = "//DOMICDOM";
  static final String mcteParam_DOMICDNU = "//DOMICDNU";
  static final String mcteParam_DOMICPIS = "//DOMICPIS";
  static final String mcteParam_DOMICPTA = "//DOMICPTA";
  static final String mcteParam_DOMICPOB = "//DOMICPOB";
  static final String mcteParam_USOTIPOS = "//USOTIPOS";
  static final String mcteParam_CALDERA = "//CALDERA";
  static final String mcteParam_ASCENSOR = "//ASCENSOR";
  static final String mcteParam_ALARMTIP = "//ALARMTIP";
  static final String mcteParam_GUARDTIP = "//GUARDTIP";
  static final String mcteParam_REJAS = "//REJAS";
  static final String mcteParam_PUERTABLIND = "//PUERTABLIND";
  static final String mcteParam_DISYUNTOR = "//DISYUNTOR";
  static final String mcteParam_BARRIOCOUNT = "//BARRIOCOUNT";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_CoberCod = "COBERCOD";
  static final String mcteParam_ContrMod = "CONTRMOD";
  static final String mcteParam_CapitAsg = "CAPITASG";
  /**
   * DATOS DE LAS CAMPAÑAS
   */
  static final String mcteNodos_Campa = "//Request/CAMPANIAS/CAMPANIA";
  static final String mcteParam_CampaCod = "CAMPACOD";
  /**
   * PRODUCTOR
   */
  static final String mctePORTAL = "LBA";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    String wvarNroCot = "";
    String wvarFechaDia = "";
    String wvarFechaVenc = "";
    String wvarRamoPCod = "";
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarMensaje = "";
    String wvarCodErr = "";
    String wvarRequestID = "";
    String wvarCodinst = "";
    String wvarPolizAnn = "";
    String wvarPolizSec = "";
    String wvarCobroCod = "";
    String wvarCobroTip = "";
    String wvarTipoHogar = "";
    String wvarPlanncod = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarCoberturas = "";
    String wvarCampanias = "";
    String wvarClienIVA = "";
    String wvarCertisec = "";
    String wvarCertipol = "";
    String wvarCertiann = "";
    String wvarDOMICDOM = "";
    String wvarDOMICDNU = "";
    String wvarDOMICPIS = "";
    String wvarDOMICPTA = "";
    String wvarDOMICPOB = "";
    String wvarUSOTIPOS = "";
    String wvarCALDERA = "";
    String wvarASCENSOR = "";
    String wvarALARMTIP = "";
    String wvarGUARDTIP = "";
    String wvarREJAS = "";
    String wvarPUERTABLIND = "";
    String wvarDISYUNTOR = "";
    String wvarBARRIOCOUNT = "";
    String wvarTIPO = "";
    String wvarMARCA = "";
    String wvarMODELO = "";
    String wvarPrima = "";
    String wvarRecargos = "";
    String wvarIVA = "";
    String wvarDerEmi = "";
    String wvarSellados = "";
    String wvarIngBru = "";
    String wvarImpuestos = "";
    String wvarPrecioMensual = "";
    String wvarParseString = "";
    int wvarCounter = 0;
    //
    //
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      wvarStep = 10;
      //
      // CARGO EL XML DE ENTRADA
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );

      wvarStep = 20;

      //CARGO LAS VARIABLES CON LOS DATOS DEL XML
      wvarRequestID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REQUESTID ) */ );
      wvarCodinst = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CODINST ) */ ), 4 );
      wvarNroCot = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroCot ) */ ), 9 );
      wvarFechaDia = String.valueOf( DateTime.year( DateTime.now() ) ) + Strings.right( Strings.fill( 2, "0" ) + String.valueOf( DateTime.month( DateTime.now() ) ), 2 ) + Strings.right( (Strings.fill( 2, "0" ) + String.valueOf( DateTime.day( DateTime.now() ) )), 2 );
      wvarFechaVenc = "99991231";
      wvarRamoPCod = mcteRAMOPCOD;
      wvarPolizAnn = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ ), 2 );
      wvarPolizSec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ ), 6 );
      wvarPlanncod = Strings.right( Strings.fill( 3, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANNCOD ) */ ), 3 );
      wvarCobroCod = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CobroCod ) */ ), 4 );
      wvarCobroTip = Strings.right( Strings.fill( 2, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CobroTip ) */ ), 2 );
      wvarTipoHogar = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TipoHogar ) */ ), 2 );
      wvarProvi = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Provi ) */ ), 2 );
      wvarLocalidadCod = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod ) */ ) + Strings.fill( 8, " " ), 8 );
      wvarClienIVA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienIVA ) */ );
      wvarCertisec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ ), 6 );
      wvarCertipol = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CODINST ) */ ), 4 );
      wvarCertiann = Strings.fill( 4, "0" );
      wvarDOMICDOM = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDOM ) */ ) + Strings.fill( 40, " " ), 40 );
      wvarDOMICDNU = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICDNU ) */ ) + Strings.fill( 5, " " ), 5 );
      wvarDOMICPIS = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPIS ) */ ) + Strings.fill( 4, " " ), 4 );
      wvarDOMICPTA = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPTA ) */ ) + Strings.fill( 4, " " ), 4 );
      wvarDOMICPOB = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DOMICPOB ) */ ) + Strings.fill( 40, " " ), 40 );
      wvarUSOTIPOS = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_USOTIPOS ) */ ), 2 );
      wvarCALDERA = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CALDERA ) */ ) + Strings.fill( 1, " " ), 1 );
      wvarASCENSOR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ASCENSOR ) */ ) + Strings.fill( 1, " " ), 1 );
      wvarALARMTIP = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ALARMTIP ) */ ), 2 );
      wvarGUARDTIP = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_GUARDTIP ) */ ), 2 );
      wvarREJAS = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REJAS ) */ ) + Strings.fill( 1, " " ), 1 );
      wvarPUERTABLIND = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PUERTABLIND ) */ ) + Strings.fill( 1, " " ), 1 );
      wvarDISYUNTOR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DISYUNTOR ) */ ) + Strings.fill( 1, " " ), 1 );
      wvarBARRIOCOUNT = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_BARRIOCOUNT ) */ ) + Strings.fill( 14, " " ), 14 );

      wvarStep = 30;
      //CARGO LAS COBERTURAS
      wvarCoberturas = "";

      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 3, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CoberCod ) */ ), 3 );
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_ContrMod ) */ ), 2 );
        //NroseCor
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_ContrMod ) */ ), 2 );
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 15, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CapitAsg ) */ ), 15 );
        //SelecMod
        wvarCoberturas = wvarCoberturas + "1";
      }

      for( wvarCounter = wobjXMLList.getLength(); wvarCounter <= 49; wvarCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.fill( 22, "0" ) + " ";
      }
      //FIN COBERTURAS
      wvarStep = 40;
      //CARGO LAS CAMPANIAS
      wvarCampanias = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Campa ) */;

      for( wvarCounter = 0; wvarCounter <= (wobjXMLList.getLength() - 1); wvarCounter++ )
      {
        wvarCampanias = wvarCampanias + Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvarCounter ).selectSingleNode( mcteParam_CampaCod ) */ ), 4 );
      }

      for( wvarCounter = wobjXMLList.getLength(); wvarCounter <= 9; wvarCounter++ )
      {
        wvarCampanias = wvarCampanias + Strings.fill( 4, " " );
      }
      //FIN CAMPANIAS
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 50;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );

      //
      wvarMensaje = mcteOpID + wvarNroCot;
      // precio que va a calcular
      wvarMensaje = wvarMensaje + Strings.fill( 18, "0" );
      // zona
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" );
      wvarMensaje = wvarMensaje + wvarClienIVA;

      // *** PRODUCTOR ***
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" );

      //CIAASCOD
      wvarMensaje = wvarMensaje + "0001";
      //RAMO = HOM1 - Lo toma del archivo ParametrosBroker.xml
      wvarMensaje = wvarMensaje + wvarRamoPCod;
      wvarMensaje = wvarMensaje + wvarPolizAnn;
      wvarMensaje = wvarMensaje + wvarPolizSec;
      wvarMensaje = wvarMensaje + wvarCertipol;
      wvarMensaje = wvarMensaje + wvarCertiann;
      //CertiSec
      wvarMensaje = wvarMensaje + wvarCertisec;
      //SupleNum
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" );
      //EfectAnn
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaDia, 1, 4 );
      //EfectMes
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaDia, 5, 2 );
      //EfectDia
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaDia, 7, 2 );

      //VenvgAnn
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 1, 4 );
      //VenvgMes
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 5, 2 );
      //VenvgDia
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 7, 2 );

      //VeninAnn
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 1, 4 );
      //VeninMes
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 5, 2 );
      //VeninDia
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 7, 2 );

      //EmisiAnn
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaDia, 1, 4 );
      //EmisiMes
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaDia, 5, 2 );
      //EmisiDia
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaDia, 7, 2 );

      //EfevgAnn
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 1, 4 );
      //EfevgMes
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 5, 2 );
      //EfevgDia
      wvarMensaje = wvarMensaje + Strings.mid( wvarFechaVenc, 7, 2 );

      //CobroFor --> Siempre va 1
      wvarMensaje = wvarMensaje + "1";

      wvarMensaje = wvarMensaje + wvarCobroCod;
      wvarMensaje = wvarMensaje + wvarCobroTip;

      //CapitAsg
      wvarMensaje = wvarMensaje + Strings.fill( 15, "0" );
      //CapitImp
      wvarMensaje = wvarMensaje + Strings.fill( 15, "0" );
      //ValorRie
      wvarMensaje = wvarMensaje + Strings.fill( 15, "0" );

      wvarMensaje = wvarMensaje + wvarTipoHogar;
      //AlturNum
      wvarMensaje = wvarMensaje + "00";
      wvarMensaje = wvarMensaje + wvarUSOTIPOS;
      wvarMensaje = wvarMensaje + wvarCALDERA;
      wvarMensaje = wvarMensaje + wvarASCENSOR;
      wvarMensaje = wvarMensaje + wvarALARMTIP;
      wvarMensaje = wvarMensaje + wvarGUARDTIP;
      wvarMensaje = wvarMensaje + wvarREJAS;
      wvarMensaje = wvarMensaje + wvarPUERTABLIND;
      wvarMensaje = wvarMensaje + wvarDISYUNTOR;
      //ZoRieCod
      wvarMensaje = wvarMensaje + " ";
      //swPropie
      wvarMensaje = wvarMensaje + " ";
      //MascoCod
      wvarMensaje = wvarMensaje + "00";

      wvarMensaje = wvarMensaje + wvarPlanncod;
      //ConceCod
      wvarMensaje = wvarMensaje + Strings.fill( 3, "0" );
      //FranqCod
      wvarMensaje = wvarMensaje + "  ";

      //DomicCal
      wvarMensaje = wvarMensaje + Strings.fill( 5, " " );
      wvarMensaje = wvarMensaje + wvarDOMICDOM;
      wvarMensaje = wvarMensaje + wvarDOMICDNU;
      wvarMensaje = wvarMensaje + wvarDOMICPIS;
      wvarMensaje = wvarMensaje + wvarDOMICPTA;
      //DomicEsc
      wvarMensaje = wvarMensaje + " ";

      wvarMensaje = wvarMensaje + wvarDOMICPOB;
      wvarMensaje = wvarMensaje + wvarLocalidadCod;
      wvarMensaje = wvarMensaje + wvarProvi;
      //PaissCCC
      wvarMensaje = wvarMensaje + "00";
      //BlockCod
      wvarMensaje = wvarMensaje + Strings.fill( 8, " " );

      wvarMensaje = wvarMensaje + wvarBARRIOCOUNT;

      wvarMensaje = wvarMensaje + wvarCoberturas;
      wvarMensaje = wvarMensaje + wvarCampanias;
      //
      wvarArea = wvarMensaje;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + wvarParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";
      //
      wvarStep = 150;
      if( Strings.toUpperCase( Strings.left( wvarParseString, 2 ) ).equals( "OK" ) )
      {
        wvarResult = "<LBA_WS res_code=\"OK\" res_msg=\"\"><Response>" + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REQUESTID ) */ ) + "</REQUESTID>" + "<COT_NRO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroCot ) */ ) + "</COT_NRO>" + "<PRECIOS>" + "<PRECIO>" + "<PRIMA>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 3, 11 ) ) / 100) ), "0.00" ) + "</PRIMA>" + "<RECARGOS>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 14, 11 ) ) / 100) ), "0.00" ) + "</RECARGOS>" + "<IVA>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 25, 11 ) ) / 100) ), "0.00" ) + "</IVA>" + "<DEREMI>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 36, 11 ) ) / 100) ), "0.00" ) + "</DEREMI>" + "<SELLADOS>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 47, 11 ) ) / 100) ), "0.00" ) + "</SELLADOS>" + "<INGBRU>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 58, 11 ) ) / 100) ), "0.00" ) + "</INGBRU>" + "<IMPUESTOS>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 69, 11 ) ) / 100) ), "0.00" ) + "</IMPUESTOS>" + "<VALOR>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( wvarParseString, 80, 11 ) ) / 100) ), "0.00" ) + "</VALOR>" + "</PRECIO>" + "</PRECIOS>" + "<TIEMPOPROCESO>" + String.valueOf( Obj.toDouble( Strings.mid( wvarParseString, 91, 6 ) ) ) + "</TIEMPOPROCESO>" + "<CODIZONA>" + String.valueOf( Obj.toDouble( Strings.mid( wvarParseString, 97, 4 ) ) ) + "</CODIZONA>" + "</Response></LBA_WS>";
      }
      else
      {
        wvarResult = "<LBA_WS res_code=\"" + Strings.toUpperCase( Strings.left( wvarParseString, 2 ) ) + "\" res_msg=\"\"></LBA_WS>";
      }


      //
      wvarStep = 160;
      Response.set( wvarResult );
      //
      wvarStep = 170;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        if( wvarCodErr.equals( "" ) )
        {
          wvarCodErr = "-50";
          wvarMensaje = "Ocurrio un error inesperado en la ejecucion del componente";
        }
        Response.set( "<LBA_WS res_code=\"" + wvarCodErr + "\" res_msg=\"" + wvarMensaje + "\"></LBA_WS>" );

        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetComplete() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
