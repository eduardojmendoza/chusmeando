package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetDatosClientes implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetDatosClientes";
  static final String mcteOpID = "0018";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_Poliza = "//POLIZA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarPoliza = "";
    String wvarCiaAsCod = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCliensec = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Cliensec ) */ ), 9 );
      wvarPoliza = Strings.right( Strings.fill( 30, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ ), 30 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 60;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarCliensec + wvarPoliza;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 150;
      wvarResult = "";
      wvarPos = 44;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 160;
      if( (!Strings.trim( Strings.mid( strParseString, wvarPos, 70 ) ).equals( "" )) && (wvarPos < wvarstrLen) )
      {
        wvarResult = wvarResult + "<DATOS>";
        wvarResult = wvarResult + "<CLIENTE>";
        wvarResult = wvarResult + "<NOMBRE><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 70 ) ) + "]]></NOMBRE>";
        wvarResult = wvarResult + "<TIPODOCU><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 70), 20 ) ) + "]]></TIPODOCU>";
        wvarResult = wvarResult + "<NUMEDOCU>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 90), 11 ) ) + "</NUMEDOCU>";
        wvarResult = wvarResult + "<NACIONALI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 101), 40 ) ) + "]]></NACIONALI>";
        wvarResult = wvarResult + "<SEXO><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 141), 20 ) ) + "]]></SEXO>";
        wvarResult = wvarResult + "<FECNAC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 161), 10 ) ) + "]]></FECNAC>";
        wvarResult = wvarResult + "<ESTCIVIL><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 171), 20 ) ) + "]]></ESTCIVIL>";
        wvarResult = wvarResult + "<FUMADOR>" + ((Strings.mid( strParseString, (wvarPos + 191), 1 ).equals( "S" )) ? "SI" : "NO") + "</FUMADOR>";
        wvarResult = wvarResult + "<FECALTA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 192), 10 ) ) + "]]></FECALTA>";
        wvarResult = wvarResult + "<DIEZUR>" + ((Strings.mid( strParseString, (wvarPos + 202), 1 ).equals( "Z" )) ? "ZURDO" : "DIESTRO") + "</DIEZUR>";
        wvarResult = wvarResult + "<FALLLECIM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 203), 10 ) ) + "]]></FALLLECIM>";
        wvarResult = wvarResult + "<CALLEPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 213), 60 ) ) + "]]></CALLEPART>";
        wvarResult = wvarResult + "<CPOPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 273), 5 ) ) + "]]></CPOPART>";
        wvarResult = wvarResult + "<LOCALPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 278), 40 ) ) + "]]></LOCALPART>";
        wvarResult = wvarResult + "<PROVPART><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 318), 20 ) ) + "]]></PROVPART>";
        wvarResult = wvarResult + "<CALLECOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 338), 60 ) ) + "]]></CALLECOM>";
        wvarResult = wvarResult + "<CPOCOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 398), 5 ) ) + "]]></CPOCOM>";
        wvarResult = wvarResult + "<LOCALCOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 403), 40 ) ) + "]]></LOCALCOM>";
        wvarResult = wvarResult + "<PROVCOM><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 443), 20 ) ) + "]]></PROVCOM>";
        wvarResult = wvarResult + "</CLIENTE>";
        wvarPos = wvarPos + 462;
        //
        wvarStep = 170;
        wvarResult = wvarResult + "<CONTACTOS>";
        while( (wvarPos < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos, 30 ) ).equals( "" )) )
        {
          //
          wvarStep = 180;
          wvarResult = wvarResult + "<CONTACTO>";
          wvarResult = wvarResult + "<DESCRIPCION><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 30 ) ) + "]]></DESCRIPCION>";
          wvarResult = wvarResult + "<DATOCONT><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 30), 50 ) ) + "]]></DATOCONT>";
          wvarResult = wvarResult + "</CONTACTO>";
          //
          wvarPos = wvarPos + 80;
        }
        wvarStep = 190;
        wvarResult = wvarResult + "</CONTACTOS>";
        wvarResult = wvarResult + "</DATOS>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado= 'false' mensaje='NO SE ENCONTRARON DATOS DEL CLIENTE' /></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 

      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCliensec + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
