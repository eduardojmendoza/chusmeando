package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_HSGetCotizacion implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_HSGetCotizacion";
  static final String mcteOpID = "0011";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_TipoHogar = "//TIPOHOGAR";
  static final String mcteParam_ProviCod = "//PROVICOD";
  static final String mcteParam_Localidad = "//LOCALIDAD";
  /**
   * DATOS DE LAS COBERTURAS
   */
  static final String mcteNodos_Cober = "//Request/COBERTURAS/COBERTURA";
  static final String mcteParam_CoberCod = "COBERCOD";
  static final String mcteParam_ContrMod = "CONTRMOD";
  static final String mcteParam_CapitAsg = "CAPITASG";
  /**
   * PRODUCTOR
   */
  static final String mcteParam_AgeCod = "//AGECOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    lbawA_OfVirtualLBA.lbaw_GetPortalComerc wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarRamoPCod = "";
    String wvarAgentCla = "";
    String wvarPlanncod = "";
    String wvarPolizSec = "";
    String wvarPolizAnn = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaVenc = "";
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarMensaje = "";
    String wvarTipoHogar = "";
    String wvarProviCod = "";
    String wvarLocalidad = "";
    String wvarCoberturas = "";
    String wvarPrecio = "";
    String strParseString = "";
    int wvariCounter = 0;
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // Obtengo el numero de cotizacion
      wvarStep = 10;
      wvarRequest = "<Request></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetNroCot();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      wvarStep = 20;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( wvarResponse );
      wvarCotiID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//NROCOT" ) */ );
      wvarFechaDia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( "//FECHA_DIA" ) */ );
      wvarFechaVenc = "99991231";
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarTipoHogar = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_TipoHogar ) */ ), 2 );
      wvarProviCod = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ProviCod ) */ ), 2 );
      wvarLocalidad = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Localidad ) */ ) + Strings.fill( 8, " " ), 8 );

      wvarCoberturas = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Cober ) */;
      wvarStep = 40;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.right( "000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CoberCod ) */ ), 3 );
        wvarCoberturas = wvarCoberturas + Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_ContrMod ) */ ), 2 );
        wvarCoberturas = wvarCoberturas + "00";
        wvarCoberturas = wvarCoberturas + Strings.right( Strings.fill( 15, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CapitAsg ) */ ), 15 );
        wvarCoberturas = wvarCoberturas + "S";
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 98; wvariCounter++ )
      {
        wvarCoberturas = wvarCoberturas + Strings.fill( 22, "0" ) + " ";
      }
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 50;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
      wvarStep = 70;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarRamoPCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gcteRAMOPCOD ) */ );
      wvarPlanncod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePLANNCOD ) */ );
      wvarPolizSec = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZSEC ) */ );
      wvarPolizAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosHogar + ModGeneral.gctePOLIZANN ) */ );
      //
      wvarStep = 80;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      // BUSCO LOS DATOS DE TELEFONO
      wvarStep = 90;
      wvarRequest = "<Request><PORTAL>LBA</PORTAL><RAMOPCOD>" + wvarRamoPCod + "</RAMOPCOD></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      //
      wvarStep = 100;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.loadXML( wvarResponse );
      wvarAgentCla = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( mcteParam_AgeCod ) */ ) );
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      wobjClass = (lbawA_OfVirtualLBA.lbaw_GetPortalComerc) null;
      //
      wvarMensaje = mcteOpID + Strings.right( Strings.fill( 9, "0" ) + wvarCotiID, 9 ) + Strings.fill( 18, "0" );
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" ) + "3";
      wvarMensaje = wvarMensaje + wvarAgentCla;
      wvarMensaje = wvarMensaje + "0001" + wvarRamoPCod;
      wvarMensaje = wvarMensaje + wvarPolizAnn + wvarPolizSec;
      wvarMensaje = wvarMensaje + Strings.fill( 4, "0" ) + Strings.fill( 4, "0" ) + Strings.fill( 6, "0" ) + Strings.fill( 4, "0" );
      wvarMensaje = wvarMensaje + wvarFechaDia + wvarFechaVenc + wvarFechaVenc + wvarFechaDia + wvarFechaVenc;
      wvarMensaje = wvarMensaje + "1" + "0004" + "  ";
      wvarMensaje = wvarMensaje + Strings.fill( 15, "0" ) + Strings.fill( 15, "0" ) + Strings.fill( 15, "0" );
      wvarMensaje = wvarMensaje + wvarTipoHogar + "00" + "00" + " " + " " + "00" + "00" + " " + " " + " " + " " + " " + "00";
      wvarMensaje = wvarMensaje + wvarPlanncod + Strings.fill( 3, "0" ) + "  ";
      wvarMensaje = wvarMensaje + Strings.fill( 5, " " ) + Strings.fill( 40, " " ) + Strings.fill( 5, " " ) + Strings.fill( 4, " " ) + Strings.fill( 4, " " ) + " ";
      wvarMensaje = wvarMensaje + Strings.fill( 40, " " ) + wvarLocalidad + wvarProviCod + "00" + Strings.fill( 8, " " ) + Strings.fill( 14, " " );
      wvarMensaje = wvarMensaje + wvarCoberturas;
      wvarMensaje = wvarMensaje + Strings.fill( 40, " " );

      wvarArea = wvarMensaje;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";

      wvarPrecio = ModGeneral.FormatearNumero( Strings.replace( String.valueOf( Obj.toDouble( Strings.mid( strParseString, 10, 18 ) ) / 100 ), ",", "." ) );
      wvarResult = "<COT_NRO>" + wvarCotiID + "</COT_NRO><COTIZACION>" + wvarPrecio + "</COTIZACION>";
      //
      wvarStep = 180;
      Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      //
      wvarStep = 190;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
