package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetClientes implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetClientes";
  static final String mcteOpID = "0027";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Clisecag = "//CLISECAG";
  static final String mcteParam_Documtips = "//DOCUMTIPS";
  static final String mcteParam_Documdats = "//DOCUMDATS";
  static final String mcteParam_Clienapes = "//CLIENAPES";
  static final String mcteParam_Ramopcods = "//RAMOPCODS";
  static final String mcteParam_Polizanns = "//POLIZANNS";
  static final String mcteParam_Polizsecs = "//POLIZSECS";
  static final String mcteParam_Certipols = "//CERTIPOLS";
  static final String mcteParam_Certianns = "//CERTIANNS";
  static final String mcteParam_Certisecs = "//CERTISECS";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarClisecag = "";
    String wvarDocumtips = "";
    String wvarDocumdats = "";
    String wvarClienapes = "";
    String wvarRamopcods = "";
    String wvarPolizanns = "";
    String wvarPolizsecs = "";
    String wvarCertipols = "";
    String wvarCertianns = "";
    String wvarCertisecs = "";
    String wvarCliensec = "";
    String wvarCiaAsCod = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarClisecag = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Clisecag ) */ ), 9 );
      wvarDocumtips = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Documtips ) */ ), 2 );
      wvarDocumdats = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Documdats ) */ ) + Strings.fill( 11, " " ), 11 );
      wvarClienapes = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Clienapes ) */ ) + Strings.fill( 40, " " ), 40 );
      wvarRamopcods = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Ramopcods ) */ ), 4 );
      wvarPolizanns = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Polizanns ) */ ) + Strings.fill( 2, "0" ), 2 );
      wvarPolizsecs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Polizsecs ) */ ) + Strings.fill( 6, "0" ), 6 );
      wvarCertipols = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certipols ) */ ) + Strings.fill( 4, "0" ), 4 );
      wvarCertianns = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certianns ) */ ) + Strings.fill( 4, "0" ), 4 );
      wvarCertisecs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certisecs ) */ ) + Strings.fill( 6, "0" ), 6 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 60;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarClisecag + wvarDocumtips + wvarDocumdats + wvarClienapes + wvarRamopcods + wvarPolizanns + wvarPolizsecs + wvarCertipols + wvarCertianns + wvarCertisecs;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 150;
      wvarResult = "";
      wvarPos = 93;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 160;
      if( !Strings.mid( strParseString, wvarPos, 2 ).equals( "OK" ) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='NO SE ENCONTRARON DATOS DEL CLIENTE'/></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA saca parametros de enteada y ER/OK
        strParseString = Strings.trim( Strings.mid( strParseString, 93, Strings.len( strParseString ) - 93 ) );
        strParseString = Strings.trim( Strings.mid( strParseString, 3, Strings.len( strParseString ) - 2 ) );
        wvarstrLen = Strings.len( strParseString );

        wvarPos = 1;
        wvarResult = wvarResult + "<CLIENTES>";
        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, wvarPos, 2 ).equals( "00" )) )
        {
          //
          wvarResult = wvarResult + "<CLIENTE>";
          wvarResult = wvarResult + "<DOCUMTIP><![CDATA[" + Strings.mid( strParseString, wvarPos, 2 ) + "]]></DOCUMTIP>";
          wvarResult = wvarResult + "<DOCUMDAT><![CDATA[" + Strings.mid( strParseString, (wvarPos + 2), 11 ) + "]]></DOCUMDAT>";
          wvarResult = wvarResult + "<CLIENSEC><![CDATA[" + Strings.mid( strParseString, (wvarPos + 13), 9 ) + "]]></CLIENSEC>";
          wvarResult = wvarResult + "<CLIENDES><![CDATA[" + Strings.mid( strParseString, (wvarPos + 22), 30 ) + "]]></CLIENDES>";
          wvarResult = wvarResult + "<TOMARIES><![CDATA[" + Strings.mid( strParseString, (wvarPos + 52), 30 ) + "]]></TOMARIES>";
          wvarResult = wvarResult + "<SITUCPOL><![CDATA[" + Strings.mid( strParseString, (wvarPos + 82), 12 ) + "]]></SITUCPOL>";
          wvarResult = wvarResult + "<RAMOPCOD><![CDATA[" + Strings.mid( strParseString, (wvarPos + 94), 4 ) + "]]></RAMOPCOD>";
          wvarResult = wvarResult + "<POLIZANN><![CDATA[" + Strings.mid( strParseString, (wvarPos + 98), 2 ) + "]]></POLIZANN>";
          wvarResult = wvarResult + "<POLIZSEC><![CDATA[" + Strings.mid( strParseString, (wvarPos + 100), 6 ) + "]]></POLIZSEC>";
          wvarResult = wvarResult + "<CERTIPOL><![CDATA[" + Strings.mid( strParseString, (wvarPos + 106), 4 ) + "]]></CERTIPOL>";
          wvarResult = wvarResult + "<CERTIANN><![CDATA[" + Strings.mid( strParseString, (wvarPos + 110), 4 ) + "]]></CERTIANN>";
          wvarResult = wvarResult + "<CERTISEC><![CDATA[" + Strings.mid( strParseString, (wvarPos + 114), 6 ) + "]]></CERTISEC>";
          wvarResult = wvarResult + "<SUPLENUM><![CDATA[" + Strings.mid( strParseString, (wvarPos + 120), 4 ) + "]]></SUPLENUM>";
          wvarResult = wvarResult + "</CLIENTE>";
          //
          wvarPos = wvarPos + 124;
        }
        wvarStep = 190;
        wvarResult = wvarResult + "</CLIENTES>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 

      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarClisecag + wvarDocumtips + wvarDocumdats + wvarClienapes + wvarRamopcods + wvarPolizanns + wvarPolizsecs + wvarCertipols + wvarCertianns + wvarCertisecs + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
