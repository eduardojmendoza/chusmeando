package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetDetReciboNew implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetDetReciboNew";
  static final String mcteOpID = "0031";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_RECIBANN = "//RECIBANN";
  static final String mcteParam_RECIBTIP = "//RECIBTIP";
  static final String mcteParam_RECIBSEC = "//RECIBSEC";
  /**
   *  Parametros de configuracion
   */
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarCiaAsCod = "";
    String wvarRamoPCod = "";
    String wvarPolizAnn = "";
    String wvarPolizSec = "";
    String wvarCertipol = "";
    String wvarCertiann = "";
    String wvarCertisec = "";
    String wvarSuplenum = "";
    String wvarRecibAnn = "";
    String wvarRecibTip = "";
    String wvarRecibSec = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarRamoPCod = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ ), 4 );
      wvarPolizAnn = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ ), 2 );
      wvarPolizSec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ ), 6 );
      wvarCertipol = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ ), 4 );
      wvarCertiann = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ ), 4 );
      wvarCertisec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ ), 6 );
      wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ ), 4 );
      wvarRecibAnn = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RECIBANN ) */ ), 2 );
      wvarRecibTip = Strings.right( Strings.fill( 1, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RECIBTIP ) */ ), 1 );
      wvarRecibSec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RECIBSEC ) */ ), 6 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 60;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarRecibAnn + wvarRecibTip + wvarRecibSec + wvarRamoPCod + wvarPolizAnn + wvarPolizSec + wvarCertipol + wvarCertiann + wvarCertisec + wvarSuplenum;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 150;
      wvarResult = "";
      //44
      wvarPos = 14;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 160;
      if( !Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" ) )
      {
        wvarResult = wvarResult + "<DETALLE_RECIBOS>";
        wvarStep = 170;
        wvarPos = wvarPos + 2;
        while( (wvarPos < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 90), 15 ) ).equals( "" )) )
        {
          wvarStep = 180;
          wvarResult = wvarResult + "<DETALLE>";
          wvarResult = wvarResult + "<RAMOPCOD><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 4 ) ) + "]]></RAMOPCOD>";
          wvarResult = wvarResult + "<POLIZANN>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 4), 2 ) ) + "</POLIZANN>";
          wvarResult = wvarResult + "<POLIZSEC>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 6), 6 ) ) + "</POLIZSEC>";
          wvarResult = wvarResult + "<CERTIPOL>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 12), 4 ) ) + "</CERTIPOL>";
          wvarResult = wvarResult + "<CERTIANN>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 16), 4 ) ) + "</CERTIANN>";
          wvarResult = wvarResult + "<CERTISEC>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 20), 6 ) ) + "</CERTISEC>";
          wvarResult = wvarResult + "<SUPLENUM>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 26), 4 ) ) + "</SUPLENUM>";
          wvarResult = wvarResult + "<GRUPOASEG><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 30), 60 ) ) + "]]></GRUPOASEG>";
          wvarResult = wvarResult + "<PRIMAIMP>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 90), 15 ) ) / 100) ), ".", "," ) + "</PRIMAIMP>";
          wvarResult = wvarResult + "<RECTOIMP>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 105), 15 ) ) / 100) ), ".", "," ) + "</RECTOIMP>";
          wvarResult = wvarResult + "<IVAIMPOR>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 120), 15 ) ) / 100) ), ".", "," ) + "</IVAIMPOR>";
          wvarResult = wvarResult + "<IVARETEN>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 135), 15 ) ) / 100) ), ".", "," ) + "</IVARETEN>";
          wvarResult = wvarResult + "<SIGNO>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 150), 1 ) ) + "</SIGNO>";
          wvarResult = wvarResult + "<EMISIFEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 151), 10 ) ) + "]]></EMISIFEC>";
          wvarResult = wvarResult + "<EFECFEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 161), 10 ) ) + "]]></EFECFEC>";
          wvarResult = wvarResult + "<VENCFEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 171), 10 ) ) + "]]></VENCFEC>";
          wvarResult = wvarResult + "<SITUCDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 181), 10 ) ) + "]]></SITUCDES>";
          wvarResult = wvarResult + "<SITUCFEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 191), 10 ) ) + "]]></SITUCFEC>";
          wvarResult = wvarResult + "<COBROCODDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 201), 20 ) ) + "]]></COBROCODDES>";
          wvarResult = wvarResult + "<COBROTIPDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 221), 20 ) ) + "]]></COBROTIPDES>";
          wvarResult = wvarResult + "<BANCOCODDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 241), 20 ) ) + "]]></BANCOCODDES>";
          wvarResult = wvarResult + "<SUCURCODDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 261), 20 ) ) + "]]></SUCURCODDES>";
          wvarResult = wvarResult + "<CUENTNUME><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 281), 20 ) ) + "]]></CUENTNUME>";
          wvarResult = wvarResult + "</DETALLE>";

          wvarPos = wvarPos + 301;
        }
        wvarStep = 190;
        wvarResult = wvarResult + "</DETALLE_RECIBOS>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='NO SE ENCONTRARON DATOS DE RECIBOS'/></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 

      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarRecibAnn + wvarRecibTip + wvarRecibSec + wvarRamoPCod + wvarPolizAnn + wvarPolizSec + wvarCertipol + wvarCertiann + wvarCertisec + wvarSuplenum + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
