package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetRecibos implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetRecibos";
  static final String mcteOpID = "0030";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  static final String mcteParam_FECDESDE = "//FECDESDE";
  static final String mcteParam_FECHASTA = "//FECHASTA";
  /**
   *  Parametros de configuracion
   */
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarCiaAsCod = "";
    String wvarRamoPCod = "";
    String wvarPolizAnn = "";
    String wvarPolizSec = "";
    String wvarCertipol = "";
    String wvarCertiann = "";
    String wvarCertisec = "";
    String wvarSuplenum = "";
    String wvarFecDesde = "";
    String wvarFecHasta = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarRamoPCod = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ ), 4 );
      wvarPolizAnn = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ ), 2 );
      wvarPolizSec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ ), 6 );
      wvarCertipol = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ ), 4 );
      wvarCertiann = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ ), 4 );
      wvarCertisec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ ), 6 );
      wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ ), 4 );
      wvarFecDesde = Strings.right( Strings.fill( 8, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECDESDE ) */ ), 8 );
      wvarFecHasta = Strings.right( Strings.fill( 8, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_FECHASTA ) */ ), 8 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      if( wvarFecDesde.equals( "00000000" ) )
      {
        wvarFecDesde = (DateTime.year( DateTime.now() ) - 1) + Strings.format( DateTime.month( DateTime.now() ), "00" ) + Strings.format( DateTime.day( DateTime.now() ), "00" );
      }

      if( wvarFecHasta.equals( "00000000" ) )
      {
        wvarFecHasta = DateTime.year( DateTime.now() ) + Strings.format( DateTime.month( DateTime.now() ), "00" ) + Strings.format( DateTime.day( DateTime.now() ), "00" );
      }


      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 60;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarRamoPCod + wvarPolizAnn + wvarPolizSec + wvarCertipol + wvarCertiann + wvarCertisec + wvarSuplenum + wvarFecDesde + wvarFecHasta;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 150;
      wvarResult = "";
      wvarPos = 51;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 160;
      if( !Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" ) )
      {
        wvarResult = wvarResult + "<RECIBOS>";
        wvarStep = 170;
        wvarPos = wvarPos + 2;
        while( (wvarPos < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 3), 6 ) ).equals( "000000" )) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 3), 6 ) ).equals( "" )) )
        {
          wvarStep = 180;
          wvarResult = wvarResult + "<RECIBO>";
          wvarResult = wvarResult + "<RECIBANN>" + Strings.trim( Strings.mid( strParseString, wvarPos, 2 ) ) + "</RECIBANN>";
          wvarResult = wvarResult + "<RECIBTIP>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 2), 1 ) ) + "</RECIBTIP>";
          wvarResult = wvarResult + "<RECIBSEC>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 3), 6 ) ) + "</RECIBSEC>";
          wvarResult = wvarResult + "<PRIMAIMP>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 9), 15 ) ) / 100) ), ".", "," ) + "</PRIMAIMP>";
          wvarResult = wvarResult + "<RECTOIMP>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 24), 15 ) ) / 100) ), ".", "," ) + "</RECTOIMP>";
          wvarResult = wvarResult + "<SIGNO>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 39), 1 ) ) + "></SIGNO>";
          wvarResult = wvarResult + "<EFECFEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 40), 10 ) ) + "]]></EFECFEC>";
          wvarResult = wvarResult + "<VENCFEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 50), 10 ) ) + "]]></VENCFEC>";
          wvarResult = wvarResult + "</RECIBO>";

          wvarPos = wvarPos + 60;
        }
        wvarStep = 190;
        wvarResult = wvarResult + "</RECIBOS>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='NO SE ENCONTRARON DATOS DE RECIBOS'/></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 

      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarRamoPCod + wvarPolizAnn + wvarPolizSec + wvarCertipol + wvarCertiann + wvarCertisec + wvarSuplenum + wvarFecDesde + wvarFecHasta + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
