package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCotizacion implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetCotizacion";
  static final String mcteOpID = "0000";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_Sexo = "//SEXO";
  static final String mcteParam_Estado = "//ESTADO";
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "//KMSRNGCOD";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_SiGarage = "//SIGARAGE";
  static final String mcteParam_Siniestros = "//SINIESTROS";
  static final String mcteParam_Gas = "//GAS";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  static final String mcteParam_CampaCod = "//CAMPACOD";
  static final String mcteParam_DatosPlan = "//DATOSPLAN";
  static final String mcteParam_ClubLBA = "//CLUBLBA";
  static final String mcteParam_Portal = "//PORTAL";
  static final String mcteParam_AgeCod = "//AGECOD";
  static final String mcteParam_CampaTel = "//TELEFONO";
  static final String mcteParam_CobroCod = "//COBROCOD";
  static final String mcteParam_CobroTip = "//COBROTIP";
  static final String mcteParam_EsCero = "//ESCERO";
  /**
   *  DATOS DE LOS HIJOS
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  /**
   *  DATOS DE LOS ACCESORIOS
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    Object wobjClass = null;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaSig = "";
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarMensaje = "";
    String wvarNacimAnn = "";
    String wvarNacimMes = "";
    String wvarNacimDia = "";
    String wvarSexo = "";
    String wvarEstado = "";
    String wvarModeAutCod = "";
    String wvarKMsrngCod = "";
    String wvarEfectAnn = "";
    String wvarSiGarage = "";
    String wvarSiniestros = "";
    String wvarGas = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarHijos = "";
    String wvarAccesorios = "";
    String wvarSumaMin = "";
    String wvarDatosPlan = "";
    String wvarClubLBA = "";
    String wvarSumAseg = "";
    String wvarSumaLBA = "";
    String wvarPortal = "";
    String wvarCampaCod = "";
    String wvarAgeCod = "";
    String wvarCampaTel = "";
    String wvarCampaTelForm = "";
    String wvarCobroCod = "";
    String wvarCobroTip = "";
    String wvarEsCero = "";
    String strParseString = "";
    int wvariCounter = 0;
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // CARGO EL XML DE ENTRADA
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );

      // OBTENGO EL NUMERO DE COTIZACION
      wvarStep = 10;
      wvarRequest = "<Request></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetNroCot();
      //error: function 'Execute' was not found.
      //unsup: Call wobjClass.Execute(wvarRequest, wvarResponse, "")
      wobjClass = null;
      //
      wvarStep = 20;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarCotiID = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( "//NROCOT" ) */ );
      wvarFechaDia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( "//FECHA_DIA" ) */ );
      wvarFechaSig = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( "//FECHA_SIGUIENTE" ) */ );
      //
      wobjXMLParams = (diamondedge.util.XmlDom) null;
      //
      // OBTENGO LA MINIMA SUMA
      wvarStep = 30;
      wvarRequest = "<Request><CONCEPTO>SA-SCO-MIN</CONCEPTO></Request>";
      wobjClass = new lbawA_OfVirtualLBA.lbaw_GetParamGral();
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      wobjClass = (Object) null;
      //
      wvarStep = 40;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      //wvarSumaMin = Right(String(11, "0") & CStr(CLng(CCur(.selectSingleNode("//PARAMNUM").Text) * 100)), 11)
      wvarSumaMin = Strings.right( Strings.fill( 11, "0" ) + String.valueOf( Obj.toDecimal( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( "//PARAMNUM" ) */ ) ).multiply( new java.math.BigDecimal( 100 ) ).doubleValue() ), 11 );
      //
      wobjXMLParams = (diamondedge.util.XmlDom) null;
      //
      // BUSCO LA SUMA ASEGURADA PARA AUPROCOD = 02
      wvarStep = 50;
      wvarRequest = "<Request><AUMODCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) */ ) + "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>";
      wobjClass = new Variant( new com.qbe.services.lbaw_GetSumAseg() )new com.qbe.services.lbaw_GetSumAseg().toObject();
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      //
      // LEVANTO EL VALOR DE SUMASEG
      wvarStep = 60;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarSumAseg = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( "//COMBO/OPTION[@value=" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) *) ) + "]/@sumaseg" ) */ ), ",", "" ), 11 );
      wobjXMLParams = (diamondedge.util.XmlDom) null;
      //
      // BUSCO LA SUMA ASEGURADA PARA AUPROCOD = 03
      wvarStep = 70;
      wvarRequest = "<Request><AUMODCOD>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) */ ) + "</AUMODCOD><AUPROCOD>03</AUPROCOD></Request>";
      wobjClass.Execute( wvarRequest, wvarResponse, "" );
      //
      // LEVANTO EL VALOR DE SUMALBA
      wvarStep = 80;
      wobjXMLParams = new diamondedge.util.XmlDom();
      //unsup wobjXMLParams.async = false;
      wobjXMLParams.loadXML( wvarResponse );
      wvarSumaLBA = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( "//COMBO/OPTION[@value=" + diamondedge.util.XmlDom.getText( null (*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) *) ) + "]/@sumaseg" ) */ ), ",", "" ), 11 );
      wobjXMLParams = (diamondedge.util.XmlDom) null;
      wobjClass = (Object) null;
      //
      wvarStep = 90;
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_Cliensec ) */.getLength() == 0 )
      {
        wvarCliensec = Strings.fill( 9, "0" );
      }
      else
      {
        wvarCliensec = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Cliensec ) */ ), 9 );
      }

      wvarNacimAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimAnn ) */ );
      wvarNacimMes = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimMes ) */ );
      wvarNacimDia = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimDia ) */ );
      wvarSexo = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Sexo ) */ );
      wvarEstado = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Estado ) */ );
      wvarModeAutCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) */ );
      wvarKMsrngCod = Strings.right( Strings.fill( 7, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_KMsrngCod ) */ ), 7 );
      wvarEfectAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) */ );
      wvarSiGarage = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SiGarage ) */ );
      wvarSiniestros = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Siniestros ) */ ), 2 );
      wvarGas = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Gas ) */ );
      wvarProvi = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Provi ) */ ), 2 );
      wvarLocalidadCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod ) */ );
      wvarEsCero = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EsCero ) */ ), 1 );

      wvarStep = 100;
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_Portal ) */.getLength() == 0 )
      {
        wvarPortal = "LBA";
      }
      else
      {
        wvarPortal = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Portal ) */ );
      }

      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_DatosPlan ) */.getLength() == 0 )
      {
        wvarDatosPlan = "00000";
      }
      else
      {
        wvarDatosPlan = Strings.right( Strings.fill( 5, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_DatosPlan ) */ ), 5 );
      }

      wvarClubLBA = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClubLBA ) */ );

      // EN MERCADO ABIERTO SIEMPRE COTIZA CON COBROCOD = 4 Y COBROTIP = VI
      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CobroCod ) */.getLength() == 0 )
      {
        wvarCobroCod = "4";
      }
      else
      {
        wvarCobroCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CobroCod ) */ );
      }

      if( null /*unsup wobjXMLRequest.selectNodes( mcteParam_CobroTip ) */.getLength() == 0 )
      {
        wvarCobroTip = "VI";
      }
      else
      {
        wvarCobroTip = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CobroTip ) */ );
      }

      // INICIO HIJOS
      wvarStep = 110;
      wvarHijos = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarHijos = wvarHijos + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_NacimHijo ) */ );
        wvarHijos = wvarHijos + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_SexoHijo ) */ );
        wvarHijos = wvarHijos + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_EstadoHijo ) */ );
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarHijos = wvarHijos + Strings.fill( 8, "0" ) + "  ";
      }
      // FIN HIJOS
      wvarStep = 120;
      // INICIO ACCESORIOS
      wvarAccesorios = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarAccesorios = wvarAccesorios + Strings.right( Strings.fill( 14, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_PrecioAcc ) */ ), 14 );
        wvarAccesorios = wvarAccesorios + Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_DescripcionAcc ) */ ) + Strings.fill( 30, " " ), 30 );
        wvarAccesorios = wvarAccesorios + "S";
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarAccesorios = wvarAccesorios + Strings.fill( 14, "0" ) + Strings.fill( 31, " " );
      }
      // FIN ACCESORIOS
      //
      // BUSCO LOS DATOS DE CAMPANIA, PRODUCTOR Y TELEFONO - SOLO PARA MERCADO ABIERTO
      if( Strings.trim( wvarPortal ).equals( "LBA_PRODUCTORES" ) )
      {
        wvarCampaCod = Strings.right( Strings.fill( 4, "0" ) + Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CampaCod ) */ ) ), 4 );
        wvarAgeCod = Strings.right( Strings.fill( 4, "0" ) + Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AgeCod ) */ ) ), 4 );
        wvarCampaTel = "";
        wvarCampaTelForm = "";
      }
      else
      {
        wvarStep = 130;
        wvarRequest = "<Request><PORTAL>" + wvarPortal + "</PORTAL><RAMOPCOD>AUS1</RAMOPCOD></Request>";
        wobjClass = new lbawA_OfVirtualLBA.lbaw_GetPortalComerc();
        wobjClass.Execute( wvarRequest, wvarResponse, "" );
        //
        wvarStep = 140;
        wobjXMLParams = new diamondedge.util.XmlDom();
        //unsup wobjXMLParams.async = false;
        wobjXMLParams.loadXML( wvarResponse );
        wvarCampaCod = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( mcteParam_CampaCod ) */ ) + Strings.fill( 4, " " ), 4 );
        wvarAgeCod = Strings.trim( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( mcteParam_AgeCod ) */ ) );
        wvarCampaTel = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( mcteParam_CampaTel ) */ );
        wvarCampaTelForm = invoke( "Formateo_Telefono", new Variant[] { new Variant(diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParams.selectSingleNode( new Variant(mcteParam_CampaTel) ) */ )) } );
        wobjXMLParams = (diamondedge.util.XmlDom) null;
        wobjClass = (Object) null;
      }

      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      wobjXMLList = (org.w3c.dom.NodeList) null;
      //
      wvarStep = 150;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      wvarMensaje = mcteOpID + wvarCliensec + wvarNacimAnn + wvarNacimMes + wvarNacimDia;
      wvarMensaje = wvarMensaje + wvarSexo + wvarEstado;
      wvarMensaje = wvarMensaje + "3" + wvarCobroTip + wvarCobroCod + wvarFechaDia + wvarFechaSig + wvarAgeCod + wvarDatosPlan + wvarSumAseg + wvarSumaLBA;
      wvarMensaje = wvarMensaje + wvarModeAutCod + "001" + wvarKMsrngCod + wvarEfectAnn + wvarSiGarage;
      wvarMensaje = wvarMensaje + wvarSiniestros + wvarGas + wvarClubLBA + "00" + wvarProvi + wvarLocalidadCod;
      wvarMensaje = wvarMensaje + wvarHijos;
      wvarMensaje = wvarMensaje + wvarAccesorios;
      wvarMensaje = wvarMensaje + Strings.right( Strings.fill( 9, "0" ) + wvarCotiID, 9 ) + wvarSumaMin + wvarCampaCod + Strings.fill( 36, " " );
      wvarMensaje = wvarMensaje + wvarEsCero;

      //Dim mobjFileObject As Scripting.FileSystemObject
      //Dim mobjFile As Scripting.TextStream
      //Set mobjFileObject = CreateObject("Scripting.FileSystemObject")
      //Set mobjFile = mobjFileObject.CreateTextFile("D:\Comp\LBAVirtual\FileTemp\rand2" & CStr(Int(Rnd(5000) * 5000)) & ".txt", True)
      //mobjFile.WriteLine wvarMensaje
      //mobjFile.WriteLine ""
      //mobjFile.WriteLine ""
      //mobjFile.WriteLine wvarSumaMin
      //mobjFile.Close
      //Set mobjFile = Nothing
      //Set mobjFileObject = Nothing
      wvarStep = 220;
      wvarArea = wvarMensaje;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";
      //
      wvarStep = 250;

      //Set mobjFileObject = CreateObject("Scripting.FileSystemObject")
      //Set mobjFile = mobjFileObject.CreateTextFile("D:\Comp\LBAVirtual\FileTemp\rand" & CStr(Int(Rnd(5000) * 5000)) & ".txt", True)
      //mobjFile.WriteLine strParseString
      //mobjFile.Close
      //Set mobjFile = Nothing
      //Set mobjFileObject = Nothing
      wvarResult = wvarResult + "<COT_NRO>" + wvarCotiID + "</COT_NRO>";
      wvarResult = wvarResult + "<CAMPA_TEL>" + wvarCampaTel + "</CAMPA_TEL>";
      wvarResult = wvarResult + "<CAMPA_TEL_FORM>" + wvarCampaTelForm + "</CAMPA_TEL_FORM>";
      wvarResult = wvarResult + "<CAMPA_COD>" + wvarCampaCod + "</CAMPA_COD>";
      wvarResult = wvarResult + "<AGE_COD>" + wvarAgeCod + "</AGE_COD>";
      wvarResult = wvarResult + "<RC_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 1, 18 ) ) / 100) ), "0.00" ) + "</RC_CH>";
      wvarResult = wvarResult + "<SDP_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 19, 18 ) ) / 100) ), "0.00" ) + "</SDP_CH>";
      wvarResult = wvarResult + "<TT_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 37, 18 ) ) / 100) ), "0.00" ) + "</TT_CH>";
      wvarResult = wvarResult + "<TRCF1_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 55, 18 ) ) / 100) ), "0.00" ) + "</TRCF1_CH>";
      wvarResult = wvarResult + "<TRCF2_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 73, 18 ) ) / 100) ), "0.00" ) + "</TRCF2_CH>";
      wvarResult = wvarResult + "<TRCF3_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 91, 18 ) ) / 100) ), "0.00" ) + "</TRCF3_CH>";
      wvarResult = wvarResult + "<RCROBO_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 109, 18 ) ) / 100) ), "0.00" ) + "</RCROBO_CH>";

      wvarResult = wvarResult + "<RC_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 127, 18 ) ) / 100) ), "0.00" ) + "</RC_SH>";
      wvarResult = wvarResult + "<SDP_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 145, 18 ) ) / 100) ), "0.00" ) + "</SDP_SH>";
      wvarResult = wvarResult + "<TT_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 163, 18 ) ) / 100) ), "0.00" ) + "</TT_SH>";
      wvarResult = wvarResult + "<TRCF1_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 181, 18 ) ) / 100) ), "0.00" ) + "</TRCF1_SH>";
      wvarResult = wvarResult + "<TRCF2_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 199, 18 ) ) / 100) ), "0.00" ) + "</TRCF2_SH>";
      wvarResult = wvarResult + "<TRCF3_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 217, 18 ) ) / 100) ), "0.00" ) + "</TRCF3_SH>";
      wvarResult = wvarResult + "<RCROBO_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 235, 18 ) ) / 100) ), "0.00" ) + "</RCROBO_SH>";
      //
      wvarResult = wvarResult + "<RC_COD>00199</RC_COD>";
      wvarResult = wvarResult + "<SDP_COD>00299</SDP_COD>";
      wvarResult = wvarResult + "<TT_COD>00399</TT_COD>";
      wvarResult = wvarResult + "<TRCF1_COD>00401</TRCF1_COD>";
      wvarResult = wvarResult + "<TRCF2_COD>00402</TRCF2_COD>";
      wvarResult = wvarResult + "<TRCF3_COD>00403</TRCF3_COD>";
      wvarResult = wvarResult + "<RCROBO_COD>00599</RCROBO_COD>";
      //
      wvarResult = wvarResult + "<SUMASEG>" + wvarSumAseg + "</SUMASEG>";
      wvarResult = wvarResult + "<SUMALBA>" + wvarSumaLBA + "</SUMALBA>";
      //
      wvarStep = 260;
      if( !wvarDatosPlan.equals( "00000" ) )
      {
        for( wvariCounter = 1; wvariCounter <= 20; wvariCounter++ )
        {
          wvarResult = wvarResult + "<COBERCOD" + wvariCounter + ">" + Strings.mid( strParseString, (283 + (276 * (wvariCounter - 1))), 3 ) + "</COBERCOD" + wvariCounter + ">";
          wvarResult = wvarResult + "<COBERORD" + wvariCounter + ">" + Strings.mid( strParseString, (302 + (276 * (wvariCounter - 1))), 2 ) + "</COBERORD" + wvariCounter + ">";
          wvarResult = wvarResult + "<CAPITASG" + wvariCounter + ">" + Strings.mid( strParseString, (304 + (276 * (wvariCounter - 1))), 15 ) + "</CAPITASG" + wvariCounter + ">";
          wvarResult = wvarResult + "<CAPITIMP" + wvariCounter + ">" + Strings.mid( strParseString, (319 + (276 * (wvariCounter - 1))), 15 ) + "</CAPITIMP" + wvariCounter + ">";
        }
      }

      wvarResult = Strings.replace( wvarResult, ".", "," );
      //
      wvarStep = 270;
      Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      //
      wvarStep = 280;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String Formateo_Telefono( String pvarTelefono ) throws Exception
  {
    String Formateo_Telefono = "";
    String wvarNewTelef = "";

    if( Strings.trim( Strings.left( pvarTelefono, 4 ) ).equals( "0800" ) )
    {
      wvarNewTelef = Strings.trim( Strings.left( pvarTelefono, 4 ) ) + "-" + Strings.mid( pvarTelefono, 5, 3 ) + "-" + Strings.mid( pvarTelefono, 8, Strings.len( pvarTelefono ) );
    }
    else
    {
      wvarNewTelef = pvarTelefono;
    }

    Formateo_Telefono = wvarNewTelef;

    return Formateo_Telefono;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
