package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Esto archivo es unico solo para las pruebas UAT de Brokers
 */

public class lbaws_GetCotisAUS implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  static final String gcteConfFileName2 = "MQConfig.xml";
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaws_GetCotisAUS";
  static final String mcteOpID = "0043";
  static final String mcteOpIDSolis = "0000";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_WDB_TIPO_OPERACION = "//TIPOOPERACION";
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_NacimAnn = "//NACIMANN";
  static final String mcteParam_NacimMes = "//NACIMMES";
  static final String mcteParam_NacimDia = "//NACIMDIA";
  static final String mcteParam_Sexo = "//SEXO";
  static final String mcteParam_Estado = "//ESTADO";
  static final String mcteParam_ClienIVA = "//CLIENIVA";
  static final String mcteParam_ModeAutCod = "//MODEAUTCOD";
  static final String mcteParam_KMsrngCod = "//KMSRNGCOD";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_SiGarage = "//SIGARAGE";
  static final String mcteParam_SumAseg = "//SUMAASEG";
  static final String mcteParam_Siniestros = "//SINIESTROS";
  static final String mcteParam_Gas = "//GAS";
  static final String mcteParam_Provi = "//PROVI";
  static final String mcteParam_LocalidadCod = "//LOCALIDADCOD";
  /**
   * Revisar
   */
  static final String mcteParam_CampaCod = "//CAMPACOD";
  /**
   * Revisar
   */
  static final String mcteParam_DatosPlan = "//DATOSPLAN";
  static final String mcteParam_ClubLBA = "//CLUBLBA";
  /**
   * Revisar
   */
  static final String mcteParam_Portal = "//PORTAL";
  static final String mcteParam_PRODUCTOR = "//PRODUCTOR";
  static final String mcteParam_PLANCOD = "//PLANNCOD";
  static final String mcteParam_Franqcod = "//FRANQCOD";
  /**
   * Const mcteParam_CampaTel        As String = "//TELEFONO"
   */
  static final String mcteParam_CobroCod = "//COBROCOD";
  static final String mcteParam_CobroTip = "//COBROTIP";
  static final String mcteParam_NroCot = "//WDB_NROCOT";
  static final String mcteParam_SumaMin = "//SUMA_MINIMA";
  static final String mcteParam_SumaMax_GBA = "//SUMA_MAXIMA_GBA";
  static final String mcteParam_SumaMax_INT = "//SUMA_MAXIMA_INT";
  static final String mcteParam_Control_GNC_GBA = "//CONTROL_GNC_GBA";
  static final String mcteParam_Control_GNC_INT = "//CONTROL_GNC_INT";
  static final String mcteParam_Permite_GNC = "//PERMITE_GNC";
  static final String mcteParam_Luneta = "//LUNETA";
  static final String mcteParam_VIP = "//VIP";
  /**
   *  DATOS DE LOS HIJOS
   */
  static final String mcteNodos_Hijos = "//Request/HIJOS/HIJO";
  static final String mcteParam_NacimHijo = "NACIMHIJO";
  static final String mcteParam_SexoHijo = "SEXOHIJO";
  static final String mcteParam_EstadoHijo = "ESTADOHIJO";
  /**
   *  DATOS DE LOS ACCESORIOS
   */
  static final String mcteNodos_Accesorios = "//Request/ACCESORIOS/ACCESORIO";
  static final String mcteParam_PrecioAcc = "PRECIOACC";
  static final String mcteParam_DescripcionAcc = "DESCRIPCIONACC";
  static final String mcteParam_CodigoAcc = "CODIGOACC";
  /**
   * Datos del mensaje
   */
  static final String mcteParam_REQUESTID = "//REQUESTID";
  static final String mcteParam_VEHDES = "//VEHDES";
  /**
   * Objetos del FrameWork
   */
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  public int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLParams = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    org.w3c.dom.NodeList wobjXMLList = null;
    com.qbe.services.HSBCInterfaces.IAction wobjClass = null;
    String wvarRequest = "";
    String wvarResponse = "";
    String wvarCotiID = "";
    String wvarFechaDia = "";
    String wvarFechaSig = "";
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarMensaje = "";
    String wvarNacimAnn = "";
    String wvarNacimMes = "";
    String wvarNacimDia = "";
    String wvarSexo = "";
    String wvarEstado = "";
    String wvarClienIVA = "";
    String wvarProductor = "";
    String wvarPLANCOD = "";
    String wvarFranqcod = "";
    String wvarModeAutCod = "";
    String wvarKMsrngCod = "";
    String wvarEfectAnn = "";
    String wvarSiGarage = "";
    String wvarSiniestros = "";
    String wvarGas = "";
    String wvarProvi = "";
    String wvarLocalidadCod = "";
    String wvarHijos = "";
    String wvarAccesorios = "";
    String wvarSumaMin = "";
    String wvarDatosPlan = "";
    String wvarClubLBA = "";
    String wvarSumAseg = "";
    String wvarSumaLBA = "";
    String wvarPortal = "";
    String wvarCampaCod = "";
    String wvarAgeCod = "";
    String wvarCampaTel = "";
    String wvarCampaTelForm = "";
    String wvarCobroCod = "";
    String wvarCobroTip = "";
    String wvarSumaMaximaGBA = "";
    String wvarSumaMaxmiaINT = "";
    String wvarControlGNCGBA = "";
    String wvarControlGNCINT = "";
    String wvarPermiteGNC = "";
    String wvarAuxCodigoAcc = "";
    String wvarPlanncod = "";
    String wvarLuneta = "";
    String wvarVIP = "";
    String strParseString = "";
    int wvariCounter = 0;
    //
    //
    //
    //
    //
    //Dim wvarFranqcod        As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      // CARGO EL XML DE ENTRADA
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );


      //CARGO LAS VARIABLES CON LOS DATOS DEL XML
      wvarNacimAnn = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimAnn ) */ ), 4 );
      wvarNacimMes = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimMes ) */ ), 2 );
      wvarNacimDia = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NacimDia ) */ ), 2 );
      wvarSexo = Strings.toUpperCase( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Sexo ) */ ) + " ", 1 ) );
      wvarEstado = Strings.toUpperCase( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Estado ) */ ) + " ", 1 ) );
      wvarClienIVA = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClienIVA ) */ ) + " ", 1 );
      wvarCobroTip = Strings.toUpperCase( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CobroTip ) */ ) + " ", 2 ) );
      wvarCobroCod = Strings.right( "0" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CobroCod ) */ ), 1 );
      wvarFechaDia = String.valueOf( DateTime.year( DateTime.now() ) ) + Strings.right( "0" + String.valueOf( DateTime.month( DateTime.now() ) ), 2 ) + Strings.right( ("0" + String.valueOf( DateTime.day( DateTime.now() ) )), 2 );
      wvarFechaSig = String.valueOf( DateTime.year( DateTime.add( "m", 1, DateTime.now() ) ) ) + Strings.right( "0" + String.valueOf( DateTime.month( DateTime.add( "m", 1, DateTime.now() ) ) ), 2 ) + "01";
      wvarProductor = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PRODUCTOR ) */ ) + "    ", 4 );
      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null )
      {
        //wvarPLANCOD = "000"
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ == (org.w3c.dom.Node) null) )
        {
          wvarPLANCOD = Strings.right( Strings.fill( 3, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ ), 3 );
        }
        else
        {
          wvarPLANCOD = "000";
        }
        if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Franqcod ) */ == (org.w3c.dom.Node) null) )
        {
          wvarFranqcod = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Franqcod ) */ ) + "   ", 2 );
        }
        else
        {
          wvarFranqcod = "  ";
        }
        //wvarFranqcod = "  "
      }
      else
      {
        wvarPLANCOD = Strings.right( "000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PLANCOD ) */ ), 3 );
        wvarFranqcod = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Franqcod ) */ ) + "   ", 2 );
      }
      wvarSumAseg = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( String.valueOf( (Obj.toInt( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SumAseg ) */ ) ) * 100) ), ".", "" ), 11 );
      wvarModeAutCod = Strings.right( Strings.fill( 21, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ModeAutCod ) */ ), 21 );
      wvarKMsrngCod = Strings.right( Strings.fill( 7, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_KMsrngCod ) */ ), 7 );
      wvarEfectAnn = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) */ ), 4 );
      wvarSiGarage = Strings.toUpperCase( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SiGarage ) */ ) + " ", 1 ) );
      wvarSiniestros = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Siniestros ) */ ), 2 );
      wvarGas = Strings.toUpperCase( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Gas ) */ ) + " ", 1 ) );
      wvarClubLBA = Strings.toUpperCase( Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_ClubLBA ) */ ) + " ", 1 ) );
      wvarProvi = Strings.right( "00" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Provi ) */ ), 2 );
      wvarLocalidadCod = Strings.right( "0000" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_LocalidadCod ) */ ), 4 );

      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Luneta ) */ == (org.w3c.dom.Node) null) )
      {
        wvarLuneta = Strings.right( "N" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Luneta ) */ ), 1 );
      }
      else
      {
        wvarLuneta = "N";
      }
      if( ! (null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VIP ) */ == (org.w3c.dom.Node) null) )
      {
        wvarVIP = Strings.right( "N" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VIP ) */ ), 1 );
      }
      else
      {
        wvarVIP = "N";
      }

      // INICIO HIJOS
      wvarHijos = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Hijos ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarHijos = wvarHijos + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_NacimHijo ) */ );
        wvarHijos = wvarHijos + Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_SexoHijo ) */ ) );
        wvarHijos = wvarHijos + Strings.toUpperCase( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_EstadoHijo ) */ ) );
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarHijos = wvarHijos + Strings.fill( 8, "0" ) + "  ";
      }
      // FIN HIJOS
      // INICIO ACCESORIOS
      wvarAccesorios = "";
      wobjXMLList = null /*unsup wobjXMLRequest.selectNodes( mcteNodos_Accesorios ) */;

      for( wvariCounter = 0; wvariCounter <= (wobjXMLList.getLength() - 1); wvariCounter++ )
      {
        wvarAccesorios = wvarAccesorios + Strings.right( Strings.fill( 14, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_PrecioAcc ) */ ), 14 );
        wvarAccesorios = wvarAccesorios + Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_DescripcionAcc ) */ ) + Strings.fill( 30, " " ), 30 );
        wvarAccesorios = wvarAccesorios + "S";
        wvarAuxCodigoAcc = wvarAuxCodigoAcc + Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLList.item( wvariCounter ).selectSingleNode( mcteParam_CodigoAcc ) */ ), 4 );
      }

      for( wvariCounter = wobjXMLList.getLength(); wvariCounter <= 9; wvariCounter++ )
      {
        wvarAccesorios = wvarAccesorios + Strings.fill( 14, "0" ) + Strings.fill( 31, " " );
      }
      // FIN ACCESORIOS
      wvarCotiID = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroCot ) */ ), 9 );
      wvarSumaMin = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SumaMin ) */ ), ".", "" ), 11 );
      wvarCampaCod = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CampaCod ) */ ) + Strings.fill( 40, " " ), 40 );

      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null )
      {

        wvarSumaMaximaGBA = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SumaMax_GBA ) */ ), ".", "" ), 11 );
        wvarSumaMaxmiaINT = Strings.right( Strings.fill( 11, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SumaMax_INT ) */ ), ".", "" ), 11 );
        wvarControlGNCGBA = Strings.right( Strings.fill( 1, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Control_GNC_GBA ) */ ), ".", "" ), 1 );
        wvarControlGNCINT = Strings.right( Strings.fill( 1, "0" ) + Strings.replace( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Control_GNC_INT ) */ ), ".", "" ), 1 );
        wvarPermiteGNC = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Permite_GNC ) */ ) + " ", 1 );
        //wvarPLANCOD = Right(String(3, "0") & Replace(wobjXMLRequest.selectSingleNode(mcteParam_PLANCOD).Text, ".", ""), 3)
        wvarMensaje = mcteOpID;
        wvarMensaje = wvarMensaje + Strings.fill( 9, "0" ) + wvarNacimAnn + wvarNacimMes + wvarNacimDia;
        wvarMensaje = wvarMensaje + wvarSexo + wvarEstado;
        wvarMensaje = wvarMensaje + wvarClienIVA + wvarCobroTip + wvarCobroCod + wvarFechaDia + wvarFechaSig + wvarProductor + wvarPLANCOD + wvarFranqcod + wvarSumAseg + Strings.fill( 11, "0" );
        wvarMensaje = wvarMensaje + wvarModeAutCod + "001" + wvarKMsrngCod + wvarEfectAnn + wvarSiGarage;
        wvarMensaje = wvarMensaje + wvarSiniestros + wvarGas + wvarClubLBA + "00" + wvarProvi + wvarLocalidadCod;
        wvarMensaje = wvarMensaje + wvarHijos;
        wvarMensaje = wvarMensaje + wvarAccesorios;
        wvarMensaje = wvarMensaje + wvarCotiID + wvarSumaMin + wvarCampaCod + wvarSumaMaximaGBA + wvarSumaMaxmiaINT + wvarControlGNCGBA + wvarControlGNCINT + wvarPermiteGNC + "N";
        wvarMensaje = wvarMensaje + wvarVIP + wvarLuneta;
        wvarMensaje = wvarMensaje + Strings.left( wvarAuxCodigoAcc + Strings.fill( 40, "0" ), 40 );

      }
      else
      {
        wvarMensaje = mcteOpIDSolis;
        wvarMensaje = wvarMensaje + Strings.fill( 9, "0" ) + wvarNacimAnn + wvarNacimMes + wvarNacimDia;
        wvarMensaje = wvarMensaje + wvarSexo + wvarEstado;
        wvarMensaje = wvarMensaje + wvarClienIVA + wvarCobroTip + wvarCobroCod + wvarFechaDia + wvarFechaSig + wvarProductor + wvarPLANCOD + wvarFranqcod + wvarSumAseg + Strings.fill( 11, "0" );
        wvarMensaje = wvarMensaje + wvarModeAutCod + "001" + wvarKMsrngCod + wvarEfectAnn + wvarSiGarage;
        wvarMensaje = wvarMensaje + wvarSiniestros + wvarGas + wvarClubLBA + "00" + wvarProvi + wvarLocalidadCod;
        wvarMensaje = wvarMensaje + wvarHijos;
        wvarMensaje = wvarMensaje + wvarAccesorios;
        wvarMensaje = wvarMensaje + wvarCotiID + wvarSumaMin + wvarCampaCod;
      }

      wvarStep = 150;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      wvarArea = wvarMensaje;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";
      //
      wvarStep = 250;

      if( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_WDB_TIPO_OPERACION ) */ == (org.w3c.dom.Node) null )
      {
        if( Strings.toUpperCase( Strings.left( strParseString, 2 ) ).equals( "OK" ) )
        {
          Response.set( Response + "<LBA_WS res_code=\"OK\" res_msg=\"\"><Response>" );
          Response.set( Response + "<REQUESTID>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_REQUESTID ) */ ) + "</REQUESTID>" );
          Response.set( Response + "<COT_NRO>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NroCot ) */ ) + "</COT_NRO>" );
          Response.set( Response + "<VEHDES>" + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_VEHDES ) */ ) + "</VEHDES>" );
          Response.set( Response + "<PRECIOS>" );
          for( wvariCounter = 1; wvariCounter <= 7; wvariCounter++ )
          {
            if( !Strings.mid( strParseString, ((3 + (157 * (wvariCounter - 1))) - (wvariCounter - 1)), 3 ).equals( "000" ) )
            {
              Response.set( Response + invoke( "ParseoResultadoPrecios", new Variant[] { new Variant(Strings.mid( new Variant(strParseString), new Variant(((3 + (157 * (wvariCounter - 1))) - (wvariCounter - 1))), new Variant(156) )) } ) );
            }
          }
          Response.set( Response + "</PRECIOS>" );
          //Response = Response & "<TIEMPO>" & Mid(strParseString, 1004, 6) & "</TIEMPO>"
          Response.set( Response + "<TIEMPO>" + Strings.mid( strParseString, 1095, 6 ) + "</TIEMPO>" );
          Response.set( Response + "<CODZONA>" + Strings.mid( strParseString, 1101, 4 ) + "</CODZONA>" );
          Response.set( Response + "<SUMALBA>" + Strings.format( String.valueOf( Obj.toInt( Strings.mid( strParseString, 1105, 11 ) ) ), "0.00" ) + "</SUMALBA>" );
          Response.set( Response + "</Response></LBA_WS>" );
        }
        else
        {
          Response.set( "<LBA_WS res_code=\"" + Strings.toUpperCase( Strings.left( strParseString, 2 ) ) + "\" res_msg=\"\">" );
          //Response = Response & "<TIEMPO>" & Mid(strParseString, 1004, 6) & "</TIEMPO>"
          Response.set( Response + "<TIEMPO>" + Strings.mid( strParseString, 1095, 6 ) + "</TIEMPO>" );
          Response.set( Response + "<CODZONA>" + Strings.mid( strParseString, 1101, 4 ) + "</CODZONA>" );
          Response.set( Response + "<SUMALBA>" + Strings.format( String.valueOf( Obj.toInt( Strings.mid( strParseString, 1105, 11 ) ) ), "0.00" ) + "</SUMALBA>" );
          Response.set( Response + "</LBA_WS>" );
        }

      }
      else
      {
        Response.set( Response + "<LBA_WS res_code=\"OK\" res_msg=\"\"><Response>" );
        Response.set( Response + "<COT_NRO>" + wvarCotiID + "</COT_NRO>" );
        Response.set( Response + "<PRECIOPLAN_CH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 1, 18 ) ) / 100) ), "0.00" ) + "</PRECIOPLAN_CH>" );
        Response.set( Response + "<PRECIOPLAN_SH>" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, 127, 18 ) ) / 100) ), "0.00" ) + "</PRECIOPLAN_SH>" );
        //
        wvarStep = 260;
        if( !wvarDatosPlan.equals( "00000" ) )
        {
          for( wvariCounter = 1; wvariCounter <= 20; wvariCounter++ )
          {
            Response.set( Response + "<COBERCOD" + wvariCounter + ">" + Strings.mid( strParseString, (283 + (276 * (wvariCounter - 1))), 3 ) + "</COBERCOD" + wvariCounter + ">" );
            Response.set( Response + "<COBERORD" + wvariCounter + ">" + Strings.mid( strParseString, (302 + (276 * (wvariCounter - 1))), 2 ) + "</COBERORD" + wvariCounter + ">" );
            Response.set( Response + "<CAPITASG" + wvariCounter + ">" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (304 + (276 * (wvariCounter - 1))), 15 ) ) / 100) ), "0.00" ) + "</CAPITASG" + wvariCounter + ">" );
            Response.set( Response + "<CAPITIMP" + wvariCounter + ">" + Strings.format( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (319 + (276 * (wvariCounter - 1))), 15 ) ) / 100) ), "0.00" ) + "</CAPITIMP" + wvariCounter + ">" );
          }
        }

        Response.set( Response + "</Response></LBA_WS>" );
      }
      //
      //
      wvarStep = 280;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + wvarMensaje + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoResultadoPrecios( String pvarStrRequest ) throws Exception
  {
    String ParseoResultadoPrecios = "";
    String pvarStrResponse = "";

    pvarStrResponse = "<PRECIO>";
    pvarStrResponse = pvarStrResponse + "<PLANNCOD>" + Strings.mid( pvarStrRequest, 1, 3 ) + "</PLANNCOD>";
    pvarStrResponse = pvarStrResponse + "<FRANQCOD>" + Strings.mid( pvarStrRequest, 4, 2 ) + "</FRANQCOD>";
    pvarStrResponse = pvarStrResponse + "<PLANNDES>" + Strings.trim( Strings.mid( pvarStrRequest, 6, 50 ) ) + "</PLANNDES>";
    pvarStrResponse = pvarStrResponse + "<PRIMA>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 56, 11 ) ) / 100) ), "0.00" ) + "</PRIMA>";
    pvarStrResponse = pvarStrResponse + "<RECARGOS>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 67, 11 ) ) / 100) ), "0.00" ) + "</RECARGOS>";
    pvarStrResponse = pvarStrResponse + "<IVA>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 78, 11 ) ) / 100) ), "0.00" ) + "</IVA>";
    pvarStrResponse = pvarStrResponse + "<DEREMI>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 89, 11 ) ) / 100) ), "0.00" ) + "</DEREMI>";
    pvarStrResponse = pvarStrResponse + "<SELLADOS>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 100, 11 ) ) / 100) ), "0.00" ) + "</SELLADOS>";
    pvarStrResponse = pvarStrResponse + "<INGBRU>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 111, 11 ) ) / 100) ), "0.00" ) + "</INGBRU>";
    pvarStrResponse = pvarStrResponse + "<IMPUESTOS>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 122, 11 ) ) / 100) ), "0.00" ) + "</IMPUESTOS>";
    pvarStrResponse = pvarStrResponse + "<VALOR>" + Strings.format( String.valueOf( (Obj.toInt( Strings.mid( pvarStrRequest, 133, 11 ) ) / 100) ), "0.00" ) + "</VALOR>";
    pvarStrResponse = pvarStrResponse + "<CLUB_LBA>" + Strings.mid( pvarStrRequest, 144, 1 ) + "</CLUB_LBA>";
    pvarStrResponse = pvarStrResponse + "<COB_CLUB>" + Strings.mid( pvarStrRequest, 145, 3 ) + "</COB_CLUB>";
    pvarStrResponse = pvarStrResponse + "<LUNETA>" + Strings.mid( pvarStrRequest, 148, 1 ) + "</LUNETA>";
    pvarStrResponse = pvarStrResponse + "<COB_LUNE>" + Strings.mid( pvarStrRequest, 149, 3 ) + "</COB_LUNE>";
    pvarStrResponse = pvarStrResponse + "<VIP>" + Strings.mid( pvarStrRequest, 152, 1 ) + "</VIP>";
    pvarStrResponse = pvarStrResponse + "<COB_VIP>" + Strings.mid( pvarStrRequest, 153, 3 ) + "</COB_VIP>";
    pvarStrResponse = pvarStrResponse + "<RASTREO>" + Strings.mid( pvarStrRequest, 156, 1 ) + "</RASTREO>";
    pvarStrResponse = pvarStrResponse + "</PRECIO>";

    ParseoResultadoPrecios = pvarStrResponse;
    return ParseoResultadoPrecios;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
