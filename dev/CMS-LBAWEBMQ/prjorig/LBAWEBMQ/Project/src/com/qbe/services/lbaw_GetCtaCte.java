package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetCtaCte implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetCtaCte";
  static final String mcteOpID = "0015";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_EfectAnn = "//EFECTANN";
  static final String mcteParam_EfectMes = "//EFECTMES";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private String p_GetXSL() throws Exception
  {
    String p_GetXSL = "";
    String wvarStrXSL = "";
    //
    wvarStrXSL = wvarStrXSL + "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";
    wvarStrXSL = wvarStrXSL + "<xsl:key name=\"cuentas\" match=\"CTACTE\" use=\"FECHA\" />";

    wvarStrXSL = wvarStrXSL + " <xsl:template match='/'>";
    wvarStrXSL = wvarStrXSL + "     <xsl:element name='PRODUCTOR'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:value-of select=\"//PRODUCTOR\"/>";
    wvarStrXSL = wvarStrXSL + "     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "     <xsl:element name='CTACTES'>";
    wvarStrXSL = wvarStrXSL + "         <xsl:for-each select=\"//CTACTE[count(. | key('cuentas', FECHA)[1]) = 1]\">";
    wvarStrXSL = wvarStrXSL + "             <xsl:element name='CTACTE'>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:element name='FECHA'>";
    wvarStrXSL = wvarStrXSL + "                      <xsl:value-of select=\"FECHA\"/>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:element>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='F' and MOALFCOD='$' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_PESOS'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_PESOS_SIGNO'></xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='F' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_DOLAR'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='BRUTO_DOLAR_SIGNO'></xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='C' and MOALFCOD='$' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_PESOS'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_PESOS_SIGNO'></xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='C' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_DOLAR'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='NETO_DOLAR_SIGNO'></xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='P' and MOALFCOD='$' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_PESOS'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_PESOS_SIGNO'></xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";
    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='P' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_DOLAR'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='PEND_DOLAR_SIGNO'></xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:if test=\"not(//CTACTE[CONCEPTO='D' and MOALFCOD='U$S' and FECHA=current()/FECHA])\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:element name='COTIZ_DOLAR'>0</xsl:element>";
    wvarStrXSL = wvarStrXSL + "                 </xsl:if>";

    wvarStrXSL = wvarStrXSL + "                 <xsl:for-each select=\"key('cuentas', FECHA)\">";
    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='F'\">";
    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='$'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_PESOS'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_PESOS_SIGNO'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_DOLAR'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='BRUTO_DOLAR_SIGNO'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";
    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='C'\">";
    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='$'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_PESOS'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_PESOS_SIGNO'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_DOLAR'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='NETO_DOLAR_SIGNO'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";
    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='P'\">";
    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='$'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_PESOS'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_PESOS_SIGNO'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_DOLAR'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='PEND_DOLAR_SIGNO'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"SIGNO\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";
    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                     <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                         <xsl:when test=\"CONCEPTO='D'\">";
    wvarStrXSL = wvarStrXSL + "                             <xsl:choose>";
    wvarStrXSL = wvarStrXSL + "                                 <xsl:when test=\"MOALFCOD='U$S'\">";
    wvarStrXSL = wvarStrXSL + "                                     <xsl:element name='COTIZ_DOLAR'>";
    wvarStrXSL = wvarStrXSL + "                                         <xsl:value-of select=\"IMPORTE\"/>";
    wvarStrXSL = wvarStrXSL + "                                     </xsl:element>";
    wvarStrXSL = wvarStrXSL + "                                 </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                             </xsl:choose>                                                                ";
    wvarStrXSL = wvarStrXSL + "                         </xsl:when>";
    wvarStrXSL = wvarStrXSL + "                     </xsl:choose>";

    wvarStrXSL = wvarStrXSL + "                 </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "             </xsl:element>";
    wvarStrXSL = wvarStrXSL + "      </xsl:for-each>";
    wvarStrXSL = wvarStrXSL + "   </xsl:element>";
    wvarStrXSL = wvarStrXSL + " </xsl:template>";
    wvarStrXSL = wvarStrXSL + "<xsl:output cdata-section-elements='PRODUCTOR'/>";
    wvarStrXSL = wvarStrXSL + "</xsl:stylesheet>";
    //
    p_GetXSL = wvarStrXSL;
    return p_GetXSL;
  }

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarEfectAnn = "";
    String wvarEfectMes = "";
    String wvarCiaAsCod = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCliensec = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Cliensec ) */ ), 9 );
      wvarEfectAnn = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectAnn ) */ );
      wvarEfectMes = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_EfectMes ) */ ), 2 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      if( wvarEfectAnn.equals( "" ) )
      {
        wvarEfectAnn = String.valueOf( DateTime.year( DateTime.now() ) );
        wvarEfectMes = Strings.format( DateTime.month( DateTime.now() ), "00" );
      }
      //
      wvarStep = 40;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarCliensec + wvarEfectAnn + wvarEfectMes + "00";
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = 22;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 170;
      if( !Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" ) )
      {
        wvarResult = wvarResult + "<Response>";
        wvarResult = wvarResult + "<PRODUCTOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 2), 70 ) ) + "]]></PRODUCTOR>";
        wvarResult = wvarResult + "<CTACTES>";
        wvarPos = wvarPos + 72;
        //
        wvarStep = 180;
        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, wvarPos, 11 ).equals( "00000000   " )) )
        {
          wvarResult = wvarResult + "<CTACTE>";
          wvarResult = wvarResult + "<FECHA>" + Strings.mid( strParseString, (wvarPos + 6), 2 ) + "/" + Strings.mid( strParseString, (wvarPos + 4), 2 ) + "/" + Strings.mid( strParseString, wvarPos, 4 ) + "</FECHA>";
          wvarResult = wvarResult + "<MOALFCOD>" + Strings.trim( Strings.mid( strParseString, (wvarPos + 8), 3 ) ) + "</MOALFCOD>";
          wvarResult = wvarResult + "<CONCEPTO>" + Strings.mid( strParseString, (wvarPos + 11), 1 ) + "</CONCEPTO>";
          if( Strings.mid( strParseString, (wvarPos + 11), 1 ).equals( "D" ) )
          {
            wvarResult = wvarResult + "<IMPORTE>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 12), 15 ) ) / 10000000) ), ".", "," ) + "</IMPORTE>";
          }
          else
          {
            wvarResult = wvarResult + "<IMPORTE>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 12), 15 ) ) / 100) ), ".", "," ) + "</IMPORTE>";
          }
          wvarResult = wvarResult + "<SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 27), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 27), 1 )) + "</SIGNO>";
          wvarResult = wvarResult + "</CTACTE>";
          wvarPos = wvarPos + 28;
        }
        //
        wvarStep = 190;
        wvarResult = wvarResult + "</CTACTES>";
        wvarResult = wvarResult + "</Response>";
        //
        wvarStep = 200;
        //
        wobjXMLResponse = new diamondedge.util.XmlDom();
        wobjXSLResponse = new diamondedge.util.XmlDom();
        //
        wvarStep = 210;
        wobjXMLResponse.loadXML( wvarResult );
        //
        wvarStep = 220;
        //unsup wobjXSLResponse.async = false;
        wobjXSLResponse.loadXML( invoke( "p_GetXSL", new Variant[] {} ) );
        //
        wvarStep = 230;
        wvarResult = Strings.replace( new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
        //
        wvarStep = 240;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=NO SE ENCONTRARON DATOS " + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 250;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarCliensec + wvarEfectAnn + wvarEfectMes + "00" + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
