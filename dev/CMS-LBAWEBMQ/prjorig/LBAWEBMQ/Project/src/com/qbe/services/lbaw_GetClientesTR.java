package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetClientesTR implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetClientesTR";
  static final String mcteOpID = "0032";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Clisecag = "//CLISECAG";
  static final String mcteParam_Documtips = "//DOCUMTIPS";
  static final String mcteParam_Documdats = "//DOCUMDATS";
  static final String mcteParam_Clienapes = "//CLIENAPES";
  static final String mcteParam_Ramopcods = "//RAMOPCODS";
  static final String mcteParam_Polizanns = "//POLIZANNS";
  static final String mcteParam_Polizsecs = "//POLIZSECS";
  static final String mcteParam_Certipols = "//CERTIPOLS";
  static final String mcteParam_Certianns = "//CERTIANNS";
  static final String mcteParam_Certisecs = "//CERTISECS";
  /**
   * variables para rellamada por mensaje truncado
   */
  static final String mcteParam_RamopcodTR = "//RAMOPCODTR";
  static final String mcteParam_PolizannTR = "//POLIZANNTR";
  static final String mcteParam_PolizsecTR = "//POLIZSECTR";
  static final String mcteParam_CertipolTR = "//CERTIPOLTR";
  static final String mcteParam_CertiannTR = "//CERTIANNTR";
  static final String mcteParam_CertisecTR = "//CERTISECTR";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarClisecag = "";
    String wvarDocumtips = "";
    String wvarDocumdats = "";
    String wvarClienapes = "";
    String wvarRamopcods = "";
    String wvarPolizanns = "";
    String wvarPolizsecs = "";
    String wvarCertipols = "";
    String wvarCertianns = "";
    String wvarCertisecs = "";
    String wvarRamopcodTR = "";
    String wvarPolizannTR = "";
    String wvarPolizsecTR = "";
    String wvarCertipolTR = "";
    String wvarCertiannTR = "";
    String wvarCertisecTR = "";
    String wvarCliensec = "";
    String wvarCiaAsCod = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarCodError = "";
    //
    //
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarClisecag = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Clisecag ) */ ), 9 );
      wvarDocumtips = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Documtips ) */ ), 2 );
      wvarDocumdats = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Documdats ) */ ) + Strings.fill( 11, " " ), 11 );
      wvarClienapes = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Clienapes ) */ ) + Strings.fill( 34, " " ), 34 );
      wvarRamopcods = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Ramopcods ) */ ), 4 );
      wvarPolizanns = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Polizanns ) */ ) + Strings.fill( 2, "0" ), 2 );
      wvarPolizsecs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Polizsecs ) */ ) + Strings.fill( 6, "0" ), 6 );
      wvarCertipols = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certipols ) */ ) + Strings.fill( 4, "0" ), 4 );
      wvarCertianns = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certianns ) */ ) + Strings.fill( 4, "0" ), 4 );
      wvarCertisecs = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Certisecs ) */ ) + Strings.fill( 6, "0" ), 6 );
      //
      wvarRamopcodTR = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RamopcodTR ) */ ), 4 );
      wvarPolizannTR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PolizannTR ) */ ) + Strings.fill( 2, "0" ), 2 );
      wvarPolizsecTR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_PolizsecTR ) */ ) + Strings.fill( 6, "0" ), 6 );
      wvarCertipolTR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CertipolTR ) */ ) + Strings.fill( 4, "0" ), 4 );
      wvarCertiannTR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CertiannTR ) */ ) + Strings.fill( 4, "0" ), 4 );
      wvarCertisecTR = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CertisecTR ) */ ) + Strings.fill( 6, "0" ), 6 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 60;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarClisecag + wvarDocumtips + wvarDocumdats + wvarClienapes + wvarRamopcods + wvarPolizanns + wvarPolizsecs + wvarCertipols + wvarCertianns + wvarCertisecs + wvarRamopcodTR + wvarPolizannTR + wvarPolizsecTR + wvarCertipolTR + wvarCertiannTR + wvarCertisecTR;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 150;
      wvarResult = "";
      wvarPos = 113;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 160;
      wvarCodError = Strings.mid( strParseString, wvarPos, 2 );
      if( (!wvarCodError.equals( "ER" )) && (!wvarCodError.equals( "" )) )
      {
        //Cargo las variables para llamar a un segundo mensaje de ser necesario
        //strParseString = Trim(Mid(strParseString, 93, Len(strParseString) - 93))
        //strParseString = Trim(Mid(strParseString, 3, Len(strParseString) - 2))
        //wvarstrLen = Len(strParseString)
        //wvarPos = 1
        wvarResult = wvarResult + "<CLIENTES>";
        wvarResult = wvarResult + "<RAMOPCODTR>" + Strings.mid( strParseString, (wvarPos - 26), 4 ) + "</RAMOPCODTR>";
        wvarResult = wvarResult + "<POLIZANNTR>" + Strings.mid( strParseString, (wvarPos - 22), 2 ) + "</POLIZANNTR>";
        wvarResult = wvarResult + "<POLIZSECTR>" + Strings.mid( strParseString, (wvarPos - 20), 6 ) + "</POLIZSECTR>";
        wvarResult = wvarResult + "<CERTIPOLTR>" + Strings.mid( strParseString, (wvarPos - 14), 4 ) + "</CERTIPOLTR>";
        wvarResult = wvarResult + "<CERTIANNTR>" + Strings.mid( strParseString, (wvarPos - 10), 4 ) + "</CERTIANNTR>";
        wvarResult = wvarResult + "<CERTISECTR>" + Strings.mid( strParseString, (wvarPos - 6), 6 ) + "</CERTISECTR>";
        wvarResult = wvarResult + "<CODERROR>" + wvarCodError + "</CODERROR>";
        //
        wvarPos = wvarPos + 2;
        while( (wvarPos < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, (wvarPos + 22), 11 ) ).equals( "" )) )
        {
          //
          wvarResult = wvarResult + "<CLIENTE>";
          wvarResult = wvarResult + "<DOCUMTIP><![CDATA[" + Strings.mid( strParseString, wvarPos, 2 ) + "]]></DOCUMTIP>";
          wvarResult = wvarResult + "<DOCUMDAB><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 2), 20 ) ) + "]]></DOCUMDAB>";
          wvarResult = wvarResult + "<DOCUMDAT><![CDATA[" + Strings.mid( strParseString, (wvarPos + 22), 11 ) + "]]></DOCUMDAT>";
          wvarResult = wvarResult + "<CLIENSEC><![CDATA[" + Strings.mid( strParseString, (wvarPos + 33), 9 ) + "]]></CLIENSEC>";
          wvarResult = wvarResult + "<CLIENDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 42), 30 ) ) + "]]></CLIENDES>";
          wvarResult = wvarResult + "<TOMARIES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 72), 30 ) ) + "]]></TOMARIES>";
          wvarResult = wvarResult + "<SITUCPOL><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 102), 12 ) ) + "]]></SITUCPOL>";
          wvarResult = wvarResult + "<RAMOPCOD><![CDATA[" + Strings.mid( strParseString, (wvarPos + 114), 4 ) + "]]></RAMOPCOD>";
          wvarResult = wvarResult + "<POLIZANN><![CDATA[" + Strings.mid( strParseString, (wvarPos + 118), 2 ) + "]]></POLIZANN>";
          wvarResult = wvarResult + "<POLIZSEC><![CDATA[" + Strings.mid( strParseString, (wvarPos + 120), 6 ) + "]]></POLIZSEC>";
          wvarResult = wvarResult + "<CERTIPOL><![CDATA[" + Strings.mid( strParseString, (wvarPos + 126), 4 ) + "]]></CERTIPOL>";
          wvarResult = wvarResult + "<CERTIANN><![CDATA[" + Strings.mid( strParseString, (wvarPos + 130), 4 ) + "]]></CERTIANN>";
          wvarResult = wvarResult + "<CERTISEC><![CDATA[" + Strings.mid( strParseString, (wvarPos + 134), 6 ) + "]]></CERTISEC>";
          wvarResult = wvarResult + "<SUPLENUM><![CDATA[" + Strings.mid( strParseString, (wvarPos + 140), 4 ) + "]]></SUPLENUM>";
          wvarResult = wvarResult + "</CLIENTE>";
          //
          wvarPos = wvarPos + 144;
        }
        wvarStep = 190;
        wvarResult = wvarResult + "</CLIENTES>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=Se ha producido un error en la ejecuci�n de la b�squeda.  " + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 

      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarClisecag + wvarDocumtips + wvarDocumdats + wvarClienapes + wvarRamopcods + wvarPolizanns + wvarPolizsecs + wvarCertipols + wvarCertianns + wvarCertisecs + wvarRamopcodTR + wvarPolizannTR + wvarPolizsecTR + wvarCertipolTR + wvarCertiannTR + wvarCertisecTR + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
