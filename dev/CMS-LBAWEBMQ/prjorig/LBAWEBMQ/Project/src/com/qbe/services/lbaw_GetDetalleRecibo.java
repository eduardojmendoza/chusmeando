package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetDetalleRecibo implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetDetalleRecibo";
  static final String mcteOpID = "0017";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Agente = "//AGENTE";
  static final String mcteParam_Poliza = "//POLIZA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarAgente = "";
    String wvarPoliza = "";
    String wvarCiaAsCod = "";
    String wvarAgentCod = "";
    String wvarAgentCla = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    Variant wvarArr = new Variant();
    //
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarAgente = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Agente ) */ );
      wvarPoliza = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Poliza ) */ );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 25;
      wvarArr.set( Strings.split( wvarAgente, "-", -1 ) );
      wvarAgentCla = wvarArr.getValueAt( 0 ).toString();
      wvarAgentCod = wvarArr.getValueAt( 1 ).toString();
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarAgentCod + wvarAgentCla + wvarPoliza;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos = 41;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 170;
      if( !Strings.mid( strParseString, wvarPos, 2 ).equals( "ER" ) )
      {
        wvarResult = wvarResult + "<RECIBOS>";
        wvarPos = wvarPos + 2;
        //
        wvarStep = 180;
        while( (wvarPos < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos, 9 ) ).equals( "000000000" )) )
        {
          wvarResult = wvarResult + "<RECIBO>";
          wvarResult = wvarResult + "<NRORECIBO>" + Strings.trim( Strings.mid( strParseString, wvarPos, 9 ) ) + "</NRORECIBO>";
          wvarResult = wvarResult + "<IMPORTE>" + Strings.mid( strParseString, (wvarPos + 9), 14 ) + "</IMPORTE>";
          wvarResult = wvarResult + "<IMPORTE_SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 23), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 23), 1 )) + "</IMPORTE_SIGNO>";
          wvarResult = wvarResult + "<COBRODES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 24), 20 ) ) + "]]></COBRODES>";
          wvarResult = wvarResult + "<TREINTA>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, (wvarPos + 44), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, (wvarPos + 44), 14 )) + "</TREINTA>";
          wvarResult = wvarResult + "<TREINTA_SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 58), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 58), 1 )) + "</TREINTA_SIGNO>";
          wvarResult = wvarResult + "<SESENTA>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, (wvarPos + 59), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, (wvarPos + 59), 14 )) + "</SESENTA>";
          wvarResult = wvarResult + "<SESENTA_SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 73), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 73), 1 )) + "</SESENTA_SIGNO>";
          wvarResult = wvarResult + "<NOVENTA>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, (wvarPos + 74), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, (wvarPos + 74), 14 )) + "</NOVENTA>";
          wvarResult = wvarResult + "<NOVENTA_SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 88), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 88), 1 )) + "</NOVENTA_SIGNO>";
          wvarResult = wvarResult + "<MAS90>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, (wvarPos + 89), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, (wvarPos + 89), 14 )) + "</MAS90>";
          wvarResult = wvarResult + "<MAS90_SIGNO>" + ((Strings.mid( strParseString, (wvarPos + 103), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, (wvarPos + 103), 1 )) + "</MAS90_SIGNO>";
          wvarResult = wvarResult + "<RECHAZODES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 104), 20 ) ) + "]]></RECHAZODES>";
          wvarResult = wvarResult + "<RECIBOEST><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 124), 15 ) ) + "]]></RECIBOEST>";
          wvarResult = wvarResult + "</RECIBO>";
          wvarPos = wvarPos + 139;
        }
        //
        wvarStep = 190;
        wvarResult = wvarResult + "</RECIBOS>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='NO SE ENCONTRARON DATOS DEL RECIBO DE EXIGIBLE'/></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarAgentCod + wvarAgentCla + wvarPoliza + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
