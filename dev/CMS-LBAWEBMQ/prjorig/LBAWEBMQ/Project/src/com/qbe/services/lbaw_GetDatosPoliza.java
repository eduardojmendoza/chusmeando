package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetDatosPoliza implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetDatosPoliza";
  static final String mcteOpID = "0028";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_RAMOPCOD = "//RAMOPCOD";
  static final String mcteParam_POLIZANN = "//POLIZANN";
  static final String mcteParam_POLIZSEC = "//POLIZSEC";
  static final String mcteParam_CERTIPOL = "//CERTIPOL";
  static final String mcteParam_CERTIANN = "//CERTIANN";
  static final String mcteParam_CERTISEC = "//CERTISEC";
  static final String mcteParam_SUPLENUM = "//SUPLENUM";
  /**
   *  Parametros de configuracion
   */
  static final String mcteParam_CIAASCOD = "//CIAASCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarCiaAsCod = "";
    String wvarRamoPCod = "";
    String wvarPolizAnn = "";
    String wvarPolizSec = "";
    String wvarCertipol = "";
    String wvarCertiann = "";
    String wvarCertisec = "";
    String wvarSuplenum = "";
    int wvarPos = 0;
    int wvarPosCamp = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    int wvariCounter = 0;
    //
    //
    //
    //Dim wvarPoliza          As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarRamoPCod = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_RAMOPCOD ) */ ), 4 );
      wvarPolizAnn = Strings.right( Strings.fill( 2, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZANN ) */ ), 2 );
      wvarPolizSec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_POLIZSEC ) */ ), 6 );
      wvarCertipol = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIPOL ) */ ), 4 );
      wvarCertiann = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTIANN ) */ ), 4 );
      wvarCertisec = Strings.right( Strings.fill( 6, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_CERTISEC ) */ ), 6 );
      wvarSuplenum = Strings.right( Strings.fill( 4, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_SUPLENUM ) */ ), 4 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuraci�n
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 60;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarRamoPCod + wvarPolizAnn + wvarPolizSec + wvarCertipol + wvarCertiann + wvarCertisec + wvarSuplenum;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      //
      wvarStep = 150;
      wvarResult = "";
      wvarPos = 37;
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 160;
      if( (!Strings.trim( Strings.mid( strParseString, wvarPos, 9 ) ).equals( Strings.fill( 9, "0" ) )) && (wvarPos < wvarstrLen) )
      {
        wvarResult = wvarResult + "<POLIZA>";
        wvarResult = wvarResult + "<CLIENSEC><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 9 ) ) + "]]></CLIENSEC>";
        wvarResult = wvarResult + "<CLIENDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 9), 30 ) ) + "]]></CLIENDES>";
        wvarResult = wvarResult + "<GRUPOASE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 39), 60 ) ) + "]]></GRUPOASE>";
        wvarResult = wvarResult + "<OFICINA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 99), 40 ) ) + "]]></OFICINA>";
        wvarResult = wvarResult + "<GRABA><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 139), 10 ) ) + "]]></GRABA>";
        wvarResult = wvarResult + "<EFEIN><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 149), 10 ) ) + "]]></EFEIN>";
        wvarResult = wvarResult + "<VENCI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 159), 10 ) ) + "]]></VENCI>";
        wvarResult = wvarResult + "<EMISI><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 169), 10 ) ) + "]]></EMISI>";
        wvarResult = wvarResult + "<PLANNCOD><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 179), 3 ) ) + "]]></PLANNCOD>";
        wvarResult = wvarResult + "<PLANNDAB><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 182), 20 ) ) + "]]></PLANNDAB>";
        wvarResult = wvarResult + "<FRANQCOD><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 202), 2 ) ) + "]]></FRANQCOD>";
        wvarResult = wvarResult + "<FRANQDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 204), 60 ) ) + "]]></FRANQDES>";
        wvarResult = wvarResult + "<CONCECOD><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 264), 3 ) ) + "]]></CONCECOD>";
        wvarResult = wvarResult + "<CONCEDAB><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 267), 20 ) ) + "]]></CONCEDAB>";
        wvarResult = wvarResult + "<RAMOPCOD-R><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 287), 4 ) ) + "]]></RAMOPCOD-R>";
        wvarResult = wvarResult + "<POLIZANN-R><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 291), 2 ) ) + "]]></POLIZANN-R>";
        wvarResult = wvarResult + "<POLIZSEC-R><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 293), 6 ) ) + "]]></POLIZSEC-R>";
        wvarResult = wvarResult + "<CERTIPOL-R><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 299), 4 ) ) + "]]></CERTIPOL-R>";
        wvarResult = wvarResult + "<CERTIANN-R><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 303), 4 ) ) + "]]></CERTIANN-R>";
        wvarResult = wvarResult + "<CERTISEC-R><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 307), 6 ) ) + "]]></CERTISEC-R>";
        wvarResult = wvarResult + "<FORDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 313), 20 ) ) + "]]></FORDES>";
        wvarResult = wvarResult + "<CODDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 333), 20 ) ) + "]]></CODDES>";
        wvarResult = wvarResult + "<EXESEPOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 353), 7 ) ) + "]]></EXESEPOR>";
        wvarResult = wvarResult + "<COBFORDE><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 360), 20 ) ) + "]]></COBFORDE>";
        wvarResult = wvarResult + "<COBTIPDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 380), 20 ) ) + "]]></COBTIPDES>";
        wvarResult = wvarResult + "<CUENTNUME><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 400), 20 ) ) + "]]></CUENTNUME>";
        wvarResult = wvarResult + "<DEPEXPOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 420), 7 ) ) + "]]></DEPEXPOR>";
        wvarResult = wvarResult + "<DEPEXPOR>" + Strings.replace( String.valueOf( (Obj.toDouble( Strings.mid( strParseString, (wvarPos + 420), 7 ) ) / 100) ), ".", "," ) + "</DEPEXPOR>";

        wvarResult = wvarResult + "<SITUCPOLD><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 427), 30 ) ) + "]]></SITUCPOLD>";
        wvarResult = wvarResult + "<COASEGCOD><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 457), 1 ) ) + "]]></COASEGCOD>";
        wvarResult = wvarResult + "<COASEGDES><![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 458), 20 ) ) + "]]></COASEGDES>";
        wvarResult = wvarResult + "<CAMPANAS>";

        wvarPos = wvarPos + 478;

        wvarPosCamp = wvarPos;

        for( wvariCounter = 0; wvariCounter <= 9; wvariCounter++ )
        {
          while( (wvarPosCamp < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPosCamp, 4 ) ).equals( "" )) )
          {
            //
            wvarStep = 180;
            wvarResult = wvarResult + "<CAMPANA>";
            wvarResult = wvarResult + "<CAMPACOD>  <![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPosCamp, 4 ) ) + "]]></CAMPACOD>";
            wvarResult = wvarResult + "<CAMPADES>  <![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPosCamp + 4), 20 ) ) + "]]></CAMPADES>";
            wvarResult = wvarResult + "</CAMPANA>";
            //
            wvarPosCamp = wvarPosCamp + 24;
          }
        }
        wvarPos = wvarPos + 240;

        wvarResult = wvarResult + "</CAMPANAS>";
        wvarResult = wvarResult + "<SWACREPR>  <![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos, 1 ) ) + "]]></SWACREPR>";
        wvarResult = wvarResult + "<SWADICIO>  <![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 1), 1 ) ) + "]]></SWADICIO>";
        wvarResult = wvarResult + "<ANULA>     <![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 2), 10 ) ) + "]]></ANULA>";
        wvarResult = wvarResult + "<MOTIVOANU> <![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 12), 30 ) ) + "]]></MOTIVOANU>";
        wvarResult = wvarResult + "</POLIZA>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje='No se encontraron datos de la p�liza'/></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 

      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarRamoPCod + wvarPolizAnn + wvarPolizSec + wvarCertipol + wvarCertiann + wvarCertisec + wvarSuplenum + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
