package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetVehiculos implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetVehiculos";
  /**
   * Const mcteOpID              As String = "0093"
   */
  static final String mcteOpID = "0051";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_AuMarCod = "//AUMARCOD";
  static final String mcteParam_AuVehDes = "//AUVEHDES";
  static final String mcteParam_AuFecVig = "//AUFECVIG";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarAuMarCod = "";
    String wvarAuVehDes = "";
    String wvarAuFecVig = "";
    String wvarNow = "";
    int wvarPos = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarAuMarCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AuMarCod ) */ );
      //error: syntax error: near "Not":
      //unsup: If (.selectSingleNode(mcteParam_AuFecVig) Is Not Nothing) Then
      wvarAuFecVig = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AuFecVig ) */ );
      //error: syntax error: near "Else":
      //unsup: Else
      wvarAuFecVig = DateTime.year( DateTime.now() ) + Strings.right( "00" + DateTime.month( DateTime.now() ), 2 ) + Strings.right( ("00" + DateTime.day( DateTime.now() )), 2 );
      //error: syntax error: near "End If":
      //unsup: End If
      //wvarAuVehDes = Left(.selectSingleNode(mcteParam_AuVehDes).Text & String(35, " "), 35) 'MQ 0093
      //MQ 0051
      wvarAuVehDes = Strings.left( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AuVehDes ) */ ) + Strings.fill( 65, " " ), 65 );
      //
      // Quitamos esto y ponemos una nueva variable wvarAuVehDes en donde la fecha de vigencia se puede
      // especificar
      //wvarNow = Year(Now) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2)
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      //wvarArea = mcteOpID & wvarAuMarCod & wvarAuVehDes & wvarNow
      wvarArea = mcteOpID + wvarAuMarCod + wvarAuVehDes + wvarAuFecVig;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      wvarMQError = wobjFrame2MQ.Execute( wvarArea, strParseString, null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG" ) */.toString() ).toInt();
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";
      //wvarPos = 49 'MQ 0093
      //MQ0051
      wvarPos = 79;

      wvarstrLen = Strings.len( strParseString );

      if( Strings.mid( strParseString, wvarPos, 5 ).equals( "00000" ) )
      {
        // NO SE ENCONTRARON DATOS
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /><option value='0'>NO SE ENCONTRARON DATOS</option></Response>" );
      }
      else
      {
        // ARMA LA RESPUESTA
        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, wvarPos, 5 ).equals( "00000" )) )
        {
          wvarResult = wvarResult + "<OPTION value='" + Strings.mid( strParseString, wvarPos, 21 ) + "'>";
          //wvarResult = wvarResult & "<![CDATA[" & Trim(Mid(strParseString, wvarPos + 21, 50)) & "]]></OPTION>" 'MQ 0093
          //MQ 0051
          wvarResult = wvarResult + "<![CDATA[" + Strings.trim( Strings.mid( strParseString, (wvarPos + 21), 80 ) ) + "]]></OPTION>";
          //wvarPos = wvarPos + 71 'MQ 0093
          //MQ 0051
          wvarPos = wvarPos + 101;
        }

        wvarStep = 120;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      //
      wvarStep = 130;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarAuMarCod + wvarAuVehDes + wvarNow + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
