package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetSumAseg implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetSumAseg";
  static final String mcteOpID = "0004";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_AuModCod = "//AUMODCOD";
  static final String mcteParam_AuProCod = "//AUPROCOD";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLResponse = null;
    diamondedge.util.XmlDom wobjXSLResponse = null;
    int wvarMQError = 0;
    String wvarArea = "";
    Object wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarAuModCod = "";
    String wvarAuProCod = "";
    int wvarPos = 0;
    int wvarPosCat = 0;
    String strParseString = "";
    int wvarstrLen = 0;
    //
    //
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarAuModCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AuModCod ) */ );
      wvarAuProCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AuProCod ) */ );
      //
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 20;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      wvarArea = mcteOpID + wvarAuModCod + wvarAuProCod;
      diamondedge.util.XmlDom.setText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */, String.valueOf( VB.val( diamondedge.util.XmlDom.getText( null /*unsup wobjXMLConfig.selectSingleNode( "//MQCONFIG/GMO_WAITINTERVAL" ) */ ) ) * 40 ) );
      wobjFrame2MQ = new ModGeneral.gcteClassMQConnection();
      //error: function 'Execute' was not found.
      //unsup: wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
      wobjFrame2MQ = null;
      //
      wvarStep = 150;
      if( wvarMQError != 0 )
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + "El servicio de consulta no se encuentra disponible" + String.valueOf( (char)(34) ) + " />" + "Codigo Error:" + wvarMQError + "</Response>" );
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName + "--" + mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + wvarMQError + "] - " + strParseString + " Area:" + wvarArea + " Hora:" + DateTime.now(), vbLogEventTypeError );
        //unsup GoTo ClearObjects
      }
      //
      wvarResult = "";
      wvarPos = 49;

      wvarstrLen = Strings.len( strParseString );

      if( !Strings.mid( strParseString, (wvarPos - 10), 4 ).equals( "0000" ) )
      {
        wvarResult = wvarResult + "<AUTIPCOD>" + Strings.mid( strParseString, wvarPos, 4 ) + "</AUTIPCOD>";
        wvarResult = wvarResult + "<AUTIPCOM>" + Strings.mid( strParseString, (wvarPos + 4), 1 ) + "</AUTIPCOM>";
        wvarResult = wvarResult + "<AUTIPDES>" + Strings.mid( strParseString, (wvarPos + 5), 30 ) + "</AUTIPDES>";
        wvarResult = wvarResult + "<AUCOMDES>" + Strings.mid( strParseString, (wvarPos + 30), 60 ) + "</AUCOMDES>";
        wvarPosCat = wvarPos + 2395;
        wvarResult = wvarResult + "<AUCATCOD>" + Strings.mid( strParseString, (wvarPosCat + 35), 2 ) + "</AUCATCOD>";

        wvarPos = wvarPos + 95;
        wvarResult = wvarResult + "<COMBO>";

        while( (wvarPos < wvarstrLen) && (!Strings.mid( strParseString, wvarPos, 4 ).equals( "0000" )) )
        {
          wvarResult = wvarResult + "<OPTION value='" + Strings.mid( strParseString, wvarPos, 4 ) + "' ";
          wvarResult = wvarResult + "sumaseg='" + String.valueOf( Obj.toDouble( Strings.mid( strParseString, (wvarPos + 4), 13 ) ) ) + "," + Strings.mid( strParseString, (wvarPos + 4 + 13), 2 ) + "'>";
          wvarResult = wvarResult + Strings.mid( strParseString, wvarPos, 4 ) + "</OPTION>";
          wvarPos = wvarPos + 23;
        }

        wvarResult = wvarResult + "</COMBO>";
        wvarStep = 120;
        //
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "false" + String.valueOf( (char)(34) ) + " mensaje=NO SE ENCONTRARON DATOS DEL VEHICULO " + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " /></Response>" );
      }
      //
      wvarStep = 130;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;

      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        wobjXMLResponse = (diamondedge.util.XmlDom) null;
        wobjXSLResponse = (diamondedge.util.XmlDom) null;
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarAuModCod + wvarAuProCod + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
