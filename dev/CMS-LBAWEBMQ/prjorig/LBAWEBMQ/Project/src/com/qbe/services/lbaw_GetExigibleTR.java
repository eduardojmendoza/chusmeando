package com.qbe.services;
import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
/**
 * Objetos del FrameWork
 */

public class lbaw_GetExigibleTR implements com.qbe.services.Variant, com.qbe.services.ObjectControl, com.qbe.services.HSBCInterfaces.IAction
{
  /**
   * Implementacion de los objetos
   * Datos de la accion
   */
  static final String mcteClassName = "lbawA_OVLBAMQ.lbaw_GetExigibleTR";
  static final String mcteOpID = "0016";
  /**
   * Parametros XML de Entrada
   */
  static final String mcteParam_Cliensec = "//CLIENSEC";
  static final String mcteParam_AgentCod = "//AGENTCOD";
  static final String mcteParam_AgentCla = "//AGENTCLA";
  static final String mcteParam_NrPoliza = "//NRPOLIZA";
  private Object mobjCOM_Context = null;
  private Object mobjEventLog = null;
  /**
   * static variable for method: IAction_Execute
   */
  private final String wcteFnName = "IAction_Execute";

  private int IAction_Execute( String Request, Variant Response, String ContextInfo )
  {
    int IAction_Execute = 0;
    Variant vbLogEventTypeError = new Variant();
    diamondedge.util.XmlDom wobjXMLRequest = null;
    diamondedge.util.XmlDom wobjXMLConfig = null;
    diamondedge.util.XmlDom wobjXMLParametros = null;
    int wvarMQError = 0;
    String wvarArea = "";
    com.qbe.services.HSBCInterfaces.IAction wobjFrame2MQ = null;
    int wvarStep = 0;
    String wvarResult = "";
    String wvarCliensec = "";
    String wvarAgentCod = "";
    String wvarAgentCla = "";
    String wvarNrPoliza = "";
    String wvarCiaAsCod = "";
    Variant wvarPos = new Variant();
    String strParseString = "";
    int wvarstrLen = 0;
    String wvarCodError = "";
    //
    //
    //
    //
    //    Dim wvarParamPoliza     As String
    //    Dim wvarParamAgentCod   As String
    //    Dim wvarParamAgentCla   As String
    //
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~
    try 
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~
      //
      wvarStep = 10;
      wobjXMLRequest = new diamondedge.util.XmlDom();
      //unsup wobjXMLRequest.async = false;
      wobjXMLRequest.loadXML( Request );
      wvarCliensec = Strings.right( Strings.fill( 9, "0" ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_Cliensec ) */ ), 9 );
      wvarAgentCod = Strings.right( Strings.fill( 4, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AgentCod ) */ ), 4 );
      wvarAgentCla = Strings.right( Strings.fill( 2, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_AgentCla ) */ ), 2 );
      wvarNrPoliza = Strings.right( Strings.fill( 30, " " ) + diamondedge.util.XmlDom.getText( null /*unsup wobjXMLRequest.selectSingleNode( mcteParam_NrPoliza ) */ ), 30 );
      //
      wvarStep = 20;
      wobjXMLRequest = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 30;
      //Levanto los datos de la cola de MQ del archivo de configuración
      wobjXMLConfig = new diamondedge.util.XmlDom();
      //unsup wobjXMLConfig.async = false;
      wobjXMLConfig.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteConfFileName );
      //
      // LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
      wvarStep = 60;
      wobjXMLParametros = new diamondedge.util.XmlDom();
      //unsup wobjXMLParametros.async = false;
      wobjXMLParametros.load( System.getProperty("user.dir") + "\\" + ModGeneral.gcteParamFileName );
      wvarCiaAsCod = diamondedge.util.XmlDom.getText( null /*unsup wobjXMLParametros.selectSingleNode( ModGeneral.gcteNodosComisiones + ModGeneral.gcteCIAASCOD ) */ );
      //
      wvarStep = 70;
      wobjXMLParametros = (diamondedge.util.XmlDom) null;
      //
      wvarStep = 80;
      wvarArea = mcteOpID + wvarCiaAsCod + wvarCliensec + wvarAgentCod + wvarAgentCla + wvarNrPoliza;
      //
      wvarStep = 160;
      wvarResult = "";
      wvarPos.set( 50 );
      //
      wvarstrLen = Strings.len( strParseString );
      //
      wvarStep = 170;
      wvarCodError = Strings.mid( strParseString, wvarPos.toInt(), 2 );
      if( (!wvarCodError.equals( "ER" )) && (!wvarCodError.equals( "" )) )
      {
        //Cargo las variables para llamar a un segundo mensaje de ser necesario
        wvarResult = wvarResult + "<AGENTCOD>" + Strings.mid( strParseString, wvarPos.subtract( new Variant( 36 ) ).toInt(), 4 ) + "</AGENTCOD>";
        wvarResult = wvarResult + "<AGENTCLA>" + Strings.mid( strParseString, wvarPos.subtract( new Variant( 32 ) ).toInt(), 2 ) + "</AGENTCLA>";
        wvarResult = wvarResult + "<NRPOLIZA>" + Strings.mid( strParseString, wvarPos.subtract( new Variant( 30 ) ).toInt(), 30 ) + "</NRPOLIZA>";
        wvarResult = wvarResult + "<CODERROR>" + wvarCodError + "</CODERROR>";
        wvarResult = wvarResult + "<PRODUCTOR><![CDATA[" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 2 ) ).toInt(), 30 ) ) + "]]></PRODUCTOR>";
        wvarResult = wvarResult + "<DEUDAS_EXIGIBLES>";
        wvarPos.set( wvarPos.add( new Variant( 32 ) ) );
        //
        wvarStep = 180;
        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(strParseString), new Variant(wvarstrLen) } );
        //
        //            While wvarCodError = "TR"
        //                '
        //                Set wobjMessage = Nothing
        //                Set wobjMessage = New MQAX200.MQMessage
        //                '
        //                With wobjMessage
        //                    .Format = "MQSTR"
        //                    .MessageData = mcteOpID & wvarCiaasCod & wvarCliensec & wvarParamAgentCod & wvarParamAgentCla & wvarParamPoliza
        //                    .Expiry = 40
        //                    .Report = MQRO_EXPIRATION + MQRO_COPY_MSG_ID_TO_CORREL_ID
        //                    .ReplyToQueueManagerName = wvarQueueManager
        //                    .ReplyToQueueName = wvarGetQueue
        //                End With
        //                '
        //                Set wobjPMO = Nothing
        //                Set wobjPMO = New MQAX200.MQPutMessageOptions
        //                wobjPMO.Options = MQPMO_NEW_MSG_ID
        //                '
        //                wvarStep = 140
        //                wobjPutQueue.Put wobjMessage, wobjPMO
        //                '
        //                Set wobjGMO = Nothing
        //                Set wobjGMO = New MQAX200.MQGetMessageOptions
        //                wobjGMO.MatchOptions = MQMO_MATCH_MSG_ID
        //                wobjGMO.Options = MQGMO_WAIT
        //                wobjGMO.WaitInterval = wvarGMOWaitInterval * 2
        //                '
        //                wvarStep = 150
        //                On Error Resume Next
        //                wobjGetQueue.Get wobjMessage, wobjGMO
        //                '
        //                If wobjGetQueue.CompletionCode = MQCC_FAILED Then
        //                    wobjGMO.WaitInterval = wvarGMOWaitInterval * 4
        //                    wobjGetQueue.Get wobjMessage, wobjGMO
        //                End If
        //                '
        //                On Error GoTo ErrorHandler
        //                '
        //                strParseString = wobjMessage.MessageData
        //                '
        //                wvarPos = 50
        //                wvarstrLen = Len(strParseString)
        //                '
        //                wvarStep = 170
        //                wvarCodError = Mid(strParseString, wvarPos, 2)
        //                If wvarCodError <> "ER" Then
        //                    '
        //                    wvarParamPoliza = Mid(strParseString, wvarPos - 30, 30)
        //                    wvarParamAgentCod = Mid(strParseString, wvarPos - 36, 4)
        //                    wvarParamAgentCla = Mid(strParseString, wvarPos - 32, 2)
        //
        //                    wvarPos = wvarPos + 32
        //                    '
        //                    wvarStep = 180
        //                    wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        //                End If
        //            Wend
        //
        wvarStep = 190;
        wvarResult = wvarResult + "</DEUDAS_EXIGIBLES>";
        //
        wvarStep = 200;
        Response.set( "<Response><Estado resultado=" + String.valueOf( (char)(34) ) + "true" + String.valueOf( (char)(34) ) + " mensaje=" + String.valueOf( (char)(34) ) + String.valueOf( (char)(34) ) + " />" + wvarResult + "</Response>" );
      }
      else
      {
        Response.set( "<Response><Estado resultado='false' mensaje='NO SE ENCONTRARON DATOS DE LA DEUDA EXIGIBLE' /></Response>" );
      }
      //
      wvarStep = 210;
      /*unsup mobjCOM_Context.SetComplete() */;
      IAction_Execute = 0;
      //
      ClearObjects: 
      // LIBERO LOS OBJETOS
      wobjXMLConfig = (diamondedge.util.XmlDom) null;
      return IAction_Execute;
      //
      //~~~~~~~~~~~~~~~
    }
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        mobjEventLog.Log( this.getValue( "EventLog_Category.evtLog_Category_Unexpected" ), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarCliensec + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*unsup mobjCOM_Context.SetAbort() */;
        //unsup Resume ClearObjects
        Err.clear();
      }
      catch( Exception _e2_ )
      {
      }
    }
    return IAction_Execute;
  }

  private void ObjectControl_Activate() throws Exception
  {
  }

  private boolean ObjectControl_CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    return ObjectControl_CanBePooled;
  }

  private void ObjectControl_Deactivate() throws Exception
  {
  }

  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
  {
    String ParseoMensaje = "";
    String wvarResult = "";


    while( (wvarPos.toInt() < wvarstrLen) && (!Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 30 ) ).equals( "" )) )
    {
      wvarResult = wvarResult + "<DEUDA_EXIGIBLE>";
      wvarResult = wvarResult + "<CLIENDES><![CDATA[" + ((Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 30 ) ).equals( "," )) ? "" : Strings.trim( Strings.mid( strParseString, wvarPos.toInt(), 30 ) )) + "]]></CLIENDES>";
      wvarResult = wvarResult + "<POLIZA>" + Strings.trim( Strings.mid( strParseString, wvarPos.add( new Variant( 30 ) ).toInt(), 30 ) ) + "</POLIZA>";
      wvarResult = wvarResult + "<IMPTOT>" + Strings.mid( strParseString, wvarPos.add( new Variant( 60 ) ).toInt(), 14 ) + "</IMPTOT>";
      wvarResult = wvarResult + "<IMPTOT_SIGNO>" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 74 ) ).toInt(), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, wvarPos.add( new Variant( 74 ) ).toInt(), 1 )) + "</IMPTOT_SIGNO>";
      wvarResult = wvarResult + "<TREINTA>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, wvarPos.add( new Variant( 75 ) ).toInt(), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, wvarPos.add( new Variant( 75 ) ).toInt(), 14 )) + "</TREINTA>";
      wvarResult = wvarResult + "<TREINTA_SIGNO>" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 89 ) ).toInt(), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, wvarPos.add( new Variant( 89 ) ).toInt(), 1 )) + "</TREINTA_SIGNO>";
      wvarResult = wvarResult + "<SESENTA>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, wvarPos.add( new Variant( 90 ) ).toInt(), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, wvarPos.add( new Variant( 90 ) ).toInt(), 14 )) + "</SESENTA>";
      wvarResult = wvarResult + "<SESENTA_SIGNO>" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 104 ) ).toInt(), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, wvarPos.add( new Variant( 104 ) ).toInt(), 1 )) + "</SESENTA_SIGNO>";
      wvarResult = wvarResult + "<NOVENTA>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, wvarPos.add( new Variant( 105 ) ).toInt(), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, wvarPos.add( new Variant( 105 ) ).toInt(), 14 )) + "</NOVENTA>";
      wvarResult = wvarResult + "<NOVENTA_SIGNO>" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 119 ) ).toInt(), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, wvarPos.add( new Variant( 119 ) ).toInt(), 1 )) + "</NOVENTA_SIGNO>";
      wvarResult = wvarResult + "<MAS90>" + ((String.valueOf( Obj.toDouble( Strings.mid( strParseString, wvarPos.add( new Variant( 120 ) ).toInt(), 14 ) ) ).equals( "0" )) ? "0" : Strings.mid( strParseString, wvarPos.add( new Variant( 120 ) ).toInt(), 14 )) + "</MAS90>";
      wvarResult = wvarResult + "<MAS90_SIGNO>" + ((Strings.mid( strParseString, wvarPos.add( new Variant( 134 ) ).toInt(), 1 ).equals( "+" )) ? "" : Strings.mid( strParseString, wvarPos.add( new Variant( 134 ) ).toInt(), 1 )) + "</MAS90_SIGNO>";

      wvarResult = wvarResult + "<SITUCPOL>";
      if( Strings.mid( strParseString, wvarPos.add( new Variant( 135 ) ).toInt(), 1 ).equals( "S" ) )
      {
        wvarResult = wvarResult + "SUSPEN";
      }
      else if( Strings.mid( strParseString, wvarPos.add( new Variant( 135 ) ).toInt(), 1 ).equals( "V" ) )
      {
        wvarResult = wvarResult + "VIGENTE";
      }
      else
      {
        wvarResult = wvarResult + Strings.mid( strParseString, wvarPos.add( new Variant( 135 ) ).toInt(), 1 );
      }
      wvarResult = wvarResult + "</SITUCPOL>";
      wvarResult = wvarResult + "<AGENTE><![CDATA[" + Strings.mid( strParseString, wvarPos.add( new Variant( 140 ) ).toInt(), 2 ) + "-" + Strings.mid( strParseString, wvarPos.add( new Variant( 136 ) ).toInt(), 4 ) + "]]></AGENTE>";
      wvarResult = wvarResult + "</DEUDA_EXIGIBLE>";
      wvarPos.set( wvarPos.add( new Variant( 142 ) ) );
    }
    ParseoMensaje = wvarResult;
    return ParseoMensaje;
  }

  /**
   * -------------------------------------------------------------------------------------------------------------------
   * // Metodos del Framework
   * -------------------------------------------------------------------------------------------------------------------
   */
  public void Activate() throws Exception
  {
    //
    mobjCOM_Context = null /*unsup this.GetObjectContext() */;
    mobjEventLog = new HSBC.EventLog();
    //
  }

  public boolean CanBePooled() throws Exception
  {
    boolean ObjectControl_CanBePooled = false;
    //
    ObjectControl_CanBePooled = true;
    //
    return ObjectControl_CanBePooled;
  }

  public void Deactivate() throws Exception
  {
    //
    mobjCOM_Context = (Object) null;
    mobjEventLog = null;
    //
  }
}
