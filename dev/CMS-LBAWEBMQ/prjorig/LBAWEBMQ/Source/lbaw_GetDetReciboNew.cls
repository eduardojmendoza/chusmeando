VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetDetReciboNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetDetReciboNew"
Const mcteOpID              As String = "0031"

'Parametros XML de Entrada
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_SUPLENUM    As String = "//SUPLENUM"

Const mcteParam_RECIBANN    As String = "//RECIBANN"
Const mcteParam_RECIBTIP    As String = "//RECIBTIP"
Const mcteParam_RECIBSEC    As String = "//RECIBSEC"
' Parametros de configuracion
Const mcteParam_CIAASCOD    As String = "//CIAASCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    Dim wvarCiaAsCod        As String
    Dim wvarRamoPCod        As String
    Dim wvarPolizAnn        As String
    Dim wvarPolizSec        As String
    Dim wvarCertipol        As String
    Dim wvarCertiann        As String
    Dim wvarCertisec        As String
    Dim wvarSuplenum        As String
    Dim wvarRecibAnn        As String
    Dim wvarRecibTip        As String
    Dim wvarRecibSec        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarRamoPCod = Right(String(4, " ") & .selectSingleNode(mcteParam_RAMOPCOD).Text, 4)
        wvarPolizAnn = Right(String(2, "0") & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizSec = Right(String(6, "0") & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertipol = Right(String(4, "0") & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiann = Right(String(4, "0") & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertisec = Right(String(6, "0") & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_SUPLENUM).Text, 4)
        wvarRecibAnn = Right(String(2, "0") & .selectSingleNode(mcteParam_RECIBANN).Text, 2)
        wvarRecibTip = Right(String(1, "0") & .selectSingleNode(mcteParam_RECIBTIP).Text, 1)
        wvarRecibSec = Right(String(6, "0") & .selectSingleNode(mcteParam_RECIBSEC).Text, 6)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 60
    wvarArea = mcteOpID & wvarCiaAsCod & wvarRecibAnn & wvarRecibTip & wvarRecibSec & wvarRamoPCod & wvarPolizAnn & wvarPolizSec & wvarCertipol & wvarCertiann & wvarCertisec & wvarSuplenum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 150
    wvarResult = ""
    wvarPos = 14 '44
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 160
    If Mid(strParseString, wvarPos, 2) <> "ER" Then
        wvarResult = wvarResult & "<DETALLE_RECIBOS>"
        wvarStep = 170
        wvarPos = wvarPos + 2
        While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos + 90, 15)) <> ""
            wvarStep = 180
            wvarResult = wvarResult & "<DETALLE>"
            wvarResult = wvarResult & "<RAMOPCOD><![CDATA[" & Trim(Mid(strParseString, wvarPos, 4)) & "]]></RAMOPCOD>"
            wvarResult = wvarResult & "<POLIZANN>" & Trim(Mid(strParseString, wvarPos + 4, 2)) & "</POLIZANN>"
            wvarResult = wvarResult & "<POLIZSEC>" & Trim(Mid(strParseString, wvarPos + 6, 6)) & "</POLIZSEC>"
            wvarResult = wvarResult & "<CERTIPOL>" & Trim(Mid(strParseString, wvarPos + 12, 4)) & "</CERTIPOL>"
            wvarResult = wvarResult & "<CERTIANN>" & Trim(Mid(strParseString, wvarPos + 16, 4)) & "</CERTIANN>"
            wvarResult = wvarResult & "<CERTISEC>" & Trim(Mid(strParseString, wvarPos + 20, 6)) & "</CERTISEC>"
            wvarResult = wvarResult & "<SUPLENUM>" & Trim(Mid(strParseString, wvarPos + 26, 4)) & "</SUPLENUM>"
            wvarResult = wvarResult & "<GRUPOASEG><![CDATA[" & Trim(Mid(strParseString, wvarPos + 30, 60)) & "]]></GRUPOASEG>"
            wvarResult = wvarResult & "<PRIMAIMP>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 90, 15)) / 100), ".", ",") & "</PRIMAIMP>"
            wvarResult = wvarResult & "<RECTOIMP>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 105, 15)) / 100), ".", ",") & "</RECTOIMP>"
            wvarResult = wvarResult & "<IVAIMPOR>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 120, 15)) / 100), ".", ",") & "</IVAIMPOR>"
            wvarResult = wvarResult & "<IVARETEN>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 135, 15)) / 100), ".", ",") & "</IVARETEN>"
            wvarResult = wvarResult & "<SIGNO>" & Trim(Mid(strParseString, wvarPos + 150, 1)) & "</SIGNO>"
            wvarResult = wvarResult & "<EMISIFEC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 151, 10)) & "]]></EMISIFEC>"
            wvarResult = wvarResult & "<EFECFEC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 161, 10)) & "]]></EFECFEC>"
            wvarResult = wvarResult & "<VENCFEC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 171, 10)) & "]]></VENCFEC>"
            wvarResult = wvarResult & "<SITUCDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 181, 10)) & "]]></SITUCDES>"
            wvarResult = wvarResult & "<SITUCFEC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 191, 10)) & "]]></SITUCFEC>"
            wvarResult = wvarResult & "<COBROCODDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 201, 20)) & "]]></COBROCODDES>"
            wvarResult = wvarResult & "<COBROTIPDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 221, 20)) & "]]></COBROTIPDES>"
            wvarResult = wvarResult & "<BANCOCODDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 241, 20)) & "]]></BANCOCODDES>"
            wvarResult = wvarResult & "<SUCURCODDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 261, 20)) & "]]></SUCURCODDES>"
            wvarResult = wvarResult & "<CUENTNUME><![CDATA[" & Trim(Mid(strParseString, wvarPos + 281, 20)) & "]]></CUENTNUME>"
            wvarResult = wvarResult & "</DETALLE>"
            
            wvarPos = wvarPos + 301
        Wend
        wvarStep = 190
        wvarResult = wvarResult & "</DETALLE_RECIBOS>"
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='NO SE ENCONTRARON DATOS DE RECIBOS'/></Response>"
    End If
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:

    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarRecibAnn & wvarRecibTip & wvarRecibSec & wvarRamoPCod & wvarPolizAnn & wvarPolizSec & wvarCertipol & wvarCertiann & wvarCertisec & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub





















