VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_HSGetCotizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OVLBAMQ.lbaw_HSGetCotizacion"
Const mcteOpID                  As String = "0011"

'Parametros XML de Entrada
Const mcteParam_TipoHogar       As String = "//TIPOHOGAR"
Const mcteParam_ProviCod        As String = "//PROVICOD"
Const mcteParam_Localidad       As String = "//LOCALIDAD"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober           As String = "//Request/COBERTURAS/COBERTURA"
Const mcteParam_CoberCod        As String = "COBERCOD"
Const mcteParam_ContrMod        As String = "CONTRMOD"
Const mcteParam_CapitAsg        As String = "CAPITASG"
'PRODUCTOR
Const mcteParam_AgeCod          As String = "//AGECOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarRamoPCod        As String
    Dim wvarAgentCla        As String
    Dim wvarPlanncod        As String
    Dim wvarPolizSec        As String
    Dim wvarPolizAnn        As String
    Dim wvarCotiID          As String
    Dim wvarFechaDia        As String
    Dim wvarFechaVenc       As String
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarMensaje         As String
    Dim wvarTipoHogar       As String
    Dim wvarProviCod        As String
    Dim wvarLocalidad       As String
    Dim wvarCoberturas      As String
    Dim wvarPrecio          As String
    '
    Dim strParseString      As String
    Dim wvariCounter        As Integer
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    ' Obtengo el numero de cotizacion
    wvarStep = 10
    wvarRequest = "<Request></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetNroCot")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 20
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(wvarResponse)
        wvarCotiID = .selectSingleNode("//NROCOT").Text
        wvarFechaDia = .selectSingleNode("//FECHA_DIA").Text
        wvarFechaVenc = "99991231"
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarTipoHogar = Right("00" & .selectSingleNode(mcteParam_TipoHogar).Text, 2)
        wvarProviCod = Right("00" & .selectSingleNode(mcteParam_ProviCod).Text, 2)
        wvarLocalidad = Left(.selectSingleNode(mcteParam_Localidad).Text & String(8, " "), 8)
        
        wvarCoberturas = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Cober)
        wvarStep = 40
        
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarCoberturas = wvarCoberturas & Right("000" & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CoberCod).Text, 3)
            wvarCoberturas = wvarCoberturas & Right("00" & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_ContrMod).Text, 2)
            wvarCoberturas = wvarCoberturas & "00"
            wvarCoberturas = wvarCoberturas & Right(String(15, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CapitAsg).Text, 15)
            wvarCoberturas = wvarCoberturas & "S"
        Next
        
        For wvariCounter = wobjXMLList.length To 98
            wvarCoberturas = wvarCoberturas & String(22, "0") & " "
        Next
    End With
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLList = Nothing
    '
    wvarStep = 50
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS PARA COTIZAR HOGAR (ParametrosCotizador.xml)
    wvarStep = 70
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarRamoPCod = .selectSingleNode(gcteNodosHogar & gcteRAMOPCOD).Text
        wvarPlanncod = .selectSingleNode(gcteNodosHogar & gctePLANNCOD).Text
        wvarPolizSec = .selectSingleNode(gcteNodosHogar & gctePOLIZSEC).Text
        wvarPolizAnn = .selectSingleNode(gcteNodosHogar & gctePOLIZANN).Text
    End With
    '
    wvarStep = 80
    Set wobjXMLParametros = Nothing
    '
    ' BUSCO LOS DATOS DE TELEFONO
    wvarStep = 90
    wvarRequest = "<Request><PORTAL>LBA</PORTAL><RAMOPCOD>" & wvarRamoPCod & "</RAMOPCOD></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetPortalComerc")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    '
    wvarStep = 100
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    wobjXMLParametros.async = False
    wobjXMLParametros.loadXML wvarResponse
    wvarAgentCla = Trim(wobjXMLParametros.selectSingleNode(mcteParam_AgeCod).Text)
    Set wobjXMLParametros = Nothing
    Set wobjClass = Nothing
    '
    wvarMensaje = mcteOpID & Right(String(9, "0") & wvarCotiID, 9) & String(18, "0")
    wvarMensaje = wvarMensaje & String(4, "0") & "3"
    wvarMensaje = wvarMensaje & wvarAgentCla
    wvarMensaje = wvarMensaje & "0001" & wvarRamoPCod
    wvarMensaje = wvarMensaje & wvarPolizAnn & wvarPolizSec
    wvarMensaje = wvarMensaje & String(4, "0") & String(4, "0") & String(6, "0") & String(4, "0")
    wvarMensaje = wvarMensaje & wvarFechaDia & wvarFechaVenc & wvarFechaVenc & wvarFechaDia & wvarFechaVenc
    wvarMensaje = wvarMensaje & "1" & "0004" & "  "
    wvarMensaje = wvarMensaje & String(15, "0") & String(15, "0") & String(15, "0")
    wvarMensaje = wvarMensaje & wvarTipoHogar & "00" & "00" & " " & " " & "00" & "00" & " " & " " & " " & " " & " " & "00"
    wvarMensaje = wvarMensaje & wvarPlanncod & String(3, "0") & "  "
    wvarMensaje = wvarMensaje & String(5, " ") & String(40, " ") & String(5, " ") & String(4, " ") & String(4, " ") & " "
    wvarMensaje = wvarMensaje & String(40, " ") & wvarLocalidad & wvarProviCod & "00" & String(8, " ") & String(14, " ")
    wvarMensaje = wvarMensaje & wvarCoberturas
    wvarMensaje = wvarMensaje & String(40, " ")
    
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
     
      wvarResult = ""
    
      wvarPrecio = FormatearNumero(Replace(CStr(CDbl(Mid(strParseString, 10, 18)) / 100), ",", "."))
      wvarResult = "<COT_NRO>" & wvarCotiID & "</COT_NRO><COTIZACION>" & wvarPrecio & "</COTIZACION>"
    '
    wvarStep = 180
    Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    '
    wvarStep = 190
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub










