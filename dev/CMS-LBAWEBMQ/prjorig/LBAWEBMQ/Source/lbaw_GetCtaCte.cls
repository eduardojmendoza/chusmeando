VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetCtaCte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetCtaCte"
Const mcteOpID              As String = "0015"

'Parametros XML de Entrada
Const mcteParam_Cliensec    As String = "//CLIENSEC"
Const mcteParam_EfectAnn    As String = "//EFECTANN"
Const mcteParam_EfectMes    As String = "//EFECTMES"

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:key name=""cuentas"" match=""CTACTE"" use=""FECHA"" />"
  
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='PRODUCTOR'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:value-of select=""//PRODUCTOR""/>"
    wvarStrXSL = wvarStrXSL & "     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='CTACTES'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:for-each select=""//CTACTE[count(. | key('cuentas', FECHA)[1]) = 1]"">"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='CTACTE'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:element name='FECHA'>"
    wvarStrXSL = wvarStrXSL & "                      <xsl:value-of select=""FECHA""/>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:element>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='F' and MOALFCOD='$' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_PESOS'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_PESOS_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='F' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='BRUTO_DOLAR_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='C' and MOALFCOD='$' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_PESOS'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_PESOS_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='C' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='NETO_DOLAR_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='P' and MOALFCOD='$' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_PESOS'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_PESOS_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='P' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='PEND_DOLAR_SIGNO'></xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:if test=""not(//CTACTE[CONCEPTO='D' and MOALFCOD='U$S' and FECHA=current()/FECHA])"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:element name='COTIZ_DOLAR'>0</xsl:element>"
    wvarStrXSL = wvarStrXSL & "                 </xsl:if>"
    
    wvarStrXSL = wvarStrXSL & "                 <xsl:for-each select=""key('cuentas', FECHA)"">"
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='F'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='$'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_PESOS'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_PESOS_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='BRUTO_DOLAR_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='C'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='$'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_PESOS'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_PESOS_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='NETO_DOLAR_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='P'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='$'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_PESOS'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_PESOS_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='PEND_DOLAR_SIGNO'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""SIGNO""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                     <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                         <xsl:when test=""CONCEPTO='D'"">"
    wvarStrXSL = wvarStrXSL & "                             <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "                                 <xsl:when test=""MOALFCOD='U$S'"">"
    wvarStrXSL = wvarStrXSL & "                                     <xsl:element name='COTIZ_DOLAR'>"
    wvarStrXSL = wvarStrXSL & "                                         <xsl:value-of select=""IMPORTE""/>"
    wvarStrXSL = wvarStrXSL & "                                     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "                                 </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                             </xsl:choose>                                                                "
    wvarStrXSL = wvarStrXSL & "                         </xsl:when>"
    wvarStrXSL = wvarStrXSL & "                     </xsl:choose>"
    
    wvarStrXSL = wvarStrXSL & "                 </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "             </xsl:element>"
    wvarStrXSL = wvarStrXSL & "      </xsl:for-each>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PRODUCTOR'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    Dim wvarEfectAnn        As String
    Dim wvarEfectMes        As String
    Dim wvarCiaAsCod        As String

    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCliensec = Right(String(9, "0") & .selectSingleNode(mcteParam_Cliensec).Text, 9)
        wvarEfectAnn = .selectSingleNode(mcteParam_EfectAnn).Text
        wvarEfectMes = Right(String(2, "0") & .selectSingleNode(mcteParam_EfectMes).Text, 2)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    If wvarEfectAnn = "" Then
        wvarEfectAnn = Year(Date)
        wvarEfectMes = Format(Month(Date), "00")
    End If
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarCliensec & wvarEfectAnn & wvarEfectMes & "00"
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 160
    wvarResult = ""
    wvarPos = 22
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 170
    If Mid(strParseString, wvarPos, 2) <> "ER" Then
        wvarResult = wvarResult & "<Response>"
        wvarResult = wvarResult & "<PRODUCTOR><![CDATA[" & Trim(Mid(strParseString, wvarPos + 2, 70)) & "]]></PRODUCTOR>"
        wvarResult = wvarResult & "<CTACTES>"
        wvarPos = wvarPos + 72
        '
        wvarStep = 180
        While wvarPos < wvarstrLen And Mid(strParseString, wvarPos, 11) <> "00000000   "
            wvarResult = wvarResult & "<CTACTE>"
            wvarResult = wvarResult & "<FECHA>" & Mid(strParseString, wvarPos + 6, 2) & "/" & Mid(strParseString, wvarPos + 4, 2) & "/" & Mid(strParseString, wvarPos, 4) & "</FECHA>"
            wvarResult = wvarResult & "<MOALFCOD>" & Trim(Mid(strParseString, wvarPos + 8, 3)) & "</MOALFCOD>"
            wvarResult = wvarResult & "<CONCEPTO>" & Mid(strParseString, wvarPos + 11, 1) & "</CONCEPTO>"
            If Mid(strParseString, wvarPos + 11, 1) = "D" Then
                wvarResult = wvarResult & "<IMPORTE>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 12, 15)) / 10000000), ".", ",") & "</IMPORTE>"
            Else
                wvarResult = wvarResult & "<IMPORTE>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 12, 15)) / 100), ".", ",") & "</IMPORTE>"
            End If
            wvarResult = wvarResult & "<SIGNO>" & IIf(Mid(strParseString, wvarPos + 27, 1) = "+", "", Mid(strParseString, wvarPos + 27, 1)) & "</SIGNO>"
            wvarResult = wvarResult & "</CTACTE>"
            wvarPos = wvarPos + 28
        Wend
        '
        wvarStep = 190
        wvarResult = wvarResult & "</CTACTES>"
        wvarResult = wvarResult & "</Response>"
        '
        wvarStep = 200
        '
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 210
        wobjXMLResponse.loadXML wvarResult
        '
        wvarStep = 220
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 230
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 240
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=NO SE ENCONTRARON DATOS " & Chr(34) & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 250
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarCliensec & wvarEfectAnn & wvarEfectMes & "00" & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub










