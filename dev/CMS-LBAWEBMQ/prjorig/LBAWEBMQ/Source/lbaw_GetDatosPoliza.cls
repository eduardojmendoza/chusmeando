VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetDatosPoliza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetDatosPoliza"
Const mcteOpID              As String = "0028"

'Parametros XML de Entrada
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_SUPLENUM    As String = "//SUPLENUM"
' Parametros de configuracion
Const mcteParam_CIAASCOD    As String = "//CIAASCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    'Dim wvarPoliza          As String
    Dim wvarCiaAsCod        As String
    Dim wvarRamoPCod        As String
    Dim wvarPolizAnn        As String
    Dim wvarPolizSec        As String
    Dim wvarCertipol        As String
    Dim wvarCertiann        As String
    Dim wvarCertisec        As String
    Dim wvarSuplenum        As String
    '
    Dim wvarPos             As Long
    Dim wvarPosCamp         As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvariCounter        As Integer
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarRamoPCod = Right(String(4, " ") & .selectSingleNode(mcteParam_RAMOPCOD).Text, 4)
        wvarPolizAnn = Right(String(2, "0") & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizSec = Right(String(6, "0") & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertipol = Right(String(4, "0") & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiann = Right(String(4, "0") & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertisec = Right(String(6, "0") & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_SUPLENUM).Text, 4)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 60
    wvarArea = mcteOpID & wvarCiaAsCod & wvarRamoPCod & wvarPolizAnn & wvarPolizSec & wvarCertipol & wvarCertiann & wvarCertisec & wvarSuplenum
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 150
    wvarResult = ""
    wvarPos = 37
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 160
    If Trim(Mid(strParseString, wvarPos, 9)) <> String(9, "0") And wvarPos < wvarstrLen Then
        wvarResult = wvarResult & "<POLIZA>"
        wvarResult = wvarResult & "<CLIENSEC><![CDATA[" & Trim(Mid(strParseString, wvarPos, 9)) & "]]></CLIENSEC>"
        wvarResult = wvarResult & "<CLIENDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 9, 30)) & "]]></CLIENDES>"
        wvarResult = wvarResult & "<GRUPOASE><![CDATA[" & Trim(Mid(strParseString, wvarPos + 39, 60)) & "]]></GRUPOASE>"
        wvarResult = wvarResult & "<OFICINA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 99, 40)) & "]]></OFICINA>"
        wvarResult = wvarResult & "<GRABA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 139, 10)) & "]]></GRABA>"
        wvarResult = wvarResult & "<EFEIN><![CDATA[" & Trim(Mid(strParseString, wvarPos + 149, 10)) & "]]></EFEIN>"
        wvarResult = wvarResult & "<VENCI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 159, 10)) & "]]></VENCI>"
        wvarResult = wvarResult & "<EMISI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 169, 10)) & "]]></EMISI>"
        wvarResult = wvarResult & "<PLANNCOD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 179, 3)) & "]]></PLANNCOD>"
        wvarResult = wvarResult & "<PLANNDAB><![CDATA[" & Trim(Mid(strParseString, wvarPos + 182, 20)) & "]]></PLANNDAB>"
        wvarResult = wvarResult & "<FRANQCOD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 202, 2)) & "]]></FRANQCOD>"
        wvarResult = wvarResult & "<FRANQDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 204, 60)) & "]]></FRANQDES>"
        wvarResult = wvarResult & "<CONCECOD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 264, 3)) & "]]></CONCECOD>"
        wvarResult = wvarResult & "<CONCEDAB><![CDATA[" & Trim(Mid(strParseString, wvarPos + 267, 20)) & "]]></CONCEDAB>"
        wvarResult = wvarResult & "<RAMOPCOD-R><![CDATA[" & Trim(Mid(strParseString, wvarPos + 287, 4)) & "]]></RAMOPCOD-R>"
        wvarResult = wvarResult & "<POLIZANN-R><![CDATA[" & Trim(Mid(strParseString, wvarPos + 291, 2)) & "]]></POLIZANN-R>"
        wvarResult = wvarResult & "<POLIZSEC-R><![CDATA[" & Trim(Mid(strParseString, wvarPos + 293, 6)) & "]]></POLIZSEC-R>"
        wvarResult = wvarResult & "<CERTIPOL-R><![CDATA[" & Trim(Mid(strParseString, wvarPos + 299, 4)) & "]]></CERTIPOL-R>"
        wvarResult = wvarResult & "<CERTIANN-R><![CDATA[" & Trim(Mid(strParseString, wvarPos + 303, 4)) & "]]></CERTIANN-R>"
        wvarResult = wvarResult & "<CERTISEC-R><![CDATA[" & Trim(Mid(strParseString, wvarPos + 307, 6)) & "]]></CERTISEC-R>"
        wvarResult = wvarResult & "<FORDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 313, 20)) & "]]></FORDES>"
        wvarResult = wvarResult & "<CODDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 333, 20)) & "]]></CODDES>"
        wvarResult = wvarResult & "<EXESEPOR><![CDATA[" & Trim(Mid(strParseString, wvarPos + 353, 7)) & "]]></EXESEPOR>"
        wvarResult = wvarResult & "<COBFORDE><![CDATA[" & Trim(Mid(strParseString, wvarPos + 360, 20)) & "]]></COBFORDE>"
        wvarResult = wvarResult & "<COBTIPDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 380, 20)) & "]]></COBTIPDES>"
        wvarResult = wvarResult & "<CUENTNUME><![CDATA[" & Trim(Mid(strParseString, wvarPos + 400, 20)) & "]]></CUENTNUME>"
        wvarResult = wvarResult & "<DEPEXPOR><![CDATA[" & Trim(Mid(strParseString, wvarPos + 420, 7)) & "]]></DEPEXPOR>"
        wvarResult = wvarResult & "<DEPEXPOR>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 420, 7)) / 100), ".", ",") & "</DEPEXPOR>"
        
        wvarResult = wvarResult & "<SITUCPOLD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 427, 30)) & "]]></SITUCPOLD>"
        wvarResult = wvarResult & "<COASEGCOD><![CDATA[" & Trim(Mid(strParseString, wvarPos + 457, 1)) & "]]></COASEGCOD>"
        wvarResult = wvarResult & "<COASEGDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 458, 20)) & "]]></COASEGDES>"
        wvarResult = wvarResult & "<CAMPANAS>"
        
        wvarPos = wvarPos + 478
        
        wvarPosCamp = wvarPos
        
        For wvariCounter = 0 To 9
            While wvarPosCamp < wvarstrLen And Trim(Mid(strParseString, wvarPosCamp, 4)) <> ""
                '
                wvarStep = 180
                wvarResult = wvarResult & "<CAMPANA>"
                wvarResult = wvarResult & "<CAMPACOD>  <![CDATA[" & Trim(Mid(strParseString, wvarPosCamp, 4)) & "]]></CAMPACOD>"
                wvarResult = wvarResult & "<CAMPADES>  <![CDATA[" & Trim(Mid(strParseString, wvarPosCamp + 4, 20)) & "]]></CAMPADES>"
                wvarResult = wvarResult & "</CAMPANA>"
                '
                wvarPosCamp = wvarPosCamp + 24
            Wend
        Next
        wvarPos = wvarPos + 240
        
        wvarResult = wvarResult & "</CAMPANAS>"
        wvarResult = wvarResult & "<SWACREPR>  <![CDATA[" & Trim(Mid(strParseString, wvarPos, 1)) & "]]></SWACREPR>"
        wvarResult = wvarResult & "<SWADICIO>  <![CDATA[" & Trim(Mid(strParseString, wvarPos + 1, 1)) & "]]></SWADICIO>"
        wvarResult = wvarResult & "<ANULA>     <![CDATA[" & Trim(Mid(strParseString, wvarPos + 2, 10)) & "]]></ANULA>"
        wvarResult = wvarResult & "<MOTIVOANU> <![CDATA[" & Trim(Mid(strParseString, wvarPos + 12, 30)) & "]]></MOTIVOANU>"
        wvarResult = wvarResult & "</POLIZA>"
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='No se encontraron datos de la p�liza'/></Response>"
    End If
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:

    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarRamoPCod & wvarPolizAnn & wvarPolizSec & wvarCertipol & wvarCertiann & wvarCertisec & wvarSuplenum & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


















