VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_GetClientesTR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetClientesTR"
Const mcteOpID              As String = "0032"

'Parametros XML de Entrada
Const mcteParam_Clisecag      As String = "//CLISECAG"
Const mcteParam_Documtips     As String = "//DOCUMTIPS"
Const mcteParam_Documdats     As String = "//DOCUMDATS"
Const mcteParam_Clienapes     As String = "//CLIENAPES"
Const mcteParam_Ramopcods     As String = "//RAMOPCODS"
Const mcteParam_Polizanns     As String = "//POLIZANNS"
Const mcteParam_Polizsecs     As String = "//POLIZSECS"
Const mcteParam_Certipols     As String = "//CERTIPOLS"
Const mcteParam_Certianns     As String = "//CERTIANNS"
Const mcteParam_Certisecs     As String = "//CERTISECS"
'variables para rellamada por mensaje truncado
Const mcteParam_RamopcodTR    As String = "//RAMOPCODTR"
Const mcteParam_PolizannTR    As String = "//POLIZANNTR"
Const mcteParam_PolizsecTR    As String = "//POLIZSECTR"
Const mcteParam_CertipolTR    As String = "//CERTIPOLTR"
Const mcteParam_CertiannTR    As String = "//CERTIANNTR"
Const mcteParam_CertisecTR    As String = "//CERTISECTR"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    
    Dim wvarClisecag      As String
    Dim wvarDocumtips     As String
    Dim wvarDocumdats     As String
    Dim wvarClienapes     As String
    Dim wvarRamopcods     As String
    Dim wvarPolizanns     As String
    Dim wvarPolizsecs     As String
    Dim wvarCertipols     As String
    Dim wvarCertianns     As String
    Dim wvarCertisecs     As String
    '
    Dim wvarRamopcodTR    As String
    Dim wvarPolizannTR    As String
    Dim wvarPolizsecTR    As String
    Dim wvarCertipolTR    As String
    Dim wvarCertiannTR    As String
    Dim wvarCertisecTR    As String
    '
    Dim wvarCliensec        As String
    Dim wvarCiaAsCod        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarCodError        As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarClisecag = Right(String(9, "0") & .selectSingleNode(mcteParam_Clisecag).Text, 9)
        wvarDocumtips = Right(String(2, "0") & .selectSingleNode(mcteParam_Documtips).Text, 2)
        wvarDocumdats = Left(.selectSingleNode(mcteParam_Documdats).Text & String(11, " "), 11)
        wvarClienapes = Left(.selectSingleNode(mcteParam_Clienapes).Text & String(34, " "), 34)
        wvarRamopcods = Right(String(4, " ") & .selectSingleNode(mcteParam_Ramopcods).Text, 4)
        wvarPolizanns = Left(.selectSingleNode(mcteParam_Polizanns).Text & String(2, "0"), 2)
        wvarPolizsecs = Left(.selectSingleNode(mcteParam_Polizsecs).Text & String(6, "0"), 6)
        wvarCertipols = Left(.selectSingleNode(mcteParam_Certipols).Text & String(4, "0"), 4)
        wvarCertianns = Left(.selectSingleNode(mcteParam_Certianns).Text & String(4, "0"), 4)
        wvarCertisecs = Left(.selectSingleNode(mcteParam_Certisecs).Text & String(6, "0"), 6)
        '
        wvarRamopcodTR = Right(String(4, " ") & .selectSingleNode(mcteParam_RamopcodTR).Text, 4)
        wvarPolizannTR = Left(.selectSingleNode(mcteParam_PolizannTR).Text & String(2, "0"), 2)
        wvarPolizsecTR = Left(.selectSingleNode(mcteParam_PolizsecTR).Text & String(6, "0"), 6)
        wvarCertipolTR = Left(.selectSingleNode(mcteParam_CertipolTR).Text & String(4, "0"), 4)
        wvarCertiannTR = Left(.selectSingleNode(mcteParam_CertiannTR).Text & String(4, "0"), 4)
        wvarCertisecTR = Left(.selectSingleNode(mcteParam_CertisecTR).Text & String(6, "0"), 6)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 60
    wvarArea = mcteOpID & wvarCiaAsCod & wvarClisecag & wvarDocumtips & wvarDocumdats & wvarClienapes & wvarRamopcods & wvarPolizanns & wvarPolizsecs & wvarCertipols & wvarCertianns & wvarCertisecs & wvarRamopcodTR & wvarPolizannTR & wvarPolizsecTR & wvarCertipolTR & wvarCertiannTR & wvarCertisecTR
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 150
    wvarResult = ""
    wvarPos = 113
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 160
    wvarCodError = Mid(strParseString, wvarPos, 2)
    If wvarCodError <> "ER" And wvarCodError <> "" Then
       'Cargo las variables para llamar a un segundo mensaje de ser necesario
       'strParseString = Trim(Mid(strParseString, 93, Len(strParseString) - 93))
       'strParseString = Trim(Mid(strParseString, 3, Len(strParseString) - 2))
       'wvarstrLen = Len(strParseString)
       'wvarPos = 1
       wvarResult = wvarResult & "<CLIENTES>"
       wvarResult = wvarResult & "<RAMOPCODTR>" & Mid(strParseString, wvarPos - 26, 4) & "</RAMOPCODTR>"
       wvarResult = wvarResult & "<POLIZANNTR>" & Mid(strParseString, wvarPos - 22, 2) & "</POLIZANNTR>"
       wvarResult = wvarResult & "<POLIZSECTR>" & Mid(strParseString, wvarPos - 20, 6) & "</POLIZSECTR>"
       wvarResult = wvarResult & "<CERTIPOLTR>" & Mid(strParseString, wvarPos - 14, 4) & "</CERTIPOLTR>"
       wvarResult = wvarResult & "<CERTIANNTR>" & Mid(strParseString, wvarPos - 10, 4) & "</CERTIANNTR>"
       wvarResult = wvarResult & "<CERTISECTR>" & Mid(strParseString, wvarPos - 6, 6) & "</CERTISECTR>"
       wvarResult = wvarResult & "<CODERROR>" & wvarCodError & "</CODERROR>"
       '
       wvarPos = wvarPos + 2
       While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos + 22, 11)) <> ""
            '
            wvarResult = wvarResult & "<CLIENTE>"
            wvarResult = wvarResult & "<DOCUMTIP><![CDATA[" & Mid(strParseString, wvarPos, 2) & "]]></DOCUMTIP>"
            wvarResult = wvarResult & "<DOCUMDAB><![CDATA[" & Trim(Mid(strParseString, wvarPos + 2, 20)) & "]]></DOCUMDAB>"
            wvarResult = wvarResult & "<DOCUMDAT><![CDATA[" & Mid(strParseString, wvarPos + 22, 11) & "]]></DOCUMDAT>"
            wvarResult = wvarResult & "<CLIENSEC><![CDATA[" & Mid(strParseString, wvarPos + 33, 9) & "]]></CLIENSEC>"
            wvarResult = wvarResult & "<CLIENDES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 42, 30)) & "]]></CLIENDES>"
            wvarResult = wvarResult & "<TOMARIES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 72, 30)) & "]]></TOMARIES>"
            wvarResult = wvarResult & "<SITUCPOL><![CDATA[" & Trim(Mid(strParseString, wvarPos + 102, 12)) & "]]></SITUCPOL>"
            wvarResult = wvarResult & "<RAMOPCOD><![CDATA[" & Mid(strParseString, wvarPos + 114, 4) & "]]></RAMOPCOD>"
            wvarResult = wvarResult & "<POLIZANN><![CDATA[" & Mid(strParseString, wvarPos + 118, 2) & "]]></POLIZANN>"
            wvarResult = wvarResult & "<POLIZSEC><![CDATA[" & Mid(strParseString, wvarPos + 120, 6) & "]]></POLIZSEC>"
            wvarResult = wvarResult & "<CERTIPOL><![CDATA[" & Mid(strParseString, wvarPos + 126, 4) & "]]></CERTIPOL>"
            wvarResult = wvarResult & "<CERTIANN><![CDATA[" & Mid(strParseString, wvarPos + 130, 4) & "]]></CERTIANN>"
            wvarResult = wvarResult & "<CERTISEC><![CDATA[" & Mid(strParseString, wvarPos + 134, 6) & "]]></CERTISEC>"
            wvarResult = wvarResult & "<SUPLENUM><![CDATA[" & Mid(strParseString, wvarPos + 140, 4) & "]]></SUPLENUM>"
            wvarResult = wvarResult & "</CLIENTE>"
            '
            wvarPos = wvarPos + 144
       Wend
       wvarStep = 190
       wvarResult = wvarResult & "</CLIENTES>"
       '
       wvarStep = 200
       Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
       Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=Se ha producido un error en la ejecuci�n de la b�squeda.  " & Chr(34) & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:

    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarClisecag & wvarDocumtips & wvarDocumdats & wvarClienapes & wvarRamopcods & wvarPolizanns & wvarPolizsecs & wvarCertipols & wvarCertianns & wvarCertisecs & wvarRamopcodTR & wvarPolizannTR & wvarPolizsecTR & wvarCertipolTR & wvarCertiannTR & wvarCertisecTR & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub




















