VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetDetalleRecibo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetDetalleRecibo"
Const mcteOpID              As String = "0017"

'Parametros XML de Entrada
Const mcteParam_Agente      As String = "//AGENTE"
Const mcteParam_Poliza      As String = "//POLIZA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarAgente          As String
    Dim wvarPoliza          As String
    Dim wvarCiaAsCod        As String
    Dim wvarAgentCod        As String
    Dim wvarAgentCla        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarArr             As Variant
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarAgente = .selectSingleNode(mcteParam_Agente).Text
        wvarPoliza = .selectSingleNode(mcteParam_Poliza).Text
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 25
    wvarArr = Split(wvarAgente, "-")
    wvarAgentCla = wvarArr(0)
    wvarAgentCod = wvarArr(1)
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarAgentCod & wvarAgentCla & wvarPoliza
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 160
    wvarResult = ""
    wvarPos = 41
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 170
    If Mid(strParseString, wvarPos, 2) <> "ER" Then
        wvarResult = wvarResult & "<RECIBOS>"
        wvarPos = wvarPos + 2
        '
        wvarStep = 180
        While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 9)) <> "000000000"
            wvarResult = wvarResult & "<RECIBO>"
            wvarResult = wvarResult & "<NRORECIBO>" & Trim(Mid(strParseString, wvarPos, 9)) & "</NRORECIBO>"
            wvarResult = wvarResult & "<IMPORTE>" & Mid(strParseString, wvarPos + 9, 14) & "</IMPORTE>"
            wvarResult = wvarResult & "<IMPORTE_SIGNO>" & IIf(Mid(strParseString, wvarPos + 23, 1) = "+", "", Mid(strParseString, wvarPos + 23, 1)) & "</IMPORTE_SIGNO>"
            wvarResult = wvarResult & "<COBRODES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 24, 20)) & "]]></COBRODES>"
            wvarResult = wvarResult & "<TREINTA>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 44, 14))) = "0", "0", Mid(strParseString, wvarPos + 44, 14)) & "</TREINTA>"
            wvarResult = wvarResult & "<TREINTA_SIGNO>" & IIf(Mid(strParseString, wvarPos + 58, 1) = "+", "", Mid(strParseString, wvarPos + 58, 1)) & "</TREINTA_SIGNO>"
            wvarResult = wvarResult & "<SESENTA>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 59, 14))) = "0", "0", Mid(strParseString, wvarPos + 59, 14)) & "</SESENTA>"
            wvarResult = wvarResult & "<SESENTA_SIGNO>" & IIf(Mid(strParseString, wvarPos + 73, 1) = "+", "", Mid(strParseString, wvarPos + 73, 1)) & "</SESENTA_SIGNO>"
            wvarResult = wvarResult & "<NOVENTA>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 74, 14))) = "0", "0", Mid(strParseString, wvarPos + 74, 14)) & "</NOVENTA>"
            wvarResult = wvarResult & "<NOVENTA_SIGNO>" & IIf(Mid(strParseString, wvarPos + 88, 1) = "+", "", Mid(strParseString, wvarPos + 88, 1)) & "</NOVENTA_SIGNO>"
            wvarResult = wvarResult & "<MAS90>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 89, 14))) = "0", "0", Mid(strParseString, wvarPos + 89, 14)) & "</MAS90>"
            wvarResult = wvarResult & "<MAS90_SIGNO>" & IIf(Mid(strParseString, wvarPos + 103, 1) = "+", "", Mid(strParseString, wvarPos + 103, 1)) & "</MAS90_SIGNO>"
            wvarResult = wvarResult & "<RECHAZODES><![CDATA[" & Trim(Mid(strParseString, wvarPos + 104, 20)) & "]]></RECHAZODES>"
            wvarResult = wvarResult & "<RECIBOEST><![CDATA[" & Trim(Mid(strParseString, wvarPos + 124, 15)) & "]]></RECIBOEST>"
            wvarResult = wvarResult & "</RECIBO>"
            wvarPos = wvarPos + 139
        Wend
        '
        wvarStep = 190
        wvarResult = wvarResult & "</RECIBOS>"
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='NO SE ENCONTRARON DATOS DEL RECIBO DE EXIGIBLE'/></Response>"
    End If
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarAgentCod & wvarAgentCla & wvarPoliza & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub














