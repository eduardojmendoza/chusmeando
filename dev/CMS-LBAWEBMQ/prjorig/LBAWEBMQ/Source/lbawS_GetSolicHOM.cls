VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbawS_GetSolicHOM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Esto archivo es unico solo para las pruebas UAT de Brokers
Const gcteConfFileName2       As String = "MQConfig2.xml"

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OVLBAMQ.lbawS_GetSolicHOM"
Const mcteOpID                  As String = "0045"

'Archivo de Config. del MQ
'Const gcteConfFileName2          As String = "MQConfig.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "//REQUESTID"
Const mcteParam_CODINST              As String = "//CODINST"
Const mcteParam_POLIZANN             As String = "//POLIZANN"
Const mcteParam_POLIZSEC             As String = "//POLIZSEC"
Const mcteParam_CobroCod             As String = "//COBROCOD"
Const mcteParam_CobroTip             As String = "//COBROTIP"
Const mcteParam_TipoHogar            As String = "//TIPOHOGAR"
Const mcteParam_PLANNCOD             As String = "//PLANNCOD"
Const mcteParam_Provi                As String = "//PROVI"
Const mcteParam_LocalidadCod         As String = "//LOCALIDADCOD"
Const mcteRAMOPCOD                   As String = "HOM1"
Const mcteParam_ClienIVA             As String = "//CLIENIVA"
Const mcteParam_NroCot               As String = "//COT_NRO"
Const mcteParam_CERTISEC             As String = "//CERTISEC"
Const mcteParam_DOMICDOM             As String = "//DOMICDOM"
Const mcteParam_DOMICDNU             As String = "//DOMICDNU"
Const mcteParam_DOMICPIS             As String = "//DOMICPIS"
Const mcteParam_DOMICPTA             As String = "//DOMICPTA"
Const mcteParam_DOMICPOB             As String = "//DOMICPOB"
Const mcteParam_USOTIPOS             As String = "//USOTIPOS"
Const mcteParam_CALDERA              As String = "//CALDERA"
Const mcteParam_ASCENSOR             As String = "//ASCENSOR"
Const mcteParam_ALARMTIP             As String = "//ALARMTIP"
Const mcteParam_GUARDTIP             As String = "//GUARDTIP"
Const mcteParam_REJAS                As String = "//REJAS"
Const mcteParam_PUERTABLIND          As String = "//PUERTABLIND"
Const mcteParam_DISYUNTOR            As String = "//DISYUNTOR"
Const mcteParam_BARRIOCOUNT          As String = "//BARRIOCOUNT"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober                As String = "//Request/COBERTURAS/COBERTURA"
Const mcteParam_CoberCod             As String = "COBERCOD"
Const mcteParam_ContrMod             As String = "CONTRMOD"
Const mcteParam_CapitAsg             As String = "CAPITASG"
'DATOS DE LAS CAMPAÑAS
Const mcteNodos_Campa                As String = "//Request/CAMPANIAS/CAMPANIA"
Const mcteParam_CampaCod             As String = "CAMPACOD"
'PRODUCTOR
Const mctePORTAL                     As String = "LBA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    '
    Dim wvarNroCot          As String
    Dim wvarFechaDia        As String
    Dim wvarFechaVenc       As String
    Dim wvarRamoPCod        As String
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarMensaje         As String
    Dim wvarCodErr          As String
    '
    Dim wvarRequestID       As String
    Dim wvarCodinst         As String
    Dim wvarPolizAnn        As String
    Dim wvarPolizSec        As String
    Dim wvarCobroCod        As String
    Dim wvarCobroTip        As String
    Dim wvarTipoHogar       As String
    Dim wvarPlanncod        As String
    Dim wvarProvi           As String
    Dim wvarLocalidadCod    As String
    Dim wvarCoberturas      As String
    Dim wvarCampanias       As String
    Dim wvarClienIVA        As String
    Dim wvarCertisec        As String
    Dim wvarCertipol        As String
    Dim wvarCertiann        As String
    Dim wvarDOMICDOM        As String
    Dim wvarDOMICDNU        As String
    Dim wvarDOMICPIS        As String
    Dim wvarDOMICPTA        As String
    Dim wvarDOMICPOB        As String
    Dim wvarUSOTIPOS        As String
    Dim wvarCALDERA         As String
    Dim wvarASCENSOR        As String
    Dim wvarALARMTIP        As String
    Dim wvarGUARDTIP        As String
    Dim wvarREJAS           As String
    Dim wvarPUERTABLIND     As String
    Dim wvarDISYUNTOR       As String
    Dim wvarBARRIOCOUNT     As String
    Dim wvarTIPO            As String
    Dim wvarMARCA           As String
    Dim wvarMODELO          As String
    '
    Dim wvarPrima           As String
    Dim wvarRecargos        As String
    Dim wvarIVA             As String
    Dim wvarDerEmi          As String
    Dim wvarSellados        As String
    Dim wvarIngBru          As String
    Dim wvarImpuestos       As String
    Dim wvarPrecioMensual   As String
    '
    Dim wvarParseString     As String
    Dim wvarCounter         As Integer
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    wvarStep = 10
    '
    ' CARGO EL XML DE ENTRADA
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)
    
    wvarStep = 20

    'CARGO LAS VARIABLES CON LOS DATOS DEL XML
    With wobjXMLRequest
      wvarRequestID = .selectSingleNode(mcteParam_REQUESTID).Text
      wvarCodinst = Right(String(4, "0") & .selectSingleNode(mcteParam_CODINST).Text, 4)
      wvarNroCot = Right(String(9, "0") & .selectSingleNode(mcteParam_NroCot).Text, 9)
      wvarFechaDia = CStr(Year(Date)) & Right(String(2, "0") & CStr(Month(Date)), 2) & Right(String(2, "0") & CStr(Day(Date)), 2)
      wvarFechaVenc = "99991231"
      wvarRamoPCod = mcteRAMOPCOD
      wvarPolizAnn = Right(String(2, "0") & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
      wvarPolizSec = Right(String(6, "0") & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
      wvarPlanncod = Right(String(3, "0") & .selectSingleNode(mcteParam_PLANNCOD).Text, 3)
      wvarCobroCod = Right(String(4, "0") & .selectSingleNode(mcteParam_CobroCod).Text, 4)
      wvarCobroTip = Right(String(2, " ") & .selectSingleNode(mcteParam_CobroTip).Text, 2)
      wvarTipoHogar = Right(String(2, "0") & .selectSingleNode(mcteParam_TipoHogar).Text, 2)
      wvarProvi = Right(String(2, "0") & .selectSingleNode(mcteParam_Provi).Text, 2)
      wvarLocalidadCod = Left(.selectSingleNode(mcteParam_LocalidadCod).Text & String(8, " "), 8)
      wvarClienIVA = .selectSingleNode(mcteParam_ClienIVA).Text
      wvarCertisec = Right(String(6, "0") & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
      wvarCertipol = Right(String(4, "0") & .selectSingleNode(mcteParam_CODINST).Text, 4)
      wvarCertiann = String(4, "0")
      wvarDOMICDOM = Left(.selectSingleNode(mcteParam_DOMICDOM).Text & String(40, " "), 40)
      wvarDOMICDNU = Left(.selectSingleNode(mcteParam_DOMICDNU).Text & String(5, " "), 5)
      wvarDOMICPIS = Left(.selectSingleNode(mcteParam_DOMICPIS).Text & String(4, " "), 4)
      wvarDOMICPTA = Left(.selectSingleNode(mcteParam_DOMICPTA).Text & String(4, " "), 4)
      wvarDOMICPOB = Left(.selectSingleNode(mcteParam_DOMICPOB).Text & String(40, " "), 40)
      wvarUSOTIPOS = Right(String(2, "0") & .selectSingleNode(mcteParam_USOTIPOS).Text, 2)
      wvarCALDERA = Left(.selectSingleNode(mcteParam_CALDERA).Text & String(1, " "), 1)
      wvarASCENSOR = Left(.selectSingleNode(mcteParam_ASCENSOR).Text & String(1, " "), 1)
      wvarALARMTIP = Right(String(2, "0") & .selectSingleNode(mcteParam_ALARMTIP).Text, 2)
      wvarGUARDTIP = Right(String(2, "0") & .selectSingleNode(mcteParam_GUARDTIP).Text, 2)
      wvarREJAS = Left(.selectSingleNode(mcteParam_REJAS).Text & String(1, " "), 1)
      wvarPUERTABLIND = Left(.selectSingleNode(mcteParam_PUERTABLIND).Text & String(1, " "), 1)
      wvarDISYUNTOR = Left(.selectSingleNode(mcteParam_DISYUNTOR).Text & String(1, " "), 1)
      wvarBARRIOCOUNT = Left(.selectSingleNode(mcteParam_BARRIOCOUNT).Text & String(14, " "), 14)
      
      wvarStep = 30
      'CARGO LAS COBERTURAS
      wvarCoberturas = ""
    
      Set wobjXMLList = .selectNodes(mcteNodos_Cober)
  
      For wvarCounter = 0 To wobjXMLList.length - 1
          wvarCoberturas = wvarCoberturas & Right(String(3, "0") & wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CoberCod).Text, 3)
          wvarCoberturas = wvarCoberturas & Right(String(2, "0") & wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_ContrMod).Text, 2)
          wvarCoberturas = wvarCoberturas & Right(String(2, "0") & wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_ContrMod).Text, 2) 'NroseCor
          wvarCoberturas = wvarCoberturas & Right(String(15, "0") & wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CapitAsg).Text, 15)
          wvarCoberturas = wvarCoberturas & "1" 'SelecMod
      Next
        
      For wvarCounter = wobjXMLList.length To 49
          wvarCoberturas = wvarCoberturas & String(22, "0") & " "
      Next
      'FIN COBERTURAS
    
      wvarStep = 40
      'CARGO LAS CAMPANIAS
      wvarCampanias = ""
      Set wobjXMLList = .selectNodes(mcteNodos_Campa)
  
      For wvarCounter = 0 To wobjXMLList.length - 1
          wvarCampanias = wvarCampanias & Right(String(4, "0") & wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CampaCod).Text, 4)
      Next
        
      For wvarCounter = wobjXMLList.length To 9
          wvarCampanias = wvarCampanias & String(4, " ")
      Next
      'FIN CAMPANIAS
    
    End With
      
    Set wobjXMLList = Nothing
    '
    wvarStep = 50
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    
    '
    wvarMensaje = mcteOpID & wvarNroCot
    wvarMensaje = wvarMensaje & String(18, "0") ' precio que va a calcular
    wvarMensaje = wvarMensaje & String(4, "0") ' zona
    wvarMensaje = wvarMensaje & wvarClienIVA
    
    '*** PRODUCTOR ***
    wvarMensaje = wvarMensaje & String(4, "0")
    
    wvarMensaje = wvarMensaje & "0001"         'CIAASCOD
    wvarMensaje = wvarMensaje & wvarRamoPCod   'RAMO = HOM1 - Lo toma del archivo ParametrosBroker.xml
    wvarMensaje = wvarMensaje & wvarPolizAnn
    wvarMensaje = wvarMensaje & wvarPolizSec
    wvarMensaje = wvarMensaje & wvarCertipol
    wvarMensaje = wvarMensaje & wvarCertiann
    wvarMensaje = wvarMensaje & wvarCertisec   'CertiSec
    wvarMensaje = wvarMensaje & String(4, "0") 'SupleNum
    wvarMensaje = wvarMensaje & Mid(wvarFechaDia, 1, 4)   'EfectAnn
    wvarMensaje = wvarMensaje & Mid(wvarFechaDia, 5, 2)   'EfectMes
    wvarMensaje = wvarMensaje & Mid(wvarFechaDia, 7, 2)   'EfectDia
    
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 1, 4)  'VenvgAnn
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 5, 2)  'VenvgMes
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 7, 2)  'VenvgDia
    
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 1, 4)  'VeninAnn
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 5, 2)  'VeninMes
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 7, 2)  'VeninDia
    
    wvarMensaje = wvarMensaje & Mid(wvarFechaDia, 1, 4)   'EmisiAnn
    wvarMensaje = wvarMensaje & Mid(wvarFechaDia, 5, 2)   'EmisiMes
    wvarMensaje = wvarMensaje & Mid(wvarFechaDia, 7, 2)   'EmisiDia
    
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 1, 4)  'EfevgAnn
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 5, 2)  'EfevgMes
    wvarMensaje = wvarMensaje & Mid(wvarFechaVenc, 7, 2)  'EfevgDia
    
    wvarMensaje = wvarMensaje & "1" 'CobroFor --> Siempre va 1
    
    wvarMensaje = wvarMensaje & wvarCobroCod
    wvarMensaje = wvarMensaje & wvarCobroTip
    
    wvarMensaje = wvarMensaje & String(15, "0")  'CapitAsg
    wvarMensaje = wvarMensaje & String(15, "0")  'CapitImp
    wvarMensaje = wvarMensaje & String(15, "0")  'ValorRie
    
    wvarMensaje = wvarMensaje & wvarTipoHogar
    wvarMensaje = wvarMensaje & "00"  'AlturNum
    wvarMensaje = wvarMensaje & wvarUSOTIPOS
    wvarMensaje = wvarMensaje & wvarCALDERA
    wvarMensaje = wvarMensaje & wvarASCENSOR
    wvarMensaje = wvarMensaje & wvarALARMTIP
    wvarMensaje = wvarMensaje & wvarGUARDTIP
    wvarMensaje = wvarMensaje & wvarREJAS
    wvarMensaje = wvarMensaje & wvarPUERTABLIND
    wvarMensaje = wvarMensaje & wvarDISYUNTOR
    wvarMensaje = wvarMensaje & " "   'ZoRieCod
    wvarMensaje = wvarMensaje & " "   'swPropie
    wvarMensaje = wvarMensaje & "00"  'MascoCod
    
    wvarMensaje = wvarMensaje & wvarPlanncod
    wvarMensaje = wvarMensaje & String(3, "0") 'ConceCod
    wvarMensaje = wvarMensaje & "  "           'FranqCod
    
    wvarMensaje = wvarMensaje & String(5, " ")  'DomicCal
    wvarMensaje = wvarMensaje & wvarDOMICDOM
    wvarMensaje = wvarMensaje & wvarDOMICDNU
    wvarMensaje = wvarMensaje & wvarDOMICPIS
    wvarMensaje = wvarMensaje & wvarDOMICPTA
    wvarMensaje = wvarMensaje & " "             'DomicEsc
    
    wvarMensaje = wvarMensaje & wvarDOMICPOB
    wvarMensaje = wvarMensaje & wvarLocalidadCod
    wvarMensaje = wvarMensaje & wvarProvi
    wvarMensaje = wvarMensaje & "00"            'PaissCCC
    wvarMensaje = wvarMensaje & String(8, " ")  'BlockCod
    
    wvarMensaje = wvarMensaje & wvarBARRIOCOUNT
    
    wvarMensaje = wvarMensaje & wvarCoberturas
    wvarMensaje = wvarMensaje & wvarCampanias
    '
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, wvarParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & wvarParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarResult = ""
    '
    wvarStep = 150
     If UCase(Left(wvarParseString, 2)) = "OK" Then
        wvarResult = "<LBA_WS res_code=""OK"" res_msg=""""><Response>" & _
                        "<REQUESTID>" & wobjXMLRequest.selectSingleNode(mcteParam_REQUESTID).Text & "</REQUESTID>" & _
                        "<COT_NRO>" & wobjXMLRequest.selectSingleNode(mcteParam_NroCot).Text & "</COT_NRO>" & _
                        "<PRECIOS>" & _
                            "<PRECIO>" & _
                                "<PRIMA>" & Format(CStr(CLng(Mid(wvarParseString, 3, 11)) / 100), "0.00") & "</PRIMA>" & _
                                "<RECARGOS>" & Format(CStr(CLng(Mid(wvarParseString, 14, 11)) / 100), "0.00") & "</RECARGOS>" & _
                                "<IVA>" & Format(CStr(CLng(Mid(wvarParseString, 25, 11)) / 100), "0.00") & "</IVA>" & _
                                "<DEREMI>" & Format(CStr(CLng(Mid(wvarParseString, 36, 11)) / 100), "0.00") & "</DEREMI>" & _
                                "<SELLADOS>" & Format(CStr(CLng(Mid(wvarParseString, 47, 11)) / 100), "0.00") & "</SELLADOS>" & _
                                "<INGBRU>" & Format(CStr(CLng(Mid(wvarParseString, 58, 11)) / 100), "0.00") & "</INGBRU>" & _
                                "<IMPUESTOS>" & Format(CStr(CLng(Mid(wvarParseString, 69, 11)) / 100), "0.00") & "</IMPUESTOS>" & _
                                "<VALOR>" & Format(CStr(CLng(Mid(wvarParseString, 80, 11)) / 100), "0.00") & "</VALOR>" & _
                            "</PRECIO>" & _
                        "</PRECIOS>" & _
                        "<TIEMPOPROCESO>" & CStr(CDbl(Mid(wvarParseString, 91, 6))) & "</TIEMPOPROCESO>" & _
                        "<CODIZONA>" & CStr(CDbl(Mid(wvarParseString, 97, 4))) & "</CODIZONA>" & _
                     "</Response></LBA_WS>"
    Else
        wvarResult = "<LBA_WS res_code=""" & UCase(Left(wvarParseString, 2)) & """ res_msg=""""></LBA_WS>"
    End If
        
        
    '
    wvarStep = 160
    Response = wvarResult
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
        
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
If wvarCodErr = "" Then
   wvarCodErr = "-50"
   wvarMensaje = "Ocurrio un error inesperado en la ejecucion del componente"
End If
Response = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetComplete
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


