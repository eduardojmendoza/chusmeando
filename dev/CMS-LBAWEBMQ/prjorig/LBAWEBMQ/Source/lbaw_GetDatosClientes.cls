VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetDatosClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetDatosClientes"
Const mcteOpID              As String = "0018"

'Parametros XML de Entrada
Const mcteParam_Cliensec      As String = "//CLIENSEC"
Const mcteParam_Poliza        As String = "//POLIZA"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    Dim wvarPoliza          As String
    Dim wvarCiaAsCod        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCliensec = Right(String(9, "0") & .selectSingleNode(mcteParam_Cliensec).Text, 9)
        wvarPoliza = Right(String(30, " ") & .selectSingleNode(mcteParam_Poliza).Text, 30)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 60
    wvarArea = mcteOpID & wvarCiaAsCod & wvarCliensec & wvarPoliza
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
        '
        wvarStep = 150
        wvarResult = ""
        wvarPos = 44
        '
        wvarstrLen = Len(strParseString)
        '
        wvarStep = 160
        If Trim(Mid(strParseString, wvarPos, 70)) <> "" And wvarPos < wvarstrLen Then
            wvarResult = wvarResult & "<DATOS>"
            wvarResult = wvarResult & "<CLIENTE>"
            wvarResult = wvarResult & "<NOMBRE><![CDATA[" & Trim(Mid(strParseString, wvarPos, 70)) & "]]></NOMBRE>"
            wvarResult = wvarResult & "<TIPODOCU><![CDATA[" & Trim(Mid(strParseString, wvarPos + 70, 20)) & "]]></TIPODOCU>"
            wvarResult = wvarResult & "<NUMEDOCU>" & Trim(Mid(strParseString, wvarPos + 90, 11)) & "</NUMEDOCU>"
            wvarResult = wvarResult & "<NACIONALI><![CDATA[" & Trim(Mid(strParseString, wvarPos + 101, 40)) & "]]></NACIONALI>"
            wvarResult = wvarResult & "<SEXO><![CDATA[" & Trim(Mid(strParseString, wvarPos + 141, 20)) & "]]></SEXO>"
        wvarResult = wvarResult & "<FECNAC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 161, 10)) & "]]></FECNAC>"
        wvarResult = wvarResult & "<ESTCIVIL><![CDATA[" & Trim(Mid(strParseString, wvarPos + 171, 20)) & "]]></ESTCIVIL>"
        wvarResult = wvarResult & "<FUMADOR>" & IIf(Mid(strParseString, wvarPos + 191, 1) = "S", "SI", "NO") & "</FUMADOR>"
        wvarResult = wvarResult & "<FECALTA><![CDATA[" & Trim(Mid(strParseString, wvarPos + 192, 10)) & "]]></FECALTA>"
        wvarResult = wvarResult & "<DIEZUR>" & IIf(Mid(strParseString, wvarPos + 202, 1) = "Z", "ZURDO", "DIESTRO") & "</DIEZUR>"
        wvarResult = wvarResult & "<FALLLECIM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 203, 10)) & "]]></FALLLECIM>"
        wvarResult = wvarResult & "<CALLEPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 213, 60)) & "]]></CALLEPART>"
        wvarResult = wvarResult & "<CPOPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 273, 5)) & "]]></CPOPART>"
        wvarResult = wvarResult & "<LOCALPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 278, 40)) & "]]></LOCALPART>"
        wvarResult = wvarResult & "<PROVPART><![CDATA[" & Trim(Mid(strParseString, wvarPos + 318, 20)) & "]]></PROVPART>"
        wvarResult = wvarResult & "<CALLECOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 338, 60)) & "]]></CALLECOM>"
        wvarResult = wvarResult & "<CPOCOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 398, 5)) & "]]></CPOCOM>"
        wvarResult = wvarResult & "<LOCALCOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 403, 40)) & "]]></LOCALCOM>"
        wvarResult = wvarResult & "<PROVCOM><![CDATA[" & Trim(Mid(strParseString, wvarPos + 443, 20)) & "]]></PROVCOM>"
        wvarResult = wvarResult & "</CLIENTE>"
        wvarPos = wvarPos + 462
        '
        wvarStep = 170
        wvarResult = wvarResult & "<CONTACTOS>"
        While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 30)) <> ""
            '
            wvarStep = 180
            wvarResult = wvarResult & "<CONTACTO>"
            wvarResult = wvarResult & "<DESCRIPCION><![CDATA[" & Trim(Mid(strParseString, wvarPos, 30)) & "]]></DESCRIPCION>"
            wvarResult = wvarResult & "<DATOCONT><![CDATA[" & Trim(Mid(strParseString, wvarPos + 30, 50)) & "]]></DATOCONT>"
            wvarResult = wvarResult & "</CONTACTO>"
            '
            wvarPos = wvarPos + 80
        Wend
        wvarStep = 190
        wvarResult = wvarResult & "</CONTACTOS>"
        wvarResult = wvarResult & "</DATOS>"
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado= 'false' mensaje='NO SE ENCONTRARON DATOS DEL CLIENTE' /></Response>"
    End If
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:

    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCliensec & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
















