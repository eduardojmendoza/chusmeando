VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetZona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetZona"
Const mcteOpID              As String = "0001"

'Parametros XML de Entrada
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_ProviCod    As String = "//PROVICOD"
Const mcteParam_CPACodPo    As String = "//CPACODPO"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarMensaje         As String
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarRamoPCod        As String
    Dim wvarProviCod        As String
    Dim wvarCPACodPo        As String
    Dim wvarPos             As Long
    Dim wvarPolizSec        As String
    Dim wvarPolizAnn        As String
    Dim strParseString      As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarRamoPCod = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarProviCod = Right("0" & .selectSingleNode(mcteParam_ProviCod).Text, 2)
        wvarCPACodPo = Left(.selectSingleNode(mcteParam_CPACodPo).Text & String(4, " "), 8)
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    ' LEVANTO LOS PARAMETROS PARA HOGAR (ParametrosCotizador.xml)
    wvarStep = 20
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarPolizSec = .selectSingleNode(IIf(wvarRamoPCod = "HOM1", gcteNodosHogar, gcteNodosAutoScoring) & gctePOLIZSEC).Text
        wvarPolizAnn = .selectSingleNode(IIf(wvarRamoPCod = "HOM1", gcteNodosHogar, gcteNodosAutoScoring) & gctePOLIZANN).Text
    End With
    '
    wvarStep = 30
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 40
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarMensaje = mcteOpID
    wvarMensaje = wvarMensaje & "0001"
    wvarMensaje = wvarMensaje & wvarRamoPCod
    wvarMensaje = wvarMensaje & wvarPolizAnn
    wvarMensaje = wvarMensaje & wvarPolizSec
    wvarMensaje = wvarMensaje & "000"
    wvarMensaje = wvarMensaje & "00"
    wvarMensaje = wvarMensaje & wvarProviCod
    wvarMensaje = wvarMensaje & "01"
    wvarMensaje = wvarMensaje & wvarCPACodPo
    
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
      wvarStep = 160
      wvarResult = ""
      wvarPos = 34
    
    
      If Mid(strParseString, wvarPos, 4) = "0000" Or Trim(Mid(strParseString, wvarPos, 4)) = "" Then
          ' NO SE ENCONTRARON DATOS
          Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><CODIZONA>0000</CODIZONA><ZONASDES></ZONASDES></Response>"
      Else
          ' ARMA LA RESPUESTA
          wvarResult = "<CODIZONA>" & Mid(strParseString, wvarPos, 4) & "</CODIZONA>"
          wvarResult = wvarResult & "<ZONASDES>" & Trim(Mid(strParseString, wvarPos + 4, 30)) & "</ZONASDES>"
          Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
      End If
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub








