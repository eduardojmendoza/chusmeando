VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetVehiculos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetVehiculos"
'Const mcteOpID              As String = "0093"
Const mcteOpID              As String = "0051"

'Parametros XML de Entrada
Const mcteParam_AuMarCod    As String = "//AUMARCOD"
Const mcteParam_AuVehDes    As String = "//AUVEHDES"
Const mcteParam_AuFecVig    As String = "//AUFECVIG"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarAuMarCod        As String
    Dim wvarAuVehDes        As String
    Dim wvarAuFecVig        As String
    Dim wvarNow             As String
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarAuMarCod = .selectSingleNode(mcteParam_AuMarCod).Text
        If (.selectSingleNode(mcteParam_AuFecVig) Is Not Nothing) Then
            wvarAuFecVig = .selectSingleNode(mcteParam_AuFecVig).Text
        Else
            wvarAuFecVig = Year(Now) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2)
        End If
        'wvarAuVehDes = Left(.selectSingleNode(mcteParam_AuVehDes).Text & String(35, " "), 35) 'MQ 0093
        wvarAuVehDes = Left(.selectSingleNode(mcteParam_AuVehDes).Text & String(65, " "), 65) 'MQ 0051
    End With
    '
    ' Quitamos esto y ponemos una nueva variable wvarAuVehDes en donde la fecha de vigencia se puede
    ' especificar
    'wvarNow = Year(Now) & Right("00" & Month(Now), 2) & Right("00" & Day(Now), 2)
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'wvarArea = mcteOpID & wvarAuMarCod & wvarAuVehDes & wvarNow
    wvarArea = mcteOpID & wvarAuMarCod & wvarAuVehDes & wvarAuFecVig
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
  
    wvarResult = ""
    'wvarPos = 49 'MQ 0093
    wvarPos = 79 'MQ0051
  
    wvarstrLen = Len(strParseString)

    If Mid(strParseString, wvarPos, 5) = "00000" Then
        ' NO SE ENCONTRARON DATOS
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><option value='0'>NO SE ENCONTRARON DATOS</option></Response>"
    Else
        ' ARMA LA RESPUESTA
        While wvarPos < wvarstrLen And Mid(strParseString, wvarPos, 5) <> "00000"
            wvarResult = wvarResult & "<OPTION value='" & Mid(strParseString, wvarPos, 21) & "'>"
            'wvarResult = wvarResult & "<![CDATA[" & Trim(Mid(strParseString, wvarPos + 21, 50)) & "]]></OPTION>" 'MQ 0093
            wvarResult = wvarResult & "<![CDATA[" & Trim(Mid(strParseString, wvarPos + 21, 80)) & "]]></OPTION>" 'MQ 0051
            'wvarPos = wvarPos + 71 'MQ 0093
            wvarPos = wvarPos + 101 'MQ 0051
        Wend
        
        wvarStep = 120
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    End If
'
    wvarStep = 130
    mobjCOM_Context.SetComplete
    IAction_Execute = 0

ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarAuMarCod & wvarAuVehDes & wvarNow & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub






