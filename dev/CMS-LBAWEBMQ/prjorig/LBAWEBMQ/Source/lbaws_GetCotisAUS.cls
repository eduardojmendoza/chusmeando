VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaws_GetCotisAUS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Esto archivo es unico solo para las pruebas UAT de Brokers
Const gcteConfFileName2       As String = "MQConfig.xml"

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OVLBAMQ.lbaws_GetCotisAUS"
Const mcteOpID                  As String = "0043"
Const mcteOpIDSolis             As String = "0000"

'Parametros XML de Entrada
Const mcteParam_WDB_TIPO_OPERACION As String = "//TIPOOPERACION"

Const mcteParam_Cliensec        As String = "//CLIENSEC"
Const mcteParam_NacimAnn        As String = "//NACIMANN"
Const mcteParam_NacimMes        As String = "//NACIMMES"
Const mcteParam_NacimDia        As String = "//NACIMDIA"
Const mcteParam_Sexo            As String = "//SEXO"
Const mcteParam_Estado          As String = "//ESTADO"
Const mcteParam_ClienIVA          As String = "//CLIENIVA"
Const mcteParam_ModeAutCod      As String = "//MODEAUTCOD"
Const mcteParam_KMsrngCod       As String = "//KMSRNGCOD"
Const mcteParam_EfectAnn        As String = "//EFECTANN"
Const mcteParam_SiGarage        As String = "//SIGARAGE"
Const mcteParam_SumAseg         As String = "//SUMAASEG"
Const mcteParam_Siniestros      As String = "//SINIESTROS"
Const mcteParam_Gas             As String = "//GAS"
Const mcteParam_Provi           As String = "//PROVI"
Const mcteParam_LocalidadCod    As String = "//LOCALIDADCOD"
Const mcteParam_CampaCod        As String = "//CAMPACOD"  'Revisar
Const mcteParam_DatosPlan       As String = "//DATOSPLAN" 'Revisar
Const mcteParam_ClubLBA         As String = "//CLUBLBA"
Const mcteParam_Portal          As String = "//PORTAL"    'Revisar
Const mcteParam_PRODUCTOR       As String = "//PRODUCTOR"
Const mcteParam_PLANCOD       As String = "//PLANNCOD"
Const mcteParam_Franqcod       As String = "//FRANQCOD"
'Const mcteParam_CampaTel        As String = "//TELEFONO"
Const mcteParam_CobroCod        As String = "//COBROCOD"
Const mcteParam_CobroTip        As String = "//COBROTIP"
Const mcteParam_NroCot          As String = "//WDB_NROCOT"
Const mcteParam_SumaMin          As String = "//SUMA_MINIMA"
Const mcteParam_SumaMax_GBA          As String = "//SUMA_MAXIMA_GBA"
Const mcteParam_SumaMax_INT          As String = "//SUMA_MAXIMA_INT"
Const mcteParam_Control_GNC_GBA          As String = "//CONTROL_GNC_GBA"
Const mcteParam_Control_GNC_INT          As String = "//CONTROL_GNC_INT"
Const mcteParam_Permite_GNC          As String = "//PERMITE_GNC"

Const mcteParam_Luneta               As String = "//LUNETA"
Const mcteParam_VIP                  As String = "//VIP"



' DATOS DE LOS HIJOS
Const mcteNodos_Hijos           As String = "//Request/HIJOS/HIJO"
Const mcteParam_NacimHijo       As String = "NACIMHIJO"
Const mcteParam_SexoHijo        As String = "SEXOHIJO"
Const mcteParam_EstadoHijo      As String = "ESTADOHIJO"
' DATOS DE LOS ACCESORIOS
Const mcteNodos_Accesorios      As String = "//Request/ACCESORIOS/ACCESORIO"
Const mcteParam_PrecioAcc       As String = "PRECIOACC"
Const mcteParam_DescripcionAcc  As String = "DESCRIPCIONACC"
Const mcteParam_CodigoAcc       As String = "CODIGOACC"


'Datos del mensaje
Const mcteParam_REQUESTID       As String = "//REQUESTID"
Const mcteParam_VEHDES          As String = "//VEHDES"

Public Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParams       As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarCotiID          As String
    Dim wvarFechaDia        As String
    Dim wvarFechaSig        As String
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarCliensec        As String
    Dim wvarMensaje         As String
    Dim wvarNacimAnn        As String
    Dim wvarNacimMes        As String
    Dim wvarNacimDia        As String
    Dim wvarSexo            As String
    Dim wvarEstado          As String
    Dim wvarClienIVA        As String
    Dim wvarProductor       As String
    Dim wvarPLANCOD         As String
    Dim wvarFranqcod        As String
    Dim wvarModeAutCod      As String
    Dim wvarKMsrngCod       As String
    Dim wvarEfectAnn        As String
    Dim wvarSiGarage        As String
    Dim wvarSiniestros      As String
    Dim wvarGas             As String
    Dim wvarProvi           As String
    Dim wvarLocalidadCod    As String
    Dim wvarHijos           As String
    Dim wvarAccesorios      As String
    Dim wvarSumaMin         As String
    Dim wvarDatosPlan       As String
    Dim wvarClubLBA         As String
    Dim wvarSumAseg         As String
    Dim wvarSumaLBA         As String
    Dim wvarPortal          As String
    Dim wvarCampaCod        As String
    Dim wvarAgeCod          As String
    Dim wvarCampaTel        As String
    Dim wvarCampaTelForm    As String
    Dim wvarCobroCod        As String
    Dim wvarCobroTip        As String
    Dim wvarSumaMaximaGBA   As String
    Dim wvarSumaMaxmiaINT   As String
    Dim wvarControlGNCGBA   As String
    Dim wvarControlGNCINT   As String
    Dim wvarPermiteGNC      As String
    Dim wvarAuxCodigoAcc    As String
    
    Dim wvarPlanncod        As String
    'Dim wvarFranqcod        As String
    Dim wvarLuneta          As String
    Dim wvarVIP             As String
     
    '
    Dim strParseString      As String
    Dim wvariCounter        As Integer
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    
    ' CARGO EL XML DE ENTRADA
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)


    'CARGO LAS VARIABLES CON LOS DATOS DEL XML
    With wobjXMLRequest
      wvarNacimAnn = Right("0000" & .selectSingleNode(mcteParam_NacimAnn).Text, 4)
      wvarNacimMes = Right("00" & .selectSingleNode(mcteParam_NacimMes).Text, 2)
      wvarNacimDia = Right("00" & .selectSingleNode(mcteParam_NacimDia).Text, 2)
      wvarSexo = UCase(Left(.selectSingleNode(mcteParam_Sexo).Text & " ", 1))
      wvarEstado = UCase(Left(.selectSingleNode(mcteParam_Estado).Text & " ", 1))
      wvarClienIVA = Left(.selectSingleNode(mcteParam_ClienIVA).Text & " ", 1)
      wvarCobroTip = UCase(Left(.selectSingleNode(mcteParam_CobroTip).Text & " ", 2))
      wvarCobroCod = Right("0" & .selectSingleNode(mcteParam_CobroCod).Text, 1)
      wvarFechaDia = CStr(Year(Now())) & Right("0" & CStr(Month(Now())), 2) & Right("0" & CStr(Day(Now())), 2)
      wvarFechaSig = CStr(Year(DateAdd("m", 1, Now()))) & Right("0" & CStr(Month(DateAdd("m", 1, Now()))), 2) & "01"
      wvarProductor = Left(.selectSingleNode(mcteParam_PRODUCTOR).Text & "    ", 4)
      If .selectSingleNode(mcteParam_WDB_TIPO_OPERACION) Is Nothing Then
        'wvarPLANCOD = "000"
        If Not wobjXMLRequest.selectSingleNode(mcteParam_PLANCOD) Is Nothing Then
            wvarPLANCOD = Right(String(3, "0") & wobjXMLRequest.selectSingleNode(mcteParam_PLANCOD).Text, 3)
        Else
            wvarPLANCOD = "000"
        End If
        If Not wobjXMLRequest.selectSingleNode(mcteParam_Franqcod) Is Nothing Then
            wvarFranqcod = Left(wobjXMLRequest.selectSingleNode(mcteParam_Franqcod).Text & "   ", 2)
        Else
            wvarFranqcod = "  "
        End If
        'wvarFranqcod = "  "
      Else
        wvarPLANCOD = Right("000" & .selectSingleNode(mcteParam_PLANCOD).Text, 3)
        wvarFranqcod = Left(.selectSingleNode(mcteParam_Franqcod).Text & "   ", 2)
      End If
      wvarSumAseg = Right(String(11, "0") & Replace((.selectSingleNode(mcteParam_SumAseg).Text * 100), ".", ""), 11)
      wvarModeAutCod = Right(String(21, "0") & .selectSingleNode(mcteParam_ModeAutCod).Text, 21)
      wvarKMsrngCod = Right(String(7, "0") & .selectSingleNode(mcteParam_KMsrngCod).Text, 7)
      wvarEfectAnn = Right("0000" & .selectSingleNode(mcteParam_EfectAnn).Text, 4)
      wvarSiGarage = UCase(Left(.selectSingleNode(mcteParam_SiGarage).Text & " ", 1))
      wvarSiniestros = Right("00" & .selectSingleNode(mcteParam_Siniestros).Text, 2)
      wvarGas = UCase(Left(.selectSingleNode(mcteParam_Gas).Text & " ", 1))
      wvarClubLBA = UCase(Left(.selectSingleNode(mcteParam_ClubLBA).Text & " ", 1))
      wvarProvi = Right("00" & .selectSingleNode(mcteParam_Provi).Text, 2)
      wvarLocalidadCod = Right("0000" & .selectSingleNode(mcteParam_LocalidadCod).Text, 4)
      
      If Not .selectSingleNode(mcteParam_Luneta) Is Nothing Then
        wvarLuneta = Right("N" & .selectSingleNode(mcteParam_Luneta).Text, 1)
      Else
        wvarLuneta = "N"
      End If
      If Not .selectSingleNode(mcteParam_VIP) Is Nothing Then
        wvarVIP = Right("N" & .selectSingleNode(mcteParam_VIP).Text, 1)
      Else
        wvarVIP = "N"
      End If
      
        ' INICIO HIJOS
        wvarHijos = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Hijos)
  
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_NacimHijo).Text
            wvarHijos = wvarHijos & UCase(wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_SexoHijo).Text)
            wvarHijos = wvarHijos & UCase(wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_EstadoHijo).Text)
        Next
        
        For wvariCounter = wobjXMLList.length To 9
            wvarHijos = wvarHijos & String(8, "0") & "  "
        Next
        ' FIN HIJOS
        ' INICIO ACCESORIOS
        wvarAccesorios = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Accesorios)
  
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarAccesorios = wvarAccesorios & Right(String(14, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_PrecioAcc).Text, 14)
            wvarAccesorios = wvarAccesorios & Left(wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_DescripcionAcc).Text & String(30, " "), 30)
            wvarAccesorios = wvarAccesorios & "S"
            wvarAuxCodigoAcc = wvarAuxCodigoAcc & Right(String(4, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CodigoAcc).Text, 4)
        Next
        
        For wvariCounter = wobjXMLList.length To 9
            wvarAccesorios = wvarAccesorios & String(14, "0") & String(31, " ")
        Next
        ' FIN ACCESORIOS
        wvarCotiID = Right(String(9, "0") & .selectSingleNode(mcteParam_NroCot).Text, 9)
        wvarSumaMin = Right(String(11, "0") & Replace(.selectSingleNode(mcteParam_SumaMin).Text, ".", ""), 11)
        wvarCampaCod = Left(.selectSingleNode(mcteParam_CampaCod).Text & String(40, " "), 40)
  End With
      
        If wobjXMLRequest.selectSingleNode(mcteParam_WDB_TIPO_OPERACION) Is Nothing Then
          
            wvarSumaMaximaGBA = Right(String(11, "0") & Replace(wobjXMLRequest.selectSingleNode(mcteParam_SumaMax_GBA).Text, ".", ""), 11)
            wvarSumaMaxmiaINT = Right(String(11, "0") & Replace(wobjXMLRequest.selectSingleNode(mcteParam_SumaMax_INT).Text, ".", ""), 11)
            wvarControlGNCGBA = Right(String(1, "0") & Replace(wobjXMLRequest.selectSingleNode(mcteParam_Control_GNC_GBA).Text, ".", ""), 1)
            wvarControlGNCINT = Right(String(1, "0") & Replace(wobjXMLRequest.selectSingleNode(mcteParam_Control_GNC_INT).Text, ".", ""), 1)
            wvarPermiteGNC = Left(wobjXMLRequest.selectSingleNode(mcteParam_Permite_GNC).Text & " ", 1)
            'wvarPLANCOD = Right(String(3, "0") & Replace(wobjXMLRequest.selectSingleNode(mcteParam_PLANCOD).Text, ".", ""), 3)
          
            wvarMensaje = mcteOpID
            wvarMensaje = wvarMensaje & String(9, "0") & wvarNacimAnn & wvarNacimMes & wvarNacimDia
            wvarMensaje = wvarMensaje & wvarSexo & wvarEstado
            wvarMensaje = wvarMensaje & wvarClienIVA & wvarCobroTip & wvarCobroCod & wvarFechaDia & wvarFechaSig & wvarProductor & wvarPLANCOD & wvarFranqcod & wvarSumAseg & String(11, "0")
            wvarMensaje = wvarMensaje & wvarModeAutCod & "001" & wvarKMsrngCod & wvarEfectAnn & wvarSiGarage
            wvarMensaje = wvarMensaje & wvarSiniestros & wvarGas & wvarClubLBA & "00" & wvarProvi & wvarLocalidadCod
            wvarMensaje = wvarMensaje & wvarHijos
            wvarMensaje = wvarMensaje & wvarAccesorios
            wvarMensaje = wvarMensaje & wvarCotiID & wvarSumaMin & wvarCampaCod & wvarSumaMaximaGBA & wvarSumaMaxmiaINT & wvarControlGNCGBA & wvarControlGNCINT & wvarPermiteGNC & "N"
            wvarMensaje = wvarMensaje & wvarVIP & wvarLuneta
            wvarMensaje = wvarMensaje & Left(wvarAuxCodigoAcc & String(40, "0"), 40)
        
        Else
            wvarMensaje = mcteOpIDSolis
            wvarMensaje = wvarMensaje & String(9, "0") & wvarNacimAnn & wvarNacimMes & wvarNacimDia
            wvarMensaje = wvarMensaje & wvarSexo & wvarEstado
            wvarMensaje = wvarMensaje & wvarClienIVA & wvarCobroTip & wvarCobroCod & wvarFechaDia & wvarFechaSig & wvarProductor & wvarPLANCOD & wvarFranqcod & wvarSumAseg & String(11, "0")
            wvarMensaje = wvarMensaje & wvarModeAutCod & "001" & wvarKMsrngCod & wvarEfectAnn & wvarSiGarage
            wvarMensaje = wvarMensaje & wvarSiniestros & wvarGas & wvarClubLBA & "00" & wvarProvi & wvarLocalidadCod
            wvarMensaje = wvarMensaje & wvarHijos
            wvarMensaje = wvarMensaje & wvarAccesorios
            wvarMensaje = wvarMensaje & wvarCotiID & wvarSumaMin & wvarCampaCod
      End If
      
    wvarStep = 150
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarResult = ""
    '
    wvarStep = 250

      If wobjXMLRequest.selectSingleNode(mcteParam_WDB_TIPO_OPERACION) Is Nothing Then
        If UCase(Left(strParseString, 2)) = "OK" Then
          Response = Response & "<LBA_WS res_code=""OK"" res_msg=""""><Response>"
          Response = Response & "<REQUESTID>" & wobjXMLRequest.selectSingleNode(mcteParam_REQUESTID).Text & "</REQUESTID>"
          Response = Response & "<COT_NRO>" & wobjXMLRequest.selectSingleNode(mcteParam_NroCot).Text & "</COT_NRO>"
          Response = Response & "<VEHDES>" & wobjXMLRequest.selectSingleNode(mcteParam_VEHDES).Text & "</VEHDES>"
          Response = Response & "<PRECIOS>"
          For wvariCounter = 1 To 7
            If Mid(strParseString, 3 + (157 * (wvariCounter - 1)) - (wvariCounter - 1), 3) <> "000" Then
              Response = Response & ParseoResultadoPrecios(Mid(strParseString, 3 + (157 * (wvariCounter - 1)) - (wvariCounter - 1), 156))
            End If
          Next
          Response = Response & "</PRECIOS>"
          'Response = Response & "<TIEMPO>" & Mid(strParseString, 1004, 6) & "</TIEMPO>"
          Response = Response & "<TIEMPO>" & Mid(strParseString, 1095, 6) & "</TIEMPO>"
          Response = Response & "<CODZONA>" & Mid(strParseString, 1101, 4) & "</CODZONA>"
          Response = Response & "<SUMALBA>" & Format(CStr(CLng(Mid(strParseString, 1105, 11))), "0.00") & "</SUMALBA>"
          Response = Response & "</Response></LBA_WS>"
        Else
          Response = "<LBA_WS res_code=""" & UCase(Left(strParseString, 2)) & """ res_msg="""">"
          'Response = Response & "<TIEMPO>" & Mid(strParseString, 1004, 6) & "</TIEMPO>"
          Response = Response & "<TIEMPO>" & Mid(strParseString, 1095, 6) & "</TIEMPO>"
          Response = Response & "<CODZONA>" & Mid(strParseString, 1101, 4) & "</CODZONA>"
          Response = Response & "<SUMALBA>" & Format(CStr(CLng(Mid(strParseString, 1105, 11))), "0.00") & "</SUMALBA>"
          Response = Response & "</LBA_WS>"
        End If
          
      Else
        Response = Response & "<LBA_WS res_code=""OK"" res_msg=""""><Response>"
        Response = Response & "<COT_NRO>" & wvarCotiID & "</COT_NRO>"
       Response = Response & "<PRECIOPLAN_CH>" & Format(CStr(CDbl(Mid(strParseString, 1, 18)) / 100), "0.00") & "</PRECIOPLAN_CH>"
        Response = Response & "<PRECIOPLAN_SH>" & Format(CStr(CDbl(Mid(strParseString, 127, 18)) / 100), "0.00") & "</PRECIOPLAN_SH>"
        '
        wvarStep = 260
        If wvarDatosPlan <> "00000" Then
            For wvariCounter = 1 To 20
                Response = Response & "<COBERCOD" & wvariCounter & ">" & Mid(strParseString, 283 + (276 * (wvariCounter - 1)), 3) & "</COBERCOD" & wvariCounter & ">"
                Response = Response & "<COBERORD" & wvariCounter & ">" & Mid(strParseString, 302 + (276 * (wvariCounter - 1)), 2) & "</COBERORD" & wvariCounter & ">"
                Response = Response & "<CAPITASG" & wvariCounter & ">" & Format(CStr(CDbl(Mid(strParseString, 304 + (276 * (wvariCounter - 1)), 15)) / 100), "0.00") & "</CAPITASG" & wvariCounter & ">"
                Response = Response & "<CAPITIMP" & wvariCounter & ">" & Format(CStr(CDbl(Mid(strParseString, 319 + (276 * (wvariCounter - 1)), 15)) / 100), "0.00") & "</CAPITIMP" & wvariCounter & ">"
           Next
        End If
    
      Response = Response & "</Response></LBA_WS>"
      End If
    '
    '
   wvarStep = 280
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoResultadoPrecios(ByVal pvarStrRequest As String) As String
Dim pvarStrResponse As String

pvarStrResponse = "<PRECIO>"
pvarStrResponse = pvarStrResponse & "<PLANNCOD>" & Mid(pvarStrRequest, 1, 3) & "</PLANNCOD>"
pvarStrResponse = pvarStrResponse & "<FRANQCOD>" & Mid(pvarStrRequest, 4, 2) & "</FRANQCOD>"
pvarStrResponse = pvarStrResponse & "<PLANNDES>" & Trim(Mid(pvarStrRequest, 6, 50)) & "</PLANNDES>"
pvarStrResponse = pvarStrResponse & "<PRIMA>" & Format(CStr(CLng(Mid(pvarStrRequest, 56, 11)) / 100), "0.00") & "</PRIMA>"
pvarStrResponse = pvarStrResponse & "<RECARGOS>" & Format(CStr(CLng(Mid(pvarStrRequest, 67, 11)) / 100), "0.00") & "</RECARGOS>"
pvarStrResponse = pvarStrResponse & "<IVA>" & Format(CStr(CLng(Mid(pvarStrRequest, 78, 11)) / 100), "0.00") & "</IVA>"
pvarStrResponse = pvarStrResponse & "<DEREMI>" & Format(CStr(CLng(Mid(pvarStrRequest, 89, 11)) / 100), "0.00") & "</DEREMI>"
pvarStrResponse = pvarStrResponse & "<SELLADOS>" & Format(CStr(CLng(Mid(pvarStrRequest, 100, 11)) / 100), "0.00") & "</SELLADOS>"
pvarStrResponse = pvarStrResponse & "<INGBRU>" & Format(CStr(CLng(Mid(pvarStrRequest, 111, 11)) / 100), "0.00") & "</INGBRU>"
pvarStrResponse = pvarStrResponse & "<IMPUESTOS>" & Format(CStr(CLng(Mid(pvarStrRequest, 122, 11)) / 100), "0.00") & "</IMPUESTOS>"
pvarStrResponse = pvarStrResponse & "<VALOR>" & Format(CStr(CLng(Mid(pvarStrRequest, 133, 11)) / 100), "0.00") & "</VALOR>"
pvarStrResponse = pvarStrResponse & "<CLUB_LBA>" & Mid(pvarStrRequest, 144, 1) & "</CLUB_LBA>"
pvarStrResponse = pvarStrResponse & "<COB_CLUB>" & Mid(pvarStrRequest, 145, 3) & "</COB_CLUB>"
pvarStrResponse = pvarStrResponse & "<LUNETA>" & Mid(pvarStrRequest, 148, 1) & "</LUNETA>"
pvarStrResponse = pvarStrResponse & "<COB_LUNE>" & Mid(pvarStrRequest, 149, 3) & "</COB_LUNE>"
pvarStrResponse = pvarStrResponse & "<VIP>" & Mid(pvarStrRequest, 152, 1) & "</VIP>"
pvarStrResponse = pvarStrResponse & "<COB_VIP>" & Mid(pvarStrRequest, 153, 3) & "</COB_VIP>"
pvarStrResponse = pvarStrResponse & "<RASTREO>" & Mid(pvarStrRequest, 156, 1) & "</RASTREO>"
pvarStrResponse = pvarStrResponse & "</PRECIO>"

ParseoResultadoPrecios = pvarStrResponse
End Function


