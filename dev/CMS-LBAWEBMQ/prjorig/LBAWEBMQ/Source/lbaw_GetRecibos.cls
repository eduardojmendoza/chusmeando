VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetRecibos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetRecibos"
Const mcteOpID              As String = "0030"

'Parametros XML de Entrada
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_POLIZANN    As String = "//POLIZANN"
Const mcteParam_POLIZSEC    As String = "//POLIZSEC"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
Const mcteParam_SUPLENUM    As String = "//SUPLENUM"
Const mcteParam_FECDESDE    As String = "//FECDESDE"
Const mcteParam_FECHASTA    As String = "//FECHASTA"
' Parametros de configuracion
Const mcteParam_CIAASCOD    As String = "//CIAASCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    Dim wvarCiaAsCod        As String
    Dim wvarRamoPCod        As String
    Dim wvarPolizAnn        As String
    Dim wvarPolizSec        As String
    Dim wvarCertipol        As String
    Dim wvarCertiann        As String
    Dim wvarCertisec        As String
    Dim wvarSuplenum        As String
    Dim wvarFecDesde        As String
    Dim wvarFecHasta        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarRamoPCod = Right(String(4, " ") & .selectSingleNode(mcteParam_RAMOPCOD).Text, 4)
        wvarPolizAnn = Right(String(2, "0") & .selectSingleNode(mcteParam_POLIZANN).Text, 2)
        wvarPolizSec = Right(String(6, "0") & .selectSingleNode(mcteParam_POLIZSEC).Text, 6)
        wvarCertipol = Right(String(4, "0") & .selectSingleNode(mcteParam_CERTIPOL).Text, 4)
        wvarCertiann = Right(String(4, "0") & .selectSingleNode(mcteParam_CERTIANN).Text, 4)
        wvarCertisec = Right(String(6, "0") & .selectSingleNode(mcteParam_CERTISEC).Text, 6)
        wvarSuplenum = Right(String(4, "0") & .selectSingleNode(mcteParam_SUPLENUM).Text, 4)
        wvarFecDesde = Right(String(8, "0") & .selectSingleNode(mcteParam_FECDESDE).Text, 8)
        wvarFecHasta = Right(String(8, "0") & .selectSingleNode(mcteParam_FECHASTA).Text, 8)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    If wvarFecDesde = "00000000" Then
        wvarFecDesde = Year(Date) - 1 & Format(Month(Date), "00") & Format(Day(Date), "00")
    End If
    
    If wvarFecHasta = "00000000" Then
        wvarFecHasta = Year(Date) & Format(Month(Date), "00") & Format(Day(Date), "00")
    End If

    
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 60
    wvarArea = mcteOpID & wvarCiaAsCod & wvarRamoPCod & wvarPolizAnn & wvarPolizSec & wvarCertipol & wvarCertiann & wvarCertisec & wvarSuplenum & wvarFecDesde & wvarFecHasta
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 150
    wvarResult = ""
    wvarPos = 51
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 160
    If Mid(strParseString, wvarPos, 2) <> "ER" Then
        wvarResult = wvarResult & "<RECIBOS>"
        wvarStep = 170
        wvarPos = wvarPos + 2
        While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos + 3, 6)) <> "000000" And Trim(Mid(strParseString, wvarPos + 3, 6)) <> ""
            wvarStep = 180
            wvarResult = wvarResult & "<RECIBO>"
            wvarResult = wvarResult & "<RECIBANN>" & Trim(Mid(strParseString, wvarPos, 2)) & "</RECIBANN>"
            wvarResult = wvarResult & "<RECIBTIP>" & Trim(Mid(strParseString, wvarPos + 2, 1)) & "</RECIBTIP>"
            wvarResult = wvarResult & "<RECIBSEC>" & Trim(Mid(strParseString, wvarPos + 3, 6)) & "</RECIBSEC>"
            wvarResult = wvarResult & "<PRIMAIMP>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 9, 15)) / 100), ".", ",") & "</PRIMAIMP>"
            wvarResult = wvarResult & "<RECTOIMP>" & Replace(CStr(CDbl(Mid(strParseString, wvarPos + 24, 15)) / 100), ".", ",") & "</RECTOIMP>"
            wvarResult = wvarResult & "<SIGNO>" & Trim(Mid(strParseString, wvarPos + 39, 1)) & "></SIGNO>"
            wvarResult = wvarResult & "<EFECFEC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 40, 10)) & "]]></EFECFEC>"
            wvarResult = wvarResult & "<VENCFEC><![CDATA[" & Trim(Mid(strParseString, wvarPos + 50, 10)) & "]]></VENCFEC>"
            wvarResult = wvarResult & "</RECIBO>"
            
            wvarPos = wvarPos + 60
        Wend
        wvarStep = 190
        wvarResult = wvarResult & "</RECIBOS>"
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
            Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje='NO SE ENCONTRARON DATOS DE RECIBOS'/></Response>"
    End If
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:

    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarRamoPCod & wvarPolizAnn & wvarPolizSec & wvarCertipol & wvarCertiann & wvarCertisec & wvarSuplenum & wvarFecDesde & wvarFecHasta & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub




















