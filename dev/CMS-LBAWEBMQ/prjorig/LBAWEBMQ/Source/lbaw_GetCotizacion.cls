VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetCotizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_OVLBAMQ.lbaw_GetCotizacion"
Const mcteOpID                  As String = "0000"

'Parametros XML de Entrada
Const mcteParam_Cliensec        As String = "//CLIENSEC"
Const mcteParam_NacimAnn        As String = "//NACIMANN"
Const mcteParam_NacimMes        As String = "//NACIMMES"
Const mcteParam_NacimDia        As String = "//NACIMDIA"
Const mcteParam_Sexo            As String = "//SEXO"
Const mcteParam_Estado          As String = "//ESTADO"
Const mcteParam_ModeAutCod      As String = "//MODEAUTCOD"
Const mcteParam_KMsrngCod       As String = "//KMSRNGCOD"
Const mcteParam_EfectAnn        As String = "//EFECTANN"
Const mcteParam_SiGarage        As String = "//SIGARAGE"
Const mcteParam_Siniestros      As String = "//SINIESTROS"
Const mcteParam_Gas             As String = "//GAS"
Const mcteParam_Provi           As String = "//PROVI"
Const mcteParam_LocalidadCod    As String = "//LOCALIDADCOD"
Const mcteParam_CampaCod        As String = "//CAMPACOD"
Const mcteParam_DatosPlan       As String = "//DATOSPLAN"
Const mcteParam_ClubLBA         As String = "//CLUBLBA"
Const mcteParam_Portal          As String = "//PORTAL"
Const mcteParam_AgeCod          As String = "//AGECOD"
Const mcteParam_CampaTel        As String = "//TELEFONO"
Const mcteParam_CobroCod        As String = "//COBROCOD"
Const mcteParam_CobroTip        As String = "//COBROTIP"
Const mcteParam_EsCero        As String = "//ESCERO"
' DATOS DE LOS HIJOS
Const mcteNodos_Hijos           As String = "//Request/HIJOS/HIJO"
Const mcteParam_NacimHijo       As String = "NACIMHIJO"
Const mcteParam_SexoHijo        As String = "SEXOHIJO"
Const mcteParam_EstadoHijo      As String = "ESTADOHIJO"
' DATOS DE LOS ACCESORIOS
Const mcteNodos_Accesorios      As String = "//Request/ACCESORIOS/ACCESORIO"
Const mcteParam_PrecioAcc       As String = "PRECIOACC"
Const mcteParam_DescripcionAcc  As String = "DESCRIPCIONACC"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParams       As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    Dim wvarCotiID          As String
    Dim wvarFechaDia        As String
    Dim wvarFechaSig        As String
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarCliensec        As String
    Dim wvarMensaje         As String
    Dim wvarNacimAnn        As String
    Dim wvarNacimMes        As String
    Dim wvarNacimDia        As String
    Dim wvarSexo            As String
    Dim wvarEstado          As String
    Dim wvarModeAutCod      As String
    Dim wvarKMsrngCod       As String
    Dim wvarEfectAnn        As String
    Dim wvarSiGarage        As String
    Dim wvarSiniestros      As String
    Dim wvarGas             As String
    Dim wvarProvi           As String
    Dim wvarLocalidadCod    As String
    Dim wvarHijos           As String
    Dim wvarAccesorios      As String
    Dim wvarSumaMin         As String
    Dim wvarDatosPlan       As String
    Dim wvarClubLBA         As String
    Dim wvarSumAseg         As String
    Dim wvarSumaLBA         As String
    Dim wvarPortal          As String
    Dim wvarCampaCod        As String
    Dim wvarAgeCod          As String
    Dim wvarCampaTel        As String
    Dim wvarCampaTelForm    As String
    Dim wvarCobroCod        As String
    Dim wvarCobroTip        As String
    Dim wvarEsCero          As String
    '
    Dim strParseString      As String
    Dim wvariCounter        As Integer
     
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    ' CARGO EL XML DE ENTRADA
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)

    ' OBTENGO EL NUMERO DE COTIZACION
    wvarStep = 10
    wvarRequest = "<Request></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetNroCot")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 20
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParams
        .async = False
        Call .loadXML(wvarResponse)
        wvarCotiID = .selectSingleNode("//NROCOT").Text
        wvarFechaDia = .selectSingleNode("//FECHA_DIA").Text
        wvarFechaSig = .selectSingleNode("//FECHA_SIGUIENTE").Text
    End With
    '
    Set wobjXMLParams = Nothing
    '
    ' OBTENGO LA MINIMA SUMA
    wvarStep = 30
    wvarRequest = "<Request><CONCEPTO>SA-SCO-MIN</CONCEPTO></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetParamGral")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 40
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParams
        .async = False
        Call .loadXML(wvarResponse)
        'wvarSumaMin = Right(String(11, "0") & CStr(CLng(CCur(.selectSingleNode("//PARAMNUM").Text) * 100)), 11)
        wvarSumaMin = Right(String(11, "0") & CStr(CCur(.selectSingleNode("//PARAMNUM").Text) * 100), 11)
    End With
    '
    Set wobjXMLParams = Nothing
    '
    ' BUSCO LA SUMA ASEGURADA PARA AUPROCOD = 02
    wvarStep = 50
    wvarRequest = "<Request><AUMODCOD>" & wobjXMLRequest.selectSingleNode(mcteParam_ModeAutCod).Text & "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaw_GetSumAseg")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    '
    ' LEVANTO EL VALOR DE SUMASEG
    wvarStep = 60
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    wobjXMLParams.async = False
    wobjXMLParams.loadXML wvarResponse
    wvarSumAseg = Right(String(11, "0") & Replace(wobjXMLParams.selectSingleNode("//COMBO/OPTION[@value=" & wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn).Text & "]/@sumaseg").Text, ",", ""), 11)
    Set wobjXMLParams = Nothing
    '
    ' BUSCO LA SUMA ASEGURADA PARA AUPROCOD = 03
    wvarStep = 70
    wvarRequest = "<Request><AUMODCOD>" & wobjXMLRequest.selectSingleNode(mcteParam_ModeAutCod).Text & "</AUMODCOD><AUPROCOD>03</AUPROCOD></Request>"
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    '
    ' LEVANTO EL VALOR DE SUMALBA
    wvarStep = 80
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    wobjXMLParams.async = False
    wobjXMLParams.loadXML wvarResponse
    wvarSumaLBA = Right(String(11, "0") & Replace(wobjXMLParams.selectSingleNode("//COMBO/OPTION[@value=" & wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn).Text & "]/@sumaseg").Text, ",", ""), 11)
    Set wobjXMLParams = Nothing
    Set wobjClass = Nothing
    '
    wvarStep = 90
    With wobjXMLRequest
        If .selectNodes(mcteParam_Cliensec).length = 0 Then
            wvarCliensec = String(9, "0")
        Else
            wvarCliensec = Right(String(9, "0") & .selectSingleNode(mcteParam_Cliensec).Text, 9)
        End If
        
        wvarNacimAnn = .selectSingleNode(mcteParam_NacimAnn).Text
        wvarNacimMes = .selectSingleNode(mcteParam_NacimMes).Text
        wvarNacimDia = .selectSingleNode(mcteParam_NacimDia).Text
        wvarSexo = .selectSingleNode(mcteParam_Sexo).Text
        wvarEstado = .selectSingleNode(mcteParam_Estado).Text
        wvarModeAutCod = .selectSingleNode(mcteParam_ModeAutCod).Text
        wvarKMsrngCod = Right(String(7, "0") & .selectSingleNode(mcteParam_KMsrngCod).Text, 7)
        wvarEfectAnn = .selectSingleNode(mcteParam_EfectAnn).Text
        wvarSiGarage = .selectSingleNode(mcteParam_SiGarage).Text
        wvarSiniestros = Right("00" & .selectSingleNode(mcteParam_Siniestros).Text, 2)
        wvarGas = .selectSingleNode(mcteParam_Gas).Text
        wvarProvi = Right("00" & .selectSingleNode(mcteParam_Provi).Text, 2)
        wvarLocalidadCod = .selectSingleNode(mcteParam_LocalidadCod).Text
        wvarEsCero = Left(.selectSingleNode(mcteParam_EsCero).Text, 1)
        
        wvarStep = 100
        If .selectNodes(mcteParam_Portal).length = 0 Then
            wvarPortal = "LBA"
        Else
            wvarPortal = .selectSingleNode(mcteParam_Portal).Text
        End If
        
        If .selectNodes(mcteParam_DatosPlan).length = 0 Then
            wvarDatosPlan = "00000"
        Else
            wvarDatosPlan = Right(String(5, "0") & .selectSingleNode(mcteParam_DatosPlan).Text, 5)
        End If
        
        wvarClubLBA = .selectSingleNode(mcteParam_ClubLBA).Text
        
        ' EN MERCADO ABIERTO SIEMPRE COTIZA CON COBROCOD = 4 Y COBROTIP = VI
        If .selectNodes(mcteParam_CobroCod).length = 0 Then
            wvarCobroCod = "4"
        Else
            wvarCobroCod = .selectSingleNode(mcteParam_CobroCod).Text
        End If
        
        If .selectNodes(mcteParam_CobroTip).length = 0 Then
            wvarCobroTip = "VI"
        Else
            wvarCobroTip = .selectSingleNode(mcteParam_CobroTip).Text
        End If
        
        ' INICIO HIJOS
        wvarStep = 110
        wvarHijos = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Hijos)

        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_NacimHijo).Text
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_SexoHijo).Text
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_EstadoHijo).Text
        Next
        
        For wvariCounter = wobjXMLList.length To 9
            wvarHijos = wvarHijos & String(8, "0") & "  "
        Next
        ' FIN HIJOS
        
        wvarStep = 120
        ' INICIO ACCESORIOS
        wvarAccesorios = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Accesorios)

        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarAccesorios = wvarAccesorios & Right(String(14, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_PrecioAcc).Text, 14)
            wvarAccesorios = wvarAccesorios & Left(wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_DescripcionAcc).Text & String(30, " "), 30)
            wvarAccesorios = wvarAccesorios & "S"
        Next
        
        For wvariCounter = wobjXMLList.length To 9
            wvarAccesorios = wvarAccesorios & String(14, "0") & String(31, " ")
        Next
        ' FIN ACCESORIOS
        '
        ' BUSCO LOS DATOS DE CAMPANIA, PRODUCTOR Y TELEFONO - SOLO PARA MERCADO ABIERTO
        If Trim(wvarPortal) = "LBA_PRODUCTORES" Then
            wvarCampaCod = Right(String(4, "0") & Trim(.selectSingleNode(mcteParam_CampaCod).Text), 4)
            wvarAgeCod = Right(String(4, "0") & Trim(.selectSingleNode(mcteParam_AgeCod).Text), 4)
            wvarCampaTel = ""
            wvarCampaTelForm = ""
        Else
            wvarStep = 130
            wvarRequest = "<Request><PORTAL>" & wvarPortal & "</PORTAL><RAMOPCOD>AUS1</RAMOPCOD></Request>"
            Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetPortalComerc")
            Call wobjClass.Execute(wvarRequest, wvarResponse, "")
            '
            wvarStep = 140
            Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
            wobjXMLParams.async = False
            wobjXMLParams.loadXML wvarResponse
            wvarCampaCod = Left(wobjXMLParams.selectSingleNode(mcteParam_CampaCod).Text & String(4, " "), 4)
            wvarAgeCod = Trim(wobjXMLParams.selectSingleNode(mcteParam_AgeCod).Text)
            wvarCampaTel = wobjXMLParams.selectSingleNode(mcteParam_CampaTel).Text
            wvarCampaTelForm = Formateo_Telefono(wobjXMLParams.selectSingleNode(mcteParam_CampaTel).Text)
            Set wobjXMLParams = Nothing
            Set wobjClass = Nothing
        End If
    End With

    Set wobjXMLRequest = Nothing
    Set wobjXMLList = Nothing
    '
    wvarStep = 150
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarMensaje = mcteOpID & wvarCliensec & wvarNacimAnn & wvarNacimMes & wvarNacimDia
    wvarMensaje = wvarMensaje & wvarSexo & wvarEstado
    wvarMensaje = wvarMensaje & "3" & wvarCobroTip & wvarCobroCod & wvarFechaDia & wvarFechaSig & wvarAgeCod & wvarDatosPlan & wvarSumAseg & wvarSumaLBA
    wvarMensaje = wvarMensaje & wvarModeAutCod & "001" & wvarKMsrngCod & wvarEfectAnn & wvarSiGarage
    wvarMensaje = wvarMensaje & wvarSiniestros & wvarGas & wvarClubLBA & "00" & wvarProvi & wvarLocalidadCod
    wvarMensaje = wvarMensaje & wvarHijos
    wvarMensaje = wvarMensaje & wvarAccesorios
    wvarMensaje = wvarMensaje & Right(String(9, "0") & wvarCotiID, 9) & wvarSumaMin & wvarCampaCod & String(36, " ")
    wvarMensaje = wvarMensaje & wvarEsCero
    
    'Dim mobjFileObject As Scripting.FileSystemObject
    'Dim mobjFile As Scripting.TextStream
    
    'Set mobjFileObject = CreateObject("Scripting.FileSystemObject")
    'Set mobjFile = mobjFileObject.CreateTextFile("D:\Comp\LBAVirtual\FileTemp\rand2" & CStr(Int(Rnd(5000) * 5000)) & ".txt", True)
    'mobjFile.WriteLine wvarMensaje
    'mobjFile.WriteLine ""
    'mobjFile.WriteLine ""
    'mobjFile.WriteLine wvarSumaMin
    'mobjFile.Close
    'Set mobjFile = Nothing
    'Set mobjFileObject = Nothing
            
    
    wvarStep = 220
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    wvarResult = ""
    '
    wvarStep = 250
    
    'Set mobjFileObject = CreateObject("Scripting.FileSystemObject")
    'Set mobjFile = mobjFileObject.CreateTextFile("D:\Comp\LBAVirtual\FileTemp\rand" & CStr(Int(Rnd(5000) * 5000)) & ".txt", True)
    'mobjFile.WriteLine strParseString
    'mobjFile.Close
    'Set mobjFile = Nothing
    'Set mobjFileObject = Nothing
    
    wvarResult = wvarResult & "<COT_NRO>" & wvarCotiID & "</COT_NRO>"
    wvarResult = wvarResult & "<CAMPA_TEL>" & wvarCampaTel & "</CAMPA_TEL>"
    wvarResult = wvarResult & "<CAMPA_TEL_FORM>" & wvarCampaTelForm & "</CAMPA_TEL_FORM>"
    wvarResult = wvarResult & "<CAMPA_COD>" & wvarCampaCod & "</CAMPA_COD>"
    wvarResult = wvarResult & "<AGE_COD>" & wvarAgeCod & "</AGE_COD>"
    wvarResult = wvarResult & "<RC_CH>" & Format(CStr(CDbl(Mid(strParseString, 1, 18)) / 100), "0.00") & "</RC_CH>"
    wvarResult = wvarResult & "<SDP_CH>" & Format(CStr(CDbl(Mid(strParseString, 19, 18)) / 100), "0.00") & "</SDP_CH>"
    wvarResult = wvarResult & "<TT_CH>" & Format(CStr(CDbl(Mid(strParseString, 37, 18)) / 100), "0.00") & "</TT_CH>"
    wvarResult = wvarResult & "<TRCF1_CH>" & Format(CStr(CDbl(Mid(strParseString, 55, 18)) / 100), "0.00") & "</TRCF1_CH>"
    wvarResult = wvarResult & "<TRCF2_CH>" & Format(CStr(CDbl(Mid(strParseString, 73, 18)) / 100), "0.00") & "</TRCF2_CH>"
    wvarResult = wvarResult & "<TRCF3_CH>" & Format(CStr(CDbl(Mid(strParseString, 91, 18)) / 100), "0.00") & "</TRCF3_CH>"
    wvarResult = wvarResult & "<RCROBO_CH>" & Format(CStr(CDbl(Mid(strParseString, 109, 18)) / 100), "0.00") & "</RCROBO_CH>"
    
    wvarResult = wvarResult & "<RC_SH>" & Format(CStr(CDbl(Mid(strParseString, 127, 18)) / 100), "0.00") & "</RC_SH>"
    wvarResult = wvarResult & "<SDP_SH>" & Format(CStr(CDbl(Mid(strParseString, 145, 18)) / 100), "0.00") & "</SDP_SH>"
    wvarResult = wvarResult & "<TT_SH>" & Format(CStr(CDbl(Mid(strParseString, 163, 18)) / 100), "0.00") & "</TT_SH>"
    wvarResult = wvarResult & "<TRCF1_SH>" & Format(CStr(CDbl(Mid(strParseString, 181, 18)) / 100), "0.00") & "</TRCF1_SH>"
    wvarResult = wvarResult & "<TRCF2_SH>" & Format(CStr(CDbl(Mid(strParseString, 199, 18)) / 100), "0.00") & "</TRCF2_SH>"
    wvarResult = wvarResult & "<TRCF3_SH>" & Format(CStr(CDbl(Mid(strParseString, 217, 18)) / 100), "0.00") & "</TRCF3_SH>"
    wvarResult = wvarResult & "<RCROBO_SH>" & Format(CStr(CDbl(Mid(strParseString, 235, 18)) / 100), "0.00") & "</RCROBO_SH>"
    '
    wvarResult = wvarResult & "<RC_COD>00199</RC_COD>"
    wvarResult = wvarResult & "<SDP_COD>00299</SDP_COD>"
    wvarResult = wvarResult & "<TT_COD>00399</TT_COD>"
    wvarResult = wvarResult & "<TRCF1_COD>00401</TRCF1_COD>"
    wvarResult = wvarResult & "<TRCF2_COD>00402</TRCF2_COD>"
    wvarResult = wvarResult & "<TRCF3_COD>00403</TRCF3_COD>"
    wvarResult = wvarResult & "<RCROBO_COD>00599</RCROBO_COD>"
    '
    wvarResult = wvarResult & "<SUMASEG>" & wvarSumAseg & "</SUMASEG>"
    wvarResult = wvarResult & "<SUMALBA>" & wvarSumaLBA & "</SUMALBA>"
    '
    wvarStep = 260
    If wvarDatosPlan <> "00000" Then
        For wvariCounter = 1 To 20
            wvarResult = wvarResult & "<COBERCOD" & wvariCounter & ">" & Mid(strParseString, 283 + (276 * (wvariCounter - 1)), 3) & "</COBERCOD" & wvariCounter & ">"
            wvarResult = wvarResult & "<COBERORD" & wvariCounter & ">" & Mid(strParseString, 302 + (276 * (wvariCounter - 1)), 2) & "</COBERORD" & wvariCounter & ">"
            wvarResult = wvarResult & "<CAPITASG" & wvariCounter & ">" & Mid(strParseString, 304 + (276 * (wvariCounter - 1)), 15) & "</CAPITASG" & wvariCounter & ">"
            wvarResult = wvarResult & "<CAPITIMP" & wvariCounter & ">" & Mid(strParseString, 319 + (276 * (wvariCounter - 1)), 15) & "</CAPITIMP" & wvariCounter & ">"
        Next
    End If
    
    wvarResult = Replace(wvarResult, ".", ",")
    '
    wvarStep = 270
    Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    '
    wvarStep = 280
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function Formateo_Telefono(pvarTelefono As String) As String
Dim wvarNewTelef As String

If Trim(Left(pvarTelefono, 4)) = "0800" Then
    wvarNewTelef = Trim(Left(pvarTelefono, 4)) & "-" & Mid(pvarTelefono, 5, 3) & "-" & Mid(pvarTelefono, 8, Len(pvarTelefono))
Else
    wvarNewTelef = pvarTelefono
End If

Formateo_Telefono = wvarNewTelef

End Function






