VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetExigible"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetExigible"
Const mcteOpID              As String = "0016"

'Parametros XML de Entrada
Const mcteParam_Cliensec    As String = "//CLIENSEC"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCliensec        As String
    Dim wvarCiaAsCod        As String
    '
    Dim wvarPos             As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    Dim wvarCodError        As String
    Dim wvarParamPoliza     As String
    Dim wvarParamAgentCod   As String
    Dim wvarParamAgentCla   As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarCliensec = Right(String(9, "0") & .selectSingleNode(mcteParam_Cliensec).Text, 9)
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    ' LEVANTO LOS PARAMETROS (ParametrosCotizador.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosComisiones & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarCliensec & Space(36)
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    On Error GoTo ErrorHandler
    '
    wvarStep = 160
    wvarResult = ""
    wvarPos = 50
    '
    wvarstrLen = Len(strParseString)
    '
    wvarStep = 170
    wvarCodError = Mid(strParseString, wvarPos, 2)
    If wvarCodError <> "ER" And wvarCodError <> "" Then
        'Cargo las variables para llamar a un segundo mensaje de ser necesario
        
        wvarParamPoliza = Mid(strParseString, wvarPos - 30, 30)
        wvarParamAgentCod = Mid(strParseString, wvarPos - 36, 4)
        wvarParamAgentCla = Mid(strParseString, wvarPos - 32, 2)
        
        wvarResult = wvarResult & "<PRODUCTOR>" & Trim(Mid(strParseString, wvarPos + 2, 30)) & "</PRODUCTOR>"
        wvarResult = wvarResult & "<DEUDAS_EXIGIBLES>"
        wvarPos = wvarPos + 32
        '
        wvarStep = 180
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
        '
        While wvarCodError = "TR"
            '
            wvarArea = mcteOpID & wvarCiaAsCod & wvarCliensec & wvarParamAgentCod & wvarParamAgentCla & wvarParamPoliza
            wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
            Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
            wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
            Set wobjFrame2MQ = Nothing
            '
            wvarStep = 150
            If wvarMQError <> 0 Then
                Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
                mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                        mcteClassName & "--" & mcteClassName, _
                        wcteFnName, _
                        wvarStep, _
                        Err.Number, _
                        "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                        vbLogEventTypeError
                GoTo ClearObjects:
            End If
            '
            '
            wvarPos = 50
            wvarstrLen = Len(strParseString)
            '
            wvarStep = 170
            wvarCodError = Mid(strParseString, wvarPos, 2)
            If wvarCodError <> "ER" Then
                '
                wvarParamPoliza = Mid(strParseString, wvarPos - 30, 30)
                wvarParamAgentCod = Mid(strParseString, wvarPos - 36, 4)
                wvarParamAgentCla = Mid(strParseString, wvarPos - 32, 2)
                
                wvarPos = wvarPos + 32
                '
                wvarStep = 180
                wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString, wvarstrLen)
            End If
        Wend
        '
        wvarStep = 190
        wvarResult = wvarResult & "</DEUDAS_EXIGIBLES>"
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=NO SE ENCONTRARON DATOS DE LA DEUDA EXIGIBLE  " & Chr(34) & Chr(34) & " /></Response>"
    End If
'

    wvarStep = 210
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarCliensec & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(wvarPos As Long, strParseString As String, wvarstrLen As Long) As String

Dim wvarResult As String

    While wvarPos < wvarstrLen And Trim(Mid(strParseString, wvarPos, 30)) <> ""
        wvarResult = wvarResult & "<DEUDA_EXIGIBLE>"
        wvarResult = wvarResult & "<CLIENDES><![CDATA[" & IIf(Trim(Mid(strParseString, wvarPos, 30)) = ",", "", Trim(Mid(strParseString, wvarPos, 30))) & "]]></CLIENDES>"
        wvarResult = wvarResult & "<POLIZA>" & Trim(Mid(strParseString, wvarPos + 30, 30)) & "</POLIZA>"
        wvarResult = wvarResult & "<IMPTOT>" & Mid(strParseString, wvarPos + 60, 14) & "</IMPTOT>"
        wvarResult = wvarResult & "<IMPTOT_SIGNO>" & IIf(Mid(strParseString, wvarPos + 74, 1) = "+", "", Mid(strParseString, wvarPos + 74, 1)) & "</IMPTOT_SIGNO>"
        wvarResult = wvarResult & "<TREINTA>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 75, 14))) = "0", "0", Mid(strParseString, wvarPos + 75, 14)) & "</TREINTA>"
        wvarResult = wvarResult & "<TREINTA_SIGNO>" & IIf(Mid(strParseString, wvarPos + 89, 1) = "+", "", Mid(strParseString, wvarPos + 89, 1)) & "</TREINTA_SIGNO>"
        wvarResult = wvarResult & "<SESENTA>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 90, 14))) = "0", "0", Mid(strParseString, wvarPos + 90, 14)) & "</SESENTA>"
        wvarResult = wvarResult & "<SESENTA_SIGNO>" & IIf(Mid(strParseString, wvarPos + 104, 1) = "+", "", Mid(strParseString, wvarPos + 104, 1)) & "</SESENTA_SIGNO>"
        wvarResult = wvarResult & "<NOVENTA>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 105, 14))) = "0", "0", Mid(strParseString, wvarPos + 105, 14)) & "</NOVENTA>"
        wvarResult = wvarResult & "<NOVENTA_SIGNO>" & IIf(Mid(strParseString, wvarPos + 119, 1) = "+", "", Mid(strParseString, wvarPos + 119, 1)) & "</NOVENTA_SIGNO>"
        wvarResult = wvarResult & "<MAS90>" & IIf(CStr(CDbl(Mid(strParseString, wvarPos + 120, 14))) = "0", "0", Mid(strParseString, wvarPos + 120, 14)) & "</MAS90>"
        wvarResult = wvarResult & "<MAS90_SIGNO>" & IIf(Mid(strParseString, wvarPos + 134, 1) = "+", "", Mid(strParseString, wvarPos + 134, 1)) & "</MAS90_SIGNO>"
        
        wvarResult = wvarResult & "<SITUCPOL>"
            If Mid(strParseString, wvarPos + 135, 1) = "S" Then
                wvarResult = wvarResult & "SUSPEN"
            ElseIf Mid(strParseString, wvarPos + 135, 1) = "V" Then
                wvarResult = wvarResult & "VIGENTE"
            Else
                wvarResult = wvarResult & Mid(strParseString, wvarPos + 135, 1)
            End If
        wvarResult = wvarResult & "</SITUCPOL>"
        wvarResult = wvarResult & "<AGENTE><![CDATA[" & Mid(strParseString, wvarPos + 140, 2) & "-" & Mid(strParseString, wvarPos + 136, 4) & "]]></AGENTE>"
        wvarResult = wvarResult & "</DEUDA_EXIGIBLE>"
        wvarPos = wvarPos + 142
    Wend
    ParseoMensaje = wvarResult
End Function









