VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetSumAseg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVLBAMQ.lbaw_GetSumAseg"
Const mcteOpID              As String = "0004"

'Parametros XML de Entrada
Const mcteParam_AuModCod    As String = "//AUMODCOD"
Const mcteParam_AuProCod    As String = "//AUPROCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarAuModCod        As String
    Dim wvarAuProCod        As String
    
    Dim wvarPos             As Long
    Dim wvarPosCat          As Long
    Dim strParseString      As String
    Dim wvarstrLen          As Long
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarAuModCod = .selectSingleNode(mcteParam_AuModCod).Text
        wvarAuProCod = .selectSingleNode(mcteParam_AuProCod).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarArea = mcteOpID & wvarAuModCod & wvarAuProCod
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
      
      wvarResult = ""
      wvarPos = 49
    
      wvarstrLen = Len(strParseString)

      If Mid(strParseString, wvarPos - 10, 4) <> "0000" Then
          wvarResult = wvarResult & "<AUTIPCOD>" & Mid(strParseString, wvarPos, 4) & "</AUTIPCOD>"
          wvarResult = wvarResult & "<AUTIPCOM>" & Mid(strParseString, wvarPos + 4, 1) & "</AUTIPCOM>"
          wvarResult = wvarResult & "<AUTIPDES>" & Mid(strParseString, wvarPos + 5, 30) & "</AUTIPDES>"
          wvarResult = wvarResult & "<AUCOMDES>" & Mid(strParseString, wvarPos + 30, 60) & "</AUCOMDES>"
          wvarPosCat = wvarPos + 2395
          wvarResult = wvarResult & "<AUCATCOD>" & Mid(strParseString, wvarPosCat + 35, 2) & "</AUCATCOD>"
          
          wvarPos = wvarPos + 95
          wvarResult = wvarResult & "<COMBO>"
          
          While wvarPos < wvarstrLen And Mid(strParseString, wvarPos, 4) <> "0000"
              wvarResult = wvarResult & "<OPTION value='" & Mid(strParseString, wvarPos, 4) & "' "
              wvarResult = wvarResult & "sumaseg='" & CStr(CDbl(Mid(strParseString, wvarPos + 4, 13))) & "," & Mid(strParseString, wvarPos + 4 + 13, 2) & "'>"
              wvarResult = wvarResult & Mid(strParseString, wvarPos, 4) & "</OPTION>"
              wvarPos = wvarPos + 23
          Wend
          
          wvarResult = wvarResult & "</COMBO>"
          wvarStep = 120
          '
          Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
      Else
          Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=NO SE ENCONTRARON DATOS DEL VEHICULO " & Chr(34) & Chr(34) & " /></Response>"
      End If
    '
    wvarStep = 130
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarAuModCod & wvarAuProCod & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub








