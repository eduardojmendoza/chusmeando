package com.qbe.services.scriptventas;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;


import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;

/**
 * Simula el comportamiento de la aplicacion Script de Ventas, poniendo mensajes en la queue específica
 * @author ramiro
 *
 */
public class ScriptDeVentasSim {

	private static final long WAIT_INTERVAL = 60000;
	
	protected static Logger logger = Logger.getLogger(ScriptDeVentasSim.class.getName());

	
	private ApplicationContext context = new ClassPathXmlApplicationContext("ScriptDeVentasBeans.xml");

	public static void main(String... args) throws Exception {
		//Sí, es feo. Pero más feo es tener que poner -Djava.util.logging.config.file=logging.properties
		//http://www.hildeberto.com/2009/04/using-java-logging-configuration-file.html
		Properties prop = System.getProperties();
		  prop.setProperty(
		     "java.util.logging.config.file",
		     "src/main/resources/logging.properties");
		  
		  LogManager.getLogManager().readConfiguration();
		ScriptDeVentasSim sim = new ScriptDeVentasSim();
		int cant = 1;
		long[] tiempos = new long[cant];
		for (int i = 0; i < cant; i++) {
			tiempos[i] = sim.runForrestRun(); 
		}
		long acum = 0;
		for (int i = 0; i < cant; i++) {
			acum += tiempos[i];
		}
		System.out.println("Total: " + acum + " promedio: " + acum/cant);
	}
	
	public long runForrestRun() throws Exception  {
		MQQueueConnectionFactory connFactory = (MQQueueConnectionFactory)context.getBean("connectionFactory");
		MQQueue putQueue = (MQQueue)context.getBean("PUT_QUEUE");
		MQQueue getQueue = (MQQueue)context.getBean("GET_QUEUE");
		
		//Probamos con ambos valores, mismo resultado. Dejo en "no jms" que es lo que debería usar el script de ventas
		//Está configurado en spring
//		putQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_JMS_COMPLIANT);
//		getQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_JMS_COMPLIANT);
//		putQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_NONJMS_MQ);
//		getQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_NONJMS_MQ);
		
		JmsTemplate aisOutboundJMSTemplate;
		JmsTemplate aisInboundJMSTemplate;
		
		aisInboundJMSTemplate = new JmsTemplate(connFactory);
		aisInboundJMSTemplate.setDefaultDestination(putQueue);
		aisInboundJMSTemplate.setMessageConverter(new MQTextMessageConverter());
		
		aisOutboundJMSTemplate = new JmsTemplate(connFactory);
		aisOutboundJMSTemplate.setDefaultDestination(getQueue);
		aisOutboundJMSTemplate.setReceiveTimeout(WAIT_INTERVAL);
		aisOutboundJMSTemplate.setMessageConverter(new MQTextMessageConverter());

		String msg = "000810402602120141201";
//		String msg = "004700000000019731214FS3000ES12013102820131101323300000000120000000001200000000002000450001203244N00100150002012S00NSNSNN0001100500000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               002052238000029999000900                                    000000000000000000000000NNNSSSN0000000000000000000000000000000000000000NSPR";
//		String msg = "1526                  000000000    000000000  000000000  000000000AUS100000001900020166449150000      00000000000000000000000000000000";
//		String msg = IOUtils.toString(ScriptDeVentasSim.class.getResourceAsStream("/script-1540.dat"), "ISO-8859-1");
//		String msg = "1540AUS1SCRIPT    0000000100000000000654   201402172014021700000032996090000000N                                                  7907PR0000                                                                                                      PIÑERO                                  FERNANDO RAUL                 0000000000123262571   19730413MCCOR  ISLAS MALVINAS                          1969          RADA TILLY                              090010017        TEP0297154606685                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  4001000110019     10019     00000000AE377792001023982       2015023      00003000180002501527NPEUGEOT 206 SW XT 1.6 PREMIUM Nacio0005200610DBTH0039556            9362EN6AD6B030207             FJX538    022140010015000001709001   S                    00000000N0123262571   PIÑERO              FERNANDO RAUL                   19730413MCN00                                                               00000000   00                                                               00000000   00                                                               00000000   00                                                               00000000   00                                                               00000000   00                                                               00000000   00                                                               00000000   00                                                               00000000   00                                                               00000000   0000                              000000000000000000                              000000000000000000                              000000000000000000                              000000000000000000                              000000000000000000                              000000000000000000                              000000000000000000                              000000000000000000                              000000000000000000                              00000000000000 0                                                                      00                                                                                                              00000   0000                                   014990300100000000000000000000000000000001312000000006100000000000006100000028130000000061000000000000061000000091400000000610000000000000610000002715000000006100000000000006100000004160000000061000000000000061000008101700000000610000000000000610000003102000000000000000000000000000000032030000000000000000000000000000000033200000000610000000000000610000003304000000000000000000000000000000024410000000061000000000000061000000020500000000000000000000000000000090506000000006100000000000006100000041070000000061000000000000061000000390800000000610000000000000610000004009000000006100000000000006100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                                                                      00000000                                                                                                       000000000                                                                                    00                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    PAR  ISLAS MALVINAS                          1969          RADA TILLY                              090010017        00SCRIPT    04                                     0000000000000000000000";
//		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("script-0047-con-hijos.dat");
//		String msg = IOUtils.toString(is, "ISO-8859-1");
		MQTextMessageCreator mc = new MQTextMessageCreator();
		mc.value = msg;
		long sentTime = System.currentTimeMillis();
		aisInboundJMSTemplate.send(mc);
		logger.log(Level.FINE,"Enviamos: {0}",mc.tmessage);

		//Así debe funcionar, si está comentado es para debuggear la implementación, así que usamos el método que sigue que agarra lo que venga
		String response = (String)aisOutboundJMSTemplate.receiveSelectedAndConvert("JMSMessageID = '" + mc.tmessage.getJMSMessageID() + "'");
		long receiveTime = System.currentTimeMillis();
		
		//Para probar, agarro lo que venga, sin el select
//		String response = (String)aisOutboundJMSTemplate.receiveAndConvert();
//		long receiveTime = System.currentTimeMillis();
		
		long delta = receiveTime - sentTime ;
		logger.log(Level.FINE, "RESP/REQ OK: [[" + response + "],[" + msg + "]]");
		logger.log(Level.FINE, "Tardó : " + delta);
		if ( response == null ) {
			throw new Exception("TIMEOUT " + WAIT_INTERVAL + " excedido.");
		}
		return delta;
	}
}