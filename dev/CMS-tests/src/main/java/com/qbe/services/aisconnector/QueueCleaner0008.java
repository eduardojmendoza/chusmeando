package com.qbe.services.aisconnector;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;

import org.springframework.jms.core.JmsTemplate;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.common.CommonConstants;

public class QueueCleaner0008 {

	protected static Logger logger = Logger.getLogger(QueueCleaner0008.class.getName());

	/**
	 * @param args
	 * @throws JMSException 
	 */
	public static void main(String[] args) throws JMSException {
		QueueCleaner0008 tester = new QueueCleaner0008();
		tester.cleanUp();
	}
	
	public void cleanUp() throws JMSException {

		MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();
		connectionFactory.setHostName("ARD810VLNCAP");
		connectionFactory.setPort(5102);
		connectionFactory.setChannel("DQMLBAFWAR01");
		connectionFactory.setQueueManager("DQMLBAFWAR01");
		connectionFactory.setTransportType(1);

		int client = CommonConstants.WMQ_CLIENT_NONJMS_MQ; //1

		MQQueue getQueue = new MQQueue();
		getQueue.setBaseQueueManagerName("DQMLBAFWAR01");
		getQueue.setBaseQueueName("DARLBA01.OSB.SNCV.SCRIPTVENTAS_RESP");
		getQueue.setTargetClient(client);
		
		int waitInterval = 5000;

//		JmsTemplate putTemplate = new JmsTemplate();
//		putTemplate.setConnectionFactory(connectionFactory);
//		putTemplate.setDefaultDestination(putQueue);
		
		JmsTemplate getTemplate = new JmsTemplate();
		getTemplate.setConnectionFactory(connectionFactory);
		getTemplate.setDefaultDestination(getQueue);
		getTemplate.setReceiveTimeout(waitInterval);
		Object response;
		int cant = 0;
//		do {
//			response = getTemplate.receiveAndConvert();
//			
//			if (response != null ) {
//				System.out.println(response.toString());
//				cant++;
//			}
//		} while ( response != null);

		Message m;
		do {
			m = getTemplate.receive();
			
			if (m != null ) {
				System.out.println(m);
				cant++;
			}
		} while ( m != null);
		
		
//		System.out.println("Limpio: " + cant + " mensajes");
		logger.log(Level.FINE, "Limpio: " + cant + " mensajes");

	}

}
