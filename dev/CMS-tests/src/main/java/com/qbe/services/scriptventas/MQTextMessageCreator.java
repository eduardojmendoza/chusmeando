package com.qbe.services.scriptventas;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.MessageCreator;

/**
 * Crea un Message usando un MQTextMessageConverter
 * 
 * @author ramiro
 *
 */
public class MQTextMessageCreator implements MessageCreator {

	public TextMessage tmessage;
	public String value;
	
	@Override
	public Message createMessage(Session session) throws JMSException {
		MQTextMessageConverter conv = new MQTextMessageConverter();
		tmessage = (TextMessage) conv.toMessage(value, session);
		return tmessage;
	}
	
}