package com.qbe.services.aisconnector;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.common.CommonConstants;
import com.ibm.msg.client.wmq.common.internal.messages.WMQTextMessage;

/**
 * Limpia mensajes de una Queue MQ, y los graba en un directorio para un análisis forense
 * 
 * No es parametrizable, modificar las constantes para apuntar a otra Queue
 * 
 * @author ramiro
 * @Deprecated usar la que está en com.qbe.mq.utils o el WebService com.qbe.services.queueutils.QueueUtilsServiceImpl
 */
@Deprecated 
public class QueueCleaner {

//	private static final String QUEUE_NAME = "DARLBA01.SNCV.OSB.SCRIPTVENTAS_REQ";
//	private static final String QUEUE_NAME = "DARLBA.CLEAS.FWORK.INQRY_RESP";
//	private static final String QUEUE_NAME = "DARLBA..VENDIR.INQRY_RESP";
//	private static final String CHANNEL = "DQMLBAFWAR01";
//	private static final String QUEUE_MANAGER = "DQMLBAFWAR01";
//	private static final int PORT = 5102;
//	private static final String HOSTNAME = "ARD810VLNCAP";

//	private static final String QUEUE_NAME = "TARLBA01.SNCV.OSB.SCRIPTVENTAS_REQ";
	private static final String QUEUE_NAME = "TARLBA01.OSB.SNCV.SCRIPTVENTAS_RESP";
	private static final String CHANNEL = "DQMLBAFWAR01";
	private static final String QUEUE_MANAGER = "DQMLBAFWAR01";
	private static final int PORT = 5102;
	private static final String HOSTNAME = "ARD810VLNCAP";
	
	
	
	
	protected static Logger logger = Logger.getLogger(QueueCleaner.class.getName());

	/**
	 * @param args
	 * @throws JMSException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws JMSException, IOException {
		QueueCleaner tester = new QueueCleaner();
		tester.cleanUp();
	}
	
	public void cleanUp() throws JMSException, IOException {

		MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();
		connectionFactory.setHostName(HOSTNAME);
		connectionFactory.setPort(PORT);
		connectionFactory.setChannel(CHANNEL);
		connectionFactory.setQueueManager(QUEUE_MANAGER);
		connectionFactory.setTransportType(1);

		int client = CommonConstants.WMQ_CLIENT_NONJMS_MQ; //1

		
		MQQueue getQueue = new MQQueue();
		getQueue.setBaseQueueManagerName(CHANNEL);

		getQueue.setBaseQueueName(QUEUE_NAME);
		getQueue.setTargetClient(client);
		
		int waitInterval = 30000;

		JmsTemplate getTemplate = new JmsTemplate();
		getTemplate.setConnectionFactory(connectionFactory);
		getTemplate.setDefaultDestination(getQueue);
		getTemplate.setReceiveTimeout(waitInterval);
		Object response;
		
		//Para Browsear:
		getTemplate.browse(getQueue, new BrowserCallback<Object>() {

			@Override
			public Object doInJms(Session session, QueueBrowser browser) throws JMSException {
				@SuppressWarnings("unchecked")
				Enumeration<Object> enu =  browser.getEnumeration();
				while ( enu.hasMoreElements()) {
					Object msg = enu.nextElement();
					if ( msg instanceof TextMessage) {
						TextMessage textMsg = (TextMessage) msg;
						Object putDate = textMsg.getObjectProperty("JMS_IBM_PutDate");
					}
					
				}
				return null;
			}
		});
		int cant = 0;
		do {
			response = getTemplate.receiveAndConvert();
			if (response != null )  {
				FileUtils.writeStringToFile(new File("/tmp/queuecleaner/" + cant + ".mqmsg"), response.toString(), Charset.forName("UTF-8"));
				cant++;
//				System.out.print('.');
			}
		} while ( response != null);
//		System.out.println("Limpio: " + cant + " mensajes");
		logger.log(Level.FINE, "Limpio: " + cant + " mensajes");

	}

}
