package com.qbe.services.aisconnector;

import java.util.logging.Logger;

import javax.jms.JMSException;

import org.springframework.jms.core.JmsTemplate;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.common.CommonConstants;

/**
 * Pone mensajes en la queue, sin sacarlos.
 * Para probar correlación
 * 
 * @author ramiro
 *
 */
public class QueueFiller {

	protected static Logger logger = Logger.getLogger(QueueFiller.class.getName());

	/**
	 * @param args
	 * @throws JMSException 
	 */
	public static void main(String[] args) throws JMSException {
		QueueFiller tester = new QueueFiller();
		tester.cleanUp();
	}
	
	public void cleanUp() throws JMSException {

		MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();
		connectionFactory.setHostName("ARD810VLNCAP");
		connectionFactory.setPort(5102);
		connectionFactory.setChannel("DQMLBAFWAR01");
		connectionFactory.setQueueManager("DQMLBAFWAR01");
		connectionFactory.setTransportType(1);

		int client = CommonConstants.WMQ_CLIENT_NONJMS_MQ; //1

		MQQueue putQueue = new MQQueue();
		putQueue.setBaseQueueName("DARLBA01.SNCV.OSB.SCRIPTVENTAS_REQ");
		putQueue.setTargetClient(client);
		
		JmsTemplate putTemplate = new JmsTemplate();
		putTemplate.setConnectionFactory(connectionFactory);
		putTemplate.setDefaultDestination(putQueue);
		
//		String msg = "11010001              000000000    000000000  000000000  000000000AUT100807109710900130000010000";
		String msg = "004700000000019731214FS3000ES12013102820131101323300000000120000000001200000000002000450001203244N00100150002012S00NSNSNN0001100500000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               002052238000029999000900                                    000000000000000000000000NNNSSSN0000000000000000000000000000000000000000NSPR";
		for (int i = 0; i < 100; i++) {
			putTemplate.convertAndSend(msg);
		}

	}

}
