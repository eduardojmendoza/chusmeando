package com.qbe.services.scriptventas;

import java.io.UnsupportedEncodingException;

public class WTFString {

	public WTFString() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws UnsupportedEncodingException 
	 */
	public static void main(String[] args) throws UnsupportedEncodingException {
		//	Recibimos:
//		  JMSMessageID:     ID:4553414141454e434f4e5452454c4146414c4c4100000000
//		  JMSTimestamp:     0
//		  JMSCorrelationID: ID:414d512044514d4c42414657415230314967175202670c20

		System.out.println("0047 responde:");
		printAsString("3c3f786d6c2076657273696f6e3d22312e302220656e636f64696e673d2249534f2d383835392d31");
		
		System.out.println("viene...");
		printAsString("414d512044514d4c424146574152");

		printAsString("4553414141454e434f4e5452454c4146414c4c4100000000");
		System.out.println("\n\n\n");
		printAsString("414d512044514d4c42414657415230314967175202670c20"); //Este no es un ISO-8859-1 ni UTF-8, tiene caracteres de control. Parece que no es un string!
		//Este es el mensaje
		System.out.println("\n\n\n");
		printAsString("31303430323630323132303133303930314155533130303030303030313030303030303031313839303039303030303030303020202020303030303030303030303030303030303030303030303030303030303030415553313030303030303031303030303030303133343830303530303030303030302020202030303030303030303030303030303030303030303030303030303030303041555331303030303030303130303030303030313439383632333030303030303030202020203030303030303030303030303030303030303030303030303030303030302020202030303030303030303030303030303030303030303030303030303030303020202020303030303030303030303030303030303030303030303030303030303030202020203030303030303030303030303030303030303030303030303030303030302020202030303030303030303030303030303030303030303030303030303030303020202020303030303030303030303030303030303030303030303030303030303030202020203030303030");
		
	}

	private static void printAsString(String bytesString) throws UnsupportedEncodingException {
		if ( bytesString.length() % 2 != 0 ) {
			System.out.println("Eh? no lo puedo leer como byte[] en hexa si es impar");
		}
		byte[] byteArray = new byte[bytesString.length() / 2];
		int i;
		for (i = 0; i < bytesString.length(); i+=2) {
			byte b = Byte.parseByte(bytesString.substring(i,i+2),16);
			if ( b == 0 ) break;
			if ( b < 32 || ( 127 <= b && b <= 159) ) b = (byte)0xBF;
			byteArray[i/2] = b;
		}
		byte[] chopped = new byte[i/2];
		System.arraycopy(byteArray, 0, chopped, 0, i/2);
		String mistery = new String(chopped,"ISO-8859-1");
		System.out.println(String.format("[%s] = [decode(%s)]", mistery, bytesString));
	}

}
