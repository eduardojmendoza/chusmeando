package com.qbe.services.scriptventas;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.MQHeaderList;
import com.ibm.mq.headers.MQMD;

@Deprecated
public class ScriptDeVentasRawSim {

	/**
	 * @param args
	 * @throws MQDataException 
	 */
	public static void main(String[] args) throws MQDataException {

		try {

			Properties props = new Properties();
			props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));

			MQEnvironment.hostname = props.getProperty("queue_hostname");
			MQEnvironment.port = Integer.parseInt(props.getProperty("queue_port"));
			MQEnvironment.channel = props.getProperty("queue_channel");

			MQQueueManager qMgr = new MQQueueManager(props.getProperty("queue_manager"));
			putMessage(props, qMgr);
			//OJO! si habilitamos el getMessage se trae el primer mensaje que encuentra!!
//			getMessage(props, qMgr);			
			qMgr.disconnect();

		} catch (MQException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void putMessage(Properties props, MQQueueManager qMgr)
			throws MQException, IOException, MQDataException {
		int openOptions_put = CMQC.MQOO_FAIL_IF_QUIESCING  | CMQC.MQOO_OUTPUT | CMQC.MQPMO_NEW_MSG_ID;

		MQQueue putQueue = qMgr.accessQueue(props.getProperty("inbound_queue_name"), openOptions_put);
		MQMessage mqmsg = new MQMessage();
		mqmsg.characterSet = 819;
		mqmsg.writeString(IOUtils.toString(ScriptDeVentasRawSim.class.getResourceAsStream("/script-1540.dat"), "ISO-8859-1"));
//		mqmsg.writeString("1526                  000000000    000000000  000000000  000000000AUS100000001900020166449130000      00000000000000000000000000000000");
//		mqmsg.writeString("004700000000019731214FS3000ES12013102820131101323300000000120000000001200000000002000450001203244N00100150002012S00NSNSNN0001100500000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000  00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               00000000000000                               002052238000029999000900                                    000000000000000000000000NNNSSSN0000000000000000000000000000000000000000NSPR");
//		mqmsg.writeString("000810402602120130901");
//		mqmsg.messageId = "SNOOP_123456789012345678".getBytes();
		
		MQPutMessageOptions pmo = new MQPutMessageOptions();
		putQueue.put(mqmsg, pmo);

		//Debug
//			http://publib.boulder.ibm.com/infocenter/wmqv7/v7r1/index.jsp?topic=%2Fcom.ibm.mq.doc%2Ffr13030_.htm
		System.out.println (new MQHeaderList (mqmsg, true));			
		
//			This example also prints out the message descriptor fields, using the MQMD class. 
//			The copyFrom() method of the com.ibm.mq.headers.MQMD class populates the header object from
//			the message descriptor fields of the MQMessage rather than by reading the message body.
		MQMD md = new MQMD ();
		md.copyFrom (mqmsg);
		System.out.println (md + "\n" + new MQHeaderList (mqmsg, true));

		System.out.println ("--------------------------------------------");
		
		// Probar para manejar el encoding:
//			http://publib.boulder.ibm.com/infocenter/wmqv7/v7r1/topic/com.ibm.mq.doc/ja11130_.htm
//			This example writes to a byte array using a DataOutputStream:
//				MQHeader header = ... // Could be any header type
//				ByteArrayOutputStream out = new ByteArrayOutputStream ();
//
//				header.write (new DataOutputStream (out), CMQC.MQENC_NATIVE, CMQC.MQCCSI_DEFAULT);
//				byte [] bytes = out.toByteArray ();
//				When you work with streams in this way, be careful to use the correct values for the encoding and characterSet arguments. When reading headers, specify the encoding and CCSID with which the byte content was originally written. When writing headers, specify the encoding and CCSID that you want to produce. The data conversion is performed automatically by the header classes.

		putQueue.close();
	}

	private static void getMessage(Properties props, MQQueueManager qMgr)
			throws MQException, IOException, MQDataException {
		//Leo de nuevo el mensaje
		int openOptions_get = CMQC.MQOO_INPUT_AS_Q_DEF | CMQC.MQOO_FAIL_IF_QUIESCING | CMQC.MQGMO_CONVERT | CMQC.MQGMO_WAIT; 
		
		
		MQQueue getQueue = qMgr.accessQueue(props.getProperty("outbound_queue_name"), openOptions_get);

		MQMessage readMessage = new MQMessage();
		MQGetMessageOptions gmo = new MQGetMessageOptions();
		gmo.waitInterval = 10000;
		gmo.options = openOptions_get;
		getQueue.get(readMessage,gmo);  // has default values

		//OJO si escribo esto, mueve el buffer y no puedo leer el contenido abajo
//		System.out.println (new MQHeaderList (readMessage, true));			

		// Extract the message data
		int dataLength = readMessage.getDataLength();
		System.out.println("Datalength: " + dataLength);
		
		int dataOffset = readMessage.getDataOffset();
		if ( dataOffset!= 0 ) {
			System.out.println("WARNING no estoy posicionado al inicio del body");
		}
		System.out.println("Dataoffset: " + dataOffset);

		//Coded Character Set Identifier (CCSID) aka Codepage
		int characterSet = readMessage.characterSet;
		System.out.println("Characterset: " + characterSet);
		//De javadoc:
//		850		commonly used ASCII codeset
//		819		the ISO standard ASCII codeset
//		37		the American EBCDIC codeset
//		1200	Unicode
//		1208	UTF-8
		String charsetName;
		switch (characterSet) {
		case 850: 
			charsetName = "US-ASCII";
			break;
		case 819: 
			charsetName = "ISO-8859-1";
			break;
		case 37: //no anda
			charsetName = "?????";
			break;
		case 1200: //será unicode-16??? 
			charsetName = "?????";
			break;
		case 1208: 
			charsetName = "UTF-8";
			break;
		default:
			charsetName = "?????";
			break;
		}
		System.out.println("Character set : " + charsetName);
		//Acá lo leo como venga, puede salir cualquier cosa porque no está claro en qué encoding convierto.
		//String data = readMessage.readStringOfByteLength(dataLength);
		
		CharsetDecoder cd = Charset.forName(charsetName).newDecoder();
		byte[] stringBytes = new byte[dataLength]; 
		readMessage.readFully(stringBytes);
		ByteBuffer bbuffer = ByteBuffer.wrap(stringBytes);
		CharBuffer cbuffer = cd.decode(bbuffer);
		String data = cbuffer.toString();
		System.out.println("cbuffer: " + data);
		
		
		getQueue.close();
		
	}

}
