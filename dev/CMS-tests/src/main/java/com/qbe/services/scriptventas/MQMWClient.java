package com.qbe.services.scriptventas;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.util.StreamUtils;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.WMQConstants;

/**
 * Simula el comportamiento de la aplicacion Script de Ventas, poniendo mensajes en la queue específica
 * @author ramiro
 *
 */
public class MQMWClient {

	private static final long WAIT_INTERVAL = 20000;
	
	protected static Logger logger = Logger.getLogger(MQMWClient.class.getName());

	
	private ApplicationContext context = new ClassPathXmlApplicationContext("MQMWBeans.xml");

	public static void main(String... args) throws Exception {
		//Sí, es feo. Pero más feo es tener que poner -Djava.util.logging.config.file=logging.properties
		//http://www.hildeberto.com/2009/04/using-java-logging-configuration-file.html
		Properties prop = System.getProperties();
		  prop.setProperty(
		     "java.util.logging.config.file",
		     "src/main/resources/logging.properties");
		  
		  LogManager.getLogManager().readConfiguration();
		MQMWClient sim = new MQMWClient();
		sim.runForrestRun();
	}
	
	public void runForrestRun() throws Exception  {
		MQQueueConnectionFactory connFactory = (MQQueueConnectionFactory)context.getBean("connectionFactory");
		MQQueue putQueue = (MQQueue)context.getBean("PUT_QUEUE");
		MQQueue getQueue = (MQQueue)context.getBean("GET_QUEUE");
		
		//Probamos con ambos valores, mismo resultado. Dejo en "no jms" que es lo que debería usar el script de ventas
		//Está configurado en spring
//		putQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_JMS_COMPLIANT);
//		getQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_JMS_COMPLIANT);
//		putQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_NONJMS_MQ);
//		getQueue.setIntProperty(WMQConstants.WMQ_TARGET_CLIENT,WMQConstants.WMQ_CLIENT_NONJMS_MQ);
		
		JmsTemplate aisOutboundJMSTemplate;
		JmsTemplate aisInboundJMSTemplate;
		
		aisInboundJMSTemplate = new JmsTemplate(connFactory);
		aisInboundJMSTemplate.setDefaultDestination(putQueue);
		aisInboundJMSTemplate.setMessageConverter(new MQTextMessageConverter());
		
		aisOutboundJMSTemplate = new JmsTemplate(connFactory);
		aisOutboundJMSTemplate.setDefaultDestination(getQueue);
		aisOutboundJMSTemplate.setReceiveTimeout(WAIT_INTERVAL);
		aisOutboundJMSTemplate.setMessageConverter(new MQTextMessageConverter());

		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("mqmw_msg.txt");
		String msg = StreamUtils.copyToString(is, Charset.forName("UTF-8"));
		is.close();
		
		MQTextMessageCreator mc = new MQTextMessageCreator();
		mc.value = msg;
		aisInboundJMSTemplate.send(mc);
		logger.log(Level.FINE,"Enviamos: {0}",mc.tmessage);

		//Así debe funcionar, lo comentamos porque estamos debuggeando la implementación, así que usamos el método que sigue que agarra lo que venga
//		String response = (String)aisOutboundJMSTemplate.receiveSelectedAndConvert("JMSMessageID = '" + mc.tmessage.getJMSMessageID() + "'");

		//Para probar, agarro lo que venga, sin el select
		String response = (String)aisOutboundJMSTemplate.receiveAndConvert();
		
		logger.log(Level.FINE, "REQ/RESP OK: [[" + msg + "],[" + response + "]]");
		if ( response == null ) {
			throw new Exception("TIMEOUT " + WAIT_INTERVAL + " excedido.");
		}
	}
}

