package com.qbe.services.aisconnector;

import java.util.logging.Logger;

import javax.jms.JMSException;

import org.springframework.jms.core.JmsTemplate;

import com.ibm.mq.jms.MQQueue;
import com.ibm.mq.jms.MQQueueConnectionFactory;
import com.ibm.msg.client.wmq.common.CommonConstants;

/**
 * Pone mensajes en la queue, sin sacarlos.
 * Para probar correlación
 * 
 * @author ramiro
 *
 */
public class QueueFiller0008 {

	protected static Logger logger = Logger.getLogger(QueueFiller0008.class.getName());

	/**
	 * @param args
	 * @throws JMSException 
	 */
	public static void main(String[] args) throws JMSException {
		QueueFiller0008 tester = new QueueFiller0008();
		tester.cleanUp();
	}
	
	public void cleanUp() throws JMSException {

		MQQueueConnectionFactory connectionFactory = new MQQueueConnectionFactory();
		connectionFactory.setHostName("ARD810VLNCAP");
		connectionFactory.setPort(5102);
		connectionFactory.setChannel("DQMLBAFWAR01");
		connectionFactory.setQueueManager("DQMLBAFWAR01");
		connectionFactory.setTransportType(1);

		int client = CommonConstants.WMQ_CLIENT_NONJMS_MQ; //1

		MQQueue putQueue = new MQQueue();
		putQueue.setBaseQueueName("DARLBA01.SNCV.OSB.SCRIPTVENTAS_REQ");
		putQueue.setTargetClient(client);
		
		JmsTemplate putTemplate = new JmsTemplate();
		putTemplate.setConnectionFactory(connectionFactory);
		putTemplate.setDefaultDestination(putQueue);
		
		String msg = "000810402602120130902";
		for (int i = 0; i < 1; i++) {
			putTemplate.convertAndSend(msg);
		}
		System.out.println("Finished");
	}

}
