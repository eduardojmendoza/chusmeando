package com.qbe.services.multitester;

import java.io.File;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * 
 * 
 * 
 * Tomado de http://jorgemanrubia.net/2008/09/18/generating-junit-test-cases-dynamically/
 * @author ramiro
 *
 */
public class MultiFileTestSuite {
	private static final String[] dirs = {
		
		"../../dev/CMS-mqgen",
		"../../dev/CMS-LBAWEBMQ",
		"../../dev/CMS-LBAVIRTUAL",
		"../../dev/CMS-nbwsA_Transacciones",
		"../../dev/CMS-ovmq",
		"../../dev/CMS-OVMQCotizar",
		"../../dev/CMS-OVMQEmision",
		"../../dev/CMS-lbawA_OVCotEndosos",
		"../../dev/CMS-lbawA_SiniestroDenuncia",
		"../../dev/CMS-nbwsA_Transacciones",
		"../../dev/CMS-OVMQRiesgos",
		"../../dev/CMS-OVMQUsuario"
		};
	 
	  public static Test suite() {
	    TestSuite suite = new TestSuite(
	        "Multitester");
	    //$JUnit-BEGIN$
		for (String dir : dirs) {
			suite.addTest(new ProjectTestSuite(dir));
		}
	    //$JUnit-END$
	    return suite;
	  }
	 }