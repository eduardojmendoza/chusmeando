package com.qbe.services.scriptventas;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.common.CurrentProfile;


public class ScriptDeVentasSimTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		String prof = CurrentProfile.getProfileName();
		Assume.assumeTrue(prof.equals("prod"));
		ScriptDeVentasSim sim = new ScriptDeVentasSim();
		sim.runForrestRun();
	}

}
