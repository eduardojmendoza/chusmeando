package com.qbe.services.multitester;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.qbe.services.cms.osbconnector.OSBConnector;
import com.qbe.vbcompat.xml.XmlDomExtended;
import com.qbe.vbcompat.xml.XmlDomExtendedException;

import junit.framework.TestCase;

public class LogFileTestCase extends TestCase {

	public static final String TEST_SERVICE = "http://10.1.10.98:8011/OV/Proxy/QBESvc?wsdl";
	
	private File file;

	public LogFileTestCase() {
	}
	
	public LogFileTestCase(File file) {
		this();
		this.file = file;
		setName(file.getName());
	}

	@Override
	public void setUp() throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
	}

	//Puesto solamente para que pase cuando ejecuto mvn test, sin especificar que corra la suite
	public void testDummy() {
	}
	
	protected void runTest() throws Throwable {
		if ( file != null) {
			this.runTestsInFile();
		}
	}
	
	/**
	 * Testea que 
	 * @throws Exception 
	 * @throws MalformedURLException 
	 * @throws Exception 
	 * 
	 */
	public void runTestsInFile() throws DOMException, XmlDomExtendedException, MalformedURLException, Exception  {
//		System.out.println(file.getAbsolutePath());
		XmlDomExtended xmlDom = new XmlDomExtended();
		xmlDom.load(file);
		String actionCode = xmlDom.selectSingleNode("//LOGS/ACTIONCODE").getTextContent();
		NodeList logNodes = xmlDom.selectNodes("//LOGS/LOG");
		for (int i = 0; i < logNodes.getLength(); i++) {
			Node logNode = logNodes.item(i);
			Node requestNode = XmlDomExtended.Node_selectSingleNode(logNode, "//Request");
			Node responseNode = XmlDomExtended.Node_selectSingleNode(logNode, "//Response");
			
			String requestXML = XmlDomExtended.marshal(requestNode);
			String responseXML = XmlDomExtended.marshal(responseNode);
			
			OSBConnector osbcon = new OSBConnector();
			String endpoint = System.getProperty("cms.test.endpoint");
			if ( endpoint == null ) endpoint = TEST_SERVICE; 
			osbcon.setServiceURL(endpoint);
//			try {
//				System.out.println(actionCode);
				String actualResponse = osbcon.executeRequest(actionCode, requestXML);
				DetailedDiff myDiff = new DetailedDiff(XMLUnit.compareXML(responseXML, actualResponse));
				List allDifferences = myDiff.getAllDifferences();
//				assertEquals(myDiff.toString(), 0, allDifferences.size());
//				System.out.println("Diferencias = " + allDifferences.size());
				//Eliminar los &lt; y &gt;
				//XMLAssert.assertXMLEqual("No volvió la respuesta esperada", expectedResponse, responseText);
//			} catch (OSBConnectorException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (MalformedURLException e) {
//				// TODO Auto-generated catch block
////				e.printStackTrace();
//			} catch (SAXException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
		}
		
	}
	
}