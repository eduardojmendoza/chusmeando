package com.qbe.services.multitester;

import java.io.File;

import junit.framework.TestSuite;

public class ProjectTestSuite extends TestSuite {
	public ProjectTestSuite(String dirName) {
		super(dirName);
		String fullDir = dirName + "/src/test/resources";
		File directory = new File(fullDir);
		for (File file : directory.listFiles()) {
			if ( file.isFile() ) {
				String filename = file.getName();
				if ( filename.matches("Log(\\w+)\\.xml")) {
					addTest(new LogFileTestCase(file));
				}
			}
		}
	}
}