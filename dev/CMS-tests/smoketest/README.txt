Ejecutar estos comandos para probar varios servicios:
En cada caso no debe haber error de ejecución ( HTTP 200), y además se pueden revisar los resultados comparando con los archivs *.expected
Todos estos archivos se crearon ejecutando contra el 10.1.10.70:7101

export OSBHOST=10.1.10.202
export OSBPORT=7201

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" --data \
@commconverter.xml \
http://$OSBHOST:$OSBPORT/CMS-fewebservices/CommStructureConverter

Comparar con commconverter.xml.expected

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @nbwsA_Alta.xml \
http://$OSBHOST:$OSBPORT/CMS-fewebservices/MigratedComponentService

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @msg_1308.xml \
http://$OSBHOST:$OSBPORT/CMS-fewebservices/MigratedComponentService

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @msg_1419.xml \
http://$OSBHOST:$OSBPORT/CMS-fewebservices/MigratedComponentService

curl -v -X POST -H "Content-Type: text/xml;charset=UTF-8" \
--data @mqbridge.xml \
http://$OSBHOST:$OSBPORT/CMS-fewebservices/MQBridge

