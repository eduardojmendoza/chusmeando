package com.qbe.services.servlets;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;



public class ProxyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
		
	private static Logger logger = Logger.getLogger(ProxyServlet.class.getName());
	
	private String urlBase = "http://10.1.10.218:8090/analytics/saw.dll?";
	private String urlDashboard1 = "Dashboard&PortalPath=%2Fshared%2FInformes de Ventas%2F_portal%2FInformes Impositivos - INSIS&Page=Requerimientos AFIP&Action=Navigate&col1=%22PERIODO EMISION%22.%22NMES%22&valeval1=%22Septiembre%22&psa1=%22LBA_OPERACIONES_INSIS%22&col2=%22PERIODO EMISION%22.%22ANO%22&valeval2=%222013%22&psa2=%22LBA_OPERACIONES_INSIS%22";
	private String paramsAuth = "&nquser=Consumer&nqpassword=Userpass1";

    /**

     * @see HttpServlet#HttpServlet()

     */

    public ProxyServlet() {

        super();

        // TODO Auto-generated constructor stub

    }


    /**

     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)

     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.getParameter("tipo");
    	logger.log(Level.INFO, request.getParameter("url"));
    	if(request.getParameter("tipo")=="1"){
    		logger.log(Level.WARNING, request.getParameter("url"));    		
    		System.out.println(request.getParameter("url"));
    	}
    	/*
        StringBuffer requestURL = request.getRequestURL();
    	if (request.getQueryString() != null) {
    	    requestURL.append("?").append(request.getQueryString());
    	}
    	*/
    	// Set response content type
        response.setContentType("text/html");
        // New location to be redirected
        //response.setStatus(response.SC_MOVED_TEMPORARILY);
            
    	//String completeURL = requestURL.toString();
        HttpClient httpClient = new DefaultHttpClient();
		
	try {
		
		HttpGet pageGet = new HttpGet(buildURI(buildURLtoDashboard()));
		HttpResponse response1 = httpClient.execute(pageGet);
		
	} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
        service(request,response);
    }

    /**

     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)

     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	service(request,response);
        // TODO Auto-generated method stub


    }
    protected void service(HttpServletRequest request, HttpServletResponse response){
    	try {
			response.sendRedirect(urlDashboard1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }
	
    
    public java.net.URI buildURI (String URLBase) throws URISyntaxException {
		    
		    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		    nameValuePairs.add(new BasicNameValuePair("nquser","Consumer"));
		    nameValuePairs.add(new BasicNameValuePair("nqpassword","Userpass1"));
		      
		    URIBuilder builder = new URIBuilder()
		            .setHost(URLBase);
		            
		    
		    java.net.URI uri = builder.build();
		    
		    return uri;
	}
    protected String buildURLtoDashboard(){
    	return urlBase+urlDashboard1+paramsAuth;
    }
}

