package com.qbe.services.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import com.sun.org.apache.xerces.internal.util.URI;



public class URLProto {
	
	public static void main(String args[]) throws IOException, URISyntaxException{
		String nquser="Consumer";
		String nqpassword="Userpass1";
		String query = "Dashboard&PortalPath=%2Fshared%2FInformes%20de%20Ventas%2F_portal%2FInformes%20Impositivos%20-%20INSIS&Page=Requerimientos%20AFIP&Action=Navigate&col1=%22PERIODO%20EMISION%22.%22NMES%22&valeval1=%22Septiembre%22&psa1=%22LBA_OPERACIONES_INSIS%22&col2=%22PERIODO%20EMISION%22.%22ANO%22&valeval2=%222013%22&psa2=%22LBA_OPERACIONES_INSIS%22";
		String charset = "UTF-8";
		String URLBase = "10.1.10.218:8090/analytics/saw.dll?";
		HttpClient httpClient = new DefaultHttpClient();
		//HttpGet pageGet = new HttpGet(buildURI(URLBase+query));
		HttpGet pageGet = new HttpGet(buildURI(URLBase));
		HttpResponse response = httpClient.execute(pageGet);
		
		System.out.println(EntityUtils.toString(response.getEntity()));
		System.out.println(response.getFirstHeader("Set-Cookie") == null ? "" : 
            response.getFirstHeader("Set-Cookie").toString());
		System.out.println(response.getStatusLine().getStatusCode()+"\n"+response.getStatusLine().getReasonPhrase());
	}
	
	public static java.net.URI buildURI (String URLBase) throws URISyntaxException {
	    
	    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
	    nameValuePairs.add(new BasicNameValuePair("nquser","Consumer"));
	    nameValuePairs.add(new BasicNameValuePair("nqpassword","Userpass1"));
	      
	    URIBuilder builder = new URIBuilder()
	            .setScheme("http")
	            .setHost(URLBase)
	            .setParameters(nameValuePairs);
	    
	    java.net.URI uri = builder.build();
	    System.out.println(uri);
	    return uri;
	}
}