package com.qbe.services.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.qbe.services.utils.filter.Filter;
import com.qbe.services.utils.filter.FilterEngine;
import com.qbe.services.utils.filter.FilterException;

public class RegExTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAsync() {
		String code = "        wvarStep = 230;\n" +
"        //unsup wobjXMLResponse.async = false;\n" +
"        wobjXMLResponse.loadXML( wvarResult );\n";
		String newCode = code.replaceAll("\\s*//unsup (\\w+)\\.async = false;",
				"");
		System.out.println(newCode);
		assertTrue(newCode.equals("        wvarStep = 230;\n" + "        wobjXMLResponse.loadXML( wvarResult );\n"));
	}

	@Test
	public void testDirs() {
	
		String code = "      wobjXMLConfig.load( System.getProperty(\"user.dir\") + \"\\\" + ModGeneral.gcteConfFileName );\n"+
      "//\n"+
      "//Levanto los Parametros de la cola de MQ del archivo de configuraci�n\n"+
      "wvarStep = 60;\n"+
      "wobjXMLParametros = new XmlDomExtended();\n"+
      "wobjXMLParametros.load( System.getProperty(\"user.dir\") + \"\\\" + ModGeneral.gcteParamFileName );\n";
		String expected = "      wobjXMLConfig.load( Thread.currentThread().getContextClassLoader().getResource(Constants.ROOT_DEF_DIR + File.separator + ModGeneral.gcteConfFileName).getFile());\n"+
			      "//\n"+
			      "//Levanto los Parametros de la cola de MQ del archivo de configuraci�n\n"+
			      "wvarStep = 60;\n"+
			      "wobjXMLParametros = new XmlDomExtended();\n"+
			      "wobjXMLParametros.load( Thread.currentThread().getContextClassLoader().getResource(Constants.ROOT_DEF_DIR + File.separator + ModGeneral.gcteParamFileName).getFile());\n";
		String pattern = "load\\( System\\.getProperty\\(\"user\\.dir\"\\) \\+ \"\\\\+\" \\+ ModGeneral\\.(\\w+)+ \\);";
		String newCode = code.replaceAll(pattern,
			      "load( Thread.currentThread().getContextClassLoader().getResource(Constants.ROOT_DEF_DIR + File.separator + ModGeneral.$1).getFile());");
		System.out.println(newCode);
		assertTrue("No coinciden", newCode.equals(expected));
	}
	
	@Test
	public void testUTFFinalReplace() {
		//primero testeo si la conversión que va a hacer el componente funciona
		String doc = "<Response><Estado resultado='true' mensaje='' /><?xml version=\"1.0\" encoding=\"UTF-8\"?><REGS><REG>";
		String xmlPattern = "<\\?xml version=\"1\\.0\" encoding=\"UTF-\\d+\"\\?>";
		doc = doc.replaceAll( xmlPattern, "" );
		assertTrue("No sacó el header xml", doc.equals("<Response><Estado resultado='true' mensaje='' /><REGS><REG>"));
	}
	
	@Test
	public void testUTF_1() throws FilterException {
		String code = "         wvarResult = Strings.replace( wobjXMLResponse.transformNode( wobjXSLResponse ).toString(), \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-16\\\"?>\", \"\" );";
		String expected = "         wvarResult = wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( \"<\\\\?xml version=\\\"1\\\\.0\\\" encoding=\\\"UTF-\\\\d+\\\"\\\\?>\", \"\" );";
		Filter f = ConvertsFixer.getFixUTF();
		String newCode = f.filter(code);
		assertTrue("No coinciden", newCode.equals(expected));
	}
	
	@Test
	public void testUTF_2() throws FilterException {
		String code = "       wvarResult = \"<ROW>\" + wvarResult + Strings.replace( wobjXMLResponse.transformNode( wobjXSLResponse ).toString(), \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-16\\\"?>\", \"\" ) + \"</ROW>\";";
		String expected = "       wvarResult = \"<ROW>\" + wvarResult + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( \"<\\\\?xml version=\\\"1\\\\.0\\\" encoding=\\\"UTF-\\\\d+\\\"\\\\?>\", \"\" ) + \"</ROW>\";";
		Filter f = ConvertsFixer.getFixUTF();
		String newCode = f.filter(code);
		assertTrue("No coinciden", newCode.equals(expected));
	}
	
	@Test
	public void testUTF_3() throws FilterException {
		String code = "wvarNuevoXML = Strings.replace( wobjXmlDOMResp.transformNode( wobjXslDOMResp ).toString(), \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-16\\\"?>\", \"\" );";
		String expected = "wvarNuevoXML = wobjXmlDOMResp.transformNode( wobjXslDOMResp ).toString().replaceAll( \"<\\\\?xml version=\\\"1\\\\.0\\\" encoding=\\\"UTF-\\\\d+\\\"\\\\?>\", \"\" );";
		Filter f = ConvertsFixer.getFixUTF();
		String newCode = f.filter(code);
		assertTrue("No coinciden", newCode.equals(expected));
	}
	
	@Test
	public void testUTF_4() throws FilterException {
		String code = "wvarResultParcial = wvarResultParcial + Strings.replace( wobjXMLResponse.transformNode( wobjXSLResponse ).toString(), \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-16\\\"?>\", \"\" );";
		String expected = "wvarResultParcial = wvarResultParcial + wobjXMLResponse.transformNode( wobjXSLResponse ).toString().replaceAll( \"<\\\\?xml version=\\\"1\\\\.0\\\" encoding=\\\"UTF-\\\\d+\\\"\\\\?>\", \"\" );";
		Filter f = ConvertsFixer.getFixUTF();
		String newCode = f.filter(code);
		assertTrue("No coinciden", newCode.equals(expected));
	}

	@Test
	public void testPrettyPrint() throws FilterException {
		String code = "ParseoMensaje = wobjXmlDOMResp.getDocument().getDocumentElement().toString();";
		Filter f = ConvertsFixer.getMigratedFilters();
		String expected = "ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());";
		String newCode = f.filter(code);
		assertTrue("No coinciden", newCode.equals(expected));
	}
	@Test
	public void testExitFor() throws FilterException {
		String code = "//unsup: If Trim(Strings.replace(wobjMatch.SubMatches(0), \"_\", \" \")) = \"\" Then Exit For";
		Filter f = ConvertsFixer.getMigratedFilters();
//		String expected = "ParseoMensaje = XmlDomExtended.marshal(wobjXmlDOMResp.getDocument().getDocumentElement());";
		String newCode = f.filter(code);
		System.out.println(newCode);
//		assertTrue("No coinciden", newCode.equals(expected));
	}
		
}
