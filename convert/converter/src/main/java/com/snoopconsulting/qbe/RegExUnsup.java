package com.snoopconsulting.qbe;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.qbe.services.utils.filter.Filter;
import com.qbe.services.utils.filter.FilterException;

public class RegExUnsup implements Filter {
	
	private String delimiter;
	
	private String unsupPrefix;
	
	public RegExUnsup(String delimiter, String unsupPrefix) {
		this.delimiter = delimiter;
		this.unsupPrefix = unsupPrefix;
	}
	
	/**
	 * Limpia la line que recibe como par�metro de expresiones "unsup" que genera el conversor de VB a Java
	 * Borra el \/\* unsup inicial y el \*\/ final para cada par que encuentra
	 * Si hay un unsupPrefix tambi�n lo elimina
	 * 
	 * @param line
	 * @param delimiter
	 * @param unsupPrefix prefijo del unsup. Se toma tal cual, o sea es un String NO una regexp
	 * @return
	 */
	public String removeUnsup(String line, String delimiter, String unsupPrefix) {
		
//		if (delimiter == null && (line.indexOf("null /*unsup") != -1 || line.indexOf("null (*unsup") != -1)) {
//			return tratamientoNullUnsup(line);
//		}
		
		final String unsupexpr;
		if (unsupPrefix != null) {
			unsupexpr = Pattern.quote(unsupPrefix) + "/\\*unsup ";
		} else {
			unsupexpr = "/\\*unsup ";
		}
		
		// 1er pasada, separo en bloques de selectSingleNode
		List<String> tokens = new ArrayList<String>();
		Scanner s = new Scanner(line);
		s.useDelimiter(Pattern.quote(delimiter)); 
		while (s.hasNext()) {
			String t = s.next();
			tokens.add(t);
		}
		
		if ( tokens.size() == 1) {
			// No encontr� nada, devuelvo la linea sin modificarla
			return line;
		}
		
		StringBuffer sb = new StringBuffer(line.length());
		boolean first = true;
		for (int i = 0; i < tokens.size(); i++) {
			String token = (String)tokens.get(i);
			token = token.replaceFirst(unsupexpr, "");
			if (!first) {
				// Si ya pas� el mensaje, tengo que sacar el */ del token.
				// Solamente tomo el primero por si hay otros /* */ a continuaci�n
				token = token.replaceFirst(" \\*/", "");
			}
			sb.append(token);
			if (!(i == tokens.size() - 1)) {
				sb.append(delimiter);
			}
			first = false;
		}
		return sb.toString();
	}

	public String filter(String input) throws FilterException {
		String buffer;
		StringBuffer retorno = new StringBuffer();
		if (input != null && input.indexOf(delimiter) != -1) {
			buffer = input;
			String[] lineas = buffer.split("\n");
			for (String linea : lineas) {
				retorno.append(removeUnsup(linea, delimiter, unsupPrefix));
			}
		}
		return retorno.toString();
	}
	
	
//	public String tratamientoNullUnsup(String origen) {
//		StringBuffer rtado = null;
//		if (origen.indexOf("null /*unsup ") != -1) {
//			int posiNull = origen.indexOf("null /*unsup ");
//			int posiBarra = origen.indexOf("*/", posiNull);
//			// Me quedo con la primer parte hasta el null
//			rtado = new StringBuffer(origen.substring(0, posiNull));
//			// Me quedo con todo el contenido entre el null y el */
//			rtado.append(origen.substring(posiNull + 12, posiBarra - 1));
//			// Agrego el contenido final.
//			rtado.append(origen.substring(posiBarra + 2, origen.length()));
//		}
//		
//		if (origen.indexOf("null (*unsup ") != -1) {
//			int posiNull = origen.indexOf("null (*unsup ");
//			int posiBarra = origen.indexOf("*)", posiNull);
//			// Me quedo con la primer parte hasta el null
//			rtado = new StringBuffer(origen.substring(0, posiNull));
//			// Me quedo con todo el contenido entre el null y el */
//			rtado.append(origen.substring(posiNull + 12, posiBarra - 1));
//			// Agrego el contenido final.
//			rtado.append(origen.substring(posiBarra + 2, origen.length()));
//		}
//		
//		return rtado.toString();
//	} 
	
}
