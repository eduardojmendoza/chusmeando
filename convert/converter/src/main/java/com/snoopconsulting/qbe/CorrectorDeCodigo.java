package com.snoopconsulting.qbe;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class CorrectorDeCodigo {

	private static final String CTE_PAQUETE = "package com.qbe.services.mqgeneric.impl;";
	
	private boolean esDeclaracion = false;
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
//		File claseIn = new File( "/home/leandrocohn/workspace/PruebasIniciales/src/com/snoopconsulting/qbe/lbaw_MQMensaje.java");
//		File claseOut = new File( "/home/leandrocohn/workspace/PruebasIniciales/src/com/snoopconsulting/qbe/Prueba_Salida.java");
		File claseIn = new File( "/home/leandrocohn/Proyectos/Complus/gitrepo/migracioncomplus/dev/CMS-ovmq/src/main/java/com/qbe/services/ovmq/impl/lbaw_OVAnulxPagoDetalles.java");
		File claseOut = new File( "/home/leandrocohn/Proyectos/Complus/gitrepo/migracioncomplus/dev/CMS-ovmq/src/main/java/com/qbe/services/ovmq/impl/Prueba_Salida.java");
		BufferedReader file   = new BufferedReader(new FileReader(claseIn));
		BufferedWriter out = new BufferedWriter(new FileWriter(claseOut));
		
		CorrectorDeCodigo correc = new CorrectorDeCodigo();
		if  (file.ready()) {
		
			out = correc.agregarPackageImports(out);
			while (file.ready()) {
			String line = file.readLine();
			out.write(correc.corregirCodigo(line));
			out.write('\n');
			}
		}
		out.flush();
		out.close();
		file.close();
		String filename = claseIn.getName();
		boolean ok1 = claseIn.renameTo(new File( "/home/leandrocohn/Proyectos/Complus/gitrepo/migracioncomplus/dev/CMS-ovmq/src/main/java/com/qbe/services/ovmq/impl/" + filename + ".bak"));
		boolean ok2 = claseOut.renameTo(new File("/home/leandrocohn/Proyectos/Complus/gitrepo/migracioncomplus/dev/CMS-ovmq/src/main/java/com/qbe/services/ovmq/impl/" + filename));
//		boolean ok1 = claseIn.renameTo(new File( "/home/leandrocohn/workspace/PruebasIniciales/src/com/snoopconsulting/qbe/" + filename + ".bak"));
//		boolean ok2 = claseOut.renameTo(new File("/home/leandrocohn/workspace/PruebasIniciales/src/com/snoopconsulting/qbe/" + filename));

		System.out.println(ok1 +" " + ok2);
	}
	

	private BufferedWriter agregarPackageImports(BufferedWriter out) throws IOException {
		out.write(CTE_PAQUETE);
		out.write('\n');
		out.write('\n');
		out.write("import com.qbe.lbawA_OVMQCotizar.HSBCInterfaces_IAction;");
		out.write('\n');
		out.write("import java.io.File;");
		out.write('\n');
		out.write("	import com.qbe.lbawA_OVMQCotizar.ObjectControl;");
		out.write('\n');
		out.write("import com.qbe.lbawA_OVMQCotizar.ModGeneral;");
		out.write('\n');
		out.write("import com.snoopconsulting.qbe.VBObjectClass;");
		out.write('\n');
		out.write("import com.qbe.HSBC.EventLog;");
		out.write('\n');
		out.write("import com.snoopconsulting.qbe.XmlDomExtended;");
		out.write('\n');
		out.write("import org.w3c.dom.Node;");
		out.write('\n');
		return out;
	}


	public String corregirCodigo (String origen) {
		System.out.println("Corregir Código Inicio: " + origen);
		StringBuffer salida = new StringBuffer();
		
		if (origen.indexOf("public class") != -1) {
			origen =  arreglarClase(origen);
		}
		
		//Busca la declaración de los atributos de la clase.
		if (origen.indexOf("* Implementacion de los objetos") != -1) {
			esDeclaracion = true;
		}
		
		if (esDeclaracion && origen.indexOf("*/") != -1) {
			esDeclaracion = true;
			origen = agregarAtributosDeClase(origen);
		}
			
		if (origen.indexOf("private Object mobjEventLog") != -1) {
			origen = modificarLogers(origen);
		}
		if (origen.indexOf("diamondedge.util.XmlDom") != -1) {
			origen = modificarXmlDom(origen).toString();
		}
		
		if (origen.indexOf("invoke") != -1) {
			origen = quitarInvoke(origen).toString();
		}
		
		if (origen.indexOf("Object wobjFrame2MQ") != -1) {
			origen = origen.replace("Object wobjFrame2MQ", "MQConnectionConnector wobjFrame2MQ");
		}
		
		if (origen.indexOf("new ModGeneral.gcteClassMQConnection();") != -1) {
			origen = origen.replace("new ModGeneral.gcteClassMQConnection();", "new MQConnectionConnector();");
		}

		if (origen.indexOf("//unsup: wvarMQError") != -1) {
			origen = removeUnsupComents(origen);
		}
		
		if (origen.indexOf("removeChild") != -1) {
			origen = removeUnsup(origen, "removeChild", "null ");
			origen = removeUnsup(origen, "selectSingleNode(", null);
		}
		
		if (origen.indexOf("appendChild") != -1) {
			origen = removeUnsup(origen, "appendChild", "null ");
			origen = removeUnsup(origen, "selectSingleNode(", null);
		}
		
		if (origen.indexOf("null /*unsup") != -1 && origen.indexOf("selectSingleNode") != -1) {
			origen = removeUnsup(origen, "selectSingleNode", "null ");
		}
		
		if (origen.indexOf("selectSingleNode") != -1) {
			origen = removeUnsup(origen, "selectSingleNode", null);
		}
		
		if (origen.indexOf("transformNode") != -1) {
			if (origen.indexOf("new Variant() /*unsup") != -1) {
				origen = removeUnsup(origen, "transformNode", "new Variant() ");
			} else {
				origen = removeUnsup(origen, "transformNode", null);
			}
		}
		
		if (origen.indexOf("selectNodes") != -1) {
			origen = removeUnsup(origen, "selectNodes", "null ");
		}
		
		if (origen.indexOf("null /*unsup") != -1 || origen.indexOf("null (*unsup") != -1) {
			origen = tratamientoNullUnsup(origen);
		}
		
		if (origen.indexOf("GetRequestRetornado") != -1) {
			origen =  tratarRequestRetornado(origen);
		}
		
		if (origen.indexOf(".load( System.getProperty(\"user.dir\")") != -1) {
			origen = origen
					.replace("\"\\\\\"", "File.separator")
					.replace(";", ".getFile());")
					.replace("System.getProperty(\"user.dir\")",
							"Thread.currentThread().getContextClassLoader().getResource(ROOT_DEF_DIR");
		}
		
		//Verifica que sea una linea compuesta por Variants pero no una simple instanciación del Variant.
		if (origen.indexOf("new Variant()") == -1 && origen.indexOf("Variant") != -1) {
			if (origen.indexOf("isNumeric()") != -1) {
				//Tratamiento si es numeric.
				origen = origen.replace(" new Variant(", "").replace(").isNumeric()", ".isNumeric()");
			} else {
				//Tratamiento gral.
				origen = removeNewVariants(origen).toString();
			}
		}
		
		
		//Casos Especiales:
		if (origen.indexOf("org.w3c.dom.Node pobjNodo") != -1) {
			origen = origen.replace("org.w3c.dom.Node pobjNodo",
					"XmlDomExtended pobjNodo");
		}
		
		if (origen.indexOf("GetVectorDatos(wobjXMLRequest.getDocument().getChildNodes().item( 0 )") != -1) {
			origen = origen.replace("GetVectorDatos(wobjXMLRequest.getDocument().getChildNodes().item( 0 )",
					"GetVectorDatos(wobjXMLRequest");
		}
		
		if (origen.indexOf("ParseoMensaje( int pvarPos, String pstrParseString, XmlDomExtended pobjNodoDefiniciones, XmlDomExtended") != -1) {
			origen = origen.replace("ParseoMensaje( int pvarPos, String pstrParseString, XmlDomExtended",
					"ParseoMensaje( int pvarPos, String pstrParseString, Node");
		}
		
		salida.append(origen);
		
		System.out.println("Corregir Código Fin: " + salida);
		return salida.toString();
	}
	
	private String agregarAtributosDeClase(String origen) {
		
		StringBuffer salida = new StringBuffer(origen);
		salida.append("\n").append("private static final String ROOT_DEF_DIR = \"DefinicionesXML\";");
		return salida.toString();
	}


	private String tratarRequestRetornado(String origen) {
		String salida = origen.replace("pobjNodoDefiniciones.selectSingleNode(", "XmlDomExtended.Node_selectSingleNode( pobjNodoDefiniciones,");
		return salida;
	}


	private String modificarLogers(String origen) {
		return origen.replaceAll("private Object mobjEventLog", "private EventLog mobjEventLog");
	}


	public String tratamientoNullUnsup(String origen) {
		StringBuffer rtado = null;
		if (origen.indexOf("null /*unsup ") != -1) {
			int posiNull = origen.indexOf("null /*unsup ");
			int posiBarra = origen.indexOf("*/", posiNull);
			// Me quedo con la primer parte hasta el null
			rtado = new StringBuffer(origen.substring(0, posiNull));
			// Me quedo con todo el contenido entre el null y el */
			rtado.append(origen.substring(posiNull + 12, posiBarra - 1));
			// Agrego el contenido final.
			rtado.append(origen.substring(posiBarra + 2, origen.length()));
		}
		
		if (origen.indexOf("null (*unsup ") != -1) {
			int posiNull = origen.indexOf("null (*unsup ");
			int posiBarra = origen.indexOf("*)", posiNull);
			// Me quedo con la primer parte hasta el null
			rtado = new StringBuffer(origen.substring(0, posiNull));
			// Me quedo con todo el contenido entre el null y el */
			rtado.append(origen.substring(posiNull + 12, posiBarra - 1));
			// Agrego el contenido final.
			rtado.append(origen.substring(posiBarra + 2, origen.length()));
		}
		
		return rtado.toString();
	}

	public String tratamientoUnsup(String origen) {
		int posiUnsup = origen.indexOf("/*unsup ");
		int posiBarra = origen.indexOf("*/", posiUnsup);
		//Me quedo con la primer parte hasta el posiUnsup
		StringBuffer rtado = new StringBuffer(origen.substring(0, posiUnsup));
		//Me quedo con todo el contenido entre el posiUnsup y el */
		rtado.append(origen.substring(posiUnsup + 8, posiBarra-1));
		//Agrego el contenido final.
		rtado.append(origen.substring(posiBarra + 2, origen.length()));
		return rtado.toString();
	}
	
	public StringBuffer quitarInvoke (String origen) {
		StringBuffer salida = new StringBuffer(origen.replaceFirst("invoke\\( \"(.+)\", new Variant\\[\\] \\{ (.+)\\}", "$1($2"));
		return salida;
	}
	
	private String removeUnsupComents(String origen) {
		origen = origen.replace("//unsup: ", "");
		origen.replace("//unsup: ", "");
		int abrir = origen.indexOf("(");
		int cerrar = origen.indexOf(")");
		String argumento = origen.substring(abrir + 1, cerrar);
		String[] argu = argumento.split(",");
		StringBuffer salida = new StringBuffer(origen.substring(0, abrir + 1));
		salida.append("\""+argu[0]+"\", ");
		salida.append("new StringHolder("+argu[1]+"), ");
		salida.append("\""+argu[2]+"\");");
		return salida.toString();
	}
	
	private static String modificarXmlDom (String origen) {
		return origen.replaceAll("diamondedge.util.XmlDom", "XmlDomExtended");
	}
	
	private String arreglarClase (String origen) {
		return origen.replaceAll("implements Variant, (.+)\\.(.+)", "extends VBObjectClass implements $1_$2");
	}
	
	public static int countOccurrences(String find, String string) {
		int count = 0;
		int indexOf = 0;

		while (indexOf > -1) {
			indexOf = string.indexOf(find, indexOf + 1);
			if (indexOf > -1)
				count++;
		}

		return count;
	}
	
	/**
	 * Limpia la line que recibe como par�metro de expresiones "unsup" que genera el conversor de VB a Java
	 * Borra el \/\* unsup inicial y el \*\/ final para cada par que encuentra
	 * Si hay un unsupPrefix tambi�n lo elimina
	 * 
	 * @param line
	 * @param delimiter
	 * @param unsupPrefix prefijo del unsup. Se toma tal cual, o sea es un String NO una regexp
	 * @return
	 */
	public String removeUnsup(String line, final String delimiter, final String unsupPrefix) {

		final String unsupexpr;
		if (unsupPrefix != null) {
			unsupexpr = Pattern.quote(unsupPrefix) + "/\\*unsup ";
		} else {
			unsupexpr = "/\\*unsup ";
		}
		
		// 1er pasada, separo en bloques de selectSingleNode
		List<String> tokens = new ArrayList<String>();
		Scanner s = new Scanner(line);
		s.useDelimiter(Pattern.quote(delimiter)); 
		while (s.hasNext()) {
			String t = s.next();
			System.out.println(t);
			tokens.add(t);
		}
		
		if ( tokens.size() == 1) {
			// No encontr� nada, devuelvo la linea sin modificarla
			return line;
		}
		
		StringBuffer sb = new StringBuffer(line.length());
		boolean first = true;
		for (int i = 0; i < tokens.size(); i++) {
			String token = (String)tokens.get(i);
			token = token.replaceFirst(unsupexpr, "");
			if (!first) {
				// Si ya pas� el mensaje, tengo que sacar el */ del token.
				// Solamente tomo el primero por si hay otros /* */ a continuaci�n
				token = token.replaceFirst(" \\*/", "");
			}
			sb.append(token);
			if (!(i == tokens.size() - 1)) {
				sb.append(delimiter);
			}
			first = false;
		}
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	public StringBuffer removeNewVariants(String original) {
		String[] argumentos = original.split("new Variant\\(");
		StringBuffer salida = new StringBuffer();
		boolean primeraVez = true;
		int sacarParen = 1;
		String divisor = ",";
		for (int numArgumen = 0; numArgumen < argumentos.length; numArgumen++) {
			//Se itera por todas las partes que tenian new Variant. 

			if(numArgumen == argumentos.length - 1) {
				//Es la última porción de código
				//Como termina, se deberá terminar con un punto y coma
				divisor = ";";
			}
			
			if (primeraVez) {
				//La primera parte se agrega tal cual llega.
				salida.append(argumentos[numArgumen]);
				primeraVez = false;
			} else {
				//Es la parte del medio o del final
				if (argumentos[numArgumen].trim().endsWith("),") || argumentos[numArgumen].trim().endsWith(");") || numArgumen == argumentos.length - 1) {
					//Cierra un argumento
					String arguTemp = argumentos[numArgumen];
						//saca la cantidad de paréntesis definidas en la variable: sacarParen.
						for (int cantParen = 0; cantParen < sacarParen; cantParen++) {
							int ultimoParentesis = arguTemp.lastIndexOf(")");
							arguTemp = arguTemp.substring(0, 	ultimoParentesis);
						}
						salida.append(arguTemp + divisor);
						sacarParen = 1;
				} else {
					//Esta linea no cierra paréntesis, por lo tanto se agrega la línea y se anota un parentesis para sacar.
					salida.append(argumentos[numArgumen]);
					sacarParen ++; 
				}
			}
			System.out.println("Argu "+ numArgumen + ": " + salida.toString());
			
		}
		
		return salida;
	}
	
}
