package com.qbe.services.utils.filter;


/**
 * Implementa un filter usando String.replaceAll()
 * 
 * @author ramiro
 *
 */
public class RegExFilter implements Filter {

	protected RegExPair rpair;
	
	public RegExFilter(String matcher, String replacement) {
		rpair = new RegExPair(matcher, replacement);
	}

	public String filter(String input) throws FilterException {
		
		return input.replaceAll(rpair.getMatcher(), rpair.getReplacement());
	}

}
