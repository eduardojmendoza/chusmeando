package com.qbe.services.utils.filter;

import java.util.List;


/**
 * Clase encargada de agregar imports inexistentes
 * @author leandrocohn
 */
public class AddImportsFilter implements Filter {
	private List<String> imports;
	private String paquete;
	
	/**
	 * Constructor donde se setearán los imports a agregar.
	 * @param imports
	 */
	public AddImportsFilter(String paquete, List<String> imports) {
		this.imports = imports;
		this.paquete = paquete;
	}
	
	/* (non-Javadoc)
	 * @see com.qbe.services.utils.Filter#filter(java.lang.String)
	 */
	public String filter(String input) throws FilterException {
		String buffer = input;
		int ubicacionPaquete = buffer.indexOf("package");
		if (paquete != null) {
			if (ubicacionPaquete == -1) {
				//Si no tiene paquete previo
				buffer = paquete + "\n" + buffer;
			} else {
				//Tiene paquete
				buffer = buffer.replace("package com.qbe.services;", paquete);
			}
		}
		if  (buffer != null && imports != null && imports.size() > 0) {
			
			for (String importacion : imports) {
				if (buffer.indexOf(importacion) == -1) {
					//El import no se encuentra, por lo tanto se agrega
						buffer = buffer.replaceAll("package (.*);", "package $1;\n" + importacion);
				}
			}
		}
		return buffer;
	}

}
