package com.qbe.services.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.qbe.services.utils.filter.AddImportsFilter;
import com.qbe.services.utils.filter.Filter;
import com.qbe.services.utils.filter.FilterEngine;
import com.qbe.services.utils.filter.FilterException;
import com.qbe.services.utils.filter.RegExChainFilter;
import com.qbe.services.utils.filter.RegExFilter;

/**
 * Arregla los resultados de la conversión de las clases de un package.
 * 
 * 
 * @author ramiro
 *
 */
public class ConvertsFixer {
	
//	private static final String SOURCES_DIR = "../../dev/CMS-nbwsA_Transacciones/src/main/java/com/qbe/services/transacciones/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-OVMQUsuario/src/main/java/com/qbe/services/ovmqusuario/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-LBAVIRTUAL/src/main/java/com/qbe/services/ofvirtuallba/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-OVMQEmision/src/main/java/com/qbe/services/ovmqemision/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-InterWSBrok/src/main/java/com/qbe/services/interWSBrok/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-OfVirtualLBA/src/main/java/com/qbe/services/ofVirtualLBA/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-OVMQRiesgos/src/main/java/com/qbe/services/ovmqriesgos/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-LBAWEBMQ/src/main/java/com/qbe/services/webmq/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-OVMQCotizar/src/main/java/com/qbe/services/ovmqcotizar/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-Productor/src/main/java/com/qbe/services/productor/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-LBAVirtualMQEmision/src/main/java/com/qbe/services/lBAVirtualMQEmision/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-lbawA_OVSQLCotizar/src/main/java/com/qbe/services/lbawA_OVSQLCotizar/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-LBAA_MWGenerico/src/main/java/com/qbe/services/mwGenerico/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-SegurosOnline/src/main/java/com/qbe/services/segurosOnline/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-Siniestros/src/main/java/com/qbe/services/siniestros/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-tests/src/main/java/com/qbe/services/converts/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-OVMQEmision/prjorig/LBAVirtualMQEmision/Project/src/com/qbe/services";
//	private static final String SOURCES_DIR = "/tmp/mongo";
//	private static final String SOURCES_DIR = "../../dev/CMS-camOficinaVirtual/src/main/java/com/qbe/services/cam30";
//	private static final String SOURCES_DIR = "../../dev/CMS-SegurosOnline/prjorig/NBWS_SegurosOnline/Project/src/com/qbe/services";
//	private static final String SOURCES_DIR = "../../dev/CMS-LBAHOGARMQ/src/main/java/com/qbe/services/lbahogarmq/impl";
//	private static final String SOURCES_DIR = "../../dev/CMS-COLDVIEW/src/main/java/com/qbe/services/coldview/estructurado";
//	private static final String SOURCES_DIR = "../../dev/CMS-LBAVIRTUALMQIII/src/main/java/com/qbe/services/lbavirtualmqiii";
//  private static final String SOURCES_DIR = "../../dev/CMS-ecoupons/src/main/java/com/qbe/services/ecoupons/impl";
//  private static final String SOURCES_DIR = "../../dev/CMS-SegurosOnline/src/main/java/com/qbe/services/segurosOnline/impl";
	private static final String SOURCES_DIR = "../../dev/CMS-InterWSBrok/src/main/java/com/qbe/services/interWSBrok/impl";
	
	/**
	 * package de las clases
	 */
//	private static final String CTE_PACKAGE = "package com.qbe.services.interWSBrok.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ovmqusuario.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ofvirtuallba.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ovmqemision.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ofVirtualLBA.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ovmqriesgos.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.webmq.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ovmqcotizar.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.productor.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.lBAVirtualMQEmision.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.lbawA_OVSQLCotizar.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.mwGenerico.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.segurosOnline.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.siniestros.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ovmqemision.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.converts.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.camOficinaVirtual.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.segurosOnline.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.lbahogarmq.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.coldview.estructurado;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.lbavirtualmqiii;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.ecoupons.impl;";
//	private static final String CTE_PACKAGE = "package com.qbe.services.segurosOnline.impl;";
	private static final String CTE_PACKAGE = "package com.qbe.services.interWSBrok.impl;";

	private String sourcesDir = SOURCES_DIR;
	
	private String packageName = CTE_PACKAGE;
	
	/**
	 * @param args
	 * @throws FilterException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, FilterException {
		ConvertsFixer fixer = new ConvertsFixer();
		fixer.processFiles();
	}

	public ConvertsFixer() {
	}
	
	public ConvertsFixer(String sourcesDir, String packageName) {
		this.sourcesDir = sourcesDir;
		this.packageName = packageName;
	}
	
	
	/**
	 * Ejecuta los Filters que arreglan lo generado por el conversor de VB a Java.
	 * 
	 * 
	 * @throws IOException
	 * @throws FilterException
	 */
	private void processFiles() throws IOException, FilterException {
		
		//OJO No modificar el orden de los filters sin revisar las dependencias
		FilterEngine engine = new FilterEngine();
		engine.addFilter(getDeclaracionClaseFilter());
		engine.addFilter(getFixDeclaracionMetodo());
		engine.addFilter(getFixMQAccess());
		engine.addFilter(getFixXmlDom());
		engine.addFilter(getFixXmlMethod());
		engine.addFilter(getFixLog());
		engine.addFilter(getRemovesCOMMethodsFilter());
		engine.addFilter(getUnsupSelectNodeFilter());
		engine.addFilter(getRemoveHSBCLog());
		engine.addFilter(getFixGoTos());
		engine.addFilter(getFixLoadFile());
		engine.addFilter(getFixInvokes());
		engine.addFilter(getDBConnectionFilter());
		engine.addFilter(getFormatNumberXSLFilter());
		engine.addFilter(getFixTimeOut());
//		engine.addFilter(getFixOpenDB());
		engine.addFilter(getRemoveADO());
		
		//Agrego los imports
		List<String> imports = new ArrayList<String>();
		imports.add(getPackageName().replace("package", "import").replace(";", ".ModGeneral;"));
		imports.add("import com.qbe.vbcompat.framework.VBObjectClass;");
		imports.add("import java.io.File;");
		imports.add("import com.qbe.vbcompat.string.StringHolder;");
		imports.add("import com.qbe.services.mqgeneric.impl.ErrorConstants;");
		imports.add("import com.qbe.services.mqgeneric.impl.EventLog;");
		imports.add("import com.qbe.vbcompat.xml.XmlDomExtended;");
		imports.add("import com.qbe.connector.mq.MQProxy;");
		imports.add("import com.qbe.connector.mq.MQProxyTimeoutException;");
		imports.add("import com.qbe.connector.mq.MQProxyException;");
		imports.add("import com.qbe.vbcompat.regexp.VbScript_RegExp;");
		imports.add("import com.qbe.vbcompat.regexp.MatchCollection;");
		imports.add("import com.qbe.vbcompat.regexp.Match;");
		imports.add("import com.qbe.services.db.JDBCConnectionFactory;");
		imports.add("import com.qbe.services.db.AdoUtils;");

		engine.addFilter(getImports(getPackageName() , imports));
		engine.addFilter(getMigratedFilters());
		engine.addFilter(getFixUTF());
		
		File[] javas = new File(getSourcesDir()).listFiles();
		for (File file : javas) {
			if ( file.isFile()) {
				engine.applyFiltersToFile(file);
				System.out.println("Procesando" + file.getAbsolutePath());
			}
		}
	}

	/**
	 * Cuando los componentes procesan un XSL generan lineas ?xml que intenta remover, pero buscando un UTF-16, que es el default
	 * en MSXML, pero en Java al aplicar un XSL genera UTF-8
	 * 
	 * Ej:
	 * response = <Response><Estado resultado='true' mensaje='' /><?xml version="1.0" encoding="UTF-8"?><REGS><REG><ENDOSO>000004</ENDOSO><RECNUM>651950118</RECNUM><SIGIMP/><IMP>30,00</IMP><COB><![CDATA[EFEC]]></COB><FECVTO><![CDATA[18/12/2012]]></FECVTO><SIGDIA/><DIAS>118</DIAS><RECHA/><MON><![CDATA[$]]
	 * 
	 * El de microsoft usa UTF-16: http://www.tek-tips.com/viewthread.cfm?qid=1379722
	 * Para resolverlo, cambio el "parche" que tienen los componentes para que en vez de UTF-16 tome UTF-?, y que agarre los dos ( no sé si alguna vez un XSL no generará UTF-16 )
	 * 
	 * Depende del getFixInvokes 
	 * 
	 * @return
	 */
	protected static Filter getFixUTF() {
		//         wvarResult = Strings.replace( wobjXMLResponse.transformNode( wobjXSLResponse ).toString(), "<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "" );
		RegExChainFilter filters = new RegExChainFilter();
		filters.addRegExPair("Strings.replace\\( (\\w+)\\.transformNode\\( (\\w+) \\)\\.toString\\(\\), \"<\\?xml version=\\\\\"1\\.0\\\\\" encoding=\\\\\"UTF-16\\\\\"\\?>\", \"\" \\)",
				"$1.transformNode( $2 ).toString().replaceAll( \"<\\\\\\\\?xml version=\\\\\"1\\\\\\\\.0\\\\\" encoding=\\\\\"UTF-\\\\\\\\d+\\\\\"\\\\\\\\?>\", \"\" )");
		return filters;
	}

	/**
	 * Este mensaje crea filters que corrigen los invoke()
	 * 
	 * Hay dos issues con esto:
	 * 
	 * 1) FALTA hacerlo con expresiones regulares no es genérico porque debemos cubrir las variantes 1 por 1 básicamente. El problema radica en que 
	 * las expresiones posibles no son regulares, puede haber Variant()s anidados. Para resolver esto deberíamos parsear el código y analizar por estructura
	 * 2) FALTA resolver los warnings que marca el comentario del vbc. Cuando pasa como parámetro un new Variant(wvarpos + 1), en el método no puede actualizar 
	 * el wvarpos, esto se pierde. Falta cambiarlo por otro bloque que pase una variable, previamente inicializada. Ver los casos.
	 * 	Ver qué ocurre en VB si le paso var + algo!!
	 * 
	 * @see getFixMQAccess()
	 * @return
	 */
	protected static Filter getFixInvokes() {

//      wvarStatus = invoke( "CorrerMensaje", new Variant[] { new Variant(wvarParametros), new Variant(wvarContinuar) } );
//Al pedo lo del variant porque no modifica los parámetros. Por suerte en general no hacen eso:
//
//  public boolean CorrerMensaje( String pvarParametros, String pvarContinuar ) throws Exception
//
		RegExChainFilter filters = new RegExChainFilter();
		filters.addRegExPair("wvarStatus = invoke\\( \"CorrerMensaje\", new Variant\\[\\] \\{ new Variant\\(wvarParametros\\), new Variant\\(wvarContinuar\\) \\} \\);",
				"wvarStatus = this.CorrerMensaje(wvarParametros,wvarContinuar);");
		filters.addRegExPair("wvarResult = wvarResult \\+ invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(wvarPos\\), new Variant\\(strParseString\\), new Variant\\(wvarstrLen\\) \\} \\);",
				"wvarResult = wvarResult + ParseoMensaje(wvarPos, strParseString, wvarstrLen);");
		filters.addRegExPair("(\\w+)\\.loadXML\\( invoke\\( \"(\\w+)\", new Variant\\[\\] \\{\\} \\) \\);",
				"$1.loadXML( $2());");
		
		//Uno de los invokes se complica porque para el wvarPos como parámetro ref... que engendro
//        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 61 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen) } );
//        wvarResultInt = invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 308 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString) } );
//        wvarResultPagos = invoke( "ParseoMensajePagos", new Variant[] { new Variant(new Variant( wvarPos + 179 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen) } );

		//private String CargarVectorMasivos( String strParseString, int wvarstrLen, Variant wvarPos ) throws Exception



		// En este caso hay que efectivamente pasarla como referencia, porque el método lo modifica y se usan los valores actualizados después
//		En lbaw_OVLOReimpLiqui
//        wvarResultInt = invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 308 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString) } );
//llama a:
//  	private String ParseoMensaje( Variant wvarPos, String strParseString ) throws Exception
//convertir a:
//        wvarResultInt = this.ParseoMensaje(new Variant( wvarPos + 308 ),strParseString);
		filters.addRegExPair("(\\w+) = invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(new Variant\\( wvarPos \\+ (\\d+) \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\) \\} \\);",
				"$1 = this.ParseoMensaje(new Variant( wvarPos + $2 ),strParseString);");

// Similar al anterior, misma clase
//      wvarResultPagos = invoke( "ParseoMensajePagos", new Variant[] { new Variant(new Variant( wvarPos + 179 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen) } );
// Para método   private String ParseoMensajePagos( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception

//	convierte a
//      wvarResultPagos = ParseoMensajePagos(new Variant( wvarPos + 179 ), strParseString, wvarstrLen);


		filters.addRegExPair("(\\w+) = invoke\\( \"ParseoMensajePagos\", new Variant\\[\\] \\{ new Variant\\(new Variant\\( wvarPos \\+ (\\d+) \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\), new Variant\\(wvarstrLen\\) \\} \\);",
				"$1 = ParseoMensajePagos(new Variant( wvarPos + $2 ), strParseString, wvarstrLen);");
		
//	En OVPerfilUsuario
//  wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 61 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen) } );
//	
//	private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen ) throws Exception
// convierte a
//	wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 61 ), strParseString,wvarstrLen);
		filters.addRegExPair("(\\w+) = (\\w+) \\+ invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(new Variant\\( wvarPos \\+ (\\d+) \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\), new Variant\\(wvarstrLen\\) \\} \\);",
				"$1 = $2 + ParseoMensaje(new Variant( wvarPos + $3 ), strParseString, wvarstrLen);");
		
//		En OVLOGetNroLiqui la variante x = x + invoke con 2 parámetros, como no tiene warning en realidad no va variant sino la variable directamente
		
		filters.addRegExPair("(\\w+) = (\\w+) \\+ invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(wvarPos\\), new Variant\\(strParseString\\) \\} \\);",
				"$1 = $2 + ParseoMensaje(wvarPos, strParseString);");
		
// En OVCoberturasDet
//  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen, String wvarFlag ) throws Exception

//		filters.addRegExPair("(\\w+) = (\\w+) \\+ ParseoMensaje\\(new Variant\\( wvarPos \\+ (\\d+) \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\), new Variant\\(wvarstrLen\\), new Variant\\(Strings\\.mid\\( new Variant\\(strParseString,\\(wvarPos + (\\d+)\\),1\\) \\)\\);",
//				"$1 = $2 + ParseoMensaje(new Variant( wvarPos + $3 ), strParseString, wvarstrLen, Strings.mid( new Variant(strParseString,(wvarPos + 125),1) );");

		
// OVLOGetCantRecibos
//        wvarEstado = wvarEstado + invoke( "CargarVectorMasivos", new Variant[] { new Variant(strParseString), new Variant(wvarstrLen), new Variant(new Variant( wvarPos + 11619 ))/*warning: ByRef value change will be lost.*/ } );
//  private String CargarVectorMasivos( String strParseString, int wvarstrLen, Variant wvarPos ) throws Exception
		filters.addRegExPair("(\\w+) = (\\w+) \\+ invoke\\( \"CargarVectorMasivos\", new Variant\\[\\] \\{ new Variant\\(strParseString\\), new Variant\\(wvarstrLen\\), new Variant\\(new Variant\\( wvarPos \\+ (\\d+) \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/ \\} \\);",
				"$1 = $2 + CargarVectorMasivos(strParseString, wvarstrLen, new Variant( wvarPos + $3 ));");
// OVLOGetCantRecibos
		
//        wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 19 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarCantLin) } );
//  private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarCantLin ) throws Exception
		filters.addRegExPair("wvarResult = wvarResult \\+ invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(new Variant\\( wvarPos \\+ 19 \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\), new Variant\\(wvarCantLin\\) \\} \\);",
				"wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 19 ), strParseString, wvarCantLin);");
		
		
		// OVLOGetRecibos
		//wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 15 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString) } );
		//  private String ParseoMensaje( Variant wvarPos, String strParseString ) throws Exception

		filters.addRegExPair("wvarResult = wvarResult \\+ invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(new Variant\\( wvarPos \\+ 15 \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\) \\} \\);", 
				"wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 15 ), strParseString);");
        
		// OVCoberturasDetalle
		//         wvarResult = wvarResult + invoke( "ParseoMensaje", new Variant[] { new Variant(new Variant( wvarPos + 126 ))/*warning: ByRef value change will be lost.*/, new Variant(strParseString), new Variant(wvarstrLen), new Variant(Strings.mid( new Variant(strParseString), new Variant((wvarPos + 125)), new Variant(1) )) } );
		//   private String ParseoMensaje( Variant wvarPos, String strParseString, int wvarstrLen, String wvarFlag ) throws Exception
		filters.addRegExPair("wvarResult = wvarResult \\+ invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\(new Variant\\( wvarPos \\+ 126 \\)\\)\\/\\*warning: ByRef value change will be lost\\.\\*\\/, new Variant\\(strParseString\\), new Variant\\(wvarstrLen\\), new Variant\\(Strings\\.mid\\( new Variant\\(strParseString\\), new Variant\\(\\(wvarPos \\+ 125\\)\\), new Variant\\(1\\) \\)\\) \\} \\);",
				"wvarResult = wvarResult + ParseoMensaje(new Variant( wvarPos + 126 ), strParseString, wvarstrLen, Strings.mid( strParseString, wvarPos + 125, 1 ) );");
		
		//GetDatoFormateado
		//	wvarArea = wvarArea + invoke( "GetDatoFormateado", new Variant[] { new Variant(wvarNodop), new Variant(mcteParam_FUNCION), new Variant(0), new Variant(1), new Variant(0) } );
		//	private String GetDatoFormateado( org.w3c.dom.Node pobjXMLContenedor, String pvarPathDato, int pvarTipoDato, int pvarLongitud, int pvarDecimales ) throws Exception
		filters.addRegExPair("(\\w+) \\+ invoke\\( \"GetDatoFormateado\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\) \\} \\);",
				"$1 + GetDatoFormateado( $2, $3, $4, $5, $6);");

		//GetVectorDatos
		//wvarArea = wvarArea + invoke( "GetVectorDatos", new Variant[] { new Variant(wvarNodop), new Variant(mcteParam_CONDUCTORES), new Variant(mcteParam_CONDUSEC + ",1,2,0|" + mcteParam_CONDUTDO + ",1,2,0|" + mcteParam_DOCUMDAT + ",0,11,0|" + mcteParam_CONDUAPE + ",0,20,0|" + mcteParam_CONDUNOM + ",0,30,0|" + mcteParam_CONDUVIN + ",0,2,0|" + mcteParam_CONDUFEC + ",3,8,0|" + mcteParam_CONDUSEX + ",0,1,0|" + mcteParam_CONDUEST + ",0,1,0|" + mcteParam_CONDUEXC + ",0,1,0"), new Variant(10) } );
		//		private String GetVectorDatos( org.w3c.dom.Node pobjNodo, String pVarPathVector, String pVarDefinicionSubNodos, int pVarCantElementos ) throws Exception
//		filters.addRegExPair("(\\w+) \\+ invoke\\( \"GetVectorDatos\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\(((\\w+) \\+ \"(,(\\d+))*\\|\" \\+ )*(\\w+) \\+ \"(,(\\d+))*\"\\), new Variant\\((\\w+)\\) \\} \\);",
//				"$1 + GetVectorDatos($2, $3,  $4 $5 $6 $7, $8);");
		
		//AddCoberturas
		//invoke( "AddCoberturas", new Variant[] { new Variant(wobjXMLRequest) } )
		filters.addRegExPair("invoke\\( \"AddCoberturas\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\) \\} \\)",
				"AddCoberturas($1)");
		
		//ParseoMensaje
//		invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(wvarParseString), new Variant(wvarStringLen), new Variant(wvarEstado) } );
		filters.addRegExPair("invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\) \\} \\);",
						"ParseoMensaje($1, $2, $3, $4);");
		//invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(wvarParseString), new Variant(wvarStringLen) } );
		filters.addRegExPair("invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\) \\} \\);",
				"ParseoMensaje($1, $2, $3);");
		//wvarResult = invoke( "ParseoMensaje", new Variant[] { new Variant(wvarPos), new Variant(wvarParseString) } );
		filters.addRegExPair("invoke\\( \"ParseoMensaje\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\), new Variant\\((\\w+)\\) \\} \\);",
				"ParseoMensaje($1, $2);");
		
		//GetColFromString
//		invoke( "GetColFromString", new Variant[] { new Variant(pVarDefinicionSubNodos) } );
		filters.addRegExPair("invoke\\( \"GetColFromString\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\) \\} \\);",
						"GetColFromString($1);");
		
		//GetDefinicionElemento
//		 invoke\( "GetDefinicionElemento", new Variant\[\] \{ new Variant\((\w+)\.(\w+)\((\w+)\)\.(\w+)\(\)\), new Variant\((\w+)\), new Variant\((\w+)\), new Variant\((\w+)\), new Variant\((\w+)\) \} \);
		filters.addRegExPair(" invoke\\( \"GetDefinicionElemento\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\.(\\w+)\\((\\w+)\\)\\.(\\w+)\\(\\)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\), new Variant\\((\\w+)\\) \\} \\);",
						"GetDefinicionElemento($1.$2($3).$4(), $5, $6, $7, $8);");
		
		//CompleteZero
//		 invoke( "CompleteZero", new Variant[] { new Variant(wvarDatoValue), new Variant(pvarLongitud) } );
		filters.addRegExPair("invoke\\( \"CompleteZero\", new Variant\\[\\] \\{ new Variant\\((\\w+)\\), new Variant\\((\\w+)\\) \\} \\);",
						"CompleteZero($1, $2);");
		
		return filters;
	}

	/**
	 * Crea un filter para corregir las expresiones para cargar archivos.
	 * @return
	 */
	protected static Filter getFixLoadFile() {

		RegExChainFilter filters = new RegExChainFilter();

		filters.addRegExPair("load\\( System\\.getProperty\\(\"user\\.dir\"\\) \\+ \"\\\\\\\\\" \\+ ModGeneral\\.(\\w+)+ \\);",
			      "load( Thread.currentThread().getContextClassLoader().getResourceAsStream(ModGeneral.$1));");
		filters.addRegExPair("load\\( System\\.getProperty\\(\"user\\.dir\"\\) \\+ \"\\\\\\\\\" \\+ \"(\\w+)+\\.xsl\" \\);",
			      "load( Thread.currentThread().getContextClassLoader().getResourceAsStream(\"$1.xsl\"));");
		filters.addRegExPair("load\\( System\\.getProperty\\(\"user\\.dir\"\\) \\+ \"\\\\\\\\\" \\+ (\\w+) \\);",
			      "load( Thread.currentThread().getContextClassLoader().getResourceAsStream($1));");
		return filters;
	}

	/**
	 * Acomoda los GoTos y Resumes
	 * @return
	 */
	protected static Filter getFixGoTos() {

//		Dos gotos o Resume:
//			dentro de un bloque de exception:
//			        /*unsup mobjCOM_Context.SetAbort() */;
//			        //unsup Resume ClearObjects
//			y este goto que limpia:
//			        /*unsup mobjCOM_Context.SetAbort() */;
//			        //unsup GoTo ClearObjects
//
//			Si la clase tiene esto que sigue, en vez del goto puedo hacer el return directamente
//			      ClearObjects: 
//			      //~~~~~~~~~~~~~~~
//			      // LIBERO LOS OBJETOS
//			      wobjXMLConfig = null;
//			      return IAction_Execute;
//
		
		RegExChainFilter filters = new RegExChainFilter();

		filters.addRegExPair("//unsup GoTo ClearObjects(\\s*)\\}","return IAction_Execute;$1}");
		//Acá además de sacar el unsup arreglo un bug, porque hacen el goto antes de setear el código de error Doh!
		filters.addRegExPair("//unsup GoTo ClearObjects(\\s*)IAction_Execute = 1;(\\s*)",
				"IAction_Execute = 1;$1return IAction_Execute;$2");

		filters.addRegExPair("//unsup Resume ClearObjects(\\s*)Err\\.clear\\(\\);(\\s*)}","Err.clear();$1return IAction_Execute;$2}");

	
		return filters;
	}

	/**
	 * Acomoda expresiones de log
	 * @return
	 */
	private static RegExChainFilter getFixLog() {
		RegExChainFilter log = new RegExChainFilter();
		log.addRegExPair("private Object mobjEventLog = null;","private EventLog mobjEventLog = new EventLog();");
		log.addRegExPair("this\\.getValue\\( \"EventLog_Category\\.evtLog_Category_Unexpected\" \\)","ErrorConstants\\.getValue\\(\"EventLog_Category\\.evtLog_Category_Unexpected\"\\)");
		return log;
	}

	/**
	 * Remueve expresiones ".xml" estilo propiedad
	 * @return
	 */
	private static RegExChainFilter getFixXmlMethod() {
		RegExChainFilter sacaXml = new RegExChainFilter();
		//Saca el .xml de espresiones como 
		//wobjXMLConfig.selectSingleNode("//MQCONFIG").xml
		sacaXml.addRegExPair("(\\s)(\\w+)\\.select(.*)\\.xml"," $2\\.select$3");
		return sacaXml;
	}

	/**
	 * Cambia XmlDom por XmlDomExtended
	 * @return
	 */
	private static RegExChainFilter getFixXmlDom() {
		RegExChainFilter xmlDom = new RegExChainFilter();
		xmlDom.addRegExPair("diamondedge.util.XmlDom (.+) =", "XmlDomExtended $1 =");
		xmlDom.addRegExPair("(.+) = \\(diamondedge\\.util\\.XmlDom\\) null;","$1 = null;");
		xmlDom.addRegExPair("(.+) = new diamondedge\\.util\\.XmlDom\\(\\);","$1 = new XmlDomExtended();");
		xmlDom.addRegExPair("\\([\\s]*diamondedge\\.util\\.XmlDom[\\s]*","\\( XmlDomExtended ");
		return xmlDom;
	}

	/**
	 * Bloque para invocar a MQ
	 * @return
	 */
	private static RegExChainFilter getFixMQAccess() {
		RegExChainFilter mqConector = new RegExChainFilter();
		mqConector.addRegExPair("Object wobjFrame2MQ = null;","MQProxy wobjFrame2MQ = null;");
		mqConector.addRegExPair("[\\w]*wobjFrame2MQ = new ModGeneral\\.gcteClassMQConnection\\(\\);[\\s]*//error: function 'Execute' was not found\\.[\\s]*//unsup: wvarMQError = wobjFrame2MQ\\.Execute\\((.*), strParseString, (.*)\\)[\\s]*wobjFrame2MQ = null;","wobjFrame2MQ = MQProxy\\.getInstance(MQProxy.AISPROXY);\n\tStringHolder strParseStringHolder = new StringHolder\\(strParseString\\);\n\twvarMQError = wobjFrame2MQ.execute($1, strParseStringHolder);\n\tstrParseString = strParseStringHolder.getValue();\n");
		mqConector.addRegExPair("wobjFrame2MQ\\.execute\\(pvarParametros & pvarContinuar","wobjFrame2MQ.execute(pvarParametros + pvarContinuar");
		return mqConector;
	}

	/**
	 * Arregla la declaración del método IAction_Execute
	 * @return
	 */
	private static RegExFilter getFixDeclaracionMetodo() {
		RegExFilter declaMetodo = new RegExFilter("private int IAction_Execute\\( String Request, Variant Response, String ContextInfo \\)[ \t\r\n]*\\{","@Override\n\tpublic int IAction_Execute\\( String Request, StringHolder Response, String ContextInfo \\) {");
		return declaMetodo;
	}

	/**
	 * Arregla la declaración de la clase
	 * @return
	 */
	private static RegExChainFilter getDeclaracionClaseFilter() {
		RegExChainFilter declaracionClase = new RegExChainFilter();
		declaracionClase.addRegExPair("public class (.+) implements Variant, ObjectControl, HSBCInterfaces.IAction", "public class $1 implements VBObjectClass");
		return declaracionClase;
	}

	/**
	 * Agrega los imports que usamos en las correcciones
	 * 
	 * @param imports 
	 * @return
	 */
	protected static Filter getImports(String paquete, List<String> imports) {
		
		return new AddImportsFilter(paquete, imports);
	}
	
	/**
	 * corrige unsip de selectNodes()
	 * @return
	 */
	protected static Filter getUnsupSelectNodeFilter() {

		return new RegExFilter("null \\/\\*unsup (\\w+)\\.selectNodes\\((.*)\\) \\*\\/",
				"$1\\.selectNodes\\($2\\) ");
	}

	/**
	 * Saca referencias a HSBC.EventLog
	 * @return
	 */
	protected static Filter getRemoveHSBCLog() {

		return new RegExFilter("\\s*mobjEventLog = new HSBC.EventLog\\(\\);","");
	}
	
	/**
	 * Comenta (la impl de ) los "métodos del framework", en vez de borrarlos, por si quiero hacer uso de esto en el futuro
	 * 
	 * @return
	 */
	protected static Filter getRemovesCOMMethodsFilter() {
		RegExChainFilter chain = new RegExChainFilter();
//		Dejo la decla "private Object mobjCOM_Context = null;"
		chain.addRegExPair("\\/\\*unsup mobjCOM_Context\\.SetComplete\\(\\) \\*\\/;","/*TBD mobjCOM_Context.SetComplete() ;*/");
		chain.addRegExPair("\\/\\*unsup mobjCOM_Context\\.SetAbort\\(\\) \\*\\/;","/*TBD mobjCOM_Context.SetAbort() ;*/");
		chain.addRegExPair("mobjCOM_Context = null \\/\\*unsup this\\.GetObjectContext\\(\\) \\*\\/;","/* TBD mobjCOM_Context = this.GetObjectContext() ;*/");
		return chain;
	}


	/**
	 * Filters que fueron importados de los drafts de arreglos previos
	 * 
	 */
	protected static RegExChainFilter getMigratedFilters() {
		RegExChainFilter chain = new RegExChainFilter();
		
		//FALTA: Esta funciona, pero se detiene cuanto encuentra un "*" ( ^* ), mientras que debería detenerse 
		// al encontrar "*/" pero no sé como ponerlo
		//Arregla los implements de las clases 
		chain.addRegExPair("(\\w+) implements com\\.qbe\\.services\\.Variant, com\\.qbe\\.services\\.ObjectControl, com\\.qbe\\.services\\.HSBCInterfaces\\.IAction",
				"$1 implements VBObjectClass");
		chain.addRegExPair("(\\w+) implements Variant, com\\.qbe\\.services\\.ObjectControl, com\\.qbe\\.services\\.HSBCInterfaces\\.IAction",
				"$1 implements VBObjectClass");
		chain.addRegExPair("(\\w+) implements Variant, com\\.qbe\\.services\\.HSBCInterfaces\\.IAction, com\\.qbe\\.services\\.ObjectControl",
				"$1 implements VBObjectClass");
		chain.addRegExPair("com\\.qbe\\.services\\.Variant\\[",
				"Variant\\[");
		chain.addRegExPair("(\\w+) implements com\\.qbe\\.services\\.Variant, com\\.qbe\\.services\\.HSBCInterfaces\\.IAction, com\\.qbe\\.services\\.ObjectControl",
				"$1 implements VBObjectClass");
		chain.addRegExPair("com\\.qbe\\.services\\.Variant\\.class",
				"Variant\\.class");
		
		chain.addRegExPair("null \\/\\*unsup (\\w+)\\.selectSingleNode\\(([^\\*]*)\\) \\*\\/",
				"$1\\.selectSingleNode\\($2\\) ");
		chain.addRegExPair("null \\/\\*unsup (\\w+)\\.item\\(\\s(\\w+)\\s\\)\\.selectSingleNode\\(([^\\*]*)\\) \\*\\/",
				"XmlDomExtended.Node_selectSingleNode\\($1\\.item\\($2\\),$3\\) ");
		
		chain.addRegExPair("diamondedge\\.util\\.XmlDom\\.getText\\(",
				"XmlDomExtended\\.getText\\(");
		chain.addRegExPair("diamondedge\\.util\\.XmlDom\\.setText\\(",
				"XmlDomExtended\\.setText\\(");

		//Saco las lineas del .async = false.
		chain.addRegExPair("\\s*//unsup (\\w+)\\.async = false;","");
		
		//Elimina el String == "" por isEmpty
				chain.addRegExPair("\\(Strings\\.replace\\((.+) == \"\"\\)",
						"\\(Strings\\.replace\\($1\\.isEmpty\\(\\)\\)");
				
		//Replace
		chain.addRegExPair("Replace","Strings.replace");
		
		//Linea: new Variant() /*unsup wobjXMLResponse.transformNode( wobjXSLResponse ) */.
		chain.addRegExPair("new Variant\\(\\) \\/\\*unsup (\\w+)\\.transformNode\\( (\\w+) \\) \\*\\/\\.",
				"$1\\.transformNode\\( $2 \\).");
		
		chain.addRegExPair("//unsup: wobjXmlElemenRespD\\.Text = Trim\\(Strings\\.replace\\(wobjMatch\\.SubMatches\\((\\d+)\\), \"_\", \" \"\\)\\)",
				"wobjXmlElemenRespD.setTextContent(Strings.replace(wobjMatch.SubMatches($1), \"_\", \" \").trim());");
		
		//Soluciona RegEx
		chain.addRegExPair("VbScript.RegExp",
				"VbScript_RegExp");
		chain.addRegExPair("wobjRegExp.Global.set\\( true \\);",
				"wobjRegExp.setGlobal\\( true \\); ");
		chain.addRegExPair("Object wobjMatch",
				"Match wobjMatch");

		chain.addRegExPair("wobjRegExp.Pattern.set",
				"wobjRegExp.setPattern");
		chain.addRegExPair("void wobjColMatch",
				"MatchCollection wobjColMatch");
		chain.addRegExPair("wobjMatch = invoke\\( \"wobjColMatch\", new Variant\\[\\] \\{ new Variant\\(0\\) \\} \\).toObject\\(\\);",
				"wobjMatch = wobjColMatch.next\\(\\);");
		chain.addRegExPair("//wobjRegExp.Global = True",
				" wobjRegExp.setGlobal(true);");
		
		//Conexión MQ
		chain.addRegExPair("wobjFrame2MQ = new gcteClassMQConnection\\(\\);",
				"wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);");
		chain.addRegExPair("wobjFrame2MQ = new ModGeneral.gcteClassMQConnection\\(\\);",
				"wobjFrame2MQ = MQProxy.getInstance(MQProxy.AISPROXY);");
		
		//Marshal (Para los CDATA)
		chain.addRegExPair("(\\w+)\\.getDocument\\(\\)\\.getDocumentElement\\(\\)\\.toString\\(\\);",
				"XmlDomExtended\\.marshal\\($1\\.getDocument\\(\\)\\.getDocumentElement\\(\\)\\);");
		//Marshal para los selectSingleNode.toString()
		chain.addRegExPair("(\\w+)\\.selectSingleNode\\( \"(.+)\" \\)[ \\t\\r\\n]*\\.toString\\(\\)",
				"XmlDomExtended\\.marshal\\($1\\.selectSingleNode\\( \"$2\" \\)\\)");
		chain.addRegExPair("(\\w+)\\.item\\([ \\t\\r\\n]*(\\w+)[ \\t\\r\\n]*\\)\\.toString\\(\\)",
				"XmlDomExtended\\.marshal\\($1\\.item\\( $2 \\)\\)");
		//En caso que el wobjMatch esté vacio sale del for
				chain.addRegExPair("//unsup: If Trim\\(Strings\\.replace\\((\\w+)\\.SubMatches\\(0\\), \"_\", \" \"\\)\\) = \"\" Then Exit For",
						"if \\(Strings\\.replace\\(wobjMatch\\.SubMatches\\(0\\), \"_\", \" \"\\)\\.trim\\(\\)\\.equals\\(\"\"\\)\\) { \n      		 break;\n}");
		
			//Cuando el VB.val pone .0 en el caso que el tipo de datos sea un Entero
			chain.addRegExPair("if\\( XmlDomExtended\\.getText\\( (\\w+)\\.getAttributes\\(\\)\\.getNamedItem\\( \"TipoDato\" \\) \\)\\.equals\\( \"ENTERO\" \\) \\)(\\s*)\\{(\\s*)XmlDomExtended\\.setText\\( (\\w+), String\\.valueOf\\( VB\\.val\\( (\\w+) \\) \\) \\);(\\s*)\\}",
				" if\\( XmlDomExtended\\.getText\\( $1\\.getAttributes\\(\\)\\.getNamedItem\\( \"TipoDato\" \\) \\)\\.equals\\( \"ENTERO\" \\) \\)\n        \\{\n        	String newValue = \"\";\n        	try \\{\n				//Esto lo hago, para que solo lo convierta a int, si viene informado, en caso contrario tendrá un 0 por default\n				int parsedInt = Integer\\.parseInt\\(Strings\\.trim\\($5\\.toString\\(\\)\\)\\);\n				newValue \\+= parsedInt;\n			\\} catch \\(NumberFormatException e\\) \\{\n				newValue \\+= \"0\";\n			\\}\n          XmlDomExtended\\.setText\\( $4, newValue\\);\n        \\}");
			
			//Para el caso que se use el Node.createNode
			chain.addRegExPair("/\\*unsup (\\w+)\\.selectSingleNode\\( \"//(\\w+)\" \\) \\*/\\.appendChild\\( (\\w+)\\.createNode\\( new Variant\\( org\\.w3c\\.dom\\.Node\\.(\\w+) \\), \"(\\w+)\" \\+ (\\w+), \"\" \\) \\*/ \\);",
				"$1\\.selectSingleNode\\( \"//$2\" \\)\\.appendChild\\( $3\\.createNode\\(org\\.w3c\\.dom\\.Node\\.$4, \"$5\" \\+ $6, \"\" \\)  \\);");

			//Convierte el IAction_Execute al estandard de VBObjectClass
			chain.addRegExPair("(\\w+) int IAction_Execute\\( String (\\w+), Variant (\\w+),[\t\r\n| ]*String (\\w+) \\)",
					"@Override\n\tpublic int IAction_Execute\\( String $2, StringHolder $3, String $4 \\)");
			//Convierte los cloneNode
			chain.addRegExPair("(\\w+)\\.selectSingleNode\\( \"(.+)\" \\)\\.appendChild\\( (\\w+)\\.selectSingleNode\\( \"(.+)\" \\) \\.cloneNode\\( true \\) \\);",
					"XmlDomExtended\\.nodeImportAsChildNode\\($1\\.selectSingleNode\\(\"$2\" \\),wobjXMLRespuestas\\.selectSingleNode\\( \"$3\" \\)\\);");
			//Convierte los Strings.format
			chain.addRegExPair("XmlDomExtended\\.setText\\( (\\w+), Strings\\.format\\( XmlDomExtended\\.getText\\( (\\w+) \\), XmlDomExtended\\.getText\\( (\\w+)\\.getAttributes\\(\\)\\.getNamedItem\\((.+) \\) \\) \\) \\);",
					"XmlDomExtended\\.setText\\( $1, Formater\\.doubleFormat\\( XmlDomExtended\\.getText\\( $2 \\), XmlDomExtended\\.getText\\( $3\\.getAttributes\\(\\)\\.getNamedItem\\( $4 \\) \\) \\) \\);");
			//Convierte los Obj.toDouble
			chain.addRegExPair("(\\w+) = (\\w+) \\+ \"(.+)\" \\+ String\\.valueOf\\( Obj\\.toDouble\\( Strings\\.mid\\( (\\w+), (\\w+), (\\d+) \\) \\) \\) \\+ \"(.+)\";",
					"$1 = $2 \\+ \"$3\" \\+ com\\.qbe\\.services\\.format\\.Formater\\.convertsDouble\\( Strings\\.mid\\( $4, $5, $6 \\) \\) \\+ \"$7\";");

		return chain;
	}

	/**
	 *  Se arreglan las conexiones a Base de Datos.
	 * 
	 * @return
	 */
	protected static Filter getDBConnectionFilter() {
		RegExChainFilter chain = new RegExChainFilter();
		chain.addRegExPair("wobjHSBC_DBCnn = new HSBC\\.DBConnection\\(\\);","com\\.qbe\\.services\\.db\\.JDBCConnectionFactory connFactory = new com\\.qbe\\.services\\.db\\.JDBCConnectionFactory\\(\\);");
		chain.addRegExPair("//Set (\\w+) = (\\w+)\\.CreateInstance\\(\"(\\w+)\\.(\\w+)\"\\)[ \\t\\r\\n]*//error: function '(\\w+)' was not found\\.","com\\.qbe\\.services\\.db\\.JDBCConnectionFactory connFactory = new com\\.qbe\\.services\\.db\\.JDBCConnectionFactory\\(\\);");
		chain.addRegExPair("wobjHSBC_DBCnn = new HSBC\\.DBConnectionTrx\\(\\);","com\\.qbe\\.services\\.db\\.JDBCConnectionFactory connFactory = new com\\.qbe\\.services\\.db\\.JDBCConnectionFactory\\(\\);");
		chain.addRegExPair("Variant gcteDB = new Variant\\(\\);","com\\.qbe\\.services\\.db\\.JDBCConnectionFactory connFactory = null;");
		chain.addRegExPair("//unsup: Set wobjDBCnn = wobjHSBC_DBCnn\\.GetDBConnection\\(gcteDB\\)","wobjDBCnn = connFactory\\.getDBConnection\\(ModGeneral\\.gcteDB\\);");
		chain.addRegExPair("/\\*unsup (\\w+)\\.Save\\( (\\w+)\\.toString\\(\\), AdoConst\\.adUnsupported \\) \\*/;","$2.load(AdoUtils.saveRecordset($1));");
		//TODO Correr esta línea solo la primera vez, sino despues concatena las condiciones
//				chain.addRegExPair("wrstDBResult = wobjDBCmd\\.execute\\(\\);","wrstDBResult = wobjDBCmd\\.execute\\(\\);\n\t\tif \\( wrstDBResult\\.getRecordCount\\(\\)==-1\\) {\n\t\t\twrstDBResult = wrstDBResult\\.nextRecordset\\(\\);\n	\t}\n\t\tif \\( wrstDBResult\\.getRecordCount\\(\\)==-1\\) {\n\t\t//Sigue en -1\\?\n\t\t\tthrow new Exception\\(\"No puedo recuperar el recordSet\"\\);\n\t\t}\n\twrstDBResult\\.setActiveConnection\\( \\(Connection\\) null \\);");
		chain.addRegExPair("if\\( 0 /\\*unsup (\\w+)\\.State \\*/ == 0/\\*unsup adStateOpen\\*/ \\)","");
		chain.addRegExPair("//unsup: Set (\\w+) = (\\w+)\\.GetDBConnection\\((\\w+)\\)","$1 = connFactory\\.getDBConnection\\(ModGeneral\\.$3\\);");
		chain.addRegExPair("\\(Connection\\) \\(Connection\\)\\( (\\w+)\\.GetDBConnection\\( ModGeneral\\.(\\w+) \\) \\);","connFactory\\.getDBConnection\\(ModGeneral\\.$2\\);");
		return chain;
	}

	/**
	 * Acomoda expresiones de log
	 * @return
	 */
	private static Filter getFixTimeOut() {
		RegExChainFilter chain = new RegExChainFilter();
		chain.addRegExPair("catch \\( Exception _e_ \\)[ \\t\\r\\n]*\\{[ \\t\\r\\n]*[ \\t\\r\\n]*Err\\.set \\( _e_ \\);[ \\t\\r\\n]*java\\.util\\.logging\\.Logger logger = java\\.util\\.logging\\.Logger\\.getLogger\\(this\\.getClass\\(\\)\\.getName\\(\\)\\);",
				"catch \\(com\\.qbe\\.services\\.mq\\.connector\\.TimeOutAISConnectionException toe\\) \\{\n\t\tResponse\\.set\\( \"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>\" \\);\n\t\treturn 3;\n\t}\n\tcatch( Exception _e_ )\n\t{\n\t\tErr.set( _e_ );\n\t\tjava\\.util\\.logging\\.Logger logger = java\\.util\\.logging\\.Logger\\.getLogger\\(this\\.getClass\\(\\)\\.getName\\(\\)\\);");
		chain.addRegExPair("catch\\( Exception _e_ \\)[ \\t\\r\\n]*\\{[ \\t\\r\\n]*Err\\.set\\( _e_ \\);[ \\t\\r\\n]*try [ \\t\\r\\n]*\\{[ \\t\\r\\n]*//~~~~~~~~~~~~~~~[ \\t\\r\\n]*//[[ \\t\\r\\n]*wobjXMLResponse = null;[ \\t\\r\\n]*wobjXSLResponse = null;[ \\t\\r\\n]*//|]*[ \\t\\r\\n]*mobjEventLog\\.Log\\( ErrorConstants\\.getValue\\(\"EventLog_Category\\.evtLog_Category_Unexpected\"\\), mcteClassName, wcteFnName, wvarStep, Err\\.getError\\(\\)\\.getNumber\\(\\), \"Error= \\[\" \\+ Err\\.getError\\(\\)\\.getNumber\\(\\) \\+ \"\\] - \" \\+ Err\\.getError\\(\\)\\.getDescription\\(\\) \\+ \" Mensaje:\" \\+ mcteOpID \\+ wvarCiaAsCod \\+ wvarUsuario \\+ \" Hora:\" \\+ DateTime\\.now\\(\\), vbLogEventTypeError \\);",
				"catch (MQProxyTimeoutException toe) {\n\t\tResponse.set( \"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>\" );\n\t\treturn 3;\n\t}\n\tcatch\\( Exception _e_ \\)\n\t\\{\n\t\tErr\\.set\\( _e_ \\);\n\ttry \n\t\\{\n\t\t//~~~~~~~~~~~~~~~\n\t\t\tmobjEventLog\\.Log\\( ErrorConstants\\.getValue\\(\"EventLog_Category\\.evtLog_Category_Unexpected\"\\), mcteClassName, wcteFnName, wvarStep, Err\\.getError\\(\\)\\.getNumber\\(\\), \"Error= \\[\" \\+ Err\\.getError\\(\\)\\.getNumber\\(\\) \\+ \"\\] - \" \\+ Err\\.getError\\(\\)\\.getDescription\\(\\) \\+ \" Mensaje:\" \\+ mcteOpID \\+ wvarCiaAsCod \\+ wvarUsuario \\+ \" Hora:\" \\+ DateTime\\.now\\(\\), vbLogEventTypeError \\);");
		return chain;
	}
	
	/**
	 *  Arregla los tags number del XSL para que no devuelva con notación científica.
	 * 
	 * @return
	 */
	protected static Filter getFormatNumberXSLFilter() {
		RegExChainFilter chain = new RegExChainFilter();
		chain.addRegExPair("<xsl:value-of select='number\\(substring\\(\\.,(\\d+),(\\d+)\\)\\)' />\";","<xsl:value-of select='format-number\\(number\\(substring\\(\\.,$1,$2\\)\\), \\\\\"0\\\\\"\\)' />\";");
		return chain;
	}
	
	public String getSourcesDir() {
		return sourcesDir;
	}

	public void setSourcesDir(String sourcesDir) {
		this.sourcesDir = sourcesDir;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	/**
	 * Acomoda expresiones de log
	 * @return
	 */
	private static Filter getFixOpenDB() {
		RegExChainFilter chain = new RegExChainFilter();
		//Para Numbers
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\", AdoConst\\.adNumeric, AdoConst\\.adParamInput, 0, new Variant\\( (.+) \\) \\);",
		"wobjDBParm = new Parameter\\( \"@$1\", AdoConst\\.adNumeric, AdoConst\\.adParamInput, 0, new Variant\\( $2 \\) \\);\n\tif \\($2\\.isEmpty\\(\\)\\) {\n\t\tps\\.setNull\\(posiSP\\+\\+, Types\\.NUMERIC\\);\n\t\t} else {\n\t\tps\\.setInt\\(posiSP\\+\\+, Integer\\.parseInt\\($2\\)\\);\n\t}");
		//Para inputoutput Numbers:
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\", AdoConst\\.adNumeric, AdoConst\\.adParamInputOutput, 0, new Variant\\( (.+) \\) \\);",
		"wobjDBParm = new Parameter\\( \"@$1\", AdoConst\\.adNumeric, AdoConst\\.adParamInputOutput, 0, new Variant\\( $2 \\) \\);\n\tif \\($2\\.isEmpty\\(\\)\\) {\n\t\tps\\.setNull\\(posiSP\\+\\+, Types\\.NUMERIC\\);\n\t\t} else {\n\t\tps\\.setInt\\(posiSP\\+\\+, Integer\\.parseInt\\($2\\)\\);\n\t}");

		//Para Strings
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\", AdoConst\\.adChar, AdoConst\\.adParamInput, (.+), new Variant\\( (.+) \\) \\);",
				"wobjDBParm = new Parameter\\( \"@$1\", AdoConst\\.adChar, AdoConst\\.adParamInput, $2, new Variant\\( $3 \\) \\);\n\t\tps\\.setString\\(posiSP\\+\\+, String\\.valueOf\\($3\\)\\);");
		
		//Para Varchar
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\", AdoConst\\.advarChar, AdoConst\\.adParamInput, (.+), new Variant\\( (.+) \\) \\);",
				"wobjDBParm = new Parameter\\( \"@$1\", AdoConst\\.adChar, AdoConst\\.adParamInput, $2, new Variant\\( $3 \\) \\);\n\t\tps\\.setString\\(posiSP\\+\\+, String\\.valueOf\\($3\\)\\);");

		//For<
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\" (.+), AdoConst\\.adNumeric, AdoConst\\.adParamInput, 0, new Variant\\( (.+) \\) \\);",
				"wobjDBParm = new Parameter\\( \"@$1\", AdoConst\\.adChar, AdoConst\\.adParamInput, $2, new Variant\\( $3 \\) \\);\n\t\tps\\.setString\\(posiSP\\+\\+, String\\.valueOf\\($3\\)\\);");
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\" (.+), AdoConst\\.adChar, AdoConst\\.adParamInput, (.+), new Variant\\( (.+)\\) \\);",
				"wobjDBParm = new Parameter\\( \"@$1\" $2, AdoConst\\.adChar, AdoConst\\.adParamInput, $3, new Variant\\( $4 \\) \\);\n\t\tps\\.setString\\(posiSP\\+\\+, String\\.valueOf\\($4\\)\\);");

		return chain;
	}
	
	
	/**
	 * Elimina las sentencias ADO.
	 * @return
	 */
	private static Filter getRemoveADO() {
		RegExChainFilter chain = new RegExChainFilter();
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(.+)\", AdoConst\\.(.+), AdoConst\\.(.+), (\\d)+, new Variant\\( (.+) \\) \\);",
		"");
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(\\w)+\" \\+ String\\.valueOf\\( (\\w)+ \\), AdoConst\\.adNumeric, AdoConst\\.adParamInput, 0, new Variant\\( (\\w)+ \\) \\);",
		"");
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(\\w)+\" \\+ String\\.valueOf\\( wvarCounter \\+ 1 \\), AdoConst\\.(\\w)+, AdoConst\\.adParamInput, (\\d)+, new Variant\\( (\\w)+ \\) \\);",
		"");
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(\\w)+\" \\+ String\\.valueOf\\( wvarCounter \\+ 1 \\), AdoConst\\.(\\w)+, AdoConst\\.adParamInput, (\\d)+, new Variant\\( \"\" \\) \\);",
		"");
		chain.addRegExPair("wobjDBParm\\.setScale\\( \\(byte\\)\\( 0 \\) \\);[ \\t\\r\\n]*wobjDBParm\\.setPrecision\\( \\(byte\\)\\( (\\d)+ \\) \\);[ \\t\\r\\n]*wobjDBCmd\\.getParameters\\(\\)\\.append\\( wobjDBParm \\);[ \\t\\r\\n]*wobjDBParm = \\(Parameter\\) null;",
		"");
		chain.addRegExPair("wobjDBCmd\\.getParameters\\(\\)\\.append\\( wobjDBParm \\);[ \\t\\r\\n]*wobjDBParm = \\(Parameter\\) null;",
		"");
		chain.addRegExPair("wobjDBParm = new Parameter\\( \"@(\\w)+\", AdoConst\\.(\\w)+, AdoConst\\.(\\w)+, (\\d)+, \\((\\w)+\\.equals\\( \"\" \\) \\? new Variant\\(\\(Object\\)null\\) : new Variant\\((\\w)+\\)\\) \\);",
		"");
		chain.addRegExPair("wobjDBParm\\.setPrecision\\( \\(byte\\)\\( (\\d)+ \\) \\);[ \\t\\r\\n]*wobjDBParm\\.setScale\\( \\(byte\\)\\( (\\d)+ \\) \\);",
				"");
		return chain;
	}
}
