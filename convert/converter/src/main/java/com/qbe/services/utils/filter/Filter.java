package com.qbe.services.utils.filter;



public interface Filter {

	public String filter(String input) throws FilterException;
}
