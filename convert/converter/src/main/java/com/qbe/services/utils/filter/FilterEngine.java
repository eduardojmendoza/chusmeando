package com.qbe.services.utils.filter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;


/**
 * Aplica un conjunto de Filters a un archivo, varios archivos en un directorio y otras opciones
 * 
 * @author ramiro
 *
 */
public class FilterEngine {

	private List<Filter> filters = new ArrayList<Filter>();
	
	public void addFilter(Filter f) {
		filters.add(f);
	}
	
	/**
	 * Lee un archivo y aplica todos los filtros configurados.
	 * Genera el resultado en el mismo archivo ( lo pisa )
	 * 
	 * @param f
	 * @throws IOException
	 * @throws FilterException 
	 */
	public void applyFiltersToFile( File f) throws IOException, FilterException {
		String buffer = IOUtils.toString(f.toURI(),"UTF-8");
		for (Filter filter : this.filters) {
			buffer = filter.filter(buffer);
		}
		FileWriter fw = new FileWriter(f);
		IOUtils.write(buffer, fw);
		fw.close();
	}
}
