package com.qbe.services.utils.filter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;


/**
 * Agrupa varios pares de match/replace, y los ejecuta en orden
 * 
 * @author ramiro
 *
 */
public class RegExChainFilter implements Filter {

	private List<RegExPair> regexpairs = new ArrayList<RegExPair>();

	public void addRegExPair(String matcher, String replacement) {
		regexpairs.add(new RegExPair(matcher, replacement));
	}
	
	public String filter(String input) throws FilterException {
		String buffer = input;
		for (RegExPair pair : regexpairs) {
			RegExFilter filter = new RegExFilter(pair.getMatcher(), pair.getReplacement());
			buffer = filter.filter(buffer);
		}
		return buffer;
	}

}
