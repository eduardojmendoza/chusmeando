package com.qbe.services.utils;

import java.io.File;
import java.io.IOException;

import com.qbe.services.utils.filter.Filter;
import com.qbe.services.utils.filter.FilterEngine;
import com.qbe.services.utils.filter.FilterException;
import com.qbe.services.utils.filter.RegExChainFilter;

public class MultiDirConvertsFixer {

	private String[] dirs = {
			"../../dev/CMS-LBAWEBMQ/src/main/java/com/qbe/services/webmq/impl",
			"../../dev/CMS-LBAVIRTUAL/src/main/java/com/qbe/services/ofvirtuallba/impl",
// No convierto en este package porque es un genérico, y hay un ModGeneral que tiene para convertir un static con lo cual no anda la expresión por la que reemplazo ( no this )
			//			"../../dev/CMS-nbwsA_Transacciones/src/main/java/com/qbe/services/transacciones/impl",
			"../../dev/CMS-mqgen/src/main/java/com/qbe/services/mqgeneric/impl",
			"../../dev/CMS-ovmq/src/main/java/com/qbe/services/ovmq/impl",
			"../../dev/CMS-OVMQCotizar/src/main/java/com/qbe/services/ovmqcotizar/impl",
			"../../dev/CMS-OVMQEmision/src/main/java/com/qbe/services/ovmqemision/impl",
			"../../dev/CMS-lbawA_OVCotEndosos/src/main/java/com/qbe/services/cotizadorEndosos/impl",
			"../../dev/CMS-lbawA_SiniestroDenuncia/src/main/java/com/qbe/services/siniestroDenuncia/impl",
			"../../dev/CMS-nbwsA_Transacciones/src/main/java/com/qbe/services/transacciones/impl",
			"../../dev/CMS-OVMQRiesgos/src/main/java/com/qbe/services/ovmqriesgos/impl",
			"../../dev/CMS-InterWSBrok/src/main/java/com/qbe/services/interWSBrok/impl",
			"../../dev/CMS-lbawA_OVSQLCotizar/src/main/java/com/qbe/services/lbawA_OVSQLCotizar/impl",
			"../../dev/CMS-OfVirtualLBA/src/main/java/com/qbe/services/ofVirtualLBA/impl",
			"../../dev/CMS-Productor/src/main/java/com/qbe/services/productor/impl",
			"../../dev/CMS-SegurosOnline/src/main/java/com/qbe/services/segurosOnline/impl",
			"../../dev/CMS-LBAVirtualMQEmision/src/main/java/com/qbe/services/lBAVirtualMQEmision/impl",
			"../../dev/CMS-Siniestros/src/main/java/com/qbe/services/siniestros/impl",
			"../../dev/CMS-OVMQUsuario/src/main/java/com/qbe/services/ovmqusuario/impl"};
	
	private FilterEngine engine;
	
	public static void main( String[] args ) throws IOException, FilterException {
		MultiDirConvertsFixer multi = new MultiDirConvertsFixer();
		multi.processDirs();
	}
	
	public MultiDirConvertsFixer() {
		engine = new FilterEngine();
		engine.addFilter(getTimeOutFilter());
	}
	
	private Filter getErrorLogFilter() {
		RegExChainFilter filters = new RegExChainFilter();
		
		filters.addRegExPair("catch\\( Exception _e_ \\)(\\s*)\\{(\\s*)Err\\.set\\( _e_ \\);(\\s*)try(\\s*)\\{",
				"catch ( Exception _e_ )$1 {$2 Err.set ( _e_ );$3java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());$3logger.log(java.util.logging.Level.SEVERE, \"Exception al ejecutar el request\", _e_);$3try$4 {");
		return filters;
	
	}
	
	private Filter getFormatNumberXSLFilter() {
		RegExChainFilter filters = new RegExChainFilter();
		filters.addRegExPair("<xsl:value-of select='number\\(substring\\(\\.,(\\d+),(\\d+)\\)\\)' />\";",
				"<xsl:value-of select='format-number\\(number\\(substring\\(\\.,$1,$2\\)\\), \\\\\"0\\\\\"\\)' />\";");
		return filters;
	
	}
	
	private Filter getTimeOutFilter() {
		RegExChainFilter filters = new RegExChainFilter();
		filters.addRegExPair("catch \\( Exception _e_ \\)[ \\t\\r\\n]*\\{[ \\t\\r\\n]*[ \\t\\r\\n]*Err\\.set \\( _e_ \\);[ \\t\\r\\n]*java\\.util\\.logging\\.Logger logger = java\\.util\\.logging\\.Logger\\.getLogger\\(this\\.getClass\\(\\)\\.getName\\(\\)\\);",
				"catch \\(com\\.qbe\\.services\\.mq\\.connector\\.TimeOutAISConnectionException toe\\) \\{\n\t\tResponse\\.set\\( \"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>\" \\);\n\t\treturn 3;\n\t}\n\tcatch( Exception _e_ )\n\t{\n\t\tErr.set( _e_ );\n\t\tjava\\.util\\.logging\\.Logger logger = java\\.util\\.logging\\.Logger\\.getLogger\\(this\\.getClass\\(\\)\\.getName\\(\\)\\);");
		filters.addRegExPair("catch\\( Exception _e_ \\)[ \\t\\r\\n]*\\{[ \\t\\r\\n]*Err\\.set\\( _e_ \\);[ \\t\\r\\n]*try [ \\t\\r\\n]*\\{[ \\t\\r\\n]*//~~~~~~~~~~~~~~~[ \\t\\r\\n]*//[[ \\t\\r\\n]*wobjXMLResponse = null;[ \\t\\r\\n]*wobjXSLResponse = null;[ \\t\\r\\n]*//|]*[ \\t\\r\\n]*mobjEventLog\\.Log\\( ErrorConstants\\.getValue\\(\"EventLog_Category\\.evtLog_Category_Unexpected\"\\), mcteClassName, wcteFnName, wvarStep, Err\\.getError\\(\\)\\.getNumber\\(\\), \"Error= \\[\" \\+ Err\\.getError\\(\\)\\.getNumber\\(\\) \\+ \"\\] - \" \\+ Err\\.getError\\(\\)\\.getDescription\\(\\) \\+ \" Mensaje:\" \\+ mcteOpID \\+ wvarCiaAsCod \\+ wvarUsuario \\+ \" Hora:\" \\+ DateTime\\.now\\(\\), vbLogEventTypeError \\);",
				"catch (com.qbe.services.mq.connector.TimeOutAISConnectionException toe) {\n\t\tResponse.set( \"<Response><Estado resultado='false' mensaje='El servicio de consulta no se encuentra disponible' />Codigo Error:3</Response>\" );\n\t\treturn 3;\n\t}\n\tcatch\\( Exception _e_ \\)\n\t\\{\n\t\tErr\\.set\\( _e_ \\);\n\ttry \n\t\\{\n\t\t//~~~~~~~~~~~~~~~\n\t\t\tmobjEventLog\\.Log\\( ErrorConstants\\.getValue\\(\"EventLog_Category\\.evtLog_Category_Unexpected\"\\), mcteClassName, wcteFnName, wvarStep, Err\\.getError\\(\\)\\.getNumber\\(\\), \"Error= \\[\" \\+ Err\\.getError\\(\\)\\.getNumber\\(\\) \\+ \"\\] - \" \\+ Err\\.getError\\(\\)\\.getDescription\\(\\) \\+ \" Mensaje:\" \\+ mcteOpID \\+ wvarCiaAsCod \\+ wvarUsuario \\+ \" Hora:\" \\+ DateTime\\.now\\(\\), vbLogEventTypeError \\);");
		return filters;
	
	}
	
	
	private void processDirs() throws IOException, FilterException {
		for (String dir : this.dirs) {
			this.processDir(dir);
		}
	}
	private Filter getFixerModGeneralFilter() {
		RegExChainFilter filters = new RegExChainFilter();
		filters.addRegExPair("diamondedge.util.XmlDom (.+) =", "XmlDomExtended $1 =");
		filters.addRegExPair("(.+) = \\(diamondedge\\.util\\.XmlDom\\) null;","$1 = null;");
		filters.addRegExPair("(.+) = new diamondedge\\.util\\.XmlDom\\(\\);","$1 = new XmlDomExtended();");
		//unsup GoTo ClearObjects
		filters.addRegExPair("//unsup GoTo ClearObjects(\\s*)\\}","return GetErrorInformacionDato;$1}");
		filters.addRegExPair("diamondedge\\.util\\.XmlDom\\.getText\\(",
				"XmlDomExtended\\.getText\\(");
		filters.addRegExPair("diamondedge\\.util\\.XmlDom\\.setText\\(",
				"XmlDomExtended\\.setText\\(");
		filters.addRegExPair("if\\( XmlDomExtended\\.getText\\( (\\w+)\\.getAttributes\\(\\)\\.getNamedItem\\( \"TipoDato\" \\) \\)\\.equals\\( \"ENTERO\" \\) \\)(\\s*)\\{(\\s*)XmlDomExtended\\.setText\\( (\\w+), String\\.valueOf\\( VB\\.val\\( (\\w+) \\) \\) \\);(\\s*)\\}",
				" if\\( XmlDomExtended\\.getText\\( $1\\.getAttributes\\(\\)\\.getNamedItem\\( \"TipoDato\" \\) \\)\\.equals\\( \"ENTERO\" \\) \\)\n        \\{\n        	String newValue = \"\";\n        	try \\{\n				//Esto lo hago, para que solo lo convierta a int, si viene informado, en caso contrario tendrá un 0 por default\n				int parsedInt = Integer\\.parseInt\\(Strings\\.trim\\($5\\.toString\\(\\)\\)\\);\n				newValue \\+= parsedInt;\n			\\} catch \\(NumberFormatException e\\) \\{\n				newValue \\+= \"0\";\n			\\}\n          XmlDomExtended\\.setText\\( $4, newValue\\);\n        \\}");
		return filters;
	
	}
	private void processDir(String dir) throws IOException, FilterException {
		File[] javas = new File(dir).listFiles();
		for (File file : javas) {
			if ( file.isFile()) {
				engine.applyFiltersToFile(file);
			}
		}
		
	}
}
