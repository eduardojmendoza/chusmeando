package com.qbe.services.utils.filter;

public class RegExPair {
	
	private String matcher;
	private String replacement;
	
	public RegExPair(String matcher, String replacement) {
		this.matcher = matcher;
		this.replacement = replacement;
	}

	public String getMatcher() {
		return matcher;
	}
	public void setMatcher(String matcher) {
		this.matcher = matcher;
	}
	public String getReplacement() {
		return replacement;
	}
	public void setReplacement(String replacement) {
		this.replacement = replacement;
	}

}
