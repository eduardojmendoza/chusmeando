VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Send"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Execute(ByVal strUrl As String, ByVal actionCode As String, ByVal request As String, ByRef response As String) As String
'
' Invoca al WebService especificado en strUrl. Arma el request soap para el actionCode, y usando
' el valor de la var request como contenido del <request> element en el body
'
' El strUrl debe apuntar a un MigratedComponentService como:
' strUrl = "http://jupiter.snoopconsulting.com:9080/CMS-fewebservices-0.0.2-SNAPSHOT/MigratedComponentService"
'
' el request debe ser <Request>...</Request>
'
' Retorna el elemento <return> que est'a dentro del <soap:Body><ns2:executeRequestResponse xmlns:ns2="http://fewebservices.services.qbe.com/"
' con el tag incluido, o sea <return>.....</return>
'
' FALTA procesar los Faults
'
'
    Dim strHeader As String
    Dim strXml As String
    Dim strParam As String
    Dim strResponse As String
    '
    strHeader = ""
    
    strXml = "<?xml version=""1.0"" encoding=""utf-8""?>" & _
        "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & _
            "<soapenv:Body>" & _
                "<executeRequest xmlns=""http://fewebservices.services.qbe.com/"">" & _
                    "<actionCode xmlns="""">" & actionCode & "</actionCode>" & _
                        request & _
                    "<contextInfo xmlns="""" xsi:nil=""true""/>" & _
                "</executeRequest>" & _
            "</soapenv:Body>" & _
        "</soapenv:Envelope>"
   '
    strResponse = PostWebservice(strUrl, strXml)
    'response = strResponse
    Execute = strResponse
End Function


Private Function PostWebservice(ByVal Url As String, XmlBody As String) As String
    Dim objDom As Object
    Dim objXmlHttp As Object
    Dim strRet As String
    Dim intPos1 As Integer
    Dim intPos2 As Integer
    
    On Error GoTo Err_PW
    
    ' Create objects to DOMDocument and XMLHTTP
    Set objDom = CreateObject("MSXML2.DOMDocument")
    Set objXmlHttp = CreateObject("MSXML2.XMLHTTP")
    
    ' Load XML
    objDom.async = False
    objDom.loadXML XmlBody

    ' Open the webservice
    objXmlHttp.Open "POST", Url, False
    
    ' Create headings
    objXmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
    ' objXmlHttp.setRequestHeader "SOAPAction", SoapActionUrl
    
    ' Send XML command
    ' objDom.xml
    objXmlHttp.Send (XmlBody)

    ' Get all response text from webservice
    strRet = objXmlHttp.responseText
    
    ' Close object
    Set objXmlHttp = Nothing
    
'--
' Si da Run-time error '91' : Object variable or With block variable not set.
' Ver http://support.microsoft.com/kb/283803
'
    Dim retDom As Object
    Dim returnStr As String
    Dim retNode As Object
    
    Set retDom = CreateObject("MSXML2.DOMDocument")
    retDom.async = False
    retDom.loadXML strRet
    Set retNode = retDom.selectSingleNode("/soap:Envelope/soap:Body/ns0:executeRequestResponse/return")
    If retNode Is Nothing Then
        PostWebservice = "Error: " & Err.Number & " - Error al recuperar el tag return del XML, no se encontro el nodo. Lo que vino es: " & strRet
        Exit Function
    End If
    returnStr = retNode.xml
    
    PostWebservice = returnStr
    
Exit Function
Err_PW:
    PostWebservice = "Error: " & Err.Number & " - " & Err.Description

End Function





