<!--
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Para llamar al real cambiar el actionCode
-->

<!--#include virtual="incCoreActions.asp"-->

<%

' *****************************************************************************************
' ********************** Verifica la existencia del PDF antes de mostrarlo ****************
' *****************************************************************************************
Function Msg1535()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<DEFINICION>CotizarICOModular.xml</DEFINICION><USUARCOD>MERCADOAB</USUARCOD><RAMOPCOD>ICM1</RAMOPCOD><CLIENIVA>1</CLIENIVA><PERIODO>0005</PERIODO><ACTIVIDAD>050052</ACTIVIDAD><RECARPOR>21</RECARPOR><RECARFIN>0</RECARFIN><COMPCPOR>23</COMPCPOR><PLANPAGO>10</PLANPAGO><FORMPAGO>4</FORMPAGO><PAISSCOD>00</PAISSCOD><PROVICOD>1</PROVICOD><CODIZONA>0001</CODIZONA><CLIEIBTP/><CLIENTIP>00</CLIENTIP><CAN-COBER-ENT>11</CAN-COBER-ENT><COBERTURAS><COBERTURA><COBERCOD>100</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>50000</CAPITASG></COBERTURA><COBERTURA><COBERCOD>101</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>60000</CAPITASG></COBERTURA><COBERTURA><COBERCOD>129</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>2500</CAPITASG></COBERTURA><COBERTURA><COBERCOD>134</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>5000</CAPITASG></COBERTURA><COBERTURA><COBERCOD>200</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>6000</CAPITASG></COBERTURA><COBERTURA><COBERCOD>304</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>30000</CAPITASG></COBERTURA><COBERTURA><COBERCOD>211</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>1500</CAPITASG></COBERTURA><COBERTURA><COBERCOD>274</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>600</CAPITASG></COBERTURA><COBERTURA><COBERCOD>279</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>4500</CAPITASG></COBERTURA><COBERTURA><COBERCOD>280</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>24000</CAPITASG></COBERTURA><COBERTURA><COBERCOD>370</COBERCOD><NUMERMOD>1</NUMERMOD><CAPITASG>120000</CAPITASG></COBERTURA></COBERTURAS><EFECTANN>2011</EFECTANN><EFECTMES>11</EFECTMES><EFECTDIA>3</EFECTDIA>"
	
	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ_TEST", _
				 "lbaw_GetConsultaMQ_TEST.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		
							%>								
								<td class="form_titulos_camp">
									<div id= "DVErrorInterno" class="frequentProblemsSubtitle" align=Center> 
										<b>Hubo un problema al invocar el 1535</b>
									</div>
								</td>
							<%
		Response.End
	End If
	
	Msg1535 = mobjXMLDoc.selectSingleNode("/response/CAMPOS").xml
End Function
' *****************************************************************************************
%>

<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1535</h1>
<%
	Response.Write "<TEXTAREA>" & Msg1535() & "</TEXTAREA>"
%>
</body>
</html>
