<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 30/06/2009
' PPCR: 2008-00790
' Desarrollador: Leonardo Ruiz
' Descripci�n: Se reemplazan literales JAVASCRIPT por TEXT/JAVASCRIPT por QA.
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 03/12/2008
' PPCR: Ticket 324896
' Desarrollador: Osores Fernando
' Descripci�n: Correccion de bug introducido por nueva versi�n de Adobe Reader.
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 13/08/2008
' PPCR: 2008-00516 y 2008-00552
' Desarrollador: Marroni Juan Pablo
' Descripci�n: Cambio de Firma y Marca
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 22/07/2008
' PPcR: 50016/6010344
' Desarrollador: Marroni Juan Pablo
' Descripci�n: Se modifica un error para la constancia de pagos en cuanto a la visualizaci�n en pantalla
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 10/05/2008
' PPcR: 50016/6010344
' Desarrollador: Fernando Osores
' Descripci�n: Se agrega la l�gica para visualizar la Constancia de Pagos de la p�liza
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2008. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: popImprimeDocumentos.asp
' 
' Fecha de Creaci�n: desconocido
' 
' PPcR: desconocido
' 
' Desarrollador: desconocido
' 
' Descripci�n: Se incorpora al fuente el ammendment para cumplir con los 
'	requisitos de QA. El fuente es preexistente, se desconoce la fecha real de 
'	creaci�n y el PPCR original.
' 
' ------------------------------------------------------------------------------
%>
<!--#include virtual="incCoreActions.asp"-->

<%

' *****************************************************************************************
' ********************** Verifica la existencia del PDF antes de mostrarlo ****************
' *****************************************************************************************
Function ExistePDF(pRuta)

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc
	Dim mvarRUTA

	mvarRUTA = pRuta

	'Con la existencia del nodo <SOLOBUSCARARCHIVO>, el COM+ me responde si ya est� generado el PDF o no
	mvarRequest =	"<Request><RUTA>" & mvarRUTA & "</RUTA><SOLOBUSCARARCHIVO></SOLOBUSCARARCHIVO></Request>" 
	'Response.Write "<textarea>" & mvarRequest  & "</textarea>" 
	
	Call cmdp_ExecuteTrn("lbaw_OVGetBinaryFile", _
				 "lbaw_OVGetBinaryFile.biz", _
				 mvarRequest, _
				 mvarResponse)
	'Response.Write "<textarea>" & mvarResponse  & "</textarea>" 
	
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		
							%>								
								<td class="form_titulos_camp">
									<div id= "DVErrorInterno" class="frequentProblemsSubtitle" align=Center> 
										<% Session("mvarResponsepopImprimeDocumentos") = mvarResponse %>
										Hubo un problema al recuperar los documentos<BR> Por favor, vuelva a intentar mas tarde <BR>Muchas gracias.</b>
									</div>
								</td>
							<%
		Response.End
	End If
	
	If (uCase(mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado").text) = "TRUE" ) Then
		Set mobjXMLDoc = Nothing
		ExistePDF = True
	Else
		Set mobjXMLDoc = Nothing
		ExistePDF = False
	End If
End Function
' *****************************************************************************************
%>
<%	
' *****************************************************************************************
' *
' * En la reimpresi�n de documentos puede presentarse dos problemas						  
' * 1.- Que NO se haya podido generar los archivos PDFs habiendo llamado a lbaw_OVImprimirPoliza
' *			En este caso se muestra un mensaje descriptivo del motivo del error.
' * 2.- Que SI se haya podido generar los archivos PDFs pero por tardar �stos en llegar a la ruta
' *     desde donde se los recupera, puedan no estar disponibles. 
' *			En este caso se muestra un link para volver a cargar los PDFs. 
' *         (en caso de haber llamado a lbaw_OVGetBinaryFile con SOLOBUSCARARCHIVOS y haya faltado alguno de los PDFs) 
' *
' *****************************************************************************************	
	
	Dim mvarRAMOPCOD
	Dim mvarPOLIZANN
	Dim mvarPOLIZSEC
	Dim mvarCERTIPOL
	Dim mvarCERTIANN
	Dim mvarCERTISEC
	Dim mvarOPERAPOL
	Dim mvarTIPODOCU
	Dim mvarTIPOIMPR
	
	Dim mvarRuta
	Dim mvarMostrarPDFs
	
	Dim mvarTIPOPROD
			
	'Dim mvarRequest
	'Dim mvarResponse

	'Preparo los datos para el Request
	mvarRAMOPCOD = Request.QueryString("RAMOPCOD")
	mvarPOLIZANN = Request.QueryString("POLIZANN")
	mvarPOLIZSEC = Request.QueryString("POLIZSEC")
	mvarCERTIPOL = Request.QueryString("CERTIPOL")
	mvarCERTIANN = Request.QueryString("CERTIANN")
	mvarCERTISEC = Request.QueryString("CERTISEC")
	mvarOPERAPOL = Request.QueryString("OPERAPOL")
	mvarTIPODOCU = trim(Request.QueryString("TIPODOCU")	)
	
	'TIPOPROD viene SOLO desde el ASP de Listado de Riesgos
	mvarTIPOPROD = Request.QueryString("TIPOPROD")
	
	'Valores posibles para mvarTIPODOCU
	'CC = Certificado de Cobertura
	'TC = Tarjeta de Circulaci�n
	'CM = Certificado de Mercosur
	'PC = P�liza Completa. En este caso, se debe indicar el tipo de impresi�n en mvarTIPOIMPR
	'CO = Copia de P�liza Original
	'CP = Constancia de Pagos 'FJO 2008-04-07
	
	
	'Valores posibles para mvarTIPOIMPR
	'CO = Copia de P�liza Original
	'OR = Original de p�liza
		
	Select Case mvarTIPODOCU
		Case "CO"
			mvarTIPODOCU = "PC"
			mvarTIPOIMPR = "CO"
		Case "OR"
			mvarTIPODOCU = "PC"
			mvarTIPOIMPR = "OR"
		Case "TC"
			'28/07/2006: Para los productos tanto MASIVOS como GENERAL la T.C. se debe pedir como ORIGINAL.
			'11/08/2006: Se defini� que para TC de MASIVOS el TIPOIMPR debe ser CO; 
			'							Para TC de GENERALES el TIPOIMPR debe ser OR;
			'If mvarTIPOPROD = "G" Then
				'Tipo de producto: GENERAL
			'	mvarTIPOIMPR = "OR"
			'Else
				'Tipo de producto: MASIVO
			'	mvarTIPOIMPR = "CO"
			'End If		
			mvarTIPODOCU = "CM"	
			mvarTIPOIMPR = "OR"
		Case Else
			mvarTIPOIMPR = "OR"
	End Select
	'Response.Write "<TEXTAREA>" & mvarOPERAPOL & "</TEXTAREA>"
    'Response.End	
	If mvarOPERAPOL = "" Then
	    mvarOPERAPOL = "0"
	'If mvarOPERAPOL = "0" Then
	    'HRF Se cambia la l�gica para que traiga el �ltimo endoso
		'mvarOPERAPOL = 9999
	End If
	
	'Preparo el Request
	mvarRequest = "<Request>" & _
						"<USUARIO>" & Session("USER") & "</USUARIO>" & _
						"<RAMOPCOD>" & mvarRAMOPCOD & "</RAMOPCOD>" & _
						"<POLIZANN>" & mvarPOLIZANN & "</POLIZANN>" & _
						"<POLIZSEC>" & mvarPOLIZSEC & "</POLIZSEC>" & _
						"<CERTIPOL>" & mvarCERTIPOL & "</CERTIPOL>" & _
						"<CERTIANN>" & mvarCERTIANN & "</CERTIANN>" & _
						"<CERTISEC>" & mvarCERTISEC & "</CERTISEC>" & _
						"<OPERAPOL>" & mvarOPERAPOL & "</OPERAPOL>" & _
						"<TIPODOCU>" & mvarTIPODOCU & "</TIPODOCU>" & _						
						"<TIPOIMPR>" & mvarTIPOIMPR & "</TIPOIMPR>" & _
					"</Request>"
					
					
	'**************************************************************************				
	'Response.Write "<TEXTAREA>" & mvarRequest & "</TEXTAREA>"
   'Response.End
	'**************************************************************************	
	If (Session("mvarResponsepopImprimeDocumentos") = "") Then
		'Llamo al COM+ ya que no se pidi� previamente la generaci�n de las archivos PDFs
		Call cmdp_ExecuteTrn("lbaw_OVImprimirPoliza", _
							 "lbaw_OVImprimirPoliza.biz", _
							 mvarRequest , _
							 mvarResponse)						 	 
    Else
		'Solo recupero las rutas de los archivos previamente generados para su visualizaci�n
		mvarResponse = Session("mvarResponsepopImprimeDocumentos")		
    End If

'Response.Write "<TEXTAREA>" & mvarResponse & "</TEXTAREA>"
'Response.End
	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
		mobjXMLDoc.async = False
		mobjXMLDoc.loadXML(mvarResponse)
		
	'Verifico la respuesta del COM+
	mvarError = ""
	mvarErrorGeneral = ""
	mvarCodError = ""
	mvarErrorPuntutal = "Por favor, vuelva a Intentar mas tarde <BR>Muchas gracias.</b>"
	If UCase(mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado").text) <> "TRUE" Then
		If Not (mobjXMLDoc.selectSingleNode("//CODIGOERROR")) Is Nothing Then
			mvarCodError = mobjXMLDoc.selectSingleNode("//CODIGOERROR").text
		End If
		mvarErrorGeneral = "En este momento no se pueden reimprimir los documentos.<BR>"
		select case mvarCodError
			case "30"
				mvarErrorPuntutal = "Motivo: El original aun no ha sido impreso."
			case "31"
				mvarErrorPuntutal = "Motivo: No se encontr� la p�liza."
			case "32"
				mvarErrorPuntutal = "Motivo: Impresi�n de Operaci�n no permitida (VALIDACION GRO1581A)."
			case "39"
				mvarErrorPuntutal = "<br>La p�liza seleccionada no posee las condiciones necesarias para la impresion de la Constancia de Pagos<br><br>"	
				mvarErrorGeneral =""
			case "40"
				mvarErrorPuntutal = "Motivo: Error en llamada al GR16000P."
			case "50"
				mvarErrorPuntutal = "Motivo: P�liza dada de Baja."
			case "51"
				mvarErrorPuntutal = "Motivo: Certificado Anulado."
			case "52"
				mvarErrorPuntutal = "Motivo: No se pudo generar el documento."
			case "77"
				mvarErrorPuntutal = "Motivo: Registro bloqueado."
			case "88"
				mvarErrorPuntutal = "Motivo: Esta operaci�n no puede imprimirse en este momento."
			case "97"
				mvarErrorPuntutal = "Motivo: El servicio de impresi�n no est� disponible en este momento por procesos batch."								

			case else:
				mvarErrorPuntutal = mvarErrorPuntutal + "<BR> Codigo Error: " + mvarCodError
		end select
		mvarError = mvarErrorGeneral + mvarErrorPuntutal 
	End If
	
	Set mobjFiles = mobjXMLDoc.selectNodes("//FILE")
%>
<html>
	<head>		
		<link rel="stylesheet" type="text/css" href="/Oficina_virtual/Html/Css/Css_gral.css">
		<script type="text/javascript" src="/Oficina_Virtual/JS/funciones.js"></script>
		<script type="text/javascript">
		<!--
			/* *************************************************************** */
			var vPDFActual = 'divPDF0';				
			/* *************************************************************** */			
			//alert('<%=mvarRequest%>');
			function CargarPagina()
			{
				try 
					{						
						parent.Espera.style.display = 'none';
						parent.document.getElementById('ipopImprimeDocumentos').style.display = 'block';
						//idPDF0.setShowToolbar(false);
						idPDF0.setZoom(100);						
						eval(vPDFActual).style.display = 'block';
						if (divPDF0.TipoHoja == 'LG')
							{
								alert("Para la correcta impresi�n de este documento es necesario utilizar papel tama�o oficio.\n       Recuerde configurar la impresora para que imprima en dicho formato.");
							}
						else
							{
								alert("Para la correcta impresi�n de este documento es necesario utilizar papel tama�o A4.\n       Recuerde configurar la impresora para que imprima en dicho formato.");
							}

						//setInterval("idTablaPpal.focus();",100);
					}
				catch(err)
					{
						//setInterval("idTablaPpal.focus();",100);
						alert("No se pudieron recuperar los documentos.");
					}
			}			
			/* *************************************************************** */
			function Refrescar()
			{	
				parent.Espera.style.display = 'block';
				parent.document.getElementById('ipopImprimeDocumentos').style.display = 'none';
				window.location.href = 'popImprimeDocumentos.asp';
			}			
			/* *************************************************************** */			
			function MostrarPDF(pDiv, pidPDF)
			{			 
			 eval(vPDFActual).style.display = 'none';
			 vPDFActual = pDiv;
			 //eval(pidPDF).setShowToolbar(false);
			 eval(pidPDF).setZoom(100);			 
			 eval(pDiv).style.display = 'block';
			 
			 if (eval(pDiv).TipoHoja == 'LG')
			    {
					alert("Para la correcta impresi�n de este documento es necesario utilizar papel tama�o oficio.\n       Recuerde configurar la impresora para que imprima en dicho formato.");
				}
			 else
				{
					alert("Para la correcta impresi�n de este documento es necesario utilizar papel tama�o A4.\n       Recuerde configurar la impresora para que imprima en dicho formato.");
				}
			}
			/* *************************************************************** */
			function ImprimirPDF(pidPDF)
			{
				eval(pidPDF).printWithDialog();
				eval(pidPDF).Visible = true;
			}
			/* *************************************************************** */
			function ctrlEsc()
			{				
				if(event.keyCode == 27)
					{
						parent.parent.close();
					}					
			}
			/* *************************************************************** */					
		-->
		</script>
	</head>
<body onLoad="CargarPagina();" onKeyPress="JavaScript: ctrlEsc();">

<BR>

	<table width="100%" border="0" cellpadding="0" cellspacing="0" Id="idTablaPpal">
		<!--Encabezado-->
		<tr>
			<td> 
			<%titulo="Reimpresi�n de Documentos"%>
			<%texto="ReimpresionDeDocumentos.htm" 'Revisar ya que no existe este html %>
			
			
			</td>
		</tr>
		<!--Fin encabezado-->
		<tr>
			<td class="form_titulos_camp" align="center">
				<!--div para mostrar ERROR -->		
				<div id= "DVErrorInterno" class="frequentProblemsSubtitle"> 
					<% 
						If mvarError <> "" Then
							Response.Write mvarError
							Set mobjFiles  = Nothing		
							Set mobjXMLDoc = Nothing							
							Response.End
						End If					
					%>
				</div>
				<!--Fin Div para mostrar ERROR -->
			</td>
		</tr>
		<tr>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				<table width="96%"  border="0" class ="TDCont" align=Center>					
					<tr>
					<%
					
						'Pregunto si todos los PDFs est�n disponibles para ser mostrados.
						mvarMostrarPDFs = True
						For i = 0 To (mobjFiles.length - 1)
							mvarRuta = Trim(mobjXMLDoc.selectNodes("//FILE").item(i).selectSingleNode("RUTA").text)
							'mvarRuta =  "\\arp603vlncfs\planetpress\GR17210D_611856_000001.PDF"
													
							'Response.Write "<TEXTAREA>" & mvarRuta & "</TEXTAREA>"
							If ExistePDF(mvarRuta) Then								
								mvarMostrarPDFs = True
								'Response.Write "<TEXTAREA>" & "EXISTE EL NOMBRE DE LA RUTA" & "</TEXTAREA>"
							Else
								mvarMostrarPDFs = False								
								'Response.Write "<TEXTAREA>" & "NO EXISTE EL NOMBRE DE LA RUTA" & "</TEXTAREA>"
								Exit For
							End If
						Next

						'Con solo un PDF que no se pueda mostrar, no se muestra nada.						
						If Not mvarMostrarPDFs Then
							%>								
								<td class="form_titulos_camp">
									<div id= "DVErrorInterno" class="frequentProblemsSubtitle" align=Center> 
										<% Session("mvarResponsepopImprimeDocumentos") = mvarResponse %>
										Hubo un problema al recuperar los documentos<BR> Por favor, vuelva a intentar haciendo <DIV onClick="JavaScript: Refrescar();" Style="Cursor:hand; Color:#CC0000; text-decoration:underline;">click aqu�.</DIV>Muchas gracias.</b>
									</div>
								</td>
							<%
							Set mobjFiles  = Nothing
							Set mobjXMLDoc = Nothing
							Response.End					
						End If
									
						For i = 0 To (mobjFiles.length - 1)
							mvarDescripcion = mobjXMLDoc.selectNodes("//FILE").item(i).selectSingleNode("DESCRIPCION").text
							mvarRuta = Trim(mobjXMLDoc.selectNodes("//FILE").item(i).selectSingleNode("RUTA").text)							
							%>								
								<td class="form_titulos02c" onClick="JavaScript: MostrarPDF('divPDF<%=i%>','idPDF<%=i%>')" Style="Cursor:hand" Title="Click aqu� para ver el documento">
									<%				
										Response.Write mvarDescripcion
									%>
								</td>
							<%
						Next
					%>
					</tr>
					<tr>
						<td ColSpan = "<%= mobjFiles.length %>" align=Center class="form_titulos_camp">
							<%				
								For i = 0 To (mobjFiles.length - 1)
									mvarDescripcion = mobjXMLDoc.selectNodes("//FILE").item(i).selectSingleNode("DESCRIPCION").text
									mvarRuta = Trim(mobjXMLDoc.selectNodes("//FILE").item(i).selectSingleNode("RUTA").text)								
									mvarTipoHoja = Trim(mobjXMLDoc.selectNodes("//FILE").item(i).selectSingleNode("TIPOHOJA").text)
									
									%>
									<DIV Id="divPDF<%=i%>" Style="display:none;" onKeyPress="JavaScript: ctrlEsc();" TipoHoja="<%=mvarTipoHoja%>">
									<%
										If mobjFiles.length > 1 Then
											Response.Write mvarDescripcion
										End If
									%>		
										<OBJECT classid=clsid:CA8A9780-280D-11CF-A24D-444553540000 height="370"  width="580" VIEWASTEXT Id="idPDF<%=i%>" Border=1>
											<PARAM NAME="SRC" VALUE="procesa_pdf.asp?Ruta=<%=mvarRuta%>">																							
										</OBJECT>		
							
										<DIV>
											<IMG SRC="/Oficina_Virtual/html/images/Imprimir.gif" Alt="Imprimir" onClick="ImprimirPDF('idPDF<%=i%>')" Style="Cursor:hand;">
											<BR>
										</DIV>
									</DIV>
									<%
								Next
							%>
						</td>
					</tr>
				</table>			
			</td>
		</tr>		
	</table>
<%
	Set mobjFiles  = Nothing		
	Set mobjXMLDoc = Nothing
%>
</body>
</html>
