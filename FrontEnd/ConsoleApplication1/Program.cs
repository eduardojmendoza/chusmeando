﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBEDispatcher;
using System.Diagnostics;
using System.Xml;
using System.ServiceModel;
using ConsoleApplication1.MSCService;
using System.Xml.Serialization;
using System.IO;


namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Program app = new Program();
            bool cont = true;
            Deserializador des = new Deserializador();
            
            while (cont)
            {
                //des.deserializarSerializarConsole();
                app.logMensaje();
                
                Console.WriteLine("presione ENTER");

                string input = Console.ReadLine();
                
                if (!input.Equals("1")) cont = false;
            }
        }


        private void logMensaje()
        {
            

            try 
	            {	        
		            Boolean httpSoap = false;

                        if (httpSoap)
                            soap();
                        else
                        {


                            String actionCode = "lbaw_GetConsultaMQ_TEST";
                            actionCode = "lbaw_OVOpeEmiTotales";
                            actionCode = "lbaw_OVClientesConsulta";
                            actionCode = "lbaw_OVExigibleaPC";

                            MigratedComponentServiceClient servicio = getDefaultServiceClient();

                            AnyXmlElement xml = new AnyXmlElement();

                            xml.Any = new XmlElement[1];

                            Serializador s = new Serializador();
                            Deserializador deserializador = new Deserializador();
                            xml.Any = deserializador.getXMLElementsRequest(actionCode);

                            Console.WriteLine("Action: " + actionCode + " " + s.serializar(xml));


                            Console.WriteLine("Invocando servicio");

                            composedResponse respuesta = null;
                            //composedResponse respuesta = servicio.executeRequest("lbaw_OVOpeEmiTotales", xml, "");
                            respuesta = servicio.executeRequest(actionCode, xml, "");



                            String res = s.serializar(respuesta);
                            Console.WriteLine(res);
                        }
	            }
	            catch (Exception e)
	            {
                    Console.WriteLine(e);
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.InnerException);
                    Console.WriteLine(e.StackTrace);
		           
	            }

            
             
        }

        private MigratedComponentServiceClient getDefaultServiceClient()
        {
            BasicHttpSecurityMode securitymode = BasicHttpSecurityMode.None;
            BasicHttpBinding binding = new BasicHttpBinding(securitymode);
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = 107500;
            //binding.ReaderQuotas.MaxStringContentLength = 2147483647;
            Console.WriteLine(binding.ReaderQuotas.MaxStringContentLength);

            Uri uri = new Uri("http://jupiter.snoopconsulting.com:9080/CMS-fewebservices/MigratedComponentService");

            EndpointAddress eAddress = new EndpointAddress(uri);

            return new MigratedComponentServiceClient(binding, eAddress);
        
        }


        private void soap()
        {
            System.Net.WebClient manualWebClient = new System.Net.WebClient();

            manualWebClient.Headers.Add("Content-Type", "application/soap+xml;  charset=utf-8");

        // Note: don't put the <?xml... tag in--otherwise it will blow up with a 500 internal error message!
            Byte[] bytArguments = System.Text.Encoding.ASCII.GetBytes(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" + 
	                "<soapenv:Body>	<executeRequest xmlns=\"http://fewebservices.services.qbe.com/\"><actionCode xmlns=\"\">lbaw_OVExigibleaPC</actionCode> " + 
                "<Request><USUARIO xmlns=\"\">EX009005L</USUARIO>	<NIVELAS xmlns=\"\">GO</NIVELAS><CLIENSECAS xmlns=\"\">100029438</CLIENSECAS>" +
                 "<NIVEL1  xmlns=\"\"/><CLIENSEC1 xmlns=\"\" /><NIVEL2 xmlns=\"\" /><CLIENSEC2 xmlns=\"\" /><NIVEL3 xmlns=\"\">PR</NIVEL3><CLIENSEC3 xmlns=\"\">100029438</CLIENSEC3> " +
			    "<TIPO_DISPLAY xmlns=\"\">2</TIPO_DISPLAY><ORIGEN xmlns=\"\">E</ORIGEN></Request><contextInfo xmlns=\"\" xsi:nil=\"true\"/> " +
		     "</executeRequest></soapenv:Body></soapenv:Envelope>");


            Byte[] bytRetData = manualWebClient.UploadData("http://jupiter.snoopconsulting.com:9080/CMS-fewebservices/MigratedComponentService", "POST", bytArguments);

            String soapResp = System.Text.Encoding.ASCII.GetString(bytRetData);

            soapResp = soapResp.Replace("</ns0:executeRequestResponse></soap:Body></soap:Envelope>", "");
            soapResp = soapResp.Replace("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns0:executeRequestResponse xmlns:ns0=\"http://fewebservices.services.qbe.com/\">", "");
            Console.WriteLine("Respuesta Serializada");
            Console.WriteLine(soapResp);


            /************************* deserialización *********************************************/
            /*
            XmlRootAttribute myRoot = new XmlRootAttribute("return");

            XmlSerializer serializer = new XmlSerializer(typeof(composedResponse), myRoot);
            //XmlSerializer serializer = new XmlSerializer(typeof(composedResponse));

            StringReader rd = new StringReader(soapResp);

            composedResponse x = (composedResponse)serializer.Deserialize(rd);

            Serializador s = new Serializador();

            Console.WriteLine(s.serializar(x));
             * */

        }
    }
}
