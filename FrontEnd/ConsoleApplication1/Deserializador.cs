﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using ConsoleApplication1.MSCService;



namespace ConsoleApplication1
{
	class Deserializador
	{
       
        public void deserializarSerializarConsole()
        {
            XmlDocument doc = new XmlDocument();
            composedResponse cResponse = null;            
            String result = "";
            //FileStream fs = new FileStream("C:\\temp\\respuesta_fewebservice_1419.xml", FileMode.Open);
            FileStream fs = new FileStream("C:\\temp\\respuesta_1419.xml", FileMode.Open);

            try
            {
                XmlRootAttribute myRoot = new XmlRootAttribute("return");                
                XmlSerializer serializer = new XmlSerializer(typeof(composedResponse), myRoot);
                cResponse = (composedResponse)serializer.Deserialize(fs);
                fs.Close();
                Serializador s = new Serializador();
                result = s.serializar(cResponse);
                Console.WriteLine(result);
                fs.Close();

            }
            catch
            {
                fs.Close();
                throw;
            }
        }

        public XmlElement[] getXMLElementsRequest(String action)
        {
            XmlDocument doc = new XmlDocument();

            String request = getXML(action);

            try
            {
                doc.LoadXml(request);
            }
            catch (System.Xml.XmlException)
            {
                request = "<doc>" + request + "</doc>"; //agrega la raiz para convertir a XmlDocument
                doc.LoadXml(request);
            }

            XmlElement[] result = new XmlElement[doc.FirstChild.ChildNodes.Count];

            int i = 0;
            foreach (XmlNode node in doc.FirstChild.ChildNodes)
            {

                XmlDocument xNode = new XmlDocument();
                xNode.LoadXml(node.OuterXml);
                result[i] = xNode.DocumentElement;
                i++;

            }

            return result;

        }


        public String getXML(String action)
        {
            if ("lbaw_GetConsultaMQ_TEST".Equals(action))
                return "<DEFINICION>CotizarICOModular.xml</DEFINICION><USUARCOD>MERCADOAB</USUARCOD><RAMOPCOD>ICM1</RAMOPCOD> <CLIENIVA>1</CLIENIVA> <PERIODO>0005</PERIODO> <ACTIVIDAD>050052</ACTIVIDAD> <RECARPOR>21</RECARPOR><RECARFIN>0</RECARFIN> <COMPCPOR>23</COMPCPOR> <PLANPAGO>10</PLANPAGO> <FORMPAGO>4</FORMPAGO> <PAISSCOD>00</PAISSCOD> <PROVICOD>1</PROVICOD><CODIZONA>0001</CODIZONA> <CLIEIBTP/> <CLIENTIP>00</CLIENTIP> <CAN-COBER-ENT>11</CAN-COBER-ENT> <COBERTURAS><COBERTURA> <COBERCOD>100</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>50000</CAPITASG> </COBERTURA> <COBERTURA> <COBERCOD>101</COBERCOD><NUMERMOD>1</NUMERMOD> <CAPITASG>60000</CAPITASG> </COBERTURA> <COBERTURA> <COBERCOD>129</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>2500</CAPITASG></COBERTURA> <COBERTURA> <COBERCOD>134</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>5000</CAPITASG> </COBERTURA> <COBERTURA><COBERCOD>200</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>6000</CAPITASG> </COBERTURA> <COBERTURA> <COBERCOD>304</COBERCOD> <NUMERMOD>1</NUMERMOD><CAPITASG>30000</CAPITASG> </COBERTURA> <COBERTURA> <COBERCOD>211</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>1500</CAPITASG> </COBERTURA><COBERTURA> <COBERCOD>274</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>600</CAPITASG> </COBERTURA> <COBERTURA> <COBERCOD>279</COBERCOD> <NUMERMOD>1</NUMERMOD><CAPITASG>4500</CAPITASG> </COBERTURA> <COBERTURA> <COBERCOD>280</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>24000</CAPITASG></COBERTURA> <COBERTURA> <COBERCOD>370</COBERCOD> <NUMERMOD>1</NUMERMOD> <CAPITASG>120000</CAPITASG> </COBERTURA></COBERTURAS> <EFECTANN>2011</EFECTANN> <EFECTMES>11</EFECTMES> <EFECTDIA>3</EFECTDIA>";
            else if ("lbaw_OVOpeEmiTotales".Equals(action))
                return "<USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS><NIVEL1/><CLIENSEC1/><NIVEL2/><CLIENSEC2/><NIVEL3/><CLIENSEC3/><FECDES>20130324</FECDES><FECHAS>20130424</FECHAS><MSGEST/><CONTINUAR/>";
            else if ("lbaw_OVClientesConsulta".Equals(action))
                return "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS><DOCUMTIP/><DOCUMNRO/><CLIENDES><![CDATA[ARMATI]]></CLIENDES><PRODUCTO/><POLIZA/><CERTI/><PATENTE/><MOTOR/><ESTPOL>TODAS</ESTPOL><QRYCONT/><CLICONT/><PRODUCONT/><POLIZACONT/><SUPLENUMS/><CLIENSECS/><AGENTCLAS/><AGENTCODS/><NROITEMS/></Request>";
            else if ("lbaw_OVExigibleaPC".Equals(action))
                return "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><NIVEL1/><CLIENSEC1/><NIVEL2/><CLIENSEC2/><NIVEL3>PR</NIVEL3><CLIENSEC3>100029438</CLIENSEC3><TIPO_DISPLAY>2</TIPO_DISPLAY><ORIGEN>E</ORIGEN></Request>";
           
            
            return null;
        }
    }
}
