﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QBEDispatcher.MigratedComponentService;
using System.IO;
using System.Xml;

namespace ConsoleApplication1
{
    class Serializador
    {
        /// <summary>
        /// Serializa el objeto dado
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String serializar(Object obj)
        {
            Console.WriteLine("Serializando...");

            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(obj.GetType());

            StringWriter sw = new StringWriter();

            x.Serialize(sw, obj);

            return sw.ToString().Replace("composedResponse", "return");
        }

        /// <summary>
        /// Serializa en forma poco ortodoxa
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String crearXml(composedResponse obj)
        {
            Console.WriteLine("Serializador poco ortodoxo");
            String result = "<return><code>" + obj.code + "</code>";
            result += "<response>";

            foreach (XmlElement xml in obj.response.Any)
            {
                if (xml != null)
                {
                    if (xml.GetType() != typeof(XmlText))
                    {
                        result += xml.OuterXml;
                    }
                    else
                    {
                        result += xml.Value;
                    }
                }

            }


            result += "</response></return>";


            return result;
        }

    }
}
