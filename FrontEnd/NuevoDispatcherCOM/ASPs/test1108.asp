<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->

<%
Function Msg1010()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS><DOCUMTIP/><DOCUMNRO/><CLIENDES><![CDATA[ARMATI]]></CLIENDES><PRODUCTO/><POLIZA/><CERTI/><PATENTE/><MOTOR/><ESTPOL>TODAS</ESTPOL><QRYCONT/><CLICONT/><PRODUCONT/><POLIZACONT/><SUPLENUMS/><CLIENSECS/><AGENTCLAS/><AGENTCODS/><NROITEMS/></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_OVClientesConsulta", _
				 "lbaw_OVClientesConsulta.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1010. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1010 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1010, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1010 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1301()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS><NIVEL1/><CLIENSEC1/><NIVEL2/><CLIENSEC2/><NIVEL3/><CLIENSEC3/><FECDES>20130324</FECDES><FECHAS>20130424</FECHAS><MSGEST/><CONTINUAR/></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_OVSiniListadoDetalles", _
				 "lbaw_OVSiniListadoDetalles.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1301. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1301 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1301, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1301 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1427()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><DEFINICION>GetDeudaCobradaCanales.xml</DEFINICION><USUARCOD>EX009001L</USUARCOD><NIVELAS>PR</NIVELAS><CLIENSECAS>100223914</CLIENSECAS><NIVELCLA1/><NIVELCLA2/><CLIENSEC2/><NIVELCLA3/><CLIENSEC3/><FECHADESDE>19/2/2013</FECHADESDE><FECHAHASTA>19/3/2013</FECHAHASTA></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion", _
				 "lbaw_GetConsultaMQGestion.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1427. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1427 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1427 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1108()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "	<Request><USUARIO>EX009005L</USUARIO><NIVELAS>OR</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><FECDES>20130324</FECDES><FECHAS>20130424</FECHAS></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVOpeEmiTotales", _
				 "lbaw_OVOpeEmiTotales.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1108. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1108 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1108 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1403()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVDeudaVceTotales", _
				 "lbaw_OVDeudaVceTotales.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1403. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1403 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1403 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1110()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><PRODUCTO>AUI1</PRODUCTO><POLIZA>00150396</POLIZA><CERTI>03960009004226</CERTI></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVCoberturasDetalle", _
				 "lbaw_OVCoberturasDetalle.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1110. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1110 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1110 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>
<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1108</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1108() & "</TEXTAREA>"
%>
</body>
</html>
