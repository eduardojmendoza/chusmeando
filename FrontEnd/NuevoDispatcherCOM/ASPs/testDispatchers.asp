<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->

<%
Function Msg1010()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS><DOCUMTIP/><DOCUMNRO/><CLIENDES><![CDATA[ARMATI]]></CLIENDES><PRODUCTO/><POLIZA/><CERTI/><PATENTE/><MOTOR/><ESTPOL>TODAS</ESTPOL><QRYCONT/><CLICONT/><PRODUCONT/><POLIZACONT/><SUPLENUMS/><CLIENSECS/><AGENTCLAS/><AGENTCODS/><NROITEMS/></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_OVClientesConsulta", _
				 "lbaw_OVClientesConsulta.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1010. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1010 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1010, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1010 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1123()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><DEFINICION>LBA_1123_DetalleRiesgo.xml</DEFINICION><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>000001</POLIZSEC><CERTIPOL>0000</CERTIPOL><CERTIANN>0001</CERTIANN><CERTISEC>383390</CERTISEC><SUPLENUM>0</SUPLENUM></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("nbwsA_MQGenericoAIS", _
				 "nbwsA_MQGenericoAIS.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1123. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1123 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1301, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1123 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1301()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS><NIVEL1/><CLIENSEC1/><NIVEL2/><CLIENSEC2/><NIVEL3/><CLIENSEC3/><FECDES>20130324</FECDES><FECHAS>20130424</FECHAS><MSGEST/><CONTINUAR/></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_OVSiniListadoDetalles", _
				 "lbaw_OVSiniListadoDetalles.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1301. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1301 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1301, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1301 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1309()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><FECDES>20130324</FECDES><FECHAS>20130424</FECHAS></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_OVSiniListadoTotales", _
				 "lbaw_OVSiniListadoTotales.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1309. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1309 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1309, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1309 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1419()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><NIVEL1/><CLIENSEC1/><NIVEL2/><CLIENSEC2/><NIVEL3>PR</NIVEL3><CLIENSEC3>100029438</CLIENSEC3><TIPO_DISPLAY>2</TIPO_DISPLAY><ORIGEN>E</ORIGEN></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVExigibleaPC", _
				 "lbaw_OVExigibleaPC.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1419. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1419 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1419, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	Msg1419 = mobjXMLDoc.selectSingleNode("/Response").xml
	
End Function
' *****************************************************************************************
%>

<%
Function Msg1427()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><DEFINICION>GetDeudaCobradaCanales.xml</DEFINICION><USUARCOD>EX009001L</USUARCOD><NIVELAS>PR</NIVELAS><CLIENSECAS>100223914</CLIENSECAS><NIVELCLA1/><NIVELCLA2/><CLIENSEC2/><NIVELCLA3/><CLIENSEC3/><FECHADESDE>19/2/2013</FECHADESDE><FECHAHASTA>19/3/2013</FECHAHASTA></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion", _
				 "lbaw_GetConsultaMQGestion.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1427. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1427 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1427 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1108()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "	<Request><USUARIO>EX009005L</USUARIO><NIVELAS>OR</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><FECDES>20130324</FECDES><FECHAS>20130424</FECHAS></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVOpeEmiTotales", _
				 "lbaw_OVOpeEmiTotales.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1108. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1108 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1108 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1403()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>PR</NIVELAS><CLIENSECAS>100072343</CLIENSECAS></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVDeudaVceTotales", _
				 "lbaw_OVDeudaVceTotales.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1403. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1403 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1403 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1110()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><PRODUCTO>AUI1</PRODUCTO><POLIZA>00150396</POLIZA><CERTI>03960009004226</CERTI></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVCoberturasDetalle", _
				 "lbaw_OVCoberturasDetalle.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1110. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1110 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1535, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1110 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1525()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>000001</POLIZSEC><CERTIPOL>9000</CERTIPOL><CERTIANN>2015</CERTIANN><CERTISEC>955143</CERTISEC><SUPLENUM/><CAUSA>PRUEBA DE BAJA</CAUSA></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_BajaPropuesta", _
				 "lbaw_BajaPropuesta.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1525. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1525 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1525, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1525 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>

<%
Function Msg1540()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest ="<Request>" & _
				"<RAMOPCOD>AUS1</RAMOPCOD>" & _
				"<POLIZANN>0</POLIZANN>" & _
				"<POLIZSEC>1</POLIZSEC>" & _
				"<CERTIPOL>0150</CERTIPOL>" & _
				"<CERTIANN>0001</CERTIANN>" & _
				"<SUPLENUM>0</SUPLENUM>" & _
				"<POLVENCIANN>0000</POLVENCIANN>" & _
				"<POLVENCIMES>00</POLVENCIMES>" & _
				"<POLVENVIDIA>00</POLVENVIDIA>" & _
				"<FRANQCOD>01</FRANQCOD>" & _
				"<PQTDES>TODO RIESGO CON FRANQUICIA/NAC. $ 1.300.- / IMP. $ 3.500.</PQTDES>" & _
				"<PLANCOD>015</PLANCOD>" & _
				"<HIJOS1416>0</HIJOS1416>" & _
				"<HIJOS1729>0</HIJOS1729>" & _
				"<HIJOS1729ND>0</HIJOS1729ND>" & _
				"<ZONA>0001</ZONA>" & _
				"<CODPROV>1</CODPROV>" & _
				"<SUMALBA>245000.0</SUMALBA>" & _
				"<CLIENIVA>3</CLIENIVA>" & _
				"<SUMASEG>245000.0</SUMASEG>" & _
				"<CLUB_LBA>S</CLUB_LBA>" & _
				"<DESTRUCCION_80>N</DESTRUCCION_80>" & _
				"<CPAANO>2012</CPAANO>" & _
				"<CTAKMS>0</CTAKMS>" & _
				"<ESCERO>N</ESCERO>" & _
				"<TIENEPLAN>N</TIENEPLAN>" & _
				"<COND_ADIC>0</COND_ADIC>" & _
				"<ASEG_ADIC>N</ASEG_ADIC>" & _
				"<FH_NAC>19760801</FH_NAC>" & _
				"<SEXO>F</SEXO>" & _
				"<ESTCIV>S</ESTCIV>" & _
				"<SIFMVEHI_DES><![CDATA[A4 2.0 TDI (L/08) PLUS SP CU MULTITRONIC IMPORTADO (2008-2012)]]></SIFMVEHI_DES>" & _
				"<PROFECOD>000000</PROFECOD>" & _
				"<SIACCESORIOS>N</SIACCESORIOS>" & _
				"<REFERIDO>0</REFERIDO>" & _
				"<MOTORNUM>GRT56YRY56</MOTORNUM>" & _
				"<CHASINUM>546546546</CHASINUM>" & _
				"<PATENNUM>ESD432</PATENNUM>" & _
				"<AUMARCOD>00055</AUMARCOD>" & _
				"<AUMODCOD>00004</AUMODCOD>" & _
				"<AUSUBCOD>00033</AUSUBCOD>" & _
				"<AUADICOD>03177</AUADICOD>" & _
				"<AUMODORI>I</AUMODORI>" & _
				"<AUUSOCOD>1</AUUSOCOD>" & _
				"<AUVTVCOD>N</AUVTVCOD>" & _
				"<AUVTVDIA>0</AUVTVDIA>" & _
				"<AUVTVMES>0</AUVTVMES>" & _
				"<AUVTVANN>0</AUVTVANN>" & _
				"<VEHCLRCOD>15</VEHCLRCOD>" & _
				"<AUKLMNUM>15000</AUKLMNUM>" & _
				"<FABRICAN>2012</FABRICAN>" & _
				"<FABRICMES>1</FABRICMES>" & _
				"<GUGARAGE>S</GUGARAGE>" & _
				"<GUDOMICI>S</GUDOMICI>" & _
				"<AUCATCOD>14</AUCATCOD>" & _
				"<AUTIPCOD>0001</AUTIPCOD>" & _
				"<AUCIASAN/>" & _
				"<AUANTANN>5</AUANTANN>" & _
				"<AUNUMSIN>0</AUNUMSIN>" & _
				"<AUNUMKMT>0</AUNUMKMT>" & _
				"<AUUSOGNC>N</AUUSOGNC>" & _
				"<USUARCOD>EX009005L</USUARCOD>" & _
				"<SITUCPOL>O</SITUCPOL>" & _
				"<CLIENSEC>44932</CLIENSEC>" & _
				"<NUMEDOCU>1677676576</NUMEDOCU>" & _
				"<TIPODOCU>1</TIPODOCU>" & _
				"<DOMICSEC>1</DOMICSEC>" & _
				"<CUENTSEC>1</CUENTSEC>" & _
				"<COBROFOR>1</COBROFOR>" & _
				"<EDADACTU>36</EDADACTU>" & _
				"<COBERTURAS/>" & _
				"<ASEGURADOS/>" & _
				"<CAMP_CODIGO>0900</CAMP_CODIGO>" & _
				"<NRO_PROD>3233</NRO_PROD>" & _
				"<INS_DOMICDOM>01500 2010019  ENUEVO          </INS_DOMICDOM>" & _
				"<INS_DOMICPOB>PLAT                         </INS_DOMICPOB>" & _
				"<SUCURSAL_CODIGO>0001</SUCURSAL_CODIGO>" & _
				"<LEGAJO_VEND>9991</LEGAJO_VEND>" & _
				"<EMPRESA_CODIGO> 2</EMPRESA_CODIGO>" & _
				"<EMPL> E</EMPL>" & _
				"<LEGAJO_GTE>9992</LEGAJO_GTE>" & _
				"<EMPL_NOMYAPE>PLAT NUEVO                                        </EMPL_NOMYAPE>" & _
				"<CLIENTE_NOMYAPE>PRUEBITA ADRIANA</CLIENTE_NOMYAPE>" & _
				"<ACCESORIOS/>" & _
				"<ESTAINSP>2</ESTAINSP>" & _
				"<INSPECOD>0</INSPECOD>" & _
				"<INSPECTOR/>" & _
				"<INSPEADOM>S</INSPEADOM>" & _
				"<OBSERTXT/>" & _
				"<INSPEOBLEA/>" & _
				"<INSPEKMS>0</INSPEKMS>" & _
				"<CENTRCOD_INS>0044</CENTRCOD_INS>" & _
				"<CENTRDES>DRIVE IN CENTRO</CENTRDES>" & _
				"<RASTREO>0</RASTREO>" & _
				"<ALARMCOD/>" & _
				"<INSPEANN>0</INSPEANN>" & _
				"<INSPEMES>0</INSPEMES>" & _
				"<INSPEDIA>0</INSPEDIA>" & _
				"<NUMEDOCU_ACRE/>" & _
				"<TIPODOCU_ACRE>0</TIPODOCU_ACRE>" & _
				"<APELLIDO/>" & _
				"<NOMBRES/>" & _
				"<DOMICDOM/>" & _
				"<DOMICDNU/>" & _
				"<DOMICESC/>" & _
				"<DOMICPIS/>" & _
				"<DOMICPTA/>" & _
				"<DOMICPOB/>" & _
				"<DOMICCPO>0</DOMICCPO>" & _
				"<PROVICOD>0</PROVICOD>" & _
				"<PAISSCOD/>" & _
				"<BANCOCOD>0150</BANCOCOD>" & _
				"<COBROCOD>1</COBROCOD>" & _
				"<EFECTANN2>2013</EFECTANN2>" & _
				"<EFECTMES>07</EFECTMES>" & _
				"<EFECTDIA>03</EFECTDIA>" & _
				"<INSPEANN>0</INSPEANN>" & _
				"<INSPEMES>0</INSPEMES>" & _
				"<INSPEDIA>0</INSPEDIA>" & _
				"<ENTDOMSC>2</ENTDOMSC>" & _
				"<ANOTACIO><![CDATA[]]></ANOTACIO>" & _
				"<PRECIO_MENSUAL>2448.49</PRECIO_MENSUAL>" & _
				"<MODEAUTCOD>00055000040003303177I</MODEAUTCOD>" & _
				"<EFECTANN>2012</EFECTANN>" & _
				"<NACIMANN>1976</NACIMANN>" & _
				"<NACIMMES>08</NACIMMES>" & _
				"<NACIMDIA>01</NACIMDIA>" & _
				"<IVA>3</IVA>" & _
				"<IBB>0</IBB>" & _
				"<ESTADO>S</ESTADO>" & _
				"<KMSRNGCOD>15000</KMSRNGCOD>" & _
				"<SIGARAGE>S</SIGARAGE>" & _
				"<SINIESTROS>0</SINIESTROS>" & _
				"<GAS>N</GAS>" & _
				"<PROVI>1</PROVI>" & _
				"<LOCALIDADCOD>1005</LOCALIDADCOD>" & _
				"<CLUBLBA>S</CLUBLBA>" & _
				"<CLUBECO>S</CLUBECO>" & _
				"<GRANIZO>N</GRANIZO>" & _
				"<ROBOCONT>N</ROBOCONT>" & _
				"<DATOSPLAN>01501</DATOSPLAN>" & _
				"<CAMPACOD>0900</CAMPACOD>" & _
				"<CAMP_DESC><![CDATA[BAJA POR ALTA 2005]]></CAMP_DESC>" & _
				"<AGECLA>PR</AGECLA>" & _
				"<AGENTCLA>PR</AGENTCLA>" & _
				"<AGECOD>3233</AGECOD>" & _
				"<PORTAL>LBA_PRODUCTORES</PORTAL>" & _
				"<COBROTIP>ES</COBROTIP>" & _
				"<CUITNUME/>" & _
				"<RAZONSOC><![CDATA[]]></RAZONSOC>" & _
				"<CLIEIBTP>0</CLIEIBTP>" & _
				"<NROIIBB/>" & _
				"<LUNETA>N</LUNETA>" & _
				"<NROFACTU/>" & _
				"<DDJJ_STRING/>" & _
				"<DERIVACI>S</DERIVACI>" & _
				"<ALEATORIO/>" & _
				"<ESTAIDES>DERIVA A CENTRO DE INSPECCI�N</ESTAIDES>" & _
				"<DISPODES/>" & _
				"<INSTALADP>N</INSTALADP>" & _
				"<POSEEDISP>N</POSEEDISP>" & _
				"<AUTIPGAMA>B</AUTIPGAMA>" & _
				"<SN_NBWS>S</SN_NBWS>" & _
				"<EMAIL_NBWS>MAILDEPRUEBA@HOTMAIL.COM</EMAIL_NBWS>" & _
				"<SN_EPOLIZA>S</SN_EPOLIZA>" & _
				"<EMAIL_EPOLIZA>MAILDEPRUEBA@HOTMAIL.COM</EMAIL_EPOLIZA>" & _
				"<SN_PRESTAMAIL>S</SN_PRESTAMAIL>" & _
				"<PASSEPOL>576432</PASSEPOL>" & _
				"<MEDIOSCONTACTO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC/>" & _
				"<MEDCOCOD/>" & _
				"<MEDCODAT/>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC>0</MEDCOSEC>" & _
				"<MEDCOCOD>EMA</MEDCOCOD>" & _
				"<MEDCODAT>MAILDEPRUEBA@HOTMAIL.COM</MEDCODAT>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"<MEDIO>" & _
				"<MEDCOSEC>0</MEDCOSEC>" & _
				"<MEDCOCOD>EMA</MEDCOCOD>" & _
				"<MEDCODAT>MAILDEPRUEBA@HOTMAIL.COM</MEDCODAT>" & _
				"<MEDCOSYS/>" & _
				"</MEDIO>" & _
				"</MEDIOSCONTACTO>" & _
				"<CERTISEC>700019</CERTISEC>" & _
				"<TIPOOPERACION>8</TIPOOPERACION>" & _
				"<OPCION>01</OPCION>" & _
				"<FUNCION>1</FUNCION>" & _
				"<GENERARLOG/>" & _
				"<CLICLIENAP1><![CDATA[PRUEBITA]]></CLICLIENAP1>" & _
				"<AISCLICLIENNOM><![CDATA[ADRIANA]]></AISCLICLIENNOM>" & _
				"<CLICLIENNOM><![CDATA[ADRIANA]]></CLICLIENNOM>" & _
				"<CLICLIENTIP>00</CLICLIENTIP>" & _
				"<UIFCUIT/>" & _
				"<CLIDOMICCAL>PAR</CLIDOMICCAL>" & _
				"<CLIDOMICDOM><![CDATA[FLORIDA]]></CLIDOMICDOM>" & _
				"<CLIDOMICDNU>229</CLIDOMICDNU>" & _
				"<CLIDOMICESC/>" & _
				"<CLIDOMICPIS/>" & _
				"<CLIDOMICPTA/>" & _
				"<CLIDOMICPOB><![CDATA[CAPITAL FEDERAL]]></CLIDOMICPOB>" & _
				"<CLIDOMICCPO>1005</CLIDOMICCPO>" & _
				"<CLIDOMPROVICOD>1</CLIDOMPROVICOD>" & _
				"<CLIDOMPAISSCOD>00</CLIDOMPAISSCOD>" & _
				"<CUENTDC/>" & _
				"<CUENNUME/>" & _
				"<CONDUCTORES>" & _
				"<CONDUCTOR>" & _
				"<CONDUSEC>1</CONDUSEC>" & _
				"<CONDUTDO>1</CONDUTDO>" & _
				"<CONDUDOC>1677676576</CONDUDOC>" & _
				"<APELLIDOHIJO><![CDATA[PRUEBITA]]></APELLIDOHIJO>" & _
				"<NOMBREHIJO><![CDATA[ADRIANA]]></NOMBREHIJO>" & _
				"<SEXOHIJO>F</SEXOHIJO>" & _
				"<ESTADOHIJO>S</ESTADOHIJO>" & _
				"<NACIMHIJO>01/08/1976</NACIMHIJO>" & _
				"<EXCLUIDO>N</EXCLUIDO>" & _
				"</CONDUCTOR>" & _
				"</CONDUCTORES>" & _
				"<FESOLANN>2013</FESOLANN>" & _
				"<FESOLMES>7</FESOLMES>" & _
				"<FESOLDIA>3</FESOLDIA>" & _
				"<FEINGANN>2013</FEINGANN>" & _
				"<FEINGMES>7</FEINGMES>" & _
				"<FEINGDIA>3</FEINGDIA>" & _
				"<AGECLA>PR</AGECLA>" & _
				"<AGENTCLA>PR</AGENTCLA>" & _
				"<AGECOD>3233</AGECOD>" & _
				"<COBERTURAS>" & _
				"<COBERTURA>" & _
				"<COBERCOD>002</COBERCOD>" & _
				"<COBERORD>02</COBERORD>" & _
				"<CAPITASG>300000.00</CAPITASG>" & _
				"<CAPITIMP>300000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>004</COBERCOD>" & _
				"<COBERORD>03</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>009</COBERCOD>" & _
				"<COBERORD>05</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>013</COBERCOD>" & _
				"<COBERORD>07</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>003</COBERCOD>" & _
				"<COBERORD>09</COBERORD>" & _
				"<CAPITASG>6.00</CAPITASG>" & _
				"<CAPITIMP>6.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>905</COBERCOD>" & _
				"<COBERORD>17</COBERORD>" & _
				"<CAPITASG>20000.00</CAPITASG>" & _
				"<CAPITIMP>20000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>027</COBERCOD>" & _
				"<COBERORD>22</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>028</COBERCOD>" & _
				"<COBERORD>23</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>030</COBERCOD>" & _
				"<COBERORD>26</COBERORD>" & _
				"<CAPITASG>120000.00</CAPITASG>" & _
				"<CAPITIMP>120000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>031</COBERCOD>" & _
				"<COBERORD>27</COBERORD>" & _
				"<CAPITASG>2880000.00</CAPITASG>" & _
				"<CAPITIMP>2880000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>032</COBERCOD>" & _
				"<COBERORD>28</COBERORD>" & _
				"<CAPITASG>60000.00</CAPITASG>" & _
				"<CAPITIMP>60000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>033</COBERCOD>" & _
				"<COBERORD>29</COBERORD>" & _
				"<CAPITASG>60000.00</CAPITASG>" & _
				"<CAPITIMP>60000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>026</COBERCOD>" & _
				"<COBERORD>30</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>040</COBERCOD>" & _
				"<COBERORD>31</COBERORD>" & _
				"<CAPITASG>3000.00</CAPITASG>" & _
				"<CAPITIMP>3000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>041</COBERCOD>" & _
				"<COBERORD>32</COBERORD>" & _
				"<CAPITASG>245000.00</CAPITASG>" & _
				"<CAPITIMP>2450.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>039</COBERCOD>" & _
				"<COBERORD>33</COBERORD>" & _
				"<CAPITASG>30000.00</CAPITASG>" & _
				"<CAPITIMP>30000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"<COBERTURA>" & _
				"<COBERCOD>810</COBERCOD>" & _
				"<COBERORD>35</COBERORD>" & _
				"<CAPITASG>3000.00</CAPITASG>" & _
				"<CAPITIMP>3000.00</CAPITIMP>" & _
				"</COBERTURA>" & _
				"</COBERTURAS>" & _
				"<AUPAISCOD>00</AUPAISCOD>" & _
				"<AUPROVICOD>1</AUPROVICOD>" & _
				"<AUDOMCPACODPO>1005</AUDOMCPACODPO>" & _
				"<CENTRCOD>0010</CENTRCOD>" & _
				"<PLANPOLICOD>015</PLANPOLICOD>" & _
				"<ASEG_ADIC>N</ASEG_ADIC>" & _
				"<CLIDOMCPACODPO>1005</CLIDOMCPACODPO>" & _
				"<CLIDOMICPRE/>" & _
				"<CLIDOMICTLF>7657567567</CLIDOMICTLF>" & _
				"<SWACREPR>0</SWACREPR>" & _
				"<SUCURCOD>8888</SUCURCOD>" & _
				"<VIENEDMS>O</VIENEDMS>" & _
				"<PLANPAGOCOD>998</PLANPAGOCOD>" & _
				"<APOD>00<OCUPACOD/>" & _
				"</APOD>" & _
				"<SWAPODER>N</SWAPODER>" & _
				"<OCUPACODCLIE/>" & _
				"<OCUPACODAPOD/>" & _
				"<CLIENSECAPOD>0</CLIENSECAPOD>" & _
				"<DOMISECAPOD/>" & _
				"</Request>"
				
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_AisPutSolicAUS", _
				 "lbaw_AisPutSolicAUS.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1540. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1540 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecuci�n normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1540, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1540 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>
				
<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1540</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1540() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1010</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1010() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1123</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1123() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1301</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1301() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1309</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1309() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1419</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1419() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1427</h1>
<%
'	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1427() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1108</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1108() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1403</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1403() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1110</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1110() & "</TEXTAREA>"
%>
<h1>Invocando mensaje 1525</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1525() & "</TEXTAREA>"
%>

</body>
</html>
