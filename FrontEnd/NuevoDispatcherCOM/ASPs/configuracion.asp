<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="nuevoDispatcherCOM/incCoreActionsCOM.asp"-->
<div style="font-family: arial; font-size:12px">
<%
	Set dispatcher = Server.CreateObject("NuevoDispatcherCOM.Dispatcher")
	If dispatcher Is Nothing then
			Response.Write "<textarea cols=""80"" rows=""10""> Dispatcher is Nothing!!</textarea>" 
	End If
	
	
	Response.write "<strong>POR DEFECTO</strong><br/>"
	Response.write "Default ENDPOINT <br/>"
	Response.write dispatcher.endpoint()	
	Response.write "<br/><br/>"
	Response.write "CONFIGURACION<br/>"
	Response.write dispatcher.getConfiguracion()
	Response.write "<br/><br/>"
	rem -------------------------------------------------------------------------------------
	Response.write "<strong>ARCHIVO DE CONFIGURACION NuevoDispatcherCOM.config</strong><br/>"
	dispatcher.init "C:\\inetpub\\wwwroot\\nuevodispatchercom\\NuevoDispatcherCOM.config2"
	Response.write "Default ENDPOINT <br/>"
	Response.write dispatcher.endpoint()
	Response.write "<br/><br/>"
	Response.write "CONFIGURACION<br/>"
	Response.write dispatcher.getConfiguracion()
	
	
%>
</div>