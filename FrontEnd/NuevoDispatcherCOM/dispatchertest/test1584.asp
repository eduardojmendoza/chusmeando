<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->


<%
Function Msg1584()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>000001</POLIZSEC><CERTIPOL>0000</CERTIPOL><CERTIANN>0001</CERTIANN><CERTISEC>437693</CERTISEC><SUPLENUM>0</SUPLENUM><PATENTE/><MOTOR/></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_GetCertMercosur", _
				 "lbaw_GetCertMercosur.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1584. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1584 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1584, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1584 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>


<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1584</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1584() & "</TEXTAREA>"
%>
</body>
</html>
