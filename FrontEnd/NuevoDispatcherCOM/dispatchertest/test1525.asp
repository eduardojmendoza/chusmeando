<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->


<%
Function Msg1525()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request>		<USUARIO>EX009005L</USUARIO>		<RAMOPCOD>AUS1</RAMOPCOD>		<POLIZANN>00</POLIZANN>		<POLIZSEC>000001</POLIZSEC>		<CERTIPOL>9000</CERTIPOL>		<CERTIANN>2015</CERTIANN>		<CERTISEC>955143</CERTISEC>		<SUPLENUM/>		<CAUSA>PRUEBA DE BAJA</CAUSA>	</Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_BajaPropuesta", _
				 "lbaw_BajaPropuesta.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1525. Response: " & mvarResponse & ". Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1525 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1525, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1525 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>


<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1525</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1525() & "</TEXTAREA>"
%>
</body>
</html>