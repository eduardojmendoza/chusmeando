<%
' ------------------------------------------------------------------------------
' incCoreActions.asp
'
' Invoca al componente correspondiente al actionCode y mensaje, enviándolo por el
' viejo Dispatcher o el NuevoDispatcherCOM según esté cargado en el archivo de
' configuración del NuevoDispatcherCOM
'
' ------------------------------------------------------------------------------
%>
<%
'=======================================================
'--- Errores
Const xERR_UNEXPECTED = 1
Const xERR_REQUEST_XML_ERROR = 101
Const xERR_FORMAT_XML_ERROR = 102
Const xERR_ACTIONCODE_NOT_FOUND = 103
Const xERR_RESPONSE_XML_ERROR = 104
Const xERR_ACCOUNTNAME_ERROR = 105
'
Const xERR_TIMEOUT_RETURN = 501
'On Error Resume Next
'=======================================================
'
'
'=======================================================
'--- Objeto para manejar la Ejecución de Transacciones
Public mobjCmdProcessor
Set mobjCmdProcessor = Nothing
'=======================================================
'
'
'=======================================================
'--- Obtiene el Objeto para la Ejecución de Transacciones
Public Function cmdp_GetCmdProcessor()
	'
'''	If mobjCmdProcessor Is Nothing Then
		Set mobjCmdProcessor = Server.CreateObject("HSBC_ASP.CmdProcessor")
'''	End If
	'		
	Set cmdp_GetCmdProcessor = mobjCmdProcessor
End Function
'=======================================================
'--- Formatea el documento para la transacción
Public Function cmdp_FormatRequest(pvarActionCode, _
								   pvarSchemaFile, _
								   pvarBody)
	Dim wvarRequest
	Dim wvarSchema
	'
    'wvarRequest = "<?xml version='1.0' encoding='UTF-8'?>" 
    wvarRequest = "" 
    wvarSchema = "" 
	'
    If xXML_UseSchema Then
        wvarSchema = " xmlns='x-schema:" & xXML_PATH & pvarSchemaFile & "'"
    End If
    wvarRequest = wvarRequest & _
                  "<HSBC_MSG" & wvarSchema & ">" & _
                      "<HEADER>" & _
                          "<APPLICATION_CODE>" & _
							  xAPP_CODE & _
                          "</APPLICATION_CODE>" & _
                          "<ACTION_CODE>" & _
							  pvarActionCode & _
                          "</ACTION_CODE>" & _
                      "</HEADER>" & _
                      "<BODY>" & _
						   pvarBody & _
                      "</BODY>" & _
                  "</HSBC_MSG>"
	'		
	cmdp_FormatRequest = wvarRequest
End Function
'=======================================================
'
'
'=======================================================
'--- Realiza la Ejecución de Transacciones
Public Function cmdp_ExecuteTrn(pvarActionCode, _
								pvarSchemaFile , _
								pvarRequest, _
								pvarResponse)
	Dim wobjCmdProcessor
	Dim wvarRequest
	Dim wvarExecReturn
	Dim wvarMsg
	Dim strUrl 
	Dim strxmlResponse 
	Dim mobjXMLDoc 
	Dim dispatcher 
	Dim unusedResponse
	Dim pvarActionCodeExtended
	'Obtengo la DEFINICION si existe
	Dim mobjXMLRequest
	Dim wDefinicion
	Dim pNodeDefinicion
	Response.Write "<textarea cols=""80"" rows=""5"">" & "Request:" & pvarRequest  & "</textarea>" 

	Set mobjXMLRequest = Server.CreateObject("MSXML2.DOMDocument")	
	mobjXMLRequest.async = False
	Call mobjXMLRequest.loadXML(pvarRequest)
	Response.Write "<textarea cols=""80"" rows=""3"">" & "mobjXMLRequest:" & mobjXMLRequest.xml  & "</textarea>" 

	Set pNodeDefinicion = mobjXMLRequest.selectSingleNode("/Request/DEFINICION")
	If pNodeDefinicion Is Nothing Then
		pvarActionCodeExtended = pvarActionCode
	Else
		wDefinicion = trim(pNodeDefinicion.text)
		pvarActionCodeExtended = pvarActionCode & ";" & wDefinicion
	End If
	Response.Write "<textarea cols=""80"" rows=""1"">" & "pvarActionCodeExtended:" & pvarActionCodeExtended  & "</textarea>" 

	Set dispatcher = Server.CreateObject("NuevoDispatcherCOM.Dispatcher")
	'Set dispatcher = CreateObject("NuevoDispatcherCOM.Dispatcher")
	If dispatcher Is Nothing then
			Response.Write "<textarea cols=""80"" rows=""10""> Dispatcher is Nothing!!</textarea>" 
	End If
	
	dispatcher.isMensajeMigrado(pvarActionCodeExtended)
	
	response.write 	"SIMULADO_OK"
	cmdp_ExecuteTrn = wvarExecReturn
	Set wobjCmdProcessor = Nothing
End Function
'=======================================================
Function EnviarPorNuevoDispatcher( pvarActionCode )

Dim ActionCodes, Results


ActionCodes = Array("lbaw_GetConsultaMQ_TEST","lbaw_GetConsultaMQ","lbaw_OVClientesConsulta","lbaw_OVDatosGralCliente","lbaw_OVEndososDetalles","lbaw_OVEndososPrima","lbaw_OVOpePendTotales","lbaw_OVOpePendDetalles","lbaw_OVOpeEmiTotales","lbaw_OVOpeEmiDetalles","lbaw_OVCoberturasDetalle","lbaw_OVSiniListadoDetalles","lbaw_OVSiniConsulta","lbaw_OVSiniDetalle","lbaw_OVSiniDatosAdic","lbaw_OVSiniGestiones","lbaw_OVSiniConsPagos","lbaw_OVSiniListadoTotales","lbaw_OVExigibleTotales","lbaw_OVExigibleDetalles","lbaw_OVExigibleDetPoliza","lbaw_OVDeudaVceTotales","lbaw_OVDeudaVceDetalles","lbaw_OVCartasReclaTotales","lbaw_OVDetalleRecibo","lbaw_OVAnulxPagoTotales","lbaw_OVSitCobranzas","lbaw_OVDeudaVceDetPoliza","lbaw_OVLOGetCantRecibos","lbaw_OVLOGetRecibos","lbaw_OVLOLiquixProd")
'Si quiero mandar todo por el viejo dispatcher uso:
'ActionCodes = Array()

' 1 = vbTextCompare
Results = Filter(ActionCodes, pvarActionCode, True, 1)
' Para ver si vino vacio el array uso:
' An array with no elements has an upper bound of -1, and the one returned by Filter() is no exception.
If (( UBound(Results) + 1) = 0 ) Then
	EnviarPorNuevoDispatcher = False
Else
	EnviarPorNuevoDispatcher = True
End If

End Function

'=======================================================

function escribirError (pvarTipo, pvarActionCode,pvarRequest, pvarRes, pvarMsg)
'pvarTipo:	1 - Error Inesperado
'			2 - Error Controlado

dim wvarArchivo
dim wvarArchivoEnt
dim wvarPath
dim wvarRutaComp
dim wvarRutaCompEnt 
dim wvarVirtual
dim wobjFso
dim wobjStr

wvarArchivo = "result.xml"
wvarArchivoEnt = "request.xml"
wvarVirtual = "."
wvarPath = server.MapPath(wvarVirtual)
wvarRutaComp = wvarPath & "\" & wvarArchivo
wvarRutaCompEnt = wvarPath & "\" & wvarArchivoEnt

if pvarTipo = 1 then

	set wobjFso = server.CreateObject("Scripting.FileSystemObject")
	Set wobjStr = wobjFso.CreateTextFile(wvarRutaComp, True)


	if trim(pvarMsg) <> "" then
		wobjStr.write pvarMsg
	else
		wobjStr.write "<Response>Sin parametros de salida</Response>"
	end if
	
	wobjStr.close
	set wobjStr = nothing

	'wvarArchivoEnt
	Set wobjStr = wobjFso.CreateTextFile(wvarRutaCompEnt, True)
	
	if trim(pvarRequest) <> "" then
		wobjStr.write pvarRequest
	else
		wobjStr.write "<Request>Sin parametros de entrada</Request>"
	end if
	
	wobjStr.close
	set wobjStr = nothing

	set wobjFso = nothing

end if

%>
<script language="JavaScript" src="/its/includes/rollOver.js"></script>
<link rel="stylesheet" href="../styles/hsbcdotcom.css">
<table width="95%" border="0" align="center" cellspacing="0" cellpadding="0" height="17">
  <tr bgcolor="#333399"> 
    <td width="2%" align="left" valign="top" rowspan="3" height="16" class="sHead">&nbsp;</td>
    <td width="23%" height="16" bgcolor="#333399" class="sHead"> 
      <div align="left"></div>
    </td>
    <td width="50%" height="16" bgcolor="#333399" class="sHead"> 
      <div align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">Error 
        en la ejecuci&oacute;n de un componente</font></b></div>
    </td>
    <td width="23%" height="16" bgcolor="#333399" class="sHead"> 
      <div align="right"></div>
    </td>
    <td width="2%" align="right" valign="top" rowspan="3" height="16" class="sHead">&nbsp;</td>
  </tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="ivory" class="dsp">
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td align="center"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="3">
      <%=pvarRes%></font></b></td>
  </tr>
  <tr> 
    <td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">
      Action Code: <%=pvarActionCode%></font></td>
  </tr>
  <tr> 
    <td onclick="JavaScript:window.open('<%=wvarVirtual & "/" & wvarArchivoEnt%>',null,'width = 400,height = 400,directories = no,fullscreen = no,location =no,menubar =no,resizable =yes,scrollbars = yes,status = no,titlebar = no,toolbar = no,top=' + ((screen.height-200)/2) + ',left=' + ((screen.width-400)/2));" style="cursor:hand" align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Ver XML de Entrada 
      </font></td>
  </tr>
  <tr> 
    <%if pvarTipo=2 then%>
    <td align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Mensaje: 
      <%= " " & pvarMsg%></font></td>
    <%else%>
    <td onclick="JavaScript:window.open('<%=wvarVirtual & "/" & wvarArchivo%>',null,'width = 400,height = 400,directories = no,fullscreen = no,location =no,menubar =no,resizable =yes,scrollbars = yes,status = no,titlebar = no,toolbar = no,top=' + ((screen.height-200)/2) + ',left=' + ((screen.width-400)/2));" style="cursor:hand" align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Ver XML de Salida 
      </font></td>
    <%end if%>
  </tr>
  <tr>
    <td align="center"><a ID=Salir LANGUAGE=javascript onclick="JavaScript:history.back();"
   onMouseOut='MM_swapImgRestore()' onMouseOver=MM_swapImage('Image12','','/its/imagenes/botonera/add40_ovr.gif',1)> 
      <img name="Image12" src="/its/imagenes/botonera/add40_up.gif" width="40" height="40" alt="Aceptar" border="0"> 
      </a></td>
  </tr>
</table>
<%
end function
%>