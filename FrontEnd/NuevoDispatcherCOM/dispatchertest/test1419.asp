<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->


<%
Function Msg1419()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>GO</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><NIVEL1/><CLIENSEC1/><NIVEL2/><CLIENSEC2/><NIVEL3>PR</NIVEL3><CLIENSEC3>100029438</CLIENSEC3><TIPO_DISPLAY>2</TIPO_DISPLAY><ORIGEN>E</ORIGEN></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVExigibleaPC", _
				 "lbaw_OVExigibleaPC.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1419. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1419 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1419, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1419 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>


<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1419</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1419() & "</TEXTAREA>"
%>
</body>
</html>
