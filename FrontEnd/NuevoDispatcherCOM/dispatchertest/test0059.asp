<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->


<%
Function Msg0059()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request>		<DEFINICION>0059_CotizadorEndosos.xml</DEFINICION>		<CLIENSEC>101879610</CLIENSEC>		<NACIMANN>1966</NACIMANN>		<NACIMMES>05</NACIMMES>		<NACIMDIA>30</NACIMDIA>		<CLIENSEX>F</CLIENSEX>		<CLIENEST>D</CLIENEST>		<CLIENIVA>3</CLIENIVA>		<COBROTIP>MC</COBROTIP>		<COBROCOD>4</COBROCOD>		<EMISIANN>2013</EMISIANN>		<EMISIMES>04</EMISIMES>		<EMISIDIA>15</EMISIDIA>		<EFECTANN>2013</EFECTANN>		<EFECTMES>05</EFECTMES>		<EFECTDIA>01</EFECTDIA>		<PRODUCTOR>2000</PRODUCTOR>		<PLANCOD>16</PLANCOD>		<FRANQCOD>04</FRANQCOD>		<SUMAASEG>6050000</SUMAASEG>		<SUMALBA>6050000</SUMALBA>		<AUMARCOD>00010</AUMARCOD>		<AUMODCOD>00034</AUMODCOD>		<AUSUBCOD>00006</AUSUBCOD>		<AUADICOD>02387</AUADICOD>		<AUMODORI>N</AUMODORI>		<AUUSOCOD>1</AUUSOCOD>		<AUKLMNUM/>		<FABRICAN>2010</FABRICAN>		<GUGARAGE>S</GUGARAGE>		<AUNUMSIN>0</AUNUMSIN>		<AUUSOGNC>N</AUUSOGNC>		<CLUBLBA>S</CLUBLBA>		<LUNETA>4</LUNETA>		<LUNDISPO/>		<PREMIER>N</PREMIER>		<GRANIZO>S</GRANIZO>		<ROBOCONT>N</ROBOCONT>		<CLUBECO>N</CLUBECO>		<PREDISPO/>		<PAISSCOD>00</PAISSCOD>		<PROVICOD>2</PROVICOD>		<POSTACOD>1828</POSTACOD>		<CODIZONA>38</CODIZONA>		<HIJOS/>		<ACCESORIOS>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>			<ACCESORIO>				<AUVEASUM>0</AUVEASUM>				<AUVEADES/>				<AUVEADEP/>			</ACCESORIO>		</ACCESORIOS>		<ACCESORIOSADIC/>		<COTNRO>001411665</COTNRO>		<SUMAMIN>100</SUMAMIN>		<CAMPANAS>			<CAMPANA>				<CAMPACOD>0221</CAMPACOD>			</CAMPANA>		</CAMPANAS>		<SINCONTROL>S</SINCONTROL>		<RIESGSEC>2832448</RIESGSEC>		<PRECIO>0</PRECIO>		<ACCESORIOSADIC/>		<AUES0KLM>S</AUES0KLM>		<SWDT80/>		<SUPLENUM>0</SUPLENUM>		<COBERTURAS/>	</Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_CotEndosos", _
				 "lbaw_CotEndosos.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 0059. Response: " & mvarResponse & ". Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg0059 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 0059, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg0059 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>


<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 0059</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg0059() & "</TEXTAREA>"
%>
</body>
</html>
