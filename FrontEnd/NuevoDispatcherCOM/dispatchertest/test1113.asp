<!--

Usado para probar la invocacion a dos disptachers.
Llama al mensaje 1535 de test y muestra la respuesta en una textarea.
Despues llama al 1010 y hace lo mismo

Ver el incCoreActions para definir cual va por el nuevo dispatcher y cual por el viejo.
Ademas ajustar el URL del WebService

-->

<!--#include virtual="incCoreActions.asp"-->


<%
Function Msg1113()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><USUARIO>EX009005L</USUARIO><NIVELAS>OR</NIVELAS><CLIENSECAS>100029438</CLIENSECAS><FECDES>20130612</FECDES><FECHAS>20130812</FECHAS></Request>"
	
	On Error Resume Next
	Call cmdp_ExecuteTrn("lbaw_OVAvisoVtoPolTotales", _
				 "lbaw_OVAvisoVtoPolTotales.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1113. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1113 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1113, no encuentro el Estado.resultado</b>"	
		Response.End
	End If
	
	Msg1113 = mobjXMLDoc.selectSingleNode("/Response").xml
End Function
' *****************************************************************************************
%>


<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1113</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1113() & "</TEXTAREA>"
%>
</body>
</html>
