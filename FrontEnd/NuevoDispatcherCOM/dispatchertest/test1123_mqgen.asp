
<!--#include virtual="incCoreActions.asp"-->

<%
Function Msg1123()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><DEFINICION>1123_OVDetalleCoberXProd.xml</DEFINICION><USUARCOD>PR20485270</USUARCOD><RAMOPCOD>ICO1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>168841</POLIZSEC><CERTIPOL>8841</CERTIPOL><CERTIANN>0012</CERTIANN><CERTISEC>000001</CERTISEC><SUPLENUM>0</SUPLENUM></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", _
				 "lbaw_GetConsultaMQ", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1123. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1123 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1123, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1123 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1123</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1123() & "</TEXTAREA>"
%>
</body>
</html>