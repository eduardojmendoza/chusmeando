﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using log4net;

namespace NuevoDispatcherCOM.eventLog
{
    class EventLogger
    {
        static readonly ILog _log = LogManager.GetLogger(typeof(EventLogger));
        private static short errorCategory = 3;
        private static short warningCategory = 2;
        private static short infoCategory = 1;
        private static string sSource = "Nuevo Dispatcher COM+";
        private static string sLog = "Application";
        private static int CANT_MAX_CARACTERES_MENSAJE = 31720;

        private static void crearEventLog()
        {
            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
                _log.Debug("Crea event source");
            }
        }


        /// <summary>
        /// Registra un error en el EventViewer sin importar cómo esté seteado el archivo de configuración.
        /// Utilizar únicamne
        /// </summary>
        /// <param name="mensaje"></param>
        public static void error(String mensaje)
        {            
            EventLog.WriteEntry(sSource, mensaje, EventLogEntryType.Error, 300, errorCategory);
        }

      

        public static void error(String mensaje, Configuracion conf)
        {
            
            writeEntry(mensaje, EventLogEntryType.Error, 300, errorCategory, conf);
        }


        public static void warn(String mensaje, Configuracion conf)
        {
            writeEntry(mensaje, EventLogEntryType.Warning, 200, warningCategory, conf);
        }


        public static void info(String mensaje, Configuracion conf)
        {
            writeEntry(mensaje, EventLogEntryType.Information, 100, infoCategory, conf);
        }




        private static void writeEntry(String mensaje, EventLogEntryType tipo, Int32 identificador, short categoria, Configuracion configuracion)
        {
            try
            {
                if (isLoggingActivo(tipo, configuracion))
                {
                    _log.Debug("Mensaje: " + mensaje);
                    crearEventLog();
                    string msg = mensajeFormateado(mensaje);
                    EventLog.WriteEntry(sSource, msg, tipo, identificador, categoria);
                }
            }
            catch (Exception e)
            {

                error(e.Message);
            }
        }

        private static bool isLoggingActivo(EventLogEntryType tipo, Configuracion configuracion)
        {
            try
            {
                String eventLogLevel = configuracion.getAppSettingsNameValueCollection()["eventLogLevel"].ToString();
                _log.Debug(eventLogLevel);

                if ("ALL".Equals(eventLogLevel))
                    return true;
                else if ("INFO".Equals(eventLogLevel))
                    return true;
                else if ("WARN".Equals(eventLogLevel) && (EventLogEntryType.Error.Equals(tipo) || EventLogEntryType.Warning.Equals(tipo)))
                    return true;
                else if ("ERROR".Equals(eventLogLevel) && EventLogEntryType.Error.Equals(tipo))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                error("Ocurrio un error al intentar obtener el parametro eventLogActive del archivo de configuracion. " +
                    "Se considera ALL ese valor. Defina la propiedad y utilice valores INFO ERROR ALL");
                return true;
            }
            
        }

        public static String mensajeFormateado(String mensaje)
        {
            if (mensaje.Length > CANT_MAX_CARACTERES_MENSAJE)
                return (mensaje.Substring(0, CANT_MAX_CARACTERES_MENSAJE) + " [Mensaje Truncado]");
            else
                return mensaje;
        }
    }
}
