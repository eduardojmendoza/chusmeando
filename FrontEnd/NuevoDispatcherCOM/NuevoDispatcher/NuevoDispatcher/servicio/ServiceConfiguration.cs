﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Reflection;
using System.ServiceModel;
using NuevoDispatcherCOM.MCS;
using log4net;


namespace NuevoDispatcherCOM
{
    class ServiceConfiguration
    {
        static readonly int SEND_TIMEOUT = 300;
        static readonly ILog _log = LogManager.GetLogger(typeof(ServiceConfiguration));

        Configuracion configuracion;

        public ServiceConfiguration()
        {
            configuracion = new Configuracion();
        }

        public ServiceConfiguration(Configuracion conf)
        {
            configuracion = conf;

        }
        
        public String getDefaultEndPointAddressString()
        {
            String defaultEndpoint = configuracion.getAppSettingsNameValueCollection()["defaultEndpoint"];

            String result = configuracion.getEndpointsNameValueCollection()[defaultEndpoint];

            _log.Debug("Obtiene default endpoint: " + result);

            return result;


        }


        public BasicHttpBinding getBinding()
        {
            BasicHttpSecurityMode securitymode = BasicHttpSecurityMode.None;
            BasicHttpBinding binding = new BasicHttpBinding(securitymode);
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = 2147483647;
            //binding.ReceiveTimeout = new TimeSpan(3*TimeSpan.TicksPerSecond);
            
            _log.Debug("sendTimeout: " + SEND_TIMEOUT);
            binding.SendTimeout = new TimeSpan(0, 0, 0, SEND_TIMEOUT, 0);            
            //binding.OpenTimeout = new TimeSpan(10 * TimeSpan.TicksPerSecond);
            


            XmlDictionaryReaderQuotas myReaderQuotas = new XmlDictionaryReaderQuotas();
            myReaderQuotas.MaxStringContentLength = 2147483647;
            myReaderQuotas.MaxArrayLength = 2147483647;
            myReaderQuotas.MaxBytesPerRead = 2147483647;
            myReaderQuotas.MaxDepth = 2147483647;
            myReaderQuotas.MaxNameTableCharCount = 2147483647;            
            binding.GetType().GetProperty("ReaderQuotas").SetValue(binding, myReaderQuotas, null);


            

            return binding;
        }

        internal EndpointAddress getDefaultEndpointAddress()
        {

            _log.Debug("getDefaultEndpointAddress");

            Uri uri = getUri(getDefaultEndPointAddressString());

            return new EndpointAddress(uri);
        }


        internal EndpointAddress getEndpointAddress(string endpoint)
        {
            _log.Debug("getEndpointAddress: " + endpoint);

            if ("".Equals(endpoint)) endpoint = getDefaultEndPointAddressString();
            
            Uri uri = getUri(endpoint);

            return new EndpointAddress(uri);
        }

        private Uri getUri(String endpoint)
        {
            _log.Debug("Endpoint: " + endpoint);
            Uri result = new Uri(endpoint);

            return result;
        }

        internal MigratedComponentServiceClient getDefaultService()
        {
            EndpointAddress eAddress = getDefaultEndpointAddress();
            return new MigratedComponentServiceClient(getBinding(), eAddress);
        }


        internal MigratedComponentServiceClient getService()
        {
            EndpointAddress eAddress = getEndpointAddress(getDefaultEndPointAddressString());
            return new MigratedComponentServiceClient(getBinding(), eAddress);
        }

        internal MigratedComponentServiceClient getService(string endpoint)
        {
            EndpointAddress eAddress = getEndpointAddress(endpoint);
            return new MigratedComponentServiceClient(getBinding(), eAddress);
        }


    }
}
