﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel;
using System.Xml;
using NuevoDispatcherCOM.MCS;
using log4net;
using System.Net;
using System.IO;
using System.ServiceModel.Channels;

namespace NuevoDispatcherCOM.util
{
    /// <summary>
    /// Se encarga de procesar una excepción. Serializa la excepción para luego generar un responsedRequest y poder responder la serialización de este objeto.
    /// </summary>
    class GeneradorMensajeError
    {
        private Serializador serializador = new Serializador();
        private Deserializador deserializador = new Deserializador();
        static readonly ILog _log = LogManager.GetLogger(typeof(GeneradorMensajeError));
        
        private string MENSAJE_ERROR_INESPERADO = "Ocurrió un error inesperado.";
        private string ERROR_INTEGRACION = "Ocurrió un error de integración en la ejecución de la operación";
        private string ERROR_CONFIGURACION = "Existe un error en el archivo de configuración del dispatcher.";
        
        public GeneradorMensajeError() { }

        public String generarMensajeError(Exception e) {
            try
            {
                //Logueo la excepción cualquiera fuese.
                _log.Error(e.Message, e);
                throw e;
            }
            catch (ConfigurationErrorsException)
            {
                //Problemas en el archivo de configuración (NuevoDispatcherCOM.dll.config)
                composedResponse cr = generarComposedResponseError(1, ERROR_CONFIGURACION);

                return serializador.serializar(cr);

            }           
            catch (FaultException fe)
            {               
                try
                {

                    MessageFault msgFault = fe.CreateMessageFault();

                    StringBuilder sb = new StringBuilder();
                    XmlWriter writer = XmlWriter.Create(sb);
                    writer.WriteStartDocument();
                    writer.WriteStartElement("fault");
                    writer.WriteElementString("faultCode", msgFault.Code.Name);
                    writer.WriteElementString("faultString", msgFault.Reason.Translations.Single().Text);
                    writer.WriteElementString("faultActor", msgFault.Actor);
                    if (msgFault.HasDetail)
                    {
                        var detail = msgFault.GetDetail<XmlElement>();
                        writer.WriteElementString("Detail", detail.OuterXml);
                    }

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Close();

                    _log.Debug(sb.ToString());

                    //composedResponse cr = generarComposedResponseError(500, e, ERROR_INTEGRACION);
                    composedResponse cr = generarComposedResponseError(500, ERROR_INTEGRACION, sb.ToString());
                    
                    return serializador.serializar(cr);

                }
                catch (Exception ex)
                {
                    _log.Error("Error al generar composedResponse", ex);

                    //Solo para que en caso de que ocurriera alguna excepción al armar el response se captura la excepción
                    composedResponse cr = generarComposedResponseError(1, "Ocurrió un fault inesperado.");

                    return serializador.serializar(cr);
                }
            }
            
            catch (Exception)
            {
                ///Cuando no sabemos de qué se trata el error.                 
                try
                {                    
                    composedResponse cr = generarComposedResponseError(1, e);
                    return serializador.serializar(cr);
                }
                catch (Exception ex)
                {
                    _log.Error("Error al generar composedResponse", ex);

                    //Solo para que en caso de que ocurriera alguna excepción al armar el response se captura la excepción                   
                    composedResponse cr = generarComposedResponseError(1, MENSAJE_ERROR_INESPERADO);

                    return serializador.serializar(cr);
                }

            }

        }



        /// <summary>
        /// Genera el contenido del composedResponse
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        private composedResponse generarComposedResponseError(int codigo, String mensaje)
        {

            composedResponse cr = new composedResponse();
            //Code
            cr.code = codigo;

            //Response
            AnyXmlElement anyXmlElement = new AnyXmlElement();

            anyXmlElement.Any = new XmlElement[1];

            anyXmlElement.Any = deserializador
                .getXMLElements(String.Format("<root><Estado resultado=\"false\" mensaje=\"{0}\" /></root>", mensaje));
           
            cr.Response = anyXmlElement;


            _log.Debug("Serialización response: " + serializador.serializar(cr));
             return cr;
            

        }


        /// <summary>
        /// Genera el contenido del composedResponse
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        private composedResponse generarComposedResponseError(int codigo, Exception ex)
        {
            return generarComposedResponseError(codigo, ex, null);
        }


        /// <summary>
        /// Genera un mensaje de error a partir de la excepción. Si mensajeError != null entonces usará ese mensaje como descripción
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="ex"></param>
        /// <param name="mensajeError"></param>
        /// <returns></returns>
        private composedResponse generarComposedResponseError(int codigo, Exception ex, String mensajeError)
        {

            composedResponse cr = new composedResponse();
            //Code
            cr.code = codigo;

            //Response
            AnyXmlElement anyXmlElement = new AnyXmlElement();

            anyXmlElement.Any = new XmlElement[1];
            _log.Debug("Serializando...");
            //String excepcionSerializada = new ExceptionXElement(ex, true).ToString(); //se omite stacktrace
            String excepcionSerializada = new ExceptionXElement(ex, false).ToString();
            _log.Debug("Serializando error: " + excepcionSerializada);

            //deserializa para armar el composedResponse
            anyXmlElement.Any = deserializador
                .getXMLElements(String.Format("<root><Estado resultado=\"false\" mensaje=\"{0}\" />{1}</root>", 
                        (mensajeError == null || "".Equals(mensajeError)) ? MENSAJE_ERROR_INESPERADO : mensajeError, 
                        excepcionSerializada));

            cr.Response = anyXmlElement;


            _log.Debug("Serialización response: " + serializador.serializar(cr));
            
            return cr;

        }


        /// <summary>
        /// Genera un composedResponse como un mensaje de error
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="mensaje">un mensaje de texto</param>
        /// <param name="fault">formato xml</param>
        /// <returns></returns>
        private composedResponse generarComposedResponseError(int codigo, String mensaje, String fault)
        {

            composedResponse cr = new composedResponse();
            //Code
            cr.code = codigo;

            //Response
            AnyXmlElement anyXmlElement = new AnyXmlElement();

            anyXmlElement.Any = new XmlElement[1];
            _log.Debug("Serializando...");            
            
            _log.Debug("Serializando error: " + fault);

            anyXmlElement.Any = deserializador
                .getXMLElements(String.Format("<root><Estado resultado=\"false\" mensaje=\"{0}\" />{1}</root>", mensaje, fault.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>","")));

            cr.Response = anyXmlElement;


            _log.Debug("Serialización response: " + serializador.serializar(cr));
            return cr;


        }


        /// <summary>
        /// TODO: obtener esquema de manera correcta. Ignorar, solo para prueba inicial
        /// </summary>
        /// <param name="codigo"></param>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        private String mensajeErrorIntegracion(String mensaje)
        {
            String inicioReturnTag = "<?xml version=\"1.0\" encoding=\"utf-16\"?> " +
            "<return xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";

            String finReturnTag = "</return>";

            String contentTag = 
                String.Format("<code>1</code><Response><Estado resultado=\"false\" mensaje=\"{0}\" />{1}</Response>",  ERROR_INTEGRACION, mensaje);

            return inicioReturnTag + contentTag + finReturnTag;

        }
       

    }

   


}
