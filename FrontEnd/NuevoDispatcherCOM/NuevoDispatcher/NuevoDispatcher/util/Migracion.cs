﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;
using log4net;
using System.Xml;
using System.IO;
using System.Collections;

namespace NuevoDispatcherCOM
{
    /// <summary>
    /// Se encarga de mantener una lista de action codes actualizados (aún si se modifica el archivo de mensajes migrados)
    /// </summary>    
    class Migracion
    {
        static readonly ILog _log = LogManager.GetLogger(typeof(Migracion));
        Configuracion configuracion;

        private IDictionary<string,string> keyValues = new Dictionary<string, string>();

        private DateTime mensajesMigradosUltimaModificacion = DateTime.MinValue;

       

        public Migracion(Configuracion conf)
        {
             configuracion = conf;
        }

        public bool mensajeMigrado(String mensaje)
        {
            try
            {
                

                sincronizarDictionario("mensajesMigradosPath");

                return keyValues.Keys.Contains(mensaje);


            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }

        }

        /// <summary>
        /// Lee un archivo y lo convierte en una lista
        /// </summary>
        /// <param name="configAppParam"></param>
        /// <returns></returns>
        private ArrayList convertMensajesToArrayListFromParam(String configAppParam)
        {
            String path = configuracion.getAppSettingsNameValueCollection()[configAppParam];

            keyValues.Clear();
            _log.Debug(DateTime.Now);
            try
            {
                if (File.Exists(path))
                {

                    StreamReader objReader = new StreamReader(path);
                    string sLine = "";
                    ArrayList arrText = new ArrayList();

                    while (sLine != null)
                    {
                        sLine = objReader.ReadLine();
                        if (sLine != null)
                            arrText.Add(sLine);
                        
                    }
                    objReader.Close();
                    return arrText;
                }
                else
                    throw new Exception("No existe archivo en path: " + path);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }

            

        }


        /// <summary>
        /// Sincroniza la información del diccionario con el archivo que indique la propiedad del parámetro pasado
        /// </summary>
        /// <param name="configAppParam"></param>
        private void sincronizarDictionario(String configAppParam)
        {
            _log.Debug("************ LLAMADO A SINCRONIZACION *************");

            //solo sincroniza luego de una modificación o cuando el objeto keyValues no tiene valores
            if (keyValues.Count > 0 &&  !esArchivoModificado(configAppParam)) return;

            _log.Debug("************ SINCRONIZANDO *************");
            String path = configuracion.getAppSettingsNameValueCollection()[configAppParam];

            mensajesMigradosUltimaModificacion = DateTime.Now;


            keyValues.Clear();
            
            try
            {
                if (File.Exists(path))
                {

                    StreamReader objReader = new StreamReader(path);
                    string sLine = "";
                    

                    while (sLine != null)
                    {
                        sLine = objReader.ReadLine();
                        if (sLine != null)
                        {
                            //Por cada línea se esperan 1 o dos valores separados por espacio
                            string[] kvalues = sLine.Split(' ');

                            if (kvalues.Length > 0)
                            {

                                string key = kvalues[0];

                                if (!"".Equals(key))
                                {

                                    string endpoint = "";
                                    if (kvalues.Length > 1 && !"".Equals(kvalues[1].Trim()))
                                    {
                                        endpoint = kvalues[1].Trim();
                                        _log.Debug(key + " -> setea nuevo alias " + endpoint);
                                    }

                                    KeyValuePair<string, string> keyValue = new KeyValuePair<string, string>(key, endpoint);
                                    keyValues.Add(keyValue);

                                }
                            }

                            
                        }
                    }
                    objReader.Close();
                    
                }
                else
                    throw new Exception("No existe archivo en path: " + path);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }



        }


        public bool mensajeParaDebug(string actionCode)
        {
            try
            {
                //log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).FilePath));

                ArrayList list = convertMensajesToArrayListFromParam("mensajesDebuggingPath");

                return list.Contains(actionCode);


            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }


        public string getEndpointAliasByActionCode(string actionCode)
        {
            
            sincronizarDictionario("mensajesMigradosPath");

            string value = "";
            bool result = keyValues.TryGetValue(actionCode, out value);

            _log.Debug(String.Format("Buscando endpoint por actionCode: {0} - alias: [{1}]",actionCode, value));

            
            return value;
            

        }


        private Boolean esArchivoModificado(String configAppParam)
        {
            String path = configuracion.getAppSettingsNameValueCollection()[configAppParam];
            System.IO.FileInfo infoReader = new FileInfo(path);

            _log.Debug("Ult modificacion  :" + infoReader.LastWriteTime.ToString());            
            _log.Debug("Ult Sincronizacion: " + mensajesMigradosUltimaModificacion.ToString());

            bool result = false;
            if (DateTime.Compare(mensajesMigradosUltimaModificacion, infoReader.LastWriteTime) < 0)
                result = true;

            _log.Debug("Requiere modificación: " + result);


            return result;

        }

    }
}
