﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Configuration;
using System.Reflection;
using System.Xml;
using System.Collections.Specialized;
using System.EnterpriseServices;

namespace NuevoDispatcherCOM
{
    /// <summary>
    /// Se encarga de mantener las variables de configuración de la aplicación
    /// </summary>
    class Configuracion
    {
         static readonly ILog _log = LogManager.GetLogger(typeof(Configuracion));
         
         private String configurationPath = "";

         public Configuracion() { }

         public Configuracion(String path)
         {
             configurationPath = path;
         }

         public XmlNodeList nodes(String tagName)
         {
             try
             {                 
                 _log.Debug("Obteniendo nodos de tag: " + tagName);
                
                 string path = getConfigurationPath();

                 XmlDocument xmlDoc = new XmlDocument();

                 xmlDoc.Load(path);

                 XmlNodeList list = xmlDoc.GetElementsByTagName(tagName);

                 return list;

             }
             catch (Exception e)
             {
                 _log.Error(e);
                 throw;
             }
         }

         public NameValueCollection getAppSettingsNameValueCollection()
         {

            
             NameValueCollection configuraciones = new NameValueCollection();

             configuraciones.Clear();

                 XmlNodeList listaDeNodo = nodes("appSettings");

                 foreach (XmlNode nodo in listaDeNodo)
                 {
                     foreach (XmlNode llave in nodo.ChildNodes)
                     {
                         configuraciones.Add(llave.Attributes["key"].Value, llave.Attributes["value"].Value);
                     }
                 }
            

            return configuraciones;
         }

        
       

        /// <summary>
        /// Retorna la colección keyValue de endpoints existentes
        /// </summary>
        /// <returns></returns>
        public NameValueCollection getEndpointsNameValueCollection()
        {
            NameValueCollection endpoints = new NameValueCollection();

            XmlNodeList listaDeNodo = nodes("endpoints");
            if (endpoints.Count == 0)
            {
                foreach (XmlNode nodo in listaDeNodo)
                {
                    foreach (XmlNode llave in nodo.ChildNodes)
                    {
                        endpoints.Add(llave.Attributes["key"].Value, llave.Attributes["value"].Value);
                    }
                }
            }

            return endpoints;
        }

        internal string getConfigurationPath()
        {
            
            Configuration appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

            String path = appConfig.FilePath;

            if (!"".Equals(configurationPath))
                path = configurationPath;

            return path;
        }


        /// <summary>
        /// Genera un string que lista los valores de configuración de la aplicación
        /// </summary>
        /// <returns></returns>
        internal string propiedades()
        {
            String result = "";         

            result += "Config path: " + getConfigurationPath() + "<br/>";

            NameValueCollection appSettings = getAppSettingsNameValueCollection();

            NameValueCollection endpoints = getEndpointsNameValueCollection();

            result += "<br/>App Settings<br/>";
            foreach (String k in appSettings.Keys)
            {
                result += k + ": " + appSettings[k] + "<br/>";
            }


            result += "<br/>Endpoints<br/>";
            foreach (String k in endpoints.Keys)
            {
                result += k + ": " + endpoints[k] + "<br/>";
            }

            return result;
        }
    }
}
