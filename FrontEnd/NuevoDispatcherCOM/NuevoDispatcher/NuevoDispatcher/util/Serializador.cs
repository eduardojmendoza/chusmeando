﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NuevoDispatcherCOM.MCS;
using System.IO;
using System.Xml;

namespace NuevoDispatcherCOM
{
    class Serializador
    {
        /// <summary>
        /// Serializa el objeto dado
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String serializar(Object obj)
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(obj.GetType());

            StringWriter sw = new StringWriter();

            x.Serialize(sw, obj);

            return sw.ToString().Replace("composedResponse", "return");
        }

        /// <summary>
        /// Serializa en forma poco ortodoxa (no usar)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public String crearXml(composedResponse obj)
        {

            String result = "<return><code>" + obj.code + "</code>";
            result += "<response>";

            foreach (XmlElement xml in obj.Response.Any)
            {
                result += xml.GetType();
                if (xml.GetType() != typeof(XmlText)) {
                    result += xml.OuterXml;
                } else {
                    result += xml.Value;
                }

            }

            result += "</response></return>";

                            
            
            return result;
        }


        
    }
}
