﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.EnterpriseServices;
using System.Xml;
using System.ServiceModel.Channels;
using System.ServiceModel;
using System.Configuration;
using System.Reflection;

using log4net;
using log4net.Config;
using System.Diagnostics;
using NuevoDispatcherCOM.eventLog;
using NuevoDispatcherCOM.MCS;
using System.Threading;
using NuevoDispatcherCOM.util;


[assembly: ApplicationName("Nuevo Dispatcher COM")]
[assembly: Description("Dispatcher para migracion com+")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(false,
           AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent)]

namespace NuevoDispatcherCOM
{

    [GuidAttribute("6F560928-232E-405D-88F8-011A7353EBAB")]
    [ObjectPooling(Enabled = true, MinPoolSize = 10, MaxPoolSize = 10000, CreationTimeout = 60000)]
    [JustInTimeActivation]    
    public class Dispatcher : ServicedComponent, IDisposable
    {
        

        private static readonly ILog _log = LogManager.GetLogger(typeof(Dispatcher));
        private Deserializador deserializador = new Deserializador();
        private Serializador serializador = new Serializador();
        private ServiceConfiguration serviceConfiguration;
        private Configuracion configuracion;
        private Migracion migracion;
        

        private String configPath = "";

        override protected bool CanBePooled()
        {
            return true;
        }

        protected override void Activate()
        {
            
            String logMsg = String.Format("Now entering...\nInstance: {0}\nContext: {1}\n",
                                           ContextUtil.ApplicationInstanceId.ToString(), 
                                           ContextUtil.ContextId.ToString());
            _log.Debug("Activate: " + logMsg);
        }

        protected override void Deactivate()
        {
            String logMsg = String.Format("Now entering...\nInstance: {0}\nContext: {1}\n",
                                           ContextUtil.ApplicationInstanceId.ToString(),
                                           ContextUtil.ContextId.ToString());
            _log.Debug("Deactivate: " + logMsg);
        }
        protected override void Dispose(bool disposing)
        {
            String logMsg = String.Format("Now entering...\nInstance: {0}\nContext: {1}\n",
                                           ContextUtil.ApplicationInstanceId.ToString(),
                                           ContextUtil.ContextId.ToString());
            _log.Debug("Dispose: " + logMsg);
            base.Dispose(disposing);
        }


        /// <summary>
        /// Desde la ASP no se puede llamar al constructor con diferentes parámetros por lo que se creó un método de configuración para 
        /// poder configurar el dispatcher con diferentes archivos de configuración
        /// </summary>
        public Dispatcher()
        {
            try
            {
                
                configPath = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).FilePath;
                init(configPath);
            }
            catch (ConfigurationErrorsException cee)
            {
                configPath = Assembly.GetExecutingAssembly().Location + ".config";
                EventLogger.error(String.Format("Error al intentar cargar el archivo de configuración: {0}.config error: {1} ", configPath, cee.Message));
                
            }
            catch (Exception e)
            {
                EventLogger.error(e.Message);
                throw e;
            }
        }

        /// <summary>
        /// Espera un path absoluto del archivo de configuración que se quiera usar. Si se invoca con vacío, utilizará el que exista 
        /// en el mismo path donde se encuentra la dll que registró el servicio. Debería ser NuevoDispatcherCOM.dll.config
        /// </summary>
        /// <param name="path"></param>
        public void init(String path)
        {
            try
            {

                configPath = path;

                if ("".Equals(configPath))
                    configPath = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).FilePath;

                inicializacion();
            }
            catch (Exception e)
            {                

                if (!"".Equals(path))
                {
                    ///Cuando se intenta cargar un archivo de configuración distinto al archivo de configuración por defecto
                    EventLogger.error("Error al intentar cargar el archivo de configuración: " + e.Message);
                    //throw new Exception("Error al intentar cargar el archivo de configuración");
                }
                else
                {
                    throw e;
                    
                }
                
            }
        }


        private void inicializacion()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(configPath));
                _log.Debug("Path de configuracion: " + configPath);

                this.configuracion = new Configuracion(configPath);
                serviceConfiguration = new ServiceConfiguration(this.configuracion);
                migracion = new Migracion(configuracion);
            }
            catch (Exception e)
            {                
                throw e;
            }

        }

        public String name()
        {
            _log.Info("Nuevo dispatcher COM");
            return "Nuevo Dispatcher COM";
        }



        /// <summary>
        /// La respuesta del web service es serializada y el tag raiz de la respuesta es composedResponse
        /// </summary>
        /// <param name="endpoint">url del endpoint ej: "http://jupiter.snoopconsulting.com:9080/CMS-fewebservices-0.0.2-SNAPSHOT/MigratedComponentService" </param>
        /// <param name="actionCode"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [AutoComplete]        
        public String Execute(String endpoint, String actionCode, String request, String contextInfo)
        {
            Guid g = Guid.NewGuid();
            String guid = Convert.ToBase64String(g.ToByteArray());

            try
            {

               

                
                if (configuracion == null)
                {
                    throw new ConfigurationErrorsException("Existe un error en el archivo de configuración del dispatcher.");
                }

                
                //
                if (contextInfo.Equals("SIMULADO"))
                {
                    int milliseconds = 10000;
                    System.Threading.Thread.Sleep(milliseconds);
                    return "SIMULADO_OK";
                }

           

                _log.Debug("configPath: " + configPath);
                _log.Debug(contextInfo);               
                _log.Debug("urlEndpoint: " + endpoint);
                _log.Debug("******** actionCode: " + actionCode + " *************");


                contextInfo = "{\"requestId\" : \"" + guid + "\"}";                
               

                //Si endpoint es vacío (o default) busca el alias asociado al mensaje migrado. 
                //Si no encuentra alias, usa el endpoint seteado por defecto
                String endpointAlias = endpoint;
                if ("".Equals(endpoint) || "default".Equals(endpoint.ToLower()))
                {
                    //Regenera actionCodeExtended
                    String actionCodeExtended = "";
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(request);

                    XmlNode pNodeDefinicion = doc.SelectSingleNode("/Request/DEFINICION");

                    if (pNodeDefinicion != null && !"".Equals(pNodeDefinicion.InnerText.Trim()))
                        actionCodeExtended = String.Format("{0};{1}", actionCode, pNodeDefinicion.InnerText.Trim());
                    else
                        actionCodeExtended = actionCode;

                    //Obtiene el Alias a través del actionCodeExtended
                    endpointAlias = migracion.getEndpointAliasByActionCode(actionCodeExtended);

                    //Obtiene el EndPoint a través del Alias.
                    if (endpointAlias != null && !"".Equals(endpointAlias))
                    {
                        //Toma el endpoint según el alias asociado al actionCodeExtended
                        endpoint = configuracion.getEndpointsNameValueCollection()[endpointAlias];
                        _log.Debug("Toma el endpoint según el alias asociado al actionCodeExtended");
                        _log.Debug(String.Format("ActionCodeExtended: [{0}] Alias: {1} - Endpoint: {2}", actionCodeExtended, endpointAlias, endpoint));
                    }
                    else
                    {
                        //Toma el endpoint por defecto porque no encontró ningún alias asociado al actionCode
                        endpoint = serviceConfiguration.getDefaultEndPointAddressString();
                        _log.Debug("Toma el endpoint por defecto porque no encontró ningún alias asociado al actionCode");
                        _log.Debug(String.Format("ActionCodeExtended [{0}] usa DEFAULT ENDPOINT: {1}", actionCodeExtended, endpoint));
                    }
                }

                String log = String.Format("INPUT ({3}): endpoint [{0}] actionCode [{1}] request: {2}", ("".Equals(endpointAlias) ? "defaultEndpoint" : endpointAlias), actionCode, request, guid);
                EventLogger.info(log, configuracion);
                _log.Info(log);

                //Invocación al Servicio
                composedResponse respuesta = null;
               
                respuesta =  execute(endpoint, actionCode, request, contextInfo);


                //Respuesta
                String respuestaSerializada = serializador.serializar(respuesta);

                //Loggin               
                log = String.Format("OUTPUT({3}) {0}:{1} Response: {2} ", actionCode, request, respuestaSerializada, guid);
                _log.Info(log);
                EventLogger.info(log, configuracion);

                //En caso de error registra el mismo
                registrarCodigoRespuestaError(respuesta, actionCode, request);

                return respuestaSerializada;


            }
            
            catch (Exception e)
            {
                _log.Error("requestId: " + guid, e);

                EventLogger.error(e.Message, configuracion);
                GeneradorMensajeError gErr = new GeneradorMensajeError();
                String result = gErr.generarMensajeError(e);

                //Loggin                               
                _log.Info(String.Format("OUTPUT({3}) {0}:{1} Response: {2} ", actionCode, request, result, guid));

                EventLogger.error(e.Message);
                return result;
            }

        }

        private void registrarCodigoRespuestaError(composedResponse respuesta, String actionCode, String xml)
        {
            if (respuesta != null && respuesta.code < 0)
            {
                String mensaje = String.Format("Action Code: {0}:{1} Error: {2}", actionCode, xml, serializador.serializar(respuesta));
                _log.Error(mensaje);
                EventLogger.error(mensaje, configuracion);
            }
        }

        private composedResponse execute(String endpoint, String actionCode, String request, String contextInfo)
        {

            MigratedComponentServiceClient servicio = serviceConfiguration.getService(endpoint);

            AnyXmlElement xml = new AnyXmlElement();

            xml.Any = new XmlElement[1];

            xml.Any = deserializador.getXMLElements(request);

            return servicio.executeRequest(actionCode, xml, contextInfo);            
        }



        /// <summary>
        /// Devuelve si el mensaje fue migrado o no
        /// </summary>
        /// <returns></returns>

        public bool isMensajeMigrado(String mensaje)
        {
            try
            {
                bool resultado = migracion.mensajeMigrado(mensaje);

                String strLog = String.Format("Mensaje Migrado [{0}]: {1}", mensaje, resultado);
                _log.Info(strLog);
                
                EventLogger.info(strLog, configuracion);

                return resultado;
            }
            catch (Exception e)
            {
                _log.Error(e);
                EventLogger.error(e.Message, configuracion);
                return true;
            }

        }



        /// <summary>
        /// Devuelve si el mensaje fue migrado o no, pero además registra en el log 
        /// el valor de request si es que el actionCode es uno de los que se quiere debuggear.
        /// </summary>
        /// <returns></returns>

        public bool isMensajeMigrado(String actionCode, String request)
        {

            try
            {

                debugging(actionCode, request);
                return isMensajeMigrado(actionCode);

            }
            catch (Exception e)
            {
                _log.Error(e);
                EventLogger.error(e.Message, configuracion);
                return true;
            }

        }


        /// <summary>
        /// Devuelve si el mensaje fue migrado o no
        /// </summary>
        /// <returns></returns>
        public void debugging(String actionCode, String message)
        {

            try
            {
                if (migracion.mensajeParaDebug(actionCode))
                    _log.Info(String.Format("Action Code: {0} - msg: ", actionCode, message));                    
                

            }
            catch (Exception e)
            {

                _log.Error(e);
                EventLogger.error(e.Message, configuracion);
                //No se lanza ningún error intencionalmente.
            }

        }


        /***************************************************/

        public String getConfiguracion()
        {

            _log.Debug("Prueba log debug");

            _log.Info("Prueba log info");

            _log.Error("Prueba log error");

            Assembly assem = Assembly.GetExecutingAssembly();
            AssemblyName assemName = assem.GetName();
            Version ver = assemName.Version;

            String result = "";



            try
            {
                result = String.Format("{0}, Version {1}", assemName.Name, ver.ToString()) + "<br/>";


                ObjectPoolingAttribute attribute = (ObjectPoolingAttribute)Attribute.GetCustomAttribute(
                                                    this.GetType(), typeof(ObjectPoolingAttribute), false);

                if (attribute != null)
                {
                    result += String.Format("minPoolSize = {0}", attribute.MinPoolSize) + "<br/>";

                    result += "maxPoolSize = " + attribute.MaxPoolSize + "<br/>";
                    result += "creationTimeout = " + attribute.CreationTimeout + "<br/>";

                }
            }
            catch (Exception)
            {
                
                //No hacer nada
            }

            return result + configuracion.propiedades(); ;


        }


        public String endpoint()
        {
            String endpoint = serviceConfiguration.getDefaultEndPointAddressString();
            _log.Info("endpoint " + endpoint);
            return endpoint;

        }



        public void logErrorTest()
        {
            EventLogger.error("Prueba error", configuracion);
            EventLogger.info("Prueba info", configuracion);
            EventLogger.warn("Prueba Warn", configuracion);

        }




        [AutoComplete]
        public String ExecuteFault(String endpoint, String actionCode, String request, String contextInfo)
        {
          

            _log.Info("Error fault...");

            try
            {


               


                if (contextInfo.Trim().ToUpper().Equals("CONF")) throw new ConfigurationErrorsException("Existe un error en el archivo de configuración del dispatcher.");

                if (contextInfo.Trim().ToUpper().Equals("TIMEOUT")) throw new TimeoutException("Existe un error de timeout.");


                //
                if (contextInfo.Equals("SIMULADO"))
                {
                    int milliseconds = 10000;
                    System.Threading.Thread.Sleep(milliseconds);
                    return "SIMULADO_OK";
                }

                return Execute(endpoint, actionCode, request, contextInfo);

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw ex;
            }



        }


    }
}
