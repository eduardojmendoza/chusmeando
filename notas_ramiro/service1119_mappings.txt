Service 1119
------------


We get all data from the "Participants" branch:

for $Participant in $findPolicyByNumberRs/ns0:Policy/ns0:InsuranceConditions/ns0:Participants/ns0:Participant
	$Participant/ns0:Addresses/ns0:Address[1]/ns0:CountryCode
	$Participant/ns0:Addresses/ns0:Address[1]/ns0:StateRegion
	$Participant/ns0:Entity/ns0:IndustryCode
	$Participant/ns0:Entity/ns0:PersonalData

	for $Contact in $Participant/ns0:Contacts/ns0:Contact
			$Contact/ns0:Type

$participants := $findPolicyByNumberRs/ns0:Policy/ns0:InsuranceConditions/ns0:Participants/ns0:Participant[ns0:ParticipantRole = 'CREDITOR' or ns0:ParticipantRole = 'LEGAL']
 
for $Participant in $participants
	$Participant/ns0:ParticipantRole
	$Participant/ns0:Entity/ns0:PersonalData/ns0:PID
	$Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID
	$Participant/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Family
	$Participant/ns0:Entity/ns0:PersonalData/ns0:Name/ns0:Given
	$Participant/ns0:Entity/ns0:CompanyData/ns0:Name

	$Participant/ns0:Entity/ns0:PersonalData/ns0:BirthDate

	$Participant/ns0:Entity/ns0:PersonalData/ns0:Gender

	$Participant/ns0:Entity/ns0:DataSource

	let $address := $Participant/ns0:Addresses/ns0:Address[1]
        $address/ns0:AddressType
        $address/ns0:ResidentialAddress/ns0:StreetName
        $address/ns0:ResidentialAddress/ns0:StreetNo
        $address/ns0:ResidentialAddress/ns0:Floor
        $address/ns0:ResidentialAddress/ns0:Apartment
        $address/ns0:CityCode
        $address/ns0:StateRegion
		$address/ns0:CountryCode
		$address/ns0:ZipCode

	$Participant/ns0:Entity/ns0:CompanyData/ns0:CustomerID
	$Participant/ns0:Entity/ns0:IndustryCode

	for $Contact in $Participant/ns0:Contacts/ns0:Contact
		$Contact/ns0:Type
		$Contact/ns0:Details

