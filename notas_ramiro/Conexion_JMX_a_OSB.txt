Conexión JMX a OSB



Requiere un jar específico. Si tenemos un weblogic instalado en nuestra máquina podemos usarlo. Ejemplo:

export WL_HOME=~/bin/weblogic_10.3.6/wlserver

y levantar jconsole con:

jconsole -J-Djava.class.path=$JAVA_HOME/lib/jconsole.jar:$JAVA_HOME/lib/tools.jar:$WL_HOME/server/lib/wljmxclient.jar -J-Djmx.remote.protocol.provider.pkgs=weblogic.management.remote -debug


Poner como URL de conexión:
service:jmx:iiop://10.1.10.98:8011/jndi/weblogic.management.mbeanservers.runtime

y cargar el usuario/clave ( ej weblogic/welcome1 en desa98 )

Si hay error ver los detalles en la consola


ref: https://blogs.oracle.com/WebLogicServer/entry/managing_weblogic_servers_with

