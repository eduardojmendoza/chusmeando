
<!--#include virtual="incCoreActionsDEMO.asp"-->

<%
Function Msg1101_INSIS()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><DEFINICION>1101_OVDatosGralPoliza.xml</DEFINICION><RAMOPCOD>AUS1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>000001</POLIZSEC><CERTIPOL>0000</CERTIPOL><CERTIANN>0001</CERTIANN><CERTISEC>123610</CERTISEC><SUPLENUM>0</SUPLENUM></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", _
				 "lbaw_GetConsultaMQ.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1101. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1101 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1101, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1101 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>

<%
Function Msg1101_AIS()

	Dim mvarRequest
	Dim mvarResponse
	Dim mobjXMLDoc

	mvarRequest = "<Request><DEFINICION>1101_OVDatosGralPoliza.xml</DEFINICION><RAMOPCOD>ICO1</RAMOPCOD><POLIZANN>00</POLIZANN><POLIZSEC>164670</POLIZSEC><CERTIPOL>0</CERTIPOL><CERTIANN>0</CERTIANN><CERTISEC>0</CERTISEC><SUPLENUM>0</SUPLENUM></Request>"
	On Error Resume Next
	
	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", _
				 "lbaw_GetConsultaMQ.biz", _
				 mvarRequest, _
				 mvarResponse)
	
	If Err Then
		Response.Write "<b>Error al invocar el 1101. Error: " & Err.Number & " - " & Err.Description & "</b>"
		Msg1101 = "###ERROR###"
		Exit Function
	End If
	On Error GoTo 0
	

	'Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & mvarResponse & "</TEXTAREA>"

	Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")	
		mobjXMLDoc.async = False
		
	Call mobjXMLDoc.loadXML(mvarResponse)		
	
	'Si el COM+ no finaliza su ejecución normalmente
	If (mobjXMLDoc.selectSingleNode("/Response/Estado/@resultado")) Is Nothing Then
		Set mobjXMLDoc = Nothing
		Response.Write "<b>Hubo un problema al invocar el 1101, no encuentro el Estado.resultado</b>"
		Response.End
	End If
	
	Msg1101 = mobjXMLDoc.selectSingleNode("/Response").xml

End Function
' *****************************************************************************************
%>
<html>
	<head>		

	</head>
<body >

<BR>

<h1>Invocando mensaje 1101_INSIS</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1101_INSIS() & "</TEXTAREA>"
%>

<h1>Invocando mensaje 1101_AIS</h1>
<%
	Response.Write "<TEXTAREA cols=""80"" rows=""10"">" & Msg1101_AIS() & "</TEXTAREA>"
%>
</body>
</html>
