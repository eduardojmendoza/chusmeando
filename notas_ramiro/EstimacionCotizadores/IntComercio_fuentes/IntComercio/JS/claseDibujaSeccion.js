/*
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 21/12/2011
 PPCR: 50055/6010661
 Desarrolador: Leonardo Ruiz
 Descripci�n: Soporte de HTTPS.-
--------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: claseDibujaSeccion.js

Fecha de Creaci�n: 01/07/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripci�n: Genera mediante un XML de definici�n una secci�n de un formulario de ingreso


Metodos:

* constructor: Instancia variables de entorno

* configuracion(xmlRequest, xmlCombos): 
		-xmlRequest: Par�metro con la ruta del XML de definici�n de la secci�n a dibujar
		-xmlCombos: Par�metro con la ruta del XML que contiene los options de los combos declarados en el front

* cargaCombo(pId,pDato): Carga el combo pId con la seccion pDato del XML recibido en xmlCombos (metodo configuracion)

* cargaComboAIS(pId,pDato): Carga el combo pId con el xml pDato recibido como resultado de la llamada al AIS

* cargaComboSQL(pId,pDato):Carga el combo pId con el xml pDato recibido como resultado de la llamada al SQL 

* ocultar(): Oculta la seccion completa

* mostrar(): Muestra la seccion completa

* ocultarCampo(pCampo): Oculta el campo pCampo

* mostarCampo(pCampo): Muestra el campo pCampo

* getCampo(pCampo): Devuelve el valor del campo pCampo

* setCampo(pCampo,pValor): Setea el campo pCampo con el valor pValor


*/

/*LR - 22/12/2011 Esto tiene q tomarlo del xml "CombosFront.xml", hago los cambios para tomarlo de ahi
function variableCombosFront(){
	
	
	var combosFront=
'<COMBOS>'+
	'<TIPODOC>'+
		'<option value="-1" selected="">TIPO</option>'+
		'<option value="1">DNI</option>'+
		'<option value="3">LC</option>'+
		'<option value="2">LE</option>'+
		'<!--		<option value="47">PASAPORTE</option>'+
		'<option value="4">C.U.I.T.</option>'+
		'<option value="5">C.U.I.L.</option>-->'+
	'</TIPODOC>'+
	'<CLIENSEX>'+
		'<option value="-1" selected="">Seleccionar...</option>'+
		'<option value="F">FEMENINO</option>'+
		'<option value="M">MASCULINO</option>'+
	'</CLIENSEX>'+
	'<PROVICOD>'+
		'<option value="-1">SELECCIONAR...</option>'+
		'<option value="2">BUENOS AIRES</option>'+
		'<option value="1">CAPITAL FEDERAL</option>'+
		'<option value="3">CATAMARCA</option>'+
		'<option value="16">CHACO</option>'+
		'<option value="17">CHUBUT</option>'+
		'<option value="4">CORDOBA</option>'+
		'<option value="5">CORRIENTES</option>'+
		'<option value="6">ENTRE RIOS</option>'+
		'<option value="18">FORMOSA</option>'+
		'<option value="7">JUJUY</option>'+
		'<option value="19">LA PAMPA</option>'+
		'<option value="8">LA RIOJA</option>'+
		'<option value="9">MENDOZA</option>'+
		'<option value="21">MISIONES</option>'+
		'<option value="22">NEUQUEN</option>'+
		'<option value="23">RIO NEGRO</option>'+
		'<option value="10">SALTA</option>'+
		'<option value="11">SAN JUAN</option>'+
		'<option value="12">SAN LUIS</option>'+
		'<option value="24">SANTA CRUZ</option>'+
		'<option value="13">SANTA FE</option>'+
		'<option value="14">SANTIAGO DEL ESTERO</option>'+
		'<option value="25">TIERRA DEL FUEGO</option>'+
		'<option value="15">TUCUMAN</option>'+
	'</PROVICOD>'+
	'<PAISSCOD>' + 
		'<option value="-1">Seleccionar...</option>' + 
		'<option value="01">ALBANIA</option>' + 
		'<option value="02">ALEMANIA</option>' + 
		'<option value="03">ANDORRA</option>' + 
		'<option value="00" selected="">ARGENTINA</option>' + 
		'<option value="04">AUSTRALIA</option>' + 
		'<option value="05">AUSTRIA</option>' + 
		'<option value="06">BELGICA</option>' + 
		'<option value="07">BOLIVIA</option>' + 
		'<option value="08">BOSNIA Y HERZEGOVINA</option>' + 
		'<option value="09">BRAZIL</option>' + 
		'<option value="10">BULGARIA</option>' + 
		'<option value="11">CANADA</option>' + 
		'<option value="12">CHILE</option>' + 
		'<option value="13">CHINA</option>' + 
		'<option value="14">COLOMBIA</option>' + 
		'<option value="62">COREA</option>' + 
		'<option value="15">COSTA RICA</option>' + 
		'<option value="16">CROACIA</option>' + 
		'<option value="17">CUBA</option>' + 
		'<option value="18">DINAMARCA</option>'+
		'<option value="19">ECUADOR</option>'+
		'<option value="20">ELSALVADOR</option>'+
		'<option value="21">ESPADA</option>'+
		'<option value="22">ESTADOS UNIDOS</option>'+
		'<option value="23">FINLANDIA</option>'+
		'<option value="24">FRANCIA</option>'+
		'<option value="25">GRECIA</option>'+
		'<option value="26">GUATEMALA</option>'+
		'<option value="27">HOLANDA</option>'+
		'<option value="28">HONDURAS</option>'+
		'<option value="29">HONG KONG</option>'+
		'<option value="30">HUNGRIA</option>'+
		'<option value="31">INDIA</option>'+
		'<option value="32">INGLATERRA</option>'+
		'<option value="33">IRLANDA</option>'+
		'<option value="34">ISLANDIA</option>'+
		'<option value="35">ITALIA</option>'+
		'<option value="36">JAMAICA</option>'+
		'<option value="37">JAPON</option>'+
		'<option value="38">LIECHTENSTEIN</option>'+
		'<option value="39">LUXEMBURGO</option>'+
		'<option value="40">MEXICO</option>'+
		'<option value="41">MONACO</option>'+
		'<option value="42">NICARAGUA</option>'+
		'<option value="43">NORUEGA</option>'+
		'<option value="44">NUEVA ZELANDA</option>'+
		'<option value="45">OCEANIA</option>'+
		'<option value="46">PANAMA</option>'+
		'<option value="47">PARAGUAY</option>'+
		'<option value="48">PERU</option>'+
		'<option value="49">POLONIA</option>'+
		'<option value="50">PORTUGAL</option>'+
		'<option value="51">PUERTORICO</option>'+
		'<option value="52">REPUBLICA CHECA</option>'+
		'<option value="53">REPUBLICA DOMINICANA</option>'+
		'<option value="54">RUMANIA</option>'+
		'<option value="55">RUSIA</option>'+
		'<option value="56">SLOVAQUIA</option>'+
		'<option value="57">SUDAFRICA</option>'+
		'<option value="58">SUECIA</option>'+
		'<option value="59">SUIZA</option>'+
		'<option value="60">URUGUAY</option>'+
		'<option value="61">VENEZUELA</option>'+
	'</PAISSCOD>'+
	'<INTERVALOPAGO>'+
		'<option value="-1">Seleccionar...</option>'+
		'<option value="01" selected="">MENSUAL</option>'+
		'<option value="02">BIMESTRAL</option>'+
		'<option value="03">TRIMESTRAL</option>'+
		'<option value="04">SEMESTRAL</option>'+
		'<option value="05">ANUAL</option>'+
		'<option value="06">UNICA</option>'+
		'<option value="07">CUATRIMESTRAL</option>'+
	'</INTERVALOPAGO>'+
	'<ESTCIVIL>'+
		'<option value="" selected="">Seleccionar...</option>'+
		'<option value="S">SOLTERO/A</option>'+
		'<option value="C">CASADO/A</option>'+
		'<option value="E">SEPARADO/A</option>'+
		'<option value="V">VIUDO/A</option>'+
		'<option value="D">DIVORCIADO/A</option>'+
	'</ESTCIVIL>'+
	'<SIFTCIVA>'+
		'<option value="" selected="">Seleccionar...</option>'+
		'<option value="0">R. INSCR. (FC A C/LEY)</option>'+
		'<option value="1">RESP. INSCRIPTO</option>'+
		'<option value="2">RESP. NO INSCRIPTO</option>' + 
		'<option value="3">CONSUMIDOR FINAL</option>'+
		'<option value="4">EXENTO</option>'+
		'<option value="5">IVA NO RESPONSABLE</option>'+
		'<option value="6">R.I.AGENTE RETENCION</option>'+
		'<option value="7">SUJETO N/CATEGORIZAD</option>'+
		'<option value="8">R.I.AGENTE RETENCION</option>'+
		'<option value="9">SIN IVA</option>'+
		'<option value="A">RESP. INSC. PLAN CANJE </option>'+
		'<option value="B">R. I. P. CANJE-NI ROG</option>' + 
	'</SIFTCIVA>'+
	'<SIFTCIBB>'+
		'<option value="" selected="">Seleccionar...</option>'+
		'<option value="1">INSCRIPTO CONVENIO MULTILATERAL</option>'+
		'<option value="2">INSCRIPTO LOCAL</option>'+
		'<option value="3">NO ACREDITA CONDICION</option>'+
		'<option value="4">EXENTO</option>'+
	'</SIFTCIBB>'+
'</COMBOS>'

	return combosFront
	
	}
*/


function DibujaSeccion()
{
   this.tituloSeccion = null;
   this.cantFilas= null;
   this.cantColumnas = null;
   this.idSeccion = null;
   this.oXML=null;
   this.oXMLCombos= null;
   this.oXMLNode=null;
   this.canalURL=null
   this.canal = null;
   this.validaObligatorio =null;
   this.moneda=null;
   this.desMoneda=null;
   
 }  
 
 //**********************************************************************************//
//19/11/2010 LR Agrego un metodo para set Y get del canal de los .gif por problemas con https
  DibujaSeccion.prototype.setCanalUrlImg = function(pCanal)
  {
  	this.canal=pCanal;
  }
    DibujaSeccion.prototype.getCanalUrlImg = function()
  {
  	return this.canal;
 	}
//*******************

  DibujaSeccion.prototype.setCanalURL = function(pCanalURL)
  {
  	
  	this.canalURL=pCanalURL
  }
    DibujaSeccion.prototype.getCanalURL = function()
  {
  	
  	return this.canalURL
  
 }
 
 DibujaSeccion.prototype.setCanal = function(pCanal)
  {
  	
  	this.canal=pCanal
  }
    DibujaSeccion.prototype.getCanal = function()
  {
  	
  	return this.canal
  
 }
 
   DibujaSeccion.prototype.setMonedaProducto = function(pMoneda)
  {
  	
  	this.moneda=pMoneda
  }
    DibujaSeccion.prototype.getMonedaProducto = function()
  {
  	
  	return this.moneda
  
 }
 
  DibujaSeccion.prototype.setDesMonedaProducto = function(pMoneda)
  {
  	
  	this.desMoneda=pMoneda
  }
    DibujaSeccion.prototype.getDesMonedaProducto = function()
  {
  	
  	return this.desMoneda
  
 }
 
    DibujaSeccion.prototype.setCotizar = function(pFlag)
  {
  	
  	this.flagCotizar=pFlag
  }
    DibujaSeccion.prototype.getCotizar = function()
  {
  	
  	return this.flagCotizar
  
 }
 
 DibujaSeccion.prototype.configuracion = function(xmlCombos)
 {
 	
 	
   	this.oXMLCombos = new ActiveXObject('MSXML2.DOMDocument');        
	this.oXMLCombos.async = false;  	
	
	//GED 05-05-2011 CARGAR COMBOS DE UNA VARIABLE PARA QUE FUNCIONE  HTTPS
  	   //this.oXMLCombos.loadXML(variableCombosFront())
  	//this.oXMLCombos=loadXMLDoc(xmlCombos)
  	this.oXMLCombos.loadXML(xmlCombos);
 	
}

DibujaSeccion.prototype.cargaCombo= function(pId,pDato)
{
	
  		
  	var oXMLValores = this.oXMLCombos.selectNodes('//'+pDato+'/option/@value');
        var oXMLEtiquetas =this.oXMLCombos.selectNodes('//'+pDato+'/option');
        var cantNodos=oXMLValores.length
     
         for(var y=0;y<cantNodos ;y++)
         {
         	     		 
	  	var nuevoElemento=document.createElement("option")
	  	
	  		
		nuevoElemento.text=oXMLEtiquetas[y].text;
		nuevoElemento.value=oXMLValores[y].text ;
		if (this.oXMLCombos.selectSingleNode('//'+pDato+'/option['+y+']/@selected'))
			{nuevoElemento.selected=true}
			
		document.getElementById(pId).add(nuevoElemento);
	}		 		
	
	
}

DibujaSeccion.prototype.cargaComboXSL= function(pId,pDato,pAplicarXSL)
{
	if(pAplicarXSL) 
	{
	oXSLCombo = new ActiveXObject('MSXML2.DOMDocument');        
	oXSLCombo.async = false;  	
	oXSLCombo.loadXML(pAplicarXSL)
	oXMLDato = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLDato.async = false;  	
	oXMLDato.loadXML(pDato)
	
	}
	
	
	
	oXMLCombo = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLCombo.async = false;  	
	
	if(pAplicarXSL)
	oXMLCombo.loadXML(oXMLDato.transformNode(oXSLCombo))
	else
	oXMLCombo.loadXML(pDato)
	
	
	
	
	var oXMLValores = oXMLCombo.selectNodes('//options/option/@value');
       var oXMLEtiquetas =oXMLCombo.selectNodes('//options/option');
        var cantNodos=oXMLValores.length
     
         for(var y=0;y<cantNodos ;y++)
         {
         		 
	  	var nuevoElemento=document.createElement("option")
		nuevoElemento.text=oXMLEtiquetas[y].text;
		nuevoElemento.value=oXMLValores[y].text ;
		
		//if (oXMLCombo.selectSingleNode('//'+pDato+'/option['+y+']/@selected'))
	//		{nuevoElemento.selected=true}
		if (oXMLEtiquetas[y].attributes.getNamedItem("selected"))	
		{//alert(oXMLEtiquetas[y].text)
		//var ypos=y
		nuevoElemento.selected=true
		}
		document.getElementById(pId).add(nuevoElemento);
	}	
	//if (ypos)	
	//{ //	alert(ypos)		
	//document.getElementById(pId).value=  ypos
//}
}

DibujaSeccion.prototype.cargaComboSQL= function(pId,pDato,pXSL)
{
	oXMLDato = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLDato.async = false;  	
	oXMLDato.loadXML(pDato)
	
	oXSLCombo = new ActiveXObject('MSXML2.DOMDocument');        
	oXSLCombo.async = false;  	
	oXSLCombo.loadXML(pXSL)
	
	oXMLCombo = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLCombo.async = false; 
	 //window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+oXMLDato.xml+"</textarea>")
	 //window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+oXMLDato.transformNode(oXSLCombo)+"</textarea>")
	
	oXMLCombo.loadXML(oXMLDato.transformNode(oXSLCombo))
	
	//oXMLOptions = oXMLCombo.selectSingleNode("options").xml
	// window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+oXMLCombo.selectSingleNode("options").xml+"</textarea>")
	
	var combo = oXMLCombo.xml.replace("<options>","")
	 combo = combo.replace("</options>","")
	
	
	var oXMLValores = oXMLCombo.selectNodes('//options/option/@value');
       var oXMLEtiquetas =oXMLCombo.selectNodes('//options/option');
        var cantNodos=oXMLValores.length
        
     var objSelect=document.getElementById(pId)     
       var cantAtributos=objSelect.attributes.length;    

       
     var sOpts = '<select '
		for(var y=0; y<cantAtributos;y++)
		{	
			if(objSelect.attributes.item(y).value!='null' && objSelect.attributes.item(y).value !='' && objSelect.attributes.item(y).value!='false' )
			sOpts+=	objSelect.attributes.item(y).name + '="' + objSelect.attributes.item(y).value +'"  '
		}
	sOpts+= '>'

		sOpts+= combo
		
	
		objSelect.outerHTML = sOpts + "</select>";

     
   
	

}

/*function addOpt(oCntrl, iPos, sTxt, sVal){
     var selOpcion=new Option(sTxt, sVal);
     eval(oCntrl.options[iPos]=selOpcion);
   }  


*/
  


DibujaSeccion.prototype.ocultar = function()
{
	
	document.getElementById(this.idSeccion).style.display="none"
}
DibujaSeccion.prototype.mostrar = function()
{
	
	document.getElementById(this.idSeccion).style.display="inline"
}

DibujaSeccion.prototype.ocultarCampo = function(pCampo)
{
	
	document.getElementById(pCampo+"_etiq").style.display="none"
	document.getElementById(pCampo).style.display="none"
	if (document.getElementById(pCampo+"_tipo2")) document.getElementById(pCampo+"_tipo2").style.display="none"
}


DibujaSeccion.prototype.mostrarCampo = function(pCampo)
{
	
	document.getElementById(pCampo+"_etiq").style.display="inline"
	document.getElementById(pCampo).style.display="inline"
	if (document.getElementById(pCampo+"_tipo2")) document.getElementById(pCampo+"_tipo2").style.display="inline"

}


DibujaSeccion.prototype.getCampo = function(pCampo)
{
	
	return document.getElementById(pCampo).innerText
	
	
}

DibujaSeccion.prototype.setEtiqueta = function(pCampo,pValor)
{
	
	document.getElementById(pCampo + "_etiq").innerText=pValor;
	
	
}

DibujaSeccion.prototype.setCampo = function(pCampo,pValor)
{
	
	document.getElementById(pCampo).innerText=pValor;
	
	
}

DibujaSeccion.prototype.setCampoHTML = function(pCampo,pValor)
{
	
	document.getElementById(pCampo).innerHTML=pValor;
	
	
}
 
 DibujaSeccion.prototype.dibujaSeccion = function()
 {
 	//Comienzo seccion AREA
 	this._write("<div ")
  
  	 var cantAtributos=this.oXML.selectSingleNode("//area").attributes.length;
                              
        for ( var atributo=0;atributo<cantAtributos;atributo++)
        	{ 
             var nombreAtributo=this.oXML.selectSingleNode("//area").attributes.item(atributo).name
             var valorAtributo =this.oXML.selectSingleNode("//area").attributes.item(atributo).value
                                  
             this._write( " " + nombreAtributo + "=")
             this._write( "'" + valorAtributo + "'")
  		}
   
    	this._write(">")
    	// Fin Seccion AREA 
  	this._write('<table width="690" border="0" align="center" cellpadding="0" cellspacing="0">');
  	//Fila Titulo Seccion
     	this._write('<tr><td colspan="2" style="height: 25px"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#99CCCC" class="form_titulos02">');
      	this._write('<tr><td bordercolor="#FFFFFF"><img src="/Oficina_Virtual/HTML/Images//circ_bco.gif" width="7" height="15" align="absmiddle">'+this.tituloSeccion+'</td></tr>');
      this._write('</table></td></tr>')
   	// Fin Titulo Seccion
      
  	this._write('<tr><td align="center"><table  id="'+valorAtributo+'_tabla" width="99%" border="0" cellpadding="0" cellspacing="0">'); 
    	this._write('<tbody  id="'+valorAtributo+'_filasTabla"  >');
	  
 	
    	
    	  for(var fila= 1; fila<= this.cantFilas; fila++)
      {		
              
                this._write("<tr>");               
                 
                   
                    for( var columna = 1;columna<= this.cantColumnas;columna++)
                    {      
                    	  if (this.oXML.selectSingleNode("//campo[@fila="+ fila + "]/@fila"))
                    		{              	
                    			this._dibujaEtiqueta(fila,columna); 
                       			this._dibujaTipoCampo(fila,columna);                       		
                       		}
                       	else
                       		{
                       		this._write("<td></td>")
                       		}
                    }
                   this._write( "</tr>")
               
                 //   this._write( "<tr><td colspan='"+ this.cantColumnas+ "'></td></tr>")
                //	   this._write( "<tr><td></td></tr>")
             }
     
     this._write('</tbody>');  
     this._write('</table></td></tr></table>')          
 
	 this._write("</div>")
}

DibujaSeccion.prototype._dibujaEtiqueta = function(fila,columna,id)

{
	  if (this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta")  ) 
	{
	 this._write( "<td " );
	  if (this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta")  ) 
	  {
	 this._write( " id=")	
	
	 var idCampo= this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]").attributes.getNamedItem("id").value;
	 this._write( "'" + idCampo + "_etiq" + "'")
	} 					
	 
	 if (this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta")  ) 
             var cantAtributos=this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta").attributes.length
               	else
              var cantAtributos=0
	             	    for (var atributo=0;atributo<cantAtributos;atributo++)
	                      		{ 
	                                    var nombreAtributo=this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta").attributes.item(atributo).name
	                                    var valorAtributo =this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta").attributes.item(atributo).value
	                                  
	                                    this._write( " " + nombreAtributo + "=")
	                                    this._write( "'" + valorAtributo + "'")
	  					}
	  this._write( ">" );
	  
	    if ( this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/@columna") )
                        { this._write (this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/etiqueta").text) }
                        
           this._write( "</td>" );
	}
}

DibujaSeccion.prototype._dibujaTipoCampo = function(fila,columna)

{
	
	        //Dibuja tipo
                        this._write( "<td>" );
                         
                         if ( this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/@columna")) 
                         
                         {  
                       		var oXMLTipo = this.oXML.selectNodes("//campo[@fila="+ fila + "and @columna=" + columna + "]/tipo");
        			
        				var cantNodos=oXMLTipo.length
     					
         				for(var y=0;y<cantNodos ;y++)
                       		
                       		{
                       		var tipoTag=oXMLTipo[y].text;
                       		//var tipoTag= this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/tipo").text 
                                this._write( "<" + tipoTag )
                               
                             	this._write( " id=")
	 				var idCampo= this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]").attributes.getNamedItem("id").value;
	 				if (y==0)
	 					this._write( "'" + idCampo +"'")
	 				else
	 					this._write( "'" + idCampo + "_tipo" +eval(y+1)+ "'")
                            
                               var cantAtributos=this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/tipo["+y+"]").attributes.length
                              
                               for (var atributo=0;atributo<cantAtributos;atributo++)
                                { 
                                    
                                    var nombreAtributo=this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/tipo["+y+"]").attributes.item(atributo).name
                                    var valorAtributo =this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/tipo["+y+"]").attributes.item(atributo).value
                                  if (nombreAtributo!='valor'){
                                    this._write( " " + nombreAtributo + "=")
                                    this._write( "'" + valorAtributo + "'")
                                }
                                if(nombreAtributo=="validarTipeo")
                                {
                                	
                                	
                                	switch (valorAtributo)
                                	{
                                	
                                	case "numero":
                                	
                                	{ 	                          
                                		this._write( " onfocus=")  
                                    		this._write('"document.attachEvent(\'onkeydown\', capturaTecla)"')
                                		this._write( " onblur=")
                                    		this._write('"document.detachEvent(\'onkeydown\', capturaTecla)"' )
                                		break;
                                	}
                                	case "texto":
                                	{
                                		this._write( " onfocus=")
                                    		this._write("\"document.attachEvent('onkeydown', capturaTecla)\"" )
                                		this._write( " onblur=")
                                    		this._write("\"document.detachEvent('onkeydown', capturaTecla)\"" )
                                		
                                	}
                                	
                                   }
                                  }
                                }
                               
                               this._write( ">")  //Fin nombre de TAG
                               
                               switch (tipoTag)
                                	{
                                	
                                	case "a":
                                		{ 
                                	var valorTag = this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]/tipo["+y+"]/@valor").value
                                	
                                	this._write(valorTag)
                                //
                                //	this._write( " id=")
	 				//	var idCampo= this.oXML.selectSingleNode("//campo[@fila="+ fila + "and @columna=" + columna + "]").attributes.getNamedItem("id").value;
	 				//	this._write( "'" + idCampo + "_tipo" +eval(y+1)+ "'")
                                //	
                                	}
                               	}
                               if ( nombreAtributo =="lista")
                                    this._write( " "+ valorAtributo + " ")
                               
                               this._write( "</" + tipoTag + ">&nbsp;")
                               
                               
                            } //fin for tipo
                         
                        } //fin columna
                       
                        
                         
                       this._write( "</td>" )
	
	
	
	}
	
DibujaSeccion.prototype.agregaCampo = function(pId,pFila,pColumna,tipo,estilo,evento)
{
	
	var idBox= document.getElementById(this.idSeccion	+"_filasTabla")		
	
	 for(var x=0, xCont=pColumna;x< xCont;x++)	
		if (!idBox.rows[pFila].cells[pColumna]) idBox.rows[pFila].insertCell(0)
	var elem = document.createElement("input");				 
	 elem.type=tipo;		
	 if (evento!="")
	 {
		var tipoEvento=evento.split("=")[0]
		var fncEvento=evento.split("=")[1]
		
		if (tipoEvento=="onclick")
			elem.onclick=new Function (fncEvento );			
	}
	
	if(pId != "")
	{	
		var nuevoId=document.createAttribute("id")	
		nuevoId.value=pId
		elem.setAttributeNode(nuevoId)	
	} 	
	
	if (estilo)
	{		
		elem.style.cssText=estilo;		
		
	}
	if (idBox.rows[pFila].cells[pColumna]) 
	idBox.rows[pFila].cells[pColumna].appendChild(elem)
	
	
	
	
}
DibujaSeccion.prototype.agregaCombo = function(pId,pFila,pColumna,estilo,evento)
{
	
	var idBox= document.getElementById(this.idSeccion	+"_filasTabla")		
	
	 for(var x=0, xCont=pColumna;x< xCont;x++)	
		if (!idBox.rows[pFila].cells[pColumna]) idBox.rows[pFila].insertCell(0)
	var elem = document.createElement("select");	
	
	 if (evento!="")
	 {
		var tipoEvento=evento.split("=")[0]
		var fncEvento=evento.split("=")[1]
		
		if (tipoEvento=="onchange")
			elem.onchange=new Function (fncEvento );			
	}
			 
	 					
	if(pId != "")
	{	
		var nuevoId=document.createAttribute("id")	
		nuevoId.value=pId
		elem.setAttributeNode(nuevoId)	
	} 	
	
	if (estilo)
	{		
		elem.style.cssText=estilo;		
		
	}
	if (idBox.rows[pFila].cells[pColumna]) 
	idBox.rows[pFila].cells[pColumna].appendChild(elem)
	
	
	
	
}

DibujaSeccion.prototype.agregaEtiqueta = function(pFila,pColumna,pEtiqueta)

{
	
	var idBox= document.getElementById(this.idSeccion	+"_filasTabla")		
	
	 for(var x=0, xCont=pColumna;x< xCont;x++)	
		if (!idBox.rows[pFila].cells[pColumna]) idBox.rows[pFila].insertCell(0)
	
		if (idBox.rows[pFila].cells[pColumna]) 
			idBox.rows[pFila].cells[pColumna].innerHTML=pEtiqueta
	
}

DibujaSeccion.prototype.validaCamposObligatorios = function()

{
	var idBox= document.getElementById(this.idSeccion	+"_filasTabla")		
	this.validaObligatorio=true
 	for (var y=0; y< this.cantFilas;y++)
	 	for(var x=0; x<  this.cantColumnas;x++)	 
	 		try{		
	 		
	 		if( idBox.rows[y].cells[x].childNodes(0).obligatorio)
	 			if (idBox.rows[y].cells[x].childNodes(0).value =="")
	 			{
					//alert("El campo " + idBox.rows[y].cells[x].childNodes(0).value )
					this.validaObligatorio=false;
				}
				
				
			}
	 		 catch(err){break}
	 		 
	
	
}

DibujaSeccion.prototype._write = function(pValor)
	
{
try
{
		if (document.all ) document.write(pValor)
	
}
catch(err)
{	
		response.write(pValor)
}
}

DibujaSeccion.prototype.habilitarCampo = function(pCampo)

{
	document.getElementById(pCampo).disabled= false
	
}

DibujaSeccion.prototype.deshabilitarCampo = function(pCampo)

{
	document.getElementById(pCampo).disabled= true
	
}



DibujaSeccion.prototype.parseaXSLParam =function(pxml,pxsl,psalida,pParametros)

{       
  //--------Carga los parametros del xsl desde pParametros-----------
 
        oXMLParam = new ActiveXObject('MSXML2.DOMDocument');
        
        oXMLParam.async = false;
       
        oXMLParam.loadXML(pParametros.innerHTML);
         
        var nombreParametro= oXMLParam.getElementsByTagName("nombre")
      
        var valorParametro= oXMLParam.getElementsByTagName("valor")
       
    //-------Carga xml entrada desde parametro   ----------  
         
        oXML = new ActiveXObject('MSXML2.DOMDocument');
        
        oXML.async = false;
       
         oXML.loadXML(pxml); //usar  cambiar para xml de mvarresponse
        
                
       //---------- Carga entrada parametor xsl
         oXSL=new ActiveXObject('MSXML2.FreeThreadedDOMDocument');
         
         oXSL.async = false;
         
         oXSL.loadXML(pxsl);       
            
         objCache   = new ActiveXObject("Msxml2.XSLTemplate"); 
         
         objCache.stylesheet = oXSL;
      
         var oXSLT = objCache.createProcessor();
         
         oXSLT.input = oXML 
        
         for (x=0;x<nombreParametro.length;x++)
      
            { 		    
		     oXSLT.addParameter(nombreParametro[x].text, valorParametro[x].text, ""); 		 
	        }  	
	
		 oXSLT.transform();		
		
		//alert(oXSLT.output)  
		return oXSLT.output
		// psalida.innerHTML = oXSLT.output;        
      }