/*
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 03/11/2011
 Ticket: 675119
 Desarrolador: Marcelo Gasparini
 Descripci�n: Se agregan nuevo validador de Localidades con coma y ap�strofes
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 19/04/2011
Ticket 599651
Desarrolador: Adriana Armati
Descripci�n: Se agrega funcionalidad de Prima m�nima
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------

COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
ALL RIGHTS RESERVED
 
This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a 
retrieval system or translated in any human or computer language in any way or
for any other purposes whatsoever without the prior written consent of the Hong
Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
serious civil and criminal offence, which can result in heavy fines and payment 
of substantial damages.

Nombre del JavaScript: ComunValidacion.js

Fecha de Creaci�n: 15/07/2010

PPcR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripci�n: Este archivo contiene todas las funciones de validacion comunes a 
todos los formularios indistintamente del canal por el que se entre 
a la aplicaci�n web.

--------------------------------------------------------------------------------
*/

/*
	Este archivo contiene todas las funciones comunes a todos los formularios
	indistintamente del canal por el que se entre a la aplicaci&oacuten web.

	Funciones de validacion generales.
	
	function validarFormulario(pXmlDoc, pXmlXPathRoot, pXslListadoErrores, pObligatoriedadEstricta);
	function valida_texto(varCampo, descripcion, long_minima);
	function validaNoTodosCeros(pTexto);
	function validaNoSecuencia(pTexto);
	function validaNoLetrasRepetidas(pTexto);
	function validaNoNrosIguales(pTexto);
	function letternumber(e);
	function evalNumber();
	function completar(pCadena, pTotal);
	function irALabel(objName);
	function limitarCampoEditable(pObj, pCantidad);	
	function trim(texto01);
	function modifyValue(objResultId, varObj)	
	function ValidaFrmFecha(strDate)
	function validateDateCal(inputDate)
	function IsNumber(theField)
	function agregarError(pIdObj, pMensaje, pSolapa);
	function validarCalle(pTipo, pSINIEDOM, pSINIEDNU, pINTERDOM, pRUTANRO, pKILOMETRO, pTIPORUTA, pCRUCERUT, pMSGERROR);

	function valida_texto(texto, tipo, campo, area, mensaje);
	function validarPatente(pPatente);
*/
 
 
 var aValidarSolapas = new Array();
 var cantidadErrores
 function inicializarMatrizValidacion()
 {
       aValidarSolapas[0]="S"
       aValidarSolapas[1]="N"
       aValidarSolapas[2]="N"
       aValidarSolapas[3]="N"
       aValidarSolapas[4]="N"
       aValidarSolapas[5]="N"
       aValidarSolapas[6]="N"
}
function irALabel(pSolapa, pObjName)
{
//	irASolapa(pSolapa);
	//HRF 20100901 Si no existe el label no da error
	try {
	var tipo = document.getElementById(pObjName).type;
	if (tipo.toLowerCase() != "hidden" && document.getElementById(pObjName).style.display != "none")
		eval(document.getElementById(pObjName).focus());}  catch(err){ return}	
}

function pintarNegroIndividual() {
	//Resalta en azul los campos obligatorios del formulario seg�n el XML asociado
	var varXmlCampos = document.getElementById('xmlValidacionIndividual').XMLDocument;
	for (var i=0; i< varXmlCampos.selectSingleNode('//OTROS').childNodes.length ; i++ )
	{	
		var varCampo = varXmlCampos.selectSingleNode('//OTROS').childNodes[i].attributes.getNamedItem('nombre').value;
		var varValor = varXmlCampos.selectSingleNode('//OTROS').childNodes[i].attributes.getNamedItem('obligatorio').value;
		if (varValor == "S") 
		{
			//document.getElementById( varCampo + "_etiq").className="luzObligatorio";
			document.getElementById( varCampo + "_etiq").style.color="#000000";
		}
	}
	varXmlCampos = null;	
}

function pintarAzul() {
	//Resalta en azul los campos obligatorios del formulario seg�n el XML asociado
	var varXmlCampos = document.getElementById('xmlDefinicionCamposForm').XMLDocument;
	for (i=0; i< varXmlCampos.selectSingleNode('//CAMPOS').childNodes.length ; i++ )
	{	
		var varCampo = varXmlCampos.selectSingleNode('//CAMPOS').childNodes[i].attributes.getNamedItem('nombre').value;
		var varValor = varXmlCampos.selectSingleNode('//CAMPOS').childNodes[i].attributes.getNamedItem('obligatorio').value;
		if (varValor == "S") 
		{
			document.getElementById( varCampo + "_etiq").className="luzObligatorio";
			document.getElementById( varCampo + "_etiq").style.color="blue";
		}
	}
	varXmlCampos = null;
	
	//--------------------------------------------------------------------------------------------------------//
	//LR 08/10/2010 Resalta en azul los campos obligatorios del formulario seg�n el XML asociado para Items
	// 06/10/2010 LR Defect 21 Estetica, pto 13
	var varXmlCamposItem = document.getElementById('xmlValidacionIndividual').XMLDocument;
	for (i=0; i< varXmlCamposItem.selectSingleNode('//OTROS').childNodes.length ; i++ )
	{
		var varCampoItem = varXmlCamposItem.selectSingleNode('//OTROS').childNodes[i].attributes.getNamedItem('nombre').value;
		var varValorItem = varXmlCamposItem.selectSingleNode('//OTROS').childNodes[i].attributes.getNamedItem('obligatorio').value;
		if (varValorItem == "S")
		{
			document.getElementById( varCampoItem + "_etiq").className="luzObligatorio";
			document.getElementById( varCampoItem + "_etiq").style.color="blue";
		}
	}
	varXmlCamposItem = null;
	//--------------------------------------------------------------------------------------------------------//
}

/* ****************************************************** */
 	  //    validarFormulario('xmlValidacionIndividual', pXPath, 'XSLAlert', false)
function validarFormulario(pXmlDoc, pXmlXPathRoot, pXslListadoErrores, pObligatoriedadEstricta, pErrorPrevio, pCantidadErrores) {
	pintarAzul()
	cantidadErrores=0	
	var varErrorEncontrado = false;
	var varCampoObligatoriedad = "";

	//vacia el div de errores del form...
	document.getElementById("divTablaErrores").innerHTML = "";

	if (!pErrorPrevio) 
	{
		oXMLErrores.async = false;
		oXMLErrores.loadXML("<ERRORES></ERRORES>");	
	}

	//Obtener campos del xml y matchearlos con el form
	var xmlCampos = document.getElementById(pXmlDoc).XMLDocument;
	// window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+xmlCampos.xml+"</textarea>")
	  
	//Si el flag de obligatorioEstricto esta en true lee los valores de obligatoriedad de ese atributo del xml, sino
	//lo lee del atributo "obligatorio"...
	if (pObligatoriedadEstricta) 
	{
		varCampoObligatoriedad = "obligatorioEstricto";
	} else {
		varCampoObligatoriedad = "obligatorio";
	}
	//if(pXmlXPathRoot=="//DOMICILIO")	alert(xmlCampos.xml)
	for (i=0; i< xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes.length ; i++ )
	{	 
		if(cantidadErrores==pCantidadErrores)break
		//Variables definadas por cada campo dentro del XML
		var varCampo = "";
		var varCampoDependiente = "";
		var varValorCampoDependiente = "";
		var varLeyendaErrorPersonalizado = "";
		var varObligatorio = "";
		var varTipo = "";
		var varLongitud = "";
		var varLongitudMinima = "";
		var varOffLine = "";
		var varSolapa = "";
		var varValidaOffLine = "N"
		varValidaValor =""

		//Campos leidos del XML
		varCampo = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('nombre').value;
		varObligatorio = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem(varCampoObligatoriedad).value;
		varTipo = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('tipo')? xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('tipo').value: ""
		varLongitud = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('longitud').value;
		varLongitudMinima = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('longitudMinima').value;
		varSolapa = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('solapa').value;
		if ( xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('campoDependiente') != null )		
			varCampoDependiente = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('campoDependiente').value;
		if ( xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('valorCampoDependiente') != null)
			varValorCampoDependiente = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('valorCampoDependiente').value;
		if ( xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('leyendaErrorPersonalizado') != null)
			varLeyendaErrorPersonalizado = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('leyendaErrorPersonalizado').value;		
		if ( xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('offLine') != null)
			varOffLine = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('offLine').value;		
		
		if ( xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('validaOffLine') != null)
			varValidaOffLine = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('validaOffLine').value;		
			
		if ( xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('validaValor') != null)
			varValidaValor = xmlCampos.selectSingleNode(pXmlXPathRoot).childNodes[i].attributes.getNamedItem('validaValor').value;		

		//Variables propias del proceso de validaci�n
		var varEsObligatorio = false;
		var varAvanzarSiguiente = false;
		var varLeyendaErrorObligatorio;
			//alert(varCampo)
		//Si el campo esta disabled o readOnly no realiza validacion y continua con el siguiente campo
		if (document.getElementById(varCampo).disabled == true || document.getElementById(varCampo).readOnly == true) {
			varAvanzarSiguiente = true;
			
		}
		if (varValidaOffLine == "S") varAvanzarSiguiente = false;

		if (document.getElementById(varCampo).offLine != null) {			
			if (document.getElementById(varCampo).offLine != varOffLine) {varAvanzarSiguiente = true;}
		} 
	//	if(pXmlXPathRoot=="//DOMICILIO")	alert(document.getElementById(varCampo+ "_etiq"))
		//Recupera el color original del label
		if (document.getElementById(varCampo+ "_etiq") != null  && varAvanzarSiguiente == false) 
		{	
			document.getElementById(varCampo + "_etiq").style.color="";
		}
		
		//Determinar obligatoriedad del campo (por atributo o por dependencia)
		
		if (varObligatorio == "S" && varAvanzarSiguiente == false) 
		{			
			if (varCampoDependiente != "") 
			{				
				if (varValorCampoDependiente.substring(0,1) == '!')
				{					
					if (varValorCampoDependiente.substring(1) != document.getElementById(varCampoDependiente).value) 
					{						
						varLeyendaErrorObligatorio = varLeyendaErrorPersonalizado;
						varEsObligatorio = true;
					} 
					else 
					{						
						varEsObligatorio = false;
					}
				} 
				else 
				{
					if (varValorCampoDependiente == document.getElementById(varCampoDependiente).value) 
					{						
						varLeyendaErrorObligatorio = varLeyendaErrorPersonalizado;
						varEsObligatorio = true;
					} 
					else 
					{					
						varEsObligatorio = false;
					}
				}
			} else 
			{	
				varLeyendaErrorObligatorio = "El campo es obligatorio, debe seleccionar una opci�n.";
				varEsObligatorio = true;
			}
		} 
		else 
		{
			
			if (pObligatoriedadEstricta) 
			{
				varEsObligatorio = false;	
			} 
			else 
			{				
				
			if (varCampoDependiente != "") 
				{
					if (varValorCampoDependiente == document.getElementById(varCampoDependiente).value) 
					{						
						varLeyendaErrorObligatorio = varLeyendaErrorPersonalizado;
						varEsObligatorio = true;
					} 
					else 
					{
						varEsObligatorio = false;
					}
				} 
				else 
				{
					varEsObligatorio = false;
				}
			}
		}
		//**
		//************* GED 17-03-2011 SE CORRIGR BUG QUE NO ENTRA A VALIDACION PERSONALIZADA CUANDO EL 
		//**
			//Tiene atributo de validaci�n personalizada?
		if (document.getElementById(varCampo).validacionPersonalizada != null && varAvanzarSiguiente == false && aValidarSolapas[varSolapa-1]=="S") 
		{
			//alert("pasa:" + varCampo )
			validarObligatoriosEstrictos = pObligatoriedadEstricta;			
			var res = eval(document.getElementById(varCampo).validacionPersonalizada + ";"); 
			
			if (res == true) 
			{	
				varAvanzarSiguiente = false;	
			}
			else
			{  
				varErrorEncontrado = true;
				
				//alert("Caso Person actual: "  +tabActual + " solapa del error:  " + varSolapa)
				
				if ( aValidarSolapas[varSolapa-1]=="S" ) 
					if (tabActual>=varSolapa-1)
					{				
						insertarError("",varCampo, res, varSolapa,"E");
						varAvanzarSiguiente = true
					}
					else
					{
						varAvanzarSiguiente = true;						
					}
				
				varAvanzarSiguiente = true;
			}
		}
	
		//**
		
		
		if (varEsObligatorio == true && varAvanzarSiguiente == false && aValidarSolapas[varSolapa-1]=="S") 
		{		
			if (document.getElementById(varCampo).value == "") 
			{ 	
				varErrorEncontrado = true;
				
				if ( aValidarSolapas[varSolapa-1]=="S" ) 
				if (tabActual>=varSolapa-1)
					{
					//alert("caso Oblig actual: "  +tabActual + " solapa del error:  " + varSolapa)	
					insertarError("",varCampo, varLeyendaErrorObligatorio, varSolapa,"E");
					}
					else
					{
					varAvanzarSiguiente = true;	
					}
				varAvanzarSiguiente = true;
			}
		}
		 else 
		{
			
			if (document.getElementById(varCampo).value == "") 
			{
				
				varAvanzarSiguiente = true;
			}
		}
		// GED: 12-8-2010   VALIDA POR VALOR  ATRIBUTO(EJ. validaValor="&gt;0") LLEVA leyendaErrorPersonalizado
		if (varValidaValor != "") 
			{
				
				if (  !eval( document.getElementById(varCampo).value +varValidaValor)) 
				{					
					insertarError("",varCampo, varLeyendaErrorPersonalizado, varSolapa,"E");
					varAvanzarSiguiente = true;
				}
				
			}
		
		//Tiene formato correcto?
		if (varAvanzarSiguiente == false && aValidarSolapas[varSolapa-1]=="S")
		{		
			if (validarFormato(document.getElementById(varCampo), varTipo, varLongitud, varLongitudMinima)) 
			{ 
				
				varAvanzarSiguiente = false;
				
			} 
			else 
			{
				varErrorEncontrado = true;				
				if ( aValidarSolapas[varSolapa-1]=="S" ) 
				if (tabActual>=varSolapa-1)
					{					
					insertarError("",varCampo, leyendaError, varSolapa,"E");
					}
				else
					{
					varAvanzarSiguiente = true;						
					}
				varAvanzarSiguiente = true;
			}
		}
		
		
		
		
		
	}				

	//Si encontro error, entonces muestra el XML de errores, sino devuelve true
	
	if(cantidadErrores>0)
	{
		
		var varXslTablaErrores = new ActiveXObject("MSXML2.DOMDocument");
		varXslTablaErrores.async = false;
		varXslTablaErrores.load(document.getElementById(pXslListadoErrores).XMLDocument);
			
		var varTablaErrores;	
		varTablaErrores = oXMLErrores.transformNode(varXslTablaErrores);
		if (varTablaErrores!="") 
		{
			
			document.getElementById("divTablaErrores").innerHTML = varTablaErrores;
			
		}
		window.scrollTo(1000);

		varTablaErrores = null;		
		varXslTablaErrores = null;
		
		return false;
	} 
	else 
	{	
		return true;
	}
}

function fncMuestraFueraNorma() {
	       		
	        document.getElementById("xmlfueraNorma").value = oXMLFueraNorma.xml
	        var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
	        varXslFueraNorma.async = false;
	        varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);
	       var  mvarTablaFueraNorma = oXMLFueraNorma.transformNode(varXslFueraNorma);
	        document.getElementById("divTablaFueraNorma").innerHTML = mvarTablaFueraNorma;  
	        document.getElementById("divTablaFueraNorma").style.display ='inline';

}
function insertarFueraNorma(pItem, pUbicacion, pCampo, pMensaje, pSolapa,pTipoError,pEtiqueta,pCertisec) 
{
		
		
	//alert(pItem+"**"+ pCampo+"**"+pMensaje+"**"+ pSolapa+"**"+pTipoError+"**"+pEtiqueta+"**"+pCertisec)
	//Colorea el lbl de ROJO
	
	//Inserta en XML
	var varONodo = oXMLFueraNorma.createElement("ERROR");
	var varOElement;
	

	//cantidadErrores+=1
	
	varOElement = oXMLFueraNorma.createElement("NROITEM");
	varOElement.text = pItem;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLFueraNorma.createElement("UBICACION");
	varOElement.text = pUbicacion;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLFueraNorma.createElement("TIPOERROR");
	varOElement.text = pTipoError;
	varONodo.appendChild(varOElement);
	
	
	varOElement = oXMLFueraNorma.createElement("CAMPO");
	varOElement.text = pCampo;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLFueraNorma.createElement("CERTISEC");
	varOElement.text = pCertisec;
	varONodo.appendChild(varOElement);
	
	
	varOElement = oXMLFueraNorma.createElement("LEYENDAERROR");
	varOElement.text = pMensaje;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLFueraNorma.createElement("LABELCAMPO");
	
	

	
	if (!pEtiqueta)
	{
		
		varOElement.text = document.getElementById(pCampo + "_etiq")?document.getElementById(pCampo + "_etiq").innerHTML:"";
		
	
	}
	else
		{
		try{
		varOElement.text= document.getElementById(pEtiqueta).innerText
		
		}
		catch(err){
		
		varOElement.text = pEtiqueta
		
		}
		
			}
	
	
	varONodo.appendChild(varOElement);
	
	

	if (oXMLFueraNorma.selectSingleNode("//ERRORES/SOLAPA[@nro = " + pSolapa + "]") != null) {
		oXMLFueraNorma.selectSingleNode("//ERRORES/SOLAPA[@nro = " + pSolapa + "]").appendChild(varONodo);	
	} else {
		var varOSolapa = oXMLFueraNorma.createElement("SOLAPA");
		var oAttr = oXMLFueraNorma.createAttribute("nro");
		oAttr.value = pSolapa;
		varOSolapa.setAttributeNode(oAttr);	
		var oAttr = oXMLFueraNorma.createAttribute("nom");
		if (document.getElementById("spanTextoEtiqSolapa_"+ pSolapa))
		oAttr.value = document.getElementById("spanTextoEtiqSolapa_"+ pSolapa).outerText;
		else
			oAttr.value = ""
		varOSolapa.setAttributeNode(oAttr);	
		oXMLFueraNorma.selectSingleNode("//ERRORES").appendChild(varOSolapa).appendChild(varONodo);
	}
	
	//CONTROLA FUERA MANUAL POR ITEM
	//alert("fuera manual ON")
	//fueraManual[pItem]= true

//window.document.body.insertAdjacentHTML("beforeEnd", "fuera norma:<textarea>"+oXMLFueraNorma.xml+"</textarea>")				
		
}
/*******************************************************************************/
function insertarPopUpFueraNorma(pItem,pUbicacion ,pCampo, pMensaje, pSolapa,pTipoError,pEtiqueta,pCertisec) 
{
		
	//alert(pItem+"  "+pUbicacion+"  "+pCampo+"  "+ pMensaje+"  "+ pSolapa+"  "+pTipoError+"  "+pEtiqueta+"  "+pCertisec)
		
	var varONodoPop = oXMLPopUpFueraNorma.createElement("ERROR");
	var varOElementPop;
	
	varOElementPop = oXMLPopUpFueraNorma.createElement("NROITEM");
	varOElementPop.text = pItem;
	varONodoPop.appendChild(varOElementPop);	
	
	varOElementPop = oXMLPopUpFueraNorma.createElement("UBICACION");
	varOElementPop.text = pUbicacion;
	varONodoPop.appendChild(varOElementPop);
	
	varOElementPop = oXMLPopUpFueraNorma.createElement("TIPOERROR");
	varOElementPop.text = pTipoError;
	varONodoPop.appendChild(varOElementPop);
	
	varOElementPop = oXMLPopUpFueraNorma.createElement("CAMPO");
	varOElementPop.text = pCampo;
	varONodoPop.appendChild(varOElementPop);

	varOElementPop = oXMLPopUpFueraNorma.createElement("LEYENDAERROR");
	varOElementPop.text = pMensaje;
	varONodoPop.appendChild(varOElementPop);
	
	varOElementPop = oXMLPopUpFueraNorma.createElement("CERTISEC");
	varOElementPop.text = pCertisec;
	varONodoPop.appendChild(varOElementPop);
	

	
	varOElementPop = oXMLPopUpFueraNorma.createElement("LABELCAMPO");

	//varOElement.text = document.getElementById("lbl_" + pCampo).innerHTML;
	if (!pEtiqueta)
	{
		
		varOElementPop.text = document.getElementById(pCampo + "_etiq")?document.getElementById(pCampo + "_etiq").innerHTML:"";
	
	
	}
	else
		{
		try{
		//document.getElementById(pEtiqueta).style.color='red';
		}
		catch(err){return}
			varOElementPop.text= document.getElementById(pEtiqueta).innerText
		}
	
	

	
	varONodoPop.appendChild(varOElementPop);

	
	
	if (oXMLPopUpFueraNorma.selectSingleNode("//ERRORES/SOLAPA[@nro = " + pSolapa + "]") != null) {
		oXMLPopUpFueraNorma.selectSingleNode("//ERRORES/SOLAPA[@nro = " + pSolapa + "]").appendChild(varONodoPop);	
	} else {
		var varOSolapa = oXMLPopUpFueraNorma.createElement("SOLAPA");
		var oAttr = oXMLPopUpFueraNorma.createAttribute("nro");
		oAttr.value = pSolapa;
		varOSolapa.setAttributeNode(oAttr);	
		var oAttr = oXMLPopUpFueraNorma.createAttribute("nom");
		oAttr.value = document.getElementById("spanTextoEtiqSolapa_"+ pSolapa).outerText;
		varOSolapa.setAttributeNode(oAttr);	
		oXMLPopUpFueraNorma.selectSingleNode("//ERRORES").appendChild(varOSolapa).appendChild(varONodoPop);
	}
	

//window.document.body.insertAdjacentHTML("beforeEnd", "fuera norma:<textarea>"+oXMLPopUpFueraNorma.xml+"</textarea>")				
		
}





/*******************************************************************************/
function insertarError(pItem, pCampo, pMensaje, pSolapa,pTipoError,pEtiqueta) 
{
		
	//Colorea el lbl de ROJO
	
	
	if ( eval(document.getElementById(pCampo + "_etiq")) != null  && pTipoError=="E") {
		document.getElementById( pCampo+  "_etiq").style.color='red';
	}
	
	
	//Inserta en XML
	var varONodo = oXMLErrores.createElement("ERROR");
	var varOElement;
	cantidadErrores+=1
	
	varOElement = oXMLErrores.createElement("NROITEM");
	varOElement.text = pItem;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLErrores.createElement("TIPOERROR");
	varOElement.text = pTipoError;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLErrores.createElement("CAMPO");
	varOElement.text = pCampo;
	varONodo.appendChild(varOElement);

	varOElement = oXMLErrores.createElement("LEYENDAERROR");
	varOElement.text = pMensaje;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLErrores.createElement("LABELCAMPO");

	//varOElement.text = document.getElementById("lbl_" + pCampo).innerHTML;
	if (!pEtiqueta)
	{
		
		varOElement.text = document.getElementById(pCampo + "_etiq")?document.getElementById(pCampo + "_etiq").innerHTML:"";
	}
	else
		{
		try{
		document.getElementById(pEtiqueta).style.color='red';
		varOElement.text= document.getElementById(pEtiqueta).innerText
		}
		catch(err){}
		
		}
	
	
	varONodo.appendChild(varOElement);

	if (oXMLErrores.selectSingleNode("//ERRORES/SOLAPA[@nro = " + pSolapa + "]") != null) {
		oXMLErrores.selectSingleNode("//ERRORES/SOLAPA[@nro = " + pSolapa + "]").appendChild(varONodo);	
	} else {
		var varOSolapa = oXMLErrores.createElement("SOLAPA");
		var oAttr = oXMLErrores.createAttribute("nro");
		oAttr.value = pSolapa;
		varOSolapa.setAttributeNode(oAttr);	
		var oAttr = oXMLErrores.createAttribute("nom");
		oAttr.value = document.getElementById("spanTextoEtiqSolapa_"+ pSolapa).outerText;
		varOSolapa.setAttributeNode(oAttr);	
		oXMLErrores.selectSingleNode("//ERRORES").appendChild(varOSolapa).appendChild(varONodo);
	}

//window.document.body.insertAdjacentHTML("beforeEnd", "errores:<textarea>"+oXMLErrores.xml+"</textarea>")				
		
}

//Se definen validaciones individuales segun el tipo de dato declarado en el xml de definici�n
function validarFormato(pObjCampo, pTipo, pLongitudMax, pLongitudMin) {
	
	//Elimina espacios al comienzo y final de la cadena...
	pObjCampo.value = trim((pObjCampo.value).toUpperCase());
	
	if (pLongitudMin == '') 
		pLongitudMin = (pObjCampo.value).length;
				
	switch (pTipo)
	{
		case "char":
			var texto = pObjCampo.value;
			if (!valida_texto(texto, 'T', '', '', '')) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;	
			}			
			if (texto.length > pLongitudMax) 
			{
				leyendaError  = "El texto es mas largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}	
			break;	
			
		case "combo":
			
			var texto = pObjCampo.value;
			
			if (texto == "-1"  ) 
			{
				leyendaError  = "El campo es obligatorio, debe seleccionar una opci�n.";
				return false;
			}
			
			break;	
		case "xml":
			
			var texto = pObjCampo.value;
			
			if (texto.length < 150)
			{
				leyendaError  = "Debe Ingresar al menos un Item a la lista";
				return false;
			}
			
			break;			
			
		case "text":
			var texto = pObjCampo.value;
			if (!valida_texto(texto, 'A', '', '', '')) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;	
			}			
			if (texto.length> pLongitudMax) 
			{
				leyendaError  = "El texto es mas largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}	
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}
			break;
			
		case "numero":
			var valor = pObjCampo.value;
			if (!valida_texto(valor, 'N', '', '', ''))
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;	
			}			
			if (valor.length> pLongitudMax) 
			{
				leyendaError  = "El n�mero ingresado es mas largo de lo permitido";
				return false;
			}
			if (valor.length < pLongitudMin) 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;
			}			
			break;

		case "numeroNoRep":
			var valor = pObjCampo.value;
			if (valor == '0') 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;				
			}
			if (!valida_texto(valor, 'N', '', '', ''))
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;	
			}			
			if (valor.length> pLongitudMax) 
			{
				leyendaError  = "El n�mero ingresado es mas largo de lo permitido";
				return false;
			}
			if (valor.length < pLongitudMin) 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;
			}					
			break;

		case "altura":
			var valor = pObjCampo.value;
			if (valor == '0') 
			{
				//HRF Def.96 2010-10-26
				leyendaError  = "El dato ingresado no es correcto, solo se permiten car�cteres numericos o S/N";
				return false;				
			}
			if (valor != 'S/N' && !valida_texto(valor, 'N', '', '', ''))
			{
				//HRF Def.96 2010-10-26
				leyendaError  = "El dato ingresado no es correcto, solo se permiten car�cteres numericos o S/N";
				return false;	
			}			
			if (valor.length> pLongitudMax) 
			{
				leyendaError  = "El n�mero ingresado es mas largo de lo permitido";
				return false;
			}
			if (valor.length < pLongitudMin) 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;
			}			
			break;

		case "piso":
			var valor = pObjCampo.value;
			if (valor == '0') 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;				
			}			
			if (valor != 'PB' && !valida_texto(valor, 'N', '', '', ''))
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;	
			}			
			if (valor.length> pLongitudMax) 
			{
				leyendaError  = "El n�mero ingresado es mas largo de lo permitido";
				return false;
			}
			if (valor.length < pLongitudMin) 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;
			}			
			break;
			
		case "email":
			var texto = pObjCampo.value;
			if (!valida_texto(texto, 'E', '', '', '')) 
			{
				//	06/10/2010 LR Defect 21 Estetica, pto 6.-
				leyendaError  = "El mail ingresado no posee el formato correcto";
				return false;	
			}
			if (texto.length> pLongitudMax) 
			{
				leyendaError  = "El mail ingresado es mas largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				//	06/10/2010 LR Defect 21 Estetica, pto 6.-
				leyendaError  = "El mail ingresado no posee el formato correcto";
				return false;
			}
			break;
			
		case "patente":
			var texto = pObjCampo.value;
			if (!validarPatente(texto)) 
			{
				leyendaError  = "La patente ingresada no tiene el formato correcto";
				return false;	
			}
			if (texto.length> pLongitudMax) 
			{
				leyendaError  = "La patente ingresada es mas larga de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "La patente ingresada no tiene el formato correcto";
				return false;
			}			
			break;
		//DEFECT 277
		case "telefono":
			var texto = pObjCampo.value;
			if (!valida_texto(texto, 'TEL', '', '', '')) 
			{
				leyendaError  = "El N�mero de Tel�fono ingresado no tiene el formato correcto";
				return false;	
			}
			if (!validaNoTodosCeros(texto))
			{
				leyendaError  = "El N�mero de Tel�fono ingresado no tiene el formato correcto";
				return false;	
			}
			/*if (!validaNoSecuencia(texto))
			{
				leyendaError  = "El N�mero de Tel�fono ingresado no tiene el formato correcto";
				return false;	
			}*/
			if (texto.length> pLongitudMax) 
			{
				leyendaError  = "El N�mero de Tel�fono ingresado es m�s largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El N�mero de Tel�fono ingresado no tiene el formato correcto";
				return false;
			}
			/*if (!validaNoNrosIguales(texto)) 
			{
				leyendaError  = "Los n�meros que conforman el tel�fono no pueden ser todos iguales";
				return false;
			}*/
			break;

		case "documento":
			var texto = pObjCampo.value;
			if (!valida_texto(texto, 'N', '', '', '')) 
			{
				leyendaError  = "El N�mero de Documento ingresado no tiene el formato correcto";
				return false;	
			}
			if (!validaNoTodosCeros(texto))
			{
				leyendaError  = "El N�mero de Documento ingresado no tiene el formato correcto";
				return false;	
			}
			/*if (!validaNoSecuencia(texto))
			{
				leyendaError  = "El N�mero de Documento ingresado no tiene el formato correcto";
				return false;	
			}*/
			if (texto.length> pLongitudMax) 
			{
				leyendaError  = "El N�mero de Documento ingresado es m�s largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El N�mero de Documento ingresado no tiene el formato correcto";
				return false;
			}
			/*if (!validaNoNrosIguales(texto)) 
			{
				leyendaError  = "Los n�meros no pueden ser todos iguales";
				return false;
			}*/
			break;
			
		case "apenom":
			var texto = pObjCampo.value;
			if (!valida_texto(texto, 'X', '', '', '')) 
			{
				leyendaError  = "El texto ingresado solo puede contener letras";
				return false;	
			}		
			if (texto.length > pLongitudMax) 
			{
				leyendaError  = "El texto es mas largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}	
			break;			

		case "localidad":
			var texto = pObjCampo.value;			
			//Valida que el texto contenga letras y numeros
			if (!valida_texto(texto, 'T', '', '', '')) 
			{
				leyendaError  = "El texto ingresado no puede contener solo n�meros";
				return false;	
			}
			//Valida que el texto no tenga solo numeros
			if (valida_texto(texto, 'N', '', '', '')) 
			{
				leyendaError  = "El texto ingresado no puede contener solo n�meros";
				return false;	
			}				
			//Valida caracteres repetidos
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}		
			if (texto.length > pLongitudMax) 
			{
				leyendaError  = "El texto es mas largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}					
			break;	

		case "calle":
			var texto = pObjCampo.value;
			if (texto == '0') 
			{
				leyendaError  = "El n�mero ingresado no tiene el formato correcto";
				return false;				
			}			
			//Valida que el texto contenga letras
			if (valida_texto(texto, 'X', '', '', '')) 
			{
				if (texto.length < 3)
				{
					leyendaError  = "El texto ingresado no tiene el formato correcto";
					return false;	
				}
			} else if (!valida_texto(texto, 'T', '', '', '')) {
				//Valida que el texto tenga algun caracter v�lido
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;	
			}				
			//Valida caracteres repetidos
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}		
			if (texto.length > pLongitudMax) 
			{
				leyendaError  = "El texto es mas largo de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}					
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}
			break;

        //Ticket: 675119           case "localidadcoma":
            var texto = pObjCampo.value;
            //Valida que el texto contenga letras y numeros
            if (!valida_texto(texto, 'CC', '', '', '')) {
            
                leyendaError = "El texto ingresado no tiene el formato correcto";
                return false;
            }
            //Valida que el texto no tenga solo numeros
            if (valida_texto(texto, 'N', '', '', '')) {
                leyendaError = "El texto ingresado no puede contener solo n�meros";
                return false;
            }
            //Valida caracteres repetidos
            if (!validaNoLetrasIguales(texto)) {
                leyendaError = "El texto ingresado no tiene el formato correcto";
                return false;
            }
            if (texto.length > pLongitudMax) {
                leyendaError = "El texto es mas largo de lo permitido";
                return false;
            }
            if (texto.length < pLongitudMin) {
                leyendaError = "El texto ingresado no tiene el formato correcto";
                return false;
            }
            break;				

		case "desc":
			var texto = pObjCampo.value;
			if (texto.length > pLongitudMax) 
			{
				leyendaError  = "La descripci�n ingresada es m�s larga de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "La descripci�n ingresada es m�s corta de lo requerido";	
				return false;
			}
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}		
			if (!validaExisteEspacio(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}			
			break;	

		case "descCorta":
			var texto = pObjCampo.value;
			if (texto.length > pLongitudMax) 
			{
				leyendaError  = "La descripci�n ingresada es m�s larga de lo permitido";
				return false;
			}
			if (texto.length < pLongitudMin) 
			{
				leyendaError  = "La descripci�n ingresada es m�s corta de lo requerido";	
				return false;
			}
			if (!validaNoLetrasIguales(texto)) 
			{
				leyendaError  = "El texto ingresado no tiene el formato correcto";
				return false;
			}			
			break;							
	}
	return true;
}
/* ******************************************************************** */
//	FASE 2
function insertarXMLCambios(pId, pItem, pSolapa, pCampo, pValorNuevo, pValorInicial, pTipoCambio)
{	
//if(pTipoCambio == "B")
//	alert(pItem +"  "+ pSolapa+"  "+ pCampo+"  "+ pValorNuevo+"  "+ pValorInicial+"  "+ pTipoCambio)
//	alert("inserta despues")
	var varONodo = oXMLCambios.createElement("CAMBIO");
	var varOElement;
	
	varOElement = oXMLCambios.createElement("IDITEM");
	varOElement.text =  pId;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("ITEM");
	varOElement.text =  pItem;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("NROSOLAPA");
	varOElement.text =  pSolapa;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("SOLAPA");
	var solapa = pSolapa <= 4? "Cotizaci�n - ": "Solicitud - "
	varOElement.text = solapa +  document.getElementById("spanTextoEtiqSolapa_"+ pSolapa).outerText  ;
	//varOElement.text =  document.getElementById("spanTextoEtiqSolapa_"+ pSolapa).outerText;
	varONodo.appendChild(varOElement);
	
	if(pCampo.indexOf("|") != -1)
	{
		if  (pCampo.split("|")[0]== "COBERCOD")
		{		
			var	mvarIDCampo = pCampo.split("|")[1]
			var	mvarCampo = getCOBERDES(mvarIDCampo)  
		}
		if  (pCampo.split("|")[0]== "CERTISEC")
		{		
			var	mvarIDCampo = pCampo.split("|")[1]
			var	mvarCampo = pItem
		}
		
		if  (pCampo.split("|")[0]== "ITEM")
		{
			var	mvarIDCampo = pCampo.split("|")[1]
			
			switch (pTipoCambio)
	    		{
		    		case "A":
		    		{var	mvarCampo = pItem		
		    		break;
		    		}
		    		case "B": 
		    		{
		    		var	mvarCampo = pItem	
		    		break;
		    		}
		    			
		    		default: mvarCampo = pItem  
			}
		
		
		}
		
	}
	else
	{
		var	mvarIDCampo = pCampo
		
		try
		{
			var mvarCampo = document.getElementById(pCampo + "_etiq")?document.getElementById(pCampo + "_etiq").innerText:document.getElementById(pCampo).innerText;	
		}
		catch (error)
		{var mvarCampo ="" }
		
	}
	
	
	eliminarXMLCambios(mvarIDCampo, pId )
	
	varOElement = oXMLCambios.createElement("CAMPO");	
	varOElement.text = mvarCampo	
	varONodo.appendChild(varOElement);	
	
	varOElement = oXMLCambios.createElement("IDCAMPO");	
	varOElement.text = mvarIDCampo ;	
	varONodo.appendChild(varOElement);	
	
	varOElement = oXMLCambios.createElement("TIPOCAMBIO");	
	varOElement.text = pTipoCambio ;	
	varONodo.appendChild(varOElement);	
	
	//17/12/2010 LR Se agrega descripcion a TIPOCAMBIO
	switch (pTipoCambio)
	{
		case "A":
		{
			var	mvarCampoDescr = "Alta"
			break;
		}
		case "B":
		{
			var	mvarCampoDescr = "Baja"
			break;
		}
		default: mvarCampoDescr = "Modificaci�n"
	}
	varOElement = oXMLCambios.createElement("DESCTIPCAMB");
	varOElement.text = mvarCampoDescr ;	
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("VNUEVO");
	varOElement.text = pValorNuevo;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("VINICIAL");
	varOElement.text = pValorInicial;
	varONodo.appendChild(varOElement);
	
	oXMLCambios.selectSingleNode("//CAMBIOS").appendChild(varONodo);
	//alert(oXMLCambios.xml)
}
function eliminarXMLCambios(pCampo,pItem)
{	

	try
	{
	varXmlNodo= oXMLCambios.selectSingleNode("//CAMBIO[IDCAMPO='" + pCampo  + "' and IDITEM='"+pItem+"']")
	varXmlNodo.parentNode.removeChild(varXmlNodo);		
	
	}
	catch(error){}
	


	
	
}
//aca es
function insertaXMLBaja(pCampo)

{		
			//REVISAR
			
			
			
			var puntero = pCampo.split("COBCHECK")[1]
			//if(existeCoberturaXMLRen(document.all.COBCHECK[puntero].cobercod, document.getElementById("OTID").value ))
			if(existeCoberturaXMLRen(document.all.COBCHECK[puntero].cobercod, document.getElementById("spCertisecAnt").innerText ))
			{
				//alert(document.getElementById("OTID").value)
				var nroSolapa = 2
				insertarXMLCambios(document.getElementById("OTID").value,document.getElementById("localidadRiesgo").value,nroSolapa, "COBERCOD|" + document.all.COBCHECK[puntero].cobercod, "", document.getElementById("SUMAASEG"+ puntero).value, "B")
			}
		else
				eliminarXMLCambios(document.all.COBCHECK[puntero].cobercod, document.getElementById("OTID").value )

		

}

function insertaXMLBajaItem(pCampo)
{
	
	//alert("baja item " + pCampo)
	insertarXMLCambios(pCampo,getItemDesc(pCampo),"2", "BAJAITEM"+pCampo, "", "", "B")
	
	
}


function validaUltimoItemRen()

{
	

		var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		varXml.setProperty("SelectionLanguage", "XPath");
		if (varXml.loadXML(document.getElementById("varXmlItems").value))
		{	
		
			
			if( varXml.selectNodes("//ITEMS/ITEM[CERTISECANT!='']").length== "1")
				return true
			else
				return false
	
		}
	
}
	
	


/************************************************************************/

function letternumber(e)
{
	var key;
	var keychar;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		// control keys
	if ((key==null) || (key==0) || (key==8)  || (key==9) || (key==13) || (key==27) || (key==96) )
		return true;
		// alphas and numbers
	else if ((("abcdefghijklmn�opqrstuvwxyz0123456789/.- ").indexOf(keychar) > -1))
		return true;
	else
		return false;
}

/* ******************************************************************** */
function evalNumber()
{

	var sCadena = event.srcElement.value;
	if (!((event.keyCode >= 96 && event.keyCode <= 105)
			|| (event.keyCode <= 40 && event.keyCode >= 35)
			|| (event.keyCode >= 48 && event.keyCode <= 57)
			|| event.keyCode == 45
			|| event.keyCode == 46
			|| event.keyCode == 8
			|| event.keyCode == 9
			|| event.keyCode == 13)
		     //|| event.keyCode == 13 || event.keyCode == 32)  
			|| (event.altLeft || event.shiftLeft || event.ctrlLeft) 
			|| event.keyCode == 188
			)
	
	{
		event.returnValue = false;
	}
}
/* ******************************************************************** */
function evalAltura(pObj)
{
	var sCadena = event.srcElement.value;
	if (event.keyCode == 83) 
	{
		pObj.value = 'S/N';
		event.returnValue = false;
	} else {
		if (!((event.keyCode >= 96 && event.keyCode <= 105)
				|| (event.keyCode <= 40 && event.keyCode >= 35)
				|| (event.keyCode >= 48 && event.keyCode <= 57)
				|| event.keyCode == 45
				|| event.keyCode == 46
				|| event.keyCode == 8
				|| event.keyCode == 9
				|| event.keyCode == 13)
			     //|| event.keyCode == 13 || event.keyCode == 32)  
				|| (event.altLeft || event.shiftLeft || event.ctrlLeft) 
				|| event.keyCode == 188
				)		
		{
			event.returnValue = false;
		} else {
			if (pObj.value == 'S/N' && event.keyCode != 9)
				pObj.value = '';
		}
	}
}
/* ******************************************************************** */
function evalPiso(pObj)
{
	var sCadena = event.srcElement.value;
	if (event.keyCode == 80) 
	{
		pObj.value = 'PB';
		event.returnValue = false;
	} else {
		if (!((event.keyCode >= 96 && event.keyCode <= 105)
				|| (event.keyCode <= 40 && event.keyCode >= 35)
				|| (event.keyCode >= 48 && event.keyCode <= 57)
				|| event.keyCode == 45
				|| event.keyCode == 46
				|| event.keyCode == 8
				|| event.keyCode == 9
				|| event.keyCode == 13)
				|| (event.altLeft || event.shiftLeft || event.ctrlLeft) 
				|| event.keyCode == 188
				)		
		{
			event.returnValue = false;
		} else {
			if (pObj.value == 'PB' && event.keyCode != 9)
				pObj.value = '';
		}
	}
}
/* ************************************************************************* */
function completar(pCadena, pTotal)
{
	var vEspacios = "";
	var vDevolver;

	for (i=0; i<pTotal; i++)
	{
		vEspacios = vEspacios + " ";
	}

	vDevolver = pCadena + vEspacios;
	return vDevolver.substring(0,pTotal).toUpperCase();
}

/* ******************************************************************** */
function limitarCampoEditable(pObj, pCantidad)
{
	var texto = pObj.innerText;
	if (texto.length > pCantidad) {
		pObj.innerText = texto.substr(0,pCantidad);
		alert('El campo ' + pObj.Descripcion + ' no puede exceder los ' + pCantidad + ' caracteres');
		return false;
	}
}
/* ******************************************************************** */
function trim(texto01) 
{
	while(texto01.substring(0,1) == ' ')
		texto01 = texto01.substring(1,texto01.length);

	while(texto01.substring(texto01.length-1,texto01.length) == ' ')
		texto01 =texto01.substring(0, texto01.length-1);   
	
	return texto01;
}
/* ******************************************************************** */
function modifyValue(objResultId, varObj) {
	var resAux = document.getElementById(objResultId).value;
	var varPos = varObj.value;
	var largo = resAux.length;
	var valor = 0;

	if (varObj.checked) 
		valor = 1;
	document.getElementById(objResultId).value = resAux.substring(0, varPos - 1) + valor + resAux.substr(varPos, largo - 1);
}
/* ******************************************************************** */
function validaFrmFecha(strDate)
{
	// Parsea global de la fecha: formato (dd/mm/aaaa)
	lstDate = strDate.split('/');
	
	if (lstDate.length != 3)
		return false;
		
	var intDia  = lstDate[0];
	var intMes  = lstDate[1];
	var intAnio = lstDate[2];
	 
	if (intAnio < 1900) return false; 
	if (intMes < 1) return false;
	if (intDia < 1) return false;
	if (intMes > 12) return false;
	if (intDia > 31) return false;

	if (intAnio.length > 4) return false; 
	if (intMes.length > 2) return false;
	if (intDia.length > 2) return false;
	
	if (!isNumber(intAnio)) return false; 
	if (!isNumber(intMes)) return false; 
	if (!isNumber(intDia)) return false; 
    
	if (intMes==2) {
		//Se fija si el anio es biciesto
		if ( !(intAnio % 4) && ( intAnio % 100 || ! (intAnio % 400)))
		{ // Bisiesto			
			if (intDia > 29) return false;
		} 
		else 
		{
			if (intDia > 28) return false;
		}           
	}
    
	if (intMes==4) {
		if (intDia > 30) return false;
	}           
    
	if (intMes==6) {
		if (intDia > 30) return false;
	}           

	if (intMes==9) {
		if (intDia > 30) return false;
	}           
		
	if (intMes==11) {
		if (intDia > 30) return false;
	}           
 	  
	return true;
}
/* ******************************************************************** */
//Valida fechas en caso de que alguien la ingrese manualmente y no use el calendario
function validateDateCal(inputDate, pControlarFechaActual)
{
	if ((validaFrmFecha(inputDate.value) && (validarFechaActual(inputDate.value, pControlarFechaActual))) || (inputDate.value == ''))
	{
		return true;
	}
	else
	{	inputDate.value='';
		alert_customizado("La fecha ingresada es inv�lida");
		//document.getElementById(inputDate.id + "DIA").value = '';
		//document.getElementById(inputDate.id + "MES").value = '';
		//document.getElementById(inputDate.id + "ANN").value = '';
		//inputDate.value='';
		//popFrame.fPopCalendar(inputDate,inputDate,popCal);
		return false;
	}	
}
/* ******************************************************************** */
function validarFechaActual(pFecha, pControlarFechaActual)
{
	if (pControlarFechaActual)
	{
		// Parsea global de la fecha: formato (dd/mm/aaaa)
		lstDate = pFecha.split('/');
		var varFecha1=new Date(lstDate[2],lstDate[1] - 1,lstDate[0]);
		var varFecha2=new Date();
		if (varFecha2 - varFecha1 < 0)
		{
			return false;
		} else {
			return true;
		}
			
	}
	else
	{
		return true;
	}	
}
/* ******************************************************************** */
function distribuirFecha(obj)
{
	var strDate=obj.value;

	if (strDate != "") {
		// Parsea global de la fecha: formato (dd/mm/aaaa)
		lstDate = strDate.split('/');
		
		document.getElementById(obj.id + "DIA").value = lstDate[0];
		document.getElementById(obj.id + "MES").value = lstDate[1];
		document.getElementById(obj.id + "ANN").value = lstDate[2];
	}
}
/* ******************************************************************** */
function isNumber(theField)
{
	var checkOK = "0123456789";
	var checkStr = theField;
	var allValid = true;
	for (i = 0;  i < checkStr.length;  i++)
	{
		ch = checkStr.charAt(i);
		for (j = 0;  j < checkOK.length;  j++)
			if (ch == checkOK.charAt(j))
				break;
		if (j == checkOK.length)
		{
			allValid = false;
			break;
		}
	}
	if (!allValid)
	{
		return (false);
	}
	else
	{
		return (true);
	}
}
/* ******************************************************************** */
function agregarError(pIdObj, pMensaje, pSolapa) {

	//XSL
	var xslTablaErrores = new ActiveXObject("MSXML2.DOMDocument");
	xslTablaErrores.async = false;
	xslTablaErrores.load(document.getElementById("XSLTablaErrores").XMLDocument);
	var varResultTabla;
	xslTablaErrores.selectSingleNode("//img[@name='titulo']/@src").value = document.frmPaso2.CANALURL.value + "Canales/" + document.frmPaso2.CANAL.value + "/Images/circ_bco.gif";

	
}
/* ************************************************************************* */
function validarCalle(pTipo, pSINIEDOM, pSINIEDNU, pINTERDOM, pRUTANRO, pKILOMETRO, pTIPORUTA, pCRUCERUT, pMSGERROR) {

	//--- Combinaciones con calle / nro / interseccion ---------------------
	if (pTipo == "calle") {
		if (pSINIEDOM != "" && pSINIEDNU != "") 
			return true;
			
		if (pSINIEDOM != "" && pINTERDOM != "") 
			return true;
			
		if (pSINIEDOM != "" || pSINIEDNU != "" || pINTERDOM != "") 
		{
			return "Debe ingresar Calle/Nro o Calle/Intersecci�n";		
		}
	}
	//--- Fin de combinaciones con calle -----------------------------------
	
	//--- Combinaciones con ruta / kilometro / tipo / cruce ----------------
	if (pTipo == "ruta") {
		if (pRUTANRO != "" && pKILOMETRO != "")
			return true;		
			
		if (pRUTANRO != "" && pCRUCERUT != "") 
			return true;	
			
		if (pRUTANRO != "" || pKILOMETRO != "" || pCRUCERUT != "")
		{
			return "Debe ingresar Ruta Nro/Kil�metro o Ruta nro/Cruce con ruta Nro";
		}
	}
	//--- Fin de combinaciones con ruta -------------------------------------

	//Verifica que se haya ingresado algun dato...
	if (pSINIEDOM != "" || pSINIEDNU != "" || pINTERDOM != "" || pRUTANRO != "" || pKILOMETRO != "" || pCRUCERUT != "") 
	{
		return true;
	} else {	
		//Si llega hasta aca es porque no ingres� ningun dato...			
		if (pTipo == "ruta" || pTipo == "calle") {	
			return pMSGERROR;
		} else {
			return true;	
		}
	}
}
/* ******************************************************************** */

/*
Nombre Funcion: valida_texto.
Autor: LSP.
Fecha: 31/1/2005  14:31.
Descripcion: Funcion que recibe los siguientes parametros y valida segun ellos:
		texto 			=  el texto a verificar
		Tipo de dato a validar	= 'T' --> texto
				  		  'A' --> Alfanumerico
			  		  'N' --> numerico
			  		  'E' --> Email
			  		  'X' --> Nombre / Apellido
			  		 
	         Campo  		= Nombre del Campo
	         Area			= ?
	         Mensaje 		= El mensaje que se desea mostrar en el alert;
*/
function valida_texto(texto, tipo, campo, area, mensaje)
{
	var solotexto = "abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789./ -_'�";
	var alfanum = 'abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789.,;/()�?�!�� -_';
	var mail = 'abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789@._-';
	//26/10/2010 LR Defect 30, pto 5
	var nombre = "abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ 0123456789./-'&";
	var numeros = '0123456789';
	text=trim(texto);

	if(tipo=='X' || tipo=='x')
	{
	    for(k=0;k<texto.length;k++)
		{
			if(nombre.indexOf(texto.charAt(k)) == -1)
			{
				return false;
			}
		}
	}
	
	if(tipo=='T' || tipo=='t')
	{
	    for(k=0;k<texto.length;k++)
		{
			if(solotexto.indexOf(texto.charAt(k)) == -1)
			{
				return false;
			}
		}
	}

	if(tipo=='A' || tipo=='a')
	{
		for(k=0;k<texto.length;k++)
		{
			if(alfanum.indexOf(texto.charAt(k)) == -1)
			{
				return false;
			}
		}
	}
  
	if(tipo=='N' || tipo=='n')
	{
		for(k=0;k<texto.length;k++)
		{
			if(numeros.indexOf(text.charAt(k)) == -1)
			{
				return false;
			}
		}
	}
	
	if(tipo=='E' || tipo=='e')
	{
		if (texto == "")
			return false;
		else
		{
			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(texto))   
				return true;
			else 
				return false;			
		}
	}
	//DEFECT 277
	var telefono = '0123456789-() '
	if(tipo.toUpperCase()=='TEL' )
	{
		for(k=0;k<texto.length;k++)
		{
			if(telefono.indexOf(text.charAt(k)) == -1)
			{
				return false;
			}
		}
    }
    //Ticket 675119
    var nombreycoma = alfanum + "'";
    if (tipo.toUpperCase() == 'CC') {
        for (k = 0; k < texto.length; k++) {
            if (nombreycoma.indexOf(text.charAt(k)) == -1) {
                return false;
            }
        }
    }
    return true;
}
/* ******************************************************************** */        
function validaNoTodosCeros(pTexto)
{
	var varCeros = '0';
	var varTextAux = trim(pTexto);
	
	if (varTextAux.replace(/0/g,'') == '')
	{
		return false;
	} else {
		return true;
	}
}
/* ******************************************************************** */        		
function validaNoSecuencia(pTexto)
{
	//valida secuencia ascendente
	var varNroAnt = pTexto.charAt(0) * 1;
	var varSecuenciaAsc = true;
	for(k=1; k<pTexto.length; k++)
	{
		if((pTexto.charAt(k) * 1) != (varNroAnt + 1))
		{
			varSecuenciaAsc = false;
		}
		varNroAnt = pTexto.charAt(k) * 1;
	}

	//valida secuencia descendente
	varNroAnt = pTexto.charAt(0) * 1;
	var varSecuenciaDes = true;
	for(k=1; k<pTexto.length; k++)
	{
		if((pTexto.charAt(k) * 1) != (varNroAnt - 1))
		{
			varSecuenciaDes = false;
		}
		varNroAnt = pTexto.charAt(k) * 1;
	}
	
	if (varSecuenciaAsc || varSecuenciaDes) 
	{
		return false;
	} else {
		return true;	
	}
}
/* ******************************************************************** */        		
function validaNoLetrasRepetidas(pTexto)
{
	//valida repeticion de letras
	var varLetraAnt = pTexto.charAt(0);
	var varCantLetraAnt = 1;
	var varLetrasRepetidas = false;
	
	//Constante que determina el maximo de letras repetidas en una cadena
	var varMaximoRepeticionesPermitidas = 3;
	
	for(k=1; k<pTexto.length; k++)
	{
		if((pTexto.charAt(k)) == (varLetraAnt))
		{
			varCantLetraAnt++;
		} else {
			varCantLetraAnt = 1;
		}
		
		if (varCantLetraAnt >= varMaximoRepeticionesPermitidas)
		{
			varLetrasRepetidas = true;
		}
			
		varLetraAnt = pTexto.charAt(k);
	}

	
	if (varLetrasRepetidas) 
	{
		return false;
	} else {
		return true;	
	}
}
/* ******************************************************************** */        		
function validaNoNrosIguales(pTexto)
{
	//valida repeticion de n�meros
	var varContIguales = 0;
	var varInd;
	var varLetraPivot = pTexto.charAt(0);

	for (varInd=0; varInd < pTexto.length; varInd++)
	{
		if (pTexto.charAt(varInd) == varLetraPivot)
		{
			varContIguales++;
		}
	}

	if (varContIguales == pTexto.length)
		return false;
	else
		return true;

}
/* ******************************************************************** */        
function validaNoLetrasIguales(pTexto)
{
	//valida repeticion de Letras
	var varContIguales = 0;
	var varInd;
	var varLetraPivot = pTexto.charAt(0);

	for (varInd=0; varInd < pTexto.length; varInd++)
	{
		if (pTexto.charAt(varInd) == varLetraPivot)
		{
			varContIguales++;
		}
	}

	if (varContIguales == pTexto.length)
		return false;
	else
		return true;

}
/* ******************************************************************** */        
function validaExisteEspacio(pTexto)
{
	//Si pTexto contiene un espacio, devuelve true, sino false
	if (pTexto.indexOf(' ') >= 0)
		return true;
	else
		return false;

}
/* ******************************************************************** */        
function validaFechaDenuncia()
{
	lstDate = (document.getElementById("SINFE").value).split('/');
	var varFecSin=new Date(lstDate[2],lstDate[1] - 1,lstDate[0]);
	lstDate = (document.getElementById("DENUC").value).split('/');
	var varFecDen=new Date(lstDate[2],lstDate[1] - 1,lstDate[0]);

	if (varFecDen - varFecSin >= 0)			
	{
		return true;
	} else {
		return false;	
	}
}
/* ******************************************************************** */        
function validaFechaAparecido()
{
	var varFecSin=new Date(document.getElementById("SINFEANN").value, document.getElementById("SINFEMES").value - 1, document.getElementById("SINFEDIA").value);
	lstDate = (document.getElementById("APARE").value).split('/');
	var varFecDen=new Date(lstDate[2],lstDate[1] - 1,lstDate[0]);

	if (varFecDen - varFecSin >= 0)			
	{
		return true;
	} else {
		return false;	
	}
}
/* ******************************************************************** */        
function validarPatente(pPatente)
{
	var varLetras = 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ';
	var varNumeros = '0123456789';

	var varText = (trim(pPatente.replace(/-/g,""))).toUpperCase();
	var varError = false;
	
	if (varText.length == 3)
	{
		if (varText != 'A/D') 
		{
			varError = true;
		} else {
			return true;	
		}
	} 
	else if (varText.length == 6)
	{
		//Prueba con el formato LLLNNN (auto com�n)
		for(k=0;k<3;k++)
		{
			if(varLetras.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		for(k=3;k<6;k++)
		{
			if(varNumeros.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		if (!varError) 
		{
			return true;
		}	

		//Prueba con el formato LLNNNN (auto diplom�tico)	
		varError = false;
		for(k=0;k<2;k++)
		{
			if(varLetras.indexOf(varText.charAt(k)) == -1) 
			{
				varError = true;
			}
		}
		for(k=2;k<6;k++)
		{
			if(varNumeros.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		if (!varError) 
		{
			return true;
		}	
		
		//Prueba con el formato NNNLLL (moto)	
		varError = false;
		for(k=0;k<3;k++)
		{
			if(varNumeros.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		for(k=3;k<6;k++)
		{
			if(varLetras.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		if (!varError) 
		{
			return true;
		}	
	} 
	else if (varText.length == 9)
	{	
		//Prueba con el formato NNNLLLNNN (acoplado)
		varError = false;

		for(k=0;k<3;k++)
		{
			if(varNumeros.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		for(k=3;k<6;k++)
		{
			if(varLetras.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		for(k=6;k<9;k++)
		{
			if(varNumeros.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		if (!varError) 
		{
			return true;
		}	
	}
	else if (varText.length >= 7)
	{	
		//Prueba con el formato de patente vieja (1 letra, 6 numeros o mas) LNNNNNN
		varError = false;
		if(varLetras.indexOf(varText.charAt(0)) == -1)
		{
			varError = true;
		}
		for(k=1;k<(varText.length - 1);k++)
		{
			if(varNumeros.indexOf(varText.charAt(k)) == -1)
			{
				varError = true;
			}
		}
		if (!varError) 
		{
			return true;
		}	
	}
		
	//Si llego hasta aca es porque no matcheo con ningun formato de
	//patente permitido, por lo tanto devuelve false
	return false;
}
/* ******************************************************************** */        
function validarCampoIndividual(pObj)
{
	//Obtener campos del xml y matchearlos con el form
	var xmlCampos = document.getElementById('xmlDefinicionCamposForm').XMLDocument;
	
	//Variables definadas por cada campo dentro del XML
	var varTipo = "";
	var varLongitud = "";
	var varLongitudMinima = "";
	if (xmlCampos.selectSingleNode("//CAMPO[@nombre='" + pObj.id + "']") != null)
	{
		//Campos leidos del XML
		varTipo = xmlCampos.selectSingleNode("//CAMPO[@nombre='" + pObj.id + "']").attributes.getNamedItem('tipo').value;
		varLongitud = xmlCampos.selectSingleNode("//CAMPO[@nombre='" + pObj.id + "']").attributes.getNamedItem('longitud').value;
		varLongitudMinima = xmlCampos.selectSingleNode("//CAMPO[@nombre='" + pObj.id + "']").attributes.getNamedItem('longitudMinima').value;
		return validarFormato(pObj, varTipo, varLongitud, varLongitudMinima);
	} else {
		return true;
	}
}

// Adriana Funcion para mostrar Fuera de norma  19-4-11 ticket 599651 PRIMA MINIMA 
/*function fncMuestraFueraNorma() {
	       		
	        document.getElementById("xmlfueraNorma").value = oXMLFueraNorma.xml
	        var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
	        varXslFueraNorma.async = false;
	        varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);	        
	        var  mvarTablaFueraNorma = oXMLFueraNorma.transformNode(varXslFueraNorma);
	        document.getElementById("divTablaFueraNorma").innerHTML = mvarTablaFueraNorma;  
	        document.getElementById("divTablaFueraNorma").style.display ='inline';
	          if (document.getElementById("chk_circuito").checked || document.getElementById("divTablaFueraNorma").innerHTML!="")
	 	{
	 		document.getElementById("copiaCliente").disabled = true
	 	 	document.getElementById("copiaAmbos").disabled = true
	 	 	document.getElementById("divBtnEnvioEmail").style.display='none'
	 	} else {
			document.getElementById("divBtnEnvioEmail").style.display='inline'
			document.getElementById("copiaCliente").disabled = false
			document.getElementById("copiaAmbos").disabled = false
		}

}*/

// Adriana Funcion para eliminar un error de Fuera de norma  19-4-11 ticket 599651 PRIMA MINIMA
function eliminarXMLFueraNorma(pCampo,pItem)
{	

	try
	{
	varXmlNodo= oXMLFueraNorma.selectSingleNode("//ERRORES/SOLAPA/ERROR[CAMPO='" + pCampo  + "' and NROITEM='"+pItem+"']")
	varXmlNodo.parentNode.removeChild(varXmlNodo);		
	
	}
	catch(error){}
}