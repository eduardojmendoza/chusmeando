/*
 ------------------------------------------------------------------------------
 Fecha de Modificaci�n: 20-8-12
 PPCR: SPLI LBA
 Desarrolador: Adriana Armati
 Descripci�n: Sacar accesos a componentes HSBC

------------------------------------------------------------------------------
Fecha de Modificaci�n: 02/05/2012
Ticket: 731841
Desarrollador: Mart�n Cabrera
Descripci�n: Se recuperan legajos tambien del AIS si no se encuentra en RRHH
------------------------------------------------------------------------------
Fecha de Modificaci�n: 08/02/2012
PPCR: 2011-00056
Desarrollador: Marcelo Gasparini
Descripci�n: Se agrega producto ICB1
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 21/12/2011
PPCR: 50055/6010661
Desarrolador: Leonardo Ruiz
Descripci�n: Soporte de HTTPS.-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 01/12/2011
Ticket: 682585
Desarrollador: Marcelo Gasparini
Descripci�n: Se agrega validaci�n de no permitir CUIL con Persona Fisica en Datos Cliente
'--------------------------------------------------------------------------------
' Fecha de Modificaci�n: 01/07/2011
' Ticket 632535
' Desarrolador: Gabriel D'Agnone
' Descripci�n: MOSTRAR PRIMAS Y TASAS EN IMPRESO A REVISION
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 02/06/2011
PPCR: 50055/6010661
Desarrollador: Leonardo Ruiz
Descripci�n: Validar que para enviar o guardar la soli debe Elegir un Vendedor.-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 19/04/2011
Ticket 599651
Desarrolador: Adriana Armati
Descripci�n: Se agrega funcionalidad de Prima m�nima
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 25/04/2011
Ticket : 609478
Desarrolador: Marcelo Gasparini
Descripci�n: Se corrige nuevamente envio de Recargo Financiero discriminado por Forma de pago
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 26/01/2011
Ticket: 577743
Desarrolador: Elizabeth Gregorio
Descripci�n: Se corrige cantidad de alumnos que no este en cero y carteles para ICO
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 03/01/2011
Ticket: 569288
Desarrolador: Elizabeth Gregorio
Descripci�n: Se corrige cantidad de alumnos y carteles para ICO
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 30/11/2010
PPCR: 50055/6010661
Desarrolador: Marcelo Gasparini
Descripci�n: Se corrige envio de Recargo Financiero discriminado por Forma de pago
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 29/07/2010
PPCR: 50055/6010661
Desarrolador: Hern�n R. Fus�
Descripci�n: Adapataci�n de XMLs de mas de 8000 caracteres pasados a BD
--------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: IntComercio.js

Fecha de Creaci�n: 03/05/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripci�n: Este archivo JS se crea para definir funciones generales

*/

/* ************************************************************************* 
	VARIABLES GLOBALES
**************************************************************************/

oXML = new ActiveXObject('MSXML2.DOMDocument');    
oXML.async = false;  

oXMLFueraNorma = new ActiveXObject('MSXML2.DOMDocument');    
oXMLFueraNorma.async = false;  

oXMLPopUpFueraNorma = new ActiveXObject('MSXML2.DOMDocument');    
oXMLPopUpFueraNorma.async = false;  
oXMLPopUpFueraNorma.loadXML("<ERRORES></ERRORES>")

oXMLPlanesAIS = new ActiveXObject('MSXML2.DOMDocument');    
oXMLPlanesAIS = false

oXMLPlanes = new ActiveXObject('MSXML2.DOMDocument');        
oXMLPlanes.async = false;  	
oXMLPlanes.setProperty("SelectionLanguage", "XPath");

var xmlItemNro = "1";
//GED Nro Maximo de Items por default para el alta
var xmlItemNroMaximo

var datosCompletos=false
var varTablaFueraNorma

var varPopUpFueraNorma

var mvarItemsParseados = false

var mvarObjAsegMaxIte = 50;
var mvarObjAsegMaxCob = 40;
var mvarObjAsegMaxObj = 10;

var flagCotizacionModificada = false

var idItemEditado = ""

var flagPoseeSiniestralidad= false

var matrizCalle = new Array();
 var matrizNroCalle = new Array();
 var matrizPiso = new Array();
var  matrizDpto = new Array();
 var matrizCodPostal = new Array();

 var fueraManual = new Array()


 
 
 
 var flagPuedeCambiarSolapa =true
 for (var x=0;x<50;x++)
 {
 	matrizCalle[x] = ""
	 matrizNroCalle[x]  = ""
	 matrizCodPostal[x] = ""
	 matrizDpto[x] = ""
	 matrizPiso[x] = ""
	//fueraManual[x] = false

}
/*****************************************************************************/
function obtenerPolizasAIS()
{
	
	var strData = 'FUNCION=mensajeMQ&RequestXMLHTTP=<Request><DEFINICION>1121_ListadePolizas.xml</DEFINICION><AplicarXSL>cotizador/getComboPolizasMasivos.xsl</AplicarXSL><CIAASCOD>0020</CIAASCOD><CANAL></CANAL><PRODUCTO></PRODUCTO></Request>';
	
	var respuestaXMLHTTP =fncLlamadaXMLhttp(strData)		
	//   window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+respuestaXMLHTTP+"</textarea>")
	     
	    
	     return respuestaXMLHTTP
	
	
}

function fncProvinciaSeleccionada(pCombo,pLocalidad)
{
	
	//document.getElementById(pLocalidad).length = 0;
	var mvarRequest="<Request>" +
			"<DEFINICION>SPSNCV_LBATPOSL_TRAELOCALIDADES.xml</DEFINICION>" +
			"<PROVICOD>" + pCombo.value + "</PROVICOD>" +
			"<LOCALIDAD></LOCALIDAD>" + 	
			"</Request>"

        //window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+"/Oficina_Virtual/ASP/Cotizador/CoreCotizadores/CotizadoresHogar/Forms/XSL/getComboLocalidades.xsl"+"</textarea>")
       
        //datosCotizacion.cargaComboSQL(pLocalidad,llamadaMensajeSQL(mvarRequest),datosCotizacion.getCanalURL() +"Forms/XSL/getComboLocalidades.xsl")
        datosCotizacion.cargaComboSQL(pLocalidad,llamadaMensajeSQL(mvarRequest),document.getElementById("getComboLocalidades").xml)
       
       if (document.getElementById(pLocalidad).length==2) document.getElementById(pLocalidad).selectedIndex=1
	document.getElementById(pLocalidad).disabled=false
}

function fncProvinciaSeleccionadaConCP(pCombo,pLocalidad,pcodPostal)
{
	 
      document.getElementById(pLocalidad).value=""
	
	if(pcodPostal)
		document.getElementById(pcodPostal).value=""
       	 
      if ( pCombo.value==-1)   
      		{      		
         	document.getElementById(pLocalidad).disabled=true
        	}
        else
        	{
		document.getElementById(pLocalidad).disabled=false
		datosCotizacion.mostrarCampo("codPostalRiesgo")
		
		document.getElementById("botonBuscarLocalidad").style.display="inline"
		  if ( pCombo.value=="1") 
		  { //capital federal
		  document.getElementById(pLocalidad).disabled=true
	  	  document.getElementById("botonBuscarLocalidad").style.display="none"
		   datosCotizacion.ocultarCampo("codPostalRiesgo")
	         document.getElementById(pLocalidad).disabled=true
	         document.getElementById(pLocalidad).value="CAPITAL FEDERAL"
		  if(pcodPostal) document.getElementById(pcodPostal).value="" //"1005";	
				
        	} 
		  
	}
	
}

function fncCargaParamBuscaLoc(pProvincia,pLocalidad,pCodPostal,pCalle,pNroCalle,pSolapa)
{
 document.getElementById("pCpoProvLocBuscar").value=pProvincia
document.getElementById("pCpoLocBuscar").value= pLocalidad
document.getElementById("pCpoCodPostalLocBuscar").value = pCodPostal
document.getElementById("pCpoCalleBuscar").value= pCalle
document.getElementById("pCpoNroCalleBuscar").value = pNroCalle

	//Callejero para Capital
	if( document.getElementById(pProvincia).value=="1")
		{//Si recibo la Calle vengo de busqueda codigos para capital
		if (pCalle)
			{				
				var vueltaCallejero= fncCallejero(pCalle, pNroCalle, pProvincia ,pCodPostal,pSolapa);
				
				if (vueltaCallejero)	return true
				else
				return false
			}	
		}
	else
		fncBuscaLocalidad()

return false
}

function fncBuscaLocalidad(pInterno)
{
	pProvincia= document.getElementById("pCpoProvLocBuscar").value
	pLocalidad=document.getElementById("pCpoLocBuscar").value
	pCodPostal=document.getElementById("pCpoCodPostalLocBuscar").value
	
	document.getElementById(pCodPostal).value=""
	
	puntero=-1
	var provincia=document.getElementById(pProvincia).value
	if(pInterno){
		var localidad= document.getElementById("localidadBuscar").value
		
	}
	else{
		var localidad= document.getElementById(pLocalidad).value
		
	}

document.getElementById("localidadBuscar").value=localidad

if (localidad.length<3 ) 
{
	if (!pInterno)	alert_customizado("Debe ingresar al menos tres caracteres")
else
	alert_customizado("Debe ingresar al menos tres caracteres")
return
}
var pX=400
var pY= 200
	
//	document.getElementById("spanEsperando").innerHTML="<b><br/>Buscando, por favor espere...</b>"	
	var strData = 'FUNCION=mensajeSQL&'+	
				'RequestXMLHTTP=<Request>'+
				'<DEFINICION>SPSNCV_LBATPOSL_TRAELOCALIDADES.xml</DEFINICION>'+
				'<PROVICOD>'+ provincia +'</PROVICOD>'+
				'<LOCALIDAD>'+ localidad +'</LOCALIDAD>'+
				'</Request>'
				
											
		var respuestaXMLHTTP =fncLlamadaXMLhttp(strData);
				
		//oXML = new ActiveXObject('MSXML2.DOMDocument');        
		//oXML.async = false;  	
		if(oXML.loadXML(respuestaXMLHTTP))
		{			
		
			if (oXML.selectSingleNode("//LOCALIDADES"))
			{	
				
				var cantLocalidades=oXML.selectNodes("//LOCALIDADCABECERA").length
				
				
				if (cantLocalidades==0) 
				{
					
				var tabla="<table width='100%' bgcolor='#FFFFFF' cellspacing='0' cellpadding='0' style='border: solid #D0D0D0 1px';>"+
					"<tr class='Form_campname_bcoL'><td colspan='2' style='text-align:right' >"+
					"<tr ><td width='380px' ><div class='areaDatTit'><div class='areaDatTitTex' style='font-size:10px;'>Localidad</div>"+
       				 "</div></td><td width='100px'>"+
       				 "<div class='areaDatTit'><div class='areaDatTitTex' style='font-size:10px;'>Cod. Postal</div></div></td></tr>"
				
					tabla+="<tr><td colspan='2'><div style='width:"+pX+"px; height:"+pY+"px; overflow:auto'><table>"					
					tabla+="<tr id='fila_"+x+ "'  class='form_campname_bcoR'   style='border:solid 1px #000000' onclick='fondoGrisSeleccion(this)' >"+
					    "<td colspan='2' width='400px' style='text-align:left'>No se encontraron localidades</td>"+					   		
					    "</tr>"
			
					tabla+="</table></div></td></tr></table><p></p>"	
					
					var ancho=document.body.clientWidth
					var body = document.body, 	html = document.documentElement; 		
					var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

					
					
						
					document.getElementById('popUp_opacidad').style.width  = ancho
					document.getElementById('popUp_opacidad').style.height  = alto
					document.getElementById('popUp_Localidad').style.left  = 200
					document.getElementById('popUp_Localidad').style.top  =   body.scrollTop+100
					document.getElementById('popUp_opacidad').style.display = 'inline';
					//opacity('popUp_opacidad', 0, 10, 300);
					document.getElementById('popUp_Localidad').style.display = 'inline';
					document.getElementById("div_contenidoLocalidad").innerHTML = tabla;
				
				}
				
			else if (cantLocalidades>1)
				{
			//*************
			
					
				var tabla="<table width='100%' bgcolor='#FFFFFF' cellspacing='0' cellpadding='0' style='border: solid #D0D0D0 1px';>"+
					"<tr class='Form_campname_bcoL'><td colspan='2' style='text-align:right' >"+
					"<tr ><td width='380px' ><div class='areaDatTit'><div class='areaDatTitTex' style='font-size:10px;'>Localidad</div>"+
       				 "</div></td><td width='100px'>"+
       				 "<div class='areaDatTit'><div class='areaDatTitTex' style='font-size:10px;'>Cod. Postal</div></div></td></tr>"
				
				
					tabla+="<tr><td colspan='2'><div style='width:"+pX+"px; height:"+pY+"px; overflow:auto'><table cellspacing='0' cellpadding='0'>"
					for (var x=0;x<cantLocalidades;x++)
					{
						var colorFondo= (x%2==0)?"form_campname_bcoR":"Form_campname_colorL"
						tabla+="<tr id='fila_"+x+ "'  class='"+colorFondo+"'  style='height:15px'  onclick='fondoGrisSeleccion(this)' >"+
					    	"<td width='400px' style='text-align:left'>"+
					    	"<a style='font-weight:normal;color:#000000;cursor:hand' ondblclick='completaLocalidad("+x+","+pLocalidad.toString()+","+pCodPostal.toString()+");  fncMuestraCoberturas(); return false;'  >"+oXML.selectSingleNode("//LOCALIDADCABECERA["+x+"]/LOCALIDAD").text+"</a></td>"+
					    	"<td width='70px' style='text-align:center'>"+oXML.selectSingleNode("//LOCALIDADCABECERA["+x+"]/CODPOS").text+"</td>"+							
					    	"</tr>"
					}
					tabla+="</table></div></td></tr></table><p></p>"	
			
					var ancho=document.body.clientWidth
					var body = document.body, 	html = document.documentElement; 		
					var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
					
					document.getElementById('popUp_opacidad').style.width  = ancho
					document.getElementById('popUp_opacidad').style.height  = alto
					document.getElementById('popUp_Localidad').style.left  = 200
					document.getElementById('popUp_Localidad').style.top  =  body.scrollTop+100
					
					document.getElementById('popUp_opacidad').style.display = 'inline';
					//opacity('popUp_opacidad', 0, 10, 300);
					document.getElementById('popUp_Localidad').style.display = 'inline';
					document.getElementById("div_contenidoLocalidad").innerHTML = tabla;
						
					//*************	
				}
				else
				{	
				//GED 17-8-2010 OCULTA CUANDO HAY UN SOLO RESUTADO 
				if(pInterno) ocultarPopUpLoc('popUp_Localidad');
					document.getElementById(pLocalidad).value=oXML.selectSingleNode("//LOCALIDADCABECERA[0]/LOCALIDAD").text;	
					document.getElementById(pCodPostal).value=oXML.selectSingleNode("//LOCALIDADCABECERA[0]/CODPOS").text;	
					fncMuestraCoberturas()
				}
		
		


		}
	}
}

function completaLocalidad(pPuntero,pLocalidad,pCodPostal)
{
	
     	pLocalidad	=document.getElementById("pCpoLocBuscar").value
	pCodPostal =document.getElementById("pCpoCodPostalLocBuscar").value  
	
	
	//alert (pLocalidad+ " - "+ pCodPostal)
	if(puntero!=-1)
	{	x=pPuntero
		if(pPuntero=="x")
			x=puntero
			
		
		//var oXMLLocalidad = new ActiveXObject('MSXML2.DOMDocument');    
		var localidad= oXML.selectSingleNode("//LOCALIDADCABECERA["+x+"]/LOCALIDAD").text;
		var codpostal= oXML.selectSingleNode("//LOCALIDADCABECERA["+x+"]/CODPOS").text;
		//Asigo los valores a los imput de la pagina
		//pCodPostal.value=codpostal;
		//pLocalidad.value=localidad;
		
		document.getElementById(pCodPostal).value = codpostal
		
		document.getElementById(pLocalidad).value = localidad
		
		//document.getElementById(pCodPostal).value=codpostal;
		//document.getElementById(pLocalidad).value=localidad;
		// Oculto el PopUp al seleccionar un dato
		ocultarPopUpLoc('popUp_Localidad');
	}
}
/**********************************************************************************************/
function fncConvenioProdSeleccionado()
{
	//------------------------------------------------------------------------------------------//
	//	18/03/2011 SE SEPARAN CONVENIOS/COMISIONES PARA LOS CASOS DE ALTAS Y RENOVACIONES
	//------------------------------------------------------------------------------------------//
	//------------------------------------------------------------------------------------------//
	//	BEGIN	-	CASO ALTA
	//------------------------------------------------------------------------------------------//
	if (document.getElementById("TIPOOPER").value == "A")
	{
		document.getElementById("convenioOrganizador").length = 0;
		if (document.getElementById("convenioProductor").value.split("|")[0]== '-2'){
	
			oXMLRenovaPoliCol = new ActiveXObject('MSXML2.DOMDocument');        
			oXMLRenovaPoliCol.async = false;       
		  	oXMLRenovaPoliCol.loadXML(document.getElementById("xmlRenovaPoliCol").innerHTML); 
			
			var vCOMPCPOROR = Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text)
		  	var nuevoElemento=document.createElement("option");
			nuevoElemento.text='FUERA DE CONVENIO';
			nuevoElemento.value='-2|-2|0|'+vCOMPCPOROR;
			nuevoElemento.selected=true;
			document.getElementById("convenioOrganizador").length=0;					
			document.getElementById("convenioOrganizador").add(nuevoElemento);
			document.getElementById("convenioOrganizador").disabled=false;
			
			
			
			document.getElementById("comisionTotal").length=0;
			var vComisionTotal=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text) + Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text);	
		  	var nuevoElemento=document.createElement("option");
			nuevoElemento.text=formatoNro('',vComisionTotal,2,",")+ ' %';
			nuevoElemento.value=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text) + Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text);
			nuevoElemento.selected=true;
			document.getElementById("comisionTotal").add(nuevoElemento);
			document.getElementById("comisionTotal").disabled=false;		
		}
		else{	 	
			mvarRequest = "<Request>"+		               
				"<DEFINICION>1555_ConvenioProdOrganiz.xml</DEFINICION>"+
				"<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
				"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
				"<AGENCOD>"+ document.getElementById("I_ORGANIZADOR").value.split("|")[0] +"</AGENCOD>"+
				"<AGENCLA>"+ document.getElementById("I_ORGANIZADOR").value.split("|")[1]  +"</AGENCLA>"+
				"<CONVECOP>"+ document.getElementById("convenioProductor").value.split("|")[0] +"</CONVECOP>"+
				"</Request>"
			//window.document.body.insertAdjacentHTML("beforeEnd", "ConvenioOrganizador<textarea>"+mvarRequest+"</textarea>")
			document.getElementById("convenioOrganizador").disabled=false
			document.getElementById("comisionTotal").disabled=false
			var respuestaMQ=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')
			//datosCotizacion.cargaComboXSL("convenioOrganizador",respuestaMQ,datosCotizacion.getCanalURL() +"Forms/XSL/getComboConveniosProd.xsl")
			datosCotizacion.cargaComboXSL("convenioOrganizador",respuestaMQ,document.getElementById("getComboConveniosProd").xml)
		}	
	}
	//------------------------------------------------------------------------------------------//
	//	END	-	CASO ALTA
	//------------------------------------------------------------------------------------------//
	
	//------------------------------------------------------------------------------------------//
	//	BEGIN	-	CASO RENOVACION
	//------------------------------------------------------------------------------------------//
	if (document.getElementById("TIPOOPER").value == "R")
	{
		//alert(  document.getElementById("convenioProductor").value  +" ** "+ document.getElementById("convenioOrganizador").value )
	
		document.getElementById("convenioOrganizador").length = 0;
		oXMLRenovaPoliCol = new ActiveXObject('MSXML2.DOMDocument');
		oXMLRenovaPoliCol.async = false;
		 oXMLRenovaPoliCol.loadXML(document.getElementById("xmlRenovaPoliCol").innerHTML);
		
		//03/01/2011 LR Si la poliza tiene conv de organiz, pero no de prod
		if (document.getElementById("convenioProductor").value.split("|")[0]== '-2')
		{
			var vCOMPCPOROR = Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text)
		  	var nuevoElemento=document.createElement("option");
			nuevoElemento.text='FUERA DE CONVENIO';
			nuevoElemento.value='-2|-2|0|'+vCOMPCPOROR;
			nuevoElemento.selected=true;
			document.getElementById("convenioOrganizador").length=0;					
			document.getElementById("convenioOrganizador").add(nuevoElemento);
			document.getElementById("convenioOrganizador").disabled=false;		
			document.getElementById("comisionTotal").length=0;
			var vComisionTotal=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text) + Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text);	
		  	var nuevoElemento=document.createElement("option");
			nuevoElemento.text=formatoNro('',vComisionTotal,2,",")+ ' %';
			nuevoElemento.value=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text) + Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text);
			nuevoElemento.selected=true;
			document.getElementById("comisionTotal").add(nuevoElemento);
			document.getElementById("comisionTotal").disabled=false;		
		}
		else
		{
			//03/01/2011 LR Si la poliza tiene conv de organiz, pero no de prod
			var vConvProd=document.getElementById("convenioProductor").value.split("|")[0]!="-2"?document.getElementById("convenioProductor").value.split("|")[0]:""
			if(vConvProd == "-1")
				vConvProd = "";
			
			mvarRequest = "<Request>"+		               
				"<DEFINICION>1555_ConvenioProdOrganiz.xml</DEFINICION>"+
				"<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
				"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
				"<AGENCOD>"+ document.getElementById("I_ORGANIZADOR").value.split("|")[0] +"</AGENCOD>"+
				"<AGENCLA>"+ document.getElementById("I_ORGANIZADOR").value.split("|")[1]  +"</AGENCLA>"+
				"<CONVECOP>"+ vConvProd +"</CONVECOP>"+
				"</Request>"
			//window.document.body.insertAdjacentHTML("beforeEnd", "ConvenioOrganizador<textarea>"+mvarRequest+"</textarea>")
			document.getElementById("convenioOrganizador").disabled=false
			document.getElementById("comisionTotal").disabled=false
			var respuestaMQ=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')
			
			/*//DEFECT 
			
			if (document.getElementById("TIPOOPER").value == "R")
			{
				oXMLMQ = new ActiveXObject('MSXML2.DOMDocument');
				oXMLMQ.async = false;
			  	oXMLMQ.loadXML(respuestaMQ);
			  	
			  	if (oXMLMQ.selectSingleNode("//CANTELEM").text == "0") 
			  	{
			  		mvarRequest = "<Request>"+		               
					"<DEFINICION>1555_ConvenioProdOrganiz.xml</DEFINICION>"+
					"<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
					"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
					"<AGENCOD>"+ document.getElementById("I_ORGANIZADOR").value.split("|")[0] +"</AGENCOD>"+
					"<AGENCLA>"+ document.getElementById("I_ORGANIZADOR").value.split("|")[1]  +"</AGENCLA>"+
					"<CONVECOP></CONVECOP>"+
					"</Request>"
			  	
			  	var respuestaMQ=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')
			
		  		oXMLMQ.loadXML(respuestaMQ);
		  
		  		var vConvOrg = oXMLRenovaPoliCol.selectSingleNode("//CONVECODOR").text
		  	
		  	var cantConvenios = oXMLMQ.selectNodes("//CONVENIO[CODIGO='" + vConvOrg+ "']" ).length
		  	if ( cantConvenios != 1) 	  	
		  	{
		  		var nuevoElemento=document.createElement("option");
						nuevoElemento.text='NO SE ENCONTRARON CONVENIOS';
						nuevoElemento.value='0|0|0';			
						nuevoElemento.selected=true;
						document.getElementById("convenioOrganizador").length=0;					
						document.getElementById("convenioOrganizador").add(nuevoElemento);
						document.getElementById("convenioOrganizador").disabled=false;
		  	 }
			}
		}
		
		// FIN DEFECT*/
			
			//datosCotizacion.cargaComboXSL("convenioOrganizador",respuestaMQ,datosCotizacion.getCanalURL() +"Forms/XSL/getComboConveniosProd.xsl")
			datosCotizacion.cargaComboXSL("convenioOrganizador",respuestaMQ,document.getElementById("getComboConveniosProd").xml)
		}
		
		//Verifica si se modific� el valor del campo
		fncCambiarColorCpoRen(document.getElementById("convenioOrganizador"))
	}
	//------------------------------------------------------------------------------------------//
	//	END	-	CASO RENOVACION
	//------------------------------------------------------------------------------------------//
}
/**********************************************************************************************/
function fncPeriodoSeleccionado(){
	
	var vPeriCoti = document.getElementById("periodoCotizar").value==-1?0:document.getElementById("periodoCotizar").value;
		
		document.getElementById("estabilizacion").length = 0;
		
		mvarRequest = "<Request>"+		               
	                   	"<DEFINICION>1552_Estabilizacion.xml</DEFINICION>"+
				            	"<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
				            	"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
				           		"<PERIODO>"+ vPeriCoti +"</PERIODO>"+
									"</Request>"
		//datosCotizacion.cargaComboXSL("estabilizacion",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL()+"Forms/XSL/getComboEstabilizacion.xsl")
		if (document.getElementById("RAMOPCODSQL").value!="ICB1"){
			datosCotizacion.cargaComboXSL("estabilizacion",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboEstabilizacion").xml)
		}
		else{
        	var xmlSinEst = fncSinEst(llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'));
			datosCotizacion.cargaComboXSL("estabilizacion",xmlSinEst,document.getElementById("getComboEstabilizacion").xml)
		}		
		
	  mvarRequest = "<Request>"+		               
	                   "<DEFINICION>2120_PlanPagoxProdPolMedCobro.xml</DEFINICION>"+
			            "<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
			            "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value  +"</RAMOPCOD>"+
			            "<POLIZANN>00</POLIZANN>"+
	                    "<POLIZSEC>000000</POLIZSEC>"+
	                    "<COBROCOD>0000</COBROCOD>"+
	                    "<COBROTIP> </COBROTIP>"+  
	                    "<COBROFOR>"+ vPeriCoti +"</COBROFOR>"    +
			"</Request>"

	  //window.document.body.insertAdjacentHTML("beforeEnd", "<br>objXMLItem <textarea>" + mvarRequest + "</textarea>");
		 
		 document.getElementById("planPago").length = 0;
		 //datosCotizacion.cargaComboXSL("planPago",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL()+"Forms/XSL/getComboPlanPago.xsl")
	//jc icobb, si es icobb solo se acepta el plan de 10 cuotas
		//datosCotizacion.cargaComboXSL("planPago",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboPlanPago").xml)
    if (document.getElementById("RAMOPCOD").value == document.getElementById("RAMOPCODSQL").value) {
		datosCotizacion.cargaComboXSL("planPago",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboPlanPago").xml)
    }
    else
    {
        var xml10cuotas = fncSolo10(llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'));
		datosCotizacion.cargaComboXSL("planPago",xml10cuotas,document.getElementById("getComboPlanPago").xml)
    }
	// jc fini icobb ef
	
	
	fncFechaFin("C")
	
	//Renovaci�n
	if (document.getElementById("TIPOOPER").value == "R")
	{
		if (document.getElementById("periodoCotizar").valorOriginal!=null)
		{
			if (trim(document.getElementById("periodoCotizar").value.toUpperCase()) == trim(document.getElementById("periodoCotizar").valorOriginal.toUpperCase()))
			{
				oXMLRenovaPoliCol = new ActiveXObject('MSXML2.DOMDocument');
				oXMLRenovaPoliCol.async = false;
			  oXMLRenovaPoliCol.loadXML(document.getElementById("xmlRenovaPoliCol").innerHTML);
				
				//Estabilizaci�n
				if (fncBuscarEnCombo("estabilizacion",oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text))
					recuperaCombo("estabilizacion",oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text);
				else
					fncComboAgregarItem(document.getElementById("estabilizacion"), 
													oXMLRenovaPoliCol.selectSingleNode("//CLAUADES").text,
													oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text,
												true);
				
				//Def.347 HRF 20110323
				//Plan Pago
				if (fncBuscarEnCombo("planPago",oXMLRenovaPoliCol.selectSingleNode("//PLANPCOD").text,0))
					recuperaCombo("planPago",oXMLRenovaPoliCol.selectSingleNode("//PLANPCOD").text,0)
				else
					fncComboAgregarItem(document.getElementById("planPago"), 
													oXMLRenovaPoliCol.selectSingleNode("//PLANPDES").text,
													oXMLRenovaPoliCol.selectSingleNode("//PLANPCOD").text +"|0",
													true);
			}
		}
		//Verifica si se modific� el valor del campo
		fncCambiarColorCpoRen(document.getElementById("estabilizacion"))
		fncCambiarColorCpoRen(document.getElementById("planPago"))
	}
}

function	fncFechaFin(pEstado)
{
	//GED 16-09-2010 DEFECT
	var fechaHasta = ""
	if(pEstado=="C")
	{
		var vigenciaDesde="vigenciaDesde"
		var vigenciaHasta = "vigenciaHasta"
	}
	if(pEstado=="S")
	{
		var vigenciaDesde="vigenciaDesdeSoli"
		var vigenciaHasta = "vigenciaHastaSoli"
	}
	
	//GED 27-8-2010 Borra fecha hasta si desde vacio
	if(document.getElementById(vigenciaDesde).value== "") {
	document.getElementById("vigenciaHastaSoli").value=fechaHasta
	return
	}
	
		var dia=document.getElementById(vigenciaDesde).value.split("/")[0]
		var mes=document.getElementById(vigenciaDesde).value.split("/")[1]
		var anio=document.getElementById(vigenciaDesde).value.split("/")[2]
		
		
		switch(document.getElementById("periodoCotizar").value)
		{
			case '1': //Mensual 1
				
				fechaHasta=dateAddExtention(dia,mes,anio,"m",1)
			break;
			case '2': //Bimestral 2
				
				fechaHasta=dateAddExtention(dia,mes,anio,"m",2)
			break;
			case '3': //Trimestral 3
				fechaHasta=dateAddExtention(dia,mes,anio,"m",3)
			break;
			case '4': //Semestral 4
				fechaHasta=dateAddExtention(dia,mes,anio,"m",6)
			break;
			case '5': //Anual 5
				fechaHasta=dateAddExtention(dia,mes,anio,"yyyy",1)
			break;
			case '6':
				fechaHasta=dateAddExtention(dia,mes,anio,"yyyy",1)
			break;
			case '7': //Cuatrimestral 7
				fechaHasta=dateAddExtention(dia,mes,anio,"m",4)
			break;
		}
	
	if(pEstado=="C")
	{
		document.getElementById("vigenciaDesdeSoli").value = document.getElementById("vigenciaDesde").value
	}
	if(pEstado=="S")
	{
		document.getElementById("vigenciaDesde").value = document.getElementById("vigenciaDesdeSoli").value
	}
	document.getElementById("vigenciaHasta").value=fechaHasta
	document.getElementById("vigenciaHastaSoli").value=fechaHasta
}

function sincronizaCampos(pCampo1, pCampo2)
{
	
	
	pCampo2.value=pCampo1.value
	
	
}

function fncCargaComboComision()
{
	if (document.getElementById("convenioProductor").value.split("|")[0]!='-2')
	{
		document.getElementById("comisionTotal").length = 0;
		
		var convProductor = document.getElementById("convenioProductor")
		var convOrganizador =  document.getElementById("convenioOrganizador")
		
		if (convProductor.value!=0)
		{
			var comisionProd = convProductor.value.split("|")[3]?convProductor.value.split("|")[3]:0
			var comisionOrg = convOrganizador.value.split("|")[3]?convOrganizador.value.split("|")[3]:0
			//alert("comisionProd: "+ comisionProd + " - comisionOrg: "+ comisionOrg)
			var totalComision= Number(comisionProd) +Number(comisionOrg)
			
			/*for (var x=0; x<= (totalComision+5); x++)
			{
				var nuevoElemento=document.createElement("option")
				nuevoElemento.text= x + ",00 %";
				nuevoElemento.value=x ;
				if (x == totalComision)
					{nuevoElemento.selected=true}
				
				document.getElementById("comisionTotal").add(nuevoElemento);
			}*/
			
			//GED 09-03-2011 - DEFECT 279 - SE HABILITAN COMISIONES CON DECIMALES
			var intComision =parseInt(totalComision,10)
			//Def.369 HRF 20110329 - Se agreg� fncRoundNumber porque en algunos funcionaba mal
			var fracComision = String(parseInt(( fncRoundNumber((totalComision - intComision),2))*100,10)).length>1?String(parseInt(( fncRoundNumber((totalComision - intComision),2))*100,10)): "00"
			
			//GED 11-04-2011 - DEFECT 279 - SE AGREGA LA OPTION 0 CUANDO TIENE DECIMALES
			if (fracComision != "00")
			{
				var nuevoElemento=document.createElement("option")	  	 		
				nuevoElemento.text='0,00 %';				
				nuevoElemento.value='0';				
				document.getElementById("comisionTotal").add(nuevoElemento);
			}
			
			for (var x=0; x<= (intComision+5); x++)
			{
				var nuevoElemento=document.createElement("option")
				nuevoElemento.text= x +","+ fracComision + " %";				
				var comision = Number(x +"."+ fracComision)
				nuevoElemento.value=comision ;
				if (x == intComision)
					{nuevoElemento.selected=true}				
				document.getElementById("comisionTotal").add(nuevoElemento);
			}
			
		}
	}
	// GED 21-02-2010  DEFECT 287		
	else
		{
			
			vCOMPCPORPR1 =Number(document.getElementById("convenioProductor").value.split("|")[3])	
			vCOMPCPOROR = Number(document.getElementById("convenioOrganizador").value.split("|")[3])	
			
			
		var vComisionTotal=vCOMPCPORPR1 + vCOMPCPOROR
	 	//alert("ged: " +oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text +"  ** "+ oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text )
	  
	  	var nuevoElemento=document.createElement("option");
	  	nuevoElemento.text=formatoNro('',vComisionTotal,2,",")+ ' %';
		nuevoElemento.value= vComisionTotal
		nuevoElemento.selected=true;
		document.getElementById("comisionTotal").add(nuevoElemento);
			
			
			
			}
		
	
	//Verifica si se modific� el valor del campo
	fncCambiarColorCpoRen(document.getElementById("comisionTotal"))
}
/******************************************************************************************/
function fncCalculaComisiones()
{
	//------------------------------------------------------------------------------------------//
	//	18/03/2011 SE SEPARAN CONVENIOS/COMISIONES PARA LOS CASOS DE ALTAS Y RENOVACIONES
	//------------------------------------------------------------------------------------------//
	//------------------------------------------------------------------------------------------//
	//	BEGIN	-	CASO ALTA
	//------------------------------------------------------------------------------------------//
	if (document.getElementById("TIPOOPER").value == "A")
	{
		document.getElementById("claseOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[1]
		document.getElementById("codigoOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[0]		
		document.getElementById("nombreOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[2]
		
		document.getElementById("claseProductor").value = document.getElementById("I_AGENTE").value.split("|")[1]
		document.getElementById("codigoProductor").value = document.getElementById("I_AGENTE").value.split("|")[0]		
		document.getElementById("nombreProductor").value = document.getElementById("I_AGENTE").value.split("|")[2]
	
		if (document.getElementById("convenioProductor").value.split("|")[0]!='-2')
		{
	/*		document.getElementById("claseOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[1]
			document.getElementById("codigoOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[0]		
			document.getElementById("nombreOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[2]
			
			document.getElementById("claseProductor").value = document.getElementById("I_AGENTE").value.split("|")[1]
			document.getElementById("codigoProductor").value = document.getElementById("I_AGENTE").value.split("|")[0]		
			document.getElementById("nombreProductor").value = document.getElementById("I_AGENTE").value.split("|")[2]*/
			
			var convProductor = document.getElementById("convenioProductor")
		  	var convOrganizador =  document.getElementById("convenioOrganizador")  	
		  	
		  	var comisionProd = convProductor.value.split("|")[3]?Number(convProductor.value.split("|")[3]):Number(0)
			var comisionOrg = convOrganizador.value.split("|")[3]?Number(convOrganizador.value.split("|")[3]):Number(0)
			
			//alert("antes prod:"+comisionProd+" org:"+comisionOrg)
			var recargoProd = convProductor.value.split("|")[2]?convProductor.value.split("|")[2]:0
			var recargoOrg = convOrganizador.value.split("|")[2]?convOrganizador.value.split("|")[2]:0
			
			var comisionTotal = Number(comisionProd) + Number(comisionOrg)
			var recargoTotal = Number(recargoProd) + Number(recargoOrg)
	
			var comisionCombo = Number(document.getElementById("comisionTotal").value)
			var diferenciaComision = Number(comisionCombo)-Number(comisionTotal)
			
			recargoTotal+=diferenciaComision
			
			comisionProd+= diferenciaComision
			
			if (comisionProd<0)
			{		
				comisionOrg+=  comisionProd
				comisionProd=0	
			}
			
			//alert("desp prod:"+comisionProd+" org:"+comisionOrg)
			document.getElementById("COMISION_PROD").value=comisionProd
			document.getElementById("COMISION_ORG").value=comisionOrg
			document.getElementById("RECARGO").value=recargoTotal
			
			document.getElementById("porcOrganizador").value =comisionOrg
			document.getElementById("porcProductor").value =comisionProd
		}
		else
		{
			//## Mje 2400 ##//	
			oXMLRenovaPoliCol = new ActiveXObject('MSXML2.DOMDocument');        
			oXMLRenovaPoliCol.async = false;       
	  		oXMLRenovaPoliCol.loadXML(document.getElementById("xmlRenovaPoliCol").innerHTML); 
			
			//if (oXMLRenovaPoliCol.xml)
				//alert("xmlRenovaPoliCol="+document.getElementById("xmlRenovaPoliCol").innerHTML)  		
			//else{
			//	if (oXMLCotiRecuperada.xml)
					//alert("oXMLCotiRecuperada="+document.getElementById("xmlCotiRecuperada").innerHTML)	
	  		//}
			document.getElementById("COMISION_PROD").value=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text)
			document.getElementById("COMISION_ORG").value=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text)
			document.getElementById("RECARGO").value=Number(oXMLRenovaPoliCol.selectSingleNode("//RECARPOR").text)
			
			document.getElementById("porcOrganizador").value=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text)
			document.getElementById("porcProductor").value=Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text)
		}	
	}
	//------------------------------------------------------------------------------------------//
	//	END	-	CASO ALTA
	//------------------------------------------------------------------------------------------//
	
	//------------------------------------------------------------------------------------------//
	//	BEGIN	-	CASO RENOVACION
	//------------------------------------------------------------------------------------------//
	if (document.getElementById("TIPOOPER").value == "R")
	{
		document.getElementById("claseOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[1]
		document.getElementById("codigoOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[0]		
		document.getElementById("nombreOrganizador").value = document.getElementById("I_ORGANIZADOR").value.split("|")[2]
		
		document.getElementById("claseProductor").value = document.getElementById("I_AGENTE").value.split("|")[1]
		document.getElementById("codigoProductor").value = document.getElementById("I_AGENTE").value.split("|")[0]		
		document.getElementById("nombreProductor").value = document.getElementById("I_AGENTE").value.split("|")[2]
	
		var convProductor = document.getElementById("convenioProductor")
		 var convOrganizador =  document.getElementById("convenioOrganizador")
		
		if (document.getElementById("convenioProductor").value.split("|")[0]!='-2')
		{
			
		  
		  var comisionProd = convProductor.value.split("|")[3]?Number(convProductor.value.split("|")[3]):Number(0)
			var comisionOrg = convOrganizador.value.split("|")[3]?Number(convOrganizador.value.split("|")[3]):Number(0)
			
			//alert("antes prod:"+comisionProd+" org:"+comisionOrg)
			var recargoProd = convProductor.value.split("|")[2]?convProductor.value.split("|")[2]:0
			var recargoOrg = convOrganizador.value.split("|")[2]?convOrganizador.value.split("|")[2]:0
			
			var comisionTotal = Number(comisionProd) + Number(comisionOrg)
			var recargoTotal = Number(recargoProd) + Number(recargoOrg)
			
			var comisionCombo = Number(document.getElementById("comisionTotal").value)
			var diferenciaComision = Number(comisionCombo)-Number(comisionTotal)
			
			recargoTotal+=diferenciaComision
			recargoTotal = fncRoundNumber(recargoTotal,2)
			
			comisionProd+= diferenciaComision
			comisionProd = fncRoundNumber(comisionProd,2)
			
			if (comisionProd<0)
			{		
				comisionOrg+= comisionProd
				comisionOrg = fncRoundNumber(comisionOrg,2)
				comisionProd=0	
			}
			
			// GED 21-02-2010  DEFECT 287
		}else
			{
				
			
				var comisionProd = convProductor.value.split("|")[3]?Number(convProductor.value.split("|")[3]):Number(0)
				var comisionOrg = convOrganizador.value.split("|")[3]?Number(convOrganizador.value.split("|")[3]):Number(0)
				var recargoTotal = convProductor.value.split("|")[2]?Number(convProductor.value.split("|")[2]):Number(0)
				
				
			}
			//alert("desp prod:"+comisionProd+" org:"+comisionOrg)
			document.getElementById("COMISION_PROD").value=comisionProd
			document.getElementById("COMISION_ORG").value=comisionOrg
			document.getElementById("RECARGO").value=recargoTotal
			
			document.getElementById("porcOrganizador").value =comisionOrg
			document.getElementById("porcProductor").value =comisionProd
	}
	//------------------------------------------------------------------------------------------//
	//	END	-	CASO RENOVACION
	//------------------------------------------------------------------------------------------//
}
/*******************************************************************/
function fncCuitSeleccionado(pthis)
{
	var objSelIB  = document.getElementById("IngresosBrutos");
 	var objTipoPersona = document.getElementById("tipoPersona");
 	
 	if (pthis.value != 3)
  	{
  	
  	// !Consumidor Final
		objSelIB.disabled=false
		objSelIB.style.display="inline"
		document.getElementById("spanIngresosBrutos").style.display="inline"
		
		//No hacer para Renovaci�n
		if (document.getElementById("TIPOOPER").value != "R")
		{
  		objTipoPersona.selectedIndex=0
  		objTipoPersona.disabled=false
		}
		objTipoPersona.style.display="inline"
		document.getElementById("spanTipoPersona").style.display="inline"
	} 
	else 
	{
		//Consumidor Final
		objSelIB.selectedIndex = 0;
		objSelIB.disabled=true
		objSelIB.style.display="none"
		document.getElementById("spanIngresosBrutos").style.display="none"

		//No hacer para Renovaci�n
		if (document.getElementById("TIPOOPER").value != "R")
		{
			objTipoPersona.disabled=true
			objTipoPersona.value="00"
		}
		objTipoPersona.style.display="none"
		document.getElementById("spanTipoPersona").style.display="none"
	}
}
//********************************************************************************************
function fncValCliTipoPersona()
{
	//Si es Persona Jur�dica y selecciona != CUIT
	if (document.getElementById('tipoPersona').value == "15" &&   document.getElementById('tipoDocCliente').value != "4" )
	{
		return 'Para Personas Jur�dicas debe seleccionar C.U.I.T.';
	} else {
		//Si es Persona Fisica y selecciona = CUIT
		if (document.getElementById('tipoPersona').value == "00" && 
		  	(document.getElementById('tipoDocCliente').value == "4" || document.getElementById('tipoDocCliente').value == "5"))
		{
		    return 'Para Personas F�sicas no debe seleccionar C.U.I.T./C.U.I.L.';
		} else {
			return true;
		}
	}
}
/**************************************************************************/
function fncCotizar2480(pNroItem, pXmlImpresoRen)
{

	var strXML;
	var strHTML;
	var strData;
	var vCoberturas;
	var vCantCoberturas;
	var vRequest;
	
	//GED 09-05-2011 PRIMA MINIMA
				document.getElementById('SWPRIMIN').value = "N"
	//Se chequea campos
	var vNROCOTI = fncGetNroCot();	
	var tipoPersona= document.getElementById("tipoPersona").value=="-1"?"0":document.getElementById("tipoPersona").value
	var mvarIIBB= document.getElementById("IngresosBrutos").value=="-1"?"0":document.getElementById("IngresosBrutos").value
	if (mvarIIBB=="")
		mvarIIBB="0"
		
	var objXMLItems = new ActiveXObject("Msxml2.DOMDocument");
	
	if(pXmlImpresoRen != undefined)
	{
	 	if(pXmlImpresoRen == "R")
	 		objXMLItems.loadXML(document.getElementById("varXmlItemsImpresoRev").value);
			
	}
	else
		objXMLItems.loadXML(document.getElementById("varXmlItems").value);
	
	
	
	
	
	var objXMLCobertura = objXMLItems.getElementsByTagName('COBERTURA');
	
	var objXMLItem = objXMLItems.selectNodes("//ITEM")
	var cantItems=objXMLItem.length
	//alert(cantItems)
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "xmlDatosItem: <textarea>"+objXMLItem.xml+"</textarea>")
	
	var POLIZANN = existeItemXMLRen(pNroItem)?document.getElementById("POLIZANNANT").value:0
	var POLIZSEC = existeItemXMLRen(pNroItem)?document.getElementById("POLIZSECANT").value:0
	
	var CANTALUM = objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CANTALUM").text!=""?objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CANTALUM").text:0
	
	var cpostal=objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/PROVICOD").text!="1"?objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CPOSDOM").text:"1005"
	
	var xmlDatosCotizacion="<Request>"
		xmlDatosCotizacion+="<DEFINICION>2480_CotizacionUnificadoRenovacion.xml</DEFINICION>"
		
		xmlDatosCotizacion+="<NROCOTI>"+vNROCOTI+"</NROCOTI>"		//Ver el nro de Coti!!!lo va a sacar tincho
		xmlDatosCotizacion+="<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"
		xmlDatosCotizacion+="<POLIZANN>"+POLIZANN+"</POLIZANN>"
		xmlDatosCotizacion+="<POLIZSEC>"+POLIZSEC+"</POLIZSEC>"		
		xmlDatosCotizacion+="<CERTIPOL>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CERTIPOLANT").text+"</CERTIPOL>"
		xmlDatosCotizacion+="<CERTIANN>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CERTIANNANT").text+"</CERTIANN>"
		xmlDatosCotizacion+="<CERTISEC>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CERTISECANT").text+"</CERTISEC>"		
		xmlDatosCotizacion+="<FECSOLIC>0</FECSOLIC>"
		var vFECINIVG = document.getElementById("vigenciaDesde").value.split("/")[2]+document.getElementById("vigenciaDesde").value.split("/")[1]+document.getElementById("vigenciaDesde").value.split("/")[0];
		xmlDatosCotizacion+="<FECINIVG>"+ vFECINIVG +"</FECINIVG>"
		var vFECFINVG = document.getElementById("vigenciaHasta").value.split("/")[2]+document.getElementById("vigenciaHasta").value.split("/")[1]+document.getElementById("vigenciaHasta").value.split("/")[0];
		xmlDatosCotizacion+="<FECFINVG>"+ vFECFINVG +"</FECFINVG>"
		xmlDatosCotizacion+="<AGENTCLAPR1>"+document.getElementById("I_AGENTE").value.split("|")[1]+"</AGENTCLAPR1>"
		xmlDatosCotizacion+="<AGENTCODPR1>"+document.getElementById("I_AGENTE").value.split("|")[0]+"</AGENTCODPR1>"
		xmlDatosCotizacion+="<AGENTCLAPR2></AGENTCLAPR2>"
		xmlDatosCotizacion+="<AGENTCODPR2>0</AGENTCODPR2>"
		xmlDatosCotizacion+="<AGENTCLAOR>"+document.getElementById("I_ORGANIZADOR").value.split("|")[1]+"</AGENTCLAOR>"
		xmlDatosCotizacion+="<AGENTCODOR>"+document.getElementById("I_ORGANIZADOR").value.split("|")[0]+"</AGENTCODOR>"
		xmlDatosCotizacion+="<COMISIONPR1>"+document.getElementById("COMISION_PROD").value+"</COMISIONPR1>"
		xmlDatosCotizacion+="<COMISIONPR2>0</COMISIONPR2>"
		xmlDatosCotizacion+="<COMISIONOR>"+document.getElementById("COMISION_ORG").value+"</COMISIONOR>"		
		xmlDatosCotizacion+="<PLANPCOD>"+document.getElementById("planPago").value.split("|")[0]+"</PLANPCOD>"
		xmlDatosCotizacion+="<COBROCOD>"+document.getElementById("formaPago").value.split("|")[0]+"</COBROCOD>"
		xmlDatosCotizacion+="<COBROTIP>"+document.getElementById("formaPago").value.split("|")[1]+"</COBROTIP>"
		xmlDatosCotizacion+="<PROFECOD>"+document.getElementById("cboActividadComercio").value.split("|")[0]+"</PROFECOD>"
		xmlDatosCotizacion+="<OPERATIP>AP</OPERATIP>"	//AP es Alta de Poliza
		xmlDatosCotizacion+="<CAUSAEND>0</CAUSAEND>"	//en cero	
		xmlDatosCotizacion+="<COBROFOR>"+document.getElementById("periodoCotizar").value+"</COBROFOR>"
		xmlDatosCotizacion+="<CLIENTIP>"+ tipoPersona +"</CLIENTIP>"
		xmlDatosCotizacion+="<CLIENSEX></CLIENSEX>"	//en blanco no tengo el sexo
		xmlDatosCotizacion+="<FECNACIM>0</FECNACIM>"	//cero no tengo la fech nac
		xmlDatosCotizacion+="<CLIENEST></CLIENEST>"		//en blanco no tengo est civil
		xmlDatosCotizacion+="<NACIONAL>00</NACIONAL>"	//Nacionalidad Argentina
		xmlDatosCotizacion+="<CLIENIVA>"+document.getElementById("IVA").value+"</CLIENIVA>"
		xmlDatosCotizacion+="<CLIEIBTP>"+mvarIIBB+"</CLIEIBTP>"
		xmlDatosCotizacion+="<CLIENSECPC>0</CLIENSECPC>"
		xmlDatosCotizacion+="<CLIENTIPPC></CLIENTIPPC>"
		xmlDatosCotizacion+="<DOMICSECPC></DOMICSECPC>"
		xmlDatosCotizacion+="<CLIENIVAPC></CLIENIVAPC>"
		xmlDatosCotizacion+="<CLIEIBTPPC></CLIEIBTPPC>"	
		xmlDatosCotizacion+="<CLAUAJUS>"+document.getElementById("estabilizacion").value+"</CLAUAJUS>"
		
		xmlDatosCotizacion+="<ITIPALAR>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/ALARMA").text+"</ITIPALAR>"		
		xmlDatosCotizacion+="<ITIPGUAR>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/GUARDIA").text+"</ITIPGUAR>"
		//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
		xmlDatosCotizacion+="<IDISYUNT>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/IDISYUNT").text+"</IDISYUNT>"
		
		xmlDatosCotizacion+="<CANTCOMER>"+cantItems+"</CANTCOMER>"
		xmlDatosCotizacion+="<CANTALUM>"+CANTALUM+"</CANTALUM>"		
		//Coberturas
		xmlDatosCotizacion+="<COBER>"
		
		for (i=0;i<objXMLCobertura.length;i++)
 		{
 			if (objXMLCobertura[i].selectSingleNode("IDITEM").text==pNroItem)
 			{
 				var COBERCOD = objXMLCobertura[i].selectSingleNode("COBERCOD").text 
 				var SUMAASEG = objXMLCobertura[i].selectSingleNode("SUMAASEG").text
	 			xmlDatosCotizacion+="<COBERTURAS>"
				xmlDatosCotizacion+="<COBERCOD>"+ COBERCOD +"</COBERCOD>";
				xmlDatosCotizacion+="<SUMAASEG>"+ SUMAASEG +"</SUMAASEG>";
				xmlDatosCotizacion+="<NUMERMOD>"+calculaModulo(COBERCOD, SUMAASEG )	+"</NUMERMOD>";
				
				if(existeItemXMLRen(pNroItem)){					
					var SWCOBERNW =  existeCoberturaXMLRen(COBERCOD,objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/CERTISECANT").text)?"N":"S"
					
				}
				else
					var SWCOBERNW = "S"
					
				xmlDatosCotizacion+="<SWCOBERNW>"+ SWCOBERNW +"</SWCOBERNW>";
				
				xmlDatosCotizacion+="</COBERTURAS>"
			}
		}				
		xmlDatosCotizacion+="</COBER>"

		//DOMI-RIESGO
		xmlDatosCotizacion+="<PARTICOD>0</PARTICOD>"	//en cero
		xmlDatosCotizacion+="<LOCALCOD>0</LOCALCOD>"	//en cero
		xmlDatosCotizacion+="<DOMICCPO>X"+cpostal+"</DOMICCPO>"	//Ej:'X1005   ' el FORMATO del CP es el nuevo osea 8 pos X0000XXX
		xmlDatosCotizacion+="<PROVICOD>"+objXMLItems.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/PROVICOD").text+"</PROVICOD>"
		xmlDatosCotizacion+="<PAISSCOD>00</PAISSCOD>"	//fijo Argentina, porque no tengo el pais
		
		xmlDatosCotizacion+="</Request>"
		
		llamadaMensajeMQ(xmlDatosCotizacion,"lbaw_GetConsultaMQ")
		var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
		oXMLValores.async = false; 
		
		if(oXMLValores.loadXML(llamadaMensajeMQ(xmlDatosCotizacion,"lbaw_GetConsultaMQ")))
		{
			//window.document.body.insertAdjacentHTML("beforeEnd", "xmlDatosCotizacion: <textarea style='position:absolute; z-index:100'>"+oXMLValores.xml+"</textarea>")	
			if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text=="true")
			{
				if(oXMLValores.selectSingleNode("//Response/CAMPOS/CANTRESU").text != 0)
				{	
					actualizarXMLItems(oXMLValores.xml,pNroItem, pXmlImpresoRen )
					document.getElementById("fechaUltimaCoti").value = document.getElementById("fechaHoy").value;
					return true
				}
				else
				{
				//window.document.body.insertAdjacentHTML("beforeEnd", "xmlDatosCotizacion2200: <textarea>"+xmlDatosCotizacion+"</textarea>")		
				//alert("//errorInesperado(Error al cotizar)")
				return false
				}	
				
			}
			else
				alert_customizado(oXMLValores.selectSingleNode("//Response/Estado/@mensaje").text)
		}	
		else{
			//alert("No ingres�")
				return false		
		}				

	
}

/********************************************************************************************/

function fncComposicionPrecio()
{
		var ancho=document.body.clientWidth
		var body = document.body, 	html = document.documentElement; 		
		var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
		
		document.getElementById('popUp_opacidad').style.width  = ancho
		document.getElementById('popUp_opacidad').style.height  = alto
		document.getElementById('popUp_compPrecio').style.left  = 200
		document.getElementById('popUp_compPrecio').style.top  =document.body.scrollTop+100
		document.getElementById('popUp_opacidad').style.display = 'inline';
		//opacity('popUp_opacidad', 0, 10, 300);
		document.getElementById('popUp_compPrecio').style.display = 'inline';
}
/**************************************/
function fncCotizar(pXmlImpresoRev)
{
	
	
	    //GED 09-05-2011 MEJORA 373
	  if(pXmlImpresoRev == undefined)
		{
	 
			var condTecnicasFM = fncCondicionesTecnicasFM()
		}	
			
	var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	objXMLItem.async = false; 
	
	//GED 01-07-2010 TICKET 632535
	if(pXmlImpresoRev != undefined)
	{
	 	if(pXmlImpresoRev == "R")
	 		objXMLItem.loadXML(document.getElementById("varXmlItemsImpresoRev").value);
			
	}
	else
		objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	
	
	
	var oXMLRegItems = objXMLItem.selectNodes("//ITEM");
	var CantItem = oXMLRegItems.length;
	var vNROCOTI = fncGetNroCot();
	var pX=400
	var pY= 200
	
	/* Adriana 19-4-2011 Ticket 599651 Prima Minima .Se agregan campos en la llamada al msg 2340*/
	var mvarTipooper = document.getElementById("TIPOOPER").value ;
	if (mvarTipooper =="A")
	        mvarTipooper="AP"
        else	mvarTipooper="RP";
        /* Adriana 19-4-2011 Ticket 599651 Prima Minima .Se agregan campos en la llamada al msg 2340*/        
	var mvarIVA =  document.getElementById("IVA").value ==-1?0 :document.getElementById("IVA").value
	var mvarIIBB= document.getElementById("IngresosBrutos").value ==-1?0 :document.getElementById("IngresosBrutos").value
    
    //MAG INCIDENTE  RECARGO FINANCIERO
    var mvarRecFinanciero
    if (document.getElementById("ESRECFINAN").value == "S")
        mvarRecFinanciero = document.getElementById("planPago").value.split("|")[1];
    else
        mvarRecFinanciero = 0;
        
    
	
	var mvarRequest='<Request>'+
			'<DEFINICION>2340_CalculoPremio.xml</DEFINICION>'+
			'<USUARCOD>'+ document.getElementById("LOGON_USER").value +'</USUARCOD>'+
			'<NROCOTI>'+vNROCOTI+'</NROCOTI>'+
			'<RAMOPCOD>'+ document.getElementById("RAMOPCOD").value +'</RAMOPCOD>'+
			'<CLIENTIP>'+ document.getElementById("tipoPersona").value +'</CLIENTIP>'+
			'<CLIENIVA>'+ mvarIVA +'</CLIENIVA>'+
			'<CLIEIBTP>'+ mvarIIBB +'</CLIEIBTP>'+
			'<COBROCOD>'+ document.getElementById("formaPago").value.split("|")[0] +'</COBROCOD>'+
			'<FECINIVG>'+ document.getElementById("vigenciaDesde").value.split("/")[2]+document.getElementById("vigenciaDesde").value.split("/")[1]+document.getElementById("vigenciaDesde").value.split("/")[0] +'</FECINIVG>'+
			'<FECFINVG>'+ document.getElementById("vigenciaHasta").value.split("/")[2]+document.getElementById("vigenciaHasta").value.split("/")[1]+document.getElementById("vigenciaHasta").value.split("/")[0] +'</FECFINVG>'+
			'<TIPOOPER>'+ mvarTipooper +'</TIPOOPER>'
			/* Adriana 19-4-2011 fin */
			var RECADMIN= Number(document.getElementById("RECARGO").value)
			if(  RECADMIN<0 )
				{
			        mvarRequest+=		'<SGNRECADM>-</SGNRECADM>'
					RECADMIN= RECADMIN * (-1)
				}					
		    mvarRequest+=	'<RECADMIN>'+ RECADMIN +'</RECADMIN>'+


			'<RECFINAN>' + mvarRecFinanciero + '</RECFINAN>' +
			'<CANTITEMS>'+ CantItem +'</CANTITEMS>'
			//LR 05/01/2011 Enviar los recargos Administrativo y Financiero(RECADMIN/RECFINAN)
		
			
			for (var i=0;i<CantItem;i++)
			{
			
			var provincia=oXMLRegItems[i].selectSingleNode("PROVICOD").text
			//****SUMA PRIMAS DE TODAS LAS COBERTURAS DEL ITEM	
			var objXMLCobertura =  objXMLItem.selectNodes("//COBERTURA[IDITEM='"+eval(i+1)+"']");
			
			//alert(objXMLNode.xml)
			//alert(objXMLCobertura.length)
			var mvarSumatoriaPrima = new Number()
			for (var x=0;x<objXMLCobertura.length;x++)
			{
				
				mvarSumatoriaPrima +=	Number(objXMLCobertura[x].selectSingleNode("PRIMA").text)
				
						
			}
			//*************************	
			//Valor fijo de prima
				mvarRequest+='<ITEMS>'+
					'<PRIMAITEM>'+fncRoundNumber(mvarSumatoriaPrima ,2)+'</PRIMAITEM>'+
					'<PROVICOD>'+ provincia +'</PROVICOD>'+
				'</ITEMS>'
			}
			mvarRequest+='</Request>'
			
	        //Adriana 5-5-2011 Mejoras Prorrateo de Prima m�nima
	        document.getElementById("xmlRequest2340").innerHTML = mvarRequest;
			
			
		document.getElementById("resulCotiTotales").style.display='none';
		document.getElementById("resultadoCotizacion").innerHTML=""
		document.getElementById("spanEsperando").innerHTML="<b><br/>Su cotizaci�n se est� procesando, por favor aguarde un instante.</b>"	
		llamadaMensajeMQAsync(mvarRequest,"lbaw_GetConsultaMQ","resultadoCotizacion")
	
	
	
}

function resultadoCotizacion(pXML)
{ 

	//Adriana 19-4-11 ticket 599651
	eliminarXMLFueraNorma("divCotiPRIMA","")
	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;
	var pXMLVal = new ActiveXObject('MSXML2.DOMDocument');    
	pXMLVal.async = false;
	
	//Adriana 5-5-2011 Mejoras Prorrateo de Prima m�nima
	
	if(pXML)
	  {
	   pXMLVal.loadXML(pXML);
	   if (pXMLVal.selectSingleNode("//Response/CAMPOS/SWPRIMIN").text == "S")
	      {
	      fncProrrateoPrimaMinima(pXML);
            }
       if (document.getElementById('SWPRIMIN').value == "N")
					document.getElementById('SWPRIMIN').value = pXMLVal.selectSingleNode("//Response/CAMPOS/SWPRIMIN").text;
		
       
        }
	
	
	
	
	
	
	if(oXMLValores.loadXML(document.getElementById("varXmlItems").value))
	{		
			
		//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/resultadoCotizacion.xsl"))
		if(oXSLValores.loadXML(document.getElementById("resultadoCotizacionXSL").xml))
		{	
			var xslParam="<parametros>"+
			"<parametro>"+
				"<nombre>moneda</nombre><valor>"+datosCotizacion.getMonedaProducto()+"</valor>"+
			"</parametro>"+
			"<parametro>"+
						"<nombre>SWPRIMIN</nombre>"+
						"<valor>"+ document.getElementById("SWPRIMIN").value+"</valor>"+
					"</parametro>"+					
			"</parametros>"			
			var varResultTabla = parseaXSLParam(oXMLValores.xml,oXSLValores,xslParam)
			document.getElementById("circuitoRevision").style.display='inline';
			document.getElementById("resultadoCotizacion").innerHTML = varResultTabla;	
			if(pXML)	
			{	
			//	if( pXMLVal.loadXML(pXML) )
				//
					document.getElementById("resulCotiTotales").style.display='';
					//adriana 19-4-2011 Ticket 599651 se muestra si se modific� la prima por prima minima 
					
					//ADRIANA 19-4-2011 ticket 599651 se agrega el campo SWPRIMIN
			
					
				if(document.getElementById('SWPRIMIN').value == "S")
					{
						document.getElementById("divCotiPRIMA").innerHTML = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",")+'<sup>(1) </sup>';
				        	document.getElementById("primaMinima").innerHTML = '<sup>(1) </sup>Se ha incrementado la prima para alcanzar la Prima M�nima establecida para este producto';
		       				insertarFueraNorma("", "", "divCotiPRIMA","Se ha incrementado la prima para alcanzar la Prima M�nima establecida para este producto",3,"N","divCotiPRIMA")
						fncMuestraFueraNorma()
					 }
				        else
					{
						document.getElementById("divCotiPRIMA").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",");
						document.getElementById("primaMinima").innerHTML = '';
					}				
					
					
					
					//document.getElementById("divCotiPRIMA").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",");
					document.getElementById("divCotiPRECIO").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRECIOTOT").text,2,",");
				}
		}
				
	}
	if (document.getElementById("SWSINIES").value == "S" && document.getElementById("TIPOOPER").value == "R" && document.getElementById("xmlFueraNorma").value != "<ERRORES></ERRORES>"){
		if (oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='0']/ERROR").length==1 && oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='2']/ERROR").length==0 && oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='3']/ERROR").length==0){
			flagPoseeSiniestralidad = false
			document.getElementById("xmlFueraNorma").value="<ERRORES></ERRORES>"
			//oXMLFueraNorma.loadXML("<ERRORES></ERRORES>")
			document.getElementById('divTablaFueraNorma').innerHTML=""
			
			
		}
		else{	
			if (!flagPoseeSiniestralidad){
				if(oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='0']/ERROR").length==1){					
					if(oXMLFueraNorma.selectSingleNode("ERRORES/SOLAPA[@nro='0']/ERROR/CAMPO").text!="SW"){
						flagPoseeSiniestralidad = true	
						insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
					}	
				}
				else if (oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='3']/ERROR").length==1)
				{
					flagPoseeSiniestralidad = true
					insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
				}		
			}							
			fncMuestraFueraNorma()	
		}
	}else{	
		fncMuestraFueraNorma() }
	
	var pX=400
	var pY= 200
	if(pXML)
	if(pXMLVal.selectSingleNode("//Response/Estado/@resultado").text)
	{
			var tabla="<table width='100%' bgcolor='#FFFFFF' cellspacing='0' cellpadding='0' style='border: solid #D0D0D0 1px'>"+
			"<tr ><td width='410px' ><div class='areaDatTit'><div class='areaDatTitTex' style='font-size:10px;'>Composici�n del precio</div>"+
			"</div></td></tr>"

			tabla+="<tr><td><div style='width:"+pX+"px; height:"+pY+"px; overflow:auto'><table cellspacing='0' cellpadding='0'>"
				//21/10/2010 LR Defect 21 Estetica, pto 40
				tabla+="<tr id='fila_1'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Prima Comisionable</td>"+
			    	"<td width='130px' id='tdPrimComi' style='text-align:right'>"+	formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",") +"</td>"+	
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				//MAG INCIDENTE  RECARGO FINANCIERO
				var vPorcGastComerc
				if (document.getElementById("ESRECFINAN").value == "S")
				    vPorcGastComerc = Number(document.getElementById("RECARGO").value) + Number(document.getElementById("planPago").value.split("|")[1]); //RECADMIN + RECFINAN
				else
				    vPorcGastComerc = Number(document.getElementById("RECARGO").value); //RECADMIN + RECFINAN =0
			  
			    //var vPorcGastComerc = Number(document.getElementById("RECARGO").value) + Number(document.getElementById("planPago").value.split("|")[1]); //RECADMIN + RECFINAN
				//GED 09-08/2010 Cuando gastos negativo se pone en cero
				if (vPorcGastComerc <0 ) 
				{
					vPorcGastComerc=0
					var recargo = 0
				}
			else
				var recargo= formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/RECARGOS").text,2,",") 
				tabla+="<tr id='fila_2'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Gastos Comercializaci�n</td>"+
			    	"<td width='130px' id='tdGastCome' style='text-align:right'>"+ recargo +"</td>"+
			    	"<td width='70px' id='tdGastComePorc' style='text-align:center'>("+ formatoNro("",vPorcGastComerc,2,",") +"%)</td>"+
			    	"</tr>"
				tabla+="<tr id='fila_3'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Gastos Administrativos</td>"+
			    	"<td width='130px' id='tdGastAdm' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/DEREMI").text,2,",") +"</td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				tabla+="<tr id='fila_4'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Sellados</td>"+
			    	"<td width='130px' id='tdSellados' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/SELLADOS").text,2,",") +
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				tabla+="<tr id='fila_5'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Impuestos</td>"+
			    	"<td width='130px' id='tdImpuestos' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/OTROSIMP").text,2,",") +"</td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
			  var vIVAIMPOR = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOR").text);
			  var vIVAIMPOA = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOA").text);
			  var vIVARETEN = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IVARETEN").text);
			  var vSumaIVA = vIVAIMPOR + vIVAIMPOA + vIVARETEN;
				tabla+="<tr id='fila_6'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"I.V.A.</td>"+
			    	"<td width='130px' id='tdIVA' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vSumaIVA,2,",") +"</td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				tabla+="<tr id='fila_7'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Ingresos Brutos</td>"+
			    	"<td width='130px' id='tdIIBB' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/INGBRUTO").text,2,",") +"</td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				tabla+="<tr id='fila_8'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"<strong>Precio por Per�odo</strong></td>"+
			    	"<td width='130px' id='tdPrecPeri' style='text-align:right'><strong>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRECIOTOT").text,2,",") +"<strong></td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
			    	var vPRIMAIMP = pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text;
			    	var vPorcProductor = Number(document.getElementById("porcProductor").value);
			    	var vCOMIPROD = (vPRIMAIMP * vPorcProductor)/100;
			    	var vPorcOrganizador = Number(document.getElementById("porcOrganizador").value);
			    	var vCOMIORG = (vPRIMAIMP * vPorcOrganizador)/100;
				tabla+="<tr id='fila_8'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Comisi�n Productor</td>"+
			    	"<td width='130px' id='tdComiProd' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vCOMIPROD,2,",") +"</td>"+
			    	"<td width='70px' id='tdComiProdPorc' style='text-align:center'>("+ formatoNro("",vPorcProductor,2,",") +"%)</td>"+
			    	"</tr>"
			    	var vComiTotal = formatoNro(datosCotizacion.getMonedaProducto(),(vCOMIPROD + vCOMIORG),2,",");
			    	var vComiTotalPorc = formatoNro("",(vPorcProductor + vPorcOrganizador),2,",");
			    	var vMostrarComiTotal = vComiTotal +' ('+ vComiTotalPorc +'%)';
				tabla+="<tr id='fila_8'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='210px' style='text-align:left'>"+
			    	"Comisi�n Organizador</td>"+
			    	"<td width='130px' id='tdComiOrg' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vCOMIORG,2,",") +"</td>"+
			    	"<td width='70px' id='tdComiOrgPorc' style='text-align:center'>("+ formatoNro("",vPorcOrganizador,2,",") +"%)</td>"+
			    	"</tr>"
			tabla+="</table></div></td></tr></table><p></p>"
			
			//Guardar en el los hidden de la pag main los valores calculados para poder recuperarlos
			document.getElementById('PRIMAIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text;
			document.getElementById('DEREMIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/DEREMI").text;
			document.getElementById('RECAIMPO').value = pXMLVal.selectSingleNode("//Response/CAMPOS/RECARGOS").text;
			document.getElementById('RECFIMPO').value = 0	//Se va a obtener del combo de plan de pago q va a arreglar fito
			document.getElementById('TASUIMPO').value = 0   // pXMLVal.selectSingleNode("//Response/CAMPOS/TASUIMPO").text;
			document.getElementById('IVAIMPOR').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOR").text;
			document.getElementById('IVAIBASE').value = 0	//Va cero porq no las vamos a tener
			document.getElementById('IVAIMPOA').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOA").text;
			document.getElementById('IVAABASE').value = 0	//Va cero porq no las vamos a tener
			document.getElementById('IVARETEN').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IVARETEN").text;
			document.getElementById('IVARBASE').value = 0	//Va cero porq no las vamos a tener
			document.getElementById('IMPUEIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/OTROSIMP").text;
			document.getElementById('SELLAIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/SELLADOS").text;
			document.getElementById('INGBRIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/INGBRUTO").text;
			document.getElementById('RECTOIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/PRECIOTOT").text;
			document.getElementById('COMISIMP').value = vCOMIPROD + vCOMIORG;
			
			
		
			//alert(vMostrarComiTotal)
			document.getElementById('hComiTotal').value = vMostrarComiTotal;
	if(!muestraCuadroPrecio()) document.getElementById('resulCotiTotales').style.display="none"	
		else
	 document.getElementById('resulCotiTotales').style.display="inline"	
		
	
	document.getElementById('conten_popUp_compPrecio').innerHTML = tabla;
	
	
}
	
	ocultarPopUp2('popUp_compPrecio');
				
}
	
function resultadoCotizacionSQL(pXML)
	
{
	
	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;  		
	var pXMLVal = new ActiveXObject('MSXML2.DOMDocument');    
	pXMLVal.async = false;
	
	//GED 09-05-2011 MEJORA 373
	//var condTecnicasFM = fncCondicionesTecnicasFM()
	
	
	
	if(oXMLValores.loadXML(document.getElementById("varXmlItems").value))	
	{//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+ oXMLValores.xml+"</textarea>")	
		//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/resultadoCotizacion.xsl"))
		if(oXSLValores.loadXML(document.getElementById("resultadoCotizacionXSL").xml))
		{	
			//TODO: parsear con param para que le pase el simbolo de la moneda
			var xslParam="<parametros>"+
			"<parametro>"+
				"<nombre>moneda</nombre><valor>"+datosCotizacion.getMonedaProducto()+"</valor>"+
			"</parametro>"+
			"<parametro>"+
				"<nombre>SWPRIMIN</nombre>"+
				"<valor>"+ document.getElementById("SWPRIMIN").value+"</valor>"+
			"</parametro>"+			
			"</parametros>"			
			//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+oXMLValores.xml+"</textarea>")			
			var varResultTabla = parseaXSLParam(oXMLValores.xml,oXSLValores,xslParam)	
			
			// GED 03-08-2010: BUG_21 Punto 4	
			document.getElementById("circuitoRevision").style.display='inline';
			//	var varResultTabla =  oXMLValores.transformNode(oXSLValores);				
			document.getElementById("resultadoCotizacion").innerHTML = varResultTabla;
			
			
				
			if(pXML)		
				if( pXMLVal.loadXML(pXML) )
				{
					//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+pXMLVal.xml+"</textarea>")			
					if(document.getElementById('SWPRIMIN').value == "S")
					{
						document.getElementById("divCotiPRIMA").innerHTML = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",")+'<sup>(1) </sup>';
				        	document.getElementById("primaMinima").innerHTML = '<sup>(1) </sup>Se ha incrementado la prima para alcanzar la Prima M�nima establecida para este producto';
		       				insertarFueraNorma("", "", "divCotiPRIMA","Se ha incrementado la prima para alcanzar la Prima M�nima establecida para este producto",3,"N","divCotiPRIMA")
						fncMuestraFueraNorma()
					 }
				        else
					{
						document.getElementById("divCotiPRIMA").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",");
						document.getElementById("primaMinima").innerHTML = '';
					}				
					
					
					document.getElementById("resulCotiTotales").style.display='';
					//document.getElementById("divCotiPRIMA").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",");
					document.getElementById("divCotiPRECIO").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/RECTOIMP").text,2,",");
				}
			}
			else
			{
					errorInesperado("Datos Incorrectos")
					alert_customizado(" Error inesperado")
			}
	}
	else
	 {
	 	errorInesperado("Datos Incorrectos")
	 	alert("mas errores xml")
	 }
	 
	 var pX=400
	var pY= 200
	if(pXML)
	if(pXMLVal.selectSingleNode("//Response/Estado/@resultado").text) {
	
		//MAG INCIDENTE  RECARGO FINANCIERO
	    var vPorcGastComerc
	    if (document.getElementById("ESRECFINAN").value == "S")

	        vPorcGastComerc = Number(document.getElementById("RECARGO").value) + Number(document.getElementById("planPago").value.split("|")[1]); //RECADMIN + RECFINAN
	    else
	        vPorcGastComerc = Number(document.getElementById("RECARGO").value); //RECADMIN + RECFINAN =0
		 //GED 09-08-2010 Se suma el recargo financiero ??pregunta
		 //var vPorcGastComerc = Number(document.getElementById("RECARGO").value)  + Number(document.getElementById("planPago").value.split("|")[1]) ; //RECADMIN + RECFINAN
		
		 var RECAIMPO = pXMLVal.selectSingleNode("//Response/CAMPOS/RECAIMPO").text
		 var RECFIMPO =  pXMLVal.selectSingleNode("//Response/CAMPOS/RECFIMPO")? pXMLVal.selectSingleNode("//Response/CAMPOS/RECFIMPO").text:0
			
		//GED 09-08-2010 Cuando gastos negativo se pone en cero		
		if (vPorcGastComerc <0 )
		{
			vPorcGastComerc=0; 
			 var RECARGOS = 0
		}
		else
		{	
			 var RECARGOS = Number(RECAIMPO)+Number(RECFIMPO)
		 }
		
			var tabla="<table width='100%' bgcolor='#FFFFFF' cellspacing='0' cellpadding='0' style='border: solid #D0D0D0 1px'>"+
			"<tr ><td width='410px' ><div class='areaDatTit'><div class='areaDatTitTex' style='font-size:10px;'>Composici�n del precio</div>"+
			"</div></td></tr>"

			tabla+="<tr><td><div style='width:"+pX+"px; height:"+pY+"px; overflow:auto'><table cellspacing='0' cellpadding='0'>"

				tabla+="<tr id='fila_1'  class='Form_campname_colorL'  style='height:15px' >"
			    	tabla+="<td width='270px' style='text-align:left'>"
			    	tabla+="Prima Comisionable</td>"
			    	tabla+="<td width='70px' id='tdPrimComi' style='text-align:right'>"+	formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",") +"</td>"
			    	tabla+="<td width='70px' style='text-align:center'></td>"
			    	tabla+="</tr>"
			
				tabla+="<tr id='fila_2'  class='form_campname_bcoR'  style='height:15px' >"
			    	tabla+="<td width='270px' style='text-align:left'>"
			    	tabla+="Gastos Comercializaci�n</td>"
			    	tabla+="<td width='70px' id='tdGastCome' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),RECARGOS,2,",") +"</td>"
			    	
			    	tabla+="<td width='70px' id='tdGastComePorc' style='text-align:center'>("+ formatoNro("",vPorcGastComerc,2,",") +"%)</td>"
			    	tabla+="</tr>"
				tabla+="<tr id='fila_3'  class='Form_campname_colorL'  style='height:15px' >"
			    	tabla+="<td width='270px' style='text-align:left'>"
			    	tabla+="Gastos Administrativos</td>"
			    	tabla+="<td width='70px' id='tdGastAdm' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/DEREMIMP").text,2,",") +"</td>"
			    	tabla+="<td width='70px' style='text-align:center'></td>"
			    	tabla+="</tr>"
				tabla+="<tr id='fila_4'  class='form_campname_bcoR'  style='height:15px' >"
			    	tabla+="<td width='270px' style='text-align:left'>"
			    	tabla+="Sellados</td>"
			    	tabla+="<td width='70px' id='tdSellados' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/SELLAIMP").text,2,",") 
			    	tabla+="<td width='70px' style='text-align:center'></td>"
			    	tabla+="</tr>"
			    	var vIMPUESTOS = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/TASUIMPO").text) + Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IMPUEIMP").text)
			    	
				tabla+="<tr id='fila_5'  class='Form_campname_colorL'  style='height:15px' >"
			    	tabla+="<td width='270px' style='text-align:left'>"
			    	tabla+="Impuestos</td>"
			    	tabla+="<td width='70px' id='tdImpuestos' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vIMPUESTOS,2,",") +"</td>"
			    	tabla+="<td width='70px' style='text-align:center'></td>"
			    	tabla+="</tr>"
			  var vIVAIMPOR = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOR").text);
			  var vIVAIMPOA = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOA").text);
			  var vIVARETEN = Number(pXMLVal.selectSingleNode("//Response/CAMPOS/IVARETEN").text);
			  var vSumaIVA = vIVAIMPOR + vIVAIMPOA + vIVARETEN;
				tabla+="<tr id='fila_6'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='270px' style='text-align:left'>"+
			    	"I.V.A.</td>"+
			    	"<td width='70px' id='tdIVA' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vSumaIVA,2,",") +"</td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				tabla+="<tr id='fila_7'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='270px' style='text-align:left'>"+
			    	"Ingresos Brutos</td>"+
			    	"<td width='70px' id='tdIIBB' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/INGBRIMP").text,2,",") +"</td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
				tabla+="<tr id='fila_8'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='270px' style='text-align:left'>"+
			    	"<strong>Precio por Per�odo</strong></td>"+
			    	"<td width='70px' id='tdPrecPeri' style='text-align:right'><strong>"+ formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/RECTOIMP").text,2,",") +"<strong></td>"+
			    	"<td width='70px' style='text-align:center'></td>"+
			    	"</tr>"
			    	var vPRIMAIMP = pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text;
			    	var vPorcProductor = Number(document.getElementById("porcProductor").value);
			    	var vCOMIPROD = (vPRIMAIMP * vPorcProductor)/100;
			    	var vPorcOrganizador = Number(document.getElementById("porcOrganizador").value);
			    	var vCOMIORG = (vPRIMAIMP * vPorcOrganizador)/100;
				tabla+="<tr id='fila_8'  class='Form_campname_colorL'  style='height:15px' >"+
			    	"<td width='270px' style='text-align:left'>"+
			    	"Comisi�n Productor</td>"+
			    	"<td width='70px' id='tdComiProd' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vCOMIPROD,2,",") +"</td>"+
			    	"<td width='70px' id='tdComiProdPorc' style='text-align:center'>("+ formatoNro("",vPorcProductor,2,",") +"%)</td>"+
			    	"</tr>"
			    	var vComiTotal = formatoNro(datosCotizacion.getMonedaProducto(),(vCOMIPROD + vCOMIORG),2,",");
			    	var vComiTotalPorc = formatoNro("",(vPorcProductor + vPorcOrganizador),2,",");
			    	var vMostrarComiTotal = vComiTotal +' ('+ vComiTotalPorc +'%)';
				tabla+="<tr id='fila_8'  class='form_campname_bcoR'  style='height:15px' >"+
			    	"<td width='270px' style='text-align:left'>"+
			    	"Comisi�n Organizador</td>"+
			    	"<td width='70px' id='tdComiOrg' style='text-align:right'>"+ formatoNro(datosCotizacion.getMonedaProducto(),vCOMIORG,2,",") +"</td>"+
			    	"<td width='70px' id='tdComiOrgPorc' style='text-align:center'>("+ formatoNro("",vPorcOrganizador,2,",") +"%)</td>"+
			    	"</tr>"
			tabla+="</table></div></td></tr></table><p></p>"
			
			//Guardar en el los hidden de la pag main los valores calculados para poder recuperarlos
			document.getElementById('PRIMAIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text;
			document.getElementById('DEREMIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/DEREMIMP").text;
			document.getElementById('RECAIMPO').value = pXMLVal.selectSingleNode("//Response/CAMPOS/RECAIMPO").text;
			document.getElementById('RECFIMPO').value = 0	//Se va a obtener del combo de plan de pago q va a arreglar fito
			document.getElementById('TASUIMPO').value = pXMLVal.selectSingleNode("//Response/CAMPOS/TASUIMPO").text;
			document.getElementById('IVAIMPOR').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOR").text;
			document.getElementById('IVAIBASE').value = 0	//Va cero porq no las vamos a tener
			document.getElementById('IVAIMPOA').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IVAIMPOA").text;
			document.getElementById('IVAABASE').value = 0	//Va cero porq no las vamos a tener
			document.getElementById('IVARETEN').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IVARETEN").text;
			document.getElementById('IVARBASE').value = 0	//Va cero porq no las vamos a tener
			document.getElementById('IMPUEIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/IMPUEIMP").text;
			document.getElementById('SELLAIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/SELLAIMP").text;
			document.getElementById('INGBRIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/INGBRIMP").text;
			document.getElementById('RECTOIMP').value = pXMLVal.selectSingleNode("//Response/CAMPOS/RECTOIMP").text;;
			document.getElementById('COMISIMP').value = formatoNro("",(vCOMIPROD + vCOMIORG),2,".");
			document.getElementById("RECADMIN").value = document.getElementById("RECARGO").value;
			//document.getElementById("RECFINAN").value = document.getElementById("planPago").value.split("|")[1];
			if (document.getElementById("ESRECFINAN").value == "S")
			    document.getElementById("RECFINAN").value = document.getElementById("planPago").value.split("|")[1];
			else
			    document.getElementById("RECFINAN").value = 0;
			
			document.getElementById('hComiTotal').value = vMostrarComiTotal;
	
	
	document.getElementById('conten_popUp_compPrecio').innerHTML = tabla;
}
	ocultarPopUp2('popUp_compPrecio');
			
}
/***************************************************************************/
function fncMuestraCoberturas()
{

	if (document.getElementById('RAMOPCODSQL').value!='ICB1'){
		document.getElementById('divSeccionCoberturas').innerHTML=""
		document.getElementById('botonDeducibles').style.display = "none"
	}	
	/*else
	{
	    document.getElementById("cboActividadICOBB").value = -1
	    document.getElementById("cboPlanICOBB").length = 0
		fncActividadICOBBSeleccionada()
	}*/
	
	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;  		
	
	var objActividad = document.getElementById('cboActividadComercio');
	var codActividad = objActividad.value.split("|")[0]
	if(document.getElementById('CBO_PROVINCIA').value!=1)
	{
		if (document.getElementById('codPostalRiesgo').value=="") 
		{
		
		return
		}
	var mvarCpostal= document.getElementById('codPostalRiesgo').value
	}
	else
	{
		var mvarCpostal= 1005
	}
	var mvarRequest=	"<Request>"+
	"<PROVICOD>"+document.getElementById('CBO_PROVINCIA').value+"</PROVICOD>"+
	"<CPACODPO>"+mvarCpostal+"</CPACODPO>"+
	"<RAMOPCOD>"+document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
	"</Request>"
	
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetZona') 
	
	if(oXMLValores.loadXML(mvarResponse))	
	{
		if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text = "true")
		{
			if(oXMLValores.selectSingleNode("//CODIZONA").text !="0000") {
			    var mvarZona = oXMLValores.selectSingleNode("//CODIZONA").text
			    var mvarZonaDes = oXMLValores.selectSingleNode("//ZONASDES").text
			    document.getElementById("CODIGOZONA").value = mvarZona
			    document.getElementById("verZona").innerText = mvarZonaDes
	
			}
			else
			{
			alert_customizado("No se pudo determinar zona para esta localidad")
			return false
			
			}
		}
		else
		{	alert_customizado(oXMLValores.selectSingleNode("//Response/Estado/@mensaje").text)
			
			return false
		}
	}
	
	
	if (document.getElementById('RAMOPCODSQL').value=='ICB1')
		return
		
	// MAA 14-06-11 CUIDADO !! la cantidad de coberturas del ms2390 debe ser igual al ms1530
	var mvarRequest = "<Request>"+
	          "<DEFINICION>2390_ListadoCoberturas.xml</DEFINICION>"+        
	          "<USUARCOD>" + document.getElementById("LOGON_USER").value +"</USUARCOD>" +
	          "<CODIZONA>"+mvarZona+"</CODIZONA>" +
	          "<PROFECOD>"+codActividad+"</PROFECOD>"+
	          "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
              "</Request>"
              
        var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ') 
        
	
	if(oXMLValores.loadXML(mvarResponse))	
	{//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+ oXMLValores.xml+"</textarea>")	
		//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/getseccionCoberturas.xsl"))
		if(oXSLValores.loadXML(document.getElementById("getseccionCoberturas").xml))
		{	
			
			  var  xslParam="<parametros><parametro><nombre>estadoOperacion</nombre><valor>C</valor></parametro>"+
			  "<parametro>"+
				"<nombre>monedaProducto</nombre><valor>"+datosCotizacion.getMonedaProducto()+"</valor>"+
			"</parametro></parametros>"  
						
			//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varXml.xml+"</textarea>")			
			var varResultTabla = parseaXSLParam(oXMLValores.xml,oXSLValores,xslParam)	
			
			document.getElementById("xmlCoberturas").innerHTML = oXMLValores.xml
			document.getElementById("divSeccionCoberturas").innerHTML = varResultTabla;	
			document.getElementById('botonDeducibles').style.display="inline"
			
			document.getElementById('divBotonAgregarItem').style.display = "inline"
			
			
			
			
			if(oXMLValores.selectSingleNode("//SWALUMNO").text=="S") document.getElementById('seccion_CantAlumnosCarteles').style.display = "inline"
			
		}
	}
	
	//LR 02/05/2011 Se habilitan Coberturas habilit para el Alta
	fncHabilitarCoberturasDisp('A');
	
	/******************************/
	var cantCober= document.all.COBCHECK.length
	var mvarRequest = "<Request>"+
	"<DEFINICION>1530_DeduciblesICO.xml</DEFINICION>"+
	"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
	"<ACTIVIDAD>"+codActividad+"</ACTIVIDAD>"+
	"<CODIZONA>"+mvarZona+"</CODIZONA>"+
	"<CANT>"+cantCober+"</CANT>"+	
	"<COBERTURAS>"
	for (x=0; x<cantCober; x++)
	{
	mvarRequest +="<COBERTURA>"
			mvarRequest +="<COBERCOD>"+document.all.COBCHECK[x].cobercod+"</COBERCOD>"
			mvarRequest +="<NUMERMOD>1</NUMERMOD>"
	mvarRequest +="</COBERTURA>"
	}
	mvarRequest +="</COBERTURAS>"
	mvarRequest +="</Request>"
	
	    var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ') 
	    
	    if(oXMLValores.loadXML(mvarResponse))	
	{
		if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text = "true")
		{
			{
				fncCargaDeducibles(oXMLValores)
			}
		}
		else
		{	alert_customizado(oXMLValores.selectSingleNode("//Response/Estado/@mensaje").text)
			
			return false
		} 
   }
	/******************************/
	var mvarRequest = "<Request>"+	         
	          "<USUARIO>" + document.getElementById("LOGON_USER").value +"</USUARIO>" +
	          "<ZONA>"+mvarZona+"</ZONA>" +
	          "<ACTIVIDAD>"+codActividad+"</ACTIVIDAD>"+
	          "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
              "</Request>"

	var mvarResponse = llamadaMensajeMQ(mvarRequest, 'lbaw_GetActivPreguntas')
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "preguntas:<textarea>" + mvarResponse + "</textarea>")

	document.getElementById("xmlPreguntas").value = mvarResponse
	  	
	  if(oXMLValores.loadXML(mvarResponse))	
	{
		if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text = "true")
		{
			{
				fncCargaPreguntas(oXMLValores)
			}
		}
		else
		{	alert_customizado(oXMLValores.selectSingleNode("//Response/Estado/@mensaje").text)
			
			return false
		}
	}
	
	/******************************************/
	
	var totalCoberturas = document.all.COBCHECK? document.all.COBCHECK.length:0; //document.all.COBCHECK.length;
	
	for (var i=0;i<totalCoberturas;i++) 
		{ 
			if (document.all.COBCHECK[i].checked)
			{	
				/*if (!document.all.PREGUNTA_C.length)
				{
					alert(i +"-"+ document.all.COBCHECK[i].cobercod +'-'+ document.all.PREGUNTA.coberturaActivante)
					if(document.all.COBCHECK[i].cobercod == document.all.PREGUNTA.coberturaActivante)
					{
						alert(i)
						document.getElementById("DIVPREGUNTA-0").style.display="inline"
						fncColoreoRenglones();
					}
				} else {*/
					var cantPreguntas = document.all.PREGUNTA.length
					
					for(var x=1; x<cantPreguntas; x++)
					  {
					  if(document.all.COBCHECK[i].cobercod == document.all.PREGUNTA[x].coberturaActivante)
					  	{
					  	//alert(document.all.PREGUNTA[x].coberturaActivante+"  "+document.all.PREGUNTA[x].codigoAIS +"  "+document.all.PREGUNTA[x].id +" "+document.all.PREGUNTA[x].style.display)
					  	//if (pThis.checked)	
				   			document.getElementById("DIVPREGUNTA-" + x).style.display="inline"
				   			fncColoreoRenglones();
				   			
				   			/*else
							{document.getElementById("DIVPREGUNTA-" + x).style.display="none"
							fncColoreoRenglones();
							}*/
						}	
		
		  			}
				//}
			}
		}
	if(document.getElementById("FILA280"))		document.getElementById("FILA280").style.display = "none"
	/*******************************************/
}

function fncCargaPreguntas(pXMLValores)

{
	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;  		
	
	if(oXMLValores.loadXML(pXMLValores.xml))	
	{//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+ oXMLValores.xml+"</textarea>")	
		//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/getCombosPreguntas.xsl"))
		if(oXSLValores.loadXML(document.getElementById("getCombosPreguntas").xml))
		{	
			  var  xslParam="<parametros><parametro><nombre>estadoOperacion</nombre><valor>C</valor></parametro></parametros>"  
						
			//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varXml.xml+"</textarea>")			
			var varResultTabla = parseaXSLParam(oXMLValores.xml,oXSLValores,xslParam)
			document.getElementById("seccion_Preguntas").innerHTML = varResultTabla;	
			
		}
	}
	
	
	
	
}

function ocultaPreguntas()

{
	if (document.all.PREGUNTA){
	var cantPreguntas = document.all.PREGUNTA.length
	
	for(var x=1; x<cantPreguntas; x++)
	  {
	  document.getElementById("DIVPREGUNTA-" + x).style.display="none"
	 
	}	
	
}
}
function fncCargaDeducibles(oXMLValores)


{
	var cantCober = document.all.COBCHECK.length
	
	var oXMLDeducible= oXMLValores.selectNodes("//DEDUCIBLE")
	for (var x=0; x<cantCober; x++)
	{
		for(var y=0; y<oXMLDeducible.length;y++)
		if(oXMLDeducible[y].selectSingleNode("COBERCOD").text == document.all.COBCHECK[x].cobercod)
		{
			document.getElementById("COBERDES" +x).innerText += " *"		
			
		}
		
		
	}
	
	
}



/***************************************************************/
// Funciones SELECCIONAR COBERTURAS

function _indiceCobertura(pPropiedad,pCodigo)
{	//

	var cantidadCoberturas = document.all.COBCHECK.length
	for(var x=0; x<cantidadCoberturas; x++)
		{	
				if(eval("document.all.COBCHECK[x]."+pPropiedad) == pCodigo )
				{	
				//alert(pPropiedad +"  | "+ pCodigo +"  | "+ x)			
				  return x				
				 break
				}
			} 
	
}

function fncCoberturaSeleccionada(pThis)
{	
	
	//window.status=""
	var cantidadCoberturas = document.all.COBCHECK.length
	var pDato= pThis.id
	var subIndice=pDato.substring(8,pDato.length)
	
	//alert(document.getElementById( "SUMAASEG"+subIndice ).valorOriginal )
	if(pThis.cobercod == '101')
	{
	if(pThis.checked ) 
	{
		var indiceCob = _indiceCobertura("cobercod","120")
		if (indiceCob != undefined)
		{
			//Def.296 HRF 20110329
			document.all.COBCHECK[indiceCob].checked= false
			document.all.COBCHECK[indiceCob].disabled= true   
			document.getElementById("SUMAASEG"+indiceCob).disabled=true
			document.getElementById("SUMAASEG"+indiceCob).value=""
			
		}	
	}
	else
	{
		var indiceCob = _indiceCobertura("cobercod","120")
		
		document.all.COBCHECK[indiceCob].disabled= false   
		document.getElementById("SUMAASEG"+indiceCob).disabled=false
	//	document.getElementById("SUMAASEG"+indiceCob).value=""	
	}
	}
	
	if(pThis.cobercod == '120')
	if(pThis.checked ) 
	{
		var indiceCob = _indiceCobertura("cobercod","101")
		//Def.296 HRF 20110329
		document.all.COBCHECK[indiceCob].checked = false
		document.all.COBCHECK[indiceCob].disabled= true   
		document.getElementById("SUMAASEG"+indiceCob).disabled=true
		document.getElementById("SUMAASEG"+indiceCob).value=""
	}
	else
	{
		var indiceCob = _indiceCobertura("cobercod","101")
		document.all.COBCHECK[indiceCob].disabled= false   
		document.getElementById("SUMAASEG"+indiceCob).disabled=false
		//document.getElementById("SUMAASEG"+indiceCob).value=""	
	}
	
	/************ Habilita secundarias si ppal checked  *******************************************/
var cobActual = pThis.cobercod
 if (cobActual=="120") cobActual="101"
 
 

   for(var x=0; x<cantidadCoberturas; x++)
   {  
   	//Si es una secundaria
   	  if(document.all.COBCHECK[x].codigoCoberturaPrincipal == cobActual )
   		if (pThis.checked)	
   			{	document.all.COBCHECK[x].disabled= false   	
   				if(document.all.COBCHECK[x].obligSiPpal=="S")
   				{
   					document.all.COBCHECK[x].checked= true
   					document.all.COBCHECK[x].disabled= true
   				}
   							
   			
   			}
   		else
   			{
   			
   			document.all.COBCHECK[x].disabled= true
   			document.all.COBCHECK[x].checked= false
   			document.getElementById("SUMAASEG" + x).value=""
   			}
   	
   	  if(document.all.COBCHECK[subIndice].cobercod == "120" ||document.all.COBCHECK[subIndice].cobercod == "101" )
    		 if(document.all.COBCHECK[x].obligatoria == "S"  )
     		{
     			if (!pThis.checked)	{
     				
     				//document.all.COBCHECK[x].disabled= true
   				//document.all.COBCHECK[x].checked= false
   				document.getElementById("SUMAASEG" + x).value=""
     				
     				}
     		}
   
   }
   
	
	
/******************************************************************/	
//
	var codPpal =  pThis.codigoCoberturaPrincipal 
	var indice120 = _indiceCobertura("cobercod","120")
	if(indice120 !=undefined)
		if(document.all.COBCHECK[indice120].checked )
			if (codPpal=="101") codPpal="120"
	
	if (pThis.checked)	
	{
		if(pThis.codigoCoberturaPrincipal != "0" )  //Si this es secundaria and topeSuma =F
		{ 
			if(pThis.topeSuma=="F")
			{	
				var indicePpal = _indiceCobertura("cobercod",codPpal )			
				//window.status="Subindice:"+subIndice+ " Indice Ppal: " +eval(indicePpal)+ " Cod Ppal: "+ codPpal+" Suma Ppal: "+ document.getElementById("SUMAASEG" + eval(indicePpal)).value+" Porc: "+ document.all.COBCHECK[subIndice].porcPrincipal
	   			document.getElementById("SUMAASEG" +subIndice).value= Number(document.getElementById("SUMAASEG" + eval(indicePpal)).value * document.all.COBCHECK[subIndice].porcPrincipal/100)
   			}else if(pThis.topeSuma=="T")
			{
				document.getElementById("SUMAASEG"+subIndice).disabled=false
				
				
   			}
   			else
   			{
   				document.getElementById("SUMAASEG"+subIndice).disabled=false
   				
   			}
		 
		 
		 }
		 else
		 	document.getElementById("SUMAASEG"+subIndice).disabled=false
	}
	else
	{	
		document.getElementById("SUMAASEG"+subIndice).disabled=true
		document.getElementById("SUMAASEG"+subIndice).value=""		
	}
	


/******************************************************/
//Adriana Incidente cuando no hay preguntas
	if (document.all.PREGUNTA){
		
	var cantPreguntas = document.all.PREGUNTA.length
	
	for(var x=1; x<cantPreguntas; x++)
	  {
	  if(document.all.COBCHECK[subIndice].cobercod == document.all.PREGUNTA[x].coberturaActivante)
	  	{
	  	//alert(document.all.PREGUNTA[x].coberturaActivante+"  "+document.all.PREGUNTA[x].codigoAIS +"  "+document.all.PREGUNTA[x].id +" "+document.all.PREGUNTA[x].style.display)
	  	if (pThis.checked)	
   			{document.getElementById("DIVPREGUNTA-" + x).style.display="inline"
   			fncColoreoRenglones();
   			}
   			else
			{document.getElementById("DIVPREGUNTA-" + x).style.display="none"
			fncColoreoRenglones();
			}
		}	
	
	  }
	  
	}

}

function calculaModulo(pCobercod,pSuma)
{
/*  
TODO: se calcula el modulo
  //COBERTURA[COBERCOD=312]/MODULOS/MODULO[COBECMIN<=250000 and COBECMAX>=250000]/CODMODUL

*/	
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	var modulo = 0
	
	if (varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO[COBECMIN<="+pSuma+" and COBECMAX>="+pSuma+"]/CODMODUL"))
		modulo = varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO[COBECMIN<="+pSuma+" and COBECMAX>="+pSuma+"]/CODMODUL").text
	else
		modulo = mayorModulo(pCobercod)
	
	return modulo
}

function cantidadModulos(pCobercod)
{
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	
	var cantmodulo = varOXML.selectNodes("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO").length;
	
	return cantmodulo
}

function mayorModulo(pCobercod)
{
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	
	var varModulo = varOXML.selectNodes("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO");
	var varCanMod = varModulo.length;
	var mayorMod = 0;
	
	for(x=0;x<varCanMod;x++)
	{
		mayorMod = varModulo[x].selectSingleNode("CODMODUL").text;
	}
	
	return mayorMod
}

function menorModulo(pCobercod)
{
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	
	var menorMod = varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO/CODMODUL").text;
	
	return menorMod
}

function esModulada()
{
 	var cantMod101 = cantidadModulos('101')
	var cantMod120 =  cantidadModulos('120')
	if (cantMod101 > 1 || cantMod120 > 1)
		return true
	else
		return false

}
function topeMinimoModulo(pCobercod,pModulo)
{
	
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	
	//var tope = varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO["+ eval(pModulo-1) +"]/COBECMIN").text
	if(varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO[CODMODUL="+ pModulo +"]/COBECMIN"))
		var tope = varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO[CODMODUL="+ pModulo +"]/COBECMIN").text
	else
	{
		errorInesperado("Error en tope m�nimo para la cobertura " + getCOBERDES(pCobercod),true)
		return false
	}
	return tope
}

function topeMaximoModulo(pCobercod,pModulo)
{
	
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	
	//var tope = varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO["+ eval(pModulo-1) +"]/COBECMAX").text
	if(varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO[CODMODUL="+ pModulo +"]/COBECMAX"))
		var tope = varOXML.selectSingleNode("//COBERTURA[COBERCOD='"+pCobercod+"']/MODULOS/MODULO[CODMODUL="+ pModulo +"]/COBECMAX").text
	else
		errorInesperado("Error en tope m�ximo para la cobertura " + getCOBERDES(pCobercod),true)
	
	return tope
}
//Variable global que mantiene el modulo para las moduladas
var moduloActual = 1

function siCambiaSuma(pThis, pError)
{

var cantidadCoberturas = document.all.COBCHECK.length
	var pDato= pThis.id
	var subIndice=pDato.substring(8,pDato.length)
var estaFueraNorma 
	
	if (document.all.COBCHECK[subIndice].checked)	
	{ 
			var tipoModulo = document.all.COBCHECK[subIndice].detModulo
			var codActual=document.all.COBCHECK[subIndice].cobercod
			var codPpal=document.all.COBCHECK[subIndice].codigoCoberturaPrincipal	
			var cantModulos=	mayorModulo(codActual)	
   		var sumaActual =  Number(document.getElementById("SUMAASEG" +subIndice).value)
   		var menModulo = menorModulo(codActual)
		//alert("tipoModulo: " + tipoModulo + " codActual: " + codActual+ " codPpal: " +codPpal+ " cantModulos: " +cantModulos+ " sumaActual: " + sumaActual + " menModulo: " +menModulo)		
   		// TIPO Modulo: I= Independiente; S=es la cobertura que det mod; N=depende de la S  			
   			
   			//************* POR TOPES *****************************
			if(document.all.COBCHECK[subIndice].topeSuma=="T")
			{ 
				var topeMaximo = Number(topeMaximoModulo(codActual,moduloActual)) 						
	   			var topeMinimo = Number(topeMinimoModulo(codActual,moduloActual))	
				
			// Adriana 7-3-13	
			//	var codPpal =  pThis.codigoCoberturaPrincipal 
	            var indice120 = _indiceCobertura("cobercod","120")
	            if(indice120 !=undefined)
		        if(document.all.COBCHECK[indice120].checked )
			       if (codPpal=="101") codPpal="120"
				
				
			// Fin Adriana
				var indicePpal = _indiceCobertura("cobercod",codPpal )
				
				//alert("Adriana-codppal="+codPpal)
	   			var cobepmaxPpal = Number(document.getElementById("SUMAASEG" + eval(indicePpal)).value * document.all.COBCHECK[subIndice].porcPrincipal/100)
   				var sumaActual =  Number(document.getElementById("SUMAASEG" +subIndice).value)
   				var maximoCob=Math.min(topeMaximo,cobepmaxPpal)
   				var minimoCob =  topeMinimo  
				
   				if(sumaActual >maximoCob )
   				{ 
   				/*	if (!pError)
	   				{   					
   						alert_customizado ("La suma debe ser menor a " + maximoCob)
   				   		document.getElementById("SUMAASEG" +subIndice).value= maximoCob  	
   				   	}
   				   	else*/
   				   	//CASO QUE TIENE TOPE Y LA SUMA DE LA SECUNDARIA ES MAYOR AL TOPE 	
   				   if(itemActualEditado) 	 
   				   	
	   				{
	   						//alert('Adriana-tope maximo1='+maximoCob)
							insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" + subIndice,"Suma Asegurada debe ser menor a "+  formatoNro(datosCotizacion.getMonedaProducto(),maximoCob,2,",") ,2,"N","COBERDES" +subIndice,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)	   				
	   						insertarPopUpFueraNorma(
	   						document.getElementById("OTID").value, 
	   						document.getElementById("localidadRiesgo").value ,
	   						 "SUMAASEG" + subIndice,
	   						 "Suma Asegurada debe ser menor a "+  formatoNro(datosCotizacion.getMonedaProducto(),maximoCob,2,",") ,
	   						 2,"N","COBERDES" +subIndice)		   	
   				  		
	   						 estaFueraNorma = false 
	   						 return estaFueraNorma
	   					
	   					//insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser menor a " +formatoNro(datosCotizacion.getMonedaProducto(),maximoCob,2,","),2,"E","COBERDES" +subIndice)
	   					//return
	   				}
	   				
	   				
	   				
	   			   // estaFueraNorma = true 				 
				}
				if(sumaActual <minimoCob )
   				{ 
   					if (!pError)
	   				{   					
   						alert_customizado ("La suma debe ser mayor a " + minimoCob)
   				   	 	document.getElementById("SUMAASEG" +subIndice).value= minimoCob  
   				   	}
   				   	 else
	   				{
	   					if(itemActualEditado) 
	   					{
	   						insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a " + formatoNro(datosCotizacion.getMonedaProducto(),minimoCob,2,",")  ,2,"E","COBERDES" +subIndice)
	   						return 
	   					}
	   					else
	   					{
	   						
	   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a "+  formatoNro(datosCotizacion.getMonedaProducto(),minimoCob,2,",") ,2,"N","COBERDES" +subIndice,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
	   						insertarPopUpFueraNorma(document.getElementById("OTID").value, document.getElementById("localidadRiesgo").value , "SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a "+  formatoNro(datosCotizacion.getMonedaProducto(),minimoCob,2,",") ,2,"N","COBERDES" +subIndice)		   	
   				  		
	   						 estaFueraNorma = false 
	   						 return estaFueraNorma
	   					}
	   				}		
				}
		  }//
   			
   			
   			//****************** POR SUMAS ********************
   			if(tipoModulo=="S" )  
   			{
   				if (codActual=="101" ) 
   				{
   					if (sumaActual > topeMaximoModulo("101",cantModulos) ) 
   					{
   						codActual="120"  
   						cantModulos=mayorModulo(codActual)   					
   						menModulo = menorModulo(codActual)
   					}
   					var topeMaximo = Number(topeMaximoModulo(codActual,cantModulos)) 						
	   				var topeMinimo = Number(topeMinimoModulo(codActual,menModulo))
	   				if(sumaActual >topeMaximo )
	   				{ 	/*if (!pError)
	   					{	
	   						alert_customizado ("La suma debe ser menor a " + topeMaximo)
	   						document.getElementById("SUMAASEG" +subIndice).value= topeMaximo	   				   	
	   				   	}  
	   					else
	   					{
	   						insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser menor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",")   ,2,"E","COBERDES" +subIndice)
	   					}*/
	   					
	   					 estaFueraNorma = true
	   				   //FN sumaActual=topeMaximo
					}
					if(sumaActual <topeMinimo )
	   				{ 
	   					if (!pError)
	   					{
	   						alert_customizado ("La suma debe ser mayor a  " + topeMinimo)
	   				   		document.getElementById("SUMAASEG" +subIndice).value= topeMinimo  
	   				   	}
	   				   	else
	   					{
	   						if(itemActualEditado) 
			   					{
			   						 insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a  "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,","),2,"E","COBERDES" +subIndice)	   				
			   						return 
			   					}
			   					else
			   					{
			   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a "+  formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,",") ,2,"N","COBERDES" +subIndice,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
	   				
			   						 estaFueraNorma = false 
			   						 return estaFueraNorma
			   					}
	   						
	   						}
	   				   	sumaActual=topeMinimo	 				   		
					}
   					var indiceCob = _indiceCobertura("cobercod",codActual) 
   					document.getElementById("SUMAASEG" +indiceCob).value = sumaActual
   					if(indiceCob!=subIndice)
   					{
   						if (!pError)
	   					{
   							alert_customizado("Corresponde "  +  document.getElementById("COBERDES" +indiceCob).innerText)
   						}
   						else
	   					{
	   						if(itemActualEditado) 
			   					{
			   						 insertarError( "","SUMAASEG" + subIndice,"Corresponde "  +  document.getElementById("COBERDES" +indiceCob).innerText,2,"E","COBERDES" +subIndice)
	   			
			   						return 
			   					}
			   					else
			   					{
			   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" +  subIndice,"Corresponde "  +  document.getElementById("COBERDES" +indiceCob).innerText ,2,"N","COBERDES" +subIndice,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
	   				
			   						 estaFueraNorma = false 
			   						 return estaFueraNorma
			   					}	
	   						
	   					}
	   					document.getElementById("SUMAASEG" +subIndice).value = ""
	   					document.getElementById("SUMAASEG" +indiceCob).disabled = false   
	   					document.getElementById("SUMAASEG" +subIndice).disabled = true  
	   				}
	   				document.getElementById("COBCHECK" +subIndice).checked = false
	   				document.getElementById("COBCHECK" +subIndice).disabled = true   				  
	   				document.getElementById("COBCHECK" +indiceCob).checked = true	   				
	   				fncCoberturaSeleccionada(document.getElementById("COBCHECK" +indiceCob))
	   				document.getElementById("COBCHECK" +indiceCob).disabled = false
   				}  // FIN 101
   				
   				if (codActual=="120" )
   				{
   				 	if (  sumaActual < topeMinimoModulo("120",menModulo)  )
   					{
   						codActual="101"  
   						cantModulos=	mayorModulo(codActual)	
   						menModulo = menorModulo(codActual)  
   					}
	   				var topeMaximo = Number(topeMaximoModulo(codActual,cantModulos)) 
		   			var topeMinimo = Number(topeMinimoModulo(codActual,menModulo))
	   				if(sumaActual >topeMaximo )
	   				{ 
	   					/*if (!pError)
		   				{	   					
	   						alert_customizado  ("La suma debe ser menor a " + topeMaximo)
	   				   		document.getElementById("SUMAASEG" +subIndice).value= topeMaximo   
	   				   	}
   				   	 	else
	   					{
	   						 insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada deber ser menor a " + formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"E","COBERDES" +subIndice)
	   					}*/
	   				
   				   	//FN sumaActual=topeMaximo 	
   				   	estaFueraNorma = true			   	
					}
					if(sumaActual <topeMinimo )
   					{ 
	   					if (!pError)
		   				{   					
	   						alert_customizado ("La suma debe ser mayor a " + topeMinimo)
	   				   		document.getElementById("SUMAASEG" +subIndice).value= topeMinimo   
	   				   	}
	   				   	 else
		   				{
		   						if(itemActualEditado) 
			   					{
			   						 insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a " + formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,","),2,"E","COBERDES" +subIndice)
		   				
			   						return 
			   					}
			   					else
			   					{
			   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a "+  formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,",") ,2,"N","COBERDES" +subIndice,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
	   				
			   						 estaFueraNorma = false 
			   						 return estaFueraNorma
			   					}	
		   						
		   						
		   						
		   				} 
	   				
   				   		sumaActual=topeMinimo  				   		
					}
   				
   					var indiceCob = _indiceCobertura("cobercod",codActual)   				 
   				 	document.getElementById("SUMAASEG" +indiceCob).value = sumaActual   				
   				 	if(indiceCob!=subIndice)
   				 	{   				 	
	   				 	if (!pError)
		   				{
	   				 	 	alert_customizado("Corresponde "  +  document.getElementById("COBERDES" +indiceCob).innerText)
	   					}   					
   				 		document.getElementById("SUMAASEG" +subIndice).value = ""
   				 		document.getElementById("SUMAASEG" +indiceCob).disabled = false 
   					 	document.getElementById("SUMAASEG" +subIndice).disabled = true   
   					} 				 
   				 	document.getElementById("COBCHECK" +subIndice).checked = false
   				  	document.getElementById("COBCHECK" +subIndice).disabled = true   				  
   				  	document.getElementById("COBCHECK" +indiceCob).checked = true   				 
	   				fncCoberturaSeleccionada(document.getElementById("COBCHECK" +indiceCob)) 
   				   	document.getElementById("COBCHECK" +indiceCob).disabled = false
   					  									
   				} // FIN SI ES 120
   			
   			}  // FIN TIPO S
   			
   			if(tipoModulo=="I" )  
   			{
   				var topeMaximo = Number(topeMaximoModulo(codActual,cantModulos)) 						
	   			var topeMinimo = Number(topeMinimoModulo(codActual,menModulo))
				
   			}
   			if(tipoModulo=="N"  )  
   			{
   				var topeMaximo = Number(topeMaximoModulo(codActual,moduloActual)) 						
	   			var topeMinimo = Number(topeMinimoModulo(codActual,moduloActual))	
   			}
   			if(tipoModulo!="S" )
   			{
			
	   		if(sumaActual >topeMaximo )
   				{ 
   					/*
   					if (!pError)
	   				{   					
   						alert_customizado ("La suma debe ser menor a xx" + topeMaximo)
   				   		document.getElementById("SUMAASEG" +subIndice).value= topeMaximo 
   				   	}
   				   	 else
	   				{
	   					 insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser menor a " + formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"E","COBERDES" +subIndice)
	   				} 
	   				*/
   				   	sumaActual=topeMaximo 
   				   	 estaFueraNorma = true
				}
				if(sumaActual <topeMinimo )
   				{ 
   					if (!pError)
	   				{   					
   						alert_customizado ("La suma debe ser mayor a " + topeMinimo)
   				   		document.getElementById("SUMAASEG" +subIndice).value= topeMinimo   
   				   	}
   				   	 else
	   				{
	   					if(itemActualEditado) 
			   					{
			   						 insertarError( "","SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a " + formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,","),2,"E","COBERDES" +subIndice)
	   				
			   						return 
			   					}
			   					else
			   					{
			   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" + subIndice,"Suma Asegurada debe ser mayor a "+  formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,",") ,2,"N","COBERDES" +subIndice,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
	   				
			   						 estaFueraNorma = false 
			   						 return estaFueraNorma
			   					}	
	   					
	   					
	   				}
   				   	sumaActual=topeMinimo
				}
			}
			if(tipoModulo=="S")
   			{	
   			//DETERMINO MODULO   		
   			 moduloActual = calculaModulo(codActual,sumaActual)
   			
			}
			//RECORRE TODAS LAS COBERTURAS			
			for(var x=0; x<cantidadCoberturas; x++)
			{	var modulo = document.all.COBCHECK[x].detModulo
			if(tipoModulo=="S")
			{				
				if(modulo=="N" &&  document.all.COBCHECK[x].obligatoria== "S") 
				{
					if (!pError)	   				
						document.getElementById("SUMAASEG" +x).value= topeMinimoModulo(document.all.COBCHECK[x].cobercod,moduloActual)
				}
			}
			if(document.all.COBCHECK[x].codigoCoberturaPrincipal == codActual )
			{
					if(document.all.COBCHECK[x].checked)
					{
						if(document.all.COBCHECK[x].topeSuma=="F")
						{	if (!pError)
								document.getElementById("SUMAASEG" +x).value= Number(document.getElementById("SUMAASEG" + subIndice).value * document.all.COBCHECK[x].porcPrincipal/100)
   						}
						if(document.all.COBCHECK[x].topeSuma=="T")	{
							if (!pError)				
								document.getElementById("SUMAASEG" +x).value= ""//Number(document.getElementById("SUMAASEG" + subIndice).value * document.all.COBCHECK[x].porcPrincipal/100)
   						}
   					}
			}
			
		}
		  
		  
	}	
	
	return estaFueraNorma
	
}
/***********************************************************************/

function validaInsertaItemRiesgo()
{	
	
	

	//19/10/2010 LR Defect 21 Estetica, pto 36
	if(!validarFormulario('xmlValidacionIndividual', "//OTROS", 'XSLTablaErrores', false,false))
		return
		
	//	window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+document.getElementById("varXmlItems").value+"</textarea>")

/*	if (document.getElementById("RAMOPCODSQL").value=="ICB1"){
		alert("Antes de agregarItemXMLOT")
		agregarItemXMLOT(document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value,"FN" );
		return true
	}	*/
//try{	
	oXMLErrores.async = false;	
	oXMLErrores.loadXML("<ERRORES></ERRORES>") 
	
	varXmlItems = new ActiveXObject("MSXML2.DOMDocument");
	varXmlItems.async = false;
	varXmlItems.loadXML(document.getElementById("varXmlItems").value);
	
	
	var cantItems=varXmlItems.selectNodes("//ITEMS/ITEM").length
	var varNroItem=0 

	if (trim(varXmlItems.xml)=="<ITEMS/>"){
		varNroItem=1}
	else
		{
		//for (v)
		varNroItem=varXmlItems.selectNodes("//ITEMS/ITEM").length+1
		}

	document.getElementById("varItemActual").value=varNroItem		
		
	var varXslTablaErrores = new ActiveXObject("MSXML2.DOMDocument");	
	varXslTablaErrores.async = false;		
	varXslTablaErrores.load(document.getElementById('XSLTablaErrores').XMLDocument);
	
	var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
	varXslFueraNorma.async = false;	
	varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);
	
	var cantidadCoberturas = document.all.COBCHECK?document.all.COBCHECK.length:0
	
	auxXml = new ActiveXObject('MSXML2.DOMDocument'); 	  
	auxXml.async = false;
	auxXml.loadXML(document.getElementById("xmlFueraNorma").value);
	
	//valida cantidad de coberturas	minimas
	
	var cantFueraNorma = auxXml.selectNodes("//ERROR").length
	
	if (cantFueraNorma == 0)
	{  document.getElementById("divTablaFueraNorma").innerHTML =""
		oXMLFueraNorma.loadXML("<ERRORES></ERRORES>")		
	}
	else				
		oXMLFueraNorma.loadXML(document.getElementById("xmlFueraNorma").value)
		
	//Vacio para que no muestre lo anterior
		oXMLPopUpFueraNorma.loadXML("<ERRORES></ERRORES>")
		
	/*****************************************/	
	
	if (document.getElementById("cboActividadComercio").value.split("|")[0]== "050040"){
	   //MAG 20-03-2014 Se aumenta a 200 el minimo de alumnos para tocar la Tasa
		if (Number(document.getElementById("cantAlumnosCarteles").value) < 200)
			insertarError( "Cant. Alumnos:", "cantAlumnosCarteles","La cantidad de alumnos debe ser m�nimo 200.",2,"E","")
	}
	
	/*****************************************/	
	if (getCantCoberturasChecked()<3)
	{
	 //GED - 20/09/2010 -  Defect 22 
			if(itemActualEditado) 	
				insertarError( "Coberturas", "SUMAASEG0","Debe seleccionar al menos tres coberturas principales",2,"E","")
			//else
				//insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "Debe seleccionar al menos tres coberturas principales" ,2,"N","",document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)				
	   	
   	}
	else	
	{	
	/*****************************************/	
		if(document.all.COBCHECK[ _indiceCobertura("cobercod","100")].checked == false && document.all.COBCHECK[ _indiceCobertura("cobercod","101")].checked==false && document.all.COBCHECK[ _indiceCobertura("cobercod","120")].checked==false  )
	 	{
	 		
	 			if(itemActualEditado) 	
	 				insertarError( "", "SUMAASEG0","Debe seleccionar alguna cobertura de Incendio",2,"E","")
	 			else
   					insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "Debe seleccionar alguna cobertura de Incendio" ,2,"N","",document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)				
	   	
   			
	 	}
	 /*****************************************/	
	 	if(esModulada)
	 		if( document.all.COBCHECK[ _indiceCobertura("cobercod","101")].checked==false && document.all.COBCHECK[ _indiceCobertura("cobercod","120")].checked==false  )
	 	{
	 				if(itemActualEditado) 	
	 					insertarError( "", "SUMAASEG0","Debe seleccionar alguna cobertura de Incendio Contenido",2,"E","")
   					else
   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "Debe seleccionar alguna cobertura de Incendio Contenido" ,2,"N","" ,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)				
	   	
	 	}
	 	
	 /*****************************************/	
	 	
	 	var sumaAseg100=Number(document.getElementById("SUMAASEG" + _indiceCobertura("cobercod","100")).value)
	   	var sumaAseg101=Number(document.getElementById("SUMAASEG" + _indiceCobertura("cobercod","101")).value)
	   	
	   	var indice120 = _indiceCobertura("cobercod","120")
	   	if (indice120 != undefined)
	   		var sumaAseg120=Number(document.getElementById("SUMAASEG" + indice120).value)
	   	else
	   		var sumaAseg120 = 0
	   //arreglar 120
	   	var sumaCompara = sumaAseg101 ==0?sumaAseg120:sumaAseg101
	   		
	 	if( document.getElementById("cboActividadComercio").value.split("|")[0]== "050002")
	 	{
	 		if (sumaCompara!=0 && sumaAseg100< Number(sumaCompara*0.2)) 
	   		{		// GED 27-12-2010 SE CORRIGE ERROR EN VALIDACION
	   				//if( document.all.COBCHECK[ _indiceCobertura("cobercod","100")].disabled == false)
	   				
	   				if( document.all.COBCHECK[ _indiceCobertura("cobercod","100")].checked == true)
	   					/*if(itemActualEditado) 	
	   						insertarError( "", "SUMAASEG0","La suma asegurada no puede se menor al 20 % de las otras Incendio",2,"E","COBERDES0")
   						else*/
   						{
   							insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "La suma asegurada no puede se menor al 20 % de las otras Incendio" ,2,"N","COBERDES0",document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)				
   							if(itemActualEditado)
	   							insertarPopUpFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "La suma asegurada no puede se menor al 20 % de las otras Incendio" ,2,"N","COBERDES0")
	   					}
	   		}
	 		
	 	}else
	 	{
	 		if (sumaCompara!=0 && sumaAseg100< Number(sumaCompara*0.35)) 
	 		
	   		{	// GED 27-12-2010 SE CORRIGE ERROR EN VALIDACION
	   			//if( document.all.COBCHECK[ _indiceCobertura("cobercod","100")].disabled == false)	   			
	   			if( document.all.COBCHECK[ _indiceCobertura("cobercod","100")].checked == true)	   			
	   				/*if(itemActualEditado) 
	   					insertarError( "", "SUMAASEG0","La suma asegurada no puede se menor al 35 % de las otras Incendio",2,"E","COBERDES0")
   					else*/
   					{
   						insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "La suma asegurada no puede se menor al 35 % de las otras Incendio" ,2,"N","COBERDES0",document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
   						if(itemActualEditado)
	   						insertarPopUpFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "La suma asegurada no puede se menor al 35 % de las otras Incendio" ,2,"N","COBERDES0")
	   				}
	   		}
	   	}
	   	
	   /*****************************************/	
	   
	   	var indice302 =  _indiceCobertura("cobercod","302")
	   	var sumaAseg302=Number(document.getElementById("SUMAASEG" + indice302).value)
	   
	  	 if ( sumaAseg302> Number(sumaAseg100*0.5)) 
	  	 
	   		{	// GED 27-12-2010 SE CORRIGE ERROR EN VALIDACION
	   			//if( document.all.COBCHECK[indice302 ].disabled == false)	   			
	   			if( document.all.COBCHECK[indice302 ].checked == true)
		   			/*if(itemActualEditado) 
		   				insertarError( "", "SUMAASEG"+ indice302,"La suma asegurada no puede superar al 50 % de Incendio Edificio",2,"E","COBERDES" + indice302)
		   			else*/
		   			{
		   				insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "La suma asegurada no puede superar al 50 % de Incendio Edificio" ,2,"N","COBERDES" + indice302,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)				
   						if(itemActualEditado)
	   						insertarPopUpFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value, "", "La suma asegurada no puede superar al 50 % de Incendio Edificio" ,2,"N","COBERDES" + indice302)
  					 }
	   		}
	   	/*****************************************/	
	 	for(var x=0; x<cantidadCoberturas; x++)
   		{
	   		document.getElementById("COBERDES" + x).style.color='black';
	   		
	   		//16/06/2011 LR - La cobertura checkeada tambien debe estar visible o habilitada.-
				varCOBERCOD = document.all.COBCHECK[x].cobercod;
	   		if(document.all.COBCHECK[x].checked && document.getElementById("FILA"+ varCOBERCOD).style.display!='none')
	   		{
		   		var sumaAseg=Number(document.getElementById("SUMAASEG" + x).value)
	   			var topeMinimo=Number(topeMinimoModulo(document.all.COBCHECK[x].cobercod,moduloActual))
	   			var topeMaximo=Number(topeMaximoModulo(document.all.COBCHECK[x].cobercod,moduloActual))
				
				//Se agrega control de modulo cuando es Independiente de la cobertura principal
				var tipoModulo = document.all.COBCHECK[x].detModulo
				var cantModulos= Number(mayorModulo(document.all.COBCHECK[x].cobercod))	
				
				if (tipoModulo == "I")
					topeMaximo = Number(topeMaximoModulo(document.all.COBCHECK[x].cobercod,cantModulos));
	   			//Hasta aca control
				
	   			if ( sumaAseg=="" || sumaAseg=="0")	
	   				{   			
	   				if(itemActualEditado) 	 insertarError( "","SUMAASEG" + x,"Debe completar la Suma Asegurada ",2,"E","COBERDES" +x)
	   				
	   				//break
	   				}
		   			//if(document.getElementById("SUMAASEG" + x).disabled == false)
		   			//ver que pasa cuando no edito
	   			
	   			    	if (siCambiaSuma(document.getElementById("SUMAASEG" + x),true))
	   			    	{
	   			     		//alert('Adriana-tope maximo1='+topeMaximo)
							insertarFueraNorma(document.getElementById("OTID").value  ,document.getElementById("localidadRiesgo").value , "SUMAASEG" + x,"Suma Asegurada debe ser menor a "+  formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"N","COBERDES" +x,document.getElementById("spCertipolAnt").innerText+"-"+ document.getElementById("spCertiannAnt").innerText+"-"+document.getElementById("spCertisecAnt").innerText)
	   					if(itemActualEditado) {
						    //alert('Adriana-tope maximo2='+topeMaximo)
	   						insertarPopUpFueraNorma(document.getElementById("OTID").value, document.getElementById("localidadRiesgo").value , "SUMAASEG" + x,"Suma Asegurada debe ser menor a "+  formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"N","COBERDES" +x)		   	
   				  		}
   				  	}
   				  	
   					
	   			     		
   				  
   				  	
	   			
   			}
   		
   		}
   	}
   
	var varTablaErrores;	
	varTablaErrores = oXMLErrores.transformNode(varXslTablaErrores);
	
	if (document.getElementById("SWSINIES").value == "S" && document.getElementById("TIPOOPER").value == "R"){
		if (oXMLFueraNorma.selectNodes("//ERRORES/SOLAPA").length!=0){
			if (!flagPoseeSiniestralidad){
				if(oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='0']/ERROR").length==1){					
					if(oXMLFueraNorma.selectSingleNode("ERRORES/SOLAPA[@nro='0']/ERROR/CAMPO").text!="SW"){
						flagPoseeSiniestralidad = true	
						insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
					}	
				}
				else{
					flagPoseeSiniestralidad = true	
					insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
				}	
			}
		}
	}	
	
	
	document.getElementById("xmlfueraNorma").value = oXMLFueraNorma.xml		
	varTablaFueraNorma = oXMLFueraNorma.transformNode(varXslFueraNorma);
		
	varPopUpFueraNorma = oXMLPopUpFueraNorma.transformNode(varXslFueraNorma);
	
	if (varTablaErrores!="" ) 	
	{
		document.getElementById("divTablaErrores").innerHTML = varTablaErrores;
		quitarItemDeTablaError(document.getElementById("varItemActual").value)
		return false
	}
	
	var oXMLItems = oXMLErrores.selectNodes("//ERROR[NROITEM='"+document.getElementById("localidadRiesgo").value+"']") 
	
    	if (varPopUpFueraNorma!="") 	
		{	
			openPopUpFueraNorma()			
		}	
	
	if (varTablaFueraNorma !="") fncMuestraFueraNorma()
		
	if(varTablaErrores=="" && varPopUpFueraNorma=="") 
	 	if(getItemFueraManual(document.getElementById("OTID").value))
	 	{	
			agregarItemXMLOT(document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value,"FN" );
		
		}
		else
 			agregarItemXMLOT(document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value,"N" );

return true



}
//*********************
function continuarFueraNorma()

{	
		fncMuestraFueraNorma()
		ocultarPopUp2('confirma_FueraNorma')	
		agregarItemXMLOT(document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value,"FN" );
		
		pintarNegroIndividual()		
		
		
		return true
	
	
	}
	
function ocultarPopUpFueraNorma()

{
	
	quitarItemDeTablaError(document.getElementById("varItemActual").value)
	ocultarPopUp2('confirma_FueraNorma')		
}

function validarDomiciliosItems()
{
		oXMLItems = new ActiveXObject('MSXML2.DOMDocument');        
      		oXMLItems.async = false;       
      		oXMLItems.loadXML(document.getElementById("varXmlItems").value); 
      
 		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+document.getElementById("varXmlItems").innerHTML+"</textarea>")
	      		
      		var oXMLRegItems = oXMLItems.selectNodes("//ITEM");
      		var varCantItems = oXMLRegItems.length;      		
		var resul= validarFormulario('xmlValidaDomiciliosItem', '//DOMICILIO', 'XSLTablaErrores', false,false,99)
		//alert(varCantItems)
		if(document.getElementById("CBO_PROVINCIA").value=="1" && resul)
		{	//TODO: 
			//flagPuedeCambiarSolapa=false
			
			var callejeroOK =false
			for( var x=1; x<=varCantItems;x++)				
			{			
				callejeroOK = fncCargaParamBuscaLoc("CBO_PROVINCIA","", "codPostalRiesgoItem_"+x,"calleRiesgoItem_"+x,"nroCalleRiesgoItem_"+x,6)			  	
				if(!callejeroOK)
					{					
					break
					}
				
			}
		}
		return resul


}




function validarDatosFormulario(pValidarObligatorios)
{
	if(tabActual==1 && document.getElementById("RAMOPCODSQL").value=="ICB1"){
		document.getElementById("varXmlItems").value="<ITEMS/>"	
		if(tabClick > tabActual && document.getElementById("divSeccionCoberturas").innerHTML!=""){
			document.getElementById("PROVICOD").value=document.getElementById("CBO_PROVINCIA").value
			document.getElementById("DOMICPOB").value=document.getElementById("localidadRiesgo").value
			document.getElementById("CPOSDOM").value=document.getElementById("codPostalRiesgo").value	
			document.getElementById("CodigoZonaICOBB").value=document.getElementById("CODIGOZONA").value
			document.getElementById("verZonaICOBB").value=document.getElementById("verZona").innerText
			document.getElementById("SUMAASEGTOTICOBB").value=document.getElementById("SUMAASEGTOTAL").value
		}	
		
		agregarItemXMLOT(document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value,"FN" );		
		validaInsertaItemRiesgo()
	}
	//Valida los datos del formulario
	var formValidado=false
	formValidado = validarFormulario('xmlDefinicionCamposForm', '//CAMPOS', 'XSLTablaErrores', pValidarObligatorios,false,99);
	
	if (formValidado && tabActual==1) 
	{ 		
		formValidado=validarItemsVacios()
	}
	
	if(formValidado && tabActual>=5 &&  mvarItemsParseados )
		{

			formValidado=validarDomiciliosItems()
			
		
		}	
		
			
		if(!formValidado) alert_customizado("Para continuar, es necesario que Usted complete como m�nimo los datos marcados en rojo.");
	
	
	return formValidado
	
}


function grabarCotizacionIrSolicitud()
{
	
    
    
	datosCompletos=true
	 document.getElementById("spanTextoEtiqSolapa_4").disabled=false   
	 mostrarTab('divDatosCliente','','#9CBACE','');   
	   aValidarSolapas[3]="S"
}
/************************************************************************************/
function enviarSolicitud()
{
	//02/06/2011 LR - Validar que para enviar o guardar la soli debe Elegir un Vendedor.-
	if (!document.getElementById("I_VENDEDOR") && document.getElementById("CANALHSBC").value == "S")
	{
		oXMLErrores.async = false;
		oXMLErrores.loadXML("<ERRORES></ERRORES>")
		document.getElementById("divTablaErrores").innerHTML = "";
		insertarError( "","legajoVendedor","Campo obligatorio, Nombre y Apellido Vendedor",2,"E","")
		muestraTablaErrores();
		return;
	}
	
	 //Adriana 22-8-12   NUEVO PLATAFORMISTA
        //Validar que se ingresen apellido y nombre del plataformista nuevo
        if ((document.getElementById("APELLIDOVEND").value=='NUEVO' ) && (document.getElementById("NOMBREVEND").value=='PLATAFORMISTA' )&& (document.getElementById("CANALHSBC").value == "S"))
	{
		//alert("Adriana: Valida plataformista");
		if(document.getElementById("nombrePlat").value=="")
		{
			insertarError( "","nombrePlat","Campo obligatorio, Nombre del Plataformista",2,"E","")
			muestraTablaErrores();
			return;		
		}
		if(document.getElementById("apellidoPlat").value=="")
		{
			insertarError( "","apellidoPlat","Campo obligatorio, Apellido del Plataformista",2,"E","")
			muestraTablaErrores();
			return;		
		}			
		document.getElementById("NOMBREVEND").value = document.getElementById("nombrePlat").value;
		document.getElementById("APELLIDOVEND").value = document.getElementById("apellidoPlat").value;
	}
	aValidarSolapas[5]="S"
	aValidarSolapas[6]="S"
	 
 	var res= validarDatosFormulario("true")
	//se graba provisoriamebnte la soli hasta que este la pantalla
	if (res) openPopUpEnviar()
}
/************************************************************************************/
function openPopUpEnviar()
{
		
	//Fondo de pantalla
	var ancho=document.body.clientWidth
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	// Fin Fondo
	document.getElementById('div_cierre').style.left  = 200
	document.getElementById('div_cierre').style.top  =   body.scrollTop+100
	document.getElementById('div_cierre').style.display = 'inline';
	
	//*************************	
	//	CUADRO CAMBIOS RENOVACION
	if (document.getElementById("TIPOOPER").value == "R")
		{
			
			
			fncRenovacionCambios("7")
			comparaCoberturasItemsRen()
			fncRenovacionCambiosObjAseg()
			
			
			var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
			oXMLValores.async = false; 
			
			 var oXSLConvierte = new ActiveXObject('MSXML2.DOMDocument');    
			oXSLConvierte.async = false;
			
			//oXSLConvierte.load( datosCotizacion.getCanalURL() + "Forms/XSL/CONVIERTE_XMLCAMBIOS.xsl")
			oXSLConvierte.loadXML(document.getElementById("CONVIERTE_XMLCAMBIOS").xml)
			
			 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
			oXSLValores.async = false;
			
			
				//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/CuadroCambiosRenovacion.xsl"))
				if(oXSLValores.loadXML(document.getElementById("CuadroCambiosRenovacion").xml))
				{	
			
				  var xslParam="<parametros>"+
				       "<parametro>"+
		                    "<nombre>urlCanal</nombre><valor>"+document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value +"</valor>"+
	                    "</parametro>"+		
					"</parametros>"			
					//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varXml.xml+"</textarea>")		
					
					var xmlTemp = parseaXSLParam(oXMLCambios.xml,oXSLConvierte,xslParam)	
					
				//	window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+ xmlTemp + "</textarea>")
					
					var varResultTabla = parseaXSLParam(xmlTemp,oXSLValores,xslParam)
					
					//window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+ document.getElementById("cuadroCambiosRenovacion").innerHTML + "</textarea>")
					
					document.getElementById("cuadroCambiosRenovacion").innerHTML = "";
					document.getElementById("cuadroCambiosEnvio").style.display='inline';					
					document.getElementById("cuadroCambiosEnvio").innerHTML = varResultTabla;	
			    }
		    
		
		}
	//************************
	
	
}

function aceptaEnviar()
{
	var opcionEnvio= document.getElementById('estado_solicitud').value
	if (opcionEnvio!="")
	{	
			if (grabarOperacion('S',false) == 0)
		{
			document.getElementById('div_cierre').style.display = 'none';
			document.getElementById('popUp_opacidad').style.display = 'none';
			
			alert_customizado("Error al guardar solicitud...");
			return false;
		}		
	//	grabarOperacion('S',false)  //poner Y luego
		if (opcionEnvio=="1")		
		
		{ 
		  document.getElementById("dFormularioCompleto").style.display="none"
		  document.getElementById("pantallaCierre").style.display='inline'
			//Apagar botones
			fncApagarBotones();
			//Prender boton nuevacotizacion
			document.getElementById('hNueCotConfirm').value='N';
			document.getElementById("divBtnNuevaCotizacion").style.display = "inline";
		}
		if (opcionEnvio=="2")
		{
		  document.getElementById("dFormularioCompleto").style.display="none" 		     
			document.getElementById("pantallaCierre").style.display='inline'
			//Apagar botones
			fncApagarBotones();
			//Prender boton nuevacotizacion
			document.getElementById('hNueCotConfirm').value='N';
			document.getElementById("divBtnNuevaCotizacion").style.display = "inline";
			
			if (fncIntegracion())
			{
				//HRF Def.136 12-10-2010
				if(document.getElementById("COTISOLI").value!="V")
					document.getElementById("divBtnImprimirCertificado").style.display='inline'
				
				document.getElementById("lblPantallaCierre").innerHTML =
					"Ingres� correctamente la propuesta a la Cia. con el n�mero "+
					document.getElementById("RAMOPCOD").value +'-'+
					fncSetLength(document.getElementById("AIS_POLIZANN").value,2) +'-'+
					fncSetLength(document.getElementById("AIS_POLIZSEC").value,6) 
					//GED 14-09-2010 DEFECT 74
					//+'-'+
					//fncSetLength(document.getElementById("AIS_CERTIPOL").value,4) +'-'+
					//fncSetLength(document.getElementById("AIS_CERTIANN").value,4) +'-'+
					//fncSetLength(document.getElementById("AIS_CERTISEC").value,6)
				//HRF Def.141 12-10-2010
				if(document.getElementById("COTISOLI").value=="V")
					document.getElementById("lblPantallaCierre").innerHTML += "<BR><BR>Recuerde que los datos definitivos se ver�n reflejados en la emisi�n de la p�liza."
				//jc icobb tabla para consultar ventas x intra
				if (document.getElementById("CANALHSBC").value == "S")
				{
					var mvarRequest = "<Request>"+
					"<DEFINICION>P_PROD_ICOBB_OPER_INSERT.xml</DEFINICION>"+
					"<SUCURSAL>"+document.getElementById("cbo_vendSucursal").value+"</SUCURSAL>"+
					"<RAMO>88</RAMO>"+
					"<NRO_CERTISEC>"+fncSetLength(document.getElementById("CERTISEC").value,6)+"</NRO_CERTISEC>"+
					"<DOCU_TIPO>1</DOCU_TIPO>" +
					"<DOCU_NUMERO>"+trim(document.getElementById("nroDocumentoCliente").value)+"</DOCU_NUMERO>"+
					"<NOMBRE_ASEGURADO>"+trim(document.getElementById("nombreCliente").value) + " " + trim(document.getElementById("apellidoCliente").value)+"</NOMBRE_ASEGURADO>"+
					"<ESTADO>E</ESTADO>"+
					"<LEGAJO_VEND>"+document.getElementById("legajoVendedor").value+"</LEGAJO_VEND>"+
					"<NOMBRE_VEND>"+trim(document.getElementById("APELLIDOVEND").value) + " " + trim(document.getElementById("NOMBREVEND").value)+"</NOMBRE_VEND>"+
					"</Request>"
					//alert(mvarRequest)
					var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVSQLGen')
					//alert(mvarResponse)
					}
				//fin jc
			}
			
			
		}
		
		
		document.getElementById('popUp_opacidad').style.display ='none';
		document.getElementById("div_cierre").style.display ='none';
		
		
	}
	
	else
	alert("Para poder Continuar, deber� seleccionar primero una opci�n del combo.")
}
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
function fncIntegracion()
{
	
	mvarNROSOLTMP = fncIntegracionGral()
	
	
	
	//return
	var oXMLItems = new ActiveXObject('MSXML2.DOMDocument');
			oXMLItems.async = false;
			oXMLItems.loadXML(document.getElementById("varXmlItems").value);
	//document.body.insertAdjacentHTML("beforeEnd", "Antes de enviar<textarea>"+oXMLItems.xml+"</textarea>")
		
	var varCantItems = oXMLItems.selectNodes('//ITEM').length;
	
	var mvarResulItem = "";
	var mvarFINPOLIZA = "";
	
	if(!mvarNROSOLTMP) return false
	
	if (mvarNROSOLTMP == "ERROR")
	{
		document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente.";
		return false;
	}	
	else 
	{ if(mvarNROSOLTMP)
		for(var x=0;x<varCantItems;x++)
		{	
			
			mvarFINPOLIZA = (x+1)>=varCantItems?"S":"N";
			if (!fncIntegracionItems(x, mvarNROSOLTMP, mvarFINPOLIZA))
			{
				//document.getElementById("lblPantallaCierre").innerHTML = "Error en la integracion de riesgos, Podr� recuperar la solicitud para intentar enviarla a la Compa��a nuevamente...";
				return false;
			}
		}
		
		
		
		if (fncCambioEstado())
			return true;
		else
		{
			document.getElementById("lblPantallaCierre").innerHTML = "Error en actualizaci�n de la solicitud, Podr� recuperar la solicitud para intentar enviarla a la Compa��a nuevamente...";
			return false;
		}
	}
}
/******************************************************************************************/
function fncCambioEstado()
{
	
	//jc se agrega icobb
	var wCerti ="";
	if (document.getElementById("RAMOPCODSQL").value=="ICB1"){
		wCerti = "CERT_PROV_ICB"
	}else{	
		wCerti = "CERT_PROV_ICO"
	}	

	//Impresos
	var wImpreso="";
	wImpreso = fncGenerarImpreso();
	wImpreso = wImpreso.replace(/&/g,"~~");
	var wDatosImpresos =
		"IMPREGRABAR=S&"+
		"TEMPL_COTI_P=&"+
		"IMPRE_COTI_P=&"+
		"TEMPL_COTI_C=&"+
		"IMPRE_COTI_C=&"+
		"TEMPL_SOLI=&"+
		"IMPRE_SOLI=&"+
		//"TEMPL_CERTI=CERT_PROV_ICO&"+
		"TEMPL_CERTI="+ wCerti +"&"+	
		"IMPRE_CERTI="+ wImpreso +"&"+
		"TEMPL_AUTDEB=&"+
		"IMPRE_AUTDEB=&"+
		"TEMPL_TARJ_CIR=&"+
		"IMPRE_TARJ_CIR=&"+
		"TEMPL_CLAU_SUB=&"+
		"IMPRE_CLAU_SUB="
	
	//Armo el XML de entrada para llamar al SP
	strData = "FUNCION=CAMBIARESTADO&";
	strData+= "RAMOPCOD=" + document.getElementById("RAMOPCODSQL").value + "&";
	strData+= "POLIZANN=0&";
	strData+= "POLIZSEC=0&";
	strData+= "CERTIPOL=" + document.getElementById("CERTIPOL").value + "&";
	strData+= "CERTIANN=0&";
	strData+= "CERTISEC=" + document.getElementById("CERTISEC").value + "&";
	strData+= "SUPLENUM=0&";
	strData+= "SITUCPOL=S&";
	strData+= "CIRCUREV=N&";
	strData+= "ANOTACION="+ document.getElementById("revision_comentario").value.replace(/&/g,"~~") + "&";
	strData+= "EJEC_PPLSOFT="+ document.getElementById("EJEC_PPLSOFT").value + "&";
	strData+= "EJEC_APENOM="+ document.getElementById("EJEC_APENOM").value + "&";
	strData+= "EJEC_EMAIL="+ document.getElementById("EJEC_EMAIL").value + "&";
	strData+= "AIS_POLIZANN="+ document.getElementById("AIS_POLIZANN").value +"&";
	strData+= "AIS_POLIZSEC="+ document.getElementById("AIS_POLIZSEC").value +"&";
	strData+= "AIS_CERTIPOL="+ document.getElementById("AIS_CERTIPOL").value +"&";
	strData+= "AIS_CERTIANN="+ document.getElementById("AIS_CERTIANN").value +"&";
	strData+= "AIS_CERTISEC="+ document.getElementById("AIS_CERTISEC").value +"&";
	strData+= "AIS_SUPLENUM="+ document.getElementById("AIS_SUPLENUM").value +"&";
	strData+= "USUARCOD=" + document.getElementById("LOGON_USER").value +"&";
	strData+= wDatosImpresos;
	
    //window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>Cambio Estado"+strData+"</textarea>")
	
	//Llamada
	xmlHTTPCotiza = new ActiveXObject("Msxml2.XMLHTTP");		
	xmlHTTPCotiza.open("POST","DefinicionFrameWork.asp", false);
	xmlHTTPCotiza.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHTTPCotiza.send(encodeURI(strData)); 	
	
	var mvarResult = xmlHTTPCotiza.responseText;
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarResult+"</textarea>")
	if (mvarResult=="0")
	{
		return true;
	} else {
		return false;
	}
}



/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
function fncIntegracionGral()
{

	//Variable con el XML de Datos
	
	var xmlDatosInt = "<Request>";
	   
	xmlDatosInt+= "<DEFINICION>2210_IntegracionGral.xml</DEFINICION>"
	xmlDatosInt+="<INTERSEC>NOBK</INTERSEC>"
	xmlDatosInt+="<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"
	xmlDatosInt+="<SECUECOD>"+ document.getElementById("fechahoy").value +"</SECUECOD>"
	
	//GED 09-11-2010 SE CORRIGE INTEGRACION NROSOLICITUD
	if(document.getElementById("TIPOOPER").value == "R")
		var nroSolicitud = fncSetLength(document.getElementById("CERTIPOL").value,4) + fncSetLength(document.getElementById("CERTISEC").value,6) + document.getElementById("RAMOPCOD").value;
	else
		var nroSolicitud = document.getElementById("nroSolicitud").value == ""? fncSetLength(document.getElementById("CERTIPOL").value,4) + fncSetLength(document.getElementById("CERTISEC").value,6) + document.getElementById("RAMOPCOD").value:  document.getElementById("nroSolicitud").value.toUpperCase();
	
	xmlDatosInt+="<SOLICITU>"+ nroSolicitud +"</SOLICITU>"	
	
	//xmlDatosInt+="<SOLICITU>"+  fncSetLength(document.getElementById("CERTIPOL").value,4) + fncSetLength(document.getElementById("CERTISEC").value,6) + fncSetLength(document.getElementById("RAMOPCOD").value,4) +"</SOLICITU>"	
	
	xmlDatosInt+="<FESOLANN>"+document.getElementById("fechaHoy").value.substr(0,4)+"</FESOLANN>"
	xmlDatosInt+="<FESOLMES>"+document.getElementById("fechaHoy").value.substr(4,2)+"</FESOLMES>"
	xmlDatosInt+="<FESOLDIA>"+document.getElementById("fechaHoy").value.substr(6,2)+"</FESOLDIA>"
	xmlDatosInt+="<EFECTANN>"+parteFecha(document.getElementById("vigenciaDesdeSoli").value,"DD/MM/AAAA","AAAA")+"</EFECTANN>"
	xmlDatosInt+="<EFECTMES>"+parteFecha(document.getElementById("vigenciaDesdeSoli").value,"DD/MM/AAAA","MM")+"</EFECTMES>"
	xmlDatosInt+="<EFECTDIA>"+parteFecha(document.getElementById("vigenciaDesdeSoli").value,"DD/MM/AAAA","DD")+"</EFECTDIA>"
	xmlDatosInt+="<VENCIANN>"+parteFecha(document.getElementById("vigenciaHastaSoli").value,"DD/MM/AAAA","AAAA")+"</VENCIANN>"
	xmlDatosInt+="<VENCIMES>"+ parteFecha(document.getElementById("vigenciaHastaSoli").value,"DD/MM/AAAA","MM")+"</VENCIMES>"
	xmlDatosInt+="<VENCIDIA>"+ parteFecha(document.getElementById("vigenciaHastaSoli").value,"DD/MM/AAAA","DD")+"</VENCIDIA>"
	xmlDatosInt+="<AGENTCLAPR>"+document.getElementById("I_AGENTE").value.split("|")[1]+"</AGENTCLAPR>"
	xmlDatosInt+="<AGENTCODPR>"+document.getElementById("I_AGENTE").value.split("|")[0] +"</AGENTCODPR>"
	
	//GED 07-09-2010 - SE AGREGA PR2 
	xmlDatosInt+="<AGENTCLAPR2>"+document.getElementById("I_PRODUCTOR2").value.split("|")[1]+"</AGENTCLAPR2>"
	xmlDatosInt+="<AGENTCODPR2>"+document.getElementById("I_PRODUCTOR2").value.split("|")[0] +"</AGENTCODPR2>"
	xmlDatosInt+="<AGENTNOMPR2>"+document.getElementById("I_PRODUCTOR2").value.split("|")[2] +"</AGENTNOMPR2>"
	//Adriana 12-04-2011 - Se agregan convenio y comision del PR2
	var CONVECODPR2 = document.getElementById("I_PRODUCTOR2").value.split("|")[3]?document.getElementById("I_PRODUCTOR2").value.split("|")[3]:""
	xmlDatosInt+="<CONVECODPR2>"+CONVECODPR2 +"</CONVECODPR2>"
	
	//xmlDatosInt+="<CONVECODPR2>"+document.getElementById("I_PRODUCTOR2").value.split("|")[3] +"</CONVECODPR2>"
	wvarCOMISIONPR2 = document.getElementById("I_PRODUCTOR2").value.split("|")[4]?document.getElementById("I_PRODUCTOR2").value.split("|")[4]:0;
	xmlDatosInt+="<COMISIONPR2>"+ wvarCOMISIONPR2 +"</COMISIONPR2>"
	wvarCHGCOMCVPR2 = document.getElementById("I_PRODUCTOR2").value.split("|")[5]?document.getElementById("I_PRODUCTOR2").value.split("|")[5]:"N";
	xmlDatosInt+="<CHGCOMCVPR2>"+ wvarCHGCOMCVPR2 +"</CHGCOMCVPR2>"
	
	xmlDatosInt+="<AGENTCLAOR>"+document.getElementById("I_ORGANIZADOR").value.split("|")[1] +"</AGENTCLAOR>"
	xmlDatosInt+="<AGENTCODOR>"+document.getElementById("I_ORGANIZADOR").value.split("|")[0] +"</AGENTCODOR>"
	xmlDatosInt+="<COMISIONPR1>"+document.getElementById("COMISION_PROD").value+"</COMISIONPR1>"
	xmlDatosInt+="<COMISIONPR2>0</COMISIONPR2>"
	xmlDatosInt+="<COMISIONOR>"+document.getElementById("COMISION_ORG").value+"</COMISIONOR>"
	
	//----------------------------------------------------------------------------------------------------------//
	//10/01/2011 LR - Se agregan marcas para convenios Prod y Organizador
	var vCHGCOMCVPR1 = 'N';
	var vCHGCOMCVOR = 'N';
		
	//*CONVECOD*= corresponde al c�digo de convenio
	if(document.getElementById("TIPOOPER").value == "R")
	{
		//Se setean los valores que vinieron del AIS para cuando es Fuera de Convenio
		oXMLRenovaPoliCol = new ActiveXObject('MSXML2.DOMDocument');        
		oXMLRenovaPoliCol.async = false;       
  	oXMLRenovaPoliCol.loadXML(document.getElementById("xmlRenovaPoliCol").innerHTML);
  	
  	var vConvProdAnt = oXMLRenovaPoliCol.selectSingleNode("//CAMPOS/CONVECODPR1").text;
  	var vConvOrgAnt = oXMLRenovaPoliCol.selectSingleNode("//CAMPOS/CONVECODOR").text;
	} else {
		var vConvProdAnt = "";
  	var vConvOrgAnt = "";
	}
	
	var vConvProd=document.getElementById("convenioProductor").value.split("|")[0]!="-2"?document.getElementById("convenioProductor").value.split("|")[0]:vConvProdAnt;
	var vConvOrg=document.getElementById("convenioOrganizador").value.split("|")[0]!="-2"?document.getElementById("convenioOrganizador").value.split("|")[0]:vConvOrgAnt;
	
	//*CHGCOMCV*=S/N seg�n el usuario cambie o no la comisi�n provista por el convenio.
	//Si vine (M)anual [Fuera de Norma] se toma como q cambio
	if(document.getElementById("convenioProductor").value.split("|")[0] != "-2")
	{
		if(Number(document.getElementById("convenioProductor").value.split("|")[3]) !=
				Number(document.getElementById("COMISION_PROD").value))
					vCHGCOMCVPR1 = 'S';
	}
	else
		vCHGCOMCVPR1 = 'S';			
	
	if(document.getElementById("convenioOrganizador").value.split("|")[0] != "-2")
	{
		if(Number(document.getElementById("convenioOrganizador").value.split("|")[3]) !=
				Number(document.getElementById("COMISION_ORG").value))
					vCHGCOMCVOR = 'S';
	}
	else
		vCHGCOMCVOR = 'S';
	
	xmlDatosInt+="<CONVECODPR1>"+ vConvProd +"</CONVECODPR1>"
	xmlDatosInt+="<CHGCOMCVPR1>"+ vCHGCOMCVPR1 +"</CHGCOMCVPR1>"
	
	//adriana 12-4-11
	//xmlDatosInt+="<CONVECODPR2></CONVECODPR2>"
	//xmlDatosInt+="<CHGCOMCVPR2>N</CHGCOMCVPR2>"
	
	xmlDatosInt+="<CONVECODOR>"+ vConvOrg +"</CONVECODOR>"
	xmlDatosInt+="<CHGCOMCVOR>"+ vCHGCOMCVOR +"</CHGCOMCVOR>"
	//----------------------------------------------------------------------------------------------------------//
	
	xmlDatosInt+="<PRIMA>"+ document.getElementById("PRIMAIMP").value +"</PRIMA>"
	
	//GED 13-09-2010 INCIDENTE 179	
	//var RECADMIN= Number(document.getElementById("RECARGO").value) + Number(document.getElementById("planPago").value.split("|")[1])
	
	//LR 05/01/2011 Enviar los recargos Administrativo y Financiero(RECADMIN/RECFINAN)

	//var RECADMIN = Number(document.getElementById("RECARGO").value)

	//MAG INCIDENTE  RECARGO FINANCIERO
	var RECADMIN
	if (document.getElementById("ESRECFINAN").value == "S")
	    RECADMIN = Number(document.getElementById("RECARGO").value) + Number(document.getElementById("planPago").value.split("|")[1])
	else
	    RECADMIN = Number(document.getElementById("RECARGO").value) //Se toma Rec Finan = 0
	    
	if( RECADMIN<0 )
	    RECADMIN = 0;
	if (fncEsICB1())
	    xmlDatosInt += "<RECARGOADM>" + document.getElementById("PORCCOMERICOBB").value + "</RECARGOADM>";
	else
	    xmlDatosInt+="<RECARGOADM>"+RECADMIN+"</RECARGOADM>";
	//xmlDatosInt+="<RECARGOFIN>"+ Number(document.getElementById("planPago").value.split("|")[1]) +"</RECARGOFIN>"
	xmlDatosInt += "<RECARGOFIN>0</RECARGOFIN>"
	xmlDatosInt+="<DERPOIMP>"+ document.getElementById("DEREMIMP").value +"</DERPOIMP>"
	xmlDatosInt+="<PLANPCOD>"+document.getElementById("planPago").value.split("|")[0]+"</PLANPCOD>"
	xmlDatosInt+="<COBROCOD>"+document.getElementById("formaPago").value.split("|")[0]+"</COBROCOD>"
	//Guardar datos de cuenta
	var CUENNUME = '';
	var VENCIANN = 0;
	var VENCIMES = 0;
	var COBROTIP = document.getElementById("formaPago").value.split("|")[1]
	if (document.getElementById("seccion_Tarjeta").style.display!="none")
	{	CUENNUME = document.getElementById("numTarjeta").value;
		VENCIANN = document.getElementById("tarvtoann").value;
		VENCIMES = document.getElementById("tarvtomes").value;
		COBROTIP = document.getElementById("cbo_nomTarjeta").value.split("|")[1]
	}
	else if (document.getElementById("seccion_PagoCuenta").style.display!="none")
	{	CUENNUME = document.getElementById("numCuentaDC").value; }
	else if (document.getElementById("divNroCBU").style.display!="none")
	{	CUENNUME = document.getElementById("numCBU").value; }
	
	xmlDatosInt+="<COBROTIP>"+ COBROTIP +"</COBROTIP>"
	xmlDatosInt+="<CUENNUME>"+ CUENNUME +"</CUENNUME>"
	xmlDatosInt+="<VENCIANNT>"+ VENCIANN +"</VENCIANNT>"
	xmlDatosInt+="<VENCIMEST>"+ VENCIMES +"</VENCIMEST>"
	xmlDatosInt += "<MONEDA>" + document.getElementById("CODMONEDA").value + "</MONEDA>"
	if (fncEsICB1())
	    xmlDatosInt += "<ACTIVPPAL>" + fncSetLength(document.getElementById("cboActividadICOBB").value,6,'0','R') + "</ACTIVPPAL>"	//para icoBB cambia
	else
	    xmlDatosInt+="<ACTIVPPAL>"+ document.getElementById("cboActividadComercio").value.split("|")[0]  +"</ACTIVPPAL>"	//Optativa, para ico va seguro
	
	if(document.getElementById("TIPOOPER").value == "R")
	{
		var vTIPOOPER = "RP";
		var vCIAASCODANT = "0001";
		var vRAMOPCODANT = document.getElementById("RAMOPCOD").value;
		var vPOLIZANNANT = document.getElementById("POLIZANNANT").value;
		var vPOLIZSECANT = document.getElementById("POLIZSECANT").value;
	}	else {
		var vTIPOOPER = "AP";
		var vCIAASCODANT = "";
		var vRAMOPCODANT = "";
		var vPOLIZANNANT = "0";
		var vPOLIZSECANT = "0";
	}

	xmlDatosInt+="<TIPOOPER>"+ vTIPOOPER +"</TIPOOPER>"	//AP(Alta)	RP(Renovacion)
	xmlDatosInt+="<MARCOPER>"+ fncSetLength(document.getElementById("MARCOPER").value,5,' ','L') +"</MARCOPER>"
	xmlDatosInt+="<CIAASCODANT>"+ vCIAASCODANT +"</CIAASCODANT>"	//Mandar la poliza q estamos renovando
	xmlDatosInt+="<RAMOPCODANT>"+ vRAMOPCODANT +"</RAMOPCODANT>"	//
	xmlDatosInt+="<POLIZANNANT>"+ vPOLIZANNANT +"</POLIZANNANT>"	//
	xmlDatosInt+="<POLIZSECANT>"+ vPOLIZSECANT +"</POLIZSECANT>"	//
	xmlDatosInt+="<CAUSAEND></CAUSAEND>"
	xmlDatosInt+="<ORIOPERA>OV</ORIOPERA>"
	//GED 19-8-2010 SI ES CIRCUITO DE REVISION
	if(document.getElementById("COTISOLI").value=="V")
		xmlDatosInt+="<REVISION>S</REVISION>"
	
	//CLIENTES
	xmlDatosInt+="<CLIENTES>";
	//PERSONA
	var APELLIDO =trim(document.getElementById("apellidoCliente").value)
	if (APELLIDO.length>20)
	{
		var APELLIDO1= APELLIDO.substr(0,20);
		var APELLIDO2 = APELLIDO.substr(20);
	}	else	{
		var APELLIDO1= APELLIDO;
		var APELLIDO2 = "";
	}
	var PERSOTIP = document.getElementById("tipoPersona").value=='00'?'F':'J';
	var CLIENTIP = document.getElementById("tipoPersona").value=="-1"?"0":document.getElementById("tipoPersona").value;
	var FECNACIM = document.getElementById("fechaNacimientoClienteSoli").value.length<8?"0/0/0":document.getElementById("fechaNacimientoClienteSoli").value;
	//GED 18-8-2010 - MANDAR ESPACIO EN VEZ DE CERO EN SEXO Y EST CIVIL
	var CLIENSEX = document.getElementById("sexoCliente").value=="-1"?" ":document.getElementById("sexoCliente").value;
	var ESTCIVIL = document.getElementById("estadoCivilCliente").value=="-1"?" ":document.getElementById("estadoCivilCliente").value;
	var PAISNACI = document.getElementById("paisNacimientoCliente").value=="-1"?"0":document.getElementById("paisNacimientoCliente").value;
	//Domicilio
	var PROVICOD = document.getElementById("provinciaDomicilio").value!="0"?document.getElementById("provinciaDomicilio").value:"0";
	var DOMICPOB = document.getElementById("localidadDomicilio").value;
	//C�digo Postal
		if (document.getElementById("TIPOOPER").value == "R")
		{
			if (fncEditarDomicilioChk())
			{
				oXMLRenovaDomiCorresp = new ActiveXObject('MSXML2.DOMDocument');        
				oXMLRenovaDomiCorresp.async = false;       
			  oXMLRenovaDomiCorresp.loadXML(document.getElementById("xmlRenovaDomiCorresp").innerHTML);
			  var DOMICCPO = oXMLRenovaDomiCorresp.selectSingleNode("//CAMPOS/CPACODPO").text;
			}	else {
				var DOMICCPO = trim(document.getElementById("codPosDomicilio").value)!=""?trim(document.getElementById("codPosDomicilio").value):"0";
			}
		}	else {
			var DOMICCPO = trim(document.getElementById("codPosDomicilio").value)!=""?trim(document.getElementById("codPosDomicilio").value):"0";
		}
	//Cuenta
	var COBROTIP = document.getElementById("formaPago").value.split("|")[1];
	var BANCOCOD = document.getElementById("BANCOCOD").value==""?"0":document.getElementById("BANCOCOD").value;
	var SUCURCOD = 0;
	var CUENNUME = '';
	var TARJECOD = 0;
	var VENCIANN = 0;
	var VENCIMES = 0;
	var VENCIDIA = 0;
	if (document.getElementById("seccion_Tarjeta").style.display!="none")
	{	COBROTIP = document.getElementById("cbo_nomTarjeta").selectedIndex==0?COBROTIP:document.getElementById("cbo_nomTarjeta").value.split("|")[1];
		TARJECOD = document.getElementById("cbo_nomTarjeta").selectedIndex==0?0:document.getElementById("cbo_nomTarjeta").value.split("|")[0];
		CUENNUME = document.getElementById("numTarjeta").value;
		VENCIANN = document.getElementById("tarvtoann").value;
		VENCIMES = document.getElementById("tarvtomes").value;
		VENCIDIA = 1; }
	else if (document.getElementById("seccion_PagoCuenta").style.display!="none")
	{	SUCURCOD = document.getElementById("cboSucursalDC").selectedIndex==0?0:document.getElementById("cboSucursalDC").value;
		CUENNUME = document.getElementById("numCuentaDC").value; }
	else if (document.getElementById("divNroCBU").style.display!="none")
	{	CUENNUME = document.getElementById("numCBU").value; }
	
	var mvarIIBB= document.getElementById("IngresosBrutos").value=="-1"?"0":document.getElementById("IngresosBrutos").value
	var CUITL = document.getElementById("nroCuit").value==""?"0":document.getElementById("nroCuit").value
	
	xmlDatosInt+="<CLIENTE>";
			xmlDatosInt+="<TIPOCLIE>P</TIPOCLIE>"
			xmlDatosInt+="<CLIENTIP>"+ CLIENTIP +"</CLIENTIP>"
			//10/11/2010 LR, En todos los productos deja cargar los 5 caracteres especiales y pasar a la solicitud, pero cuando quiero enviar la solicitud sale un error
			xmlDatosInt+="<CLIENNOM><![CDATA["+ trim(document.getElementById("nombreCliente").value.toUpperCase()) +"]]></CLIENNOM>"
			xmlDatosInt+="<CLIENAP1><![CDATA["+ APELLIDO1.toUpperCase() +"]]></CLIENAP1>"
			xmlDatosInt+="<CLIENAP2><![CDATA["+ APELLIDO2.toUpperCase() +"]]></CLIENAP2>"
			xmlDatosInt+="<CLIENSEX>"+ CLIENSEX +"</CLIENSEX>"
			xmlDatosInt+="<NACIMANN>"+ FECNACIM.split("/")[2] +"</NACIMANN>"
			xmlDatosInt+="<NACIMMES>"+ FECNACIM.split("/")[1] +"</NACIMMES>"
			xmlDatosInt+="<NACIMDIA>"+ FECNACIM.split("/")[0] +"</NACIMDIA>"
			xmlDatosInt+="<PAISSCODNAC>"+ PAISNACI +"</PAISSCODNAC>"
			xmlDatosInt+="<CLIENEST>"+ ESTCIVIL +"</CLIENEST>"
			xmlDatosInt+="<DOCUMTIP>"+ document.getElementById("tipoDocCliente").value +"</DOCUMTIP>"
			xmlDatosInt+="<DOCUMDAT>"+ trim(document.getElementById("nroDocumentoCliente").value) +"</DOCUMDAT>"
			xmlDatosInt+="<CLIENIVA>"+document.getElementById("IVA").value+"</CLIENIVA>"
			xmlDatosInt+="<CUITL>"+ CUITL +"</CUITL>"
			xmlDatosInt+="<CLIEIBTP>"+ mvarIIBB +"</CLIEIBTP>"
			xmlDatosInt+="<CLIEIBNU>0</CLIEIBNU>"
			xmlDatosInt+="<DOMICDOM>"+ trim(document.getElementById("calleDomicilio").value) +"</DOMICDOM>"
			xmlDatosInt+="<DOMICDNU>"+ trim(document.getElementById("nroDomicilio").value) +"</DOMICDNU>"
			xmlDatosInt+="<DOMICPIS>"+ trim(document.getElementById("pisoDomicilio").value) +"</DOMICPIS>"
			xmlDatosInt+="<DOMICPTA>"+ trim(document.getElementById("dptoDomicilio").value) +"</DOMICPTA>"
			xmlDatosInt+="<DOMICCPO>"+ DOMICCPO +"</DOMICCPO>"
			xmlDatosInt+="<DOMICPOB>"+ DOMICPOB +"</DOMICPOB>"
			xmlDatosInt+="<PROVICOD>"+ PROVICOD +"</PROVICOD>"
			
			//GED defect nro 67 Pais debe ser 00
			//xmlDatosInt+="<PAISSCODDOM>"+ PAISNACI +"</PAISSCODDOM>"
			xmlDatosInt+="<PAISSCODDOM>00</PAISSCODDOM>"
			var codTelefono = document.getElementById("codTelefonoCliente").value != "" ? document.getElementById("codTelefonoCliente").value+ " " :document.getElementById("codTelefonoCliente").value
			xmlDatosInt+="<TELPPAL>"+ codTelefono + document.getElementById("telefonoCliente").value +"</TELPPAL>"
			xmlDatosInt+="<TELTRAB></TELTRAB>"
			xmlDatosInt+="<TELCELU></TELCELU>"
			xmlDatosInt+="<TELOTRO></TELOTRO>"
			xmlDatosInt+="<EMAIL>"+ document.getElementById("emailCliente").value +"</EMAIL>"
			xmlDatosInt+="<DOMICCPOBNACAML></DOMICCPOBNACAML>"
			xmlDatosInt+="<CATEGCLIEAML>0</CATEGCLIEAML>"
			xmlDatosInt+="<CLIENSEC>"+ document.getElementById("IDCLIENTE").value +"</CLIENSEC>"
		xmlDatosInt+="</CLIENTE>";
	//APODERADO
	var apoApellido =trim(document.getElementById("apoApellido").value)
	if (apoApellido.length>20)
	{	
		var apoApellido1= apoApellido.substr(0,20)
		var apoApellido2 = apoApellido.substr(20)
	}
	else
	{
		var apoApellido1= apoApellido
		var apoApellido2 = ""
	}
	//HRF Def.245 P.1 2010-11-04
	var apoCUITL = document.getElementById("apoCuit").value==""?"0":document.getElementById("apoCuit").value
	//var annHoy= document.getElementById("fechaHoy").value.substr(0,4)
	//var mesHoy=document.getElementById("fechaHoy").value.substr(4,2)
	//var diaHoy= document.getElementById("fechaHoy").value.substr(6,2)
	//var persoTip= document.getElementById("cboApoTipDoc").value!='4'?'F':'J'
	//var apoPROVICOD =document.getElementById("apoProvinciaDomicilio").value!="0"?document.getElementById("apoProvinciaDomicilio").value:"0"
	var apoDOMICCPO = trim(document.getElementById("apoCodPosDomicilio").value)!=""?trim(document.getElementById("apoCodPosDomicilio").value):"0"
	var apoDOMICPOB = document.getElementById("apoLocalidadDomicilio").value
	var apoTipoPersona= document.getElementById("cboApoTipDoc").value!="4"?"00":"15"
	var apoFechaNacim =  document.getElementById("apoFechaNacimiento").value
	//GED 18-8-2010 - MANDAR ESPACIO EN VEZ DE CERO EN SEXO Y EST CIVIL
	var apoCliensex =  document.getElementById("apoSexo").value=="-1"?" ":document.getElementById("apoSexo").value
	var apoEstadoCivil =  document.getElementById("apoEstadoCivil").value=="-1"?" ":document.getElementById("apoEstadoCivil").value
	var apoPaisNacimiento = document.getElementById("apoPais").value=="-1"?"0":document.getElementById("apoPais").value
	var apoTipDoc = document.getElementById("cboApoTipDoc").value=="-1"?"0":document.getElementById("cboApoTipDoc").value
	var apoPROVICOD = document.getElementById("apoProvinciaDomicilio").value=="-1"?"0":document.getElementById("apoProvinciaDomicilio").value
	
	xmlDatosInt+="<CLIENTE>";
	if(document.getElementById("tieneApoderado").value == "S")
	{
			xmlDatosInt+="<TIPOCLIE>O</TIPOCLIE>"
			xmlDatosInt+="<CLIENTIP>"+ apoTipoPersona +"</CLIENTIP>"
	}
	else
	{
			xmlDatosInt+="<TIPOCLIE></TIPOCLIE>"
			xmlDatosInt+="<CLIENTIP></CLIENTIP>"
	}
			xmlDatosInt+="<CLIENNOM>"+ trim(document.getElementById("apoNombre").value.toUpperCase()) +"</CLIENNOM>"
			xmlDatosInt+="<CLIENAP1>"+ apoApellido1.toUpperCase() +"</CLIENAP1>"
			xmlDatosInt+="<CLIENAP2>"+ apoApellido2.toUpperCase() +"</CLIENAP2>"
			xmlDatosInt+="<CLIENSEX>"+ apoCliensex +"</CLIENSEX>"
			xmlDatosInt+="<NACIMANN>"+ parteFecha(apoFechaNacim,"DD/MM/AAAA","AAAA") +"</NACIMANN>"
			xmlDatosInt+="<NACIMMES>"+ parteFecha(apoFechaNacim,"DD/MM/AAAA","MM") +"</NACIMMES>"
			xmlDatosInt+="<NACIMDIA>"+ parteFecha(apoFechaNacim,"DD/MM/AAAA","DD") +"</NACIMDIA>"
			xmlDatosInt+="<PAISSCODNAC>"+ apoPaisNacimiento +"</PAISSCODNAC>"
			xmlDatosInt+="<CLIENEST>"+ apoEstadoCivil+"</CLIENEST>"
			xmlDatosInt+="<DOCUMTIP>"+ apoTipDoc +"</DOCUMTIP>"
			xmlDatosInt+="<DOCUMDAT>"+ trim(document.getElementById("apoNroDoc").value) +"</DOCUMDAT>"
			xmlDatosInt+="<CLIENIVA></CLIENIVA>"
			xmlDatosInt+="<CUITL>"+ apoCUITL +"</CUITL>"
			xmlDatosInt+="<CLIEIBTP></CLIEIBTP>"
			xmlDatosInt+="<CLIEIBNU>0</CLIEIBNU>"
			xmlDatosInt+="<DOMICDOM>"+ trim(document.getElementById("apoCalleDomicilio").value) +"</DOMICDOM>"
			xmlDatosInt+="<DOMICDNU>"+ trim(document.getElementById("apoNroDomicilio").value) +"</DOMICDNU>"
			xmlDatosInt+="<DOMICPIS>"+ trim(document.getElementById("apoPisoDomicilio").value) +"</DOMICPIS>"
			xmlDatosInt+="<DOMICPTA>"+ trim(document.getElementById("apoDptoDomicilio").value) +"</DOMICPTA>"
			xmlDatosInt+="<DOMICCPO>"+ apoDOMICCPO +"</DOMICCPO>"
			xmlDatosInt+="<DOMICPOB>"+ apoDOMICPOB +"</DOMICPOB>"
			xmlDatosInt+="<PROVICOD>"+ apoPROVICOD +"</PROVICOD>"
			
			//GED defect nro 67 Pais debe ser 00
			//xmlDatosInt+="<PAISSCODDOM>"+ PAISNACI +"</PAISSCODDOM>"
			xmlDatosInt+="<PAISSCODDOM>00</PAISSCODDOM>"
			//DEFECT 277
			var codTelefono = document.getElementById("apoCodTelefono").value != "" ? document.getElementById("apoCodTelefono").value+ " " :document.getElementById("apoCodTelefono").value
						
			xmlDatosInt+="<TELPPAL>"+ codTelefono+ document.getElementById("apoTelefono").value +"</TELPPAL>"
			xmlDatosInt+="<TELTRAB></TELTRAB>"
			xmlDatosInt+="<TELCELU></TELCELU>"
			xmlDatosInt+="<TELOTRO></TELOTRO>"
			xmlDatosInt+="<EMAIL>"+ document.getElementById("apoMail").value +"</EMAIL>"
			xmlDatosInt+="<DOMICCPOBNACAML></DOMICCPOBNACAML>"
			xmlDatosInt+="<CATEGCLIEAML>0</CATEGCLIEAML>"
			xmlDatosInt+="<CLIENSEC>"+ document.getElementById("CLIENSECAPO").value +"</CLIENSEC>"
		xmlDatosInt+="</CLIENTE>";
	//HRF 20100922 - Completan con blancos los que faltan (9 mas que antes)
	for(var y=0; y<11; y++)
	{
		xmlDatosInt+="<CLIENTE>";
			xmlDatosInt+="<TIPOCLIE></TIPOCLIE>"
			xmlDatosInt+="<CLIENTIP></CLIENTIP>"
			xmlDatosInt+="<CLIENNOM></CLIENNOM>"
			xmlDatosInt+="<CLIENAP1></CLIENAP1>"
			xmlDatosInt+="<CLIENAP2></CLIENAP2>"
			xmlDatosInt+="<CLIENSEX></CLIENSEX>"
			xmlDatosInt+="<NACIMANN>0</NACIMANN>"
			xmlDatosInt+="<NACIMMES>0</NACIMMES>"
			xmlDatosInt+="<NACIMDIA>0</NACIMDIA>"
			xmlDatosInt+="<PAISSCODNAC>0</PAISSCODNAC>"
			xmlDatosInt+="<CLIENEST></CLIENEST>"
			xmlDatosInt+="<DOCUMTIP>0</DOCUMTIP>"
			xmlDatosInt+="<DOCUMDAT></DOCUMDAT>"
			xmlDatosInt+="<CLIENIVA></CLIENIVA>"
			xmlDatosInt+="<CUITL>0</CUITL>"
			xmlDatosInt+="<CLIEIBTP></CLIEIBTP>"
			xmlDatosInt+="<CLIEIBNU>0</CLIEIBNU>"
			xmlDatosInt+="<DOMICDOM></DOMICDOM>"
			xmlDatosInt+="<DOMICDNU></DOMICDNU>"
			xmlDatosInt+="<DOMICPIS></DOMICPIS>"
			xmlDatosInt+="<DOMICPTA></DOMICPTA>"
			xmlDatosInt+="<DOMICCPO></DOMICCPO>"
			xmlDatosInt+="<DOMICPOB></DOMICPOB>"
			xmlDatosInt+="<PROVICOD>0</PROVICOD>"
			xmlDatosInt+="<PAISSCODDOM>0</PAISSCODDOM>"
			xmlDatosInt+="<TELPPAL></TELPPAL>"
			xmlDatosInt+="<TELTRAB></TELTRAB>"
			xmlDatosInt+="<TELCELU></TELCELU>"
			xmlDatosInt+="<TELOTRO></TELOTRO>"
			xmlDatosInt+="<EMAIL></EMAIL>"
			xmlDatosInt+="<DOMICCPOBNACAML></DOMICCPOBNACAML>"
			xmlDatosInt+="<CATEGCLIEAML>0</CATEGCLIEAML>"
			xmlDatosInt+="<CLIENSEC>0</CLIENSEC>"
		xmlDatosInt+="</CLIENTE>";
	}
	xmlDatosInt+="</CLIENTES>";
	
	xmlDatosInt+="<ANOTACIONES>";
	
	//PRIMA MINIMA ANOTACIONES	
	
	var txtAnotaI =trim(document.getElementById('observClausulas').value)	
	 
	var resulI = dividirAnotacion(txtAnotaI, "I", 600)
	
	var cantAnotI = resulI.cantidad
	document.getElementById("cantAnotacionesI").value = cantAnotI
	
	xmlDatosInt+= resulI.xml
	
	var txtAnotaF =trim(document.getElementById('textoPoliza').value)
	
	var resulF = dividirAnotacion(txtAnotaF, "F", 600)
	
	var cantAnotF = resulF.cantidad
	
	xmlDatosInt+= resulF.xml
	

	//'I'nterna
	/*if(trim(document.getElementById('observClausulas').value) != '')
	{
		var vTIPOANOTA = 'I'
		var vORDENTEXT = '1'
	}
	else
	{
		var vTIPOANOTA = ''
		var vORDENTEXT = '0'
	}
	xmlDatosInt+="<ANOTACION>";
		xmlDatosInt+="<TIPOANOTA>"+ vTIPOANOTA +"</TIPOANOTA>"
		xmlDatosInt+="<ORDENTEXT>"+ vORDENTEXT +"</ORDENTEXT>"
		xmlDatosInt+="<TEXTO><![CDATA["+ trim(document.getElementById('observClausulas').value) +"]]></TEXTO>"
	xmlDatosInt+="</ANOTACION>";
	//'F'rente de p�liza
	if(trim(document.getElementById('textoPoliza').value) != '')
	{
		vTIPOANOTA = 'F'
		vORDENTEXT = '1'
	}
	else
	{
		vTIPOANOTA = ''
		vORDENTEXT = '0'
	}
	xmlDatosInt+="<ANOTACION>";
		xmlDatosInt+="<TIPOANOTA>"+ vTIPOANOTA +"</TIPOANOTA>"
		xmlDatosInt+="<ORDENTEXT>"+ vORDENTEXT +"</ORDENTEXT>"
		xmlDatosInt+="<TEXTO><![CDATA["+ trim(document.getElementById('textoPoliza').value) +"]]></TEXTO>"
	xmlDatosInt+="</ANOTACION>";
	
	*/
	
	
	
	var restoAnotacion = Number(8-cantAnotI-cantAnotF)
	
	for(var y=0; y<restoAnotacion ; y++)
	{
		xmlDatosInt+="<ANOTACION>";
			xmlDatosInt+="<TIPOANOTA></TIPOANOTA>"
			xmlDatosInt+="<ORDENTEXT>0</ORDENTEXT>"
			xmlDatosInt+="<TEXTO></TEXTO>"
		xmlDatosInt+="</ANOTACION>";
	}
	xmlDatosInt+="</ANOTACIONES>";
	xmlDatosInt+="</Request>";	

	//window.document.body.insertAdjacentHTML("beforeEnd", "XML INTEGRACION<textarea>" + xmlDatosInt + "</textarea>")
	//return '650';
	
	//var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ') 
	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	if(oXMLValores.loadXML(llamadaMensajeMQ(xmlDatosInt.replace(/&/g,"~~"),"lbaw_GetConsultaMQ")))
	{//alert(oXMLValores.xml)
		if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text=="true")
		{ 
			if(oXMLValores.selectSingleNode("//Response/CAMPOS/MENSAJE").text =="" )
			{	
				return  oXMLValores.selectSingleNode("//NROSOLTMP").text
			}
			else
			{	
				document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente.<BR/>" + trim(oXMLValores.selectSingleNode("//Response/CAMPOS/MENSAJE").text);
				return false;		
			}
		}
		else
		{
			document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente."
			return false;		
		}			
	}
	else
	{
		document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente."
		return false;		
	}
	
	
}

function dividirAnotacion(pAnotacion, pTipoanotacion,  pLongitud)

{	  var resultado = {       
        xml: "",
        cantidad: 0}
        
	var xmlAnota = ""
	var txtAnota = pAnotacion	
	
	var nroAnota =0
	if(txtAnota != '')
		{
			while (txtAnota.length >0)	
			{   
				nroAnota ++
				xmlAnota+="<ANOTACION>";
				xmlAnota+="<TIPOANOTA>"+ pTipoanotacion +"</TIPOANOTA>";
				xmlAnota+="<ORDENTEXT>"+ nroAnota +"</ORDENTEXT>";
				xmlAnota+="<TEXTO><![CDATA["+ leftString(txtAnota,pLongitud) +"]]></TEXTO>";
				xmlAnota+="</ANOTACION>";			
				txtAnota = txtAnota.slice(pLongitud)
		
			}
	
		}
	else
		{
			xmlAnota+="<ANOTACION>";
			xmlAnota+="<TIPOANOTA></TIPOANOTA>";
			xmlAnota+="<ORDENTEXT>0</ORDENTEXT>";
			xmlAnota+="<TEXTO></TEXTO>";
			xmlAnota+="</ANOTACION>";			
			
		}
	resultado.xml = xmlAnota
	resultado.cantidad = nroAnota
	return resultado
	
}



/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
function fncIntegracionItems(pDesde, pNROSOLTMP, pFINPOLIZA)
{
	//Items
	var oXMLItems = new ActiveXObject('MSXML2.DOMDocument');
			oXMLItems.async = false;
			oXMLItems.loadXML(document.getElementById("varXmlItems").value);
	
	
	var oXMLRegItems = oXMLItems.selectNodes("//ITEM");
	var varCantItems = oXMLRegItems.length;
	
	//Cantidad m�xima de items
	//LR 04/01/2011 Soporta de 1 solo item, porq subieron la cant de cobert a 30
	var cntCantCtrl = pDesde+1;
	//Control de items
	var varContCtrl = 0;
	
	
			//ObjetosAsegurados
			var oXMLObjAseg = new ActiveXObject('MSXML2.DOMDocument');
			oXMLObjAseg.async = false;
			oXMLObjAseg.loadXML(document.getElementById("varXmlObjAseg").value);
			var oXMLRegObjAseg = oXMLObjAseg.selectNodes("//OBJETO");
			//Cantidad m�xima de objetos asegurados
			var cntCantCtrlObjAseg = 20;
			//Control de items
			var varContCtrlObjAseg = 0;
	
	
	//Calcular Desde (item/grilla)
	var varDesdeItems = 0;
	var varDesdeGrilla = 0;
	var varCantAcum = 0
	

	//Variable con el XML de Datos
	var xmlDatosInt = "";
	
	xmlDatosInt+=
		"<INTERSEC>NOBK</INTERSEC>"+
		"<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
		"<SECUECOD>"+ document.getElementById("fechahoy").value +"</SECUECOD>"
		//GED 09-11-2010 SE CORRIGE INTEGRACION NROSOLICITUD
		
		if(document.getElementById("TIPOOPER").value == "R")
			var nroSolicitud = fncSetLength(document.getElementById("CERTIPOL").value,4) + fncSetLength(document.getElementById("CERTISEC").value,6) + document.getElementById("RAMOPCOD").value;
		else
			var nroSolicitud = document.getElementById("nroSolicitud").value == ""? fncSetLength(document.getElementById("CERTIPOL").value,4) + fncSetLength(document.getElementById("CERTISEC").value,6) + document.getElementById("RAMOPCOD").value:  document.getElementById("nroSolicitud").value.toUpperCase();
	
		//var nroSolicitud = document.getElementById("nroSolicitud").value == ""? fncSetLength(document.getElementById("CERTIPOL").value,4) + fncSetLength(document.getElementById("CERTISEC").value,6) + document.getElementById("RAMOPCOD").value:  document.getElementById("nroSolicitud").value.toUpperCase()
	
		xmlDatosInt+="<SOLICITU>"+ nroSolicitud +"</SOLICITU>"	
		
		//"<SOLICITU>"+  fncSetLength(document.getElementById("CERTIPOL").value,4) +	fncSetLength(document.getElementById("CERTISEC").value,6) +  fncSetLength(document.getElementById("RAMOPCOD").value,4)+"</SOLICITU>"+
		xmlDatosInt+="<NROSOLTMP>"+ pNROSOLTMP +"</NROSOLTMP>"+
		"<FINPOLIZA>"+ pFINPOLIZA +"</FINPOLIZA>"+
		"<RAMO>"+ document.getElementById("RAMOPCOD").value +"</RAMO>"+
		"<ORIOPERA>OV</ORIOPERA>"
	
	//HRF 02/11/2010 Cambios Vendedor
	//R65-VENDEDORES
	if (document.getElementById("SOLIC_VEND").value == "S")
	{
		if (document.getElementById("CANALHSBC").value == "S")
		{
			//**** Datos SEFMGEBA
			xmlDatosInt+="<BANCOCOD>"+ document.getElementById("INSTITUCION").value +"</BANCOCOD>";
			xmlDatosInt+="<SUCURCOD>"+ document.getElementById("cbo_vendSucursal").value +"</SUCURCOD>";
			xmlDatosInt+="<LEGAJAPERT></LEGAJAPERT>";
			xmlDatosInt+="<LEGAJCIERRE></LEGAJCIERRE>";
			//**** Datos GRFMGEEM
			xmlDatosInt+="<BANCOCODEM>0150</BANCOCODEM>";
			xmlDatosInt+="<LEGAJCIA>"+ document.getElementById("CODEMP").value +"</LEGAJCIA>";
			xmlDatosInt+="<LEGAJNUMAP>"+ document.getElementById("legajoVendedor").value +"</LEGAJNUMAP>";
			xmlDatosInt+="<COMISIONAP>S</COMISIONAP>";
			xmlDatosInt+="<LEGAJAPEAP>"+ document.getElementById("APELLIDOVEND").value +"</LEGAJAPEAP>";
			xmlDatosInt+="<LEGAJNOMAP>"+ document.getElementById("NOMBREVEND").value +"</LEGAJNOMAP>";
			xmlDatosInt+="<LEGAJNUMCR>"+ document.getElementById("legajoVendedor").value +"</LEGAJNUMCR>";
			xmlDatosInt+="<LEGAJAPECR>"+ document.getElementById("APELLIDOVEND").value +"</LEGAJAPECR>";
			xmlDatosInt+="<LEGAJNOMCR>"+ document.getElementById("NOMBREVEND").value +"</LEGAJNOMCR>";
			xmlDatosInt+="<COMISIONCR>S</COMISIONCR>";
		} else {
			//**** Datos SEFMGEBA
			xmlDatosInt+="<BANCOCOD>"+ document.getElementById("INSTITUCION").value +"</BANCOCOD>";
			xmlDatosInt+="<SUCURCOD>"+ document.getElementById("cbo_vendSucursal").value +"</SUCURCOD>";
			xmlDatosInt+="<LEGAJAPERT>"+ document.getElementById("legajoVendedor").value +"</LEGAJAPERT>";
			xmlDatosInt+="<LEGAJCIERRE>"+ document.getElementById("legajoVendedor").value +"</LEGAJCIERRE>";
			//**** Datos GRFMGEEM
			xmlDatosInt+="<BANCOCODEM>0</BANCOCODEM>";
			xmlDatosInt+="<LEGAJCIA>0</LEGAJCIA>";
			xmlDatosInt+="<LEGAJNUMAP>0</LEGAJNUMAP>";
			xmlDatosInt+="<COMISIONAP></COMISIONAP>";
			xmlDatosInt+="<LEGAJAPEAP></LEGAJAPEAP>";
			xmlDatosInt+="<LEGAJNOMAP></LEGAJNOMAP>";
			xmlDatosInt+="<LEGAJNUMCR>0</LEGAJNUMCR>";
			xmlDatosInt+="<LEGAJAPECR></LEGAJAPECR>";
			xmlDatosInt+="<LEGAJNOMCR></LEGAJNOMCR>";
			xmlDatosInt+="<COMISIONCR></COMISIONCR>";
		}
	} else {
		//**** Datos SEFMGEBA
		xmlDatosInt+="<BANCOCOD>0</BANCOCOD>";
		xmlDatosInt+="<SUCURCOD>0</SUCURCOD>";
		xmlDatosInt+="<LEGAJAPERT></LEGAJAPERT>";
		xmlDatosInt+="<LEGAJCIERRE></LEGAJCIERRE>";
		//**** Datos GRFMGEEM
		xmlDatosInt+="<BANCOCODEM>0</BANCOCODEM>";
		xmlDatosInt+="<LEGAJCIA>0</LEGAJCIA>";
		xmlDatosInt+="<LEGAJNUMAP>0</LEGAJNUMAP>";
		xmlDatosInt+="<COMISIONAP></COMISIONAP>";
		xmlDatosInt+="<LEGAJAPEAP></LEGAJAPEAP>";
		xmlDatosInt+="<LEGAJNOMAP></LEGAJNOMAP>";
		xmlDatosInt+="<LEGAJNUMCR>0</LEGAJNUMCR>";
		xmlDatosInt+="<LEGAJAPECR></LEGAJAPECR>";
		xmlDatosInt+="<LEGAJNOMCR></LEGAJNOMCR>";
		xmlDatosInt+="<COMISIONCR></COMISIONCR>";
	}
	
		//<CAMPO Nombre="SECUECOD" TipoDato="ENTERO" Enteros="9"/>
		//<CAMPO Nombre="SOLICITU" TipoDato="TEXTO" Enteros="25"/>
		//<CAMPO Nombre="NROSOLTMP" TipoDato="ENTERO" Enteros="18"/>
		//<CAMPO Nombre="FINPOLIZA" TipoDato="TEXTO" Enteros="1"/>
		//<CAMPO Nombre="RAMO" TipoDato="TEXTO" Enteros="4"/>
		//<CAMPO Nombre="ORIOPERA" TipoDato="TEXTO" Enteros="2" Default=""/>
	xmlDatosInt+="<RIESGOS>";

	vCLAUSAJUST = document.getElementById("estabilizacion").value;
	
	for(var x=pDesde; x<  cntCantCtrl; x++)
	{
		if(x < varCantItems)
		{
					xmlDatosInt+="<RIESGO>";
					
					xmlDatosInt+="<ITEM>"+ eval(x+1) +"</ITEM>";
					xmlDatosInt+="<RIESGTIP>21</RIESGTIP>";	//Esto va fijo
					xmlDatosInt+="<TIPOMOVI>I</TIPOMOVI>";	//Inclusion es una alta
					xmlDatosInt+="<CERTIPOLANT>"+ Number(oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text) +"</CERTIPOLANT>";
					xmlDatosInt+="<CERTIANNANT>"+ Number(oXMLRegItems[x].selectSingleNode("CERTIANNANT").text) +"</CERTIANNANT>";
					xmlDatosInt+="<CERTISECANT>"+ Number(oXMLRegItems[x].selectSingleNode("CERTISECANT").text) +"</CERTISECANT>";
					xmlDatosInt+="<CLAUSAJUST>"+ vCLAUSAJUST +"</CLAUSAJUST>";
					
					xmlDatosInt+="<ITIPALAR>"+ oXMLRegItems[x].selectSingleNode("ALARMA").text +"</ITIPALAR>";
					xmlDatosInt+="<ITIPGUAR>"+ oXMLRegItems[x].selectSingleNode("GUARDIA").text +"</ITIPGUAR>";
					//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
					xmlDatosInt+="<IDISYUNT>"+ oXMLRegItems[x].selectSingleNode("IDISYUNT").text +"</IDISYUNT>";
					
					
			/*<CAMPO Nombre="ITIPALAR" TipoDato="ENTERO" Enteros="2"/>
			<CAMPO Nombre="ITIPGUAR" TipoDato="ENTERO" Enteros="2"/>		
			<CAMPO Nombre="IDISYUNT" TipoDato="TEXTO" Enteros="1"/>*/				
					//Cobertura
					
					var objXMLCobertura = oXMLItems.selectNodes("//COBERTURA[IDITEM='"+eval(x+1)+"']");
					var cantCoberturas = objXMLCobertura.length
					xmlDatosInt+="<COBERTURAS>";
					for (var y=0;y<cantCoberturas;y++)
						{
						var COBERCOD = fncSetLength(objXMLCobertura[y].selectSingleNode("COBERCOD").text, 3)
		
						
						var SUMAASEG =formatoNroAIS(objXMLCobertura[y].selectSingleNode("SUMAASEG").text,13,2)
						
						var PRIMA = formatoNroAIS(objXMLCobertura[y].selectSingleNode("PRIMA").text,10,2)
						
						//GED 19-05-2011 - CUANDO ES PRIMA MINIMA MANDA 0 EN LA TASA
						var TASA = document.getElementById('SWPRIMIN').value == "N"? formatoNroAIS(objXMLCobertura[y].selectSingleNode("TASA").text,3,6): fncSetLength("0",9)
						
						//var TASA = formatoNroAIS(objXMLCobertura[y].selectSingleNode("TASA").text,3,6)
						xmlDatosInt+= COBERCOD + SUMAASEG + TASA + PRIMA;
						//alert(COBERCOD +" "+ SUMAASEG +" "+ TASA +" "+ PRIMA)
						}
						for (var y=cantCoberturas;y<30;y++)
						{
							
						var COBERCOD =fncSetLength("0",3)						
						var SUMAASEG = fncSetLength("0",15)
						var PRIMA =  fncSetLength("0",12)
						var TASA =  fncSetLength("0",9)
					
						xmlDatosInt+= COBERCOD + SUMAASEG + TASA + PRIMA;
						}
						
					xmlDatosInt+="</COBERTURAS>";	
					
					xmlDatosInt+="<DOMICDOM>"+ oXMLRegItems[x].selectSingleNode("DOMICDOM").text +"</DOMICDOM>";
					xmlDatosInt+="<DOMICDNU>"+  oXMLRegItems[x].selectSingleNode("DOMICDNU").text  +"</DOMICDNU>";
					xmlDatosInt+="<DOMICPIS>"+ oXMLRegItems[x].selectSingleNode("DOMICPIS").text +"</DOMICPIS>";
					xmlDatosInt+="<DOMICPTA>"+ oXMLRegItems[x].selectSingleNode("DOMICPTA").text +"</DOMICPTA>";
					xmlDatosInt+="<DOMICCPO>"+ oXMLRegItems[x].selectSingleNode("CPOSDOM").text +"</DOMICCPO>";
					xmlDatosInt+="<DOMICPOB>"+ oXMLRegItems[x].selectSingleNode("DOMICPOB").text +"</DOMICPOB>";
					xmlDatosInt+="<PROVICOD>"+ oXMLRegItems[x].selectSingleNode("PROVICOD").text+"</PROVICOD>";
					xmlDatosInt+="<PAISSCOD>00</PAISSCOD>";
					
					xmlDatosInt+="<ACREENOM></ACREENOM>";
					xmlDatosInt+="<ACREEAP1></ACREEAP1>";
					xmlDatosInt+="<ACREEAP2></ACREEAP2>";
					xmlDatosInt+="<ADOCUMTIP>0</ADOCUMTIP>";
					xmlDatosInt+="<ADOCUMDAT></ADOCUMDAT>";
					xmlDatosInt+="<ACLIENTIP>0</ACLIENTIP>";
					xmlDatosInt+="<ADOMICDOM></ADOMICDOM>";
					xmlDatosInt+="<ADOMICDNU></ADOMICDNU>";
					xmlDatosInt+="<ADOMICPIS></ADOMICPIS>";
					xmlDatosInt+="<ADOMICPTA></ADOMICPTA>";
					xmlDatosInt+="<ADOMICCPO></ADOMICCPO>";
					xmlDatosInt+="<ADOMICPOB></ADOMICPOB>";
					xmlDatosInt+="<APROVICOD>0</APROVICOD>";
					xmlDatosInt+="<APAISSCOD>0</APAISSCOD>";
					xmlDatosInt+="<TIPOACRE></TIPOACRE>";
					
					//OBJETOS
					
						xmlDatosInt+="<OBJETOS>";
					
					var oXMLRegObjAseg = oXMLObjAseg.selectNodes("//OBJETO[IDITEM='"+eval(x+1)+"']");
					
					var cantObjAseg = oXMLRegObjAseg.length
				
					for (var y=0;y<cantObjAseg;y++)
					{							
						var COBERCOD = fncSetLength(oXMLRegObjAseg[y].selectSingleNode("COBERCOD").text,3);
						var RIESGTIP = fncSetLength("0",2);
						var OBJETCOD = fncSetLength(oXMLRegObjAseg[y].selectSingleNode("CODIGOOBJETO").text,3);
						var SUMAASEG = formatoNroAIS(oXMLRegObjAseg[y].selectSingleNode("SUMAASEG").text,13,2);
						var DETALLE1 = fncSetLength(oXMLRegObjAseg[y].selectSingleNode("DETALLEOBJETO").text,100," ","L");
						var DETALLE2 = fncSetLength("",100," ");
						var DETALLE3 = fncSetLength("",100," ");
						
						
						xmlDatosInt+=COBERCOD+RIESGTIP+OBJETCOD+SUMAASEG+DETALLE1+DETALLE2+DETALLE3;
					}
					for(var y=cantObjAseg; y<cntCantCtrlObjAseg; y++)
					{
						var COBERCOD = fncSetLength("0",3);
						var RIESGTIP = fncSetLength("0",2);
						var OBJETCOD = fncSetLength("0",3);
						var SUMAASEG = fncSetLength("0",15);
						var DETALLE1 = fncSetLength("",100," ");
						var DETALLE2 = fncSetLength("",100," ");
						var DETALLE3 = fncSetLength("",100," ");
						
						xmlDatosInt+=COBERCOD+RIESGTIP+OBJETCOD+SUMAASEG+DETALLE1+DETALLE2+DETALLE3;
						 
					}
					xmlDatosInt+="</OBJETOS>";
				//ANOTACIONES
				//GED 19-05-2011 TEXTO PRIMA MINIMA
			xmlDatosInt+="<ANOTACIONES>";
				var anotacionPrimin = 0
					if (document.getElementById('SWPRIMIN').value == "S" &&  x==0)
					{
						var ANOTACION_PRIMIN = "ESTA POLIZA SUFRIO UN INCREMENTO EN LA PRIMA PARA LLEGAR A  LA PRIMA MINIMA ESTABLECIDA PARA ESTE PRODUCTO"				
						var cantAnotI = Number(document.getElementById('cantAnotacionesI').value)+1
						varTip = fncSetLength("C",1," ");
						varOrd = fncSetLength(cantAnotI,4);
						varTex = fncSetLength(ANOTACION_PRIMIN,600," ","L");						
						xmlDatosInt+= "<![CDATA["+ varTip + varOrd + varTex + "]]>";
						anotacionPrimin = 1
					}
					
					
					for(var y=0; y<Number(4-anotacionPrimin) ; y++)
					{
						varTip = fncSetLength("",1," ");
						varOrd = fncSetLength("0",4);
						varTex = fncSetLength("",600," ");
						
						xmlDatosInt+= "<![CDATA["+ varTip + varOrd + varTex + "]]>";
						
						//xmlDatosInt+= varTip + varOrd + varTex 
					}
					xmlDatosInt+="</ANOTACIONES>";
					
					xmlDatosInt+="</RIESGO>";
					
				
				
					
					
		}
		else
		{
			xmlDatosInt+="<RIESGO>";
					
					xmlDatosInt+="<ITEM>0</ITEM>";
					xmlDatosInt+="<RIESGTIP>0</RIESGTIP>";	//Esto va fijo para autos
					xmlDatosInt+="<TIPOMOVI></TIPOMOVI>";	//Inclusion es una alta
					xmlDatosInt+="<CERTIPOLANT>0</CERTIPOLANT>";
					xmlDatosInt+="<CERTIANNANT>0</CERTIANNANT>";
					xmlDatosInt+="<CERTISECANT>0</CERTISECANT>";
					xmlDatosInt+="<CLAUSAJUST>0</CLAUSAJUST>";
					
					xmlDatosInt+="<ITIPALAR>0</ITIPALAR>";
					xmlDatosInt+="<ITIPGUAR>0</ITIPGUAR>";
					xmlDatosInt+="<IDISYUNT>N</IDISYUNT>";
					
					xmlDatosInt+="<CCANTASCE>0</CCANTASCE>";
									
					//Cobertura
					
					
					xmlDatosInt+="<COBERTURAS>";
					
						for (var y=0;y<30;y++)
						{
							
						var COBERCOD =fncSetLength("0",3)						
						var SUMAASEG = fncSetLength("0",15)
						var PRIMA =  fncSetLength("0",12)
						var TASA =  fncSetLength("0",9)							
						
						xmlDatosInt+= COBERCOD + SUMAASEG + TASA + PRIMA;
						}
						
					xmlDatosInt+="</COBERTURAS>";	
					
					xmlDatosInt+="<DOMICDOM></DOMICDOM>";
					xmlDatosInt+="<DOMICDNU></DOMICDNU>";
					xmlDatosInt+="<DOMICPIS></DOMICPIS>";
					xmlDatosInt+="<DOMICPTA></DOMICPTA>";
					xmlDatosInt+="<DOMICCPO></DOMICCPO>";
					xmlDatosInt+="<DOMICPOB></DOMICPOB>";
					xmlDatosInt+="<PROVICOD>0</PROVICOD>";
					xmlDatosInt+="<PAISSCOD>0</PAISSCOD>";
					
					xmlDatosInt+="<ACREENOM></ACREENOM>";
					xmlDatosInt+="<ACREEAP1></ACREEAP1>";
					xmlDatosInt+="<ACREEAP2></ACREEAP2>";
					xmlDatosInt+="<ADOCUMTIP>0</ADOCUMTIP>";
					xmlDatosInt+="<ADOCUMDAT></ADOCUMDAT>";
					xmlDatosInt+="<ACLIENTIP>0</ACLIENTIP>";
					xmlDatosInt+="<ADOMICDOM></ADOMICDOM>";
					xmlDatosInt+="<ADOMICDNU></ADOMICDNU>";
					xmlDatosInt+="<ADOMICPIS></ADOMICPIS>";
					xmlDatosInt+="<ADOMICPTA></ADOMICPTA>";
					xmlDatosInt+="<ADOMICCPO></ADOMICCPO>";
					xmlDatosInt+="<ADOMICPOB></ADOMICPOB>";
					xmlDatosInt+="<APROVICOD>0</APROVICOD>";
					xmlDatosInt+="<APAISSCOD>0</APAISSCOD>";
					xmlDatosInt+="<TIPOACRE></TIPOACRE>";
					
					//OBJETOS
					xmlDatosInt+="<OBJETOS>";
					
				
					
					for(var y=0; y<20; y++)
					{
						
						xmlDatosInt+=fncSetLength("0",3)+
									fncSetLength("0",2)+
									fncSetLength(" ",3," ")+
									fncSetLength("0",15)+	
									fncSetLength(" ",100," ")+	
									fncSetLength(" ",100," ")+	
									fncSetLength(" ",100," ")
							
						}
						
					
					xmlDatosInt+="</OBJETOS>";
					
				
					
					//ANOTACIONES
					xmlDatosInt+="<ANOTACIONES>";
					for(var y=0; y<4; y++)
					{
						varTip = fncSetLength("",1," ");
						varOrd = fncSetLength("0",4);
						varTex = fncSetLength("",600," ");
						
					
						xmlDatosInt+= "<![CDATA["+ varTip + varOrd + varTex + "]]>";
						
						//xmlDatosInt+= varTip + varOrd + varTex 
					}
					xmlDatosInt+="</ANOTACIONES>";
					
					xmlDatosInt+="</RIESGO>";
			
			
		}
		
	}




	xmlDatosInt += "</RIESGOS>";
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "XML Items<textarea>" + xmlDatosInt + "</textarea>")
	//document.body.insertAdjacentHTML("beforeEnd", "Items<textarea>"+xmlDatosInt+"</textarea>")
	
	//xmlDatosInt = encodeEntities(xmlDatosInt)
	//Armo el XML de entrada para guardar los datos de la cotizacion
	strData = "FUNCION=INTEGRAITEM&";
	strData+= "RequestXMLHTTP=" + xmlDatosInt.replace(/&/g,"~~");
	xmlHTTPCotiza = new ActiveXObject("Msxml2.XMLHTTP");
	
	xmlHTTPCotiza.open("POST","DefinicionFrameWork.asp", false);
	xmlHTTPCotiza.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	//document.body.insertAdjacentHTML("beforeEnd", "Int.Items<textarea>"+xmlDatosInt+"</textarea>")
	//return true;
	
	xmlHTTPCotiza.send(encodeURI(strData));
	
	if (xmlHTTPCotiza.readyState==4)
	{
		xmlResultado = new ActiveXObject("Msxml2.DOMDocument");
		xmlResultado.async = false;
		xmlResultado.loadXML(xmlHTTPCotiza.responseText);
	//	document.body.insertAdjacentHTML("beforeEnd", "Devol.Items<textarea>"+xmlResultado.xml+"</textarea>")
		
		if(xmlResultado.xml.length > 0)
		{
			if(xmlResultado.selectSingleNode("//Response/Estado/@resultado").text == "true")
			{
				if(trim(xmlResultado.selectSingleNode("//Response/CAMPOS/MENSAJE").text) == "")
				{
			
				//Propuesta AIS
				document.getElementById("AIS_POLIZANN").value = xmlResultado.selectSingleNode("//Response/CAMPOS/POLIZANN").text;
				document.getElementById("AIS_POLIZSEC").value = xmlResultado.selectSingleNode("//Response/CAMPOS/POLIZSEC").text;
				document.getElementById("AIS_CERTIPOL").value = xmlResultado.selectSingleNode("//Response/CAMPOS/CERTIPOL").text;
				document.getElementById("AIS_CERTIANN").value = xmlResultado.selectSingleNode("//Response/CAMPOS/CERTIANN").text;
				document.getElementById("AIS_CERTISEC").value = xmlResultado.selectSingleNode("//Response/CAMPOS/CERTISEC").text;
				document.getElementById("AIS_SUPLENUM").value = xmlResultado.selectSingleNode("//Response/CAMPOS/SUPLENUM").text;
				return true;
				}
				else
				 {
				 	document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente<BR/>" + trim(xmlResultado.selectSingleNode("//Response/CAMPOS/MENSAJE").text);
					return false;				
				}
			} else 
				{
				document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente.";
				return false;				
				}
		} else
			{
			document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente.";				
			return false;
			}
	}	else 
		{
		document.getElementById("lblPantallaCierre").innerHTML = "Ha ocurrido un error con el env�o de la Solicitud. Desde la consulta de Solicitudes Generadas en OV podr� recuperar la misma y enviar nuevamente.";				
		return false;
		}
}


/**************************************************************/
function openPopUpFueraNorma()
{
	
	
	//Fondo de pantalla
	var ancho=document.body.clientWidth
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	// Fin Fondo
	document.getElementById('confirma_FueraNorma').style.left  = 150
	document.getElementById('confirma_FueraNorma').style.top  =   body.scrollTop+100
	document.getElementById('confirma_FueraNorma').style.display = 'inline';
	
	document.getElementById("tablaFN").innerHTML = varPopUpFueraNorma;
}



function ocultarPopUpEnviar()
{
	document.getElementById('popUp_opacidad').style.display ='none';
	document.getElementById("div_cierre").style.display ='none';
	
	
}

function ocultarPopUpRevision()
{
	document.getElementById('popUp_opacidad').style.display ='none';
	document.getElementById("envio_revision").style.display ='none';
	
	
	
}

function openPopUpRevision()
{
	
	//llamada al 1000 para recuperar los mails
	aValidarSolapas[3]="S"
		  
	validarDatosFormulario("true")
	if(document.getElementById("divTablaErrores").innerHTML=="")	
	{
			
		if( grabarOperacion("C",false) == 0 )
		{
			alert_customizado("Se ha producido un error y la cotizaci�n no se ha guardado correctamente.")
			return false
		}
		
	//*************************	
	//	CUADRO CAMBIOS RENOVACION
	if (document.getElementById("TIPOOPER").value == "R")
		{
			
			
			fncRenovacionCambios("4")
			comparaCoberturasItemsRen()
			
			
			var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
			oXMLValores.async = false; 
			
			 var oXSLConvierte = new ActiveXObject('MSXML2.DOMDocument');    
			oXSLConvierte.async = false;
			
			//oXSLConvierte.load( datosCotizacion.getCanalURL() + "Forms/XSL/CONVIERTE_XMLCAMBIOS.xsl")
			oXSLConvierte.loadXML(document.getElementById("CONVIERTE_XMLCAMBIOS").xml)
			
			 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
			oXSLValores.async = false;
			
			
				//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/CuadroCambiosRenovacion.xsl"))
				if(oXSLValores.loadXML(document.getElementById("CuadroCambiosRenovacion").xml))
				{	
			
				  var xslParam="<parametros>"+
				       "<parametro>"+
		                    "<nombre>urlCanal</nombre><valor>"+document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value +"</valor>"+
	                    "</parametro>"+		
					"</parametros>"			
					//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varXml.xml+"</textarea>")		
					
					var xmlTemp = parseaXSLParam(oXMLCambios.xml,oXSLConvierte,xslParam)	
					
			//		window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+ xmlTemp + "</textarea>")
					
					var varResultTabla = parseaXSLParam(xmlTemp,oXSLValores,xslParam)
					
					//window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+ document.getElementById("cuadroCambiosRenovacion").innerHTML + "</textarea>")
					
					document.getElementById("cuadroCambiosEnvio").innerHTML = "";
					document.getElementById("cuadroCambiosRenovacion").style.display='inline';					
					document.getElementById("cuadroCambiosRenovacion").innerHTML = varResultTabla;	
			    }
		    
		
		}
	//************************
	mvarRequest = "<Request><DEFINICION>1000_PerfilUsuario.xml</DEFINICION>"+
	"<AplicarXSL>1000_PerfilUsuario.xsl</AplicarXSL>"+
	"<USUARIO>" +  document.getElementById("LOGON_USER").value + "</USUARIO></Request>"
	
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQGestion') 
	
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(mvarResponse);	
	
	//GED 09-09-2010 - DEFECT 138	
	
		mvarRequest = "<Request><Usuario>" +document.getElementById("LOGON_USER").value  + "</Usuario></Request>"
		
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'camA_ObtenerEmail') 	
	varXmlCAM = new ActiveXObject("MSXML2.DOMDocument");
	varXmlCAM.async = false;
	varXmlCAM.loadXML(mvarResponse);	
	
	

	if(varXmlCAM.selectSingleNode("//Estado/@resultado").text != "true" ) {
		errorInesperado("Error Recuperando Mail del Usuario")	
		return false
	}
	var mailUser= varXmlCAM.selectSingleNode("//Email").text
	
	//var mailUser= varXml.selectSingleNode("//MAILUSER").text
	var usuarNomec= varXml.selectSingleNode("//USUARNOMEC").text
	var mailEc = varXml.selectSingleNode("//MAILEC").text	
	
	//Datos del ejecutivo de cuenta
	document.getElementById("EJEC_PPLSOFT").value = varXml.selectSingleNode("//PEOPLESOFTEC").text
	document.getElementById("EJEC_APENOM").value = varXml.selectSingleNode("//USUARNOMEC").text	
	document.getElementById("EJEC_EMAIL").value = varXml.selectSingleNode("//MAILEC").text
		
	//Fondo de pantalla
	var ancho=document.body.clientWidth
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	// Fin Fondo
	document.getElementById('envio_revision').style.left  = 200
	document.getElementById('envio_revision').style.top  =   body.scrollTop+100
	document.getElementById('envio_revision').style.display = 'inline';
	document.getElementById('revision_de').value= mailUser
	document.getElementById('revision_para').value= mailEc
	document.getElementById('usuarNomec').value= usuarNomec
	
	document.getElementById('revision_nroCoti').innerText = document.getElementById("RAMOPCOD").value + "-"+
	
	rightString("0000" + document.getElementById("CERTIPOL").value,4)  +"-"+ 	
	
	rightString("000000" + document.getElementById("CERTISEC").value,6)	
	
	//GED 08-11-2010 defect 239
	
}
	
	
	
}



function grabarOperacion(pTipoOperacion,pPopUp)
{ 
	if( validarDatosFormulario("true"))
	{ 	window.status="Grabando..."
		var clienSec= fncGuardarCliente()
		if ( document.getElementById("tieneApoderado").value =="S")
			var apoSec= fncGuardarApoderado()
		else
			var apoSec= "0"
			
		var vTIPOOPER = document.getElementById("TIPOOPER").value;
			
		if(pTipoOperacion== "C")	
			var mvarCertisec = fncGuardarCotizacion(clienSec,apoSec,"P", vTIPOOPER,pPopUp)
		if(pTipoOperacion== "S")
			var mvarCertisec = fncGuardarCotizacion(clienSec,apoSec,"Y", vTIPOOPER,pPopUp)
		
		window.status="Listo"		
		return mvarCertisec
		
		//document.getElementById("CERTISEC").value 	= mvarCertisec
		//window.status= "certisec: " +mvarCertisec
	}
	
	
	
}

function completarSolicitud()
{
	var resultadoDatosGrales = validarDatosFormulario("true")
	if(document.getElementById("divTablaErrores").innerHTML=="")
	{
		if( grabarOperacion("C",false) == 0 )
		{
			alert_customizado("Se ha producido un error y la cotizaci�n no se ha guardado correctamente.")
			return false
		}
		
		ocultaEtiquetaSolapa("1")
		ocultaEtiquetaSolapa("2")
		ocultaEtiquetaSolapa("3")
		ocultaEtiquetaSolapa("4")
		muestraEtiquetaSolapa("5")
		muestraEtiquetaSolapa("6")
		muestraEtiquetaSolapa("7")
		
		mostrarTab('divDatosSolicitud','','#9CBACE','');
		
	}
}

function modificarCotizacion()
{	
	
	if (tabActual==5)
		fncInsertarObjAsegTotal();
	
	 muestraEtiquetaSolapa("1")
	 muestraEtiquetaSolapa("2")
	 muestraEtiquetaSolapa("3")
	 muestraEtiquetaSolapa("4")
	 ocultaEtiquetaSolapa("5")
	 ocultaEtiquetaSolapa("6")
	 ocultaEtiquetaSolapa("7")
	  mostrarTab('divDatosGenerales','','#9CBACE','');   
	  
	
	

}	
	


function fncSiCambiaSolapa()
{		
	//GED 28-7-2010: Titulo para renovaciones
	var txtRenov=""
	   if ( document.getElementById("TIPOOPER").value == "R") txtRenov="Renovaci�n"
	   		
			document.getElementById("divBtnNuevaCotizacion").style.display='none'
			document.getElementById("divBtnAnterior").style.display='none'
			document.getElementById("divBtnConfirmarSolicitud").style.display='none';
			document.getElementById("divBtnCompletarSolicitud").style.display='none';
			document.getElementById("divBtnEnvioRevision").style.display='none';
			document.getElementById("divBtnModificarCotizacion").style.display='none'	
			document.getElementById("divBtnContinuar").style.display='none'
			document.getElementById("divBtnGrabarCotizacion").style.display='none'
			document.getElementById("divBtnGrabarIrSolicitud").style.display='none'
			document.getElementById("divBtnEnvioEmail").style.display='none'
			document.getElementById("divBtnImprimirCotizacion").style.display='none'	
			document.getElementById("divCodigoColores").style.display='none'			
			document.getElementById("divTablaFueraNorma").style.display='inline'	
			

/*document.getElementById("PROVICOD").value=document.getElementById("CBO_PROVINCIA").value
document.getElementById("DOMICPOB").value=document.getElementById("localidadRiesgo").value
document.getElementById("CPOSDOM").value=document.getElementById("codPostalRiesgo").value	
document.getElementById("CodigoZonaICOBB").value=document.getElementById("CODIGOZONA").value
document.getElementById("verZonaICOBB").value=document.getElementById("verZona").innerText*/
			
			var resultadoDatosGrales = validarDatosFormulario("true")			

			var resultadoItems= validarItemsVacios() 		


	if(tabActual==0) 
	{
		aValidarSolapas[0]="S"
             aValidarSolapas[1]="N"
		aValidarSolapas[2]="N"
		aValidarSolapas[3]="N"
	       aValidarSolapas[4]="N"
		 aValidarSolapas[5]="N"
		 aValidarSolapas[6] = "N"
		 if (fncEsICB1())
		     document.getElementById("td_Titulo").innerText = "Cotizaci�n Integral de Comercio Business Bank"
         else
		    document.getElementById("td_Titulo").innerText = "Cotizaci�n "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
		 document.getElementById("divBtnNuevaCotizacion").style.display='inline'
	       document.getElementById("divBtnContinuar").style.display='inline'	    	 
	       document.getElementById("divCodigoColores").style.display='inline'
        	if (document.getElementById("TIPOOPER").value == "R")
    			document.getElementById("divBtnRecuperarDatos").style.display='inline'
	   
    
	}
	
	if(tabActual==1) 
	{	
		aValidarSolapas[0]="S"
    		aValidarSolapas[1]="S"
		 aValidarSolapas[2]="N"
		 aValidarSolapas[3]="N"
		 aValidarSolapas[4]="N"
		 aValidarSolapas[5]="N"
		 aValidarSolapas[6] = "N"

		 if (fncEsICB1()){
		     document.getElementById("td_Titulo").innerText = "Cotizaci�n Integral de Comercio Business Bank"
			 fncObtenerItemsICB1()
		 }
		 else
		    document.getElementById("td_Titulo").innerText = "Cotizaci�n "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
		

		aValidarSolapas[1]="S"
		document.getElementById("divBtnNuevaCotizacion").style.display='inline'
	  	document.getElementById("divBtnAnterior").style.display='inline'   	
    		document.getElementById("divBtnContinuar").style.display='inline'
    		document.getElementById("divCodigoColores").style.display='inline'	  
    		document.getElementById("verActividad").innerText =  document.getElementById("cboActividadComercio").options[document.getElementById("cboActividadComercio").selectedIndex].text
    
	    //----------------------------------------------------------------------------------//
	    //Def 393 LR - Si la poliza a renovar tiene el plan de pago 999- Plan libre debe permitir 
	    //renovarla sin ir al circuito de revision pero se debe formazar al usuario a elegir un 
	    //plan de pago de los habilitados en el alta.-
	  	var vPlanPago = document.getElementById("planPago").value.split("|")[0];
	  	
	  	if(vPlanPago == '999')
			{
				mostrarTab(aScreenTabs[tabAnterior],'','#9CBACE','');
				insertarError("", "planPago","Debe seleccionar la cantidad de cuotas que desee.",1,"E");
				muestraTablaErrores();
				return;
			}
	  	//----------------------------------------------------------------------------------//
    
		
		    if (document.getElementById("TIPOOPER").value == "R"){
		    	document.getElementById("divBtnRecuperarDatos").style.display='inline'
				
				if (document.getElementById("estabilizacion").value != document.getElementById("CLAUSULARENOV").value && document.getElementById("RAMOPCOD").value == "ICO1"){ //MAG RENOV
					mostrarTab(aScreenTabs[tabAnterior],'','#9CBACE','');
					insertarError("", "estabilizacion","Debido a la modificaci�n de la estabilizaci�n, esta renovaci�n no podr� ser cotizada a trav�s de la Oficina Virtual. Requiere la aprobaci�n de la compa��a, por favor cont�ctese con su Ejecutivo de Cuentas.",1,"E");
					muestraTablaErrores();
					return;
				}
			}
		    	
	}
	
	//si la solapa es la de Cotizaci�n
	if(tabActual==2) {
	    if (fncEsICB1()){
	        document.getElementById("td_Titulo").innerText = "Cotizaci�n Integral de Comercio Business Bank" 
				        	        
			/*			
			if (document.getElementById("CBO_PROVINCIA").value=="-1"){
				mostrarTab(aScreenTabs[tabAnterior],'','#9CBACE','');
				insertarError("", "CBO_PROVINCIA","Debe Seleccionar Provincia antes de pasar a la solapa Resultado Cotizaci�n.",2,"E")
				muestraTablaErrores();
				return;				
			}	*/
	    }    
	    else
			document.getElementById("td_Titulo").innerText = "Cotizaci�n "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
		
		 //GED - 11/07/2011 - CR5 PUNTO 5
	   if (document.getElementById("TIPOOPER").value == "R")
	   {
	   	if (!fncCheckAumentoSumasIncendioTodos())
	   	{
		   	confirm_customizado("<p align='center'>Recuerde actualizar las sumas aseguradas de Incendio  �Desea modificarlas? </p>") 		
			document.getElementById("funcion_confirma").value = "solapa2cancelar()"
			document.getElementById("funcion_cancela").value = "solapa2continuar()"
			return
	   	}
	   }
		
		
	  
	   
	
		if (fncEsICB1()){
			solapa2continuarICB1()
			//solapa2continuar()
		}	
		else{
	    	solapa2continuar()
	    }	
	    	
	   // FIN CR5
	}        
	
	//Solapa Datos del Cliente  
	if(tabActual==3)  
	{
		
			
		if (datosCotizacion.getCotizar())
		{
			mostrarTab(aScreenTabs[tabAnterior],'','#9CBACE','');
			insertarError("", "","Debe cotizar antes de pasar a la solapa Datos del Cliente.",2,"E")
			muestraTablaErrores()
			return
		}
			
		  aValidarSolapas[0]="N"
    		  aValidarSolapas[1]="N"
		  aValidarSolapas[2]="N"
		  aValidarSolapas[3]="S"
		  aValidarSolapas[4]="N"
		  aValidarSolapas[5]="N"
		  aValidarSolapas[6] = "N"
		  if (fncEsICB1())
		      document.getElementById("td_Titulo").innerText = "Cotizaci�n Integral de Comercio Business Bank"
		  else	    		
		      document.getElementById("td_Titulo").innerText = "Cotizaci�n "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
	  
	   //document.getElementById("divBtnRecuperarDatos").style.display='none' 	
	     if (document.getElementById("TIPOOPER").value == "R")
    			document.getElementById("divBtnRecuperarDatos").style.display='inline'
	   
	   	
	  if(datosCompletos)
	  {
	  	document.getElementById("divBtnAnterior").style.display='inline' 
	    		
			//Si va a revision
	    if (document.getElementById("chk_circuito").checked  || document.getElementById("divTablaFueraNorma").innerHTML!="")
	    {
	    	document.getElementById("divBtnEnvioRevision").style.display='inline';
	    	document.getElementById("divBtnEnvioEmail").style.display='none'
			}	else { //Si no pasa por revision
				document.getElementById("divBtnCompletarSolicitud").style.display='inline'
	    	document.getElementById("divBtnEnvioEmail").style.display='inline'	
			}
	    		
  		document.getElementById("divBtnGrabarCotizacion").style.display='inline'
  		document.getElementById("divBtnImprimirCotizacion").style.display='inline'	
  		document.getElementById("divCodigoColores").style.display='inline'
   	} else {
			mostrarTab(aScreenTabs[tabAnterior],'','#9CBACE','');
 		}
 		
 		//19/10/2010 LR Defect 21 Estetica, pto 35
		fncCheckNomCUITOpc();
	}

	//Solapa Datos Grales Solicitud
	if(tabActual==4)
	{
		aValidarSolapas[0]="N"
		aValidarSolapas[1]="N"
		aValidarSolapas[2]="N"
		aValidarSolapas[3]="N"
		aValidarSolapas[4]="S"
		aValidarSolapas[5]="N"
		aValidarSolapas[6] = "N"
		if (fncEsICB1())
		    document.getElementById("td_Titulo").innerText = "Solicitud Integral de Comercio Business Bank"
		else
		    document.getElementById("td_Titulo").innerText = "Solicitud "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
 		//	if (!mvarItemsParseados) fncParseaRiegosSoli()
	    	document.getElementById("divBtnRecuperarDatos").style.display='none'	
 		var varImgLeftOff = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_mi_lo.jpg";	
		document.getElementById('L0').src = varImgLeftOff;		    		
	    		
	  document.getElementById("vigenciaDesdeSoli").value = document.getElementById("vigenciaDesde").value
	  document.getElementById("vigenciaHastaSoli").value = document.getElementById("vigenciaHasta").value
	    		
	  document.getElementById("divBtnContinuar").style.display='inline'
	  document.getElementById("divTablaFueraNorma").style.display='none'
	    		
	  if (document.getElementById("COTISOLI").value != "V")
	  	document.getElementById("divBtnModificarCotizacion").style.display='inline'	
	  document.getElementById("divCodigoColores").style.display='inline'

		//actualizarListadoXML(datosCotizacion.getCanalURL()  + "Canales/" + datosCotizacion.getCanal() )
		 actualizarListadoXML(datosCotizacion.getCanalUrlImg() )		
	  //XML Objetos Asegurados
		fncInsertarObjAsegTotal();
	}
	    		
	if(tabActual==5)
	{
		aValidarSolapas[0]="N"
		aValidarSolapas[1]="N"
		aValidarSolapas[2]="N"
		aValidarSolapas[3]="N"
		aValidarSolapas[4]="N"
		aValidarSolapas[5]="S"
		aValidarSolapas[6] = "N"
		if (fncEsICB1())
		    document.getElementById("td_Titulo").innerText = "Solicitud Integral de Comercio Business Bank"
		else	
		    document.getElementById("td_Titulo").innerText = "Solicitud "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
	  	document.getElementById("divBtnRecuperarDatos").style.display='none'
	  
	   //alert(xmlItemNro)
		//if (!mvarItemsParseados) fncParseaRiegosSoli()
		for (var x=1; x<xmlItemNro; x++)
		{
			document.getElementById("calleRiesgoItemNro_"+ x +"_etiq").style.color = "#0000FF"
			document.getElementById("numeroRiesgoItemNro_"+ x +"_etiq").style.color = "#0000FF"
			document.getElementById("codposRiesgoItemNro_"+ x +"_etiq").style.color = "#0000FF"
			//FASE 2 todo
			if(document.getElementById("TIPOOPER").value == "R")
			{ if (!esItemNuevo(x)){
				document.getElementById("calleRiesgoItemNro_"+ x ).disabled=true
				document.getElementById("numeroRiesgoItemNro_"+ x ).disabled=true
				document.getElementById("pisoRiesgoItemNro_"+ x ).disabled=true
				document.getElementById("dptoRiesgoItemNro_"+ x ).disabled=true
				
			//	alert(document.getElementById("cboTipoObjetoNro_"+x +"_"+ "279" +"_"+ "1").value)
				}
					
			
			
				
			}
			
		}
		
		if (document.getElementById("COTISOLI").value != "V")
			document.getElementById("divBtnModificarCotizacion").style.display='inline'	
		document.getElementById("divBtnContinuar").style.display='inline'	    			
		document.getElementById("divBtnAnterior").style.display='inline'
		document.getElementById("divCodigoColores").style.display='inline'
		document.getElementById("divTablaFueraNorma").style.display='none'
	    		  
		// TODO:  llamar al callejero por cada domicilio en capital
		//fncCallejero('calleDomicilio', 'nroDomicilio', 'provinciaDomicilio', 'codPosDomicilio');
	    	
  	if (tabAnterior == tabActual)
		{
			if (!fncValidarObjAsegTotal())
				muestraTablaErrores();
		}
		 else 
		{	
			fncSumarObjetosAseguradosTotal();
			fncAyudaObjetos()
		}
	}	
	    	
	if(tabActual==6)
	{
  	if (!fncValidarObjAsegTotal())
		{
			mostrarTab(aScreenTabs[5],'','#9CBACE','');
			if (!fncValidarObjAsegTotal())
				muestraTablaErrores();
			return;
		}
	    		
		aValidarSolapas[0]="N"
		aValidarSolapas[1]="N"
		aValidarSolapas[2]="N"
		aValidarSolapas[3]="N"
		aValidarSolapas[4]="N"
		aValidarSolapas[5]="N"
		aValidarSolapas[6]="S"
		document.getElementById("divBtnRecuperarDatos").style.display = 'none'
		if (fncEsICB1())
		    document.getElementById("td_Titulo").innerText = "Solicitud Integral de Comercio Business Bank"
		else
		    document.getElementById("td_Titulo").innerText = "Solicitud "+txtRenov+" Integral de Comercio " +  datosCotizacion.getDesMonedaProducto()
		if (document.getElementById("COTISOLI").value != "V")
			document.getElementById("divBtnModificarCotizacion").style.display='inline'	
		document.getElementById("divBtnAnterior").style.display='inline'
		document.getElementById("divBtnConfirmarSolicitud").style.display='inline';
		document.getElementById("divCodigoColores").style.display='inline'
		document.getElementById("divTablaFueraNorma").style.display='none'
		//if (!mvarItemsParseados) fncParseaRiegosSoli()	 
		if (document.getElementById('legajoVendedor').value != "")  	
			fncBuscarVendedor(document.getElementById("INSTITUCION").value,  document.getElementById('cbo_vendSucursal').value, document.getElementById('legajoVendedor').value, document.getElementById("CANALHSBC").value)

		//actualizarListadoXML(datosCotizacion.getCanalURL()  + "Canales/" + datosCotizacion.getCanal() )
		 actualizarListadoXML(datosCotizacion.getCanalUrlImg() )			
		//XML Objetos Asegurados
		fncInsertarObjAsegTotal();	
		/*if (document.getElementById("RAMOPCODSQL").value=="ICB1")
			formaPagoSelFP();*/
	}
}
//CR5 PUNTO 5
function solapa2continuar()

{
	//lo que falta de solapa 2
	document.getElementById("circuitoRevision").style.display='none';
	 	aValidarSolapas[0]="S"
    		aValidarSolapas[1]="S"
		 aValidarSolapas[2]="S"
		 aValidarSolapas[3]="N"
		 aValidarSolapas[4]="N"
		 aValidarSolapas[5]="N"
		 aValidarSolapas[6]="N"
	 	document.getElementById("divBtnNuevaCotizacion").style.display='inline'
	 	document.getElementById("divBtnAnterior").style.display='inline'   
	 	document.getElementById("divBtnImprimirCotizacion").style.display='inline'	
	 	document.getElementById("divBtnEnvioEmail").style.display='inline'
	  	document.getElementById("divBtnGrabarIrSolicitud").style.display='inline'
	
	
	  if (document.getElementById("TIPOOPER").value == "R")
	   {
	   	document.getElementById("divBtnRecuperarDatos").style.display='inline'	   
	   }	
	   
	 	document.getElementById("resultadoCotizacion").innerHTML=""		
		
		
		var resultadoItems= validarItemsVacios() 	 	
	 	if (!resultadoItems) {return}
		
	 	var resultadoDatosGrales = validarDatosFormulario("true")
		
	 	if (!resultadoDatosGrales) {return}
		
		
	 	
	 	var oXMLItems = new ActiveXObject('MSXML2.DOMDocument');        
    		oXMLItems.async = false;       
      		oXMLItems.loadXML(document.getElementById("varXmlItems").value);       
      //		 window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+document.getElementById("varXmlItems").value+"</textarea>")	

      		
 		var oXMLRegItems = oXMLItems.selectNodes("//ITEM");
		var varCantItems = oXMLRegItems.length;   
      		
	 	if (resultadoDatosGrales && resultadoItems) 
	 	{
			
			if (datosCotizacion.getCotizar())
	 		{	
	 	
	 			for (var x=1; x<=varCantItems; x++)
				{						
					var flagErrorCotizar = false	
					if (getItemEstado(x) != "FN")			
					{
	 	
			   			if(!fncCotizar2480(x)) 		   		
			   			{
	 	
			   				flagErrorCotizar = true
			   				break
			   			}
						document.getElementById("chk_circuito").checked = false
						document.getElementById("chk_circuito").disabled = false					
					}			
					else
					{					
			   			flagErrorCotizar = false	
						document.getElementById("divBtnEnvioEmail").style.display = 'none'
					}					
				} // FIN X CADA ITEM			
				if (flagErrorCotizar)
				{					
					mostrarTab(aScreenTabs[tabAnterior],'','#9CBACE','');
					var nombreTorre=""//oXMLRegItems[x].selectSingleNode("ITEMDESC").text	
					insertarError( nombreTorre, "","Error al cotizar el item",2,"E")
					muestraTablaErrores()
				return
				}	

	 			fncCotizar()	// 2340
	 			datosCotizacion.setCotizar(false)
	 			datosCompletos = true
	 		}
	 		else
	 		{
	 			fncCotizar()	// 2340
	 			datosCompletos=true
	 			resultadoCotizacion()	
	 		}
	 	}
	 	if (fncLeyendaImpuestos()) 
	 	{
			document.getElementById("divLeyendaImpuestos").style.display = "";
		} else {
			document.getElementById("divLeyendaImpuestos").style.display = "none";
		}
	
	//
	}        
	
	
//CR5 PUNTO 5	
function solapa2cancelar()

{
	//regresa a la solapa anterior
	mostrarTab(aScreenTabs[1],'','#9CBACE','');
}



function fncParseaRiegosSoli()


{
	
	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	 var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;  		
	if(oXMLValores.loadXML( document.getElementById("varXmlItems").value)	)
	{
		//if(oXSLValores.load( datosCotizacion.getCanalURL() + "Forms/XSL/datosRiesgosSolicitud.xsl"))
		if(oXSLValores.loadXML(document.getElementById("datosRiesgosSolicitud").xml))
		{	
		//	window.document.body.insertAdjacentHTML("beforeEnd", "Itmes<textarea>"+document.getElementById("varXmlItems").value+"</textarea>")	
			var varResultTabla =  oXMLValores.transformNode(oXSLValores);
			document.getElementById("riesgosSolicitud").innerHTML = varResultTabla;
			//actualizarListadoXML(datosCotizacion.getCanalURL()  + "Canales/" + datosCotizacion.getCanal() )
			 actualizarListadoXML(datosCotizacion.getCanalUrlImg() )		
			//TODO: armar xml valida la solapa Riesgo Soli
			var domiItems ="<?xml version='1.0' encoding='ISO-8859-1'?><CAMPOS ><DOMICILIO>"
			for(var x = 0; x<oXMLValores.selectNodes("//ITEM").length;x++)	
			{
				 domiItems +=	'<CAMPO nombre="calleRiesgoItem_'+ Number(x+1) +'" resaltarCampo="S" obligatorio="S" obligatorioEstricto="S" tipo="text" longitud="60" longitudMinima="1" solapa="6"/>'+				
					'<CAMPO nombre="nroCalleRiesgoItem_'+ Number(x+1) +'" resaltarCampo="S" obligatorio="S" obligatorioEstricto="S" tipo="numero" longitud="6" longitudMinima="1" solapa="6"/>'
			}
				
			 domiItems +=	'</DOMICILIO></CAMPOS>'			 
			  var oXMLDomiItems= new ActiveXObject('MSXML2.DOMDocument');    
			  oXMLDomiItems.async = false;  		
			  oXMLDomiItems.loadXML( domiItems)			
			  mvarItemsParseados = true
			  document.getElementById("xmlValidaDomiciliosItem").XMLDocument=oXMLDomiItems
		}
		else
		{
			alert("Error xsl")
		}
	}
	else
	 {
	 	alert("Error xml")
	 }
			
}	

/******************************************************************************************/
function fncBuscarVendedor(pBancocod, pSucursal, pLegVend, pGrupoHSBC)
{
	//document.getElementById("I_LEGVEND").valido="false";
	document.getElementById("divEmpleado").innerHTML = "";
	//document.getElementById("CODEMP").value = "";
	//document.getElementById("TIPEMP").value = "";
	document.getElementById("NOMBREVEND").value = "";
	document.getElementById("APELLIDOVEND").value = "";
	if (pSucursal == -1)
	{
		if (tabActual == 6)
		{
			alert("Debe seleccionar la Sucursal a la cual pertenece el Vendedor");
			document.getElementById("cbo_vendSucursal").focus();
		}
		return;
	}

	if (pLegVend == '')
	{
		if (tabActual == 6)
		{
			alert("Debe ingresar el Legajo del Vendedor");
			document.getElementById("legajoVendedor").focus();
		}
		return;
	}

	//llamo al xmlhttp
	var DataToSend = "FUNCION=ValidarVendedorLegajoLBA&BANCOCOD=" + pBancocod +"&SUCURCOD=" + pSucursal +"&LEGAJNUM=" + pLegVend +"&GRUPOHSBC=" + pGrupoHSBC;
	//alert(DataToSend)
	var xmlhttp = new ActiveXObject("MSXML2.XMLHTTP");
	var i = 0;
	xmlhttp.Open("POST","DefinicionFrameWork.asp",false);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlhttp.send(DataToSend);
	var XmlDom  = new ActiveXObject("MSXML2.DOMDocument");

			
	XmlDom.loadXML(xmlhttp.responseText);
	//document.body.insertAdjacentHTML("beforeEnd", "vendedor: <textarea>"+xmlhttp.responseText+"</textarea>")
	if (XmlDom.parseError.errorCode != 0)
	{
		alert('Error en el XML: ' + XmlDom.parseError.reason);
		return false;
	}
	else
	{
		if (!XmlDom.selectNodes('LEGAJO'))
		{
			if (tabActual == 6)
			{
				alert("El empleado ingresado NO EXISTE");
				document.getElementById("legajoVendedor").focus();
			}
			return false;
		}

		//Si es empleado del grupo HSBC...
		if (pGrupoHSBC=="S") 
		{
			if (XmlDom.selectNodes('//LEGAJO').length==0)
			{
				if (tabActual == 6)
				{
					alert_customizado("El empleado ingresado NO EXISTE");
					document.getElementById("legajoVendedor").focus();
				}
				return false;											
			}
			else
			{
				var varOptions = '';
				for (i=0; i< XmlDom.selectSingleNode('//LEGAJOS').childNodes.length ; i++ )
				{	
					varOptions += "<option value='";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('LEG').text;
					varOptions += "' codemp='";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('COD_EMP').text;					
					varOptions += "' tipemp='";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('TIP_EMP').text;					
					varOptions += "' apellido='";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('APE').text;					
					varOptions += "' nombre='";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('NOM').text;					
					varOptions += "'>";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('APE').text;			
					varOptions += " ";
					varOptions += XmlDom.selectSingleNode('//LEGAJOS').childNodes[i].selectSingleNode('NOM').text;
					varOptions += "</option>";
				}			
				//document.getElementById("divEmpleado").innerHTML = "<select class='areaDatCpoConInpFieM' id='legajoVendedor' style='TEXT-TRANSFORM: uppercase;' onChange='muestraVendedor()'>" + varOptions + "</select>";
				//actualizarCodEmp(document.getElementById('I_VENDEDOR'));
				document.getElementById("divEmpleado").innerHTML = "<select class='form_camposimple' id='I_VENDEDOR' style='TEXT-TRANSFORM: uppercase;width:150px'  onChange='actualizarCodEmp(this);'>" + varOptions + "</select>";
				actualizarCodEmp(document.getElementById('I_VENDEDOR'));
				
				return true;
			}
		} else {	
		    if (!XmlDom.selectSingleNode('//APE'))
		    { 
				alert("El empleado ingresado NO EXISTE");
			    document.getElementById("legajoVendedor").focus();
			    return false;
		    }							
			//cargo el div con el nombre del empleado
			//document.getElementById("I_LEGVEND").valido="true";
			document.getElementById("divEmpleado").innerHTML = XmlDom.selectSingleNode('//APE').text + ' ' + XmlDom.selectSingleNode('//NOM').text;
			document.getElementById("NOMBREVEND").value = XmlDom.selectSingleNode('//APE').text;
			document.getElementById("APELLIDOVEND").value = XmlDom.selectSingleNode('//NOM').text;
			return true;
		}
	}
	return;
}


function actualizarCodEmp(pObjCodEmp)
{
	//alert(pObjCodEmp.options[pObjCodEmp.selectedIndex].codemp)
	
	document.getElementById("CODEMP").value = pObjCodEmp.options[pObjCodEmp.selectedIndex].codemp;
	//HRF 2010-11-01 Cambios Vendedor
	document.getElementById("NOMBREVEND").value = pObjCodEmp.options[pObjCodEmp.selectedIndex].nombre;
	document.getElementById("APELLIDOVEND").value = pObjCodEmp.options[pObjCodEmp.selectedIndex].apellido;	
	//Adriana 22-8-12   NUEVO PLATAFORMISTA
	document.getElementById("divPlataformista").style.display="none";
	//alert("Adriana ="+document.getElementById("APELLIDOVEND").value);
	
	if ((document.getElementById("APELLIDOVEND").value=='NUEVO' ) && (document.getElementById("NOMBREVEND").value=='PLATAFORMISTA' )&& (document.getElementById("CANALHSBC").value == "S"))
	{
		document.getElementById("divPlataformista").style.display="inline";
	}
	else	{
		document.getElementById("nombrePlat").value= "";
		document.getElementById("apellidoPlat").value= "";
		}
	
}


function fncGuardarCliente()
{
	//Persona
	var APELLIDO =trim(document.getElementById("apellidoCliente").value)
	if (APELLIDO.length>20)
	{
		var APELLIDO1= APELLIDO.substr(0,20);
		var APELLIDO2 = APELLIDO.substr(20);
	}	else	{
		var APELLIDO1= APELLIDO;
		var APELLIDO2 = "";
	}
	var PERSOTIP = document.getElementById("tipoPersona").value=='00'?'F':'J';
	var CLIENTIP = document.getElementById("tipoPersona").value=="-1"?"0":document.getElementById("tipoPersona").value;
	var FECNACIM = document.getElementById("fechaNacimientoClienteSoli").value.length<8?"0/0/0":document.getElementById("fechaNacimientoClienteSoli").value;
	var CLIENSEX = document.getElementById("sexoCliente").value=="-1"?"0":document.getElementById("sexoCliente").value;
	var ESTCIVIL = document.getElementById("estadoCivilCliente").value=="-1"?"0":document.getElementById("estadoCivilCliente").value;
	var PAISNACI = document.getElementById("paisNacimientoCliente").value=="-1"?"0":document.getElementById("paisNacimientoCliente").value;
	
	
	//Domicilio
	var PROVICOD = document.getElementById("provinciaDomicilio").value!="0"?document.getElementById("provinciaDomicilio").value:"0";
	var DOMICCPO = trim(document.getElementById("codPosDomicilio").value)!=""?trim(document.getElementById("codPosDomicilio").value):"0";
	var DOMICPOB = document.getElementById("localidadDomicilio").value;
	//Cuenta
	var COBROTIP = document.getElementById("formaPago").value.split("|")[1];
	var BANCOCOD = document.getElementById("BANCOCOD").value==""?"0":document.getElementById("BANCOCOD").value;
	var SUCURCOD = 0;
	var CUENNUME = '';
	var TARJECOD = 0;
	var VENCIANN = 0;
	var VENCIMES = 0;
	var VENCIDIA = 0;
	if (document.getElementById("seccion_Tarjeta").style.display!="none")
	{	COBROTIP = document.getElementById("cbo_nomTarjeta").selectedIndex==0?COBROTIP:document.getElementById("cbo_nomTarjeta").value.split("|")[1];
		TARJECOD = document.getElementById("cbo_nomTarjeta").selectedIndex==0?0:document.getElementById("cbo_nomTarjeta").value.split("|")[0];
		CUENNUME = document.getElementById("numTarjeta").value;
		VENCIANN = document.getElementById("tarvtoann").value;
		VENCIMES = document.getElementById("tarvtomes").value;
		VENCIDIA = 1; }
	else if (document.getElementById("seccion_PagoCuenta").style.display!="none")
	{	SUCURCOD = document.getElementById("cboSucursalDC").selectedIndex==0?0:document.getElementById("cboSucursalDC").value;
		CUENNUME = document.getElementById("numCuentaDC").value; }
	else if (document.getElementById("divNroCBU").style.display!="none")
	{	CUENNUME = document.getElementById("numCBU").value; }
	
	var strData = "FUNCION=GUARDARCLIENTE&";
	//Persona
	strData= strData + "TIPODOCU=" + document.getElementById("tipoDocCliente").value + "&";
	strData= strData + "NUMEDOCU=" + trim(document.getElementById("nroDocumentoCliente").value) + "&";
	strData= strData + "USUARCOD=" + document.getElementById("LOGON_USER").value + "&";
	strData= strData + "IDCLIENTE=" + document.getElementById("IDCLIENTE").value + "&";
	strData= strData + "CLIENNOM=" + trim(document.getElementById("nombreCliente").value.toUpperCase().replace(/&/g,"~~")) + "&";
	strData= strData + "CLIENAP1=" + APELLIDO1.toUpperCase().replace(/&/g,"~~") + "&";
	strData= strData + "CLIENAP2=" + APELLIDO2.toUpperCase().replace(/&/g,"~~") + "&";
	strData= strData + "NACIMANN=" + FECNACIM.split("/")[2] + "&";
	strData= strData + "NACIMMES=" + FECNACIM.split("/")[1] + "&";
	strData= strData + "NACIMDIA=" + FECNACIM.split("/")[0] + "&";
	strData= strData + "CLIENSEX=" + CLIENSEX + "&";
	strData= strData + "CLIENEST=" + ESTCIVIL + "&";
	strData= strData + "PAISSCOD=" + PAISNACI + "&";
	strData= strData + "PERSOTIP=" + PERSOTIP + "&";	
	strData= strData + "CLIENTIP=" + CLIENTIP + "&";
	strData= strData + "AGENTCLA=" + document.getElementById("I_AGENTE").value.split("|")[1]  + "&";
	strData= strData + "AGENTCOD=" + document.getElementById("I_AGENTE").value.split("|")[0]  + "&";
	//Domicilio
	strData= strData + "DOMICDOM=" + trim(document.getElementById("calleDomicilio").value) + "&";
	strData= strData + "DOMICDNU=" + trim(document.getElementById("nroDomicilio").value) + "&";
	strData= strData + "DOMICPIS=" + trim(document.getElementById("pisoDomicilio").value) + "&";
	strData= strData + "DOMICPTA=" + trim(document.getElementById("dptoDomicilio").value) + "&";
	strData= strData + "PROVICOD=" + PROVICOD + "&";
	strData= strData + "DOMICPOB=" + DOMICPOB + "&";
	strData= strData + "DOMICCPO=" + DOMICCPO + "&";
	strData= strData + "TELEFONOCOD=" + document.getElementById("codTelefonoCliente").value + "&";
	strData= strData + "TELEFONONRO=" + document.getElementById("telefonoCliente").value + "&";
	strData= strData + "EMAIL=" + document.getElementById("emailCliente").value + "&";	
	//Cuenta
	strData= strData + "GRABACTA=S&";
	strData= strData + "COBROTIP=" + COBROTIP + "&";
	strData= strData + "BANCOCOD=" + BANCOCOD + "&";
	strData= strData + "SUCURCOD=" + SUCURCOD + "&";
	strData= strData + "CUENNUME=" + CUENNUME + "&";
	strData= strData + "TARJECOD=" + TARJECOD + "&";
	strData= strData + "VENCIANN=" + VENCIANN + "&";
	strData= strData + "VENCIMES=" + VENCIMES + "&";
	strData= strData + "VENCIDIA=" + VENCIDIA;
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "CLIENTE<textarea>"+strData+"</textarea>")
		
	xmlHTTPCotiza = new ActiveXObject("Msxml2.XMLHTTP");		
	xmlHTTPCotiza.open("POST","DefinicionFrameWork.asp", false);
	xmlHTTPCotiza.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHTTPCotiza.send(encodeURI(strData)); 	
	
	var mvarIdCliente = xmlHTTPCotiza.responseText;
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarIdCliente+"</textarea>")
	if (mvarIdCliente == "ERROR" || isNaN(mvarIdCliente))
	{
		alert_customizado("El cliente no pudo ser guardado, intente nuevamente...");
		return 0;
	}	else {
		document.getElementById("IDCLIENTE").value = mvarIdCliente;
		return parseInt(mvarIdCliente);
	}
}

function fncGuardarApoderado()
	{var apellido =trim(document.getElementById("apoApellido").value)
	if (apellido.length>20)
	{	
		var apellido1= apellido.substr(0,20)
		var apellido2 = apellido.substr(20)
	}
	else
	{
		var apellido1= apellido
		var apellido2 = ""
	}
	
	var annHoy= document.getElementById("fechaHoy").value.substr(0,4)
	var mesHoy=document.getElementById("fechaHoy").value.substr(4,2)
	var diaHoy= document.getElementById("fechaHoy").value.substr(6,2)
	var persoTip= document.getElementById("cboApoTipDoc").value!='4'?'F':'J'
	//var cliensec= 0
	
	
	
		var  PROVICOD =document.getElementById("apoProvinciaDomicilio").value!="0"?document.getElementById("apoProvinciaDomicilio").value:"0"
		var  DOMICCPO = trim(document.getElementById("apoCodPosDomicilio").value)!=""?trim(document.getElementById("apoCodPosDomicilio").value):"0"
		var  DOMICPOB = document.getElementById("apoLocalidadDomicilio").value
		var tipoPersona= document.getElementById("cboApoTipDoc").value!="4"?"00":"15"
		var fechaNacim =  document.getElementById("apoFechaNacimiento").value
		
		var cliensex =  document.getElementById("apoSexo").value=="-1"?"0":document.getElementById("apoSexo").value
		var estadoCivilCliente =  document.getElementById("apoEstadoCivil").value=="-1"?"0":document.getElementById("apoEstadoCivil").value
		var paisNacimientoCliente =  document.getElementById("apoPais").value=="-1"?"0":document.getElementById("apoPais").value
		
		
		var strData = "FUNCION=GUARDARCLIENTE&";
		strData= strData + "TIPODOCU=" + document.getElementById("cboApoTipDoc").value + "&";
		strData= strData + "NUMEDOCU=" + trim(document.getElementById("apoNroDoc").value) + "&";
		strData= strData + "USUARCOD=" + document.getElementById("LOGON_USER").value + "&";
		strData= strData + "IDCLIENTE=" + document.getElementById("CLIENSECAPO").value + "&";
		strData= strData + "CLIENNOM=" + trim(document.getElementById("apoNombre").value.toUpperCase()) + "&";
		strData= strData + "CLIENAP1=" + apellido1.toUpperCase() + "&";
		strData= strData + "CLIENAP2=" + apellido2.toUpperCase() + "&";
		strData= strData + "NACIMANN=" + parteFecha(fechaNacim,"DD/MM/AAAA","AAAA") + "&";
		strData= strData + "NACIMMES=" + parteFecha(fechaNacim,"DD/MM/AAAA","MM") + "&";
		strData= strData + "NACIMDIA=" + parteFecha(fechaNacim,"DD/MM/AAAA","DD") + "&";		
		strData= strData + "CLIENSEX=" + cliensex + "&";
		strData= strData + "CLIENEST=" + estadoCivilCliente+ "&";
		strData= strData + "PAISSCOD=" + paisNacimientoCliente + "&";
		strData= strData + "AGENTCLA=" + document.getElementById("I_AGENTE").value.split("|")[1]  + "&";
		strData= strData + "AGENTCOD=" + document.getElementById("I_AGENTE").value.split("|")[0]  + "&";
		
		strData= strData + "DOMICDOM=" + trim(document.getElementById("apoCalleDomicilio").value) + "&";
		
		strData= strData + "DOMICDNU=" + trim(document.getElementById("apoNroDomicilio").value) + "&";
		strData= strData + "DOMICPIS=" + trim(document.getElementById("apoPisoDomicilio").value) + "&";
		strData= strData + "DOMICPTA=" + trim(document.getElementById("apoDptoDomicilio").value) + "&";		
		strData= strData + "PROVICOD=" + PROVICOD + "&";		
		strData= strData + "DOMICPOB=" + DOMICPOB + "&";
		strData= strData + "DOMICCPO=" + DOMICCPO + "&";
		strData= strData + "TELEFONOCOD=" + document.getElementById("apoCodTelefono").value + "&";
		strData= strData + "TELEFONONRO=" + document.getElementById("apoTelefono").value + "&";
		strData= strData + "EMAIL=" + document.getElementById("apoMail").value + "&";	
		strData= strData + "PERSOTIP=" + persoTip + "&";	
		strData= strData + "CLIENTIP=" + tipoPersona;
		//document.body.insertAdjacentHTML("beforeEnd", "apoderado:<textarea>"+strData+"</textarea>")
		
		xmlHTTPCotiza = new ActiveXObject("Msxml2.XMLHTTP");		
		xmlHTTPCotiza.open("POST","DefinicionFrameWork.asp", false);
		xmlHTTPCotiza.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xmlHTTPCotiza.send(encodeURI(strData)); 	
		//xmlHTTPCotiza.send(ReplaceChar(strData," "));
		//alert(xmlHTTPCotiza.responseText);
		
		var  mvarIdCliente= xmlHTTPCotiza.responseText;
		//document.body.insertAdjacentHTML("beforeEnd", "apoderado:<textarea>"+mvarIdCliente+"</textarea>")
		
		
		if (mvarIdCliente == "ERROR" || isNaN(mvarIdCliente))
		{
			alert_customizado("El apoderado no pudo ser guardado, intente nuevamente...");
			return 0;
		}
		else
		{
			document.getElementById("CLIENSECAPO").value = mvarIdCliente;
			return parseInt(mvarIdCliente);
		}
	}
	



function fncGuardarCotizacion(pCliensec,pAposec,pEstado, pOperacion,pPopUp)
	{ 
	//try{
	
	oXMLItems = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLItems.async = false;       
	oXMLItems.loadXML(document.getElementById("varXmlItems").value);
      		
	oXMLObjAse = new ActiveXObject('MSXML2.DOMDocument');
	oXMLObjAse.async = false;
	oXMLObjAse.loadXML(document.getElementById("varXmlObjAseg").value);
      
 		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea style='position:absolute; z-index:100'>"+document.getElementById("varXmlItems").value+"</textarea>")
	      		
      		var oXMLRegItems = oXMLItems.selectNodes("//ITEM");
      		var varCantItems = oXMLRegItems.length;      		
		
		//document.getElementById('xmlDatosCotizacion').XMLDocument;
		var mvarCertisec =  document.getElementById("CERTISEC").value
		var mvarCertipol =   mvarCertisec==0?document.getElementById("BANCOCOD").value:document.getElementById("CERTIPOL").value
		document.getElementById("CERTIPOL").value = mvarCertipol
		
		var mvarIIBB= document.getElementById("IngresosBrutos").value=="-1"?"0":document.getElementById("IngresosBrutos").value
		var tipoPersona= document.getElementById("tipoPersona").value=="-1"?"0":document.getElementById("tipoPersona").value
	
		var cliensecapo = document.getElementById("CLIENSECAPO").value==""?"0":document.getElementById("CLIENSECAPO").value
		
		if (document.getElementById("RAMOPCODSQL").value=="ICB1")
			var mvarRamopcod = document.getElementById("RAMOPCODSQL").value
		else
			var mvarRamopcod = document.getElementById("RAMOPCOD").value
        
		var xmlDatosCotizacion = " " +
		"<RAMOPCOD>"+ mvarRamopcod +"</RAMOPCOD>"+
		"<CERTIPOL>"+ mvarCertipol+"</CERTIPOL>"+
		"<CERTIANN>0</CERTIANN>" +
		"<CERTISEC>" + mvarCertisec + "</CERTISEC>" +
		"<EFECTANN>"+document.getElementById("fechaHoy").value.substr(0,4)+"</EFECTANN>"+
		"<EFECTMES>"+document.getElementById("fechaHoy").value.substr(4,2)+"</EFECTMES>" +
		"<EFECTDIA>"+document.getElementById("fechaHoy").value.substr(6,2)+"</EFECTDIA>" +
		"<USUARCOD>"+document.getElementById("LOGON_USER").value+"</USUARCOD>" +
		"<SITUCPOL>"+pEstado+"</SITUCPOL>"+  
		"<CLIENSEC>"+document.getElementById("IDCLIENTE").value +"</CLIENSEC>"+		
		"<DOMICSEC>1</DOMICSEC>"+
		"<CUENTSEC>1</CUENTSEC>"+
		
		"<SWAPODER>" +document.getElementById("tieneApoderado").value+ "</SWAPODER>"+
		"<OCUPACODCLIE>" +document.getElementById("actividadesCliente").value+ "</OCUPACODCLIE>"+
		"<OCUPACODAPOD>" +document.getElementById("apoActividad").value+ "</OCUPACODAPOD>"+
		
		"<CLIENSECAPOD>"+pAposec+"</CLIENSECAPOD>"+
		"<DOMISECAPOD>1</DOMISECAPOD>"+
		"<NCUITCLIE>" +document.getElementById("nroCuit").value+ "</NCUITCLIE>"+
		"<NCUITAPOD>" +document.getElementById("apoCuit").value+ "</NCUITAPOD>"+
		
		"<TIPOOPER>"+pOperacion+"</TIPOOPER>"
		

		if(mvarCertisec==0)
		{
			xmlDatosCotizacion+="<FECCOTIALTANN>"+document.getElementById("fechaHoy").value.substr(0,4)+"</FECCOTIALTANN>"+
			"<FECCOTIALTMES>"+document.getElementById("fechaHoy").value.substr(4,2)+"</FECCOTIALTMES>"+
			"<FECCOTIALTDIA>"+document.getElementById("fechaHoy").value.substr(6,2)+"</FECCOTIALTDIA>"+
			"<FECCOTIULTANN>"+document.getElementById("fechaHoy").value.substr(0,4)+"</FECCOTIULTANN>"+
			"<FECCOTIULTMES>"+document.getElementById("fechaHoy").value.substr(4,2)+"</FECCOTIULTMES>"+
			"<FECCOTIULTDIA>"+document.getElementById("fechaHoy").value.substr(6,2)+"</FECCOTIULTDIA>"		
			document.getElementById("fechaAltaCoti").value=  document.getElementById("fechaHoy").value
			document.getElementById("fechaUltimaCoti").value=  document.getElementById("fechaHoy").value
		}
		else
		{
			xmlDatosCotizacion+="<FECCOTIALTANN>"+document.getElementById("fechaAltaCoti").value.substr(0,4)+"</FECCOTIALTANN>"+
			"<FECCOTIALTMES>"+document.getElementById("fechaAltaCoti").value.substr(4,2)+"</FECCOTIALTMES>"+
			"<FECCOTIALTDIA>"+document.getElementById("fechaAltaCoti").value.substr(6,2)+"</FECCOTIALTDIA>"+
			"<FECCOTIULTANN>"+document.getElementById("fechaUltimaCoti").value.substr(0,4)+"</FECCOTIULTANN>"+
			"<FECCOTIULTMES>"+document.getElementById("fechaUltimaCoti").value.substr(4,2)+"</FECCOTIULTMES>"+
			"<FECCOTIULTDIA>"+document.getElementById("fechaUltimaCoti").value.substr(6,2)+"</FECCOTIULTDIA>"		
		
		}
		
		if (pEstado=="Y")  // Si es solicitud
		{
		xmlDatosCotizacion+="<FECSOLICANN>"+document.getElementById("fechaHoy").value.substr(0,4)+"</FECSOLICANN>"
		xmlDatosCotizacion+="<FECSOLICMES>"+document.getElementById("fechaHoy").value.substr(4,2)+"</FECSOLICMES>" //Fecha dia
		xmlDatosCotizacion+="<FECSOLICDIA>"+document.getElementById("fechaHoy").value.substr(6,2)+"</FECSOLICDIA>"
		
		xmlDatosCotizacion+="<FECINIVGANN>"+parteFecha(document.getElementById("vigenciaDesdeSoli").value,"DD/MM/AAAA","AAAA")+"</FECINIVGANN>"  //Viene de la pantalla
		xmlDatosCotizacion+="<FECINIVGMES>"+parteFecha(document.getElementById("vigenciaDesdeSoli").value,"DD/MM/AAAA","MM")+"</FECINIVGMES>"
		xmlDatosCotizacion+="<FECINIVGDIA>"+parteFecha(document.getElementById("vigenciaDesdeSoli").value,"DD/MM/AAAA","DD")+"</FECINIVGDIA>"
		
		xmlDatosCotizacion+="<FECFINVGANN>"+parteFecha(document.getElementById("vigenciaHastaSoli").value,"DD/MM/AAAA","AAAA")+"</FECFINVGANN>"  //Pantalla
		xmlDatosCotizacion+="<FECFINVGMES>"+ parteFecha(document.getElementById("vigenciaHastaSoli").value,"DD/MM/AAAA","MM") +"</FECFINVGMES>"
		xmlDatosCotizacion+="<FECFINVGDIA>"+ parteFecha(document.getElementById("vigenciaHastaSoli").value,"DD/MM/AAAA","DD") +"</FECFINVGDIA>"
		}
		else
		{
		xmlDatosCotizacion+="<FECSOLICANN>0</FECSOLICANN>"
		xmlDatosCotizacion+="<FECSOLICMES>0</FECSOLICMES>" 
		xmlDatosCotizacion+="<FECSOLICDIA>0</FECSOLICDIA>"
		
		xmlDatosCotizacion+="<FECINIVGANN>0</FECINIVGANN>"  
		xmlDatosCotizacion+="<FECINIVGMES>0</FECINIVGMES>"
		xmlDatosCotizacion+="<FECINIVGDIA>0</FECINIVGDIA>"
		
		xmlDatosCotizacion+="<FECFINVGANN>0</FECFINVGANN>" 
		xmlDatosCotizacion+="<FECFINVGMES>0</FECFINVGMES>"
		xmlDatosCotizacion+="<FECFINVGDIA>0</FECFINVGDIA>"
		}
		

		xmlDatosCotizacion+="<AGENTCLAPR>"+document.getElementById("I_AGENTE").value.split("|")[1]+"</AGENTCLAPR>"+
		"<AGENTCODPR>"+document.getElementById("I_AGENTE").value.split("|")[0] +"</AGENTCODPR>"+
		"<AGENTNOMPR><![CDATA["+document.getElementById("I_AGENTE").value.split("|")[2]+"]]></AGENTNOMPR>"
		
		xmlDatosCotizacion+="<AGENTCLAPR2></AGENTCLAPR2>"+
		"<AGENTCODPR2>0</AGENTCODPR2>"+
		"<AGENTNOMPR2></AGENTNOMPR2>"
		
		xmlDatosCotizacion+="<AGENTCLAOR>"+document.getElementById("I_ORGANIZADOR").value.split("|")[1] +"</AGENTCLAOR>"+
		"<AGENTCODOR>"+document.getElementById("I_ORGANIZADOR").value.split("|")[0] +"</AGENTCODOR>"+
		"<AGENTNOMORG><![CDATA["+document.getElementById("I_ORGANIZADOR").value.split("|")[2] +"]]></AGENTNOMORG>"
		xmlDatosCotizacion+="<COMISIONPR1>"+document.getElementById("COMISION_PROD").value+"</COMISIONPR1>"+ 
		"<COMISIONPR2>0</COMISIONPR2>"
		xmlDatosCotizacion+="<COMISIONOR>"+document.getElementById("COMISION_ORG").value+"</COMISIONOR>"
		xmlDatosCotizacion+="<CONVENIOPR>"+document.getElementById("convenioProductor").value.split("|")[0]+"</CONVENIOPR>"
		xmlDatosCotizacion+="<CONVENIOOR>"+document.getElementById("convenioOrganizador").value.split("|")[0]+"</CONVENIOOR>"
		xmlDatosCotizacion+="<COMISTOT>"+ document.getElementById("comisionTotal").value+"</COMISTOT>"
		xmlDatosCotizacion+="<PERIODO>"+document.getElementById("periodoCotizar").value+"</PERIODO>"		
		xmlDatosCotizacion+="<ESTABILIZACION>"+document.getElementById("estabilizacion").value+"</ESTABILIZACION>"
		xmlDatosCotizacion+="<CLIENIVA>"+document.getElementById("IVA").value+"</CLIENIVA>"
		xmlDatosCotizacion+="<TIPO_PERSONA>"+ tipoPersona +"</TIPO_PERSONA>"
		xmlDatosCotizacion+="<CATEGORIA_IIBB>"+mvarIIBB+"</CATEGORIA_IIBB>"
		xmlDatosCotizacion+="<CANTRIESGOS>" + varCantItems + "</CANTRIESGOS>"
		xmlDatosCotizacion+="<COBROCOD>"+document.getElementById("formaPago").value.split("|")[0]+"</COBROCOD>"
		xmlDatosCotizacion+="<PLANPCOD>"+document.getElementById("planPago").value.split("|")[0]+"</PLANPCOD>"
		xmlDatosCotizacion+="<COBROTIP>"+document.getElementById("formaPago").value.split("|")[1]+"</COBROTIP>"
		

		if (document.getElementById("RAMOPCODSQL").value=="ICB1")
			xmlDatosCotizacion+="<NRO_INTERNO_SOLI>"+document.getElementById("cboPlanICOBB").value.split("|")[0]+"</NRO_INTERNO_SOLI>"
		else
			xmlDatosCotizacion+="<NRO_INTERNO_SOLI>"+document.getElementById("nroSolicitud").value.toUpperCase() +"</NRO_INTERNO_SOLI>"
		
		xmlDatosCotizacion+="<VEND_BANCO>"+document.getElementById("BANCOCOD").value +"</VEND_BANCO>"
		

		var vendSucursal=document.getElementById("cbo_vendSucursal").value!="-1"?document.getElementById("cbo_vendSucursal").value:""
		xmlDatosCotizacion+="<VEND_SUCURSAL>"+vendSucursal +"</VEND_SUCURSAL>"
		xmlDatosCotizacion+="<VEND_EMPRESA>"+document.getElementById("CODEMP").value+"</VEND_EMPRESA>"
		xmlDatosCotizacion+="<VEND_LEGAJO>"+document.getElementById("legajoVendedor").value +"</VEND_LEGAJO>"
		
		var wRevision = document.getElementById("chk_circuito").checked?"S":"N";
		auxXml = new ActiveXObject('MSXML2.DOMDocument');
		auxXml.async = false;
		auxXml.loadXML(document.getElementById("xmlFueraNorma").value);		
		var wFueraNorma = auxXml.selectNodes("//ERROR").length==0?"N":"S";
		var wConformEMail = document.getElementById("seccion_ConformEMail").style.display=="inline"?document.getElementById("cboConformEMail").value:"";
	
		xmlDatosCotizacion+=
			"<EJEC_PPLSOFT>"+ document.getElementById("EJEC_PPLSOFT").value +"</EJEC_PPLSOFT>"+
			"<EJEC_EMAIL>"+ document.getElementById("EJEC_EMAIL").value +"</EJEC_EMAIL>"+
			"<CONFORM_EMAIL>"+ wConformEMail +"</CONFORM_EMAIL>"+
			"<SOLICITA_REVISION>"+ wRevision +"</SOLICITA_REVISION>"+
			"<FUERA_NORMA>"+ wFueraNorma +"</FUERA_NORMA>"
		

		if (document.getElementById("RAMOPCODSQL").value=="ICB1")
			var mvarActividad=document.getElementById("cboActividadICOBB").value
		else
			var mvarActividad=document.getElementById("cboActividadComercio").value.split("|")[0]
				
		xmlDatosCotizacion+="<ACTIVIDAD>"+mvarActividad+"</ACTIVIDAD>"
		
		var wPOLIZANNANT = 	document.getElementById("POLIZANNANT").value==""?"0":document.getElementById("POLIZANNANT").value;
		var wPOLIZSECANT = 	document.getElementById("POLIZSECANT").value==""?"0":document.getElementById("POLIZSECANT").value;			
		xmlDatosCotizacion+="<POLIZANNANT>"+ wPOLIZANNANT +"</POLIZANNANT>";
		xmlDatosCotizacion+="<POLIZSECANT>"+ wPOLIZSECANT +"</POLIZSECANT>"
		xmlDatosCotizacion+="<REN_MODIF_DAT>"+ document.getElementById("REN_MODIF_DAT").value +"</REN_MODIF_DAT>"
		xmlDatosCotizacion+="<REN_COTIZ_NUE>"+ document.getElementById("REN_COTIZ_NUE").value +"</REN_COTIZ_NUE>"		
		xmlDatosCotizacion+="<OPERAPOL>"+document.getElementById("OPERAPOLAIS").value+"</OPERAPOL>"
		
		//ADRIANA 19-4-2011 ticket 599651 SE AGREGA EL CAMPO SWPRIMIN
		xmlDatosCotizacion+="<PRIMAIMP>"+ document.getElementById("PRIMAIMP").value +"</PRIMAIMP>"+
		"<DEREMIMP>"+ document.getElementById("DEREMIMP").value +"</DEREMIMP>"+
		"<RECAIMPO>"+ document.getElementById("RECAIMPO").value +"</RECAIMPO>"+
		"<RECFIMPO>"+ document.getElementById("RECFIMPO").value +"</RECFIMPO>"+
		"<TASUIMPO>"+ document.getElementById("TASUIMPO").value +"</TASUIMPO>"+
		"<IVAIMPOR>"+ document.getElementById("IVAIMPOR").value +"</IVAIMPOR>"+
		"<IVAIBASE>"+ document.getElementById("IVAIBASE").value +"</IVAIBASE>"+
		"<IVAIMPOA>"+ document.getElementById("IVAIMPOA").value +"</IVAIMPOA>"+
		"<IVAABASE>"+ document.getElementById("IVAABASE").value +"</IVAABASE>"+
		"<IVARETEN>"+ document.getElementById("IVARETEN").value +"</IVARETEN>"+
		"<IVARBASE>"+ document.getElementById("IVARBASE").value +"</IVARBASE>"+
		"<IMPUEIMP>"+ document.getElementById("IMPUEIMP").value +"</IMPUEIMP>"+
		"<SELLAIMP>"+ document.getElementById("SELLAIMP").value +"</SELLAIMP>"+
		"<INGBRIMP>"+ document.getElementById("INGBRIMP").value +"</INGBRIMP>"+
		"<RECTOIMP>"+ document.getElementById("RECTOIMP").value +"</RECTOIMP>"+
		"<SWPRIMIN>"+ document.getElementById("SWPRIMIN").value +"</SWPRIMIN>"+
		"<COMISIMP>"+ document.getElementById("COMISIMP").value +"</COMISIMP>"
		//LR 05/01/2011 Enviar los recargos Administrativo y Financiero(RECADMIN/RECFINAN)
		xmlDatosCotizacion+="<RECADMIN>"+ document.getElementById("RECARGO").value +"</RECADMIN>"
		xmlDatosCotizacion+="<RECFINAN>"+ document.getElementById("planPago").value.split("|")[1] +"</RECFINAN>"+
		"<ANOTACIONES><ROOT>"+		
			"<REG IT='0'  NR='1'  TI='A' TX='"+ Base64.encode(document.getElementById('observClausulas').value) +"'  />"+
			"<REG IT='0'  NR='2'  TI='F' TX='"+ Base64.encode(document.getElementById('textoPoliza').value)  +"'   />"+
		"</ROOT></ANOTACIONES>"

		

		//if (pEstado=="Y")
		//{
			/*for (var x=0; x<varCantItems; x++)
			{
			matrizCalle[x]=document.getElementById("calleRiesgoItem_" + eval(x+1) )?document.getElementById("calleRiesgoItem_" + eval(x+1) ).value:""
			matrizNroCalle[x]= document.getElementById("nroCalleRiesgoItem_" + eval(x+1) )?document.getElementById("nroCalleRiesgoItem_" + eval(x+1) ).value:""
			matrizCodPostal[x] = document.getElementById("codPostalRiesgoItem_" + eval(x+1) )?document.getElementById("codPostalRiesgoItem_" + eval(x+1) ).value:""
			matrizPiso[x] = document.getElementById("pisoRiesgoItem_" + eval(x+1) )?document.getElementById("pisoRiesgoItem_" + eval(x+1) ).value:""
			matrizDpto[x] = document.getElementById("dptoRiesgoItem_" + eval(x+1) )?document.getElementById("dptoRiesgoItem_" + eval(x+1) ).value:""
			
			}*/
		//}
		/* ITEMS ico
		IT	numeric(4), Nro. Item
		PR	decimal(2), Cod. Provincia
		LC	varchar(90), Localidad base64
		CA varchar(60), calle base64
		PI varchar(4) Piso
		DP varchar(4) Depto
		AC  numeric(5,0)  Cant. alumnos o carteles
		CP	varchar(8),  Codigo Postal
		AL	decimal(2), alarma
		GU	decimal(2) guardia
		*/
		
		xmlDatosCotizacion+="<ITEMS>"+
		"<ROOT>"
			for(var x=0; x<varCantItems; x++)
			{
				var provincia=oXMLRegItems[x].selectSingleNode("PROVICOD").text
				var localidad=  oXMLRegItems[x].selectSingleNode("DOMICPOB").text 
				//var cpostal= provincia!="1"?oXMLRegItems[x].selectSingleNode("CPOSDOM").text:matrizCodPostal[x]  // guardo "" en vez de 1005
				var cpostal= oXMLRegItems[x].selectSingleNode("CPOSDOM").text
				var calle=oXMLRegItems[x].selectSingleNode("DOMICDOM").text
				var nroCalle=oXMLRegItems[x].selectSingleNode("DOMICDNU").text
				var piso=oXMLRegItems[x].selectSingleNode("DOMICPIS").text
				var dpto=oXMLRegItems[x].selectSingleNode("DOMICPTA").text
				var alarma= oXMLRegItems[x].selectSingleNode("ALARMA").text
				var guardia=oXMLRegItems[x].selectSingleNode("GUARDIA").text
				
				var cpa=  oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text != "" ? oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text : "0"	//CERTIPOLANT
				var caa=oXMLRegItems[x].selectSingleNode("CERTIANNANT").text != "" ? oXMLRegItems[x].selectSingleNode("CERTIANNANT").text : "0" 		//CERTIANNANT
				var csa=oXMLRegItems[x].selectSingleNode("CERTISECANT").text  	!= "" ? oXMLRegItems[x].selectSingleNode("CERTISECANT").text : "0"		//CERTISECANT
				
				/*var calle = matrizCalle[x]
				var nroCalle = matrizNroCalle[x]
				var piso = matrizPiso[x]
				var dpto = matrizDpto[x]*/
				var cantAlumnos = oXMLRegItems[x].selectSingleNode("CANTALUM").text==""?0:oXMLRegItems[x].selectSingleNode("CANTALUM").text //TODO: crear campo front y pasar valor
				if(cantAlumnos=='') cantAlumnos=0
				xmlDatosCotizacion+="<REG IT='"+eval(x+1)+				
				"'  PR='"+provincia+
				"' LC='"+Base64.encode(localidad)+
				"' CA='"+Base64.encode(calle)+
				"' NU='"+nroCalle+
				"' PS='"+piso+
				"' DP='"+dpto+
				"' AC='"+cantAlumnos+
				"' CP='"+cpostal+
				"' AL='"+alarma+
				"'  GU='"+guardia+
				"' CPA='"+cpa+
				"' CAA='"+caa+
				"' CSA='"+csa+
				"' />"			
			}			
		xmlDatosCotizacion+="</ROOT></ITEMS>"

	//HRF INI-20100729
		

	//Primas
	xmlDatosCotizacion+=fncGetXMLPrimas(varCantItems,oXMLItems);
	
	//HRF FIN-20100729
	
	xmlDatosCotizacion+=
		"<OBJETOS_X_COB>"+
		"<ROOT>"
		var oXMLObjAseg = oXMLObjAse.selectNodes("//OBJETOS/OBJETO");
		var varCantObj = oXMLObjAseg.length;
		
		for(var y=0; y<varCantObj; y++)
		{
			var iditem=oXMLObjAseg[y].selectSingleNode("IDITEM").text
			var cobercod=oXMLObjAseg[y].selectSingleNode("COBERCOD").text
			var nroobj=oXMLObjAseg[y].selectSingleNode("NROOBJ").text
			var tiporiesgo=0;//oXMLObjAseg[y].selectSingleNode("TIPORIESGO").text
			var codigoobjeto=oXMLObjAseg[y].selectSingleNode("CODIGOOBJETO").text				
			var sumaaseg=oXMLObjAseg[y].selectSingleNode("SUMAASEG").text==""?0:oXMLObjAseg[y].selectSingleNode("SUMAASEG").text;
			var detalleobjeto=oXMLObjAseg[y].selectSingleNode("DETALLEOBJETO").text
			xmlDatosCotizacion+="<REG IT='"+iditem+"' CB='"+cobercod+"' NR='"+nroobj+"' TR='"+tiporiesgo+"' CO='"+codigoobjeto+"' SM='"+sumaaseg+"' DO='"+detalleobjeto+"'/>"
		}
		xmlDatosCotizacion+="</ROOT>"+
		"</OBJETOS_X_COB>"
	
	xmlDatosCotizacion+="<PREGUNTAS><ROOT>"
	

	for(var x=0; x<varCantItems; x++)
		{
		var oXMLRegCobs = oXMLItems.selectNodes("//PREGUNTA[IDITEM='"+eval(x+1)+"']");
      		var varCantCobs = oXMLRegCobs.length;
		for(var y=0; y<varCantCobs; y++)
			{		
			var iditem=oXMLRegCobs[y].selectSingleNode("IDITEM").text
			var codPreg = oXMLRegCobs[y].selectSingleNode("CODPREG").text
			var resPreg = oXMLRegCobs[y].selectSingleNode("RESPREG").text
			xmlDatosCotizacion+="<REG IT='"+iditem+"'  PR='"+codPreg+"'  RS='"+resPreg+"' />"
			
			}
		}
	xmlDatosCotizacion+="</ROOT></PREGUNTAS>"
	

	//Convertir &
	xmlDatosCotizacion = xmlDatosCotizacion.replace(/&/g,"~~");
	
	var wImpreso="";
	if (pEstado="P")
	{
		wImpreso = fncGenerarImpreso("C");
	} 
	else 
	{
		wImpreso = fncGenerarImpreso("S");
	}
	

	wImpreso = wImpreso.replace(/&/g,"~~");

	//Impresos
	var wDatosImpresos
	
	if (fncEsICB1()) {
	    wDatosImpresos = "TEMPL_COTI_P=&" +
	    "IMPRE_COTI_P=&"
	}
	else {
	    wDatosImpresos = "TEMPL_COTI_P=COTI_ICO_Productor&" +
	    "IMPRE_COTI_P=" + wImpreso + "&"
	}
	
	if (document.getElementById("chk_circuito").checked  || document.getElementById("divTablaFueraNorma").innerHTML!="")
	{
		wDatosImpresos +=
			"TEMPL_COTI_C=&"+
			"IMPRE_COTI_C=&"
	} 
	else 
	{
		if (document.getElementById("RAMOPCODSQL").value=="ICB1"){
			wDatosImpresos +=	
			"TEMPL_COTI_C=COTI_ICB_Cliente&"+
			"IMPRE_COTI_C="+ wImpreso +"&"
		}
		else{	
			wDatosImpresos +=	
			"TEMPL_COTI_C=COTI_ICO_Cliente&"+
			"IMPRE_COTI_C="+ wImpreso +"&"
		}
	}
	wDatosImpresos +=	
	"TEMPL_SOLI=&"+
	"IMPRE_SOLI=&"+
	"TEMPL_CERTI=&"+
	"IMPRE_CERTI=&"+
	"TEMPL_AUTDEB=&"+
	"IMPRE_AUTDEB=&"+
	"TEMPL_TARJ_CIR=&"+
	"IMPRE_TARJ_CIR=&"+
	"TEMPL_CLAU_SUB=&"+
	"IMPRE_CLAU_SUB="
	
	
	xmlDatosCotizacion= encodeEntities(xmlDatosCotizacion)
		document.getElementById('xmlDatosCotizacion').XMLDocument="<COTIZACION>"+xmlDatosCotizacion+"</COTIZACION>"
		
		//window.document.body.insertAdjacentHTML("beforeEnd", "Preguntas recuperadas:<textarea>" + document.getElementById('xmlDatosCotizacion').XMLDocument + "</textarea>")
		//return 0
		
				
		//Armo el XML de entrada para guardar los datos de la cotizacion
		strData = "FUNCION=GUARDARCOTIZACION&";
		strData+= "REQUESTCOTIZACION=" + xmlDatosCotizacion + "&";
		strData+= wDatosImpresos
		xmlHTTPCotiza = new ActiveXObject("Msxml2.XMLHTTP");
		xmlHTTPCotiza.open("POST","DefinicionFrameWork.asp", false);
		xmlHTTPCotiza.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		
		xmlHTTPCotiza.send(encodeURI(strData));
		mvarIdCotizacion = xmlHTTPCotiza.responseText;
	
			
		

		if (mvarIdCotizacion == "ERROR" || isNaN(mvarIdCotizacion))
		{	if (pPopUp)		
			alert_customizado("Su Cotizaci�n nro. "+mvarCertisec+" no pudo ser guardada, intente nuevamente...");
			return 0;
		}
		else
		{
			if (pPopUp)	
			alert_customizado("La cotizaci�n se ha guardado exitosamente");
			document.getElementById("CERTISEC").value= mvarIdCotizacion
			return mvarIdCotizacion;
		}
	
}	
/******************************************************************************************/
function fncGetXMLPrimas(pCantItems, pXMLItems)
{
	var xmlPrimas = "";
	var xmlPrimasAux = "<PRIMAS_X_ITEM><ROOT>"
	var xmlAux = "";
	var wCont = 1
	
	for(var x=0; x<pCantItems; x++)
	{
		var oXMLRegCobs = pXMLItems.selectNodes("//COBERTURA[IDITEM='"+eval(x+1)+"']");
    var varCantCobs = oXMLRegCobs.length;

		for(var y=0; y<varCantCobs; y++)
		{
			var wIT = oXMLRegCobs[y].selectSingleNode("IDITEM").text;
			var wCB = oXMLRegCobs[y].selectSingleNode("COBERCOD").text;
			var wSM = oXMLRegCobs[y].selectSingleNode("SUMAASEG").text;
			var wPR = oXMLRegCobs[y].selectSingleNode("PRIMA").text;
			var wPO = oXMLRegCobs[y].selectSingleNode("TASA").text;
			var wRT = 0;
			if (document.getElementById("RAMOPCODSQL").value=="ICB1"){
				var wOA = "N"
				var wVM = 0
			}	
			else{
				var wOA = oXMLRegCobs[y].selectSingleNode("OBJASEG").text;
				var wVM = oXMLRegCobs[y].selectSingleNode("SWTASAVM").text;
			}
			xmlAux = "<PR CB='"+ wCB +"' IT='"+ wIT +"' PO='"+ wPO +"' PR='"+ wPR +"' SM='"+ wSM +"' RT='"+ wRT +"' OA='"+ wOA +"'   VM='"+ wVM +"'/>";
			
			if ((xmlPrimasAux + xmlAux + "<PRIMAS_X_ITEM_X><ROOT>").length > 8000)
			{
				if (wCont==1)
				{
					xmlPrimas+= xmlPrimasAux + "</ROOT></PRIMAS_X_ITEM>";
					xmlPrimasAux = "<PRIMAS_X_ITEM_2><ROOT>"
				} else if (wCont==2) {
					xmlPrimas+= xmlPrimasAux + "</ROOT></PRIMAS_X_ITEM_2>";
					xmlPrimasAux = "<PRIMAS_X_ITEM_3><ROOT>"
				}
				wCont++;
			}
			xmlPrimasAux+=xmlAux
		}
	}
	
	if (wCont==1)
	{
		xmlPrimas+= xmlPrimasAux + "</ROOT></PRIMAS_X_ITEM><PRIMAS_X_ITEM_2/><PRIMAS_X_ITEM_3/>";
	} else if (wCont==2) {
		xmlPrimas+= xmlPrimasAux + "</ROOT></PRIMAS_X_ITEM_2><PRIMAS_X_ITEM_3/>";
	} else if (wCont==3) {
		xmlPrimas+= xmlPrimasAux + "</ROOT></PRIMAS_X_ITEM_3>";
	}
	
	return xmlPrimas;
}
/******************************************************************************************/
function fncGenerarImpreso(pTipo)
{
	var xmlDatosImpresos = ''
	armaXMLCotizacion();
	xmlDatosImpresos = document.getElementById("hDatos").value;
	return xmlDatosImpresos;
}
/************************************/	
	
function fncRecuperaCotizacion()
{
	window.status="Recuperando datos 10%"
	//alert(encodeEntities("� � � � � � ABCDEFGHI � JKL � MN �"))
	
	oXMLCotiRecuperada = new ActiveXObject('MSXML2.DOMDocument');        
      oXMLCotiRecuperada.async = false;       
      oXMLCotiRecuperada.loadXML(document.getElementById("xmlCotiRecuperada").innerHTML); 
      
     
    //  document.body.insertAdjacentHTML("beforeEnd", "CotiRecuperada:<textarea>"+oXMLCotiRecuperada.xml+"</textarea>")
	 if (oXMLCotiRecuperada.xml==""){ errorInesperado("Datos cotizaci�n")	; return}
      
      oXMLPersRecuperada = new ActiveXObject('MSXML2.DOMDocument');        
      oXMLPersRecuperada.async = false; 
      oXMLPersRecuperada.setProperty("SelectionLanguage", "XPath");
      
      oXMLPersRecuperada.loadXML(document.getElementById("xmlPersRecuperada").innerHTML); 
       if (oXMLPersRecuperada.xml==""){ errorInesperado("Datos persona")	; return}
      
       oXMLApodRecuperada = new ActiveXObject('MSXML2.DOMDocument');             
     	 oXMLApodRecuperada.async = false;   
       oXMLApodRecuperada.setProperty("SelectionLanguage", "XPath"); 
     	 oXMLApodRecuperada.loadXML(document.getElementById("xmlApodRecuperada").innerHTML); 
      
      
      oXMLItemsRecuperados = new ActiveXObject('MSXML2.DOMDocument');        
      oXMLItemsRecuperados.async = false;       
      oXMLItemsRecuperados.loadXML(document.getElementById("xmlItemsRecuperados").innerHTML); 
       // document.body.insertAdjacentHTML("beforeEnd", "ITEMSRecuperada:<textarea style='position:absolute; z-index:100'>"+oXMLItemsRecuperados.xml+"</textarea>")

        if (oXMLItemsRecuperados.xml==""){ errorInesperado("Datos Items")	; return}
      
       oXMLPregItemsRecuperados = new ActiveXObject('MSXML2.DOMDocument');        
      oXMLPregItemsRecuperados.async = false;       
      oXMLPregItemsRecuperados.loadXML(document.getElementById("xmlPregItemsRecuperados").innerHTML); 
      
    //   document.body.insertAdjacentHTML("beforeEnd", "PREGRecuperada:<textarea>"+oXMLPregItemsRecuperados.xml+"</textarea>")

      
        if (oXMLPregItemsRecuperados.xml==""){ errorInesperado("Preguntas Items")	; return}
      
      
      var oXMLRegItems = oXMLItemsRecuperados.selectNodes("//REG");
      var varCantItems = oXMLItemsRecuperados.selectNodes("//REG").length;
      
      
      oXMLCobItemsRecuperados = new ActiveXObject('MSXML2.DOMDocument');        
      oXMLCobItemsRecuperados.async = false;       
      oXMLCobItemsRecuperados.loadXML(document.getElementById("xmlCobItemsRecuperados").innerHTML); 
      
       if (oXMLCobItemsRecuperados.xml==""){ errorInesperado("Datos coberturas")	; return}
       
	 oXMLAnotacionesRecuperadas = new ActiveXObject('MSXML2.DOMDocument');        
      oXMLAnotacionesRecuperadas.async = false;       
      oXMLAnotacionesRecuperadas.loadXML(document.getElementById("xmlAnotacionesRecuperada").innerHTML); 
      
	oXMLObjAsegRecuperados = new ActiveXObject('MSXML2.DOMDocument');
	oXMLObjAsegRecuperados.async = false;
	oXMLObjAsegRecuperados.loadXML(document.getElementById("xmlObjAsegRecuperados").innerHTML);
    
  //Coti - Datos Generales
  window.status="Recuperando datos 20%"
  //-----------------------------------------------------------------------------------------------------//
	//Convenio
	if ((document.getElementById("TIPOOPER").value == "R")&&(oXMLCotiRecuperada.selectSingleNode("//CONVENIOPR").text == "-2"))
	{
		if (trim(oXMLCotiRecuperada.selectSingleNode("//CONVENIOOR").text)=="-2")
		{
			var vCOMPCPOROR = Number(oXMLCotiRecuperada.selectSingleNode("//COMISIONOR").text)
		  var nuevoElemento=document.createElement("option")
			nuevoElemento.text='FUERA DE CONVENIO';
			nuevoElemento.value='-2|-2|0|'+vCOMPCPOROR;
			nuevoElemento.selected=true;
			document.getElementById("convenioOrganizador").length=0;
			document.getElementById("convenioOrganizador").add(nuevoElemento);
			document.getElementById("convenioOrganizador").disabled=false;
			
			vCOMPCPORPR1 = Number(oXMLCotiRecuperada.selectSingleNode("//COMISIONPR1").text)
			vCOMPCPOROR = Number(oXMLCotiRecuperada.selectSingleNode("//COMISIONOR").text)
		}
		else
		{
			//03/01/2011 LR Si la poliza tiene conv de organiz, pero no de prod
			fncConvenioProdSeleccionado();
			//-----------------------------------------------------------------------------------------------------//
				recuperaCombo("convenioOrganizador",oXMLCotiRecuperada.selectSingleNode("//CONVENIOOR").text,0)
			//-----------------------------------------------------------------------------------------------------//
			fncCargaComboComision();
		}
		window.status="Recuperando datos 30%"
		
		var vComisionTotal= vCOMPCPORPR1 + vCOMPCPOROR
		var nuevoElemento=document.createElement("option")
				nuevoElemento.text=formatoNro('',vComisionTotal,2,",")+ ' %';
				nuevoElemento.value= vCOMPCPORPR1 + vCOMPCPOROR;
				nuevoElemento.selected=true
		document.getElementById("comisionTotal").length=0;
		document.getElementById("comisionTotal").add(nuevoElemento);
		document.getElementById("comisionTotal").disabled=false;
		
		document.getElementById("COMISION_PROD").value= vCOMPCPORPR1
		document.getElementById("COMISION_ORG").value= vCOMPCPOROR
		document.getElementById("RECARGO").value=Number(oXMLRenovaPoliCol.selectSingleNode("//RECARPOR").text)
		
		document.getElementById("porcOrganizador").value= vCOMPCPOROR
		document.getElementById("porcProductor").value= vCOMPCPORPR1
	}
	else
	{
		recuperaCombo("convenioProductor",oXMLCotiRecuperada.selectSingleNode("//CONVENIOPR").text,0)
		fncConvenioProdSeleccionado();
		
		//Organizador
		//-----------------------------------------------------------------------------------------------------//
			recuperaCombo("convenioOrganizador",oXMLCotiRecuperada.selectSingleNode("//CONVENIOOR").text,0)
		//-----------------------------------------------------------------------------------------------------//
		fncCargaComboComision();
		//Comision
		recuperaCombo("comisionTotal",oXMLCotiRecuperada.selectSingleNode("//COMISTOT").text)
	}
	
	fncCalculaComisiones();
	//-----------------------------------------------------------------------------------------------------//
	window.status="Recuperando datos 40%"
	
	recuperaCombo("periodoCotizar",oXMLCotiRecuperada.selectSingleNode("//PERIODO").text)
	
	recuperaCombo("estabilizacion",rightString("0"+ oXMLCotiRecuperada.selectSingleNode("//ESTABILIZACION").text,2))
	
	recuperaCombo("IVA",oXMLCotiRecuperada.selectSingleNode("//CLIENIVA").text)		
	fncCuitSeleccionado(document.getElementById("IVA"))
	
	recuperaCombo("IngresosBrutos",oXMLCotiRecuperada.selectSingleNode("//CATEGORIA_IIBB").text)
	
	recuperaCombo("tipoPersona",oXMLCotiRecuperada.selectSingleNode("//TIPO_PERSONA").text)
	
	 fncTipoPersonaSel()
	 	
	//recuperaCombo("formaPago",oXMLCotiRecuperada.selectSingleNode("//COBROCOD").text,0)
	
	//ACTIVIDAD*****************
	 if (document.getElementById("RAMOPCODSQL").value == "ICB1") {
		recuperaCombo("cboActividadICOBB",oXMLCotiRecuperada.selectSingleNode("//ACTIVIDAD").text)				
		//alert(document.getElementById("cboActividadICOBB").value)
		fncActividadICOBBSeleccionada()
		fncRecuperaPlanICOBB(oXMLCotiRecuperada.selectSingleNode("//NRO_INTERNO_SOLI").text)
	}
	else{
		document.getElementById("filtroActividadComercio").value=   "*"
		document.getElementById("actividadComercio").value= oXMLCotiRecuperada.selectSingleNode("//ACTIVIDAD").text
		filtrarActividadCom('cboActividadComercio','filtroActividadComercio');	

		recuperaComboxTexto("cboActividadComercio",oXMLCotiRecuperada.selectSingleNode("//ACTIVIDAD").text,true )
	}
	window.status="Recuperando datos 50%"
	 
	//**************************
	
	recuperaCombo("formaPago",oXMLCotiRecuperada.selectSingleNode("//COBROCOD").text + "|" +oXMLCotiRecuperada.selectSingleNode("//COBROTIP").text )
	formaPagoSel()
	
	recuperaCombo("planPago",oXMLCotiRecuperada.selectSingleNode("//PLANPCOD").text,0)
	
	//recuperaCombo("comisionTotal",oXMLCotiRecuperada.selectSingleNode("//COMISTOT").text)
	
	
	var fechaAltaCoti = oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTANN").text
	fechaAltaCoti +=  oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTMES").text.length==1?"0"+oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTMES").text:oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTMES").text
	fechaAltaCoti +=  oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTDIA").text.length==1?"0"+oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTDIA").text:oXMLCotiRecuperada.selectSingleNode("//FECCOTIALTDIA").text
	
	
	document.getElementById("fechaAltaCoti").value=  fechaAltaCoti
											     
	var fechaUltimaCoti = oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTANN").text
	fechaUltimaCoti +=  oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTMES").text.length==1?"0"+oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTMES").text:oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTMES").text
	fechaUltimaCoti +=  oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTDIA").text.length==1?"0"+oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTDIA").text:oXMLCotiRecuperada.selectSingleNode("//FECCOTIULTDIA").text
		
											     
	
	document.getElementById("fechaUltimaCoti").value=  fechaUltimaCoti
	
	//Recotizacion *****************
	
	var fechaCotiTemp=convierteFecha(fechaUltimaCoti,"AAAAMMDD","DD/MM/AAAA")
	
	var fechaRecotizarDias = convierteFecha(dateAddExtention(parteFecha(fechaCotiTemp,"DD/MM/AAAA","DD"),parteFecha(fechaCotiTemp,"DD/MM/AAAA","MM"),parteFecha(fechaCotiTemp,"DD/MM/AAAA","AAAA"),"d", 15),"DD/MM/AAAA","AAAAMMDD")
	
	//Recotizar Dias ( no recotiza > dejar asi)    < si recotiza
	if (fechaRecotizarDias > document.getElementById("fechaHoy").value) 
	 	datosCotizacion.setCotizar(false)
	else
	 	datosCotizacion.setCotizar(true)
		
	//alert(datosCotizacion.getCotizar())							    
	//document.getElementById("IDCLIENTE").value	=	oXMLCotiRecuperada.selectSingleNode("//CLIENSEC").text					     
	
	
	/*document.getElementById("vigenciaDesde").value= oXMLCotiRecuperada.selectSingleNode("//FECINIVGDIA").text  +"/"+
											     oXMLCotiRecuperada.selectSingleNode("//FECINIVGMES").text  +"/"+
											     oXMLCotiRecuperada.selectSingleNode("//FECINIVGANN").text 
	
	document.getElementById("vigenciaHasta").value= oXMLCotiRecuperada.selectSingleNode("//FECFINVGDIA").text  +"/"+
											     oXMLCotiRecuperada.selectSingleNode("//FECFINVGMES").text  +"/"+
											     oXMLCotiRecuperada.selectSingleNode("//FECFINVGANN").text */
/*******************************************************************/	
//  Recupera los datos y coberturas de cada riesgo
/*******************************************************************/

window.status="Recuperando datos 60%"
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	oXMLValores= new ActiveXObject("MSXML2.DOMDocument");
	oXMLValores.async = false;
	
	auxXml = new ActiveXObject('MSXML2.DOMDocument');
	auxXml.async = false;
	auxXml.loadXML(document.getElementById("xmlFueraNorma").value);
	
	cantFueraNorma = auxXml.selectNodes("//ERROR").length
	
	if (cantFueraNorma == 0)
	{  
		document.getElementById("divTablaFueraNorma").innerHTML =""
		oXMLFueraNorma.loadXML("<ERRORES></ERRORES>")
	}
	else
	{	oXMLFueraNorma.loadXML(document.getElementById("xmlFueraNorma").value)}
	
	oXMLPopUpFueraNorma.loadXML("<ERRORES></ERRORES>")
	
/****************************************************************/
//  x Cada Item
/***************************************************************/	
	
for (var x=0; x<varCantItems; x++)
{
	var codActividad = rightString("000000" + oXMLCotiRecuperada.selectSingleNode("//ACTIVIDAD").text,6)
	var mvarProviCod =oXMLRegItems[x].selectSingleNode("PROVICOD").text
	
	if(mvarProviCod !="1")	
		var mvarCpostal= oXMLRegItems[x].selectSingleNode("CPOSTAL").text;	
	else
		var mvarCpostal= 1005
	

	var mvarRequest=	"<Request>"+
	"<PROVICOD>"+mvarProviCod+"</PROVICOD>"+
	"<CPACODPO>"+mvarCpostal+"</CPACODPO>"+
	"<RAMOPCOD>"+document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
	"</Request>"
	
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetZona') 
		
	if(oXMLValores.loadXML(mvarResponse))	
	{
		if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text = "true")
		{
			if(oXMLValores.selectSingleNode("//CODIZONA").text !="0000")
			{var mvarZona=oXMLValores.selectSingleNode("//CODIZONA").text
			 var mvarZonaDes=oXMLValores.selectSingleNode("//ZONASDES").text	
			}
			else
			{
			alert_customizado("No se pudo determinar zona para esta localidad")
			return false
			
			}
		}
		else
		{	alert_customizado(oXMLValores.selectSingleNode("//Response/Estado/@mensaje").text)			
			return false
		}
	}
	
	if (document.getElementById("RAMOPCODSQL").value!="ICB1"){
		var mvarRequest = "<Request>"+
	          "<DEFINICION>2390_ListadoCoberturas.xml</DEFINICION>"+        
	          "<USUARCOD>" + document.getElementById("LOGON_USER").value +"</USUARCOD>" +
	          "<CODIZONA>"+mvarZona+"</CODIZONA>" +
	          "<PROFECOD>"+codActividad+"</PROFECOD>"+
	          "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
              "</Request>"

    	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')       
	}
	else{

	  
		var mvarRequest = "<Request>"+
		      "<DEFINICION>SP_LISTAR_ICOBB_COBERTURAS.xml</DEFINICION>"+        
	    	  "<IDFOLLETO>"+document.getElementById("cboPlanICOBB").value.split("|")[1]+"</IDFOLLETO>" +
	          "<IDPLANES>"+document.getElementById("cboPlanICOBB").value.split("|")[0]+"</IDPLANES>" +
              "</Request>"
		//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCoberturas Request:<textarea>" + mvarRequest + "</textarea>")

		var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVSQLGen') 
	
	}
       //window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCoberturas:<textarea>"+mvarResponse+"</textarea>")
       var oXMLCoberturas = new ActiveXObject("MSXML2.DOMDocument");
	oXMLCoberturas.async = false;
	oXMLCoberturas.setProperty("SelectionLanguage", "XPath");
	if (oXMLCoberturas.loadXML(mvarResponse))
	
		if(oXMLCoberturas.selectSingleNode("//Response/Estado/@resultado").text = "true")
			{
			 document.getElementById("xmlCoberturas").innerHTML = oXMLCoberturas.xml
			}else
			{ alert_customizado("Error al recuperar coberturas 2390") }

		var mvarRequest = "<Request>" +
          "<USUARIO>" + document.getElementById("LOGON_USER").value + "</USUARIO>" +
          "<ZONA>" + mvarZona + "</ZONA>" +
          "<ACTIVIDAD>" + codActividad + "</ACTIVIDAD>" +
          "<RAMOPCOD>" + document.getElementById("RAMOPCOD").value + "</RAMOPCOD>" +
          "</Request>"

		var mvarResponse = llamadaMensajeMQ(mvarRequest, 'lbaw_GetActivPreguntas')

		//window.document.body.insertAdjacentHTML("beforeEnd", "Preguntas recuperadas:<textarea>" + mvarResponse + "</textarea>")

		document.getElementById("xmlPreguntas").value = mvarResponse	
			
        
	/*****************************/
		varOElement = varXml.createElement('ITEM');
		varOAttr = varXml.createAttribute('ID');
		varOAttr.value = oXMLRegItems[x].selectSingleNode("NROITEM").text;
		
		varOElement.setAttributeNode(varOAttr);
		varONodo = varXml.createElement("ESTADO");
		varONodo.text = "";
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("ID");
		varONodo.text = oXMLRegItems[x].selectSingleNode("NROITEM").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("PROVICOD");
		varONodo.text = oXMLRegItems[x].selectSingleNode("PROVICOD").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("DOMICDOM");
		varONodo.text = Base64.decode(oXMLRegItems[x].selectSingleNode("DOMICDOM").text);
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("DOMICDNU");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICDNU").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("DOMICPIS");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICPIS").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("DOMICPTA");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICPTA").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("CANTALUM");
		varONodo.text = oXMLRegItems[x].selectSingleNode("CANTALUM").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("PROVICODDES");
		for(var i=0; i<document.getElementById("CBO_PROVINCIA").options.length;i++)	
		{
		if ( document.getElementById("CBO_PROVINCIA").options[i].value==oXMLRegItems[x].selectSingleNode("PROVICOD").text)		
			varONodo.text = document.getElementById("CBO_PROVINCIA").options[i].text
			if (document.getElementById("RAMOPCODSQL").value=="ICB1"){
				recuperaCombo("CBO_PROVINCIA",oXMLRegItems[x].selectSingleNode("PROVICOD").text)							
				fncProvinciaSeleccionadaConCP('CBO_PROVINCIA','localidadRiesgo','codPostalRiesgo'); 
				fncMuestraCoberturasICOBB();
				muestraLeyendas=false
			}
		}
		
		varOElement.appendChild(varONodo);
		
		
		varONodo = varXml.createElement("DOMICPOB");
		var localidad= Base64.decode(oXMLRegItems[x].selectSingleNode("DOMICPOB").text);
		varONodo.text = localidad
		varOElement.appendChild(varONodo);
						
		varONodo = varXml.createElement("CPOSDOM");
		varONodo.text = oXMLRegItems[x].selectSingleNode("CPOSTAL").text;
		varOElement.appendChild(varONodo);
		
		if (document.getElementById("RAMOPCODSQL").value=="ICB1"){			
			document.getElementById("localidadRiesgo").value=localidad
			document.getElementById("codPostalRiesgo").value=oXMLRegItems[x].selectSingleNode("CPOSTAL").text;
			document.getElementById("CODIGOZONA").value=mvarZona
			document.getElementById("verZona").innerText=mvarZonaDes

			document.getElementById("PROVICOD").value=oXMLRegItems[x].selectSingleNode("PROVICOD").text			
			document.getElementById("DOMICPOB").value=localidad
			document.getElementById("CPOSDOM").value=oXMLRegItems[x].selectSingleNode("CPOSTAL").text;			
			document.getElementById("CodigoZonaICOBB").value=mvarZona
			document.getElementById("verZonaICOBB").value=mvarZonaDes
			//document.getElementById("xmlCoberturas").innerHTML =document.getElementById("xmlCoberturasICOBB").value
			//document.getElementById("divSeccionCoberturas").innerHTML=document.getElementById("divSeccionCoberturasICOBB").value					
		}
		varONodo = varXml.createElement("ZONAITEM");
		varONodo.text = mvarZona ;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("ITEMDESC");
		var itemDesc = localidad //+"-" +eval(x+1);
		//var itemDesc= Base64.decode(oXMLRegItems[x].selectSingleNode("NOMBRE_EDIFICIO").text);
		varONodo.text = itemDesc
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("ALARMA");
		varONodo.text = oXMLRegItems[x].selectSingleNode("ALARMATIP").text;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("GUARDIA");
		varONodo.text = oXMLRegItems[x].selectSingleNode("GUARDIATIP").text;
		varOElement.appendChild(varONodo);		
			
		
				
		//varOElement.appendChild(varONodo);
		//FASE 2
		if (oXMLRegItems[x].selectSingleNode("CERTISECANT").text == "0")
		{
			//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
			varONodo = varXml.createElement("IDISYUNT");
			varONodo.text = "N";
			varOElement.appendChild(varONodo);
			
			varONodo = varXml.createElement("CERTIPOLANT");
			varONodo.text = "";
			varOElement.appendChild(varONodo);
				
			varONodo = varXml.createElement("CERTIANNANT");
			varONodo.text ="";
			varOElement.appendChild(varONodo);
			
			varONodo = varXml.createElement("CERTISECANT");
			varONodo.text = "" ;
			varOElement.appendChild(varONodo);
			
		}
		else
		{
			//--------------------------------------------------------------------------------------//
			//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
			//ITEMS ORIGINALES: Recupera los datos originales que no se guardan en SQL
			varXmlRen = new ActiveXObject('MSXML2.DOMDocument');
			varXmlRen.async = false;
			varXmlRen.loadXML(document.getElementById("varXmlRenovItems").value);
			varNodRen = varXmlRen.selectSingleNode('//ITEM[CERTISECANT="'+ fncSetLength(oXMLRegItems[x].selectSingleNode("CERTISECANT").text,6) +'"]')
						
			varONodo = varXml.createElement("IDISYUNT");
			varONodo.text = varNodRen.selectSingleNode("IDISYUNT").text;
			varOElement.appendChild(varONodo);
			//--------------------------------------------------------------------------------------//
			
			varONodo = varXml.createElement("CERTIPOLANT");
			varONodo.text = rightString("0000" + oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text,4);
			varOElement.appendChild(varONodo);
				
			varONodo = varXml.createElement("CERTIANNANT");
			varONodo.text = rightString("0000" + oXMLRegItems[x].selectSingleNode("CERTIANNANT").text,4);
			varOElement.appendChild(varONodo);
			var certisecAnt =  rightString("000000" + oXMLRegItems[x].selectSingleNode("CERTISECANT").text,6)  ;
			varONodo = varXml.createElement("CERTISECANT");
			varONodo.text = certisecAnt ;
			varOElement.appendChild(varONodo);
		}
		
		varXml.selectSingleNode("//ITEMS").appendChild(varOElement);
		
		varONodo = varXml.createElement("COBERTURAS");
		varOElement.appendChild(varONodo);
		//"+oXMLRegItems[x].selectSingleNode("NROITEM").text+"
		var oXMLRegCobs = oXMLCobItemsRecuperados.selectNodes("//REG[NROITEM='"+eval(x+1)+"']");
      		var varCantCobs = oXMLRegCobs.length;
		// window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varCantCobs+"</textarea>")
		//var totalCoberturas = document.all.COBCHECK.length;	
		
		window.status="Recuperando datos 70%"
		//window.document.body.insertAdjacentHTML("beforeEnd", "mobjXMLFormasPago1<textarea>"+varXml.xml+"</textarea>")
		for (var y=0; y<varCantCobs; y++)
		{
				
				
				
				var varONodoCob = varXml.createElement("COBERTURA");
				
				varONodo = varXml.createElement("IDITEM");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("NROITEM").text;
				varONodoCob.appendChild(varONodo);	
				
				varONodo = varXml.createElement("COBERCOD");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("COBERCOD").text;
				varONodoCob.appendChild(varONodo);

				fncCoberMensajeLeyenda(oXMLRegCobs[y].selectSingleNode("COBERCOD").text, mvarZona, oXMLRegCobs[y].selectSingleNode("SUMA_ASEGURADA").text);
				
				varONodo = varXml.createElement("COBERDES");
				/*******************************************/
				
				//  asignar la descripcion de cada cobertura
//				alert("asignar la descripcion de cada cobertura="+ y)		
				//alert("COBERCOD.TEXT="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text)		
				if (document.getElementById("RAMOPCODSQL").value=="ICB1")
					varONodo.text = 	oXMLCoberturas.selectSingleNode("//COBER[IDCOBERTURA="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/COBERTURA").text	
				else					
					varONodo.text = 	oXMLCoberturas.selectSingleNode("//COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/COBERDES").text	
				
				/********************************************/				
				varONodoCob.appendChild(varONodo);	
						
				varONodo = varXml.createElement("SUMAASEG");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("SUMA_ASEGURADA").text;
				varONodoCob.appendChild(varONodo)
				
				varONodo = varXml.createElement("PRIMA");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("PRIMAIMP").text;
				varONodoCob.appendChild(varONodo)
				
				varONodo = varXml.createElement("TASA");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("TARIFPOR").text;
				varONodoCob.appendChild(varONodo)
				
				varONodo = varXml.createElement("SWTASAVM");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("SWTASAVM").text;
				varONodoCob.appendChild(varONodo)

				varONodo = varXml.createElement("OBJASEG");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("OBJASEG").text;
				varONodoCob.appendChild(varONodo)
			
				varXml.selectSingleNode('//ITEM[ID='+eval(x+1)+']/COBERTURAS').appendChild(varONodoCob);
		}
	
	//Objetos Asegurados
	varXmlObjAse = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAse.async = false;
	varXmlObjAse.loadXML(document.getElementById("varXmlObjAseg").value);
	
	var oXMLObjAseg = oXMLObjAsegRecuperados.selectNodes("//REG");
	var varCantObj = oXMLObjAseg.length;

	//varONodoObj = varXmlObjAse.createElement("OBJETOS");
	//varXmlObjAse.appendChild(varONodoObj);
	
	window.status="Recuperando datos 80%"
	
	for (var y=0; y<varCantObj; y++)
	{
		varONodoObj = varXmlObjAse.createElement("OBJETO");
		varONodo = varXmlObjAse.createElement("IDITEM");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("NROITEM").text
		varONodoObj.appendChild(varONodo);

		varONodo = varXmlObjAse.createElement("COBERCOD");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("COBERCOD").text
		varONodoObj.appendChild(varONodo);
		
		varONodo = varXmlObjAse.createElement("NROOBJ");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("NROOBJ").text
		varONodoObj.appendChild(varONodo);			
		
		varONodo = varXmlObjAse.createElement("TIPORIESGO");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("TIPO_RIESGO").text
		//varONodo.text=''
		varONodoObj.appendChild(varONodo);			

		varONodo = varXmlObjAse.createElement("CODIGOOBJETO");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("CODIGO_OBJETO").text
		varONodoObj.appendChild(varONodo);
				
		varONodo = varXmlObjAse.createElement("SUMAASEG");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("SUMA_ASEG").text
		varONodoObj.appendChild(varONodo);

		varONodo = varXmlObjAse.createElement("DETALLEOBJETO");
		varONodo.text=oXMLObjAseg[y].selectSingleNode("DETALLE_OBJETO").text
		varONodoObj.appendChild(varONodo);
		
		varXmlObjAse.selectSingleNode("//OBJETOS").appendChild(varONodoObj);
	}
		
		var tempProvincia = oXMLRegItems[x].selectSingleNode("PROVICOD").text
		var tempLocalidad = Base64.decode(oXMLRegItems[x].selectSingleNode("DOMICPOB").text);
	
	//RECUPERA PREGUNTAS
		varONodo = varXml.createElement("PREGUNTAS");
		varOElement.appendChild(varONodo);
			
		var oXMLRegCobs = oXMLPregItemsRecuperados.selectNodes("//REG[NROITEM='"+eval(x+1)+"']");
      		var varCantCobs = oXMLRegCobs.length;
      		for (var y=0; y<varCantCobs; y++)      		
      		{
      			
      			var varONodoCob = varXml.createElement("PREGUNTA");
				
				varONodo = varXml.createElement("IDITEM");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("NROITEM").text;
				varONodoCob.appendChild(varONodo);	
				
				varONodo = varXml.createElement("CODPREG");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("CODPREG").text;
				varONodoCob.appendChild(varONodo);	
				
				varONodo = varXml.createElement("RESPREG");
				varONodo.text = oXMLRegCobs[y].selectSingleNode("RESPREG").text;
				varONodoCob.appendChild(varONodo);	
				
				varXml.selectSingleNode('//ITEM[ID='+eval(x+1)+']/PREGUNTAS').appendChild(varONodoCob);
      		}
      		
	}
	
	//guardo errores para no perderlos entre items para fuera de norma
	document.getElementById("xmlfueraNorma").value = oXMLFueraNorma.xml
	

	
	var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
	varXslFueraNorma.async = false;	
	varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);
		
	varTablaFueraNorma = oXMLFueraNorma.transformNode(varXslFueraNorma);	
	
	document.getElementById("divTablaFueraNorma").innerHTML = varTablaFueraNorma;


	
	 //window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varXml.xml+"</textarea>")
	   document.getElementById("varXmlItems").value= varXml.xml
	   
	   
	   
	   	if (document.getElementById("TIPOOPER").value == "R"){
	   		
	   		
	   		comparaCoberturasItemsRen()
	   		
	   		}
	   
		//XML de Objetos Asegurados
		document.getElementById("varXmlObjAseg").value= varXmlObjAse.xml
	   


	   	//actualizarListadoXML(datosCotizacion.getCanalURL()  + "Canales/" + datosCotizacion.getCanal() )
	   	 actualizarListadoXML(datosCotizacion.getCanalUrlImg() )		
	  /******************************************************************/ 
	   //Recotizar despues de obtener los items si corresponde por los dias

	   if (datosCotizacion.getCotizar())
		 for (var x=1; x<=varCantItems; x++)
		{
		  // fncCotizarItem(x)
		}
		
		fncCargarRiesgoSolicitud();
	/******************************************************************/

	
	// recuperaCombo("CBO_PROVINCIA",tempProvincia,0)		
	// document.getElementById("CBO_PROVINCIA").disabled = true
	// document.getElementById("localidadRiesgo").value=tempLocalidad
//	 document.getElementById("localidadRiesgo").disabled =true
	
	//Recupera Datos Solapa Cliente
	recuperaCombo("tipoDocCliente",oXMLPersRecuperada.selectSingleNode("//TIPODOCU").text)
	
	//var DOMICCAL 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICCAL").text)
	var DOMICDOM 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICDOM").text)
	var DOMICDNU 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICDNU").text)
	var DOMICESC 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICESC").text)
	var DOMICPIS 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICPIS").text)
	var DOMICPTA 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICPTA").text)
	var DOMICPOB 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICPOB").text)
	var DOMICCPO 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICCPO").text)
	var PROVICOD 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/PROVICOD").text)
	var PAISSCOD 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/PAISSCOD").text)
	var TELCOD 		= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/TELCOD").text)
	var TELNRO 		= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/TELNRO").text)
	var PRINCIPAL 	= trim(oXMLPersRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/PRINCIPAL").text)
	
	
	document.getElementById("nroDocumentoCliente").value =trim(oXMLPersRecuperada.selectSingleNode("//NUMEDOCU").text)
	document.getElementById("apellidoCliente").value =trim(oXMLPersRecuperada.selectSingleNode("//CLIENAP1").text) + trim(oXMLPersRecuperada.selectSingleNode("//CLIENAP2").text)
	document.getElementById("nombreCliente").value =trim(oXMLPersRecuperada.selectSingleNode("//CLIENNOM").text)
	if (oXMLPersRecuperada.selectSingleNode("//NACIMMES").text!="0" ) 
		document.getElementById("fechaNacimientoClienteSoli").value =
			trim(oXMLPersRecuperada.selectSingleNode("//NACIMDIA").text) + "/"+
			trim(oXMLPersRecuperada.selectSingleNode("//NACIMMES").text) +"/"+	
			 trim(oXMLPersRecuperada.selectSingleNode("//NACIMANN").text);
	
	if (document.getElementById("TIPOOPER").value == "R")
		document.getElementById("divBtnBuscarCliente").style.display = "none";
	
	recuperaCombo("sexoCliente",oXMLPersRecuperada.selectSingleNode("//CLIENSEX").text)	
	
	recuperaCombo("estadoCivilCliente",oXMLPersRecuperada.selectSingleNode("//CLIENEST").text)
	
	recuperaCombo("paisNacimientoCliente",oXMLPersRecuperada.selectSingleNode("//PAISSCOD").text)
	
	
	
	document.getElementById("codTelefonoCliente").value =TELCOD
	document.getElementById("telefonoCliente").value =TELNRO
	document.getElementById("emailCliente").value =trim(oXMLPersRecuperada.selectSingleNode("//EMAIL").text)
	document.getElementById("calleDomicilio").value =DOMICDOM
	document.getElementById("nroDomicilio").value =DOMICDNU
	document.getElementById("pisoDomicilio").value =DOMICPIS
	document.getElementById("dptoDomicilio").value =DOMICPTA	
	if(PROVICOD!="0") 
	{
		recuperaCombo("provinciaDomicilio",PROVICOD)
		document.getElementById("localidadDomicilio").value = DOMICPOB		
	}	
	if(DOMICCPO!="0")	document.getElementById("codPosDomicilio").value =DOMICCPO
	document.getElementById("CodTelPosDomicilio").value =TELCOD
	document.getElementById("TelPosDomicilio").value =TELNRO
	document.getElementById("mailDomicilio").value =trim(oXMLPersRecuperada.selectSingleNode("//EMAIL").text)
	
//Solapa Soli Datos Grales
	  document.getElementById("nroCuit").value  = oXMLCotiRecuperada.selectSingleNode("//NCUITCLIE").text
	 document.getElementById("apoCuit").value  = oXMLCotiRecuperada.selectSingleNode("//NCUITAPOD").text
	
	if (oXMLCotiRecuperada.selectSingleNode("//OCUPACODCLIE").text!=""){
	   recuperaCombo("actividadesCliente",oXMLCotiRecuperada.selectSingleNode("//OCUPACODCLIE").text)	
	 
	  document.getElementById("actividadesCliente").style.display="inline"
	  document.getElementById("filtro_OCUPACODCLIE").value=   document.getElementById("ACTIVIDADCLIENTE").value 
	  filtrarActividades('actividadesCliente','filtro_OCUPACODCLIE')
	  
	}

	fncCambiaEMail();

	if (trim(oXMLCotiRecuperada.selectSingleNode("//CONFORM_EMAIL").text) != "") 
		document.getElementById("cboConformEMail").value = trim(oXMLCotiRecuperada.selectSingleNode("//CONFORM_EMAIL").text);
	
	
	if (oXMLCotiRecuperada.selectSingleNode("//OCUPACODAPOD").text!=""){
	   recuperaCombo("apoActividad",oXMLCotiRecuperada.selectSingleNode("//OCUPACODAPOD").text)	
	 
	  document.getElementById("apoActividad").style.display="inline"
	  document.getElementById("filtro_apoActividad").value=   document.getElementById("ACTIVIDADAPOD").value 
	 
	  filtrarActividades('apoActividad','filtro_apoActividad')
	}
	
	//Editar Domicilio (Renovaci�n)
	if (document.getElementById('TIPOOPER').value == "R")
	{
		if (fncEditarDomicilioChk())
			fncEditarDomicilio(true)
		else
			fncEditarDomicilio(false)
	}
     	 
	//Observ.Clasula
	document.getElementById("observClausulas").value = Base64.decode(oXMLAnotacionesRecuperadas.selectSingleNode("//REG[TIPO_ANOTACION='A']/TEXTO_ANOTACION").text);
	//Texto P�liza
	document.getElementById("textoPoliza").value = Base64.decode(oXMLAnotacionesRecuperadas.selectSingleNode("//REG[TIPO_ANOTACION='F']/TEXTO_ANOTACION").text);
	
	
	//Apoderado
	
		recuperaCombo("tieneApoderado",document.getElementById("SWAPODER").value)	
	muestraApoderado(document.getElementById("SWAPODER"))
		
	if (document.getElementById("SWAPODER").value=="S")	
	{
		var DOMICCPOAPOD 	= trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICCPO").text)
		var PROVICODAPOD 	= trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/PROVICOD").text)
	
		document.getElementById("apoApellido").value=		trim(oXMLApodRecuperada.selectSingleNode("//CLIENAP1").text) + trim(oXMLApodRecuperada.selectSingleNode("//CLIENAP2").text)
		document.getElementById("apoNombre").value=		trim(oXMLApodRecuperada.selectSingleNode("//CLIENNOM").text)		
		recuperaCombo("cboApoTipDoc",oXMLApodRecuperada.selectSingleNode("//TIPODOCU").text)		
		document.getElementById("apoNroDoc").value=		trim(oXMLApodRecuperada.selectSingleNode("//NUMEDOCU").text)		
		if (oXMLApodRecuperada.selectSingleNode("//NACIMMES").text!="0" ) 
			document.getElementById("apoFechaNacimiento").value =
				trim(oXMLApodRecuperada.selectSingleNode("//NACIMDIA").text) + "/"+
				trim(oXMLApodRecuperada.selectSingleNode("//NACIMMES").text) +"/"+	
			 	trim(oXMLApodRecuperada.selectSingleNode("//NACIMANN").text);
		
		recuperaCombo("apoSexo",oXMLApodRecuperada.selectSingleNode("//CLIENSEX").text)	
		recuperaCombo("apoEstadoCivil",oXMLApodRecuperada.selectSingleNode("//CLIENEST").text)	
		recuperaCombo("apoPais",oXMLApodRecuperada.selectSingleNode("//PAISSCOD").text)
		
		
		document.getElementById("apoCodTelefono").value = 	trim(oXMLApodRecuperada.selectSingleNode("//TELCOD").text)
		document.getElementById("apoTelefono").value= 		trim(oXMLApodRecuperada.selectSingleNode("//TELNRO").text)	
		document.getElementById("apoMail").value= 			trim(oXMLApodRecuperada.selectSingleNode("//EMAIL").text)
		document.getElementById("apoCalleDomicilio").value=  	trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICDOM").text)
		document.getElementById("apoNroDomicilio").value= 	 	trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICDNU").text)
		
		document.getElementById("apoPisoDomicilio").value= 	trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICPIS").text)
		document.getElementById("apoDptoDomicilio").value= 	trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICPTA").text)
		
		if(PROVICODAPOD!="0") 
			{
				recuperaCombo("apoProvinciaDomicilio",PROVICODAPOD)
				document.getElementById("apoLocalidadDomicilio").value = trim(oXMLApodRecuperada.selectSingleNode("//DOMICILIO[count(//DOMICILIO)]/DOMICPOB").text)	
			}	
			
		if(DOMICCPOAPOD!="0")	document.getElementById("apoCodPosDomicilio").value =DOMICCPOAPOD
		
		
	}// Fin apoderado
	
	//Rearmar el xml para la composicion de Precio (Solapa Resultado Cotizacion)
		/*var vRequest='<Response><Estado resultado="true" mensaje=""/>'+
							'<CAMPOS>'+
								'<PRIMAIMP>'+ oXMLCotiRecuperada.selectSingleNode("//PRIMAIMP").text +'</PRIMAIMP>'+
								'<DEREMI>'+ oXMLCotiRecuperada.selectSingleNode("//DEREMIMP").text +'</DEREMI>'+
								'<RECARGOS>'+ oXMLCotiRecuperada.selectSingleNode("//RECAIMPO").text +'</RECARGOS>'+
								'<IVAIMPOR>'+ oXMLCotiRecuperada.selectSingleNode("//IVAIMPOR").text +'</IVAIMPOR>'+
								'<IVAIMPOA>'+ oXMLCotiRecuperada.selectSingleNode("//IVAIMPOA").text +'</IVAIMPOA>'+
								'<IVARETEN>'+ oXMLCotiRecuperada.selectSingleNode("//IVARETEN").text +'</IVARETEN>'+
								'<SELLADOS>'+ oXMLCotiRecuperada.selectSingleNode("//SELLAIMP").text +'</SELLADOS>'+
								'<INGBRUTO>'+ oXMLCotiRecuperada.selectSingleNode("//INGBRIMP").text +'</INGBRUTO>'+
								'<OTROSIMP>'+ oXMLCotiRecuperada.selectSingleNode("//IMPUEIMP").text +'</OTROSIMP>'+
								'<PRECIOTOT>'+ oXMLCotiRecuperada.selectSingleNode("//RECTOIMP").text +'</PRECIOTOT>'+
							'</CAMPOS>'+
						'</Response>';
						
		
		resultadoCotizacion(vRequest);*/
		
		window.status="Recuperando datos 90%"
	
//	if (document.getElementById("RAMOPCODSQL").value=="ICB1")
		
		
//	else	

	if (document.getElementById("RAMOPCODSQL").value=="ICB1")
		fncRecuperaPlanICOBB(oXMLCotiRecuperada.selectSingleNode("//NRO_INTERNO_SOLI").text)
	else
		document.getElementById("nroSolicitud").value =oXMLCotiRecuperada.selectSingleNode("//NRO_INTERNO_SOLI").text 

	
	
	
	
	document.getElementById("vendBanco").value = document.getElementById("INSTITUCION").value +" "+ document.getElementById("BANCONOM").value 
	
	document.getElementById("CODEMP").value = oXMLCotiRecuperada.selectSingleNode("//VEND_EMPRESA").text 
	recuperaCombo("cbo_vendSucursal",oXMLCotiRecuperada.selectSingleNode("//VEND_SUCURSAL").text)
	document.getElementById("legajoVendedor").value =oXMLCotiRecuperada.selectSingleNode("//VEND_LEGAJO").text 
	
	//Defect 94 - HRF/AA 20100915 INI
	//Circuito Revisi�n
	document.getElementById("EJEC_PPLSOFT").value = oXMLCotiRecuperada.selectSingleNode("//EJEC_PPLSOFT").text;
	document.getElementById("EJEC_EMAIL").value = oXMLCotiRecuperada.selectSingleNode("//EJEC_EMAIL").text;
	//document.getElementById("CONFORM_EMAIL").value = oXMLCotiRecuperada.selectSingleNode("//CONFORM_EMAIL").text;
	if (oXMLCotiRecuperada.selectSingleNode("//SOLICITA_REVISION").text == "S")
		document.getElementById("chk_circuito").checked = true;
	else
		document.getElementById("chk_circuito").checked = false;
	//Defect 94 - HRF/AA 20100915 FIN
	
	//fncBuscarVendedor(document.getElementById("INSTITUCION").value,  document.getElementById('cbo_vendSucursal').value, document.getElementById('legajoVendedor').value, document.getElementById("CANALHSBC").value)

	recuperaCombo("cbo_vendSucursal",oXMLCotiRecuperada.selectSingleNode("//VEND_SUCURSAL").text)
	//recuperaCombo("I_VENDEDOR",oXMLCotiRecuperada.selectSingleNode("//VEND_SUCURSAL").text)

	//recuperar para RENOVACION
	document.getElementById("POLIZANNANT").value = oXMLCotiRecuperada.selectSingleNode("//POLIZANNANT").text;
	document.getElementById("POLIZSECANT").value = oXMLCotiRecuperada.selectSingleNode("//POLIZSECANT").text;
	document.getElementById("REN_MODIF_DAT").value = oXMLCotiRecuperada.selectSingleNode("//REN_MODIF_DAT").text;
	document.getElementById("REN_COTIZ_NUE").value = oXMLCotiRecuperada.selectSingleNode("//REN_COTIZ_NUE").text;

	//*********************
	//Formas de Pago
	//Tarjeta
	if (document.getElementById("seccion_Tarjeta").style.display != 'none')
	{
		recuperaCombo("cbo_nomTarjeta",oXMLPersRecuperada.selectSingleNode("//TARJECOD").text +"|"+ oXMLPersRecuperada.selectSingleNode("//COBROTIP").text);
		document.getElementById("numTarjeta").value = trim(oXMLPersRecuperada.selectSingleNode("//CUENNUME").text);
		recuperaCombo("tarvtomes",oXMLPersRecuperada.selectSingleNode("//VENCIMES").text);
		recuperaCombo("tarvtoann",oXMLPersRecuperada.selectSingleNode("//VENCIANN").text);
	}
	//Debito en Cuenta
	if (document.getElementById("seccion_PagoCuenta").style.display != 'none')
	{
		document.getElementById("numCuentaDC").value = trim(oXMLPersRecuperada.selectSingleNode("//CUENNUME").text);
		recuperaCombo("cboSucursalDC",fncSetLength(oXMLPersRecuperada.selectSingleNode("//SUCURCOD").text,4));
	}
	//CBU
	if (document.getElementById("divNroCBU").style.display != 'none')
	{
		document.getElementById("numCBU").value = trim(oXMLPersRecuperada.selectSingleNode("//CUENNUME").text);
	}

	//Rearmar el xml para la composicion de Precio (Solapa Resultado Cotizacion)
	//adriana 19-4-2011 Ticket 599651 se agrega campo SWPRIMIN
	vRequest='<Response><Estado resultado="true" mensaje=""/>'+
					'<CAMPOS>'+
					'<PRIMAIMP>'+ oXMLCotiRecuperada.selectSingleNode("//PRIMAIMP").text +'</PRIMAIMP>'+
					'<DEREMIMP>'+ oXMLCotiRecuperada.selectSingleNode("//DEREMIMP").text +'</DEREMIMP>'+
					'<RECAIMPO>'+ oXMLCotiRecuperada.selectSingleNode("//RECAIMPO").text +'</RECAIMPO>'+
					'<IVAIMPOR>'+ oXMLCotiRecuperada.selectSingleNode("//IVAIMPOR").text +'</IVAIMPOR>'+
					'<IVAIMPOA>'+ oXMLCotiRecuperada.selectSingleNode("//IVAIMPOA").text +'</IVAIMPOA>'+
					'<IVARETEN>'+ oXMLCotiRecuperada.selectSingleNode("//IVARETEN").text +'</IVARETEN>'+
					'<SELLAIMP>'+ oXMLCotiRecuperada.selectSingleNode("//SELLAIMP").text +'</SELLAIMP>'+
					'<INGBRIMP>'+ oXMLCotiRecuperada.selectSingleNode("//INGBRIMP").text +'</INGBRIMP>'+
					'<IMPUEIMP>'+ oXMLCotiRecuperada.selectSingleNode("//IMPUEIMP").text +'</IMPUEIMP>'+
					'<RECTOIMP>'+ oXMLCotiRecuperada.selectSingleNode("//RECTOIMP").text +'</RECTOIMP>'+
					'<TASUIMPO>'+ oXMLCotiRecuperada.selectSingleNode("//TASUIMPO").text +'</TASUIMPO>'+
					'<SWPRIMIN>'+ oXMLCotiRecuperada.selectSingleNode("//SWPRIMIN").text +'</SWPRIMIN>'+
					'</CAMPOS>'+
					'</Response>';
					
			//GED 09-05-2011 PRIMA MINIMA
			document.getElementById('SWPRIMIN').value = oXMLCotiRecuperada.selectSingleNode("//SWPRIMIN").text

			if (document.getElementById("RAMOPCODSQL").value=="ICB1")
				resultadoCotizacionICOBB(vRequest);
			else	
				resultadoCotizacionSQL(vRequest);
				

	
	
//******************************
 if (document.getElementById("COTISOLI").value == "S" || document.getElementById("COTISOLI").value == "V")
	{
		ocultaEtiquetaSolapa("1")
		ocultaEtiquetaSolapa("2")
		ocultaEtiquetaSolapa("3")
		ocultaEtiquetaSolapa("4")
		muestraEtiquetaSolapa("5")
		muestraEtiquetaSolapa("6")
		muestraEtiquetaSolapa("7")
		
		mostrarTab('divDatosSolicitud','','#9CBACE','');
	}

	//Mostrar Diferencias para Renovaciones
	if(document.getElementById("TIPOOPER").value=="R")	
	{
		fncCambiarColorCpoRenTotal();
	}

	if(document.getElementById("RAMOPCODSQL").value!="ICB1"){
	  	fncFueraManual();	  	
		if (document.getElementById("COTISOLI").value != "S" &&  document.getElementById("COTISOLI").value != "V")
			fncMuestraFueraNorma();
	
		//06/01/2011 LR Se agrega Fuera de Norma para Convenios y Comisiones.
		fncRenovConvComisFN();
	}
	nroUnicoItem = varCantItems
	

	window.status="Listo"

}

// filtrarActividades('div_ACTIVIDADES',filtro_OCUPACODCLIE.value,'CLIE')

var filtroAnterior = "*"
function filtrarActividadCom(pCombo,pValor)
{
	objFiltro = document.getElementById(pValor);
	
	
	if (objFiltro.value.length == 0)
	{
		alert("Ingrese como m�nimo 2 caracteres.");
		if (event != null)
		{
			event.returnValue = false;
		}
		return;
	}

	//El filtro debe tener al menos 2 caracteres para poder filtrar
	if (objFiltro.value.length < 2 && objFiltro.value != "*" )
	{
		alert("Usted debe ingresar al menos 2 caracteres para poder filtrar la actividad/ocupaci�n.");
		if (event != null)
		{
			event.returnValue = false;
		}
		return;
	}
	
	if (nroUnicoItem == 0)
	{
	
		if (objFiltro.value=="*") 
		{ filtroAnterior= "*"
			var filtro = ""
		}
		else
		{
			var filtro = objFiltro.value.toUpperCase()
			filtroAnterior= filtro
		}
		
		var mvarRequest = "<Request>"+
		"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+" </RAMOPCOD>"+
		"<USUARIO>"+ document.getElementById("LOGON_USER").value + "</USUARIO>" +
		"<FILTROACTIVIDAD>" +  filtro + "</FILTROACTIVIDAD>"	+
		"</Request>"
		
		document.getElementById(pCombo).length=0
		//datosCotizacion.cargaComboXSL(pCombo,llamadaMensajeMQ(mvarRequest,'lbaw_GetActividadesComercios'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboActividades.xsl")
		datosCotizacion.cargaComboXSL(pCombo,llamadaMensajeMQ(mvarRequest,'lbaw_GetActividadesComercios'),document.getElementById("getComboActividades").xml)
		document.getElementById(pCombo).style.display='inline'
	
	
	}
	else
	 	fncCambiaFiltroActividad();

}

function mostrarDivCondiciones()
{
	

	var objDivCondiciones = document.getElementById("divBtnCondiciones");
	var objActividad = document.getElementById('cboActividadComercio');
	
	objDivCondiciones.style.display = "none";
					
					if (objActividad.value.split("|")[0] == "0" || objActividad.value.split("|")[0] == "-1" )
					{
						objDivCondiciones.style.display = "none";
						return;
					}
						else
					{
						objDivCondiciones.style.display = "inline";
						return;
					}	
	
}


function filtrarActividades(pCombo,pValor)
{
	objFiltro = document.getElementById(pValor);
	if (objFiltro.value.length == 0)
	{
		alert("Ingrese como m�nimo 2 caracteres.");
		if (event != null)
		{
			event.returnValue = false;
		}
		return;
	}

	//El filtro debe tener al menos 2 caracteres para poder filtrar
	if (objFiltro.value.length < 2 && objFiltro.value != "*" )
	{
		alert("Usted debe ingresar al menos 2 caracteres para poder filtrar la actividad/ocupaci�n.");
		if (event != null)
		{
			event.returnValue = false;
		}
		return;
	}

	//HRF 2010-11-02 Correcci�n busqueda Actividad
	var mvarRequest = "<Request><DEFINICION>getActividadOcupacion.xml</DEFINICION><AplicarXSL>getActividadOcupacion.xsl</AplicarXSL><BUSCAR>" +  objFiltro.value.substr(0,59).toUpperCase() + "</BUSCAR></Request>"
	document.getElementById(pCombo).length=0
	datosCotizacion.cargaComboXSL(pCombo,llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQGestion'))
	document.getElementById(pCombo).style.display='inline'

	
	
}


var cambiaActividad=false

function fncCambiaFiltroActividad()
{
	
	if(document.getElementById('cboActividadComercio').disabled==false){
	confirm_customizado("<p align='center'>�Esta seguro de modificar la actividad? <br>Se perder�n todos los datos</p>") 		
	document.getElementById("funcion_confirma").value = "confirmaCambiaFiltro()"
	document.getElementById("funcion_cancela").value = "cancelaCambiaActividad()"
	}
	
}

function confirmaCambiaFiltro()
{
//	'cboActividadComercio','filtroActividadComercio'
	var pCombo = 'cboActividadComercio'
	objFiltro = document.getElementById('filtroActividadComercio');
	
	
	if (objFiltro.value.length == 0)
	{
		alert("Ingrese como m�nimo 2 caracteres.");
		if (event != null)
		{
			event.returnValue = false;
		}
		return;
	}

	//El filtro debe tener al menos 2 caracteres para poder filtrar
	if (objFiltro.value.length < 2 && objFiltro.value != "*" )
	{
		alert("Usted debe ingresar al menos 2 caracteres para poder filtrar la actividad/ocupaci�n.");
		if (event != null)
		{
			event.returnValue = false;
		}
		return;
	}
	
	datosCotizacion.setCotizar(true)
	document.getElementById("resulCotiTotales").style.display='none';
	document.getElementById("resultadoCotizacion").innerHTML=""
	
	if (objFiltro.value=="*") 
		{ filtroAnterior= "*"
			var filtro = ""
		}
		else
		{
			var filtro = objFiltro.value.toUpperCase()
			filtroAnterior= filtro
		}
	
	var mvarRequest = "<Request>"+
	"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+" </RAMOPCOD>"+
	"<USUARIO>"+ document.getElementById("LOGON_USER").value + "</USUARIO>" +
	"<FILTROACTIVIDAD>" +  filtro + "</FILTROACTIVIDAD>"	+
	"</Request>"
	
	
	document.getElementById(pCombo).length=0
	//datosCotizacion.cargaComboXSL(pCombo,llamadaMensajeMQ(mvarRequest,'lbaw_GetActividadesComercios'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboActividades.xsl")
	datosCotizacion.cargaComboXSL(pCombo,llamadaMensajeMQ(mvarRequest,'lbaw_GetActividadesComercios'),document.getElementById("getComboActividades").xml)
	document.getElementById(pCombo).style.display='inline'
	confirmaCambiaActividad()
	

	
	
}



function fncCambiaActividad()
{		
		var objDivCondiciones = document.getElementById("divBtnCondiciones");
		var objActividad = document.getElementById('cboActividadComercio');
		var codActividad=objActividad.value.split("|")[0]
		//GED 04-10-2010 DEFECT 209
		if (nroUnicoItem != 0)
			if(document.getElementById("actividadComercio").value!="" && codActividad != document.getElementById("actividadComercio").value || document.getElementById("CERTISEC").value != "0")
			{ 
				confirm_customizado("<p align='center'>�Esta seguro de modificar la actividad? <br>Se perder�n todos los datos</p>") 		
				document.getElementById("funcion_confirma").value = "confirmaCambiaActividad()"
				document.getElementById("funcion_cancela").value = "cancelaCambiaActividad()"
				return 
			}
			confirmaCambiaActividad()
			document.getElementById("actividadComercio").value = codActividad
			cambiaActividad=true
						
					if (objActividad.value == "0" || objActividad.value == "-1")
					{
						objDivCondiciones.style.display = "none";
						return ;
					}
					
					
					if (objActividad.value.split("|")[0] == "0"  || objActividad.value.split("|")[0] == "-1" )
					{
						objDivCondiciones.style.display = "none";
						return ;
					}
						else
					{
						objDivCondiciones.style.display = "inline";
						return ;
					}
		
	
}

function confirmaCambiaActividad(){
	
			datosCotizacion.setCotizar(true)
			document.getElementById("resulCotiTotales").style.display='none';
			document.getElementById("resultadoCotizacion").innerHTML=""
			var objDivCondiciones = document.getElementById("divBtnCondiciones");
			var objActividad = document.getElementById('cboActividadComercio');
			document.getElementById('divSeccionCoberturas').innerHTML=""
			document.getElementById('botonDeducibles').style.display="none"
			document.getElementById('CBO_PROVINCIA').value = "-1"
			document.getElementById('localidadRiesgo').value = ""
			document.getElementById('codPostalRiesgo').value = ""
			document.getElementById('varXmlItems').value = "<ITEMS/>"
			
			//GED 10-09-2010 DEFECT 42
			nroUnicoItem = 0
			document.getElementById('OTID').value = 1//nroUnicoItem
			
			var codActividad=objActividad.value.split("|")[0]
			var varResultTabla='<table width="100%" border="1" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">'+
				'<tr>'+
					'<td bordercolor="#dedfe7">'+
						'<table width="100%" border="0" cellspacing="0">'+
							'<tr>'+								
								'<td align="center" height="15px">No hay Items en la lista</td>'+
							'</tr>'+
						'</table>'+
					'</td>'+
				'</tr>'+
			'</table>'
			
			divItemsCargadosOtros.innerHTML = varResultTabla;	
			
			if (cambiaActividad)
			{
				document.getElementById("actividadComercio").value = codActividad
				
			}
			cambiaActividad=true
				//document.getElementById("actividadComercio").value = codActividad
				
								
					if (objActividad.value == "0"|| objActividad.value == "-1")
					{
						objDivCondiciones.style.display = "none";
						return;
					}
					
					if (objActividad.value.split("|")[0] == "0" || objActividad.value.split("|")[0] == "-1" )
					{
						objDivCondiciones.style.display = "none";
						return;
					}
						else
					{
						objDivCondiciones.style.display = "inline";
						return;
					}
	
	}	
	
	
function cancelaCambiaActividad()

{
	//alert(document.getElementById("actividadComercio").value)
	// GED 31-8-2010 DEFECT - SI RECUPERADA NO CARGA COMBO
	
	
	if(document.getElementById("actividadComercio").value.length == 5)
	{var codActividad =  "0"+document.getElementById("actividadComercio").value}
	else
	{var codActividad = document.getElementById("actividadComercio").value }	
	
	document.getElementById('filtroActividadComercio').value = filtroAnterior
	
	recuperaCombo("cboActividadComercio",codActividad ,0)
		
		
	
	 
			
	
	
}

function mostrarCondicionesActividad(pCombo)
{
	var actividad= document.getElementById(pCombo).value
	
	var mvarRequest = "<Request>"+
	"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+" </RAMOPCOD>"+
	"<USUARIO>"+ document.getElementById("LOGON_USER").value + "</USUARIO>" +
	"<I_ACTIVIDAD>" +  actividad + "</I_ACTIVIDAD>"	+
	"</Request>"
	
	var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_GetCondicionesCobertura')
	
	var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
	mobjXMLDoc.async = false
	mobjXMLDoc.loadXML(mvarResponse)	
	if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text == "true") 
	{
		var objDatos = mobjXMLDoc.selectNodes("//DATO")
		var textoCondiciones =""
		
		for (var x=0;x<objDatos.length;x++)
		{
		textoCondiciones += objDatos[x].selectSingleNode("DESCRIPCION").text + "<br/>"
		}
	
		document.getElementById('contenidoCondiciones').innerHTML= "<p>"+ textoCondiciones+"</p>"
		
	
	}
	else
	{
		//HRF Def.58 2010-10-26 Se cambia la leyenda de error por una fija
		document.getElementById('contenidoCondiciones').innerHTML= "<p>No se encontraron condiciones para la actividad seleccionada.</p>"
		//document.getElementById('contenidoCondiciones').innerHTML= "<p>"+mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text+"</p>"
	}

	
	//Fondo de pantalla
	var ancho=document.body.clientWidth
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	// Fin Fondo
	document.getElementById('popUp_condiciones').style.left  = 200
	document.getElementById('popUp_condiciones').style.top  =   body.scrollTop+100
	document.getElementById('popUp_condiciones').style.display = 'inline';
	
	
	
}

function ventanaDeducibles()
{
	var codActividad = document.getElementById('cboActividadComercio').value.split("|")[0];
	var mvarZona = document.getElementById("CODIGOZONA").value
	var cantCober= document.all.COBCHECK.length
	var mvarCoberturas =""
	var contCober =0
	for (x=0; x<cantCober; x++)
	{
		if(document.all.COBCHECK[x].checked && document.getElementById("SUMAASEG" + x ).value != "" )
		{
		contCober++
		mvarCoberturas +="<COBERTURA>"
			mvarCoberturas +="<COBERCOD>"+document.all.COBCHECK[x].cobercod+"</COBERCOD>"
			mvarCoberturas +="<NUMERMOD>"+calculaModulo(document.all.COBCHECK[x].cobercod, document.getElementById("SUMAASEG" + x ).value)+"</NUMERMOD>"
		mvarCoberturas +="</COBERTURA>"
		}
	}
	
	var mvarRequest = "<Request>"+
	"<DEFINICION>1530_DeduciblesICO.xml</DEFINICION>"+
	"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
	"<ACTIVIDAD>"+codActividad+"</ACTIVIDAD>"+
	"<CODIZONA>"+mvarZona+"</CODIZONA>"+
	"<CANT>"+contCober+"</CANT>"+	
	"<COBERTURAS>"+ 	mvarCoberturas
	mvarRequest +="</COBERTURAS>"
	mvarRequest +="</Request>"
	
	var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')
	
	var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
	mobjXMLDoc.async = false
	mobjXMLDoc.loadXML(mvarResponse)	
	if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text == "true") 
	{
		var objDatos = mobjXMLDoc.selectNodes("//DEDUCIBLE")
		var textoDeducibles ="<table>"
		
		//HRF Def.246 2010-11-01
		if (objDatos.length == 0)
			textoDeducibles += "<tr><td>NO SE ENCONTRARON DEDUCIBLES PARA LA ACTIVIDAD SELECCIONADA</td><tr/>"
		
		for (var x=0;x<objDatos.length;x++)
		{
		if (x%2==0)
		textoDeducibles += "<tr><td><b>" +getDescriCobertura(objDatos[x].selectSingleNode("COBERCOD").text) +"</b> - " + objDatos[x].selectSingleNode("FRANQDES").text + "</td><tr/>"
		else
		textoDeducibles += "<tr><td bgcolor='#d0d0d0'><b>" +getDescriCobertura(objDatos[x].selectSingleNode("COBERCOD").text) +"</b> - " + objDatos[x].selectSingleNode("FRANQDES").text + "</td><tr/>"
		
		}
		textoDeducibles += "<table>"
		document.getElementById('contenidoDeducibles').innerHTML=  textoDeducibles
		
	
	}
	else
	{
		document.getElementById('contenidoDeducibles').innerHTML= "<p>"+mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text+"</p>"
		
	}

	//Fondo de pantalla
	var ancho=document.body.clientWidth
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	// Fin Fondo
	document.getElementById('popUp_deducibles').style.left  = 200
	document.getElementById('popUp_deducibles').style.top  =   body.scrollTop+100
	document.getElementById('popUp_deducibles').style.display = 'inline';
	
}

function getDescriCobertura(pCobercod)

{	
	var cantidadCoberturas = document.all.COBCHECK.length
	for(var x=0; x<cantidadCoberturas; x++)
		{	
				if(document.all.COBCHECK[x].cobercod == pCobercod )
				{	
					
				  return 	document.all.COBCHECK[x].descripcion		
				 break
				}
			} 
	
}


	


function muestraApoderado(pObj)

{
	
	if(pObj.value=="S")
	
	{
		document.getElementById("divApoderado").style.display="inline"
		document.getElementById("apoApellido").disabled=false
		document.getElementById("apoNombre").disabled=false
		document.getElementById("cboApoTipDoc").disabled=false
		document.getElementById("apoNroDoc").disabled=false	
		document.getElementById("apoFechaNacimiento").disabled=false
		document.getElementById("apoSexo").disabled=false
		document.getElementById("apoEstadoCivil").disabled=false		
		document.getElementById("apoCodTelefono").disabled=false	
		document.getElementById("apoTelefono").disabled=false	
		document.getElementById("apoMail").disabled=false
		document.getElementById("apoCalleDomicilio").disabled=false
		document.getElementById("apoNroDomicilio").disabled=false
		
		document.getElementById("apoPisoDomicilio").disabled=false
		document.getElementById("apoDptoDomicilio").disabled=false
		
		document.getElementById("apoProvinciaDomicilio").disabled=false
		if (document.getElementById('apoProvinciaDomicilio').value > 1) {document.getElementById('apoLocalidadDomicilio').disabled = false;}
		
		document.getElementById("apoPais").disabled=false
		document.getElementById("apoCuit").disabled=false
		document.getElementById("apoActividad").disabled=false
	}
	if(pObj.value=="N")
	
	{
		document.getElementById("divApoderado").style.display="none"		
		document.getElementById("apoApellido").disabled=true
		document.getElementById("apoNombre").disabled=true
		document.getElementById("cboApoTipDoc").disabled=true
		document.getElementById("apoNroDoc").disabled=true		
		document.getElementById("apoFechaNacimiento").disabled=true
		document.getElementById("apoSexo").disabled=true
		document.getElementById("apoEstadoCivil").disabled=true
		document.getElementById("apoCodTelefono").disabled=true	
		document.getElementById("apoTelefono").disabled=true		
		document.getElementById("apoMail").disabled=true
		document.getElementById("apoCalleDomicilio").disabled=true
		document.getElementById("apoNroDomicilio").disabled=true
		
		document.getElementById("apoPisoDomicilio").disabled=true
		document.getElementById("apoDptoDomicilio").disabled=true
		
		document.getElementById("apoProvinciaDomicilio").disabled=true
		document.getElementById("apoLocalidadDomicilio").disabled=true
		
		document.getElementById("apoPais").disabled=true
		document.getElementById("apoCuit").disabled=true
		document.getElementById("apoActividad").disabled=true
	}
	
	}

function fncTipoPersonaSel()
{
	if (document.getElementById('tipoPersona').value != 15)
	{
		document.getElementById('divSolPersonaFisica').style.display = '';
		if (document.getElementById('TIPOOPER').value != "R")
		{
		document.getElementById('fechaNacimientoClienteSoli').disabled = false;
		document.getElementById('sexoCliente').disabled          = false;
		document.getElementById('estadoCivilCliente').disabled   = false;
		document.getElementById('paisNacimientoCliente').disabled          = false;
		}
	} else {
		document.getElementById('divSolPersonaFisica').style.display = 'none';
		document.getElementById('fechaNacimientoClienteSoli').disabled = true;
		document.getElementById('sexoCliente').disabled          = true;
		document.getElementById('estadoCivilCliente').disabled   = true;
		document.getElementById('paisNacimientoCliente').disabled          = true;
	}
}

function formaPagoSel()

{
document.getElementById("numTarjeta").disabled=true
document.getElementById("numCuentaDC").disabled=true
document.getElementById("numCBU").disabled=true
document.getElementById("tarvtomes").disabled=true
document.getElementById("tarvtoann").disabled=true
document.getElementById("cbo_NomTarjeta").disabled=true
document.getElementById("cboSucursalDC").disabled=true

document.getElementById("seccion_Efete").style.display="none"
document.getElementById("seccion_Tarjeta").style.display="none" 
document.getElementById("seccion_PagoCuenta").style.display="none" 	
document.getElementById("divNroCBU").style.display="none"




//document.getElementById("tblPagoCuenta").style.display="none"

var  cobroCod=document.getElementById("formaPago").value.split("|")[0]

var  cobroTip=document.getElementById("formaPago").value.split("|")[1]

//alert(cobroCod +" / "+ cobroTip)
if (cobroTip == 'EM' || cobroTip == 'EF')
    document.getElementById("ESRECFINAN").value = "S";
else
    document.getElementById("ESRECFINAN").value = "N";


var cobroDescri = document.getElementById("formaPago").options[document.getElementById("formaPago").selectedIndex].text
document.getElementById("lbSolimedioPago").innerText= cobroDescri

	switch(cobroCod)
		{
			case '1','2':	{document.getElementById("seccion_Efete").style.display="inline"}
			break;
			case '4':
			{document.getElementById("seccion_Tarjeta").style.display="inline" 
			document.getElementById("numTarjeta").disabled=false
			document.getElementById("tarvtomes").disabled=false
			document.getElementById("tarvtoann").disabled=false
			document.getElementById("cbo_NomTarjeta").disabled=false
			}
			break;
			case '5': //CUENTA BANCARIA
			{switch(cobroTip)
			{
				case 'DB':
				{				
					document.getElementById("divNroCBU").style.display="inline"
					document.getElementById("numCBU").disabled=false
				}
				break;
				
				default:
				{	
					document.getElementById("seccion_PagoCuenta").style.display="inline"	
					document.getElementById("numCuentaDC").disabled=false			
					document.getElementById("cboSucursalDC").disabled=false
				}
			break;
		   }
		}//fin case 5
			break;
			
			
	}

	
}

function verificaCUIT(pCampo,pTipo)
{
	if (document.getElementById(pCampo).value=="") return true
	
	if(pTipo)
	{
		if (document.getElementById(pTipo).value!="4" && document.getElementById(pTipo).value!="5")
		{
			if (document.getElementById(pCampo).value.length > 8)
				return "Supera el tama�o permitido para el tipo de documento."
			else
				return true
		}
	}		
	
	var mvarNRODOC = document.getElementById(pCampo).value
	
	
	mvarRequest =  "<Request>" +
			       "<USUARIO/>" +
			       "<DOCUMTIP>4</DOCUMTIP>" +
			       "<DOCUMNRO>"+ mvarNRODOC +"</DOCUMNRO>" +
		       "</Request>"

	
	var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_OVValidaNumDoc')
	
	var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
	mobjXMLDoc.async = false
	mobjXMLDoc.loadXML(mvarResponse)

 	
	
	if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text == "true") {
		
		return  true
		
	}else{
		return   mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text
		
	}
    
}
	

function fncValCliCUIT(pCampo)
{
	if (document.getElementById(pCampo).value.length > 0)
	{
		if (document.getElementById(pCampo).value.length != 11)
		{
			return 'Los datos ingresados no son correctos.';
		} else {
			return verificaCUIT(pCampo)
		}
	} else {
		//Si es cero valido obligatoriedad solo para el caso de
		//persona fisica que no se consumidor final
		if (document.getElementById('tipoPersona').value != 15 && 
			  document.getElementById('IVA').value != 3)
		{
			return 'Campo obligatorio, si el campo Condici�n de IVA es distinto de Consumidor Final';
		} else {
			return true;
		}
	}
}

function fncValidaTRJ()
{
	var  cobroCod=document.getElementById("formaPago").value.split("|")[0]
	var  cobroTip=document.getElementById("formaPago").value.split("|")[1]
	var	 mvarCTANUM = document.getElementById('numTarjeta').value
	var	 mvarVENANN =  document.getElementById('tarvtoann').value
	var	 mvarVENMES = document.getElementById('tarvtomes').value
	var mvarMarcaTrj = document.getElementById('cbo_nomTarjeta').value.split("|")[1]
	var	 mvarRequest =   "<Request>" +
			                       "<USUARIO></USUARIO>"+
				                "<BCOCOD>0</BCOCOD>"+
				                "<SUCURCOD>0</SUCURCOD>" +
				                "<COBROTIP>"+ mvarMarcaTrj +"</COBROTIP>"+
				                "<CTANUM>"+ 	mvarCTANUM 	+"</CTANUM>" +
				                "<VENANN>"+ 	mvarVENANN 	+"</VENANN>" +
				                "<VENMES>"+ 	mvarVENMES 	+"</VENMES>" +
			                "</Request>"
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
	     
			var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_OVValidaCuentas')	
			
	//	window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarResponse+"</textarea>")
		
		
		var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
		mobjXMLDoc.async = false
		mobjXMLDoc.loadXML(mvarResponse)
		if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text == "true") return  true
			else
		return   mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text
		
	
			
		
}	

function fncValidaCUE()
{	
		var  cobroTip=document.getElementById("formaPago").value.split("|")[1]
				if (document.getElementById("cboSucursalDC").value == "-1")	{return 'Debe seleccionar la Sucursal.'}
				if (document.getElementById('numCuentaDC').value == "") {return 'Debe ingresar el N�mero de Cuenta.'}
				
				//		var DataToSend = "nombCombo=TarjetaCredito&BCOCOD=" + frm.I_FPBANCO.value + "&SUCURCOD=" + frm.I_FPSUCURSAL.value + "&COBROTIP=" + frm.COBROTIP.value + "&CTANUM=" + CTANUM + "&VENANN=" + VTOANN + "&VENMES=" + VTOMES;	
				
				 var mvarCTANUM = document.getElementById('numCuentaDC').value
				var	 mvarVENANN =  ""
				var	 mvarVENMES = ""
				//GED 08-09-2010 MEJORA QC NRO 134 - SE SOLICITO PONER FIJO 150
				var canalBanco = '0150'
				var	 mvarRequest =   "<Request>" +
			                       "<USUARIO></USUARIO>"+
				                "<BCOCOD>"+canalBanco	+"</BCOCOD>"+
				                "<SUCURCOD> "+ document.getElementById("cboSucursalDC").value+ " </SUCURCOD>" +
				                "<COBROTIP>"+ cobroTip +"</COBROTIP>"+
				                "<CTANUM>"+ 	mvarCTANUM 	+"</CTANUM>" +
				                "<VENANN>"+ 	mvarVENANN 	+"</VENANN>" +
				                "<VENMES>"+ 	mvarVENMES 	+"</VENMES>" +
			                "</Request>"
			//	window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
	 
				var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_OVValidaCuentas')	
				
			//	window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarResponse+"</textarea>")
	 	
				var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
				mobjXMLDoc.async = false
				mobjXMLDoc.loadXML(mvarResponse)
				if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text == "true") return  true
					else
				//GED 08-09-2010 DEFECT 97						
				return  "Los datos ingresados de la cuenta no son correctos."
				//return   mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text
					
}				
				
function fncValidaCBU()
{	var  cobroTip=document.getElementById("formaPago").value.split("|")[1]
					//if (document.getElementById('numCBU').value == "")	return 'Falta Ingresar el n�mero de CBU.'
				//	if (document.getElementById('numCBU').value == "") return true
				//	if (document.getElementById('numCBU').value.length != 22) return 'Debe completar 22 posiciones en el n�mero de CBU.'
					
					var mvarCTANUM = document.getElementById('numCBU').value.substr(0,16);
					var mvarVENMES = document.getElementById('numCBU').value.substr(16,2);
					var mvarVENANN = document.getElementById('numCBU').value.substr(18,4);
							
					//var DataToSend = "nombCombo=TarjetaCredito&BCOCOD=0&SUCURCOD=0&COBROTIP=" + document.all.COBROTIP.value + "&CTANUM=" + CTANUM + "&VENANN=" + VTOANN + "&VENMES=" + VTOMES;	
					var	 mvarRequest =   "<Request>" +
			                       "<USUARIO></USUARIO>"+
				                "<BCOCOD>0</BCOCOD>"+
				                "<SUCURCOD>0</SUCURCOD>" +
				                "<COBROTIP>"+ cobroTip +"</COBROTIP>"+
				                "<CTANUM>"+ 	mvarCTANUM 	+"</CTANUM>" +
				                "<VENANN>"+ 	mvarVENANN 	+"</VENANN>" +
				                "<VENMES>"+ 	mvarVENMES 	+"</VENMES>" +
			                "</Request>"
			//	window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
				var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_OVValidaCuentas')	
			//		window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarResponse+"</textarea>")	
				var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
				mobjXMLDoc.async = false
				mobjXMLDoc.loadXML(mvarResponse)
				if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text == "true") return  true
					else
				return   mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text
}

function imprimirCotizacion()
{
	if( validarDatosFormulario("true"))
	{
		// GED 02-09-2010 DEFECT 43
		if(tabActual==3)  
		{	
			grabarOperacion('C',false);
			
		}
		//Fondo de pantalla
		var ancho=document.body.clientWidth
		var body = document.body, 	html = document.documentElement; 		
		var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
		document.getElementById('popUp_opacidad').style.width  = ancho
		document.getElementById('popUp_opacidad').style.height  = alto
		document.getElementById('popUp_opacidad').style.display = 'inline';
		//opacity('popUp_opacidad', 0, 10, 300);
		// Fin Fondo
		document.getElementById('popUp_Impresion').style.left  = 200
		document.getElementById('popUp_Impresion').style.top  =   body.scrollTop+100
		document.getElementById('popUp_Impresion').style.display = 'inline';
		
		//Si va a revision
		if (document.getElementById("chk_circuito").checked || document.getElementById("divTablaFueraNorma").innerHTML!="")
		{
			//HRF Def.79 2010-11-03
			document.all.copia[1].checked = true;
			document.all.copia[0].disabled = true;
			document.all.copia[2].disabled = true;
		} else {
			document.all.copia[0].disabled = false;
			document.all.copia[2].disabled = false;
		}
	}
}		

function enviarMailCotizacion()

{
	if( validarDatosFormulario("true"))
	{
	// GED 02-09-2010 DEFECT 43
		if(tabActual==3)  
		{	
			grabarOperacion('C',false);
			
		}	
	//Fondo de pantalla
	var ancho=document.body.clientWidth
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	// Fin Fondo
	document.getElementById('popUp_mailCliente').style.left  = 200
	document.getElementById('popUp_mailCliente').style.top  =   body.scrollTop+100
	document.getElementById('popUp_mailCliente').style.display = 'inline';
}
	
	
}							
				
function fncActivaRecotizar()
{
	datosCotizacion.setCotizar(true)
}

/******************************************************************************************/
function fncCambiaEMail()
{
	if (trim(document.getElementById("mailDomicilio").value) != "")
	{
		fncConformEMailSel(true);
	} else {
		fncConformEMailSel(false);
	}
}

/******************************************************************************************/
function fncConformEMailSel(pMostrar)
{
	if (pMostrar)
	{
		document.getElementById("seccion_ConformEMail").style.display = "inline";
	} else {
		document.getElementById("seccion_ConformEMail").style.display = "none";
	}
}
/******************************************************************************************/
function fncNuevaCotizacion()
{
	if (document.getElementById('hNueCotConfirm').value=='S')
	{
	    //GED 08-11-2010 DEFECT 243
	    var mensaje= document.getElementById("CERTISEC").value=="0"? "Se perder�n todos los datos" : "Se perder�n todos los datos modificados"
		confirm_customizado("<p align='center'>�Esta seguro de ir a una Nueva Cotizaci�n? <br>"+mensaje+"</p>")
		document.getElementById("funcion_confirma").value = "fncVolver();";
	} else {
		fncVolver()
	}
}
/******************************************************************************************/
function fncVolver()
{
	document.frmCotizacion.target = "_self";
	document.frmCotizacion.action = document.frmCotizacion.hRutaVolver.value;
	document.frmCotizacion.submit();
}
/******************************************************************************************/

function fncEsCircuito(pObj) {

    //alert("pObj ="+pObj.checked)
    if (pObj.checked) {
        document.getElementById("divBtnEnvioEmail").style.display = 'none'
        document.frmCotizacion.copia.item(0).disabled = true
        document.frmCotizacion.copia.item(2).disabled = true
    }
    else if (document.getElementById("divTablaFueraNorma").innerHTML == "") {
        document.getElementById("divBtnEnvioEmail").style.display = 'inline'
        document.frmCotizacion.copia.item(0).disabled = false
        document.frmCotizacion.copia.item(2).disabled = false
    }
}
//GED 02-11-2010 SE REDEFINE ESTA VARIABLE COMO GLOBAL
 var mAcumSumaAseg = 0;
function fncCoberMensajeLeyenda(CodCober,intZona,SumaAseg) {
    var ramopcod = document.getElementById("RAMOPCOD").value
    var strProvincia;
    var vCanal = datosCotizacion.getCanalURL();
   

    var objXMLLeyendas = document.getElementById('xmlMensajesLeyendas').XMLDocument;

    
    if ((parseInt(intZona) == 1) || (parseInt(intZona) == 2))
        strProvincia = "capital";
    else
        strProvincia = "interior";


    if (parseInt(CodCober) == 200 || parseInt(CodCober) == 279 || parseInt(CodCober) == 280)
        mAcumSumaAseg += parseFloat(SumaAseg)
   

    if (mAcumSumaAseg > 0) {
        var varXmlNodo = new ActiveXObject("MSXML2.DOMDocument");
	 //GED 02-11-2010 - DEFECT 234
        varXmlNodo = objXMLLeyendas.selectNodes("//" + ramopcod + "/mensaje");

        for (var j = 0; j < varXmlNodo.length; j++) {

            var intMinimo = varXmlNodo[j].selectSingleNode(strProvincia + "/minimo").text;
            var intMaximo = varXmlNodo[j].selectSingleNode(strProvincia + "/maximo").text;

            if ((mAcumSumaAseg >= intMinimo) && (mAcumSumaAseg <= intMaximo)) {
                   document.getElementById('mensajeAlerta').value = varXmlNodo[j].selectSingleNode("alerta").text;
            }
        }
    }
    else
        document.getElementById('mensajeAlerta').value = "";


}
/******************************************************************************************/
function fncActivaRenDatosModificados()
{
	document.getElementById("REN_MODIF_DAT").value = "S"	
	document.getElementById("periodoCotizar").disabled = false;
	document.getElementById("estabilizacion").disabled = false;
	
	 datosCotizacion.setCotizar(true) 
	
}
/******************************************************************************************/
function fncLeyendaImpuestos()
{
	var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
			objXMLItem.async = false;
			objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	
	var wMostrar = false;
	
	if (document.getElementById("IVA").value != "3")
	{
		if (objXMLItem.selectNodes("//ITEMS/ITEM[PROVICOD=2]").length > 0)
		{
			wMostrar = true;
		}
	}
	
	return wMostrar
}
/******************************************************************************************/
function fncArmadoRenovacion()
{	
	
	 window.status="Procesando 10%"
	 	    
	 document.getElementById("divTablaFueraNorma").innerHTML =""
	 oXMLFueraNorma.loadXML("<ERRORES></ERRORES>")	    
	    
	//## Mje 2400 ##//	
	oXMLRenovaPoliCol = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLRenovaPoliCol.async = false;       
	oXMLRenovaPoliCol.loadXML(document.getElementById("xmlRenovaPoliCol").innerHTML); 
	 //window.document.body.insertAdjacentHTML("afterBegin", "<textarea  style='position:absolute; z-index:100'>"+document.getElementById("xmlRenovaPoliCol").innerHTML+"</textarea>")									

	//####//
	
	//## Mje 1116 - Datos del Cliente ##//
	oXMLRenovaDomiCorresp = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLRenovaDomiCorresp.async = false;       
  	oXMLRenovaDomiCorresp.loadXML(document.getElementById("xmlRenovaDomiCorresp").innerHTML); 
	//####//
	
	//## Mje 3000 - Datos del Apoderado ##//
	oXMLRenovaApoderado = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLRenovaApoderado.async = false;       
  	oXMLRenovaApoderado.loadXML(document.getElementById("xmlRenovaApoderado").innerHTML); 
	//####//
	
	//## Mje 1101 ##//
	oXMLRenovaDatosGralPoliza = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLRenovaDatosGralPoliza.async = false;       
  	oXMLRenovaDatosGralPoliza.loadXML(document.getElementById("xmlRenovaDatosGralPoliza").innerHTML); 
   	//window.document.body.insertAdjacentHTML("afterBegin", "<textarea  style='position:absolute; z-index:100'>"+document.getElementById("xmlRenovaDatosGralPoliza").innerHTML+"</textarea>")									

	//####//
	
	//## Datos de Items ##//
	oXMLRenovaItems = new ActiveXObject('MSXML2.DOMDocument');        
  	oXMLRenovaItems.async = false;       
  	oXMLRenovaItems.loadXML(document.getElementById("xmlRenovaRiesgosItems").innerHTML); 
 	// window.document.body.insertAdjacentHTML("afterBegin", "<textarea  style='position:absolute; z-index:100'>"+document.getElementById("xmlRenovaRiesgosItems").innerHTML+"</textarea>")									
		
 	//## XML Unificado de datos Originales ##//
	oXMLRenovacionDatos = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLRenovacionDatos.async = false;       
  	oXMLRenovacionDatos.loadXML(document.getElementById("xmlRenovacionDatos").innerHTML);
 	//window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+oXMLRenovacionDatos.xml+"</textarea>")	
		
  	var oXMLRegItems = oXMLRenovaItems.selectNodes("//CAMPOS");
  	var varCantItems = oXMLRenovaItems.selectNodes("//CAMPOS").length;
  
 window.status="Procesando 20%" 	
	
	//Control recuperaci�n de datos
	if (!oXMLRenovaPoliCol.selectSingleNode("//OPERAPOL"))
	{
		errorInesperado("Error en la recuperaci�n de datos de la p�liza.");
		return false;
	}
	
	//Control de estado
	if (trim(oXMLRenovaDatosGralPoliza.selectSingleNode("//SITUCPOL").text.toUpperCase()) != "VIGENTE")
	{
		errorInesperado("La p�liza se encuentra en estado "+ trim(oXMLRenovaDatosGralPoliza.selectSingleNode("//SITUCPOL").text.toUpperCase()) +", no se puede continuar con la renovaci�n.",true);
		return false;
	}

	//### Solapa Datos Generales ###
	document.getElementById("OPERAPOLAIS").value = oXMLRenovaPoliCol.selectSingleNode("//OPERAPOL").text
	
	//-----------------------------------------------------------------------------------------------------//
	//Si por lo menos uno de los convenio es (M)anual, deben ir fuera de norma el PR y OR
	vControlCOMISTIP = false;
	
	if (trim(oXMLRenovaPoliCol.selectSingleNode("//COMISTIPPR1").text) != "A")
		vControlCOMISTIP = true;
	if (trim(oXMLRenovaPoliCol.selectSingleNode("//COMISTIPOR").text) != "A")
		vControlCOMISTIP = true;
	//Si tiene prod y viene una N en org
	if (trim(oXMLRenovaPoliCol.selectSingleNode("//COMISTIPPR1").text) == "A" &&
			trim(oXMLRenovaPoliCol.selectSingleNode("//COMISTIPOR").text) == "N")
				vControlCOMISTIP = false;
	
	if (vControlCOMISTIP)
	{
		//-----------------------------------------------------------------------------------------------------//
		//Tratamiento de Comiciones.
		if (trim(oXMLRenovaPoliCol.selectSingleNode("//COMISTIPPR1").text) == "A")
		{
			recuperaCombo("convenioProductor",oXMLRenovaPoliCol.selectSingleNode("//CONVECODPR1").text,0)
			vCOMPCPORPR1 = Number(document.getElementById("convenioProductor").value.split("|")[3])
		}
		else
		{
			//GED 15-02-2011 - SE CARGA COMBO PRODUCTOR CORRIGE ERROR 
			recuperaCombo("convenioProductor",oXMLRenovaPoliCol.selectSingleNode("//CONVECODPR1").text,0)		
			vCOMPCPORPR1 = Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR1").text)
			
		}
		
			
		
		if (trim(oXMLRenovaPoliCol.selectSingleNode("//COMISTIPOR").text) == "A")
		{
			fncConvenioProdSeleccionado();
			recuperaCombo("convenioOrganizador",oXMLRenovaPoliCol.selectSingleNode("//CONVECODOR").text,0)
			vCOMPCPOROR = Number(document.getElementById("convenioOrganizador").value.split("|")[3])
		}
		else
			vCOMPCPOROR = Number(oXMLRenovaPoliCol.selectSingleNode("//COMPCPOROR").text)
		//-----------------------------------------------------------------------------------------------------//
		
		var vRECARPOR = Number(oXMLRenovaPoliCol.selectSingleNode("//RECARPOR").text)		
	  var nuevoElemento=document.createElement("option")	  	 		
				nuevoElemento.text='FUERA DE CONVENIO';
				//el -2 es el cod interno de FUERA DE CONVENIO
				nuevoElemento.value='-2|-2|'+vRECARPOR+'|'+vCOMPCPORPR1;
				nuevoElemento.selected=true	
		document.getElementById("convenioProductor").add(nuevoElemento);
		
	  var nuevoElemento=document.createElement("option")
				nuevoElemento.text='FUERA DE CONVENIO';
				nuevoElemento.value='-2|-2|0|'+vCOMPCPOROR;
				nuevoElemento.selected=true;
		document.getElementById("convenioOrganizador").length=0;
		document.getElementById("convenioOrganizador").add(nuevoElemento);
		document.getElementById("convenioOrganizador").disabled=false;
		
		var vComisionTotal= vCOMPCPORPR1 + vCOMPCPOROR
		var nuevoElemento=document.createElement("option")
				nuevoElemento.text=formatoNro('',vComisionTotal,2,",")+ ' %';
				nuevoElemento.value= vCOMPCPORPR1 + vCOMPCPOROR;
				nuevoElemento.selected=true
		document.getElementById("comisionTotal").length=0;
		document.getElementById("comisionTotal").add(nuevoElemento);
		document.getElementById("comisionTotal").disabled=false;
		
		document.getElementById("COMISION_PROD").value= vCOMPCPORPR1
		document.getElementById("COMISION_ORG").value= vCOMPCPOROR
		document.getElementById("RECARGO").value=Number(oXMLRenovaPoliCol.selectSingleNode("//RECARPOR").text)
		
		document.getElementById("porcOrganizador").value= vCOMPCPOROR
		document.getElementById("porcProductor").value= vCOMPCPORPR1
	}
	else
	{
		//-----------------------------------------------------------------------------------------------------//
		//if (fncBuscarEnCombo("convenioProductor",oXMLRenovaPoliCol.selectSingleNode("//CONVECODPR1").text,0))
			recuperaCombo("convenioProductor",oXMLRenovaPoliCol.selectSingleNode("//CONVECODPR1").text,0)
		//-----------------------------------------------------------------------------------------------------//
		fncConvenioProdSeleccionado();
		//-----------------------------------------------------------------------------------------------------//
		//if (fncBuscarEnCombo("convenioOrganizador",oXMLRenovaPoliCol.selectSingleNode("//CONVECODOR").text,0))
			recuperaCombo("convenioOrganizador",oXMLRenovaPoliCol.selectSingleNode("//CONVECODOR").text,0)
		//-----------------------------------------------------------------------------------------------------//
		fncCargaComboComision();
	}
	
	fncCalculaComisiones();
	//-----------------------------------------------------------------------------------------------------//
	
	//Per�odo
	if (fncBuscarEnCombo("periodoCotizar",oXMLRenovaPoliCol.selectSingleNode("//PERIODO").text))
		recuperaCombo("periodoCotizar",oXMLRenovaPoliCol.selectSingleNode("//PERIODO").text)
	else
		fncComboAgregarItem(document.getElementById("periodoCotizar"), 
										oXMLRenovaPoliCol.selectSingleNode("//PERIODES").text,
										oXMLRenovaPoliCol.selectSingleNode("//PERIODO").text,
										true);
	fncPeriodoSeleccionado();
	
	//Estabilizaci�n
	if (fncBuscarEnCombo("estabilizacion",oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text))
		recuperaCombo("estabilizacion",oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text);
	else
		fncComboAgregarItem(document.getElementById("estabilizacion"), 
							oXMLRenovaPoliCol.selectSingleNode("//CLAUADES").text,
							oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text,
							true);
							
	document.getElementById("CLAUSULARENOV").value = oXMLRenovaPoliCol.selectSingleNode("//CLAUAJUS").text; //MAG RENOVACION
	
	document.getElementById("IngresosBrutos").length = 0;
	datosCotizacion.cargaCombo("IngresosBrutos","SIFTCIBB");
	recuperaCombo("IngresosBrutos",oXMLRenovaDomiCorresp.selectSingleNode("//CLIEIBTP").text)
	//Situaci�n IVA
	if(trim(oXMLRenovaPoliCol.selectSingleNode("//CLIENIVA").text) != "")
	{
		recuperaCombo("IVA",oXMLRenovaPoliCol.selectSingleNode("//CLIENIVA").text)
		recuperaCombo("IngresosBrutos",oXMLRenovaDomiCorresp.selectSingleNode("//CLIEIBTP").text)

	}
	else
	{
		document.getElementById("IVA").length = 0;
		datosCotizacion.cargaCombo("IVA","SIFTCIVA");
		recuperaCombo("IVA",3);		
	}
		
	//datosCotizacion.deshabilitarCampo("IVA");
	//datosCotizacion.deshabilitarCampo("IngresosBrutos");	
	fncCuitSeleccionado(document.getElementById("IVA"))
	//Tipo de Persona
	recuperaCombo("tipoPersona",oXMLRenovaDomiCorresp.selectSingleNode("//CLIENTIP").text)
	fncTipoPersonaSel()
	window.status="Procesando 30%"
	//ACTIVIDAD***************************
	document.getElementById("filtroActividadComercio").value=   "*"
	filtrarActividadCom('cboActividadComercio','filtroActividadComercio');
	
	recuperaComboxTexto("cboActividadComercio",oXMLRenovaDomiCorresp.selectSingleNode("//PROFECOD").text,true )
	if(document.getElementById("cboActividadComercio").value== -1){
		document.getElementById("divTablaFueraNorma").innerHTML=""
		errorInesperado("La actividad "+ oXMLRenovaDomiCorresp.selectSingleNode("//PROFECOD").text + " - "+oXMLRenovaDomiCorresp.selectSingleNode("//PROFEDES").text +" no se encuentra habilitada en la Oficina Virtual\n",true)
		return
	
	}
				
	
	datosCotizacion.deshabilitarCampo("filtroActividadComercio");
	datosCotizacion.deshabilitarCampo("btnFiltrarActividad");	
	datosCotizacion.deshabilitarCampo("cboActividadComercio");
	
	
	////***********************************
	
	recuperaCombo("formaPago",oXMLRenovaDatosGralPoliza.selectSingleNode("//COBROCOD").text +"|"+ oXMLRenovaDatosGralPoliza.selectSingleNode("//COBROTIPCBO").text);
	formaPagoSel()
	//Def.347 HRF 20110323
	if (fncBuscarEnCombo("planPago",oXMLRenovaPoliCol.selectSingleNode("//PLANPCOD").text,0))
		recuperaCombo("planPago",oXMLRenovaPoliCol.selectSingleNode("//PLANPCOD").text,0)
	else
		fncComboAgregarItem(document.getElementById("planPago"), 
										oXMLRenovaPoliCol.selectSingleNode("//PLANPDES").text,
										oXMLRenovaPoliCol.selectSingleNode("//PLANPCOD").text +"|0",
										true);
	//P�liza Anterior
	document.getElementById("inpNroPolizaRen").value = document.getElementById("RAMOPCOD").value +"-"+ fncSetLength(document.getElementById("POLIZANNANT").value,2) + "-" + fncSetLength(document.getElementById("POLIZSECANT").value,6);
	document.getElementById("divNroPolizaRen").style.display = "";
	document.getElementById("polizaAnterior").innerText = document.getElementById("RAMOPCOD").value +"-"+ fncSetLength(document.getElementById("POLIZANNANT").value,2) + "-" + fncSetLength(document.getElementById("POLIZSECANT").value,6);
	document.getElementById("spanPolizaAnterior").style.display='inline';
	
	//Hidden en la pag main 
	document.getElementById("divCotiFechIniFin").style.display=''
	document.getElementById("vigenciaDesde").value= oXMLRenovaPoliCol.selectSingleNode("//FECINIVIG").text.substr(6,2)  +"/"+
							oXMLRenovaPoliCol.selectSingleNode("//FECINIVIG").text.substr(4,2)  +"/"+
     							oXMLRenovaPoliCol.selectSingleNode("//FECINIVIG").text.substr(0,4)	
     							
	//## Solapa Cotizacion DatosRiesgos ##//
	
	window.status="Procesando 40%"
	//ITEMS: Recupera los datos y planes de cada riesgo
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	varXmlObjAse = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAse.async = false;
	varXmlObjAse.loadXML(document.getElementById("varXmlObjAseg").value);
		
	oXMLValores= new ActiveXObject("MSXML2.DOMDocument");
	oXMLValores.async = false;
		
	for (var x=0; x<varCantItems; x++)
	{
		var codActividad = rightString("000000" + oXMLRenovaDomiCorresp.selectSingleNode("//PROFECOD").text,6)
		var mvarProviCod =oXMLRegItems[x].selectSingleNode("PROVICOD").text
		
		
		  if(isNumber(trim(oXMLRegItems[x].selectSingleNode("CPACODPO").text)))	
		  	var mvarCpostal= oXMLRegItems[x].selectSingleNode("CPACODPO").text
		else
			var mvarCpostal= oXMLRegItems[x].selectSingleNode("CPACODPO").text.substr(1,4);
			
		
		
		var mvarRequest=	"<Request>"+
											"<PROVICOD>"+mvarProviCod+"</PROVICOD>"+
											"<CPACODPO>"+mvarCpostal+"</CPACODPO>"+
											"<RAMOPCOD>"+document.getElementById("RAMOPCOD").value+"</RAMOPCOD>"+
											"</Request>"
											
		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")									
		var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetZona') 
		
		if(oXMLValores.loadXML(mvarResponse))
		{
			if(oXMLValores.selectSingleNode("//Response/Estado/@resultado").text = "true")
			{
				if(oXMLValores.selectSingleNode("//CODIZONA").text !="0000")
				{
					var mvarZona=oXMLValores.selectSingleNode("//CODIZONA").text
				} else {
					alert_customizado("No se pudo determinar zona para esta localidad")
					return false
				}
			}
			else
			{
				alert_customizado(oXMLValores.selectSingleNode("//Response/Estado/@mensaje").text)			
				return false
			}
		}
		
		var mvarRequest = "<Request>"+
		          				"<DEFINICION>2390_ListadoCoberturas.xml</DEFINICION>"+        
		          				"<USUARCOD>" + document.getElementById("LOGON_USER").value +"</USUARCOD>" +
		          				"<CODIZONA>"+mvarZona+"</CODIZONA>" +
		          				"<PROFECOD>"+codActividad+"</PROFECOD>"+
		          				"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
	              			"</Request>"
		var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ') 
		
		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
	  var oXMLCoberturas = new ActiveXObject("MSXML2.DOMDocument");
				oXMLCoberturas.async = false;
				oXMLCoberturas.setProperty("SelectionLanguage", "XPath");
		
		if (oXMLCoberturas.loadXML(mvarResponse))
			if(oXMLCoberturas.selectSingleNode("//Response/Estado/@resultado").text = "true")
			{
				document.getElementById("xmlCoberturas").innerHTML = oXMLCoberturas.xml
				//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCoberturas:<textarea>"+oXMLCoberturas.xml+"</textarea>")
			} else {
				alert_customizado("Error al recuperar coberturas 2390")
			}
			
			var mvarRequest = "<Request>" +
	          						"<USUARIO>" + document.getElementById("LOGON_USER").value + "</USUARIO>" +
	          						"<ZONA>" + mvarZona + "</ZONA>" +
	          						"<ACTIVIDAD>" + codActividad + "</ACTIVIDAD>" +
	          						"<RAMOPCOD>" + document.getElementById("RAMOPCOD").value + "</RAMOPCOD>" +
	          						"</Request>"
		var mvarResponse = llamadaMensajeMQ(mvarRequest, 'lbaw_GetActivPreguntas')
	window.status="Procesando 50%"
		//window.document.body.insertAdjacentHTML("beforeEnd", "Preguntas recuperadas:<textarea>" + mvarResponse + "</textarea>")
		document.getElementById("xmlPreguntas").value = mvarResponse	
				
	        
		/*****************************/
		varOElement = varXml.createElement('ITEM');
		varOAttr = varXml.createAttribute('ID');
		varOAttr.value = (x+1);
			
		varOElement.setAttributeNode(varOAttr);
		varONodo = varXml.createElement("ESTADO");
		varONodo.text = "";
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("ID");
		varONodo.text = (x+1);
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("PROVICOD");
		varONodo.text = oXMLRegItems[x].selectSingleNode("PROVICOD").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("DOMICDOM");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICDOM").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("DOMICDNU");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICDNU").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("DOMICPIS");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICPIS").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("DOMICPTA");
		varONodo.text = oXMLRegItems[x].selectSingleNode("DOMICPTA").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("CANTALUM");
		//////varONodo.text = oXMLRegItems[x].selectSingleNode("CANTALUM").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("PROVICODDES");
		for(var i=0; i<document.getElementById("CBO_PROVINCIA").options.length;i++)	
		{
			if (document.getElementById("CBO_PROVINCIA").options[i].value==oXMLRegItems[x].selectSingleNode("PROVICOD").text)
				varONodo.text = document.getElementById("CBO_PROVINCIA").options[i].text
		}
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("DOMICPOB");
		var localidad= oXMLRegItems[x].selectSingleNode("LOCALIDAD").text;
		varONodo.text = localidad
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("CPOSDOM");
		
	/*	if (oXMLRegItems[x].selectSingleNode("PROVICOD").text==1)
		{
			if (oXMLRegItems[x].selectSingleNode("CPACODPO").text)		
				varONodo.text = oXMLRegItems[x].selectSingleNode("CPACODPO").text.substr(1,4);
			else
				varONodo.text = "";			
		} else 
		{*/
			if (trim(oXMLRegItems[x].selectSingleNode("CPACODPO").text))
			{	//Tambien si no es capital en algunos casos puede venir el cp en formato nuevo B1824EAV, hay q mandar solo el 1824
				if(isNumber(trim(oXMLRegItems[x].selectSingleNode("CPACODPO").text)))
					varONodo.text = trim(oXMLRegItems[x].selectSingleNode("CPACODPO").text);
				else
					varONodo.text = trim(oXMLRegItems[x].selectSingleNode("CPACODPO").text.substr(1,4));
			} 
			else
			{
				varONodo.text = "0";
			}
		//}
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("ZONAITEM");
		varONodo.text = mvarZona ;
		varOElement.appendChild(varONodo);
		
		varONodo = varXml.createElement("ITEMDESC");
		var itemDesc = localidad //+"-" +eval(x+1);
		varONodo.text = itemDesc
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("ALARMA");
		varONodo.text = oXMLRegItems[x].selectSingleNode("ALARMTIP").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("GUARDIA");
		varONodo.text = oXMLRegItems[x].selectSingleNode("GUARDTIP").text;
		varOElement.appendChild(varONodo);
		
		//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
		varONodo = varXml.createElement("IDISYUNT");
		varONodo.text = oXMLRegItems[x].selectSingleNode("SWDISYUN").text;
		varOElement.appendChild(varONodo);
		
	//FASE 2
		varONodo = varXml.createElement("CERTIPOLANT");
		varONodo.text = oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text;
		varOElement.appendChild(varONodo);
			
		varONodo = varXml.createElement("CERTIANNANT");
		varONodo.text = oXMLRegItems[x].selectSingleNode("CERTIANNANT").text;
		varOElement.appendChild(varONodo);
		
		//var certisecAnt = 	oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text +"-"+oXMLRegItems[x].selectSingleNode("CERTIANNANT").text +"-"+oXMLRegItems[x].selectSingleNode("CERTISECANT").text;
		// DEFECT 284
		var certisecAnt = oXMLRegItems[x].selectSingleNode("CERTISECANT").text
		varONodo = varXml.createElement("CERTISECANT");
		varONodo.text = oXMLRegItems[x].selectSingleNode("CERTISECANT").text;
		varOElement.appendChild(varONodo);
			
		varXml.selectSingleNode("//ITEMS").appendChild(varOElement);
			
		varONodo = varXml.createElement("COBERTURAS");
		varOElement.appendChild(varONodo);
		
		var oXMLRegCobs = oXMLRenovaItems.selectNodes("//CAMPOS[@ID='"+eval(x+1)+"']/COBERTURAS/COBERTURA");
		var varCantCobs = oXMLRegCobs.length;
		//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLRegCobs<textarea>"+oXMLRegCobs.xml+"</textarea>")
		for (var y=0; y<varCantCobs; y++)
		{
			//alert(oXMLRegCobs[y].selectSingleNode("COBERCOD").text +" "+ oXMLCoberturas.selectSingleNode("//COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/COBERDES"))
			if (!oXMLCoberturas.selectSingleNode("//COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/COBERDES"))
			{
				errorInesperado("POLIZA "+document.getElementById("POLIZANNANT").value+"-"+document.getElementById("POLIZSECANT").value+": No puede realizarse la Renovaci�n porque las coberturas existentes no coinciden con las permitidas.\n",true)
				document.getElementById("mensajeErrorDescrip").innerText = ""
				return false
			}
			//**********************************
	window.status="Procesando 60%"		
			/*if (document.getElementById("CERTISEC").value == "0")	
				if(fncCoberturaFueraManual(x+1, certisecAnt,oXMLRegCobs[y].selectSingleNode("COBERCOD").text,oXMLRegCobs[y].selectSingleNode("SUMAASEG").text))
				{ 
					fueraManual[x+1] = true
				
				}*/
			
			
			//**********************************
			var varONodoCob = varXml.createElement("COBERTURA");
			
			varONodo = varXml.createElement("IDITEM");
			varONodo.text = (x+1);
			varONodoCob.appendChild(varONodo);	
					
			varONodo = varXml.createElement("COBERCOD");
			varONodo.text = oXMLRegCobs[y].selectSingleNode("COBERCOD").text;
			varONodoCob.appendChild(varONodo);
			
			var varSumaAseg=0
			
				varSumaAseg = oXMLRegCobs[y].selectSingleNode("SUMAASEG").text;
		
			fncCoberMensajeLeyenda(oXMLRegCobs[y].selectSingleNode("COBERCOD").text, mvarZona, varSumaAseg);
					
			varONodo = varXml.createElement("COBERDES");
			if (oXMLCoberturas.selectSingleNode("//COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/COBERDES"))			
			    varONodo.text = oXMLCoberturas.selectSingleNode("//COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/COBERDES").text
			else
			    varONodo.text =""
			  
			varONodoCob.appendChild(varONodo);

			varONodo = varXml.createElement("SUMAASEG");
			varONodo.text = oXMLRegCobs[y].selectSingleNode("SUMAASEG").text;
			varONodoCob.appendChild(varONodo)
			
			
			var vPRIMA2420 = 0;
				if (oXMLRegItems[x].selectSingleNode("//CAMPOS[@ID='"+(x+1)+"']/COTIZACIONES/COTIZACION[COBERCOD='"+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"']/PRIMAIMP"))
					 vPRIMA2420 = oXMLRegItems[x].selectSingleNode("//CAMPOS[@ID='"+(x+1)+"']/COTIZACIONES/COTIZACION[COBERCOD='"+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"']/PRIMAIMP").text;
			
			
					
			varONodo = varXml.createElement("PRIMA");
			varONodo.text = vPRIMA2420
			varONodoCob.appendChild(varONodo)
			
			
			var vTASA2420 = 0;	
			if (oXMLRegItems[x].selectSingleNode("//CAMPOS[@ID='"+(x+1)+"']/COTIZACIONES/COTIZACION[COBERCOD='"+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"']/TARIFPOR"))
					 vTASA2420 = oXMLRegItems[x].selectSingleNode("//CAMPOS[@ID='"+(x+1)+"']/COTIZACIONES/COTIZACION[COBERCOD='"+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"']/TARIFPOR").text
			
			
			varONodo = varXml.createElement("TASA");
			varONodo.text = vTASA2420
			varONodoCob.appendChild(varONodo)
			
			varONodo = varXml.createElement("SWTASAVM");
			varONodo.text = ""
			varONodoCob.appendChild(varONodo)
			
			if (oXMLCoberturas.selectSingleNode("//COBERTURAS/COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/TEXTOCOD"))
			{
			var marcaObjEsp = oXMLCoberturas.selectSingleNode("//COBERTURAS/COBERTURA[COBERCOD="+oXMLRegCobs[y].selectSingleNode("COBERCOD").text+"]/TEXTOCOD").text
			
			}
		else
			{
			errorInesperado("No coinciden las coberturas en el item a renovar",true)
			return false
			}
			
			window.status="Procesando 70%"
			
			varONodo = varXml.createElement("OBJASEG");
			varONodo.text = marcaObjEsp
			
			// oXMLRegCobs[y].selectSingleNode("SEWRIEDES").text=="S"?"N":"S";//LO TRAE AL REVES
			varONodoCob.appendChild(varONodo)
			
			varXml.selectSingleNode('//ITEM[ID='+eval(x+1)+']/COBERTURAS').appendChild(varONodoCob);
		}
		
		//Objetos Asegurados
		var oXMLObjAseg = oXMLRenovaItems.selectNodes("//CAMPOS[@ID='"+eval(x+1)+"']/OBJETOS/OBJETO");
		var varCantObj = oXMLObjAseg.length;
		var cobActual=0
		var cobAnterior=0
		var cont=0

		for (var y=0; y<varCantObj; y++)
		{
			if (y==0)
				cobAnterior=oXMLObjAseg[y].selectSingleNode("COBERCOD").text
			cobActual=oXMLObjAseg[y].selectSingleNode("COBERCOD").text
				
			if (cobActual==cobAnterior)
				cont++
			else
				cont=1
				
			if (oXMLRenovaItems.selectSingleNode("//CAMPOS[@ID='"+eval(x+1)+"']/COBERTURAS/COBERTURA[COBERCOD='"+cobActual+"']/RIESGTIP")!=null){
			varONodoObj = varXmlObjAse.createElement("OBJETO");
			varONodo = varXmlObjAse.createElement("IDITEM");
			//GED 23-02-2011 DEFECT 284
			varOAttr = varXmlObjAse.createAttribute('CERTISEC');
			varOAttr.value = certisecAnt;			
			varONodo.setAttributeNode(varOAttr);
			//FIN 284
			varONodo.text=eval(x+1) 
			varONodoObj.appendChild(varONodo);
	
			varONodo = varXmlObjAse.createElement("COBERCOD");
			varONodo.text=oXMLObjAseg[y].selectSingleNode("COBERCOD").text
			varONodoObj.appendChild(varONodo);
			
			varONodo = varXmlObjAse.createElement("NROOBJ");
			varONodo.text=cont
			//hacer la logica		
			varONodoObj.appendChild(varONodo);
			
			varONodo = varXmlObjAse.createElement("TIPORIESGO");
			varONodo.text=oXMLRenovaItems.selectSingleNode("//CAMPOS[@ID='"+eval(x+1)+"']/COBERTURAS/COBERTURA[COBERCOD='"+cobActual+"']/RIESGTIP").text;
			varONodoObj.appendChild(varONodo);
	
			varONodo = varXmlObjAse.createElement("CODIGOOBJETO");
			varONodo.text=oXMLObjAseg[y].selectSingleNode("TEXTOCOD").text;
			varONodoObj.appendChild(varONodo);
			
			varONodo = varXmlObjAse.createElement("OBJETODES");
			varONodo.text="";
			varONodoObj.appendChild(varONodo);
					
			varONodo = varXmlObjAse.createElement("SUMAASEG");
			varONodo.text=oXMLObjAseg[y].selectSingleNode("SUMAASEG").text;
			varONodoObj.appendChild(varONodo);

			varONodo = varXmlObjAse.createElement("DETALLEOBJETO");
			varONodo.text=oXMLObjAseg[y].selectSingleNode("TEXTODES").text;
			varONodoObj.appendChild(varONodo);
			
			cobAnterior=oXMLObjAseg[y].selectSingleNode("COBERCOD").text
			
			varXmlObjAse.selectSingleNode("//OBJETOS").appendChild(varONodoObj);
			}
		}
			
		var tempProvincia = oXMLRegItems[x].selectSingleNode("PROVICOD").text
		var tempLocalidad = oXMLRegItems[x].selectSingleNode("LOCALIDAD").text;
	//GED 22-02-2011 
	document.getElementById("varXmlRenovObjAseg").value= varXmlObjAse.xml
	}

//*** FASE 2
	//XML de Items
	//document.getElementById("varXmlItems").value= varXml.xml
	
	//Solo carga los items cuando es una renovaci�n no grabada
	//alert(document.getElementById("CERTISEC").value)
	if (document.getElementById("CERTISEC").value == 0)
	{
		document.getElementById("varXmlObjAseg").value= varXmlObjAse.xml
		document.getElementById("varXmlItems").value= varXml.xml		
		fncFueraManual();	   
	      fncMuestraFueraNorma() ;	
	}
	window.status="Procesando 80%"
	//carga los varoles originales de los items
	
	if(varXml.selectNodes("//ITEM").length > 0)	
	{
		if(varXml.selectNodes("//ITEM").length > xmlItemNroMaximo)
		{
			errorInesperado("La p�liza posee mas items de los permitidos para el producto, no se puede continuar con la renovaci�n.",true);
			return false;
		} else {
			document.getElementById("varXmlRenovItems").value = varXml.xml;
		}
	} else {
		errorInesperado("La p�liza no posee certificados vigentes",true);
		return false;
	}
   	//window.document.body.insertAdjacentHTML("beforeEnd", "ais<textarea style='position:absolute; z-index:100'>"+document.getElementById('varXmlRenovItems').value+"</textarea>")	
	
	//document.getElementById("varXmlRenovObjAseg").value= varXmlObjAse.xml//varXml.xml
	
	//XML de Objetos Asegurados
	//document.getElementById("varXmlObjAseg").value= varXmlObjAse.xml
		
	//actualizarListadoXML(datosCotizacion.getCanalURL()  + "Canales/" + datosCotizacion.getCanal() )
	 actualizarListadoXML(datosCotizacion.getCanalUrlImg() )					
	fncCargarRiesgoSolicitud();
	
	
	////////////////////////////////////////
	
  	//### Solapa Datos Clientes ###//      	
	//1116
	
	
	//var DOMICCAL 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICCAL").text)
	var DOMICDOM 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICDOM").text)
	var DOMICDNU 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICDNU").text)
	var DOMICESC 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICESC").text)
	var DOMICPIS 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICPIS").text)
	var DOMICPTA 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICPTA").text)
	if (trim(oXMLRenovaDomiCorresp.selectSingleNode("//PROVICOD").text)==1)
	{	
		
		if(isNumber(trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text)))
				var DOMICCPO = trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text);
			else
				var DOMICCPO = trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text.substr(1,4));
		
		
		//				
		//if (trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text))		
			//var DOMICCPO = trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text).substr(1,4);
		//else
			//var DOMICCPO = "0";			
	}	
	else
	{
		if (trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text))
		{	//Tambien si no es capital en algunos casos puede venir el cp en formato nuevo B1824EAV, hay q mandar solo el 1824
			if(isNumber(trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text)))
				var DOMICCPO = trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text);
			else
				var DOMICCPO = trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text.substr(1,4));
		}
		else
			var DOMICCPO = "0";
	}
	var DOMICPOB 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICPOB").text)
	//var DOMICCPO 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//CPACODPO").text)
	var PROVICOD 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//PROVICOD").text)
	var PAISSCOD 	= trim(oXMLRenovaDomiCorresp.selectSingleNode("//PAISSCOD").text)
	var TELCOD 		= ""
	var TELNRO 		= trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOMICTLF").text)

	
	
	//Tip.Doc. Cliente
	if (fncBuscarEnCombo("tipoDocCliente",Number(oXMLRenovaDomiCorresp.selectSingleNode("//DOCUMTIPCOD").text)))
		recuperaCombo("tipoDocCliente",Number(oXMLRenovaDomiCorresp.selectSingleNode("//DOCUMTIPCOD").text))
	else
		fncComboAgregarItem(document.getElementById("tipoDocCliente"), 
										oXMLRenovaDomiCorresp.selectSingleNode("//DOCUMTIP").text,
										Number(oXMLRenovaDomiCorresp.selectSingleNode("//DOCUMTIPCOD").text),
										true);
	//Nro.Doc. Cliente
	document.getElementById("nroDocumentoCliente").value =trim(oXMLRenovaDomiCorresp.selectSingleNode("//DOCUMDAT").text)
	document.getElementById("apellidoCliente").value =trim(oXMLRenovaDomiCorresp.selectSingleNode("//CLIENAP1").text) + trim(oXMLRenovaDomiCorresp.selectSingleNode("//CLIENAP2").text)
	document.getElementById("nombreCliente").value =trim(oXMLRenovaDomiCorresp.selectSingleNode("//CLIENNOM").text)
	
	document.getElementById("tipoDocCliente").disabled = true;
	document.getElementById("nroDocumentoCliente").disabled = true;
	document.getElementById("apellidoCliente").disabled = true;
	document.getElementById("nombreCliente").disabled = true;
	document.getElementById("divBtnBuscarCliente").style.display = "none";
	//GED 16-9-2010 DEFECT 164
	document.getElementById("divLeyBuscarCliente").style.display = "none";
	
	document.getElementById("fechaNacimientoClienteSoli").value = trim(oXMLRenovaDomiCorresp.selectSingleNode("//FECNACIM").text);
	
	if(oXMLRenovaDomiCorresp.selectSingleNode("//SEXO").text== "MASCULINO")
		recuperaCombo("sexoCliente","M")	
	else if(oXMLRenovaDomiCorresp.selectSingleNode("//SEXO").text== "FEMENINO")
		recuperaCombo("sexoCliente","F")	
	else
		fncComboAgregarItem(document.getElementById("sexoCliente"),
												"NO INFORMADO",
												"",
												true)
	
	recuperaCombo("estadoCivilCliente",oXMLRenovaDomiCorresp.selectSingleNode("//CLIENEST").text)
	if(trim(oXMLRenovaDomiCorresp.selectSingleNode("//PAISSCOD").text)!="")
		recuperaCombo("paisNacimientoCliente",oXMLRenovaDomiCorresp.selectSingleNode("//PAISSCOD").text)
	else
		recuperaCombo("paisNacimientoCliente",00)
		
	//Mail
	var vCantMedios = oXMLRenovaDomiCorresp.selectSingleNode("//MEDCOSEC").text
	var vEMAIL
	if (vCantMedios > 0)
		vEMAIL = trim(oXMLRenovaDomiCorresp.selectSingleNode("//MEDIOCONTACTOS/MEDIOCONTACTO[MEDCOCOD='EMA']/MEDCODAT").text)
	else
		vEMAIL = ""
	
	document.getElementById("codTelefonoCliente").value =TELCOD
	document.getElementById("telefonoCliente").value =TELNRO
	document.getElementById("emailCliente").value =vEMAIL
	document.getElementById("calleDomicilio").value =DOMICDOM
	document.getElementById("nroDomicilio").value =DOMICDNU
	document.getElementById("pisoDomicilio").value =DOMICPIS
	document.getElementById("dptoDomicilio").value =DOMICPTA	
	if(PROVICOD!="0") 
	{
		recuperaCombo("provinciaDomicilio",PROVICOD)
		fncProvinciaSeleccionadaConCP(document.getElementById("provinciaDomicilio"),"localidadDomicilio","codPosDomicilio");
		document.getElementById("localidadDomicilio").value = DOMICPOB		
	}	
	if(DOMICCPO!="0")	document.getElementById("codPosDomicilio").value =DOMICCPO
	document.getElementById("CodTelPosDomicilio").value =TELCOD
	document.getElementById("TelPosDomicilio").value =TELNRO
	document.getElementById("mailDomicilio").value = vEMAIL

	//Solapa Soli Datos Grales
	document.getElementById("nroCuit").value = oXMLRenovaDomiCorresp.selectSingleNode("//CUIT").text
	
	if (trim(oXMLRenovaDomiCorresp.selectSingleNode("//PROFECOD").text)!="")
	{
		//alert("ArmadoRenovacion")	
		recuperaCombo("actividadesCliente",oXMLRenovaDomiCorresp.selectSingleNode("//PROFECOD").text)
	 	document.getElementById("actividadesCliente").style.display="inline"
	  	document.getElementById("filtro_OCUPACODCLIE").value=document.getElementById("ACTIVIDADCLIENTE").value
	  //Preguntar...
	  //filtrarActividades('actividadesCliente','filtro_OCUPACODCLIE')
	}
window.status="Procesando 90%"
	//fncCambiaEMail();
	
	if(Number(oXMLRenovaPoliCol.selectSingleNode("//CANTANOT").text) > 0)
	{
		document.getElementById("observClausulas").value = oXMLRenovaPoliCol.selectSingleNode("//CAMPOS/ANOTACIONES[TIPOANOTA='I']/TEXTO").text;
	  	document.getElementById("textoPoliza").value = oXMLRenovaPoliCol.selectSingleNode("//CAMPOS/ANOTACIONES[TIPOANOTA='F']/TEXTO").text;
	}

	
	//### Solapa - Datos Comerciales ###//
	document.getElementById("nroSolicitud").value = oXMLRenovaPoliCol.selectSingleNode("//EXPEDNUM").text;
	
	//Apoderado
	wTIENEAPODERADO = oXMLRenovaPoliCol.selectSingleNode("//CLIENSECAP").text>0?"S":"N";
	recuperaCombo("tieneApoderado",wTIENEAPODERADO);
	if (wTIENEAPODERADO=="S")
	{
		muestraApoderado(document.getElementById("tieneApoderado"));
		var PROVICODAPOD = trim(oXMLRenovaApoderado.selectSingleNode("//PROVICOD").text);
		
		if (trim(oXMLRenovaApoderado.selectSingleNode("//PROVICOD").text)==1)
		{
			if(isNumber(trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text)))
				var DOMICCPOAPOD = trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text);
			else
				var DOMICCPOAPOD = trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text.substr(1,4));
		}	else {
			if (trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text))
			{	//Tambien si no es capital en algunos casos puede venir el cp en formato nuevo B1824EAV, hay q mandar solo el 1824
				if(isNumber(trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text)))
					var DOMICCPOAPOD = trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text);
				else
					var DOMICCPOAPOD = trim(oXMLRenovaApoderado.selectSingleNode("//CPACODPO").text.substr(1,4));
			} else {
				var DOMICCPOAPOD = "0";
			}
		}
		
		//Apellido
		document.getElementById("apoApellido").value = trim(oXMLRenovaApoderado.selectSingleNode("//CLIENAP1").text) + trim(oXMLRenovaApoderado.selectSingleNode("//CLIENAP2").text);
		//Nombre
		document.getElementById("apoNombre").value=		trim(oXMLRenovaApoderado.selectSingleNode("//CLIENNOM").text);
		//Tip.Doc.
		recuperaCombo("cboApoTipDoc",oXMLRenovaApoderado.selectSingleNode("//DOCUMTIPCOD").text);
		//Nro.Doc.
		document.getElementById("apoNroDoc").value=		trim(oXMLRenovaApoderado.selectSingleNode("//DOCUMDAT").text);
		//Fec.Nacimiento
		document.getElementById("apoFechaNacimiento").value = trim(oXMLRenovaApoderado.selectSingleNode("//FECNACIM").text);
		//Sexo
		if(oXMLRenovaApoderado.selectSingleNode("//SEXO").text== "MASCULINO")
			recuperaCombo("apoSexo","M")	
		else if(oXMLRenovaApoderado.selectSingleNode("//SEXO").text== "FEMENINO")
			recuperaCombo("apoSexo","F")	
		else
			fncComboAgregarItem(document.getElementById("apoSexo"),
													"NO INFORMADO",
													"",
													true)
		//Estado Civil
		recuperaCombo("apoEstadoCivil",oXMLRenovaApoderado.selectSingleNode("//CLIENEST").text);
		//Pa�s
		if(trim(oXMLRenovaApoderado.selectSingleNode("//PAISSCOD").text)!="")
			recuperaCombo("apoPais",trim(oXMLRenovaApoderado.selectSingleNode("//PAISSCOD").text))
		else
			recuperaCombo("apoPais",00)
		//CUIT
		document.getElementById("apoCuit").value  = oXMLRenovaApoderado.selectSingleNode("//CUIT").text
		//Nro.Tel�fono
		document.getElementById("apoTelefono").value = trim(oXMLRenovaApoderado.selectSingleNode("//DOMICTLF").text);
		
		var vCantMedios = oXMLRenovaApoderado.selectSingleNode("//MEDCOSEC").text
		var vEMAIL
		if (vCantMedios > 0)
			vEMAIL = trim(oXMLRenovaApoderado.selectSingleNode("//MEDIOCONTACTOS/MEDIOCONTACTO[MEDCOCOD='EMA']/MEDCODAT").text)
		else
			vEMAIL = ""
		
		//EMail
		document.getElementById("apoMail").value = trim(vEMAIL);
		//Actividad
		if (oXMLRenovaApoderado.selectSingleNode("//PROFECOD").text!="")
		{
			recuperaCombo("apoActividad",oXMLRenovaApoderado.selectSingleNode("//PROFECOD").text)	
		  document.getElementById("apoActividad").style.display="inline"
	  	document.getElementById("filtro_apoActividad").value=   document.getElementById("ACTIVIDADAPOD").value 
	 	  filtrarActividades('apoActividad','filtro_apoActividad')
		}
		//Calle
		document.getElementById("apoCalleDomicilio").value = trim(oXMLRenovaApoderado.selectSingleNode("//DOMICDOM").text);
		//N�mero
		if (trim(oXMLRenovaApoderado.selectSingleNode("//DOMICDNU").text)!="" && Number(trim(oXMLRenovaApoderado.selectSingleNode("//DOMICDNU").text)) > 0)
			document.getElementById("apoNroDomicilio").value = Number(trim(oXMLRenovaApoderado.selectSingleNode("//DOMICDNU").text));
		else
			document.getElementById("apoNroDomicilio").value = trim(oXMLRenovaApoderado.selectSingleNode("//DOMICDNU").text);
		//Piso
		document.getElementById("apoPisoDomicilio").value = trim(oXMLRenovaApoderado.selectSingleNode("//DOMICPIS").text);
		//Dpto.
		document.getElementById("apoDptoDomicilio").value = trim(oXMLRenovaApoderado.selectSingleNode("//DOMICPTA").text);
		if(PROVICODAPOD!="0")
		{
			//Provincia
			recuperaCombo("apoProvinciaDomicilio",PROVICODAPOD);
			fncProvinciaSeleccionadaConCP(document.getElementById("apoProvinciaDomicilio"),"apoLocalidadDomicilio","apoCodPosDomicilio");
			//Localidad
			document.getElementById("apoLocalidadDomicilio").value = trim(oXMLRenovaApoderado.selectSingleNode("//DOMICPOB").text);
		}
		//Cod.Postal
		if(DOMICCPOAPOD!="0")	document.getElementById("apoCodPosDomicilio").value =DOMICCPOAPOD
	}// Fin apoderado
		
	//Vendedor
	document.getElementById("vendBanco").value = document.getElementById("INSTITUCION").value +" "+ document.getElementById("BANCONOM").value;
	document.getElementById("CODEMP").value = oXMLRenovaPoliCol.selectSingleNode("//VENDEDDOR").text 
	recuperaCombo("cbo_vendSucursal",oXMLRenovaPoliCol.selectSingleNode("//SUCURCOD").text)
	document.getElementById("legajoVendedor").value =oXMLRenovaPoliCol.selectSingleNode("//VENDEDDOR").text 
	if (trim(document.getElementById("legajoVendedor").value)!="")
		fncBuscarVendedor(document.getElementById("INSTITUCION").value,  document.getElementById('cbo_vendSucursal').value, document.getElementById('legajoVendedor').value, document.getElementById("CANALHSBC").value)

	recuperaCombo("cbo_vendSucursal",oXMLRenovaPoliCol.selectSingleNode("//SUCURCOD").text)

	//Formas de Pago
	//LR 28/07/2010 Se corrige la recuperacion de las formas de pago
	//Tarjeta
	if (document.getElementById("seccion_Tarjeta").style.display != 'none')
	{
		//Def.291 HRF 20110329 - Falta que venga el TARJECOD del AIS, sino no se puede hacer
		if (fncBuscarEnCombo("cbo_nomTarjeta",oXMLRenovaDatosGralPoliza.selectSingleNode("//COBROTIP").text,1))
			recuperaCombo("cbo_nomTarjeta",oXMLRenovaDatosGralPoliza.selectSingleNode("//COBROTIP").text,1);
		else
			fncComboAgregarItem(document.getElementById("cbo_nomTarjeta"),
											oXMLRenovaDatosGralPoliza.selectSingleNode("//COBRODES").text,
											oXMLRenovaDatosGralPoliza.selectSingleNode("//TARJECOD").text +"|"+ oXMLRenovaDatosGralPoliza.selectSingleNode("//COBROTIP").text,
											true);
											
		recuperaCombo("cbo_nomTarjeta",oXMLRenovaDatosGralPoliza.selectSingleNode("//COBROTIP").text,1);
		document.getElementById("numTarjeta").value = trim(oXMLRenovaDatosGralPoliza.selectSingleNode("//CUENTNUM").text);
		if(oXMLRenovaPoliCol.selectSingleNode("//VENCIMIENTO").text.length > 0)
		{
			recuperaCombo("tarvtomes",Number(oXMLRenovaPoliCol.selectSingleNode("//VENCIMIENTO").text.substr(4,2)));
			
			recuperaCombo("tarvtoann",oXMLRenovaPoliCol.selectSingleNode("//VENCIMIENTO").text.substr(0,4));
		}
	}
	//Debito en Cuenta
	if (document.getElementById("seccion_PagoCuenta").style.display != 'none')
	{
		document.getElementById("numCuentaDC").value = trim(oXMLRenovaDatosGralPoliza.selectSingleNode("//CUENTNUM").text);
		recuperaCombo("cboSucursalDC",fncSetLength(oXMLRenovaPoliCol.selectSingleNode("//SUCURCOD").text,4));
	}
	//CBU
	if (document.getElementById("divNroCBU").style.display != 'none')
	{
		document.getElementById("numCBU").value = trim(oXMLRenovaDatosGralPoliza.selectSingleNode("//CUENTNUM").text);
	}
	 fncFechaFin("C")
	fncAyudaCampoRenovacion()
	
	//Domicilio
	fncEditarDomicilio(true)
	
	//Validaci�n FN AIS
	//GED - 20/7/2011 - CR5 PUNTO 7 
//	if (document.getElementById("SWSINIES").value == "S")
//		document.getElementById("divPoseeSiniestralidad").innerHTML = "<p style='margin-left:15px;padding:3px 3px 3px 3px; width:670px; height:20px;vertical-align:middle; font-size: 10px; font-weight: bold; background-color:#dedfde; color: #000000; font-family:Verdana; text-align:left; text-decoration: none' >La p�liza a renovar posee siniestralidad</p>"
		//insertarFueraNorma("","", "","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
	//if (document.getElementById("SWEXIGIB").value == "S")
		//insertarFueraNorma("","", "","La p�liza a renovar posee deuda exigible",0,"N","SWEXIGIB")
		
	//CR5 - MANTENER CUBIERTO
	
	   //EL INICIO DE VIGENCIA SUPERA EL MES
	        var wFechaHoy = document.getElementById("fechaHoy").value
              var wFechaSuperaMesTemp=convierteFecha(document.getElementById("fechaHoy").value,"AAAAMMDD","DD/MM/AAAA")
              var wFechaIniVig = convierteFecha(document.getElementById("vigenciaDesde").value,"DD/MM/AAAA","AAAAMMDD")
            
           //GED 21-07-2011 CR5 MANTENER CUBIERTO
           //CAMBIAR POR EL SW QUE VIENE DEL AIS
           var wFechaMantenerCubierto = convierteFecha(dateAddExtention(parteFecha(wFechaSuperaMesTemp,"DD/MM/AAAA","DD"),parteFecha(wFechaSuperaMesTemp,"DD/MM/AAAA","MM"),parteFecha(wFechaSuperaMesTemp,"DD/MM/AAAA","AAAA"),"m", -1),"DD/MM/AAAA","AAAAMMDD")           
         
           if (wFechaIniVig < wFechaHoy)              
	          if (wFechaIniVig < wFechaMantenerCubierto) 
	          {
	            errorInesperado("POLIZA "+document.getElementById("POLIZANNANT").value+"-"+document.getElementById("POLIZSECANT").value+":  ESTA POLIZA SE ENCUENTRA VENCIDA",true)
	            document.getElementById("mensajeErrorDescrip").innerText = ""               
	            return;
	          }
	          else
	          {
	          document.getElementById("SWMANCUB").value ="S"
	          //alert("MANTENER CUBIERTO")          
	         
	          }
         //
	
	if (document.getElementById("SWSINIES").value == "S" && document.getElementById("TIPOOPER")=="R"){	
		if (oXMLFueraNorma.selectNodes("//ERRORES/SOLAPA").length!=0){
			if (!flagPoseeSiniestralidad){
				if(oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='0']/ERROR").length==1){					
					if(oXMLFueraNorma.selectSingleNode("ERRORES/SOLAPA[@nro='0']/ERROR/CAMPO").text!="SW"){
						flagPoseeSiniestralidad = true	
						insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
					}	
				}
				else{
					flagPoseeSiniestralidad = true	
					insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
				}					
			}
		}
	}	

	document.getElementById("xmlfueraNorma").value = oXMLFueraNorma.xml
	var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
			varXslFueraNorma.async = false;
			varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);
	varTablaFueraNorma = oXMLFueraNorma.transformNode(varXslFueraNorma);
	document.getElementById("divTablaFueraNorma").innerHTML = varTablaFueraNorma;
	
	//06/01/2011 LR Se agrega Fuera de Norma para Convenios y Comisiones.
	fncRenovConvComisFN();
	
      window.status="Listo"
	return true
}
//****************************************************************************
// GED 11-11-2010  - NUEVAS FUNCIONES ETAPA 2

function fncAyudaCampoRenovacion()
{
	//Valor original para renovaci�n
	var varXmlCampos = document.getElementById('xmlDefinicionCamposForm').XMLDocument;
	for (i=0; i< varXmlCampos.selectSingleNode('//CAMPOS').childNodes.length ; i++ )
	{
		var varCampo = varXmlCampos.selectSingleNode('//CAMPOS').childNodes[i].attributes.getNamedItem('nombre').value;
		
		if (varXmlCampos.selectSingleNode('//CAMPOS/CAMPO[@nombre="'+ varCampo +'"]/@idNodoRenovacion'))
		{
			try	{
				var varValor = varXmlCampos.selectSingleNode('//CAMPOS').childNodes[i].attributes.getNamedItem('tipo').value;
			} catch(err) {}
			if (varValor == "combo") 		
			{
				try{
					document.getElementById( varCampo ).onmouseover=new Function ("showAlt('<b>Valor Inicial:</b> <br/>"+ trim(recuperaTextoDeCombo(varCampo).toUpperCase()) +"')");
					document.getElementById( varCampo ).onmouseout=new Function ("hideAlt()");
					document.getElementById( varCampo ).valorOriginal = document.getElementById( varCampo ).value;
				} catch(err) {
					if (document.getElementById( varCampo ))
						document.getElementById( varCampo ).valorOriginal = ""
				}
			} else {
				try {
				    
				    document.getElementById( varCampo ).valorOriginal = document.getElementById( varCampo ).value;
				    //GED 04-04-2011 - Se escapea para que no de error al cargar un campo con apostrofe
					document.getElementById( varCampo ).onmouseover=new Function ("showAlt(\"<b>Valor Inicial:</b> <br/>"+ trim(document.getElementById( varCampo ).value.toUpperCase()) +" \")");					
				    document.getElementById( varCampo ).onmouseout=new Function ("hideAlt()");
					
				} catch(err) {
					if (document.getElementById( varCampo ))
						document.getElementById( varCampo ).valorOriginal = ""
				}
			}
		}
	}
	varXmlCampos = null;
}



function fncCambiarColorCpoRen(pCampo)
{

if(document.getElementById("TIPOOPER").value=="R")
	{
		if (pCampo)
		{
			var varChanged = false;
			var varCampoId = pCampo.id;
			var varXmlCampos = document.getElementById('xmlDefinicionCamposForm').XMLDocument;
			
			if (varXmlCampos.selectSingleNode('//CAMPOS/CAMPO[@nombre="'+ varCampoId +'"]/@idNodoRenovacion'))
				if (pCampo.valorOriginal != null)
					if (trim(pCampo.value.toUpperCase()) != trim(pCampo.valorOriginal.toUpperCase()))
						varChanged = true;
						
	if (!varChanged)
			{
				//eliminarXMLCambios(varCampoId)
				pCampo.style.fontWeight="normal";
			} else {
				//var nroSolapa = varXmlCampos.selectSingleNode('//CAMPOS/CAMPO[@nombre="'+ varCampoId +'"]/@solapa').value
				//insertarXMLCambios("","", nroSolapa, varCampoId, vNuevo, vInicial, "M")				
				pCampo.style.fontWeight="bold";
			}
		}
	}
	
	varXmlCampos = null;
}
function fncAyudaItemRenovacion(pCertisec)
{
	
	
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	//varXmlNodoItem = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	if (varXml.loadXML(document.getElementById("varXmlRenovItems").value))
	{
		
	var varXmlNodoItem = varXml.selectNodes("//ITEMS/ITEM[CERTISECANT="+ pCertisec +"]/COBERTURAS/COBERTURA");
	
	var cantNodos =varXmlNodoItem.length
		
	var totalCoberturas =  document.all.COBCHECK? document.all.COBCHECK.length:0;
	
	
		for(var x=0; x<totalCoberturas;x++)		
		{	//GED 18-02-2011 DEFECT 283
			//if(document.all.COBCHECK[x].checked)
			//{
				var wEnc = false;
				for(var y=0; y<cantNodos;y++)	
				{
					if (varXmlNodoItem[y].selectSingleNode("COBERCOD").text == document.all.COBCHECK[x].cobercod)
					{	//alert(varXmlNodoItem[y].selectSingleNode("SUMAASEG").text)
						document.getElementById( "SUMAASEG"+x ).onmouseover=new Function ("showAlt('<b>Valor Inicial:</b> <br/>"+ varXmlNodoItem[y].selectSingleNode("SUMAASEG").text +"')");
						document.getElementById( "SUMAASEG"+x ).onmouseout=new Function ("hideAlt()");
						document.getElementById( "SUMAASEG"+x ).valorOriginal = varXmlNodoItem[y].selectSingleNode("SUMAASEG").text;
						wEnc = true;
					}
				}
				if (!wEnc)
				{
					document.getElementById( "SUMAASEG"+x ).onmouseover=new Function ("showAlt('<b>Valor Inicial:</b> <br/>"+ "0" +"')");
					document.getElementById( "SUMAASEG"+x ).onmouseout=new Function ("hideAlt()");
					document.getElementById( "SUMAASEG"+x ).valorOriginal = "";
				}
			/*} else {
				document.getElementById( "SUMAASEG"+x ).onmouseover=new Function ("showAlt('<b>Valor Inicial:</b> <br/>"+ "0" +"')");
				document.getElementById( "SUMAASEG"+x ).onmouseout=new Function ("hideAlt()");
				document.getElementById( "SUMAASEG"+x ).valorOriginal = "";
			}*/
		}
	}
}

function fncAyudaObjetos()
{
	var varObjAseg;
	varObjAseg = new ActiveXObject('MSXML2.DOMDocument');
	varObjAseg.async = false;
	varObjAseg.loadXML(document.getElementById("varXmlObjAseg").value);
	
	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	
	
	if(document.all.txtTotalSuma){
	var totalCoberturas = document.all.txtTotalSuma.length
	
	for(var x=0; x<totalCoberturas;x++)	
	{	
		var nroItem =  document.all.txtTotalSuma[x].id.split("_")[1]
		var objXMLCobertura =  varXml.selectNodes("//COBERTURA[IDITEM='"+nroItem+"']");
		
		var sumaAseg = varObjAseg.selectSingleNode("//OBJETO[IDITEM="+ nroItem +" and COBERCOD="+ document.all.txtTotalSuma[x].cobercod +"]/SUMAASEG").text
		//alert(document.getElementById( document.all.txtTotalSuma[x].id ).value +" *** " +sumaAseg)	
		if (document.getElementById( document.all.txtTotalSuma[x].id ).value != sumaAseg )
			document.getElementById( document.all.txtTotalSuma[x].id  ).style.fontWeight="bold";
		
		document.getElementById( document.all.txtTotalSuma[x].id  ).onmouseover=new Function ("showAlt('<b>Valor Inicial:</b> "+ sumaAseg +"')");			
		document.getElementById(document.all.txtTotalSuma[x].id  ).onmouseout=new Function ("hideAlt()");			
		
			
	 	
	}
}
}
//GED 23-02-2011 DEFECT 284
function fncClickCoberturasRen(pThis){
	if(document.getElementById("TIPOOPER").value=="R")
	{
		var certisec = document.getElementById("spCertisecAnt").innerText
		if (!pThis.checked)
		{
			var varCampoId = pThis.id
			insertaXMLBaja(varCampoId)
			
			if(existeObjXMLRen(pThis.cobercod,certisec))
				{	
					var varXmlObj = new ActiveXObject("MSXML2.DOMDocument");	
					varXmlObj.async = false;
					varXmlObj.loadXML(document.getElementById("varXmlObjAsegAux").value)
					
					objetos = varXmlObj.selectNodes("//OBJETOS/OBJETO[COBERCOD="+pThis.cobercod+"]")
				
				for (var x= 0 ; x<objetos.length; x++)
					{	
						varXmlObj.selectSingleNode("//OBJETOS").removeChild(objetos[x]);
						
					}
				document.getElementById("varXmlObjAsegAux").value = varXmlObj.xml
				}
			
		}
		else
		{
			
				
				if(existeObjXMLRen(pThis.cobercod,certisec))
				{	var varXmlObj = new ActiveXObject("MSXML2.DOMDocument");	
					varXmlObj.async = false;
					varXmlObj.loadXML(document.getElementById("varXmlObjAsegAux").value)
					
					var varXmlObjRen = new ActiveXObject("MSXML2.DOMDocument");	
					varXmlObjRen.async = false;
					varXmlObjRen.loadXML(document.getElementById("varXmlRenovObjAseg").value)
					
					objetos = varXmlObjRen.selectNodes("//OBJETOS/OBJETO[IDITEM/@CERTISEC='"+ certisec  +"'][COBERCOD="+pThis.cobercod+"]")
					for (var x= 0 ; x<objetos.length; x++)
					{
						
						varXmlObj.selectSingleNode("//OBJETOS").appendChild(objetos[x])
					}
					document.getElementById("varXmlObjAsegAux").value = varXmlObj.xml
					
				}
				
		}
			
	}
}

// FUNCION QUE SE INVOCA DESDE EDITAR ITEM PARA RESALTAR COBERTURAS MODIFICADAS 
function fncCambiarColorTodas()
{
	
if( document.getElementById("spCertisecAnt").innerText != "")

{
	var idCertisec = document.getElementById("spCertisecAnt").innerText
	var totalCoberturas = 	document.all.COBCHECK? document.all.COBCHECK.length:0;	// document.all.COBCHECK.length;
		for(var i=0; i<totalCoberturas;i++)
		{
			var puntero = getPunteroCobertura(document.all.COBCHECK[i].cobercod)	
			if (document.all.COBCHECK[i].checked)
			{						
				fncCambiarColorItemRen(idCertisec ,document.getElementById("SUMAASEG"+puntero) , document.all.COBCHECK[i].cobercod)				
			}
		
				
			
		}
	}
}



function existeItemXMLRen(pNroItem)
{
	
		var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlItems").value))
		{	
			
			if( varXml.selectSingleNode("//ITEMS/ITEM[@ID='"+ pNroItem  +"']/CERTISECANT").text== "")
				return false
			else
				return true
	
		}
	
}

function getCertisecXMLRen(pNroItem)
{
	
		var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlItems").value))
		{	
			
			if( varXml.selectSingleNode("//ITEMS/ITEM[@ID='"+ pNroItem  +"']/CERTISECANT").text!= "")				
			
			return	varXml.selectSingleNode("//ITEMS/ITEM[@ID='"+ pNroItem  +"']/CERTISECANT").text
		else
			return ""
	
		}
	
}

function existeCoberturaXMLRen(pCobercod,pNroItem)
{
	var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlRenovItems").value)){
			
			if( varXml.selectNodes("//ITEMS/ITEM[CERTISECANT='"+ pNroItem  +"']/COBERTURAS/COBERTURA[COBERCOD="+pCobercod+"]").length== 0)
				return false
			else
				return true
			
			
			}
	
}

function existeCoberturaXMLItem(pCobercod,pNroItem)
{
	var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlItems").value)){
			
			if( varXml.selectNodes("//ITEMS/ITEM[@ID='"+ pNroItem  +"']/COBERTURAS/COBERTURA[COBERCOD="+pCobercod+"]").length== 0)
				return false
			else
				return true
			
			
			}
	
}

function existeCoberturaCertisecXMLRen(pCobercod,pCertisec)
{
	var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlRenovItems").value)){
			
			if( varXml.selectNodes("//ITEMS/ITEM[CERTISECANT='"+ pCertisec  +"']/COBERTURAS/COBERTURA[COBERCOD="+pCobercod+"]").length== 0)
				return false
			else
				return true
			
			
			}
	
}
//DEFECT 284
function existeObjXMLRen(pCobercod,pCertisec)
{
	var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlRenovObjAseg").value)){
			if( varXml.selectNodes("//OBJETOS/OBJETO[IDITEM/@CERTISEC='"+ pCertisec  +"'][COBERCOD="+pCobercod+"]").length== 0)
				return false
			else
				return true
			
			
			}
	
}

function getItemDescRen(pCertisec){
	
var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlRenovItems").value))
		{	
			return varXml.selectSingleNode("//ITEM[CERTISECANT='"+pCertisec+"']/ITEMDESC").text
		}

}

function getItemDesc(pItem){
	
var varXml = new ActiveXObject("MSXML2.DOMDocument");	
		varXml.async = false;
		if (varXml.loadXML(document.getElementById("varXmlItems").value))
		{	
			return varXml.selectSingleNode("//ITEM[@ID='"+pItem+"']/ITEMDESC").text
		}

}

/******************************************************************************************/
function fncCambiarColorCpoRenTotal()
{
	if(document.getElementById("TIPOOPER").value=="R")
	{
		var varXmlCpos = document.getElementById("xmlDefinicionCamposForm").XMLDocument;
		for(var x=0;x<varXmlCpos.selectSingleNode('//CAMPOS').childNodes.length;x++)
		{ 
			if (varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('idNodoRenovacion'))
			{	if(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('idNodoRenovacion').text == "FECINIVIG") alert(document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).value)
				fncCambiarColorCpoRen(document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value))
			}
		}
	
		varXmlCpos = null;
	}
}
/******************************************************************************************/
function fncCambiarColorItemRen(pItem, pCampo, pCobercod)  //nro Item, ID input de suma, codigo cobertura
{
	// RESALTA COBERTURA E INSERTA EN XML CAMBIO
	if(document.getElementById("TIPOOPER").value=="R")
	{
		var varXml = new ActiveXObject("MSXML2.DOMDocument");	
				varXml.async = false;
		
		if (varXml.loadXML(document.getElementById("varXmlRenovItems").value))
		{
			var vInicial = varXml.selectSingleNode("//ITEMS/ITEM[@ID="+pItem+"]/COBERTURAS/COBERTURA[COBERCOD="+pCobercod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[@ID="+pItem+"]/COBERTURAS/COBERTURA[COBERCOD="+pCobercod+"]/SUMAASEG").text:0		     
			var existeCob = varXml.selectSingleNode("//ITEMS/ITEM[@ID="+pItem+"]/COBERTURAS/COBERTURA[COBERCOD="+pCobercod+"]")? true: false
			
			if (pCampo.value != vInicial)
		  {
		  	pCampo.style.fontWeight="bold";
			} else {
				pCampo.style.fontWeight="normal";
			}
		}
	}
}
/******************************************************************************************/
/*function fncEsCircuito(pObj){
	
	//alert("pObj ="+pObj.checked)
	if (pObj.checked){
		document.getElementById("divBtnEnvioEmail").style.display='none'
			
	}
	else if(document.getElementById("divTablaFueraNorma").innerHTML=="")
	{
		document.getElementById("divBtnEnvioEmail").style.display='inline'
		
	}
}*/
/********************************************************************************************************************/
function fncCheckNomCUITOpc()
{
	//19/10/2010 LR Defect 21 Estetica, pto 35
	//Datos cliente cotizaci�n - Campo nombre: Al seleccionar tipo de documento CUIT, si bien el campo es opcional, sigue figurando en azul. Deber�a estar en negro
	var wvalTipDoc = document.getElementById("tipoDocCliente").value;
	//alert(wvalTipDoc)
	var varCampo = "nombreCliente";
	//Si es CUIT el campo es opcional
	if (wvalTipDoc == 4) 
		document.getElementById( varCampo + "_etiq").style.color="#000000";
	else
	{
		//alert("asd1")
		document.getElementById( varCampo + "_etiq").className="luzObligatorio";
		document.getElementById( varCampo + "_etiq").style.color="blue";
	}
	
}
//HRF 2010-11-08 Def.27
/******************************************************************************************/
function fncLocalidadLimpiar(pProvincia, pCodPostal, pCalleOLocalidad)
{
	//Capital Federal
	if (document.getElementById(pProvincia).value == "1")
	{
		if (pCalleOLocalidad=="C") {document.getElementById(pCodPostal).value = "0";}
	} else {
		if (pCalleOLocalidad=="L") {document.getElementById(pCodPostal).value = "0";}
	}
}
/******************************************************************************************/
function fncValCalle(wProvincia,wCodPostal,wCalle,wNroCalle)
{
	if (document.getElementById(wCalle).disabled == false)
	{
		if (document.getElementById(wProvincia).value == "1")
		{
			if (document.getElementById(wCodPostal).value == "0" || 
				  document.getElementById(wCodPostal).value == "" )
			{
				return true;
			} else {
				return fncVerificarCalleNro(document.getElementById(wCalle).value,document.getElementById(wNroCalle).value,document.getElementById(wCodPostal).value);
			}
		} else {
			return true;
		}
	} else {
		return true;
	}
}
/******************************************************************************************/
/******************************************************************************************/
function fncRecuperarDatos()
{
	confirm_customizado("<p align='center'>�Esta seguro de recuperar los datos originales de la cotizaci�n? <br>Se perder�n todos los datos modificados</p>")
	document.getElementById("funcion_confirma").value = "fncRecuperarDatosEsperar();"
}
/******************************************************************************************/
function fncRecuperarDatosEsperar()
{
	//Mostrar espera
	document.getElementById("spanEsperando").innerHTML="<b><br/>Recuperando datos originales, por favor aguarde un instante.</b>"
	document.getElementById("spanEsperando").style.display="inline"
	document.body.style.cursor = "wait";
	document.body.style.cursor = "default";
	document.getElementById("capaEsperando").style.display = 'inline';
	var body = document.body, 	html = document.documentElement;
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	document.getElementById("capaEsperando").style.height=altoPagina
	document.getElementById("spanEsperando").style.display= 'inline'
	//Llamada a la funci�n
  setTimeout("fncRecuperarDatosContinuar();",0);
}
/******************************************************************************************/
function fncRecuperarDatosContinuar()
{
	//fncLimpiarItem();
	document.getElementById("varXmlItems").value="<ITEMS/>";
	document.getElementById("varXmlRenovItems").value="";
	document.getElementById("varXmlObjAseg").value="<OBJETOS/>";
	document.getElementById("varXmlRenovObjAseg").value="<OBJETOS/>";
	document.getElementById("divTablaFueraNorma").innerHTML = "";
	document.getElementById("xmlFueraNorma").value ="<ERRORES/>"
	oXMLFueraNorma.loadXML("<ERRORES/>")
	//document.getElementById("divTablaFueraNorma").style.display='none'
	document.getElementById("divTablaErrores").innerHTML = "";
	//document.getElementById("divTablaErrores").style.display='none'
	
	document.getElementById("verZona").innerText = ""
	
	document.getElementById('divSeccionCoberturas').innerHTML=""
	document.getElementById('botonDeducibles').style.display="none"
	document.getElementById('divBotonAgregarItem').style.display = "none"
	document.getElementById('divBotonEditarItem').style.display = "none"	
	
	//ocultaPreguntas()		
	document.getElementById("spCertipolAnt").innerHTML =""
	document.getElementById("spCertiannAnt").innerHTML =""
	document.getElementById("spCertisecAnt").innerHTML =""
	document.getElementById("spCertificado").style.display = "none"
	
	document.getElementById("CBO_PROVINCIA").disabled= false 	
	document.getElementById("localidadRiesgo").disabled= false 
	document.getElementById("codPostalRiesgo").disabled= false 
	
	document.getElementById("cboTipoAlarma").disabled= false 
	document.getElementById("cboTipoGuardia").disabled= false 

	// Adriana 9-2-2011 Defect UAT : No se ocultan las preguntas al recuperar datos iniciales
	//document.getElementById("seccion_Preguntas").style.display = "inline"
	ocultaPreguntas()
	
	document.getElementById("CBO_PROVINCIA").value = -1
	document.getElementById("codPostalRiesgo").value = ""
	document.getElementById("localidadRiesgo").value = ""
	document.getElementById("cboTipoAlarma").value = 3
	document.getElementById("cboTipoGuardia").value = 3
	
	document.getElementById("calleRiesgoItem").value =""
	document.getElementById("nroCalleRiesgoItem").value =""
	document.getElementById("pisoRiesgoItem").value = ""
	document.getElementById("dptoRiesgoItem").value = ""
		
	//sumarTotalesItem()
	
	//Pone variable en false para que vuelva a parsear cuando se inserta un item
	
	mvarItemsParseados=false		
	
	
	
	//if(!validarItemsVacios()) return 
	if (fncArmadoRenovacion())
	{	//DEFECT 297
		//document.getElementById("varXmlItems").value = document.getElementById("varXmlRenovItems").value;
		document.getElementById("varXmlObjAseg").value = document.getElementById("varXmlRenovObjAseg").value;
		
		actualizarListadoXML(datosCotizacion.getCanalUrlImg());
		fncCambiarColorCpoRenTotal();
	if (document.getElementById("CERTISEC").value != 0)	
	{ document.getElementById("varXmlItems").value = document.getElementById("varXmlRenovItems").value;
		actualizarListadoXML(datosCotizacion.getCanalUrlImg());
	    fncFueraManual();
	    
	 }
	  
	   	
	   
	   fncMuestraFueraNorma() ;
	       		
	      
		//Limpiar espera
		document.getElementById("capaEsperando").style.display= 'none'	
		var body = document.body, 	html = document.documentElement; 		
		var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
		document.getElementById("capaEsperando").style.height=altoPagina
		document.getElementById("spanEsperando").style.display= 'none'
		
		datosCotizacion.setCotizar(true)
		
		//Ir a Solapa 1
		mostrarTab('divDatosGenerales','','#9CBACE','');
	}
}
/******************************************************************************************/
function fncRenovacionCambios(pSolapa)
{
	
	oXMLCambios.loadXML("<CAMBIOS/>") 
	
			
	
	if(document.getElementById("TIPOOPER").value=="R")
	{
		var varXmlCpos = document.getElementById("xmlDefinicionCamposForm").XMLDocument;
		for(var x=0;x<varXmlCpos.selectSingleNode('//CAMPOS').childNodes.length;x++)
		{	//Si existe el Nodo Renovaci�n
			if (varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('idNodoRenovacion'))
			{ //Si es una solapa permitida
				if (Number(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('solapa').value) <= Number(pSolapa))
				{ //Si cambi� el valor
					if (trim(document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).value.toUpperCase()) != 
							trim(document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).valorOriginal.toUpperCase()))
					{ //Si es combo
						
					if(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('tipo')){
						if (varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('tipo').value == "combo")
						{
							var wValorAct = recuperaTextoDeCombo(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value);
							
							//Valor anterior
							var wValorAnt = "";
							if (fncBuscarEnCombo(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value,document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).valorOriginal))
							{
								//Si lo encuentra
								wValorAnt = recuperaBuscaTextoDeCombo(document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value),document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).valorOriginal)
								//Si es SELECCIONAR...
								if (trim(wValorAnt.toUpperCase())=="SELECCIONAR...")
									wValorAnt = ""
							} else {
								//Si no lo encuentra en el combo lo busca en el Tooltip
								try {
									var wValorAux = document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).onmouseover.toString();
									wValorAnt = wValorAux.substr(wValorAux.indexOf("<br/>")+5,wValorAux.length - (wValorAux.indexOf("<br/>")+9));
								} catch(err){
									wValorAnt = "";
								}
							}
							
							var wNodo = fncInsertarXMLCambios(oXMLCambios,
															varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('solapa').value,
															varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value,
															wValorAct,
															wValorAnt)
						//Si no es combo
						} else {
							var wNodo = fncInsertarXMLCambios(oXMLCambios,
															varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('solapa').value,
															varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value,
															document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).value,
															document.getElementById(varXmlCpos.selectSingleNode('//CAMPOS').childNodes[x].attributes.getNamedItem('nombre').value).valorOriginal)
						}
						oXMLCambios.selectSingleNode("//CAMBIOS").appendChild(wNodo);
					}
					}
				}
			}
		}
		
		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+ varXmlCambios.xml +"</textarea>")
		varXmlCpos = null;
		
		//return varXmlCambios;
	}
}
/******************************************************************************************/
function fncComboAgregarItem(pCombo, pDescrip, pValor, pSelected)
{
	var vNewOption = document.createElement("option")
	vNewOption.text = pDescrip;
	vNewOption.value = pValor;
	vNewOption.selected = pSelected;
	pCombo.add(vNewOption);
}
/******************************************************************************************/
function fncEditarDomicilioConf()
{
	if (document.getElementById("TIPOOPER").value == "R")
	{	
		confirm_customizado("�Quiere modificar el domicilio de correspondencia?") 
		document.getElementById("funcion_confirma").value = "fncEditarDomicilio(false)"
	}
}
/******************************************************************************************/
function fncEditarDomicilio(pValor)
{
	document.getElementById("calleDomicilio").disabled = pValor;
	document.getElementById("nroDomicilio").disabled = pValor;
	document.getElementById("pisoDomicilio").disabled = pValor;
	document.getElementById("dptoDomicilio").disabled = pValor;
	document.getElementById("provinciaDomicilio").disabled = pValor;
	if (pValor == false)
	{
		if (document.getElementById("provinciaDomicilio").value > 1)
			document.getElementById("localidadDomicilio").disabled = pValor;
	} else {
		document.getElementById("localidadDomicilio").disabled = pValor;
	}
	
	if (pValor)
	{
		document.getElementById("btnBuscarCPDom").style.display = "none";
		document.getElementById("divBotonEditarDom").style.display = "";
	}	else {
		document.getElementById("btnBuscarCPDom").style.display = "";
		document.getElementById("divBotonEditarDom").style.display = "none";
	}
}
/******************************************************************************************/
function fncEditarDomicilioChk()
{
	try {
		if (trim(document.getElementById("calleDomicilio").value.toUpperCase())!= trim(document.getElementById("calleDomicilio").valorOriginal.toUpperCase()))
			return false
		if (trim(document.getElementById("nroDomicilio").value.toUpperCase())!= trim(document.getElementById("nroDomicilio").valorOriginal.toUpperCase()))
			return false
		if (trim(document.getElementById("pisoDomicilio").value.toUpperCase())!= trim(document.getElementById("pisoDomicilio").valorOriginal.toUpperCase()))
			return false
		if (trim(document.getElementById("dptoDomicilio").value.toUpperCase())!= trim(document.getElementById("dptoDomicilio").valorOriginal.toUpperCase()))
			return false
		if (trim(document.getElementById("provinciaDomicilio").value.toUpperCase())!= trim(document.getElementById("provinciaDomicilio").valorOriginal.toUpperCase()))
			return false
		if (trim(document.getElementById("localidadDomicilio").value.toUpperCase())!= trim(document.getElementById("localidadDomicilio").valorOriginal.toUpperCase()))
			return false
		
		return true
		
	} catch(err) {
		return false
	}
}
/******************************************************************************************/
function fncRenovConvComisFN()
{
	//06/01/2011 LR - Chequeo si va o no a Circuito de Revision
	var vStatusRenovConvComisFN = "";
	
	if(document.getElementById("TIPOOPER").value == "R")
	{
		//En caso de traer convenio manual, siempre debe irse al circuito de revisi�n.
		if(document.getElementById("convenioProductor").value.split("|")[0] == "-2")
			vStatusRenovConvComisFN = 1;
		if(document.getElementById("convenioOrganizador").value.split("|")[0] == "-2")
			vStatusRenovConvComisFN = 1;
		//En caso que al renovar el convenio del a�o pasado este habilitado pero el productor decida 
		//modificarlo por otro, se enviar� la renovaci�n al circuito de revisi�n para ser aprobada.
		if(fncCambioValorRenov(document.getElementById("convenioProductor")))
			vStatusRenovConvComisFN = 2;
		if(fncCambioValorRenov(document.getElementById("convenioOrganizador")))
			vStatusRenovConvComisFN = 2;
		//Integrar tanto en las altas como en las renovaciones los convenios, 
		//siempre y cuando no se modifique la comisi�n definida en estos.  
		if(fncCambioValorRenov(document.getElementById("comisionTotal")))
			vStatusRenovConvComisFN = 2;
		
		//Validaci�n FN AIS
		oXMLFueraNorma.loadXML(document.getElementById("xmlFueraNorma").value);
		
		if(oXMLFueraNorma.selectNodes("//SOLAPA[@nro=1]").item(0))
		{
			varXmlNodo= oXMLFueraNorma.selectNodes("//SOLAPA[@nro='1']").item(0)
			oXMLFueraNorma.selectSingleNode("//ERRORES").removeChild(varXmlNodo);
		}
		
		//Adriana 26-3-13 Prueba de no mandar a circuito por Fuera de Convenio
		//if (vStatusRenovConvComisFN == 1)
		//	insertarFueraNorma("", "", "" , "Condiciones comerciales fuera de convenio",1,"N","convenioProductor")
		if (vStatusRenovConvComisFN == 2)
			insertarFueraNorma("", "", "" ,"Modificaci�n de las condiciones comerciales",1,"N","convenioProductor")

	if (document.getElementById("SWSINIES").value == "S" && document.getElementById("TIPOOPER").value == "R"){
		if (oXMLFueraNorma.selectNodes("//ERRORES/SOLAPA").length!=0){
			if (!flagPoseeSiniestralidad){
				if(oXMLFueraNorma.selectNodes("ERRORES/SOLAPA[@nro='0']/ERROR").length==1){					
					if(oXMLFueraNorma.selectSingleNode("ERRORES/SOLAPA[@nro='0']/ERROR/CAMPO").text!="SW"){
						flagPoseeSiniestralidad = true	
						insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")
					}	
				}
				else{
					flagPoseeSiniestralidad = true	
					insertarFueraNorma("","", "SW","La p�liza a renovar posee siniestralidad",0,"N","SWSINIES")				
				}				
			}			
		}
	}	

			
		document.getElementById("xmlfueraNorma").value = oXMLFueraNorma.xml
		
		var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
		varXslFueraNorma.async = false;
		varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);
		
		varTablaFueraNorma = oXMLFueraNorma.transformNode(varXslFueraNorma);
		
		document.getElementById("divTablaFueraNorma").innerHTML = varTablaFueraNorma;
		
	}
}
/******************************************************************************************/
function fncModificarSumasMinimas()
{
	//RECORRE TODAS LAS COBERTURAS
	var cantidadCoberturas = document.all.COBCHECK.length
	for(var x=0; x<cantidadCoberturas; x++)
	{
		if(document.all.COBCHECK[x].checked) 
			if (Number(document.getElementById("SUMAASEG" +x).value) < topeMinimoModulo(document.all.COBCHECK[x].cobercod,moduloActual))
				document.getElementById("SUMAASEG" +x).value= topeMinimoModulo(document.all.COBCHECK[x].cobercod,moduloActual)
	}
	fncCambiarColorTodas()
}

//Adriana 5-5-2011 Mejoras Prorrateo de Prima m�nima
function fncProrrateoPrimaMinima(pXML)
{
	var objXMLRequest2340 = new ActiveXObject("Msxml2.DOMDocument");
	objXMLRequest2340.async = false; 
	objXMLRequest2340.loadXML(document.getElementById("xmlRequest2340").innerHTML);
	
	var oXMLRegItemsAnt = objXMLRequest2340.selectNodes("//ITEMS");
	var cantItemAnt = oXMLRegItemsAnt.length;
	
	var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	objXMLItem.async = false;	
	objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	
	var pXMLVal = new ActiveXObject('MSXML2.DOMDocument');    
	pXMLVal.async = false;
	pXMLVal.loadXML(pXML)
	
	var pXMLItem = pXMLVal.selectNodes("//ITEM");
	
	
	var oXMLRegItems = objXMLItem.selectNodes("//ITEM");
	var cantItem = oXMLRegItems.length;
	
	for(var x=0; x<cantItem; x++)
	{
		mvarRequest='<Request>' +
		'<DEFINICION>2341_Prorrateo _Primas.xml</DEFINICION>'+
		'<USUARCOD>'+ document.getElementById("LOGON_USER").value  +'</USUARCOD>'+
		'<NROCOTI>'+objXMLRequest2340.selectSingleNode("//NROCOTI").text+'</NROCOTI>'+
		'<RAMOPCOD>'+ document.getElementById("RAMOPCOD").value +'</RAMOPCOD>'+
		'<NROITEM>'+Number(x+1)+'</NROITEM>'+
		'<PRIMAITEM_A>'+ oXMLRegItemsAnt[x].selectSingleNode("PRIMAITEM").text +'</PRIMAITEM_A>'+
		'<PRIMAITEM_N>'+ pXMLItem[x].selectSingleNode("PRIMAITEM").text +'</PRIMAITEM_N>'+
		'<RC>0</RC>'				
		var objXMLCobertura =  objXMLItem.selectNodes("//COBERTURA[IDITEM='"+Number(x+1)+"']");
		mvarRequest+='<CANTCOBER>'+objXMLCobertura.length+'</CANTCOBER>'
		
			
			for (var y=0;y<objXMLCobertura.length;y++)
			{	mvarRequest+='<COBERS>'
				mvarRequest+='<COBERCOD>'+ objXMLCobertura[y].selectSingleNode("COBERCOD").text  +'</COBERCOD>'
				mvarRequest+='<SUMAASEG>'+ objXMLCobertura[y].selectSingleNode("SUMAASEG").text  +'</SUMAASEG>'
				mvarRequest+='<TARIFPOR>'+  objXMLCobertura[y].selectSingleNode("TASA").text +'</TARIFPOR>'
				mvarRequest+='<PRIMACOB>'+  objXMLCobertura[y].selectSingleNode("PRIMA").text +'</PRIMACOB>'
				mvarRequest+='</COBERS>'
			}
			
			mvarRequest+='</Request>'
			
			var respuestaMQ=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')
			
			var objXMLrespuestaMQ = new ActiveXObject("Msxml2.DOMDocument");
			objXMLrespuestaMQ.async = false;	
			objXMLrespuestaMQ.loadXML(respuestaMQ);
			
			var coberRespuestaMQ= objXMLrespuestaMQ.selectNodes("//COBERTURA")
			
			for (var y=0;y<coberRespuestaMQ.length;y++)
			{	var coberActual = coberRespuestaMQ[y].selectSingleNode("COBERCOD").text
			
				objXMLItem.selectSingleNode("//ITEM[ID="+Number(x+1)+"]/COBERTURAS/COBERTURA[COBERCOD="+coberActual+"]/PRIMA").text = coberRespuestaMQ[y].selectSingleNode("PRIMAIMP").text
				objXMLItem.selectSingleNode("//ITEM[ID="+Number(x+1)+"]/COBERTURAS/COBERTURA[COBERCOD="+coberActual+"]/TASA").text = coberRespuestaMQ[y].selectSingleNode("TARIFPOR").text
				//alert( coberRespuestaMQ[y].selectSingleNode("COBERCOD").text +" ** "+coberRespuestaMQ[y].selectSingleNode("TARIFPOR").text)
				
				
			}
			
	}
	
		document.getElementById("varXmlItems").value = objXMLItem.xml
	
	
}
/******************************************************************************************/
function fncActualizarEstadoSiniestralidad(pXml){
	
	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(pXml);

	if (varXml.selectNodes("ERRORES/SOLAPA[@nro='0']/ERROR").length==1 && varXml.selectNodes("ERRORES/SOLAPA[@nro='2']/ERROR").length==0 && varXml.selectNodes("ERRORES/SOLAPA[@nro='3']/ERROR").length==0){
		flagPoseeSiniestralidad = false
		document.getElementById("xmlFueraNorma").value="<ERRORES></ERRORES>"
		//oXMLFueraNorma.loadXML("<ERRORES></ERRORES>")
		document.getElementById('divTablaFueraNorma').innerHTML=""
		
	}

}

function fncEsICB1() {

    if (document.getElementById("RAMOPCODSQL").value == 'ICB1')
        return true;
    else
        return false;

}

/******************************************************************************************/

function fncQuitarEF(pXml){
	
	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(pXml);
	var objetos = varXml.selectNodes("//FORMADEPAGO")
	
	for (var x= 0 ; x<objetos.length; x++)
	{	
		if(objetos[x].selectSingleNode("COBROTIP").text.toUpperCase() == "EF")
			objetos[x].parentNode.removeChild(objetos[x])
		
	}
	return varXml.xml
}
/******************************************************************************************/

function fncSolo10(pXml){
	
	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(pXml);
	var objetos = varXml.selectNodes("//PLAN")
	
	for (var x= 0 ; x<objetos.length; x++)
	{	
		if(objetos[x].selectSingleNode("PLANPCOD").text.toUpperCase() != "30")
			objetos[x].parentNode.removeChild(objetos[x])
		
	}
	return varXml.xml
}

/******************************************************************************************/

function fncSoloAnual(pXml){

	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(pXml);
	var objetos = varXml.selectNodes("//PERIODO")
	
	for (var x= 0 ; x<objetos.length; x++)
	{	
		if(objetos[x].selectSingleNode("FORMACOBRO").text.toUpperCase() != "5")
			objetos[x].parentNode.removeChild(objetos[x])
		
	}
	return varXml.xml
}

/******************************************************************************************/

function fncSinEst(pXml){

	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(pXml);
	var objetos = varXml.selectNodes("//CLAUSULA")
	
	for (var x= 0 ; x<objetos.length; x++)
	{	
		if(objetos[x].selectSingleNode("CODIGO").text.toUpperCase() != "01")
			objetos[x].parentNode.removeChild(objetos[x])
		
	}
	return varXml.xml
}

/******************************************************************************************/

function fncActividadICOBBSeleccionada(){
    if (document.getElementById("cboActividadICOBB").value != -1) {
        if (document.getElementById("localidadRiesgo").value == "" && document.getElementById("CERTISEC").value == "0") {
            document.getElementById("cboActividadICOBB").value = -1
            alert_customizado("Debe seleccionar previamente ubicaci�n del riesgo");
            return;
        }

        document.getElementById("cboPlanICOBB").length = 0
        mvarRequest = "<Request>" +
				"<DEFINICION>SP_LISTAR_ICOBB_PLANES.xml</DEFINICION>" +
				"<IDACTIVIDAD>" + document.getElementById("cboActividadICOBB").value + "</IDACTIVIDAD>" +
				"</Request>"
        datosCotizacion.cargaComboXSL("cboPlanICOBB", llamadaMensajeMQ(mvarRequest, 'lbaw_OVSQLGen'), document.getElementById("getComboPlanesICOBB").xml)
        if (document.getElementById("cboPlanICOBB").length == 1)
            fncMuestraCoberturasICOBB()

        if (document.getElementById("cboPlanICOBB").value == 0)
            document.getElementById("divSeccionCoberturas").innerHTML = ""
    }
	else{
		document.getElementById("divSeccionCoberturas").innerHTML = ""
		document.getElementById("divSeccionCoberturasICOBB").value = ""
		document.getElementById("cboPlanICOBB").length = 0
	}
}

/******************************************************************************************/

function fncMuestraCoberturasICOBB(){

	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;
	
	var mvarRequest = "<Request>"+
	          "<DEFINICION>SP_LISTAR_ICOBB_COBERTURAS.xml</DEFINICION>"+        
	          "<IDFOLLETO>"+document.getElementById("cboPlanICOBB").value.split("|")[1]+"</IDFOLLETO>" +
	          "<IDPLANES>"+document.getElementById("cboPlanICOBB").value.split("|")[0]+"</IDPLANES>" +
              "</Request>"
	//alert(mvarRequest)
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVSQLGen')

	if(varXml.loadXML(mvarResponse)) {

		//JC ICOBB
		//var CantCober = varXml.selectNodes("//COBERTURAS/COBER").length;
		var Cober = varXml.getElementsByTagName('COBER');
		var sumaAsegurada=0
		for (i=0;i<Cober.length;i++)
 		{
				sumaAsegurada= eval(Cober[i].selectSingleNode("SUMA_ASEGURADA_ICOBB").text) + eval(sumaAsegurada);
		}
		//FIN JC
		
		if(oXSLValores.loadXML(document.getElementById("getseccionCoberturasICOBB").xml))
		{	
			
			var  xslParam=""
						
			var varResultTabla = parseaXSLParam(varXml.xml,oXSLValores,xslParam)	
			
			document.getElementById("xmlCoberturas").innerHTML = varXml.xml
			document.getElementById("divSeccionCoberturas").innerHTML = varResultTabla;		
		}
		//jc icobb
		document.getElementById("SUMAASEGTOTAL").value=sumaAsegurada;
	}	

	document.getElementById("PROVICOD").value=document.getElementById("CBO_PROVINCIA").value
	document.getElementById("DOMICPOB").value=document.getElementById("localidadRiesgo").value
	document.getElementById("CPOSDOM").value=document.getElementById("codPostalRiesgo").value	
	document.getElementById("xmlCoberturasICOBB").value=varXml.xml
	document.getElementById("divSeccionCoberturasICOBB").value=varResultTabla
	document.getElementById("CodigoZonaICOBB").value=document.getElementById("CODIGOZONA").value
	document.getElementById("verZonaICOBB").value=document.getElementById("verZona").innerText
	document.getElementById("SUMAASEGTOTICOBB").value=sumaAsegurada	
	
	document.getElementById("PRIMAIMP").value = document.getElementById("cboPlanICOBB").value.split("|")[2]
	document.getElementById("DEREMIMP").value = document.getElementById("cboPlanICOBB").value.split("|")[5]
	document.getElementById("IMPUEIMP").value = document.getElementById("cboPlanICOBB").value.split("|")[4]
	document.getElementById("RECTOIMP").value = document.getElementById("cboPlanICOBB").value.split("|")[3]	
	document.getElementById("RECADMIN").value = document.getElementById("cboPlanICOBB").value.split("|")[6]
	document.getElementById("PRECIOICOBB").value = document.getElementById("cboPlanICOBB").value.split("|")[3]
	document.getElementById("PORCCOMERICOBB").value = document.getElementById("cboPlanICOBB").value.split("|")[7]
	document.getElementById("COMISORICOBB").value = document.getElementById("cboPlanICOBB").value.split("|")[8]
	document.getElementById("COMISPRICOBB").value = document.getElementById("cboPlanICOBB").value.split("|")[9]	
	
	//se tapan las comisiones del ais con las de la tabla:
	document.getElementById("COMISION_ORG").value=document.getElementById("COMISORICOBB").value
	document.getElementById("COMISION_PROD").value=document.getElementById("COMISPRICOBB").value
	document.getElementById("porcOrganizador").value =document.getElementById("COMISORICOBB").value
	document.getElementById("porcProductor").value =document.getElementById("COMISPRICOBB").value

	//document.body.insertAdjacentHTML("beforeEnd", "Items<textarea>" + document.getElementById("varXmlItems").value + "</textarea>")

}	

function solapa2continuarICB1(){
   
	//lo que falta de solapa 2
	document.getElementById("circuitoRevision").style.display='none';
	aValidarSolapas[0]="S"
	aValidarSolapas[1]="S"
	aValidarSolapas[2]="S"
	aValidarSolapas[3]="N"
	aValidarSolapas[4]="N"
	aValidarSolapas[5]="N"
	aValidarSolapas[6]="N"
	document.getElementById("divBtnNuevaCotizacion").style.display='inline'
	document.getElementById("divBtnAnterior").style.display='inline'   
	document.getElementById("divBtnImprimirCotizacion").style.display='inline'	
	document.getElementById("divBtnEnvioEmail").style.display='inline'
	document.getElementById("divBtnGrabarIrSolicitud").style.display='inline'
	
 	document.getElementById("resultadoCotizacion").innerHTML=""			

	//fncCotizar()	// 2340
	datosCompletos=true
	datosCotizacion.setCotizar(false)	
	resultadoCotizacionICOBB()	


}

function resultadoCotizacionICOBB(pXML)
{ 

	var oXMLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXMLValores.async = false; 
	var oXSLValores = new ActiveXObject('MSXML2.DOMDocument');    
	oXSLValores.async = false;
	var pXMLVal = new ActiveXObject('MSXML2.DOMDocument');    
	pXMLVal.async = false;
	
	if(oXMLValores.loadXML(document.getElementById("varXmlItems").value))
	{				
		if(oXSLValores.loadXML(document.getElementById("resultadoCotizacionICOBBXSL").xml))
		{	
			var xslParam="<parametros>"+
			"<parametro>"+
				"<nombre>moneda</nombre><valor>"+datosCotizacion.getMonedaProducto()+"</valor>"+
			"</parametro>"+
			"<parametro>"+
						"<nombre>SWPRIMIN</nombre>"+
						"<valor>"+ document.getElementById("SWPRIMIN").value+"</valor>"+
					"</parametro>"+					
			"</parametros>"			
			var varResultTabla = parseaXSLParam(oXMLValores.xml,oXSLValores,xslParam)
			document.getElementById("resultadoCotizacion").innerHTML = varResultTabla;	

			document.getElementById("resulCotiTotales").style.display='none';
			document.getElementById("resulCotiTotalesICOBB").style.display='';
					
			//document.getElementById("divCotiPRIMA").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRIMAIMP").text,2,",");
			//document.getElementById("primaMinima").innerHTML = '';
					
			//document.getElementById("divCotiPRECIO").innerText = formatoNro(datosCotizacion.getMonedaProducto(),pXMLVal.selectSingleNode("//Response/CAMPOS/PRECIOTOT").text,2,",");
			document.getElementById("divPrimaICOBB").innerText=formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("PRIMAIMP").value,2,",")
			document.getElementById("divGastosAdminICOBB").innerText=formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("DEREMIMP").value,2,",")
			document.getElementById("divGastosComerICOBB").innerText="% "+document.getElementById("PORCCOMERICOBB").value+"          "+formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("RECADMIN").value,2,",")
			document.getElementById("divImpICOBB").innerText=formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("IMPUEIMP").value,2,",")
			document.getElementById("divPrecioICOBB").innerText=formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("PRECIOICOBB").value,2,",")
			
		}
				
	}	
	
}	

function fncObtenerItemsICB1(){
	
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	var objXMLNode = varXml.getElementsByTagName('ITEM');	
	
	document.getElementById("CBO_PROVINCIA").value=document.getElementById("PROVICOD").value
	document.getElementById("localidadRiesgo").value=document.getElementById("DOMICPOB").value
	document.getElementById("codPostalRiesgo").value=document.getElementById("CPOSDOM").value
	document.getElementById("xmlCoberturas").innerHTML =document.getElementById("xmlCoberturasICOBB").value
	document.getElementById("divSeccionCoberturas").innerHTML=document.getElementById("divSeccionCoberturasICOBB").value
	document.getElementById("CODIGOZONA").value=document.getElementById("CodigoZonaICOBB").value
	document.getElementById("verZona").innerText=document.getElementById("verZonaICOBB").value
	if (document.getElementById("CERTISEC").value!="0")
		document.getElementById("SUMAASEGTOTAL").value=document.getElementById("SUMAASEGTOTICOBB").value
		
	if (document.getElementById("CBO_PROVINCIA").value!="-1" && document.getElementById("divSeccionCoberturas").innerHTML!="")
		document.getElementById("SUMAASEGTOTAL").value=document.getElementById("SUMAASEGTOTICOBB").value	

	if(document.getElementById("CBO_PROVINCIA").value=="1"){
		document.getElementById("codPostalRiesgo_etiq").style.display='none'
		document.getElementById("codPostalRiesgo").style.display='none'		
	}
}

function fncRecuperaPlanICOBB(pValor){

	for(var x=0;x<document.getElementById("cboPlanICOBB").length;x++)
		if (document.getElementById("cboPlanICOBB").options[x].value.split("|")[0]==pValor)	
		{
			document.getElementById("cboPlanICOBB").selectedIndex= x	
			return
		}
}	

function fncVerificarActividadComercial(pValor)
{
	switch(pValor)
	{	
	case '050019':
		return false;			
	break;
	
	case '050022':
		return false;	
	break;
	
	case '050040':
		return false;
	break;
	
	case '050121':
		return false;
	break;
	
	case '050029':
		return false;
	break;
	
	case '050129':
		return false;
	break;
	
	case '050030':
		return false;
	break;
	
	case '050041':
		return false;
	break;
	
	case '050139':
		return false;
	break;
	
	case '050053':
		return false;
	break;
	
	case '050060':
		return false;
	break;
	
	case '050074':
		return false;
	break;
	
	case '050080':
		return false;
	break;
	
	case '050128':
		return false;
	break;
	
	case '050093':
		return false;
	break;
	
	case '050107':
		return false;
	break;
	
	case '050095':
		return false;
	break;
	
	case '050100':
		return false;
	break;
	
	case '050111':
		return false;
	break;
	
	case '050113':
		return false;
	break;
	}
	return true;
}