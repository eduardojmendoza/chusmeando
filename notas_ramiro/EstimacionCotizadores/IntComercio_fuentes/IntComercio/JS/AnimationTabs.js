/*
--------------------------------------------------------------------------------
 Fecha de Modificación: 12/04/2011
 PPCR: 50055/6010661
 Desarrolador: Gabriel D'Agnone
 Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------

 COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
 ALL RIGHTS RESERVED
 
 This software is only to be used for the purpose for which it has been provided.
 No part of it is to be reproduced, disassembled, transmitted, stored in a 
 retrieval system or translated in any human or computer language in any way or
 for any other purposes whatsoever without the prior written consent of the Hong
 Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
 serious civil and criminal offence, which can result in heavy fines and payment 
 of substantial damages.

Nombre del JavaScript: AnimationTabs.js

Fecha de Creación: 15/07/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D Agnone

Descripción: Desarrollo de funciones genéricas para la implementacion de solapas
	     para los formularios de la aplicación.

--------------------------------------------------------------------------------
*/

/*
	
	Este archivo contiene todas las funciones concernientes a las solapas

	function objTab(pId, pDependency);		
	function mostrarTab(divName, IsInner, TabColor, TabColorDependency);
	function verSolapa(varPos);
	
*/

/* ************************************************************************* */
if (typeof aScreenTabs == "undefined") var aScreenTabs = new Array();
if (typeof aScreenTabsInt == "undefined") var aScreenTabsInt = new Array();
var tabActual = 0;
var tabAnterior=0;
var tabClick=0;

/* ************************************************************************* */	
function objTab(pId, pDependency)
{
	this.id = pId;
	this.dependency = pDependency;
}
/* ************************************************************************* */
function mostrarTab(divName, IsInner, TabColor, TabColorDependency)
{ 	
	 var formValido=true
		for (key in aScreenTabs)   
		{
			if (aScreenTabs[key]==divName)
				tabClick = key;
		}
		
	
	if(tabClick > tabActual )  formValido=validarDatosFormulario("true")?true:false
	
	if(tabClick == tabActual )  formValido=true
	
	
	//alert("flag:"+flagPuedeCambiarSolapa  + " formValido:"+formValido)
	if(formValido && flagPuedeCambiarSolapa){
	if (IsInner == 1)
	{		//alert("inner")
		//debug
		for (key in aScreenTabsInt)   
		{
			document.all[aScreenTabsInt[key].id].style.display='none';
			document.all['TD'+ String(aScreenTabsInt[key].id).substr(3)].bgColor ="#DEDFDE"
		}
		
		document.all[divName].style.display='';
		var oActualTab = 'TD' + divName.substr(3)
		//document.all[oActualTab].bgColor="#FF0000";
		document.all[oActualTab].bgColor=TabColor;
		//mostrarTab(divNameContainer,'');
	}
	else
	{	
		var i = 0;
		var EsPrimero = true;
		
		for (i==0 ; i < aScreenTabsInt.length ; i++)
		{

			if (EsPrimero && aScreenTabsInt[i].dependency == divName)
			{
				//document.all[aScreenTabsInt[i].id].style.display='';
				mostrarTab(aScreenTabsInt[i].id, 1, TabColorDependency,'')
				EsPrimero = false
			}
			else if (aScreenTabsInt[i].dependency == divName)
			{
				document.all[aScreenTabsInt[i].id].style.display='none' ;

			}

		}	

	
		var varImgLeftOff = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_mi_of.jpg";	
		document.getElementById('L0').src = varImgLeftOff;			
		for (key in aScreenTabs)   
		{
			
			document.all[aScreenTabs[key]].style.display='none';
			var varImgCenterOff = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_center_of.jpg";
			document.getElementById('TD'+((key*1) + 1)).style.backgroundImage = 'url(' + varImgCenterOff + ')';
			document.getElementById('TD'+((key*1) + 1)).style.backgroundRepeat;
			var varImgMiddleOff = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_mi_of.jpg";
			document.getElementById('R'+((key*1) + 1)).src = varImgMiddleOff;
		}

		//Agregado para la funcionalidad del navegador (Muestra / Oculta el boton de navegaci&oacuten entre solapas)
		var varPos=0;
		for (key in aScreenTabs)   
		{
			if (aScreenTabs[key]==divName)
				varPos = key;
		}
		tabAnterior = tabActual
		tabActual = varPos;
		
		document.all[divName].style.display='';  
		//var oActualTab ='TD' + divName.substr(3)
		
		var varImgLeftOn = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_mi_lo.jpg";	
		var varImgCenterOn = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_center_on.jpg";
		var varImgMiddleOn = document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value + "/Images/solapas_mi_ro.jpg";
		
		if (varPos==0) 
		{
			document.getElementById('L' +((varPos*1))).src = varImgLeftOn;		
		} else {
			document.getElementById('R' +((varPos*1))).src = varImgLeftOn;		
		}
		document.getElementById('TD'+((varPos*1) + 1)).style.backgroundImage = 'url(' + varImgCenterOn + ')';
		document.getElementById('TD'+((varPos*1) + 1)).style.backgroundRepeat;
		document.getElementById('R'+((varPos*1) + 1)).src = varImgMiddleOn;			
	}
	
		fncSiCambiaSolapa()
 
	
}

//window.status=" pos:"+ varPos +" activa: "+ tabActual +" click: "+tabClick
}
/* ************************************************************************* */
function verSolapa(varPos) {
	var varAux = (tabActual * 1) + varPos;
	if (varAux < 0) 
		varAux = 0;
	if (varAux >= aScreenTabs.length) 
		varAux = aScreenTabs.length - 1;
	
	   if(document.getElementById("TD"+eval(varAux+1)).style.display!="none")				
		mostrarTab(aScreenTabs[varAux],'','#9CBACE','');
	
}
/* ************************************************************************* */
function irASolapa(varPos) {
	var varAux =  varPos;
	if (varAux < 0) 
		varAux = 0;
	if (varAux >= aScreenTabs.length) 
		varAux = aScreenTabs.length - 1;
		
	
		mostrarTab(aScreenTabs[varAux-1],'','#9CBACE','');
	
	//window.scrollTo(0);
}
function showAlt(txt) {
	
		var varDesplazamiento = document.body.scrollTop;
		//document.getElementById('divAlt').style.width = "120px"
		document.getElementById('divAlt').style.pixelLeft = event.clientX - 10; 
		document.getElementById('divAlt').style.pixelTop  = event.clientY + 10 + varDesplazamiento;	
		document.getElementById('divAlt').style.display = 'inline';
		document.getElementById('divAlt').innerHTML = txt;
		if(document.getElementById('divAlt').style.width < 120 ) 
			document.getElementById('divAlt').style.width  = '120px'
	
		
}
/* ******************************************************************** */
function hideAlt() {
		document.getElementById('divAlt').style.display = 'none';
}


function muestraEtiquetaSolapa(pNroSolapa)

{
	
  document.getElementById("TD"+pNroSolapa).style.display= "inline"	   
  document.getElementById("DTD"+pNroSolapa ).style.display= "inline"	   
  	
}

function ocultaEtiquetaSolapa(pNroSolapa)

{
	
  document.getElementById("TD"+pNroSolapa).style.display= "none"	   
  document.getElementById("DTD"+pNroSolapa ).style.display= "none"	   
  	
}
