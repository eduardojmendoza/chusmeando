/*
--------------------------------------------------------------------------------
 Fecha de Modificación: 12/04/2011
 PPCR: 50055/6010661
 Desarrolador: Gabriel D'Agnone
 Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------

 COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
 ALL RIGHTS RESERVED
 
 This software is only to be used for the purpose for which it has been provided.
 No part of it is to be reproduced, disassembled, transmitted, stored in a 
 retrieval system or translated in any human or computer language in any way or
 for any other purposes whatsoever without the prior written consent of the Hong
 Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
 serious civil and criminal offence, which can result in heavy fines and payment 
 of substantial damages.

Nombre del JavaScript: VariablesGlobales.js

Fecha de Creación: 15/07/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripción: Este archivo contiene todas las variables de javascript globales
	     para el tratamiento de errores y validaciones.

--------------------------------------------------------------------------------
*/

/* ****************************************************** */
/*                     Variables globales 
/* ****************************************************** */
//Variables globales para tratamiento de errores
var leyendaError;
var oXMLErrores = new ActiveXObject("MSXML2.DomDocument");
var oXMLCambios = new ActiveXObject("MSXML2.DomDocument");
    oXMLCambios.async = false;	
    oXMLCambios.loadXML("<CAMBIOS/>") 
   
  var varXmlCambios = new ActiveXObject('MSXML2.DOMDocument');
			varXmlCambios.async = false;
			varXmlCambios.loadXML("<CAMBIOS/>") 
    
var oXMLErroresAux = new ActiveXObject("MSXML2.DomDocument");
var validarObligatoriosEstrictos; //Variable utilizada por las validaciones personalizadas para determinar su comportamiento.
var mostrarAdvertenciaDatosAsegurado = true; //Variable utilizada para mostrar la advertencia una unica vez en la denuncia
var mostrarAdvertenciaDatosVehiculoAsegurado = true; //Variable utilizada para mostrar la advertencia una unica vez en la denuncia

//Variables globales (otras)
var xmlHTTP;
var xmlDOM;