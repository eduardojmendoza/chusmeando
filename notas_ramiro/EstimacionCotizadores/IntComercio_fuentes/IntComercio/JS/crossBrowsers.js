/*
--------------------------------------------------------------------------------

 COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011. 
 ALL RIGHTS RESERVED
 
 This software is only to be used for the purpose for which it has been provided.
 No part of it is to be reproduced, disassembled, transmitted, stored in a 
 retrieval system or translated in any human or computer language in any way or
 for any other purposes whatsoever without the prior written consent of the Hong
 Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
 serious civil and criminal offence, which can result in heavy fines and payment 
 of substantial damages.

Nombre del JavaScript: crossBrowsers.js

Fecha de Creaci�n: 12/04/2011

PPcR: 50055/6010661 

Desarrollador: Gabriel D'Agnone

Descripci�n: Este archivo contiene todas las funciones para  Firefox.

--------------------------------------------------------------------------------
*/


var client = function(){

    //rendering engines
    var engine = {
        ie: 0,
        gecko: 0,
        webkit: 0,
        khtml: 0,
        opera: 0,

        //complete version
        ver: null
    };

    //browsers
    var browser = {

        //browsers
        ie: 0,
        firefox: 0,
        safari: 0,
        konq: 0,
        opera: 0,
        chrome: 0,
       

        //specific version
        ver: null
    };

    //platform/device/OS
    var system = {
        win: false,
        mac: false,
        x11: false,

        //mobile devices
        iphone: false,
        ipod: false,
        nokiaN: false,
        winMobile: false,
        macMobile: false,

        //game systems
        wii: false,
        ps: false
    };

    //detect rendering engines/browsers
    var ua = navigator.userAgent;
    if (window.opera){
        engine.ver = browser.ver = window.opera.version();
        engine.opera = browser.opera = parseFloat(engine.ver);
    } else if (/AppleWebKit\/(\S+)/.test(ua)){
        engine.ver = RegExp["$1"];
        engine.webkit = parseFloat(engine.ver);

        //figure out if it's Chrome or Safari
        if (/Chrome\/(\S+)/.test(ua)){
            browser.ver = RegExp["$1"];
            browser.chrome = parseFloat(browser.ver);
        } else if (/Version\/(\S+)/.test(ua)){
            browser.ver = RegExp["$1"];
            browser.safari = parseFloat(browser.ver);
        } else {
            //approximate version
            var safariVersion = 1;
            if (engine.webkit  < 100){
                safariVersion = 1;
            } else if (engine.webkit < 312){
                safariVersion = 1.2;
            } else if (engine.webkit < 412){
                safariVersion = 1.3;
            } else {
                safariVersion = 2;
            }

            browser.safari = browser.ver = safariVersion;
        }
    } else if (/KHTML\/(\S+)/.test(ua) || /Konqueror\/([^;]+)/.test(ua)){
        engine.ver = browser.ver = RegExp["$1"];
        engine.khtml = browser.konq = parseFloat(engine.ver);
    } else if (/rv:([^\)]+)\) Gecko\/\d{8}/.test(ua)){

        engine.ver = RegExp["$1"];
        engine.gecko = parseFloat(engine.ver);

        //determine if it's
        if (/Firefox\/(\S+)/.test(ua)){
            browser.ver = RegExp["$1"];
            browser.firefox = parseFloat(browser.ver);
        }
    } else if (/MSIE ([^;]+)/.test(ua)){
        engine.ver = browser.ver = RegExp["$1"];
        engine.ie = browser.ie = parseFloat(engine.ver);
    }

    //detect browsers
    browser.ie = engine.ie;
    browser.opera = engine.opera;

    //detect platform
    var p = navigator.platform;
    system.win = p.indexOf("Win") == 0;
    system.mac = p.indexOf("Mac") == 0;
    system.x11 = (p == "X11") || (p.indexOf("Linux") == 0);

    //detect windows operating systems
    if (system.win){
        if (/Win(?:dows )?([^do]{2})\s?(\d+\.\d+)?/.test(ua)){
            if (RegExp["$1"] == "NT"){
                switch(RegExp["$2"]){
                    case "5.0":
                        system.win = "2000";
                        break;
                    case "5.1":
                        system.win = "XP";
                        break;
                    case "6.0":
                        system.win = "Vista";
                        break;
                    default:
                        system.win = "NT";
                        break;
                }
            } else if (RegExp["$1"] == "9x"){
                 system.win = "ME";
            } else {
                 system.win = RegExp["$1"];
            }
        }
    }

    //mobile devices
    system.iphone = ua.indexOf("iPhone") > -1;
    system.ipod = ua.indexOf("iPod") > -1;
    system.nokiaN = ua.indexOf("NokiaN") > -1;
    system.winMobile = (system.win == "CE");
    system.macMobile = (system.iphone || system.ipod);

    //gaming systems
    system.wii = ua.indexOf("Wii") > -1;
    system.ps = /playstation/i.test(ua);

    //return it
    return {
        engine:     engine,
        browser:    browser,
        system:     system
    };

}();

/***********************************************************************/

function detectBrowser()
{
	/*
	alert( navigator.userAgent)
	alert(client.browser.ie)
	alert(client.browser.chrome)
	alert(client.browser.firefox)
	alert(client.browser.opera)
	*/
	 var navegador= ""
	 
	if ( client.browser.ie > 0 )
	{
		navegador= "Internet Explorer - ver.: " + client.browser.ie
		
	}
	else if ( client.browser.firefox > 0 )
	{
		navegador= "Firefox - ver.: " + client.browser.firefox
		
	}
	else if ( client.browser.chrome > 0 )
	{
		navegador= "Google Chrome - ver.: " + client.browser.chrome
		
	}
	else if ( client.browser.opera > 0 )
	{
		navegador= "Opera - ver.: " + client.browser.opera
		
	}
	else if ( client.browser.safari > 0 )
	{
		navegador= "Safari - ver.: " + client.browser.safari
		
	}
	else if ( client.browser.konq > 0 )
	{
		navegador= "Konqueror - ver.: " + client.browser.konq
		
	}
	
	
	
else
	navegador= "Navegador no identificado " 
	
	return navegador

}

/***********************************************************************/
// SELECT NODES SELECT SINGLE NODE

if( document.implementation.hasFeature("XPath", "3.0") )

{
	if( typeof XMLDocument == "undefined" )
		{ XMLDocument = Document; }
		
/************* SELECT NODES ***************************/
	
  	XMLDocument.prototype.selectNodes = function(cXPathString, xNode)
  		{
    		if( !xNode ) 
    			{ xNode = this; } 
		var oNSResolver = this.createNSResolver(this.documentElement)
		var aItems = this.evaluate(cXPathString, xNode, oNSResolver, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null)
		var aResult = [];
		for( var i = 0; i < aItems.snapshotLength; i++)
			{aResult[i] =  aItems.snapshotItem(i);	}
		return aResult;
	}
	
/*************** SELECT SINGLE NODE ********************************/
	
	XMLDocument.prototype.selectSingleNode = function(cXPathString, xNode)
	{
		if( !xNode ) { xNode = this; } 
		var xItems = this.selectNodes(cXPathString, xNode);
		if( xItems.length > 0 ){return xItems[0];	}
		else{return null;	}
	}
	
	
	
	Element.prototype.selectNodes = function(cXPathString){
		if(this.ownerDocument.selectNodes){	return this.ownerDocument.selectNodes(cXPathString, this);}
		else{throw "For XML Elements Only";}
	}
	Element.prototype.selectSingleNode = function(cXPathString){	
		if(this.ownerDocument.selectSingleNode){return this.ownerDocument.selectSingleNode(cXPathString, this);	}
		else{throw "For XML Elements Only";}
	}
	
	XMLDocument.prototype.__defineGetter__('xml', function() 
			{	
				 var serializer = new XMLSerializer();  
    			return serializer.serializeToString(this)
				//return XMLSerializer().serializeToString(this);
			} );
	
	if(typeof HTMLElement.prototype.__defineGetter__ != 'undefined'){
		
		Element.prototype.__defineGetter__('xml', function() 
			{	 var serializer = new XMLSerializer();  
    				return serializer.serializeToString(this)
				//return XMLSerializer().serializeToString(this);
			} );
		Element.prototype.__defineGetter__('text', function() 
			{
				return this.textContent;
				} );
		Element.prototype.__defineSetter__('text', function(s) 
			{this.textContent=s;} );
		//Element.prototype.__defineSetter__('xml', function(s) {} );
	
		Attr.prototype.__defineSetter__('text', function(s) 
			{this.textContent=s;} );
	
	
	}

/***************** LOADXML *********************************/
	
	XMLDocument.prototype.loadXML = function(xmlString)
	{ 
	var childNodes = this.childNodes;
	for (var i = childNodes.length - 1; i >= 0; i--)
	this.removeChild(childNodes[i]);
	
	var dp = new DOMParser();
	 try 
	 {
		var newDOM = dp.parseFromString(xmlString, "text/xml");
	  } 
	catch (e) 
	{                      
         return false;
         };
	
	var newElt = this.importNode(newDOM.documentElement, true);
	this.appendChild(newElt);
	
	return true
	};

/* Document.prototype.loadXML = function(strXML) {
        
    //create a DOMParser
    var objDOMParser = new DOMParser();
     
    //create new document from string
    var objDoc = objDOMParser.parseFromString(strXML, "text/xml");
	
  	return objDoc;
 
  } //End: function
  */
	Document.prototype.loadXML = function(str) 
	{	
		var domParser = new DOMParser();
		 try {
			var domObj = domParser.parseFromString(str, "text/xml");
		      } 
		      catch (e) 
		      {                      
                    return false;
                	};

		while (this.hasChildNodes())
				this.removeChild(this.lastChild);
		for (var i = 0; i < domObj.childNodes.length; i++) 
		{
			var importedNode = this.importNode(domObj.childNodes[i], true);
			this.appendChild(importedNode);
		}
	return true
	
	}


}


//***********************************************************************
// FUNCIONES CREACION DE XMLDOM MULTIBROWSERS
//***********************************************************************

function newXMLDOM()
{
if (window.ActiveXObject) 	
	 {
	    	var xmldom = new ActiveXObject('MSXML2.DOMDocument');  
	    	xmldom.setProperty("SelectionLanguage","XPath");      
		xmldom.async = false;  
		return xmldom
	}
	else
	{			
		var xmldom = document.implementation.createDocument("", "", null);  
		return xmldom		
	}		
	
	
}

function loadXMLDoc(dname)
{
	if (window.XMLHttpRequest)
  	{
  		xhttp=new XMLHttpRequest();
  	}
	else
  	{
  		xhttp=new ActiveXObject("Microsoft.XMLHTTP");
  	}
	xhttp.open("GET",dname,false);
	xhttp.send("");
	return xhttp.responseXML;
}


if (!window.ActiveXObject) 	
{
	Node.prototype.transformNode = function (oXslDom) 
	{	
	    	var oProcessor = new XSLTProcessor();
	    	oProcessor.importStylesheet(oXslDom);    
	    	var oResultDom = oProcessor.transformToDocument(this);
	    	var serializer = new XMLSerializer();  
	    	return serializer.serializeToString(oResultDom)	    
	}

	
	String.prototype.trim = function () 
	{
		return this.replace(/(^\s+)|(\s+$)/, '');
	}
	String.prototype.addTrailingSlash = function () 
	{
		return ('/' != this.charAt(this.length - 1)) ? this.toString() + '/' : this.toString();
	}
	String.prototype.left = function (n) 
	{
		if (n <= 0) return "";
		else if (n > String(this).length) return this;
		else return String(this).substring(0,n);
	}
	String.prototype.right = function (n)
	{
	    if (n <= 0) return "";
	    else if (n > String(this).length) return str;
	    else {
	       var iLen = String(this).length;
	       return String(this).substring(iLen, iLen - n);
	    }
	}


}








/*****************************************************/
// FUNCIONES PARA EMULACION IE
/******************************************************/

var ie = document.all != null;
var moz = !ie && document.getElementById != null && document.layers == null;



if (moz) {	// set up ie environment for Moz

	extendEventObject();
	emulateAttachEvent();	
	emulateEventHandlers(["click", "dblclick", "mouseover", "mouseout", "mousedown", "mouseup", "mousemove" ,"keydown", "keypress", "keyup" ]);
	
	//,"keydown", "keypress", "keyup"
	emulateAllModel();
	emulateHTMLModel();	
	extendElementModel()
	emulateCurrentStyle(["left", "right", "top", "bottom", "width", "height"]);

	// Mozilla returns the wrong button number
	Event.LEFT = 1;
	Event.MIDDLE = 2;
	Event.RIGHT = 3;
	

	
}
else {
	Event = {};
	// IE is returning wrong button number as well :-)
	Event.LEFT = 1;
	Event.MIDDLE = 4;
	Event.RIGHT = 2;
}




/*
 * Extends the event object with srcElement, cancelBubble, returnValue,
 * fromElement and toElement
 */
function extendEventObject() {
	Event.prototype.__defineSetter__("returnValue", function (b) {
		if (!b) this.preventDefault();
	});
	
	Event.prototype.__defineSetter__("cancelBubble", function (b) {
		if (b) this.stopPropagation();
	});
	
	Event.prototype.__defineGetter__("srcElement", function () {
		var node = this.target;
		//while (node.nodeType != 1) node = node.parentNode;
		return node;
	});

	Event.prototype.__defineGetter__("fromElement", function () {
		var node;
		if (this.type == "mouseover")
			node = this.relatedTarget;
		else if (this.type == "mouseout")
			node = this.target;
		if (!node) return;
		while (node.nodeType != 1) node = node.parentNode;
		return node;
	});

	Event.prototype.__defineGetter__("toElement", function () {
		var node;
		if (this.type == "mouseout")
			node = this.relatedTarget;
		else if (this.type == "mouseover")
			node = this.target;
		if (!node) return;
		while (node.nodeType != 1) node = node.parentNode;
		return node;
	});
	
	Event.prototype.__defineGetter__("offsetX", function () {
		return this.layerX;
	});
	Event.prototype.__defineGetter__("offsetY", function () {
		return this.layerY;
	});
	
	 Event.prototype.__defineGetter__
        (
                "x",
                function()
                {
                        return this.pageX
                }
        );
        Event.prototype.__defineGetter__
        (
                "y",
                function()
                {
                        return this.pageY
                }
        );

Event.prototype.__defineSetter__
        (
                "keyCode",
                function(value)
                {
                        this.which = value;
                        return value
                }
        );
	
	 Event.prototype.__defineGetter__
        (
                "keyCode",
                function()
                {		
                        return this.which
                }
        );


}

/*
 * Emulates element.attachEvent as well as detachEvent
 */
function emulateAttachEvent() {
	window.attachEvent =
	HTMLDocument.prototype.attachEvent = 
	HTMLElement.prototype.attachEvent = function (sType, fHandler) {
		var shortTypeName = sType.replace(/on/, "");
		fHandler._ieEmuEventHandler = function (e) {
			window.event = e;
			return fHandler();
		};
		this.addEventListener(shortTypeName, fHandler._ieEmuEventHandler, false);
	};

	window.detachEvent =
	HTMLDocument.prototype.detachEvent = 
	HTMLElement.prototype.detachEvent = function (sType, fHandler) {
		var shortTypeName = sType.replace(/on/, "");
		if (typeof fHandler._ieEmuEventHandler == "function")
			this.removeEventListener(shortTypeName, fHandler._ieEmuEventHandler, false);
		else
			this.removeEventListener(shortTypeName, fHandler, true);
	};
}



/*
 * This function binds the event object passed along in an
 * event to window.event
 */
function emulateEventHandlers(eventNames) {
	for (var i = 0; i < eventNames.length; i++) {	
		document.addEventListener(eventNames[i], function (e) {
			window.event = e;
		}, true);	// using capture
	}
}

/*
 * Simple emulation of document.all
 * this one is far from complete. Be cautious
 */
 
function emulateAllModel() {
	var allGetter = function () {
		var a = this.getElementsByTagName("*");
		var node = this;
		a.tags = function (sTagName) {
			return node.getElementsByTagName(sTagName);
		};
		return a;
	};
	HTMLDocument.prototype.__defineGetter__("all", allGetter);
	HTMLElement.prototype.__defineGetter__("all", allGetter);
}

function extendElementModel() {
	HTMLElement.prototype.__defineGetter__("parentElement", function () {
		if (this.parentNode == this.ownerDocument) return null;
		return this.parentNode;
	});
	
	HTMLElement.prototype.__defineGetter__("children", function () {
		var tmp = [];
		var j = 0;
		var n;
		for (var i = 0; i < this.childNodes.length; i++) {
			n = this.childNodes[i];
			if (n.nodeType == 1) {
				tmp[j++] = n;
				if (n.name) {	// named children
					if (!tmp[n.name])
						tmp[n.name] = [];
					tmp[n.name][tmp[n.name].length] = n;
				}
				if (n.id)		// child with id
					tmp[n.id] = n
			}
		}
		return tmp;
	});
	
	HTMLElement.prototype.contains = function (oEl) {
		if (oEl == this) return true;
		if (oEl == null) return false;
		return this.contains(oEl.parentNode);		
	};
}

/*

document.defaultView.getComputedStyle(el1,<BR>null).getPropertyValue('top');

*/
function emulateCurrentStyle(properties) {
	HTMLElement.prototype.__defineGetter__("currentStyle", function () {
		var cs = {};
		var el = this;
		for (var i = 0; i < properties.length; i++) {
			//cs.__defineGetter__(properties[i], function () {
			//	window.status = "i: " + i	;
			//	return document.defaultView.getComputedStyle(el, null).getPropertyValue(properties[i]);
			//});
			cs.__defineGetter__(properties[i], encapsulateObjects(el, properties[i]));
		}
		return cs;
	});
}
// used internally for emualteCurrentStyle
function encapsulateObjects(el, sProperty) {
	return function () {
		return document.defaultView.getComputedStyle(el, null).getPropertyValue(sProperty);
	};
}

function emulateHTMLModel() {

	// This function is used to generate a html string for the text properties/methods
	// It replaces '\n' with "<BR"> as well as fixes consecutive white spaces
	// It also repalaces some special characters	
	function convertTextToHTML(s) {
		s = s.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\n/g, "<BR>");
		while (/\s\s/.test(s))
			s = s.replace(/\s\s/, "&nbsp; ");
		return s.replace(/\s/g, " ");
	}

	HTMLElement.prototype.insertAdjacentHTML = function (sWhere, sHTML) {
		var df;	// : DocumentFragment
		var r = this.ownerDocument.createRange();
		
		switch (String(sWhere).toLowerCase()) {
			case "beforebegin":
				r.setStartBefore(this);
				df = r.createContextualFragment(sHTML);
				this.parentNode.insertBefore(df, this);
				break;
				
			case "afterbegin":
				r.selectNodeContents(this);
				r.collapse(true);
				df = r.createContextualFragment(sHTML);
				this.insertBefore(df, this.firstChild);
				break;
				
			case "beforeend":
				r.selectNodeContents(this);
				r.collapse(false);
				df = r.createContextualFragment(sHTML);
				this.appendChild(df);
				break;
				
			case "afterend":
				r.setStartAfter(this);
				df = r.createContextualFragment(sHTML);
				this.parentNode.insertBefore(df, this.nextSibling);
				break;
		}	
	};

HTMLElement.prototype.__defineGetter__("canHaveChildren", function () {
		switch (this.tagName) {
			case "AREA":
			case "BASE":
			case "BASEFONT":
			case "COL":
			case "FRAME":
			case "HR":
			case "IMG":
			case "BR":
			case "INPUT":
			case "ISINDEX":
			case "LINK":
			case "META":
			case "PARAM":
				return false;
		}
		return true;
	});
	
	
//if (document.body.__defineGetter__)
 //{        
 	if (HTMLElement) 
 	{            
 		var element = HTMLElement.prototype;               
 		if (element.__defineGetter__)  
 		{                
 			element.__defineGetter__("outerHTML",  function () 
	 		{                                 
	 			var parent = this.parentNode;                                   
	 			var el = document.createElement(parent.tagName);                                   
	 			el.appendChild(this);                                   
	 			var shtml = el.innerHTML;                                   
	 			parent.appendChild(this);                                  
	  			return shtml;                           
	   		} );               
   		}        
   	} 
 //}

 if (element.__defineSetter__ && DOMParser) {
    element.__defineSetter__("outerHTML",
        function (v) {
            var e = document.createElement("div");
            var root = null;
            e.innerHTML = v;
            for(var i=0; i<e.childNodes.length; i++) {
                if(e.childNodes[i].nodeType == 1) {
                    root = e.childNodes[i];
                    break;
                }
            }
            if(root)
                this.parentNode.replaceChild(root, this);
        }
    );
}




	

	HTMLElement.prototype.__defineSetter__("innerText", function (sText) {
		this.innerHTML = convertTextToHTML(sText);
		return sText;		
	});

	var tmpGet;
	HTMLElement.prototype.__defineGetter__("innerText", tmpGet = function () {
		var r = this.ownerDocument.createRange();
		r.selectNodeContents(this);
		return r.toString();
	});

	HTMLElement.prototype.__defineSetter__("outerText", function (sText) {
		this.outerHTML = convertTextToHTML(sText);
		return sText;
	});
	HTMLElement.prototype.__defineGetter__("outerText", tmpGet= function()
	{
		var r = this.ownerDocument.createRange();
		r.selectNodeContents(this);
		return r.toString();
	});
	

	HTMLElement.prototype.insertAdjacentText = function (sWhere, sText) {
		this.insertAdjacentHTML(sWhere, convertTextToHTML(sText));
	};

}

//Fix para getElementsByName() de IE que no devuelve coleccion para las TR y TD's
//Its supported by both Internet Explorer and Firefox since it is part of the DOM Level 1 specification. 

//Unfortunately theres a problem. 
//From the Html 4.01 Spec, only the A, APPLET, BUTTON, FORM, FRAME, IFRAME, IMG, INPUT, OBJECT, MAP, META, PARAM, TEXTAREA and SELECT 
//are supported which means that you can't just name all your divs, 
//spans or rows with the same name and use getElementsByName() to return an array of items. 

function getElementsByName_iefix(tag, name) 
{          
	var elem = document.getElementsByTagName(tag);     
	var arr = new Array();     
	for(i = 0,iarr = 0; i < elem.length; i++) 
	{         
		att = elem[i].getAttribute("name");         
	 	if(att == name) 
	 	{               
	 		arr[iarr] = elem[i];              
	 	 	iarr++;          
	 	 }     
	 }    
 	 
 	 return arr;
}


