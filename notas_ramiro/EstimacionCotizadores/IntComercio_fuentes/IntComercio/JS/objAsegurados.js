/*
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 21/12/2011
PPCR: 50055/6010661
Desarrolador: Leonardo Ruiz
Descripci�n: Soporte de HTTPS.-
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 12/04/2011
 PPCR: 50055/6010661
 Desarrolador: Gabriel D'Agnone
 Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 05/08/2010
 PPCR: 50055/6010661
 Desarrolador: Hern�n R. Fus�
 Descripci�n: Para objetos asegurados cicla por 50 items (para renovaci�n)
--------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: objAsegurados.js

Fecha de Creaci�n: 12/07/2010

PPcR: 50055/6010661

Desarrollador: Hern�n R. Fus�

Descripci�n: Funciones para manejar objetos asegurados
--------------------------------------------------------------------------------
*/

/******************************************************************************************/
function fncCargarRiesgoSolicitud()
{
	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);

	var varObjAseg;
	varObjAseg = new ActiveXObject('MSXML2.DOMDocument');
	varObjAseg.async = false;
	varObjAseg.loadXML(document.getElementById("varXmlObjAseg").value);

	var objXMLCobertura = varXml.getElementsByTagName('COBERTURA');
	var objXMLNode = varXml.getElementsByTagName('ITEM');
	var objXMLObjAseg = varObjAseg.getElementsByTagName('OBJETO');	

	var varTabla="";
	var cont="";
	var datosVivienda	
	var domicilioRiesgo
	var objetosRiesgo
	var objAsegurado=""
	
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "<br>SOLIRIESGO <textarea>"+document.getElementById("SOLIRIESGO").value+"</textarea>");	
	
	document.getElementById("riesgosSolicitudMolde").innerHTML=document.getElementById("SOLIRIESGO").value

	var CantItem = varXml.selectNodes("//ITEM").length;	
	
	for (var i=0;i<objXMLNode.length;i++)
 	{ 		
 		var mProv= objXMLNode[i].selectSingleNode("PROVICODDES").text
 		var mProvCod= objXMLNode[i].selectSingleNode("PROVICOD").text
 		var mLoc= objXMLNode[i].selectSingleNode("DOMICPOB").text
 		var mCodPos= objXMLNode[i].selectSingleNode("CPOSDOM").text
 		var mCalle= objXMLNode[i].selectSingleNode("DOMICDOM").text
 		var mNumPue= objXMLNode[i].selectSingleNode("DOMICDNU").text
 		var mPiso= objXMLNode[i].selectSingleNode("DOMICPIS").text
 		var mDepto= objXMLNode[i].selectSingleNode("DOMICPTA").text
 		 		 		
 		domicilioRiesgo=document.getElementById("domicilioRiesgoNro##").outerHTML
		var re = /##/gi; 		
		domicilioRiesgo = domicilioRiesgo.replace(re, "_" + String(i + 1));
		
		re = /DViv/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, String(i + 1));
		
		re = /Nro_#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, "Nro_"+ String(i + 1));
		
		re = /#IngresoCERTISEC#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + fncSetLength(objXMLNode[i].selectSingleNode("CERTISECANT").text,6) + '"');

		re = /#IngresoProvincia#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mProv + '"');
		
		re = /#IngresoProvinCod#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mProvCod + '"');
		
		re = /#IngresoLocalidad#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mLoc + '"');
		
		re = /#IngresoCodPos#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mCodPos + '"');		
		
		re = /#IngresoCalle#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mCalle + '"');
		
		re = /#IngresoNumPue#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mNumPue + '"');
		
		re = /#IngresoPiso#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mPiso + '"');
		
		re = /#IngresoDepto#/gi;
		domicilioRiesgo = domicilioRiesgo.replace(re, '"' + mDepto + '"');
		
		
		varTabla+=domicilioRiesgo
		
		cont=1
		if (document.getElementById("RAMOPCODSQL").value!="ICB1"){
		for (var j=0;j<objXMLCobertura.length;j++)
		{	
			//alert("valor de pCont= " + cont)
			var pItem=objXMLCobertura[j].selectSingleNode("IDITEM").text
			var pCoberDes=objXMLCobertura[j].selectSingleNode("COBERDES").text
			var pTieneObjAseg=objXMLCobertura[j].selectSingleNode("OBJASEG").text
			var pCoberCod=objXMLCobertura[j].selectSingleNode("COBERCOD").text
			var pCoberSuma=objXMLCobertura[j].selectSingleNode("SUMAASEG").text
			var pIngresoObjAseg=false
			if ((pItem==i+1)&&(pTieneObjAseg=="S")){
				//alert("Contador= "+cont)
				varTabla+=fncSeccionCoberturas(i+1,cont,pCoberCod,pCoberDes +" "+ datosCotizacion.getMonedaProducto() +" "+ pCoberSuma,pCoberSuma)
				objAsegurado=""			
				for (var k=0;k<objXMLObjAseg.length;k++){
					if (objXMLObjAseg[k].selectSingleNode("IDITEM").text == pItem){
						//alert("Valor de k="+k)
						var pItemObjAseg=objXMLObjAseg[k].selectSingleNode("IDITEM").text
						var pCoberCodObjAseg=objXMLObjAseg[k].selectSingleNode("COBERCOD").text
						var pNroObj=objXMLObjAseg[k].selectSingleNode("NROOBJ").text
						var pDescripcion=objXMLObjAseg[k].selectSingleNode("DETALLEOBJETO").text
						var pSuma=objXMLObjAseg[k].selectSingleNode("SUMAASEG").text
						var pCodObj=objXMLObjAseg[k].selectSingleNode("CODIGOOBJETO").text
						//alert("Detalle = "+ pDescripcion)
						//tArray[k]=pItemObjAseg+'|'+pCoberCodObjAseg+'|'+pNroObj
						
						if ((pItem==pItemObjAseg)&&(pCoberCod==pCoberCodObjAseg)){
							pIngresoObjAseg=true
							//varTabla+=fncSeccionObjAsegurados(pItemObjAseg,cont,pNroObj,pCoberCodObjAseg,pDescripcion,pSuma, pCodObj)
							objAsegurado+=fncSeccionObjAsegurados(pItemObjAseg,cont,pNroObj,pCoberCodObjAseg,pDescripcion,pSuma, pCodObj)						
						}
					}	
				}
				//alert(pIngresoObjAseg)
				if (pIngresoObjAseg==false){					
					//varTabla+=fncSeccionObjAsegurados(pItem,cont,1,pCoberCod,' ',' ','0')
					objAsegurado+=fncSeccionObjAsegurados(pItem,cont,1,pCoberCod,'','','0')
					
				}
				document.getElementById("OBJ_LISTNro##").innerHTML=objAsegurado
				
				var varObjList=document.getElementById("OBJ_LISTNro##").outerHTML
				
				var re = /##/gi;
				varObjList = varObjList.replace(re, "_" + String(pItem) + "_" + String(cont))
				
				varTabla+=varObjList
				
				var varSumaTotal=document.getElementById("totalSumaObjetos").outerHTML
				re = /##/gi;
				varSumaTotal = varSumaTotal.replace(re, "_" + String(pItem) + "_" + String(cont))
				
				//GED							
				re = /codCobtxtTotalSuma/gi
				varSumaTotal = varSumaTotal.replace(re,pCoberCod);
				
				varTabla+=varSumaTotal
				
				cont=cont + 1
			}
		}
		}
	}
//	window.document.body.insertAdjacentHTML("beforeEnd", "<br>varTabla <textarea>"+varTabla+"</textarea>");
	document.getElementById("riesgosSolicitudMolde").innerHTML=varTabla
	for (i = 0; i <= window.document.getElementsByTagName('SELECT').length - 1; i++) {
            var mobjCampo = window.document.getElementsByTagName('SELECT').item(i);
            if (mobjCampo.name == 'cboTipoObjeto'){
		//alert("Valor ID="+mobjCampo.id)
		var pItem = mobjCampo.id.split("_")[1]
		var pCober = mobjCampo.id.split("_")[2]
    		var pObjeto = mobjCampo.id.split("_")[3]

		fncObjetosCargar(mobjCampo.id)
		recuperaCombo(mobjCampo.id,mobjCampo.referencia,0)
		}
	}
	cont=0

	//window.document.body.insertAdjacentHTML("beforeEnd", "<br>varTabla <textarea>"+document.getElementById("riesgosSolicitudMolde").innerHTML+"</textarea>");
	
	fncColoreoRenglonesObjAse();
	fncSeccionObjAseguradosBotones();
	varXml = null;
}
/******************************************************************************************/
function fncSeccionCoberturas(pId, pCobNro, pCoberCod, pLeyendaCobertura, pSumaCob)
{
	var varRiesgoCobertura = document.getElementById("detalleRiesgoCoberturaNro##").outerHTML

	var re = /##/gi;
	varRiesgoCobertura = varRiesgoCobertura.replace(re, "_" + String(pId) + "_" + String(pCobNro));

	re = /CCCOOOBBB/gi;
	varRiesgoCobertura = varRiesgoCobertura.replace(re, pLeyendaCobertura);
	
	re = /#IngresoSumaCob#/gi;
	varRiesgoCobertura = varRiesgoCobertura.replace(re, '"' + pSumaCob + '"');
	
	re = /#IngresoCOBERCOD#/gi;
	varRiesgoCobertura = varRiesgoCobertura.replace(re, '"' + pCoberCod + '"');

	return varRiesgoCobertura
}
/******************************************************************************************/
function fncSeccionObjAsegurados(pItem,pCont,pObjNro,pCodCobObjAseg,pDescripcion,pSuma,pCodObj){
	var varObjAsegurado = document.getElementById("ObjetoNro##").outerHTML
	
	var re = /##/gi;
	varObjAsegurado = varObjAsegurado.replace(re, "_" + String(pItem) + "_" + String(pCont) + "_" + String(pObjNro));

	re = /CodCob/gi;
	varObjAsegurado = varObjAsegurado.replace(re, pCodCobObjAseg);
	
	re = /#txtDescripcion#/gi;
	varObjAsegurado = varObjAsegurado.replace(re,'"'+pDescripcion+'"');
	
	re = /#txtSuma#/gi;
	varObjAsegurado = varObjAsegurado.replace(re,'"'+pSuma+'"');
	
	re = /cboTipoObjetoSelec/gi;
	varObjAsegurado = varObjAsegurado.replace(re,pCodObj);
	
	
	
//	window.document.body.insertAdjacentHTML("beforeEnd", "<br>varObjList <textarea>"+varObjList+"</textarea>");	

	return varObjAsegurado
}
/******************************************************************************************/
function fncSeccionObjAseguradosBotones()
{
	for (var x=1;x<=mvarObjAsegMaxIte;x++)
	{
		if(document.getElementById("btnCodPosNro_"+ x))
		{
			if (Number(document.getElementById("hidCERTISECNro_"+ x).value) == "0")
			{
				if (document.getElementById("hidProvinCodItemNro_"+ x).value=="1")
					document.getElementById("btnCodPosNro_"+ x).style.display = "";
				else
					document.getElementById("btnCodPosNro_"+ x).style.display = "none";
			} else {
				document.getElementById("btnCodPosNro_"+ x).style.display = "none";
			}
		}
		
		for (var y=1;y<=mvarObjAsegMaxCob;y++)
		{
			for (var z=1;z<=mvarObjAsegMaxObj;z++)
			{
				if(document.getElementById("ObjetoNro_"+ x +"_"+ y +"_"+ z))
				{
					//Agregar
					if(document.getElementById("ObjetoNro_"+ x +"_"+ y +"_"+ (z+1)))
					{
						document.getElementById("btnAgregarNro_"+ x +"_"+ y +"_"+ z).style.display = "none";
						document.getElementById("btnEliminarNro_"+ x +"_"+ y +"_"+ z).style.display = "";
					} else {
						document.getElementById("btnAgregarNro_"+ x +"_"+ y +"_"+ z).style.display = "";
						if (z==1)
							document.getElementById("btnEliminarNro_"+ x +"_"+ y +"_"+ z).style.display = "none";
						else
							document.getElementById("btnEliminarNro_"+ x +"_"+ y +"_"+ z).style.display = "";
					}
				} else {
					break;
				}
			}
		}
	}
}
/******************************************************************************************/
function fncObjetosCargar(pId)
{
	var wvarCantidad;
	
	
	mvarRequest="<Request>" +
        			"<DEFINICION>2350_ListadoObjetos.xml</DEFINICION>" +
	    	    	"</Request>"
	//datosCotizacion.cargaComboXSL(pId,llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboObjetosAsegurados.xsl")
	datosCotizacion.cargaComboXSL(pId,llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboObjetosAsegurados").xml)
}
/******************************************************************************************/
function fncObjetosVerificarDatos(pId)
{
  var pIte = pId.split("_")[1]
  var pCob = pId.split("_")[2]
  var pObj = pId.split("_")[3]
  
	if (!fncValidarObjAseg(pIte, pCob, pObj))
	{
		alert_customizado("Revise los datos del Objeto Asegurado. Fue omitido alg�n dato o el objeto est� repetido.");
		return false
	} else {
		return true
	}
}
/******************************************************************************************/
function fncValidarObjAsegTotal()
{
	var varValOk = true;
	
	for (var x=1;x<=mvarObjAsegMaxIte;x++)
	{
		//Valida domicilio
		if(document.getElementById("domicilioRiesgoNro_"+ x))
			if(!fncValidarObjAsegDomic(x))
				varValOk = false;
			
		for (var y=1;y<=mvarObjAsegMaxCob;y++)
		{
			//Validar objetos asegurados
			if(document.getElementById("txtTotalSuma_"+ x +"_"+ y))
			{	
				//Si no tiene nada cargado, no valido datos de los objetos
				if(!fncValidarObjAsegBlancos(x, y))
				{
					if(!fncValidarObjAsegSuma(x, y))
						varValOk = false;
					
					for (var z=1;z<=mvarObjAsegMaxObj;z++)
					{
						if(document.getElementById("ObjetoNro_"+ x +"_"+ y +"_"+ z))
						{
							if (!fncValidarObjAseg(x, y, z))
								varValOk = false;
						} else {
							break;
						}
					}
				}
			}
		}
	}
	return varValOk;
}
/******************************************************************************************/
function fncValidarObjAseg(pIte, pCob, pObj)
{
	var varValOk = true;
	
	//Tipo de Objeto
	if (document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj).value == 0 ||
			document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj).value == -1 ||
			document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj).value == "")
	{
		insertarError("", "cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj, "Debe seleccionar un Tipo de Objeto",6,"E","");
		varValOk = false
	} else {
		if (document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj +"_etiq"))
			colorNegro(document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj +"_etiq"));
	}
	//Descripci�n
	if (trim(document.getElementById("txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ pObj).value) == "")
	{
		insertarError("", "txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ pObj, "Debe ingresar una descripci�n",6,"E","");
		varValOk = false
	} else {
		if (document.getElementById("txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ pObj +"_etiq"))
			colorNegro(document.getElementById("txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ pObj +"_etiq"));
	}
	//Suma
	if (parseInt(document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ pObj).value) == 0 ||
			trim(document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ pObj).value) == "" ||
			isNaN(trim(document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ pObj).value)))
	{
		insertarError("", "txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ pObj, "Debe ingresar una suma",6,"E","");
		varValOk = false
	} else {
		if (document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ pObj +"_etiq"))
			colorNegro(document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ pObj +"_etiq"));
	}
	
	//Repetidos
	//if (!fncValidarObjAsegRepetidos(pIte, pCob, pObj))
		//varValOk = false
	
	return varValOk;
}
/******************************************************************************************/
function fncValidarObjAsegDomic(pIte)
{
	var varValOk = true;
	
	if (Number(document.getElementById("hidCERTISECNro_"+ pIte).value) == "0")
	{
		//Calle
		if (trim(document.getElementById("calleRiesgoItemNro_"+ pIte).value) == "")
		{
			insertarError("", "calleRiesgoItemNro_"+ pIte,"Debe ingresar una calle",6,"E","");
			varValOk = false;
		} else {
			if (document.getElementById("calleRiesgoItemNro_"+ pIte +"_etiq"))
				colorNegro(document.getElementById("calleRiesgoItemNro_"+ pIte +"_etiq"));
		}
		//Puerta
		if (trim(document.getElementById("numeroRiesgoItemNro_"+ pIte).value) == "")
		{
			insertarError("", "numeroRiesgoItemNro_"+ pIte,"Debe ingresar un n�mero de puerta",6,"E","");
			varValOk =false;
		} else {
			//HRF 2010-11-10 Def.96
			if (isNaN(trim(document.getElementById("numeroRiesgoItemNro_"+ pIte).value)) &&
					trim(document.getElementById("numeroRiesgoItemNro_"+ pIte).value.toUpperCase()) != "S/N")
			{
				insertarError("", "numeroRiesgoItemNro_"+ pIte,"El dato ingresado no es correcto, solo se permiten car�cteres numericos o S/N",6,"E","");
				varValOk =false;
			} else {
				if (document.getElementById("numeroRiesgoItemNro_"+ pIte +"_etiq"))
					colorNegro(document.getElementById("numeroRiesgoItemNro_"+ pIte +"_etiq"));
			}
		}
		//C�digo Postal
		if (trim(document.getElementById("codposRiesgoItemNro_"+ pIte).value) == "" ||
				trim(document.getElementById("codposRiesgoItemNro_"+ pIte).value) == "0")
		{
			insertarError("", "codposRiesgoItemNro_"+ pIte,"Debe seleccionar un c�digo postal",6,"E","");
			varValOk =false;
		} else {
			if (document.getElementById("codposRiesgoItemNro_"+ pIte +"_etiq"))
				colorNegro(document.getElementById("codposRiesgoItemNro_"+ pIte +"_etiq"));
		}
		//HRF 2010-11-08 Def.27
		if (varValOk && document.getElementById("hidProvinCodItemNro_"+ pIte).value=="1")
		{
			var varVerCal = fncVerificarCalleNro(document.getElementById("calleRiesgoItemNro_"+ pIte).value,document.getElementById("numeroRiesgoItemNro_"+ pIte).value,document.getElementById("codposRiesgoItemNro_"+ pIte).value);
			if ( varVerCal != true)
			{
				insertarError("", "codposRiesgoItemNro_"+ pIte,varVerCal,6,"E","");
				varValOk =false;
			}
		}
	}

	return varValOk;
}
/******************************************************************************************/
function fncValidarObjAsegSuma(pIte, pCob)
{
	var varValOk = true;
	
	//Suma Cobertura contra el Total
	if (document.getElementById("txtTotalSuma_"+ pIte +"_"+ pCob).value != 
			document.getElementById("hidRiesgoCobSumaNro_"+ pIte +"_"+ pCob).value)
	{
		insertarError("", "txtTotalSuma_"+ pIte +"_"+ pCob,"La suma de objetos asegurados debe coincidir con el valor de la cobertura.",6,"E","");
		varValOk = false;
	} else {
		if (document.getElementById("txtTotalSuma_"+ pIte +"_"+ pCob +"_etiq"))
			colorNegro(document.getElementById("txtTotalSuma_"+ pIte +"_"+ pCob +"_etiq"));
	}

	return varValOk;
}
/******************************************************************************************/
function fncValidarObjAsegRepetidos(pIte, pCob, pObj)
{
	var varValOk = true;
	
	for (var z=1;z<=mvarObjAsegMaxObj;z++)
	{
		if(document.getElementById("ObjetoNro_"+ pIte +"_"+ pCob +"_"+ z))
		{
			if (z!=pObj && document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ z).value != 0)
			{
				if (document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj).value == 
						document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ z).value)
				{
					insertarError("", "cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj, "Tipo de Objeto repetido.",6,"E","");
					varValOk = false
				}
			}
		}
	}
	
	return varValOk;
}
/******************************************************************************************/
function fncValidarObjAsegBlancos(pIte, pCob)
{
	var varValOk = true;
	
	//Color Negro
	if (document.getElementById("txtTotalSuma_"+ pIte +"_"+ pCob +"_etiq"))
		colorNegro(document.getElementById("txtTotalSuma_"+ pIte +"_"+ pCob +"_etiq"));
	
	for (var z=1;z<=mvarObjAsegMaxObj;z++)
	{
		if(document.getElementById("ObjetoNro_"+ pIte +"_"+ pCob +"_"+ z))
		{
			//Color Negro
			if (document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ z +"_etiq"))
				colorNegro(document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ z +"_etiq"));
			if (document.getElementById("txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ z +"_etiq"))
				colorNegro(document.getElementById("txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ z +"_etiq"));
			if (document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ z +"_etiq"))
				colorNegro(document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ z +"_etiq"));
			
			//Valida si tiene algo cargado
			if (document.getElementById("cboTipoObjetoNro_"+ pIte +"_"+ pCob +"_"+ z).value != 0 ||
					document.getElementById("txtDescripcionObjNro_"+ pIte +"_"+ pCob +"_"+ z).value != "" ||
					(document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ z).value != "" &&
					 document.getElementById("txtSumaObjNro_"+ pIte +"_"+ pCob +"_"+ z).value != "0"))
			{
				varValOk = false
			}
		}
	}
	
	return varValOk;
}
/******************************************************************************************/
function fncInsertarObjAsegTotal()
{
	varXmlItems = new ActiveXObject("MSXML2.DOMDocument");
	varXmlItems.async = false;
	varXmlItems.loadXML(document.getElementById("varXmlItems").value);
	
	var wXMLObjAse = ""
	
	wXMLObjAse += "<OBJETOS>"
	
	for (var x=1;x<=mvarObjAsegMaxIte;x++)
	{
		//Direcci�n de riesgo
		if (varXmlItems.selectSingleNode('//ITEM[@ID='+ x +']') &&
				document.getElementById("calleRiesgoItemNro_"+x))
		{
			var objXMLItems = varXmlItems.selectNodes('//ITEM[@ID='+ x +']');
			objXMLItems[0].selectSingleNode("CPOSDOM").text  = document.getElementById("codposRiesgoItemNro_"+x).value.toUpperCase();
			objXMLItems[0].selectSingleNode("DOMICDOM").text = document.getElementById("calleRiesgoItemNro_"+x).value.toUpperCase();
			objXMLItems[0].selectSingleNode("DOMICDNU").text = document.getElementById("numeroRiesgoItemNro_"+x).value.toUpperCase();
			objXMLItems[0].selectSingleNode("DOMICPIS").text = document.getElementById("pisoRiesgoItemNro_"+x).value.toUpperCase();
			objXMLItems[0].selectSingleNode("DOMICPTA").text = document.getElementById("dptoRiesgoItemNro_"+x).value.toUpperCase();
		}
		
		for (var y=1;y<=mvarObjAsegMaxCob;y++)
		{
			for (var z=1;z<=mvarObjAsegMaxObj;z++)
			{
				if(document.getElementById("txtSumaObjNro_"+ x +"_"+ y +"_"+ z))
					wXMLObjAse += fncInsertarObjAseg(x, y, z);
				else
					break;
			}
		}
	}
	
	wXMLObjAse += "</OBJETOS>"
	
	document.getElementById("varXmlObjAseg").value = wXMLObjAse;
	document.getElementById("varXmlItems").value = varXmlItems.xml;
}
/******************************************************************************************/
function fncInsertarObjAseg(pIte, pCob, pObj)
{
	var varONodo;
	var varONodoObj;
	
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	var objXMLCobertura = varXml.getElementsByTagName('COBERTURA');
	
	//IDITEM
	varONodoObj = varXml.createElement("OBJETO");
	varONodo = varXml.createElement("IDITEM");
	varONodo.text = pIte;
	varONodoObj.appendChild(varONodo);
	//COBERCOD
	varONodo = varXml.createElement("COBERCOD");
	var pCobCod = document.getElementById("ObjetoNro_"+ pIte +"_"+ pCob +"_"+ pObj).descripcion;
	varONodo.text = pCobCod
	varONodoObj.appendChild(varONodo);
	//NROOBJ		
	varONodo = varXml.createElement("NROOBJ");
	varONodo.text = pObj;
	varONodoObj.appendChild(varONodo);			
	//CODIGOOBJETO
	varONodo = varXml.createElement("CODIGOOBJETO");
	varONodo.text=document.getElementById("cboTipoObjetoNro_"+pIte+"_"+pCob+"_"+pObj).value
	varONodoObj.appendChild(varONodo);
	//OBJETODES
	varONodo = varXml.createElement("OBJETODES");
	varONodo.text=document.getElementById("cboTipoObjetoNro_"+pIte+"_"+pCob+"_"+pObj).options[document.getElementById("cboTipoObjetoNro_"+pIte+"_"+pCob+"_"+pObj).selectedIndex].text
	varONodoObj.appendChild(varONodo);			
	//SUMAASEG
	varONodo = varXml.createElement("SUMAASEG");
	varONodo.text=document.getElementById("txtSumaObjNro_"+pIte+"_"+pCob+"_"+pObj).value==""?0:document.getElementById("txtSumaObjNro_"+pIte+"_"+pCob+"_"+pObj).value;
	varONodoObj.appendChild(varONodo);
	//DETALLEOBJETO
	varONodo = varXml.createElement("DETALLEOBJETO");
	varONodo.text=document.getElementById("txtDescripcionObjNro_"+pIte+"_"+pCob+"_"+pObj).value.toUpperCase()
	varONodoObj.appendChild(varONodo);

	return String(varONodoObj.xml);
}
/******************************************************************************************/
function fncXMLObjAseResguardo(pIDITEM, pIDNEW)
{	//alert(pIDITEM+','+ pIDNEW)
	//Resguardar XML de Objetos Asegurados en varXmlObjAsegAux para volver a agregarlo luego de Editar
	var varXmlObjAseg = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAseg.async = false;
	varXmlObjAseg.loadXML(document.getElementById("varXmlObjAseg").value);
	
	var varXmlObjAsegAux = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAsegAux.async = false;
	varXmlObjAsegAux.loadXML(document.getElementById("varXmlObjAsegAux").value);
	
	var oXMLObjAseg = varXmlObjAseg.selectNodes("//OBJETOS/OBJETO[IDITEM='"+ pIDITEM +"']");
	var varNodo;
	
	for (var x=0;x<oXMLObjAseg.length;x++)
	{
		varNodo = oXMLObjAseg[x];
		varXmlObjAsegAux.selectSingleNode("//OBJETOS").appendChild(varNodo);
	}
	
	//Agrego los Objetos Asegurados en el XML Auxiliar
	document.getElementById("varXmlObjAsegAux").value = varXmlObjAsegAux.xml;
	var wvarIDOLD = new RegExp("<IDITEM>"+ String(pIDITEM) +"</IDITEM>","gi")	
	var wvarIDNEW = "<IDITEM>"+ String(pIDNEW) +"</IDITEM>"
	document.getElementById("varXmlObjAsegAux").value = document.getElementById("varXmlObjAsegAux").value.replace(wvarIDOLD,wvarIDNEW);
	//Elimino los Objetos Asegurados en el XML
	fncXMLObjAseBorrar(pIDITEM, pIDNEW)
}
/****************************************************************************************************************************************/
function fncXMLObjAseBorrar(pIDITEM, pIDNEW)
{

	//Eliminar un IDITEM de varXmlObjAseg
	var varXmlObjAseg = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAseg.async = false;
	varXmlObjAseg.loadXML(document.getElementById("varXmlObjAseg").value);
	
	var oXMLObjAseg = varXmlObjAseg.selectNodes("//OBJETOS/OBJETO[IDITEM='"+ pIDITEM +"']");
	var varNodo;
	
	for (var x=0;x<oXMLObjAseg.length;x++)
	{
		varNodo = oXMLObjAseg[x];
		varXmlObjAseg.selectSingleNode("//OBJETOS").removeChild(varNodo);
	}
	
	//Actualizo el XML
	document.getElementById("varXmlObjAseg").value = varXmlObjAseg.xml;
	var wvarIDOLD = new RegExp();
	var wvarIDNEW = "";
	
	for (var y=parseInt(pIDITEM)+1;y<=pIDNEW;y++)
	{
		wvarIDOLD = RegExp("<IDITEM>"+ String(y) +"</IDITEM>","gi")
		wvarIDNEW = "<IDITEM>"+ String(y-1) +"</IDITEM>"
		//alert (wvarIDOLD +"-"+ wvarIDNEW)
		document.getElementById("varXmlObjAseg").value = document.getElementById("varXmlObjAseg").value.replace(wvarIDOLD,wvarIDNEW);
	}
}
/****************************************************************************************************************************************/
function fncXMLObjAseAgregar()
{
	//Agregar un desde varXmlObjAsegAux a varXmlObjAseg
	var varXmlObjAseg = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAseg.async = false;
	varXmlObjAseg.loadXML(document.getElementById("varXmlObjAseg").value);
	
	var varXmlObjAsegAux = new ActiveXObject('MSXML2.DOMDocument');
	varXmlObjAsegAux.async = false;
	varXmlObjAsegAux.loadXML(document.getElementById("varXmlObjAsegAux").value);
	
	var oXMLObjAseg = varXmlObjAsegAux.selectNodes("//OBJETOS/OBJETO");
	var varNodo;
	
	for (var x=0;x<oXMLObjAseg.length;x++)
	{
		varNodo = oXMLObjAseg[x];
		varXmlObjAseg.selectSingleNode("//OBJETOS").appendChild(varNodo);
	}
	
	//Agrego los Objetos Asegurados en el XML
	document.getElementById("varXmlObjAseg").value = varXmlObjAseg.xml;
	//Limpio el XML Auxiliar
	document.getElementById("varXmlObjAsegAux").value = "<OBJETOS/>";
}
/******************************************************************************************/
function fncSumarObjetosAsegurados(pObject)
{
	var vIndIte = pObject.id.split("_")[1];
	var vIndCob = pObject.id.split("_")[2];
	var vIndObj = pObject.id.split("_")[3];
	var vSuma = 0;

	for (var x=1;x<=mvarObjAsegMaxObj;x++)
	{
		if(document.getElementById("txtSumaObjNro_"+ vIndIte +"_"+ vIndCob +"_"+ x))
			vSuma += parseInt(document.getElementById("txtSumaObjNro_"+ vIndIte +"_"+ vIndCob +"_"+ x).value);
		else
			break;
	}
	
	if (isNaN(vSuma))
		document.getElementById("txtTotalSuma_"+ vIndIte +"_"+ vIndCob).value = "0";
	else
		document.getElementById("txtTotalSuma_"+ vIndIte +"_"+ vIndCob).value = vSuma;
		
	//Valor anterior de coberturas
	if(document.getElementById("TIPOOPER").value=="R")
	{
		var varXmlRen;
				varXmlRen = new ActiveXObject('MSXML2.DOMDocument');
				varXmlRen.async = false;
				varXmlRen.loadXML(document.getElementById("varXmlRenovItems").value);		
		
		var wCERTISEC = fncSetLength(document.getElementById("hidCERTISECNro_"+ vIndIte).value,6);
		
		if (Number(wCERTISEC) != 0)
		{
			var wSUMAASEG = 0
			
			if (varXmlRen.selectSingleNode("//ITEM[CERTISECANT='"+ wCERTISEC +"']/COBERTURAS/COBERTURA[COBERCOD='"+ document.getElementById("txtTotalSuma_"+ vIndIte +"_"+ vIndCob).cobercod +"']/SUMAASEG"))
			{
				wSUMAASEG = varXmlRen.selectSingleNode("//ITEM[CERTISECANT='"+ wCERTISEC +"']/COBERTURAS/COBERTURA[COBERCOD='"+ document.getElementById("txtTotalSuma_"+ vIndIte +"_"+ vIndCob).cobercod +"']/SUMAASEG").text
			
				if (document.getElementById("txtTotalSuma_"+ vIndIte +"_"+ vIndCob).value != wSUMAASEG )
					document.getElementById( "txtTotalSuma_"+ vIndIte +"_"+ vIndCob ).style.fontWeight="bold";
				else
					document.getElementById( "txtTotalSuma_"+ vIndIte +"_"+ vIndCob ).style.fontWeight="normal";
			}
			document.getElementById( "txtTotalSuma_"+ vIndIte +"_"+ vIndCob ).onmouseover=new Function ("showAlt('<b>Valor Inicial:</b> "+ wSUMAASEG +"')");
			document.getElementById( "txtTotalSuma_"+ vIndIte +"_"+ vIndCob ).onmouseout=new Function ("hideAlt()");
			document.getElementById( "txtTotalSuma_"+ vIndIte +"_"+ vIndCob ).valorOriginal = wSUMAASEG;
		}
	}
}
/******************************************************************************************/
function fncSumarObjetosAseguradosTotal()
{
	for (var x=1;x<=mvarObjAsegMaxIte;x++)
	{
		for (var y=1;y<=mvarObjAsegMaxCob;y++)
		{
		if(document.getElementById("txtSumaObjNro_"+ x +"_"+ y +"_1"))
			fncSumarObjetosAsegurados(document.getElementById("txtSumaObjNro_"+ x +"_"+ y +"_1"));
		else
			break;
		}
	}
}
/****************************************************************************************************************************************/
function fncColoreoRenglonesObjAse()
{
var mvarAlternoColor = 0;
var mobjCampo;

	for (i = 0; i <= window.document.getElementsByTagName('DIV').length - 1; i++)
	{
		mobjCampo = window.document.getElementsByTagName('DIV').item(i);
		if (mobjCampo.className == 'Objetos')
		{
			if (mobjCampo.style.display != 'none')
			{
				if (mobjCampo.id.split("_")[3] == "1")
					mvarAlternoColor = 0
					
				if (mvarAlternoColor == 0)
				{
					mobjCampo.style.backgroundColor = '#EDEDF1';
					mvarAlternoColor = 1;
				}
				else
				{
					mobjCampo.style.backgroundColor = '#FFFFFF';
					mvarAlternoColor = 0;
				}
			}
		}
	}
}
/******************************************************************************************/
function fncCodigoPostalLimpiar(pObjeto)
{
	var wIte = pObjeto.id.split("_")[1]
	
	if (document.getElementById("hidProvinCodItemNro_"+ wIte))
	{
		if (document.getElementById("hidProvinCodItemNro_"+ wIte).value=="1")
		{
			document.getElementById("codposRiesgoItemNro_"+ wIte).value = "0"
		}
	}
}
/******************************************************************************************/
function fncCodigoPostalBuscar(pObjeto)
{
	var wIte = pObjeto.id.split("_")[1]
	
	if (document.getElementById("hidProvinCodItemNro_"+ wIte).value=="1")
	{	
		document.getElementById("pCpoProvLocBuscar").value      = "hidProvinCodItemNro_"+ wIte;
		document.getElementById("pCpoLocBuscar").value          = "localidadRiesgoItemNro_"+ wIte;
		document.getElementById("pCpoCodPostalLocBuscar").value = "codposRiesgoItemNro_"+ wIte;
		document.getElementById("pCpoCalleBuscar").value        = "calleRiesgoItemNro_"+ wIte;
		document.getElementById("pCpoNroCalleBuscar").value     = "numeroRiesgoItemNro_"+ wIte;
	
		fncCallejero("calleRiesgoItemNro_"+ wIte, "numeroRiesgoItemNro_"+ wIte, "hidProvinCodItemNro_"+ wIte ,"codposRiesgoItemNro_"+ wIte);
	}
}
/******************************************************************************************/
function fncRenovacionCambiosObjAseg()
{
	for (var x=1;x<=mvarObjAsegMaxIte;x++)
	{
		if (document.getElementById("hidCERTISECNro_"+ x))
		{
			if (Number(document.getElementById("hidCERTISECNro_"+ x).value) > 0)
			{
				for (var y=1;y<=mvarObjAsegMaxCob;y++)
				{
					if (document.getElementById("txtTotalSuma_"+ x +"_"+ y))
					{
						if (document.getElementById("txtTotalSuma_"+ x +"_"+ y).value != document.getElementById("txtTotalSuma_"+ x +"_"+ y).valorOriginal)
						{
							insertarXMLCambios("ObjAseg"+ x,
																 "Obj.Esp. "+x+"-"+ getItemDesc(x)+ " (Modificado)",
																 6,
																 "COBERCOD|"+ document.getElementById("hidRiesgoCOBERCODNro_"+ x +"_"+ y).value,
																 document.getElementById("txtTotalSuma_"+ x +"_"+ y).value,
																 document.getElementById("txtTotalSuma_"+ x +"_"+ y).valorOriginal,
																 "M")
						}
					}
				}
			}
		}
	}
}
/******************************************************************************************/