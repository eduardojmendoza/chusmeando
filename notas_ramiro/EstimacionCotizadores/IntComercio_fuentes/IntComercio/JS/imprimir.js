/*
'--------------------------------------------------------------------------------
'Fecha de Modificaci�n: 19/03/2012
'PPCR: 2011-00056
'Desarrollador: Jose Casais
'Descripci�n: Se agrega producto ICB1
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 02/12/2011
 PPCR: 50055/6010661
 Desarrolador: Marcelo Gasparini
 Descripci�n: Se agrega cambios en los impresos de Solicitud y Autorizaciones de tarjetas y d�bito
'--------------------------------------------------------------------------------
' Fecha de Modificaci�n: 01/07/2011
' Ticket 632535
' Desarrolador: Gabriel D'Agnone
' Descripci�n: MOSTRAR PRIMAS Y TASAS EN IMPRESO A REVISION
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 03/02/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripci�n: Se corrigi� DEFECT 258
--------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: imprimir.js

Fecha de Creaci�n: 03/05/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripci�n: Este archivo JS se crea para definir funciones de impresion

*/


/**************************************************************************************************/
function armaXMLCotizacion(pTipoImpreso)
{
	//GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision
	var impresoRev = false	
	if (pTipoImpreso!= undefined) 
		if(pTipoImpreso == "R") 
			var impresoRev = true
	//	alert("armaXMLCotizacion -->" +  pTipoImpreso)
		
	var hdatos;	
	var pvalor = getRadioButtonSelectedValue(document.all.copia);
	
	objXMLLeyenda = new ActiveXObject("Msxml2.DOMDocument");
	objXMLLeyendasPreg = new ActiveXObject("Msxml2.DOMDocument");
	objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	
	objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	
	//GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision
	objXMLItemImpresoRev = new ActiveXObject("Msxml2.DOMDocument");
	
	//if (document.getElementById("varXmlItemsImpresoRev").value != "")
	if(pTipoImpreso == "R") 
		objXMLItemImpresoRev.loadXML(document.getElementById("varXmlItemsImpresoRev").value);	
	else
		objXMLItemImpresoRev.loadXML(document.getElementById("varXmlItems").value);	
		
	objXMLItemImpresoRev.setProperty("SelectionLanguage", "XPath");
	var objXMLCoberturaImpresoRev = objXMLItemImpresoRev.getElementsByTagName('COBERTURA');
	
	
	objXMLItem.setProperty("SelectionLanguage", "XPath");
	var objXMLNode = objXMLItem.getElementsByTagName('ITEM');
//	window.document.body.insertAdjacentHTML("beforeEnd", "<br>objXMLItem <textarea>"+objXMLItem.xml+"</textarea>");
	var CantItem = objXMLItem.selectNodes("//ITEM").length;
	var CantCobertura = objXMLItem.selectNodes("//COBERTURA").length;;
	var objXMLCobertura = objXMLItem.getElementsByTagName('COBERTURA');
	var objXMLPreguntas = objXMLItem.getElementsByTagName('PREGUNTA');
	var objXMLItemLeyenda = "";
	var objXMLLeyendaCobertura= ""; 
	var pCoberCod = "";
	var pZonaCod = "";
	var pExiste = false;
	var pMjeCober = "";
	var mLeyendaCondGen = "";
	var mLeyendaBenAdic = "";
//	alert("objXMLNode.length="+ objXMLNode.length)
	var mvarTIPOPERSONA = document.getElementById("tipoPersona").value!="-1"?document.getElementById("tipoPersona").options[document.getElementById("tipoPersona").selectedIndex].text:""
	var mvarIVA = document.getElementById("IVA").value!="-1"?document.getElementById("IVA").options[document.getElementById("IVA").selectedIndex].text:""
	var mvarIIBB = document.getElementById("IngresosBrutos").value!="-1"?document.getElementById("IngresosBrutos").options[document.getElementById("IngresosBrutos").selectedIndex].text:""
	//03/11/2010 LR Defect 167-	Periodo a cotizar: debe salir en may�scula como el resto de los datos.
	var mvarPERIODO = document.getElementById("periodoCotizar").value!="-1"?document.getElementById("periodoCotizar").options[document.getElementById("periodoCotizar").selectedIndex].text.toUpperCase() :""
	var mvarESTABILIZACION = document.getElementById("estabilizacion").value!="-1"?document.getElementById("estabilizacion").options[document.getElementById("estabilizacion").selectedIndex].text:""
	var mvarFORMAPAGO = document.getElementById("formaPago").value!="-1"?document.getElementById("formaPago").options[document.getElementById("formaPago").selectedIndex].text:""
	var mvarPLANPAGO = document.getElementById("planPago").value.split("|")[0]!="-1"?document.getElementById("planPago").options[document.getElementById("planPago").selectedIndex].text:""
	var mvarLeyendasPreg= document.getElementById("xmlPreguntas").value;
	//03/11/2010 LR Defect 167-	No figura el domicilio de correspondencia ni el c�digo postal en los datos del cliente.
	var Prov = document.getElementById("provinciaDomicilio").value != "-1" ? document.getElementById("provinciaDomicilio").options[document.getElementById("provinciaDomicilio").selectedIndex].text : ""
	var DOMICCPO = trim(document.getElementById("codPosDomicilio").value)!=""?trim(document.getElementById("codPosDomicilio").value):"";

	hdatos="<COTIZACION_CON>";	
	if (document.getElementById("apellidoCliente").value.toUpperCase()=="" )
		{hdatos+="<DATOSCLIENTE>";
			hdatos+="<APELLIDORAZONSOCIAL><![CDATA["+ document.getElementById("apellidoCliente").value.toUpperCase() +"]]></APELLIDORAZONSOCIAL>";
			hdatos+="<NOMBRE><![CDATA["+ document.getElementById("nombreCliente").value.toUpperCase() +"]]></NOMBRE>";
			hdatos+="<TIPODOC></TIPODOC>";
			hdatos+="<NRODOCUMENTO></NRODOCUMENTO>";
			hdatos+="<TIPODEPERSONA>"+ mvarTIPOPERSONA.toUpperCase()+"</TIPODEPERSONA>";
			hdatos+="<POSICION_IVA>"+ mvarIVA+"</POSICION_IVA>";
			hdatos+="<IIBB>"+ mvarIIBB +"</IIBB>";
			hdatos+="<TEL>"+ document.getElementById("telefonoCliente").value +"</TEL>";
			hdatos+="<EMAIL>"+ document.getElementById("emailCliente").value.toUpperCase() +"</EMAIL>";
			//03/11/2010 LR Defect 167-	No figura el domicilio de correspondencia ni el c�digo postal en los datos del cliente.
			hdatos += "<DOMICORRESP>" + trim(document.getElementById("calleDomicilio").value + " " + document.getElementById("nroDomicilio").value + " " + document.getElementById("pisoDomicilio").value + " " + document.getElementById("dptoDomicilio").value + " " + document.getElementById("localidadDomicilio").value) + "</DOMICORRESP>";
			hdatos += "<PROV>" + Prov + "</PROV>";
			hdatos += "<CODPOS>" + DOMICCPO + "</CODPOS>";
		hdatos+="</DATOSCLIENTE>";}
	else		
		{hdatos+="<DATOSCLIENTE>";
			hdatos+="<APELLIDORAZONSOCIAL><![CDATA["+ document.getElementById("apellidoCliente").value.toUpperCase() +"]]></APELLIDORAZONSOCIAL>";
			hdatos+="<NOMBRE><![CDATA["+ document.getElementById("nombreCliente").value.toUpperCase() +"]]></NOMBRE>";
			hdatos+="<TIPODOC>"+ document.getElementById("tipoDocCliente").options[document.getElementById("tipoDocCliente").selectedIndex].text +"</TIPODOC>";
			hdatos+="<NRODOCUMENTO>"+ document.getElementById("nroDocumentoCliente").value +"</NRODOCUMENTO>";
			hdatos+="<TIPODEPERSONA>"+ mvarTIPOPERSONA +"</TIPODEPERSONA>";
			hdatos+="<POSICION_IVA>"+ mvarIVA +"</POSICION_IVA>";
			hdatos+="<IIBB>"+ mvarIIBB +"</IIBB>";
			hdatos+="<TEL>"+ document.getElementById("telefonoCliente").value +"</TEL>";
			hdatos+="<EMAIL>"+ document.getElementById("emailCliente").value.toUpperCase() +"</EMAIL>";
			//03/11/2010 LR Defect 167-	No figura el domicilio de correspondencia ni el c�digo postal en los datos del cliente.
			hdatos += "<DOMICORRESP>" + trim(document.getElementById("calleDomicilio").value + " " + document.getElementById("nroDomicilio").value + " " + document.getElementById("pisoDomicilio").value + " " + document.getElementById("dptoDomicilio").value + " " + document.getElementById("localidadDomicilio").value) + "</DOMICORRESP>";
			hdatos += "<PROV>" + Prov + "</PROV>";
			hdatos += "<CODPOS>" + DOMICCPO + "</CODPOS>";
		hdatos+="</DATOSCLIENTE>";}
		/******************************************************************************************************************/
		hdatos+="<CONDICIONESCOTIZACION>";
		//jc se agrega icobb
		

		if (document.getElementById("RAMOPCODSQL").value == "ICB1")
		{
			hdatos += "<PLAN>" +  document.getElementById("cboPlanICOBB").options[document.getElementById("cboPlanICOBB").selectedIndex].text+ "</PLAN>"; 
			hdatos += "<ACTIVIDAD>" +  document.getElementById("cboActividadICOBB").options[document.getElementById("cboActividadICOBB").selectedIndex].text+ "</ACTIVIDAD>"; 
		}else{
			hdatos += "<ACTIVIDAD>" +  document.getElementById("cboActividadComercio").options[document.getElementById("cboActividadComercio").selectedIndex].text+ "</ACTIVIDAD>";
}

    
			var vPRODUCTOR = document.getElementById("I_AGENTE").value.split("|")[2] + ' (' + document.getElementById("I_AGENTE").value.split("|")[1] + '-' + document.getElementById("I_AGENTE").value.split("|")[0] + ')';
			hdatos+="<PRODUCTOR><![CDATA["+ vPRODUCTOR +"]]></PRODUCTOR>";
			hdatos+="<ORGANIZADOR><![CDATA["+ document.getElementById("I_ORGANIZADOR").value.split("|")[2] +"]]></ORGANIZADOR>";
			hdatos+="<VIGENCIADESDE>"+ document.getElementById("vigenciaDesde").value +"</VIGENCIADESDE>";
			//-------------------------------------------------------------------------------------------------------//
			//01/11/2010 LR Defect 167. La fecha de inicio y fin de vigencia hay que modificarlas para que sean 15 d�as.
			var dia=fncSetLength(document.getElementById("vigenciaDesde").value.split("/")[0],2)
			var mes=fncSetLength(document.getElementById("vigenciaDesde").value.split("/")[1],2)
			var anio=document.getElementById("vigenciaDesde").value.split("/")[2]
			fechaVig=dateAddExtention(dia,mes,anio,"d",15)
			dia=fncSetLength(fechaVig.split("/")[0],2)
			mes=fncSetLength(fechaVig.split("/")[1],2)
			anio=fechaVig.split("/")[2]			
			if (mes=='00')
			{
				mes='12'
				anio=Number(anio)-1
				fechaVig=dia+'/'+mes+'/'+anio
			}
			hdatos+="<VIGENCIAHASTA>"+ fechaVig +"</VIGENCIAHASTA>";
			//-------------------------------------------------------------------------------------------------------//
			hdatos+="<PERIODOCOTIZAR>"+ mvarPERIODO +"</PERIODOCOTIZAR>";
			hdatos+="<ESTABILIZACION>"+ mvarESTABILIZACION +"</ESTABILIZACION>";
			hdatos += "<FORMADEPAGO>" + mvarFORMAPAGO + "</FORMADEPAGO>";
			
			//----------------------------------------------------------------------------------------------------------------//
			//05/11/2010 LR Defect 167 -	Para los impresos de canal 150 no figuran los datos de la Sucursal y del vendedor
			if (document.getElementById("CANALHSBC").value == "S")
			{
				hdatos += "<SUCURSAL>" + document.getElementById("cbo_vendSucursal").options[document.getElementById("cbo_vendSucursal").selectedIndex].text + "</SUCURSAL>";
				if (document.getElementById('I_VENDEDOR'))
					hdatos += "<VENDEDOR>" + document.getElementById("I_VENDEDOR").options[document.getElementById("I_VENDEDOR").selectedIndex].text + "</VENDEDOR>";
				else
					hdatos += "<VENDEDOR></VENDEDOR>";
				hdatos += "<CANALHSBC>"+ document.getElementById("CANALHSBC").value +"</CANALHSBC>";
			}
			else
			{
				hdatos += "<SUCURSAL></SUCURSAL>";
				hdatos += "<VENDEDOR></VENDEDOR>";
				hdatos += "<CANALHSBC>"+ document.getElementById("CANALHSBC").value +"</CANALHSBC>";
			}
			//----------------------------------------------------------------------------------------------------------------//

			//DATOS DE FORMAS DE PAGO
			var Tarjeta = document.getElementById("cbo_nomTarjeta").value != "-1" ? document.getElementById("cbo_nomTarjeta").options[document.getElementById("cbo_nomTarjeta").selectedIndex].text : ""

			if (Tarjeta != "")
			    hdatos += "<DESC_FP1>" + document.getElementById("cbo_nomTarjeta").options[document.getElementById("cbo_nomTarjeta").selectedIndex].text + "</DESC_FP1>";
			else {
			    var Sucursal = document.getElementById("cboSucursalDC").value != "-1" ? document.getElementById("cboSucursalDC").options[document.getElementById("cboSucursalDC").selectedIndex].text : ""
			    if (Sucursal != "")
			        hdatos += "<DESC_FP1>" + document.getElementById("cboSucursalDC").options[document.getElementById("cboSucursalDC").selectedIndex].text + "</DESC_FP1>";

			    else

			        hdatos += "<DESC_FP1></DESC_FP1>";
			}

			if (Tarjeta != "")
			    hdatos += "<DESC_FP2>" + document.getElementById("numTarjeta").value + "</DESC_FP2>";

			else {
			    if (document.getElementById("numCuentaDC").value != "")
			        hdatos += "<DESC_FP2>" + document.getElementById("numCuentaDC").value + "</DESC_FP2>";

			    else {
			        if (document.getElementById("numCBU").value != "")

			            hdatos += "<DESC_FP2>" + document.getElementById("numCBU").value + "</DESC_FP2>";

			        else

			            hdatos += "<DESC_FP2></DESC_FP2>";
			    }

			}
			//********************************************************
			
			
			hdatos += "<PLANDEPAGO>" + mvarPLANPAGO + "</PLANDEPAGO>";
			hdatos += "<OBSERVACIONCLAUSULA>" + document.getElementById("observClausulas").innerText + "</OBSERVACIONCLAUSULA>"
			hdatos += "<TEXTO>" + document.getElementById("textoPoliza").innerText + "</TEXTO>"
		hdatos+="</CONDICIONESCOTIZACION>";
		hdatos+="<ITEMS>";
		for (i=0;i<objXMLNode.length;i++)
 		{
 			//04/11/2010 LR Defect 167-	-	Falta el campo localidad y el c�digo postal dentro de cada item.
 			var wCODPOS = objXMLNode[i].selectSingleNode("CPOSDOM").text==""?'1005':objXMLNode[i].selectSingleNode("CPOSDOM").text;
 			//alert("Valor de i="+ i)	
			hdatos+="<ITEM>";
				hdatos+="<ID>"+ objXMLNode[i].selectSingleNode("ID").text +"</ID>";
				hdatos+="<PROVICODDES>"+ objXMLNode[i].selectSingleNode("PROVICODDES").text +"</PROVICODDES>";
				hdatos+="<CPOSDOM>"+ wCODPOS +"</CPOSDOM>";
				//04/11/2010 LR Defect 167- En el campo Ubicaci�n del riesgo en los datos del item, debe ser la direcci�n exacta del domicilio del riesgo y no la localidad
				var vUBICARIESGO = objXMLNode[i].selectSingleNode("DOMICDOM").text+" "+objXMLNode[i].selectSingleNode("DOMICDNU").text
				hdatos+="<UBICARIESGO>"+ vUBICARIESGO +"</UBICARIESGO>";
				hdatos += "<DOMICPOB>" + objXMLNode[i].selectSingleNode("DOMICPOB").text + "</DOMICPOB>";
				hdatos += "<TIPOALARMA>" + document.getElementById("cboTipoAlarma").options[parseInt(objXMLNode[i].selectSingleNode("ALARMA").text) - 1].text + "</TIPOALARMA>"
				hdatos += "<TIPOGUARDIA>" + document.getElementById("cboTipoGuardia").options[parseInt(objXMLNode[i].selectSingleNode("GUARDIA").text) - 1].text + "</TIPOGUARDIA>" 
				//04/11/2010 LR Defect 167-	No figura el campo �Zona� dentro de los �tems
				hdatos += "<ZONAITEM>"+ objXMLNode[i].selectSingleNode("ZONAITEM").text +"</ZONAITEM>"
				//------------------------------------------------------------------------------------------//
				//LR 25/01/2011 - Marca para mostrar en el impreso la Tasa y Prima Comisionable
				hdatos+="<ESTADO>"+ objXMLNode[i].selectSingleNode("ESTADO").text +"</ESTADO>";
				//------------------------------------------------------------------------------------------//
			hdatos+="</ITEM>";
		var zonaItem=  objXMLNode[i].selectSingleNode("ZONAITEM").text
		
		}
		hdatos+="</ITEMS>";
		//Empiezo con las coberturas
	//GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision	
	 if (!impresoRev)
	 {
		hdatos+="<COTIZACIONCOBERTURAS>";
		

		for (i=0;i<objXMLCobertura.length;i++)
 		{
 				hdatos+="<COBERTURA>";				
				hdatos+="<ITEM>"+ objXMLCobertura[i].selectSingleNode("IDITEM").text +"</ITEM>";
				hdatos+="<DESCRIPCION>"+ objXMLCobertura[i].selectSingleNode("COBERDES").text +"</DESCRIPCION>";
				hdatos+="<SUMAASEG>"+ formatoNro(datosCotizacion.getMonedaProducto(),Number(objXMLCobertura[i].selectSingleNode("SUMAASEG").text),0,"") +"</SUMAASEG>";
				hdatos+="<TASA>"+ formatoNroDec("",objXMLCobertura[i].selectSingleNode("TASA").text,2,",") +"</TASA>";
				hdatos+="<PRIMACOMISION>"+ formatoNro(datosCotizacion.getMonedaProducto(),objXMLCobertura[i].selectSingleNode("PRIMA").text,2,",") +"</PRIMACOMISION>";
				
				hdatos+="</COBERTURA>";
				pCoberCod += objXMLCobertura[i].selectSingleNode("COBERCOD").text + "|"
				var zonaCod = objXMLNode[Number(objXMLCobertura[i].selectSingleNode("IDITEM").text)-1].selectSingleNode("ZONAITEM").text + "|"
				pZonaCod +=  zonaCod
		}
		hdatos+="</COTIZACIONCOBERTURAS>";
	}
	
	else
	{
		hdatos+="<COTIZACIONCOBERTURAS>";
		

		for (i=0;i<objXMLCobertura.length;i++)
 		{
 				hdatos+="<COBERTURA>";				
				hdatos+="<ITEM>"+ objXMLCoberturaImpresoRev[i].selectSingleNode("IDITEM").text +"</ITEM>";
				hdatos+="<DESCRIPCION>"+ objXMLCoberturaImpresoRev[i].selectSingleNode("COBERDES").text +"</DESCRIPCION>";
				hdatos+="<SUMAASEG>"+ formatoNro(datosCotizacion.getMonedaProducto(),Number(objXMLCoberturaImpresoRev[i].selectSingleNode("SUMAASEG").text),0,"") +"</SUMAASEG>";
				
				//GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision
				if( objXMLCobertura[i].selectSingleNode("SWTASAVM").text == "F")				
					hdatos+="<TASA>"+ formatoNroDec("",objXMLCoberturaImpresoRev[i].selectSingleNode("TASA").text,2,",") +" (*) " +"</TASA>";
				else
					hdatos+="<TASA>"+ formatoNroDec("",objXMLCoberturaImpresoRev[i].selectSingleNode("TASA").text,2,",") +"</TASA>";
			
				
				hdatos+="<PRIMACOMISION>"+ formatoNro(datosCotizacion.getMonedaProducto(),objXMLCoberturaImpresoRev[i].selectSingleNode("PRIMA").text,2,",") +"</PRIMACOMISION>";
				hdatos+="</COBERTURA>";
				pCoberCod += objXMLCoberturaImpresoRev[i].selectSingleNode("COBERCOD").text + "|"
				var zonaCod = objXMLNode[Number(objXMLCoberturaImpresoRev[i].selectSingleNode("IDITEM").text)-1].selectSingleNode("ZONAITEM").text + "|"
				pZonaCod +=  zonaCod
		}
		hdatos+="</COTIZACIONCOBERTURAS>";
		
	}
		
		var mvarCantCoberturas=pCoberCod.split("|").length-1
		//Empiezo con Leyendas Coberturas
		
		for (var i=0; i<mvarCantCoberturas;i++){
			//alert("Valor de i = " + i)
			//alert("Valor de pCoberCod = "+ pCoberCod)
			//alert(pCoberCod.split("|")[i])
			//pMjeCober+=pCoberCod.split("|")[i]
			if (i==0)
				pMjeCober+=pCoberCod.split("|")[i]+"|"
			else{
				pExiste=false;
				for(var j=0; j<i;j++){
					if (pCoberCod.split("|")[i]==pMjeCober.split("|")[j])
						pExiste=true;
				}
				if (pExiste!=true){
					pMjeCober+=pCoberCod.split("|")[i]+"|"
				}	
			}
		}
		//Agrego los C�digos de los Mjes, Leyendas Beneficios Adicionales, y Leyendas Condiciones Generales. 
		pMjeCober+='998|999'
		pZonaCod += zonaCod + "|" + zonaCod
		//GED 09-08-2010 NUEVO 2100
		objXMLLeyenda= fncMostrarLeyenda(pMjeCober,pZonaCod);
		
		var objXMLCoberLeyenda = objXMLLeyenda.getElementsByTagName('COBERTURA');
		hdatos+="<LEYENDASCOBERTURAS>";
	//	if (objXMLLeyenda.selectSingleNode("//Response/Estado/@resultado").text=="true"){
			for(var i=0; i<objXMLCoberLeyenda.length;i++){
					var mLeyenda="";
					var mCantLeye=objXMLCoberLeyenda[i].selectSingleNode("CANTLEYE").text;
					var objXMLLeyendas =objXMLCoberLeyenda[i].getElementsByTagName("LEYEITEMS/LEYEITEM")
					for (var j=0; j<mCantLeye;j++){					
						mLeyenda+=objXMLLeyendas[j].selectSingleNode("LEYEDESC").text+" "
					}	
					var mCoberCod=objXMLCoberLeyenda[i].selectSingleNode("COBERCOD").text
					var mCoberDes="";				
					if (objXMLItem.selectSingleNode("//COBERTURA[COBERCOD="+mCoberCod+"]/COBERDES"))
						mCoberDes=objXMLItem.selectSingleNode("//COBERTURA[COBERCOD="+mCoberCod+"]/COBERDES").text+" : ";				
					else
						mCoberDes="";
										
					if (mCoberCod=='999'){
						mLeyendaCondGen=replace(mLeyenda.toUpperCase(),"�","�");
						mLeyenda="";
					}
					if (mCoberCod=='998'){
						mLeyendaBenAdic=replace(mLeyenda.toUpperCase(),"�","�");
						mLeyenda="";
					}				
					
					hdatos+="<LEYENDACOBERTURA>"
					hdatos+="<COBERTURA>"+mCoberDes.toUpperCase()+"</COBERTURA>"
					hdatos+="<LEYENDA>"+replace(mLeyenda.toUpperCase(),"�","�")+"</LEYENDA>";
					hdatos+="</LEYENDACOBERTURA>"
			}
			
			//LR 02/11/2010 Defect 167
			if (objXMLCoberLeyenda.length == 0)
			{
				hdatos+="<LEYENDACOBERTURA>"
					hdatos+="<COBERTURA></COBERTURA>"
					hdatos+="<LEYENDA></LEYENDA>";
				hdatos+="</LEYENDACOBERTURA>"
			}
			
		//}
		hdatos+="</LEYENDASCOBERTURAS>";
	
		//Empiezo con Leyendas Beneficios Adicionales
		/*if (mLeyendaBenAdic == "")
		    mLeyendaBenAdic = "DA�OS POR INTENTO DE ROBO HASTA EL 2% DE LA SUMA ASEGURADA DE INCENDIO EDIFICIO"

		if (mLeyendaCondGen == "")
		    mLeyendaCondGen = "COTIZACION VALIDA PARA VIVIENDA DE OCUPACION PERMANENTE"*/

		hdatos += "<LEYENDASBENEFICIOSADICIONALES>"
		hdatos += "<BENEFICIOADICIONAL>" + mLeyendaBenAdic.toUpperCase() + "</BENEFICIOADICIONAL>";
		hdatos += "</LEYENDASBENEFICIOSADICIONALES>"

		//Empiezo con Leyendas CondicionesGenerales
		hdatos += "<LEYENDACONDICIONESGENERALES>"
		hdatos += "<CONDICIONGENERAL>" + mLeyendaCondGen.toUpperCase() + "</CONDICIONGENERAL>";
		hdatos += "</LEYENDACONDICIONESGENERALES>"

		hdatos += "<PREGUNTASITEMS>";
		//window.document.body.insertAdjacentHTML("beforeEnd", "<br>Leyendas<textarea>" + mvarLeyendasPreg + "</textarea>");
		if (mvarLeyendasPreg != "") {
		    objXMLLeyendasPreg.loadXML(mvarLeyendasPreg);

		    for (var i = 0; i < objXMLPreguntas.length; i++) {

		        hdatos += "<PREGUNTAS>";
	
		        hdatos += "<ITEM>" + objXMLPreguntas[i].selectSingleNode("IDITEM").text + "</ITEM>";
		        hdatos += "<PREGUNTA>" + objXMLLeyendasPreg.selectSingleNode("//preguntas/pregunta[@Codigo=" + objXMLPreguntas[i].selectSingleNode("CODPREG").text + "]/leyendas/leyenda[@Item=1]/@Descripcion").text + ": " + objXMLPreguntas[i].selectSingleNode("RESPREG").text + "</PREGUNTA>";
		        hdatos += "</PREGUNTAS>";
		    }
		}
		else {
		    hdatos += "<PREGUNTAS>";
		    hdatos += "<ITEM></ITEM>";
		    hdatos += "<PREGUNTA></PREGUNTA>";
		    hdatos += "</PREGUNTAS>";
		}  
		  
		hdatos += "</PREGUNTASITEMS>";
		
		//--------------------------------------------------------------------------------------------//
		//17/11/2010 LR Mostrar todos los msj de leyendas en el impreso		
		hdatos+="<LEYENDASEGURIDADES>";
		
			var wvarIDItem;
			var wTieneLeyendaSeg = false;
			var ramopcod = document.getElementById("RAMOPCOD").value
			var vCanal = datosCotizacion.getCanalURL();
			var acumSumaAseg = 0;
			var objXMLLeyendas = document.getElementById('xmlMensajesLeyendas').XMLDocument;
			var intZona = parseInt(document.getElementById('CODIGOZONA').value);
			var strProvincia;
			if ((intZona == 1) || (intZona == 2))
			  strProvincia = "capital";
			else
			  strProvincia = "interior";
			
			//Cliclo por item
			for (x=0;x<objXMLNode.length;x++)
	 		{
	 			wvarIDItem = objXMLNode[x].selectSingleNode("ID").text;
				//ciclo por c/cobertura
				acumSumaAseg=0
				for (i=0;i<objXMLCobertura.length;i++)
		 		{
		 			wvarIDCober = objXMLCobertura[i].selectSingleNode("IDITEM").text;
		 			wvarCOBERCOD = objXMLCobertura[i].selectSingleNode("COBERCOD").text
		 			//si esta cobertura corresponde a este item
		 			if (wvarIDItem == wvarIDCober)
		 			{
						if (wvarCOBERCOD == 200 || wvarCOBERCOD == 279 || wvarCOBERCOD == 280)
				             acumSumaAseg +=  parseFloat(objXMLCobertura[i].selectSingleNode("SUMAASEG").text)
		 			}
				}
				//alert("sumaseg "+ acumSumaAseg)
				if (acumSumaAseg > 0)
				{
					var varXmlNodoI = new ActiveXObject("MSXML2.DOMDocument");
					varXmlNodoI = objXMLLeyendas.selectNodes("//" + ramopcod + "/mensaje");
				  
					for (var j = 0; j < varXmlNodoI.length; j++)
					{
					   var intMinimo = varXmlNodoI[j].selectSingleNode(strProvincia+"/minimo").text;
					   var intMaximo =  varXmlNodoI[j].selectSingleNode(strProvincia+"/maximo").text;
						//alert(intMinimo +"-"+ intMaximo)
					   if ((acumSumaAseg >= intMinimo) && (acumSumaAseg <= intMaximo))
					   {//alert("alerta "+ varXmlNodoI[j].selectSingleNode("alerta").text)
								hdatos += "<LEYENDASEGURIDAD>";
									hdatos += "<ITEMLEYE>"+ wvarIDItem +"</ITEMLEYE>";
					    		hdatos += "<LEYENDA>"+ varXmlNodoI[j].selectSingleNode("alerta").text +"</LEYENDA>";
				        hdatos += "</LEYENDASEGURIDAD>";
				        wTieneLeyendaSeg=true;
					   }
					}
				}
			//Termina este item
			}
			
		hdatos+="</LEYENDASEGURIDADES>";
		
		if (!wTieneLeyendaSeg) {
		    hdatos += "<TITULOLEYENDASEGU>"
		    hdatos += "<TITULO></TITULO>"; //LLENAR
		    hdatos += "</TITULOLEYENDASEGU>"
		}
		else {

		    hdatos += "<TITULOLEYENDASEGU>"
		    hdatos += "<TITULO>RECUERDE QUE DEBE CONTAR CON LA/S SIGUIENTE/S MEDIDA/S DE SEGURIDAD PARA PODER ADQUIRIR EL PRODUCTO:</TITULO>";
		    hdatos += "</TITULOLEYENDASEGU>"
		}
		
		//--------------------------------------------------------------------------------------------//
		// Detalle del Precio. 	
		
		hdatos+="<DETALLEDELPRECIO>";
		if (document.getElementById("RAMOPCODSQL").value!="ICB1"){		
			hdatos+="<PRIMA>"+ document.getElementById("tdPrimComi").outerText +"</PRIMA>";
			var vTdPrecPeri = document.getElementById("tdGastCome").outerText +" "+ document.getElementById("tdGastComePorc").outerText;
			hdatos+="<GASTOSCOMERCIALIZACION>"+ vTdPrecPeri +"</GASTOSCOMERCIALIZACION>";
			hdatos+="<GASTOSADMINISTRATIVOS>"+ document.getElementById("tdGastAdm").outerText +"</GASTOSADMINISTRATIVOS>";
			hdatos+="<IMPUESTOS>"+ document.getElementById("tdImpuestos").outerText +"</IMPUESTOS>";
			hdatos+="<SELLADO>"+ document.getElementById("tdSellados").outerText +"</SELLADO>";
			hdatos+="<IVA>"+ document.getElementById("tdIVA").outerText +"</IVA>";
			hdatos+="<IIBB>"+ document.getElementById("tdIIBB").outerText +"</IIBB>";
			var vTdComiProd = document.getElementById("tdComiProd").outerText +" "+ document.getElementById("tdComiProdPorc").outerText;
			hdatos+="<COMISIONPRODUCTOR>"+ vTdComiProd +"</COMISIONPRODUCTOR>";
			var vTdComiOrg = document.getElementById("tdComiOrg").outerText +" "+ document.getElementById("tdComiOrgPorc").outerText;
			hdatos+="<COMISIONORGANIZADOR>"+ vTdComiOrg +"</COMISIONORGANIZADOR>";
			hdatos+="<COMISIONTOTAL>"+ document.getElementById("hComiTotal").value +"</COMISIONTOTAL>";

		}	
		else{	
			
			var comisionTotal=eval(document.getElementById("COMISORICOBB").value)+eval(document.getElementById("COMISPRICOBB").value)
			var comisProd = eval(document.getElementById("PRIMAIMP").value) * eval(document.getElementById("COMISPRICOBB").value)/100;
			var comisOrg = eval(document.getElementById("PRIMAIMP").value) * eval(document.getElementById("COMISORICOBB").value)/100;
			var comisTot = comisProd + comisOrg;
			hdatos+="<PRIMA>"+formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("PRIMAIMP").value,2,",")+"</PRIMA>"			
			hdatos+="<GASTOSCOMERCIALIZACION>"+formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("RECADMIN").value,2,",")+ " ("+document.getElementById("PORCCOMERICOBB").value+" %)</GASTOSCOMERCIALIZACION>"
			hdatos+="<GASTOSADMINISTRATIVOS>"+formatoNro(datosCotizacion.getMonedaProducto(),document.getElementById("DEREMIMP").value,2,",")+"</GASTOSADMINISTRATIVOS>"
			hdatos+="<IMPUESTOS>"+document.getElementById("IMPUEIMP").value+"</IMPUESTOS>"
			hdatos+="<SELLADO/>"
			hdatos+="<IVA/>"
			hdatos+="<IIBB/>"
			hdatos+="<COMISIONPRODUCTOR>"+formatoNro(datosCotizacion.getMonedaProducto(),comisProd,2,",")+" ("+document.getElementById("COMISPRICOBB").value+" %)</COMISIONPRODUCTOR>"
			hdatos+="<COMISIONORGANIZADOR>"+formatoNro(datosCotizacion.getMonedaProducto(),comisOrg,2,",")+" ("+document.getElementById("COMISORICOBB").value+" %)</COMISIONORGANIZADOR>"
			hdatos+="<COMISIONTOTAL>"+formatoNro(datosCotizacion.getMonedaProducto(),comisTot,2,",")+" ("+comisionTotal+" %)</COMISIONTOTAL>"
		}
		hdatos+="</DETALLEDELPRECIO>";
	
	
		hdatos+="<SUMASASEGURADAS>";
		//jc se agrega icobb 
		if (document.getElementById("RAMOPCODSQL").value == "ICB1")
		{
			var vPRECIOFINAL = document.getElementById("divPrecioICOBB").outerText;
		}else{
			var vPRECIOFINAL = document.getElementById("divCotiPRECIO").outerText;
		}	
		hdatos+="<PRECIOFINAL>"+ vPRECIOFINAL +"</PRECIOFINAL>";
		hdatos+="</SUMASASEGURADAS>";
		hdatos+="<DATOSCOTIZACION>";
			var vMONEDA=datosCotizacion.getMonedaProducto()
			if (vMONEDA=="$") vMONEDA="Pesos"
			hdatos += "<MONEDA>" + vMONEDA + "</MONEDA>"
			hdatos += "<TIPOIMPRESO>C</TIPOIMPRESO>"
			var vNROREFERENCIA = document.getElementById("RAMOPCOD").value +'-'+
				fncSetLength(document.getElementById("CERTIPOL").value,4) +'-'+
				fncSetLength(document.getElementById("CERTISEC").value,6);
			hdatos+="<NROREFERENCIA>"+ vNROREFERENCIA +"</NROREFERENCIA>";
			hdatos+="<FECHACOTIZACION>"+ document.getElementById("FECHACOTIZACION").value +"</FECHACOTIZACION>";
			//------------------------------------------------------------------------------------------//
			if(document.getElementById("TIPOOPER").value == "R")
				var wRENOVACIONPOLIZA = document.getElementById("RAMOPCOD").value+'-'+fncSetLength(document.getElementById("POLIZANNANT").value,2)+'-'+fncSetLength(document.getElementById("POLIZSECANT").value,6);
			else
				var wRENOVACIONPOLIZA = "";
			hdatos+="<RENOVACIONPOLIZA>"+ wRENOVACIONPOLIZA +"</RENOVACIONPOLIZA>";
			//------------------------------------------------------------------------------------------//
			if(document.getElementById("TIPOOPER").value == "R")
				var wFECHAFINVIG = document.getElementById("fecVenPol").value	
			else
				var wFECHAFINVIG = fechaVig
					
			hdatos+="<FECHAFINVIG>"+ wFECHAFINVIG +"</FECHAFINVIG>";			
			//------------------------------------------------------------------------------------------//

			if (document.getElementById("chk_circuito").checked  || document.getElementById("divTablaFueraNorma").innerHTML!="")
			{
			    	hdatos += "<ENVIAREV>S</ENVIAREV>";
			    	 //GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision
			    	if(pTipoImpreso == "R" && getCondicionesTecnicasFM())			    	
			  		hdatos +=   "<CONDTECNICASFM>S</CONDTECNICASFM>"
				else
			      		hdatos +=   "<CONDTECNICASFM>N</CONDTECNICASFM>"
			    
			    
			}
			else
			    hdatos += "<ENVIAREV>N</ENVIAREV>";
			    
			
			
		hdatos+="</DATOSCOTIZACION>";
		
		//Cargo un nuevo nodo para RENOVACION DATOS MODIFICADOS
		if(document.getElementById("TIPOOPER").value == "R")
		{
			fncRenovacionCambios("4")
			comparaCoberturasItemsRen()
			hdatos+=oXMLCambios.xml
			
			//��������������������������������������������������������������������������//
			//Se manda una marca para ver si hay o no cambios en la renovacion
			if (oXMLCambios.selectNodes("//CAMBIO").length > 0)
				var vCAMBIO_SN = "S";
			else
				var vCAMBIO_SN = "N";
			
			var vVerCuadroCAMBIOS = "S";
		}
		else
		{
			var vCAMBIO_SN = "N";
			var vVerCuadroCAMBIOS = "N";
		}
		
		hdatos+="<MOSTRARCAMBIOS>";
			hdatos+="<CAMBIOSN>"+ vCAMBIO_SN +"</CAMBIOSN>";
			hdatos+="<VERCAMBIOS>"+ vVerCuadroCAMBIOS +"</VERCAMBIOS>";
		hdatos+="</MOSTRARCAMBIOS>";
		//��������������������������������������������������������������������������//
		//GED 06/07/2011 - CR5	
		if(impresoRev) 	hdatos+=datosCuadroFueraNorma()
	
	
	hdatos+="</COTIZACION_CON>";
	//document.getElementById("hDatos").value = hdatos;
	//GED 27-9-2010 SE ENCODEAN CHARS
	document.getElementById("hDatos").value = encodeEntities(hdatos)
	//window.document.body.insertAdjacentHTML("beforeEnd", "<br>Impresi�n cotizacion<textarea>"+hdatos+"</textarea>");
}
/******************************************************************************************/
function datosCuadroFueraNorma()
{
	var hdatos = ""
	auxXml = new ActiveXObject('MSXML2.DOMDocument'); 	  
	auxXml.async = false;
	auxXml.loadXML(document.getElementById("xmlFueraNorma").value);
	oXMLFueraNorma = auxXml.selectNodes("//ERROR")
	
	var cantFueraNorma = oXMLFueraNorma.length
	var cantTextoFN =0
	var cantItemFN =0
	
	hdatos+="<TEXTOSFUERANORMA>"
	for(var x=0; x<cantFueraNorma; x++)
	{
		if (oXMLFueraNorma[x].selectSingleNode("NROITEM").text == "")
		{
		hdatos+="<TEXTOFN>"
			hdatos+="<ID>"+x+"</ID>"
			hdatos+="<TEXTO>"+  oXMLFueraNorma[x].selectSingleNode("LEYENDAERROR").text   +"</TEXTO>"
		hdatos+="</TEXTOFN>"
		cantTextoFN++
		}
		
	}
	if (document.getElementById("SWSINIES").value == "S" && cantFueraNorma==0 && document.getElementById("TIPOOPER").value=="R")
	{
		hdatos+="<TEXTOFN>"
			hdatos+="<ID>"+cantTextoFN+"</ID>"
			hdatos+="<TEXTO>La p�liza a renovar posee siniestralidad</TEXTO>"
		hdatos+="</TEXTOFN>"
		cantTextoFN++
		
	}
	
/*	if (document.getElementById("chk_circuito").checked)
	{
		hdatos+="<TEXTOFN>"
			hdatos+="<ID>"+cantTextoFN+"</ID>"
			hdatos+="<TEXTO>La presente cotizaci�n fue enviada a revisi�n por decisi�n del productor.</TEXTO>"
		hdatos+="</TEXTOFN>"
		cantTextoFN++		
	}*/
	
	hdatos+="</TEXTOSFUERANORMA>"
	
	hdatos+="<TEXTOREV>"
	if (document.getElementById("chk_circuito").checked)
	{
		hdatos+="<TEXTOER>"
			hdatos+="<ID>1</ID>"
			hdatos+="<TEXTO>La presente cotizaci�n fue enviada a revisi�n por decisi�n del productor.</TEXTO>"
		hdatos+="</TEXTOER>"				
		cantTextoFN++
	}	
	else{
		hdatos+="<TEXTOER>"
			hdatos+="<ID></ID>"
			hdatos+="<TEXTO></TEXTO>"
		hdatos+="</TEXTOER>"				
	
	}
	hdatos+="</TEXTOREV>"
	
	hdatos+="<ITEMSFUERANORMA>"
	for(var x=0; x<cantFueraNorma; x++)
	{   
		if (oXMLFueraNorma[x].selectSingleNode("NROITEM").text != "")
		{
		hdatos+="<CAMPOFN>"
			hdatos+="<NROITEM>"+  oXMLFueraNorma[x].selectSingleNode("NROITEM").text   +"</NROITEM>"
			hdatos+="<UBICACION>"+  oXMLFueraNorma[x].selectSingleNode("UBICACION").text   +"</UBICACION>"
			hdatos+="<LABELCAMPO>"+  oXMLFueraNorma[x].selectSingleNode("LABELCAMPO").text   +"</LABELCAMPO>"
			hdatos+="<LEYENDAERROR>"+  oXMLFueraNorma[x].selectSingleNode("LEYENDAERROR").text   +"</LEYENDAERROR>"
			hdatos+="<CERTISEC>"+  oXMLFueraNorma[x].selectSingleNode("CERTISEC").text   +"</CERTISEC>"			
		hdatos+="</CAMPOFN>"
		cantItemFN++
		}
	}
	hdatos+="</ITEMSFUERANORMA>"
	hdatos+="<MOSTRARFUERANORMA>"
	if (cantItemFN ==0  && cantTextoFN == 0)
		hdatos+="<OCULTACUADROFN>S</OCULTACUADROFN>";
	else
		hdatos+="<OCULTACUADROFN>N</OCULTACUADROFN>";
	if (cantItemFN ==0  )
		hdatos+="<OCULTAITEMFN>S</OCULTAITEMFN>";
	else
		hdatos+="<OCULTAITEMFN>N</OCULTAITEMFN>";
	hdatos+="</MOSTRARFUERANORMA>"
	hdatos+="<MOSTRARENVIOREVISION>"
	if (document.getElementById("chk_circuito").checked)
		hdatos+="<OCULTARER>N</OCULTARER>";
	else
		hdatos+="<OCULTARER>S</OCULTARER>";
		
	hdatos+="</MOSTRARENVIOREVISION>"
	
	
	
	return hdatos
	
	
	
}



function  continuarRevision()
{         
	//GED 01-07-2010 TICKET 632535
	
	var oXMLItems = new ActiveXObject('MSXML2.DOMDocument');        
    	oXMLItems.async = false;       
      	oXMLItems.loadXML(document.getElementById("varXmlItems").value);   
      document.getElementById("varXmlItemsImpresoRev").value =	document.getElementById("varXmlItems").value
      	    
    	var oXMLRegItems = oXMLItems.selectNodes("//ITEM");
	var varCantItems = oXMLRegItems.length;   
		
		for (var x=1; x<=varCantItems; x++)
		{	
			var flagErrorCotizar = false
			if(!fncCotizar2480(x,"R")) 		   		
			   { 
			   	flagErrorCotizar = true
			   	break
			   }
					
						
	 	}
	fncCotizar("R")	
	armaXMLCotizacion("R")
	// Ticket 632535
	var mvarxmlDataSource =document.getElementById("hDatos").value
	
       var mvarReportId = "COTI_ICO_REV"
       var mvarRAMOPCOD = document.getElementById("RAMOPCOD").value
       var mvarPOLIZANN = "00"
       var mvarPOLIZSEC = "000000"
       var mvarCERTIPOL =rightString( "0000" +document.getElementById("CERTIPOL").value,4) //completar con ceros
       var mvarCERTIANN = "0000"
       var mvarCERTISEC = rightString("000000"+document.getElementById("CERTISEC").value,6)
       var mvarSUPLENUM = "0000"
       var mvarTIPODOCU = "CO"
   
	
	 mvarRequest = "<Request>" +
			"<DEFINICION>ismGenerateAndPublishPdfReportLBAVO.xml</DEFINICION>" +
			"<Raiz>share:generatePdfReport</Raiz>"+
			"<xmlDataSource>"+ mvarxmlDataSource + "</xmlDataSource>" +
			"<reportId>"+ mvarReportId + "</reportId>" +
			"<coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/LBAOV/LBACircuitoRevision</coldViewPubNode>" +
			"<coldViewMetadata>RAMOPCOD=" + mvarRAMOPCOD + ";POLIZANN=" + mvarPOLIZANN + ";POLIZSEC=" + mvarPOLIZSEC + ";CERTIPOL=" + mvarCERTIPOL + ";CERTIANN=" + mvarCERTIANN + ";CERTISEC=" + mvarCERTISEC + ";SUPLENUM=" + mvarSUPLENUM + ";TIPODOCU=" + mvarTIPODOCU + "</coldViewMetadata>" +
			"</Request>"
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "request coldview<textarea>"+mvarxmlDataSource+"</textarea>")
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
	document.getElementById("spanEsperando").innerHTML="<b><br/>Su cotizaci�n esta siendo enviada, por favor espere�</b>"	
	  var respuestaMQ=llamadaMensajeMQAsync(mvarRequest,"lbaw_OVMWGen",'resultadoEnvioColdView') 
	
	ocultarPopUpRevision()
	
}
/******************************************************************************************/
function resultadoEnvioColdView(pXMLRespuesta)
{

	xmlResultado = new ActiveXObject("Msxml2.DOMDocument");
	if ( !xmlResultado.loadXML(pXMLRespuesta))
	{
	    //GED 03-02-2011 DEFECT 258
		alert_customizado("En este momento el sistema no se encuentra disponible para realizar la acci�n solicitada, por favor int�ntelo mas tarde. ")
		return false
	}
	
	  if(xmlResultado.selectSingleNode("//Response/Estado/@resultado").text == "true" && !xmlResultado.selectSingleNode("//faultstring"))
	  {
		//TODO:	 envio de mail con texto y asunto
		if(_envioEmailRevision())
		{		
			if(_cambioEstadoRevision())
			{
				document.getElementById("dFormularioCompleto").style.display="none"
				document.getElementById("pantallaCierre").style.display='inline'
				document.getElementById("divPoseeSiniestralidad").innerHTML=""
				document.getElementById("lblPantallaCierre").innerHTML = "La cotizaci�n se ha enviado exitosamente"
				//Apagar botones
				fncApagarBotones();
				//Prender boton nuevacotizacion
				document.getElementById('hNueCotConfirm').value='N';
				document.getElementById("divBtnNuevaCotizacion").style.display = "inline";
				
				
				return true
			}
			else
			{
				alert_customizado("Error cambio de estado. Intente m�s tarde nuevamente")
				return false
			}
		}
		else
		{
				alert_customizado("Error envio de mail. Intente m�s tarde nuevamente")
				return false
		}
		}
		else
		{
		alert_customizado("Ha habido un error en el env�o del impreso. Intente m�s tarde nuevamente ")
		return false
		}
}
/**************************************************************************************************/
function _envioEmailRevision()
{
  var mvarRAMOPCOD = document.getElementById("RAMOPCOD").value
   var mvarPOLIZANN = "00"
   var mvarPOLIZSEC = "000000"
   var mvarCERTIPOL =rightString( "0000" +document.getElementById("CERTIPOL").value,4) //completar con ceros
   var mvarCERTIANN = "0000"
   var mvarCERTISEC = rightString("000000"+document.getElementById("CERTISEC").value,6)
   var mvarSUPLENUM = "0000"
	//cuando continuar con la pantalla de envio a revision
		//TO DO: enviar el mail por mdw
		//mvarEmailCliente= document.getElementById("mailCliente_Para").value		
		//mvarEmailCC=   document.getElementById("mailCliente_Copia").value
	
/*	var mvarRequest = "<Request><DEFINICION>wsSendPdfReport.xml</DEFINICION>" +
        "<files></files>" +
        "<applicationId>LBAVO</applicationId>" +
        "<recipientTO>" +  document.getElementById('revision_para').value + "</recipientTO>" +
        "<recipientsCC></recipientsCC>" +
        "<recipientsBCC></recipientsBCC>" +
        "<templateFileName>LBAVO_COTI_REVISION</templateFileName>"+
        "<parametersTemplate>NROCOTI="+mvarRAMOPCOD+"-" + mvarCERTIPOL + "-" + mvarCERTISEC+",NROPRODUCTOR="+ document.getElementById("USER_AS").value + " ("+ document.getElementById("PERFIL_AS").value + ") "  +"</parametersTemplate>"+
        "<from/><replyTO/><bodyText/><subject/><importance>1</importance><imagePathFile/>"+
        "</Request>"
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVMWSOAPGen') */
	
	// GED 02-08-2010 - Se envia mail por MQ
	
	//GED 01-11-2010 
	var nombreProductor = document.getElementById("USER_AS").value !=""? document.getElementById("USER_AS").value + " ("+ document.getElementById("PERFIL_AS").value + ") ":  document.getElementById("NOMBREUSER").value.replace(/,/g,"")
   
  if (oXMLCambios.selectNodes("//CAMBIO").length > 0)
  {
  	//-----------------------------------------------------------------------------------//
		//20/12/2010 LR Se agrega el cuadro de cambios en la renovacion en el envio por mail al ejecutivo.
		//-----------------------------------------------------------------------------------//
		var mvarxmlDataSource =document.getElementById("hDatos").value;
		
		var mvarRequest = "<Request>"
		mvarRequest += "<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>"
			mvarRequest += "<Raiz>share:generateAndSendPdfReport</Raiz>"
			mvarRequest += "<applicationId>LBAVO</applicationId>"
			mvarRequest += "<xmlDataSource>"+ mvarxmlDataSource +"</xmlDataSource>"
			mvarRequest += "<reportId>CAMBIOS_RENOVACION</reportId>"
			mvarRequest += "<attachPassword></attachPassword>"
			mvarRequest += "<attachFileName>Cambios_en_la_Renovaci�n.pdf</attachFileName>"
			mvarRequest += "<recipientTO>"+ document.getElementById('revision_para').value +"</recipientTO>"
			//mvarRequest += "<recipientTO>leonardo.ruiz@hsbc.com.ar</recipientTO>"
			mvarRequest += "<recipientsCC></recipientsCC>"
			mvarRequest += "<recipientsBCC/>"
			mvarRequest += "<templateFileName>LBAVO_COTI_REVISION_CONCAMBIOS</templateFileName>"
			mvarRequest += "<parametersTemplate>NROCOTI="+mvarRAMOPCOD+"-" + mvarCERTIPOL + "-" + mvarCERTISEC+",NROPRODUCTOR="+ nombreProductor +"</parametersTemplate>"
			mvarRequest += "<from></from>"
			mvarRequest += "<replyTO/>"
			mvarRequest += "<bodyText></bodyText>"
			mvarRequest += "<subject></subject>"
			mvarRequest += "<importance/>"
			mvarRequest += "<imagePathFile></imagePathFile>"
		mvarRequest += "</Request>"
		//-----------------------------------------------------------------------------------//
  }
  else
  {
    // GED 02-08-2010 - Se envia mail por MQ
    var mvarRequest = "<Request>"
		mvarRequest += "<DEFINICION>ismSendPdfReportLBAVO.xml</DEFINICION>"
		mvarRequest += "<Raiz>share:sendPdfReport</Raiz>"
		mvarRequest += "<files/>"
		mvarRequest += "<applicationId>LBAVO</applicationId>"
		mvarRequest += "<reportId></reportId>"
		mvarRequest += "<recipientTO>" +  document.getElementById('revision_para').value + "</recipientTO>"
		mvarRequest += "<recipientsCC/>"
		mvarRequest += "<recipientsBCC/>"
		mvarRequest += "<templateFileName>LBAVO_COTI_REVISION</templateFileName>"
		mvarRequest += "<parametersTemplate>NROCOTI="+mvarRAMOPCOD+"-" + mvarCERTIPOL + "-" + mvarCERTISEC+",NROPRODUCTOR="+  nombreProductor  +"</parametersTemplate>"
		mvarRequest += "<from/>"
		mvarRequest += "<replyTO/>"
		mvarRequest += "<bodyText/>"
		mvarRequest += "<subject></subject>"
		mvarRequest += "<importance/>"
		mvarRequest += "<imagePathFile/>"
		mvarRequest += "<attachPassword/>"
		mvarRequest += "<attachFileName/>"
		mvarRequest += "</Request>"
	}
	//document.body.insertAdjacentHTML("beforeEnd", "mail<textarea>"+ mvarRequest +"</textarea>")
	
	var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVMWGen')
	
	xmlResultado = new ActiveXObject("Msxml2.DOMDocument");
	xmlResultado.loadXML(mvarResponse);
	//document.body.insertAdjacentHTML("beforeEnd", "mail<textarea>"+mvarResponse+"</textarea>")
	if(xmlResultado.selectSingleNode("//Response/Estado/@resultado").text == "true")
		if (xmlResultado.selectSingleNode("//faultstring"))
			return false
		else
			return true
	else
		return false
}
/**************************************************************************************************/
function _cambioEstadoRevision()
{
	//Armo el XML de entrada para llamar al SP
	strData = "FUNCION=CAMBIARESTADO&";
	strData+= "RAMOPCOD=" + document.getElementById("RAMOPCOD").value + "&";
	strData+= "POLIZANN=0&";
	strData+= "POLIZSEC=0&";
	strData+= "CERTIPOL=" + document.getElementById("CERTIPOL").value + "&";
	strData+= "CERTIANN=0&";
	strData+= "CERTISEC=" + document.getElementById("CERTISEC").value + "&";
	strData+= "SUPLENUM=0&";
	strData+= "SITUCPOL=C&";
	strData+= "CIRCUREV=S&";
	strData+= "ANOTACION="+ document.getElementById("revision_comentario").value.replace(/&/g,"~~") + "&";
	strData+= "EJEC_PPLSOFT="+ document.getElementById("EJEC_PPLSOFT").value + "&";
	strData+= "EJEC_APENOM="+ document.getElementById("EJEC_APENOM").value + "&";
	strData+= "EJEC_EMAIL="+ document.getElementById("EJEC_EMAIL").value + "&";
	strData+= "AIS_POLIZANN=0&";
	strData+= "AIS_POLIZSEC=0&";
	strData+= "AIS_CERTIPOL=0&";
	strData+= "AIS_CERTIANN=0&";
	strData+= "AIS_CERTISEC=0&";
	strData+= "AIS_SUPLENUM=0&";
	strData+= "USUARCOD=" + document.getElementById("LOGON_USER").value;
	
	//Llamada
	xmlHTTPCotiza = new ActiveXObject("Msxml2.XMLHTTP");		
	xmlHTTPCotiza.open("POST","DefinicionFrameWork.asp", false);
	xmlHTTPCotiza.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHTTPCotiza.send(encodeURI(strData)); 	
	
	var mvarResult = xmlHTTPCotiza.responseText;
	//window.document.body.insertAdjacentHTML("beforeEnd", mvarResult)
	if (mvarResult=="0")
	{
		return true;
	} else {
		return false;
	}
}
/**************************************************************************************************/
function fncImprimirCotizacion(pMedio)
{
	
	var pvalor = getRadioButtonSelectedValue(document.all.copia);
	var i;
  	var url;
	var formaVentana;	
	armaXMLCotizacion()
	/* Aca tomo la ruta del canal actual para pasarlo en un js */
	vCanal = document.getElementById("CANALURL").value;
	
  	if (pMedio=='I')
  	{document.getElementById("pAccion").value= "IMPRIMIR"
		if(pvalor=="C")
    		{	
    		//Mostrar PDF de Cliente
			// jc se incorpora icobb
    			if (document.getElementById("RAMOPCODSQL").value == "ICB1")
					{document.getElementById("hTipoImp").value = "COTI_ICB_Cliente"
					}else {
    			document.getElementById("hTipoImp").value = "COTI_ICO_Cliente";
				}
			url = "DefinicionFramePopUp.asp";
			formaVentana= "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
			document.all.frmCotizacion.action = url;
		  	ventImpre1=	window.open(vCanal +"Includes/Impresos/cargandoPDF.html","winCliente", formaVentana);
		  	document.all.frmCotizacion.target = "winCliente";
		  	document.all.frmCotizacion.submit();
		}
		if(pvalor=="P")
    		{
			//Mostrar PDF de Productor
			document.getElementById("hTipoImp").value = "COTI_ICO_Productor";
			url = "DefinicionFramePopUp.asp";
			formaVentana= "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
			document.all.frmCotizacion.action = url; 
		 	ventImpre2=	window.open(vCanal +"Includes/Impresos/cargandoPDF.html","winProductor", formaVentana); 
		  	document.all.frmCotizacion.target = "winProductor";
		  	document.all.frmCotizacion.submit();
		}	
		if(pvalor=="A")
    		{	
			//Mostrar PDF de Cliente
    			document.getElementById("hTipoImp").value = "COTI_ICO_Cliente";
			url = "DefinicionFramePopUp.asp";
			formaVentana= "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
			document.all.frmCotizacion.action = url;
		  	ventImpre1=	window.open(vCanal +"Includes/Impresos/cargandoPDF.html","winCliente", formaVentana);
		  	document.all.frmCotizacion.target = "winCliente";
		  	document.all.frmCotizacion.submit();
		  	//Mostrar PDF de Productor
		  	document.getElementById("hTipoImp").value = "COTI_ICO_Productor";	
			url = "DefinicionFramePopUp.asp";
			formaVentana= "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
			document.all.frmCotizacion.action = url; 
		  	ventImpre2=	window.open(vCanal +"Includes/Impresos/cargandoPDF.html","winProductor", formaVentana); 
		  	document.all.frmCotizacion.target = "winProductor";
		  	document.all.frmCotizacion.submit();
		}
	}
	else
	{
		document.getElementById("pAccion").value= "ENVIOMAIL"
		//Limpiar errores
		document.getElementById("spnMensajeErrorMailPara").innerHTML = ""
		document.getElementById("spnMensajeErrorMailCopy").innerHTML = ""
		//Validar EMails
		var wEMailValidos = true;
		if (!validarFormato(document.getElementById('mailCliente_Para'),"email"))
		{
			//	06/10/2010 LR Defect 21 Estetica, pto 6.-
			document.getElementById("spnMensajeErrorMailPara").innerHTML = '<B><a style="color:#000000;font-size:11px;font-weight:bold;font-family:Arial;">Campo Para: '+ leyendaError +'.</a></B>'
			wEMailValidos = false;
		}
		if (document.getElementById('mailCliente_Copia').value.length > 0)
		{
			if (!validarFormato(document.getElementById('mailCliente_Copia'),"email"))
			{
				//	06/10/2010 LR Defect 21 Estetica, pto 6.-
				document.getElementById("spnMensajeErrorMailCopy").innerHTML = '<B><a style="color:#000000;font-size:11px;font-weight:bold;font-family:Arial;">Campo CC: '+ leyendaError +'.</a></B>'
				wEMailValidos = false;
			}
		}
		//Si los Emails estan OK realizo el env�o
		if (wEMailValidos)
		{
			//frmCotizacion.emailCliente.value = pvalor;			
			formaVentana= "height=50,width=300,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
			url = "DefinicionFramePopUp.asp";
			if (fncEsICB1())
			    document.getElementById("hTipoImp").value = "COTI_ICB_Cliente";
			else
	   		    document.getElementById("hTipoImp").value = "COTI_ICO_Cliente";
			//frmCotizacion.hDatos.value = hdatos;		
	    		document.all.frmCotizacion.action = url; 
			ventImpre=	window.open(vCanal +"Includes/Impresos/cargandoPDF.html","newWin", formaVentana); 
			document.all.frmCotizacion.target = "newWin";
			document.all.frmCotizacion.submit();
			
			ocultarPopUp2('popUp_mailCliente'); 
		}
	}
}
/**************************************************************************************************/
function fncImprimirSoli(pMedio)
{
	//01/06/2011 LR - Se agrega funcionalid para mostrar el impreso del Solicitud.-
    var pvalor = getRadioButtonSelectedValue(document.all.copiaSol);
    var i;
    var url;
    var formaVentana;
    armaXMLCotizacion()
    /* Aca tomo la ruta del canal actual para pasarlo en un js */
    vCanal = document.getElementById("CANALURL").value;

    if (pMedio == 'I')
    {
        document.getElementById("pAccion").value = "IMPRIMIR"
        if (pvalor == "S") {
            //Mostrar PDF de Solicitud
    		// jc se incorpora icobb
    		if (document.getElementById("RAMOPCODSQL").value == "ICB1")
				{document.getElementById("hTipoImp").value = "SOLI_ICB"
			}else{
				document.getElementById("hTipoImp").value = "SOLI_ICO";
				}
            url = "DefinicionFramePopUp.asp";
            formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
            document.all.frmCotizacion.action = url;
            ventImpre1 = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winCliente", formaVentana);
            document.all.frmCotizacion.target = "winCliente";
            document.all.frmCotizacion.submit();

            //Autorizaciones
            if (document.getElementById("seccion_Tarjeta").style.display != "none") {

                //Tarjeta de Cr�dito
                document.getElementById("hTipoImp").value = "AUTDEB_TC_ICO";
                url = "DefinicionFramePopUp.asp";
                formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
                ventImpre3a = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winAutDeb", formaVentana);
                document.all.frmCotizacion.target = "winAutDeb";
                document.all.frmCotizacion.submit();
            } else if (document.getElementById("seccion_PagoCuenta").style.display != "none") {
                //D�bito en CA/CC
                document.getElementById("hTipoImp").value = "AUTDEB_CA_CC_ICO";
                url = "DefinicionFramePopUp.asp";
                formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
                ventImpre3b = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winAutDeb", formaVentana);
                document.all.frmCotizacion.target = "winAutDeb";
                document.all.frmCotizacion.submit();
            } else if (document.getElementById("divNroCBU").style.display != "none") {
                //CBU
                document.getElementById("hTipoImp").value = "AUTDEB_CBU_ICO";
                url = "DefinicionFramePopUp.asp";
                formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
                ventImpre3c = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winAutDeb", formaVentana);
                document.all.frmCotizacion.target = "winAutDeb";
                document.all.frmCotizacion.submit();
            }
        }
        if (pvalor == "C") {
            //Mostrar PDF de Productor
           	// jc se incorpora icobb
    		if (document.getElementById("RAMOPCODSQL").value == "ICB1")
				{document.getElementById("hTipoImp").value = "CERT_PROV_ICB"
			}else{
				document.getElementById("hTipoImp").value = "CERT_PROV_ICO";
				}
            url = "DefinicionFramePopUp.asp";
            formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
            document.all.frmCotizacion.action = url;
            ventImpre2 = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winProductor", formaVentana);
            document.all.frmCotizacion.target = "winProductor";
            document.all.frmCotizacion.submit();
        }

        if (pvalor == "A") {

            //Mostrar PDF de Solicitud
           	// jc se incorpora icobb
    		if (document.getElementById("RAMOPCODSQL").value == "ICB1")
				{document.getElementById("hTipoImp").value = "SOLI_ICB"
			}else{
				document.getElementById("hTipoImp").value = "SOLI_ICO";
            }
			url = "DefinicionFramePopUp.asp";
            formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
            document.all.frmCotizacion.action = url;
            ventImpre1 = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winCliente", formaVentana);
            document.all.frmCotizacion.target = "winCliente";
            document.all.frmCotizacion.submit();

            //Autorizaciones
            if (document.getElementById("seccion_Tarjeta").style.display != "none") {

                //Tarjeta de Cr�dito
                document.getElementById("hTipoImp").value = "AUTDEB_TC_ICO";
                url = "DefinicionFramePopUp.asp";
                formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
                ventImpre3a = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winAutDeb", formaVentana);
                document.all.frmCotizacion.target = "winAutDeb";
                document.all.frmCotizacion.submit();
            } else if (document.getElementById("seccion_PagoCuenta").style.display != "none") {
                //D�bito en CA/CC
                document.getElementById("hTipoImp").value = "AUTDEB_CA_CC_ICO";
                url = "DefinicionFramePopUp.asp";
                formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
                ventImpre3b = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winAutDeb", formaVentana);
                document.all.frmCotizacion.target = "winAutDeb";
                document.all.frmCotizacion.submit();
            } else if (document.getElementById("divNroCBU").style.display != "none") {
                //CBU
                document.getElementById("hTipoImp").value = "AUTDEB_CBU_ICO";
                url = "DefinicionFramePopUp.asp";
                formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
                ventImpre3c = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winAutDeb", formaVentana);
                document.all.frmCotizacion.target = "winAutDeb";
                document.all.frmCotizacion.submit();
            }

            //Mostrar PDF de Productor
            // jc se incorpora icobb
    		if (document.getElementById("RAMOPCODSQL").value == "ICB1")
				{document.getElementById("hTipoImp").value = "CERT_PROV_ICB"
			}else{
				document.getElementById("hTipoImp").value = "CERT_PROV_ICO";
            }
			url = "DefinicionFramePopUp.asp";
            formaVentana = "height=550,width=750,scrollbars=1,toolbar=0,location=0,status=1,menubar=0,resizable=0";
            document.all.frmCotizacion.action = url;
            ventImpre2 = window.open(vCanal + "Includes/Impresos/cargandoPDF.html", "winProductor", formaVentana);
            document.all.frmCotizacion.target = "winProductor";
            document.all.frmCotizacion.submit();

        }
         
    }
}
//------------------------------------------------------------------------------------------
function imprimirCertificado()
{
	//01/06/2011 LR - Se agrega funcionalid para mostrar el impreso del Solicitud.-
	//Fondo de pantalla
  var ancho = document.body.clientWidth
  var body = document.body, html = document.documentElement;
  var alto = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
  document.getElementById('popUp_opacidadOutForm').style.width = ancho
  document.getElementById('popUp_opacidadOutForm').style.height = alto
  document.getElementById('popUp_opacidadOutForm').style.display = 'inline';
  //opacity('popUp_opacidadOutForm', 0, 10, 300);
  // Fin Fondo
  document.getElementById('popUp_ImpresionSoli').style.left = 200
  document.getElementById('popUp_ImpresionSoli').style.top = body.scrollTop + 100
  document.getElementById('popUp_ImpresionSoli').style.display = 'inline';
}
/**************************************************************************************************/
function fncMostrarLeyenda(pCoberCod,pZonaItem)
{
	objXMLLeyendas = new ActiveXObject("Msxml2.DOMDocument");
	objXMLCoberturas = new ActiveXObject("Msxml2.DOMDocument");
	objXMLCoberturas.loadXML("<COBERTURAS/>")
	
	
	
	var mCantCoberturas=pCoberCod.split("|").length;
	
	
	//varOElement = varXml.createElement("ITEM");
	for(var i=0;i<mCantCoberturas;i++)
	{
 			var mvarRequest="<Request>"+
			"<DEFINICION>2100_LeyendasxProdActCobertura.xml</DEFINICION>"+
			"<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
			"<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
			"<PROFECOD>"+document.getElementById('cboActividadComercio').value.split("|")[0]+"</PROFECOD>"+
			"<CODIZONA>"+  rightString("0000" + pZonaItem.split("|")[i],4 )+"</CODIZONA>"+
			"<COBERCOD>" + pCoberCod.split("|")[i] + "</COBERCOD>"+
			"</Request>"
	
			var mvarResponse= llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')
			objXMLLeyendas.loadXML(mvarResponse)
			
			var leyendasxcob = objXMLLeyendas.selectNodes("//COBERTURA").item(0)
			
			//var varONodo = objXMLCoberturas.createElement("COBERTURA");
			if (objXMLLeyendas.selectNodes("//COBERTURA").length != 0) 			
				objXMLCoberturas.selectSingleNode("//COBERTURAS").appendChild(leyendasxcob)
	
	}
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+"LlamadaMjeLeyendas"+llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ')+"</textarea>")
	//window.document.body.insertAdjacentHTML("beforeEnd", "mvarRequestLeyendas<textarea>"+objXMLCoberturas.xml+"</textarea>")
	return objXMLCoberturas
}
/**************************************************************************/
function fncMensajesLeyendas(){
    var ramopcod = document.getElementById("RAMOPCOD").value
    var strProvincia;
    var vCanal = datosCotizacion.getCanalURL();
    var totalCoberturas = document.all.COBCHECK ? document.all.COBCHECK.length : 0;
    var acumSumaAseg = 0;

    var objXMLLeyendas = document.getElementById('xmlMensajesLeyendas').XMLDocument;
	
	var intZona = parseInt(document.getElementById('CODIGOZONA').value);
		
	if ((intZona == 1) || (intZona == 2))
	    strProvincia = "capital";
	 else
	    strProvincia = "interior";
	
	 for (var i = 0; i < totalCoberturas; i++) {
	     if (document.all.COBCHECK[i].checked) {
	         if (document.all.COBCHECK[i].cobercod == 200 || document.all.COBCHECK[i].cobercod == 279 || document.all.COBCHECK[i].cobercod == 280)
	             acumSumaAseg +=  parseFloat(document.getElementById("SUMAASEG" + i).value)
	             //alert( document.all.COBCHECK[i].cobercod +" acumulado:"+ acumSumaAseg)
	     }
	 }
	
	 if (acumSumaAseg > 0) {
	     var varXmlNodo = new ActiveXObject("MSXML2.DOMDocument");
	   //GED 02-11-2010 - DEFECT 234
	    varXmlNodo = objXMLLeyendas.selectNodes("//" + ramopcod + "/mensaje");
	   window.scrollTo(0)
	     for (var j = 0; j < varXmlNodo.length; j++) {
	     
	         var intMinimo = varXmlNodo[j].selectSingleNode(strProvincia+"/minimo").text;
	         var intMaximo =  varXmlNodo[j].selectSingleNode(strProvincia+"/maximo").text;

	         if ((acumSumaAseg >= intMinimo) && (acumSumaAseg <= intMaximo)) {
	         	
	             alert_customizado("Recuerde informarle al cliente que debe contar con la siguiente/s medida/s de seguridad para poder adquirir el producto: </br></br><span style='color:black; '>" + varXmlNodo[j].selectSingleNode("alerta").text)+"</span>";
	             document.getElementById('mensajeAlerta').value = varXmlNodo[j].selectSingleNode("alerta").text;
	         }
	     }
	 }
	 else
	     document.getElementById('mensajeAlerta').value = "";

     
}
/**************************************************************************************/

//GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision

function getCondicionesTecnicasFM()
{
	
	  var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	        objXMLItem.async = false; 
	        objXMLItem.loadXML(document.getElementById("varXmlItemsImpresoRev").value);
	        
	        var oXMLRegItems = objXMLItem.selectNodes("//ITEM");
	        var varCantItems = oXMLRegItems.length;
	
		var condTecnicasFM = objXMLItem.selectNodes("//COBERTURA[SWTASAVM='F']").length
		if (condTecnicasFM > 0) return true
		
	else
		return false
	
	
}
