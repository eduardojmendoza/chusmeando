/*
'-------------------------------------------------------------------------------
' Fecha de Modificaci�n: 08/02/2012
' PPCR: 2011-00056
' Desarrollador: Mart�n Cabrera
' Descripci�n: Se agrega producto ICB1
'--------------------------------------------------------------------------------
' Fecha de Modificaci�n: 01/07/2011
' Ticket 632535
' Desarrolador: Gabriel D'Agnone
' Descripci�n: MOSTRAR PRIMAS Y TASAS EN IMPRESO A REVISION
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 12/04/2011
 PPCR: 50055/6010661
 Desarrolador: Gabriel D'Agnone
 Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 03/02/2011
 PPCR: 50055/6010661
 Desarrolador: Gabriel D'Agnone
 Descripci�n: Se corrigi� DEFECT 254
--------------------------------------------------------------------------------
 Fecha de Modificaci�n: 26/01/2011
 Ticket: 577743
 Desarrolador: Elizabeth Gregorio
 Descripci�n: Se corrige cantidad de alumnos que no este en cero y carteles para ICO
--------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: funcionesItems.js

Fecha de Creaci�n: 09/09/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripci�n: Funciones para manejar los items
--------------------------------------------------------------------------------
*/
var muestraLeyendas = true
var nroUnicoItem = 0
var xmlItemNroMaximo 
//Agrega items a la lista
function agregarItemXMLOT(pUrlCanal,pEstado)
{
	
	nroUnicoItem++
	var varOElement;
	var varONodo;
	var varONodoCob;
	var varOAttr;

		if (xmlItemNro > xmlItemNroMaximo) 
	
	{
		alert("No se pueden agregar m�s Items al listado.");
		return;
	}
	
	itemActualEditado= true;
	
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	//Agrego el ID del elemento 
	varOElement = varXml.createElement('ITEM');
	
	varOAttr = varXml.createAttribute('ID');
	varOAttr.value = xmlItemNro;
	varOElement.setAttributeNode(varOAttr);
	
	varONodo = varXml.createElement('ITEMDESC');
	//varONodo.text  =  document.getElementById("localidadRiesgo").value+"-" +nroUnicoItem;
	varONodo.text  =  document.getElementById("localidadRiesgo").value
	varOElement.appendChild(varONodo);
	
	varONodo = varXml.createElement('ZONAITEM');
	varONodo.text  =  document.getElementById("CODIGOZONA").value;
	varOElement.appendChild(varONodo);
	
	varONodo = varXml.createElement("DOMICDOM");
	if(document.getElementById("calleRiesgoItem"))
		varONodo.text =document.getElementById("calleRiesgoItem").value ;
	else
		varONodo.text ="" 
	
	varOElement.appendChild(varONodo);
	
	varONodo = varXml.createElement("DOMICDNU");
	if(document.getElementById("nroCalleRiesgoItem"))
		varONodo.text =document.getElementById("nroCalleRiesgoItem").value ;
	else
		varONodo.text ="" 
		
	varOElement.appendChild(varONodo);	
	
	varONodo = varXml.createElement("DOMICPIS");
	if(document.getElementById("pisoRiesgoItem"))
		varONodo.text =document.getElementById("pisoRiesgoItem").value ;
	else
		varONodo.text ="" 
	
	varOElement.appendChild(varONodo);
	
	varONodo = varXml.createElement("DOMICPTA");
	if(document.getElementById("dptoRiesgoItem"))
		varONodo.text =document.getElementById("dptoRiesgoItem").value ;
	else
		varONodo.text ="" 
	
	varOElement.appendChild(varONodo);
	
/*	varONodo = varXml.createElement("CANTALUM");
	if(document.getElementById("cantAlumnosCarteles"))
		varONodo.text =document.getElementById("cantAlumnosCarteles").value 
	else
		varONodo.text ="" 
		
			varOElement.appendChild(varONodo);*/
//FASE 2

		//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
		varONodo = varXml.createElement("IDISYUNT");
		varONodo.text = document.getElementById("hIDISYUNT").value 
		varOElement.appendChild(varONodo);
		document.getElementById("hIDISYUNT").value ="N"

		varONodo = varXml.createElement("CERTIPOLANT");
		varONodo.text = document.getElementById("spCertipolAnt").innerHTML 
		varOElement.appendChild(varONodo);
		document.getElementById("spCertipolAnt").innerHTML =""
			
		varONodo = varXml.createElement("CERTIANNANT");
		varONodo.text = document.getElementById("spCertiannAnt").innerHTML 
		varOElement.appendChild(varONodo);
		document.getElementById("spCertiannAnt").innerHTML =""
			
		varONodo = varXml.createElement("CERTISECANT");
		varONodo.text = document.getElementById("spCertisecAnt").innerHTML 
		varOElement.appendChild(varONodo);
		document.getElementById("spCertisecAnt").innerHTML =""
		
		document.getElementById("spCertificado").style.display = "none"

		document.getElementById("CBO_PROVINCIA").disabled= false 	
		document.getElementById("localidadRiesgo").disabled= false 
		//GED 03-02-2011 DEFECT 254
		document.getElementById("botonBuscarLocalidad").disabled= false 
		document.getElementById("codPostalRiesgo").disabled= false 
	

	varONodo = varXml.createElement("ESTADO");
	
	varONodo.text = pEstado;
	varOElement.appendChild(varONodo);
	
	
	varONodo = varXml.createElement("PROVICODDES");

	if (document.getElementById("CBO_PROVINCIA").value!="-1")
	{
		var provicodDes= (document.getElementById("CBO_PROVINCIA").options[document.getElementById("CBO_PROVINCIA").selectedIndex].text).toUpperCase();
		varONodo.text = provicodDes
		
	} 
	else 
	{
		varONodo.text = "";
	}
	varOElement.appendChild(varONodo);	
	//Recorre la solapa insertando los campos como elementos del xml
	for (i=0;i<document.getElementById("divDatosRiesgo_Otro").all.length;i++)
	{
		var varObj;
		var varCampo;
		varObj = document.getElementById("divDatosRiesgo_Otro").all[i];
		if (varObj.vaEnXml!=null) {
			varONodo = varXml.createElement(varObj.vaEnXml);
			
			//si el campo es numerico y esta vacio, lo reemplaza por 0.00
			if (varObj.value=='' && varObj.porDefecto!=null) varObj.value = varObj.porDefecto;			
			varONodo.text = varObj.value;
			
			//varObj.value = '';
			varOElement.appendChild(varONodo);
		
		}
	}
	
	document.getElementById("varItemActual").value=xmlItemNro
	
		//GED 30-8-2010: Defect 36
	if (xmlItemNro > xmlItemNroMaximo-1) 
	{
		alert_customizado("Se ha agregado el �ltimo item, no se podr�n agregar nuevos.");
		
	}
	
	
	//Actualiza el nro de item en la p�gina
	document.getElementById("OTID").value = ++xmlItemNro;
	
	getCoberturas(varXml, varOElement);
	
	getPreguntas(varXml, varOElement)

	varXml.selectSingleNode("//ITEMS").appendChild(varOElement);

	var proviCod=document.getElementById("CBO_PROVINCIA").value 

	var domicPob=document.getElementById("localidadRiesgo").value
	
	if(proviCod !="1")
	{
		datosCotizacion.mostrarCampo("codPostalRiesgo")
		document.getElementById("botonBuscarLocalidad").style.display="inline"	
	}
		
	//Actualiza el hidden que contiene el XML de items en la p�gina
	document.getElementById("varXmlItems").value = varXml.xml;

	if (pEstado == "FN" && !fncEsICB1()) setPrimasEnCero(xmlItemNro - 1)
	
	//Recupero los Objetos Asegurados si los hubiera
	fncXMLObjAseAgregar();

	//restaura la tabla din�mica a su estado original...
	actualizarListadoXML(pUrlCanal);	

	//PARA RENOVACION CUANDO SE AGREGA UN ITEM NUEVO 
	  if (document.getElementById("TIPOOPER").value == "R") 
	  {		//alert("entra")
			var nroSolapa = 2	
			var pNroitem = xmlItemNro-1
			if(!existeItemXMLRen(pNroitem))
			{	//alert(pNroitem)
				insertarXMLCambios(pNroitem,document.getElementById("localidadRiesgo").value,nroSolapa, "ITEM|ITEM"+pNroitem, "", "", "A")
			}
			/*else
				{
					//sacar la baja que se introdujo en quitar
					eliminarXMLCambios("BAJAITEM"+pNroitem)
				}*/
	
		//document.getElementById("seccion_Preguntas").style.display = "inline"
		document.getElementById("cboTipoAlarma").disabled= false 
		document.getElementById("cboTipoGuardia").disabled= false 
		document.getElementById("seccion_Preguntas").style.display = "inline"
	}
	
	if (muestraLeyendas)	fncMensajesLeyendas();
	
	muestraLeyendas = true
	inicializaCoberturas()	
	//GED 01-09-2010 DEFECT 45
	//if(document.getElementById("RAMOPCODSQL").value!="ICB1"){
	document.getElementById("verZona").innerText = ""
	
	document.getElementById('divSeccionCoberturas').innerHTML=""
	document.getElementById('botonDeducibles').style.display="none"
	document.getElementById('divBotonAgregarItem').style.display = "none"
	document.getElementById('divBotonEditarItem').style.display = "none"
	
	
	ocultaPreguntas()

    document.getElementById("CBO_PROVINCIA").value = -1;
	document.getElementById("codPostalRiesgo").value = ""
	document.getElementById("localidadRiesgo").value = ""
	document.getElementById("cboTipoAlarma").value = 3
	document.getElementById("cboTipoGuardia").value = 3
	
	document.getElementById("calleRiesgoItem").value =""
	document.getElementById("nroCalleRiesgoItem").value =""
	document.getElementById("pisoRiesgoItem").value = ""
	document.getElementById("dptoRiesgoItem").value = ""
	//}	

	
	//Pone variable en false para que vuelva a parsear cuando se inserta un item
	
	mvarItemsParseados=false		
	
	//datosCotizacion.setCotizar(true)
	
	
	
	varXml = null;		
	fncCargarRiesgoSolicitud()
	
	//Habilitar para Renovaci�n
	if (document.getElementById("TIPOOPER").value == "R")
	{
		//LR 02/05/2011 Se habilitan Coberturas habilit para el Alta
		fncHabilitarCoberturasDisp('A');
	}
	
}

function actualizarXMLItems(pXMLValores,pItemNro,pXmlImpresoRev)
{
	var wEnc = false
	var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	var objXMLValores = new ActiveXObject("Msxml2.DOMDocument");
	
	if(pXmlImpresoRev != undefined)
	{
	 	if(pXmlImpresoRev == "R")
	 		objXMLItem.loadXML(document.getElementById("varXmlItemsImpresoRev").value);
			
	}
	else
		objXMLItem.loadXML(document.getElementById("varXmlItems").value);
		
	
	
	objXMLValores.loadXML(pXMLValores)
	
	var objXMLCobertura = objXMLItem.getElementsByTagName('COBERTURA');
	var objXMLCobVal = objXMLValores.getElementsByTagName('COBERTURA');
	for (var i=0;i<objXMLCobertura.length;i++)
	{
		if (objXMLCobertura[i].selectSingleNode("IDITEM").text==pItemNro) 		
		{	
			if (objXMLValores.selectSingleNode("//Response/CAMPOS/CANTRESU"))
			{
				var cant=objXMLValores.selectSingleNode("//Response/CAMPOS/CANTRESU").text
				wEnc = false
				for (j=0;j<cant;j++)
				{
					if (objXMLCobVal[j].selectSingleNode("COBERCOD"))
					{
						if (objXMLCobVal[j].selectSingleNode("COBERCOD").text == objXMLCobertura[i].selectSingleNode("COBERCOD").text)
						{
							objXMLCobertura[i].selectSingleNode("PRIMA").text=objXMLCobVal[j].selectSingleNode("PRIMAIMP").text
							objXMLCobertura[i].selectSingleNode("TASA").text=objXMLCobVal[j].selectSingleNode("TARIFPOR").text
							//Solo para Renovaciones (2480)
							if (objXMLCobVal[j].selectSingleNode("SWTASAVM"))
								objXMLCobertura[i].selectSingleNode("SWTASAVM").text=objXMLCobVal[j].selectSingleNode("SWTASAVM").text
							wEnc = true
						}
					} else {
						return false
					}
				}
				//Si no encuentra una cobertura q viene de la coti mandada "Error"
				if(!wEnc)
				{
					return false
				}
			} else {
				alert_customizado("En estos momento el servicio no se encuentra disponible.")
				return false
			}
		}
	}
	
	
	//GED 01-07-2011 - Ticket 632535 - Mostrar Primas y Tasas en Impreso a Revision
	
	if(pXmlImpresoRev != undefined)
	{
	 	if(pXmlImpresoRev == "R")
			document.getElementById("varXmlItemsImpresoRev").value=objXMLItem.xml
	}
	else
		document.getElementById("varXmlItems").value=objXMLItem.xml
	
	
	return true
}





/* ************************************************************************* */
function actualizarListadoXML(pUrlCanal)
{	
	var varXml;
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	
	var totalCoberturas =  document.all.COBCHECK? document.all.COBCHECK.length:0;
	var sumaAsegurada=0
		for(var x=0; x<totalCoberturas;x++)		
		{	
			if(document.all.COBCHECK[x].checked==true)				
				sumaAsegurada += Number(document.getElementById("SUMAASEG" +eval(x)).value)
		
		}
	
	

	//Actualizacion de operacion realizada...
	if (varXml.selectSingleNode("//TIPOENDOSO")==null)
	{
		var varONodo = varXml.createElement("TIPOENDOSO");
		varONodo.text = '';
		varXml.selectSingleNode("//ITEMS").appendChild(varONodo);
	}	
	if (document.getElementById("TIPOENDOSO")!=null)
	{
		varXml.selectSingleNode("//TIPOENDOSO").text = document.getElementById("TIPOENDOSO").value;
	} else {
		varXml.selectSingleNode("//TIPOENDOSO").text = '';
	}	
	//Renumerar el xml...
	var varCantElementos = varXml.selectNodes("//ITEM").length;
	var k;	
	for (k=1; k<=varCantElementos; k++) 	
	{
		varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("@ID").value=k;
		varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("ID").text=k;
		
		
	//nuev	
		if(document.getElementById("calleRiesgoItem_" +k))
		{
			if (document.getElementById("calleRiesgoItem_" +k).value!="")
			{
			 varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("DOMICDOM").text = document.getElementById("calleRiesgoItem_" +k).value ;
			}
		}
		
		
		if(document.getElementById("nroCalleRiesgoItem_" +k))
			if(document.getElementById("nroCalleRiesgoItem_" +k).value !="")
				varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("DOMICDNU").text =document.getElementById("nroCalleRiesgoItem_" +k).value ;
				
		if (varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("COBERTURAS") != null)
		{
			var varCantCob = varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("COBERTURAS").selectNodes("COBERTURA").length;	
			for (j=1; j<=varCantCob; j++) 	
			{
				varXml.selectNodes("//ITEM").item(k-1).selectSingleNode("COBERTURAS").selectNodes("COBERTURA").item(j-1).selectSingleNode("IDITEM").text=k;
				
			}
		}
	
	}
	
	
	
	document.getElementById("varXmlItems").value = varXml.xml;	
	
	var xslParam="<parametros>"+
	"<parametro>"+
		"<nombre>urlCanal</nombre><valor>"+pUrlCanal+"</valor>"+
	"</parametro>"+
	"<parametro>"+
		"<nombre>tipoOperacion</nombre><valor>"+ document.getElementById("TIPOOPER").value +"</valor>"+
	"</parametro>"+
	"<parametro>"+
		"<nombre>sumaAsegurada</nombre><valor>"+sumaAsegurada+"</valor>"+
	"</parametro>"+
	"<parametro>"+
		"<nombre>moneda</nombre><valor>"+datosCotizacion.getMonedaProducto()+"</valor>"+
	"</parametro>"+
	"</parametros>"			
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+varXml.xml+"</textarea>")			
	var varResultTabla = parseaXSLParam(varXml.xml,document.getElementById("XSLOtros"),xslParam)				
	divItemsCargadosOtros.innerHTML = varResultTabla;	
	xmlItemNro = varCantElementos + 1;	
	var varItemNro = "OTID";
	document.getElementById(varItemNro).value = xmlItemNro;
	varXml = null;	
}

/***************************************************************************/
function validarItemsVacios()
{
	//alert(document.getElementById("CBO_PROVINCIA").value)
var provincia= document.getElementById("CBO_PROVINCIA").value=="" ||document.getElementById("CBO_PROVINCIA").value=="-1" ?true:false

var localidad= document.getElementById("localidadRiesgo").value==""?true:false

var alarma= document.getElementById("cboTipoAlarma").value=="3"?true:false

var guardia = document.getElementById("cboTipoGuardia").value=="3"?true:false

//alert(provincia +" ** "+document.getElementById("CBO_PROVINCIA").value)
//alert(localidad +"--"+document.getElementById("localidadRiesgo").value)
//alert(torre +"--"+document.getElementById("NOMBRE_TORRE").value)
//alert(ascensores +"--"+document.getElementById("CANT_ASCENSORES").value)
//alert(document.getElementById("cboTipoAlarma").value)

if (!fncEsICB1()){
	
	if(!provincia || !localidad ) 
	{
		alert_customizado("No se puede modificar, debe agregar antes el item")
		return false
	}
}	
/*if( !alarma || !guardia)
	{	
	if(!validarFormulario('xmlValidacionIndividual', "//OTROS", 'XSLTablaErrores', false) ) 	
		return false
	
	}	*/

if( provincia || localidad||alarma )
	{
	//alert("algun dato = vacio ")
	return true
	} 
	else
	{ 	//alert("entro validaInsertaItemRiesgo")
	if(!validaInsertaItemRiesgo())
		{
		//alert("no valido bien validaInsertaItemRiesgo()")
		return false
		}
	//alert("Si valido bien validaInsertaItemRiesgo()")
	return true
}

//alert("llego al final")
return false
	
}


function validarDatosXML(pXPath)
{ 
	var oResultado = true;
	
	var pvarEError = "";
	
	
	
	oResultado = validarFormulario('xmlValidacionIndividual', pXPath, 'XSLTablaErrores', false) && oResultado;
	
	if (!oResultado)
	{
		var oTexto;
		try{
			oTexto = new String(document.getElementById("alertText").value);
		}catch(E){
			oTexto = new String("");
		}
		var myRETab = new RegExp("#TAB#","gi");
		var myREEnter = /#ENTER#/g;
		oTexto = oTexto.replace(myRETab,"\t");
		oTexto = oTexto.replace(myREEnter,"\n");
		var oTexto1 = new String(pvarEError);
		oTexto1 = oTexto1.replace(myRETab,"\t");
		oTexto1 = oTexto1.replace(myREEnter,"\n");
		//alert('No es posible agregar al listado por el o los siguientes motivos: \n\n' + oTexto + oTexto1);
		return false;
	}
	
		if (getCantCoberturasChecked() < 1)
		{
			alert('No es posible agregar al listado por el o los siguientes motivos: \n\n - Debe ingresar al menos una cobertura para el item.');
			return false;
		}
	
	
	return oResultado;
}

function getCantCoberturasPpal(pId)
{
	var cantCoberturas = 0;
	 var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	 objXMLItem.async = false; 
	 objXMLItem.loadXML(document.getElementById("xmlCoberturas").innerHTML);
	 
	 var oXMLRegCobs =  objXMLItem.selectNodes("//COBERTURA");
	var varCantCobs = oXMLRegCobs.length;        
					
       var varXmlItems = new ActiveXObject("MSXML2.DOMDocument");	
	varXmlItems.async = false;				
	varXmlItems.loadXML(document.getElementById("varXmlItems").value)	
			
	for (var y=0; y<varCantCobs; y++)
	{
			 		
			 	if(oXMLRegCobs[y].selectSingleNode("COBERPRI").text == "0" )
			 	{	
			 		var coberCod = oXMLRegCobs[y].selectSingleNode("COBERCOD").text
			 		
			 		if( varXmlItems.selectSingleNode("//ITEMS/ITEM[ID='"+ pId  +"']/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")	)		   						 		
			 			cantCoberturas++;
			 		
			 	}
	}
			 
	return cantCoberturas
}

/* ************************************************************************* */
function getCantCoberturasChecked()
{
	var cantCoberturas = 0;
	var totalCoberturas = document.all.COBCHECK?document.all.COBCHECK.length:0
	
	
		for(var i=0; i<totalCoberturas;i++)
		{
			//15/06/2011 LR - La cobertura checkeada tambien debe estar visible o habilitada.-
			varCOBERCOD = document.all.COBCHECK[i].cobercod;
			if (document.all.COBCHECK[i].checked && document.all.COBCHECK[i].codigoCoberturaPrincipal =="0" && document.getElementById("FILA"+ varCOBERCOD).style.display!='none')
			{
				cantCoberturas++;
			}
		}
	
	return cantCoberturas;
}
/* ************************************************************************* */
function cargaCoberturas()
{
	var cantCoberturas = 0;
	var totalCoberturas =  document.all.COBCHECK? document.all.COBCHECK.length:0;
	
	
		for(var x=0; x<totalCoberturas;x++)		
		{	
			if(!document.all.COBCHECK[x].checked)
				document.getElementById("SUMAASEG"+x).disabled=true
		     for(var y=0; y<totalCoberturas;y++)		     
			if (document.all.COBCHECK[y].cobercod == document.all.COBCHECK[x].codigoCoberturaPrincipal)
			{			
				if (document.all.COBCHECK[y].obligatoria=="N")
				document.all.COBCHECK[x].disabled=true
			}
		}
	
	
}

//se utiliza para mostrar el resultaado de las preguntas cunado se carga o edita un item
function fncMuestraPreguntas(pxml,pid)
{
	//document.body.insertAdjacentHTML("beforeEnd", "XMLPREGUNTAS<textarea>"+ pxml.xml+"</textarea>")	
	
 		var oXMLPreg = pxml.selectNodes("//ITEM[@ID="+pid+"]/PREGUNTAS/PREGUNTA");
      		var varCantPreg = oXMLPreg.length;   
		var totalPreguntas = 	document.all.PREGUNTA? document.all.PREGUNTA.length:0;	// document.all.COBCHECK.length;
	//alert(varCantPreg)
	
	for(var x=0; x<varCantPreg;x++)
	{
		for(var y=1;y<totalPreguntas;y++)
		{
		
			if (document.all.PREGUNTA[y].codigoAIS == oXMLPreg[x].selectSingleNode("CODPREG").text ){
				//alert("Item:"+ pid +" Codigo: "+ oXMLPreg[x].selectSingleNode("CODPREG").text +"  " +oXMLPreg[x].selectSingleNode("RESPREG").text)
				document.getElementById("DIVPREGUNTA-"+eval(y)).style.display = "inline"
				document.getElementById("PREGUNTA-"+eval(y)).value = oXMLPreg[x].selectSingleNode("RESPREG").text 
				
			}
		}
		
	}		
//	var totalPreguntas = 	document.all.PREGUNTA? document.all.PREGUNTA.length:0;	// document.all.COBCHECK.length;
	
	//var i=1
	//document.getElementById("DIVPREGUNTA-"+eval(i)).style.display != "inline"
}


function getPreguntas(pVarXml, pVarOElement) 
{
		
	//Genera un elemento para las coberturas y recorre la tabla agregando aquellas que tengan PREGUNTA en true
	varONodo = pVarXml.createElement("PREGUNTAS");
	pVarOElement.appendChild(varONodo);
	var varIdItem = pVarOElement.selectSingleNode("ID").text;
	var totalPreguntas = 	document.all.PREGUNTA? document.all.PREGUNTA.length:0;	// document.all.COBCHECK.length;
		
	
		for(var i=1; i<totalPreguntas;i++)
		{
			if (document.getElementById("DIVPREGUNTA-"+eval(i)).style.display != "none")
			{
				varONodoCob = pVarXml.createElement("PREGUNTA");
				varONodo = pVarXml.createElement("IDITEM");
				varONodo.text = varIdItem;
				varONodoCob.appendChild(varONodo);	
				
				varONodo = pVarXml.createElement("CODPREG");
				varONodo.text = document.all.PREGUNTA[i].codigoAIS;
				varONodoCob.appendChild(varONodo);	
						
				varONodo = pVarXml.createElement("RESPREG");
				varONodo.text = document.getElementById("PREGUNTA-"+eval(i)).value;
				varONodoCob.appendChild(varONodo)
				
				varONodoCob.appendChild(varONodo);						
				pVarOElement.selectSingleNode('//PREGUNTAS').appendChild(varONodoCob);
			}
		}
	
}

function getCoberturas(pVarXml, pVarOElement) 
{
		
	//Genera un elemento para las coberturas y recorre la tabla agregando aquellas que tengan COBCHECK en true
	varONodo = pVarXml.createElement("COBERTURAS");
	pVarOElement.appendChild(varONodo);
	var varIdItem = pVarOElement.selectSingleNode("ID").text;
	var totalCoberturas = 	document.all.COBCHECK? document.all.COBCHECK.length:0;	// document.all.COBCHECK.length;
		
	
		for(var i=0; i<totalCoberturas;i++)
		{
			//15/06/2011 LR - La cobertura checkeada tambien debe estar visible o habilitada.-
			varCOBERCOD = document.all.COBCHECK[i].cobercod;
			if (document.all.COBCHECK[i].checked && document.getElementById("FILA"+ varCOBERCOD).style.display!='none')
			{
				varONodoCob = pVarXml.createElement("COBERTURA");
				varONodo = pVarXml.createElement("IDITEM");
				varONodo.text = varIdItem;
				varONodoCob.appendChild(varONodo);	
				
				varONodo = pVarXml.createElement("COBERCOD");
				varONodo.text = document.all.COBCHECK[i].cobercod;
				varONodoCob.appendChild(varONodo);	
						
				varONodo = pVarXml.createElement("COBERDES");
				varONodo.text = document.all.COBCHECK[i].descripcion;;
				//varONodo.text = document.getElementById("COBERDES" +eval(i)).innerText;
				varONodoCob.appendChild(varONodo)
				
				varONodo = pVarXml.createElement("SUMAASEG");
				if (document.getElementById("SUMAASEG" + i ).value == '') document.getElementById("SUMAASEG" + i).value = '0';
				varONodo.text = document.getElementById("SUMAASEG" +eval(i)).value
				varONodoCob.appendChild(varONodo);
				
				varONodo = pVarXml.createElement("PRIMA");
				if (document.getElementById("PRIMA" + i ).value == '') document.getElementById("PRIMA" + i).value = '0.00';
				varONodo.text = document.getElementById("PRIMA" +eval(i)).value
				varONodoCob.appendChild(varONodo);
				varONodo = pVarXml.createElement("TASA");
				if (document.getElementById("TASA" + i ).value == '') document.getElementById("TASA" + i).value = '0.00';
				varONodo.text = document.getElementById("TASA" +eval(i)).value
				varONodoCob.appendChild(varONodo);
				if(document.getElementById("RAMOPCODSQL").value!="ICB1"){
				varONodo = pVarXml.createElement("SWTASAVM");
				varONodo.text = document.getElementById("SWTASAVM" +eval(i)).value
				varONodoCob.appendChild(varONodo);
				
				varONodo = pVarXml.createElement("OBJASEG");
				if (document.getElementById("OBJASEG" + i ).value == '') document.getElementById("OBJASEG" + i).value = 'S';
				varONodo.text = document.getElementById("OBJASEG" +eval(i)).value
				varONodoCob.appendChild(varONodo);
				}
				//varONodo = pVarXml.createElement("MODULO");
				//varONodo.text = calculaModulo(document.all.COBCHECK[i].cobercod, document.getElementById("SUMAASEG" +eval(i)).value)
				//varONodoCob.appendChild(varONodo);
				
				varONodoCob.appendChild(varONodo);						
				pVarOElement.selectSingleNode('//COBERTURAS').appendChild(varONodoCob);
			}
		}
	
}
/* ************************************************************************* */
function inicializaCoberturas()
{
	
	var totalCoberturas = document.all.COBCHECK? document.all.COBCHECK.length:0; //document.all.COBCHECK.length;
	
	for (var i=0;i<totalCoberturas;i++) 
		{ 
			if (document.all.COBCHECK[i].checked)
			{	if (!document.all.COBCHECK[i].disabled)
					document.all.COBCHECK[i].checked=false
				document.getElementById("SUMAASEG" + i ).value = ""
				document.getElementById("PRIMA" + i ).value = ""
				document.getElementById("TASA" + i ).value = ""
			}
		}
	
}


function setCoberturas(pVarXml,pId)
{
	
	//Elimina las coberturas existentes para reemplazarlas por las cargadas en el xml
	var provincia =pVarXml.selectSingleNode("//ITEM[ID="+pId+"]/PROVICOD").text
	var localidad=  pVarXml.selectSingleNode("//ITEM[ID="+pId+"]/DOMICPOB").text
	document.getElementById("CBO_PROVINCIA").value = provincia
	document.getElementById("localidadRiesgo").value = localidad
	document.getElementById("codPostalRiesgo").value = pVarXml.selectSingleNode("//ITEM[ID="+pId+"]/CPOSDOM").text
	
	fncMuestraCoberturas()
	

	
	if(document.getElementById("TIPOOPER").value !="R"){
	
	fncMuestraPreguntas(pVarXml,pId)
	
	}else
	 if(! existeItemXMLRen(pId)) fncMuestraPreguntas(pVarXml,pId)
	
	
	
	
	
//	alert(localidad +"  "+ provincia)
	
	if (pVarXml.selectSingleNode("COBERTURAS") != null)
		addCoberturasDeXml(pVarXml.selectSingleNode("COBERTURAS").xml);
}




/* ************************************************************************* */
function editarItemOT(pId,pUrlCanal,pEditar,pValida)
{
	
	document.getElementById("spCertisecAnt").innerHTML =""
	
	if(pValida){
	var resultadoItems= validarItemsVacios() //valida y si estan completos agrega el item actual		
	if (!resultadoItems) return
	}
	
	
	//inicializaCoberturas()
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXmlNodoItem = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	if (varXml.loadXML(document.getElementById("varXmlItems").value))
		{	
	
		//window.document.body.insertAdjacentHTML("beforeEnd", "completo<textarea style='position:absolute; z-index:100'>"+varXml.xml+"</textarea>")	
		varXmlNodoItem = varXml.selectNodes("//ITEM[@ID=" + pId + "]")[0];
		//window.document.body.insertAdjacentHTML("beforeEnd", "Nodo<textarea>"+varXmlNodoItem.xml+"</textarea>")	
		}
	else
		alert("Error en XML")
	

	setCoberturas(varXmlNodoItem,pId);
	
	
	
	//Verifica el estado del item y si este est� vacio, obtiene los datos del AIS...
	// OJO!!! ver cuando es renovacion
	/*if (varXmlNodoItem.selectSingleNode("ESTADO").text=='' || varXmlNodoItem.selectSingleNode("ESTADO").text=='R')
	{
		obtenerDatosItem("OT",varXmlNodoItem.selectSingleNode("RAMO").text, varXmlNodoItem.selectSingleNode("POLIZA").text, varXmlNodoItem.selectSingleNode("CERTIF").text)
		document.getElementById("OTCERTIF").value = varXmlNodoItem.selectSingleNode("CERTIF").text;
		document.getElementById("OTID").value = pId;
	} */
	
	//Recorre la solapa tomando insertando los campos como elementos del xml
	for (i=0;i<document.getElementById("divDatosRiesgo_Otro").all.length;i++)
	{
		var varObj;
		var varCampo;
		varObj = document.getElementById("divDatosRiesgo_Otro").all[i];

		if (varObj.vaEnXml!=null && varXmlNodoItem.selectSingleNode(varObj.vaEnXml)!=null) 
		{			
		varObj.value = varXmlNodoItem.selectSingleNode(varObj.vaEnXml).text;
			if (varObj.vaEnXml=='PROVICOD')	 
			fncProvinciaSeleccionadaConCP(document.getElementById("CBO_PROVINCIA"),'localidadRiesgo','codPostalRiesgo');
	
			
		}
	}
	
	if (document.getElementById("CBO_PROVINCIA").value!='1')
		{
		document.getElementById("localidadRiesgo").disabled=false
		//GED 03-02-2011 DEFECT 254
		document.getElementById("botonBuscarLocalidad").disabled= false 
		}
	
	document.getElementById("CBO_PROVINCIA").disabled = true
	
	document.getElementById("localidadRiesgo").disabled = true
	
	//GED 03-02-2011 DEFECT 254
		document.getElementById("botonBuscarLocalidad").disabled= true 
	
	var mvarID= varXmlNodoItem.selectSingleNode("ITEMDESC").text
	
	//FASE 2
	if (document.getElementById("FILA280"))	document.getElementById("FILA280").style.display = "none"
	
	if(document.getElementById("TIPOOPER").value == "R"){
	 if (varXmlNodoItem.selectSingleNode("CERTISECANT").text!=''){
	 	
	 	//if(existeCoberturaXMLRen("280",pId)) 
	 	if(existeCoberturaXMLRen("280",varXmlNodoItem.selectSingleNode("CERTISECANT").text)) 
	 	
	 	
	 	{
	 	if (document.getElementById("FILA280")) document.getElementById("FILA280").style.display = "inline"
	 	}
		document.getElementById("spCertificado").style.display = "inline"
		document.getElementById("spCertipolAnt").innerHTML = varXmlNodoItem.selectSingleNode("CERTIPOLANT").text
		document.getElementById("spCertiannAnt").innerHTML = varXmlNodoItem.selectSingleNode("CERTIANNANT").text
		document.getElementById("spCertisecAnt").innerHTML  = varXmlNodoItem.selectSingleNode("CERTISECANT").text
		 	
		document.getElementById("CBO_PROVINCIA").disabled= true 	
		document.getElementById("localidadRiesgo").disabled= true 
		//GED 03-02-2011 DEFECT 254
		document.getElementById("botonBuscarLocalidad").disabled= true 
		
		document.getElementById("codPostalRiesgo").disabled= true 
		
		document.getElementById("cboTipoAlarma").disabled= true 
		document.getElementById("cboTipoGuardia").disabled= true
		
		//LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214
		document.getElementById("hIDISYUNT").value = varXmlNodoItem.selectSingleNode("IDISYUNT").text
		
		
		document.getElementById("seccion_Preguntas").style.display = "none"
	
	if(!pEditar)
		bloquearCoberturasRen()
	
		fncAyudaItemRenovacion(varXmlNodoItem.selectSingleNode("CERTISECANT").text)
		fncCambiarColorTodas()
		
	}
else
	{	document.getElementById("seccion_Preguntas").style.display = "inline"
		document.getElementById("cboTipoAlarma").disabled= false 
		document.getElementById("cboTipoGuardia").disabled= false
		document.getElementById("CBO_PROVINCIA").disabled= false 	
		document.getElementById("localidadRiesgo").disabled= false
		//GED 03-02-2011 DEFECT 254
		document.getElementById("botonBuscarLocalidad").disabled= false 
		document.getElementById("codPostalRiesgo").disabled= false 
		 
	}
} else {
	document.getElementById("seccion_Preguntas").style.display = "inline"
	document.getElementById("cboTipoAlarma").disabled= false 
	document.getElementById("cboTipoGuardia").disabled= false
	document.getElementById("CBO_PROVINCIA").disabled= false 	
	document.getElementById("localidadRiesgo").disabled= false 
	//GED 03-02-2011 DEFECT 254
	document.getElementById("botonBuscarLocalidad").disabled= false 
	document.getElementById("codPostalRiesgo").disabled= false 
}
	
	varXml = null;
	varXmlNodoItem = null;
	//GED 01-09-2010 DEFECT 45
	
	var codigoZona = document.getElementById("verZona").innerText
	
	quitarItemXML(pId,pUrlCanal,"M");
	//TO DO: VER QUE PASA CUANDO VEO ITEM SIN EDITAR QUE NO MANEJA FN
	//if(pEditar){
	if(document.getElementById("xmlFueraNorma").value  !=""  && itemActualEditado)	
		quitarItemDeTablaError(pId)
		
	//}else{ actualizarListadoDeTablaError(pId); }
			
	document.getElementById("divTablaErrores").innerHTML = "";
		  
	document.getElementById("verZona").innerText = codigoZona
	
	fncColoreoRenglones();
	
	//Resguardar Objetos Asegurados en XML Aux
	fncXMLObjAseResguardo(pId, document.all.OTID.value);
	
	//LR 02/05/2011 Se habilitan Coberturas habilit para el Alta
	if(document.getElementById("TIPOOPER").value=="R" &&
	 	 Number(document.getElementById("spCertipolAnt").innerHTML) > 0)
		fncHabilitarCoberturasDisp('R');
}

function bloquearCoberturasRen()
{
	
	
	var totalCoberturas = document.all.COBCHECK? document.all.COBCHECK.length:0; //document.all.COBCHECK.length;
	
	for (var i=0;i<totalCoberturas;i++) 
		{ 
			
			document.all.COBCHECK[i].disabled = true
			
			document.getElementById("SUMAASEG" + i ).disabled = true
				
			}
		
		
}
	



function quitarItemDeTablaError(pNroItem)
{
	
	varXml = new ActiveXObject('MSXML2.DOMDocument');    
	
	varXml.async = false;
	varXml.loadXML(document.getElementById("xmlFueraNorma").value);
	

	var cantErrores = varXml.selectNodes("//ERROR[NROITEM='"+pNroItem+"']").length
	for (var x=0; x<cantErrores; x++)
	{	
		varXmlNodo= varXml.selectNodes("//ERROR[NROITEM='"+pNroItem+"']").item(0)		
		varXmlNodo.parentNode.removeChild(varXmlNodo);
	}
	
	//Actualizo el Hidden con el XML
	document.getElementById("xmlFueraNorma").value = varXml.xml;
	
	//---------------------------------------------------------------------
	var varXslFueraNorma = new ActiveXObject("MSXML2.DOMDocument");
	varXslFueraNorma.async = false;	
	varXslFueraNorma.load(document.getElementById("XSLFueraNorma").XMLDocument);
	
	varTablaFueraNorma = varXml.transformNode(varXslFueraNorma);
	document.getElementById("divTablaFueraNorma").innerHTML = varTablaFueraNorma;
	
	varXml = null;
	varXmlNodo = null;
	
	actualizarListadoDeTablaError(pNroItem);
	if (document.getElementById("TIPOOPER").value=="R" && document.getElementById("SWSINIES").value=="S")
		fncActualizarEstadoSiniestralidad(document.getElementById("xmlFueraNorma").value);
		
}

function actualizarListadoDeTablaError(pId)
{	
	var varXml;
	varXml = new ActiveXObject('MSXML2.DOMDocument');
	varXml.async = false;
	varXml.loadXML(document.getElementById("xmlFueraNorma").value);

	var varXslTablaErrores = new ActiveXObject('MSXML2.DOMDocument');
	varXslTablaErrores.async = false;
	varXslTablaErrores.load(document.getElementById('XSLTablaErrores').XMLDocument);

	auxXml = new ActiveXObject('MSXML2.DOMDocument');
	auxXml.async = false;
	auxXml.loadXML(document.getElementById("xmlFueraNorma").value);
	
	var varXslFueraNorma = new ActiveXObject('MSXML2.DOMDocument');
	varXslFueraNorma.async = false;
	varXslFueraNorma.load(document.getElementById('XSLFueraNorma').XMLDocument);
	
	oXMLErrores.async = false;
	oXMLErrores.loadXML("<ERRORES></ERRORES>")

	//Renumerar el xml...
	var varCantElementos = varXml.selectNodes("//ERROR").length;
	var k;
//	window.document.body.insertAdjacentHTML("beforeEnd", "varXml.xml Antes de Enumerar = <textarea>"+varXml.xml+"</textarea>")	

	for (k=1; k<=varCantElementos; k++)
	{
		if (varXml.selectNodes("//ERROR").item(k-1).selectSingleNode("NROITEM").text>=pId)
		{
		varXml.selectNodes("//ERROR").item(k-1).selectSingleNode("NROITEM").text=Number(varXml.selectNodes("//ERROR").item(k-1).selectSingleNode("NROITEM").text) - 1
		}
	}	
	document.getElementById("xmlFueraNorma").value = varXml.xml;		
	varTablaFueraNorma = varXml.transformNode(varXslFueraNorma);	
	var varTablaErrores;	
	varTablaErrores = oXMLErrores.transformNode(varXslTablaErrores);
	//window.document.body.insertAdjacentHTML("beforeEnd", "varXslTablaErrores = <textarea>"+varTablaFueraNorma +"</textarea>")	
	if (varCantElementos >= 1){
		
		document.getElementById("divTablaFueraNorma").innerHTML = varTablaFueraNorma
		
	}		
	else{	
		
		document.getElementById('confirma_FueraNorma').style.display ='none';
		document.getElementById("divTablaFueraNorma").style.display ='none';
		document.getElementById('popUp_opacidad').style.display ='none'		
	}
}


function habilitarEditarItem()
{
	confirm_customizado("�Quiere modificar sumas y/o coberturas?") 
	
	document.getElementById("funcion_confirma").value = "confirma_habilitarEditarItem()"
	
	
	
}
var itemActualEditado= true;

function confirma_habilitarEditarItem()
{	
	var idItem = document.getElementById('OTID').value
	agregarItemXMLOT(document.getElementById("CANALURL").value + "Canales/" + document.getElementById("CANAL").value, "N" );
	itemActualEditado= true
	fncActivaRecotizar()
	editarItemOT(idItem,datosCotizacion.getCanalUrlImg(),true,false)
	fncModificarSumasMinimas()
	}

function getCertisecAnt(pId)

{
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	if (varXml.selectSingleNode("//ITEM[@ID=" + pId + "]/CERTISEC"))
		return varXml.selectSingleNode("//ITEM[@ID=" + pId + "]/CERTISEC").text;
	else
		return false
		
	
	
	
}
function itemModificadoCertisec(pId)

{
	var certisecAnt= getCertisecAnt(pId)
	
	 comparaCoberturasItemsRen()
	 
	 varXml = new ActiveXObject("MSXML2.DOMDocument");
	
	varXml.async = false;
	varXml.loadXML(oXMLCambios.xml);
	 
	return true
}


/* ************************************************************************* */
function confirm_editarItemOT(pId,pUrlCanal)
{
	
	  if (document.getElementById("TIPOOPER").value == "R" && existeItemXMLRen(pId)) // && itemModificadoCertisec(pId))
			
	  {	//modo readonly para ver cob.
	  	 itemActualEditado = false	    
		  document.getElementById('divBotonEditarItem').style.display = "inline"			        
		  muestraLeyendas= false	
		   if(document.getElementById("xmlFueraNorma").value != "")
			{
			quitarItemDeTablaError(pId)
			}
		  editarItemOT(pId,pUrlCanal,false,true)
		
		
	}
	else
	{ //item nuevo 
	    itemActualEditado = true	    
	    editarItemOT(pId,pUrlCanal,true,true)
	}

}


/* ************************************************************************* */
function confirm_quitarItemXML(pId,pUrlCanal)
{
	
	
	
		confirm_customizado("<p align='center'>�Esta seguro de eliminar el Comercio? <br>Se perder�n todos los datos</p>") 
		
		document.getElementById("funcion_confirma").value = "quitarItemXML('"+pId+"','"+pUrlCanal+ "', 'B')"
		if(document.getElementById("xmlFueraNorma").value != "")
		{
			quitarItemDeTablaError(pId)
		}

}

function quitarItemXML(pId,pUrlCanal,pOrigen)
{
	
	//PARA RENOVACION CUANDO SE AGREGA UN ITEM NUEVO 
	  if (document.getElementById("TIPOOPER").value == "R" ) 
	  {		
	  //SI SE ELIMINA ITEM
	  if(pOrigen == "B")
	  {	  
	 	 var nroSolapa = 2	
			if(existeItemXMLRen(pId))
			{	 
				if(validaUltimoItemRen()) 
				{
					alert_customizado("No se puede eliminar el �ltimo item")
					return
				}
			    	insertaXMLBajaItem(pId) 
			    	
			 }
	        	else
	            		eliminarXMLCambios("ITEM"+pId, pId)
	  }
	
	  }
	
	
	varXml = new ActiveXObject("MSXML2.DOMDocument");
	varXmlNodo = new ActiveXObject("MSXML2.DOMDocument");
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value);
	
	
	
	varXmlNodo = varXml.selectSingleNode("//ITEM[@ID=" + pId + "]");
	var varItemFN = varXml.selectSingleNode("//ITEM[@ID=" + pId + "]/ITEMDESC").text;
	//var varItemFN = varXml.selectSingleNode("//ITEM[@ID=" + pId + "]/ID").text;
	varXml.documentElement.removeChild(varXmlNodo);
	//Actualizo el Hidden con el XML
	document.getElementById("varXmlItems").value = varXml.xml;
	
	//Eliminar XML de Objetos Asegurados
	//fncXMLObjAseBorrar(pId,15)
	
	varXml = null;
	varXmlNodo = null;
	
	//GED 01-09-2010 DEFECT 45
	document.getElementById("verZona").innerText = ""	
	
	actualizarListadoXML(pUrlCanal);
	
	//GED al eliminar item que obligue a recotizar
	datosCotizacion.setCotizar("true")
	
	
}


/* ************************************************************************* */
function addCoberturasDeXml(pXmlCoberturas)
{	       
	var wvarHTML;
	var varOXML = new ActiveXObject("MSXML2.DomDocument");		
	varOXML.async = false;
	varOXML.loadXML(pXmlCoberturas);
	
//window.document.body.insertAdjacentHTML("beforeEnd", "Nodo<textarea>"+varOXML.xml+"</textarea>")	
	
	var totalCoberturas = document.all.COBCHECK? document.all.COBCHECK.length:0; //document.all.COBCHECK.length;

	if (varOXML.selectSingleNode("COBERTURAS").childNodes != null)
	{
		for (j=0; j<varOXML.selectSingleNode("COBERTURAS").childNodes.length; j++)
		{	
			//Con este if se elimina la cobertura que viene vacia por default
			if (varOXML.selectSingleNode("COBERTURAS").childNodes[j].selectSingleNode("COBERCOD").text != '')
			{
				for (var i=0;i<totalCoberturas;i++) 
					{ //alert(document.all.COBCHECK[i].cobercod +" "+document.all.COBCHECK[i].topeSuma)
					if(document.all.COBCHECK[i].topeSuma=="F")
					{
							document.getElementById("SUMAASEG" + i ).disabled=true	
							
					}
					
					
					if(document.all.COBCHECK[i].cobercod == varOXML.selectSingleNode("COBERTURAS").childNodes[j].selectSingleNode("COBERCOD").text)
			
						{	
									
						document.all.COBCHECK[i].checked=true											
						document.getElementById("SUMAASEG" + i ).disabled=false						
						document.getElementById("SUMAASEG" + i ).value = varOXML.selectSingleNode("COBERTURAS").childNodes[j].selectSingleNode("SUMAASEG").text
						
						
						document.getElementById("PRIMA" + i ).value = varOXML.selectSingleNode("COBERTURAS").childNodes[j].selectSingleNode("PRIMA").text
						document.getElementById("TASA" + i ).value = varOXML.selectSingleNode("COBERTURAS").childNodes[j].selectSingleNode("TASA").text
					
						document.getElementById("SWTASAVM" + i ).value = varOXML.selectSingleNode("COBERTURAS").childNodes[j].selectSingleNode("SWTASAVM").text
				
						}
					//GED 31-8-2010 DEFECT 40  ojo...
					if(document.all.COBCHECK[i].obligSiPpal =="N" )
							document.all.COBCHECK[i].disabled=false					
					
					
				}
			}
		}
	
	}	
	//GED 03-11-2010 ERROR AL RECUPERAR COBERTURAS NO TILDADAS
	for (var y=0;y<totalCoberturas;y++) 
				for (var x=0;x<totalCoberturas;x++) 
					if(document.all.COBCHECK[x].codigoCoberturaPrincipal == document.all.COBCHECK[y].cobercod )
   					if (!document.all.COBCHECK[y].checked)	
   						{ 					
   							document.all.COBCHECK[x].disabled= true
   						
   						}
   					
					//	
	
	
	
	
	
	return;
}
/* ************************************************************************* */
function getPunteroCobertura(pCobercod)
{
	
	var totalCoberturas =  document.all.COBCHECK? document.all.COBCHECK.length:0;
	
	
		for(var x=0; x<totalCoberturas;x++)		
		{	
			if(document.all.COBCHECK[x].cobercod == pCobercod ){ return x}
		}
	
}

/* ************************************************************************* */
function sumarTotalesItem()
{
	var varSubtotalSAseg = 0;
	var varSubtotalPrima = 0;	
	var totalCoberturas = document.all.COBCHECK? document.all.COBCHECK.length:0;// document.all.COBCHECK.length;
	
	//Recorre todos los campos de la tabla dinamica sumando aquellos correspondientes
	//a la suma asegurada.
	
		for (var i=0;i<totalCoberturas;i++) 
		{
			if (document.all.COBCHECK[i].checked)
			{		
				varSumaAseg = 1 * document.getElementById("SUMAASEG" + i ).value				
				varPrima = 1 * document.getElementById("PRIMA" + i ).value
				varTasa = 1 * document.getElementById("TASA" + i ).value
				if (varSumaAseg	!= 0 || varPrima != 0) 
				{
					//calculo de la prima total (parcial)
					if (varPrima==0) 
					{
						varSubtotalPrima += (varSumaAseg*1) * (varTasa*1) / 1000;
					} else {
						varSubtotalPrima += varPrima*1;
					}
				}
				varSubtotalSAseg += varSumaAseg;
			}
				
		}
	
	
	//trunca a dos decimales	
	var num1 = new Number(varSubtotalSAseg);
	document.getElementById('SUMATOT').value = num1.toFixed(2);
	//trunca a dos decimales	
	var num2 = new Number(varSubtotalPrima);
	document.getElementById('SUMAPRIMA').value = num2.toFixed(2);
}

function muestraTablaErrores()
{
	var varXslTablaErrores = new ActiveXObject("MSXML2.DOMDocument");	
	varXslTablaErrores.async = false;		
	varXslTablaErrores.load(document.getElementById('XSLTablaErrores').XMLDocument);
	
	var varTablaErrores;	
	varTablaErrores = oXMLErrores.transformNode(varXslTablaErrores);
	
	
	
	if (varTablaErrores!="" ) 	
	{
		document.getElementById("divTablaErrores").innerHTML = varTablaErrores;
	
	
	}
}

function esItemNuevo(pItem)
{
	
	var objXMLItemCotiza = new ActiveXObject("Msxml2.DOMDocument");
	objXMLItemCotiza.async = false; 
	objXMLItemCotiza.loadXML(document.getElementById("varXmlItems").value);	
	
	
	var certisecItem = objXMLItemCotiza.selectSingleNode("//ITEMS/ITEM[ID="+pItem+"]/CERTISECANT").text
	
	
	if (certisecItem=="") 
	return true
		else
	return false
	
	
}

//Etapa 2

function comparaCoberturasItemsRen()
{
	
	var varXml = new ActiveXObject("MSXML2.DOMDocument");	
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value)
	
	var varXmlRen = new ActiveXObject("MSXML2.DOMDocument");	
	varXmlRen.async = false;
	varXmlRen.loadXML(document.getElementById("varXmlRenovItems").value)
	
	var objXMLItem = varXml.selectNodes("//ITEM")
	var cantItems=objXMLItem.length
	
	var objXMLItemRen = varXmlRen.selectNodes("//ITEM")
	var cantItemsRen=objXMLItemRen.length
	
		for (var x=0; x< cantItemsRen ; x++)
		{  var encontroItem = false
			var idItem = objXMLItemRen[x].selectSingleNode("ID").text
			
			var CERTISECANT = varXmlRen.selectSingleNode("//ITEMS/ITEM[@ID='"+ idItem  +"']/CERTISECANT").text
			
			for (var y=0; y< cantItems ; y++)
			{
				var idItemNvo = objXMLItem[y].selectSingleNode("ID").text
				if( varXml.selectSingleNode("//ITEMS/ITEM[@ID='"+ idItemNvo  +"']/CERTISECANT").text== CERTISECANT )
				{
					encontroItem= true
					break
				}
			}
			// BAJA DE ITEM EXISTENTE
		if (!encontroItem)
		 {	
			insertarXMLCambios(idItem,"Certificado " +CERTISECANT +"-"+ getItemDescRen(CERTISECANT) + " (Baja)", 2, "CERTISEC|"+CERTISECANT, "", "", "B")  
		}
		
		}
		
					
		
	
	for (var x=0; x< cantItems ; x++)
	{    var idItem = objXMLItem[x].selectSingleNode("ID").text
	
	//	alert( existeItemXMLRen(x+1))
		 
	  if  (existeItemXMLRen(idItem))
			{
			var certisecAnt = getCertisecXMLRen(idItem)
			var objXMLCober = varXml.selectNodes("//ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA")
			var cantCober=objXMLCober.length
			
			var objXMLCoberRen = varXmlRen.selectNodes("//ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA")
			var cantCoberRen=objXMLCoberRen.length
			/*******/
			
			
				for (var i=0; i< cantCoberRen ; i++)
				{ 
				var encontroCober = false	 
				var nroCobercod = objXMLCoberRen[i].selectSingleNode("COBERCOD").text
				var sumaAsegRen =  objXMLCoberRen[i].selectSingleNode("SUMAASEG").text
					for (var j=0; j< cantCober ; j++)
					{	//alert(nroCobercodNvo  +"   " +nroCobercod)
						var nroCobercodNvo = objXMLCober[j].selectSingleNode("COBERCOD").text
						if( nroCobercodNvo== nroCobercod )
							encontroCober= true						
					}
					if (!encontroCober)
				 		{
				 			//alert(nroCobercod)
							insertarXMLCambios(idItem, idItem+"-"+ getItemDesc(idItem)+ " (Modificado)", 2, "COBERCOD|"+ nroCobercod  , "",sumaAsegRen, "B")  
						}
	
				}
				
			/*******/
			for (var y=0; y< cantCober ; y++)
			{    var coberCod = objXMLCober[y].selectSingleNode("COBERCOD").text
				//alert(coberCod +":  " + existeCoberturaXMLRen(coberCod))
				
				
				//if (existeCoberturaXMLRen(coberCod, idItem))
				
				if(existeCoberturaCertisecXMLRen(coberCod,certisecAnt))
				{   //INSERTA COBERTURAS "M"ODIFICADAS
					var 	varCampoId = "COBERDES"+ eval(y+1)
					var vInicial = varXmlRen.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXmlRen.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/SUMAASEG").text:0		     
					var 	vNuevo =  varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/SUMAASEG").text:0		     
					//var coberDes = varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/COBERDES").text:""
					
					if (vInicial != vNuevo)
					{					
						insertarXMLCambios(idItem, idItem+"-"+ getItemDesc(idItem)+ " (Modificado)", 2, "COBERCOD|"+ coberCod , vNuevo, vInicial, "M")
					}
				}
				else   
				{  //INSERTA COBERTURAS "A"LTAS	
						var 	varCampoId = "COBERDES"+ eval(y+1)
						var vInicial = 0
						var 	vNuevo =  varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/SUMAASEG").text:0		     
						var coberDes = varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/COBERDES").text:""
				
						insertarXMLCambios(idItem, idItem+"-"+ getItemDesc(idItem)+ " (Modificado)", 2, "COBERCOD|"+ coberCod, vNuevo, "", "A")
					
				}			
			}		
	  }
	else
		{insertarXMLCambios(idItem,idItem+"-"+  getItemDesc(idItem)+ " (Alta)", 2, "ITEM|ITEM"+ idItem, "", "", "A")}
		
	}
	
}

function getCOBERDES(pCobercod){
  var oXMLCoberturas = new ActiveXObject("MSXML2.DOMDocument");
	oXMLCoberturas.async = false;
	oXMLCoberturas.setProperty("SelectionLanguage", "XPath");
	
	
	oXMLCoberturas.loadXML(document.getElementById("xmlCoberturas").innerHTML)
	
	
		return oXMLCoberturas.selectSingleNode("//COBERTURA[COBERCOD="+pCobercod+"]/COBERDES").text	
				
		
	

}
function muestraCuadroPrecio()
{
	var varXmlItems = new ActiveXObject("MSXML2.DOMDocument");
	varXmlItems.async = false;
	varXmlItems.loadXML(document.getElementById("varXmlItems").value);	
	
	var cantItems=varXmlItems.selectNodes("//ITEMS/ITEM").length
	var muestraPrecio = true
	for (var x=0; x< cantItems;x++)
	{
		if(getItemFueraManual(x+1))
			muestraPrecio = false
		
	}

return muestraPrecio
}

function getItemEstado(pNroItem)
{

 var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	       objXMLItem.async = false; 
	       objXMLItem.setProperty("SelectionLanguage", "XPath");
	       objXMLItem.loadXML(document.getElementById("varXmlItems").value);

        return objXMLItem.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/ESTADO").text	

}

function setItemEstado(pNroItem,pEstado)
{

 var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	       objXMLItem.async = false; 
	       objXMLItem.setProperty("SelectionLanguage", "XPath");
	       objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	       
	       
	       objXMLItem.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/ESTADO").text= pEstado

		document.getElementById("varXmlItems").value = objXMLItem.xml
        //return objXMLItem.selectSingleNode("//ITEMS/ITEM[ID="+pNroItem+"]/ESTADO").text	

}


function getItemFueraManual(pNroItem)

{
	       var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	       objXMLItem.async = false; 
	       objXMLItem.setProperty("SelectionLanguage", "XPath");
	       objXMLItem.loadXML(document.getElementById("xmlFueraNorma").value);
		
		if(objXMLItem.selectNodes("//ERROR[NROITEM="+pNroItem+"]").length>0)
			return true
		else
			return false
		
}

function fncFueraManual()
{
	
	 // FUERA DE NORMA DE ITEM RECUPERADO
	        var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	        objXMLItem.async = false; 
	        objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	        var oXMLRegItems = objXMLItem.selectNodes("//ITEM");
	        var varCantItems = oXMLRegItems.length;
        	
		        for (var x=0; x<varCantItems; x++)
			        {	var itemFueraManual = false
				        var CERTIFICADO =  oXMLRegItems[x].selectSingleNode("CERTIPOLANT").text +
						        "-" +   oXMLRegItems[x].selectSingleNode("CERTIANNANT").text+
						        "-" +   oXMLRegItems[x].selectSingleNode("CERTISECANT").text
        				
					  
					if ( fncCantCoberFueraManual(x+1,CERTIFICADO) ) 					
					{					
					itemFueraManual = true
					}
				
					  var oXMLRegCobs =  objXMLItem.selectNodes("//COBERTURA[IDITEM='"+eval(x+1)+"']");
				        
				        var varCantCobs = oXMLRegCobs.length;
        				
				        for (var y=0; y<varCantCobs; y++)
				        {
        						
						      if( fncCoberturaFueraManual(x+1,CERTIFICADO,oXMLRegCobs[y].selectSingleNode("COBERCOD").text,oXMLRegCobs[y].selectSingleNode("SUMAASEG").text))
						      		itemFueraManual = true
						        
        				  
				        }
			        
			        if ( itemFueraManual )
			        {
			        	for (var y=0; y<varCantCobs; y++)
				        {
        					oXMLRegCobs[y].selectSingleNode("PRIMA").text = 0
        					oXMLRegCobs[y].selectSingleNode("TASA").text = 0
			        	
			        	}
			        	objXMLItem.selectSingleNode("//ITEMS/ITEM[ID="+eval(x+1)+"]/ESTADO").text = "FN"
			        	
			        	document.getElementById("varXmlItems").value = objXMLItem.xml
			        	
			        
			        }
			        
			        
			        }
        
	
		
	
}

function setPrimasEnCero(pId)
{	
	   var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	   objXMLItem.async = false; 
	   objXMLItem.loadXML(document.getElementById("varXmlItems").value);	
	   var oXMLRegCobs =  objXMLItem.selectNodes("//COBERTURA[IDITEM='"+pId+"']");				        
	   var varCantCobs = oXMLRegCobs.length;
        		
	for (var y=0; y<varCantCobs; y++)
	{	
		
		oXMLRegCobs[y].selectSingleNode("PRIMA").text = 0
		oXMLRegCobs[y].selectSingleNode("TASA").text = 0
	
	
	}
	
	document.getElementById("varXmlItems").value = objXMLItem.xml
		
	
	
	
}
function fncCantCoberFueraManual(pId,pCertisec)
{
	var varXmlItems = new ActiveXObject("MSXML2.DOMDocument");	
	varXmlItems.async = false;			
	varXmlItems.loadXML(document.getElementById("varXmlItems").value)	
	var DOMICPOB = varXmlItems.selectSingleNode("//ITEMS/ITEM[ID='"+ pId  +"']/DOMICPOB").text
	var itemFueraManual = false		
	/*****************************************/	
	if(getCantCoberturasPpal(pId) < 3 )
	 {	// SE COTIZA Y NO SE MANDA A REVISION-POR MAIL DE ECHANNELS
	//	insertarFueraNorma(pId,  DOMICPOB, "", "Debe seleccionar al menos tres coberturas principales" ,2,"N","",pCertisec)				
   		itemFueraManual = false
   	}		
	 if(!existeCoberturaXMLItem("100",pId)  &&  !existeCoberturaXMLItem("101",pId)  && !existeCoberturaXMLItem("120",pId))	
	 	{
	 		insertarFueraNorma(pId,  DOMICPOB, "", "Debe seleccionar alguna cobertura de Incendio " ,2,"N","",pCertisec)				
   			itemFueraManual = true
	 	}
	 /*****************************************/	
		if(esModulada)
	 	if(!existeCoberturaXMLItem("101",pId)  && !existeCoberturaXMLItem("120",pId))
	 	{
	 		//insertarFueraNorma(pId,  DOMICPOB, "", "Debe seleccionar alguna cobertura de Incendio Contenido" ,2,"N","",pCertisec)				
   			itemFueraManual = false
	 	}
	/*****************************************/	
	if(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='100' ]")) 	
		var sumaAseg100 = Number(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='100' ]/SUMAASEG").text)
	else
		var sumaAseg100 = 0
		
	if(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='101' ]")) 	
		var sumaAseg101 = Number(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='101' ]/SUMAASEG").text)
	else
		var sumaAseg101 = 0
		
	if(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='120' ]")) 	
		var sumaAseg120 = Number(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='120' ]/SUMAASEG").text)
	else
		var sumaAseg120 = 0
		
	var sumaCompara = sumaAseg101 ==0?sumaAseg120:sumaAseg101
	
	if( document.getElementById("cboActividadComercio").value.split("|")[0]== "050002")
	{
		
		if (sumaCompara!=0 && sumaAseg100< Number(sumaCompara*0.2)) 
	   		{		
	   				if(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='100' ]")) 
	   					{ 
	   						var coberDes = getCOBERDES("100")	
	   						insertarFueraNorma(pId,  DOMICPOB, "", "La suma asegurada no puede se menor al 20 % de las otras Incendio" ,2,"N",coberDes,pCertisec)				
   							itemFueraManual = true
   					}
	   		}
		
	}
	else
	 {
	 		if (sumaCompara!=0 && sumaAseg100< Number(sumaCompara*0.35)) 
	 		
	   			if(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='100' ]")) 
	   				{ 
	   						var coberDes = getCOBERDES("100")	
	   						insertarFueraNorma(pId,  DOMICPOB, "", "La suma asegurada no puede se menor al 35 % de las otras Incendio" ,2,"N",coberDes,pCertisec)				
   							itemFueraManual = true
   					}
	 }
	/*****************************************/	
	if(varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='302' ]")) 
	{	
	 var sumaAseg302=Number( varXmlItems.selectSingleNode("//COBERTURA[IDITEM='"+pId+"' and COBERCOD='302' ]/SUMAASEG").text   )  //document.getElementById("SUMAASEG" + indice302).value)
	 
	  	 if ( sumaAseg302> Number(sumaAseg100*0.5)) 
	  	 
	   		{	
	   			var coberDes = getCOBERDES("302")	
	   			insertarFueraNorma(pId,  DOMICPOB, "", "La suma asegurada no puede superar al 50 % de Incendio Edificio" ,2,"N",coberDes,pCertisec)				
   				itemFueraManual = true
	   		}
	}
	
return itemFueraManual 
}

function fncCoberturaFueraManual(pId,pCertisec, pCobercod, pSuma,pXMLRenov)
{			//alert(pId+" * "+pCertisec+" * "+ pCobercod+" * "+ pSuma)
			var fueramanual = false
			var varOXML = new ActiveXObject("MSXML2.DomDocument");		
			varOXML.async = false;
			varOXML.loadXML(document.getElementById("xmlCoberturas").innerHTML);	
			
			//var varXmlRenovItems = new ActiveXObject("MSXML2.DOMDocument");	
			//varXmlRenovItems.async = false;
			//varXmlRenovItems.loadXML(document.getElementById("xmlRenovaRiesgosItems").innerHTML)
			
			var varXmlItems = new ActiveXObject("MSXML2.DOMDocument");	
			varXmlItems.async = false;
			if(pXMLRenov)
				varXmlItems.loadXML(document.getElementById("varXmlRenovItems").value)	
			else		
				varXmlItems.loadXML(document.getElementById("varXmlItems").value)	
				
			var certisecAnt = pCertisec.split("-")[2]	
			
			var DOMICPOB = varXmlItems.selectSingleNode("//ITEMS/ITEM[ID='"+ pId  +"']/DOMICPOB").text
			
			var xmlCoberturas = varOXML.selectNodes("//COBERTURA")	
			var cantidadCoberturas = xmlCoberturas.length
	
			var tipoModulo =  varOXML.selectSingleNode("//COBERTURA[COBERCOD="+pCobercod+"]/SWDETMOD").text  //document.all.COBCHECK[subIndice].detModulo
			var codActual=pCobercod
			var codPpal= varOXML.selectSingleNode("//COBERTURA[COBERCOD="+pCobercod+"]/COBERPRI").text       //document.all.COBCHECK[subIndice].codigoCoberturaPrincipal	
			var cantModulos=	mayorModulo(codActual)				
			var coberDes =  	 varOXML.selectSingleNode("//COBERTURA[COBERCOD="+pCobercod+"]/COBERDES").text 
   		var sumaActual =  Number(pSuma)
   		var menModulo = menorModulo(codActual) 
   			
   			// TIPO Modulo: I= Independiente; S=es la cobertura que det mod; N=depende de la S  
   			//**********POR TOPES*********
   			if(tipoModulo=="N"  )
   			{
   				var topeMaximo = Number(topeMaximoModulo(codActual,moduloActual)) 						
	   			var topeMinimo = Number(topeMinimoModulo(codActual,moduloActual))
   				var codigoPpal = codPpal
						
			if(varOXML.selectSingleNode("//COBERTURA[COBERCOD="+pCobercod+"]/MODULOS/MODULO/SWSUMTOP").text == "T"  && 	varXmlItems.selectSingleNode("//ITEMS/ITEM[ID='"+ pId  +"']/COBERTURAS/COBERTURA[COBERCOD="+codigoPpal+"]/SUMAASEG"))
			
			{ 	var sumaPpal = varXmlItems.selectSingleNode("//ITEMS/ITEM[ID='"+ pId  +"']/COBERTURAS/COBERTURA[COBERCOD="+codigoPpal+"]/SUMAASEG").text		
	   			var cobepmaxPpal = Number(sumaPpal *  varOXML.selectSingleNode("//COBERTURA[COBERCOD="+pCobercod+"]/MODULOS/MODULO/COBEPMAX").text/100) 				   			
   				var sumaActual =   Number(pSuma) 
   				var maximoCob=Math.min(topeMaximo,cobepmaxPpal)
   				var minimoCob =  topeMinimo
   				if(sumaActual >maximoCob )
   				{ 
   					insertarFueraNorma(pId,  DOMICPOB, "", "Suma Asegurada debe ser menor a "  + formatoNro(datosCotizacion.getMonedaProducto(),maximoCob,2,",") ,2,"N",coberDes,pCertisec)				
   					fueramanual= true
   					return fueramanual
				}
				if(sumaActual <minimoCob )
   				{
   					insertarFueraNorma( pId,  DOMICPOB, "","Suma Asegurada debe ser mayor a "+ formatoNro(datosCotizacion.getMonedaProducto(),minimoCob,2,",") ,2,"N",coberDes, pCertisec)					
   					fueramanual= true	
   					return fueramanual	
				}
			}
   			
   		}			
   			//******** POR SUMAS*************
   			if(tipoModulo=="S" )  
   			{
   				if (codActual=="101" ) {
   					if (sumaActual > topeMaximoModulo("101",cantModulos) ) 
   					{
   					codActual="120"  
   					cantModulos=cantidadModulos(codActual)
   					menModulo = menorModulo(codActual)   					
   					}
   					var topeMaximo = Number(topeMaximoModulo(codActual,cantModulos)) 						
	   				var topeMinimo = Number(topeMinimoModulo(codActual,menModulo))	
	   				if(sumaActual >topeMaximo )
	   				{ 
	   					fueramanual= true	   					
	   					insertarFueraNorma( pId, DOMICPOB, "","Suma Asegurada debe ser menor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"N",coberDes, pCertisec)					   	
	   				   	sumaActual=topeMaximo   				   	
					}
					if(sumaActual <topeMinimo )
	   				{ 
	   					fueramanual= true	   					
	   					 insertarFueraNorma(pId, DOMICPOB, "","Suma Asegurada debe ser mayor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,",") ,2,"N",coberDes, pCertisec)					   	
	   				   	sumaActual=topeMinimo   				   		
					}
   				}
   				
   				if (codActual=="120" )
   				{
	   				 if (  sumaActual < topeMinimoModulo("120",menModulo)  ) 
	   				{
	   					codActual="101"  
	   					cantModulos=	cantidadModulos(codActual)	  
	   					menModulo = menorModulo(codActual)
	   				}
	   				var topeMaximo = Number(topeMaximoModulo(codActual,cantModulos)) 						
		   			var topeMinimo = Number(topeMinimoModulo(codActual,menModulo))	
	   				if(sumaActual >topeMaximo )
	   				{ 
	   					fueramanual= true	   					
	   					insertarFueraNorma( pId, DOMICPOB,  "","Suma Asegurada debe ser menor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"N",coberDes, pCertisec)						
	   				   	sumaActual=topeMaximo   				   	
					}
					if(sumaActual <topeMinimo )
	   				{ 
	   					fueramanual= true	   					
	   					insertarFueraNorma(  pId, DOMICPOB, "","Suma Asegurada debe ser mayor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,",") ,2,"N",coberDes, pCertisec)						   	 
	   				   	sumaActual=topeMinimo   				   		
					}								
   				}
   			}
   			
   			if(tipoModulo=="I" )  
   			{
   				var topeMaximo = Number(topeMaximoModulo(codActual,cantModulos)) 						
	   			var topeMinimo = Number(topeMinimoModulo(codActual,menModulo))	
   			}
   			
   			
   			if(tipoModulo=="N"  )  
   			{
   				var topeMaximo = Number(topeMaximoModulo(codActual,moduloActual)) 						
	   			var topeMinimo = Number(topeMinimoModulo(codActual,moduloActual))	
   			}
   			if(tipoModulo!="S" )
   			{
	   		if(sumaActual >topeMaximo )
   				{ 
   					fueramanual= true   					
   					insertarFueraNorma(  pId, DOMICPOB, "","Suma Asegurada debe ser menor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMaximo,2,",") ,2,"N",coberDes,pCertisec)
					sumaActual=topeMaximo   				   	
				}
				if(sumaActual <topeMinimo )
   				{ 
   					fueramanual= true
   					insertarFueraNorma(  pId, DOMICPOB, "","Suma Asegurada debe ser mayor a "+ formatoNro(datosCotizacion.getMonedaProducto(),topeMinimo,2,",") ,2,"N",coberDes, pCertisec)					
   				   	sumaActual=topeMinimo   				   		
				}
			}
			if(tipoModulo=="S")
   			{	
   			//ACA DETERMINO MODULO   		
   			 moduloActual = calculaModulo(codActual,sumaActual)
			}
			
			
		
	return fueramanual
	
}
/************************************************************************************************/
function fncHabilitarCoberturasDisp(pTIPOOPER)
{
	//LR 02/05/2011 Se habilitan Coberturas habilit para el Alta y para Renovacion
	var totalCoberDispon = 0;
	var countCoberDispon = 0;
	var statusColorear = false;
	
	if(pTIPOOPER == 'A')
	{
		var totalCoberturas =  document.all.COBCHECK? document.all.COBCHECK.length:0;
		
		for(var x=0; x<totalCoberturas;x++)		
		{
			if(document.all.COBCHECK[x].verOpeAlta != 'S')
			{
				//Si no esta habilit la cobert para el ALTA no la muestro
				varCOBERCOD = document.all.COBCHECK[x].cobercod;
				document.getElementById("FILA"+ varCOBERCOD).style.display='none';
				//vuelvo a colorear de nuevo lo reglones desp de esconder una fila
				statusColorear = true;
			}
			else
				totalCoberDispon++;
		}
	}
	if(pTIPOOPER == 'R')
	{
		var wXMLItemRen = new ActiveXObject('MSXML2.DOMDocument');
				wXMLItemRen.async = false;
				wXMLItemRen.loadXML(document.getElementById("varXMLRenovItems").value);
		//window.document.body.insertAdjacentHTML("beforeEnd","<textarea>"+ wXMLItemRen.xml +"</textarea>");
		var totalCoberturas =  document.all.COBCHECK? document.all.COBCHECK.length:0;
		
		for(var x=0; x<totalCoberturas;x++)		
		{
			if(document.all.COBCHECK[x].verOpeAlta != 'S')
			{
				varCOBERCOD = document.all.COBCHECK[x].cobercod;
				vCERTISECANT = fncSetLength(document.getElementById("spCertisecAnt").innerHTML,6);
				
				if(wXMLItemRen.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+vCERTISECANT+"]/COBERTURAS/COBERTURA[COBERCOD="+varCOBERCOD+"]"))
					document.getElementById("FILA"+ varCOBERCOD).style.display='';
				else
					document.getElementById("FILA"+ varCOBERCOD).style.display='none';
					
				//vuelvo a colorear de nuevo lo reglones desp de esconder una fila
				statusColorear = true;
			}
		}
		totalCoberDispon = totalCoberturas;
	}
	//vuelvo a colorear de nuevo lo reglones desp de esconder una fila
	if(statusColorear)
	{
		for(var x=0; x<totalCoberDispon;x++)		
		{
			varCOBERCOD = document.all.COBCHECK[x].cobercod;
			
			if (document.getElementById("FILA"+ varCOBERCOD).style.display != 'none')
			{
				countCoberDispon++;
				if(countCoberDispon % 2 == 1)
					document.getElementById("FILA"+ varCOBERCOD).style.backgroundColor = '#ededf1';
				if(countCoberDispon % 2 == 0)
					document.getElementById("FILA"+ varCOBERCOD).style.backgroundColor = '#ffffff';
			}
		}
	}
	//Salida!!!
	return;
}
/************************************************************************************************/	




//GED 09-05-2011 MEJORA 373
function fncCondicionesTecnicasFM()
{
	  var objXMLItem = new ActiveXObject("Msxml2.DOMDocument");
	        objXMLItem.async = false; 
	        objXMLItem.loadXML(document.getElementById("varXmlItems").value);
	        
	        var oXMLRegItems = objXMLItem.selectNodes("//ITEM");
	        var varCantItems = oXMLRegItems.length;
	
 
	
	
	//GED Mejora Tecnicas fuera de manual
	var condTecnicasFM = objXMLItem.selectNodes("//COBERTURA[SWTASAVM='F']").length
	
	eliminarXMLFueraNorma("condTecnicas","")					
	if (condTecnicasFM > 0)
		{	
		for (var x=0; x<varCantItems; x++)
			{	
				if (!esItemNuevo(Number(x+1)))
				{			
					//setItemEstado(Number(x+1),"FN")
					setPrimasEnCero(Number(x+1))
				}
			}							
			insertarFueraNorma("", "", "condTecnicas","Condiciones t�cnicas fuera de manual",3,"N","condTecnicas")
			return true	
		}	
	
	return false
	
	}
	
	//GED CR5 - SUMAS INCENDIO - usarla para la nueva funcion de validacion de cambios!!!
function fncCheckAumentoSumasIncendioTodos()
{
	var varXml = new ActiveXObject("MSXML2.DOMDocument");	
	varXml.async = false;
	varXml.loadXML(document.getElementById("varXmlItems").value)
	
	var varXmlRen = new ActiveXObject("MSXML2.DOMDocument");	
	varXmlRen.async = false;
	varXmlRen.loadXML(document.getElementById("varXmlRenovItems").value)
	
	var objXMLItem = varXml.selectNodes("//ITEM")
	var cantItems=objXMLItem.length
	
	var objXMLItemRen = varXmlRen.selectNodes("//ITEM")
	var cantItemsRen=objXMLItemRen.length	
	
	
	for (var x=0; x< cantItems ; x++)
	{    var idItem = objXMLItem[x].selectSingleNode("ID").text
	
	//	alert( existeItemXMLRen(x+1))
		 
	  if  (existeItemXMLRen(idItem))
			{
			var certisecAnt = getCertisecXMLRen(idItem)
			var objXMLCober = varXml.selectNodes("//ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA")
			var cantCober=objXMLCober.length
			
			var objXMLCoberRen = varXmlRen.selectNodes("//ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA")
			var cantCoberRen=objXMLCoberRen.length
			/*******/
			
			
			/*	for (var i=0; i< cantCoberRen ; i++)
				{ 
				var encontroCober = false	 
				var nroCobercod = objXMLCoberRen[i].selectSingleNode("COBERCOD").text
				var sumaAsegRen =  objXMLCoberRen[i].selectSingleNode("SUMAASEG").text
					for (var j=0; j< cantCober ; j++)
					{	//alert(nroCobercodNvo  +"   " +nroCobercod)
						var nroCobercodNvo = objXMLCober[j].selectSingleNode("COBERCOD").text
						if( nroCobercodNvo== nroCobercod )
							encontroCober= true						
					}
					if (!encontroCober)
				 		{
				 			//alert(nroCobercod)
							insertarXMLCambios(idItem, idItem+"-"+ getItemDesc(idItem)+ " (Modificado)", 2, "COBERCOD|"+ nroCobercod  , "",sumaAsegRen, "B")  
						}
	
				}*/
				
			/*******/
			var vSumAsegModif = false
			for (var y=0; y< cantCober ; y++)
			{    var coberCod = objXMLCober[y].selectSingleNode("COBERCOD").text
				//alert(coberCod +":  " + existeCoberturaXMLRen(coberCod))
				
				
				//if (existeCoberturaXMLRen(coberCod, idItem))
				
				if(existeCoberturaCertisecXMLRen(coberCod,certisecAnt))
				{   //INSERTA COBERTURAS "M"ODIFICADAS
					var 	varCampoId = "COBERDES"+ eval(y+1)
					var vInicial = varXmlRen.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXmlRen.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/SUMAASEG").text:0		     
					var 	vNuevo =  varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/SUMAASEG").text:0		     
					//var coberDes = varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/COBERDES").text:""
					if (coberCod =="100"  || coberCod== "101")
						if (vNuevo != vInicial)					
						{	
						vSumAsegModif = true
						//return false			
						//insertarXMLCambios(idItem, idItem+"-"+ getItemDesc(idItem)+ " (Modificado)", 2, "COBERCOD|"+ coberCod , vNuevo, vInicial, "M")
						}
				}
				/*else   
				{  //INSERTA COBERTURAS "A"LTAS	
						var 	varCampoId = "COBERDES"+ eval(y+1)
						var vInicial = 0
						var 	vNuevo =  varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/SUMAASEG").text:0		     
						var coberDes = varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]")?varXml.selectSingleNode("//ITEMS/ITEM[CERTISECANT="+ certisecAnt +"]/COBERTURAS/COBERTURA[COBERCOD="+coberCod+"]/COBERDES").text:""
				
						insertarXMLCambios(idItem, idItem+"-"+ getItemDesc(idItem)+ " (Modificado)", 2, "COBERCOD|"+ coberCod, vNuevo, "", "A")
					
				}	*/		
			}
			if (!vSumAsegModif)return false					
	  }
	else
		{insertarXMLCambios(idItem,idItem+"-"+  getItemDesc(idItem)+ " (Alta)", 2, "ITEM|ITEM"+ idItem, "", "", "A")}
		
	}
	
	return true
	
}