/*
------------------------------------------------------------------------------
Fecha de Modificaci�n: 28/03/2012
PPCR: 2011-00056
Desarrollador: Marcelo Gasparini
Descripci�n: Se agrega producto ICB1
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 21/12/2011
PPCR: 50055/6010661
Desarrolador: Leonardo Ruiz
Descripci�n: Soporte de HTTPS.-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 01/06/2011
PPCR: 50055/6010661
Desarrolador: Leonardo Ruiz
Descripci�n: Se agrega funcionalid para mostrar el impreso del Solicitud.-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
--------------------------------------------------------------------------------
Fecha de Modificaci�n: 07/12/2010
PPCR: 50055/6010661
Desarrolador: Gabriel D Agnone
Descripci�n: Se corrige propuesta ingresada
--------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: funcionesModular.js

Fecha de Creaci�n: 01/07/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripci�n: Se utiliza junto con claseDibujaSecci�n contiene las funciones de validaciones validaTipeo

1) fncValida(pThis): Realiza la comparaci�n del atributo "validar" del campo 

2) capturaTecla(): valida el tipeo cuando esta definido el atributo validarTipeo

3)  llamadaMensajeMQ(pRequest): Invoca el mensaje con el request pRequest retorna el response

4)  llamadaMensajeSQL(pRequest): Invoca el mensaje con el request pRequest retorna el response
*/

//59 minutos de time out para que este sincronizado con la hora que tienen los cotizadores
var timeOut= 59*60000
var serverTimeout =  setTimeout("fncSalir()",timeOut)

document.onkeydown = function(){ 

try{
var tag=event.srcElement.type;
 var kc=event.keyCode;
   
   
   if(window.event && ( window.event.keyCode == 27  ))
   {
   	ocultarAlert()
    //if(window.event && (window.event.keyCode == 116 || window.event.keyCode == 122 || window.event.keyCode == 8 || window.event.keyCode == 112)){
    //return false
    
    window.event.keyCode = 505; 
}

if(window.event.keyCode == 505){ 

event.returnValue = false
//return false;
 }

   
return ((kc != 8 && kc != 13) || ( tag == "text" &&  kc != 13 ) || (tag == "textarea") || ( tag == "submit" &&  kc == 13))

}

catch(err){alert("Error")}
 
}

function fncSalir()
{
	//GED 10-09-2010 DEFECT 78
	aceptar_customizado("La sesi�n de su Oficina Virtual ha expirado, para continuar operando deber� ingresar nuevamente.")
	document.getElementById("funcion_aceptar").value  = "fncConfirmaSalir()"
	
}

/******** Muestra pop Up Aceptar y espera para Time OUT*****/
function aceptar_customizado(pLeyenda)
{
	var ancho=document.body.clientWidth
	
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('aceptar_customizado').style.left  =200
	document.getElementById('aceptar_customizado').style.top  = document.body.scrollTop+100
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	document.getElementById('aceptar_customizado').style.display = 'inline';
	document.getElementById('div_contenidoAceptar').innerHTML = pLeyenda;
	
}
function resultadoAceptar(pResultadoAceptar)

{
document.getElementById('blockUI_aceptar').style.display ='none';document.getElementById('mensaje_aceptar').style.display ='none';document.getElementById('mensaje_aceptar').innerHTML = '';

if (pResultadoAceptar)
	{
	var funcion_aceptar= document.getElementById("funcion_aceptar").value 
	eval(funcion_aceptar)	
	}

}
//******************************************************
function fncConfirmaSalir()

{
	//GED 08-09-2010 
	document.frmCotizacion.target = "_self";
	document.all.frmCotizacion.action = "/default.asp"
	document.all.frmCotizacion.submit();
	
}
function ocultarAlert()
{	
	
document.getElementById('blockUI_opacidad').style.display ='none';
document.getElementById('blockUI_mensaje').style.display ='none';
document.getElementById('mensaje_alert').innerHTML = '';
return false
}

function fncValida(pThis)
{	
	var vectorValidar=pThis.validar.split(" ")
	var valor = pThis.value
	var condicion=vectorValidar[0]
	var valorTestigo=vectorValidar[1]
	
	if (vectorValidar[2]) {var valorTestigo1=vectorValidar[2]}
	
	
	switch (condicion)
    	{
    		case "entre":
        	{ 	//valorTestigo > valor > valorTestigo1
        		
        		switch (pThis.validarTipeo)
		    	{
		    		case "numero":
		        	{ 
		        	
				if( eval(eval(valor) < eval(valorTestigo) ||eval(valor) > eval(valorTestigo1) ))
					alert_customizado(pThis.mensajeError)
				else					
					return	
				break;	
				}
			
			}
        		
        	break;	
        	}	
        	
        	case "<":
        	case ">":
        	case "=":
        	case "<=":
        	case ">=":
        	{
			switch (pThis.validarTipeo)
		    	{
		    		case "fecha":
		        	{ 
				if (valor.search("/")!=-1)
						valor= convierteFecha(valor,"DD/MM/AAAA","AAAAMMDD")
				break;	
				}
				
			}
			//alert( eval(eval(valor) + condicion + eval(valorTestigo)));
			try{
				if( eval(eval(valor) + condicion + eval(valorTestigo)))
					return
				else{
					alert_customizado(pThis.mensajeError)
					return ""
					}
				}
			catch(err)
				{break}
		break;
		}
		default:
		{
			alert("Error en condicion de manejo error")
		}
	}
}
/*****************************************************************************/
function validaRangoEdad(pthis,pValida)
{	if (pthis.value=="")return
	var desde = pValida.split("..")[0]
	var hasta = pValida.split("..")[1]
	var fechaHoy= document.getElementById('fechaHoy').value
	
	var fechaNac= convierteFecha(pthis.value,"DD/MM/AAAA","AAAAMMDD")
	
	var fechaDesde=dateAddExtention(fechaNac.substr(6,2),fechaNac.substr(4,2),fechaNac.substr(0,4),"yyyy",desde)
	fechaDesde=  convierteFecha(fechaDesde,"DD/MM/AAAA","AAAAMMDD")
	var fechaHasta=dateAddExtention(fechaNac.substr(6,2),fechaNac.substr(4,2),fechaNac.substr(0,4),"yyyy",hasta)
	fechaHasta=  convierteFecha(fechaHasta,"DD/MM/AAAA","AAAAMMDD")
	
	if (fechaHoy < fechaDesde ) 
	{
		pthis.value=""
		//21/10/2010 LR Defect 22 Estetica, pto 6
		alert_customizado("La Fecha de Nacimiento no es v�lida, debe ser mayor a "+ desde + " a�os.")
	}
	if (fechaHoy >= fechaHasta ) 
	{
		pthis.value=""	
	 	alert_customizado("La Fecha de Nacimiento no es v�lida, debe ser menor a "+ hasta + " a�os.")  
	}
	
}
/*****************************************************************************/
function validaRangoVigencia(pthis,pValida)
{	if (pthis.value=="")return
	var desde = pValida.split("..")[0]
	var hasta = pValida.split("..")[1]
	var fechaHoy= document.getElementById('fechaHoy').value
	
	var fechaActual= convierteFecha(pthis.value,"DD/MM/AAAA","AAAAMMDD")
	
	//GED 27-8-2010 Defect 114
	var fechaLimite =convierteFecha(dateAddExtention(fechaHoy.substr(6,2),fechaHoy.substr(4,2),fechaHoy.substr(0,4),"d",hasta),"DD/MM/AAAA","AAAAMMDD")
	if (fechaActual >fechaLimite  )
	{pthis.value=""	
	alert_customizado("La fecha no puede ser mayor a 30 d�as")
	}
		
	if ((fechaActual - fechaHoy)<0 ) 
	{pthis.value=""	
	alert_customizado("La fecha no puede ser anterior al d�a de hoy")
	}
}

function errorInesperado(pError,pHideLabelError)
{  window.status ="Listo"
	document.getElementById("dFormularioCompleto").style.display="none" 
	document.getElementById("pantallaCarga").style.display="none" 
	document.getElementById("pantallaError").style.display='inline'
	//Ocultar el label de "Se ha producido un error inesperado."
	if(pHideLabelError)
		document.getElementById("mensajeErrorDescrip").style.display='none'
	else
		document.getElementById("mensajeErrorDescrip").style.display='inline'
	
	document.getElementById("mensajeError").innerText = pError
	
	//Apagar botones
	fncApagarBotones();
	//Prender boton nuevacotizacion
	document.getElementById('hNueCotConfirm').value='N';
	document.getElementById("divBtnNuevaCotizacion").style.display = "inline";
}

function fncApagarBotones()
{
	document.getElementById("divBtnNuevaCotizacion").style.display = "none";
	document.getElementById("divBtnModificarCotizacion").style.display = "none";
	document.getElementById("divBtnAnterior").style.display = "none";
	document.getElementById("divBtnContinuar").style.display = "none";
	document.getElementById("divBtnImprimirCotizacion").style.display = "none";
	document.getElementById("divBtnImprimirCertificado").style.display = "none";
	document.getElementById("divBtnEnvioEmail").style.display = "none";
	document.getElementById("divBtnGrabarIrSolicitud").style.display = "none";
	document.getElementById("divBtnGrabarCotizacion").style.display = "none";
	document.getElementById("divBtnCompletarSolicitud").style.display = "none";
	document.getElementById("divBtnEnvioRevision").style.display = "none";
	document.getElementById("divBtnConfirmarSolicitud").style.display = "none";
	document.getElementById("divTablaFueraNorma").style.display='none'
	document.getElementById("divTablaErrores").style.display='none'
	document.getElementById("divBtnRecuperarDatos").style.display = "none";
}

function encodeEntities(pDato)
{
	var cadena =  pDato.replace(/&/g,"&amp;").replace(/�/g,"&#209;").replace(/�/g,"&#193;").replace(/�/g,"&#201;").replace(/�/g,"&#205;").replace(/�/g,"&#211;").replace(/�/g,"&#218;");
	return cadena.replace(/&/g,"~~")
}
	
	
function leftString(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}
function rightString(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function formatoNroAIS(pNuro,pEnteros,pDecimales)

{
	var numAux=String(pNuro)
	
	
	if(numAux.indexOf(".") !=-1 )
	
	{
	var enteros = numAux.split(".")[0]
	
	var decimales = numAux.split(".")[1]
	
	return  fncSetLength(enteros,pEnteros,"0","R") +  fncSetLength(decimales,pDecimales,"0","L") 
	}
	else
	{
			
	return  	fncSetLength(numAux,pEnteros,"0","R") + fncSetLength("0",pDecimales,"0","L") 
	}
	
	
}

function convierteFecha(pFecha,pFormatoOrigen,pFormatoDestino)
{
	switch (pFormatoOrigen.toUpperCase())
	{
		case "MM/DD/AAAA":
		{
		var partesFecha= pFecha.split("/")
		if (pFormatoDestino.toUpperCase()=="DD/MM/AAAA") 
			{return  partesFecha[1] +"/"+  partesFecha[0] +"/"+  partesFecha[2] }
		
		break;
		}
		case "DD/MM/AAAA":
		{
		
		var partesFecha= pFecha.split("/")
		if(partesFecha.length == 3)
		{
			if (partesFecha[0].length==1) {partesFecha[0]= "0" +  partesFecha[0]}
			if (partesFecha[1].length==1) {partesFecha[1]= "0" +  partesFecha[1]}
		}
		if (pFormatoDestino.toUpperCase()=="AAAAMMDD") 
			{return partesFecha[2] + partesFecha[1]  + partesFecha[0] }
		
		break;
		}
		case "AAAAMMDD":
		{ 
			//var partesFecha= pFecha.split("/")
			if (pFormatoDestino.toUpperCase()=="DD/MM/AAAA") 
			{
				return pFecha.substr(6,2) + "/" + pFecha.substr(4,2) +"/"+ pFecha.substr(0,4)
			}
				
	
			
		}
	
}



}
function parteFecha(pFecha,pFormatoOrigen,pSalida)
{
	if (pFecha=="") pFecha="00/00/00"
	switch (pFormatoOrigen.toUpperCase())
	{
		case "MM/DD/AAAA":
		{
		
		var partesFecha=  pFecha.split("/")
		if (pFormatoDestino.toUpperCase()=="DD/MM/AAAA") {return  partesFecha[1] +"/"+  partesFecha[0] +"/"+  partesFecha[2] }
		
		break;
		}
		case "DD/MM/AAAA":
		{
		
		var partesFecha=  pFecha.split("/")
		if (partesFecha[0].length==1) {partesFecha[0]= "0" +  partesFecha[0]}
		if (partesFecha[1].length==1) {partesFecha[1]= "0" +  partesFecha[1]}
		
		
		
		switch(pSalida)
		{
		case 'DD': return partesFecha[0]; break;
		case 'MM': return partesFecha[1]; break;
		case 'AAAA': return partesFecha[2]; break;
		break;
		}
	
	
		}
}

}
function capturaTecla()
{

var idObjeto= window.event.srcElement.id;

var objeto= document.getElementById(idObjeto)
var tipoValidacion=objeto.attributes.getNamedItem("validarTipeo").value


switch (tipoValidacion)
    {
    case "numero":
        { 
          var stringChars="0123456789"  
        }
       break; 
       case "decimal":
        { 
        	 var stringChars="0123456789." 
        }
       break; 
	case "alfanum":
	{
	 var stringChars= " abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789.,;/()�?�!�� -_'&*:+"
	
	 // "abcdefghijklmn�opqrstuvwxyz0123456789& ������"
	  }		
	break;
       case "texto":
        { 
           var stringChars="abcdefghijklmn�opqrstuvwxyz& ������"
        }
        break;
        case "fecha":
        
        { 
           var stringChars="0123456789/" 

        }
       break;
        case "email":
        {         	
        	 var stringChars="abcdefghijklmnopqrstuvwxyz0123456789@._-"        
        }
       break;
       case "aperazsoc":
        { 
           var stringChars="abcdefghijklmn�opqrstuvwxyz& ������."
        }
        break;

	case "apellido":
        {
        	//26/10/2010 LR Defect 30, pto 2 y 3.
        	if(document.getElementById("tipoPersona").value == "00")
						var stringChars=" abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789.'"
					else
						var stringChars=" abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789./-'&"
        }
        break;
         case "calle":
        {         	
        	 var stringChars=" abcdefghijklmn�opqrstuvwxyzABCDEFGHIJKLMN�OPQRSTUVWXYZ����������0123456789'/"        
        }
    		break;
    		
        //GED 09-11-2010 PARA INTEGRAR NRO SOLICITUD
    	 case "letrasnum":
        {         	
        	 var stringChars="abcdefghijklmnopqrstuvwxyz0123456789"        
        }
    		break;
   //DEFECT 277
    case "telefono":
        {         	
        	 var stringChars="0123456789-() "        
        }
    		break;
   
   
    }
      
    
    var key;
	var keychar;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
		keychar = String.fromCharCode(key);
		keychar = keychar.toLowerCase();
		// control keys
	if ((key==null) || (key==0) || (key==8)  || (key==9) || (key==13) || (key==27) || (key==96) )
	{
		
		return 
	}
		// alphas and numbers
	else if ((stringChars.indexOf(keychar) > -1))
		return 
	else
		{
		 event.returnValue = false;  	
		return;
		
		}
	

}

function llamadaMensajeMQ(pRequest,pAction)
{
	var strData = 'FUNCION=mensajeMQ&RequestXMLHTTP='+ pRequest + '&actionCode=' +pAction ;
	
	var respuestaXMLHTTP =fncLlamadaXMLhttp(strData)		
	
	//   window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+respuestaXMLHTTP+"</textarea>")
	     
	    
	   return respuestaXMLHTTP
	
}

function llamadaMensajeMQAsync(pRequest,pAction,pfuncionRetorno)
{
	var strData = 'FUNCION=mensajeMQ&RequestXMLHTTP='+ pRequest + '&actionCode=' +pAction ;
	
	var respuestaXMLHTTP =fncLlamadaXMLhttpAsync(strData,pfuncionRetorno)		
	
	  // window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+respuestaXMLHTTP+"</textarea>")
	 	
}

function llamadaMensajeSQLAsync(pRequest,pfuncionRetorno)
{
	var strData = 'FUNCION=mensajeMQ&RequestXMLHTTP='+ pRequest  ;
	
	var respuestaXMLHTTP =fncLlamadaXMLhttpAsync(strData,pfuncionRetorno)		
	
	  // window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+respuestaXMLHTTP+"</textarea>")
	 	
}

function llamadaMensajeSQL(pRequest,pFuncion)
{
	if(pFuncion)
		var strData = 'FUNCION='+ pFuncion +'&RequestXMLHTTP='+ pRequest;
	else
		var strData = 'FUNCION=mensajeSQL&RequestXMLHTTP='+ pRequest;
	
	var respuestaXMLHTTP =fncLlamadaXMLhttp(strData)		
	  
			    
	  return respuestaXMLHTTP
	
}

function fncLlamadaXMLhttp(pDataToSend)
{

	var oXMLhttp;
	var varRespuesta;
	oXMLhttp = new ActiveXObject("Msxml2.XMLHTTP");	
 	oXMLhttp.Open("POST","DefinicionFrameWork.asp",false);
 	oXMLhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 	
 	oXMLhttp.send(encodeURI(pDataToSend)); 	
	varRespuesta = oXMLhttp.responseText;
	oXMLhttp = null;
	 clearTimeout(serverTimeout);
	serverTimeout =  setTimeout("fncSalir()",timeOut)	
	return varRespuesta;
}

var oXMLhttp;
var timer;

function fncLlamadaXMLhttpAsync(pDataToSend,pfuncionRetorno)
{

	
	var varRespuesta;
	
	oXMLhttp = new ActiveXObject("Msxml2.XMLHTTP");
	oXMLhttp.Open("POST","DefinicionFrameWork.asp",true);
 	oXMLhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 	
 	oXMLhttp.send(encodeURI(pDataToSend)); 
 	 clearTimeout(serverTimeout);
 	serverTimeout =  setTimeout("fncSalir()",timeOut)	
	document.getElementById("capaEsperando").style.display= 'inline'
	var body = document.body, 	html = document.documentElement; 		
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById("capaEsperando").style.height=altoPagina
	document.getElementById("spanEsperando").style.display= 'inline'
 	
 	 timer=setTimeout("abortaXMLHTTP()",60000)	
 	 
 	oXMLhttp.onreadystatechange = function() 
 	{
 		
 		
 		if (oXMLhttp.readyState==4)
 		{	
 			document.getElementById("capaEsperando").style.display= 'none'	
 			var body = document.body, 	html = document.documentElement; 		
			var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
			document.getElementById("capaEsperando").style.height=altoPagina
 			document.getElementById("spanEsperando").style.display= 'none'	
 			
 			 clearTimeout(timer);

 			eval(pfuncionRetorno + "(oXMLhttp.responseText)" )
 	
 			
 		} 		
 		
 		
 	}
	
}

function abortaXMLHTTP()
{
	oXMLhttp.abort()
	document.getElementById("capaEsperando").style.display= 'none'	
 	document.getElementById("spanEsperando").style.display= 'none'	
	
}

function muestraEsperando(pTexto){
	document.getElementById("spanEsperando").innerHTML="<b><br/>"+pTexto+"</b>"	
				
	document.getElementById("capaEsperando").style.display= 'inline'
	var body = document.body, 	html = document.documentElement; 		
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById("capaEsperando").style.height=altoPagina
	document.getElementById("spanEsperando").style.display= 'inline'
	
	}
	
function ocultaEsperando(){
	
	document.getElementById("capaEsperando").style.display= 'none'	
 	document.getElementById("spanEsperando").style.display= 'none'	
	
	
	}

function formatoNro(moneda,nro,decimales,separadorDecimal)
{
    var separadorMiles = '.';

    //separadorDecimal debe tomar unicamente valores , o .
    if (separadorDecimal == undefined)
	    separadorDecimal = ',';

    if (separadorDecimal == '.')
	    separadorMiles = ',';
	
    var y	
    entera=new Array
    var nroAux = new String(nro);

    var nroDec="0000000000"	

	if( nroAux.search(/\./)!=-1) { 
		entera=nroAux.split("."); 
		nroDec=entera[1]+nroDec
		entera[1]=nroDec.substr(0,decimales)
	}
	else {
		entera[0]=nroAux;
		entera[1]=nroDec.substr(0,decimales)
	}
	
	var num=""
	var cont=0
	for (y=entera[0].length;y>=0;y--) {	
		num=entera[0].substr(y,1) + num
		if (cont>=3	&&  cont!=entera[0].length)
		{
		if (eval(cont) % 3==0)
			{	
				num = separadorMiles + num  ;
			}
		}
		cont++;
	}
	if (decimales>0) {
	    num+=separadorDecimal + entera[1];
	}

	return moneda + " " + num;
		
}

function formatoNroDec(moneda,nro,decimales,separadorDecimal)
{
    var separadorMiles = '.';

    //separadorDecimal debe tomar unicamente valores , o .
    if (separadorDecimal == undefined)
	    separadorDecimal = ',';

    if (separadorDecimal == '.')
	    separadorMiles = ',';
	
    var y	
    entera=new Array
    var nroAux = new String(nro);

    var nroDec="0000000000"	

	if( nroAux.search(/\./)!=-1) { 
		entera=nroAux.split("."); 
		nroDec=entera[1]+nroDec
		if (Number(entera[1].length) <= Number(decimales))
			entera[1]=nroDec.substr(0,decimales)
	}
	else {
		entera[0]=nroAux;
		entera[1]=nroDec.substr(0,decimales)
	}
	
	var num=""
	var cont=0
	for (y=entera[0].length;y>=0;y--) {	
		num=entera[0].substr(y,1) + num
		if (cont>=3	&&  cont!=entera[0].length)
		{
		if (eval(cont) % 3==0)
			{	
				num = separadorMiles + num  ;
			}
		}
		cont++;
	}
	if (decimales>0) {
	    num+=separadorDecimal + entera[1];
	}

	return moneda + " " + num;
		
}


/*function dateAddExtention(pDia,pMes,pAnio,p_Interval, p_Number)
{ 

miFecha = new Date(pAnio,pMes,pDia) 

    var thing = new String(); 
  
    p_Interval = p_Interval.toLowerCase(); 
     
    if(isNaN(p_Number)){      
   
        throw "Falta el segundo parametro. \n Llego: " + p_Number; 
        return false; 
    } 

    p_Number = new Number(p_Number); 
   
    var  isleap = (pAnio % 4 == 0 && (pAnio % 100 != 0 || pAnio % 400 == 0));

    switch(p_Interval.toLowerCase()){ 
        case "yyyy": {// year 
        if(miFecha.getMonth()==2 && isleap && pDia==29)
        	{
        		miFecha.setMonth(3)
        		miFecha.setDate(1)
        	}
       
            miFecha.setFullYear(miFecha.getFullYear() + p_Number); 
            
            break; 
        } 
        case "q": {        // quarter 
            miFecha.setMonth(miFecha.getMonth() + (p_Number*3)); 
            break; 
        } 
        case "m": {        // month 
        	 if(miFecha.getMonth()==2 && isleap && pDia==29)
        	{
        		miFecha.setMonth(3)
        		miFecha.setDate(1)
        		 miFecha.setMonth(miFecha.getMonth() + p_Number-1); 
        	}
        	else        		
            miFecha.setMonth(miFecha.getMonth() + p_Number); 
            
            
            
            break; 
        } 
        case "y":        // day of year 
        case "d":        // day 
        case "w": {        // weekday 
            miFecha.setDate(miFecha.getDate() + p_Number); 
            break; 
        } 
        case "ww": {    // week of year 
            miFecha.setDate(miFecha.getDate() + (p_Number*7)); 
            break; 
        } 
        case "h": {        // hour 
            miFecha.setHours(miFecha.getHours() + p_Number); 
            break; 
        } 
        case "n": {        // minute 
            miFecha.setMinutes(miFecha.getMinutes() + p_Number); 
            break; 
        } 
        case "s": {        // second 
            miFecha.setSeconds(miFecha.getSeconds() + p_Number); 
            break; 
        } 
        case "ms": {        // second 
            miFecha.setMilliseconds(miFecha.getMilliseconds() + p_Number); 
            break; 
        } 
        default: { 
         
            //throws an error so that the coder can see why he effed up and 
            //a list of elegible letters. 
            throw    "The first parameter must be a string from this list: \n" + 
                    "yyyy, q, m, y, d, w, ww, h, n, s, or ms. You passed: " + p_Interval; 
            return false; 
        } 
    } 
    
    var dia = miFecha.getDate().toString().length==1? "0" + miFecha.getDate():miFecha.getDate() 
    var mes =  miFecha.getMonth().toString().length==1? "0" + miFecha.getMonth():miFecha.getMonth()
    return dia  +"/"+mes+"/"+ miFecha.getFullYear(); 
} 

*/

function dateAddExtention(pDia,pMes,pAnio,p_Interval, p_Number)
{ 
try{
miFecha = new Date(pAnio,(pMes-1),pDia) 

    var thing = new String(); 
  
    p_Interval = p_Interval.toLowerCase(); 
     
    if(isNaN(p_Number)){      
   
        throw "Falta el segundo parametro. \n Llego: " + p_Number; 
        return false; 
    } 

    p_Number = new Number(p_Number); 
   
    var  isleap = (pAnio % 4 == 0 && (pAnio % 100 != 0 || pAnio % 400 == 0));

    switch(p_Interval.toLowerCase()){ 
        case "yyyy": {// year 
        if(miFecha.getMonth()==2 && isleap && pDia==29)
        	{
        		miFecha.setMonth(3)
        		miFecha.setDate(1)
        	}
       
            miFecha.setFullYear(miFecha.getFullYear() + p_Number); 
            
            break; 
        } 
        case "q": {        // quarter 
            miFecha.setMonth(miFecha.getMonth() + (p_Number*3)); 
            break; 
        } 
        case "m": {        // month 
        	 if(miFecha.getMonth()==2 && isleap && pDia==29)
        	{
        		miFecha.setMonth(3)
        		miFecha.setDate(1)
        		 miFecha.setMonth(miFecha.getMonth() + p_Number-1); 
        	}
        	else  
        	{          		
           		 miFecha.setMonth(miFecha.getMonth() + p_Number); 
        	}
            
            
            break; 
        } 
        case "y":        // day of year 
        case "d":        // day 
        case "w": {        // weekday 
            miFecha.setDate(miFecha.getDate() + p_Number); 
            break; 
        } 
        case "ww": {    // week of year 
            miFecha.setDate(miFecha.getDate() + (p_Number*7)); 
            break; 
        } 
        case "h": {        // hour 
            miFecha.setHours(miFecha.getHours() + p_Number); 
            break; 
        } 
        case "n": {        // minute 
            miFecha.setMinutes(miFecha.getMinutes() + p_Number); 
            break; 
        } 
        case "s": {        // second 
            miFecha.setSeconds(miFecha.getSeconds() + p_Number); 
            break; 
        } 
        case "ms": {        // second 
            miFecha.setMilliseconds(miFecha.getMilliseconds() + p_Number); 
            break; 
        } 
        default: { 
         
            //throws an error so that the coder can see why he effed up and 
            //a list of elegible letters. 
            throw    "The first parameter must be a string from this list: \n" + 
                    "yyyy, q, m, y, d, w, ww, h, n, s, or ms. You passed: " + p_Interval; 
            return false; 
        } 
    } 
}
catch(e){
	throw getErrorObj("error dateAdd")
	alert(err.message)
	}
    var dia = fncSetLength(miFecha.getDate(),2);
    var mes = fncSetLength(miFecha.getMonth()+1,2);
    
    return dia  +"/"+mes+"/"+ miFecha.getFullYear(); 
} 


function sumaUnMes(Dia,Mes,Anio)
{
var pDia=Dia
var pMes=Mes
var pAnio=Anio
	
 diasMes = new Array (0,31,28,31,30,31,30,31,31,30,31,30,31)

var  isleap = (pAnio % 4 == 0 && (pAnio % 100 != 0 || pAnio % 400 == 0));
if  (isleap) 
  {	diasMes[2]=29 }
else	
  {	diasMes[2]=28 }


if (pMes == 12) 	
{	pMes=1  
	pAnio++
}	
else 	
	{
	if (diasMes[eval(pMes+1)] < parseInt(pDia)) 
	{ 
		pDia=1
		 pMes=eval(pMes+2) 
	}
	else
	{
		pMes++
	}
	}
//return formatoFechaLarga(pDia,pMes,pAnio)


return pDia + "/" + pMes + "/"  + pAnio


}
	
	
function parseaXSLParam(pxml,pxsl,pParametros)

{       
  //--------Carga los parametros del xsl desde pParametros-----------
 
        oXMLParam = new ActiveXObject('MSXML2.DOMDocument');
        
        oXMLParam.async = false;
       
        oXMLParam.loadXML(pParametros);
         
        var nombreParametro= oXMLParam.getElementsByTagName("nombre")
      
        var valorParametro= oXMLParam.getElementsByTagName("valor")
       
    //-------Carga xml entrada desde parametro   ----------  
         
        oXML = new ActiveXObject('MSXML2.DOMDocument');
        
        oXML.async = false;
       
         oXML.loadXML(pxml); //usar  cambiar para xml de mvarresponse
        
                
       //---------- Carga entrada parametor xsl
         oXSL=new ActiveXObject('MSXML2.FreeThreadedDOMDocument');
         
         oXSL.async = false;
         
         oXSL.loadXML(pxsl.xml);       
            
         objCache   = new ActiveXObject("Msxml2.XSLTemplate"); 
         
         objCache.stylesheet = oXSL;
      
         var oXSLT = objCache.createProcessor();
         
         oXSLT.input = oXML 
        
         for (x=0;x<nombreParametro.length;x++)
      
            { 		    
		     oXSLT.addParameter(nombreParametro[x].text, valorParametro[x].text, ""); 		 
	        }  	
	
		 oXSLT.transform();		
		
		//alert(oXSLT.output)  
		return oXSLT.output
		// psalida.innerHTML = oXSLT.output;        
      }
/***************************************************************************/
function changeOpac(opacity, id) {
    var object = document.getElementById(id).style;
   
   
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
   
} 

function opacity(id, opacStart, opacEnd, millisec) {
	
	//GED 03-11-2010 SE REDEFINE FUNCION OPACITY
	changeOpac(opacEnd, id )
    
}
function popUp_customizado(pLeyenda)
{	
	var ancho=document.body.clientWidth
	
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto  + document.body.scrollTop
	document.getElementById('popUp_mensaje').style.left  = 200
	document.getElementById('popUp_mensaje').style.top  = document.body.scrollTop+100
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	document.getElementById('popUp_mensaje').style.display = 'inline';
	document.getElementById('div_contenidoPopUp').innerHTML = pLeyenda; 
	
	
	
	
}

function confirm_customizado(pLeyenda)
{
	
	
	var ancho=document.body.clientWidth
	
	
	var body = document.body, 	html = document.documentElement; 		
	var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById('popUp_opacidad').style.width  = ancho
	document.getElementById('popUp_opacidad').style.height  = alto
	document.getElementById('confirm_cust').style.left  =200
	document.getElementById('confirm_cust').style.top  = document.body.scrollTop+100
	document.getElementById('popUp_opacidad').style.display = 'inline';
	//opacity('popUp_opacidad', 0, 10, 300);
	document.getElementById('confirm_cust').style.display = 'inline';
	document.getElementById('div_contenidoConfirm').innerHTML = pLeyenda;
	
	
}

function resultadoConfirm(pResultadoConfirm)

{
document.getElementById('popUp_opacidad').style.display ='none';document.getElementById('confirm_cust').style.display ='none';document.getElementById('msj_alert').innerHTML = ''	
	
if (pResultadoConfirm)
	{
	var funcion_confirma= document.getElementById("funcion_confirma").value 
	document.getElementById("funcion_confirma").value = ""
	eval(funcion_confirma)	
	}
else
	{
	var funcion_cancela= document.getElementById("funcion_cancela").value 
	document.getElementById("funcion_cancela").value = ""
	if (funcion_cancela!=""){
		eval(funcion_cancela)}
	}
	
}

function popUp_customizado2(pLeyenda,ppopUp)
{
		var ancho=document.body.clientWidth
		var body = document.body, 	html = document.documentElement; 		
		var alto = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

		
		
		document.getElementById('popUp_opacidad').style.width  = ancho
		document.getElementById('popUp_opacidad').style.height  = alto
		document.getElementById(ppopUp).style.left  = 200
		document.getElementById(ppopUp).style.top  =document.body.scrollTop+100
		document.getElementById('popUp_opacidad').style.display = 'inline';
		//opacity('popUp_opacidad', 0, 10, 300);
		document.getElementById(ppopUp).style.display = 'inline';
		document.getElementById('conten_'+ ppopUp).innerHTML = pLeyenda;

}


function alert_customizado(pLeyenda)
{
	var body = document.body, 	html = document.documentElement; 		
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	
	//window.status="ancho: "+html.clientWidth +" alto: "+altoPagina
	//window.status= "scrollTop: "+document.html.scrollTop  +" clientHeight: "+document.html.clientHeight + " alto: "+ altoPagina + " offsetHeight " + document.html.offsetHeight
	document.getElementById('blockUI_opacidad').style.width  = document.body.clientWidth
	document.getElementById('blockUI_opacidad').style.height  = altoPagina
	document.getElementById('blockUI_opacidad').style.display = 'inline';
	//opacity('blockUI_opacidad', 0, 10, 300);
	document.getElementById('blockUI_mensaje').style.left =200// Math.round((document.body.clientWidth-document.getElementById('blockUI_mensaje').style.width)/2)
	document.getElementById('blockUI_mensaje').style.top =  document.body.scrollTop+180
	
	document.getElementById('blockUI_mensaje').style.display = 'inline';
	document.getElementById('mensaje_alert').innerHTML = pLeyenda; 
	
}

/* ************************************************************ */
function detalle_customizado(pLeyenda)
{
	var body = document.body, 	html = document.documentElement; 		
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById('blockUI_opacidad').style.width  = document.body.clientWidth
	document.getElementById('blockUI_opacidad').style.height  = altoPagina
	document.getElementById('blockUI_opacidad').style.display = 'inline';
	//opacity('blockUI_opacidad', 0, 10, 300);
	document.getElementById('blockUI_mensaje').style.width = "550px";
	document.getElementById('blockUI_mensaje').style.top =  document.body.scrollTop+100
	document.getElementById('blockUI_mensaje').style.left = 200
	document.getElementById('blockUI_mensaje').style.display = 'inline';
	document.getElementById('msj_alert').innerHTML = pLeyenda; 
}

function ayudas_impresos(pLeyenda)
{
	var body = document.body, 	html = document.documentElement; 		
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById('blockUI_opacidad').style.width  = document.body.clientWidth
	document.getElementById('blockUI_opacidad').style.height  = altoPagina
	document.getElementById('blockUI_opacidad').style.display = 'inline';
	//opacity('blockUI_opacidad', 0, 10, 300);
	document.getElementById('blockUI_mensaje').style.width = "500px";
	document.getElementById('blockUI_mensaje').style.top = "20%";
	document.getElementById('blockUI_mensaje').style.left = "25%";
	document.getElementById('blockUI_mensaje').style.display = 'inline';
	document.getElementById('msj_alert').innerHTML = pLeyenda; 
}
/* ************************************************************ */
function ayuda_pregunta_personal(pLeyenda)
{
	var body = document.body, 	html = document.documentElement; 		
	var altoPagina = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); 

	document.getElementById('blockUI_opacidad').style.width  = document.body.clientWidth
	document.getElementById('blockUI_opacidad').style.height  = altoPagina
	document.getElementById('blockUI_opacidad').style.display = 'inline';
	//opacity('blockUI_opacidad', 0, 10, 300);
	document.getElementById('blockUI_mensaje').style.width = "500px";
	document.getElementById('blockUI_mensaje').style.top = "20%";
	document.getElementById('blockUI_mensaje').style.left = "25%";
	document.getElementById('blockUI_mensaje').style.display = 'inline';
	document.getElementById('msj_alert').innerHTML = pLeyenda; 
}	

function fncCodPostal(pElement, objCodPostal, objProvincia){
    if (objProvincia.value>1){
    	if(pElement.value!="-1")     
        objCodPostal.value = pElement.value;
    	else
        	objCodPostal.value = "";
    }
}
/******************************************************************************************/
function fncCallejero(vCalle, vNro, vProvincia, vCodPos,pSolapa)
{	
	/* Tomo los valores de los campos q necesito */
	var pCalle=document.getElementById(vCalle);
	var pNro=document.getElementById(vNro);
	var pProvincia=document.getElementById(vProvincia);
	var pCodPos=document.getElementById(vCodPos);
	
	
	/* Guardo los nombres de los campos para desp recuperarlos mas adeltante y asignarle datos */
	document.getElementById("lblCalle").value=vCalle;
	document.getElementById("lblNro").value=vNro;
	document.getElementById("lblCodPos").value=vCodPos;
	/* Validaciones de datos a ingresar */
	
	
	
	if (trim(pCalle.value.toUpperCase()) == "")
	{
		//alert("Ingrese Calle.");
		//irASolapa(pSolapa);
		//pCalle.focus();
		return;
	}
	if (trim(pNro.value) == "")
	{
		//alert("Ingrese Numero.");
		//irASolapa(pSolapa);
		//pNro.focus();
		return;
	}
	if (pProvincia.value == "-1")
	{
		//alert("Ingrese Provincia");
		//irASolapa(pSolapa);
		//pProvincia.focus();
		return;
	}

	/* El callejero solo aparece cuando es CApital Federal pProvincia = 1 */
	if(pProvincia.value == "1")
	{ 
		mvarRequest = "<Request>"+
						"<PROVICOD>1</PROVICOD>"+
						"<CALLE>"+ trim(pCalle.value.toUpperCase()) +"</CALLE>"+
					"</Request>"
		//Aca cargo todo el xml completo con todas las calles, alturas y CPs
		var validarCPXMLHTTP = llamadaMensajeMQ(mvarRequest,'lbaw_GetAlturas')
		//objeto xmlResultado para cargar
		xmlResultado = new ActiveXObject("Msxml2.DOMDocument");
	  xmlResultado.loadXML(validarCPXMLHTTP);
	  //Si encuentro una altura especifica(DESDE = HASTA), esto es para las galerias por ej q tienen un CP para ellos
	  if(xmlResultado.selectSingleNode("//Response/Estado/@resultado").text = "true")
	  	try {
	  		//Def.324 - HRF 20110304
	 			var vCPunico = xmlResultado.selectSingleNode('//ROW[PROVICOD="'+ pProvincia.value +'" and CALLE = "'+ trim(pCalle.value.toUpperCase()) +'" and DESDE ="'+ trim(pNro.value) +'" and HASTA ="'+ trim(pNro.value) +'"]/CODPOS');
	 		} catch(err) {}
		//Si econtro un CP en particular lo asigno directo
		
		
		if(vCPunico)
		{//	alert("entro" +vCPunico.text )
			//Encontro un CP especifico para un nro de calle puntual, en este caso el nro de DESDE = HASTA
			try {
				pCalle.focus();
				pNro.focus();
				pCodPos.value = vCPunico.text;
				pNro.focus();
			} catch(err) {}
			return true
		}
		else
		{
			//Esto es para un CP que no sea puntual, es decir el nro de DESDE es distinto de HASTA
			try {
				//Def.324 - HRF 20110304
				var vCPfilter = xmlResultado.selectSingleNode('//ROW[PROVICOD="'+ pProvincia.value +'" and CALLE = "'+ trim(pCalle.value.toUpperCase()) +'" and DESDE <='+ trim(pNro.value) +' and HASTA >='+ trim(pNro.value) +' and DESDE!=HASTA]/CODPOS');
			} catch(err) {}
			
			if(vCPfilter)
			{
				//Encontro un CP especifico para un nro de calle puntual
				try {
					pCalle.focus();
					pNro.focus();
					pCodPos.value = vCPfilter.text;
					pNro.focus();
				} catch(err) {}
				return true
			}
			else
			{
				/* Mostrar el callejero porque no se pude ubicar la direccion */
				flagPuedeCambiarSolapa=false
				irASolapa(pSolapa);
				//alert("No se puede determinar el Codigo Postal con la calle y altura ingresadas.");
				document.getElementById("spanEsperando").innerHTML="<b><br/>Buscando, por favor espere...</b>"	
				var strData = 'FUNCION=mensajeMQ&'+
											'RequestXMLHTTP=<Request>'+
											'<PROVICOD>1</PROVICOD>'+
											'<CALLE>'+ trim(pCalle.value) +'</CALLE>'+
											'</Request>&'+
											'actionCode=lbaw_GetAlturas';
											
				var respuestaXMLHTTP =fncLlamadaXMLhttpAsync(strData,"resultadoCallejero");
			}
		}
	}
return false
}
/******************************************************************************************/
function resultadoCallejero(pXML)
{
	oXML = new ActiveXObject('MSXML2.DOMDocument');        
		oXML.async = false;  	
		if(oXML.loadXML(pXML))
			var posicionCallejero= mostrarPopUp_Callejero(400,150)		
		else
			alert_customizado ("Error recuperando datos de ubicaci�n")
	return
}
/******************************************************************************************/
function mostrarPopUp_Callejero(pX,pY)
{
	puntero=-1
	/****************************** Parte de Domicilio ingresado ******************************/
	/* Recupero los label de los campos donde se deben tomar los datos */
	var vlblCalle = document.getElementById("lblCalle").value;
	var vlblNro = document.getElementById("lblNro").value;
	var vlblCodPos = document.getElementById("lblCodPos").value;
	/* Asigno los valores correspondientes */
	document.getElementById('divCalle').innerHTML = document.getElementById(vlblCalle).value;
	document.getElementById('divNro').innerHTML = document.getElementById(vlblNro).value;
	document.getElementById('divCP').innerHTML = document.getElementById(vlblCodPos).value;
	/******************************************************************************************/
	/* Muestro la grilla de reultados */
	mostrarResul_Callejero(pX,pY);
	//popUp_customizado(tabla)
}
/******************************************************************************************/
var puntero //Variable global que maneja la fila seleccionada dentro de los pop-ups customizados
var colorPuntero  //que color de fondo tiene
function completaCP(x)
{
	if(puntero!=-1)
	{
		if(x=="x")
			x=puntero
		var oXMLCliente = new ActiveXObject('MSXML2.DOMDocument');    
		var cp= oXML.selectSingleNode("//ROW["+x+"]/CODPOS").text;
		var calle= oXML.selectSingleNode("//ROW["+x+"]/CALLE").text;
		//Asigo los valores a los imput de la pagina
		
		var campoCodPostal = document.getElementById("pCpoCodPostalLocBuscar").value 
		var campoCalle = document.getElementById("pCpoCalleBuscar").value
		
		document.getElementById(campoCodPostal).value=cp;
		document.getElementById(campoCalle).value=calle;
		flagPuedeCambiarSolapa= true
		
		// Oculto el PopUp al seleccionar un dato
		ocultarPopUp2('popUp_callejero');
	}
}
/******************************************************************************************/
function ocultarPopUp2(ppopUp)
{	flagPuedeCambiarSolapa= true
	
	document.getElementById('popUp_opacidad').style.display ='none';
	//01/06/2011 LR - Se agrega funcionalid para mostrar el impreso del Solicitud.-
	document.getElementById('popUp_opacidadOutForm').style.display ='none';
	document.getElementById(ppopUp).style.display ='none';
	document.getElementById('msj_alert_'+ ppopUp).innerHTML = '';
}

function ocultarPopUpLoc(ppopUp)
{
	document.getElementById('popUp_opacidad').style.display ='none';
	document.getElementById(ppopUp).style.display ='none';
	
	if (document.getElementById("codPostalRiesgo").value=="") 
	{
	
	document.getElementById("localidadRiesgo").value=""
	}
	

}

function ocultarPopUpEnviar()
{
	document.getElementById('popUp_opacidad').style.display ='none';
	document.getElementById("div_cierre").style.display ='none';
	
	
}
/******************************************************************************************/
function fncBuscarCalle()
{
	var vCalleBuscar = document.getElementById('calleBuscar').value;
	//19/10/2010 LR Defect 21 Estetica, pto 34
	if(vCalleBuscar.length > 1)
	{
		mvarRequest = "<Request>"+
											"<PROVICOD>1</PROVICOD>"+
											"<CALLE>"+ trim(vCalleBuscar) +"</CALLE>"+
											"</Request>"
		//Aca vuelvo a cargar todo el xml completo con todas las calles, alturas y CPs
		oXML.loadXML(llamadaMensajeMQ(mvarRequest,'lbaw_GetAlturas'))
		//Mostrar el resultado de la nueva busqueda en el callejero
		mostrarResul_Callejero(400,150);
	}
}
/******************************************************************************************/
function mostrarResul_Callejero(pX,pY)
{
	/* armo la salida de resultados encontrados */
	var tabla="<table width='100%' bgcolor='#FFFFFF' style='border: solid #D0D0D0 1px';>"+
		"<tr class='Form_campname_bcoL'><td colspan='3' style='text-align:right' >"+
		"<tr class='form_titulos02c'><td width='300px'>Calle</td><td width='50px'>Desde</td><td width='50px'>Hasta</td></tr>"
		
	tabla+="<tr><td colspan='3'><div style='width:"+pX+"px; height:"+pY+"px; overflow:auto'><table>"
	for (var x=0;x<oXML.selectNodes("//ROW").length;x++)
		tabla+="<tr id='fila_"+x+ "'  class='form_campname_bcoR'    onclick='fondoGris(this)' >"+
							"<td width='300px' style='text-align:left'>"+
								"<a style='font-weight:normal;color:#000000; cursor:hand' ondblclick='completaCP("+x+")'  >"+oXML.selectSingleNode("//ROW["+x+"]/CALLE").text+"</a></td>"+
							"<td width='50px' style='text-align:right'>"+oXML.selectSingleNode("//ROW["+x+"]/DESDE").text+"</td>"+
							"<td width='50px' style='text-align:right'>"+parseNum(oXML.selectSingleNode("//ROW["+x+"]/HASTA").text)+"</td>"+
						"</tr>"
	
	tabla+="</table></div></td></tr></table><p></p>"	
	
	popUp_customizado2(tabla,'popUp_callejero')
}

function fncColoreoRenglones()
{
var mvarAlternoColor = 0;
var mobjCampo;

	for (i = 0; i <= window.document.getElementsByTagName('DIV').length - 1; i++)
	{
		mobjCampo = window.document.getElementsByTagName('DIV').item(i);
		if (mobjCampo.className == 'areaDatCpoConInp')
		{
			if (mobjCampo.style.display != 'none')
			{
				if (mvarAlternoColor == 0)
				{
					mobjCampo.style.backgroundColor = '#EDEDF1';
					mvarAlternoColor = 1;
				}
				else
				{
					mobjCampo.style.backgroundColor = '#FFFFFF';
					mvarAlternoColor = 0;
				}
			}
		}
	}
}

function recuperaComboxTexto(pCombo,pValor,pCant)
{
	var combo= document.getElementById(pCombo)
	
	
	if(pCant)
	for(var x=0; x<combo.options.length;x++)	
	{
		if ( combo.options[x].text.search(pValor)!=-1)
			combo.selectedIndex =x
	
	}
	else
	for(var x=0; x<combo.options.length;x++)	
	{
		if ( combo.options[x].text==pValor)
			combo.selectedIndex =x
	
	}
	
	
}

function recuperaCombo(pIdCombo,pValorBusqueda,pSplit)

{
	var encontro=false

	
	if(pSplit != undefined ) //pSplit=="")
	
	for(var x=0;x<document.getElementById(pIdCombo).length;x++)
	{
	  //  if(pIdCombo== "cboActividadComercio" ) alert(document.getElementById(pIdCombo).options[x].value.split("|")[pSplit])

		if (document.getElementById(pIdCombo).options[x].value.split("|")[pSplit]==pValorBusqueda)	
			{
				document.getElementById(pIdCombo).selectedIndex= x
				encontro=true
				return
			}
	}
	
	else
	for(var x=0;x<document.getElementById(pIdCombo).length;x++)
	{
		
		
		if (document.getElementById(pIdCombo).options[x].value==pValorBusqueda)	
			{
				document.getElementById(pIdCombo).selectedIndex= x	
				encontro=true
				return
			}
	}
	
	if(!encontro) document.getElementById(pIdCombo).selectedIndex= 0	
	//alert("No se encontro el valor para " + pIdCombo)
}

function fondoGrisSeleccion(pElement)
{	
	if(puntero!=-1 ) 
	{
	
	document.getElementById("fila_" +puntero).style.backgroundColor=colorPuntero
	//var idPuntero= pElement.id.split("_")[0] + puntero
	
	}
	
	
	puntero=pElement.id.split("_")[1]
	//pElement.style.border='solid 1px #000000'
	colorPuntero= document.getElementById("fila_" +puntero).style.backgroundColor	
	pElement.style.backgroundColor='rgb(178,180,181)'
}

function fncSetLength(pValor,pTamano,pValorRelleno,pLeftRight)
{
	var wRelleno = "";
	var wResult  = "";
	
	if (!pValorRelleno) {pValorRelleno = "0"}
	if (!pLeftRight)    {pLeftRight = "R"}
	
	for (var wCont = 1;wCont<=pTamano;wCont++)
	{wRelleno+= pValorRelleno;}
	
	if (pLeftRight == "R")
	{
		wResult = wRelleno.concat(pValor);
		wResult = wResult.substr(wResult.length-pTamano,pTamano);
	}	else {
		wResult = pValor.concat(wRelleno);
		wResult = wResult.substr(0,pTamano);
	}
	
	return wResult;
}

/******************************************************************************************/
function fncGetNroCot()
{
	var vNroCot;
	var mvarRequest = "<Request></Request>";
	var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_GetNroCot');
	var XmlDom = new ActiveXObject("Msxml2.DOMDocument");
	XmlDom.loadXML(mvarResponse);
	
	if (XmlDom.parseError.errorCode != 0)
	{
		alert('Error en el XML: ' + XmlDom.parseError.reason);
		return false;
	}
	else
	{
		if (XmlDom.selectSingleNode('//Estado').attributes['0'].text == 'false')
		{
			alert(XmlDom.selectSingleNode('//Estado').attributes['1'].text);
			return false;
		}
		vNroCot = XmlDom.selectSingleNode("//NROCOT").text;
	}
	return vNroCot;
}
/******************************************************************************************/
function fncBuscarCuentas(pTipoCuenta)
{
	//LR 03/09/2010 Traer datos de las cuentas del usr
	//pTipoCuenta puede ser (DB debito o TC tarjeta de cred)
	//��������������������������������������������������������������������������������//
	//DEFINICION DE LOS CAMPOS A UTILIZAR EN ESTA FNC.-
	var pApellidoCliente = document.getElementById("apellidoCliente").value;
	var pNombreCliente = document.getElementById("nombreCliente").value;
	var pTipoDoc = document.getElementById("tipoDocCliente");
	var pDocumento = document.getElementById("nroDocumentoCliente");
	var pCobroTip = document.getElementById("formaPago").value.split("|")[1];
	//��������������������������������������������������������������������������������//
	
	//Asigno el nombre del cliente al popUp
	document.getElementById("divNombreClienteBT").innerHTML = trim(pApellidoCliente+" "+pNombreCliente)
	//le asigno el texto correspondiente si es dibito o tarj de cred
	var txtTipoCuenta = ''
	if(pTipoCuenta == 'DB')
		txtTipoCuenta = "Seleccione la cuenta sobre la cual se realizar� el d�bito:"
	else
		txtTipoCuenta = "Tarjetas de Cr�dito vinculadas al Cliente:"
	document.getElementById("divTxtMedioPago").innerHTML = txtTipoCuenta;
	
	var documento=pDocumento.value
	var tipoDoc= pTipoDoc.options[pTipoDoc.selectedIndex].value	
	
	var oXMLEquiv = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLEquiv.async = false;  
	//oXMLEquiv.load(datosCotizacion.getCanalURL() +"Forms/XML/CodigosEquivalencias.xml")
	oXMLEquiv.loadXML(document.getElementById("CodigosEquivalencias").xml)
	
	var oXMLValores= new ActiveXObject('MSXML2.DOMDocument');
	oXMLValores.async = false;
	
	var xmlDom = new ActiveXObject('MSXML2.DOMDocument');        
	xmlDom.async = false;
	
	var xslDom = new ActiveXObject('MSXML2.DOMDocument');
	xslDom.async = false;
	
	var mobjXMLDocTarj = new ActiveXObject('MSXML2.DOMDocument');        
	mobjXMLDocTarj.async = false;
	
	//alert(oXMLEquiv.xml)
	var oXMLValores
	oXMLValores = oXMLEquiv.selectNodes("//TIPODOC[CODIGO='" + fncSetLength(tipoDoc, 2) + "']/EQUIV")

	var cantNodos 
	cantNodos = oXMLValores.length

	if (cantNodos == 0) {
	    oXMLValores = oXMLEquiv.selectNodes("//TIPODOC[CODIGO='" + tipoDoc + "']/EQUIV");
	    cantNodos = oXMLValores.length;
	}
	    
	//alert("cantNodos "+cantNodos)
	for(var y=0;y<cantNodos ;y++)
	{
		var tipoDocumentoBT=oXMLValores[y].text;
		
		var mvarRequest = "<Request>"+
											"<DEFINICION>BT_SPPRODUCTOSCLIENTE.xml</DEFINICION>"+
											"<tipoDoc>"+tipoDocumentoBT+"</tipoDoc>"+
											"<documento>"+fncSetLength(documento,15)+"</documento>"+
											"</Request>"
		//alert(mvarRequest)
		var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVSQLGen')
		//alert(mvarResponse)
		if(xmlDom.loadXML(mvarResponse))
		{
			var oXMLTipoProductos = xmlDom.selectNodes("//REG")
			var cantTipoProductos=oXMLTipoProductos.length
			//alert("cantTipoProductos "+ cantTipoProductos)
			
			if(cantTipoProductos > 0)
			{
				//si es Tarj de Cred, consigo los datos
				if(pTipoCuenta == 'TC')
				{
					for (x=0; x<cantTipoProductos; x++)
					{
						var mvarRequest = "<Request>"+
											"<DEFINICION>BT_SPDATOSTARJETA.xml</DEFINICION>"+
											"<tipoProducto>"+oXMLTipoProductos[x].selectSingleNode("TipoProducto").text+"</tipoProducto>"+
											"<producto>"+oXMLTipoProductos[x].selectSingleNode("Producto").text+"</producto>"+
											"</Request>"
						//alert(mvarRequest)
						var mvarResponse=llamadaMensajeMQ(mvarRequest,'lbaw_OVSQLGen')
						//alert(mvarResponse)
						if(mobjXMLDocTarj.loadXML(mvarResponse))
						{
							if(mobjXMLDocTarj.selectSingleNode("//Response/Estado/@resultado").text == "true")
							{
								if(mobjXMLDocTarj.selectNodes("//REG").length > 0)
								{
									//Le asigno los datos de la TC al nodo padre.-
									xmlDom.selectNodes("//REG").item(x).selectSingleNode("Producto").text = mobjXMLDocTarj.selectSingleNode("//NroTarjetaTitular").text;
									mvarNodoAux = xmlDom.createElement("VTOMES")
									mvarNodoAux.text = mobjXMLDocTarj.selectSingleNode("//FechaVencimiento").text.split("/")[1]
									xmlDom.selectNodes("//REG").item(x).appendChild(mvarNodoAux)
									mvarNodoAux = xmlDom.createElement("VTOANN")
									mvarNodoAux.text = mobjXMLDocTarj.selectSingleNode("//FechaVencimiento").text.split("/")[2]
									xmlDom.selectNodes("//REG").item(x).appendChild(mvarNodoAux)
								}
							}
						}
						//alert(xmlDom.xml)
					}
					//**************************************************************************/
					//SI ES TARJETA DE CRED EL pCobroTip DEBE SER IGUAL A "TC"
					pCobroTip = pTipoCuenta;
					//**************************************************************************/
				}
				
				if(xslDom.load(document.getElementById("XSLCuentas").XMLDocument))
				{
					var varResultTabla = xmlDom.transformNode(xslDom);
			
					var xmlDomResult = new ActiveXObject('MSXML2.DOMDocument');
					xmlDomResult.async = false;
					xmlDomResult.loadXML(varResultTabla);
					
					var varResultXPATH = '';
					//Concatena los options
					//alert("pCobroTip: "+ pCobroTip)
					var varCantElementos = xmlDomResult.selectNodes('//CUENTA[MOSTRAR="SI" and COBROTIP="' + pCobroTip + '"]/option').length;
					var k;					
					varResultXPATH='';
					
					varResultXPATH = '<options>';
					for (k=0; k<varCantElementos; k++) 	
					{
						varResultXPATH += xmlDomResult.selectNodes('//CUENTA[MOSTRAR="SI" and COBROTIP="' + pCobroTip + '"]/option').item(k).xml;
					}
					varResultXPATH += '</options>';
					//alert(varResultXPATH)
					
					document.getElementById("cmbInfoMedioPago").length = 1;
					datosCotizacion.cargaComboXSL("cmbInfoMedioPago",varResultXPATH)
				}
				else
				{
					alert_customizado("No se han encontrado cuentas vinculadas al cliente ingresado");
				}
			}
		}
		else
		{
			//alert_customizado("Error recuperando datos")
			return
		}
	}
	
	//Mostrar el PopUp de Seleccion de info del medio de Pago
	popUp_customizado2('','popUp_infoMedioPago')
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+ mvarResponse +"</textarea>");
}
/******************************************************************************************/
function fncSelBuscCuentas()
{
	//LR 06/09/2010 Seleccionar el medio de pago y pasar los valores a la pag
	var wInfoMedioPago = document.getElementById("cmbInfoMedioPago").value
	//Medio de Pago (DB debito o TC tarjeta de cred)
	var wMedioPago = wInfoMedioPago.split("|")[0];
	//Banco
	var wBanco = wInfoMedioPago.split("|")[1];
	//COBROTIP
	var wCobroTip = wInfoMedioPago.split("|")[2];
	//NUMERO DE CUENTA
	var wNroCuenta = wInfoMedioPago.split("|")[3];
	
	if (wInfoMedioPago != '-1')
	{
		if(wMedioPago == 'DB')
		{
			//SUCURCOD
			var wSucurCod = wInfoMedioPago.split("|")[4];
	
			//�������������������������������������������������������������������������������������//
			//RECUPERAR LOS DATOS DE LAS CUENTAS A LOS CAMPOS DE LA PAGINA
			//SUCURSAL
			recuperaCombo("cboSucursalDC",fncSetLength(wSucurCod,4))
			//NUMERO DE CUENTA
			document.getElementById("numCuentaDC").value = trim(wNroCuenta)
		}
		if(wMedioPago == 'TC')
		{
			//TARJECOD
			var wTARJECOD = wInfoMedioPago.split("|")[4];
			//TIPOPRODUCTO
			var wTIPOPRODUCTO = wInfoMedioPago.split("|")[5];
			//VTOMES
			var wVTOMES = wInfoMedioPago.split("|")[6];
			//VTOANN
			var wVTOANN = wInfoMedioPago.split("|")[7];
			
			//�������������������������������������������������������������������������������������//
			//RECUPERAR LOS DATOS DE LAS TC A LOS CAMPOS DE LA PAGINA
			//TARJECOD
			recuperaCombo("cbo_NomTarjeta",wTARJECOD +"|"+ wCobroTip)
			//NUMERO TARJETA
			document.getElementById("numTarjeta").value = trim(wNroCuenta)
			//VTOMES
			recuperaCombo("tarvtomes",wVTOMES)
			//VTOANN
			recuperaCombo("tarvtoann",wVTOANN)
		}
		
		//Cerrar el Pop Up desp de seleccionar los datos
		ocultarPopUp2('popUp_infoMedioPago');
	}
	else
		alert("No se ha seleccionado ningun medio de pago.");
}
/******************************************************************************************/
function fncVerificarCalleNro(pCalle, pNroCalle, pCodPos)
{
	//HRF 2010-11-08 Verificar calle/n�mero
	mvarRequest = "<Request>"+
								"<PROVICOD>1</PROVICOD>"+
								"<CALLE>"+ trim(pCalle.toUpperCase()) +"</CALLE>"+
								"</Request>"
	//Aca cargo todo el xml completo con todas las calles, alturas y CPs
	var validarCPXMLHTTP = llamadaMensajeMQ(mvarRequest,'lbaw_GetAlturas')
	//objeto xmlResultado para cargar
	xmlResultado = new ActiveXObject("Msxml2.DOMDocument");
  xmlResultado.loadXML(validarCPXMLHTTP);
  //Si encuentro una altura especifica(DESDE = HASTA), esto es para las galerias por ej q tienen un CP para ellos
  if(xmlResultado.selectSingleNode("//Response/Estado/@resultado").text = "true")
  	//Def.324 - HRF 20110304
 		var vCPunico = xmlResultado.selectSingleNode('//ROW[PROVICOD="1" and CALLE = "'+ trim(pCalle.toUpperCase()) +'" and DESDE ="'+ trim(pNroCalle) +'" and HASTA ="'+ trim(pNroCalle) +'"]/CODPOS');
	//Si econtro un CP en particular lo asigno directo
	if(!vCPunico)
	{
		//Esto es para un CP que no sea puntual, es decir el nro de DESDE es distinto de HASTA
		//Def.324 - HRF 20110304
		try {
			var vCPfilter = xmlResultado.selectSingleNode('//ROW[PROVICOD="1" and CALLE = "'+ trim(pCalle.toUpperCase()) +'" and DESDE <='+ trim(pNroCalle) +' and HASTA >='+ trim(pNroCalle) +' and DESDE!=HASTA and CODPOS="'+ pCodPos +'"]/CODPOS');
		} catch(err) {}
		if(!vCPfilter)
		{
			return "No se puede determinar el Codigo Postal con la calle y altura ingresadas.";
		} else {
			return true;
		}
	} else {
		return true;
	}
}
/******************************************************************************************/
//GED 11-11-2010 FUNCIONES ETAPA 2

function recuperaTextoDeCombo(pId)
{
	
	try{
	return document.getElementById(pId).options[document.getElementById(pId).selectedIndex].text
	}catch(err){
		return
		}
	
	}
function fncBuscarEnCombo(pIdCombo,pValorBusqueda,pSplit)
{
	if(pSplit=="")
	{
		for(var x=0;x<document.getElementById(pIdCombo).length;x++)
		{ 
			if (document.getElementById(pIdCombo).options[x].value.split("|")[pSplit]==pValorBusqueda)	
				return true;
		}
	}	else {
		for(var x=0;x<document.getElementById(pIdCombo).length;x++)
		{
			if (document.getElementById(pIdCombo).options[x].value==pValorBusqueda)	
				return true;
		}
	}
	
	return false;
}

function getTextoComboXIndex(pIdCombo, pValorBusqueda,pSplit)
{
	if(pSplit=="")
	{
		for(var x=0;x<document.getElementById(pIdCombo).length;x++)
		{ 
			if (document.getElementById(pIdCombo).options[x].value.split("|")[pSplit]==pValorBusqueda)	
				return document.getElementById(pIdCombo).options[x].text
		}
	}	else {
		for(var x=0;x<document.getElementById(pIdCombo).length;x++)
		{
			if (document.getElementById(pIdCombo).options[x].value==pValorBusqueda)	
				return document.getElementById(pIdCombo).options[x].text
		}
	}
	
	return "";
	
	
	
	
	
}

function verMas(mostrar,ocultar)
	{		
		mostrar.style.display = 'inline';
		ocultar.style.display = 'none'; 
	}
	
function fncInsertarXMLCambios(oXMLCambios, pSolapa, pCampo, pValorNuevo, pValorInicial)
{	
	var varONodo = oXMLCambios.createElement("CAMBIO");
	var varOElement;
	
	varOElement = oXMLCambios.createElement("IDITEM");
	varOElement.text = "";
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("ITEM");
	varOElement.text = "S"+ pSolapa;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("NROSOLAPA");
	varOElement.text = pSolapa;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("SOLAPA");
	var solapa = pSolapa <= 4? "Cotizaci�n - ": "Solicitud - "
	varOElement.text = solapa +  document.getElementById("spanTextoEtiqSolapa_"+ pSolapa).outerText  ;
	varONodo.appendChild(varOElement);
	
	var	mvarIDCampo = pCampo
	try
	{
		var mvarCampo = document.getElementById(pCampo + "_etiq")?document.getElementById(pCampo + "_etiq").innerText:document.getElementById(pCampo).innerText;	
	} catch (error) {
		var mvarCampo =""
	}
	
	varOElement = oXMLCambios.createElement("CAMPO");	
	varOElement.text = mvarCampo;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("IDCAMPO");	
	varOElement.text = mvarIDCampo ;	
	varONodo.appendChild(varOElement);	
	
	varOElement = oXMLCambios.createElement("TIPOCAMBIO");
	//GED 07-02-2011 DEFECT 255
	if(trim(pValorInicial) =="") 
	    varOElement.text = "A";
	else
	    varOElement.text = "M";
	    
	varONodo.appendChild(varOElement);			
	//17/12/2010 LR Se agrega descripcion a TIPOCAMBIO
	varOElement = oXMLCambios.createElement("DESCTIPCAMB");
	
	if(trim(pValorInicial) =="") 
	    varOElement.text = "Alta";
	else	
	    varOElement.text = "Modificaci�n";
	    
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("VNUEVO");
	varOElement.text = pValorNuevo;
	varONodo.appendChild(varOElement);
	
	varOElement = oXMLCambios.createElement("VINICIAL");
	varOElement.text = pValorInicial;
	varONodo.appendChild(varOElement);
	
	return varONodo;
} 

function recuperaBuscaTextoDeCombo(pCombo, pValor)
{
	var wSelected = -1;
	
	for(var x=0;x<pCombo.options.length;x++)	
	{
		if (pCombo.options[x].value == pValor)
		{
			wSelected = x;
			break;
		}
	}
	
	if (wSelected > -1)
		try {
			return pCombo.options[wSelected].text;
		} catch(err) {
			return "";
		}
	else
		return "";
}

function fncRoundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
/******************************************************************************************/
function fncCambioValorRenov(pCampo)
{
	//06/01/2011 LR - Cambio el valor del campo en una renovacion?
	if (pCampo)
	{
		var varChanged = false;
		var varCampoId = pCampo.id;
		
		if (pCampo.valorOriginal != null)
			if (trim(pCampo.value.toUpperCase()) != trim(pCampo.valorOriginal.toUpperCase()))
				varChanged = true;
	}
	
	return varChanged;
	varXmlCampos = null;
}
/******************************************************************************************/
