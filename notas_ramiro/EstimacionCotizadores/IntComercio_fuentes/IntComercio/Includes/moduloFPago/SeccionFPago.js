/*
--------------------------------------------------------------------------------

 COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011. 
 ALL RIGHTS RESERVED
 
 This software is only to be used for the purpose for which it has been provided.
 No part of it is to be reproduced, disassembled, transmitted, stored in a 
 retrieval system or translated in any human or computer language in any way or
 for any other purposes whatsoever without the prior written consent of the Hong
 Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
 serious civil and criminal offence, which can result in heavy fines and payment 
 of substantial damages.

Nombre del JavaScript: SeccionFPago.js

Fecha de Creaci�n: 02/05/2011

PPcR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripci�n: Rutinas JS de Incorporacion de filtros en Cuentas y TC
	    

--------------------------------------------------------------------------------
*/

/* ************************************************************************* 
	VARIABLES GLOBALES
************************************************************************* */

//oXML = new ActiveXObject('MSXML2.DOMDocument');    
//oXML.async = false;  
//var mvarCanalURLCliente = ""
/*****************************************************************************/


/*function fncConvierteFecha(pFecha,pFormatoOrigen,pFormatoDestino)
{
	switch (pFormatoOrigen.toUpperCase())
	{
		case "MM/DD/AAAA":
		{
		var partesFecha= pFecha.split("/")
		if (pFormatoDestino.toUpperCase()=="DD/MM/AAAA") {return  partesFecha[1] +"/"+  partesFecha[0] +"/"+  partesFecha[2] }
		
		break;
		}
		case "DD/MM/AAAA":
		{
		
		var partesFecha= pFecha.split("/")
		if (partesFecha[0].length==1) {partesFecha[0]= "0" +  partesFecha[0]}
		if (partesFecha[1].length==1) {partesFecha[1]= "0" +  partesFecha[1]}
		
		if (pFormatoDestino.toUpperCase()=="AAAAMMDD") {return partesFecha[2] + partesFecha[1]  + partesFecha[0] }
		
		break;
		}
	}
}*/

/*function fncParseNum(Num)
{
    while (Num.charAt(0) == '0')
    {
        Num = Num.substring(1, Num.length);
    }

    return Num;
}*/

/*function fncTrimJS(texto01) 
{
	while(texto01.substring(0,1) == ' ')
		texto01 = texto01.substring(1,texto01.length);

	while(texto01.substring(texto01.length-1,texto01.length) == ' ')
		texto01 =texto01.substring(0, texto01.length-1);   
	
	return texto01;
}*/


function fncDatosFPago_Tarjetas(pRestrCde, pAcctShrtSLName, pRestrDesc, pCardNum, pCardhName, pTypeIdfcCde, pIdfcNum, pStatDesc)
{
    // Parametros recibidos:

	// Codigo Adintar
	//alert('pRestrCde ' + pRestrCde);
	// Banco
	//alert (pAcctShrtSLName);
	// Nombre de la Tarjeta
	//alert (pRestrDesc);
	// Numero de Tarjeta
	//alert (pCardNum);
	// Nombre de Tarjeta
	//alert (pCardhName);
	// Tipo de Documento
	//alert (pTypeIdfcCde);
	// Numero de Documento
	//alert ("pIdfcNum= "+pIdfcNum);
	//alert ("document.getElementById(nroDocumentoCliente).value= "+document.getElementById("nroDocumentoCliente").value);
	// Estado
	//alert (pStatDesc);
	
	oXMLEquivMFPago= newXMLDOM() 
	oXMLEquivMFPago=loadXMLDoc(datosCotizacion.getCanalURL() + "Includes/moduloFPago/CodigosEquivalencias.xml")
	
	if (document.getElementById("seccionTomador").style.display=="none"){
		mvarTipoDocu = document.getElementById("tipoDocCliente").value
		mvarNumeDocu=document.getElementById("nroDocumentoCliente").value		
	}	
	else{
		mvarTipoDocu = document.getElementById("tipoDocTomador").value
		mvarNumeDocu=document.getElementById("nroDocumentoTomador").value		
	}
	
	// Validar Titular
	if (pIdfcNum)
	{
		if (mvarNumeDocu != pIdfcNum)
		{
			alert_customizado ('El Titular de la Cuenta no coincide con el titular del Seguro. Recuerde que la Autorizaci�n del d�bito autom�tico, debe llevar la firma y aclaraci�n del titular de la cuenta.');
		}
	}

	// Validar Estado OPERATIVA
	if (pStatDesc)
	{
		if (pStatDesc != 'OPERATIVA')
		{
			alert_customizado ('Esta Tarjeta no puede ser seleccionada, por favor, seleccionar otra o ingrese una nueva.');
			return false;
		}
	}

	// Completar valores en pantalla del SeccionFPago_Pantalla.asp
	if (pRestrCde)
	{
		var Tarjeta_Adintar = pRestrCde;

		//oXMLEquivValores ya est� creado en el ASP SeccionFPago_inc.asp
		var oXMLValores = oXMLEquivMFPago.selectSingleNode("//TARJETAS[EQUIV_ADINTAR='"+Tarjeta_Adintar+"']/COBROTIP").text
		
		// Funcion que posiciona el Combo I_TARJECOD segun el nuevo valor seleccionado
		recuperaCombo("nomTarjeta",oXMLValores);
	}

	if (pCardNum)
	{
		document.getElementById("numTarjeta").value = pCardNum;
	}
	ocultarPopUpCue('popUp_infoCuentasVinculadas');
	return true;
}


function fncDatosFPago_Cuentas(pTypeCde, pProdCde, pInstCde, pBrnchNum, pAcctNum, pCrncyCde, pPrdNum)
{
	// Tomo Parametros de Colores
	var mvarVARDESDE_R = Number(document.all.VARDESDE_R.value);
	var mvarVARHASTA_R = Number(document.all.VARHASTA_R.value);
	var mvarVARDESDE_A = Number(document.all.VARDESDE_A.value);
	var mvarVARHASTA_A = Number(document.all.VARHASTA_A.value);
	var mvarVARDESDE_V = Number(document.all.VARDESDE_V.value);
	var mvarVARHASTA_V = Number(document.all.VARHASTA_V.value);

	// Validar Cuenta Seleccionada
	var mvarResultado_MDW = "";
	var mvarUsuarCod = document.getElementById("USER").value;
	var DataToSend = "FUNCION=CONTROLCUENTAS&USUARIO=" + mvarUsuarCod + "&BRNCHNUM=" + pBrnchNum + "&ACCTNUM=" + pAcctNum + "&CRNCYCDE=" + pCrncyCde + "&ACCTSFXNUM=" + pPrdNum;
	
	var oXMLhttp = new XMLHttpRequest();
	//var oXMLhttp = new ActiveXObject('Microsoft.XMLHTTP');
	oXMLhttp.open("POST","DefinicionFrameWork.asp",false);
	oXMLhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	oXMLhttp.send(DataToSend);

	//var cbo = document.getElementById("RAMOPCOD")
	XmlDom= newXMLDOM() 
	//var XmlDom = new ActiveXObject("MSXML.DomDocument");

	//alert("Recibimos en crudo" + xmlhttp.responseText);
	XmlDom.loadXML(oXMLhttp.responseText);

	//Para ver que es lo que viene poder manejarlo con XML
	//window.document.body.insertAdjacentHTML("beforeEnd", "XmlDom<textarea>" + XmlDom.xml + "</textarea>")
	
	mvarResultado_MDW = XmlDom.selectSingleNode("//RESULTADO").text;
	mvarSaldo_MDW = Number(XmlDom.selectSingleNode("//SALDO").text);
	mvarCODError_MDW = XmlDom.selectSingleNode("//CODERROR").text;
	mvarMJEError_MDW = XmlDom.selectSingleNode("//MJEERROR").text;
	mvarFecha_Ultimo_Debito_MDW = XmlDom.selectSingleNode("//FECHA_ULTIMO_DEBITO").text;
	mvarFecha_Ultimo_Credito_MDW = XmlDom.selectSingleNode("//FECHA_ULTIMO_CREDITO").text;
	//HRF 20110509 - Ticket 613738
	mvarSobreGiro_MDW = XmlDom.selectSingleNode("//SOBREGIRO").text;
	mvarCuenta_Nueva_MDW = XmlDom.selectSingleNode("//CUENTA_NUEVA").text;
	
	mvarTipo_Cuenta = XmlDom.selectSingleNode("//TIPOCUENTA").text
	mvarNro_Cuenta = XmlDom.selectSingleNode("//NROCUENTA").text

	if (mvarResultado_MDW == "9999") {
	    alert_customizado('Por el momento no contamos con el servicio que provee la verificaci�n del Estado de la Cuenta, por favor, contin�e en forma manual.[' + mvarResultado_MDW + ']');
	}
	else {
	    if (mvarResultado_MDW == "9998") {
	        alert_customizado('N�mero de Cuenta Inexistente, por favor, seleccionar otra o ingrese una nueva.');
	        return;
	    }
	    //HRF 20110509 - Ticket 613738
	    if ((mvarSaldo_MDW <= mvarVARHASTA_R) && (mvarSobreGiro_MDW != "S") && (mvarCuenta_Nueva_MDW != "S")) {
	        alert_customizado('Esta Cuenta no puede ser seleccionada, por favor, elija otra Cuenta o medio de pago.');
	        return;
	    }
	}

	// Completar valores en pantalla del SeccionFPago_Pantalla.asp
	if (pBrnchNum)
	{
		var mSucursal = "0" + pBrnchNum
		// Funcion que posiciona el Combo I_FPSUCURSAL segun el nuevo valor seleccionado
		recuperaCombo("cboSucursalDC",mSucursal);
	}
	if (pAcctNum)
		document.getElementById("numCuentaDC").value = pAcctNum;
	
	if (mvarTipo_Cuenta)
	{
		//'Tipo de Producto 
    //' El Tipo de producto se define por el dato del numero de cuenta de la siguiente forma
    //' Tomar la 4ta posicion del Nro de Cuenta
    //' Para CA $ es:  6 o 1
    //' Para CC $ es:  3 o 0
    //' Para Cuentas en Dolares es:  8
		
		//' CA $
    if(mvarTipo_Cuenta == "6" || mvarTipo_Cuenta == "1")
				mvarTipo_CuentaDesc = "CA"
		
		//' CC $
    if(mvarTipo_Cuenta == "3" || mvarTipo_Cuenta == "0")
        mvarTipo_CuentaDesc = "CC"
		
		//' Fuerzo Error de Tipo de Cuenta
    if(mvarTipo_Cuenta != "6" && mvarTipo_Cuenta != "1" && mvarTipo_Cuenta != "3" && mvarTipo_Cuenta != "0" && mvarTipo_Cuenta != "8")
				mvarTipo_CuentaDesc = "-1"
    
		recuperaCombo("cboTipoCuenta",mvarTipo_CuentaDesc);
		fncTipoCuenta(mvarTipo_CuentaDesc)
	}
	
	ocultarPopUpCue('popUp_infoCuentasVinculadas');
	return true;
}

function fncValidar_Tarjetas()
{
	// PPCR 2009-00848 - Incorporacion en OV de filtros en Cuentas y TC para Canal HSBC
	
	if(document.getElementById("formaPago").value.split("|")[0] !=4)
		return true
  
	//GED 03-05-2011
	//VALIDA TARJETAS CUANDO BANCO EMISOR NO ES HSBC
	var mvarCTANUM = document.getElementById('numTarjeta').value
	var mvarVENANN =  document.getElementById('tarvtoann').value
	var mvarVENMES = document.getElementById('tarvtomes').value
	var mvarMarcaTrj = document.getElementById('nomTarjeta').value
	var mvarRequest =  "<Request>" +
												"<USUARIO></USUARIO>"+
				                "<BCOCOD>0</BCOCOD>"+
				                "<SUCURCOD>0</SUCURCOD>" +
				                "<COBROTIP>"+ mvarMarcaTrj +"</COBROTIP>"+
				                "<CTANUM>"+ 	mvarCTANUM 	+"</CTANUM>" +
				                "<VENANN>"+ 	mvarVENANN 	+"</VENANN>" +
				                "<VENMES>"+ 	mvarVENMES 	+"</VENMES>" +
											"</Request>"
	
	var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_OVValidaCuentas')	
	
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
	//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarResponse+"</textarea>")
	mobjXMLDoc= newXMLDOM() 
	
	//var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
	//mobjXMLDoc.async = false
	mobjXMLDoc.loadXML(mvarResponse)
	
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	if(document.getElementById("CANALHSBC").value == "S")
	{
		if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").value != "true")
			return   mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").value
		
	  // Validar Estado de Tarjeta
	  var mvarResultado_MDW = "";
	  var mvarUsuarCod = document.getElementById("USER").value;
	  var mvarNroTarjeta = document.getElementById("numTarjeta").value;
	  var mvarNroTarjeta_aux = document.getElementById("numTarjeta").value;
	  if (document.getElementById("nomTarjeta").value == "AE")
	  {
	      // Tarjeta AMERICAN EXPRESS - Hay que adicionarle un 0 para la rutina de Validacion de Midleware
	      mvarNroTarjeta = String(mvarNroTarjeta_aux) + "0";
	  }
	  
	  var DataToSend = "FUNCION=CONTROLTARJETAS&NROTARJETA=" + mvarNroTarjeta + "&USUARIO=" + mvarUsuarCod;
	  
		oXMLhttp = new XMLHttpRequest();
				oXMLhttp.open("POST","DefinicionFrameWork.asp",false);
				oXMLhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				oXMLhttp.send(DataToSend);

		XmlDom= newXMLDOM()
			  XmlDom.loadXML(oXMLhttp.responseText);
			  
		mvarResultado_MDW = XmlDom.selectSingleNode("RESULTADO").text
		
		if (document.getElementById("seccionTomador").style.display=="none"){
			mvarTipoDocu = document.getElementById("tipoDocCliente").value
			mvarNumeDocu=document.getElementById("nroDocumentoCliente").value		
		}	
		else{
			mvarTipoDocu = document.getElementById("tipoDocTomador").value
			mvarNumeDocu=document.getElementById("nroDocumentoTomador").value		
		}        
		
	  if (mvarResultado_MDW == "1010") 
			return 'La cantidad de d�gitos supera el valor m�ximo permitido [16]'	
		 	
		if(mvarResultado_MDW == "8888" )
			return 'Por el momento no contamos con el servicio que provee la verificaci�n del Estado de la Tarjeta.[' + mvarResultado_MDW + ']'
		  
		if (mvarResultado_MDW == "9999" )
			return 'Por el momento no contamos con el servicio que provee la verificaci�n del Estado de la Tarjeta.[' + mvarResultado_MDW + ']'
		  
		if( document.getElementById("radioBancoHSBC").checked)
		{
	  	if (mvarResultado_MDW == "0" || mvarResultado_MDW == "")
				return 'La Tarjeta es inexistente, por favor, seleccionar otra o ingrese una nueva. [' + mvarResultado_MDW + ']'                    
			if (mvarResultado_MDW != "4")
			 	return 'La Tarjeta es inv�lida, por favor, seleccionar otra o ingrese una nueva. [' + mvarResultado_MDW + ']'
		}
		if(document.getElementById("radioOtroBanco").checked && mvarResultado_MDW == "4" )
		{
			document.getElementById("numTarjeta").focus();
			return 'El N� pertenece a una Tarjeta HSBC Bank Argentina S.A., por favor, verifique los datos ingresados.'
		}
		if(document.getElementById("radioOtroBanco").checked)
			return true
		
		if(mvarResultado_MDW == "4" )
			return true
		
		return "Error desconocido [" + mvarResultado_MDW + "]"
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	}
	else
	{
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL NO HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
		if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").value == "true")
			return true
		else
		{
			//25/10/2010 LR Defect 22 Estetica, marcar todos los campos de la TC en rojo
			document.getElementById("tarvtomes_etiq").style.color='#ff0000';
			document.getElementById("tarvtoann_etiq").style.color='#ff0000';
			
			return mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").value
		}
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL NO HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	}
}

function fncValidar_Cuentas()
{
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	if(document.getElementById("CANALHSBC").value == "S")
	{
		// PPCR 2009-00848 - Incorporacion en OV de filtros en Cuentas y TC para Canal HSBC
		
		// Tomo Parametros de Colores
		var mvarVARDESDE_R = document.all.VARDESDE_R.value;
		var mvarVARHASTA_R = document.all.VARHASTA_R.value;
		var mvarVARDESDE_A = document.all.VARDESDE_A.value;
		var mvarVARHASTA_A = document.all.VARHASTA_A.value;
		var mvarVARDESDE_V = document.all.VARDESDE_V.value;
		var mvarVARHASTA_V = document.all.VARHASTA_V.value;
		
		// Validar Estado de Tarjeta
		var mvarResultado_MDW = "";
		var mvarUsuarCod  = document.getElementById("USER").value;
		var mvarSucursal = document.getElementById("cboSucursalDC").value;
		// Sucursal de 3 posiciones
		mvarSucursal = mvarSucursal.substring(1, 4);
		
		var mvarNroCuenta = document.getElementById("numCuentaDC").value;
		var mvarMoneda    = "080";
		
		var DataToSend = "FUNCION=CONTROLCUENTAS&USUARIO=" + mvarUsuarCod + "&BRNCHNUM=" + mvarSucursal + "&ACCTNUM=" + mvarNroCuenta + "&CRNCYCDE=" + mvarMoneda;
		
		var oXMLhttp = new XMLHttpRequest();
		
				oXMLhttp.open("POST","DefinicionFrameWork.asp",false);
				oXMLhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				oXMLhttp.send(DataToSend);
	  
		XmlDom= newXMLDOM() 
				XmlDom.loadXML(oXMLhttp.responseText);
		
		if(XmlDom.selectSingleNode("//RESULTADO"))
		{
			mvarResultado_MDW = XmlDom.selectSingleNode("//RESULTADO").text;
			mvarSaldo_MDW = XmlDom.selectSingleNode("//SALDO").text;
			mvarCODError_MDW = XmlDom.selectSingleNode("//CODERROR").text;
			mvarMJEError_MDW = XmlDom.selectSingleNode("//MJEERROR").text;
			mvarFecha_Ultimo_Debito_MDW = XmlDom.selectSingleNode("//FECHA_ULTIMO_DEBITO").text;
			mvarFecha_Ultimo_Credito_MDW = XmlDom.selectSingleNode("//FECHA_ULTIMO_CREDITO").text;
			//HRF 20110509 - Ticket 613738
			mvarSobreGiro_MDW = XmlDom.selectSingleNode("//SOBREGIRO").text;
			mvarCuenta_Nueva_MDW = XmlDom.selectSingleNode("//CUENTA_NUEVA").text;
			mvarNroDocTitular_MDW = XmlDom.selectSingleNode("//NRODOCTITULAR").text;
		  
			if (document.getElementById("seccionTomador").style.display=='inline' && Number(document.getElementById("nroDocumentoTomador").value) != Number(mvarNroDocTitular_MDW))
				return 'El nro de documento del tomador no coincide con el nro de documento del titular de la cuenta, por favor, seleccionar otra o ingrese una nueva.'
			
			if (mvarCODError_MDW == "500") 
				return 'N�mero de Cuenta Inexistente, por favor, seleccionar otra o ingrese una nueva.'
			
			if (mvarResultado_MDW == "8888" || mvarCODError_MDW == "12029") 
				return 'Por el momento no contamos con el servicio que provee la verificaci�n del Estado de la Cuenta.[' + mvarResultado_MDW + ']'            
			
		  if (mvarResultado_MDW == "9999" || mvarCODError_MDW == "12029") 
		  	return 'Por el momento no contamos con el servicio que provee la verificaci�n del Estado de la Cuenta, por favor, contin�e en forma manual.[' + mvarResultado_MDW + ']'            
		  
		  if (mvarResultado_MDW == "0")
		  	return 'N�mero de Cuenta Inexistente, por favor, seleccionar otra o ingrese una nueva.'
			
		  //HRF 20110509 - Ticket 613738
      if ((mvarSaldo_MDW <= mvarVARHASTA_R) && (mvarSobreGiro_MDW != "S") && (mvarCuenta_Nueva_MDW != "S"))
		  	return 'Esta Cuenta no puede ser seleccionada, por favor, elija otra Cuenta o medio de pago.';
		  
		  return true;
		}
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL NO HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	}
	else
	{
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL NO HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
		if (document.getElementById("cboSucursalDC").value == "-1")
			return 'Debe seleccionar la Sucursal.'
		if (document.getElementById('numCuentaDC').value == "")
			return 'Debe ingresar el N�mero de Cuenta.'
		
		var cobroTip	= document.getElementById("cboTipoCuenta").value
		var mvarSUCURCOD = document.getElementById("cboSucursalDC").value
		var mvarCTANUM = document.getElementById('numCuentaDC').value
		var	mvarVENANN = ""
		var	mvarVENMES = ""
		//GED 08-09-2010 MEJORA QC NRO 134 - SE SOLICITO PONER FIJO 150
		var canalBanco = '0150'
		
		var mvarRequest =	"<Request>"+
												"<USUARIO></USUARIO>"+
				                "<BCOCOD>"+ canalBanco	+"</BCOCOD>"+
				                "<SUCURCOD>"+ mvarSUCURCOD +"</SUCURCOD>"+
				                "<COBROTIP>"+ cobroTip +"</COBROTIP>"+
				                "<CTANUM>"+ 	mvarCTANUM 	+"</CTANUM>"+
				                "<VENANN>"+ 	mvarVENANN 	+"</VENANN>"+
				                "<VENMES>"+ 	mvarVENMES 	+"</VENMES>"+
				              "</Request>"
		
		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarRequest+"</textarea>")
		var mvarResponse = llamadaMensajeMQ(mvarRequest,'lbaw_OVValidaCuentas')	
		//window.document.body.insertAdjacentHTML("beforeEnd", "<textarea>"+mvarResponse+"</textarea>")
		
		mobjXMLDoc= newXMLDOM() 
		//var mobjXMLDoc = new ActiveXObject('MSXML2.DOMDocument');    
		//mobjXMLDoc.async = false
		mobjXMLDoc.loadXML(mvarResponse)
		if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").value == "true")
			return  true
		else
			//GED 08-09-2010 DEFECT 97						
			return  "Los datos ingresados de la cuenta no son correctos."
			//return   mobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL NO HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	}
}

function fncRecuperaCombo(pIdCombo,pValorBusqueda,pSplit)
{
	var encontro=false

	if(pSplit != undefined ) //pSplit=="")

	for(var x=0;x<document.getElementById(pIdCombo).length;x++)
	{

		if (document.getElementById(pIdCombo).options[x].value.split("|")[pSplit]==pValorBusqueda)	
			{
				document.getElementById(pIdCombo).selectedIndex= x
				encontro=true
				return
			}
	}

	else
	for(var x=0;x<document.getElementById(pIdCombo).length;x++)
	{


		if (document.getElementById(pIdCombo).options[x].value==pValorBusqueda)	
			{
				document.getElementById(pIdCombo).selectedIndex= x	
				encontro=true
				return
			}
	}

	if(!encontro) document.getElementById(pIdCombo).selectedIndex= 0	
	
}


function fncIngresoDIVFPagoInicio()

{
 

    if (document.frmMain.PAGO.value == "4") {
        document.getElementById("Tarjeta").style.display = 'inline';
        document.getElementById("DIV_CBU").style.display = 'none';
        document.getElementById("DIV_DC").style.display = 'none';
    }
    else {
        if (document.frmMain.I_FPNROCUENTA.value != "") {
            document.getElementById("Tarjeta").style.display = 'none';
            document.getElementById("DIV_DC").style.display = 'inline';
            document.getElementById("DIV_CBU").style.display = 'none';
        }
        if (document.frmMain.I_NROCBU.value != "") {
            document.getElementById("Tarjeta").style.display = 'none';
            document.getElementById("DIV_DC").style.display = 'none';
            document.getElementById("DIV_CBU").style.display = 'inline';
        }
    }	    
}

function fncIngresoDIVFPagoTarjetaProceso()
{
  

    if (document.getElementById("pId_Opcion_Tarjeta").value == "2")
	{
	
	document.frmMain.I_TARJECOD.value = ""
	document.frmMain.I_CUENNUME.value = ""
	
	document.getElementById("Tarjeta").style.display = 'inline';
	document.getElementById("DIV_CBU").style.display = 'none'
	document.getElementById("DIV_DC").style.display  = 'none'
	}
}


function fncIngresoDIVFPagoCuentaProceso() {
  
    if (document.getElementById("pId_Opcion_Cuenta").value == "1") {
        
        document.frmMain.I_FPSUCURSAL.value = ""
        document.frmMain.I_FPNROCUENTA.value = ""
        document.frmMain.I_NROCBU.value = ""

        document.getElementById("Tarjeta").style.display = 'none';
        document.getElementById("DIV_CBU").style.display = 'none';
        document.getElementById("DIV_DC").style.display = 'inline';
    }
    if (document.getElementById("pId_Opcion_Cuenta").value == "2") {
       
        document.frmMain.COBROTIP.value = "CA"
        document.frmMain.I_FPSUCURSAL.value = ""
        document.frmMain.I_FPNROCUENTA.value = ""
        document.frmMain.I_NROCBU.value = ""

        document.getElementById("Tarjeta").style.display = 'none';
        document.getElementById("DIV_CBU").style.display = 'none';
        document.getElementById("DIV_DC").style.display = 'inline';
    }
    if (document.getElementById("pId_Opcion_Cuenta").value == "3") {
     
        document.frmMain.COBROTIP.value = "DB"
        document.frmMain.I_FPSUCURSAL.value = ""
        document.frmMain.I_FPNROCUENTA.value = ""

        document.getElementById("Tarjeta").style.display = 'none';
        document.getElementById("DIV_DC").style.display = 'none';
        document.getElementById("DIV_CBU").style.display = 'inline';
    }
}

function formaPagoSelFP()
{
	
	//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	if(document.getElementById("CANALHSBC").value == "S")
	{
		
		var mvarRAMOPCOD_MDW
		var mvarUSUARCOD_MDW
		var mvarTIPODOCU_MDW
		var mvarNUMEDOCU_MDW
		var mvarResultado_MDW
		var mvarEstado_MDW
		var mvalChannel_MDW
		var mvarTokenId_MDW
		var mvarContador
		var mvarResultadoHTML
		var mvarVerDetalle
		
		mvarVerDetalle=""
		mvarContador=0
		mvarRAMOPCOD_MDW    = document.getElementById("RAMOPCOD").value;
		mvarUSUARCOD_MDW    = document.getElementById("LOGON_USER").value;
		
		//##########################
		if (document.getElementById("seccionTomador").style.display=="none")
		{
			mvarTIPODOCU_MDW    = document.getElementById("tipoDocCliente").value
			mvarNUMEDOCU_MDW    = document.getElementById("nroDocumentoCliente").value
		}
		else
		{
			mvarTIPODOCU_MDW    = document.getElementById("tipoDocTomador").value
			mvarNUMEDOCU_MDW    = document.getElementById("nroDocumentoTomador").value
		}
		if (mvarTIPODOCU_MDW=="0"){
			alert_customizado("Debe seleccionar el tipo de documento del titular de la cuenta.")
			return
		}	
		
		if (trim(mvarNUMEDOCU_MDW)==""){
			alert_customizado("Debe ingresar el n�mero de documento del titular de la cuenta.")
			return
		}	
		
		mvarFPAGO_MDW       = document.getElementById("formaPago").value.split("|")[0]
		alert("mvarFPAGO_MDW="+mvarFPAGO_MDW)
		//###########################
		mvarResultado_MDW   = ""
		mvarEstado_MDW      = ""
		
		mvalChannel_MDW     = "OVNYL"
		mvarTokenId_MDW     = ""
		mvarTokenId_MDW     = document.getElementById("TOKENID").value
		
		var mvarTIPODOCU_FIDELITY
		var mvarAlternativeIdentifier
		var mvarTIPODOCU_ADINTAR
		mvarTIPODOCU_ADINTAR = mvarTIPODOCU_MDW
		mvarNUMEDOCU_ADINTAR = mvarNUMEDOCU_MDW
		
		mvarTIPODOCU_FIDELITY = mvarTIPODOCU_MDW
		mvarNUMEDOCU_FIDELITY = trim(mvarNUMEDOCU_MDW.substr(0,8))
		
		mvarAlternativeIdentifier = ""
		
		if (mvarTIPODOCU_MDW == "1"){
		    // DNI
		    mvarTIPODOCU_FIDELITY = "50"
		    mvarTIPODOCU_ADINTAR = "DNI"
		}
		if (mvarTIPODOCU_MDW == "2"){
		    // LC
		    mvarTIPODOCU_FIDELITY = "53"
		    mvarTIPODOCU_ADINTAR = "LC"
		}
		if (mvarTIPODOCU_MDW == "3"){
		    // LE
		    mvarTIPODOCU_FIDELITY = "52"
		    mvarTIPODOCU_ADINTAR = "LE"
		}
		if (mvarTIPODOCU_MDW == "4"){
		    // CUIT
		    mvarTIPODOCU_ADINTAR = "CUT"
		    mvarNUMEDOCU_ADINTAR = mvarNUMEDOCU_MDW.substr(1,12)
		    mvarTIPODOCU_FIDELITY = mvarNUMEDOCU_MDW.substr(1,2)
		    mvarAlternativeIdentifier = mvarNUMEDOCU_MDW.substr(11,1)
		}
		if (mvarTIPODOCU_MDW == "5"){
		    // CUIL 
		    mvarTIPODOCU_ADINTAR = "CUL"
		    mvarNUMEDOCU_ADINTAR = mvarNUMEDOCU_MDW.substr(1,12)
		    mvarTIPODOCU_FIDELITY = mvarNUMEDOCU_MDW.substr(1,2)
		    mvarAlternativeIdentifier = mvarNUMEDOCU_MDW.substr(11,1)
		}
		//---------------------------------------------------------------------------
		
		// Recupero la escala de Colores
		// *****************************
		// Color Rojo
		var pvarDesde_R
		var pvarHasta_R
		// Color Amarillo
		var pvarDesde_A
		var pvarHasta_A
		// Color Verde
		var pvarDesde_V
		var pvarHasta_V
		
		//Color Rojo Desde Hasta
		mvarRequest="<Request>" +
									"<DEFINICION>P_TAB_RANGOS_FORMAS_PAGO_SELECT.xml</DEFINICION>" +
									"<RAMOPCOD>"+ mvarRAMOPCOD_MDW +"</RAMOPCOD>" +
									"<COLOR>"+"R"+"</COLOR>" +
								"</Request>"
													
								
		oXMLDoc= newXMLDOM() 
		
		oXMLDoc.loadXML(llamadaMensajeSQL(mvarRequest))
		if (oXMLDoc.selectSingleNode("//Response/Estado/@resultado"))
		{
	    if (oXMLDoc.selectSingleNode("//Response/Estado/@resultado").value.toUpperCase() == "TRUE")
	    {
        if (oXMLDoc.selectSingleNode("//FPAGO_CONSULTA"))
        {
					pvarDesde_R = Number(oXMLDoc.selectSingleNode("//FPAGO_CONSULTA/RANGO_DESDE").text);
          pvarHasta_R = Number(oXMLDoc.selectSingleNode("//FPAGO_CONSULTA/RANGO_HASTA").text);
					document.getElementById("VARDESDE_R").value=pvarDesde_R;
      		document.getElementById("VARHASTA_R").value=pvarHasta_R;
        }
	    }
		}
		
		//---------------------------------------------------------------------------
		//Color Amarillo Desde Hasta
		mvarRequest=	"<Request>" +
											"<DEFINICION>P_TAB_RANGOS_FORMAS_PAGO_SELECT.xml</DEFINICION>" +
		                	"<RAMOPCOD>"+ mvarRAMOPCOD_MDW +"</RAMOPCOD>" +
		                	"<COLOR>"+"A"+"</COLOR>" +
									"</Request>"
		
		oXMLDoc= newXMLDOM()
		
		oXMLDoc.loadXML(llamadaMensajeSQL(mvarRequest))
	
		if (oXMLDoc.selectSingleNode("//Response/Estado/@resultado"))
		{
	    if (oXMLDoc.selectSingleNode("//Response/Estado/@resultado").value.toUpperCase() == "TRUE")
	    {
        if (oXMLDoc.selectSingleNode("//FPAGO_CONSULTA"))
        {
					pvarDesde_A = Number(oXMLDoc.selectSingleNode("//FPAGO_CONSULTA/RANGO_DESDE").text);
					pvarHasta_A = Number(oXMLDoc.selectSingleNode("//FPAGO_CONSULTA/RANGO_HASTA").text);
					document.getElementById("VARDESDE_A").value=pvarDesde_A;
     			document.getElementById("VARHASTA_A").value=pvarHasta_A;
        }
	    }
		}
		//---------------------------------------------------------------------------
		//Color Verde Desde Hasta
		mvarRequest="<Request>" +
                    "<DEFINICION>P_TAB_RANGOS_FORMAS_PAGO_SELECT.xml</DEFINICION>" +
                    "<RAMOPCOD>"+ mvarRAMOPCOD_MDW +"</RAMOPCOD>" +
                    "<COLOR>"+"V"+"</COLOR>" +
								"</Request>"
		oXMLDoc= newXMLDOM()
		
		
		oXMLDoc.loadXML(llamadaMensajeSQL(mvarRequest))
		
		if (oXMLDoc.selectSingleNode("//Response/Estado/@resultado"))
		{
	    if (oXMLDoc.selectSingleNode("//Response/Estado/@resultado").value.toUpperCase() == "TRUE")
	    {
	      if (oXMLDoc.selectSingleNode("//FPAGO_CONSULTA"))
	      {
					pvarDesde_V = Number(oXMLDoc.selectSingleNode("//FPAGO_CONSULTA/RANGO_DESDE").text);
					pvarHasta_V = Number(oXMLDoc.selectSingleNode("//FPAGO_CONSULTA/RANGO_HASTA").text);
					document.getElementById("VARDESDE_V").value=pvarDesde_V;
	   			document.getElementById("VARHASTA_V").value=pvarHasta_V;
	      }
	    }
		}
	
		//---------------------------------------------------------------------------
		//Armado de Forma de Pago x P�liza Colectiva seleccionada.
		//Busco el valor DB (CBU) dentro del XML para evaluar que tipo de Formas de Pago existen para esa Poliza
	
//		var mvarPOLIZANN=document.getElementById("cboPolizaColectiva").value.split("-")[0]
//		var mvarPOLIZSEC=document.getElementById("cboPolizaColectiva").value.split("-")[1]
//mmc ICB1
		var mvarPOLIZANN=0
		var mvarPOLIZSEC=0
//mmc fin		
		
		//################################
	
			
		//TOKEN
		if (mvarTokenId_MDW == "")
		{
			mvarRequest = "<Request>" +
											"<Operation>generateToken</Operation>" +
											"<TokenUser>" + mvalChannel_MDW + "/" + mvarUSUARCOD_MDW + "</TokenUser>" +
										"</Request>"
			oXMLToken= newXMLDOM()
			
	
			oXMLToken.loadXML(llamadaMensajeMQ(mvarRequest,"lbaw_Frame2MIDD"))
			
			if (oXMLToken.selectSingleNode("//GENERAL/Response/TokenId"))
			{
				if (oXMLToken.selectSingleNode("//GENERAL/ESTADO/@RESULTADO").value.toUpperCase() == "TRUE")
				{
					mvarTokenId_MDW = oXMLToken.selectSingleNode("//GENERAL/Response/TokenId").text
					document.getElementById("TOKENID").value = mvarTokenId_MDW
					mvarEstado_MDW = "OK"
				}
		    else
		    {
					mvarResultado_MDW = "Por el momento no contamos con el servicio que provee las Cuentas / Tarjetas vinculadas al Cliente, por favor, contin�e en forma manual. (E1)"
					mvarEstado_MDW = "ER"
				}
			}
		  else
		  {
				mvarResultado_MDW = "Por el momento no contamos con el servicio que provee las Cuentas / Tarjetas vinculadas al Cliente, por favor, contin�e en forma manual. (E2)"
				mvarEstado_MDW = "ER"
			}
		}
		alert("mvarResultado_MDW="+mvarResultado_MDW)
		
		if (mvarResultado_MDW == "")
		{
			//<!-- BEGIN Consulta de Tarjetas por Cliente -->
			if (mvarFPAGO_MDW == "4")
			{
				mvarRequest = "<Request>"+
					"<Service>" + "InvolvedPartyManagement" + "</Service>"+
					"<Operation>" + "retrieveCardDetailsForIP" + "</Operation>"+
					"<Channel>" + mvalChannel_MDW + "</Channel>"+
					"<UserId>" + mvarUSUARCOD_MDW + "</UserId>"+
					"<TokenId>" + mvarTokenId_MDW + "</TokenId>"+
					"<BusDataSeg>"+
					"<involvedPartyIdentifier>"+
					"<IdfcNum>" + mvarNUMEDOCU_ADINTAR + "</IdfcNum>"+
					"<IdfcType>" + mvarTIPODOCU_ADINTAR + "</IdfcType>"+
					"</involvedPartyIdentifier>"+
					"</BusDataSeg>"+
				"</Request>"
				
				//' Cargo XML para Mostrar en Pantalla
				oXMLTarjetas= newXMLDOM() 
				oXMLTarjetas.loadXML(llamadaMensajeMQ(mvarRequest,"lbaw_Frame2MIDD"))
				
				// Cargo XML para pasar y Grabar
				mvarResponsePasajeXMLFormasPago = oXMLTarjetas.xml
				
				if (oXMLTarjetas.selectSingleNode("//GENERAL/ESTADO/@RESULTADO"))
				{
					if (oXMLTarjetas.selectSingleNode("//GENERAL/ESTADO/@RESULTADO").value.toUpperCase() == "TRUE")
					{
						if (oXMLTarjetas.selectNodes("//CardAccessArrangement").length > 0)
						{
							var xmlObjTarj=oXMLTarjetas.selectNodes("//CardAccessArrangement")
							var qnodos = xmlObjTarj.length
							for(var y=0;y<qnodos ;y++)
							{
								if(xmlObjTarj[y].selectSingleNode("PrimAcctInfo/StatDesc").text.toUpperCase() != "OPERATIVA")
				        	xmlObjTarj[y].parentNode.removeChild(xmlObjTarj[y])
				        else
									mvarContador = mvarContador + 1;
							}
						}
						
						if (mvarContador > 0)
						{
							//Cargo mvarResponsePasajeXMLFormasPago
							mvarEstado_MDW = "OK"
							
							var xmlObjTarjXML = oXMLTarjetas.selectNodes("//CardAccessArrangement")
							var qnodos = xmlObjTarjXML.length
							var mvari
							mvari = 0
							
							mvarRespFP = "<Response>"
							for(var y=0;y<qnodos ;y++)
							{
								mvari = mvari + 1
								if(xmlObjTarjXML[y].selectSingleNode("PrimAcctInfo/RestrCde").text == "7")
								{
	                //Tarjeta <RestrDesc>AMERICAN EXPRESS</RestrDesc> tiene 15 digitos y vienen informados 16 con 1 cero a derecha.
	                //Tomo las primeras 15 posiciones  de Amex para validaciones del AIS
	              	//Despues hay que volver al valor original para pasar al validacion de ADINTAR para al SeccionFPago_VTarjeta.asp
	                xmlObjTarjXML[y].selectSingleNode("CardNum").text = xmlObjTarjXML[y].selectSingleNode("CardNum").text.substring(0,15)
	            	}
								mvarRespFP =  mvarRespFP + "<NODO>" 
									mvarRespFP =  mvarRespFP + "<SECUENCIA>" + mvari + "</SECUENCIA>"                           
									mvarRespFP =  mvarRespFP + "<TIPOCUEN>" + "T" + "</TIPOCUEN>"                           
									mvarRespFP =  mvarRespFP + "<CODIGOADINTAR>" + xmlObjTarjXML[y].selectSingleNode("PrimAcctInfo/RestrCde").text.toUpperCase() + "</CODIGOADINTAR>"
									mvarRespFP =  mvarRespFP + "<DESCADINTAR>" + xmlObjTarjXML[y].selectSingleNode("PrimAcctInfo/RestrDesc").text.toUpperCase() + "</DESCADINTAR>"
									mvarRespFP =  mvarRespFP + "<NUMERO>" + xmlObjTarjXML[y].selectSingleNode("CardNum").text.toUpperCase() + "</NUMERO>"           
									mvarRespFP =  mvarRespFP + "<TITULAR_TIP_DOC>" + xmlObjTarjXML[y].selectSingleNode("TypeIdfcCde").text.toUpperCase() + "</TITULAR_TIP_DOC>"
									mvarRespFP =  mvarRespFP + "<TITULAR_NRO_DOC>" + xmlObjTarjXML[y].selectSingleNode("IdfcNum").text + "</TITULAR_NRO_DOC>"
									mvarRespFP =  mvarRespFP + "<TITULAR>" + "N" + "</TITULAR>"
									mvarRespFP =  mvarRespFP + "<ESTADO>" + mvarEstado_MDW + "</ESTADO>"
									mvarRespFP =  mvarRespFP + "<VENCIANN>" + "0000" + "</VENCIANN>"
									mvarRespFP =  mvarRespFP + "<VENCIMES>" + "00" + "</VENCIMES>"
									mvarRespFP =  mvarRespFP + "<VENCIDIA>" + "00" + "</VENCIDIA>"
									mvarRespFP =  mvarRespFP + "<MODO>" + "A" + "</MODO>"
									mvarRespFP =  mvarRespFP + "<SELECCION>" + "N" + "</SELECCION>"
									mvarRespFP =  mvarRespFP + "<COLOR>" + "-" + "</COLOR>"
									mvarRespFP =  mvarRespFP + "<ORDEN>" + mvari + "</ORDEN>"
									mvarRespFP =  mvarRespFP + "<BANCOCOD>" + "0000" + "</BANCOCOD>"
									mvarRespFP =  mvarRespFP + "<SUCURCOD>" + "0000" + "</SUCURCOD>"
									mvarRespFP =  mvarRespFP + "<COBROTIP>" + "TA" + "</COBROTIP>"
									mvarRespFP =  mvarRespFP + "<COBRODES>" + xmlObjTarjXML[y].selectSingleNode("PrimAcctInfo/RestrDesc").text.toUpperCase() + "</COBRODES>"
								mvarRespFP =  mvarRespFP + "</NODO>"
							}
							mvarRespFP =  mvarRespFP + "</Response>"
							// Paso a variable que lleva los nodos del XML
							mvarResponsePasajeXMLFormasPago = mvarRespFP
							
							//Levanta XSL para armar XML de salida
							oXSLSalida= newXMLDOM() 
							
							
							oXSLSalida=loadXMLDoc(datosCotizacion.getCanalURL() + "Includes/moduloFPago/XSL/moduloFPago_Tarjetas.xsl")
							oXSLValores=loadXMLDoc(datosCotizacion.getCanalURL() + "Forms/XSL/resulCotiPlan.xsl")	
							
							//Transformar y meter en una variable de mvarResultadoHTML
							mvarResultadoHTML = oXMLTarjetas.transformNode(oXSLSalida)
						}
						else
						{
							mvarResponsePasajeXMLFormasPago = ""
							mvarResultado_MDW = "<strong>El Cliente no posee ninguna Tarjeta vinculada.</strong>"
							mvarEstado_MDW = "SD"
							mvarResultadoHTML = mvarResultado_MDW
						}
	    		}
					else
					{
						mvarResponsePasajeXMLFormasPago = ""
						mvarResultado_MDW = "<strong>El Cliente no posee ninguna Tarjeta Vinculada.</strong>"
						mvarEstado_MDW = "ER"
						mvarResultadoHTML = mvarResultado_MDW
					}
				}
				else
				{
					mvarResponsePasajeXMLFormasPago = ""
					mvarResultado_MDW = "Por el momento no contamos con el servicio que provee las Tarjetas vinculadas al Cliente, por favor, contin�e en forma manual. (E4)"
					mvarEstado_MDW = "ER"
					mvarResultadoHTML = mvarResultado_MDW
				}
				
				//09/05/2011 LR - guardo el XML de las Formas de Pago disponibles.-
				document.getElementById("XMLFormasPago").value = mvarResponsePasajeXMLFormasPago;
			}
			//<!-- END Consulta de Tarjetas por Cliente -->
			
			//<!-- Consulta de Posici�n Cuentas por Clientes -->
			if (mvarFPAGO_MDW == "5")
			{
				mvarRequest = "<Request>"
					mvarRequest = mvarRequest + "<Service>" + "InvolvedPartyManagement" + "</Service>"
					mvarRequest = mvarRequest + "<Operation>" + "retrieveProductArrangementDetailsForIP" + "</Operation>"
					mvarRequest = mvarRequest + "<Channel>" + mvalChannel_MDW + "</Channel>"
					mvarRequest = mvarRequest + "<UserId>" + mvarUSUARCOD_MDW + "</UserId>"
					mvarRequest = mvarRequest + "<TokenId>" + mvarTokenId_MDW + "</TokenId>"
					mvarRequest = mvarRequest + "<BusDataSeg>"
					mvarRequest = mvarRequest + "<involvedPartyIdentifier>"
					mvarRequest = mvarRequest + "<uniqueIdentifier>" + mvarNUMEDOCU_FIDELITY + "</uniqueIdentifier>"
					mvarRequest = mvarRequest + "<type>" + mvarTIPODOCU_FIDELITY + "</type>"
					mvarRequest = mvarRequest + "<keyGroup>"
					mvarRequest = mvarRequest + "<alternativeIdentifier>" + mvarAlternativeIdentifier + "</alternativeIdentifier>"
					mvarRequest = mvarRequest + "</keyGroup>"
					mvarRequest = mvarRequest + "</involvedPartyIdentifier>"
					mvarRequest = mvarRequest + "</BusDataSeg>"
				mvarRequest = mvarRequest + "</Request>"
	
				oXMLCuentas=newXMLDOM() 
				
				// Cargo XML para pasar y Grabar
				oXMLCuentas.loadXML(llamadaMensajeMQ(mvarRequest,"lbaw_Frame2MIDD"));
				//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCuentas<textarea>"+oXMLCuentas.xml+"</textarea>")
				if (oXMLCuentas.selectSingleNode("//BusDataSeg/involvedPartyIdentifier/keyGroup/alternativeIdentifier"))
				{
					if (oXMLCuentas.selectSingleNode("//GENERAL/ESTADO/@RESULTADO").value.toUpperCase() == "TRUE")
					{
						//Elimino Tipos de Cuentas que no corresponden (Saco moneda <> $ y Tipo <> a C.Ahorro=ST y Cta Cte=IM)
						if (oXMLCuentas.selectNodes("//productArrangement").length > 0)
						{
							var xmlObjCtas = oXMLCuentas.selectNodes("//productArrangement")
							var qnodos = xmlObjCtas.length
							
							for(var y=0;y<qnodos ;y++)
							{
								// Tipos de Cuentas que no corresponden solo dejo a C.Ahorro = ST y Cta Cte = IM
								if ((xmlObjCtas[y].selectSingleNode("Prod/InstCde").text.toUpperCase() != "ST" &&  xmlObjCtas[y].selectSingleNode("Prod/InstCde").text.toUpperCase() != "IM") || (xmlObjCtas[y].selectSingleNode("FinSvceArr/IntAmt/CrncyCde").text != "080"))
								{
									//window.document.body.insertAdjacentHTML("beforeEnd", "xmlObjCtas[y]1<textarea>"+xmlObjCtas[y].xml+"</textarea>")
									xmlObjCtas[y].parentNode.removeChild(xmlObjCtas[y])
									//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCuentasDelete<textarea>"+oXMLCuentas.xml+"</textarea>")
								}
								else
								{
									//Consulta del Estado y Saldo de Cuenta
									//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCuentasEstado1<textarea>"+oXMLCuentas.xml+"</textarea>")
									mvarBrnchNum_MDW  = xmlObjCtas[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/BrnchNum").text;
									xmlObjCtas[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/AcctNum").text = fncSetLength(trim(xmlObjCtas[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/AcctNum").text),10,"0","R")
									mvarAcctNum_MDW   = xmlObjCtas[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/AcctNum").text
									mvarpCrncyCde_MDW = xmlObjCtas[y].selectSingleNode("FinSvceArr/IntAmt/CrncyCde").text;
									mvarPrdNum_MDW		= xmlObjCtas[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/AcctSfxNum").text;
									
									mvarRequestCtas = "<Request>"
										mvarRequestCtas = mvarRequestCtas + "<Service>" + "ArrangementReporting" + "</Service>"
										mvarRequestCtas = mvarRequestCtas + "<Operation>" + "retrieveAccountBalance" + "</Operation>"
										mvarRequestCtas = mvarRequestCtas + "<Channel>" + mvalChannel_MDW + "</Channel>"
										mvarRequestCtas = mvarRequestCtas + "<UserId>" + mvarUSUARCOD_MDW + "</UserId>"
										mvarRequestCtas = mvarRequestCtas + "<TokenId>" + mvarTokenId_MDW + "</TokenId>"
										mvarRequestCtas = mvarRequestCtas + "<BusDataSeg>"
											mvarRequestCtas = mvarRequestCtas + "<AcctBalInfo>"
												mvarRequestCtas = mvarRequestCtas + "<AcctInfo>"
													mvarRequestCtas = mvarRequestCtas + "<BrnchNum>" + mvarBrnchNum_MDW + "</BrnchNum>"
													mvarRequestCtas = mvarRequestCtas + "<AcctNum>" + mvarAcctNum_MDW + "</AcctNum>"
												mvarRequestCtas = mvarRequestCtas + "</AcctInfo>"
												mvarRequestCtas = mvarRequestCtas + "<PrdNum>"+ mvarPrdNum_MDW +"</PrdNum>"
												mvarRequestCtas = mvarRequestCtas + "<CrncyCde>" + mvarpCrncyCde_MDW +"</CrncyCde>"
												mvarRequestCtas = mvarRequestCtas + "<BankCde>01</BankCde>"
											mvarRequestCtas = mvarRequestCtas + "</AcctBalInfo>"
										mvarRequestCtas = mvarRequestCtas + "</BusDataSeg>"
									mvarRequestCtas = mvarRequestCtas + "</Request>"
									
									oXMLSaldo=newXMLDOM() 
									oXMLSaldo.loadXML(llamadaMensajeMQ(mvarRequestCtas,"lbaw_Frame2MIDD"));
									
									//window.document.body.insertAdjacentHTML("beforeEnd", "mvarRequestCtas<textarea>"+mvarRequestCtas+"</textarea>")
									//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLSaldo<textarea>"+oXMLSaldo.xml+"</textarea>")
									
									if (oXMLSaldo.selectSingleNode("//BusDataSeg/AcctBlncs/BalAmt/Amt"))
									{
										if (oXMLSaldo.selectSingleNode("//GENERAL/ESTADO/@RESULTADO").value.toUpperCase() == "TRUE")
										{
											mvarContador = mvarContador + 1;
											
											if (oXMLSaldo.selectSingleNode("//AcctBlncs/BalAmt/Amt"))
												mvarSaldo_MDW = Number(oXMLSaldo.selectSingleNode("//AcctBlncs/BalAmt/Amt").text);
											else
												mvarSaldo_MDW = 0;
											
											if (oXMLSaldo.selectSingleNode("//AcctBlncs/BalAmt/SgnInd"))
												mvarSigno_MDW = oXMLSaldo.selectSingleNode("//AcctBlncs/BalAmt/SgnInd").text;
											else
												mvarSigno_MDW = "+";
											
											//HRF 20110509 - Ticket 613738
											mvarSobreGiro = false
											if (oXMLSaldo.selectSingleNode("//AcctBlncs/TransfAmt/Amt"))
											{
											    if (oXMLSaldo.selectSingleNode("//AcctBlncs/TransfAmt/SgnInd"))
											    {
											        //Sobregiro
											        mvarSaldo_MDW_SG = Number(oXMLSaldo.selectSingleNode("//AcctBlncs/TransfAmt/Amt").text)
											        mvarSigno_MDW_SG = oXMLSaldo.selectSingleNode("//AcctBlncs/TransfAmt/SgnInd").text
											        //Calculo saldo
											        if ((mvarSaldo_MDW_SG > 0) && (mvarSigno_MDW_SG == "+"))
											            mvarSobreGiro = true
											    }
											}
											mvarCuentaNueva = false
											if (oXMLSaldo.selectSingleNode("//CustInfo/StartBlckListDt"))
											{
											   mvarFecha_alta_cuenta  = oXMLSaldo.selectSingleNode("//CustInfo/StartBlckListDt").text
											   mvarFecha_alta_cuenta = convierteFecha(mvarFecha_alta_cuenta,"AAAA-MM-DD","AAAAMMDD");
											   mvarFecha_Hoy = dateAddExtention(document.getElementById("fechaHoy").value.substr(6,2),document.getElementById("fechaHoy").value.substr(4,2),document.getElementById("fechaHoy").value.substr(0,4),"d","-90")
											   mvarFecha_Hoy = convierteFecha(mvarFecha_Hoy,"DD/MM/AAAA","AAAAMMDD");
											   //mvarAntCta = datediff("d",date,cdate(mid(mvarFecha_alta_cuenta,9,2) &"-"& mid(mvarFecha_alta_cuenta,6,2) &"-"& mid(mvarFecha_alta_cuenta,1,4)))
											   if (mvarFecha_Hoy < mvarFecha_alta_cuenta)
											        mvarCuentaNueva = true
											}
											//FIN HRF 20110509 - Ticket 613738
											
											if (oXMLSaldo.selectSingleNode("//AcctInfo/ProdArr/FixTermDepstArr/StartDt"))
												mvarFecha_ultimo_credito  = oXMLSaldo.selectSingleNode("//AcctInfo/ProdArr/FixTermDepstArr/StartDt").text;
											else
												mvarFecha_ultimo_credito = "";
											
											if (oXMLSaldo.selectSingleNode("//AcctInfo/ProdArr/FixTermDepstArr/MatDt"))
												mvarFecha_ultimo_debito   = oXMLSaldo.selectSingleNode("//AcctInfo/ProdArr/FixTermDepstArr/MatDt").text;
											else
												mvarFecha_ultimo_debito   = "";
											
											if (mvarSigno_MDW == "-")
												mvarSaldo_MDW = (mvarSaldo_MDW * -1);
											
											mvarColor = "";
											//HRF 20110509 - Ticket 613738
											if (mvarSobreGiro || mvarCuentaNueva)
                      	mvarColor = "V"
                      else
                      {
												// Color Rojo
												if (mvarSaldo_MDW <= pvarHasta_R)
													mvarColor = "R";
												
												// Color Amarillo
												if (mvarSaldo_MDW >= pvarDesde_A && mvarSaldo_MDW <= pvarHasta_A)
													mvarColor = "A";
												
												// Color Verde
												if (mvarSaldo_MDW >= pvarDesde_V)
													mvarColor = "V";
											}
											
											// Sumo un nodo llamado "COLOR" con 'R' o 'A' o 'V'
											var oNodoHijo = oXMLCuentas.createElement("COLOR");
											oNodoHijo.text = mvarColor;
											xmlObjCtas[y].selectSingleNode("Prod").appendChild(oNodoHijo);
											
											// Sumo un nodo llamado "ORDEN" 
											oNodoHijo = oXMLCuentas.createElement("ORDEN");
											oNodoHijo.text = mvarSaldo_MDW;
											xmlObjCtas[y].selectSingleNode("Prod").appendChild(oNodoHijo);
											
											// Sumo un nodo llamado "CUENTADES" 
											oNodoHijo = oXMLCuentas.createElement("CUENTADES");
											if (xmlObjCtas[y].selectSingleNode("Prod/InstCde").text.toUpperCase() == "ST")
												oNodoHijo.text = "CAJA DE AHORRO";
											
											if (xmlObjCtas[y].selectSingleNode("Prod/InstCde").text.toUpperCase() == "IM")
												oNodoHijo.text = "CUENTA CORRIENTE";
											xmlObjCtas[y].selectSingleNode("Prod").appendChild(oNodoHijo);
										}
										else
										{
											//window.document.body.insertAdjacentHTML("beforeEnd", "xmlObjCtas[y]1<textarea>"+xmlObjCtas[y].xml+"</textarea>")
											xmlObjCtas[y].parentNode.removeChild(xmlObjCtas[y])
										}
									}
									else
									{
										//window.document.body.insertAdjacentHTML("beforeEnd", "xmlObjCtas[y]1<textarea>"+xmlObjCtas[y].xml+"</textarea>")
										xmlObjCtas[y].parentNode.removeChild(xmlObjCtas[y])
									}
								}
							}
						}
						if  (mvarContador > 0)
						{
							//Cargo mvarResponsePasajeXMLFormasPago
							mvarEstado_MDW = "OK"
	
							mvarRespFP = "<Response>"
							var xmlObjCtasXML = oXMLCuentas.selectNodes("//productArrangement")
							var qnodos=xmlObjCtasXML.length
							mvari = 0
							for(var y=0;y<qnodos ;y++)
							{
								mvari = mvari + 1
								
								mvarAcctNum_MDW   = xmlObjCtasXML[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/AcctNum").text;
								mvarBrnchNum_MDW	= xmlObjCtasXML[y].selectSingleNode("FinSvceArr/AcctIntTrnsfNum/BrnchNum").text;
								
								mvarRespFP =  mvarRespFP + "<NODO>"
								mvarRespFP =  mvarRespFP + "<SECUENCIA>" + mvari + "</SECUENCIA>"
								mvarRespFP =  mvarRespFP + "<TIPOCUEN>" + "C" + "</TIPOCUEN>"
								mvarRespFP =  mvarRespFP + "<CODIGOADINTAR>" + "" + "</CODIGOADINTAR>"
								mvarRespFP =  mvarRespFP + "<DESCADINTAR>" + "" + "</DESCADINTAR>"
								mvarRespFP =  mvarRespFP + "<NUMERO>" + mvarAcctNum_MDW + "</NUMERO>"
								mvarRespFP =  mvarRespFP + "<TITULAR_TIP_DOC>" + mvarTIPODOCU_FIDELITY + "</TITULAR_TIP_DOC>"
								mvarRespFP =  mvarRespFP + "<TITULAR_NRO_DOC>" + mvarNUMEDOCU_FIDELITY + "</TITULAR_NRO_DOC>"
								mvarRespFP =  mvarRespFP + "<TITULAR>" + "N" + "</TITULAR>"
								mvarRespFP =  mvarRespFP + "<ESTADO>" + mvarEstado_MDW + "</ESTADO>"
								mvarRespFP =  mvarRespFP + "<VENCIANN>" + "0000" + "</VENCIANN>"
								mvarRespFP =  mvarRespFP + "<VENCIMES>" + "00" + "</VENCIMES>"
								mvarRespFP =  mvarRespFP + "<VENCIDIA>" + "00" + "</VENCIDIA>"
								mvarRespFP =  mvarRespFP + "<MODO>" + "A" + "</MODO>"
								mvarRespFP =  mvarRespFP + "<SELECCION>" + "N" + "</SELECCION>"
								mvarRespFP =  mvarRespFP + "<COLOR>" + xmlObjCtasXML[y].selectSingleNode("Prod/COLOR").text + "</COLOR>"
								mvarRespFP =  mvarRespFP + "<ORDEN>" + mvari + "</ORDEN>"
								mvarRespFP =  mvarRespFP + "<BANCOCOD>" + "0150" + "</BANCOCOD>"
								mvarRespFP =  mvarRespFP + "<SUCURCOD>" + mvarBrnchNum_MDW + "</SUCURCOD>"
								
								if (xmlObjCtasXML[y].selectSingleNode("Prod/InstCde").text.toUpperCase() == "IM")
									mvarRespFP =  mvarRespFP + "<COBROTIP>" + "CC" + "</COBROTIP>";
								else
									mvarRespFP =  mvarRespFP + "<COBROTIP>" + "CA" + "</COBROTIP>";
	
								mvarRespFP =  mvarRespFP + "<COBRODES>" + xmlObjCtasXML[y].selectSingleNode("Prod/CUENTADES").text.toUpperCase() + "</COBRODES>"
								mvarRespFP =  mvarRespFP + "</NODO>"
							}
							mvarRespFP =  mvarRespFP + "</Response>"
							
							// Paso a variable que lleva los nodos del XML
							mvarResponsePasajeXMLFormasPago = mvarRespFP
							
							//Levanta XSL para armar XML de salida
							oXSLSalida= newXMLDOM() 
						
							oXSLSalida=loadXMLDoc(document.getElementById('XSLFPCuentas').value)
							//window.document.body.insertAdjacentHTML("beforeEnd", "oXMLCuentasEstado2<textarea>"+oXMLCuentas.xml+"</textarea>")
							mvarResultadoHTML = oXMLCuentas.transformNode(oXSLSalida)						
						}
						else
						{
							mvarResponsePasajeXMLFormasPago = ""
							mvarResultado_MDW = "<strong>El Cliente no posee ninguna Cuenta vinculada.</strong>"
							mvarEstado_MDW = "SD" //Sin Datos	
							mvarResultadoHTML=mvarResultado_MDW
						}
					}
					else
					{
						mvarResponsePasajeXMLFormasPago = ""
						mvarResultado_MDW = "<strong>Por el momento no contamos con el servicio que provee las Cuentas vinculadas al Cliente, por favor, contin�e en forma manual. (E5)</strong>"
						mvarEstado_MDW = "ER" // Error en Middleware
						mvarVerDetalle = "" 
					}
					//09/05/2011 LR - guardo el XML de las Formas de Pago disponibles.-
					document.getElementById("XMLFormasPago").value = mvarResponsePasajeXMLFormasPago;
				}
				else
				{
					var mvarVerError = (oXMLCuentas.selectSingleNode("//Response/GENERAL/ESTADO/@MENSAJE").value.toUpperCase())
					var mvarHayError = 0
					//Este Error es que el Cliente no tiene ninguna Cuenta vinculada
					
					mvarHayError = mvarVerError.search("[READNEXT RMXID    NOTFND      ]")
					
					if (mvarHayError != -1){ 
						mvarResultado_MDW = "<strong>El Cliente no posee ninguna Cuenta vinculada.</strong>"
						mvarEstado_MDW = "SD" // Sin Datos
					}    
					else{
						// Otros errores posibles de respuesta sin Servicio pero los tomo por el else
						//mvarHayError = InStr(mvarVerError,"[STARTBR  RMXID    DISABLED") 
						//mvarHayError = InStr(mvarVerError,"[STARTBR  RMXID    NOTOPEN") 
						//mvarHayError = InStr(mvarVerError,"[READ    TSITR   NOTOPEN") 
						//mvarHayError = InStr(mvarVerError,"[READ     RMCST    NOTOPEN") 
						mvarResultado_MDW = "<strong>Por el momento no contamos con el servicio que provee las Cuentas vinculadas al Cliente, por favor, contin�e en forma manual. (E6)</strong>"
						mvarEstado_MDW = "ER" // Error en Middleware
					}
					mvarResultadoHTML=mvarResultado_MDW
	
				}
	
			}
	
		}
			
		document.getElementById("Estado_MDW").value = mvarEstado_MDW;
		alert("mvarEstado_MDW="+mvarEstado_MDW)
		//Mostrar el PopUp de Seleccion de info del medio de Pago
		var pX=595
		var pY= 200
		alert("mvarResultadoHTML="+mvarResultadoHTML)
		tabla=mvarResultadoHTML
		
		document.getElementById("lblMedioPagosVinculados").innerHTML=tabla
		fncColoreoRenglones();
		
		//Cuenta vinculadas				
		if (mvarFPAGO_MDW == "5"){
		        var div='<label id="opciones" for="opciones" style="position:relative">Opciones:</label>'+
	        	'<select size="2"  id="opcionSeleccion" name="OpcionSeleccion" style="width:250; height:30; " onclick="javascript:fncHabilitarCampos('+mvarFPAGO_MDW+',this.value);" >'
			if (mvarContador > 0)
				div=div+'<option VALUE="1" selected>Seleccionar Cuenta Vinculada</option>'+
					'<option VALUE="2">Ingresar Cuenta No Listada  </option>'
			else
				div+='<option VALUE="2" selected>Ingresar Cuenta No Listada  </option>'
				
			div+='</select>'
		}
		//Tarjetas Vinculadas
		else if (mvarFPAGO_MDW == "4"){
		        var div='<label id="opciones" for="opciones" style="position:relative">Opciones:</label>'+
		        	'<select size="2"  id="opcionSeleccion" name="OpcionSeleccion" style="width:250; height:30; " onclick="javascript:fncHabilitarCampos('+mvarFPAGO_MDW+',this.value);">'
				if (mvarContador > 0)
					div=div+'<option VALUE="1" selected>Seleccionar Tarjeta Vinculada</option>'+
						'<option VALUE="2">Ingresar Tarjeta No Listada  </option>'
				else						
					div=div+'<option VALUE="2" selected>Ingresar Tarjeta No Listada  </option>'
		        	
		        	div+='</select>'		
		}
		
	  document.getElementById("TarjCueVinculadas").innerHTML=div
	  fncHabilitarCampos(mvarFPAGO_MDW,document.getElementById("opcionSeleccion").value)
		//+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CANAL HSBC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+//
	}
	else
	{
		/*********************************************************************************************/
		//BEGIN - LR 30/06/20011 - Se agrega para Change Request Nro4: Recup de Archivo Lotes de Clientes
		if(document.getElementById("ACCEDE_LOTE").value == "S")
		{
			var mvarFPAGO = document.getElementById("formaPago").value.split("|")[0]
			var pTipoDoc=document.getElementById("tipoDocCliente").options[document.getElementById("tipoDocCliente").selectedIndex].value
			var pDocumento=document.getElementById("nroDocumentoCliente").value
			
			var strData = '<Request>'
				strData += '<DEFINICION>P_LOTE_BUSQUEDA_CLIENTES.xml</DEFINICION>'
				strData += '<TIPODOCU>'+ pTipoDoc +'</TIPODOCU>'
				strData += '<NUMEDOCU>'+ pDocumento +'</NUMEDOCU>'
				strData += '<CLIENAPE></CLIENAPE>'
				strData += '<CLIENNOM></CLIENNOM>'
			strData += '</Request>'
			
			mobjXMLDoc= newXMLDOM() 
			mobjXMLDoc.loadXML(llamadaMensajeSQL(strData))
			
			document.getElementById("XMLFormasPago").value = mobjXMLDoc.xml
			
			if (mobjXMLDoc.selectSingleNode("//REG"))
			{
				//Tarjetas Vinculadas	
				if (mvarFPAGO == "4")
				{
					document.getElementById("nomTarjetaLote").length = 0;
					//TCM = <option value='MC'>MASTERCARD</option>
					//TCV = <option value='VI'>VISA ARG</option>
					//TCA = <option value='AE'>AMERICAN EXPRESS</option>
					
					fncComboAgregarItem(document.getElementById("nomTarjetaLote"),"SELECCIONAR...","-1",true);
					
					for(var x=1; x<=3 ;x++)
					{
						switch (mobjXMLDoc.selectSingleNode("//TARJETIP"+x).text)
				    {
				    	case "TCM":
							{
								fncComboAgregarItem(document.getElementById("nomTarjetaLote"),"MASTERCARD","MC",false);
							}
							break;
							case "TCV":
							{
								fncComboAgregarItem(document.getElementById("nomTarjetaLote"),"VISA ARG","VI",false);
							}
							break;
							case "TCA":
							{
								fncComboAgregarItem(document.getElementById("nomTarjetaLote"),"AMERICAN EXPRESS","AE",false);
							}
							break;
						}
					}
					//Def 513 LR 07/12/2011 Si no encuentra nada q no lo muestre
					if(document.getElementById("nomTarjetaLote").length > 0)
					{
						document.getElementById("divNomTarjeta").style.display = 'none';
						document.getElementById("divNomTarjetaLote").style.display = 'inline';
						document.getElementById("seccFormaPagoVinc").style.display = 'none';
					}
				}
				//Cuenta vinculadas	
				if (mvarFPAGO == "5")
				{
					recuperaCombo("cboTipoCuenta",mobjXMLDoc.selectSingleNode("//COBROCOD").text)
					document.getElementById("numCuentaDC").value=mobjXMLDoc.selectSingleNode("//CUENTNUM").text;
					document.getElementById("divNumCuentaDC").style.display = 'none';
					document.getElementById("numCuentaDCMask").value = fncEsconderNros(mobjXMLDoc.selectSingleNode("//CUENTNUM").text);
					document.getElementById("divNumCuentaDCMask").style.display = 'inline'
					recuperaCombo("cboSucursalDC",fncSetLength(mobjXMLDoc.selectSingleNode("//COD_SUCU").text,4))
				}
				fncColoreoRenglones()
			}
		}
		//END - LR 30/06/20011 - Se agrega para Change Request Nro4: Recup de Archivo Lotes de Clientes
		/*********************************************************************************************/
	}	
}
/************************************************************************************************/
//BEGIN - LOGICA DE LOTE DE EMPLEADOS
function fncSetTcLOTE(pValue)
{
	//BEGIN - LR 30/06/20011 - Se agrega para Change Request Nro4: Recup de Archivo Lotes de Clientes
	if(document.getElementById("ACCEDE_LOTE").value == "S")
	{
		var vTarvtoMes
		var vTarvtoAnn
		
		mobjXMLDoc= newXMLDOM() 
		mobjXMLDoc.loadXML(document.getElementById("XMLFormasPago").value)
		
		//TCM = <option value='MC'>MASTERCARD</option>
		//TCV = <option value='VI'>VISA ARG</option>
		//TCA = <option value='AE'>AMERICAN EXPRESS</option>
		document.getElementById("nomTarjeta").value = pValue;
		
		document.getElementById("seccFormaPagoVinc").style.display = 'none';
		
		for(var x=1; x<=3 ;x++)
		{
			switch (pValue)
	    {
	    	case "MC":
				{
					if(mobjXMLDoc.selectSingleNode("//TARJETIP"+x).text == 'TCM')
					{
						document.getElementById("numTarjeta").value=mobjXMLDoc.selectSingleNode("//TARJENUM"+x+"[//TARJETIP"+x+"='TCM']").text;
						document.getElementById("divNumTarjeta").style.display = 'none';
						document.getElementById("numTarjetaMask").value = fncEsconderNros(mobjXMLDoc.selectSingleNode("//TARJENUM"+x+"[//TARJETIP"+x+"='TCM']").text);
						document.getElementById("divNumTarjetaMask").style.display = '';
						//Vencimiento Tarjeta
						vTarvtoMes = fncSetLength(mobjXMLDoc.selectSingleNode("//VENCIM"+x+"[//TARJETIP"+x+"='TCM']").text,2,"","R")
						vTarvtoAnn = fncSetLength(mobjXMLDoc.selectSingleNode("//VENCIM"+x+"[//TARJETIP"+x+"='TCM']").text,4,"","L")
						
						if(vTarvtoMes > 0)
							recuperaCombo("tarvtomes",vTarvtoMes)
						if(vTarvtoAnn > 0)
							recuperaCombo("tarvtoann",vTarvtoAnn)
							
						return;
					}
				}
				break;
				case "VI":
				{
					if(mobjXMLDoc.selectSingleNode("//TARJETIP"+x).text == 'TCV')
					{
						document.getElementById("numTarjeta").value=mobjXMLDoc.selectSingleNode("//TARJENUM"+x+"[//TARJETIP"+x+"='TCV']").text;
						document.getElementById("divNumTarjeta").style.display = 'none';
						document.getElementById("numTarjetaMask").value = fncEsconderNros(mobjXMLDoc.selectSingleNode("//TARJENUM"+x+"[//TARJETIP"+x+"='TCV']").text);
						document.getElementById("divNumTarjetaMask").style.display = '';
						//Vencimiento Tarjeta
						vTarvtoMes = fncSetLength(mobjXMLDoc.selectSingleNode("//VENCIM"+x+"[//TARJETIP"+x+"='TCV']").text,2,"","R")
						vTarvtoAnn = fncSetLength(mobjXMLDoc.selectSingleNode("//VENCIM"+x+"[//TARJETIP"+x+"='TCV']").text,4,"","L")
						
						if(vTarvtoMes > 0)
							recuperaCombo("tarvtomes",vTarvtoMes)
						if(vTarvtoAnn > 0)
							recuperaCombo("tarvtoann",vTarvtoAnn)
						
						return;
					}
				}
				break;
				case "AE":
				{
					if(mobjXMLDoc.selectSingleNode("//TARJETIP"+x).text == 'TCA')
					{
						document.getElementById("numTarjeta").value=mobjXMLDoc.selectSingleNode("//TARJENUM"+x+"[//TARJETIP"+x+"='TCA']").text;
						document.getElementById("divNumTarjeta").style.display = 'none';
						document.getElementById("numTarjetaMask").value = fncEsconderNros(mobjXMLDoc.selectSingleNode("//TARJENUM"+x+"[//TARJETIP"+x+"='TCA']").text);
						document.getElementById("divNumTarjetaMask").style.display = '';
						//Vencimiento Tarjeta
						vTarvtoMes = fncSetLength(mobjXMLDoc.selectSingleNode("//VENCIM"+x+"[//TARJETIP"+x+"='TCA']").text,2,"","R")
						vTarvtoAnn = fncSetLength(mobjXMLDoc.selectSingleNode("//VENCIM"+x+"[//TARJETIP"+x+"='TCA']").text,4,"","L")
						
						if(vTarvtoMes > 0)
							recuperaCombo("tarvtomes",vTarvtoMes)
						if(vTarvtoAnn > 0)
							recuperaCombo("tarvtoann",vTarvtoAnn)
						
						return;
					}
				}
				break;
			}
		}
	}
	
}
/************************************************************************************************/
function fncEsconderNros(pValor)
{
	var vLength = pValor.length;
	
	if(vLength > 8)
	{
		var vLengthMed = vLength - 8;
		
		var vNroIzq = fncSetLength(pValor,4,"","L");
		var vNroMed = fncSetLength("",vLengthMed,"X","L");
		var vNroDer = fncSetLength(pValor,4,"","R");
		
		return vNroIzq +''+vNroMed+''+vNroDer;
	}
	else
		return '';
}
/************************************************************************************************/
function fncComboAgregarItem(pCombo, pDescrip, pValor, pSelected)
{
	var vNewOption = document.createElement("option")
	vNewOption.text = pDescrip;
	vNewOption.value = pValor;
	vNewOption.selected = pSelected;
	pCombo.add(vNewOption);
}
/******************************************************************************************/
function fncAgregaFPLote(pTipoFP)
{
// pTipoFP
// TC(Tarjeta Cred / CU (Caja Ahorro-Cuenta Corriente))
	
	if (pTipoFP == "TC")
	{
		document.getElementById("divNomTarjeta").style.display = '';
		document.getElementById("divNomTarjetaLote").style.display = 'none';
		document.getElementById("nomTarjeta").selectedIndex = 0;
		document.getElementById("numTarjeta").value = '';
		document.getElementById("divNumTarjeta").style.display = '';
		document.getElementById("divNumTarjetaMask").style.display = 'none';
		//document.getElementById("tarvtomes").selectedIndex = 0;
		//document.getElementById("tarvtoann").selectedIndex = 0;
		document.getElementById("seccFormaPagoVinc").style.display = 'none';
	}	
	if (pTipoFP == "CU")
	{
		document.getElementById("cboTipoCuenta").selectedIndex = 0;
		document.getElementById("numCuentaDC").value = '';
		document.getElementById("divNumCuentaDC").style.display = 'inline';
		document.getElementById("divNumCuentaDCMask").style.display = 'none';
		document.getElementById("cboSucursalDC").selectedIndex = 0;
		document.getElementById("seccFormaPagoVinc").style.display = 'none';
	}
	
	fncColoreoRenglones()
}	
//END - LOGICA DE LOTE DE EMPLEADOS
/******************************************************************************************/
function fncHabilitarCampos(pFormaPago,pOpcion){

	//Tarjetas Vinculadas - Opcion Tarjetas Vinculadas
	if (pFormaPago=="4" && pOpcion=="1"){
		recuperaCombo("nomTarjeta",-1)
		document.getElementById("nomTarjeta").disabled=true
		document.getElementById("numTarjeta").value=""
		document.getElementById("numTarjeta").readOnly=true
		//GED 03-05-2011 SELECCIONAR BANCO
		document.getElementById("seccionSeleccionarBanco").style.display="none"
		document.getElementById("radioBancoHSBC").checked = true
		
	}
	//Tarjetas Vinculadas - Opcion Tarjetas No Vinculadas
	else if (pFormaPago=="4" && pOpcion=="2"){
		recuperaCombo("nomTarjeta",-1)
		document.getElementById("nomTarjeta").disabled=false
		document.getElementById("numTarjeta").value=""
		document.getElementById("numTarjeta").readOnly=false
		//GED 03-05-2011 SELECCIONAR BANCO
		document.getElementById("seccionSeleccionarBanco").style.display="inline"
		document.getElementById("radioBancoHSBC").checked = true
	}
	//Cuentas Vinculadas - Opcion Cuentas Vinculadas
	else if (pFormaPago=="5" && pOpcion=="1"){
		recuperaCombo("cboTipoCuenta",-1)
		document.getElementById("cboTipoCuenta").disabled=true
		document.getElementById("numCuentaDC").value=""
		document.getElementById("numCuentaDC").readOnly=true
		recuperaCombo("cboSucursalDC",-1)
		document.getElementById("cboSucursalDC").disabled=true
		//GED 03-05-2011 SELECCIONAR BANCO
		document.getElementById("seccionSeleccionarBanco").style.display="none"
		document.getElementById("radioBancoHSBC").checked = true
	}
	else if (pFormaPago=="5" && pOpcion=="2"){
		recuperaCombo("cboTipoCuenta",-1)
		document.getElementById("cboTipoCuenta").disabled=false
		document.getElementById("numCuentaDC").value=""
		document.getElementById("numCuentaDC").readOnly=false
		recuperaCombo("cboSucursalDC",-1)
		document.getElementById("cboSucursalDC").disabled=false
		
		//GED 03-05-2011 SELECCIONAR BANCO
		document.getElementById("seccionSeleccionarBanco").style.display="none"
		document.getElementById("radioBancoHSBC").checked = true
	}
}

/************************************************************************************************/
function fncSetCampos(){
	


	if (document.getElementById("formaPago").value.split("|")[0] ==4){		
		document.getElementById("seccionTarjetaCredito").style.display="inline"
		document.getElementById("seccionPagoCuenta").style.display="none"
		
		document.getElementById("nomTarjeta").disabled=false
		document.getElementById("nomTarjeta").value = -1
		
		document.getElementById("numTarjeta").disabled=false
		document.getElementById("numTarjeta").value = ""	
			
		document.getElementById("tarvtomes").disabled=false
		document.getElementById("tarvtomes").value = 1
				
		document.getElementById("tarvtoann").disabled=false
		//document.getElementById("tarvtoann").value = 0
		
		document.getElementById("cboTipoCuenta").disabled=true
		document.getElementById("numCuentaDC").disabled=true
		document.getElementById("cboSucursalDC").disabled=true
		document.getElementById("numCBU").disabled=true
		
	}
	else if (document.getElementById("formaPago").value.split("|")[0] ==5){
		document.getElementById("seccionTarjetaCredito").style.display="none"
		document.getElementById("seccionPagoCuenta").style.display="inline"
		document.getElementById("nomTarjeta").disabled=true
		document.getElementById("numTarjeta").disabled=true
		document.getElementById("tarvtomes").disabled=true
		document.getElementById("tarvtoann").disabled=true
		
		document.getElementById("cboTipoCuenta").disabled=false
		document.getElementById("cboTipoCuenta").value = -1
		
		document.getElementById("numCuentaDC").disabled=false
		document.getElementById("numCuentaDC").value = ""
		
		document.getElementById("cboSucursalDC").disabled=false
		document.getElementById("cboSucursalDC").value = -1
		
		document.getElementById("numCBU").disabled=false	
		document.getElementById("numCBU").value=""
	}
	else{
		document.getElementById("seccionTarjetaCredito").style.display="none"
		document.getElementById("seccionPagoCuenta").style.display="none"	
	}
	//fncColoreoRenglones();	
		
}
/******************************************************************************************/
function fncVerTomador(pValor){
	if (pValor=="N"){
		document.getElementById("seccionTomador").style.display='inline';
		document.getElementById("apellidoTomador").disabled=false;
		document.getElementById("nombreTomador").disabled=false;
		document.getElementById("tipoDocTomador").disabled=false;
		document.getElementById("nroDocumentoTomador").disabled=false;
		document.getElementById("cboRelacion").disabled=false;
		document.getElementById("lblMedioPagosVinculados").innerHTML=""
		fncSetCampos()
	}	
	else{
		document.getElementById("seccionTomador").style.display='none';
		document.getElementById("apellidoTomador").disabled=true;
		document.getElementById("nombreTomador").disabled=true;
		document.getElementById("tipoDocTomador").disabled=true;
		document.getElementById("nroDocumentoTomador").disabled=true;
		document.getElementById("cboRelacion").disabled=true;
		formaPagoSelFP()
	}	
}
/******************************************************************************************/
function fncTipoCuenta(pElement){
	
	if (pElement.value=="DB"){
		document.getElementById("divNroCBU").style.display=''
		document.getElementById("numCBU").disabled=false
	}	
	else{
		document.getElementById("divNroCBU").style.display='none'
		document.getElementById("numCBU").disabled=true
	}	
}	
/******************************************************************************************/
function fncGrabarSQLIntra()
{
	if(document.getElementById("CANALHSBC").value == "S")
	{
    //' -------------------------------------------------------------------------
    //' Inicio PPCR 2009-00848 - Incorporacion en OV de filtros en Cuentas y TC -
    //' Grabar SQL para Intranet SOLO CANAL 0150 HSBC
    //' -------------------------------------------------------------------------
    //' Recibo XML con el resultado
		
    mvarResponsePasajeXMLFormasPago = document.getElementById("XMLFormasPago").value;
		
    //'Cargo variables de trabajo con Datos
    var mvarNroDoc  =  document.getElementById("nroDocumentoCliente").value;
    var mvarTipoDoc =  document.getElementById("tipoDocCliente").value;
				
    var mvarRAMOPCOD = document.getElementById("SUBPRODUCTO").value==''?document.getElementById("PRODUCTO").value:document.getElementById("SUBPRODUCTO").value;
    var mvarPOLIZANN = fncSetLength(document.getElementById("POLIZANN").value,2)
    var mvarPOLIZSEC = fncSetLength(document.getElementById("POLIZSEC").value,6)
    var mvarCERTIPOL = fncSetLength(document.getElementById("CERTIPOL").value,4)
    var mvarCERTIANN = "0000"
    var mvarCERTISEC = document.getElementById("CERTISEC").value
    var mvarSUPLENUM = "0"

    var mvarCLIENSEC = document.getElementById("CLIENSEC").value==""?"0":document.getElementById("CLIENSEC").value;
    var mvarDOMICSEC = "0" //AVERIGUAR

    //'Datos de tarjeta / CBU
    mvarBANCOCOD = document.getElementById("INSTACOD").value
    mvarSUCURCOD = document.getElementById("cboVenSucursal").value
    mvarCOBROCOD = document.getElementById("formaPago").value.split("|")[1]
    mvarNUMERO   = ""
    
    if(document.getElementById("formaPago").value.split("|")[0] == "4")
    {
			//' Tarjetas
      mvarCUENNUME = document.getElementById("numTarjeta").value
      mvarCOBRODES = document.getElementById("nomTarjeta").options[document.getElementById("nomTarjeta").selectedIndex].text
	    mvarVENCIANN = document.getElementById("tarvtoann").value
	    mvarVENCIMES = document.getElementById("tarvtomes").value
	    mvarVENCIDIA = "01"
	    mvarCOBROTIP = "TA"
	  }
    else
    {
	    mvarVENCIANN = "0000"
	    mvarVENCIMES = "00"
	    mvarVENCIDIA = "00"
	    mvarCOBROTIP = document.getElementById("cboTipoCuenta").value
    }

    if(mvarCOBROTIP == "DB")
    {
      //' CBU
      mvarCOBRODES = "CBU"
	  	mvarCUENNUME = document.getElementById("numCBU").value
    }
   	
    if(mvarCOBROTIP == "CC" || mvarCOBROTIP == "CA")
    {
			if(mvarCOBROTIP == "CC")
				mvarCOBRODES = "CUENTA CORRIENTE"
        
      if(mvarCOBROTIP == "CA")
      	mvarCOBRODES = "CAJA DE AHORRO"
        
     	mvarCUENNUME = document.getElementById("numCuentaDC").value
      //mvarBANCOCOD = document.getElementById("BANCOCOD").value
	    //mvarSUCURCOD = document.getElementById("cbo_vendSucursal").value
    }
		
    mvarFECHASOLI = document.getElementById("fechaHoy").value
    mvarNUMERO  = mvarCUENNUME
    mvarCODEMP = document.getElementById("CODEMP").value
    mvarCODEMPLEADO = document.getElementById("inpVenLegajo").value
    mvarNOMBRE_VEND = trim(document.getElementById("APELLIDOVEND").value) +" "+ trim(document.getElementById("NOMBREVEND").value);
    mvarTIPEMP = document.getElementById("TIPEMP").value
		
    //' Cargo XML para Mostrar en Pantalla
   mobjXMLFormasPago = newXMLDOM() 
    
    //var mobjXMLFormasPago	= new ActiveXObject('MSXML2.DOMDocument');
				//mobjXMLFormasPago.async = false;
				mobjXMLFormasPago.loadXML(mvarResponsePasajeXMLFormasPago);
		
    //' Defino Objetos y variables
    var xmlObjXML
    var mvari
    var mvarseleccion
    var mvartitular

    //If trim(Request.form("NroDocTitular_MDW")) = trim(mvarNroDoc) then
       mvartitular = "S"	//ver esto para NYL
    //else   
    //   mvartitular = "N"
    //end if

    var mvarEstado_MDW = document.getElementById("Estado_MDW").value
    var mvarcolor = "-"	//chequear esto
		
    //' Verifico si exite informacion en el XML
    //window.document.body.insertAdjacentHTML("beforeEnd", "mobjXMLFormasPago1<textarea>"+mobjXMLFormasPago.xml+"</textarea>")
    if(mobjXMLFormasPago.selectSingleNode("//Response/NODO/NUMERO"))
    {
        //' Recorro el XML y marco si alguno fue Seleccionado y si es Titular
        //' Response.Write " Recorro el XML y marco si alguno fue Seleccionado " & "</br>"
        
        mvari = 0
        mvarseleccion = false
				var xmlObjXML = mobjXMLFormasPago.selectNodes("//Response/NODO")
				
				for(var y=0;y<xmlObjXML.length;y++)
      	{
            mvari = mvari + 1
 						
            if(trim(xmlObjXML[y].selectSingleNode("NUMERO").text.toUpperCase()) == trim(mvarNUMERO.toUpperCase()))
            {
               xmlObjXML[y].selectSingleNode("SELECCION").text = "S"
               xmlObjXML[y].selectSingleNode("VENCIANN").text = mvarVENCIANN
               xmlObjXML[y].selectSingleNode("VENCIMES").text = mvarVENCIMES 
               xmlObjXML[y].selectSingleNode("VENCIDIA").text = mvarVENCIDIA
               mvarseleccion = true
            }
            
            if(trim(xmlObjXML[y].selectSingleNode("TITULAR_NRO_DOC").text.toUpperCase()) == trim(mvarNroDoc.toUpperCase()))
               xmlObjXML[y].selectSingleNode("TITULAR").text = "S"
            else   
               xmlObjXML[y].selectSingleNode("TITULAR").text = "N"
        }
        
        if(!mvarseleccion)
        {
					//' Agrego nuevo Nodo ingresado manualmente por el usuario
					//' Response.Write " Agrego nuevo Nodo ingresado manualmente" & "</br>"
					mvari = mvari + 1
					
					mvarRespFP =  mvarRespFP + "<NODO>"
						mvarRespFP =  mvarRespFP + "<SECUENCIA>" + mvari + "</SECUENCIA>"
						if(document.getElementById("formaPago").value.split("|")[0] == "4")
							mvarRespFP =  mvarRespFP + "<TIPOCUEN>" + "T" + "</TIPOCUEN>"
						else
							mvarRespFP =  mvarRespFP + "<TIPOCUEN>" + "C" + "</TIPOCUEN>"
						mvarRespFP =  mvarRespFP + "<CODIGOADINTAR>" + "" + "</CODIGOADINTAR>"
						mvarRespFP =  mvarRespFP + "<DESCADINTAR>" + "" + "</DESCADINTAR>"
						mvarRespFP =  mvarRespFP + "<NUMERO>" + mvarCUENNUME + "</NUMERO>"
						mvarRespFP =  mvarRespFP + "<TITULAR_TIP_DOC>" + mvarTipoDoc + "</TITULAR_TIP_DOC>"
						mvarRespFP =  mvarRespFP + "<TITULAR_NRO_DOC>" + mvarNroDoc + "</TITULAR_NRO_DOC>"
						if(document.getElementById("formaPago").value.split("|")[0] == "4")
							mvarRespFP =  mvarRespFP + "<TITULAR>" + "-" + "</TITULAR>"
						else
							mvarRespFP =  mvarRespFP + "<TITULAR>" + mvartitular + "</TITULAR>"
						mvarRespFP =  mvarRespFP + "<ESTADO>" + mvarEstado_MDW + "</ESTADO>"
						mvarRespFP =  mvarRespFP + "<VENCIANN>" + mvarVENCIANN + "</VENCIANN>"
						mvarRespFP =  mvarRespFP + "<VENCIMES>" + mvarVENCIMES + "</VENCIMES>"
						mvarRespFP =  mvarRespFP + "<VENCIDIA>" + mvarVENCIDIA + "</VENCIDIA>"
						mvarRespFP =  mvarRespFP + "<MODO>" + "M" + "</MODO>"
						mvarRespFP =  mvarRespFP + "<SELECCION>" + "S" + "</SELECCION>"
						mvarRespFP =  mvarRespFP + "<COLOR>" + mvarcolor + "</COLOR>"
						mvarRespFP =  mvarRespFP + "<ORDEN>" + mvari + "</ORDEN>"
						mvarRespFP =  mvarRespFP + "<BANCOCOD>" + mvarBANCOCOD + "</BANCOCOD>"
						mvarRespFP =  mvarRespFP + "<SUCURCOD>" + mvarSUCURCOD + "</SUCURCOD>"
						mvarRespFP =  mvarRespFP + "<COBROTIP>" + mvarCOBROTIP + "</COBROTIP>"
						mvarRespFP =  mvarRespFP + "<COBRODES>" + mvarCOBRODES + "</COBRODES>"
					mvarRespFP =  mvarRespFP + "</NODO>"
				 mvarRespFP =  mvarRespFP + "</Response>"
          
					//' Paso el xml a String y le sumo el nuevo NODO -
					mvarResponsePasajeXMLFormasPago = mvarRespFP.replace("</Response>","")
					
					//' Pasar String a XML
					mobjXMLFormasPago= newXMLDOM() 
					//var mobjXMLFormasPago	= new ActiveXObject('MSXML2.DOMDocument');
						//mobjXMLFormasPago.async = false;
						mobjXMLFormasPago.loadXML(mvarResponsePasajeXMLFormasPago);
				}
    }
    else
    {
			//' Sin NODOS en el XML - Sin Tarjetas / Cuentas Vinculadas o sin servicio
      //' Creo cadena de nodos para el XML
      if (document.getElementById("seccionTomador").style.display=="none"){
				mvarTipoDocu = document.getElementById("tipoDocCliente").value
				mvarNumeDocu=document.getElementById("nroDocumentoCliente").value
			}
			else{
				mvarTipoDocu = document.getElementById("tipoDocTomador").value
				mvarNumeDocu=document.getElementById("nroDocumentoTomador").value
			}
      
			mvarRespFP = "<Response>"
				mvarRespFP =  mvarRespFP + "<NODO>"
					mvarRespFP =  mvarRespFP + "<SECUENCIA>" + "1"+ "</SECUENCIA>"
					if(document.getElementById("formaPago").value.split("|")[0] == "4")
					    mvarRespFP =  mvarRespFP + "<TIPOCUEN>" + "T" + "</TIPOCUEN>"
					else
					    mvarRespFP =  mvarRespFP + "<TIPOCUEN>" + "C" + "</TIPOCUEN>"
					mvarRespFP =  mvarRespFP + "<CODIGOADINTAR>" + "" + "</CODIGOADINTAR>"
					mvarRespFP =  mvarRespFP + "<DESCADINTAR>" + "" + "</DESCADINTAR>"
					mvarRespFP =  mvarRespFP + "<NUMERO>" + mvarCUENNUME + "</NUMERO>"
					mvarRespFP =  mvarRespFP + "<TITULAR_TIP_DOC>" + mvarTipoDocu + "</TITULAR_TIP_DOC>"
					mvarRespFP =  mvarRespFP + "<TITULAR_NRO_DOC>" + mvarNumeDocu + "</TITULAR_NRO_DOC>"
					if(document.getElementById("formaPago").value.split("|")[0] == "4")
					   mvarRespFP =  mvarRespFP + "<TITULAR>" + "-" + "</TITULAR>"
					else
					   mvarRespFP =  mvarRespFP + "<TITULAR>" + mvartitular + "</TITULAR>"
					mvarRespFP =  mvarRespFP + "<ESTADO>" + mvarEstado_MDW + "</ESTADO>"
					mvarRespFP =  mvarRespFP + "<VENCIANN>" + mvarVENCIANN + "</VENCIANN>"
					mvarRespFP =  mvarRespFP + "<VENCIMES>" + mvarVENCIMES + "</VENCIMES>"
					mvarRespFP =  mvarRespFP + "<VENCIDIA>" + mvarVENCIDIA + "</VENCIDIA>"
					mvarRespFP =  mvarRespFP + "<MODO>" + "M" + "</MODO>"
					mvarRespFP =  mvarRespFP + "<SELECCION>" + "S" + "</SELECCION>"
					mvarRespFP =  mvarRespFP + "<COLOR>" + mvarcolor + "</COLOR>"
					mvarRespFP =  mvarRespFP + "<ORDEN>" + "1" + "</ORDEN>"
					mvarRespFP =  mvarRespFP + "<BANCOCOD>" + mvarBANCOCOD + "</BANCOCOD>"
					mvarRespFP =  mvarRespFP + "<SUCURCOD>" + mvarSUCURCOD + "</SUCURCOD>"
					mvarRespFP =  mvarRespFP + "<COBROTIP>" + mvarCOBROTIP + "</COBROTIP>"
					mvarRespFP =  mvarRespFP + "<COBRODES>" + mvarCOBRODES + "</COBRODES>"
				mvarRespFP =  mvarRespFP + "</NODO>"
			mvarRespFP =  mvarRespFP + "</Response>"
			mvarResponsePasajeXMLFormasPago = mvarRespFP
			
			//' Pasar a XML
			mobjXMLFormasPago= newXMLDOM() 
			//var mobjXMLFormasPago	= new ActiveXObject("MSXML2.DOMDocument");
					//mobjXMLFormasPago.async = false;
					mobjXMLFormasPago.loadXML(mvarResponsePasajeXMLFormasPago);
    }

    //' Recorro para Grabar en la Tabla PROD_SOLIC_FORMAS_PAGO
    mvari = 0
    
    var xmlObjXML = mobjXMLFormasPago.selectNodes("//Response/NODO")
				
		for(var y=0;y<xmlObjXML.length;y++)
		{
        mvari = mvari + 1
				           
        mvarRequest = "<RAMOPCOD>" + mvarRAMOPCOD + "</RAMOPCOD>"											//'Type="VarChar" Size="4"/>
        mvarRequest = mvarRequest + "<POLIZANN>" + mvarPOLIZANN + "</POLIZANN>"       //'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<POLIZSEC>" + mvarPOLIZSEC + "</POLIZSEC>"       //'Type="Integer" Size="6"/>
        mvarRequest = mvarRequest + "<CERTIPOL>" + mvarCERTIPOL + "</CERTIPOL>"       //'Type="Integer" Size="4"/>
        mvarRequest = mvarRequest + "<CERTIANN>" + mvarCERTIANN + "</CERTIANN>"       //'Type="Integer" Size="4"/>
        mvarRequest = mvarRequest + "<CERTISEC>" + mvarCERTISEC + "</CERTISEC>"
        mvarRequest = mvarRequest + "<SUPLENUM>" + mvarSUPLENUM + "</SUPLENUM>"       //'Type="Integer" Size="4"/>
        pSECUENCIA = xmlObjXML[y].selectSingleNode("SECUENCIA").text;
        mvarRequest = mvarRequest + "<SECUENCIA>"+ pSECUENCIA +"</SECUENCIA>"         //'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<FECHASOLI>" + mvarFECHASOLI + "</FECHASOLI>"    //'Type="Char" Size="8"/>
        mvarRequest = mvarRequest + "<DOMICSEC>" + mvarDOMICSEC + "</DOMICSEC>"       //'Type="Integer" Size="3"/>
        mvarRequest = mvarRequest + "<INSTACOD>"+ mvarBANCOCOD +"</INSTACOD>"         //'Type="Integer" Size="4"/>
        mvarRequest = mvarRequest + "<BANCOCOD>" + mvarBANCOCOD + "</BANCOCOD>"
        mvarRequest = mvarRequest + "<SUCURCOD>" + mvarSUCURCOD + "</SUCURCOD>"
        mvarRequest = mvarRequest + "<EMPRESA_COD>" + mvarCODEMP + "</EMPRESA_COD>"
        mvarRequest = mvarRequest + "<NROLEGVEN>" + mvarCODEMPLEADO + "</NROLEGVEN>"
        mvarRequest = mvarRequest + "<APENOM_VEND>" + mvarNOMBRE_VEND + "</APENOM_VEND>"
        mvarRequest = mvarRequest + "<EMPL>" + mvarTIPEMP + "</EMPL>"
        mvarRequest = mvarRequest + "<CLIENSEC>" + mvarCLIENSEC + "</CLIENSEC>"        															//'Type="Integer" Size="9"/>
        mvarRequest = mvarRequest + "<TITULAR>"+ xmlObjXML[y].selectSingleNode("TITULAR").text  +"</TITULAR>"				//'Type="Integer" Size="1"/>
        mvarRequest = mvarRequest + "<CUENSEC>"+ xmlObjXML[y].selectSingleNode("SECUENCIA").text +"</CUENSEC>"      //'Type="Integer" Size="3"/>
        mvarRequest = mvarRequest + "<TIPOCUEN>"+ xmlObjXML[y].selectSingleNode("TIPOCUEN").text +"</TIPOCUEN>"     //'Type="Integer" Size="1"/>
        mvarRequest = mvarRequest + "<COBROTIP>"+ xmlObjXML[y].selectSingleNode("COBROTIP").text +"</COBROTIP>"     //'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<COBRODES>"+ xmlObjXML[y].selectSingleNode("COBRODES").text +"</COBRODES>"     //'Type="Integer" Size="20"/>
        mvarRequest = mvarRequest + "<CUENTDC></CUENTDC>"      																										//'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<CUENNUME>"+ trim(xmlObjXML[y].selectSingleNode("NUMERO").text) +"</CUENNUME>"	//'Type="Integer" Size="22"/>
        mvarRequest = mvarRequest + "<TARJECOD>"+ xmlObjXML[y].selectSingleNode("COBROTIP").text +"</TARJECOD>"     //'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<VENCIANN>"+ xmlObjXML[y].selectSingleNode("VENCIANN").text +"</VENCIANN>"     //'Type="Integer" Size="4"/>
        mvarRequest = mvarRequest + "<VENCIMES>"+ xmlObjXML[y].selectSingleNode("VENCIMES").text +"</VENCIMES>"     //'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<VENCIDIA>"+ xmlObjXML[y].selectSingleNode("VENCIDIA").text +"</VENCIDIA>"     //'Type="Integer" Size="2"/>
        mvarRequest = mvarRequest + "<ORDEN>"+ mvari + "</ORDEN>"                      															//'Type="Decimal"  Precision="15" Scale="2"/>
        mvarRequest = mvarRequest + "<COLOR>"+ xmlObjXML[y].selectSingleNode("COLOR").text + "</COLOR>"             //'Type="VarChar" Size="1"/>
        mvarRequest = mvarRequest + "<SELECCION>"+ xmlObjXML[y].selectSingleNode("SELECCION").text + "</SELECCION>"	//'Type="VarChar" Size="1"/>
        mvarRequest = mvarRequest + "<MODO>"+ xmlObjXML[y].selectSingleNode("MODO").text + "</MODO>"                //'Type="VarChar" Size="1"/>
        mvarRequest = mvarRequest + "<ESTADO>"+ xmlObjXML[y].selectSingleNode("ESTADO").text + "</ESTADO>"          //'Type="VarChar" Size="2"/>
				
        mvarRequestFPago="<Request>" + "<DEFINICION>P_PROD_SOLIC_FORMAS_PAGO_INSERT.xml</DEFINICION>" + mvarRequest + "</Request>"
				
				mobjXMLDoc= newXMLDOM() 
				//mobjXMLDoc	= new ActiveXObject('MSXML2.DOMDocument');    
				//mobjXMLDoc.async = false
				
				mobjXMLDoc.loadXML(llamadaMensajeSQL_LBA(mvarRequestFPago))
				
				//if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado"))
				//{
			  //  if (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text.toUpperCase() == "TRUE")
			  //  	alert("todo bien!")
			  //  else
				// 		alert(" Error al Grabar Detalle de Formas de Pago en SQL")
				//}
				//window.document.body.insertAdjacentHTML("beforeEnd", "xmlObjCtas["+y+"]<textarea>"+mobjXMLDoc.xml+"</textarea>")
				//window.document.body.insertAdjacentHTML("beforeEnd", "mvarRequestFPago["+y+"]<textarea>"+mvarRequestFPago+"</textarea>")
		}
    //' Fin PPCR 2009-00848 - Incorporacion en OV de filtros en Cuentas y TC
    //' -------------------------------------------------------------------------
	}
}
/******************************************************************************************/
