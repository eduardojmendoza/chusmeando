<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 13/12/2011
' PPCR: 50055/6010661
' Desarrollador: Mart�n Cabrera
' Descripci�n: Etapa6: Soporte a Firefox
'--------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: SeccionFPago_inc.asp
' 
' Fecha de Creaci�n: 02/05/2011
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Leonardo Ruiz
' 
' Descripci�n: Captura de Cuentas relacionados al Cliente.
'			   Servicios via Middleware a consultas de Fidelity.
'              Componente= Frame2Midd.
' 
' ------------------------------------------------------------------------------
%>


<%

'*********************************************************
'************ Funciones ASP del m�dulo *******************
'*********************************************************


'*****************************************
function FormatearFechaGenerica(mvarFecha)

	FormatearFechaGenerica="0"
	
	if (len(mvarFecha)>= 8 ) or ( len(mvarFecha) <=10) then
		pos1=Instr(1,mvarFecha, "/")
		pos2=Instr(3,mvarFecha, "/")
		if (pos1<> 0) and (pos2<>0) then 
			
			mvarArr=split(mvarFecha,"/")
		
    		FormatearFechaGenerica=mvarArr(2) & right("0" & mvarArr(1),2) & right("0" & mvarArr(0),2)
    	end if
		
	end if

end Function 

'*****************************************
'mvarCANALURL="."

'*********************************************************************************
'************ Variables de Ubicacion de dependencias *****************************
'*****Importante cambiar cuando se importe  este modulo global en la aplicacion***
'*********************************************************************************
mvarIncludesCSS = mvarCANALURL & "/Includes/moduloFPago"

' mvarIncludesXSL ==> Ubicacion de XSL
mvarIncludesXSL = mvarCANALURL & "/Includes/moduloFPago/XSL"

' mvarIncludesJS ==> Ubicacion de las librerias JS 
mvarIncludesJS = mvarCANALURL & "/Includes/moduloFPago"

' mvarURLImages ==> Ubicacion de los archivos de imagenes en la aplicacion que utilice este m�dulo
mvarURLImages = mvarCANALURL & "/Includes/moduloFPago/images"

'mvarURLCalendario ==> Ubicacion de los archivos de la libreria de calendario en la aplicacion que utilice este m�dulo
mvarURLCalendario = mvarCANALURL & "/Includes/moduloFPago/calendario"

mvarURLXMLEquiv=   mvarCANALURL & "/Includes/moduloFPago"

mvarCANALURLASP= left( replace(mvarCANALURL,"/","\"),len(mvarCANALURL)-1)

Dim mvarSucursales, mobjResponse, mCount, mvarTarjetas

' DEFECT 418 SE ORDENA COMBO ALFABETICAMENTE

mvarTarjetas=   "<option tarjecod='-1'	value='-1'>SELECCIONAR...</option>" & _
                 "<option tarjecod='05'	value='AE'>AMERICAN EXPRESS</option>" & _
                "<option tarjecod='01'	value='AC'>ARGENCARD</option>" & _                
                 "<option tarjecod='14'	value='BA'>BANELCO</option>" & _ 
                 "<option tarjecod='04'	value='BI'>BISEL</option>" & _
                   "<option tarjecod='09'	value='CB'>CABAL</option>" & _
                  "<option tarjecod='10'	value='CR'>CARTA CREDENCIAL</option>" & _
                   "<option tarjecod='06'	value='CF'>CARTA FRANCA</option>" & _
                   "<option tarjecod='03'	value='DC'>DINERS CLUB</option>" & _
                "<option tarjecod='02'	value='MC'>MASTERCARD</option>" & _
                 "<option tarjecod='13'	value='MR'>MULTIRED</option>" & _
                  "<option tarjecod='12'	value='PT'>PATAGONIA</option>" & _
                 "<option tarjecod='11'	value='PV'>PROVENCRED</option>" & _
                 "<option tarjecod='15'	value='TN'>TARJETA NARANJA</option>" & _
                "<option tarjecod='16'	value='NE'>TARJETA NEVADA</option>" & _               
                "<option tarjecod='07'	value='VI'>VISA ARG</option>" & _
                "<option tarjecod='08'	value='VI'>VISA BANCO PROV</option>"
              
%>

<!-- Incluye librerias de acceso a base de Forma de Pago  -->
 
<link type="text/css" rel="stylesheet" href="<%=mvarIncludesCSS%>/SeccionFPago.css" />
<script type="text/javascript"  src="<%=mvarIncludesJS%>/SeccionFPago.js"></script>

<!-- Paso Parametros de Colores para ser vistos por otras paginas -->

<input type="hidden" id="VARDESDE_R" name="VARDESDE_R" value="">
<input type="hidden" id="VARHASTA_R" name="VARHASTA_R" value="">

<input type="hidden" id="VARDESDE_A" name="VARDESDE_A" value="">
<input type="hidden" id="VARHASTA_A" name="VARHASTA_A" value="">

<input type="hidden" id="VARDESDE_V" name="VARDESDE_V" value="">
<input type="hidden" id="VARHASTA_V" name="VARHASTA_V" value="">

<input type="hidden" id="CONTADOR" name="CONTADOR" value="">

<!-- PPCR 2009-00848 DATOS DE FORMAS DE PAGO para Grabar -->
<!-- Inicio PPCR 2009-00848 DATOS DE FORMAS DE PAGO para Grabar -->
<input type="hidden" id="XMLFormasPago" name="XMLFormasPago" value=""/>

<input type="hidden" id="Resultado_MDW" name="Resultado_MDW"    value=""/>
<input type="hidden" id="Estado_MDW" name="Estado_MDW"          value=""/>
<input type="hidden" id="Saldo_MDW" name="Saldo_MDW"            value=""/>
<input type="hidden" id="Fecha_ultimo_credito_MDW" name="Fecha_ultimo_credito_MDW"  value=""/>
<input type="hidden" id="Fecha_ultimo_debito_MDW" name="Fecha_ultimo_debito_MDW"    value=""/>
<input type="hidden" id="NroDocTitular_MDW" name="NroDocTitular_MDW"                value=""/>
<input type="hidden" id="Color_MDW" name="Color_MDW"                                value=""/>

<!-- Cuentas Vinculadas  -->
<!--xml id='XSLCuentas' src='<%=mvarCANALURL%>Forms/XSL/BT_SPPRODUCTOSCLIENTE.xsl'></xml-->
<textarea id="XSLCuentas" style="display: none"><%=mvarCANALURL%>Forms/XSL/BT_SPPRODUCTOSCLIENTE.xsl</textarea>

<!--xml id='XSLFPTarjetas' src='<%=mvarCANALURL%>Includes/moduloFPago/XSL/moduloFPago_Tarjetas.xsl'></xml-->
<textarea id="XSLFPTarjetas" style="display: none"><%=mvarCANALURL%>Includes/moduloFPago/XSL/moduloFPago_Tarjetas.xsl</textarea>


<!--xml id='XSLFPCuentas' src='<%=mvarCANALURL%>Includes/moduloFPago/XSL/moduloFPago_Ctas.xsl'></xml-->
<textarea id="XSLFPCuentas" style="display: none"><%=mvarCANALURL%>Includes/moduloFPago/XSL/moduloFPago_Ctas.xsl</textarea>



<!-- Fin PPCR 2009-00848 DATOS DE FORMAS DE PAGO para Grabar -->
<!-- --------------------------------------------------- -->

<input type="hidden" name="TOKENID"  id="TOKENID"   value="<%= session("TokenId") %>"/>

<!--xml id="oXMLEquivMFPago" src="<%=mvarURLXMLEquiv%>/CodigosEquivalencias.xml"></xml-->

 <!-- FORMA DE PAGO -->
    <div class="areaDatTit">
        <div class="areaDatTitTex">Forma de Pago</div>
    </div>

  
<div id="seccionFormaPago" style="display:inline" >

    <!-- TOMADOR -->
    <div class="areaDatCpoConInp" id="selTomador">
        <label id="lblTomador_etiq" for="lblTomador" style="width:280px; padding-left:10px; padding-bottom:4px; padding-right:0px; margin-right: 0px;">El tomador es titular del Medio de Pago?</label>
        <label style="width:20px; padding-bottom:4px; padding-right:0px; margin-right: 0px; vertical-align:middle ">SI</label>
        <input type="radio" style="vertical-align:middle"  name="verTomador" id="verTomador" value="S" onclick="fncVerTomador('S');" checked />
        <label style="width:20px; padding-bottom:4px; padding-right:0px; margin-right: 0px;vertical-align:middle" >NO</label>
        <input type="radio" style="vertical-align:middle"  name="verTomador" id="verTomador" value="N" onclick="fncVerTomador('N');"/>            
    </div>

    <div id="seccionTomador" style="display:none">
        <div class="areaDatCpoConInp">
            <!-- AP/RAZON SOCIAL -->
            <label id="apellidoTomador_etiq" for="apellidoTomador">Apellido/s:</label>
            <input class="areaDatCpoConInpFieM" id="apellidoTomador" name="apellidoTomador" type="text" disabled="disabled"/>
            <!-- NOMBRE -->
            <label id="nombreTomador_etiq" for="nombreTomador">Nombre/s:</label>
            <input class="areaDatCpoConInpFieM" id="nombreTomador" name="nombreTomador" type="text" disabled="disabled"/>
        </div>
        <div class="areaDatCpoConInp">
            <!-- TIPO DOCUMENTO -->
            <label id="tipoDocTomador_etiq" for="tipoDocTomador">Tipo de Documento:</label>
            <select class="areaDatCpoConInpFieM" id="tipoDocTomador" name="tipoDocTomador" disabled="disabled" >                        
                <option value="0">Seleccionar...</option>
                    <%= mvarOptTDoc %>
            </select>
            
            <!-- NRO DOCUMENTO -->
            <label  id="nroDocumentoTomador_etiq" for="nroDocumentoTomador">N&uacute;mero Documento:</label>
            <input class="areaDatCpoConInpFieM" id="nroDocumentoTomador" name="nroDocumentoTomador" type="text" validarTipeo="numero" validar="&gt; 0" mensajeError="Nro de documento Inv�lido"  maxlength="11" onchange="" onkeypress="capturaTecla()" validacionPersonalizada="fncValNroDocCUIT('nroDocumentoTomador','tipoDocTomador');" disabled="disabled">
        </div>

        <div class="areaDatCpoConInp">
            <label id="cboRelacion_etiq" for="cboRelacion" style="width:483px; padding-left:10px; padding-bottom:2px; padding-right:0px; margin-right: 0px;"> Relaci&oacute;n del Titular del Medio de Pago con el asegurado</label>
            <select class="areaDatCpoConInpFieM" id="cboRelacion" name="cboRelacion" disabled="disabled">
            </select>            
        </div>
        <div id="seccFormaPagoVinc" class="areaDatCpoConInp">
            <span>
                <a href="javascript:formaPagoSelFP();" class="linksubir"> Ver Formas de Pago Vinculados</a>
            </span>
        </div>
    </div>
    
   
    
    <div class="areaDatCpoConInp" >
        <!-- MEDIO DE PAGO -->
<!--        <label id="formaPago_etiq" for="formaPago">Medio de Pago:</label>
        <select class="areaDatCpoConInpFieM" id="formaPago" name="formaPago" onchange="fncSetCampos();formaPagoSel();">
		    <option value="4" selected>TARJETA DE CREDITO</option>
		    <option value="5" >CUENTA BANCARIA</option>        
        </select>     -->
       <div class="areaDatCpoConInp" >
             <!-- BANCO -->
            <label id="SolimedioPago_etiq" for="SolimedioPago">Medio de Pago:</label>
            <strong><label id="lbSolimedioPago" style="width:250"></label></strong>
       </div>

    </div>        
    
    
    <div class="areaDatCpoConInp" >
        <div id="TarjCueVinculadas" style="<%if mvarCANALHSBC="S" then response.write "display:inline" else response.write "display:none" end if %>"></div>
    </div>
    <div class="areaDatCpoConInp" style="<%if mvarCANALHSBC="S" then response.write "display:inline" else response.write "display:none" end if %>">
        <!-- Secci�n encargada de mostrar las cuentas o tarjetas vinculadas -->        
        <label id="lblMedioPagosVinculados" for="lblMedioPagosVinculados" style="width:280px; padding-left:10px; padding-bottom:4px; padding-right:0px; margin-right: 0px;" ></label>
        <!-- ---- -->
    </div> 
   
   <!--GED 03-05-2011 SELECCIONAR BANCO EMISOR -->
   
   <div id="seccionSeleccionarBanco"  style="display:none">
       
        <div class="areaDatCpoConInp">
            <label style="width: 150px; padding-bottom: 4px; padding-right: 0px; margin-right: 0px;">
               Banco Emisor:
            </label>
            <label style="width:180px; padding-bottom:4px; padding-right:0px; margin-right: 0px; vertical-align:middle"> HSBC Bank Argentina S.A. </label> 
            <input type="radio" style="vertical-align:middle" name="BANCO_EMISOR" id="radioBancoHSBC"  checked />
            <label style="width: 100px; padding-bottom: 4px; padding-right: 0px; margin-right: 0px; vertical-align:middle "> Otro Banco </label>
            <input type="radio" style="vertical-align:middle" name="BANCO_EMISOR" id="radioOtroBanco"/>
       
    </div>
   
   </div>      
   
   <!--  FIN SELECCIONAR BANCO EMISOR -->
    
    <div id="seccionTarjetaCredito" style="display:inline" >
        <div class="areaDatCpoConInp" id="divNomTarjeta" style="width:100%">
            <!-- NOMBRE TARJETA -->
            <label id="nomTarjeta_etiq" for="nomTarjeta">Marca:</label>
            <select class="areaDatCpoConInpFieM" id="nomTarjeta" name="nomTarjeta">
            <%=mvarTarjetas%>
            </select>
        </div>
        <div class="areaDatCpoConInp" id="divNomTarjetaLote" style="display:none;width:100%">
            <!-- NOMBRE TARJETA -->
            <label id="nomTarjetaLote_etiq" for="nomTarjetaLote">Marca:</label>
            <select class="areaDatCpoConInpFieM" id="nomTarjetaLote" name="nomTarjetaLote" onchange="javascript:fncSetTcLOTE(this.value)">
            </select>
        </div>
        
        <div class="areaDatCpoConInp" id="divNumTarjeta" style="width:100%">
            <!-- NUMERO TARJETA -->
            <label id="numTarjeta_etiq" for="numTarjeta">N&uacute;mero Tarjeta:</label>
            <input class="areaDatCpoConInpFieM" id="numTarjeta" name="numTarjeta"  onkeypress="capturaTecla()" validarTipeo="numero" validacionPersonalizada="fncValidar_Tarjetas()" type="text" maxlength="22">
            <%If mvarACCEDE_LOTE="S" Then%>
            <span id="btonRecuperTC" style="display:none">
                <a href="javascript:formaPagoSel();" class="linksubir"><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_buscar.gif" align="absmiddle" border="0"> Recuperar Tarjeta</a>
            </span>
	        <%End If%>
        </div>
        <div class="areaDatCpoConInp" id="divNumTarjetaMask" style="display:none; width:100%">
            <!-- MASCARA DE NUMERO TARJETA -->
            <label id="numTarjetaMask_etiq" for="numTarjetaMask">N&uacute;mero Tarjeta:</label>
            <input class="areaDatCpoConInpFieM" id="numTarjetaMask" name="numTarjetaMask" type="text" maxlength="22" disabled="disabled">
            <%If mvarACCEDE_LOTE="S" Then%>
            <span id="btonAgregarTC" style="display:inline">
                <a href="javascript:fncAgregaFPLote('TC');" class="linksubir"><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_buscar.gif" align="absmiddle" border="0"> Agregar Tarjeta</a>
            </span>
	        <%End If%>
        </div>

        <div class="areaDatCpoConInp" >
            <!-- VENCIMIENTO-->
            <label id="tarvtomes_etiq" for="tarvtomes">Vencimiento Mes:</label>
            <select class=form_drops name="tarvtomes" id="tarvtomes" size="1"  >
            <%	
                for mCount=1 to 12
            %>
                <option value="<%=mCount%>" <%If mCount=DatePart("m", Date()) Then%>selected <%End If%>><%=right("0" & mCount,2)%></option>
		    <%  
		        next
            %>
            </select>
            <label id="tarvtoann_etiq" for="tarvtoann">Vencimiento A�o:</label>
            <select class=form_drops name="tarvtoann"  id="tarvtoann" size="1" >
		    <%	
		        for mCount=0 to 10									
		    %>
		        <option value="<%=mCount+year(now())%>"><%=mCount+year(now())%></option>
		    <%
		        next
		    %>
            </select>																       
        </div>
    </div>
    
    <!-- DEBITO EN CUENTA -->
    <div id="seccionPagoCuenta" style="display:none" >
        <div class="areaDatCpoConInp">
            <!-- TIPO CUENTA -->
            <label id="cboTipoCuenta_etiq" for="cboTipoCuenta">Tipo:</label>
                <select class="areaDatCpoConInpFieM" id="cboTipoCuenta" name="cboTipoCuenta" onchange="fncTipoCuenta(this);" disabled="disabled" >
						<option value="-1" valorSql="0">SELECCIONAR</option>
						<option value="CA" valorSql="1">CAJA DE AHORRO HSBC</option>
						<option value="CC" valorSql="2">CUENTA CORRIENTE HSBC</option>
                </select>            
                <label id="lbBancoDC" style="width:320px"><strong>HSBC BANK ARGENTINA S.A.</strong></label>            
        </div>       
        <div class="areaDatCpoConInp" id="divNumCuentaDC" style="width:100%">
            <!-- NUMERO DE CUENTA -->
            <label id="numCuentaDC_etiq" for="numCuentaDC">N&uacute;mero Cuenta:</label>
            <input class="areaDatCpoConInpFieM" id="numCuentaDC" name="numCuentaDC" validacionPersonalizada="fncValidar_Cuentas()" type="text" maxlength="22" disabled="disabled"/>
	    </div>
	    <div class="areaDatCpoConInp" id="divNumCuentaDCMask" style="display:none;width:100%">
            <!-- MASCARA DE NUMERO DE CUENTA -->
            <label id="numCuentaDCMask_etiq" for="numCuentaDCMask">N&uacute;mero Cuenta:</label>
            <input class="areaDatCpoConInpFieM" id="numCuentaDCMask" name="numCuentaDCMask" type="text" maxlength="22" disabled="disabled"/>
            <%If mvarACCEDE_LOTE="S" Then%>
            <span style="display:inline">
                <a href="javascript:fncAgregaFPLote('CU');" class="linksubir"><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_buscar.gif" align="absmiddle" border="0"> Agregar Cuenta</a>
            </span>
	        <%End If%>
	    </div>
        <div class="areaDatCpoConInp" >    
            <!-- SUCURSAL-->
            <label id="cboSucursalDC_FP_etiq" for="cboSucursalDC_FP">Sucursal:</label>
            <select class="areaDatCpoConInpFieM" id="cboSucursalDC_FP" name="cboSucursalDC_FP" style="text-transform:uppercase;" disabled="disabled" >
            </select>													       
            
        </div>          
        <!-- CBU -->
        <div id="divNroCBU" style="display:none">
            <div class="areaDatCpoConInp" >
                <!-- NUMERO DE CBU -->
                <label id="numCBU_etiq" for="numCBU">N&uacute;mero CBU:</label>
                <input class="areaDatCpoConInpFieM" id="numCBU" name="numCBU"  validacionPersonalizada="fncValidaCBU()" type="text"  maxlength="22" disabled="disabled"/>
            </div>               
        </div>
    </div>
</div>