<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: moduloFPago_Ctas.xsl

Fecha de Creacion: 02/05/2011

PPcR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripcion: Este archivo contiene la definicion del parseo de un XML
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="xml" encoding="iso-8859-1"/>
	<!-- DEFINICION DE FUNCIONES -->
	<msxsl:script language="JScript" implements-prefix="user"><![CDATA[

var g_pos = 0;

function TRStyle(pvarInc)
{
	if (pvarInc)
		g_pos++;
	if (g_pos % 2 == 0)
		return 'form_campname_bcoR';
	else
		return 'form_campname_colorR';
}

function TDStyle()
{
	if (g_pos % 2 == 0)
		return '#FFFFFF';
	else
		return '#EEEEEE';
}

function GetImgName()
{
	if (g_pos % 2 == 0)
		return '/Oficina_Virtual/Html/images/boton_lupa.gif';
	else
		return '/Oficina_Virtual/Html/images/boton_lupa.gif';
}
]]><!-- FIN DEFINICION DE FUNCIONES -->
	</msxsl:script>
	<xsl:template match="/">
		<div class="areaDatCpoCon" style="width:650">
			<table width="100%" align="center" border="1" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td bordercolor="#dedfe7">
						<xsl:if test="count(//productArrangement) != 0">
							<table id="tabla_detalle" width="100%" border="0" cellspacing="1" cellpadding="1">
								<tr class="form_titulos02">
									<td>
										<div align="center" title="Titular" class="nav1">Titular</div>
									</td>
									<td>
										<div align="center" title="Tipo Cuenta" class="nav1">Tipo Cuenta</div>
									</td>
									<td>
										<div align="center" title="Producto" class="nav1">Producto</div>
									</td>
									<td>
										<div align="center" title="Sucursal" class="nav1">Sucursal</div>
									</td>
									<td>
										<div align="center" title="N�mero" class="nav1">N�mero</div>
									</td>
									<td>
										<div align="center" title="Moneda" class="nav1">Moneda</div>
									</td>
								</tr>
								<xsl:for-each select="//productArrangement">
									<xsl:sort order="descending" select="Prod/ORDEN" data-type="number"/>
									<tr>
										<xsl:choose>
											<xsl:when test="Prod/COLOR='R'">
												<xsl:attribute name="style">background-color:RED</xsl:attribute>
											</xsl:when>
											<xsl:when test="Prod/COLOR='A'">
												<xsl:attribute name="style">background-color:YELLOW</xsl:attribute>
											</xsl:when>
											<xsl:when test="Prod/COLOR='V'">
												<xsl:attribute name="style">background-color:GREEN</xsl:attribute>
											</xsl:when>
										</xsl:choose>
										<xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute>
										<td align="center" style="cursor:pointer;text-align:center" height="26px">
											<a title="Seleccionar ">
												<xsl:attribute name="style">color:BLACK</xsl:attribute>
											<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Cuentas('<xsl:value-of select="Prod/TypeCde"/>','<xsl:value-of select="Prod/ProdCde"/>','<xsl:value-of select="Prod/InstCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>','<xsl:value-of select="FinSvceArr/IntAmt/CrncyCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctSfxNum"/>')</xsl:attribute>
												<xsl:attribute name="onmouseover">JavaScript:this.style.color='#FFFFFF' </xsl:attribute>
												<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
												<xsl:choose>
													<xsl:when test="Prod/TypeCde = 'PRI'">Primer Titular</xsl:when>
													<xsl:when test="Prod/TypeCde = 'SEC'">Segundo Titular</xsl:when>
													<xsl:otherwise>No determinado</xsl:otherwise>
												</xsl:choose>
											</a>
										</td>
										<td align="center" style="cursor:pointer;text-align:center" height="26px">
											<a title="Seleccionar ">
												<xsl:attribute name="style">color:BLACK</xsl:attribute>
											<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Cuentas('<xsl:value-of select="Prod/TypeCde"/>','<xsl:value-of select="Prod/ProdCde"/>','<xsl:value-of select="Prod/InstCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>','<xsl:value-of select="FinSvceArr/IntAmt/CrncyCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctSfxNum"/>')</xsl:attribute>
												<xsl:attribute name="onmouseover">JavaScript:this.style.color='#FFFFFF' </xsl:attribute>
												<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
												<xsl:choose>
													<xsl:when test="Prod/ProdCde= 'IND'">Individual</xsl:when>
													<xsl:when test="Prod/ProdCde= 'JAN'">Conjunta</xsl:when>
													<xsl:when test="Prod/ProdCde= 'JOR'">Indistinta</xsl:when>
													<xsl:when test="Prod/ProdCde= 'EMP'">Empresas</xsl:when>
													<xsl:otherwise>No determinado</xsl:otherwise>
												</xsl:choose>
											</a>
										</td>
										<td align="center" style="cursor:pointer;text-align:center" height="26px">
											<a title="Seleccionar ">
												<xsl:attribute name="style">color:BLACK</xsl:attribute>
											<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Cuentas('<xsl:value-of select="Prod/TypeCde"/>','<xsl:value-of select="Prod/ProdCde"/>','<xsl:value-of select="Prod/InstCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>','<xsl:value-of select="FinSvceArr/IntAmt/CrncyCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctSfxNum"/>')</xsl:attribute>
												<xsl:attribute name="onmouseover">JavaScript:this.style.color='#FFFFFF' </xsl:attribute>
												<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
												<xsl:choose>
													<xsl:when test="Prod/InstCde= 'ST'">Caja de Ahorros</xsl:when>
													<xsl:when test="Prod/InstCde= 'IM'">Cuenta Corriente</xsl:when>
													<xsl:when test="Prod/InstCde= 'TC'">Tarjeta de Crédito</xsl:when>
													<xsl:when test="Prod/InstCde= 'PR'">Préstamo</xsl:when>
													<xsl:when test="Prod/InstCde= 'DP'">Tarjeta de Débito</xsl:when>
													<xsl:otherwise>No determinado</xsl:otherwise>
												</xsl:choose>
											</a>
										</td>
										<td align="center" style="cursor:pointer;text-align:center" height="26px">
											<a title="Seleccionar ">
												<xsl:attribute name="style">color:BLACK</xsl:attribute>
											<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Cuentas('<xsl:value-of select="Prod/TypeCde"/>','<xsl:value-of select="Prod/ProdCde"/>','<xsl:value-of select="Prod/InstCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>','<xsl:value-of select="FinSvceArr/IntAmt/CrncyCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctSfxNum"/>')</xsl:attribute>
												<xsl:attribute name="onmouseover">JavaScript:this.style.color='#FFFFFF' </xsl:attribute>
												<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
												<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>
											</a>
										</td>
										<td align="center" style="cursor:pointer;text-align:center" height="26px">
											<a title="Seleccionar ">
												<xsl:attribute name="style">color:BLACK</xsl:attribute>
											<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Cuentas('<xsl:value-of select="Prod/TypeCde"/>','<xsl:value-of select="Prod/ProdCde"/>','<xsl:value-of select="Prod/InstCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>','<xsl:value-of select="FinSvceArr/IntAmt/CrncyCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctSfxNum"/>')</xsl:attribute>
												<xsl:attribute name="onmouseover">JavaScript:this.style.color='#FFFFFF' </xsl:attribute>
												<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
												<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>
											</a>
										</td>
										<td align="center" style="cursor:pointer;text-align:center" height="26px">
											<a title="Seleccionar ">
												<xsl:attribute name="style">color:BLACK</xsl:attribute>
											<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Cuentas('<xsl:value-of select="Prod/TypeCde"/>','<xsl:value-of select="Prod/ProdCde"/>','<xsl:value-of select="Prod/InstCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/BrnchNum"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctNum"/>','<xsl:value-of select="FinSvceArr/IntAmt/CrncyCde"/>','<xsl:value-of select="FinSvceArr/AcctIntTrnsfNum/AcctSfxNum"/>')</xsl:attribute>
												<xsl:attribute name="onmouseover">JavaScript:this.style.color='#FFFFFF' </xsl:attribute>
												<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
												<xsl:choose>
													<xsl:when test="FinSvceArr/IntAmt/CrncyCde= '080'">
												$ 
											</xsl:when>
													<xsl:when test="FinSvceArr/IntAmt/CrncyCde= '002'">
												Dólar
											</xsl:when>
													<xsl:otherwise>
												No determinado
											</xsl:otherwise>
												</xsl:choose>
											</a>
										</td>
									</tr>
								</xsl:for-each>
							</table>
						</xsl:if>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
