<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: moduloFPago_Tarjetas.xsl

Fecha de Creacion: 02/05/2011

PPcR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripcion: Este archivo contiene la definicion del parseo de un XML
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="xml" encoding="iso-8859-1"/>
	
	
	<!-- LR. Esto es para darle un formato como el nuestro miles "." decimal "," -->
	<xsl:decimal-format name="arg" decimal-separator="," grouping-separator="."/>
	<xsl:template match="/">
		<div class="areaDatCpoCon" style="width:650">
			<table width="100%" align="center" border="1" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td bordercolor="#dedfe7">
						<strong/>
						<xsl:if test="count(//CardAccessArrangement) != 0">
							<table id="tabla_detalle" width="100%" border="0" cellspacing="1" cellpadding="1">
								<tr class="form_titulos02">
									<td>
										<div class="areaDatTit">
											<div align="center" class="areaDatTitTex">Tarjeta</div>
										</div>
									</td>
									<td>
										<div class="areaDatTit">
											<div align="center" class="areaDatTitTex">N�mero</div>
										</div>
									</td>
									<td>
										<div class="areaDatTit">
											<div align="center" class="areaDatTitTex">Titular</div>
										</div>
									</td>
									<td>
										<div class="areaDatTit">
											<div align="center" class="areaDatTitTex">Estado</div>
										</div>
									</td>
								</tr>
								<xsl:for-each select="//CardAccessArrangement">
									<xsl:if test="PrimAcctInfo/StatDesc = 'OPERATIVA' ">
										<tr>
											<!--<xsl:attribute name="id"><xsl:value-of select="concat('fila_',position()-1)"/></xsl:attribute>-->
											<xsl:if test="position() mod 2=1">
												<xsl:attribute name="class">filaLlena</xsl:attribute>
											</xsl:if>
											<xsl:if test="position() mod 2=0">
												<xsl:attribute name="class">filaVacia</xsl:attribute>
											</xsl:if>
											<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
											<td align="center" style="cursor:hand" height="26px">
												<a title="Seleccionar ">
													<xsl:attribute name="style">color='#000000';font-weight:normal</xsl:attribute>
													<!--<xsl:attribute name="onclick">JavaScript:fondoGris(this)</xsl:attribute>-->
													<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Tarjetas('<xsl:value-of select="PrimAcctInfo/RestrCde"/>','<xsl:value-of select="PrimAcctInfo/AcctShrtSLName"/>','<xsl:value-of select="PrimAcctInfo/RestrDesc"/>','<xsl:value-of select="CardNum"/>','<xsl:value-of select="CardhName"/>','<xsl:value-of select="TypeIdfcCde"/>','<xsl:value-of select="IdfcNum"/>','<xsl:value-of select="PrimAcctInfo/StatDesc"/>')</xsl:attribute>
													<xsl:attribute name="onmouseover">JavaScript:this.style.color='#ff0000'</xsl:attribute>
													<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
													<xsl:value-of select="PrimAcctInfo/RestrDesc"/>
												</a>
											</td>
											<td align="center" style="cursor:hand" height="26px">
												<a title="Seleccionar ">
													<xsl:attribute name="style">color='#000000';font-weight:normal</xsl:attribute>
													<!--<xsl:attribute name="onclick">JavaScript:fondoGris(this)</xsl:attribute>-->
													<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Tarjetas('<xsl:value-of select="PrimAcctInfo/RestrCde"/>','<xsl:value-of select="PrimAcctInfo/AcctShrtSLName"/>','<xsl:value-of select="PrimAcctInfo/RestrDesc"/>','<xsl:value-of select="CardNum"/>','<xsl:value-of select="CardhName"/>','<xsl:value-of select="TypeIdfcCde"/>','<xsl:value-of select="IdfcNum"/>','<xsl:value-of select="PrimAcctInfo/StatDesc"/>')</xsl:attribute>
													<xsl:attribute name="onmouseover">JavaScript:this.style.color='#ff0000'</xsl:attribute>
													<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
													<xsl:value-of select="CardNum"/>
												</a>
											</td>
											<td align="center" style="cursor:hand" height="26px">
												<a title="Seleccionar ">
													<xsl:attribute name="style">color='#000000';font-weight:normal</xsl:attribute>
													<!--<xsl:attribute name="onclick">JavaScript:fondoGris(this)</xsl:attribute>-->
													<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Tarjetas('<xsl:value-of select="PrimAcctInfo/RestrCde"/>','<xsl:value-of select="PrimAcctInfo/AcctShrtSLName"/>','<xsl:value-of select="PrimAcctInfo/RestrDesc"/>','<xsl:value-of select="CardNum"/>','<xsl:value-of select="CardhName"/>','<xsl:value-of select="TypeIdfcCde"/>','<xsl:value-of select="IdfcNum"/>','<xsl:value-of select="PrimAcctInfo/StatDesc"/>')</xsl:attribute>
													<xsl:attribute name="onmouseover">JavaScript:this.style.color='#ff0000'</xsl:attribute>
													<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
													<xsl:value-of select="CardhName"/>
												</a>
											</td>
											<td align="center" style="cursor:hand" height="26px">
												<a title="Seleccionar ">
													<xsl:attribute name="style">color='#000000';font-weight:normal</xsl:attribute>
													<!--<xsl:attribute name="onclick">JavaScript:fondoGris(this)</xsl:attribute>-->
													<xsl:attribute name="onclick">JavaScript:fncDatosFPago_Tarjetas('<xsl:value-of select="PrimAcctInfo/RestrCde"/>','<xsl:value-of select="PrimAcctInfo/AcctShrtSLName"/>','<xsl:value-of select="PrimAcctInfo/RestrDesc"/>','<xsl:value-of select="CardNum"/>','<xsl:value-of select="CardhName"/>','<xsl:value-of select="TypeIdfcCde"/>','<xsl:value-of select="IdfcNum"/>','<xsl:value-of select="PrimAcctInfo/StatDesc"/>')</xsl:attribute>
													<xsl:attribute name="onmouseover">JavaScript:this.style.color='#ff0000'</xsl:attribute>
													<xsl:attribute name="onmouseout">JavaScript:this.style.color='#000000'</xsl:attribute>
													<xsl:value-of select="PrimAcctInfo/StatDesc"/>
												</a>
											</td>
										</tr>
									</xsl:if>
								</xsl:for-each>
							</table>
						</xsl:if>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
