<%
'--------------------------------------------------------------------------------
'Fecha de Modificaci�n: 19/03/2012
'PPCR: 2011-00056
'Desarrollador: Jose Casais
'Descripci�n: Se agrega producto ICB1
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: popUp_customizado.asp
' 
' Fecha de Creaci�n: 05/04/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Marco de la pantalla para mensajes al usr
' 
' ------------------------------------------------------------------------------
%>


<!--  Div Esperando  -->
<style type="text/css">

#capaEsperando {
position: absolute;
display: none;
top: 0;
left: 0;
width: 1000;
height: 600;
background: #FFFFFF ;
filter:alpha(opacity=1);
border: 0px solid #EDEDF1;
/*z-index:0;*/
cursor:wait
}

#spanEsperando{
background: #EEEEEE url(<%=mvarCANALURL & "Canales/" & mvarCanal %>/images/ajax-loader.gif) no-repeat center center;
text-align:center;
width:230; 
height:150;
border:solid 1px #C0C0C0;
position:absolute;
top:220;
left:350;
font-size:12px;



}

#blockUI_block{
	z-index:1001;
	border:none;
	margin-left:0px;
	padding:0;
	width:1000px;
	height:710px;
	top:0;
	left:0;
	background:#000000; 
	position:absolute; 
	filter: alpha(opacity='10');
}

#blockUI_blockMsg{
	top:30%; 
	left:35%;;
	width:250px;
	height:100;
 	z-index:1002; 
 	position:absolute; 
 	color: '#999999'; 
 	background:#FFFFFF; 
 	border:2px solid #999999; 
 	padding: 2em; 
 	font-size:12px;
 	font-weight:bold;
 	text-align:justify;
}

#blockUI_opacidad{
	z-index:1001;
	border:none;
	padding:0;	
	top:0;
	left:0;
	background:#000000; 
	position:absolute; 
	filter: alpha(opacity='10');
}

#blockUI_mensaje{
	top:30%; 
	left:35%;
	width:350px;
	height:100;
 	z-index:1002; 
 	position:absolute; 
 	color: '#999999'; 
 	background:#FFFFFF; 
 	border:2px solid #999999; 
 	padding: 2em; 
 	font-size:12px;
 	font-weight:bold;
}

#popUp_opacidad{
	z-index:1001;
	border:none;
	padding:0;	
	top:0;
	left:0;
	background:#000000; 
	position:absolute; 
	filter: alpha(opacity='10');
}

#popUp_mensaje, #popUp_callejero, #confirm_cust, #popUp_Localidad, #aceptar_customizado, #popUp_Impresion,#popUp_mailCliente, #popUp_compPrecio, #div_cierre, #envio_revision, #confirma_FueraNorma, #popUp_condiciones, #popUp_deducibles, #popUp_infoMedioPago{
	top:20%; 
	left:10;
	width:350px;
	height:100;
 	z-index:1002; 
 	position:absolute; 
 	color: '#999999'; 
 	background:#FFFFFF; 
 	border:2px solid #999999; 
 	padding: 2em; 
 	font-size:12px;
 	font-weight:bold;
 	
 	}

#button_close{
	margin-top:0px;
	margin-left:30px;

</style>

<div id="capaEsperando"  style="display:none"></div>
<span id="spanEsperando" style="display:none"></span>

<script type="text/javascript">

// muestraEsperando("Levantando Cotizaci�n. Aguarde por favor.")

</script>

<!-- Fin Seccion div esperando -->

 
<!-- alert_customizado -->
<div id="popUp_opacidad" style="display:none;"></div>

<div id="popUp_mensaje" style="display:none;">
<div class="areaDatTit">
            <div class="areaDatTitTex" style="width:405">Selecci�n de Cliente</div>
        </div>
	<label id="msj_alert"></label>
	
	<div id="div_contenidoPopUp" style="overflow:scroll">	
	
	</div>
	
	<div id="button_close">
	<table border=0 width=100%><tr>
	<td width="50%">
	<a style="vertical-align:bottom; cursor:hand"  href="" onclick="completaCliente('x'); return false" class="linksubir">
	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="middle" border="0" width="14" height="14"> Seleccionar</a>
	</td>
	<td width="50%" align='right' style="text-align:right">
	<a style="vertical-align:bottom; cursor:hand"  href="" onclick="document.getElementById('popUp_opacidad').style.display ='none';document.getElementById('popUp_mensaje').style.display ='none';document.getElementById('msj_alert').innerHTML = ''; return false" class="linksubir">
	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cerrar</a>
	</td>
	     
	</tr>
		</table>
	</div>
</div>

<div id="confirm_cust" style="display:none;">

	<label id="msj_confirm"></label>
	
	<div id="div_contenidoConfirm" style="overflow:scroll">	
	
	</div>
	
	<div id="Div3">
	<input type="hidden" id="funcion_confirma"  value=""/>
	<input type="hidden" id="funcion_cancela"  value=""/>
	<table border=0 width=100%><tr height="20">
	<td width="50%">
	<a style="vertical-align:bottom; cursor:hand"  href="" onclick="resultadoConfirm(true); return false " class="linksubir">
	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="absmiddle" border="0"> Aceptar</a>
	</td>
	<td width="50%" align='right' style="text-align:right">
	<a style="vertical-align:bottom; cursor:hand"  href="" onclick="resultadoConfirm(false); return false" class="linksubir">
	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cancelar</a>
	</td>
	     
	</tr>
		</table>
	</div>
</div>

<!-- ACEPTAR_customizado -->
<div id="aceptar_customizado" style="display:none;">

<div id="div_contenidoAceptar" style="overflow:scroll">	</div>

<div id="blockUI_aceptar">
	<input type="hidden" id="funcion_aceptar"  value=""/>
	<div id="mensaje_aceptar"></div>
	<p></p>
	<div id="boton_aceptar">
	<a style="vertical-align:bottom; cursor:hand"  href="" onclick="resultadoAceptar(true); return false " class="linksubir">
	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="center" border="0"> Aceptar</a>
	</div>
	
</div>
</div>

<!-- alert_customizado -->
<div id="blockUI_opacidad" style="display:none;"></div>


<div id="blockUI_mensaje" style="display:none;">
	
	<div id="mensaje_alert"></div>
	<p></p>
	<div id="pop_button_close">
	<a style="vertical-align:bottom; cursor:hand"  href="" onclick="document.getElementById('blockUI_opacidad').style.display ='none';document.getElementById('blockUI_mensaje').style.display ='none';document.getElementById('mensaje_alert').innerHTML = '';return false" class="linksubir">
	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cerrar</a>
	</div>
	
</div>

<!-- popUp para Callejero -->
<div id="popUp_callejero" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:405">B&uacute;squeda de c&oacute;digos postales de Capital Federal</div>
    </div>
	<label id="msj_alert_popUp_callejero"></label>
	<div class="areaDatTit" style="width:409px">
        <div class="areaDatTitTex">Domicilio Registrado</div>
    </div>
    <div class="areaDatCpo">
	    <div class="areaDatCpoConInp" style="width:409px">
            <label  style="width:290px">Calle</label>
            <label  style="width:50px">Nro</label>
            <label  style="width:50px">CP</label>
        </div>
	    <div class="areaDatCpoConInp" style="width:409px">
            <span style="width:10px"></span>
            <span id="divCalle" style="width:290px"></span>
			<span id="divNro" style="width:50px"></span>
			<span id="divCP" style="width:40px"></span>
        </div>
	</div>
	<div class="areaDatTit" style="width:409px">
        <div class="areaDatTitTex">Situar en Calle</div>
    </div>
    <div class="areaDatCpo">
	    <div class="areaDatCpoConInp" style="width:409px">
            <input class="areaDatCpoConInpFieL" id="calleBuscar" name="calleBuscar" type="text" onkeydown="if(window.event.keyCode==13){fncBuscarCalle();}" />
            <!-- 19/10/2010 LR Defect 21 Estetica, pto 34 -->
            <a class="linksubir" style="vertical-align:text-bottom;cursor:hand;text-align:center" title="Buscar" onclick="javascript:fncBuscarCalle();">
                &nbsp;&nbsp;Buscar
            </a>
        </div>
	</div>
	<div id="conten_popUp_callejero" style="overflow:scroll">
	</div>
	<input id="lblCalle" type="hidden" />
	<input id="lblNro" type="hidden" />
	<input id="lblCodPos" type="hidden" />
	<div id="button_close">
	    <table border=0 width=100%>
	        <tr>
	            <td width="50%" align="right" style="text-align:right">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="completaCP('x'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="absmiddle" border="0"> Seleccionar</a>
	            </td>
	            <td width="50%" align="right" style="text-align:right">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="ocultarPopUp2('popUp_callejero'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cerrar</a>
	            </td>
	        </tr>
		</table>
	</div>
</div>
<!-- popUp para Callejero -->

<!-- popUp para LOCALIDAD -->
<div id="popUp_Localidad" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:405">B&uacute;squeda de localidades</div>
    </div>
   
   <div class="areaDatCpo">
	    <div class="areaDatCpoConInp" style="width:409px">
            <input class="areaDatCpoConInpFieL" id="localidadBuscar" name="localidadBuscar" type="text" onkeydown="if(window.event.keyCode==13){ fncBuscaLocalidad('INTERNO');}"/>
           <input type="hidden"  id="pCpoLocBuscar" />
           <input type="hidden"  id="pCpoProvLocBuscar" />
            <input type="hidden"  id="pCpoCodPostalLocBuscar" /> 
             <input type="hidden"  id="pCpoCalleBuscar" />
            <input type="hidden"  id="pCpoNroCalleBuscar" /> 
            
        
            <a class="botonMenos" title="Buscar" onclick="javascript:fncBuscaLocalidad('INTERNO');">
                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="middle" width="14" height="14" border="0" alt="Buscar">
            </a>
        </div>
	</div> 
	
	<div id="div_contenidoLocalidad" style="overflow:scroll">	
	
	</div>
	
	<div id="Div5">
	    <table border=0 width=100%>
	        <tr>
	            <td width="50%" align="right" style="text-align:right">
	                <a style="vertical-align:bottom cursor:hand"  href="" onclick="completaLocalidad('x'); fncMuestraCoberturas(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="middle" border="0"> Seleccionar</a>
	            </td>
	            <td width="50%" align='right' style="text-align:right">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="ocultarPopUpLoc('popUp_Localidad');return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="middle" border="0"> Cerrar</a>
	            </td>
	        </tr>
		</table>
	</div>
</div>
<!-- popUp para CIERRE -->

<div id="div_cierre" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:576">Estado de la Solicitud</div>
    </div>
   
   <div class="areaDatCpo" style="width:575px; height:250px; border: solid 1px #E0E0E0">
	    <div class="areaDatCpoConInp" style="height:50px">
	<!-- 19/10/2010 LR Defect 21 Estetica, pto 35 -->
	    <br />
	     <center>
            <select class="areaDatCpoConInpFieM" style="width:320px" id="estado_solicitud" name="estado_solicitud"  >
           <option value="">SELECCIONAR...</option> 
          <option value="1">1- Pendiente de enviar a la Cia</option> 
            <option value="2">2- Enviar a la Cia</option> 
          </select>
         </center>
          
         
       </div>
       <p style="padding:0 3 0 3;font-size:11px"><b>1- Pendiente de enviar a la Cia:</b> <br /> Este estado significa que se ha generado una solicitud y no desea enviarla a la compa��a. Es decir que Usted podr� dejar la Solicitud en este estado con el fin de dejarla como borrador para posteriormente modificar alg�n dato.  </p>  
          
           <p style="padding:0 3 0 3;font-size:11px"><b>2- Enviar a la Cia:</b> <br /> 
          Este estado es el que Usted deber� elegir si lo que quiere es enviar la Solicitud a la compa��a. Tenga en cuenta que una vez enviada la misma no podr� ser modificada e ingresar� al sistema central de la compa��a en el proceso nocturno. Con este estado Usted podr� imprimir toda la documentaci�n provisoria para entregarle al cliente
          
           </p>  
       
	</div> 
		
	 <div id="cuadroCambiosEnvio" style="display:none"></div>
	<p></p> 
	<div id="control_Cierre">
	    <table border=0 align=center width=570px>
	   <tr height="10px"></tr> 
	        <tr> 
	        <td width="70%"  style="text-align:left">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="ocultarPopUpEnviar();return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_volver.gif" align="middle" border="0">  Revisar/ Modificar Solicitud</a>
	            </td>
	            <td width="30%"  style="text-align:right">
	                <a style="vertical-align:bottom cursor:hand"  href="" onclick="aceptaEnviar(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" align="middle" border="0">Continuar</a>
	            </td>
	           
	        </tr>
		</table>
	</div>
</div>

<!-- Env�o a revisi�n -->
<div id="envio_revision" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:576">Env�o para Revisi�n</div>
    </div>
   
   <div class="areaDatCpo" style="width:575px; height:250px; border: solid 1px #E0E0E0">
	  <div class="areaDatCpoConInp" >
	 
           <label id="" for="" style="width:80">Para:</label>
        <input class="areaDatCpoConInpFieM" id="revision_para" name="revision_para"   maxlength="50"  type="text"  style="width:250; margin-right:2 " disabled/>
   </div>
    <div class="areaDatCpoConInp" >   
            <label id="" for="" style="width:80">De:</label>
        <input class="areaDatCpoConInpFieM" id="revision_de" name="revision_de"    maxlength="50"  type="text" style="width:250; margin-right:2 " disabled/>
       <input type="hidden" id="usuarNomec" />
          
         
       </div>
       
       <div id="cuadroCambiosRenovacion" style="display:none"></div>
       
       
       <p>Solicito la revisi�n de la cotizaci�n n�mero <span id="revision_nroCoti"></span></p>
       <!-- 18/10/2010 LR Defect 21 Estetica, pto 19 -->
       <textarea cols=100 rows=10 id="revision_comentario" onkeypress="capturaTecla()" validarTipeo="alfanum" onpaste="return false;" style="TEXT-TRANSFORM: uppercase;font-family: Verdana;font-size: 10px"></textarea>
       
	</div> 

	<div id="">
	    <table border=0 align=center width=570px>
	   <tr height="10px"></tr> 
	        <tr> 
	        <td width="70%"  style="text-align:left">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="continuarRevision();return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="middle" border="0"> Continuar</a>
	            </td>
	            <td width="30%"  style="text-align:right">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="ocultarPopUpRevision(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="middle" border="0">Cerrar</a>
	            </td>
	           
	        </tr>
		</table>
	</div>
</div>
<!-- Fuera de Norma -->
<div id="confirma_FueraNorma" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:500">Confirmaci�n</div>
    </div>
      <label id="msj_alert_confirma_FueraNorma"></label>
   <div class="areaDatCpo" style="  width:500px; height:250px; border: solid 1px #E0E0E0">
	 
   
        <span id="tablaFN"  style="width:100%;height:200; overflow:auto"></span>
	</div> 
	 
<span style="width:500px;" >Presione Continuar para aceptarlas o Cancelar para modificarlas</span>
<div id="">
	    <table border=0 align=center width=500px>
	   <tr height="10px"></tr> 
	        <tr> 
	        <td width="70%"  style="text-align:left">
	                <a style="vertical-align:bottom; cursor:hand"  href="" onclick="continuarFueraNorma();return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="middle" border="0"> Continuar</a>
	            </td>
	            <td width="30%"  style="text-align:right">
	                <a style="vertical-align:bottom cursor:hand"  href="" onclick="ocultarPopUpFueraNorma(); return false" class="linksubir">
	                <!-- 06/10/2010 LR Defect 21 Estetica, pto 13 -->
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="middle" border="0">Cancelar</a>
	            </td>
	           
	        </tr>
		</table>
	</div>
</div>

<!-- popUp para Impresion -->
<!-- 06/10/2010 LR Defect 21 Estetica, pto 7 -->
<div id="popUp_Impresion" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:305">Indique las copias a imprimir</div>
    </div>
	<label id="msj_alert_popUp_Impresion"></label>
    <div class="areaDatCpo">
	    <span class="areaDatCpoConInp" style="width:309px">
	        <table width="100%" class="Form_campname_bcoL" >
	            <tr><td align="center">&nbsp;</td></tr>
                <!-- JC ICOBB NO COPIA PRODUCTOR-->
				<%If mvarRAMOPCODSQL = "ICB1" Then %>
                <tr><td><input name="copia" id="copiaCliente" style="margin-left:75px" type="radio" value="C" checked ><label for="copia" style="font-size:14px;height=15px">Para el cliente</label></td></tr>
				<tr><td><input name="copia" id="copiaProductor" style="display:none"; style="margin-left:75px" type="radio" value="P" ><label for="copia" style="display:none"; style="font-size:14px;height=15px">Para el productor</label></td></tr>
                <tr><td><input name="copia" id="copiaAmbos" style="display:none"; style="margin-left:75px" type="radio" value="A" ><label for="copia" style="display:none"; style="font-size:14px;height=15px">Para ambos</label></td></tr>
				<%Else%>
                <tr><td><input name="copia" id="copiaCliente" style="margin-left:75px" type="radio" value="C"><label for="copia" style="font-size:14px;height=15px">Para el cliente</label></td></tr>
				<tr><td><input name="copia" id="copiaProductor" style="margin-left:75px" type="radio" value="P" checked><label for="copia" style="font-size:14px;height=15px">Para el productor</label></td></tr>
                <tr><td><input name="copia" id="copiaAmbos" style="margin-left:75px" type="radio" value="A" ><label for="copia" style="font-size:14px;height=15px">Para ambos</label></td></tr>
				<%End if %>
				<tr><td align="center">&nbsp;</td></tr>
            </table>
        </span>
	</div>
	<div id="conten_popUp_Impresion" style="overflow:scroll"></div>
	<div id="button_close" width="309px">
	    <table border=0 width=100% height="50px">
	        <tr>
	            <td width="50%"  align="center" style="text-align:center">
	                <a style="vertical-align:bottom" href="" onclick="fncImprimirCotizacion('I');ocultarPopUp2('popUp_Impresion'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="absmiddle" border="0"> Continuar</a>
	            </td>
	            <td width="50%"  align="left" style="text-align:left">
	                <a style="vertical-align:bottom;" href="" onclick="ocultarPopUp2('popUp_Impresion'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cancelar</a>
	            </td>
	        </tr>
		</table>
	</div>
</div>
<!-- popUp para Impresion -->

<!-- popUp para EMail -->
<div id="popUp_mailCliente" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:405">Enviar por E-Mail</div>
    </div>
	<label id="msj_alert_popUp_mailCliente"></label>
    <div class="areaDatCpo" style="width:405">
	  
	   <div class="areaDatCpoConInp" >
	 
           <label  for="" style="width:80">Para:</label>
        <input class="areaDatCpoConInpFieM" id="mailCliente_Para" name="mailCliente_Para"     type="text" style="width:250; margin-right:2 " />
       
            <label id="" for="" style="width:80">CC:</label>
        <input class="areaDatCpoConInpFieM" id="mailCliente_Copia" name="mailCliente_Copia"     type="text" style="width:250; margin-right:2 " />
         
       </div>
	    
	       
     
	</div>
	
	<br />
	<span id="spnMensajeErrorMailPara"></span>
	<br />
	<span id="spnMensajeErrorMailCopy"></span>
	
	<div  width="405px">
	    <table border=0 width=100% height="50px">
	        <tr>
	            <td width="50%"  align='right' style="text-align:right">
	                <a style="vertical-align:bottom" href="" onclick="fncImprimirCotizacion('M');return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="absmiddle" border="0"> Continuar</a>
	            </td>
	            <td width="50%"  align='right' style="text-align:right">
	                <a style="vertical-align:bottom;" href="" onclick="ocultarPopUp2('popUp_mailCliente'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cancelar</a>
	            </td>
	        </tr>
		</table>
	</div>
</div>
<!-- popUp para EMail -->

<!-- popUp para Composici�n Precio -->
<div id="popUp_compPrecio" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:407">Cotizaci&oacute;n</div>
    </div>
    <label id="msj_alert_popUp_compPrecio"></label>
	<div id="conten_popUp_compPrecio" style="width:410">
	</div>
	<label></label>
	<!-- 08/10/2010 LR Defect 21 Estetica, pto 14 -->
	<div id="button_close_compPrecio" style="padding-left:90%">
	    <div style="width:40%" align="right">
            <a style="vertical-align:bottom;" href="#" onclick="ocultarPopUp2('popUp_compPrecio');" class="linksubir">
            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="left" border="0"> Cerrar</a>
		</div>
	</div>
</div>
<!-- popUp para Composici�n Precio -->

<div id="popUp_condiciones" style="display:none;">
 <label id="msj_alert_popUp_condiciones"></label>

    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:407">Condiciones</div>
    </div>
    <label id=""></label>
	<div id="contenidoCondiciones" style="width:410">
	</div>
	<label></label>
	<div id="" style="padding-left:200">
	    <div>
            <a style="vertical-align:bottom;" href="#" onclick="ocultarPopUp2('popUp_condiciones');" class="linksubir">
            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cerrar</a>
		</div>
	</div>
</div>


<div id="popUp_deducibles" style="display:none;">
 <label id="msj_alert_popUp_deducibles"></label>

    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:407">Deducibles</div>
    </div>
    <label id=""></label>
	<div id="contenidoDeducibles" style="width:410; font-family:Arial; font-size:11; font-weight:normal; color:Black">
	</div>
	<label></label>
	<div id="" style="padding-left:200">
	    <div>
            <a style="vertical-align:bottom;" href="#" onclick="ocultarPopUp2('popUp_deducibles');" class="linksubir">
            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cerrar</a>
		</div>
	</div>
</div>


<!-- LR 06/09/2010 popUp para info del medio de Pago -->
<div id="popUp_infoMedioPago" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:405">Datos del medio de Pago</div>
    </div>
	<label id="msj_alert_popUp_infoMedioPago"></label>
	<div id="conten_popUp_infoMedioPago" style="width:410"></div>
    <div class="areaDatCpo">
	    <span class="areaDatCpoConInp" style="width:409px">
	        <table width="100%" class="Form_campname_bcoL">
                <tr class="Form_campname_bcoL">
	                <td align="left" style="Padding-left:15px">
				        <strong>Cliente:&nbsp;&nbsp;</strong>
				        <span id="divNombreClienteBT"></span>
			        </td>
			        <tr class="Form_campname_bcoL">
				        <td align="left" style="Padding-left:15px">
							<div id="divTxtMedioPago"></div>
						</td>
				    </tr>   
	            </tr>
	            <tr><td align="center">&nbsp;</td></tr>
                <tr><td><select class="areaDatCpoConInpFieM" style="width:280px;" id="cmbInfoMedioPago" name="cmbInfoMedioPago"><option value='-1'>Ingresar Cuenta No Listada</option></select></td></tr>
                <tr><td align="center">&nbsp;</td></tr>
            </table>
        </span>
	</div>
	<div id="button_close_infoMedioPago" width="409px">
	    <table border=0 width=100% height="50px">
	        <tr>
	            <td width="50%"  align='right' style="text-align:right">
	                <a style="vertical-align:bottom" href="" onclick="fncSelBuscCuentas(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="absmiddle" border="0"> Continuar</a>
	            </td>
	            <td width="50%"  align='right' style="text-align:right">
	                <a style="vertical-align:bottom;" href="" onclick="ocultarPopUp2('popUp_infoMedioPago'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cancelar</a>
	            </td>
	        </tr>
		</table>
	</div>
</div>
<!-- LR 06/09/2010 popUp para info del medio de Pago -->
