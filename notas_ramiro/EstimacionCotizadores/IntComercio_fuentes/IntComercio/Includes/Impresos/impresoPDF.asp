<%
'------------------------------------------------------------------------------
' Fecha de Modificación: 20/03/2012
' PPCR: 2011-00056
' Desarrollador: Marcelo Gasparini
' Descripción: Se agrega producto ICB1
'--------------------------------------------------------------------------------------------------------	
'COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
'LIMITED 2010. ALL RIGHTS RESERVED

'This software is only to be used for the purpose for which it has been provided.
'No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
'system or translated in any human or computer language in any way or for any other
'purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
'Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
'offence, which can result in heavy fines and payment of substantial damages.

'Nombre del Fuente: impresoPDF.asp

'Fecha de Creación: 25/03/2010

'PPcR: 50055/6010661

'Desarrollador: Leonardo Ruiz

'Descripción: Este archivo ASP se crea para administrar y tomar los datos para la impresion de la cotizacion
'---------------------------------------------------------------------------------------------------------  
Response.Buffer = false

Response.Expires = 0




Const adTypeBinary = 1

Const adTypeText = 2


pAccion =Request.Form("pAccion")

 'hdatos = Request.form("hDatos")

'GED 27-9-2010 DESENCODEA CHARS
hdatos = replace(Request.form("hDatos"),"~~","&") 
 
 hTipoImp = Request.form("hTipoImp")




'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Set XMLDatos = Server.CreateObject("MSXML2.DOMDocument")

XMLDatos.async = false

XMLDatos.loadXML(hDatos)


'***************************************************************************************
'Response.Write "<textarea>" & pAccion & "</textarea>"
'Response.End
'***************************************************************************************


'***************** Llama componente Genera PDF *********************
Select case pAccion

	
case "IMPRIMIR"
	
			mvarRequest = "<Request>" &_
			"<DEFINICION>ismGeneratePdfReport.xml</DEFINICION>" &_						
			"<Raiz>share:generatePdfReport</Raiz>" &_
			"<xmlDataSource>"& XMLDatos.xml & "</xmlDataSource>" &_									
			"<reportId>"& hTipoImp &"</reportId>" &_															
			"</Request>"
	
	'	if XMLDatos.selectSingleNode("//APELLIDORAZONSOCIAL") is nothing then

	if not XMLDatos.selectSingleNode("//APELLIDORAZONSOCIAL") is nothing then
						
	wvarError = cmdp_ExecuteTrn( "lbaw_OVMWGen",  ,  mvarRequest,  mvarResponse)
	'***************************************************************************************
   'Response.Write "<textarea>lbaw_OVMWGen: " & mvarRequest & "</textarea>"
   'Response.End
    '***************************************************************************************

	Set XMLPdf = Server.CreateObject("MSXML2.DOMDocument") : XMLPdf.async = False : XMLPdf.loadXML  mvarResponse	    
	
	If  XMLPdf.selectSingleNode("//Estado").Attributes.getNamedItem("resultado").text ="true" Then  	'Verifica que haya respuesta de MQ
	
		If not  XMLPdf.selectSingleNode("//generatePdfReportReturn") is nothing	Then	  'Verifica que exista el PDF si no error Middle	
		
			Response.ContentType = "APPLICATION/PDF"	
				
			Response.AddHeader "Content-Disposition","inline"
							
			nodoPDF=XMLPdf.documentElement.selectSingleNode("//generatePdfReportReturn").text	
				
			Set XMLPdf2 = Server.CreateObject("MSXML2.DOMDocument") : XMLPdf2.async = False	
				
			XMLPdf2.loadXML  "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & nodoPDF & "</PDF>" 	
				
	 		Set oNode2 = XMLPdf2.documentElement.selectSingleNode("//PDF") 	 		
	 	
	 		Set objStream = Server.CreateObject("ADODB.Stream") 	
	 			
			objStream.CharSet="Windows-1252":objStream.Mode= 0 :objStream.Type = 1 	
				
			objStream.Open
					
			objStream.Write oNode2.nodeTypedValue
					
			objStream.Position = 0		
				
			Response.BinaryWrite objStream.Read	
				
			objStream.Close
	
		Else ' Si no existe PDF ==> error de Middle
			Response.ContentType = "text/html"
			
			Response.AddHeader "Content-Disposition","inline"
			%>
			<table width="100%" border="0" bgcolor="#CCCCCC">
				<tr>
				<td style="FONT-SIZE: 11px; FONT-FAMILY:Arial;" align='center' >El Servicio de consulta no se encuentra disponible. Error:3</td>
				</tr>
			</table>
			<!--Nota: ERROR 3: El servicio de Generacion de Impresos de Middleware no esta disponible, o no existe la template.-->
				<!--<%="mvarRequest: "& mvarRequest &"Request.Form('hdatos'): "& Request.Form("hdatos")%>-->
			<%
		End If	
	Else
	
		Response.ContentType = "text/html"
		
		Response.AddHeader "Content-Disposition","inline"
			%>
			<table width="100%" border="0" bgcolor="#CCCCCC" >
				<tr>
					<td align="center" style="FONT-SIZE: 11px; FONT-FAMILY:Arial;">El Servicio de consulta no se encuentra disponible. Error:2</td>
				</tr>
			</table>
			 <!-- Nota: ERROR 2: Este error se produce unicamente cuando hay un problema en el servidor de comunicaciones MQ. -->
			<%
		
	End If

else
	Response.ContentType = "text/html"
	
	Response.AddHeader "Content-Disposition","inline"
%>
	<table width="100%" border="0" bgcolor="#CCCCCC">
			<tr>
				<td align="center" style="FONT-SIZE: 11px; FONT-FAMILY:Arial;">El Servicio de consulta no se encuentra disponible. Error: 1</td>
			</tr>
		</table>
	
<%
 	
end if	


case "ENVIOMAIL"

'if instr(1,Request.Form("emailCliente"),"|") then

'	mails=split(Request.Form("emailCliente"),"|")	
	mvarEmailCliente= Request.Form("mailCliente_Para")  'mails(0)
	mvarEmailCC=   Request.Form("mailCliente_Copia")        'mails(1)
'else	
	'mvarEmailCliente=Request.Form("emailCliente")	
	'mvarEmailCC=""
'end if
dim mvarNombrePDF

    
    if hTipoImp = "COTI_ICB_Cliente" then
        mvarNombrePDF = "Cotizacion ICBB.pdf"
    else
        mvarNombrePDF = "Cotizacion ICO.pdf"
    End if
        
    mvarRequest = "<Request>" &_
		"<DEFINICION>ismGenerateAndSendPdfReport.xml</DEFINICION>" &_						
		"<Raiz>share:generateAndSendPdfReport</Raiz>"&_
		"<xmlDataSource>"& XMLDatos.xml & "</xmlDataSource>" &_	
		"<applicationId>LBAVO</applicationId>" &_					
		"<reportId>"& hTipoImp &"</reportId>" &_
		"<attachPassword></attachPassword>" &_	
		"<attachFileName>" & mvarNombrePDF & "</attachFileName>" &_
		"<recipientTO>" & mvarEmailCliente  & "</recipientTO>"&_
		"<recipientsCC>" & mvarEmailCC & "</recipientsCC>" &_
		"<recipientsBCC />" &_		
		"<templateFileName>LBAVO_COTI_CLIENTE</templateFileName>" &_
		"<parametersTemplate />" &_
		"<from></from>" &_
		"<replyTO  />" &_
		"<bodyText ></bodyText>" &_		
		"<subject></subject>" &_
		"<importance />" &_
		"<imagePathFile />" &_
		"</Request>"

'response.write "<textarea cols=50 rows=10>" & mvarRequest  &   "</textarea>"				
				
wvarError = cmdp_ExecuteTrn( "lbaw_OVMWGen",  ,  mvarRequest,  mvarResponse)



Set XMLPdf = Server.CreateObject("MSXML2.DOMDocument") : XMLPdf.async = False : XMLPdf.loadXML  mvarResponse	    

     If XMLPdf.selectSingleNode("//Estado").Attributes.getNamedItem("resultado").text ="true" Then  	'Verifica que haya respuesta de MQ
	

					
					IF XMLPdf.selectSingleNode("//Response/Estado/@resultado").text = "true" then
					'If not  XMLPdf.selectSingleNode("//p894:generateAndSendPdfReportResponse") is nothing	Then	 
		
					%>
					<table width="100%" border="0" bgcolor="#CCCCCC">
							<tr>
								<td align="center" style="FONT-SIZE: 11px; FONT-FAMILY:Arial;">Se ha enviado el mail Correctamente</td>
							</tr>
						</table>
	 				<%
					Else ' Si no existe PDF ==> error de Middle
						
						%>
						<table width="100%" border="0" bgcolor="#CCCCCC">
							<tr>
								<td align="center" style="FONT-SIZE: 11px; FONT-FAMILY:Arial;">El Servicio de consulta no se encuentra disponible. Error:3</td>
							</tr>
						</table>
		
						<%
					End If	
	Else
	
		
			%>
			<table width="100%" border="0" bgcolor="#CCCCCC">
				<tr>
					<td align="center" style="FONT-SIZE: 11px; FONT-FAMILY:Arial;">El Servicio de consulta no se encuentra disponible. Error:2</td>
				</tr>
			</table>
		
			<%
		
	End If




end select


Set objStream = nothing
Set XMLDatos = nothing
Set XMLProyeccion = nothing
Set XMLCoberturas = nothing
Set XMLCotiza = nothing
Set DicCobertura = nothing
Set Fondos = nothing
Set NuevoNodo = nothing
Set Proyecciones = nothing
Set Datos = nothing
Set Edad = nothing
Set Stable = nothing
Set Global = nothing
Set Local = nothing
            %>
