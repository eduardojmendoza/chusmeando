<%
' ------------------------------------------------------------------------------
' Fecha de Modificación: 01/12/2011
' PPCR: 50055/6010661
' Desarrolador: Marcelo Gasparini
' Descripción: Se cambia texto de popup Para Ambos por Ambos
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2011. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: popUp_customizadoOutForm.asp
' 
' Fecha de Creación: 01/06/2011
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Leonardo Ruiz
' 
' Descripción: Marco de la pantalla para mensajes al usr
' 
' ------------------------------------------------------------------------------
%>


<style type="text/css">

#popUp_ImpresionSoli
{
	top:20%; 
	left:10;
	width:350px;
	height:100;
 	z-index:1002; 
 	position:absolute; 
 	color: '#999999'; 
 	background:#FFFFFF; 
 	border:2px solid #999999; 
 	padding: 2em; 
 	font-size:12px;
 	font-weight:bold;
}

#popUp_opacidadOutForm{
	z-index:1001;
	border:none;
	padding:0;	
	top:0;
	left:0;
	background:#000000; 
	position:absolute; 
	filter: alpha(opacity='10');
}
</style>

<!--  Div opacidad  -->
<div id="popUp_opacidadOutForm" style="display:none;"></div>
<!-- Fin Seccion div opacidad -->

<!-- popUp para Impresion de Solicitud -->
<div id="popUp_ImpresionSoli" style="display:none;">
    <div class="areaDatTit">
        <div class="areaDatTitTex" style="width:305">Indique el formulario a imprimir</div>
    </div>
	<label id="msj_alert_popUp_ImpresionSoli"></label>
    <div class="areaDatCpo">
	    <span class="areaDatCpoConInp" style="width:309px">
	        <table width="100%" class="Form_campname_bcoL" >
	            <tr><td align="center">&nbsp;</td></tr>
                <tr><td><input name="copiaSol" id="copiaSoli" style="margin-left:75px" type="radio" value="S"><label for="copia" style="font-size:14px;height=15px">Solicitud</label></td></tr>
                <tr><td><input name="copiaSol" id="copiaCertProv" style="margin-left:75px" type="radio" value="C" checked><label for="copia" style="font-size:14px;height=15px">Certif. Provisorio</label></td></tr>
                <tr><td><input name="copiaSol" id="copiaAmbos" style="margin-left:75px" type="radio" value="A" ><label for="copia" style="font-size:14px;height=15px">Ambos</label></td></tr>
                <tr><td align="center">&nbsp;</td></tr>
            </table>
        </span>
	</div>
	<div id="conten_popUp_ImpresionSoli" style="overflow:scroll"></div>
	<div id="button_close2" width="309px">
	    <table border=0 width=100% height="50px">
	        <tr>
	            <td width="50%"  align="center" style="text-align:center">
	                <a style="vertical-align:bottom" href="" onclick="fncImprimirSoli('I');ocultarPopUp2('popUp_ImpresionSoli'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" align="absmiddle" border="0"> Continuar</a>
	            </td>
	            <td width="50%"  align="left" style="text-align:left">
	                <a style="vertical-align:bottom;" href="" onclick="ocultarPopUp2('popUp_ImpresionSoli'); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_cancel.gif" align="absmiddle" border="0"> Cancelar</a>
	            </td>
	        </tr>
		</table>
	</div>
</div>
<!-- popUp para Impresion Solicitud -->
