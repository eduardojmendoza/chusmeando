/*
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 21/12/2011
' PPCR: 50055/6010661
' Desarrolador: Leonardo Ruiz
' Descripci�n: Soporte de HTTPS.-
--------------------------------------------------------------------------------
'Fecha de Modificaci�n: 02/06/2011
'PPCR: 50055/6010661
'Desarrollador: Leonardo Ruiz
'Descripci�n: Se modifica el formato fecha de MM/DD/AAAA a DD/MM/AAAA
'							Se actualiza la funcion para Separar domicilios.-
--------------------------------------------------------------------------------
 COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
 ALL RIGHTS RESERVED
 
 This software is only to be used for the purpose for which it has been provided.
 No part of it is to be reproduced, disassembled, transmitted, stored in a 
 retrieval system or translated in any human or computer language in any way or
 for any other purposes whatsoever without the prior written consent of the Hong
 Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
 serious civil and criminal offence, which can result in heavy fines and payment 
 of substantial damages.

Nombre del JavaScript: seccionCliente.js

Fecha de Creaci�n: 02/02/2010

PPcR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripci�n: Este archivo contiene todas las funciones concernientes al
	     la seccion cliente modular
	    

--------------------------------------------------------------------------------
*/

/* ************************************************************************* 
	VARIABLES GLOBALES

**************************************************************************/

oXML = new ActiveXObject('MSXML2.DOMDocument');    
oXML.async = false;
/*****************************************************************************/

function fncBuscarDatosClienteBT(pCanalURL)
{
	//18/11/2010 LR - Le agrego sitio seguro a los cotizadores.-
	//var mvarCanalURLCliente = document.getElementById("URLXMLEquiv").value;
	//mvarCanalURLCliente=pCanalURL
	var pTipoDoc=document.getElementById("tipoDocCliente")
	var pDocumento=document.getElementById("nroDocumentoCliente")
	var pApellido=document.getElementById("apellidoCliente")
	
	var boolDocumento = pDocumento.value==""
	var boolApellido = pApellido.value==""
	var boolTipoDoc = pTipoDoc.selectedIndex=="0"
	
	
	if (pDocumento.value=="" && pApellido.value=="" && pTipoDoc.selectedIndex=="0")
	{
		alert("Ingrese Apellido o N�mero de documento")
		return
	}
	
	if (!(boolDocumento || boolApellido))
	{
		alert("Debe buscar por un criterio a la vez")	
		return
	}
	
	if (!boolDocumento || !boolTipoDoc)
	{
		if (pTipoDoc.options[pTipoDoc.selectedIndex].value == "-1") 
		{
			alert("Debe completar Tipo de Documento"); 
			return
		}
		if (pDocumento.value == "") 
		{
			alert("Debe completar N�mero de Documento"); 
			return
		}	
		
	var documento=pDocumento.value
	var tipoDoc= pTipoDoc.options[pTipoDoc.selectedIndex].value	
	//alert(tipoDoc)
	oXMLEquiv = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLEquiv.async = false;
	//18/11/2010 LR - Le agrego sitio seguro a los cotizadores.-
	//var mvarCanalURLCliente = document.getElementById("URLXMLEquiv").value;
	//oXMLEquiv.load(mvarCanalURLCliente +  "/CodigosEquivalencias.xml")
	oXMLEquiv.loadXML(document.getElementById("CodigosEquivalencias").xml)
	
	var oXMLValores=oXMLEquiv.selectNodes("//TIPODOC[CODIGO='"+tipoDoc+"']/EQUIV")	
	var cantNodos=oXMLValores.length
	
	
		  for(var y=0;y<cantNodos ;y++)
			{		
				var tipoDocumentoBT=oXMLValores[y].text;	
				document.getElementById("spanEsperando").innerHTML="<b><br/>Buscando, por favor espere...</b>"	
				var strData = 'FUNCION=mensajeSQL&RequestXMLHTTP=<Request><DEFINICION>COMPARTIDAS\\BT_SPDATOSCLIENTELBA.XML</DEFINICION><clave></clave><tipoDoc>'+tipoDocumentoBT+'</tipoDoc><documento>'+documento+'</documento></Request>';	
				
		
				var respuestaXMLHTTP =fncLlamadaXMLhttp(strData);
				
	 			
			
	if(oXML.loadXML(respuestaXMLHTTP))
	{
		if (oXML.selectSingleNode("//REG"))			
		{		
				var apellido=fncSeparaApellidoNombre("apellido", oXML.selectSingleNode("//RazonSocial").text )
				var nombre=fncSeparaApellidoNombre("nombre", oXML.selectSingleNode("//RazonSocial").text )
				document.getElementById("apellidoCliente").value = apellido;
				document.getElementById("nombreCliente").value = nombre;	
				
				//GED 02-09-2010 DEFECT 72
				//document.getElementById("fechaNacimientoClienteSoli").value = convierteFecha(oXML.selectSingleNode("//Nacimiento").text,"MM/DD/AAAA","DD/MM/AAAA");				
			
				
				//LR 15/09/2010 En algunos clientes viene null en la Fecha de Nacimiento
				if(oXML.selectSingleNode("//Nacimiento"))
				{
				    //	document.getElementById("fechaNacimientoClienteSoli").value = convierteFecha(oXML.selectSingleNode("//Nacimiento").text,"MM/DD/AAAA","DD/MM/AAAA");
				    //	document.getElementById("fechaNacimientoCliente").value = convierteFecha(oXML.selectSingleNode("//Nacimiento").text,"MM/DD/AAAA","DD/MM/AAAA");
				    	document.getElementById("fechaNacimientoClienteSoli").value = oXML.selectSingleNode("//Nacimiento").text;
				    	document.getElementById("fechaNacimientoCliente").value = oXML.selectSingleNode("//Nacimiento").text;
				}
				
				//GED 09-09-2010 DEFECT
				//LR 15/09/2010 En algunos clientes viene null en el Telefono
				if(oXML.selectSingleNode("//TelefonoPart"))
					document.getElementById("telefonoCliente").value = oXML.selectSingleNode("//TelefonoPart").text;
				sincronizaCampos(document.getElementById("telefonoCliente"),document.getElementById('TelPosDomicilio'))
				
				//""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""//
				//LR 29/09/2010 MOSTRAR EL DOMICILIO DEL CLIENTE
				fncSeparaDomiBT(oXML);
        //""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""//
				document.getElementById("xmlDatosClientesBT").value=oXML.xml
				return
		}		
		
	}
	else
	{alert ("Error recuperando datos del cliente")
	return
	}
}

	alert ("No se ha encontrado ning�n cliente de HSBC Bank con los datos ingresados. \n Deber� ingresar manualmente los datos necesarios.");
	
	//**********************************************************************************************//
	//LR 29/09/2010 Limpiar campos propios del modulo al no encontrar el cliente:
	fncLimpiarCamposCli();
	//**********************************************************************************************//
			
	return
}
	if (!boolApellido)
	{
		
		document.getElementById("spanEsperando").innerHTML="<b><br/>Buscando, por favor espere...</b>"	
		var strData = 'FUNCION=mensajeSQL&RequestXMLHTTP=<Request><DEFINICION>COMPARTIDAS\\BT_BUSQUEDAxAPELLIDO.XML</DEFINICION><CANT_REG_RESP>100</CANT_REG_RESP><APELLIDO>'+pApellido.value+'</APELLIDO></Request>';
	
		var respuestaXMLHTTP =fncLlamadaXMLhttpAsync(strData,"resultadoBusquedaBT");	
		
		
	}
	
	
}





function resultadoBusquedaBT(pXML)
{
	
	oXML = new ActiveXObject('MSXML2.DOMDocument');        
		oXML.async = false;  	
		if(oXML.loadXML(pXML))
		{	
		
		
		if (oXML.selectSingleNode("//REG"))
		{		
			var posicionCliente= mostrarPopUp(400,200)		
		}
		else
		{
			alert ("No se ha encontrado ning�n cliente de HSBC Bank con los datos ingresados. \n Deber� ingresar manualmente los datos necesarios.");
			
			//**********************************************************************************************//
			//LR 29/09/2010 Limpiar campos propios del modulo al no encontrar el cliente:
			fncLimpiarCamposCli();
			//**********************************************************************************************//
			
		}
	}
	else
	{alert ("Error recuperando datos del cliente")}
	return
}

function resultadoBusquedaDoc(pXML)
{
	
if(oXML.loadXML(pXML))
	{
		if (oXML.selectSingleNode("//REG"))			
		{
			//**********************************************************************************************//
			//LR 29/09/2010 Limpiar campos propios del modulo al no encontrar el cliente:
			fncLimpiarCamposCli();
			//**********************************************************************************************//
	
				var apellido=fncSeparaApellidoNombre("apellido", oXML.selectSingleNode("//RazonSocial").text )
				var nombre=fncSeparaApellidoNombre("nombre", oXML.selectSingleNode("//RazonSocial").text )
				document.getElementById("apellidoCliente").value = apellido;
				document.getElementById("nombreCliente").value = nombre;	
				
				//GED 02-09-2010 DEFECT 72
				//document.getElementById("fechaNacimientoClienteSoli").value = convierteFecha(oXML.selectSingleNode("//Nacimiento").text,"MM/DD/AAAA","DD/MM/AAAA");				
			
				//""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""//
				//LR 29/09/2010 MOSTRAR EL DOMICILIO DEL CLIENTE
				fncSeparaDomiBT(oXML);
        //""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""//
				//GED 09-09-2010 DEFECT
				//LR 15/09/2010 En algunos clientes viene null en el Telefono
				if(oXML.selectSingleNode("//TelefonoPart"))
					document.getElementById("telefonoCliente").value = oXML.selectSingleNode("//TelefonoPart").text;
				sincronizaCampos(document.getElementById("telefonoCliente"),document.getElementById('TelPosDomicilio'))
				//LR 15/09/2010 En algunos clientes viene null en la Fecha de Nacimiento
				if(oXML.selectSingleNode("//Nacimiento"))
				{
					document.getElementById("fechaNacimientoClienteSoli").value = convierteFecha(oXML.selectSingleNode("//Nacimiento").text,"MM/DD/AAAA","DD/MM/AAAA");
					document.getElementById("fechaNacimientoCliente").value = convierteFecha(oXML.selectSingleNode("//Nacimiento").text,"MM/DD/AAAA","DD/MM/AAAA");
				}
				document.getElementById("xmlDatosClientesBT").value=oXML.xml
				
				return
		}		
		
	}
	else
	{alert ("Error recuperando datos del cliente")
	return
	}	
}

function fncSeparaApellidoNombre(pCampo,pDato)

{
	
	switch (pCampo)
    	{
    		case "apellido":
        	{ 
			if (pDato.search(",")!=-1)
				{
					var vectorApellido=pDato.split(",")
					return trimJS(vectorApellido[0])}
			else
				{
				
				if (pDato.search(" ")!=-1)
				{
					posEspacio=pDato.indexOf(" ",3)
					
					
					return trimJS(pDato.substring(0,posEspacio))
				}
				
				}
		}
		break;
		case "nombre":
        	{ 
			if (pDato.search(",")!=-1)
				{
					var vectorApellido=pDato.split(",")
					return trimJS(vectorApellido[1])
					
					
					}
			else
				{
				if (pDato.search(" ")!=-1)
				{
					posEspacio=pDato.indexOf(" ",3)
					
					
					return trimJS(pDato.substring(posEspacio+1))
				}
				}
		}
		
	}
	
		
	
	
	}




function mostrarPopUp(pX,pY)

{

puntero=-1


var tabla="<table width='100%' bgcolor='#FFFFFF' style='border: solid #D0D0D0 1px';>"+
	"<tr  class='Form_campname_bcoL'><td colspan='3' style='text-align:right' >"+
	
	"<tr  class='form_titulos02c'><td width='200px'>Apellido/Raz�n Social</td><td width='50px'>Tipo Doc</td><td width='100px'>Nro. Doc</td></tr>"
	
tabla+="<tr><td colspan='3'><div style='width:"+pX+"px; height:"+pY+"px; overflow:auto'><table>"
for (var x=0;x<oXML.selectNodes("//REG").length;x++)
	tabla+="<tr id='fila_"+x+ "'  class='form_campname_bcoR'    onclick='fondoGris(this)' >"+
	"<td width='200px' style='text-align:left'>"+
	"<a style='font-weight:normal;color:#000000' ondblclick='completaCliente("+x+")'  href='#'>"+oXML.selectSingleNode("//REG["+x+"]/RazonSocial").text+"</a></td>"+
	"<td width='70px' style='text-align:right'>"+oXML.selectSingleNode("//REG["+x+"]/TipoDoc").text+"</td>"+
	"<td width='120px' style='text-align:right'>"+parseNum(oXML.selectSingleNode("//REG["+x+"]/Documento").text)+"</td>"+
	"</tr>"

tabla+="</table></div></td></tr></table><p></p>"	

popUp_customizado(tabla)




}
var puntero
function completaCliente(x)
{
	if(puntero!=-1)
	{
		if(x=="x")
			x=puntero
			
		//**********************************************************************************************//
		//LR 29/09/2010 Limpiar campos propios del modulo al no encontrar el cliente:
		fncLimpiarCamposCli();
		//**********************************************************************************************//
		
		var oXMLCliente = new ActiveXObject('MSXML2.DOMDocument');    
		var apellido=fncSeparaApellidoNombre("apellido", oXML.selectSingleNode("//REG["+x+"]/RazonSocial").text )
		var nombre=fncSeparaApellidoNombre("nombre", oXML.selectSingleNode("//REG["+x+"]/RazonSocial").text )
				
		document.getElementById("apellidoCliente").value=apellido		
		
		 document.getElementById("nombreCliente").value=nombre		 		 
		
		
		var tipoDoc=oXML.selectSingleNode("//REG["+x+"]/TipoDoc").text
		var documento=oXML.selectSingleNode("//REG["+x+"]/Documento").text
		var strData = 'FUNCION=mensajeSQL&RequestXMLHTTP=<Request><DEFINICION>COMPARTIDAS\\BT_SPDATOSCLIENTELBA.XML</DEFINICION><clave></clave><tipoDoc>'+tipoDoc+'</tipoDoc><documento>'+documento+'</documento></Request>';
		
		var respuestaXMLHTTP =fncLlamadaXMLhttp(strData);
		
		oXMLCliente.async = false;  	
		if(oXMLCliente.loadXML(respuestaXMLHTTP))
		{
			
			//LR 15/09/2010 En algunos clientes viene null en la Fecha de Nacimiento
				if(oXMLCliente.selectSingleNode("//Nacimiento"))
				{
				    //document.getElementById("fechaNacimientoClienteSoli").value = convierteFecha(oXMLCliente.selectSingleNode("//Nacimiento").text, "MM/DD/AAAA", "DD/MM/AAAA");
				    //document.getElementById("fechaNacimientoCliente").value = convierteFecha(oXMLCliente.selectSingleNode("//Nacimiento").text, "MM/DD/AAAA", "DD/MM/AAAA");
				    document.getElementById("fechaNacimientoClienteSoli").value = oXMLCliente.selectSingleNode("//Nacimiento").text;
				    document.getElementById("fechaNacimientoCliente").value = oXMLCliente.selectSingleNode("//Nacimiento").text;
				}
		
		//GED 09-09-2010 DEFECT
		//LR 15/09/2010 En algunos clientes viene null en el Telefono
		if(oXMLCliente.selectSingleNode("//TelefonoPart"))
			document.getElementById("telefonoCliente").value = oXMLCliente.selectSingleNode("//TelefonoPart").text;	
		sincronizaCampos(document.getElementById("telefonoCliente"),document.getElementById('TelPosDomicilio'))
				
		document.getElementById("nroDocumentoCliente").value=parseNum(oXMLCliente.selectSingleNode("//Documento").text)
		
		if(!convierteTipoDoc(oXMLCliente.selectSingleNode("//TipoDoc").text))
		{
			alert("Tipo de Documento incorrecto");
			fncLimpiarCamposCli();
			return false;
		}
		else
			document.getElementById("tipoDocCliente").selectedIndex = convierteTipoDoc(oXMLCliente.selectSingleNode("//TipoDoc").text) //convertir con xml
		
			//""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""//
			//LR 29/09/2010 MOSTRAR EL DOMICILIO DEL CLIENTE
			fncSeparaDomiBT(oXMLCliente);
      //""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""//
		}
			
		
		
		document.getElementById("xmlDatosClientesBT").value=oXML.xml
		
			ocultarPopUp();			
		}
}

function convierteTipoDoc(pTipoDocBT)
{
/*	var equivTipoDoc = "<EQUIVALENCIAS>"+
	"<TIPODOC>"+
		"<CODIGO>1</CODIGO>"+
		"<EQUIV>DEX</EQUIV>"+
		"<DESCRIPCION>Documento de extranjeros</DESCRIPCION>"+
	"</TIPODOC>"+
	"<TIPODOC>"+
		"<CODIGO>1</CODIGO>"+
		"<EQUIV>DNI</EQUIV>"+
		"<DESCRIPCION>Documento Nacional de Identidad</DESCRIPCION>"+
	"</TIPODOC>"+
	"<TIPODOC>"+
		"<CODIGO>2</CODIGO>"+
		"<EQUIV>LE</EQUIV>"+
		"<DESCRIPCION>Libreta Civica</DESCRIPCION>"+
	"</TIPODOC>"+
	"<TIPODOC>"+
		"<CODIGO>3</CODIGO>"+
		"<EQUIV>LC</EQUIV>"+
		"<DESCRIPCION>Libreta de Enrolamiento</DESCRIPCION>"+
	"</TIPODOC>"+
	"<TIPODOC>"+
		"<CODIGO>47</CODIGO>"+
		"<EQUIV>PAS</EQUIV>"+
		"<DESCRIPCION>Pasaporte</DESCRIPCION>"+
	"</TIPODOC>"+
	"<TIPODOC>"+
		"<CODIGO>4</CODIGO>"+
		"<EQUIV>SOC</EQUIV>"+
		"<DESCRIPCION>Sociedades</DESCRIPCION>"+
	"</TIPODOC>"+
"</EQUIVALENCIAS>"
*/
	
	oXMLEquiv = new ActiveXObject('MSXML2.DOMDocument');        
	oXMLEquiv.async = false;
	//18/11/2010 LR - Le agrego sitio seguro a los cotizadores.-
	//var mvarCanalURLCliente = document.getElementById("URLXMLEquiv").value;
	//oXMLEquiv.loadXML(equivTipoDoc)
	//oXMLEquiv.load(mvarCanalURLCliente +  "/CodigosEquivalencias.xml")
	oXMLEquiv.loadXML(document.getElementById("CodigosEquivalencias").xml)
	
	//16/11/2010 LR  compruebo que sea valida la respuesta del tipo de doc encontrado
	if(oXMLEquiv.selectSingleNode("//TIPODOC[EQUIV='"+pTipoDocBT+"']/CODIGO"))
		return oXMLEquiv.selectSingleNode("//TIPODOC[EQUIV='"+pTipoDocBT+"']/CODIGO").text;
	else
		return false
}


function ocultarPopUp()
{
	document.getElementById('popUp_opacidad').style.display ='none';
	document.getElementById('popUp_mensaje').style.display ='none';
	document.getElementById('msj_alert').innerHTML = '';
	
}

function colorRojo(pElement)
{
	pElement.style.color='#ff0000'
	pElement.style.textDecoration='underline'
	pElement.style.fontWeight='bold'	
	
}

function colorNegro(pElement)
{
	pElement.style.color='#000000'
	pElement.style.textDecoration=''
	pElement.style.fontWeight=''
	
}

function fondoGris(pElement)
{	
	if(puntero!=-1 ) document.getElementById("fila_" +puntero).style.backgroundColor="#FFFFFF"
	puntero=pElement.id.split("_")[1]
	pElement.style.backgroundColor='#A0A0A0'
}
function fondoBlanco(pElement)
{
	
	
}

     
/*function convierteFecha(pFecha,pFormatoOrigen,pFormatoDestino)
{
	switch (pFormatoOrigen.toUpperCase())
	{
		case "MM/DD/AAAA":
		{
		var partesFecha= pFecha.split("/")
		if (pFormatoDestino.toUpperCase()=="DD/MM/AAAA") {return  partesFecha[1] +"/"+  partesFecha[0] +"/"+  partesFecha[2] }
		
		break;
		}
		case "DD/MM/AAAA":
		{
		
		var partesFecha= pFecha.split("/")
		if (partesFecha[0].length==1) {partesFecha[0]= "0" +  partesFecha[0]}
		if (partesFecha[1].length==1) {partesFecha[1]= "0" +  partesFecha[1]}
		
		if (pFormatoDestino.toUpperCase()=="AAAAMMDD") {return partesFecha[2] + partesFecha[1]  + partesFecha[0] }
		
		break;
		}
		
	
}

}*/

function parseNum(Num)
{
    while (Num.charAt(0) == '0') {
        Num = Num.substring(1, Num.length);
    }

    return Num;
}

function trimJS(texto01) 
{
	while(texto01.substring(0,1) == ' ')
		texto01 = texto01.substring(1,texto01.length);

	while(texto01.substring(texto01.length-1,texto01.length) == ' ')
		texto01 =texto01.substring(0, texto01.length-1);   
	
	return texto01;
}
/******************************************************************************************/
function fncLimpiarCamposCli()
{
	//alert("entro a fncLimpiarCamposCli")
	//LR 29/09/2010 Limpiar campos propios del modulo al no encontrar el cliente:
	//��������������������������������������������������������������������//
	//Tipo de Documento
	document.getElementById("tipoDocCliente").value = -1;
	//Nro Doc
	document.getElementById("nroDocumentoCliente").value = "";
	//Apellido/Raz�n Social
	document.getElementById("apellidoCliente").value = "";
	//Nombre/s
	document.getElementById("nombreCliente").value = "";
	//Fecha de Nacimiento
	document.getElementById("fechaNacimientoClienteSoli").value = "";
	document.getElementById("fechaNacimientoCliente").value = "";
	//Sexo
  document.getElementById("sexoCliente").value = -1;
  //Estado Civil
  document.getElementById("estadoCivilCliente").value = "";
  //Pa�s de Nacimiento
  document.getElementById("paisNacimientoCliente").value = "00";
  //TELEFONO
  document.getElementById("codTelefonoCliente").value = "";
  document.getElementById("telefonoCliente").value = "";
  //EMAIL
  document.getElementById("emailCliente").value = "";
  //Domicilio
  document.getElementById("calleDomicilio").value = "";
  document.getElementById("nroDomicilio").value = "";
  document.getElementById("pisoDomicilio").value = "";
  document.getElementById("dptoDomicilio").value = "";
	//��������������������������������������������������������������������//
}
/******************************************************************************************/
function fncSeparaDomiBT(oXMLDomiBT)
{
	//LR 30/09/2010 Esta fnc Separa el Domicilio traido de BT
	if(oXMLDomiBT.selectSingleNode("//DomicilioPart"))
	{
		//Campos a utilizar durante este ciclo para separar el domicilio
		pCalle = document.getElementById("calleDomicilio");
	  pAltura = document.getElementById("nroDomicilio");
	  pPiso = document.getElementById("pisoDomicilio");
	  pDpto = document.getElementById("dptoDomicilio");
	  pLoc		=	'localidadDomicilio'
		pProv		= 'provinciaDomicilio'
		pCodPos	= 'codPosDomicilio'
		
		//DOMICDOM	char(30)	pCalle
		//DOMICDNU	char(5)		pAltura
		//DOMICPIS	char(4)		pPiso
		//DOMICPTA	char(4)		pDpto
		//��������������������������������������������������������������������//
		var wvarDomitxt = trim(oXMLDomiBT.selectSingleNode("//DomicilioPart").text);
		wvarDomitxt = wvarDomitxt.replace(/   /g," ")
		wvarDomitxt = wvarDomitxt.replace(/  /g," ")
	  var wvarDomi = wvarDomitxt.split(" ");
	  var wvarCountDomi = 0;
	  var wvarCalle = '';
	  var wvarAltura = '';
	  var wvarPiso = '';
	  var wvarDpto = '';
	  var wvarPositionArray = 0;
	  var wvarDomFueraFormat = false;
	  //��������������������������������������������������������������������//
	  for (var i=0; i<=wvarDomi.length-1; i++)
	  {
	  	//Obtener Calle y Nro
	    if(wvarCountDomi == 0)
	    {
	      if(isNaN(wvarDomi[i]))
	      {
	      	//CALLE: wvarCountDomi == 0, antes de q encuentre el primer nro lo anterior es la calle
	      	wvarCalle = trim(wvarCalle +" "+ wvarDomi[i]);
	      }
	      else
	      {
	      	//ALTURA: wvarCountDomi == 0, El primer nro q encuentre es la altura de la calle	
	      	wvarAltura = wvarDomi[i];
	      	wvarCountDomi++;
	      	wvarPositionArray = i;
	      	if(wvarAltura.length > 5)
	      		wvarDomFueraFormat = true;
	      }
	    }
	    else
	    {
	      //Obtener Piso
	      if(wvarDomi[wvarPositionArray+1])
	      {
	      	wvarPiso = wvarDomi[wvarPositionArray+1];
	      	if(wvarPiso.length > 4)
	      		wvarDomFueraFormat = true;
	      }
	      //Obtener Dto
	      if(wvarDomi[wvarPositionArray+2])
	      {
	      	wvarDpto = wvarDomi[wvarPositionArray+2];
	      	if(wvarDpto.length > 4)
	      		wvarDomFueraFormat = true;
	      }
	      //Si tiene mas campos q hasta el departamento
	      if(wvarDomi[wvarPositionArray+3])
	      	wvarDomFueraFormat = true;
	    }
	  }
	  //��������������������������������������������������������������������//
	  //Entregarle el resultado a los campos del domicilio
	  if(wvarDomFueraFormat)
	  	pCalle.value = wvarDomitxt.substr(0,30);
	  else
	  {
		  pCalle.value = wvarCalle.substr(0,30);
		  pAltura.value = wvarAltura.substr(0,5);
		  pPiso.value = wvarPiso.substr(0,4);
		  pDpto.value = wvarDpto.substr(0,4);
		 }
	  //��������������������������������������������������������������������//
	}
	//MC 
	if(oXMLDomiBT.selectSingleNode("//LocalidadPart"))
		document.getElementById(pLoc).value = oXMLDomiBT.selectSingleNode("//LocalidadPart").text;
		
	if (oXMLDomiBT.selectSingleNode("//CPPart"))
	{
		var oXMLProv = new ActiveXObject('MSXML2.DOMDocument');
		oXMLProv.async = false;
		
		var codPostal= Number(oXMLDomiBT.selectSingleNode("//CPPart").text)
		if (isNaN(codPostal)  || codPostal > 9999)
			{return}
		var strData = 'FUNCION=mensajeSQL&RequestXMLHTTP=<Request><DEFINICION>P_PROD_TRAERPROVXCP_SELECT.XML</DEFINICION><CODPOS>'+ oXMLDomiBT.selectSingleNode("//CPPart").text + '</CODPOS></Request>';
		
		var respuestaXMLHTTP =fncLlamadaXMLhttp(strData);		
		if(oXMLProv.loadXML(respuestaXMLHTTP))
			if (oXMLProv.selectSingleNode("//REG"))
			{					
				recuperaCombo(pProv,oXMLProv.selectSingleNode("//RETORNO").text);
				document.getElementById(pCodPos).value=eval(oXMLDomiBT.selectSingleNode("//CPPart").text)
			}		
	}
}
/******************************************************************************************/
