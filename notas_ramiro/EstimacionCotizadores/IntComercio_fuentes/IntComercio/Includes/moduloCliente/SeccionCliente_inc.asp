<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 20/08/2012
' Ticket: SPLIT LBA
' Desarrollador: Adriana Armati
' Descripci�n: Se elimina el acceso a RRHH
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 21/12/2011
' PPCR: 50055/6010661
' Desarrolador: Leonardo Ruiz
' Descripci�n: Soporte de HTTPS.-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: SeccionCliente_inc.asp
' 
' Fecha de Creaci�n: 04/02/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados al Cliente y realiza la busqueda de cliente en la BT
'		
'
'  <!-- #include file="../Includes/moduloCliente/SeccionCliente_inc.asp"-->	   
' 
' ------------------------------------------------------------------------------
'*********************************************************
'************ Funciones ASP del m�dulo de clientes ****************
'*********************************************************


function FormatearFechaGenerica(mvarFecha)
dim pos1, pos2, mvarArr
	FormatearFechaGenerica="0"
	
	if (len(mvarFecha)>= 8 ) or ( len(mvarFecha) <=10) then
		pos1=Instr(1,mvarFecha, "/")
		pos2=Instr(3,mvarFecha, "/")
		if (pos1<> 0) and (pos2<>0) then 
			
			mvarArr=split(mvarFecha,"/")
		
		FormatearFechaGenerica=mvarArr(2) & right("0" & mvarArr(1),2) & right("0" & mvarArr(0),2)
	end if
		
	end if
	
	
end Function 

function fncGetTiposDocu(pvarUsuarcod)
dim wvarOptTDoc, wvarRequest, wvarResponse, wobjXMLDoc
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~
    'INI: Combo Tipos Docum.
    '~~~~~~~~~~~~~~~~~~~~~~~~
    wvarRequest = "<Request></Request>"
    Call cmdp_ExecuteTrn("lbaw_GetDocumentos", "lbaw_GetDocumentos.biz", wvarRequest, wvarResponse)
    Set wobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
        wobjXMLDoc.async = false
    Call wobjXMLDoc.loadXML(wvarResponse)
    '
    If Not wobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then	
        If UCase((wobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text)) = "TRUE" then
            wvarOptTDoc = replace(replace(replace(wvarResponse,"<Response><Estado resultado=""true"" mensaje="""" />",""),"</Response>",""), "|", "&nbsp;")
            'HRF 2010-11-19 Doy vuelta LE/LC
            wvarOptTDoc = replace(wvarOptTDoc,"value=""2"">L.C.","value=""2"">L.E.")
            wvarOptTDoc = replace(wvarOptTDoc,"value=""3"">L.E.","value=""3"">L.C.")
        Else
            wvarMsgError = "Se ha producido un error." & wobjXMLDoc.selectSingleNode("//Response/Estado/@mensaje").text	
        End If
    Else
        wvarMsgError = "Se ha producido un error."
    End If
    Set wobjXMLDoc = Nothing
    '~~~~~~~~~~~~~~~~~~~~~~~~
    'FIN: Combo Tipos Docum.
    '~~~~~~~~~~~~~~~~~~~~~~~~
    '
    fncGetTiposDocu = wvarOptTDoc
end function

'***************************************************************
'************ Variables de Ubicacion de dependencias *********************
' *****Importante cambiar cuando se importe  este modulo global en la aplicacion***
'**************************************************************

'Adriana 21-8-12
'if mvarACCEDE_BT ="S" then
'    mvarCANALHSBC="S"
'else
'    mvarCANALHSBC="N"
'end if

dim mvarIncludesCSS, mvarIncludesJS, mvarURLImages, mvarURLCalendario, mvarOptTDoc
mvarIncludesCSS = mvarCANALURL & "/Includes/moduloCliente"

' mvarIncludesJS ==> Ubicacion de las librerias JS 
  mvarIncludesJS = mvarCANALURL & "/Includes/moduloCliente"

' mvarURLImages ==> Ubicacion de los archivos de imagenes en la aplicacion que utilice este m�dulo
  mvarURLImages = mvarCANALURL & "/Includes/moduloCliente/images"

'mvarURLCalendario ==> Ubicacion de los archivos de la libreria de calendario en la aplicacion que utilice este m�dulo
  mvarURLCalendario = mvarCANALURL & "/Includes/moduloCliente/calendario"
'18/11/2010 LR - Le agrego sitio seguro a los cotizadores.-
mvarURLXMLEquiv= mvarCANALURLSecure & "Includes/moduloCliente"

mvarOptTDoc = fncGetTiposDocu(Session("USUARCOD"))

%>
 <!-- Incluye librerias de acceso a base de banca telefonica  -->
 
 <link type="text/css" rel="stylesheet" href="<%=mvarIncludesCSS %>/seccionCliente.css" >

<script type="text/javascript"  src="<%=mvarIncludesJS %>/seccionCliente.js"></script>

<xml id='CodigosEquivalencias'  src='<%=mvarIncludesCSS%>/CodigosEquivalencias.xml'></xml>

<xml id="xmlDatosClientesBT" ></xml>

<input type="hidden" id="fechaHoy" name="fechahoy" value="<%=FormatearFechaGenerica(fechaHoy()) %>"  />
<!-- 18/11/2010 LR - Le agrego sitio seguro a los cotizadores.- -->
<input type="hidden" id="URLXMLEquiv" name="URLXMLEquiv" value="<%= mvarURLXMLEquiv %>"  />
<!--div class="areaDat"-->
    <div class="areaDatCpo">
<div id="seccionDatosCliente">
        <div class="areaDatTit">
            <div class="areaDatTitTex">Datos del Cliente</div>
        </div>
       <div class="areaDatCpoCon"> 
            <div class="areaDatCpoConInp">
               <!-- TIPO DOCUMENTO -->
               <label id="tipoDocCliente_etiq" for="tipoDocCliente">Tipo de Documento:</label>
               <!-- 19/10/2010 LR Defect 21 Estetica, pto 35 -->
                <select class="areaDatCpoConInpFieM" id="tipoDocCliente" onchange="fncCheckNomCUITOpc();" validacionPersonalizada="fncValCliTipoPersona();" name="tipoDocCliente"  >
                <option value="-1">Seleccionar...</option>
                        <%= mvarOptTDoc %>
                  </select>
                        
                <!-- NRO DOCUMENTO -->
                <label  id="nroDocumentoCliente_etiq" for="nroDocumentoCliente">N�mero Documento:</label>
                  <input class="areaDatCpoConInpFieM" id="nroDocumentoCliente" name="nroDocumentoCliente" type="text" validacionPersonalizada="verificaCUIT('nroDocumentoCliente','tipoDocCliente')" validarTipeo="numero" validar="&gt; 0" mensajeError="Nro de documento Inv�lido"  maxlength="11" onchange="" onkeypress="capturaTecla()"  >
               </div>
       
                <div class="areaDatCpoConInp">
                    <!-- AP/RAZON SOCIAL -->
                    <label id="apellidoCliente_etiq" for="apellidoCliente">Apellido/Raz�n Social:</label>
                    <input class="areaDatCpoConInpFieM" id="apellidoCliente" name="apellidoCliente" type="text" validarTipeo="apellido" onkeypress="capturaTecla()" maxlength="40" onpaste="return false;" />
                    <div id="apellidoCliente_popUp" style="position:absolute;  width:300px;  height:200;  z-index:-100; visibility:hidden;"></div>
                    <!-- NOMBRE -->
                    <label id="nombreCliente_etiq" for="nombreCliente">Nombre/s:</label>
                    <!-- 26/10/2010 LR Defect 30, pto 2 y 3. -->
                    <input class="areaDatCpoConInpFieM" id="nombreCliente" name="nombreCliente" type="text" validarTipeo="apellido" onkeypress="capturaTecla()" maxlength="30" onpaste="return false;" />
                    
                    <!-- 08/10/2010 LR Defect 21 Estetica, pto 15 -->
                    <div style="padding-left:154px">   
                     <!-- Adriana 22-8-23 -->                 
                        <span  id="divBtnBuscarCliente" style="<%if mvarACCEDE_BT="S" then response.write "display:inline" else response.write "display:none" end if %>">
                            <a href="#" onclick="fncBuscarDatosClienteBT('<%=mvarURLXMLEquiv%>')" class="linksubir"> <img border='0' src='<%=mvarURLImages %>/boton_buscar.gif' /> Buscar Cliente</a>
                        </span> 
                   </div>
              </div>
              
              <div class="areaDatCpoConInp"  style="display: none">
              
               <label id="fechaNacimientoCliente_etiq">Fecha de Nacimiento:</label>
               <input class="areaDatCpoConInpFieM" id="fechaNacimientoCliente" name="fechaNacimientoCliente" type="text" obligatorio="si"   validarTipeo="fecha" validar="&lt; document.getElementById('fechaHoy').value" mensajeError="Fecha de Nacimiento  no puede ser mayor a la fecha del d�a"  maxlength="10" onchange="sincronizaCampos(this,fechaNacimientoClienteSoli)" onBlur="return validateDateCal(this);">              
			   <input type="image" onClick="popFrame.fPopCalendar(document.getElementById('fechaNacimientoCliente'),document.getElementById('fechaNacimientoCliente'),popCal);return false" value="V" src="<%=mvarURLCalendario%>/calbtn.gif"></input>
	                 
	          
               
              </div>
              
             
        
        </div>


</div>
</div><!--/div-->



