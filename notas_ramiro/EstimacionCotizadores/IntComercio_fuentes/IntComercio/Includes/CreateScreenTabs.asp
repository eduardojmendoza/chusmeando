<%
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: CreateScreenTabs.asp
' 
' Fecha de Creaci�n: 15/07/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D Agnone
' 
' Descripci�n: Desarrollo de funciones gen�ricas para la implementacion de solapas
'			   para los formularios de la aplicaci�n.
' 
' ------------------------------------------------------------------------------
%>

<%	

	sub CreateScreenTabs(paScreenTabs,paNameTabs, pColorTab, pFrom, pTo, pTabColorDependency, paScreenTabsDesc, pimgPath, pVerSolapa)
	'Este c&oacutedigo hace que el armado de hojuelas sea mas r&aacutepido y din&aacutemico
	'Lo &uacutenico que deben hacer es:
	'1� - Dimensionar las variables a la cantidad de Tabs que desean crear
	'2� - Asegurarse de que los DIVS esten creados correctamente y poseean su correspondiente id y que los mismos tengan la siguiente estructura "div + sScreenTabs(n)"
	'3� - Cargar las matrices del siguiente modo 
	'	a. aScreenTabs es la matriz identificatoria del Tabs (NO DEBE LLEVAR LA PALABRA "DIV")
	'	b. aNameTabs es la matriz que contiene los nombres a mostrar aen cada tab
	'4� Se debe respetar el orden de pFrom y pTo, valores que indicaran desde que parte la matriz deber&aacute dibujar
	'5� El resto delas variables son los archvos que se utililizan para armar el tab, el color de fondo de la celda intermedia,
	'   salvo en las interiores que deben recibir el tab superior en la que esten contenidas.
	'6� pTabColorDependency debe ser cargado si el tabs tiene tabs internos 
	'Creado: Mauricio Reyna 
	'Fecha : 20010818
	'&uacuteltima modificaci&oacuten:
	'
		dim cellWidth
		dim i
		cellWidth = "8px" 'ancho de las celdas de las imagenes	
		Response.Write "<div id=""divAlt"" class=""Form_altText"" style=""display:none; position:absolute""></div>"
		response.write "<script language=""JavaScript"">" & chr(13) & "<!--" &  chr(13)
        response.Write "//aScreenTabs= new Array();" & chr(13) 
		for i = pFrom to pTo
			response.write "aScreenTabs[" & (i-1) & "] ='div"  & paScreenTabs(i) & "';" & chr(13) 'Carga la matriz del cliente
		next 

		response.write  chr(13) & "//-->"& chr(13) &"</script>" & chr(13)

		'Navegador de solapas (izquierda)
		response.write "<table id=""tablaEtiqSolapas"" width=""100%"" height="""" cellspacing=""0"" cellpadding=""0"" border=""0"" style=""background-image:url(" & pimgPath & "solapas_fondo_tabla.jpg); background-repeat:repeat-x; background-position:top; display:none"">" ' Comienza a dibujar
		response.write "<tr class=""form_titulos02"">"
		response.write chr(13) & "<td width=""" & cellWidth & """ height="""" align=""right"" BGCOLOR=""#FFFFFF""><IMG src=""" & pimgPath & "solapas_ls_of.jpg"" border=""0""></td>"
		response.write chr(13) & "<TD style=""padding-bottom:10px; background-image:url(" & pimgPath & "solapas_center_of.jpg); background-repeat:repeat-x; background-position:top;"" width=""15px"" height="""" align=""Center"" BGCOLOR=""" & pColorTab & """ onMouseOut=""hideAlt();"" onMouseOver=""showAlt('Ir a la solapa anterior')""><span id='span_irSolapaAnterior' style=""FONT-WEIGHT: bold;    FONT-SIZE: 11px;    COLOR: #003366; vertical-align: middle;    FONT-FAMILY: Arial;text-align:center; width: 100%; cursor:hand"" onclick=""Javascript:javascript:verSolapa(-1)"" >&lt&lt</span></td>"
		
		'Estructura interna de solapas
		response.write chr(13) & "<td  width=""" & cellWidth & """ height="""" align=""left"" BGCOLOR=""#FFFFFF""><IMG id=""L0"" src=""" & pimgPath & "solapas_mi_of.jpg"" border=""0""></td>"
		
		
		for i = pFrom to pTo
			response.write chr(13) & "<TD id=""TD" & i & """  style=""display:" & pVerSolapa(i) & """; padding-bottom:10px; background-image:url(" & pimgPath & "solapas_center_of.jpg); background-repeat:repeat-x; background-position:top;"" height="""" align=""Center"" BGCOLOR=""#FFFFFF"" " & pColorTab & """ onMouseOut=""hideAlt();"" onMouseOver=""showAlt('" & paScreenTabsDesc(i) & "')""><span id=""spanTextoEtiqSolapa_" & i & """  style="" FONT-WEIGHT: bold;FONT-SIZE: 11px; COLOR: #003366; vertical-align: top; FONT-FAMILY: Arial;text-align:center;width: 100%; cursor:hand"" onclick=""Javascript:mostrarTab('div"& paScreenTabs(i) & "','', '" & pColorTab & "', '"& pTabColorDependency & "');""  >" & aNameTabs(i) & "</span></td>"
			response.write chr(13) & "<td id=""DTD" & i & """ style=""display:" & pVerSolapa(i) & """  width=""" & cellWidth & """ height="""" align=""left"" BGCOLOR=""#FFFFFF""><IMG id=""R" & i & """ src=""" & pimgPath & "solapas_mi_of.jpg"" border=""0""></td>"
		next

		'Navegador de solapas (derecha)
		response.write chr(13) & "<TD style=""padding-bottom:10px; background-image:url(" & pimgPath & "solapas_center_of.jpg); background-repeat:repeat-x; background-position:top;"" width=""15px"" height="""" align=""Center"" BGCOLOR=""" & pColorTab & """ onMouseOut=""hideAlt();"" onMouseOver=""showAlt('Ir a la solapa siguiente')""><span id='span_irSolapaSiguiente' style=""FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #003366; vertical-align: middle; FONT-FAMILY: Arial;text-align:center; width: 100%; cursor:hand"" onclick=""Javascript:javascript:verSolapa(1)"" >&gt&gt</span></td>"
		
		response.write chr(13) & "<td width=""" & cellWidth & """ height="""" align=""left"" BGCOLOR=""#FFFFFF""><IMG src=""" & pimgPath & "solapas_re_of.jpg"" border=""0""></td>"
		response.write 	"</tr>" & chr(13) &"<tr>"
				
		response.write 	"</tr>" & chr(13) & "</table>"
		
	end sub




	sub CreateScreenTabsInt(paScreenTabsInt,paNameTabsInt, pimgFirstInt, pimgLeftInt, pimgRightInt, pColorTab, pFrom, pTo, pDesc, pContainer)

		dim cellWidth
		dim i
		cellWidth = "10px"	

		response.write "<script language=""JavaScript"">" & chr(13) & "<!--" &  chr(13)
		for i = pFrom to pTo
			response.write "aScreenTabsInt[" & (i-1) & "] = new objTab('div" & paScreenTabsInt(i) & "','" &  pContainer & "')" & ";" & chr(13)
		next 
		response.write  chr(13) & "//-->"& chr(13) &"</script>" & chr(13)
		response.write "<table width='100%'  cellspacing='0' cellpadding='0' border='0'  BGCOLOR='#000000'>"
		response.write "<tr>"
		for i = pFrom to pTo
			if i = 1 then 
				response.write chr(13) & "<td width=""" & cellWidth & """ height=""19px"" align=""right"" BGCOLOR=""#FFFFFF""><IMG src="& pimgFirstInt & " border=""0""></td>"
			else
				response.write chr(13) & "<td width=""" & cellWidth & """ height=""19px"" align=""right"" BGCOLOR=""#FFFFFF""><IMG src="& pimgLeftInt & " border=""0""></td>"
			end if
			response.write chr(13) & "<TD height=""19px"" align=""Center"" bgcolor=" & pColorTab & "><a hRef=""Javascript:mostrarTab('div"& paScreenTabsInt(i) & "',1, '" & pColorTab & "','');"" class=""LinkInnerCredit"">" & aNameTabsInt(i) & "</a></td>"
			response.write chr(13) & "<td width=""" & cellWidth & """ height=""19px"" align=""left"" BGCOLOR=""#FFFFFF""><IMG src=" & pimgRightInt & " border=""0""></td>"
		next
		response.write "<td BGCOLOR=""#FFFFFF"" class=""heading4"" colspan=""3"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=""../Imagenes/General/icono_ani.gif"" border=0 alt="""">" & "&nbsp;&nbsp;" & pDesc & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>"
		response.write 	"</tr>" & chr(13) &"<tr>"
		response.write chr(13) & "<td id=""TD" & paScreenTabsInt(pFrom)& """ height=""3px"" BGCOLOR=" & pColorTab & " colspan=""3""></td>"
		for i = pFrom + 1 to pTo
			response.write chr(13) & "<td id=""TD" & paScreenTabsInt(i) & """ height=""3px"" BGCOLOR=""#DEDFDE"" colspan=""3""></td>"
		next
		response.write "<td height=""3px"" BGCOLOR=""#FFFFFF"" colspan=""3""></td>"
		response.write 	"</tr>" & chr(13) & "</table>"
	end sub
%>

