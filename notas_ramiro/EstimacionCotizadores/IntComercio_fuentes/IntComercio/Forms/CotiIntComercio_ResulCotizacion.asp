<%
'--------------------------------------------------------------------------------
' Fecha de Modificación: 11/04/2012
' PPCR: 2011-00056
' Desarrolador: Alejandro Daniel Desouches
' Descripción: email de Facundo PEREIRA/HBAR/HSBC - 10/04/2012 01:02 p.m - Para arreglar - ICO BB  - UAT
' En la pestaña Resultado de la Cotización, en la parte de abajo, 
' habría que sacar las tres filas que corresponden a "Prima Comisionable", "Gastos Administrativos" y
' "Gastos Comercialización", y solamente dejar la de "Precio en 10 CUOTAS..."
'--------------------------------------------------------------------------------
' Fecha de Modificación: 19/04/2011
' Ticket 599651
' Desarrolador: Adriana Armati
' Descripción: Se agrega funcionalidad de Prima mínima
' ------------------------------------------------------------------------------
' Fecha de Modificación: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: CotiIntComercio_ResulCotizacion.asp
' 
' Fecha de Creación: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripción: Pagina q muestra el Resultado de la Coti.
'			   
' 
' ------------------------------------------------------------------------------

%>

<div id="resultadoCotizacion" ></div>

<div id="resulCotiTotales">
    <div class="areaDatTit">
	    <table class="areaDatTitTex" width="99.9%" cellSpacing="0" cellPadding="0">
		    <tr>
			    <td style="font-family:Arial;color:#003366;font-size:11px;font-weight:bold;">
				    Totales
			    </td>
			    <td width="50%">
				    <a title="Composición de precio" style="cursor:hand" onclick="fncComposicionPrecio()"
				        onmouseover="JavaScript:this.style.color='#ff0000'" onmouseout="JavaScript:this.style.color='#000000'">
					    <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_buscar.gif" style="float:right;border:0;" />
				    </a>
			    </td>
	        </tr>
        </table>
    </div>
    <div class="areaDatCpoConInp">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%">PRIMA COMISIONABLE</td>
			    <td style="width:20%; text-align:right">
				    <div id="divCotiPRIMA"></div>
			    </td>
		    </tr>
	    </table>
    </div>
    <div class="areaDatCpoConInp">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%">PRECIO</td>
			    <td style="width:20%; text-align:right">
				    <div id="divCotiPRECIO"></div>
			    </td>
		    </tr>
	    </table>
    </div>
</div>
<!-- PARA ICB1 -->
<div id="resulCotiTotalesICOBB" style="display:none">
    <div class="areaDatTit">
	    <table class="areaDatTitTex" width="99.9%" cellSpacing="0" cellPadding="0">
		    <tr>
			    <td style="font-family:Arial;color:#003366;font-size:11px;font-weight:bold;">
				    Totales
			    </td>
			    <td/>
	        </tr>
        </table>
    </div>
    <div class="areaDatCpoConInp" style="display:none">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%">Prima Comisionable</td>
			    <td style="width:20%; text-align:right">
				    <div id="divPrimaICOBB"></div>
			    </td>
		    </tr>
	    </table>
    </div>
    <div class="areaDatCpoConInp" style="display:none">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%">Gastos Administrativos</td>
			    <td style="width:20%; text-align:right">
				    <div id="divGastosAdminICOBB"></div>
			    </td>
		    </tr>
	    </table>
    </div>    
    <div class="areaDatCpoConInp" style="display:none">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%">Gastos Comercialización</td>
			    <td style="width:20%; text-align:right">
				    <div id="divGastosComerICOBB"></div>
			    </td>
		    </tr>
	    </table>
    </div>    
    <div class="areaDatCpoConInp" style="display:none">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%">Impuestos</td>
			    <td style="width:20%; text-align:right">
				    <div id="divImpICOBB"></div>
			    </td>
		    </tr>
	    </table>
    </div>        
    <div class="areaDatCpoConInp">
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%;font-size:11px;font-weight:bold;">PRECIO EN 10 CUOTAS - No incluye Impuestos (*)</td>
			    <td style="width:20%; text-align:right;font-size:11px;font-weight:bold;">
				    <div id="divPrecioICOBB"></div>
			    </td>
		    </tr>
		   
	    </table>
    </div> 
     <div>
	    <table style="width:99%">
		    <tr>
			    <td style="width:80%;font-size:9px;">(*) No incluye IVA, Ingresos Brutos, Sellados, Impuestos Internos, Obra Social de Seguros ni Tasa de Superintendencia.</td>
		    </tr>
	    </table>
	 </div>        
</div>    
<!-- FIN PARA ICB1 -->

<br />

<!-- 19/4/2011 ADRIANA SE AGREGA LEYENDA PRIMA MINIMA -->
<div id="primaMinima" style="height:6; font-size:10; font-family:verdana"><br />
</div> 

<!-- 06/10/2010 LR Defect 21 Estetica, pto 5 -->
<div id="circuitoRevision" style="font-family:Arial; font-size:12; display:none "><br /><center><b>Deseo enviar la presente cotización a la Compañía para su revisión:</b><input type="checkbox" id="chk_circuito" value="" onclick="javascript:fncEsCircuito(this)"  /></center> </div> 

<br /><br />
<div id="divLeyendaImpuestos" class="areaDatTit" style="display:none">
    <div style="color:#003366;font-size:11px;font-weight:bold;background-color: #9CBACE;padding: 5 5 5 5">
        Sr Cliente, la presente cotización está sujeta a la aplicación de la Disposición Normativa Serie B N 70/2007 y su modificatoria 74/2007, si el riesgo se encuentra inscripto en la Provincia de Buenos Aires. Con fecha 14 de noviembre de 2007, la Dirección Provincial de Rentas de la Provincia de Buenos Aires dictó dicha norma.
    </div>
</div>

