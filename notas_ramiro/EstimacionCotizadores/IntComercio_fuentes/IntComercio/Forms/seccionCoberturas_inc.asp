<%
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: seccionCoberturas_inc.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados a la cobertura.
'			   
' 
' ------------------------------------------------------------------------------
'mvarRAMOPCOD="ICO1"

mvarRequest = "<Request>" & _
	          "<DEFINICION>2390_ListadoCoberturas.xml</DEFINICION>" & _	          
	          "<USUARCOD>" & mvarLOGON_USER &"</USUARCOD>" & _
	          "<CODIZONA>0000</CODIZONA>" & _
	          "<PROFECOD>000000</PROFECOD>" & _
	          "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>" & _
              "</Request>"
	        
        	Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", _
						 "lbaw_GetConsultaMQ.biz", _
						 mvarRequest, _
						 mvarResponse)
						 
'response.write "<textarea>" &  mvarRequest & "</textarea><BR>"
'response.write "<textarea>" &  mvarResponse & "</textarea><BR>"
			 
dim mobjXSLDoc, mvarParametrosXSL

	        Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
	        Set mobjXSLDoc = Server.CreateObject("MSXML2.DOMDocument")
        		
	        Call mobjXMLDoc.loadXML (mvarResponse)
	      
	     
	       
	      
	if Not (mobjXMLDoc.selectSingleNode("//CAMPOS")) Is Nothing Then
	
				   Set mobjXSLDoc = Server.CreateObject("MSXML2.DOMDocument"):mobjXSLDoc.async = false				 
				    
				    mvarParametrosXSL="<parametros><parametro><nombre>estadoOperacion</nombre><valor>C</valor></parametro></parametros>"  
				     
				    Response.CharSet = "iso-8859-1"
				     
				'    response.Write "<textarea>"& Server.MapPath(mvarCANALURLASP) & "\Forms\XSL\getseccionCoberturas.xsl" & "</textarea>"
				    call  parseaXSLParam(mobjXMLDoc.xml,Server.MapPath(mvarCANALURLASP) & "\Forms\XSL\getseccionCoberturas.xsl",mvarParametrosXSL) 				
				
	   Else		       
		       response.Write "Ha ocurrido un Error, intente nuevamente m&aacute;s tarde."
		       
	   End If
   
	        
            


 %>                       
