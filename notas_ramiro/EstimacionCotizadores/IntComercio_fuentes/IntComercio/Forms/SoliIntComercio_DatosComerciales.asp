<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: SoliIntComercio_DatosComerciales.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados a los datos comerciales.
'			   
' 
' ------------------------------------------------------------------------------

'Dim mvarSucursales, mobjResponse, mCount, mvarTarjetas

'Llena Combo de Sucursales

 'GED 08-09-2010 MEJORA QC 134 SE SOLICITO PONER FIJO HSBC BANK 


mvarRequest = "<Request>" & _ 
              "<DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>" & _ 
              "<BANCOCOD>0150</BANCOCOD>" & _ 
              "<AplicarXSL>1018_FormatCombo.xsl</AplicarXSL>" & _ 
              "</Request>"

Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ","lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
Set mobjResponse = Server.CreateObject("MSXML2.DOMDocument")
    mobjResponse.async = False

Call mobjResponse.loadXML(mvarResponse)

If Not (mobjResponse.selectSingleNode("//Response"))Is Nothing Then
	If (UCase(mobjResponse.selectSingleNode("//Response/Estado/@resultado").text) = "TRUE") Then
		mvarSucursales = mobjResponse.selectSingleNode("//options").xml
	Else
		mvarSucursales = ""
	End If
Else
	mvarSucursales = ""
End If			


'Llena Combo de Tarjetas

mvarRequest = "<Request><DEFINICION>1659_Mediosdepagoxcia.xml</DEFINICION>"
mvarRequest = mvarRequest & "<AplicarXSL>1659_Mediosdepagoxcia.xsl</AplicarXSL>"
mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
mvarRequest = mvarRequest & "<COBROINN> 4 </COBROINN>"
mvarRequest = mvarRequest & "</Request>"

Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion", _
"lbaw_GetConsultaMQGestion.biz", _
mvarRequest, _
mvarResponse)

Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
mobjXMLDoc.async = false
Call mobjXMLDoc.loadXML(mvarResponse)

If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
    If mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text = "true" Then
        mvarResponse = replace(mobjXMLDoc.xml,"<Response><Estado resultado=""true"" mensaje=""""/>", "")
        mvarResponse = replace(mvarResponse,"</Response>", "")
        mvarTarjetas = "<option value=''>Seleccionar...</option>" & mvarResponse
    Else
	    mvarTarjetas = "Error al recuperar datos de combos (Front)"
    End If
Else
	mvarTarjetas = "Error al recuperar datos de combos (Front)"
End If


 
  %>



    <div id= "seccion_EstructuraComercial">    
       <div class="areaDatTit">
            <div class="areaDatTitTex">Estructura Comercial</div>
       </div>    
        
       
         </div>
       
       <div class="areaDatCpoCon">    							 
	      <div class="areaDatCpoConInp" >
             <!-- ORGANIZADOR -->
            <label id="Organizador_etiq" for="Organizador">Organizador:</label>
             C�digo <input class="areaDatCpoConInpFieM" id="claseOrganizador" name="claseOrganizador" maxlength="2" style="text-transform: uppercase; width:40px" disabled="" type="text"/> -
            <input class="areaDatCpoConInpFieM" id="codigoOrganizador" name="codigoOrganizador"  maxlength="4" style="text-transform: uppercase; width:40px" disabled="" type="text"/>
            Nombre <input class="areaDatCpoConInpFieM" id="nombreOrganizador" name="nombreOrganizador" maxlength="50" type="text" disabled=""/>                        				    
            Por <input class="areaDatCpoConInpFieM" id="porcOrganizador" name="porcOrganizador" maxlength="2" type="text" style="text-transform: uppercase; width:40px" disabled="" />%   
          </div>              
          <div class="areaDatCpoConInp" >
             <!-- PRODUCTOR -->
            <label id="Productor_etiq" for="Productor">Productor:</label>
              C�digo <input class="areaDatCpoConInpFieM" id="claseProductor" name="claseProductor" maxlength="2" style="text-transform: uppercase; width:40px" disabled="" type="text"/> -
            <input class="areaDatCpoConInpFieM" id="codigoProductor" name="codigoProductor" maxlength="4" style="text-transform: uppercase; width:40px" disabled="" type="text"/>
            Nombre <input class="areaDatCpoConInpFieM" id="nombreProductor" name="nombreProductor" maxlength="50" type="text" disabled=""/>                        				    
            Por <input class="areaDatCpoConInpFieM" id="porcProductor" name="porcProductor" maxlength="2" type="text" style="text-transform: uppercase; width:40px" disabled=""/>%
          </div>          
          <!--div class="areaDatCpoConInp" style="display:none">
             
            <label id="Productor2_etiq" for="Productor2">Productor 2:</label>
            C�digo <input class="areaDatCpoConInpFieM" id="claseProductor2" name="claseProductor2" maxlength="2" style="text-transform: uppercase; width:40px" type="text"/> -
           <input class="areaDatCpoConInpFieM" id="codigoProductor2" name="codigoProductor2" maxlength="4" style="text-transform: uppercase; width:40px" type="text"/>
            Nombre <input class="areaDatCpoConInpFieM" id="nombreProductor2" name="nombreProductor2" maxlength="50" type="text"/>                        				    
            Por <input class="areaDatCpoConInpFieM" id="porcProductor2" name="porcProductor2" maxlength="2" type="text" style="text-transform: uppercase; width:40px" onkeydown="Javascript:evalNumber();" validacionPersonalizada="validarPorcentajesEstructura('2')"/>%
          </div-->                    
       </div>
   
  <!-- NUEVAS FORMAS DE PAGO -->
<!--include file="../Includes/moduloFPago/SeccionFPago_inc.asp"--> 
<!-- NUEVAS FORMAS DE PAGO -->
 
    <div id="seccion_DatosVendedor" style="display: <%if mvarSOLIC_VEND="S" then response.write "inline " else response.write "none " end if %>">
       <div class="areaDatTit">
            <div class="areaDatTitTex">Datos del Vendedor</div>
       </div>
       <div class="areaDatCpoConInp" >
             <!-- BANCO -->
            <label id="banco_etiq" for="banco">Banco:</label>
            <!-- HRF Def.126 2010-10-28 -->
            <input class="areaDatCpoConInpFieM" id="vendBanco" name="vendBanco" style="text-transform: uppercase; " type="text" onchange="fncCambiarColorCpoRen(this)" value="<%=Session("INSTITUCION") & " " & Session("BANCONOM")%>" disabled/>
            <label id="cbo_vendSucursal_etiq" for="sucursal">Sucursal:</label>
            <select class="areaDatCpoConInpFieM" id="cbo_vendSucursal" style="text-transform: uppercase; " name="cbo_vendSucursal" <%if mvarSOLIC_VEND<>"S" then response.write " disabled " end if %> onchange="fncCambiarColorCpoRen(this)" >          
              
            </select>
       </div>        
       <div class="areaDatCpoConInp" >
            <!-- HRF 2010-11-02 Cambios Vendedor -->
             <!-- LEGAJO VENDEDOR -->
            <label id="legajoVendedor_etiq" for="legajoVendedor">Leg. Vendedor:</label>
            <input class="areaDatCpoConInpFieM" id="legajoVendedor" name="legajoVendedor" 
                style="text-transform: uppercase; width:155px"  <%if mvarSOLIC_VEND<>"S" then response.write " disabled " end if %>
                onkeydown="if(window.event.keyCode==13){fncBuscarVendedor('<%=Session("INSTITUCION")%>', document.getElementById('cbo_vendSucursal').value, document.getElementById('legajoVendedor').value, '<%=mvarCANALHSBC %>')};" 
                type="text"
                onkeypress="capturaTecla()" validarTipeo="numero"  maxlength="6" onpaste="return false" oncontextmenu="return false"
                onchange="fncCambiarColorCpoRen(this)"/>
              <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_buscar.gif" 
                    width="14" height="14" border="0" align="absmiddle" style="cursor:hand" 
                    onClick="fncBuscarVendedor('<%=mvarINSTITUCION%>', document.getElementById('cbo_vendSucursal').value, document.getElementById('legajoVendedor').value, '<%=mvarCANALHSBC %>')">
            
            <label id="nomApe_etiq" for="nomApe">Nombre y Apellido:</label>            
            <strong id="divEmpleado"></strong>
       </div>   
        <!--Adriana 22-8-12   NUEVO PLATAFORMISTA-->
        <div class="areaDatCpoConInp" id="divPlataformista" style='display:none'>
       	  <label id="nombrePlat_etiq" for="nombrePlat">Ingrese Nombre:</label>
      	  <input class="areaDatCpoConInpFieM" id="nombrePlat" name="nombrePlat" type="text" validarTipeo="texto" onkeypress="capturaTecla()" maxlength="30" onpaste="return false;" />
      	  <label id="apellidoPlat_etiq" for="apellidoPlat">Ingrese Apellido:</label>
       	 <input class="areaDatCpoConInpFieM" id="apellidoPlat" name="apellidoPlat" type="text" validarTipeo="texto" onkeypress="capturaTecla()" maxlength="30" onpaste="return false;" />
        </div>
                   
    </div>  
     

<div id="seccion_FormaPago">
       <div class="areaDatTit">
            <div class="areaDatTitTex">Forma de Pago</div>
       </div>
       <div class="areaDatCpoConInp" >
             <!-- BANCO -->
            <label id="SolimedioPago_etiq" for="SolimedioPago">Medio de Pago:</label>
            <strong><label id="lbSolimedioPago" style="width:250"></label></strong>
       </div>
       
       <div id="seccion_Efete">EFECTIVO</div>
       <!-- TARJETA -->
       <div id="seccion_Tarjeta">
           <div class="areaDatCpoConInp" >
                 <!-- NOMBRE TARJETA -->
                <label id="cbo_nomTarjeta_etiq" for="nomTarjeta">Nombre de la Tarjeta:</label>
                <select class="areaDatCpoConInpFieM" id="cbo_nomTarjeta" name="cbo_NomTarjeta" disabled onchange="fncCambiarColorCpoRen(this)">          
                  
                </select>
           </div>
           <div class="areaDatCpoConInp" >
                 <!-- NUMERO TARJETA-->
                <label id="numTarjeta_etiq" for="numTarjeta">N�mero Tarjeta:</label>
                <input class="areaDatCpoConInpFieM" id="numTarjeta" name="numTarjeta"  validacionPersonalizada="fncValidaTRJ()" type="text" maxlength="16" disabled onchange="fncCambiarColorCpoRen(this)">
                <!-- 06/09/2010 LR Ver Cuentas Vinculadas -->
                <%if mvarACCEDE_BT="S" then%>
				    <a href="javascript:fncBuscarCuentas('TC');" class="linksubir"><img src="<%=mvarCANALURLASP%>/Canales/OV/Images/boton_buscar.gif" align="absmiddle" border="0"> Ver Tarjetas Vinculadas</a>
			    <%end if%>
           </div>               
           <div class="areaDatCpoConInp" >
                 <!-- VENCIMIENTO-->
                <label id="tarvtomes_etiq" for="tartomes">Vencimiento Mes:</label>
                <select class=form_drops name="tarvtomes" id="tarvtomes" size=1  disabled onchange="fncCambiarColorCpoRen(this)">
										    <%	
										    for mCount=1 to 12
											    %>
											    <option value="<%=mCount%>"><%=right("0" & mCount,2)%></option>
											    <%  
										    next
										    %>
			    </select>
               <label id="tarvtoann_etiq" for="tarvtoann">Vencimiento A�o:</label>
                <select class=form_drops name="tarvtoann"  id="tarvtoann" size=1 disabled onchange="fncCambiarColorCpoRen(this)">
										    <%	
										    for mCount=0 to 10									
											    %>
											    <option value="<%=mCount+year(now())%>"><%=mCount+year(now())%></option>
											    <%
										    next
										    %>
               </select>																       
           </div>
        </div>
        <!-- DEBITO EN CUENTA -->
        <div id="seccion_PagoCuenta">
           <div class="areaDatCpoConInp" >
                 <!-- BANCO -->
                <label id="lbBancoDC_etiq" for="lbBancoDC">Banco:</label>
                <label id="lbBancoDC" style="width:320px"><strong>HSBC BANK ARGENTINA S.A.</strong></label>
           </div>
                        
           <div class="areaDatCpoConInp" >
                <!-- SUCURSAL-->
                <label id="cboSucursalDC_etiq" for="cboSucursalDC">Sucursal:</label>
                <select class="areaDatCpoConInpFieM" id="cboSucursalDC" name="cboSucursalDC" style="text-transform:uppercase;" onchange="fncCambiarColorCpoRen(this)">          
                    <option value="-1">SELECCIONAR...</option>
                    <%=mvarSucursales%>
                </select>													       
           </div>
          
          <div class="areaDatCpoConInp" >
                 <!-- NUMERO DE CUENTA -->
                <label id="numCuentaDC_etiq" for="numCuentaDC">N�mero Cuenta:</label>
                <input class="areaDatCpoConInpFieM" id="numCuentaDC" name="numCuentaDC" onchange="fncCambiarColorCpoRen(this)" validacionPersonalizada="fncValidaCUE()" type="text" maxlength="22" disabled/>
                <!-- 06/09/2010 LR Ver Cuentas Vinculadas -->
                <%if mvarACCEDE_BT="S" then%>
			        <a href="javascript:fncBuscarCuentas('DB');" class="linksubir"><img src="<%=mvarCANALURLASP%>/Canales/OV/Images/boton_buscar.gif" align="absmiddle" border="0"> Ver Cuentas Vinculadas</a>
			    <%end if%>
           </div>   
        </div>
        <!-- CBU -->
        <div id="divNroCBU">
           <div class="areaDatCpoConInp" >
                 <!-- NUMERO DE CBU -->
                <label id="numCBU_etiq" for="numCBU">N�mero CBU:</label>
                <input class="areaDatCpoConInpFieM" id="numCBU" name="numCBU" onchange="fncCambiarColorCpoRen(this)" validacionPersonalizada="fncValidaCBU()" type="text"  maxlength="22"  disabled/>
           </div>               
        </div>
    </div>

 
 




