<%
'------------------------------------------------------------------------------
' Fecha de Modificación: 08/02/2012
' PPCR: 2011-00056
' Desarrollador: Martín Cabrera
' Descripción: Se agrega producto ICB1
' ------------------------------------------------------------------------------
' Fecha de Modificación: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: CotiIntComercio_DatosRiesgo.asp
' 
' Fecha de Creación: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripción: Captura de datos relacionados al riesgo.
'			   
' 
' ------------------------------------------------------------------------------
 
  dim mobjXMLItems 
     
   Set mobjXMLItems = Server.CreateObject("MSXML2.DOMDocument")
    mobjXMLItems.async = False			

    Call mobjXMLItems.loadXML("<ITEMS/>")
 
    
   
     

%>

<div id="divDatosRiesgo_Otro">
    <div class="areaDatTit" style="display: <%if mvarRAMOPCODSQL="ICB1" then response.write "none " else response.write "inline " end if %>">
            <div class="areaDatTitTex">Detalle de Bienes Asegurados</div>
        </div>
        <div class="areaDatCpoCon" style="display: <%if mvarRAMOPCODSQL="ICB1" then response.write "none " else response.write "inline " end if %>">>
        <div class="areaDatCpoConInp">
        <label for="OTID" id="OTID_etiq" style="width:50">Item:</label>
     <span style="width:50"> <input type="text"  class="form_camposin_largo" name="OTID" id="OTID" vaEnXml="ID" style="text-transform: uppercase; width:40px" value="1" maxlength="20" disabled /></span>
           
          <span id="spCertificado" style="display:none"><label id="certificado_etiq" style="width:50">Certificado:</label><span id="spCertipolAnt" ></span>-<span id="spCertiannAnt" ></span>-<span id="spCertisecAnt" ></span></span>
            <label style=" width:50">Actividad:</label><label style="width:250" id="verActividad"></label></div>
        </div>

  <!--input type="hidden" name="OTITEMDESC" id="OTITEMDESC"  vaEnXml="ITEMDESC"   /-->
							 
						    
<div  id="seccion_UbicacionRiesgo">

        <div class="areaDatTit">
            <div class="areaDatTitTex">Ubicaci&oacute;n de Riesgo</div>
        </div>
     <div class="areaDatCpoCon">
        <div class="areaDatCpoConInp">
           <!-- PROVINCIA -->
           <label for="CBO_PROVINCIA" id="CBO_PROVINCIA_etiq">Provincia:</label>
            <select class="areaDatCpoConInpFieM" id="CBO_PROVINCIA" name="CBO_PROVINCIA" vaEnXml="PROVICOD" onchange="fncProvinciaSeleccionadaConCP(this,'localidadRiesgo','codPostalRiesgo'); fncMuestraCoberturas(); fncActivaRecotizar()" ></select>
            <label style="margin-left:130; width:50">Zona:</label><label style="width:150" id="verZona"></label>
           </div>
          <div   class="areaDatCpoConInp">       
            <!-- LOCALIDAD -->
            <label for="localidadRiesgo" id="localidadRiesgo_etiq">Localidad:</label>
            <!-- HRF Def.190 2010-10-26 -->
             <input id="localidadRiesgo" class="areaDatCpoConInpFieL" style="width:300px; margin-right: 0px;" vaEnXml="DOMICPOB" name="localidadRiesgo" type="text" onkeypress="capturaTecla()" validarTipeo="calle" 
             onkeydown="if(window.event.keyCode==13){ fncCargaParamBuscaLoc('CBO_PROVINCIA','localidadRiesgo','codPostalRiesgo'); }"
             maxlength="60" onpaste="return false;" disabled="" />
            
            <a id="botonBuscarLocalidad" style="display:none" class="botonMenos" title="Buscar" onclick="fncCargaParamBuscaLoc('CBO_PROVINCIA','localidadRiesgo','codPostalRiesgo');" > 
            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" border="0" align="middle" width="14" height="14">
           </a>
           
           <label for="codPostalRiesgo" id="codPostalRiesgo_etiq" style="width:100px; padding-left:15px; padding-right:0px; margin-right: 0px;">Cod. Postal:</label> 
         <input type="text" style="width:45px; padding-left:5px; padding-right:0px; margin-right: 0px;" class="areaDatCpoConInpFieS" vaEnXml="CPOSDOM" id="codPostalRiesgo" disabled=""/>
                  
        </div>
       </div>

<!------ -->
<div id="seccion_ActividadICOBB" style="display:none">
	  <div class="areaDatCpoConInp">	
		<label id="cboActividadICOBB_etiq" for="cboActividadICOBB" >Actividad:</label>
		<select class="areaDatCpoConInpFieL"  id="cboActividadICOBB" onchange="fncActividadICOBBSeleccionada();" ></select>
   </div>      
	  <div class="areaDatCpoConInp">	
		<label id="cboPlanICOBB_etiq" for="cboPlanICOBB" >Plan:</label>
		<select class="areaDatCpoConInpFieM"  id="cboPlanICOBB" onchange="fncMuestraCoberturasICOBB();"></select>
   </div>         
</div>    
<!------- -->       
    <div class="areaDatCpoCon">
        <input id="calleRiesgoItem"    name="calleRiesgoItem"    vaEnXml="DOMICDOM" type="hidden" />
        <input id="nroCalleRiesgoItem" name="nroCalleRiesgoItem" vaEnXml="DOMICDNU" type="hidden" />
        <input id="pisoRiesgoItem"     name="pisoRiesgoItem"     vaEnXml="DOMICPIS" type="hidden" />
        <input id="dptoRiesgoItem"     name="dptoRiesgoItem"     vaEnXml="DOMICPTA" type="hidden" />
    </div>
</div>  


<!--
MMC -- 14/10/2010 El bloque coberturas no debe figurar hasta que se selecciones la localidad...  
<div class="areaDatTit">
    <div class="areaDatTitTex">Coberturas</div>
</div>-->

            <div id="divSeccionCoberturas" style="height:100"> </div>


<div id="seccion_otrosDatos" style="display:inline">
     <div class="areaDatTit">
            <div class="areaDatTitTex">Otros Datos</div>
        </div>
         <div class="areaDatCpoCon">
         
            <div class="areaDatCpoConInp">
        <!-- TIPO ALARMA-->
                    <label for="cboTipoAlarma" id="cboTipoAlarma_etiq" style="width:300">Tipo de Alarma:</label>
                    <select class="areaDatCpoConInpFieM" id="cboTipoAlarma" name="cboTipoAlarma" vaEnXml="ALARMA" onchange="fncActivaRecotizar()" ></select>
            </div>
         <div class="areaDatCpoConInp">
            <!-- TIPO GUARDIA-->
                    <label for="cboTipoGuardia" id="cboTipoGuardia_etiq" style="width:300">Tipo de Guardia:</label>
                     <select id="cboTipoGuardia" class="areaDatCpoConInpFieM" name="cboTipoGuardia" onchange="fncActivaRecotizar()"
                      vaEnXml="GUARDIA"></select>
       
                               
                  </div>
                 
           </div>  

</div>

<div id="seccion_Preguntas">

</div>

<div id="seccion_CantAlumnosCarteles" style="display:none">
 <div class="areaDatCpoCon">
         
      <div class="areaDatCpoConInp">
        <label for="cantAlumnosCarteles" id="cantAlumnosCarteles_etiq" style="width:300">Cant. Alumnos/Carteles:</label> 
         <input id="cantAlumnosCarteles" class="areaDatCpoConInpFieM" name="cantAlumnosCarteles"
              type="text" onkeypress="capturaTecla()" validarTipeo="numero" maxlength="5" vaEnXml="CANTALUM" />
      </div>
   </div>
</div>
      
      <div style="text-align:right"  >
      <p ></p>
      <p >
      <span id="botonDeducibles" style="margin-left:10;text-align:left;width:200; display:none"><a href="" onclick="ventanaDeducibles(); return false" class="linksubir" ><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_buscar.gif" border="0" align="absmiddle" width="14" height="14">Deducibles *</a></span><span id="divBotonEditarItem" style="text-align:center;width:200; display:none"><a href="" onclick="habilitarEditarItem(); return false" class="linksubir" ><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_form.gif" border="0" align="absmiddle" width="14" height="14">Editar Item</a></span><span   id="divBotonAgregarItem" style="text-align:right;width:200; display:none"><a href="" onclick="validaInsertaItemRiesgo(); return false" class="linksubir" ><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_agregar.gif" border="0" align="absmiddle" width="14" height="14">Agregar al listado</a></span>
      </p>
      <p></p>
      </div>
	
		    
<div id="seccion_Item" style="display: <%if mvarRAMOPCODSQL="ICB1" then response.write "none " else response.write "inline " end if %>">
    <div class="areaDatTit">
            <div class="areaDatTitTex">Items Cargados</div>
     </div>
        
      <div id="divItemsCargadosOtros" height="100px">
      <table width="100%" border="1" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td bordercolor="#dedfe7">
						<table width="100%" border="0" cellspacing="0">
							<tr>								
								<td align="center" height="15px">No hay Items en la lista</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
      </div>
      
  </div>
 
</div>




<!-- MMC 13-03-2012 CONTROLES HIDDEN PARA ICOBB -->

    <input type="hidden" id="PROVICOD" value="-1" />
    <input type="hidden" id="CPOSDOM" value="" />
	<input type="hidden" id="DOMICPOB" value="" />
	<input type="hidden" id="ALARMA" value=""/>
	<input type="hidden" id="GUARDIA" value=""/>
	<input type="hidden" id="xmlCoberturasICOBB" value=""/>
	<input type="hidden" id="divSeccionCoberturasICOBB" value=""/>
	<input type="hidden" id="CodigoZonaICOBB" value=""/>
	<input type="hidden" id="verZonaICOBB" value=""/>	
	


