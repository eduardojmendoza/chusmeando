<%
'--------------------------------------------------------------------------------
' Fecha de Modificaci�n: 11/04/2012
' PPCR: 2011-00056
' Desarrolador: Alejandro Daniel Desouches
' Descripci�n: email de Facundo PEREIRA/HBAR/HSBC - 10/04/2012 01:02 p.m - Para arreglar - ICO BB  - UAT
' En la pesta�a Datos Generales, en el item "Observaciones/Cla�sulas" tendr�a que aparecer dentro de �l, 
' la leyenda "Contacto / Datos para Inspecci�n:" como algo fijo
'------------------------------------------------------------------------------
' Fecha de Modificaci�n: 08/02/2012
' PPCR: 2011-00056
' Desarrollador: Marcelo Gasparini
' Descripci�n: Se agrega producto ICB1
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 27/12/2011
' PPCR: Proyecto 5005/6010661
' Desarrolador: Leonardo Ruiz
' Descripci�n: Soporte de HTTPS.-
'--------------------------------------------------------------------------------
' Fecha de Modificaci�n: 01/07/2011
' Ticket 632535
' Desarrolador: Gabriel D'Agnone
' Descripci�n: MOSTRAR PRIMAS Y TASAS EN IMPRESO A REVISION
'--------------------------------------------------------------------------------
' Fecha de Modificaci�n: 19/04/2011
' Ticket 599651
' Desarrolador: Adriana Armati
' Descripci�n: Se agrega funcionalidad de Prima m�nima
' ------------------------------------------------------------------------------
 'Fecha de Modificaci�n: 25/04/2011
 'Ticket : 609478
 'Desarrolador: Marcelo Gasparini
 'Descripci�n: Se agrega nuevamente hidden para envio de Recargo Financiero discriminado por Forma de pago
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
 '--------------------------------------------------------------------------------
 'Fecha de Modificaci�n: 30/11/2010
 'PPCR: 50055/6010661
 'Desarrolador: Marcelo Gasparini
 'Descripci�n: Se agrega hidden para envio de Recargo Financiero discriminado por Forma de pago
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: CotiIntComercio.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Esta p�gina contiene es el marco dentro del cual se incluyen las 
'			   solapas relacionadas al formulario 1. 
' 
' ------------------------------------------------------------------------------

dim mvarCMBCOBERTURASxPRODUCTO
%>

<link rel="stylesheet" type="text/css" href="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Styles.css">
<link type="text/css" rel="stylesheet" href="<%=mvarCANALURL & "Canales/" & mvarCanal %>/css/in_site.css" >
<link type="text/css" rel="stylesheet" href="<%=mvarCANALURL & "Canales/" & mvarCanal %>/css/common.css" >
<link type="text/css" rel="stylesheet" href="<%=mvarCANALURL & "Canales/" & mvarCanal %>/css/cotizadores.css" >
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/VariablesGlobales.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/funcionesModular.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/funcionesItems.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/IntComercio.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/Imprimir.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/AnimationTabs.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/ClaseDibujaSeccion.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/ComunValidacion.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/objAsegurados.js"></Script>
<Script type="text/javascript" src="<%=mvarCANALURL%>JS/Base64.js"></Script>

<!-- GED 12-04-2011 LIBRERIAS PARA MULTIBROWSER -->
<script type="text/javascript" src="<%=mvarCANALURL%>JS/crossBrowsers.js"></script>


<script runat="server" language="javascript">

function parseaXSLParam(pxml,pxsl,pParametros)
{       
    //--------Levanta los parametros del xsl desde pParametros-----------
    oXMLParam = new ActiveXObject('MSXML2.DOMDocument');
    oXMLParam.async = false;
    oXMLParam.loadXML(pParametros);
         
    var nombreParametro= oXMLParam.getElementsByTagName("nombre")
    var valorParametro= oXMLParam.getElementsByTagName("valor")
       
    //-------Carga xml entrada desde parametro   ----------  
    oXML = new ActiveXObject('MSXML2.DOMDocument');
    oXML.async = false;
    oXML.loadXML(pxml); //usar  cambiar para xml de mvarresponse
                
    //---------- Carga entrada parametros xsl
    oXSL=new ActiveXObject('MSXML2.FreeThreadedDOMDocument');
    oXSL.async = false;
    oXSL.load(pxsl);       
    
    objCache   = new ActiveXObject("Msxml2.XSLTemplate"); 
    objCache.stylesheet = oXSL;
      
    var oXSLT = objCache.createProcessor();
    oXSLT.input = oXML 
        
    for (x=0;x<nombreParametro.length;x++)
    { 
	    oXSLT.addParameter(nombreParametro[x].text, valorParametro[x].text, ""); 
    }  	
	oXSLT.transform();				
		
	response.write(oXSLT.output)     
}
</script>

<!-- Def.264 HRF 20110330 -->
<div id="dFormularioCompleto" name="dFormularioCompleto" style="display:none">

<FORM name="frmCotizacion" id="frmCotizacion" method="post">

<table cellpadding="0" cellspacing="0" border="0" style="padding-left: 15px; width:650">
    <!--#include  file="../Includes/CreateScreenTabs.asp"-->   
    <!--#include file="../Includes/Calendario/incCalendario.htm"--> 
    
    <!-- COMIENZO: row para las solapas -->
    <tr>
	<td>	
<%

    Dim aScreenTabs(12), aNameTabs(12), imgLeft, imgRight, ColorTabGral,imgFirstGral, aScreenTabsDes(12), imgCentro, imgPath
        
    aNameTabs(1)  = "Datos Generales" 
    aNameTabs(2)  = "Datos Riesgos"	   
    aNameTabs(3)  = "Resultado Cotizaci�n"
    aNameTabs(4)  = "Datos Cliente"
    aNameTabs(5)  = "Datos Generales" 
    aNameTabs(6)  = "Datos Riesgos Solicitud" 
    aNameTabs(7)  = "Datos Comerciales" 
	    
    aScreenTabs(1)  = "DatosGenerales"
    aScreenTabs(2)  = "DatosRiesgo"	    
    aScreenTabs(3)  = "ResultadoCotizacion"
    aScreenTabs(4)  = "DatosCliente"
    aScreenTabs(5)  =  "DatosSolicitud" 
    aScreenTabs(6)  = "SolicitudRiesgos"
    aScreenTabs(7)  = "DatosComerciales"

    aScreenTabsDes(1)  = "Datos Generales"
    aScreenTabsDes(2)  = "Datos del Riesgo"	   
    aScreenTabsDes(3)  = "Resultados de la Cotizaci�n"
    aScreenTabsDes(4)  = "Datos del cliente"
    aScreenTabsDes(5)  = "Datos Generales de la Solicitud"
    aScreenTabsDes(6)  = "Datos de los Riesgos solicitud"
    aScreenTabsDes(7)  = "Datos Comerciales"

    dim aScreenVerTabs(10)

    aScreenVerTabs(1)= "inline" 
    aScreenVerTabs(2)= "inline" 
    aScreenVerTabs(3)= "inline" 
    aScreenVerTabs(4)= "inline" 
    aScreenVerTabs(5)= "none"    
    aScreenVerTabs(6)= "none"    
    aScreenVerTabs(7)= "none"    
	
	ColorTabGral = "#FFFFFF"
	
	imgPath =   mvarCANALURL & "Canales/" & mvarCanal & "/Images/"
	
	call  CreateScreenTabs( aScreenTabs,aNameTabs, ColorTabGral, 1, 7, "", aScreenTabsDes, imgPath, aScreenVerTabs)

   	Dim mvarCANMAXALT
	Dim mvarCANMAXREN
   
    wvarRequest = "<Request>"
    wvarRequest = wvarRequest & "<DEFINICION>2430_ParametrosxProducto.xml</DEFINICION>"
    wvarRequest = wvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
    wvarRequest = wvarRequest & "</Request>"
    
    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", wvarRequest, wvarResponse)
    Set wobjXMLPar = Server.CreateObject("MSXML2.DOMDocument")
    wobjXMLPar.async = false
    Call wobjXMLPar.loadXML(wvarResponse)

    If Not wobjXMLPar.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
        If UCase((wobjXMLPar.selectSingleNode("//Response/Estado/@resultado").text)) = "TRUE" then
           // mvarRENULTTAS = wobjXMLPar.selectSingleNode("//Response/CAMPOS/RENULTTAS").text 
            //mvarPORAUMTAS = wobjXMLPar.selectSingleNode("//Response/CAMPOS/PORVARTAS").text
            mvarCONSINREN = wobjXMLPar.selectSingleNode("//Response/CAMPOS/CONSINREN").text
            mvarPORAUMSAI = wobjXMLPar.selectSingleNode("//Response/CAMPOS/PORAUMSAI").text
            mvarPORDISSAI = wobjXMLPar.selectSingleNode("//Response/CAMPOS/PORDISSAI").text
            mvarCANMAXALT = wobjXMLPar.selectSingleNode("//Response/CAMPOS/CANMAXALT").text
            mvarCANMAXREN = wobjXMLPar.selectSingleNode("//Response/CAMPOS/CANMAXREN").text 
        Else
            wvarMsgError = "Se ha producido un error." & wobjXMLPar.selectSingleNode("//Response/Estado/@mensaje").text	
        End If
    Else
        wvarMsgError = "Se ha producido un error."
    End If
    Set wobjXMLPar = Nothing

   '~~~~~~~~~~~~~~~~~~~~~~~~
   'Recuperar Cotizaci�n SQL   
   '~~~~~~~~~~~~~~~~~~~~~~~~
    Set mobjXMLCotiRecuperada = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLCotiRecuperada.async = False

	Set mobjXMLCotiItemsRiesgos = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLCotiItemsRiesgos.async = False	
	
	Set mobjXMLCotiItemsPreguntas = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLCotiItemsPreguntas.async=False	
			        	
	Set mobjXMLCotiItemsCob = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLCotiItemsCob.async = False	
	
	Set mobjXMLDocPers = Server.CreateObject("MSXML2.DOMDocument")
	mobjXMLDocPers.async = False	
	
    Set mobjXMLAnotaciones  = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLAnotaciones.async = False	
    
    Set mobjXMLObjAseg  = Server.CreateObject("MSXML2.DOMDocument")
    mobjXMLObjAseg.async = False
    
    Set  mobjXMLDocApod = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLDocApod.async = False
    
    'Dom de xml para Renovacion
    Set mobjXMLRenovaDomiCorresp = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLRenovaDomiCorresp.async = False
    
    Set mobjXMLRenovaApoderado = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLRenovaApoderado.async = False
    
    Set mobjXMLRenovaRiesgosItems = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLRenovaRiesgosItems.async = False
    
    Set mobjXMLRenovaDatosGralPoliza = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLRenovaDatosGralPoliza.async = False
    
    Set mobjXMLRenovaPoliCol = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLRenovaPoliCol.async = False
  
   'FASE 2
     Set mobjXMLRenovacionDatos = Server.CreateObject("MSXML2.DOMDocument")
        mobjXMLRenovacionDatos.async = False 
    
    'Set mobjXML2420 = Server.CreateObject("MSXML2.DOMDocument")	
    'mobjXML2420.async = False
    
    Set mobjXML2460 = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXML2460.async = False
    
    Set mobjXML1123 = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXML1123.async = False
    
    Set mobjXML2440 = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXML2440.async = False
    
    Set mobjXMLParseAux = Server.CreateObject("MSXML2.DOMDocument")	
    mobjXMLParseAux.async = False
        
    Set mobjParseoXSL = Server.CreateObject("MSXML2.FreeThreadedDOMDocument")	
    mobjParseoXSL.async = False
    
    Set mobjXMLDatos = Server.CreateObject("MSXML2.DOMDocument")
    mobjXMLDatos.async = false
    Call mobjXMLDatos.loadXML("<ITEMS/>")
   
    mvarCLIENSECAPO = "0"
    mvarOPERAPOLAIS = "0"
   
    mvarSWSINIES = "N"
    mvarSWEXIGIB = "N"
   
   
        'Recupera de la Base   
        wvarRequest = "<Request><DEFINICION>P_PAROV_CANALES.xml</DEFINICION><INSTACOD>"& mvarINSTITUCION &"</INSTACOD></Request>"
        Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", wvarRequest, wvarResponse)
        Set wobjXMLPar = Server.CreateObject("MSXML2.DOMDocument")
            wobjXMLPar.async = false
        
        If wobjXMLPar.loadXML(wvarResponse) Then
            If Not wobjXMLPar.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then	
                If UCase((wobjXMLPar.selectSingleNode("//Response/Estado/@resultado").text)) = "TRUE" then
                    'HRF 2010-11-02 Cambios Vendedor
                    mvarCANALHSBC = wobjXMLPar.selectSingleNode("//Response/PARAMS/PARAM/CANALHSBC").text
                    mvarACCEDE_BT = wobjXMLPar.selectSingleNode("//Response/PARAMS/PARAM/ACCEDE_BT").text 
                    mvarSOLIC_VEND = wobjXMLPar.selectSingleNode("//Response/PARAMS/PARAM/SOLIC_VEND").text
                    mvarVEND_RRHH = wobjXMLPar.selectSingleNode("//Response/PARAMS/PARAM/VEND_RRHH").text
                    mvarVEND_AIS = wobjXMLPar.selectSingleNode("//Response/PARAMS/PARAM/VEND_AIS").text
                    mvarBANCOCOD = mvarINSTITUCION
                Else
                    wvarMsgError = "Se ha producido un error." & wobjXMLPar.selectSingleNode("//Response/Estado/@mensaje").text	
                End If
            Else
                wvarMsgError = "Se ha producido un error."
            End If
        Else
            Response.Write "Error inesperado"
            Response.end
        End If
                 
        Set wobjXMLPar = Nothing

        '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
       '  RECUPERA COTIZACION SQL
       '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
  if mvarCertisec <> "0" then   
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>P_PROD_ICO_OPERACION_RECUPERA.xml</DEFINICION>" 
                mvarRequest = mvarRequest & "<RAMOPCOD>" & mvarRAMOPCODSQL & "</RAMOPCOD>"
                mvarRequest = mvarRequest & "<CERTIPOL>" &  mvarCERTIPOL & "</CERTIPOL>"
                mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                mvarRequest = mvarRequest & "<CERTISEC>" & mvarCERTISEC  & "</CERTISEC>"
                mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"		            
                mvarRequest = mvarRequest & "</Request>"      

                Call cmdp_ExecuteTrn("lbaw_OVSQLGen",  "lbaw_OVSQLGen.biz", mvarRequest,  mvarResponse)	
                Call mobjXMLCotiRecuperada.loadXML(mvarResponse)
        		    															
                If Not mobjXMLCotiRecuperada.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then  
                      If UCase((mobjXMLCotiRecuperada.selectSingleNode("//Response/Estado/@resultado").text)) = "TRUE" then  
                            mvarAGENTCLA = mobjXMLCotiRecuperada.selectSingleNode("//AGENTCLAPR").text
                            mvarAGENTCOD = mobjXMLCotiRecuperada.selectSingleNode("//AGENTCODPR").text
                            mvarAGENTNOMPR=  mobjXMLCotiRecuperada.selectSingleNode("//AGENTNOMPR").text
                            mvarI_AGENTE =  mvarAGENTCOD & "|" &  mvarAGENTCLA & "|" & mvarAGENTNOMPR
                            mvarAGENTCLA = mobjXMLCotiRecuperada.selectSingleNode("//AGENTCLAOR").text
                            mvarAGENTCOD = mobjXMLCotiRecuperada.selectSingleNode("//AGENTCODOR").text
                            mvarAGENTNOMPR = mobjXMLCotiRecuperada.selectSingleNode("//AGENTNOMORG").text 
                            mvarI_ORGANIZADOR = mvarAGENTCOD & "|" &  mvarAGENTCLA & "|" & mvarAGENTNOMPR
                            mvarCLIENSEC = mobjXMLCotiRecuperada.selectSingleNode("//CLIENSEC").text 
                            mvarCLIENSECAPO = mobjXMLCotiRecuperada.selectSingleNode("//CLIENSECAPOD").text
                            mvarTIENEAPOD = mobjXMLCotiRecuperada.selectSingleNode("//SWAPODER").text
                            mvarOCUPACODCLIE = mobjXMLCotiRecuperada.selectSingleNode("//OCUPACODCLIE").text
                            mvarOCUPACODAPOD = mobjXMLCotiRecuperada.selectSingleNode("//OCUPACODAPOD").text  
                            mvarACTIVIDADCOMERCIO = mobjXMLCotiRecuperada.selectSingleNode("//ACTIVIDAD").text                        
                            'Recupera el tipo de Operacion A o R
                            mvarTipoOperacion = mobjXMLCotiRecuperada.selectSingleNode("//TIPOOPER").text
                            mvarRenModifDat = mobjXMLCotiRecuperada.selectSingleNode("//REN_MODIF_DAT").text
                            mvarRenCotizNue = mobjXMLCotiRecuperada.selectSingleNode("//REN_COTIZ_NUE").text
                            mvarOPERAPOLSQL = mobjXMLCotiRecuperada.selectSingleNode("//OPERAPOL").text
                            mvarPOLIZANNANT = mobjXMLCotiRecuperada.selectSingleNode("//POLIZANNANT").text
                            mvarPOLIZSECANT = mobjXMLCotiRecuperada.selectSingleNode("//POLIZSECANT").text
                      Else
                                wvarMsgError = "Se ha producido un error." & wobjXMLPar.selectSingleNode("//Response/Estado/@mensaje").text
                                Response.Redirect	
                     End If
                Else
                        wvarMsgError = "Se ha producido un error."
                End If		
        
		        '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
		        mvarRequest = "<Request><CLIENSEC>" & mvarCLIENSEC & "</CLIENSEC></Request>"
		
		        Call cmdp_ExecuteTrn("lbaw_GetPersona", "lbaw_GetPersona.biz", mvarRequest, mvarResponse)
                Set mobjXMLDocPers = Server.CreateObject("MSXML2.DOMDocument")
                    mobjXMLDocPers.async = false
                Call mobjXMLDocPers.loadXML(mvarResponse)
			        
                If (mvarTIENEAPOD="S") Then
                            mvarRequest = "<Request><CLIENSEC>" & mvarCLIENSECAPO & "</CLIENSEC></Request>"                        		
			                Call cmdp_ExecuteTrn("lbaw_GetPersona", "lbaw_GetPersona.biz", mvarRequest, mvarResponse)
			                Set mobjXMLDocApod = Server.CreateObject("MSXML2.DOMDocument")
			                mobjXMLDocApod.async = false
                            Call mobjXMLDocApod.loadXML(mvarResponse)        			        
                End If
        
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>P_PROD_SIFSFIMA_ANOTACIONES_SELECT.xml</DEFINICION>" 
                mvarRequest = mvarRequest & "<RAMOPCOD>" & mvarRAMOPCODSQL & "</RAMOPCOD>"
                mvarRequest = mvarRequest & "<POLIZANN>0</POLIZANN>"
                mvarRequest = mvarRequest & "<POLIZSEC>0</POLIZSEC>"
                mvarRequest = mvarRequest & "<CERTIPOL>" &  mvarCERTIPOL & "</CERTIPOL>"
                mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                mvarRequest = mvarRequest & "<CERTISEC>" & mvarCERTISEC  & "</CERTISEC>"
                mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
                mvarRequest = mvarRequest & "</Request>"     

                Call cmdp_ExecuteTrn("lbaw_OVSQLGen",  "lbaw_OVSQLGen.biz", mvarRequest,  mvarResponse)	

                Call mobjXMLAnotaciones.loadXML(mvarResponse)
        
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>P_PROD_SIFSFIMA_OBJETOS_ASEGURADOS_SELECT.xml</DEFINICION>" 
                mvarRequest = mvarRequest & "<RAMOPCOD>" & mvarRAMOPCODSQL & "</RAMOPCOD>"
                mvarRequest = mvarRequest & "<POLIZANN>0</POLIZANN>"
                mvarRequest = mvarRequest & "<POLIZSEC>0</POLIZSEC>"
                mvarRequest = mvarRequest & "<CERTIPOL>" &  mvarCERTIPOL & "</CERTIPOL>"
                mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                mvarRequest = mvarRequest & "<CERTISEC>" & mvarCERTISEC  & "</CERTISEC>"
                mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
                mvarRequest = mvarRequest & "</Request>"
                                    
                Call cmdp_ExecuteTrn("lbaw_OVSQLGen",  "lbaw_OVSQLGen.biz", mvarRequest,  mvarResponse)	
                Call mobjXMLObjAseg.loadXML(mvarResponse)
		        
    	        '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			        
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>P_PROD_SIFSFIMA_RIESGO_COMERCIO_SELECT.xml</DEFINICION>" 
                mvarRequest = mvarRequest & "<RAMOPCOD>" & mvarRAMOPCODSQL & "</RAMOPCOD>"
                mvarRequest = mvarRequest & "<POLIZANN>0</POLIZANN>"
                mvarRequest = mvarRequest & "<POLIZSEC>0</POLIZSEC>"
                mvarRequest = mvarRequest & "<CERTIPOL>" &  mvarCERTIPOL & "</CERTIPOL>"
                mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                mvarRequest = mvarRequest & "<CERTISEC>" & mvarCERTISEC  & "</CERTISEC>"
                mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"		            
                mvarRequest = mvarRequest & "</Request>"      

                Call cmdp_ExecuteTrn("lbaw_OVSQLGen",  "lbaw_OVSQLGen.biz", mvarRequest,  mvarResponse)	
		        Call mobjXMLCotiItemsRiesgos.loadXML(mvarResponse)
            	 			
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>P_PROD_SIFSFIMA_ICO_PREGUNTAS_SELECT.xml</DEFINICION>" 
                mvarRequest = mvarRequest & "<RAMOPCOD>" & mvarRAMOPCODSQL & "</RAMOPCOD>"
                mvarRequest = mvarRequest & "<POLIZANN>0</POLIZANN>"
                mvarRequest = mvarRequest & "<POLIZSEC>0</POLIZSEC>"
                mvarRequest = mvarRequest & "<CERTIPOL>" &  mvarCERTIPOL & "</CERTIPOL>"
                mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                mvarRequest = mvarRequest & "<CERTISEC>" & mvarCERTISEC  & "</CERTISEC>"
                mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"		            
                mvarRequest = mvarRequest & "</Request>"      

                Call cmdp_ExecuteTrn("lbaw_OVSQLGen",  "lbaw_OVSQLGen.biz", mvarRequest,  mvarResponse)	
                Call mobjXMLCotiItemsPreguntas.loadXML(mvarResponse)
		
		        If Not mobjXMLCotiItemsRiesgos.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then  
		            mvarRequest= "<Request>" 
                    mvarRequest = mvarRequest & "<DEFINICION>P_PROD_SIFSFIMA_GRAL_PRIMAS_COB_SELECT.xml</DEFINICION>" 
                    mvarRequest = mvarRequest & "<RAMOPCOD>" & mvarRAMOPCODSQL & "</RAMOPCOD>"
                    mvarRequest = mvarRequest & "<CERTIPOL>" &  mvarCERTIPOL & "</CERTIPOL>"
                    mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                    mvarRequest = mvarRequest & "<CERTISEC>" & mvarCERTISEC  & "</CERTISEC>"
                    mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"		            
                    mvarRequest = mvarRequest & "</Request>"       

                    Call cmdp_ExecuteTrn("lbaw_OVSQLGen",  "lbaw_OVSQLGen.biz", mvarRequest,  mvarResponse)	
                    Call mobjXMLCotiItemsCob.loadXML(mvarResponse)
        		    
		          ' If Not mobjXMLCotiItemsCob.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then  
                        
                  '  End If
                End If
        
       
        
         '~~~~~~~~~~~~~~~~~~~~~~~~~~
         ' Inicio: Levanta del AIS Acticvidades
         '~~~~~~~~~~~~~~~~~~~~~~~~~~
          
     If mvarOCUPACODCLIE<>"" Then
      	    mvarRequestActDes = "<Request>"  & _
      	                        "<DEFINICION>getActividadOcupacion.xml</DEFINICION><AplicarXSL>getActividadOcupacion.xsl</AplicarXSL>" & _
      	                        "<USUARCOD>" & Session("USER") & "</USUARCOD>"& _
      	                        "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD><REFPROFECOD>" & mvarOCUPACODCLIE & "</REFPROFECOD>"& _
      	                        "</Request>"
      	                        
            Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion","lbaw_GetConsultaMQGestion.biz", mvarRequestActDes, mvarResponseActDes)
            Set mobjXMLDocActDes = Server.CreateObject("MSXML2.DOMDocument")
	            mobjXMLDocActDes.async = False				
        	Call mobjXMLDocActDes.loadXML(mvarResponseActDes)
        			
		    If Not mobjXMLDocActDes.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
			    If (mobjXMLDocActDes.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
				    If Not mobjXMLDocActDes.selectSingleNode("//option[@value='" & mvarOCUPACODCLIE & "']") Is Nothing Then
					    mvarOCUPACODCLIEDES = mobjXMLDocActDes.selectSingleNode("//option[@value='" & mvarOCUPACODCLIE & "']").text
                    Else
					    mvarOCUPACODCLIEDES = ""
                    End If
                Else
                    mvarOCUPACODCLIEDES = "No se encontr� la desc de la actividad/ocupaci�n"				 
                End If
            Else
				mvarOCUPACODCLIEDES = "Error al ejecutar el COM+"				
            End If
            
            Set mobjXMLDocActDes = Nothing
        End If
	    
	    If mvarOCUPACODAPOD<>"" Then
      	    mvarRequestActDes = "<Request>"  & _
      	                        "<DEFINICION>getActividadOcupacion.xml</DEFINICION><AplicarXSL>getActividadOcupacion.xsl</AplicarXSL>" & _
      	                        "<USUARCOD>" & Session("USER") & "</USUARCOD>"& _
      	                        "<RAMOPCOD>" & mvarRAMOPCOD & "</RAMOPCOD><REFPROFECOD>" & mvarOCUPACODAPOD & "</REFPROFECOD>"& _
      	                        "</Request>"
		    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion","lbaw_GetConsultaMQGestion.biz", mvarRequestActDes, mvarResponseActDes)
		    Set mobjXMLDocActDes = Server.CreateObject("MSXML2.DOMDocument")
			    mobjXMLDocActDes.async = False				
        	Call mobjXMLDocActDes.loadXML(mvarResponseActDes)
        	
        	If Not mobjXMLDocActDes.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
			    If (mobjXMLDocActDes.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
				    If Not mobjXMLDocActDes.selectSingleNode("//option[@value='" & mvarOCUPACODAPOD & "']") Is Nothing Then
					    mvarOCUPACODAPODDES = mobjXMLDocActDes.selectSingleNode("//option[@value='" & mvarOCUPACODAPOD & "']").text
                    Else
                        mvarOCUPACODAPODDES = ""
                    End If
                Else
			        mvarOCUPACODAPODDES = "No se encontr� la desc de la actividad/ocupaci�n"				 
                End If
            Else
				mvarOCUPACODAPODDES = "Error al ejecutar el COM+"				
            End If
            
            Set mobjXMLDocActDes = Nothing
        End If
        
        '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	    
	    If mvarACTIVIDADCOMERCIO <>"" Then
		    mvarRequestActDes = "<Request>"  & _
		      	                "<DEFINICION>getActividadOcupacion.xml</DEFINICION><AplicarXSL>getActividadOcupacion.xsl</AplicarXSL>" & _
		      	                "<USUARCOD>" & Session("USER") & "</USUARCOD>"& _
		      	                "<RAMOPCOD>" & mvarRAMOPCOD & "</RAMOPCOD><REFPROFECOD>" & mvarOCUPACODAPOD & "</REFPROFECOD>"& _
		      	                "</Request>"
		    
		    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion","lbaw_GetConsultaMQGestion.biz", mvarRequestActDes, mvarResponseActDes)
		    Set mobjXMLDocActDes = Server.CreateObject("MSXML2.DOMDocument")
			    mobjXMLDocActDes.async = False				
            Call mobjXMLDocActDes.loadXML(mvarResponseActDes)
        	
        	If Not mobjXMLDocActDes.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
			        If (mobjXMLDocActDes.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
				        If Not mobjXMLDocActDes.selectSingleNode("//option[@value='" & mvarOCUPACODAPOD & "']") Is Nothing Then
					        mvarOCUPACODAPODDES = mobjXMLDocActDes.selectSingleNode("//option[@value='" & mvarOCUPACODAPOD & "']").text
                        Else
					        mvarOCUPACODAPODDES = ""
                        End If
	                Else
			            mvarOCUPACODAPODDES = "No se encontr� la desc de la actividad/ocupaci�n"				 
                    End If
            Else
				mvarOCUPACODAPODDES = "Error al ejecutar el COM+"				
		    End If
		    
		    Set mobjXMLDocActDes = Nothing
    End If
      
 '~~~~~~~~~~~~~~~~~~~~~~~~~
        ' Renovacion Datos AIS
 '~~~~~~~~~~~~~~~~~~~~~~~~~
       
   If mvarTipoOperacion = "R" Then
              '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              ' Renovacion dentro de SQL
              '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                '2400 Recuperacion de Renovacion Poliza Colectiva
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>2400_RenovacionPolizaColectiva.xml</DEFINICION>"
                mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
                mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
                mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
                mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
                mvarRequest = mvarRequest & "<CERTIPOL>0</CERTIPOL>"
                mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
                mvarRequest = mvarRequest & "<CERTISEC>0</CERTISEC>"
                mvarRequest = mvarRequest & "</Request>"
                
                Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
                Call mobjXMLRenovaPoliCol.loadXML(mvarResponse)
                
                If Not mobjXMLRenovaPoliCol.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
                            mvarOPERAPOLAIS =  mobjXMLRenovaPoliCol.selectSingleNode("//OPERAPOL").text   
                            mvarSWSINIES = mobjXMLRenovaPoliCol.selectSingleNode("//SWSINIES").text
                            mvarSWEXIGIB = mobjXMLRenovaPoliCol.selectSingleNode("//SWEXIGIB").text
                            mvarCLIENSECAPOREN = mobjXMLRenovaPoliCol.selectSingleNode("//CLIENSECAP").text
                              
                            '********* GED 07-09-2010 SE AGREGA PR2 VALIDO****************************
                            ' mvarAGENTCLA2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCLAPR2").text
                            ' mvarAGENTCOD2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCODPR2").text
                            ' mvarAGENTNOMPR2= " " ' mobjXMLRenovaPoliCol.selectSingleNode("//AGENTNOMPR2").text 
                            ' mvarI_PRODUCTOR2 =   mvarAGENTCOD2 & "|" &  mvarAGENTCLA2 & "| " & mvarAGENTNOMPR2
                            '********* ADRIANA  12-04-2011 AGREGO OTROS CAMPOS DE PR2 ****************
           	              mvarAGENTCLA2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCLAPR2").text
            	              mvarAGENTCOD2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCODPR2").text
                              mvarCONVECOD2 = mobjXMLRenovaPoliCol.selectSingleNode("//CONVECODPR2").text
                              mvarCOMPCPPR2 = mobjXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR2").text
                              mvarCOMISTIP2 = mobjXMLRenovaPoliCol.selectSingleNode("//COMISTIPPR2").text
                              mvarAGENTNOMPR2 = " " ' mobjXMLRenovaPoliCol.selectSingleNode("//AGENTNOMPR2").text 
                              If mvarCOMISTIP2 = "A" Then
       	    		         mvarRequest= "<Request>" 
            		         mvarRequest = mvarRequest & "<DEFINICION>1555_ConvenioProdOrganiz.xml</DEFINICION>"
            		         mvarRequest = mvarRequest & "<USUARCOD>"& mvarLOGON_USER &"</USUARCOD>"
             		         mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
            		         mvarRequest = mvarRequest & "<AGENCOD>"& mvarAGENTCOD2 &"</AGENCOD>"
            		         mvarRequest = mvarRequest & "<AGENCLA>"& mvarAGENTCLA2 &"</AGENCLA>"
            		         mvarRequest = mvarRequest & "<CONVECOD></CONVECOD>"
            		         mvarRequest = mvarRequest & "</Request>"
            
            		       Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
            		       Call mobjXMLConveniosProductor.loadXML(mvarResponse)
                       'Response.Write "<TEXTAREA>" & mvarRequest & "</TEXTAREA>"
                       'Response.Write "<TEXTAREA>" & mvarResponse & "</TEXTAREA>"
               		       If Not mobjXMLConveniosProductor.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
            			  'Response.Write "<TEXTAREA>" & "//CAMPOS/CONVENIOS/CONVENIO[CODIGO='" & mvarCONVECOD2 & "']/COMISION" & "</TEXTAREA>"
            			  mvarCOMPCPPR2 =  mobjXMLConveniosProductor.selectSingleNode("//CAMPOS/CONVENIOS/CONVENIO[CODIGO='" & mvarCONVECOD2 & "']/COMISION").text
                               End If
                           End  If
                           If mvarCOMISTIP2 = "A" Then
                              mvarCOMISTIP2 = "N"
                           Else
            	              mvarCOMISTIP2 = "S"
                           End if
                           mvarI_PRODUCTOR2 = mvarAGENTCOD2 & "|" &  mvarAGENTCLA2 & "| " & mvarAGENTNOMPR2 & "| " & mvarCONVECOD2 & "| " & mvarCOMPCPPR2 & "| " & mvarCOMISTIP2
                           'Response.Write "<TEXTAREA>" & mvarI_PRODUCTOR2 & "</TEXTAREA>"
                           '*fin ADRIANA 12-04-11******
                           
                           '********************************************************************** 
                
                              ' ***GED 09-03-2011 - DEFECT 313 
                   ' *** VERIFICA QUE EL OR SEA EL MISMO QUE DE LA PAGINA DE PRODUCTO 
                   
                    mvarORGCLA = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCLAOR").text
                    mvarORGCOD = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCODOR").text      
                  
                    marrayI_ORG = SPLIT(mvarI_ORGANIZADOR,"|")        
                   
                   mvarORG =  mvarORGCOD & "|" & mvarORGCLA 
                           
                   mvarI_ORG = marrayI_ORG(0) & "|" & marrayI_ORG(1)
                  
                  IF (mvarORG <> mvarI_ORG) THEN
                  
                           Response.write  " El Organizador seleccionado "&mvarI_ORG&" no coincide con el de la p�liza a renovar "& mvarORG
                           Response.end 
                  
                  
                  END IF
                   
                  '**** FIN DEFECT 313
                    
                   
              
              
                End If
    End If
    
   
  
    end if
   
    
    ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~
   ' SI ES UN ALTA de RENOVACION
   '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    If mvarTipoOperacion = "R" Then
            '~~~~~~~~~~~~~~~~~~~~~~~~~
            ' Empieza la Renovacion
            '~~~~~~~~~~~~~~~~~~~~~~~~~
            
            '2400 Recuperacion de Renovacion Poliza Colectiva
            mvarRequest= "<Request>" 
            mvarRequest = mvarRequest & "<DEFINICION>2400_RenovacionPolizaColectiva.xml</DEFINICION>"
            mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
            mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
            mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
            mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
            mvarRequest = mvarRequest & "<CERTIPOL>0</CERTIPOL>"
            mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
            mvarRequest = mvarRequest & "<CERTISEC>0</CERTISEC>"
            mvarRequest = mvarRequest & "</Request>"
            
            Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
            Call mobjXMLRenovaPoliCol.loadXML(mvarResponse)
           
            If Not mobjXMLRenovaPoliCol.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
                mvarOPERAPOLAIS =  mobjXMLRenovaPoliCol.selectSingleNode("//OPERAPOL").text   
                mvarSWSINIES = mobjXMLRenovaPoliCol.selectSingleNode("//SWSINIES").text
                mvarSWEXIGIB = mobjXMLRenovaPoliCol.selectSingleNode("//SWEXIGIB").text
                mvarCLIENSECAPOREN = mobjXMLRenovaPoliCol.selectSingleNode("//CLIENSECAP").text

                '********* GED 07-09-2010 SE AGREGA PR2 VALIDO****************************
                'mvarAGENTCLA2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCLAPR2").text
                'mvarAGENTCOD2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCODPR2").text
                'mvarAGENTNOMPR2= " " ' mobjXMLRenovaPoliCol.selectSingleNode("//AGENTNOMPR2").text 
                'mvarI_PRODUCTOR2 =   mvarAGENTCOD2 & "|" &  mvarAGENTCLA2 & "| " & mvarAGENTNOMPR2
                '********* ADRIANA  12-04-2011 AGREGO OTROS CAMPOS DE PR2 ****************
           	              mvarAGENTCLA2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCLAPR2").text
            	              mvarAGENTCOD2 = mobjXMLRenovaPoliCol.selectSingleNode("//AGENTCODPR2").text
                              mvarCONVECOD2 = mobjXMLRenovaPoliCol.selectSingleNode("//CONVECODPR2").text
                              mvarCOMPCPPR2 = mobjXMLRenovaPoliCol.selectSingleNode("//COMPCPORPR2").text
                              mvarCOMISTIP2 = mobjXMLRenovaPoliCol.selectSingleNode("//COMISTIPPR2").text
                              mvarAGENTNOMPR2 = " " ' mobjXMLRenovaPoliCol.selectSingleNode("//AGENTNOMPR2").text 
                              If mvarCOMISTIP2 = "A" Then
       	    		         mvarRequest= "<Request>" 
            		         mvarRequest = mvarRequest & "<DEFINICION>1555_ConvenioProdOrganiz.xml</DEFINICION>"
            		         mvarRequest = mvarRequest & "<USUARCOD>"& mvarLOGON_USER &"</USUARCOD>"
             		         mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
            		         mvarRequest = mvarRequest & "<AGENCOD>"& mvarAGENTCOD2 &"</AGENCOD>"
            		         mvarRequest = mvarRequest & "<AGENCLA>"& mvarAGENTCLA2 &"</AGENCLA>"
            		         mvarRequest = mvarRequest & "<CONVECOD></CONVECOD>"
            		         mvarRequest = mvarRequest & "</Request>"
            
            		       Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
            		       Call mobjXMLConveniosProductor.loadXML(mvarResponse)
                       'Response.Write "<TEXTAREA>" & mvarRequest & "</TEXTAREA>"
                       'Response.Write "<TEXTAREA>" & mvarResponse & "</TEXTAREA>"
               		       If Not mobjXMLConveniosProductor.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
            			  'Response.Write "<TEXTAREA>" & "//CAMPOS/CONVENIOS/CONVENIO[CODIGO='" & mvarCONVECOD2 & "']/COMISION" & "</TEXTAREA>"
            			  mvarCOMPCPPR2 =  mobjXMLConveniosProductor.selectSingleNode("//CAMPOS/CONVENIOS/CONVENIO[CODIGO='" & mvarCONVECOD2 & "']/COMISION").text
                               End If
                           End  If
                           If mvarCOMISTIP2 = "A" Then
                              mvarCOMISTIP2 = "N"
                           Else
            	              mvarCOMISTIP2 = "S"
                           End if
                           mvarI_PRODUCTOR2 = mvarAGENTCOD2 & "|" &  mvarAGENTCLA2 & "| " & mvarAGENTNOMPR2 & "| " & mvarCONVECOD2 & "| " & mvarCOMPCPPR2 & "| " & mvarCOMISTIP2
                          ' Response.Write "<TEXTAREA>" & mvarI_PRODUCTOR2 & "</TEXTAREA>"
                           '*fin ADRIANA 12-04-11******
                                         
                
                '**********************************************************************     
            End If
            
            '1116 Recuperacion de Datos del cliente
            mvarRequest= "<Request>" 
            mvarRequest = mvarRequest & "<DEFINICION>1116_DomiciliodeCorrespondencia.xml</DEFINICION>"
            mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
            mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
            mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
            mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
            mvarRequest = mvarRequest & "<CERTIPOL>0</CERTIPOL>"
            mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
            mvarRequest = mvarRequest & "<CERTISEC>0</CERTISEC>"
            mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
            mvarRequest = mvarRequest & "</Request>"
            
            Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion", "lbaw_GetConsultaMQGestion.biz", mvarRequest, mvarResponse)	
            Call mobjXMLRenovaDomiCorresp.loadXML(mvarResponse)
            
            if mvarCertisec = "0" then   mvarCLIENSEC = ""
            
            '3000 Datos del apoderado
            If mvarCLIENSECAPOREN > 0 Then
                mvarRequest= "<Request>" 
                mvarRequest = mvarRequest & "<DEFINICION>3000_ApoderadoxCliensec.xml</DEFINICION>"
                mvarRequest = mvarRequest & "<CLIENSEC>"& mvarCLIENSECAPOREN &"</CLIENSEC>"
                mvarRequest = mvarRequest & "</Request>"
                Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
                Call mobjXMLRenovaApoderado.loadXML(mvarResponse)
            End If
                
            '1101 Datos Grales de la Poliza
            mvarRequest= "<Request>"
            mvarRequest = mvarRequest & "<DEFINICION>1101_OVDatosGralPoliza.xml</DEFINICION>"
            mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
            mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
            mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
            mvarRequest = mvarRequest & "<CERTIPOL>0</CERTIPOL>"
            mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
            mvarRequest = mvarRequest & "<CERTISEC>0</CERTISEC>"
            mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
            mvarRequest = mvarRequest & "</Request>"
            
            Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
            Call mobjXMLRenovaDatosGralPoliza.loadXML(mvarResponse)
           
           
        If Not mobjXMLRenovaDatosGralPoliza.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
            If (mobjXMLRenovaDatosGralPoliza.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
                Set	mobjNodo = mobjXMLRenovaDatosGralPoliza.createElement("COBROTIPCBO")
                If mobjXMLRenovaDatosGralPoliza.selectSingleNode("//COBROCOD").text = 4 Then
		            mobjNodo.text = "VI"
		        Else
		            mobjNodo.text = mobjXMLRenovaDatosGralPoliza.selectSingleNode("//COBROTIP").text
		        End If
                Call mobjXMLRenovaDatosGralPoliza.selectSingleNode("//CAMPOS").appendChild(mobjNodo)
            End If
        End If
          
            
        
            '1582 Retorna los riesgos de la p�liza. Trae la lista de certificados
            mvarRequest= "<Request>" 
            mvarRequest = mvarRequest & "<USUARIO>"& Session("USER") &"</USUARIO>"
            mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
            mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
            mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
            mvarRequest = mvarRequest & "<CERTIPOL>0</CERTIPOL>"
            mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
            mvarRequest = mvarRequest & "<CERTISEC>0</CERTISEC>"
            mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
            mvarRequest = mvarRequest & "</Request>"
            
            Call cmdp_ExecuteTrn("lbaw_OVRiesgosListadoImpre", "lbaw_OVRiesgosListadoImpre.biz", mvarRequest, mvarResponse)	
            Call mobjXMLRenovaRiesgosItems.loadXML(mvarResponse)
        
            Dim cont
                cont=1 
            'Con cada Item del 1582 ciclo en el 1206, 1123 y 2440 para conseguir los datos del Item.
            If Not mobjXMLRenovaRiesgosItems.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
	            If (mobjXMLRenovaRiesgosItems.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
		            For Each objNode in mobjXMLRenovaRiesgosItems.selectNodes("//REG[EST='VIGENTE']")
                        mvarCERPOL = objNode.selectSingleNode("CERPOL").text
					    mvarCERANN = objNode.selectSingleNode("CERANN").text
					    mvarCERSEC = objNode.selectSingleNode("CERSEC").text
					    '---------------------------------------------------------------------------------------
				        '2460 Todos los datos del Riesgo
				        mvarRequest= "<Request>"
                        mvarRequest = mvarRequest & "<DEFINICION>2460_RenovacionRiesgos.xml</DEFINICION>"
                        mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
	                    mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
	                    mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
	                    mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
	                    mvarRequest = mvarRequest & "<CERTIPOL>"& mvarCERPOL &"</CERTIPOL>"
	                    mvarRequest = mvarRequest & "<CERTIANN>"& mvarCERANN &"</CERTIANN>"
	                    mvarRequest = mvarRequest & "<CERTISEC>"& mvarCERSEC &"</CERTISEC>"
	                    mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
                        mvarRequest = mvarRequest & "</Request>"
					
		                Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
                        Call mobjXML2460.loadXML(mvarResponse)
                        
                        'Creo un xml con los items del Riesgo
                        If Not mobjXML2460.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
	                        If (mobjXML2460.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
                                'Ahora le anexo el resultado al xml de items de renovacion
                                Set oXMLObj = mobjXML2460.selectNodes("//Response/CAMPOS").item(0)
                                Set	mobjNodo = mobjXMLDatos.createAttribute("ID")
							        mobjNodo.value = cont
                                Call oXMLObj.setAttributeNode(mobjNodo)
                                Call mobjXMLDatos.selectSingleNode("//ITEMS").appendChild(oXMLObj)
                            Else
                                Response.Write "Error en la consulta de p�liza."
                                Response.End
                            End If
                        Else
                            Response.Write "Error en la consulta de p�liza."
                            Response.End
                        End If
                        
                        '---------------------------------------------------------------------------------------
                        'Le agrego los datos del certificado de los items q recupere
			            Set	mobjNodo = mobjXMLDatos.createElement("CERTIPOLANT")
			                mobjNodo.Text = mvarCERPOL
                        
			            'Asigno todo al nodo padre
				        Call mobjXMLDatos.selectSingleNode("//CAMPOS[@ID=" & cont & "]").appendChild(mobjNodo)
                        
			            Set	mobjNodo = mobjXMLDatos.createElement("CERTIANNANT")
			                mobjNodo.Text = mvarCERANN
			            'Asigno todo al nodo padre
				        Call mobjXMLDatos.selectSingleNode("//CAMPOS[@ID=" & cont & "]").appendChild(mobjNodo)
			            
			            Set	mobjNodo = mobjXMLDatos.createElement("CERTISECANT")
			                mobjNodo.Text = mvarCERSEC
			            'Asigno todo al nodo padre
				        Call mobjXMLDatos.selectSingleNode("//CAMPOS[@ID=" & cont & "]").appendChild(mobjNodo)
                        '---------------------------------------------------------------------------------------
                    
                        '---------------------------------------------------------------------------------------
                        '1123 Todas las Coberturas del Riesgo
				        mvarRequest= "<Request>"
                        mvarRequest = mvarRequest & "<DEFINICION>1123_OVDetalleCoberXProd.xml</DEFINICION>"
                        mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
	                    mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
	                    mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
	                    mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
	                    mvarRequest = mvarRequest & "<CERTIPOL>"& mvarCERPOL &"</CERTIPOL>"
	                    mvarRequest = mvarRequest & "<CERTIANN>"& mvarCERANN &"</CERTIANN>"
	                    mvarRequest = mvarRequest & "<CERTISEC>"& mvarCERSEC &"</CERTISEC>"
	                    mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
                        mvarRequest = mvarRequest & "</Request>"
                        
		                Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
                        Call mobjXML1123.loadXML(mvarResponse)
                    
                        'Le anexo al xml las Coberturas
                        If Not mobjXML1123.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
	                        If (mobjXML1123.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
                                'Creo un xml con las Coberturas del Riesgo
	                            Set oXMLObj = mobjXML1123.selectNodes("//Response/CAMPOS/COBERTURAS").item(0)
						        'Asigno todo al nodo padre
						        Call mobjXMLDatos.selectSingleNode("//CAMPOS[@ID=" & cont & "]").appendChild(oXMLObj)
						     else
						        Response.Write "Error en la consulta de p�liza."
                                Response.End
						        
	                        End If
	                        
	                    else
	                        
	                         Response.Write "Error en la consulta de p�liza."
                              Response.End
	                    End If
	                    
                        '---------------------------------------------------------------------------------------
                        '2440 Objetos Asegurados de cada Cobertura del Riesgo
				        mvarRequest= "<Request>"
                        mvarRequest = mvarRequest & "<DEFINICION>2440_ObjetosXCertificados.xml</DEFINICION>"
                        mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
	                    mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
	                    mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
	                    mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
	                    mvarRequest = mvarRequest & "<CERTIPOL>"& mvarCERPOL &"</CERTIPOL>"
	                    mvarRequest = mvarRequest & "<CERTIANN>"& mvarCERANN &"</CERTIANN>"
	                    mvarRequest = mvarRequest & "<CERTISEC>"& mvarCERSEC &"</CERTISEC>"
	                    mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
                        mvarRequest = mvarRequest & "</Request>"

		                Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
                        Call mobjXML2440.loadXML(mvarResponse)

                        'Le anexo al xml las Coberturas
                        If Not mobjXML2440.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
                            If (mobjXML2440.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
                                'Creo un xml con las Coberturas del Riesgo
                                Set oXMLObj = mobjXML2440.selectNodes("//Response/CAMPOS/OBJETOS").item(0)
                                'Parseo objetos para ordenar
                                mvarParseo = "<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'><xsl:output omit-xml-declaration='yes' indent='yes'/><xsl:strip-space elements='*'/><xsl:template match='node()|@*'><xsl:copy><xsl:apply-templates select='node()|@*'><xsl:sort select='COBERCOD' order='ascending' data-type='number'/></xsl:apply-templates></xsl:copy></xsl:template></xsl:stylesheet>"
                                Call mobjParseoXSL.loadXML(mvarParseo)
                                
                                'response.Write "<textarea>"& oXMLObj.transformNode(mobjParseoXSL) &"</textarea>"
                                'response.end
                                mvarAux = oXMLObj.transformNode(mobjParseoXSL)
                                Call mobjXMLParseAux.loadXML(mvarAux)
                                
                                Set oXMLObj = mobjXMLParseAux.selectNodes("//OBJETOS").item(0)
                                
				                'Asigno todo al nodo padre
				                Call mobjXMLDatos.selectSingleNode("//CAMPOS[@ID=" & cont & "]").appendChild(oXMLObj)
                            else
                                Response.Write "Error en la consulta de p�liza."
                                Response.End
                            End If
	                    Else
                            Response.Write "Error en la consulta de p�liza."
                            Response.End
                        End If
                    
                        '---------------------------------------------------------------------------------------
                        '2420 Cotizaci�n con tasa vigente
		               ' mvarRequest= "<Request>" 
                       ' mvarRequest = mvarRequest & "<DEFINICION>2420_RenovacionCotizacionConTasaVig.xml</DEFINICION>"
                      '  mvarRequest = mvarRequest & "<USUARCOD>"& Session("USER") &"</USUARCOD>"
                       ' mvarRequest = mvarRequest & "<RAMOPCOD>"& mvarRAMOPCOD &"</RAMOPCOD>"
                       ' mvarRequest = mvarRequest & "<POLIZANN>"& mvarPOLIZANNANT &"</POLIZANN>"
                       ' mvarRequest = mvarRequest & "<POLIZSEC>"& mvarPOLIZSECANT &"</POLIZSEC>"
                      '  mvarRequest = mvarRequest & "<CERTIPOL>"& mvarCERPOL &"</CERTIPOL>"
                      '  mvarRequest = mvarRequest & "<CERTIANN>"& mvarCERANN &"</CERTIANN>"
                      '  mvarRequest = mvarRequest & "<CERTISEC>"& mvarCERSEC &"</CERTISEC>"
                       ' mvarRequest = mvarRequest & "</Request>"
                            
                           
		            '    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)	
                    '    Call mobjXML2420.loadXML(mvarResponse)
                            
                        'Creo un xml con los items del Riesgo
                     '   If Not mobjXML2420.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
	                    '    If (mobjXML2420.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then
                         '       Set mobjNodo = mobjXML2420.selectNodes("//Response/CAMPOS/COTIZACIONES").item(0)
                                'Asigno todo al nodo padre
				            '    Call mobjXMLDatos.selectSingleNode("//CAMPOS[@ID=" & cont & "]").appendChild(mobjNodo)
					      '  Else
                         '       mvarRenCotizNue="S"   
                         '   End If
                      '  End If
                        
                        '---------------------------------------------------------------------------------------                    
                        
                        cont=cont+1
	                Next
        	    End If
            End If
       
   
   ' FASE 2
     'Armo XML con todos los datos
        Dim mvarXMLRenovacionDatos
        mvarXMLRenovacionDatos = "<RENOVACION>"
        mvarXMLRenovacionDatos = mvarXMLRenovacionDatos & mobjXMLRenovaPoliCol.selectSingleNode("//CAMPOS").xml
        mvarXMLRenovacionDatos = mvarXMLRenovacionDatos & mobjXMLRenovaDomiCorresp.selectSingleNode("//CAMPOS").xml
        mvarXMLRenovacionDatos = mvarXMLRenovacionDatos & mobjXMLRenovaDatosGralPoliza.selectSingleNode("//CAMPOS").xml
        mvarXMLRenovacionDatos = mvarXMLRenovacionDatos & "</RENOVACION>"
        Call mobjXMLRenovacionDatos.loadXML(mvarXMLRenovacionDatos)
        
        '*********************
        'Response.Write "<textarea>" & mobjXMLRenovacionDatos.selectSingleNode("//RENOVACION").xml & "</textarea>"
        'Response.End
        '*********************
   
   
    End If
   
   
   
    
%>

    </td>
	</tr>
	<!-- FIN: row para las solapas -->
	<!-- COMIENZO: un DIV por cada solapa -->
	<tr><td>
	    <div id="grupoMain" style="width:670">
	        <!--#include  file="../Includes/popUp_customizado.asp"-->    
            <div class="areaDat">
                <div class="areaDatCpo">
                    <div id="divDatosGenerales" style="display:none; width:670">
	                    <!-- #include file="CotiIntComercio_DatosGenerales.asp"-->
	                </div>
                    <div id="divDatosRiesgo" style="display:none; width:670">
	                    <!-- #include file="CotiIntComercio_DatosRiesgo.asp"-->
	                </div>
	                <div id="divResultadoCotizacion" style="display:none; width:670">
	                    <!-- #include file="CotiIntComercio_ResulCotizacion.asp"-->
	                </div>
	                <div id="divDatosCliente" style="display:none; width:670">
	                    <!-- #include file="CotiIntComercio_DatosCliente.asp"-->
	                </div>
        	        <div id="divDatosSolicitud" style="display:none; width:670">
	                    <!-- #include file="SoliIntComercio_DatosGenerales.asp"-->
	                </div>
	                <div id="divSolicitudRiesgos" style="display:none; width:670">
	                    <!-- #include file="SoliIntComercio_DatosRiesgos.asp"-->
	                </div>
        	        <div id="divDatosComerciales" style="display:none; width:670">
	                    <!-- #include file="SoliIntComercio_DatosComerciales.asp"-->
	                </div>
                </div>
            </div>
        </div>
    </td></tr>
	<!-- FIN: un DIV por cada solapa -->
</table>

<div id="divCodigoColores" style="padding-top:10">
<p></p>

<table width="580px" cellpadding="0" cellspacing="0" border="0" style="padding-left: 15px">
    <tr>
	<td>					
	    <table width="565px" border="0" cellpadding="0" cellspacing="0">
		    <tr>
			    <td width="10px" class="Form_campname_bcoL" style="border:2px solid #FFFFFF;" bgcolor="blue">
			    </td>
			    <td width="110px" class="Form_campname_bcoL">
				    Campo obligatorio
			    </td>
			    <td width="9px" class="Form_campname_bcoL" style="border:2px solid #FFFFFF;" bgcolor="black">
			    </td>
			    <!-- LR 07/10/2010 Defect 21 Estetica, pto 11 -->
			    <td width="97px" class="Form_campname_bcoL">
				    Campo opcional
			    </td>
			    <td width="9px" class="Form_campname_bcoL" style="border:2px solid #FFFFFF;" bgcolor="red">
			    </td>
			    <td width="110px" class="Form_campname_bcoL">
				    Campo con error
			    </td>
			    <td width="212px"></td>
		    </tr>
	    </table>
	</td>
    </tr>
</table>
</div>

    <!-- Se utiliza para molde de Solicitud Riesgo  -->
    <input type="hidden" name="SOLIRIESGO" id="SOLIRIESGO" />
    <input type="hidden" name="SOLIOBJETO" id="SOLIOBJETO" />
    <!--  SE UTILIZAN EN ANIMATION TABS -->
    <input type="hidden" Name="CANALURL" id="CANALURL" value="<%= mvarCANALURL %>">
    <input type="hidden" Name="CANAL"	 id="CANAL"	   value="<%= mvarCanal %>">
    <!-- Nuevos Hidden para almacenar los datos de las variables de Session-->
    
    <!-- GED 01-11-2010 SE AGREGA VARIABLE PARA NOMBRE USUARIO-->
       <input type="hidden" id="NOMBREUSER"  value="<%=session("NOMBREUSER") %>"/>
    
    <input type="hidden" id="USER_AS"  value="<%=Session("USER_AS") %>"/>
    <input type="hidden" id="PERFIL_AS" value="<%=Session("PERFIL_AS")%>"/>
    <input type="hidden" id="LOGON_USER"  value="<%=mvarLOGON_USER %>" />	

    <input type="hidden" id="RAMOPCOD" value="<%=mvarRAMOPCOD %>" />
    <input type="hidden" id="RAMOPCODSQL" value="<%=mvarRAMOPCODSQL %>" />
    <input type="hidden" id="CERTIPOL" name="CERTIPOL" value="<%=mvarCERTIPOL %>" />	
    <input type="hidden" id="CERTISEC" name="CERTISEC" value="<%=mvarCERTISEC %>" />
    <input type="hidden" id="I_AGENTE" value="<%=mvarI_AGENTE %>" />
    <input type="hidden" id="I_ORGANIZADOR"	value="<%=mvarI_ORGANIZADOR%>" />
   
   	<!--******GED 06-09-2010 SE AGREGA PR2 VALIDO*****-->
	<input Type="Hidden" id="I_PRODUCTOR2"	value="<%=mvarI_PRODUCTOR2%>" /> 
    
    <input type="hidden" id="TIPOOPER" value="<%= mvarTipoOperacion %>" />
    
    <!-- LR 04/01/2011 Agrego la marca de Disyuntor para enviarla a la integr 2214 -->
    <input type="hidden" id="hIDISYUNT" name="hIDISYUNT" value="N" />
    <input type="hidden" id="CERTIPOLANT" name="CERTIPOLANT" value="" />
    <input type="hidden" id="CERTIANNANT" name="CERTIANNANT" value="" />
    <input type="hidden" id="CERTISECANT" name="CERTISECANT" value="" />
    <input type="hidden" id="RENULTTAS" name="RENULTTAS" value="<%=mvarRENULTTAS %>" />    
    <input type="hidden" id="POLIZANNANT" name="POLIZANNANT" value="<%= mvarPOLIZANNANT %>">
    <input type="hidden" id="POLIZSECANT" name="POLIZSECANT" value="<%= mvarPOLIZSECANT %>">
    <input type="hidden" id="REN_MODIF_DAT" name="REN_MODIF_DAT" value="">
    <input type="hidden" id="REN_COTIZ_NUE" name="REN_COTIZ_NUE" value="">
    <input type="hidden" id="OPERAPOLAIS" name="OPERAPOLAIS" value="<%=mvarOPERAPOLAIS%>" />
    <input type="hidden" id="OPERAPOLSQL" name="OPERAPOLSQL" value="<%=mvarOPERAPOLSQL%>" />
   
    <input type="hidden" name="SWSINIES" id="SWSINIES" value="<%=mvarSWSINIES%>" />
    <input type="hidden" name="SWEXIGIB" id="SWEXIGIB" value="<%=mvarSWEXIGIB%>" /> 
   <!-- CR5 - MANTENER CUBIERTO --> 
    <input type="hidden" name="SWMANCUB" id="SWMANCUB" />
    
    <input type="hidden" id="IDCLIENTE" name="IDCLIENTE" value="<%=mvarCLIENSEC %>"/>
    <input type="hidden" id="CLIENSECAPO" name="CLIENSECAPO" value="<%=mvarCLIENSECAPO %>"/>
    <input type="hidden" id="SWAPODER" name="SWAPODER" value="<%=mvarTIENEAPOD %>"/>
    <input type="hidden" id="CODIGOZONA" name="CODIGOZONA" />
    <input type="hidden" id="ACTIVIDADCLIENTE" name="ACTIVIDADCLIENTE" value="<%=mvarOCUPACODCLIEDES %>"/>
    <input type="hidden" id="CODACTIVIDADCLIENTE" name="CODACTIVIDADCLIENTE" value="<%=mvarOCUPACODCLIE %>"/>
    <input type="hidden" id="ACTIVIDADAPOD" name="ACTIVIDADAPOD" value="<%=mvarOCUPACODaPODDES %>"/>
    <input type="hidden" id="CODACTIVIDADAPOD" name="CODACTIVIDADAPOD" value="<%=mvarOCUPACODAPOD %>"/>
    <input type="hidden" id="COMISION_PROD" name="COMISION_PROD" />
    <input type="hidden" id="COMISION_ORG" name="COMISION_ORG" />
    <input type="hidden" id="hComiTotal" />
    <input type="hidden" id="RECARGO" name="RECARGO" />
    <input type="hidden" id="SELOPERACION" name="SELOPERACION" value="<%=mvarTipoOperacion %>"/>
    <input type="hidden" id="CODMONEDA" name="CODMONEDA" value="<%=mvarCODMONEDAPRODUCTO %>" />    
    <!-- HRF 2010-11-02 PARAMETROS POR CANAL -->
    <input type="hidden" name="CANALHSBC" value="<%=mvarCANALHSBC %>"> 
    <input type="hidden" name="SOLIC_VEND" value="<%=mvarSOLIC_VEND %>">
    <!-- HRF 2010-11-01 Datos del Vendedor -->
    <input type="hidden" id="CODEMP" name="CODEMP" />
    <input type="hidden" id="NOMBREVEND"    name="NOMBREVEND" />
    <input type="hidden" id="APELLIDOVEND"  name="APELLIDOVEND" />
    <!-- Ejecutivo de cuenta -->
    <input type="hidden" id="EJEC_PPLSOFT" name="EJEC_PPLSOFT" />
    <input type="hidden" id="EJEC_APENOM"  name="EJEC_APENOM" />
    <input type="hidden" id="EJEC_EMAIL"   name="EJEC_EMAIL" />
    <!-- campos para la composicion del Precio -->
    <input type="hidden" id="PRIMAIMP" value="0" />
    <input type="hidden" id="DEREMIMP" value="0" />
    <input type="hidden" id="RECAIMPO" value="0" />
    <input type="hidden" id="RECFIMPO" value="0" />
    <input type="hidden" id="TASUIMPO" value="0" />
    <input type="hidden" id="IVAIMPOR" value="0" />
    <input type="hidden" id="IVAIBASE" value="0" />
    <input type="hidden" id="IVAIMPOA" value="0" />
    <input type="hidden" id="IVAABASE" value="0" />
    <input type="hidden" id="IVARETEN" value="0" />
    <input type="hidden" id="IVARBASE" value="0" />
    <input type="hidden" id="IMPUEIMP" value="0" />
    <input type="hidden" id="SELLAIMP" value="0" />
    <input type="hidden" id="INGBRIMP" value="0" />
    <input type="hidden" id="RECTOIMP" value="0" />
    <input type="hidden" id="COMISIMP" value="0" />
    <input type="hidden" id="RECADMIN" value="0" />
    <input type="hidden" id="RECFINAN" value="0" />
    <!-- ADRIANA 19-4-2011 se agrega swprimin  -->
    <input type="hidden" id="SWPRIMIN" value="N" />
    
    <input type="hidden" id="PRECIOICOBB" value="0" />
    <input type="hidden" id="PORCCOMERICOBB" value="0" />
    <input type="hidden" id="IMPPLANICOBB" value="0" />
    <input type="hidden" id="COMISORICOBB" value="0" />
    <input type="hidden" id="COMISPRICOBB" value="0" />
    <input type="hidden" id="SUMAASEGTOTICOBB" value="0" />    
    
      
   
        
    <input type="hidden" id="BANCOCOD" value="<%=mvarBANCOCOD %>"/>
    <!-- LAS FECHAS SE ALMACENAN  EN LOS HIDDEN COMO GENERICAS AAAAMMDD-->
    <input type="hidden" id="fechaHoy" name="fechahoy" value="<%=FormatearFechaGenerica(fechaHoy()) %>"  />  
    <input type="hidden" id="FECHACOTIZACION"  value="<%=right("00"& day(date),2) &" de "& MesAnn(month(date)) &" de "& year(date)%>" />
    <input type="hidden" id="fecVenPol"  value="" />    
    
    <input type="hidden" name="hDatos" id="hDatos" value="">
    <input type="hidden" name="hTipoImp" id="hTipoImp" value="">
    <input type="hidden" id="pAccion" name="pAccion" value="">
    
    <input type="hidden" id="ESRECFINAN" name="ESRECFINAN" value="">

    <input type="hidden" id="fechaAltaCoti" name="fechaAltaCoti" value=""  />
    <input type="hidden" id="fechaUltimaCoti" name="fechaUltimaCoti" value=""  />
    <!-- PROPUESTA AIS -->
    <input type="hidden" id="AIS_POLIZANN" value="0" />
    <input type="hidden" id="AIS_POLIZSEC" value="0" />
    <input type="hidden" id="AIS_CERTIPOL" value="0" />
    <input type="hidden" id="AIS_CERTIANN" value="0" />
    <input type="hidden" id="AIS_CERTISEC" value="0" />
    <input type="hidden" id="AIS_SUPLENUM" value="0" />
        
    <input type="hidden" id="hRutaVolver" name="hRutaVolver" value="<%=mvarRutaVolver%>"/>
   
   	<input type="hidden" id="CANMAXALT" name="CANMAXALT" value="<%=mvarCANMAXALT %>" />
	<input type="hidden" id="CANMAXREN" name="CANMAXREN" value="<%=mvarCANMAXREN %>" /> 
       
    <input type="hidden"  id="COTISOLI"  value="<%=mvarCOTISOLI %>"/>
    <input type="hidden" id="INSTITUCION"  value="<%=mvarINSTITUCION %>" />
    <input type="hidden" id="BANCONOM"  value="<%=session("BANCONOM") %>" />
    	
    <!--Inputs del modulo de items-->
<%
	Dim wvarLimpiarXml
	wvarLimpiarXml = replace(mobjXMLItems.xml, "'","�")
%>
	<input type="hidden" id="varXmlItems" name="varXmlItems" value="<%=wvarLimpiarXml%>" />
	<!-- GED TICKET 632535-->
	<input type ="hidden" id= "varXmlItemsImpresoRev" name="varXmlItemsImpresoRev" />
	<!--FASE 2-->
	<input type="hidden" name="varXmlRenovItems" id="varXmlRenovItems" value="<%=wvarLimpiarXml%>" />
	<input id="varXmlRenovObjAseg" type="hidden" value='<OBJETOS/>' />
	
	 <input type="hidden" name="varItemActual" id="varItemActual" value=""/>  
	
    <input type="hidden" id="varXmlObjAseg" name="varXmlObjAseg" value='<OBJETOS/>' />
    <input type="hidden" id="varXmlObjAsegAux" name="varXmlObjAsegAux" value='<OBJETOS/>' />
	<input type="hidden" id="varXmlItems_etiq" value='' />
	<input type="hidden" id="xmlErrores" value=""/>
	<input type="hidden" id="xmlFueraNorma" value="<ERRORES/>"/>
	<input type="hidden" id="xmlPreguntas" value=""/>
	
    <input type="hidden" id="xmlCambios" value="" />
	
	<input type="hidden"  id="actividadComercio" />
    <input id="mensajeAlerta" type="hidden" />
   
    <!-- GED 19-5-2011 PRIMA MINIMA-->
   <input type="hidden" id= "cantAnotacionesI" value="0"/> 
   
   <!-- MMC 16-02-2012 MARCA OPERATORIA PARA ICOBB -->
   <input type="hidden" id="MARCOPER" value="" />
   
       
	<xml id='XSLOtros' src='<%=mvarCANALURL%>Forms/XSL/Form_Otros.xsl'></xml>			
	<xml id='XSLAlert'	src='<%=mvarCANALURL%>Forms/XSL/Form_Alert.xsl'></xml>
	<xml id='XSLTablaErrores' src='<%=mvarCANALURL%>Forms/XSL/TablaErrores.xsl'></xml>
	<xml id='XSLFueraNorma' src='<%=mvarCANALURL%>Forms/XSL/TablaFueraNorma.xsl'></xml>
	<!-- LR 06/09/2010 Ver Cuentas Vinculadas  -->
    <xml id='XSLCuentas' src='<%=mvarCANALURL%>Forms/XSL/BT_SPPRODUCTOSCLIENTE.xsl'></xml>
	
	<xml id="xmlDefinicionCamposForm" src="<%=mvarCANALURL%>Forms/XML/DefinicionCamposForm.xml"></xml>
    <xml id="xmlValidacionIndividual" src="<%=mvarCANALURL%>Forms/XML/ValidacionIndividual.xml"></xml>
    <xml id="xmlMensajesLeyendas" src="<%=mvarCANALURL%>Forms/XML/MensajesLeyendas.xml"></xml>
    <xml id="xmlValidaDomiciliosItem"></xml>
    <xml id="xmlDatosCotizacion" ></xml>
    
    <!-- 14/11/2011 LR - Soporte de HTTPS -->
    <xml id='getComboIVA'               src='<%=mvarCANALURL%>Forms/XSL/getComboIVA.xsl'></xml>
    <xml id='getComboIIBB'              src='<%=mvarCANALURL%>Forms/XSL/getComboIIBB.xsl'></xml>
    <xml id='getComboTipoAlarma'        src='<%=mvarCANALURL%>Forms/XSL/getComboTipoAlarma.xsl'></xml>
    <xml id='getComboTipoGuardia'       src='<%=mvarCANALURL%>Forms/XSL/getComboTipoGuardia.xsl'></xml>
    <xml id='getComboConveniosProd'     src='<%=mvarCANALURL%>Forms/XSL/getComboConveniosProd.xsl'></xml>
    <xml id='getComboLocalidades'       src='<%=mvarCANALURL%>Forms/XSL/getComboLocalidades.xsl'></xml>
    <xml id='getComboEstabilizacion'    src='<%=mvarCANALURL%>Forms/XSL/getComboEstabilizacion.xsl'></xml>
    <xml id='getComboPlanPago'          src='<%=mvarCANALURL%>Forms/XSL/getComboPlanPago.xsl'></xml>
    <xml id='resultadoCotizacionXSL'    src='<%=mvarCANALURL%>Forms/XSL/resultadoCotizacion.xsl'></xml>
    <xml id='getseccionCoberturas'      src='<%=mvarCANALURL%>Forms/XSL/getseccionCoberturas.xsl'></xml>
    <xml id='getCombosPreguntas'        src='<%=mvarCANALURL%>Forms/XSL/getCombosPreguntas.xsl'></xml>
    <xml id='CONVIERTE_XMLCAMBIOS'      src='<%=mvarCANALURL%>Forms/XSL/CONVIERTE_XMLCAMBIOS.xsl'></xml>
    <xml id='CuadroCambiosRenovacion'   src='<%=mvarCANALURL%>Forms/XSL/CuadroCambiosRenovacion.xsl'></xml>
    <xml id='datosRiesgosSolicitud'     src='<%=mvarCANALURL%>Forms/XSL/datosRiesgosSolicitud.xsl'></xml>
    <xml id='getComboActividades'       src='<%=mvarCANALURL%>Forms/XSL/getComboActividades.xsl'></xml>
    <xml id='getComboObjetosAsegurados' src='<%=mvarCANALURL%>Forms/XSL/getComboObjetosAsegurados.xsl'></xml>
    <xml id='getComboPeriodoCotizar'    src='<%=mvarCANALURL%>Forms/XSL/getComboPeriodoCotizar.xsl'></xml>
    <xml id='getComboFormaPago'         src='<%=mvarCANALURL%>Forms/XSL/getComboFormaPago.xsl'></xml>
    <xml id='getComboSucursales'        src='<%=mvarCANALURL%>Forms/XSL/getComboSucursales.xsl'></xml>
    <xml id='getComboTarjetasCredito'   src='<%=mvarCANALURL%>Forms/XSL/getComboTarjetasCredito.xsl'></xml>

    <xml id='getComboActividadesICOBB'   src='<%=mvarCANALURL%>Forms/XSL/getComboActividadesICOBB.xsl'></xml>
    <xml id='getComboPlanesICOBB'   src='<%=mvarCANALURL%>Forms/XSL/getComboPlanesICOBB.xsl'></xml>    
    <xml id='getseccionCoberturasICOBB'   src='<%=mvarCANALURL%>Forms/XSL/getseccionCoberturasICOBB.xsl'></xml>        
    <xml id='resultadoCotizacionICOBBXSL'    src='<%=mvarCANALURL%>Forms/XSL/resultadoCotizacionICOBB.xsl'></xml>    
    
    <xml id='CodigosEquivalencias'      src='<%=mvarCANALURL%>Forms/XML/CodigosEquivalencias.xml'></xml>
    <xml id='CombosFront'               src='<%=mvarCANALURL%>Forms/XML/CombosFront.xml'></xml>
    
	<xml id="xmlCotiRecuperada" ><%= mobjXMLCotiRecuperada.xml%></xml>
	<xml id="xmlItemsRecuperados" ><%= mobjXMLCotiItemsRiesgos.xml%></xml>
	<xml id="xmlCobItemsRecuperados" ><%= mobjXMLCotiItemsCob.xml%></xml>
	<xml id="xmlPersRecuperada" ><%= mobjXMLDocPers.xml%></xml>	
    <xml id="xmlAnotacionesRecuperada"><%= mobjXMLAnotaciones.xml%></xml>   
    <xml id="xmlApodRecuperada" ><%= mobjXMLDocApod.xml%></xml>   
    <xml id="xmlPregItemsRecuperados" ><%= mobjXMLCotiItemsPreguntas.xml%></xml>
    <xml id="xmlObjAsegRecuperados"><%= mobjXMLObjAseg.xml%></xml>
    <xml id="xmlCoberturas"></xml>
    
    <xml id="xmlRenovaPoliCol"><%=mobjXMLRenovaPoliCol.xml%></xml>
    <xml id="xmlRenovaDomiCorresp"><%=mobjXMLRenovaDomiCorresp.xml%></xml>
    <xml id="xmlRenovaApoderado"><%=mobjXMLRenovaApoderado.xml%></xml>
    <xml id="xmlRenovaDatosGralPoliza"><%=mobjXMLRenovaDatosGralPoliza.xml%></xml>
    <xml id="xmlRenovaRiesgosItems"><%=mobjXMLDatos.xml%></xml>
   
      <xml id="xmlRenovacionDatos"><%=mobjXMLRenovacionDatos.xml%></xml> 
      
       <!-- ADRIANA 5-5-2011 Prorrateo de prima m�nima  -->	
      <xml  id="xmlRequest2340" ></xml>
  
    <!-- DESHABILITA TERMINOS Y CONDICIONES --> 
    <map name="Map">
        <area shape="circle" coords="576,43,12" href="#">
    </map>

</FORM>
</div>
<!-- 01/06/2011 LR - Se agrega funcionalidad para mostrar el impreso del Solicitud --> 
<!--#include file="../Includes/popUp_customizadoOutForm.asp"-->

<%

''' *******TEXT AREAS CON EL CONTENIDO DE LAS VARIABLES DE SESSION Y CON LOS REQUEST FORM *********
'dim sesion, item
'response.Write "<textarea><sesiones>"
'for each sesion in Session.Contents  
'response.Write "<varsesion>"
'response.Write "<nombre>" & sesion & "</nombre>"
'response.Write "<valor>" & replace (Session.Contents(sesion) ,"?","!--") & "</valor>"
'response.Write "</varsesion>"
'next
'response.Write "</sesiones></textarea>"
'response.Write "<textarea><request>"
'for each item in Request.Form
'response.Write "<input>"
'response.Write "<nombre>" & item & "</nombre>"
'response.Write "<valor>" & replace (Request.Form(item) ,"?","!--") & "</valor>"
'response.Write "</input>"
'next
'response.Write "</request></textarea>"

set mobjXMLCotiRecuperada=nothing
set mobjXMLCotiItemsRiesgos = 	nothing	        	
set mobjXMLCotiItemsCob = nothing
set mobjXMLDocPers = nothing
set mobjXMLDocApod = nothing
set mobjXMLAnotaciones = nothing
set mobjXMLCotiItemsPreguntas = nothing
%>

<!-- Def.264 HRF 20110330 -->
<div id="pantallaCarga" style="height:300; width:700; text-align:center; display :inline">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p> 
    <p style=" border:solid 1 #E0E0E0; background:#EEEEEE; padding:5 5 5 5; font-family:Arial; font-size:12; text-align:center; color:737373 "><b><label id="lblPantallaCarga"><%If mvarTipoOperacion="R" Then %>Espere mientras se recuperan los datos de la p�liza a renovar...<%Else %>Espere mientras se cargan los datos...<%End If %></label></b></p>
</div>

<div id="pantallaCierre" style="height:300; width:700; text-align:center; display :none">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p> 
<p style=" border:solid 1 #E0E0E0; background:#EEEEEE; padding:5 5 5 5; font-family:Arial; font-size:12; text-align:center "><b><label id="lblPantallaCierre">La Solicitud ha sido guardadada exitosamente. Podr� recuperarla y/o enviarla a la Compa��a cuando lo desee, ingresando a la consulta Solicitudes Generadas en OV.</label></b></p>
</div>

<div id="pantallaError" style="height:300; width:700; text-align:center; display :none">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p> 
<p style=" border:solid 1 #E0E0E0; background:#EEEEEE; padding:5 5 5 5; font-family:Arial; font-size:12; text-align:center "><b><span id="mensajeErrorDescrip">Se ha producido un error inesperado: </span><span id="mensajeError"></span></b></p>
</div>



<div name="divTablaErrores" id="divTablaErrores"></div>
<div name="divPoseeSiniestralidad" id="divPoseeSiniestralidad" ></div>
<div name="divTablaFueraNorma" id="divTablaFueraNorma"></div>
<div name="divPopUpFueraNorma" id="divPopUpFueraNorma"></div>

<div id="divBotonera" name="divBotonera" style="display:none">

	<Table width="680" align="center">
	    <tr>	  
	        <td style="text-align:center">
        	    <span  class="btnBotonera" id="divBtnNuevaCotizacion" style="display:none">
                    <a href="" onclick="fncNuevaCotizacion(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_volver.gif" align="absmiddle" border="0"> Nueva Cotizaci�n</a>
	                <input type="hidden" id="hNueCotConfirm" value="S" />
                </span>
                <span  class="btnBotonera" id="divBtnModificarCotizacion" style="display:none">
                    <a href="" onclick="javascript:modificarCotizacion(); return false"  class="linksubir">	        
                    <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_volver.gif" border="0" align="absmiddle" width="14" height="14"> Modificar Cotizaci�n</a> 
                </span>
	            <span id="divBtnAnterior" class="btnBotonera" style="display: none">
	                <a href="" onclick="javascript:verSolapa(-1); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_volver.gif" align="absmiddle" border="0"> Solapa Anterior</a>
                </span>
	            <span id="divBtnContinuar" class="btnBotonera" style="display: none">
	                <a href="" onclick="javascript:verSolapa(1);return false" class="linksubir">	           
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Solapa Siguiente</a>
                </span>
	            <span class="btnBotonera"  id="divBtnImprimirCotizacion" style="display:none"> 
	                <a href="" onclick="javascript:imprimirCotizacion(); return false" class="linksubir">	   
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Imprimir</a>
	            </span>
	            <span class="btnBotonera"  id="divBtnImprimirCertificado" style="display:none"> 
	    	        <a href="" onclick="javascript:imprimirCertificado(); return false" class="linksubir">	   
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Imprimir</a>
	            </span>
	            <span class="btnBotonera" id="divBtnEnvioEmail" style="display:none">
                    <a href=""  onclick="javascript:enviarMailCotizacion(); return false" class="linksubir">
	    		<!-- 08/10/2010 LR Defect 21 Estetica, pto 15 -->
	   		<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Enviar por E-Mail</a>
	            </span> 
	            <span id="divBtnGrabarIrSolicitud" class="btnBotonera" style="display: none">
    	            <a href=""   onclick="javascript:grabarCotizacionIrSolicitud(); return false" class="linksubir">	        
	            	<!-- 08/10/2010 LR Defect 21 Estetica, pto 38 -->
	            	<img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Guardar/Ir a Solicitud</a>
                </span>
	            <span id="divBtnGrabarCotizacion" class="btnBotonera" style="display: none">
	                <a href="" onclick="grabarOperacion('C',true); return false" class="linksubir">	        
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Guardar</a>
                </span>
                
	            <span class="btnBotonera" align="center" id="divBtnCompletarSolicitud" style="display:none">
	                <a href="" onclick="javascript:completarSolicitud(); return false"  class="linksubir">	   
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Solicitud</a>
                </span>
               
             
	            <span class="btnBotonera" align="center" id="divBtnEnvioRevision" style="display:none">
	                <a href="" onclick="javascript:openPopUpRevision(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Enviar a Revisi�n</a>
	            </span>
	            <span class="btnBotonera" id="divBtnConfirmarSolicitud" style="display:none">
	                <a href="" onclick="javascript:enviarSolicitud(); return false" class="linksubir">
	                <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_continuar.gif" border="0" align="absmiddle" width="14" height="14"> Enviar</a>
	            </span>
	          <!-- BOTON RECUPERAR DATOS INICIALES -->
	          <br><br>
	        <span class="btnBotonera" align="center" id="divBtnRecuperarDatos" style="display:none">
                <a href="" onclick="javascript:fncRecuperarDatos(); return false"  class="linksubir">	   
	            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/boton_recargar.gif" border="0" align="absmiddle" width="14" height="14"> Recuperar datos iniciales de la p�liza</a>
            </span>  
	            
	        </td>
        </tr>
    </Table>
</div>
<br/>

<!-- EVENTO ON LOAD CARGA LOS COMBOS E INICIALIZA LA PAGINA -->

<script for=window event=onload>
    <!--
	window.status = "Cargando 20%"
	//**************************************************
    datosCotizacion = new DibujaSeccion();                            
    datosCotizacion.setCanal("<%=mvarCanal%>") 
    //datosCotizacion.configuracion("<%=mvarCANALURLSecure%>Forms/XML/CombosFront.xml");
    datosCotizacion.configuracion(document.getElementById("CombosFront").xml);
    datosCotizacion.setCanalURL("<%=mvarCANALURLSecure%>")
        
    //19/11/2010 LR Agrego un metodo para set Y get del canal de los .gif por problemas con https
       datosCotizacion.setCanalUrlImg("<%=mvarCANALURL & "Canales/" & mvarCanal %>")
    
    datosCotizacion.setMonedaProducto("<%=mvarMONEDAPRODUCTO %>")		        
	datosCotizacion.setDesMonedaProducto("<%=mvarDESMONEDAPRODUCTO %>")  
	
	var mSoliRiesgo
    var mSoliObjeto
    
    //Cargo el formulario de SoliRiesgo en un control hidden
    mSoliRiesgo = document.getElementById("riesgosSolicitudMolde").outerHTML
    mSoliObjeto = document.getElementById("ObjetoNro##").outerHTML
    document.getElementById("SOLIRIESGO").value = mSoliRiesgo
    document.getElementById("SOLIOBJETO").value = mSoliObjeto
	
    //Carga los datos para instanciar el objeto seccion unico 
    //Muestra las etiquetas de las solapas y activa la primera
        
    document.getElementById("tablaEtiqSolapas").style.display="inline" 	   
    mostrarTab('divDatosGenerales','','#9CBACE','');  
    
    if (document.getElementById("CERTISEC").value=="0")
    {
        document.getElementById("spanTextoEtiqSolapa_4").disabled=true   
    }
    else
        datosCompletos=true
       
    //**************************************************
    
    // Inicializa las solapas que seran validadas 
    inicializarMatrizValidacion()
    // Se muestra la botonera despues de dibujar las solapas 
	document.getElementById("divBotonera").style.display="inline" 	   
	window.status = "Cargando 40%"   

	//tipo de alarma
	mvarRequest ="<Request><USUARIO>"+ document.getElementById("LOGON_USER").value +"</USUARIO></Request>"
    //datosCotizacion.cargaComboXSL("cboTipoAlarma",llamadaMensajeMQ(mvarRequest,'lbaw_GetTiposAlarma'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboTipoAlarma.xsl")
    datosCotizacion.cargaComboXSL("cboTipoAlarma",llamadaMensajeMQ(mvarRequest,'lbaw_GetTiposAlarma'),document.getElementById("getComboTipoAlarma").xml)
	  
	document.getElementById("cboTipoAlarma").value="3"
	document.getElementById("cboTipoGuardia").value="3"
	
	// Se carga el combo de Tipo de guardia
	mvarRequest ="<Request><USUARIO>"+ document.getElementById("LOGON_USER").value +"</USUARIO></Request>"
    //datosCotizacion.cargaComboXSL("cboTipoGuardia",llamadaMensajeMQ(mvarRequest,'lbaw_GetTiposGuardia'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboTipoGuardia.xsl")
    datosCotizacion.cargaComboXSL("cboTipoGuardia",llamadaMensajeMQ(mvarRequest,'lbaw_GetTiposGuardia'),document.getElementById("getComboTipoGuardia").xml)
	
	document.getElementById("cboTipoGuardia").value="3"
	datosCotizacion.cargaCombo("CBO_PROVINCIA","PROVICOD")     
	datosCotizacion.cargaCombo("provinciaDomicilio","PROVICOD")
	datosCotizacion.cargaCombo("apoProvinciaDomicilio","PROVICOD")
	
	mvarRequest = "<Request>"+
	              "<DEFINICION>SPSNCV_SIFTCIVA_SELECT.xml</DEFINICION>"+
	              "<NUMERO>0</NUMERO>"+
		          "</Request>"
    //datosCotizacion.cargaComboSQL("IVA",llamadaMensajeSQL(mvarRequest),datosCotizacion.getCanalURL() +"Forms/XSL/getComboIVA.xsl")
    datosCotizacion.cargaComboSQL("IVA",llamadaMensajeSQL(mvarRequest),document.getElementById("getComboIVA").xml)
window.status = "Cargando 60%"
    mvarRequest = "<Request>"+
		          "<DEFINICION>SPSNCV_SIFTCIBB_SELECT.xml</DEFINICION>"+
	              "<NUMERO>0</NUMERO>"+
		          "</Request>"
    //datosCotizacion.cargaComboSQL("IngresosBrutos",llamadaMensajeSQL(mvarRequest),datosCotizacion.getCanalURL() +"Forms/XSL/getComboIIBB.xsl")
    datosCotizacion.cargaComboSQL("IngresosBrutos",llamadaMensajeSQL(mvarRequest),document.getElementById("getComboIIBB").xml)

    //CARGA CONVENIOS DE PRODUCTORES
    mvarRequest = "<Request>"+		               
	              "<DEFINICION>1555_ConvenioProdOrganiz.xml</DEFINICION>"+
			      "<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
			      "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
			      "<AGENCOD>"+ document.getElementById("I_AGENTE").value.split("|")[0] +"</AGENCOD>"+
			      "<AGENCLA>"+ document.getElementById("I_AGENTE").value.split("|")[1]  +"</AGENCLA>"+
			      "<CONVECOP></CONVECOP>"+
			      "</Request>"
    var respuestaMQ=llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ') 
	//datosCotizacion.cargaComboXSL("convenioProductor",respuestaMQ,datosCotizacion.getCanalURL() +"Forms/XSL/getComboConveniosProd.xsl")
	datosCotizacion.cargaComboXSL("convenioProductor",respuestaMQ,document.getElementById("getComboConveniosProd").xml)
	
	 datosCotizacion.setCanalURL("<%=mvarCANALURLSecure%>")
	
	if(document.getElementById("convenioProductor").length==1)
    {
        fncConvenioProdSeleccionado() 
        fncCargaComboComision()
        fncCalculaComisiones() 
    }
window.status = "Cargando 80%"
    //CARGA PERIODO PARA COTIZAR   
	mvarRequest = "<Request>"+		               
	              "<DEFINICION>1551_PeriodoCotizar.xml</DEFINICION>"+
			      "<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
			      "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value +"</RAMOPCOD>"+
                  "</Request>"
    //datosCotizacion.cargaComboXSL("periodoCotizar",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboPeriodoCotizar.xsl")
    //datosCotizacion.cargaComboXSL("periodoCotizar",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboPeriodoCotizar").xml)

	//mmc icobb, si es icobb se elimina efectivo del combo de forma de pago
   //datosCotizacion.cargaComboXSL("formaPago",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboFormaPago.xsl")
    if (document.getElementById("RAMOPCOD").value == document.getElementById("RAMOPCODSQL").value) {
	    datosCotizacion.cargaComboXSL("periodoCotizar",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboPeriodoCotizar").xml)
    }
    else
    {
        var xmlSoloAnual = fncSoloAnual(llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'));
		datosCotizacion.cargaComboXSL("periodoCotizar",xmlSoloAnual,document.getElementById("getComboPeriodoCotizar").xml)
			
    }
	// mmc fini icobb Periodo
	    
    fncPeriodoSeleccionado()         
    window.status = "Cargando 90%"
    //COMBO PLAN DE PAGO
    mvarRequest = "<Request>"+		               
	              "<DEFINICION>FormasDePagoxPolizaColectiva.xml</DEFINICION>"+
			      "<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>"+
			      "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value  +"</RAMOPCOD>"+			                
			      "</Request>"
    //window.document.body.insertAdjacentHTML("beforeEnd", "ConvenioOrganizador<textarea>"+mvarRequest+"</textarea>")			      
	//jc icobb, si es icobb se elimina efectivo del combo de forma de pago
   //datosCotizacion.cargaComboXSL("formaPago",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboFormaPago.xsl")
    if (document.getElementById("RAMOPCOD").value == document.getElementById("RAMOPCODSQL").value) {
	    datosCotizacion.cargaComboXSL("formaPago",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboFormaPago").xml)
    }
    else
    {
        var xmlSinEF = fncQuitarEF(llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'));
		datosCotizacion.cargaComboXSL("formaPago",xmlSinEF,document.getElementById("getComboFormaPago").xml)
    }
	// jc fini icobb ef
	
    datosCotizacion.cargaCombo("paisNacimientoCliente","PAISSCOD")  
	datosCotizacion.cargaCombo("apoPais","PAISSCOD")   
	
	mvarRequest = "<Request>" + 
                  "<DEFINICION>1018_ListadoSucursalesxBanco.xml</DEFINICION>" + 
                  "<BANCOCOD>"+ document.getElementById("BANCOCOD").value + "</BANCOCOD>" +               
                  "</Request>" 
    //datosCotizacion.cargaComboXSL("cbo_vendSucursal",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboSucursales.xsl")
    datosCotizacion.cargaComboXSL("cbo_vendSucursal",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboSucursales").xml)
	window.status = "Cargando 100%"    
	//Tarjetas de Cr�dito
	mvarRequest = "<Request>" +
	              "<DEFINICION>FormasDePagoxPolizaColectiva.xml</DEFINICION>" +
	              "<USUARCOD>"+ document.getElementById("LOGON_USER").value +"</USUARCOD>" +
	              "<RAMOPCOD>"+ document.getElementById("RAMOPCOD").value   +"</RAMOPCOD>" +
	              "<COBROCOD>4</COBROCOD>"+
	              "</Request>"
    //datosCotizacion.cargaComboXSL("cbo_nomTarjeta",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),datosCotizacion.getCanalURL() +"Forms/XSL/getComboTarjetasCredito.xsl")
	datosCotizacion.cargaComboXSL("cbo_nomTarjeta",llamadaMensajeMQ(mvarRequest,'lbaw_GetConsultaMQ'),document.getElementById("getComboTarjetasCredito").xml)
	
	//GED 31-08-2010 DEFECT 47
	filtrarActividadCom('cboActividadComercio','filtroActividadComercio')
	mostrarDivCondiciones()
	
	pintarAzul()
    cargaCoberturas()
    
    //Pone Responsable Inscripto en en combo IVA
    document.getElementById("IVA").value =1
    fncCuitSeleccionado(document.getElementById("IVA"))
    
	fncColoreoRenglones();
	
	 //GED - Limita maximo de items
	
    document.getElementById("seccion_ActividadICOBB").style.display = "none";
    document.getElementById("cboActividadICOBB").disabled=true
    document.getElementById("cboPlanICOBB").disabled=true
    // jc icobb
	if (document.getElementById("RAMOPCODSQL").value == "ICB1")
		{
		
        mvarRequest = "<Request><DEFINICION>SP_LISTAR_ICOBB_ACTIVIDADES.xml</DEFINICION></Request>"
	    datosCotizacion.cargaComboXSL("cboActividadICOBB",llamadaMensajeMQ(mvarRequest,'lbaw_OVSQLGen'),document.getElementById("getComboActividadesICOBB").xml)		
			
		document.getElementById("seccion_Actividad").style.display = "none";
        document.getElementById("divNroInternoSoli").style.display = "none";		
        //document.getElementById("seccion_FormaPago").style.display = "none";		
		document.getElementById("seccion_ActividadICOBB").style.display = "inline";
        document.getElementById("cboActividadICOBB").disabled=false
        document.getElementById("cboPlanICOBB").disabled=false
		// para no exigir validar
		document.getElementById("cboActividadComercio").disabled = true;
		// las comisiones vienen del sql
		document.getElementById("comisionTotal_etiq").style.display = "none";
		document.getElementById("comisionTotal").style.display = "none";
		document.getElementById("comisionTotal").disabled = true;
		document.getElementById("planPago_etiq").style.width = "150px";
        document.getElementById("MARCOPER").value="21"  
        document.getElementById("CBO_PROVINCIA").value="-1"   
        
        // AD* Alejandro Daniel Desouches 11/04/2012 PPCR: 2011-00056
        // Solictado por Facundo PEREIRA/HBAR/HSBC - 10/04/2012 01:02 p.m.- 
        // En la pesta�a Datos Generales, en el item "Observaciones/Cla�sulas" tendr�a que aparecer dentro de �l, 
        // la leyenda "Contacto / Datos para Inspecci�n:" como algo fijo
        document.getElementById("observClausulas").innerText="Contacto / Datos para Inspecci�n"   
          
		}
	// fin jc icobb	
	
	
	 
	/*if (document.getElementById("TIPOOPER").value =="R")  
	{
	
	    xmlItemNroMaximo = 50 	   
	  if (!fncArmadoRenovacion())
            return;
	   
	 }
	 else
	    xmlItemNroMaximo = 15*/
	    

	   
	 if (document.getElementById("TIPOOPER").value == "R")
	{
	    xmlItemNroMaximo = document.getElementById("CANMAXREN").value;
	    if (Number(xmlItemNroMaximo) <= 0) xmlItemNroMaximo = 30;
        if (!fncArmadoRenovacion())
            return;
    } else {
        xmlItemNroMaximo = document.getElementById("CANMAXALT").value;
	    if (Number(xmlItemNroMaximo) <= 0) xmlItemNroMaximo = 10;
    }  
	    

    document.getElementById("fecVenPol").value=document.getElementById("vigenciaDesde").value		    	
	if (document.getElementById("CERTISEC").value != "0")
	{
	    fncRecuperaCotizacion();
	     mostrarDivCondiciones() 
	    
        if (document.getElementById("TIPOOPER").value =="R")  //Recuperada Grabacion de Renovacion
	    {
	        if (document.getElementById("OPERAPOLSQL").value != document.getElementById("OPERAPOLAIS").value) 	        
	                        {errorInesperado("NO SE PUEDE RENOVAR POLIZA MODIFICADA",true)}
	            
            
            document.getElementById("tipoDocCliente").disabled = true;
            document.getElementById("nroDocumentoCliente").disabled = true;
            document.getElementById("apellidoCliente").disabled = true;
            document.getElementById("nombreCliente").disabled = true;
              document.getElementById("fechaNacimientoClienteSoli").disabled = true;
              document.getElementById("fechaNacimientoClienteSoliBtn").disabled = true;
	    document.getElementById("sexoCliente").disabled = true;
	    document.getElementById("estadoCivilCliente").disabled = true;
	    document.getElementById("paisNacimientoCliente").disabled = true;
            
            //document.getElementById("periodoCotizar").disabled = true;
            //document.getElementById("estabilizacion").disabled = true;
            //document.getElementById("IVA").disabled = true;
            document.getElementById("tipoPersona").disabled = true;
           // document.getElementById("IngresosBrutos").disabled = true;	           
            document.getElementById("vigenciaDesde").disabled = true;
             document.getElementById("vigenciaDesdeSoli").disabled = true;
             document.getElementById("vigenciaDesdeSoliBtn").disabled = true;
	            
	       // fncAyudaCampoRenovacion() 
	   
	   
	    }
	}
    else
    {
        datosCotizacion.setCotizar(true)
	    if (document.getElementById("TIPOOPER").value == "R")
	    {
	       
	        
	     
           
              mostrarDivCondiciones()  
        	
	        //Bloqueo campos iniciales
	        //Coti - Solapa Datos Generales
	        
	        //Siempre: IVA - Tipo Persona - IIBB Fecha Inicio
	        //Inicial : Periodo  - Estabilizacion
	        
	       // document.getElementById("periodoCotizar").disabled=true   
	       // document.getElementById("estabilizacion").disabled=true 
	       // document.getElementById("IVA").disabled=true 
	        document.getElementById("tipoPersona").disabled=true 
	        //document.getElementById("IngresosBrutos").disabled=true	           
	        document.getElementById("vigenciaDesde").disabled=true
	        document.getElementById("vigenciaDesdeSoli").disabled=true
	        document.getElementById("vigenciaDesdeSoliBtn").disabled = true;
	        document.getElementById("fechaNacimientoClienteSoli").disabled = true;
	        document.getElementById("fechaNacimientoClienteSoliBtn").disabled = true;
	        document.getElementById("sexoCliente").disabled = true;
	        document.getElementById("estadoCivilCliente").disabled = true;
	        document.getElementById("paisNacimientoCliente").disabled = true;
	         
	        
	        //EL INICIO DE VIGENCIA SUPERA EL MES
	       var wFechaHoy = document.getElementById("fechaHoy").value
            var wFechaSuperaMesTemp=convierteFecha(document.getElementById("fechaHoy").value,"AAAAMMDD","DD/MM/AAAA")
            var wFechaSuperaMes = convierteFecha(dateAddExtention(parteFecha(wFechaSuperaMesTemp,"DD/MM/AAAA","DD"),parteFecha(wFechaSuperaMesTemp,"DD/MM/AAAA","MM"),parteFecha(wFechaSuperaMesTemp,"DD/MM/AAAA","AAAA"),"m", 1),"DD/MM/AAAA","AAAAMMDD")
            var wFechaIniVig = convierteFecha(document.getElementById("vigenciaDesde").value,"DD/MM/AAAA","AAAAMMDD")
            
                 
            
           
            if (wFechaSuperaMes < wFechaIniVig)
            {
                errorInesperado("POLIZA "+document.getElementById("POLIZANNANT").value+"-"+document.getElementById("POLIZSECANT").value+":  EL INICIO DE VIGENCIA SUPERA EL MES",true)
                document.getElementById("mensajeErrorDescrip").innerText = ""
                //LR 13/04/2011 Solo debe mostrar este msj por pantalla.-
                return;
            }
            if (document.getElementById("fechaHoy").value > wFechaIniVig)
            {
                document.getElementById("vigenciaDesde").value = convierteFecha(document.getElementById("fechaHoy").value,"AAAAMMDD","DD/MM/AAAA")
            }
            
        }
        fncFechaFin("C")
    }
    
    //LR 02/05/2011 Se habilitan Coberturas habilit para el Alta
	fncHabilitarCoberturasDisp('A');
    
    //LR 07/04/2011 Saco estas intrucciones fuera del If de CERTISEC != "0"
    //Def.264 HRF 20110330
    document.getElementById("dFormularioCompleto").style.display="";
    document.getElementById("lblPantallaCarga").innerText = "";
    document.getElementById("pantallaCarga").style.display="none";
    
  
    window.status = "Listo"
    
    -->
</script>