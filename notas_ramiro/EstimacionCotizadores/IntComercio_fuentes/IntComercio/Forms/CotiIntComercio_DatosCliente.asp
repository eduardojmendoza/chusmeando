<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: CotiMasivos_Cliente.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados al Cliente.
' 
' ------------------------------------------------------------------------------
 
     

%>


    
   	    <!-- #include file="../includes/moduloCliente/SeccionCliente_inc.asp"--> 
   <div class="areaDatCpoCon">
							 
	  <div class="areaDatCpoConInp" >
         <!-- TELEFONO -->
        <!--DEFECT 277 --> 
        <label id="telefonoCliente_etiq" for="telefonoCliente">Tel�fono:</label>
        <input class="areaDatCpoConInpFieS" id="codTelefonoCliente" name="codTelefonoCliente" onkeypress="capturaTecla()" validarTipeo="telefono"         
        type="text" onchange="sincronizaCampos(this,document.getElementById('CodTelPosDomicilio')); fncCambiarColorCpoRen(this)" style="width:45; margin-right:2 " maxlength="5"/>
       
        <input class="areaDatCpoConInpFieM" id="telefonoCliente" name="telefonoCliente" type="text" onkeypress="capturaTecla()" validarTipeo="telefono"    onchange="sincronizaCampos(this,document.getElementById('TelPosDomicilio')); fncCambiarColorCpoRen(this)" style="width:120; margin-left:2" maxlength="20"/>
         
         <!-- EMAIL -->
        <!-- 08/10/2010 LR Defect 21 Estetica, pto 15 -->
        <label id="emailCliente_etiq" for="emailCliente" style=" margin-left:2 " >E-Mail:</label>
        <input class="areaDatCpoConInpFieM" id="emailCliente" name="emailCliente"  onkeypress="capturaTecla()" validarTipeo="email"   onchange="sincronizaCampos(this,document.getElementById('mailDomicilio'));fncCambiaEMail();fncCambiarColorCpoRen(this)"  type="text" style=" margin-right:2 " maxlength="50" />
                				    
</div>
<span id="divLeyBuscarCliente" style="font-size:11px; height:20 <%if mvarACCEDE_BT="S" then response.write "; display:inline" else response.write "; display:none" end if %>" ><b>A trav�s de la opci�n Buscar Cliente podr� recuperar los datos disponibles del cliente</b></span>    

</div>






