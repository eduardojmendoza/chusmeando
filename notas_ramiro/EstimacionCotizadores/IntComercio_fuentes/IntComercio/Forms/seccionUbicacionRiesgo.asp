<%
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: seccionUbicacionRiesgo.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados a la ubicacion del Riesgo.
'			   
' 
' ------------------------------------------------------------------------------
%>

<div class="areaDatTit">
    <div class="areaDatTitTex">Ubicaci�n de Riesgo</div>
</div>
<div class="areaDatCpoCon">
    <div class="areaDatCpoConInp">
        <!-- PROVINCIA -->
        <label for="LBL_PROVINCIA">Provincia:</label>
        <select class="areaDatCpoConInpFieM" id="CBO_PROVINCIA" name="CBO_PROVINCIA" onchange="fncProvinciaSeleccionada(this,'CBO_LOCALIDAD');">
		</select>
        <!-- LOCALIDAD -->
        <label for="LBL_LOCALIDAD">Localidad:</label>
        <select class="areaDatCpoConInpFieM" id="CBO_LOCALIDAD" name="CBO_LOCALIDAD" onchange="fncLocalidadSeleccionada('CBO_LOCALIDAD');">
        </select>
    </div>
</div>
<div id="divAreaCalleNumero" class="areaDatCpoConInp" disabled="disabled">
    <div class="areaDatCpoCon">
        <!-- Calle -->
        <label for="CALLE">Calle:</label>
        <input class="areaDatCpoConInpFieM" id="CALLE" name="CALLE" type="text" disabled>            
        <!-- N�mero -->
        <label for="CALLE">N�mero:</label>
        <input class="areaDatCpoConInpFieM" id="NUMERO" name="NUMERO" type="text" disabled>                                
    </div>                    
</div>                      
<div id = "divAreaPisoDepto" class="areaDatCpoConInp" disabled="disabled">
    <div class="areaDatCpoCon">
        <!-- Piso -->
        <label for="PISO">Piso:</label>
        <input class="areaDatCpoConInpFieM" id="PISO" name="PISO" type="text" disabled>            
        <!-- Departamento -->
        <label for="DEPARTAMENTO">Departamento:</label>
        <input class="areaDatCpoConInpFieM" id="DEPTO" name="DEPTO" type="text" disabled>                                                    
    </div>            
</div>                
<div class="areaDatCpoConInp" style="display:'none'">
    <div class="areaDatCpoCon">
        <!-- CODIGO POSTAL -->
        <label for="CODPOS">C�digo Postal:</label>
        <input class="areaDatCpoConInpFieM" id="CODPOS" name="CODPOS" type="text">                            
    </div>                            
</div>

