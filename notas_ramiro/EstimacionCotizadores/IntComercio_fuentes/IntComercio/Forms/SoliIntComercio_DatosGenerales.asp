<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: SoliIntComercio_DatosGenerales.asp
' 
' Fecha de Creaci�n: 15/04/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados a los datos generales.
'			   
' 
' ------------------------------------------------------------------------------
  %>


<div class="areaDatTit">
            <div class="areaDatTitTex">Datos del Cliente</div>
</div>
<div class="areaDatCpoCon">
    <div id="divSolPersonaFisica">
        <div class="areaDatCpoConInp" >
            <label id="fechaNacimientoClienteSoli_etiq">Fecha de Nacimiento:</label>
            <input class="areaDatCpoConInpFieD" id="fechaNacimientoClienteSoli" name="fechaNacimientoClienteSoli" type="text" obligatorio="si"   onkeypress="capturaTecla()" validarTipeo="fecha"  onpaste="return false;" validar="" mensajeError="Fecha de Nacimiento  no puede ser mayor a la fecha del d�a"  maxlength="10" onBlur="return validateDateCal(this);" onchange="validaRangoEdad(this,'18..99');" >
		    <input class="areaDatCpoConInpFie" id="fechaNacimientoClienteSoliBtn" style="margin-right:63px" type="image" onClick="popFrame.fPopCalendar(fechaNacimientoClienteSoli,fechaNacimientoClienteSoli,popCal);return false" value="V" src="<%=mvarURLCalendario%>/calbtn.gif"></input>
	       
            <label id="sexoCliente_etiq" for="sexoCliente">Sexo:</label>
            <select class="areaDatCpoConInpFieM" id="sexoCliente" name="sexoCliente" onchange="" >
                <option value="-1">SELECCIONAR...</option>
                <option value="F">FEMENINO</option>
                <option value="M">MASCULINO</option>
            </select>                
	    </div>
	
	    <div class="areaDatCpoConInp" >
            <label id="estadoCivilCliente_etiq" for="estadoCivilCliente" >Estado Civil:</label>
            <select class="areaDatCpoConInpFieM" id="estadoCivilCliente" name="estadoCivilCliente" onchange="" >
                <option value="-1">SELECCIONAR...</option>
                <option value='S'>SOLTERO/A</option>
                <option value='C'>CASADO/A</option>
                <option value='E'>SEPARADO/A</option>
                <option value='V'>VIUDO/A</option>
                <option value='D'>DIVORCIADO/A</option> 
            </select>   
          
            <label id="paisNacimientoCliente_etiq" for="paisNacimientoCliente">Pa�s de Nacimiento:</label>
            <select  class="areaDatCpoConInpFieM" id="paisNacimientoCliente" name="paisNacimientoCliente" ></select>
	    </div>
    </div>
	
    <div class="areaDatCpoConInp" >
        <label id="TelPosDomicilio_etiq" for="CodTelPosDomicilio" >Tel&eacute;fono:</label>
        <input class="areaDatCpoConInpFieS" id="CodTelPosDomicilio" name="CodTelPosDomicilio"   onchange="sincronizaCampos(this,document.getElementById('codTelefonoCliente'))"   type="text" onkeypress="capturaTecla()" validarTipeo="telefono" maxlength="5" onpaste="return false;" style="width:45; margin-right:2 "/>

        <input class="areaDatCpoConInpFieM" id="TelPosDomicilio" name="TelPosDomicilio"  style="width:123" onchange="sincronizaCampos(this,document.getElementById('telefonoCliente'))"   type="text" onkeypress="capturaTecla()" validarTipeo="telefono" maxlength="20" onpaste="return false;"/>
        <label id="mailDomicilio_etiq" for="mailDomicilio">E-Mail:</label>
        <input class="areaDatCpoConInpFieM" id="mailDomicilio" name="mailDomicilio"  onchange="sincronizaCampos(this,document.getElementById('emailCliente'));fncCambiaEMail();"   type="text" onkeypress="capturaTecla()" validarTipeo="email" maxlength="50" onpaste="return false;">
    </div>

    <div class="areaDatCpoConInp" >
        <!-- Def.338 HRF 20110322 -->
        <label id="actividadesCliente_etiq" for="actividadesCliente" >Actividad/Ocupaci&oacute;n:</label>
		<input type="text"  class="areaDatCpoConInpFieM"  name="filtro_OCUPACODCLIE" id="filtro_OCUPACODCLIE" value="" 
		 onkeydown="if(window.event.keyCode==13){filtrarActividades('actividadesCliente','filtro_OCUPACODCLIE'); }"
		onkeypress="capturaTecla()" validarTipeo="alfanum" maxlength="50" onpaste="return false;" style="TEXT-TRANSFORM: uppercase">&nbsp;
		<img onclick="filtrarActividades('actividadesCliente','filtro_OCUPACODCLIE');" src="<%=mvarCANALURLASP%>/Canales/OV/Images/boton_buscar.gif" border="0" Style="Cursor:hand;" alt="Filtrar">&nbsp; al menos 2 caracteres alfab�ticos<br />
		<select style="margin-left:154px;" id="actividadesCliente"></select><br />
    </div>

    <div class="areaDatCpoConInp">
         <!-- AP/RAZON SOCIAL -->
        <label id="nroCuit_etiq"  for="nroCuit">CUIT/CUIL/CDI:</label>
        <input class="areaDatCpoConInpFieM" id="nroCuit" name="nroCuit" type="text" maxlength="11" onpaste="return false"  onkeypress="capturaTecla()" validarTipeo="numero"  validacionPersonalizada="fncValCliCUIT('nroCuit')" oncontextmenu="return false">
      
        <label id="tieneApoderado_etiq" for="tieneApoderado" >�Tiene Apoderado?:</label>
        <select class="areaDatCpoConInpFieM" id="tieneApoderado" name="tieneApoderado" onchange="muestraApoderado(this);fncCambiarColorCpoRen(this);" >
            <option value='S'>SI</option>
            <option value='N' selected>NO</option>
        </select>   
    </div>
</div>

<!--Apoderado-->
<div id="divApoderado" style="display:none">
<div class="areaDatTit">
            <div class="areaDatTitTex">Datos del Apoderado</div>
</div>
<div class="areaDatCpoCon">
     <div class="areaDatCpoConInp">
        <!-- APELLIDO -->
        <label id="apoApellido_etiq" for="apoApellido">Apellido/s:</label>
        <input class="areaDatCpoConInpFieM" id="apoApellido" name="apoApellido" type="text" onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="apellido" maxlength="40" onpaste="return false;" disabled/>
        <!-- NOMBRE -->
        <label id="apoNombre_etiq" for="apoNombre">Nombre/s:</label>
        <input class="areaDatCpoConInpFieM" id="apoNombre" name="apoNombre" type="text" onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="texto" maxlength="30" onpaste="return false;" disabled/>
    </div>
    <div class="areaDatCpoConInp">
        <!-- TIPO DE DOCUMENTO -->
        <label id="cboApoTipDoc_etiq" for="cboApoTipDoc">Tipo de documento:</label>
        <select class="areaDatCpoConInpFieM" id="cboApoTipDoc" name="cboApoTipDoc" onchange="fncCambiarColorCpoRen(this);" disabled>
       
        <option value="-1">Seleccionar...</option>
                        <%= mvarOptTDoc %>
        </select>
       
        
        <!-- NUMERO DE DOCUMENTO -->
        <label id="apoNroDoc_etiq" for="apoNroDoc">Nro. de documento:</label>
        <input class="areaDatCpoConInpFieM" id="apoNroDoc" name="apoNroDoc" validacionPersonalizada="verificaCUIT('apoNroDoc','cboApoTipDoc')" type="text"  onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="numero" maxlength="15" onpaste="return false;" disabled/>
    </div>
    
    <div class="areaDatCpoConInp" >
         <label id="apoFechaNacimiento_etiq">Fecha de Nacimiento:</label>
         <input class="areaDatCpoConInpFieD" id="apoFechaNacimiento" name="apoFechaNacimiento" type="text" obligatorio="si"  onkeypress="capturaTecla()" validarTipeo="fecha" maxlength="10" onpaste="return false;" validar="&lt; document.getElementById('fechaHoy').value" mensajeError="Fecha de Nacimiento  no puede ser mayor a la fecha del d�a"  onchange="validaRangoEdad(this,'18..99');" onBlur="return validateDateCal(this);" onchange="fncCambiarColorCpoRen(this);" disabled>              
		 <input class="areaDatCpoConInpFie" style="margin-right:63px" type="image" onClick="popFrame.fPopCalendar(document.getElementById('apoFechaNacimiento'),document.getElementById('apoFechaNacimiento'),popCal);return false" value="V" src="<%=mvarURLCalendario%>/calbtn.gif"></input>
	       
	  <label id="apoSexo_etiq" for="apoSexo">Sexo:</label>
           <select class="areaDatCpoConInpFieM" id="apoSexo" name="apoSexo" onchange="fncCambiarColorCpoRen(this);" disabled>
          <option value="-1">SELECCIONAR...</option>
          <option value="F">FEMENINO</option>
          <option value="M">MASCULINO</option>
           </select>                
	 </div>  
	
	    <div class="areaDatCpoConInp" >
           <label id="apoEstadoCivil_etiq" for="apoEstadoCivil" >Estado Civil:</label>
           <select class="areaDatCpoConInpFieM" id="apoEstadoCivil" name="apoEstadoCivil" onchange="fncCambiarColorCpoRen(this);" disabled >
          <option value="-1">SELECCIONAR...</option>
          <option value='S'>SOLTERO/A</option>
          <option value='C'>CASADO/A</option>
          <option value='E'>SEPARADO/A</option>
          <option value='V'>VIUDO/A</option>
          <option value='D'>DIVORCIADO/A</option> 
           </select>   
          
            <label id="apoPais_etiq" for="apoPais">Pa�s de Nacimiento:</label>
            <select  class="areaDatCpoConInpFieM" id="apoPais" name="apoPais" onchange="fncCambiarColorCpoRen(this);" disabled></select>
          
                        
	 </div>     
	
	 <div class="areaDatCpoConInp" >
        <label id="apoTelefono_etiq" for="apoCodTelefono" >Tel&eacute;fono:</label>
        <input class="areaDatCpoConInpFieS" id="apoCodTelefono" name="apoCodTelefono"  disabled    type="text" onkeypress="capturaTecla()" validarTipeo="telefono" maxlength="5" onpaste="return false;" style="width:45; margin-right:2 "/>
       
        <input class="areaDatCpoConInpFieM" id="apoTelefono" name="apoTelefono"  style="width:123"    type="text" onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="telefono" maxlength="20" onpaste="return false;" disabled />
        <label id="apoMail_etiq" for="apoMail">E-Mail:</label>
        <input class="areaDatCpoConInpFieM" id="apoMail" name="apoMail"     type="text" onchange="fncCambiarColorCpoRen(this)"  onkeypress="capturaTecla()" validarTipeo="email" maxlength="50" onpaste="return false;" disabled />
      </div>
         <div class="areaDatCpoConInp" >
       
        <label id="apoActividad_etiq" for="apoActividad" >Actividad/Ocupaci&oacute;n:</label>
		<input id="filtro_apoActividad" class="areaDatCpoConInpFieM" maxlength="50" name="filtro_apoActividad"
            onkeydown="if(window.event.keyCode==13){ filtrarActividades('apoActividad','filtro_apoActividad');fncCambiarColorCpoRen(document.getElementById('apoActividad'));}"
            onkeypress="capturaTecla()" onpaste="return false;" style="text-transform: uppercase"
            type="text" validartipeo="alfanum" value="">&nbsp;
		<img onclick="filtrarActividades('apoActividad','filtro_apoActividad');fncCambiarColorCpoRen(document.getElementById('apoActividad'));" src="<%=mvarCANALURLASP%>/Canales/OV/Images/boton_buscar.gif" border="0" Style="Cursor:hand;" alt="Filtrar">&nbsp; al menos 2 caracteres alfab�ticos<br />
		<select style="margin-left:154px;" id="apoActividad" onchange="fncCambiarColorCpoRen(this)" disabled></select><br />
        </div>                   
						
 			 
	  <div class="areaDatCpoConInp" >
         <!-- CUIT APOD -->
        <label id="apoCuit_etiq"  for="nroCuit">CUIT/CUIL/CDI:</label>
        <input class="areaDatCpoConInpFieM" id="apoCuit" name="apoCuit"   validacionPersonalizada="verificaCUIT('apoCuit')"  type="text" maxlength="11"  onkeypress="capturaTecla()" validarTipeo="numero"  onpaste="return false" oncontextmenu="return false" onchange="fncCambiarColorCpoRen(this);" disabled>
      
       
      </div>
</div>

<!-- HRF 2010-11-09 Def.27 -->
<div class="areaDatTit">
            <div class="areaDatTitTex">Domicilio del Apoderado</div>
        </div>
<div class="areaDatCpoCon">							 
	  <div class="areaDatCpoConInp" >
        
        <label id="apoCalleDomicilio_etiq" for="apoCalleDomicilio">Calle:</label>
        <input class="areaDatCpoConInpFieM" id="apoCalleDomicilio" name="apoCalleDomicilio" type="text"  onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="50" onpaste="return false" oncontextmenu="return false" disabled
        onkeydown="if(window.event.keyCode==13){fncCargaParamBuscaLoc('apoProvinciaDomicilio','apoLocalidadDomicilio','apoCodPosDomicilio','apoCalleDomicilio', 'apoNroDomicilio'); }"
        onchange="javascript:fncLocalidadLimpiar('apoProvinciaDomicilio','apoCodPosDomicilio','C'); fncCambiarColorCpoRen(this)" validacionPersonalizada="fncValCalle('apoProvinciaDomicilio','apoCodPosDomicilio','apoCalleDomicilio','apoNroDomicilio');" />
        <label id="apoNroDomicilio_etiq" for="apoNroDomicilio">Nro:</label>
        <!-- HRF Def.190 2010-10-26 -->
        <input class="areaDatCpoConInpFieM" id="apoNroDomicilio" name="apoNroDomicilio" type="text" onkeypress="capturaTecla()" validarTipeo="calle" maxlength="10" onpaste="return false" oncontextmenu="return false" disabled
        onkeydown="if(window.event.keyCode==13){fncCargaParamBuscaLoc('apoProvinciaDomicilio','apoLocalidadDomicilio','apoCodPosDomicilio','apoCalleDomicilio', 'apoNroDomicilio'); }"
        onchange="javascript:fncLocalidadLimpiar('apoProvinciaDomicilio','apoCodPosDomicilio','C'); fncCambiarColorCpoRen(this)" />
        </div>
        <div class="areaDatCpoConInp" >
        <label id="apoPisoDomicilio_etiq" for="apoPisoDomicilio">Piso:</label>
        <input class="areaDatCpoConInpFieM" id="apoPisoDomicilio" name="apoPisoDomicilio" type="text"  onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="5" onpaste="return false" oncontextmenu="return false" disabled>
          <label id="apoDptoDomicilio_etiq" for="apoDptoDomicilio">Dpto:</label>
       
        <input class="areaDatCpoConInpFieM" id="apoDptoDomicilio" name="apoDptoDomicilio" type="text" onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="5" onpaste="return false" oncontextmenu="return false" disabled>
        </div>
       <div class="areaDatCpoConInp" > 
        <label for="apoProvinciaDomicilio" id="apoProvinciaDomicilio_etiq">Provincia:</label>
            <select class="areaDatCpoConInpFieM" id="apoProvinciaDomicilio" name="apoProvinciaDomicilio"  onchange="fncProvinciaSeleccionadaConCP(this,'apoLocalidadDomicilio','apoCodPosDomicilio');fncCambiarColorCpoRen(this)" disabled></select>
             
         </div>
          <div   class="areaDatCpoConInp">       
           <!-- LOCALIDAD -->
            <label for="apoLocalidadDomicilio" id="apoLocalidadDomicilio_etiq">Localidad:</label>
            <input id="apoLocalidadDomicilio" class="areaDatCpoConInpFieL" style="width:300px; margin-right: 0px;"  name="apoLocalidadDomicilio" type="text"  disabled=""  
             onkeydown="if(window.event.keyCode==13){ fncCargaParamBuscaLoc('apoProvinciaDomicilio','apoLocalidadDomicilio','apoCodPosDomicilio','apoCalleDomicilio', 'apoNroDomicilio',5); }"
             onkeypress="capturaTecla()" validarTipeo="calle" maxlength="50" onpaste="return false;"
             onchange="javascript:fncLocalidadLimpiar('apoProvinciaDomicilio','apoCodPosDomicilio','L'); fncCambiarColorCpoRen(this)" />
            <a class="botonMenos" title="Buscar" onclick="fncCargaParamBuscaLoc('apoProvinciaDomicilio','apoLocalidadDomicilio','apoCodPosDomicilio','apoCalleDomicilio', 'apoNroDomicilio',5);"> 
            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" border="0" align="middle" width="14" height="14">
           </a>
            
            
           <label id="apoCodPosDomicilio_etiq" for="apoCodPosDomicilio" style="width:100px; padding-left:15px; padding-right:0px; margin-right: 0px;">Cod Pos:</label>
        <input class="areaDatCpoConInpFie" id="apoCodPosDomicilio" name="apoCodPosDomicilio" style="width:45px; padding-left:5px; padding-right:0px; margin-right: 0px;" disabled="disabled" type="text">    				    
    
                    				    
</div>


</div>

</div>
<!-- Fin Apoderado-->

<div class="areaDatTit">
            <div class="areaDatTitTex">Domicilio de Correspondencia</div>
        </div>
<div class="areaDatCpoCon">							 
	  <div class="areaDatCpoConInp" >
        
        <label id="calleDomicilio_etiq" for="calleDomicilio">Calle:</label>
        <input class="areaDatCpoConInpFieM" id="calleDomicilio" name="calleDomicilio" type="text"  onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="50" onkeydown="if(window.event.keyCode==13){fncCargaParamBuscaLoc('provinciaDomicilio','localidadDomicilio','codPosDomicilio','calleDomicilio', 'nroDomicilio'); }" onpaste="return false" oncontextmenu="return false" onchange="javascript:fncLocalidadLimpiar('provinciaDomicilio','codPosDomicilio','C');fncCambiarColorCpoRen(this)" validacionPersonalizada="fncValCalle('provinciaDomicilio','codPosDomicilio','calleDomicilio','nroDomicilio');" />
          <label id="nroDomicilio_etiq" for="nroDomicilio">Nro:</label>
        <input class="areaDatCpoConInpFieM" id="nroDomicilio" name="nroDomicilio" type="text"  onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="10" onkeydown="if(window.event.keyCode==13){ fncCargaParamBuscaLoc('provinciaDomicilio','localidadDomicilio','codPosDomicilio','calleDomicilio', 'nroDomicilio',5); }" onpaste="return false" oncontextmenu="return false" onchange="javascript:fncLocalidadLimpiar('provinciaDomicilio','codPosDomicilio','C');fncCambiarColorCpoRen(this)" />
        </div>
        <div class="areaDatCpoConInp" >
        <label id="pisoDomicilio_etiq" for="pisoDomicilio">Piso:</label>
        <input class="areaDatCpoConInpFieM" id="pisoDomicilio" name="pisoDomicilio" type="text" onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="5" onpaste="return false" oncontextmenu="return false">
          <label id="dptoDomicilio_etiq" for="dptoDomicilio">Dpto:</label>
       
        <input class="areaDatCpoConInpFieM" id="dptoDomicilio" name="dptoDomicilio" type="text" onchange="fncCambiarColorCpoRen(this)" onkeypress="capturaTecla()" validarTipeo="calle"  maxlength="5" onpaste="return false" oncontextmenu="return false">
        </div>
       <div class="areaDatCpoConInp" > 
        <label for="provinciaDomicilio" id="provinciaDomicilio_etiq">Provincia:</label>
            <select class="areaDatCpoConInpFieM" id="provinciaDomicilio" name="provinciaDomicilio"  onchange="fncProvinciaSeleccionadaConCP(this,'localidadDomicilio','codPosDomicilio'); fncCambiarColorCpoRen(this)"></select>
             
         </div>
          <div   class="areaDatCpoConInp">       
           <!-- LOCALIDAD -->
            <label for="localidadDomicilio" id="localidadDomicilio_etiq">Localidad:</label>
            <input id="localidadDomicilio" class="areaDatCpoConInpFieL" style="width:300px; margin-right: 0px;"  name="localidadDomicilio" type="text"  
            onkeydown="if(window.event.keyCode==13){ fncCargaParamBuscaLoc('provinciaDomicilio','localidadDomicilio','codPosDomicilio','calleDomicilio', 'nroDomicilio',5); }"
            onkeypress="capturaTecla()" validarTipeo="calle" maxlength="50" onpaste="return false;" oncontextmenu="return false"  disabled=""
            onchange="javascript:fncLocalidadLimpiar('provinciaDomicilio','codPosDomicilio','L'); fncCambiarColorCpoRen(this)" />
            <a id="btnBuscarCPDom" class="botonMenos" title="Buscar" onclick="fncCargaParamBuscaLoc('provinciaDomicilio','localidadDomicilio','codPosDomicilio','calleDomicilio', 'nroDomicilio',5);"> 
            <img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_ok.gif" border="0" align="middle" width="14" height="14">
           </a>
            
            <!--select class="areaDatCpoConInpFieM" id="localidadDomicilio" name="localidadDomicilio"  onchange="fncCodPostal(this,frmCotizacion.codPosDomicilio, frmCotizacion.provinciaDomicilio);" disabled="">
          
                <option value="-1">SELECCIONAR...</option>
            </select-->
           <label id="codPosDomicilio_etiq" for="codPosDomicilio" style="width:100px; padding-left:15px; padding-right:0px; margin-right: 0px;">Cod Pos:</label>
        <input class="areaDatCpoConInpFie" id="codPosDomicilio" name="codPosDomicilio" style="width:45px; padding-left:5px; padding-right:0px; margin-right: 0px;" disabled="disabled" type="text" />    				    
    
                    				    
</div>

    <div id="divBotonEditarDom" class="areaDatCpoConInp" style="display:none;">
        <span style="text-align:right;width:570;padding: 5 0 5 0"><a href="" onclick="fncEditarDomicilioConf(); return false" class="linksubir" ><img src="<%=mvarCANALURL & "Canales/" & mvarCanal %>/Images/btn_form.gif" border="0" align="absmiddle" width="14" height="14">&nbsp;Editar Domicilio</a></span>
    </div>
</div>


<!-- SECCION CONROMIDAD EMAIL -->
<div id="seccion_ConformEMail" style="display:none">
    <div class="areaDatTit">
        <div class="areaDatTitTex">Conformidad EMail</div>
    </div>
    
    <div class="areaDatCpoConInp">
        <!-- CONFORMIDAD -->
        <label id="cboConformEMail_etiq" for="cboConformEMail" style="width:483px;">Autorizo a QBE Seguros La Buenos Aires S.A. a suministrar mis datos personales a otras empresas del grupo QBE y/o empresas vinculadas al mismo, a fin de poder recibir informaci&oacute;n sobre los distintos servicios y/o productos brindados por ellas</label>
        <select class="areaDatCpoConInpFieS" id="cboConformEMail" name="cboConformEMail">
            <option value="N">NO</option>
            <option value="S" selected>SI</option>
        </select>
    </div>
</div>


<div class="areaDatTit">
    <div class="areaDatTitTex">Datos Generales de la P�liza</div>
</div>

<div class="areaDatCpoCon">							 
    <div id="divNroInternoSoli" class="areaDatCpoConInp" >
        <label id="nroSolicitud_etiq" for="nroSolicitud">Nro. Interno de Solicitud:</label>
        <input class="areaDatCpoConInpFieM" id="nroSolicitud" name="nroSolicitud" type="text"  onkeypress="capturaTecla()" validarTipeo="letrasnum" maxlength="25" onpaste="return false" oncontextmenu="return false" style="text-transform: uppercase">
        <!-- RENOVACION -->
        <!--GED 28/07/2010: Se agrega etiqueta poliza anterior --> 
        <span id="spanPolizaAnterior" style="display:none"><label style="font-weight:bold;">Renovaci&oacute;n P&oacute;liza:</label> <label id="polizaAnterior" for="nroSolicitud" style="font-weight:bold;"></label>
        </span> 
   
    </div>
    
    <div class="areaDatCpoConInp">
        <label id="vigenciaDesdeSoli_etiq" for="vigenciaDesdeSoli">Fecha de Inicio:</label>
        <input  class="areaDatCpoConInpFieD" id="vigenciaDesdeSoli" name="vigenciaDesdeSoli" type="text"  validar="&gt;= document.getElementById('fechaHoy').value" mensajeError="Fecha de Vigencia no puede ser anterior a la fecha actual"   onChange="validaRangoVigencia(this,'0..30'); fncFechaFin('S')" onBlur="return validateDateCal(this);" value=""  onkeypress="capturaTecla()" validarTipeo="fecha"  maxlength="10" onpaste="return false" oncontextmenu="return false" />
        <input class="areaDatCpoConInpFie" id="vigenciaDesdeSoliBtn" style="margin-right:63px" type="image" onClick="popFrame.fPopCalendar(document.getElementById('vigenciaDesdeSoli'),document.getElementById('vigenciaDesdeSoli'),popCal);return false" value="V" src="<%=mvarCANALURL%>includes/calendario/calbtn.gif"></input>
	    
        <label id="vigenciaHastaSoli_etiq" for="vigenciaHastaSoli">Fecha de Fin:</label>
        <input class="areaDatCpoConInpFieS" id="vigenciaHastaSoli" name="vigenciaHastaSoli" type="text"  disabled="" maxlength="10" onpaste="return false" oncontextmenu="return false">
    </div>

    <div class="areaDatCpoConInp">
        <b><label id="observClausulas_etiq" for="observClausulas" style="width:400">Observaciones/Cl�usulas: (max. 3000 cars.)</label></b>
      <!-- 18/10/2010 LR Defect 21 Estetica, pto 23 -->
      <textarea rows="5" cols="80" id="observClausulas" name="observClausulas" maxlength="3000" onpaste="return false" oncontextmenu="return false" style="text-transform: uppercase"></textarea>
    </div>

    <div class="areaDatCpoConInp">
        <b><label id="textoPoliza_etiq" for="textoPoliza" style="width:400">Texto de la p�liza: (max. 3000 cars.)</label></b>
      
      <textarea rows="5" cols="80" id="textoPoliza" name="textoPoliza" maxlength="3000" onpaste="return false;" oncontextmenu="return false" style="text-transform: uppercase"></textarea>
    </div>
</div>