<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: SoliIntComercio_DatosRiesgos.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados a los datos del riesgo.
'			   
' 
' ------------------------------------------------------------------------------
 
  %>

<div id="riesgosSolicitudMolde" style="display:inline">
    <!--SECCION DATOS COMERCIOS-->
    <div  id="datosViviendaNro##">
        <div id="domicilioRiesgoNro##" class="areaDatCpoCon">
            <div class="areaDatTit">
                <div class="areaDatTitTex">Datos de Comercio DViv</div>
            </div>    
	        <table cellspacing="0" cellpadding="0" style="width:100%;border:solid 1px #EDEDF1; padding:0; margin:0" id="tblDomRiesgo" name="tblDomRiesgo">
		        <tbody style="padding: 5 0 2 0">
			        <tr style="height:15">
				        <td colspan="8" style="padding:2 2 2 2;" bgcolor="#cccccc">
					        <b>Domicilio de Riesgo</b>
				        </td>
                    </tr>		        
			        <tr>
			            <!-- CERTISECANT para Renovacion -->
			            <input type="hidden" id="hidCERTISECNro_#" value="#IngresoCERTISEC#" />
			        
			            <!-- HRF Def.190 2010-10-26 -->
				        <td>
				            <label id="calleRiesgoItemNro_#_etiq" style="padding-left:5px ; width:30px; color:Blue">Calle:</label>
				        </td>
				        <td>
                            <input class="areaDatCpoConInpFieS" id="calleRiesgoItemNro_#" name="calleRiesgoItem" type="text"
                            onkeypress="capturaTecla()" validarTipeo="calle" maxlength="50" value="#IngresoCalle#" style="padding-left:0; width:200px"
                            onchange="javascript:fncCodigoPostalLimpiar(this)"
                            onkeydown="if(window.event.keyCode==13){fncCodigoPostalBuscar(this)}" />
                        </td>
                        <td>
				            <label id="numeroRiesgoItemNro_#_etiq" style="padding-left:5px ; width:30px; color:Blue">Nro:</label>
				        </td>
				        <td>
				            <input class="areaDatCpoConInpFie" id="numeroRiesgoItemNro_#" name="numeroRiesgoItem" type="text"
				            onkeypress="capturaTecla()" validarTipeo="calle" maxlength="10" value="#IngresoNumPue#" style="padding-left:0; width:45"
				            onchange="javascript:fncCodigoPostalLimpiar(this)"
				            onkeydown="if(window.event.keyCode==13){fncCodigoPostalBuscar(this)}" />
				        </td>
				        <td>
				            <label style="padding-left:5px ; width:30px">Piso:</label>
				        </td>
				        <td>
				            <input class="areaDatCpoConInpFie" id="pisoRiesgoItemNro_#" name="pisoRiesgoItem" type="text" onkeypress="capturaTecla()" validarTipeo="calle" maxlength="5" value="#IngresoPiso#" style="padding-left:0; width:45;"/>
				        </td>
				        <td>
                            <label style="padding-left:5px ; width:30px">Dpto:</label>
				        </td>
				        <td>
                            <input class="areaDatCpoConInpFie" id="dptoRiesgoItemNro_#" name="dptoRiesgoItem" type="text" onkeypress="capturaTecla()" validarTipeo="calle" maxlength="5" value="#IngresoDepto#" style="padding-left:0; width:45;"/>
				        </td>
				    </tr>
			        <tr style="background-color:#EDEDF1">
				        <td>
                            <label style="padding-left:5px ; width:30px">Provincia:</label>
				        </td>			        
				        <td colspan="7">
				            <input type="hidden" id="hidProvinCodItemNro_#" value="#IngresoProvinCod#" />
                            <input class="form_camposin_largo" id="provinciaRiesgoItemNro_#" name="provinciaRiesgoItem" type="text" disabled value="#IngresoProvincia#" style="padding-left:0; width:200;" />
				        </td>
				    </tr>
				    <tr>
				        <td>
                            <label style="padding-left:5px ; width:30px">Localidad:</label>
				        </td>				        
				        <td colspan="4">
                            <input class="form_camposin_largo" id="localidadRiesgoItemNro_#" name="localidadRiesgoItem" type="text" disabled 
                                onkeydown="if(window.event.keyCode==13){ fncCodigoPostalBuscar(this) }"
                            
                            value="#IngresoLocalidad#" style="padding-left:0; width:403;"/>
                        </td>				        
				        <td>
                            <a id="btnCodPosNro##" class="btnCodPos" tipo="btnCodPos" descripcion='btnCodPos##' title="Codigo Postal" onclick="javascript:fncCodigoPostalBuscar(this)" style="cursor:hand" >
                                <img src="<%=mvarCANALURLASP%>/Canales/<%=mvarCanal%>/Images/btn_ok.gif" border="0" alt="Codigo Postal" style="vertical-align:middle"/>
                            </a>
				        </td>
				        <td>
                            <label id="codposRiesgoItemNro_#_etiq" style="padding-left:5px ; width:30px; color:Blue">CodPos:</label>
				        </td>
				        <td>
                            <input class="form_camposin_largo" id="codposRiesgoItemNro_#" name="codposRiesgoItem" type="text" disabled value="#IngresoCodPos#" style="padding-left:0; width:45;" />
				        </td>				        
                    </tr>
                </tbody>
            </table>             
        </div>
        <div id="detalleRiesgoCoberturaNro##" class="areaDatCpoCon" >
            <table cellspacing="0" cellpadding="0" style="width:100%;border:solid 1px #EDEDF1; padding:0; margin:0" id="Table1" name="tblDomRiesgo">
                <tbody>
                    <tr>
                        <td colspan="8" style="padding:2 2 2 2" bgcolor="#cccccc">
                            <b>Detalle de Riesgo</b>
                        </td>
                    </tr>
                </tbody>
            </table>
       
            <input type="hidden" id="hidRiesgoCOBERCODNro##" value="#IngresoCOBERCOD#" />
            <input type="hidden" id="hidRiesgoCobSumaNro##" value="#IngresoSumaCob#" />
            
            <table cellspacing="0" cellpadding="0" style="width:100%;border:solid 1px #EDEDF1; padding:0; margin:0" id="tblCob" name="tblCob" >
	            <tbody >
		            <tr>
			            <td style="padding:2 2 2 2;">
				            <b>Cobertura: </b>CCCOOOBBB
			            </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div id="divObjeto" style="display:inline">
            <div id="ObjetoNro##" class="Objetos" descripcion="CodCob">
	            <table cellspacing="0" cellpadding="0" style="width:100%;border:solid 1px #EDEDF1; padding:0; margin:0" id="tblObjEspecificos" name="tblObjEspecificos" >
	                <tbody>
			            <tr>
				            <td style="width:160">
					            <label id="cboTipoObjetoNro##_etiq" style="padding-left:5px;width:100">Tipo de Objeto</label>
				            </td>
                            <td style="width:160">
					            <label id="txtDescripcionObjNro##_etiq" style="padding-left:5px;width:100">Descripci�n</label>
				            </td>
				            <td style="width:160">
					            <label id="txtSumaObjNro##_etiq" style="padding-left:5px;width:100"><%=mvarMONEDAPRODUCTO %> Suma</label>
				            </td>
				            <td style="width:50">
				            </td>
                        </tr>
			            <tr>
				            <td style="padding-left:5;width:160">
					            <select class="areaDatCpoConInpFieM" id="cboTipoObjetoNro##" name="cboTipoObjeto" referencia="cboTipoObjetoSelec" style="width:150;"></select>
					        </td>
                            <td style="padding-left:5;width:160">
					            <input class="areaDatCpoConInpFieM" id="txtDescripcionObjNro##" name="txtDescripcionObj" type="text" value=#txtDescripcion# style="width:150;"></input>
				            </td>
				            <td style="padding-left:5;width:160">
					            <input id="txtSumaObjNro##" name="txtSumaObj" type="text" value=#txtSuma# validarTipeo="numero" maxlength="13" onkeypress="capturaTecla()" onchange="javascript:fncSumarObjetosAsegurados(this)" style="width:150;"></input>
					        </td>
					        <td style="padding-left:5;width:50">
                                <a id="btnEliminarNro##" class="botonMenos" tipo="btnEliminar" descripcion='descNro##' title="Eliminar" onclick="JavaScript:fncObjetosEliminar('ObjetoNro##');fncObjetosRenumerar('ObjetoNro##');fncSumarObjetosAsegurados(this)">
                                    <img src="<%=mvarCANALURLASP%>/Canales/<%=mvarCanal%>/Images/boton_menos.gif" border="0" alt="Eliminar" style="vertical-align:middle"/>
                                </a>
                                <a id="btnAgregarNro##" class="botonAgregar" tipo="btnAgregar" descripcion='descNro##' title="Agregar" onclick="JavaScript:fncObjetoAdd('','','ObjetoNro##');">
                                    <img src="<%=mvarCANALURLASP%>/Canales/<%=mvarCanal%>/Images/boton_agregar.gif" border="0" alt="Agregar" style="vertical-align:middle"/>
                                </a>
				            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Lista de Elementos -->
        <div id="OBJ_LISTNro##" name="OBJ_LIST" style="display: ">
        </div>        
        <div id="totalSumaObjetos" class="areaDatCpoCon">
            <table cellspacing="0" cellpadding="0" style="width:100%;border:solid 1px #EDEDF1; padding:0; margin:0" id="tblSumaObjetos" name="tblSumaObjetos" >
                <tbody style="padding: 5 0 2 0">
			        <tr>
			            <td>
			                <b><label id="txtTotalSuma##_etiq" style="padding:0 14 0 360;width:50">Total</label></b>
					        <input class="areaDatCpoConInpFieM" id="txtTotalSuma##" name="txtTotalSuma" type="text" readonly  cobercod="codCobtxtTotalSuma" style="padding-left:1; width:150; color:Gray" />
			            </td>					    					    
                    </tr>                                                            
                </tbody>
            </table>                            
        </div>
    </div>    
</div>  

<script type="text/javascript">
    var mvarPrimerEliminar = -1;
    var mvarObjetoNro = 0;

function fncObjetosEliminar(divNum) {
    var mobjCampo = document.getElementById(divNum)
    pItem = mobjCampo.id.split("_")[1]
    pCober = mobjCampo.id.split("_")[2]
    pObjeto = mobjCampo.id.split("_")[3]

    //window.document.body.insertAdjacentHTML("beforeEnd", "oXMLErrores.xml= <textarea>" + document.getElementById('OBJ_LISTNro_' + pItem + '_' + pCober).outerHTML + "</textarea>")

    var d = document.getElementById('OBJ_LISTNro_' + pItem + '_' + pCober);   
    var olddiv = document.getElementById(divNum);
    
    d.removeChild(olddiv);

    mvarObjetoNro--;
    fncObjetosRefresh(divNum, 'false');
    //fncObjetosRenumerar(divNum)
}

function fncObjetoAdd(pvarObjAsegDes, pvarObjAsegPre, divNum) {
    var mvarObjList;
    var mvarObjeto;
   
   //GED 15-09-2010 DEFECT 103 
   
       if (mvarObjetoNro >= 10) {
            alert_customizado('No se pueden ingresar m�s de 10 objetos.');
            return;
        }
    
    var cantidad = document.getElementsByTagName('div').length - 1 ? document.getElementsByTagName('div').length - 1 : 0;
    //fncObjetosVerificarDatos()
    if (fncObjetosVerificarDatos(divNum)){    
        for (i = 0; i <= cantidad; i++) {
            
            mobjCampo = window.document.getElementsByTagName('DIV').item(i);
            if (mobjCampo.className == 'Objetos' && mobjCampo.id == divNum) {
                pItem = mobjCampo.id.split("_")[1]
                pCober = mobjCampo.id.split("_")[2]
                pObjeto = mobjCampo.id.split("_")[3]
                mvarObjetoNro = Number(pObjeto)+1

                mvarObjeto = document.getElementById("SOLIOBJETO").value;

                var re = /##/gi;
                mvarObjeto = mvarObjeto.replace(re, '_' + pItem + '_' + pCober + '_' + mvarObjetoNro);

                re = /CodCob/gi;
                mvarObjeto = mvarObjeto.replace(re, mobjCampo.descripcion);

                re = /#txtDescripcion#/gi;
                mvarObjeto = mvarObjeto.replace(re, '""');

                re = /#txtSuma#/gi;
                mvarObjeto = mvarObjeto.replace(re, '""');
                
                mvarObjList = document.getElementById("OBJ_LISTNro" + '_' + pItem + '_' + pCober).innerHTML
                document.getElementById("OBJ_LISTNro" + '_' + pItem + '_' + pCober).innerHTML = mvarObjList + mvarObjeto;

                fncObjetosCargar('cboTipoObjetoNro_' + pItem + '_' + pCober + '_' + mvarObjetoNro)

                fncObjetosRefresh('Nro_' + pItem + '_' + pCober + '_' + pObjeto, false)

                //fncInsertarObjAseg(divNum, mobjCampo.descripcion)
             //   window.document.body.insertAdjacentHTML("beforeEnd", "SOLIOBJETO= <textarea>" + document.getElementById("riesgosSolicitudMolde").innerHTML + "</textarea>")   
                break;
            }
        }
//        fncInsertarObjAseg(pItem+'_'+pCober+'_'+pObjeto)
    }
}
function fncObjetosRefresh(pId,pInicial) {
    var mvarEliminarActual = -1;
    var mvarAgregarActual = -1;
    var mvarEliminarCuenta = 0;
    var cont = 0
    var mvarDelAddIni = pId.split("_")[3]
    var pItem = pId.split("_")[1]
    var pCober = pId.split("_")[2]
    var pObjeto = pId.split("_")[3]

    for (i = document.getElementsByTagName("A").length - 1; i >= 0; i--) {
        if (document.getElementsByTagName("A").item(i).tipo == 'btnAgregar' && (document.getElementsByTagName("A").item(i).descripcion == 'desc' + pId || document.getElementsByTagName("A").item(i).descripcion == pId)) {
            document.getElementsByTagName("A").item(i).style.display = 'none';
            if (pInicial) {
                document.getElementsByTagName("A").item(i).style.display = ''
            }        
        }
    }
    for (i = document.getElementsByTagName("A").length - 1; i >= 0; i--) {
        if (document.getElementsByTagName("A").item(i).tipo == 'btnEliminar' && (document.getElementsByTagName("A").item(i).descripcion == 'desc' + pId || document.getElementsByTagName("A").item(i).descripcion == pId)) {
            document.getElementsByTagName("A").item(i).style.display = '';
            if (pInicial) {
                document.getElementsByTagName("A").item(i).style.display = 'none';
            }
        }
        if (document.getElementsByTagName("A").item(i).tipo == 'btnEliminar' && document.getElementsByTagName("A").item(i).descripcion == 'descNro_' + pItem + '_' + pCober + '_' + Number(pObjeto) - 1) {
            document.getElementsByTagName("A").item(i).style.display = '';        
        }
    }

    fncColoreoRenglonesObjAse();
}

function fncObjetosRenumerar(divNum) {
    var pItem = divNum.split("_")[1]
    var pCober = divNum.split("_")[2]
    var pObjeto = divNum.split("_")[3]
    var cont = 0

    for (i = 0; i <= window.document.getElementsByTagName('DIV').length - 1; i++) {
        mobjCampo = window.document.getElementsByTagName('DIV').item(i);
        if (mobjCampo.id == 'OBJ_LISTNro_' + pItem + '_' + pCober) {
            //document.body.insertAdjacentHTML("beforeEnd", "<br>OBJ_LIST <textarea>" + document.getElementById("OBJ_LISTNro" + '_' + pItem + '_' + pCober).innerHTML + "</textarea></br>")
            var varMax = mobjCampo.childNodes.length
                for (var j = 1; j <= varMax; j++) {
                    if (j >= pObjeto) {
                        cont++
                        var varElementOld = Number(pObjeto) + Number(cont)
                        var varElementNew = j 

                        var htmlViejo = document.getElementById("OBJ_LISTNro_" + pItem + "_" + pCober).innerHTML;
                        
                        var varViejo = 'Nro_' + pItem + '_' + pCober + '_' + varElementOld
                        var regElemento = new RegExp(varViejo, "gi")
                        var varNuevo = 'Nro_' + pItem + '_' + pCober + '_' + varElementNew

                        var htmlNuevo = htmlViejo.replace(regElemento, varNuevo);
                        
                        document.getElementById("OBJ_LISTNro_" + pItem + "_" + pCober).innerHTML = htmlNuevo                       
                        //document.body.insertAdjacentHTML("beforeEnd", "<br>htmlNuevo <textarea>" + htmlNuevo + "</textarea></br>")
                    }
                }
        }        
    }
    for (i =0;i <= document.getElementsByTagName("A").length - 1;i++) {
        if (document.getElementsByTagName("A").item(i).tipo == 'btnAgregar')
        {
            if (document.getElementsByTagName("A").item(i).descripcion == 'descNro_' + pItem + '_' + pCober + '_' + varMax) {
                document.getElementsByTagName("A").item(i).style.display = '';
                break;
            }
        }
    }
    for (i = 0; i <= document.getElementsByTagName("A").length - 1; i++) {
        if (document.getElementsByTagName("A").item(i).tipo == 'btnEliminar' && document.getElementsByTagName("A").item(i).descripcion == 'descNro_' + pItem + '_' + pCober + '_' + varMax) {
            if (varMax == 1) {
                document.getElementsByTagName("A").item(i).style.display = 'none';
                break;
            }
        }
    }    
}

</script>