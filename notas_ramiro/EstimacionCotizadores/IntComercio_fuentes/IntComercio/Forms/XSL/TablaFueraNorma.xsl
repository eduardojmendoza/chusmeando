<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
Fecha de Modificación: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: TablaFueraNorma.xsl

Fecha de Creacion: 15/07/2010

PPCR: 50055/6010661
Desarrollador: Marcelo Gasparini

Descripcion: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
  -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<!-- DEFINICION DE FUNCIONES -->
	<msxsl:script language="JScript" implements-prefix="user"><![CDATA[
	/* EN ESTA FUNCION SE CREA EL LINK PARA EL HREF */
	var g_pos = 0;
	var tieneRen = 'N';
	
function trStyle(pvarInc)
{
	if (pvarInc)
		g_pos++;
	if (g_pos % 2 == 0)
		return 'Form_campname_colorL';
	else
		return 'Form_campname_bcoL';
}

function setTieneRen(pValor)
{
	tieneRen = pValor;
	return "";
}

function getTieneRen()
{
	return tieneRen;
}

	]]><!-- FIN DEFINICION DE FUNCIONES -->
	</msxsl:script>
	<xsl:template match="/">
		<!-- Verifico que sea Alta o Renovación -->
		<xsl:if test="count(//ERROR[CERTISEC!='--']) != 0">
			<xsl:value-of select="user:setTieneRen('S')"/>
		</xsl:if>
		<!--  Comienzo a mostrar los datos -->
		<xsl:if test="count(//ERROR[TIPOERROR='N'] ) != 0">
			<table width="98%" cellpadding="0" cellspacing="0" border="0" id="tblListaErrores" style="padding-left: 15px">
				<tr>
					<td colspan="5">
						<table width="670" border="1" cellpadding="0" cellspacing="0" class="TableFueraNorma">
							<tr>
								<td bordercolor="#FFFFFF" style="border-right-width:0; color:'#FFFFFF'">
									<img name="titulo" width="7" height="15" align="absmiddle" style="color:'#FFFFFF'" src=""/> La cotización posee características que requieren la aprobación de la compañía.</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="670" border="1" cellpadding="0" cellspacing="0" Class="Table1">
							<tr bordercolor="#FFFFFF">
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%">
												<table border="0" width="100%" cellspacing="0" bordercolor="black" style="table-layout: fixed;" id="filasErrores">
													<colgroup>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
													</colgroup>
													<tbody>
														<xsl:variable name="solapa" select="0"/>
														<xsl:for-each select="//SOLAPA">
															<xsl:sort select="@nro" order="ascending" data-type="number"/>
															<xsl:if test="@nro != 2">
																<xsl:for-each select="ERROR[TIPOERROR='N']">
																	<tr class="Form_campname_bcoL">
																		<td colspan="10" height="5"/>
																	</tr>
																	<tr class="Form_campname_subEncab">
																		<td colspan="10">
																			<b>
																				<xsl:value-of select="LEYENDAERROR"/>
																			</b>
																		</td>
																	</tr>
																</xsl:for-each>
															</xsl:if>
															<xsl:if test="@nro = 2 and ERROR !='' ">
																<tr class="Form_campname_bcoL">
																	<td colspan="10" height="5"/>
																</tr>
																<tr class="Form_campname_subEncab">
																	<td colspan="10">
																		<b>Solapa <xsl:value-of select="@nom"/>
																		</b>
																	</td>
																</tr>
																<!-- 06/10/2010 LR Defect 21 Estetica, pto 13 -->
																<tr class="Form_campname_bcoL">
																	<td colspan="10" height="5"/>
																</tr>
																<tr class="Form_campname_subEncab">
																	<td align="center">
																		<b>Item 
																	</b>
																	</td>
																	<td align="center" colspan="2">
																		<b>Ubicación 
																	</b>
																	</td>
																	<td colspan="2" align="center">
																		<b>Cobertura
																	</b>
																	</td>
																	<xsl:if test="user:getTieneRen()='N'">
																		<td colspan="5" align="center">
																			<b>Motivo
																		</b>
																		</td>
																	</xsl:if>
																	<xsl:if test="user:getTieneRen()!='N'">
																		<td colspan="4" align="center">
																			<b>Motivo
																		</b>
																		</td>
																	</xsl:if>
																	<xsl:if test="user:getTieneRen()!='N'">
																		<td colspan="1" align="center">
																			<b>Certificado
																		</b>
																		</td>
																	</xsl:if>
																</tr>
																<xsl:for-each select="ERROR[TIPOERROR='N'] ">
																	<!-- 06/10/2010 LR Defect 21 Estetica, pto 13 -->
																	<!--<tr style="cursor:hand">-->
																	<tr>
																		<xsl:attribute name="class"><xsl:value-of select="user:trStyle(1)"/></xsl:attribute>
																		<!--xsl:attribute name="onclick">Javascript:irALabel('<xsl:value-of select="parent::SOLAPA/@nro"/>','<xsl:value-of select="CAMPO"/>')</xsl:attribute-->
																		<td>
																			<xsl:value-of select="NROITEM"/>
																		</td>
																		<td colspan="2">
																			<xsl:value-of select="UBICACION"/>
																		</td>
																		<td colspan="2">
																			<xsl:value-of select="LABELCAMPO"/>
																		</td>
																		<xsl:if test="user:getTieneRen()='N'">
																			<td colspan="5">
																				<xsl:value-of select="LEYENDAERROR"/>
																			</td>
																		</xsl:if>
																		<xsl:if test="user:getTieneRen()!='N'">
																			<td colspan="4">
																				<xsl:value-of select="LEYENDAERROR"/>
																			</td>
																		</xsl:if>
																		<xsl:if test="user:getTieneRen()!='N'">
																			<td colspan="1">
																				<xsl:value-of select="CERTISEC"/>
																			</td>
																		</xsl:if>
																	</tr>
																</xsl:for-each>
															</xsl:if>
														</xsl:for-each>
													</tbody>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br/>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
