<?xml version="1.0" encoding="UTF-8"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: getComboConveniosProd.xsl

Fecha de Creación: 11/06/2010

PPCR: 50055/6010661

Desarrollador: Gabriel D'Agnone

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
		
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
	
	
		<options>
		<xsl:if test="count(//CONVENIO)=0">
		<option>
				<xsl:attribute name="value">0|0|0</xsl:attribute>
					NO SE ENCONTRARON CONVENIOS
						</option>
		</xsl:if>
		<xsl:if test="count(//CONVENIO)=1">
			<xsl:for-each select="//CONVENIO">
				<option>
					<xsl:attribute name="value"><xsl:value-of select="CODIGO"/>|<xsl:value-of select="CUADRO"/>|<xsl:value-of select="RECARGO"/>|<xsl:value-of select="COMISION"/></xsl:attribute>
						<xsl:attribute name="cuadro"><xsl:value-of select="CUADRO"/></xsl:attribute>
						<xsl:attribute name="recargo"><xsl:value-of select="RECARGO"/></xsl:attribute>
						<xsl:attribute name="comision"><xsl:value-of select="COMISION"/></xsl:attribute>
					<xsl:value-of select="DESCRIPCION"/>
				</option>
			</xsl:for-each>
		
		</xsl:if>
		
		<xsl:if test="count(//CONVENIO)>1">
			<option>
				<xsl:attribute name="value">-1</xsl:attribute>
					Seleccionar...
						</option>
			<xsl:for-each select="//CONVENIO">
				<option>
					<xsl:attribute name="value"><xsl:value-of select="CODIGO"/>|<xsl:value-of select="CUADRO"/>|<xsl:value-of select="RECARGO"/>|<xsl:value-of select="COMISION"/></xsl:attribute>
					<xsl:attribute name="cuadro"><xsl:value-of select="CUADRO"/></xsl:attribute>
						<xsl:attribute name="recargo"><xsl:value-of select="RECARGO"/></xsl:attribute>
						<xsl:attribute name="comision"><xsl:value-of select="COMISION"/></xsl:attribute>
					<xsl:value-of select="DESCRIPCION"/>
				</option>
			</xsl:for-each>
			
			</xsl:if>
		</options>
	</xsl:template>
</xsl:stylesheet>
