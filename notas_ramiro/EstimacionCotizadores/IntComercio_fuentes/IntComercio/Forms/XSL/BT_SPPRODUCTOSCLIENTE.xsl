<?xml version="1.0" encoding="UTF-8"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: BT_SPPRODUCTOSCLIENTE.xsl

Fecha de Creación: 03/09/2010

PPCR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<Response>
			<xsl:copy-of select="//Estado"/>
			<CUENTAS>
				<xsl:for-each select="//REG">
					<CUENTA>
						<xsl:copy-of select="./*"/>
						<xsl:choose>
							<xsl:when test="./TipoProducto = '1' or ./TipoProducto = '2'">
								<TIPODESC><![CDATA[HSBC CA $]]></TIPODESC>
								<COBROTIP>CA</COBROTIP>
								<BANCOCOD>0150</BANCOCOD>
								<SUCURCOD>
									<xsl:value-of select="substring(normalize-space(./Producto),1,3)"/>
								</SUCURCOD>
								<MOSTRAR>SI</MOSTRAR>
								<option BANCOCOD="0150" COBROTIP="CA">
									<xsl:attribute name="value">DB|0150|CA|<xsl:value-of select="normalize-space(./Producto)"/>|<xsl:value-of select="substring(normalize-space(./Producto),1,3)"/></xsl:attribute>
									<xsl:attribute name="SUCURCOD"><xsl:value-of select="substring(normalize-space(./Producto),1,3)"/></xsl:attribute>HSBC CA $ <xsl:value-of select="normalize-space(./Producto)"/>
								</option>
							</xsl:when>
							<xsl:when test="./TipoProducto = '0'">
								<TIPODESC><![CDATA[HSBC CC $]]></TIPODESC>
								<COBROTIP>CC</COBROTIP>
								<BANCOCOD>0150</BANCOCOD>
								<SUCURCOD>
									<xsl:value-of select="substring(normalize-space(./Producto),1,3)"/>
								</SUCURCOD>
								<MOSTRAR>SI</MOSTRAR>
								<option BANCOCOD="0150" COBROTIP="CC">
									<xsl:attribute name="value">DB|0150|CC|<xsl:value-of select="normalize-space(./Producto)"/>|<xsl:value-of select="substring(normalize-space(./Producto),1,3)"/></xsl:attribute>
									<xsl:attribute name="SUCURCOD"><xsl:value-of select="substring(normalize-space(./Producto),1,3)"/></xsl:attribute>HSBC CC $ <xsl:value-of select="normalize-space(./Producto)"/>
								</option>
							</xsl:when>
							<xsl:when test="./TipoProducto = '3'">
								<TIPODESC><![CDATA[HSBC CA U$D]]></TIPODESC>
								<COBROTIP>A2</COBROTIP>
								<BANCOCOD>0150</BANCOCOD>
								<SUCURCOD>
									<xsl:value-of select="substring(normalize-space(./Producto),1,3)"/>
								</SUCURCOD>
								<MOSTRAR>NO</MOSTRAR>
								<option BANCOCOD="0150" COBROTIP="CA">
									<xsl:attribute name="value">DB|0150|CA|<xsl:value-of select="normalize-space(./Producto)"/>|<xsl:value-of select="substring(normalize-space(./Producto),1,3)"/></xsl:attribute>
									<xsl:attribute name="SUCURCOD"><xsl:value-of select="substring(normalize-space(./Producto),1,3)"/></xsl:attribute>HSBC CA U$D <xsl:value-of select="normalize-space(./Producto)"/>
								</option>
							</xsl:when>
							
							<xsl:when test="./TipoProducto = '6'">
								<TIPODESC><![CDATA[HSBC VISA]]></TIPODESC>
								<COBROTIP>TC</COBROTIP>
								<BANCOCOD>0150</BANCOCOD>
								<SUCURCOD>
								</SUCURCOD>
								<MOSTRAR>SI</MOSTRAR>
								<option BANCOCOD="0150" COBROTIP="VI" TARJECOD="7" TIPOPRODUCTO="6">
									<xsl:attribute name="value">TC|0150|VI|<xsl:value-of select="normalize-space(./Producto)"/>|7|6|<xsl:value-of select="VTOMES"/>|<xsl:value-of select="VTOANN"/></xsl:attribute>
									<xsl:attribute name="VTOMES"><xsl:value-of select="VTOMES"/></xsl:attribute>
									<xsl:attribute name="VTOANN"><xsl:value-of select="VTOANN"/></xsl:attribute>									
									HSBC VISA <xsl:value-of select="normalize-space(./Producto)"/>
								</option>
							</xsl:when>	
							<xsl:when test="./TipoProducto = '7'">
								<TIPODESC><![CDATA[HSBC MASTERCARD]]></TIPODESC>
								<COBROTIP>TC</COBROTIP>
								<BANCOCOD>0150</BANCOCOD>
								<SUCURCOD>
								</SUCURCOD>
								<MOSTRAR>SI</MOSTRAR>
								<option BANCOCOD="0150" COBROTIP="MC" TARJECOD="2" TIPOPRODUCTO="7">
									<xsl:attribute name="value">TC|0150|MC|<xsl:value-of select="normalize-space(./Producto)"/>|2|7|<xsl:value-of select="VTOMES"/>|<xsl:value-of select="VTOANN"/></xsl:attribute>
									<xsl:attribute name="VTOMES"><xsl:value-of select="VTOMES"/></xsl:attribute>
									<xsl:attribute name="VTOANN"><xsl:value-of select="VTOANN"/></xsl:attribute>									
									HSBC MASTERCARD <xsl:value-of select="normalize-space(./Producto)"/>
								</option>
							</xsl:when>	
							<xsl:when test="./TipoProducto = 'E'">
								<TIPODESC><![CDATA[HSBC AMERICAN EXPRESS]]></TIPODESC>
								<COBROTIP>TC</COBROTIP>
								<BANCOCOD>0150</BANCOCOD>
								<SUCURCOD>
								</SUCURCOD>
								<MOSTRAR>SI</MOSTRAR>
								<option BANCOCOD="0150" COBROTIP="AE" TARJECOD="5" TIPOPRODUCTO="E">
									<xsl:attribute name="value">TC|0150|AE|<xsl:value-of select="normalize-space(./Producto)"/>|5|E|<xsl:value-of select="VTOMES"/>|<xsl:value-of select="VTOANN"/></xsl:attribute>
									<xsl:attribute name="VTOMES"><xsl:value-of select="VTOMES"/></xsl:attribute>
									<xsl:attribute name="VTOANN"><xsl:value-of select="VTOANN"/></xsl:attribute>									
									HSBC AMERICAN EXPRESS <xsl:value-of select="normalize-space(./Producto)"/>
								</option>
							</xsl:when>	
							
							<xsl:otherwise>
								<TIPODESC><![CDATA[Tipo desconocido]]></TIPODESC>
								<COBROTIP/>
								<BANCOCOD/>
								<SUCURCOD/>
								<MOSTRAR>NO</MOSTRAR>
							</xsl:otherwise>
						</xsl:choose>
					</CUENTA>
				</xsl:for-each>
			</CUENTAS>
		</Response>
	</xsl:template>
</xsl:stylesheet>
