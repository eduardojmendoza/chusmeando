<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
Fecha de Modificación: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: getseccionCoberturas.xsl

Fecha de Creación: 18/09/2010

Desarrollador: Gabriel D'Agnone

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
  -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:param name="estadoOperacion"/>
	<xsl:param name="monedaProducto"/>
	<xsl:template match="/">
		<xsl:if test="$estadoOperacion = 'C' ">
			<xsl:variable name="mostrar" select="concat('no','ne')"/>
		</xsl:if>
		<xsl:if test="$estadoOperacion = 'S' ">
			<xsl:variable name="mostrar">inline</xsl:variable>
		</xsl:if>
		<div class="areaDatTit">
			<div class="areaDatTitTex">Coberturas</div>
		</div>
		<!-- MMC - 14/10/2010 El bloque coberturas no debe figurar hasta que se selecciones la localidad...-->
		<div class="areaDatCpoCon">
			<table cellspacing="0" cellpadding="0" style="width:100%;border:solid 1px #EDEDF1; padding:0; margin:0" id="CobTable" name="CobTable">
				<tbody>
					<tr style="padding:0; margin:0">
						<td style="padding:0; margin:0">
							<b>Cobertura</b>
						</td>
						<td/>
						
						<td>
							<b>Suma Asegurada</b>
						</td>
						
						<td>
						</td>						
					</tr>
					<xsl:for-each select="//COBERTURAS/COBER">
						<tr style="padding:0; margin:0">
							<xsl:attribute name="id"><xsl:value-of select="concat('FILA',IDCOBERTURA)"/></xsl:attribute>
							<xsl:if test="position() mod 2=1">
								<xsl:attribute name="class">filaLlena</xsl:attribute>
							</xsl:if>
							<xsl:if test="position() mod 2=0">
								<xsl:attribute name="class">filaVacia</xsl:attribute>
							</xsl:if>
							<td style="width:550; padding:0; margin:0">
								<xsl:attribute name="id"><xsl:value-of select="concat('COBERTURA',position()-1)"/></xsl:attribute>
								<xsl:value-of select="COBERTURA"/>
							</td>
							<td style="padding:0; margin:0; width:50" colspan="1">

								<input type="checkbox">
									<xsl:attribute name="value"><xsl:value-of select="IDCOBERTURA"/></xsl:attribute>
									<xsl:attribute name="tipo">cobertura</xsl:attribute>
									<xsl:attribute name="cobercod"><xsl:value-of select="IDCOBERTURA"/></xsl:attribute>
									<xsl:attribute name="id"><xsl:value-of select="concat('COBCHECK',position()-1)"/></xsl:attribute>
									<xsl:attribute name="name">COBCHECK</xsl:attribute>
									<xsl:attribute name="descripcion"><xsl:value-of select="COBERTURA"/></xsl:attribute>
									<xsl:attribute name="oldvalue">0</xsl:attribute>
									<xsl:attribute name="sumaAsegurada"><xsl:value-of select="SUMA_ASEGURADA_ICOBB"/></xsl:attribute>
									<xsl:attribute name="primaCob"><xsl:value-of select="PRIMA_COB_ICOBB"/></xsl:attribute>
									<xsl:attribute name="tasaCob"><xsl:value-of select="TASA_COB_ICOBB"/></xsl:attribute>
									<xsl:attribute name="modulo"/>
									<!--<xsl:attribute name="onchange">fncActivaRecotizar(); </xsl:attribute>-->
									<!--<xsl:attribute name="onclick">fncCoberturaSeleccionada(this);  fncCambiarColorTodas(); fncClickCoberturasRen(this);</xsl:attribute>-->
									<!--<xsl:if test="SWOBLIG = 'S' ">-->
										<xsl:attribute name="checked"/>
										<xsl:attribute name="disabled"/>
									<!--</xsl:if>-->
									<!--xsl:if test="//COBERTURA[COBERCOD=//COBERPRI ]/SWOBLIG" >											
											<xsl:attribute name="disabled"></xsl:attribute>											
										</xsl:if-->
								</input>
								
							</td>
							<td>
								<input type="text" align="right">
									<xsl:attribute name="id"><xsl:value-of select="concat('SUMAASEG',position()-1)"/></xsl:attribute>
									<xsl:attribute name="disabled"></xsl:attribute>																						
									<xsl:attribute name="value"><xsl:value-of select="SUMA_ASEGURADA_ICOBB"/></xsl:attribute>									
								</input>
							</td>
							<td style="width:100; padding:0; margin:0">
							</td>
							<td style="display:none">
								<input type="text" align="right" style="width:45px; text-align:rigth">
									<xsl:attribute name="id"><xsl:value-of select="concat('PRIMA',position()-1)"/></xsl:attribute>
									<xsl:attribute name="disabled"></xsl:attribute>											
									<xsl:attribute name="value"><xsl:value-of select="PRIMA_COB_ICOBB"/></xsl:attribute>									
								</input>
							</td>
							<td style="display:none">
								<input type="text" align="right" style="width:45px; text-align:rigth">
									<xsl:attribute name="id"><xsl:value-of select="concat('TASA',position()-1)"/></xsl:attribute>
									<xsl:attribute name="disabled"></xsl:attribute>																				
									<xsl:attribute name="value"><xsl:value-of select="TASA_COB_ICOBB"/></xsl:attribute>
								</input>
							</td>
						</tr>
					</xsl:for-each>
					<tr>
						<xsl:if test="position() mod 2=1">
							<xsl:attribute name="class">filaLlena</xsl:attribute>
						</xsl:if>
						<xsl:if test="position() mod 2=0">
							<xsl:attribute name="class">filaVacia</xsl:attribute>
						</xsl:if>
						<td>
						</td>
						<td style="width:50; padding:0; margin:0">
							<b>Total </b>
						</td>
					
						<td align="center">
							<div id="divSUMATOT3">
								<input type="text" class="form_camposin_largo" name="SUMAASEGTOTAL" id="SUMAASEGTOTAL" value="" maxlength="20" disabled=""/>
							</div>
						</td>
						<td style="width:100; padding:0; margin:0">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
