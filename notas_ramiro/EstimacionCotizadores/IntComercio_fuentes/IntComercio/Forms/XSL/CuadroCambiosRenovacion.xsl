<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: CuadroCambiosRenovacion.xsl

Fecha de Creaci�n: 02/12/2010

PPCR: 50055/6010661

Desarrollador: Hern�n R. Fus�

Descripci�n: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:param name="urlCanal"/>
	<msxsl:script language="JScript" implements-prefix="user"><![CDATA[
var g_pos = 0;
function g_pos_init()
{
	g_pos = 0;
	return '';

}
function TRStyle(pvarInc)
{
	if (pvarInc)
		g_pos++;
	if (g_pos % 2 == 0)
		return 'Form_campname_bcoL';
	else
		return 'Form_campname_colorL';
}

function TRStyleResalta(pvarInc)
{
	if (pvarInc)
		g_pos++;
	if (g_pos % 2 == 0)
		return 'Form_campname_bco_resaltaL';
	else
		return 'Form_campname_color_resaltaL';
}
	
function TDStyle()
{
	if (g_pos % 2 == 0)
		return '#FFFFFF';
	else
		return '#EEEEEE';
}

function GetImgName()
{
	if (g_pos % 2 == 0)
		return '/Oficina_Virtual/Html/images/boton_buscar.gif';
	else
		return '/Oficina_Virtual/Html/images/boton_buscar.gif';
}

function sacocero(pvarNodeList)
{
	
	var res=/000000/gi;
	var re=/0000/gi;	
	var pvarRetorno;		
	var pvarThis;
	
	pvarThis = pvarNodeList.nextNode();	

	if (pvarThis.selectSingleNode('RAMO').text != '1')
		return '';
				
	pvarRetorno = pvarThis.selectSingleNode('CERPOL').text.replace(re,'') + pvarThis.selectSingleNode('CERANN').text.replace(re,'') + pvarThis.selectSingleNode('CERSEC').text.replace(res,'')
	
	if (pvarRetorno != '')
		pvarRetorno = ' / ' + pvarRetorno;

	return pvarRetorno;
}
function ReemplazarApostrofe(pvarNodeList)
{
	var re=/'/gi;
	var pvarThis;
	var pvarRetorno;

       return pvarNodeList.item(0).attributes.getNamedItem("NOM").text.replace(re,'&#39');
}
]]><!-- FIN DEFINICION DE FUNCIONES -->
	</msxsl:script>
	<xsl:template match="/">
		<xsl:if test="count(//SOLAPA) != 0">
			<p/>
			<p>Los cambios realizados son los siguientes:</p>
			<table width="575" border="0" cellspacing="2">
				<tr class="form_titulos02" style="height:10">
					<td height="10" colspan="1" style="width:226">
						<div align="center">Campo/Cobertura</div>
					</td>
					<td width="72">
						<div align="center">Cambio</div>
					</td>
					<td width="150">
						<div align="center">Valor Inicial</div>
					</td>
					<td width="151">
						<div align="center">Valor Actual</div>
					</td>
				</tr>
			</table>
			<xsl:for-each select="//SOLAPA">
				<table width="575" border="0" cellspacing="0" style=" border:solid 1px #DDDDDD; margin:1">
					<xsl:attribute name="id">SOLAPA<xsl:value-of select="position()"/>A</xsl:attribute>
					<tr>
						<xsl:if test="position() mod 2=1">
							<xsl:attribute name="class">filaLlena</xsl:attribute>
						</xsl:if>
						<xsl:if test="position() mod 2=0">
							<xsl:attribute name="class">filaVacia</xsl:attribute>
						</xsl:if>
						<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
						<td width="25" height="25">
							<div align="center" style="cursor:hand">
								<xsl:attribute name="onclick">verMas(SOLAPA<xsl:value-of select="position()"/>B,SOLAPA<xsl:value-of select="position()"/>A)</xsl:attribute>
								<img border="0" alt="Expandir" style="cursor:hand">
									<xsl:attribute name="src"><xsl:value-of select="concat( $urlCanal,'\Images\boton_agregar.gif' )"/></xsl:attribute>
								</img>
							</div>
						</td>
						<td width="550">
							<div align="left">
								<B><xsl:value-of select="@SOLAPA"/>
								</B>
							</div>
						</td>
						<!--td width="48" align="right"></td>
						<td width="45">	</td-->
					</tr>
				</table>
				<table width="575" border="0" style="display:none; border:solid 1px #DDDDDD; margin:1" cellspacing="0">
					<xsl:attribute name="id">SOLAPA<xsl:value-of select="position()"/>B</xsl:attribute>
					<tr>
						<xsl:if test="position() mod 2=1">
							<xsl:attribute name="class">filaLlena</xsl:attribute>
						</xsl:if>
						<xsl:if test="position() mod 2=0">
							<xsl:attribute name="class">filaVacia</xsl:attribute>
						</xsl:if>
						<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
						<td colspan="8">
							<xsl:attribute name="class">filaVacia</xsl:attribute>
							<table width="100%" border="0" style="display:inline" cellspacing="0">
								<tr>
									<xsl:if test="position() mod 2=1">
										<xsl:attribute name="class">filaLlena</xsl:attribute>
									</xsl:if>
									<xsl:if test="position() mod 2=0">
										<xsl:attribute name="class">filaVacia</xsl:attribute>
									</xsl:if>
									<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
									<td width="25" height="25">
										<div align="center" style="cursor:hand">
											<xsl:attribute name="onclick">verMas(SOLAPA<xsl:value-of select="position()"/>A,SOLAPA<xsl:value-of select="position()"/>B)</xsl:attribute>
											<img border="0" alt="Contraer" style="cursor:hand">
												<xsl:attribute name="src"><xsl:value-of select="concat( $urlCanal,'\Images\boton_menos.gif')"/></xsl:attribute>
											</img>
										</div>
									</td>
									<td width="550">
										<div align="left">
											<B><xsl:value-of select="@SOLAPA"/>
											</B>
										</div>
									</td>
									<!--td width="48" align="right"></td>
									<td width="45"></td-->
								</tr>
							</table>
						</td>
					</tr>
					<xsl:variable name="GOPOS" select="position()"/>
					<!--SI HAY UN SOLO ITEM-->
					<!--xsl:value-of select="user:g_pos_init()"/-->
					<xsl:if test="count(ITEM) = 1">
						<tr>
							<xsl:if test="position() mod 2=1">
								<xsl:attribute name="class">filaLlena</xsl:attribute>
							</xsl:if>
							<xsl:if test="position() mod 2=0">
								<xsl:attribute name="class">filaVacia</xsl:attribute>
							</xsl:if>
							<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
							<td colspan="8">
								<xsl:attribute name="class">filaVacia</xsl:attribute>
								<table width="575" border="0" style="display:inline" cellspacing="2">
									<xsl:attribute name="id">SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>B</xsl:attribute>
									<!--xsl:value-of select="user:g_pos_init()"/-->
									<xsl:for-each select="ITEM/CAMPO">
										<tr>
											<xsl:if test="position() mod 2=1">
												<xsl:attribute name="class">filaLlena</xsl:attribute>
											</xsl:if>
											<xsl:if test="position() mod 2=0">
												<xsl:attribute name="class">filaVacia</xsl:attribute>
											</xsl:if>
											<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
											<td width="25" height="25"/>
											<td width="200">
												<div style="margin-left:20">
													<xsl:value-of select="@CAMPO"/>
												</div>
											</td>
											<td width="74" align="center" style="text-align:center">
												<xsl:value-of select="@DESCTIPCAMB"/>
											</td>
											<td width="157" align="right">
												<xsl:value-of select="@VINICIAL"/>
											</td>
											<td width="155">
												<xsl:value-of select="@VNUEVO"/>
											</td>
										</tr>
									</xsl:for-each>
									<!-- FIN DE PR -->
								</table>
							</td>
						</tr>
					</xsl:if>
					<!--FIN-->
					<!--xsl:value-of select="user:g_pos_init()"/-->
					<xsl:if test="count(ITEM) > 1">
						<xsl:for-each select="ITEM">
							<tr>
								<xsl:if test="position() mod 2=1">
									<xsl:attribute name="class">filaLlena</xsl:attribute>
								</xsl:if>
								<xsl:if test="position() mod 2=0">
									<xsl:attribute name="class">filaVacia</xsl:attribute>
								</xsl:if>
								<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
								<td colspan="8">
									<xsl:attribute name="class">filaVacia</xsl:attribute>
									<table width="575"  style="display:inline; border:solid 1px #DDDDDD" cellspacing="0">
										<xsl:attribute name="id">SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>A</xsl:attribute>
										<tr>
											<xsl:if test="position() mod 2=1">
												<xsl:attribute name="class">filaLlena</xsl:attribute>
											</xsl:if>
											<xsl:if test="position() mod 2=0">
												<xsl:attribute name="class">filaVacia</xsl:attribute>
											</xsl:if>
											<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
											<td width="25" height="25">
												<xsl:if test="count(CAMPO) > 0">
													<div align="center" style="cursor:hand;margin-left:10">
														<xsl:attribute name="onclick">verMas(SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>B,SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>A)</xsl:attribute>
														<img border="0" alt="Expandir" style="cursor:hand">
															<xsl:attribute name="src"><xsl:value-of select="concat( $urlCanal,'\Images\boton_agregar.gif' )"/></xsl:attribute>
														</img>
													</div>
												</xsl:if>
											</td>
											<td width="550">
												<div style="margin-left:10" align="left"><xsl:value-of select="@ITEM"/> 
												
												
												</div>
											</td>
											<!--td width="48" align="right"></td>
										<td width="45">	</td-->
										</tr>
									</table>
									<table width="575" border="0" style="display:none; border:solid 1px #DDDDDD" cellspacing="0">
										<xsl:attribute name="id">SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>B</xsl:attribute>
										<tr>
											<xsl:if test="position() mod 2=1">
												<xsl:attribute name="class">filaLlena</xsl:attribute>
											</xsl:if>
											<xsl:if test="position() mod 2=0">
												<xsl:attribute name="class">filaVacia</xsl:attribute>
											</xsl:if>
											<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
											<td width="25" height="25">
												<div style="cursor:hand;margin-left:10">
													<xsl:attribute name="onclick">verMas(SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>A,SOLAPA<xsl:value-of select="$GOPOS"/>ITEM<xsl:value-of select="position()"/>B)</xsl:attribute>
													<img border="0" alt="Contraer" style="cursor:hand">
														<xsl:attribute name="src"><xsl:value-of select="concat( $urlCanal,'\Images\boton_menos.gif')"/></xsl:attribute>
													</img>
												</div>
											</td>
											<td colspan="5" width="550">
												<div style="margin-left:10" align="left"><xsl:value-of select="@ITEM"/>
												</div>
											</td>
											<!--td width="48" align="right">
											
										</td>
										<td width="45">
											
										</td-->
										</tr>
										<xsl:for-each select="CAMPO">
											<tr>
												<xsl:if test="position() mod 2=1">
													<xsl:attribute name="class">filaLlena</xsl:attribute>
												</xsl:if>
												<xsl:if test="position() mod 2=0">
													<xsl:attribute name="class">filaVacia</xsl:attribute>
												</xsl:if>
												<!--xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute-->
												<td colspan="8">
													<xsl:attribute name="class">filaVacia</xsl:attribute>
													<table width="575" border="0" style="display:inline;" cellspacing="2">
														<tr>
															<xsl:if test="position() mod 2=1">
																<xsl:attribute name="class">filaLlena</xsl:attribute>
															</xsl:if>
															<xsl:if test="position() mod 2=0">
																<xsl:attribute name="class">filaVacia</xsl:attribute>
															</xsl:if>
															<td width="25" height="25"/>
															<td style="width:200">
																<div style="margin-left:20">
																	<xsl:value-of select="@CAMPO"/>
																</div>
															</td>
															<td align="center" style="width:74;text-align:center">
																<xsl:value-of select="@DESCTIPCAMB"/>
															</td>
															<td align="right" style="width:157">
																<xsl:value-of select="@VINICIAL"/>
															</td>
															<td style="width:155">
																<xsl:value-of select="@VNUEVO"/>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</xsl:for-each>
										<!-- FIN DE PR -->
									</table>
								</td>
							</tr>
						</xsl:for-each>
					</xsl:if>
				</table>
				<!--table width="575" border="0">
					<tr>
					<xsl:attribute name="class"><xsl:value-of select="user:TRStyle(1)"/></xsl:attribute>
						<td colspan="2"/>
						<td/>
						<td colspan="2"/>
					</tr>
				</table-->
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="count(//SOLAPA) = 0">
			<br/>
			<table width="100%" border="0">
				<tr>
					<td align="center" class="textonormal">
						<xsl:if test="/Response/Estado/@resultado = 'false'">
							<b>
								<xsl:value-of select="/Response/Estado/@mensaje"/>
							</b>
						</xsl:if>
						<xsl:if test="/Response/Estado/@resultado = 'true'">
							<b>No se encontraron datos para los valores seleccionados</b>
						</xsl:if>
					</td>
				</tr>
			</table>
			<br/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
