<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: datosRiesgosSolicitud.xsl

Fecha de Creación: 25/11/2010

PPCR: 50055/6010661

Desarrollador:  Gabriel D'Agnone

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:decimal-format name="coma" decimal-separator="," grouping-separator="."/>
	
	<xsl:template match="/">
	
<!--div  id="seccion_CotizacionCorrespondiente">

 
	
</div-->

	
		<xsl:for-each select="//ITEM">
		
		
		
			<div class="areaDatTit">
				<div class="areaDatTitTex">Datos Edificio <xsl:value-of select="ITEMDESC"/></div>
		   </div>	
		   <div >
            <div class="filaLlena"><b>Domicilio de Riesgo:</b></div>
        </div>	
	
	 <div class="areaDatCpoConInp" style="background-color: #FFFFFF;">
	  <label style="color:blue"><xsl:attribute name="id"><xsl:value-of select="concat('calleRiesgoItem_',position(),'_etiq')"/></xsl:attribute>	 	  
	  Calle:
		  
	  </label>
	  
       <input class="areaDatCpoConInpFieM" name="calleRiesgo" type="text"  onkeypress="capturaTecla()" validarTipeo="texto"  maxlength="50" onpaste="return false" oncontextmenu="return false">
			  <xsl:attribute name="id"><xsl:value-of select="concat('calleRiesgoItem_',position())"/></xsl:attribute>
			  <xsl:attribute name="name"><xsl:value-of select="concat('calleRiesgoItem_',position())"/></xsl:attribute>
			    <xsl:attribute name="value"><xsl:value-of select="DOMICDOM"/></xsl:attribute>
		</input>
	
	<label style="color:blue">  <xsl:attribute name="id"><xsl:value-of select="concat('nroCalleRiesgoItem_',position(),'_etiq')"/></xsl:attribute>
	Nro:
	
	</label>
	
       <input class="areaDatCpoConInpFieM"   type="text"  onkeypress="capturaTecla()" validarTipeo="numero"  maxlength="6" onpaste="return false" oncontextmenu="return false">
			 <xsl:attribute name="id"><xsl:value-of select="concat('nroCalleRiesgoItem_',position())"/></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="concat('nroCalleRiesgoItem_',position())"/></xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="DOMICDNU"/></xsl:attribute>
	</input>
	</div>
	
	
	 <div class="areaDatCpoConInp" style="background-color: #FFFFFF;">
	  <label ><xsl:attribute name="id"><xsl:value-of select="concat('pisoRiesgoItem_',position(),'_etiq')"/></xsl:attribute>	 	  
	  Piso:		  
	  </label>	  
       <input class="areaDatCpoConInpFieM" name="pisoRiesgo" type="text"  onkeypress="capturaTecla()" validarTipeo="texto"  maxlength="4" onpaste="return false" oncontextmenu="return false">
			  <xsl:attribute name="id"><xsl:value-of select="concat('pisoRiesgoItem_',position())"/></xsl:attribute>
			  <xsl:attribute name="name"><xsl:value-of select="concat('pisoRiesgoItem_',position())"/></xsl:attribute>
			    <xsl:attribute name="value"><xsl:value-of select="DOMICPIS"/></xsl:attribute>
		</input>
	
	<label >  <xsl:attribute name="id"><xsl:value-of select="concat('dptoRiesgoItem_',position(),'_etiq')"/></xsl:attribute>
	Dpto:
	
	</label>
	
       <input class="areaDatCpoConInpFieM"   type="text"  onkeypress="capturaTecla()" validarTipeo="numero"  maxlength="4" onpaste="return false" oncontextmenu="return false">
			 <xsl:attribute name="id"><xsl:value-of select="concat('dptoRiesgoItem_',position())"/></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="concat('dptoRiesgoItem_',position())"/></xsl:attribute>
			<xsl:attribute name="value"><xsl:value-of select="DOMICPTA"/></xsl:attribute>
	</input>
	</div>
	
	
	<div class="areaDatCpoConInp" style="background-color: #EDEDF1;">
	<label > <xsl:attribute name="id"><xsl:value-of select="concat('provinciaRiesgoItem_',position(),'_etiq')"/></xsl:attribute>
	Provincia:
	
	</label>	
	
       <input class="areaDatCpoConInpFieM" type="text" disabled="">
        <xsl:attribute name="id"><xsl:value-of select="concat('provinciaRiesgoItem_',position())"/></xsl:attribute>
			<xsl:attribute name="name"><xsl:value-of select="concat('provinciaRiesgoItem_',position())"/></xsl:attribute>
       <xsl:attribute name="value"><xsl:value-of select="PROVICODDES"/></xsl:attribute>
	</input>
	
</div>	
	<div  class="areaDatCpoConInp">	
	
	<label > <xsl:attribute name="id"><xsl:value-of select="concat('localidadRiesgoItem_',position(),'_etiq')"/></xsl:attribute>
	Localidad:
			
	</label>
       <input class="areaDatCpoConInpFieM"  type="text" style="width:300px; margin-right: 0px;" disabled="">
		     <xsl:attribute name="id"><xsl:value-of select="concat('localidadRiesgoItem_',position())"/></xsl:attribute>
			 <xsl:attribute name="name"><xsl:value-of select="concat('localidadRiesgoItem_',position())"/></xsl:attribute>
		     <xsl:attribute name="value"><xsl:value-of select="DOMICPOB"/></xsl:attribute>      
	</input>
	
	 <label  style="width:100px; padding-left:15px; padding-right:0px; margin-right: 0px; color:blue">	 
	  <xsl:attribute name="id"><xsl:value-of select="concat('codPostalRiesgoItem_',position(),'_etiq')"/></xsl:attribute>
	 Cod Pos:
	 </label>
    <input class="areaDatCpoConInpFie"  style="width:45px; padding-left:5px; padding-right:0px; margin-right: 0px;" disabled="disabled" type="text" >    				    
     <xsl:attribute name="id"><xsl:value-of select="concat('codPostalRiesgoItem_',position())"/></xsl:attribute>
	 <xsl:attribute name="name"><xsl:value-of select="concat('codPostalRiesgoItem_',position())"/></xsl:attribute>
	 <xsl:attribute name="value"><xsl:value-of select="CPOSDOM"/></xsl:attribute>      
	</input>
	</div>
		   
		   <!--div class="areaDatCpoConInp">
		   <table style="width:100%">
						<tbody>
						<th style="font-size:10; text-align:center" class="areaDatTitTex">Cobertura</th>
						<th style="font-size:10;  text-align:center" class="areaDatTitTex">Suma Asegurada</th>
						<th style="font-size:10;  text-align:center" class="areaDatTitTex">Tasa</th>
						<th style="font-size:10;  text-align:center" class="areaDatTitTex">Prima Comisionable</th>
						<xsl:for-each select="COBERTURAS/COBERTURA">
						<xsl:if test="position() mod 2=1">
							<tr class="filaLlena">
								<td style="width:40%"><xsl:value-of select="COBERDES"/></td>
								<td style="width:20%; text-align:right"><xsl:value-of select="format-number(SUMAASEG,'###.##0,00', 'coma')"/></td>
								<td style="width:20%; text-align:right"><xsl:value-of select="TASA"/></td>
								<td style="width:20%; text-align:right"><xsl:value-of select="PRIMA"/></td>
							</tr>
							</xsl:if>
							<xsl:if test="position() mod 2!=1">
							<tr class="filaVacia">
								<td style="width:40%"><xsl:value-of select="COBERDES"/></td>
								<td style="width:20%; text-align:right"><xsl:value-of select="format-number(SUMAASEG,'###.##0,00', 'coma')"/></td>
								<td style="width:20%; text-align:right"><xsl:value-of select="TASA"/></td>
								<td style="width:20%; text-align:right"><xsl:value-of select="PRIMA"/></td>
							</tr>
							</xsl:if>
							</xsl:for-each>
						</tbody>
					</table>
		   </div-->
			   <p ></p>
		</xsl:for-each>
		
		<!--div class="areaDatTit">
            <div class="areaDatTitTex">Totales:</div>
        </div>	
	 <div class="areaDatCpoConInp">
		   <table style="width:99%">
						<tbody>
					
							<tr class="filaLlena">
								<td style="width:80%">PRIMA COMISIONABLE</td>
								<td style="width:20%; text-align:right"><xsl:value-of select="//DATOS_COTIZACION/TOTAL_PRIMA_COMISIONABLE"/></td>
								
							</tr>
							<tr class="filaVacia">
								<td style="width:80%">PRECIO</td>
								<td style="width:20%;  text-align:right"><xsl:value-of select="//DATOS_COTIZACION/TOTAL_PRECIO"/></td>
								
							</tr>
							
						</tbody>
					</table>
		   </div-->
		   
		   	<!--div class="areaDatTit">
				<div class="areaDatTitTex">Otros datos de la cotización:</div>
			</div>	
		   
		   <div class="areaDatCpoConInp">
		   
		     <table style="width:100%">
						<tbody>					
								<tr class="filaVacia">
									<td style="width:10%">Fecha de Inicio:</td>									
									<td style="width:30%; text-align:left"><xsl:value-of select="//DATOS_COTIZACION/FECHA_INICIO"/></td>	
									<td style="width:10%">Fecha de Fin:</td>									
									<td style="width:20%; text-align:left"><xsl:value-of select="//DATOS_COTIZACION/FECHA_FIN"/></td>	
									   
							  </tr>
							  <tr class="filaLlena">
									<td style="width:10%">Estabilización:</td>									
									<td style="width:30%; text-align:left"><xsl:value-of select="//DATOS_COTIZACION/ESTABILIZACION"/></td>	
									<td style="width:10%">Comisión Total:</td>									
									<td style="width:20%; text-align:left"><xsl:value-of select="//DATOS_COTIZACION/COMISION_TOTAL"/></td>	
									   
							  </tr>	
							  <tr class="filaVacia">
									<td style="width:10%">Forma de Pago:</td>									
									<td style="width:30%; text-align:left"><xsl:value-of select="//DATOS_COTIZACION/FORMA_PAGO"/></td>	
									<td style="width:10%">Plan de Pago:</td>									
									<td style="width:20%; text-align:left"><xsl:value-of select="//DATOS_COTIZACION/PLAN_PAGO"/></td>	
									   
							  </tr>								
						</tbody>
			</table>
		    
		  
		   
		   </div-->
		   <p style="height:5"></p>
		   
	
	</xsl:template>
</xsl:stylesheet>
