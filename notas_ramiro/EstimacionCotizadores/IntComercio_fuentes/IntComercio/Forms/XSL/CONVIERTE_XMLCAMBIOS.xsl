<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2011. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: CONVIERTE_XMLCAMBIOS.xsl

Fecha de Creaci�n: 11/02/2011

PPCR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripci�n: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
		
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:key name="SOLAPA" match="CAMBIO" use="SOLAPA"/>
	<xsl:key name="IDCAMPO" match="CAMBIO" use="IDCAMPO"/>
	<xsl:key name="ITEM" match="//CAMBIOS/CAMBIO" use="ITEM"/>
	<xsl:template match="/">
		<REG>
			<xsl:for-each select="//CAMBIOS/CAMBIO[count(. | key('SOLAPA', SOLAPA)[1]) = 1]">
			<xsl:sort select="NROSOLAPA"  data-type="number" />
				<SOLAPA>
					<xsl:attribute name="SOLAPA"><xsl:value-of select="SOLAPA"/></xsl:attribute>
					
					<xsl:for-each select="key('SOLAPA', SOLAPA)">
						<xsl:for-each select="(current())[count(. | key('ITEM', ITEM)[1]) = 1]">
						
							<ITEM>
								<xsl:attribute name="ITEM"><xsl:value-of select="ITEM"/></xsl:attribute>
								<xsl:attribute name="SOLAPA"><xsl:value-of select="SOLAPA"/></xsl:attribute>
								
									<xsl:for-each select="key('ITEM',ITEM)">
									<!--xsl:for-each select="(current())[count(. | key('IDCAMPO', IDCAMPO)[1]) = 1]"-->								
								
									<CAMPO>
										<xsl:attribute name="ITEM"><xsl:value-of select="ITEM"/></xsl:attribute>
										<xsl:attribute name="SOLAPA"><xsl:value-of select="SOLAPA"/></xsl:attribute>
										<xsl:attribute name="TIPOCAMBIO"><xsl:value-of select="TIPOCAMBIO"/></xsl:attribute>
										<!--GED 07-02-2011 DEFECT 256-->
										<xsl:attribute name="DESCTIPCAMB"><xsl:value-of select="DESCTIPCAMB"/></xsl:attribute>
										<xsl:attribute name="CAMPO"><xsl:value-of select="CAMPO"/></xsl:attribute>
										<xsl:attribute name="VNUEVO"><xsl:value-of select="VNUEVO"/></xsl:attribute>
										<xsl:attribute name="VINICIAL"><xsl:value-of select="VINICIAL"/></xsl:attribute>
									</CAMPO>
								
								
									<!--/xsl:for-each-->                
									</xsl:for-each>
							</ITEM>
						</xsl:for-each>
					</xsl:for-each>
				</SOLAPA>
			</xsl:for-each>
		</REG>
	</xsl:template>
</xsl:stylesheet>
