<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: Form_Alert.xsl

Fecha de Creación: 15/07/2010

PPCR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
		<xsl:if test="count(//ERROR) != 0">
			<img name="titulo" width="7" height="15" align="absmiddle" src=""/>
			<input type="hidden" name="alertText"><xsl:attribute name="value"><xsl:for-each select="//ERROR">#TAB# <xsl:value-of select="LABELCAMPO"/>: <xsl:value-of select="LEYENDAERROR"/> #ENTER#</xsl:for-each></xsl:attribute></input>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
