<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
'___________________________________________________________
' Fecha de Modificación: 10/05/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripción: DEFECT 373
__________________________________________________________
Fecha de Modificación: 12/04/2011
PPCR: 50055/6010661
Desarrolador: Gabriel D'Agnone
Descripción: Se agregan funcionalidades para Renovacion(Etapa 2).-
______________________________________________________________

COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: resultadoCotizacion.xsl

Fecha de Creacion: 25/11/2010

PPCR: 50055/6010661

Desarrollador:  Gabriel D'Agnone

Descripcion: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:decimal-format name="coma" decimal-separator="," grouping-separator="."/>
	<xsl:param name="moneda"/>
	<xsl:param name="SWPRIMIN"/>

	<xsl:template match="/">
		<xsl:for-each select="//ITEMS/ITEM">
			<div class="areaDatTit">
				<div class="areaDatTitTex">Cotización <xsl:value-of select="concat(ITEMDESC,' ')"/>
					<xsl:if test="CERTISECANT!='' ">
						<xsl:value-of select="CERTIPOLANT"/>-<xsl:value-of select="CERTIANNANT"/>-<xsl:value-of select="CERTISECANT"/>
					</xsl:if>
				</div>
			</div>
			<div class="areaDatCpoConInp">
				<table style="width:100%">
					<tbody>
						<th style="font-size:10; text-align:left">Cobertura</th>
						<xsl:if test="ESTADO!='FN' and count(./COBERTURAS/COBERTURA[SWTASAVM= 'F'])=0 ">
							<th style="font-size:10;  text-align:left">Suma Asegurada</th>
							<th style="font-size:10;  text-align:left">Tasa</th>
							<th style="font-size:10;  text-align:left">Prima Comisionable</th>
							<xsl:for-each select="./COBERTURAS/COBERTURA">
								<tr>
									<!-- COLOR FILA -->
									<xsl:if test="position() mod 2=1">
										<xsl:attribute name="class">filaLlena</xsl:attribute>
									</xsl:if>
									<xsl:if test="position() mod 2!=1">
										<xsl:attribute name="class">filaVacia</xsl:attribute>
									</xsl:if>
									<!-- DATOS FILA -->
									<td style="width:40%">
										<xsl:value-of select="COBERDES"/>
									</td>
									<td style="width:20%">
										<xsl:value-of select="concat($moneda,' ', format-number(SUMAASEG,'#.##0,00','coma') )"/>
									</td>
									<td style="width:20%">
										<xsl:value-of select="format-number(TASA, '##0,00####','coma' ) "/>
									</td>
									<td style="width:20%">
										<xsl:value-of select="concat($moneda,' ',  format-number(PRIMA,'#.##0,00','coma') )"/><xsl:if test="$SWPRIMIN ='S' ">
										<sup>(1)</sup>
									</xsl:if>

									</td>
								</tr>
							</xsl:for-each>
						</xsl:if>
						<xsl:if test="ESTADO='FN' or  count(./COBERTURAS/COBERTURA[SWTASAVM= 'F'])>0  ">
							<th style="font-size:10;  text-align:left">Suma Asegurada</th>
							<xsl:for-each select="./COBERTURAS/COBERTURA">
								<tr>
									<!-- COLOR FILA -->
									<xsl:if test="position() mod 2=1">
										<xsl:attribute name="class">filaLlena</xsl:attribute>
									</xsl:if>
									<xsl:if test="position() mod 2!=1">
										<xsl:attribute name="class">filaVacia</xsl:attribute>
									</xsl:if>
									<!-- DATOS FILA -->
									<td style="width:40%">
										<xsl:value-of select="COBERDES"/>
									</td>
									<td colspan="1" style="width:60%" align="right">
										<xsl:value-of select="concat($moneda,' ', format-number(SUMAASEG,'#.##0,00','coma') )"/>
									</td>
								</tr>
							</xsl:for-each>
						</xsl:if>
					</tbody>
				</table>
				<xsl:if test="ESTADO='FN'">
					<table style="width:100%">
						<tbody>
							<th style="font-size:10; text-align:left">El riesgo ingresado posee parámetros fuera de manual y será enviado a la compañía para su cotización. </th>
						</tbody>
					</table>
				</xsl:if>
			</div>
		</xsl:for-each>
		<p style="height:10"/>
	</xsl:template>
</xsl:stylesheet>
