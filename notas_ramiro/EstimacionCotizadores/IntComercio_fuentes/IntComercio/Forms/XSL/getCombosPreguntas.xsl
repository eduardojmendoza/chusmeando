<?xml version="1.0" encoding="UTF-8"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: getCombosPreguntas.xsl

Fecha de Creación: 28/01/2010

Desarrollador: Gabriel D'Agnone

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
		
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">	
	
<div class="areaDatCpoCon">
	<!-- DUMMY PARA QUE SIEMPRE TENGA 1 ELEMENTO -->
	<xsl:if test="count(//pregunta) != 0">
		<div id="DIVPREGUNTA-0" style="display:none" class="areaDatCpoConInp">
			<select name="PREGUNTA" id="PREGUNTA-0"></select>
		</div>
	</xsl:if>

	<xsl:for-each select="//pregunta">			
		<xsl:sort select="//pregunta/@OrdenEnPantalla" ></xsl:sort>
		
		<div class="areaDatCpoConInp"  style="width:100%;height:25px; vertical-align: middle; display:none">    
			<xsl:attribute name="id"><xsl:value-of select="concat('DIVPREGUNTA','-',position())"/></xsl:attribute>		
			<!-- LEYENDA -->
			<label style="width:305; font-size:11px; font-family: Verdana;  vertical-align: middle;"><xsl:value-of select="leyendas/leyenda/@Descripcion"/></label>
			<!-- PREGUNTA -->
			<select class="areaDatCpoConInpFieM" >
				<xsl:attribute name="name">PREGUNTA</xsl:attribute>
				<xsl:attribute name="id"><xsl:value-of select="concat('PREGUNTA','-',position())"/></xsl:attribute>				
				<xsl:attribute name="coberturaActivante"><xsl:value-of select="coberturasactivantes/cobertura"/></xsl:attribute>
				<xsl:attribute name="codigoAIS"><xsl:value-of select="@Codigo"/></xsl:attribute>
				<xsl:for-each select="valoresposibles/OPTION">						
					<option>
						<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
						<xsl:value-of select="."/>
					</option>
				</xsl:for-each>
			</select>
		</div>
	</xsl:for-each>
</div>

</xsl:template>
</xsl:stylesheet>
