<?xml version="1.0" encoding="UTF-8"?>
<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2012. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: getComboPlanesICOBB.xsl

Fecha de Creación: 28/02/2012

Desarrollador: Martín Cabrera.

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
		
<xsl:value-of select="IDPLANES"/>
|<xsl:value-of select="IDFOLLETO"/>
|<xsl:value-of select="PRIMA_COMIS_ICOBB"/>
|<xsl:value-of select="PRECIO_PLAN_ICOBB"/>
|<xsl:value-of select="IMPUESTOS_PLAN_ICOBB"/>
|<xsl:value-of select="IMPUESTOS_ADMIN_ICOBB"/>
|<xsl:value-of select="GASTOS_COMER_ICOBB"/>
|<xsl:value-of select="PORC_COMER_ICOBB"/>
|<xsl:value-of select="COMIS_OR_ICOBB"/>
|<xsl:value-of select="COMIS_PR_ICOBB"/>

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
	
	
		<options>
		<xsl:if test="count(//PLAN)=0">
		<option>
				<xsl:attribute name="value">0</xsl:attribute>
					NO SE ENCONTRARON PLANES
						</option>
		</xsl:if>
		<xsl:if test="count(//PLAN)=1">
			<xsl:for-each select="//PLAN">
				<option>
					<xsl:attribute name="value"><xsl:value-of select="IDPLANES"/>|<xsl:value-of select="IDFOLLETO"/>|<xsl:value-of select="PRIMA_COMIS_ICOBB"/>|<xsl:value-of select="PRECIO_PLAN_ICOBB"/>|<xsl:value-of select="IMPUESTOS_PLAN_ICOBB"/>|<xsl:value-of select="GASTOS_ADMIN_ICOBB"/>|<xsl:value-of select="GASTOS_COMER_ICOBB"/>|<xsl:value-of select="PORC_COMER_ICOBB"/>|<xsl:value-of select="COMIS_OR_ICOBB"/>|<xsl:value-of select="COMIS_PR_ICOBB"/></xsl:attribute>
					<xsl:value-of select="NOMBRE_PLAN_ICOBB"/>
				</option>
			</xsl:for-each>
		
		</xsl:if>
		
		<xsl:if test="count(//PLAN)>1">
			<option>
				<xsl:attribute name="value">-1</xsl:attribute>
					Seleccionar...
						</option>
			<xsl:for-each select="//PLAN">
				<option>
					<xsl:attribute name="value"><xsl:value-of select="IDPLANES"/>|<xsl:value-of select="IDFOLLETO"/>|<xsl:value-of select="PRIMA_COMIS_ICOBB"/>|<xsl:value-of select="PRECIO_PLAN_ICOBB"/>|<xsl:value-of select="IMPUESTOS_PLAN_ICOBB"/>|<xsl:value-of select="GASTOS_ADMIN_ICOBB"/>|<xsl:value-of select="GASTOS_COMER_ICOBB"/>|<xsl:value-of select="PORC_COMER_ICOBB"/>|<xsl:value-of select="COMIS_OR_ICOBB"/>|<xsl:value-of select="COMIS_PR_ICOBB"/></xsl:attribute>
					<xsl:value-of select="NOMBRE_PLAN_ICOBB"/>
				</option>
			</xsl:for-each>
			
			</xsl:if>
		</options>
	</xsl:template>
</xsl:stylesheet>
