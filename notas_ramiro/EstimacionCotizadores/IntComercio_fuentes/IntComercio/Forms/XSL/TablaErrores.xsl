<!-- 
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: TablaErrores.xsl

Fecha de Creación: 15/07/2010

PPCR: 50055/6010661

Desarrollador: Marcelo Gasparini

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -    
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<!-- DEFINICION DE FUNCIONES -->
	<msxsl:script language="JScript" implements-prefix="user"><![CDATA[
	/* EN ESTA FUNCION SE CREA EL LINK PARA EL HREF */
	var g_pos = 0;
	
function trStyle(pvarInc)
{
	if (pvarInc)
		g_pos++;
	if (g_pos % 2 == 0)
		return 'Form_campname_colorL';
	else
		return 'Form_campname_bcoL';
}
	]]><!-- FIN DEFINICION DE FUNCIONES -->
	</msxsl:script>
	<xsl:template match="/">
		<xsl:if test="count(//ERROR[TIPOERROR='E'] ) != 0">
			<table width="98%" cellpadding="0" cellspacing="0" border="0" id="tblListaErrores" style="padding-left: 15px">
				<tr>
					<td colspan="5">
						<table width="670" border="1" cellpadding="0" cellspacing="0" class="Table3">
							<tr>
								<td bordercolor="#FFFFFF" style="border-right-width:0; color:'#FFFFFF'">
									<img name="titulo" width="7" height="15" align="absmiddle" style="color:'#FFFFFF'" src=""/> Errores de datos - Deberá completar o corregir los siguientes campos:</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="670" border="1" cellpadding="0" cellspacing="0" Class="Table1">
							<tr bordercolor="#FFFFFF">
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="100%">
												<table border="0" width="100%" cellspacing="0" bordercolor="black" style="table-layout: fixed;" id="filasErrores">
													<colgroup>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
														<col width="10%" align="left"/>
													</colgroup>
													<tbody>
														<xsl:variable name="solapa" select="0"/>
														<xsl:for-each select="//SOLAPA">
															<xsl:sort select="@nro" order="ascending" data-type="number"/>
															<tr class="Form_campname_subEncab">
																<td colspan="10">
																	<b>Solapa <xsl:value-of select="@nom"/>
																	</b>
																</td>
															</tr>
															<xsl:for-each select="ERROR[TIPOERROR='E']">
																<tr style="cursor:hand">
																	<xsl:attribute name="class"><xsl:value-of select="user:trStyle(1)"/></xsl:attribute> 
																	<xsl:attribute name="onclick">Javascript:irALabel('<xsl:value-of select="parent::SOLAPA/@nro"/>','<xsl:value-of select="CAMPO"/>')</xsl:attribute>
																	
																	<td colspan="3">
																		<xsl:value-of select="LABELCAMPO"/>
																	</td>
																	<td colspan="7">
																		<xsl:value-of select="LEYENDAERROR"/>
																	</td>
																</tr>
															</xsl:for-each>
														</xsl:for-each>
													</tbody>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br/>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
