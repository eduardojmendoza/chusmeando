<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: Form_Otros.xsl

Fecha de Creación: 15/07/2010

PPCR: 50055/6010661

Desarrollador: Leonardo Ruiz

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos 
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:user-namespace-here" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:decimal-format name="coma" decimal-separator="," grouping-separator="."/>
	<xsl:param name="urlCanal"></xsl:param>
	<xsl:param name="tipoOperacion"></xsl:param>
	<xsl:param name="sumaAsegurada"></xsl:param>
		<xsl:param name="moneda"></xsl:param>
	
	<!-- DEFINICION DE FUNCIONES -->
	<msxsl:script language="JScript" implements-prefix="user"><![CDATA[
/* EN ESTA FUNCION SE CREA EL LINK PARA EL HREF */
var g_pos = 0;

function trStyle(pvarInc)
{
	if (pvarInc)
		g_pos++;
	if (g_pos % 2 == 0)
		return 'form_campname_colorR';
	else
		return 'form_campname_bcoR';
}
	
function tdStyle()
{
	if (g_pos % 2 == 0)
		return '#FFFFFF';
	else
		return '#EEEEEE';
}

]]><!-- FIN DEFINICION DE FUNCIONES -->
	</msxsl:script>
	
	<xsl:template match="/">
	
	
		<xsl:if test="count(//ITEM) != 0">
			<table width="100%" border="1" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td bordercolor="#dedfe7">
						<table width="100%" border="0" cellspacing="0">
							<tr class="Form_campname_subEncab">
								<td align="left" >Item</td>
                <td align="center" >Ubicación Comercio</td>
								<td align="center" ><xsl:text>Suma Asegurada </xsl:text></td>
								<td align="center" ><xsl:if test="$tipoOperacion = 'R' "><xsl:text >Certificado</xsl:text></xsl:if ></td>
								<td align="center" colspan="3"/>
							</tr>
							<xsl:for-each select="//ITEMS/ITEM">
							<xsl:variable name="IDEN" select="@ID"/>
								<xsl:if test="(ITEMDESC != '')">
									<tr>
										<xsl:attribute name="class"><xsl:value-of select="user:trStyle(1)"/></xsl:attribute>
										<td align="left" width="5%">
											<span class="mdtxt" style="text-transform: uppercase;">
												<xsl:value-of select="@ID"/>
											</span>
										</td>
										<td align="left" width="45%">
											<span class="mdtxt" style="text-transform: uppercase;">
												<xsl:value-of select="ITEMDESC"/>
											</span>
										</td>
										<td align="center" width="30%">
											<span class="mdtxt" style="text-transform: uppercase; text-align:center">
												<xsl:value-of select="concat($moneda,' ', format-number(sum(//ITEM[ID=$IDEN]/COBERTURAS/COBERTURA/SUMAASEG),'###.##0,00', 'coma'))"/>
												
											</span>
										</td>
										
										<td align="left" width="25%">
										<xsl:if test="$tipoOperacion = 'R' ">
											<span class="mdtxt" style="text-transform: uppercase;">
													<xsl:value-of select="CERTIPOLANT"/>-<xsl:value-of select="CERTIANNANT"/>-<xsl:value-of select="CERTISECANT"/>
											</span>
											</xsl:if>
										</td>
										
										<td align="center" width="5%">
											<span>
												<img border="0" name="btnModificar" alt="Modificar" style="cursor:hand" >
												<xsl:attribute name="src"><xsl:value-of select="concat( $urlCanal,'\Images\btn_form.gif')"></xsl:value-of></xsl:attribute>
													<xsl:attribute name="onclick">Javascript:confirm_editarItemOT('<xsl:value-of select="@ID"/>', '<xsl:value-of select="$urlCanal" />')</xsl:attribute>
												</img>
											</span>
										</td>
										<td align="center" width="5%">
											<span>
												<img border="0" name="btnEliminar" alt="Eliminar" style="cursor:hand" >
												<xsl:attribute name="src"><xsl:value-of select="concat( $urlCanal,'\Images\boton_cancel.gif')"></xsl:value-of></xsl:attribute>
												
													<xsl:attribute name="onclick">Javascript:confirm_quitarItemXML('<xsl:value-of select="@ID"/>', '<xsl:value-of select="$urlCanal" />')</xsl:attribute>
												</img>
											</span>
										</td>
									</tr>
								</xsl:if>
							</xsl:for-each>
						</table>
					</td>
				</tr>
			</table>
		</xsl:if>
		<xsl:if test="count(//ITEM) = 0">
			<table width="100%" border="1" cellpadding="0" cellspacing="1" bordercolor="#FFFFFF">
				<tr>
					<td bordercolor="#dedfe7">
						<table width="100%" border="0" cellspacing="0">
							<tr>
								<xsl:attribute name="class"><xsl:value-of select="user:trStyle(1)"/></xsl:attribute>
								<td align="center" height="15px">No hay Items en la lista</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
