<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 12/04/2011
' PPCR: 50055/6010661
' Desarrolador: Gabriel D'Agnone
' Descripci�n: Se agregan funcionalidades para Renovacion(Etapa 2).-
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: CotiIntComercio_DatosGenerales.asp
' 
' Fecha de Creaci�n: 08/06/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Captura de datos relacionados al Cliente.
'			   
' 
' ------------------------------------------------------------------------------

 
   ' mvarCANALURLASP= left( replace(mvarCANALURL,"/","\"),len(mvarCANALURL)-1)
 
function FormatearFechaGenerica(mvarFecha)

	FormatearFechaGenerica="0"
	
	if (len(mvarFecha)>= 8 ) or ( len(mvarFecha) <=10) then
		pos1=Instr(1,mvarFecha, "/")
		pos2=Instr(3,mvarFecha, "/")
		if (pos1<> 0) and (pos2<>0) then 
			
			mvarArr=split(mvarFecha,"/")
		
		FormatearFechaGenerica=mvarArr(2) & right("0" & mvarArr(1),2) & right("0" & mvarArr(0),2)
	end if
		
	end if
	
	
end Function 

'30/08/2010 LR Se Trae esta funcion q devuelve el nombre del mes al core.-
Function MesAnn(pMes)	
    Select Case pMes
        Case "1"    MesAnn = "Enero"
        Case "2"    MesAnn = "Febrero"
        Case "3"    MesAnn = "Marzo"
        Case "4"    MesAnn = "Abril"
        Case "5"    MesAnn = "Mayo"
        Case "6"    MesAnn = "Junio"
        Case "7"    MesAnn = "Julio"
        Case "8"    MesAnn = "Agosto"
        Case "9"    MesAnn = "Septiembre"
        Case "10"   MesAnn = "Octubre"
        Case "11"   MesAnn = "Noviembre"
        Case "12"   MesAnn = "Diciembre"
        Case Else   MesAnn = "No Definido"
    End Select
End Function
%>

<div  id="seccion_Convenio">

        <div class="areaDatTit" >
            <div class="areaDatTitTex">Opciones del Productor</div>
        </div>
    
        <div class="areaDatCpoConInp" >
           <!-- PRODUCTOR -->
           <label id="convenioProductor_etiq" for="convenioProductor">Convenio Productor:</label>
            <select style="width:400" class="areaDatCpoConInpFieM" id="convenioProductor" name="convenioProductor" onchange="fncConvenioProdSeleccionado(); fncCargaComboComision(); fncCalculaComisiones();fncCambiarColorCpoRen(this);fncRenovConvComisFN();"></select>
           
           </div> 
          <div class="areaDatCpoConInp" >           
            <!-- ORGANIZADOR -->
            <label id="convenioOrganizador_etiq" for="convenioOrganizador">Convenio Organizador:</label>
            <select style="width:400" class="areaDatCpoConInpFieM" id="convenioOrganizador" name="convenioOrganizador"  disabled onchange="fncCargaComboComision(); fncCalculaComisiones(); fncCambiarColorCpoRen(this);fncRenovConvComisFN();">
             <option value="0">SELECCIONAR...</option>  
            </select>
                
        </div>
       
        
       
</div>  

<div  id="seccion_DatosGenerales">

        <div class="areaDatTit">
            <div class="areaDatTitTex">Datos Generales</div>
        </div>
    
        <div class="areaDatCpoConInp">
           <!-- PERIODO -->
           <label id="periodoCotizar_etiq" for="periodoCotizar">Per&iacute;odo a cotizar:</label>
            <select class="areaDatCpoConInpFieM" id="periodoCotizar" name="periodoCotizar" onchange="fncPeriodoSeleccionado(); fncActivaRecotizar(); fncCambiarColorCpoRen(this)"></select>
                    
            <!-- ESTABILIZACION -->
            <label id="estabilizacion_etiq" for="estabilizacion">Estabilizaci&oacute;n:</label>
            <select class="areaDatCpoConInpFieM" id="estabilizacion" name="estabilizacion" onchange=" fncActivaRecotizar(); fncCambiarColorCpoRen(this)"></select>
                  
        </div>
        <div class="areaDatCpoConInp">
       
            <label id="IVA_etiq" for="IVA">Situaci�n I.V.A.:</label>
            <select class="areaDatCpoConInpFieM" id="IVA" name="IVA" onchange="fncCuitSeleccionado(this);  fncActivaRecotizar(); fncCambiarColorCpoRen(this)"></select>
            
            <span id="spanTipoPersona" style="display:none">
               <label id="tipoPersona_etiq" for="tipoPersona">Tipo de Persona:</label>
               <select class="areaDatCpoConInpFieM" id="tipoPersona" name="tipoPersona" style="display:none"  onchange="fncTipoPersonaSel();  fncActivaRecotizar(); fncCambiarColorCpoRen(this)" disabled="">
                      <option value="-1">SELECCIONAR...</option>
                      <option selected value="00">FISICA</option>
                      <option value="15">JURIDICA</option>
               </select>    
          </span>         
           
        </div >
         <div class="areaDatCpoConInp" >        
            
            <span id="spanIngresosBrutos" style="display:none">
                <label id="IngresosBrutos_etiq" for="IngresosBrutos">Ingresos Brutos:</label>
                <select class="areaDatCpoConInpFie240" id="IngresosBrutos" name="IngresosBrutos" disabled="" onchange="fncActivaRecotizar(); fncCambiarColorCpoRen(this)"></select>
           </span>
         </div>
           


</div>        
<div id="seccion_Actividad">


 <div class="areaDatTit">
            <div class="areaDatTitTex">Actividad Comercial</div>
 </div>

  <div class="areaDatCpoConInp" >
         
        <label id="filtroActividadComercio_etiq" for="filtroActividadComercio" >Filtro (* = todas):</label>
		<input type="text"  class="areaDatCpoConInpFieM"  name="filtroActividadComercio" id="filtroActividadComercio" value="*" onkeypress="capturaTecla()" validarTipeo="alfanum"
		onkeydown="if(window.event.keyCode==13){ filtrarActividadCom('cboActividadComercio','filtroActividadComercio');mostrarDivCondiciones()}"
		maxlength="50" onpaste="return false;" style="TEXT-TRANSFORM: uppercase">&nbsp;

  
		<img id="btnFiltrarActividad" onclick="  filtrarActividadCom('cboActividadComercio','filtroActividadComercio'); mostrarDivCondiciones()" src="<%=mvarCANALURLASP%>/Canales/OV/Images/btn_ok.gif" border="0" Style="Cursor:hand;" alt="Filtrar">&nbsp; al menos 2 caracteres alfab�ticos 
	  	</div>
	  <div class="areaDatCpoConInp">	
		<label id="cboActividadComercio_etiq" for="" >Actividad:</label>
		<select class="areaDatCpoConInpFieL"  id="cboActividadComercio" onchange="fncCambiaActividad(); fncCambiarColorCpoRen(this)"></select>	<span id="divBtnCondiciones" style="display:none"><img   onclick="mostrarCondicionesActividad('cboActividadComercio');" src="<%=mvarCANALURLASP%>/Canales/OV/Images/boton_buscar.gif" border="0" Style="Cursor:hand;" alt="Filtrar" /> &nbsp; Ver Condiciones  	</span>
   </div>    
 
 
  
</div>    
  
<div id="seccion_OtrosDatosCotizacion">
    <div class="areaDatTit">
        <div class="areaDatTitTex">Otros Datos de la Cotizaci�n</div>
    </div>
   
   <div id="divNroPolizaRen" class="areaDatCpoConInp" style="display: none;padding-top:5">
        <!-- RENOVACION - POLIZA ORIGINAL -->
        <label id="inpNroPolizaRen_etiq" for="inpNroPolizaRen" style="font-weight:bold;">Renovaci&oacute;n P&oacute;liza:</label>
        <input class="areaDatCpoConInpFieM" id="inpNroPolizaRen" name="inpNroPolizaRen" type="text" readonly style="border:solid 0px #FFFFFF; background-color:Transparent;vertical-align:bottom;font-weight:bold; " />
    </div>
   

    <div id="divCotiFechIniFin" class="areaDatCpoConInp" style="display:none">
        <label id="vigenciaDesde_etiq" for="vigenciaDesde">Fecha de Inicio:</label>
        <input class="areaDatCpoConInpFieD" id="vigenciaDesde"  maxlength="10" name="vigenciaDesde" type="text" onkeypress="capturaTecla()"  validarTipeo="fecha" validar="&gt;= document.getElementById('fechaHoy').value" mensajeError="Fecha de Vigencia no puede ser anterior a la fecha actual"   onChange="fncValida(this); fncFechaFin('C')" onBlur="return validateDateCal(this);" value="<%=fechaHoy() %>" />
        <input class="areaDatCpoConInpFie" style="margin-right:63px" type="image" disabled onClick="popFrame.fPopCalendar(document.getElementById('vigenciaDesde'),document.getElementById('vigenciaDesde'),popCal);return false" value="V" src="<%=mvarCANALURL%>includes/calendario/calbtn.gif"></input>
        
        <label id="vigenciaHasta_etiq" for="vigenciaHasta">Fecha de Fin:</label>
        <input class="areaDatCpoConInpFieS" id="vigenciaHasta" name="vigenciaHasta" type="text" disabled="">
    </div>
      
   <div class="areaDatCpoConInp">
        <!-- Forma Pago -->
        <label id="formaPago_etiq" for="formaPago">Forma de Pago:</label>
        <select class="areaDatCpoConInpFie240" id="formaPago" name="formaPago"  onchange="formaPagoSel(); fncActivaRecotizar(); fncCambiarColorCpoRen(this)"></select>   

     </div>
    <div class="areaDatCpoConInp">
        <!-- Comisiones -->
        <label id="comisionTotal_etiq" for="comisionTotal">Comisi�n Total:</label>
		<select class="areaDatCpoConInpFieM" id="comisionTotal" name="comisionTotal" disabled="" onchange="fncCalculaComisiones(); fncActivaRecotizar(); fncCambiarColorCpoRen(this);fncRenovConvComisFN();">
            <option value="-1" >SELECCIONAR...</option>
        </select>
       
            <!-- Plan Pago -->
           <!-- GED 03-08-2010: BUG_21 ESTETICA Punto 1 -->   
        <label id="planPago_etiq" for="planPago" style="text-align: right; width: 150px ;padding-right: 50px;padding-left:0">Plan de Pago:</label>
        <select class="areaDatCpoConInpFieM" id="planPago" name="planPago" onchange="fncActivaRecotizar(); fncCambiarColorCpoRen(this)"></select>
   
    </div>
</div>