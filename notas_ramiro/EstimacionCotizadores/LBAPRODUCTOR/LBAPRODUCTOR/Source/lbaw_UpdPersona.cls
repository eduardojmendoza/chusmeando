VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "lbaw_UpdPersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'14/11/2006 - DA
'AML (Lavado de Dinero): Se agrega el campo UIFCUIT

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorLBA.lbaw_UpdPersona"
Const mcteStoreProc             As String = "SPSNCV_PROD_SIFMPERS_UPDATE"
Const mcteStoreProcDom          As String = "SPSNCV_PROD_SIFMDOMI_UPDATE"
Const mcteStoreProcCta          As String = "SPSNCV_PROD_SIFMCUEN_UPDATE"

' Parametros XML de Entrada
' DATOS GENERALES
Const mcteParam_CLIENSEC    As String = "//CLIENSEC"
Const mcteParam_NUMEDOCU    As String = "//NUMEDOCU"
Const mcteParam_TIPODOCU    As String = "//TIPODOCU"
Const mcteParam_CLIENAP1    As String = "//CLIENAP1"
Const mcteParam_CLIENAP2    As String = "//CLIENAP2"
Const mcteParam_CLIENNOM    As String = "//CLIENNOM"
Const mcteParam_NACIMANN    As String = "//NACIMANN"
Const mcteParam_NACIMMES    As String = "//NACIMMES"
Const mcteParam_NACIMDIA    As String = "//NACIMDIA"
Const mcteParam_CLIENSEX    As String = "//CLIENSEX"
Const mcteParam_CLIENEST    As String = "//CLIENEST"
Const mcteParam_PAISSCOD    As String = "//PAISSCOD"
Const mcteParam_IDIOMCOD    As String = "//IDIOMCOD"
Const mcteParam_NUMHIJOS    As String = "//NUMHIJOS"
Const mcteParam_EFECTANN    As String = "//EFECTANN"
Const mcteParam_EFECTMES    As String = "//EFECTMES"
Const mcteParam_EFECTDIA    As String = "//EFECTDIA"
Const mcteParam_CLIENTIP    As String = "//CLIENTIP"
Const mcteParam_CLIENFUM    As String = "//CLIENFUM"
Const mcteParam_CLIENDOZ    As String = "//CLIENDOZ"
Const mcteParam_ABRIDTIP    As String = "//ABRIDTIP"
Const mcteParam_ABRIDNUM    As String = "//ABRIDNUM"
Const mcteParam_CLIENORG    As String = "//CLIENORG"
Const mcteParam_PERSOTIP    As String = "//PERSOTIP"
Const mcteParam_DOMICSEC    As String = "//DOMICSEC"
Const mcteParam_CLIENCLA    As String = "//CLIENCLA"
Const mcteParam_FALLEANN    As String = "//FALLEANN"
Const mcteParam_FALLEMES    As String = "//FALLEMES"
Const mcteParam_FALLEDIA    As String = "//FALLEDIA"
Const mcteParam_FBAJAANN    As String = "//FBAJAANN"
Const mcteParam_FBAJAMES    As String = "//FBAJAMES"
Const mcteParam_FBAJADIA    As String = "//FBAJADIA"
Const mcteParam_EMAIL       As String = "//EMAIL"
Const mcteParam_UIFCUIT     As String = "//UIFCUIT"
' Domicilios
Const mcteParam_DOMICILIOS  As String = "//DOMICILIOS/DOMICILIO"
Const mcteParam_DCLIENSEC   As String = "CLIENSEC"
Const mcteParam_DDOMICSEC   As String = "DOMICSEC"
Const mcteParam_DOMICCAL    As String = "DOMICCAL"
Const mcteParam_DOMICDOM    As String = "DOMICDOM"
Const mcteParam_DOMICDNU    As String = "DOMICDNU"
Const mcteParam_DOMICESC    As String = "DOMICESC"
Const mcteParam_DOMICPIS    As String = "DOMICPIS"
Const mcteParam_DOMICPTA    As String = "DOMICPTA"
Const mcteParam_DOMICPOB    As String = "DOMICPOB"
Const mcteParam_DOMICCPO    As String = "DOMICCPO"
Const mcteParam_PROVICOD    As String = "PROVICOD"
Const mcteParam_DPAISSCOD   As String = "PAISSCOD"
Const mcteParam_TELCOD      As String = "TELCOD"
Const mcteParam_TELNRO      As String = "TELNRO"
Const mcteParam_PRINCIPAL   As String = "PRINCIPAL"
' Cuentas
Const mcteParam_CUENTAS     As String = "//CUENTAS/CUENTA"
Const mcteParam_CCLIENSEC   As String = "CLIENSEC"
Const mcteParam_CUENTSEC    As String = "CUENTSEC"
Const mcteParam_TIPOCUEN    As String = "TIPOCUEN"
Const mcteParam_COBROTIP    As String = "COBROTIP"
Const mcteParam_BANCOCOD    As String = "BANCOCOD"
Const mcteParam_SUCURCOD    As String = "SUCURCOD"
Const mcteParam_CUENTDC     As String = "CUENTDC"
Const mcteParam_CUENNUME    As String = "CUENNUME"
Const mcteParam_TARJECOD    As String = "TARJECOD"
Const mcteParam_VENCIANN    As String = "VENCIANN"
Const mcteParam_VENCIMES    As String = "VENCIMES"
Const mcteParam_VENCIDIA    As String = "VENCIDIA"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarCounter         As Integer
    Dim wvarMensaje         As String
    '
    ' DATOS GENERALES
     Dim wvarCLIENSEC   As String
     Dim wvarDCLIENSEC  As String
     Dim wvarCCLIENSEC  As String
     Dim wvarNUMEDOCU   As String
     Dim wvarTIPODOCU   As String
     Dim wvarCLIENAP1   As String
     Dim wvarCLIENAP2   As String
     Dim wvarCLIENNOM   As String
     Dim wvarNACIMANN   As String
     Dim wvarNACIMMES   As String
     Dim wvarNACIMDIA   As String
     Dim wvarCLIENSEX   As String
     Dim wvarCLIENEST   As String
     Dim wvarPAISSCOD   As String
     Dim wvarIDIOMCOD   As String
     Dim wvarNUMHIJOS   As String
     Dim wvarEFECTANN   As String
     Dim wvarEFECTMES   As String
     Dim wvarEFECTDIA   As String
     Dim wvarCLIENTIP   As String
     Dim wvarCLIENFUM   As String
     Dim wvarCLIENDOZ   As String
     Dim wvarABRIDTIP   As String
     Dim wvarABRIDNUM   As String
     Dim wvarCLIENORG   As String
     Dim wvarPERSOTIP   As String
     Dim wvarDOMICSEC   As String
     Dim wvarCLIENCLA   As String
     Dim wvarFALLEANN   As String
     Dim wvarFALLEMES   As String
     Dim wvarFALLEDIA   As String
     Dim wvarFBAJAANN   As String
     Dim wvarFBAJAMES   As String
     Dim wvarFBAJADIA   As String
     Dim wvarEMAIL      As String
     Dim wvarUIFCUIT    As String
    ' DATOS Domicilios
     Dim wvarDDOMICSEC  As String
     Dim wvarDOMICCAL   As String
     Dim wvarDOMICDOM   As String
     Dim wvarDOMICDNU   As String
     Dim wvarDOMICESC   As String
     Dim wvarDOMICPIS   As String
     Dim wvarDOMICPTA   As String
     Dim wvarDOMICPOB   As String
     Dim wvarDOMICCPO   As String
     Dim wvarPROVICOD   As String
     Dim wvarDPAISSCOD  As String
     Dim wvarTELCOD     As String
     Dim wvarTELNRO     As String
     Dim wvarPRINCIPAL  As String
    ' DATOS Cuentas
     Dim wvarCUENTSEC   As String
     Dim wvarTIPOCUEN   As String
     Dim wvarCOBROTIP   As String
     Dim wvarBANCOCOD   As String
     Dim wvarSUCURCOD   As String
     Dim wvarCUENTDC    As String
     Dim wvarCUENNUME   As String
     Dim wvarTARJECOD   As String
     Dim wvarVENCIANN   As String
     Dim wvarVENCIMES   As String
     Dim wvarVENCIDIA   As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        ' DATOS GENERALES
        wvarCLIENSEC = .selectSingleNode(mcteParam_CLIENSEC).Text
        wvarNUMEDOCU = .selectSingleNode(mcteParam_NUMEDOCU).Text
        wvarTIPODOCU = .selectSingleNode(mcteParam_TIPODOCU).Text
        wvarCLIENAP1 = Trim(Left(.selectSingleNode(mcteParam_CLIENAP1).Text & Space(20), 20))
        wvarCLIENAP2 = Trim(Left(.selectSingleNode(mcteParam_CLIENAP2).Text & Space(20), 20))
        wvarCLIENNOM = Trim(Left(.selectSingleNode(mcteParam_CLIENNOM).Text & Space(20), 20))
        wvarNACIMANN = .selectSingleNode(mcteParam_NACIMANN).Text
        wvarNACIMMES = .selectSingleNode(mcteParam_NACIMMES).Text
        wvarNACIMDIA = .selectSingleNode(mcteParam_NACIMDIA).Text
        wvarCLIENSEX = .selectSingleNode(mcteParam_CLIENSEX).Text
        wvarCLIENEST = .selectSingleNode(mcteParam_CLIENEST).Text
        wvarPAISSCOD = .selectSingleNode(mcteParam_PAISSCOD).Text
        wvarIDIOMCOD = .selectSingleNode(mcteParam_IDIOMCOD).Text
        wvarNUMHIJOS = .selectSingleNode(mcteParam_NUMHIJOS).Text
        wvarEFECTANN = .selectSingleNode(mcteParam_EFECTANN).Text
        wvarEFECTMES = .selectSingleNode(mcteParam_EFECTMES).Text
        wvarEFECTDIA = .selectSingleNode(mcteParam_EFECTDIA).Text
        wvarCLIENTIP = .selectSingleNode(mcteParam_CLIENTIP).Text
        wvarCLIENFUM = .selectSingleNode(mcteParam_CLIENFUM).Text
        wvarCLIENDOZ = .selectSingleNode(mcteParam_CLIENDOZ).Text
        wvarABRIDTIP = .selectSingleNode(mcteParam_ABRIDTIP).Text
        wvarABRIDNUM = .selectSingleNode(mcteParam_ABRIDNUM).Text
        wvarCLIENORG = .selectSingleNode(mcteParam_CLIENORG).Text
        wvarPERSOTIP = .selectSingleNode(mcteParam_PERSOTIP).Text
        wvarDOMICSEC = .selectSingleNode(mcteParam_DOMICSEC).Text
        wvarCLIENCLA = .selectSingleNode(mcteParam_CLIENCLA).Text
        wvarFALLEANN = .selectSingleNode(mcteParam_FALLEANN).Text
        wvarFALLEMES = .selectSingleNode(mcteParam_FALLEMES).Text
        wvarFALLEDIA = .selectSingleNode(mcteParam_FALLEDIA).Text
        wvarFBAJAANN = .selectSingleNode(mcteParam_FBAJAANN).Text
        wvarFBAJAMES = .selectSingleNode(mcteParam_FBAJAMES).Text
        wvarFBAJADIA = .selectSingleNode(mcteParam_FBAJADIA).Text
        wvarEMAIL = .selectSingleNode(mcteParam_EMAIL).Text
        
        If Not .selectSingleNode(mcteParam_UIFCUIT) Is Nothing Then
            wvarUIFCUIT = Trim(Left(.selectSingleNode(mcteParam_UIFCUIT).Text & Space(11), 11))
        Else
            wvarUIFCUIT = ""
        End If
        
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    ' Personas
    wvarStep = 40
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 50
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCLIENSEC)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMEDOCU", adChar, adParamInput, 11, wvarNUMEDOCU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPODOCU", adNumeric, adParamInput, , wvarTIPODOCU)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP1", adChar, adParamInput, 20, wvarCLIENAP1)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP2", adChar, adParamInput, 20, wvarCLIENAP2)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 110
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENNOM", adChar, adParamInput, 20, wvarCLIENNOM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 120
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMANN", adNumeric, adParamInput, , wvarNACIMANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 130
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMMES", adNumeric, adParamInput, , wvarNACIMMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 140
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NACIMDIA", adNumeric, adParamInput, , wvarNACIMDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 150
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEX", adChar, adParamInput, 1, wvarCLIENSEX)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 160
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENEST", adChar, adParamInput, 1, wvarCLIENEST)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 170
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PAISSCOD", adChar, adParamInput, 2, wvarPAISSCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 180
    Set wobjDBParm = wobjDBCmd.CreateParameter("@IDIOMCOD", adChar, adParamInput, 3, wvarIDIOMCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 190
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMHIJOS", adNumeric, adParamInput, , wvarNUMHIJOS)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 200
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTANN", adNumeric, adParamInput, , wvarEFECTANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 210
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTMES", adNumeric, adParamInput, , wvarEFECTMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 220
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EFECTDIA", adNumeric, adParamInput, , wvarEFECTDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 230
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENTIP", adChar, adParamInput, 2, wvarCLIENTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 240
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENFUM", adChar, adParamInput, 1, wvarCLIENFUM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 250
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENDOZ", adChar, adParamInput, 1, wvarCLIENDOZ)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 260
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ABRIDTIP", adChar, adParamInput, 2, wvarABRIDTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 270
    Set wobjDBParm = wobjDBCmd.CreateParameter("@ABRIDNUM", adChar, adParamInput, 10, wvarABRIDNUM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 280
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENORG", adChar, adParamInput, 3, wvarCLIENORG)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 290
    Set wobjDBParm = wobjDBCmd.CreateParameter("@PERSOTIP", adChar, adParamInput, 1, wvarPERSOTIP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 300
    Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICSEC", adChar, adParamInput, 3, wvarDOMICSEC)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 310
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENCLA", adChar, adParamInput, 9, wvarCLIENCLA)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 320
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEANN", adNumeric, adParamInput, , wvarFALLEANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 330
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEMES", adNumeric, adParamInput, , wvarFALLEMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 340
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FALLEDIA", adNumeric, adParamInput, , wvarFALLEDIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 350
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJAANN", adNumeric, adParamInput, , wvarFBAJAANN)
    wobjDBParm.Precision = 4
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 360
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJAMES", adNumeric, adParamInput, , wvarFBAJAMES)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 370
    Set wobjDBParm = wobjDBCmd.CreateParameter("@FBAJADIA", adNumeric, adParamInput, , wvarFBAJADIA)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    wvarStep = 380
    Set wobjDBParm = wobjDBCmd.CreateParameter("@EMAIL", adChar, adParamInput, 60, wvarEMAIL)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 381
    Set wobjDBParm = wobjDBCmd.CreateParameter("@UIFCUIT", adChar, adParamInput, 11, wvarUIFCUIT)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    
    ' Se ejecuta el stored de personas
    wvarStep = 390
    wobjDBCmd.Execute adExecuteNoRecords
    '
    wvarStep = 400
    If wobjDBCmd.Parameters("@RETURN_VALUE").Value = 0 Then
        ' Ok
        ' Domicilios (Pueden ser varios)
        wvarStep = 410
        Set wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_DOMICILIOS)
        '
        For wvarCounter = 0 To wobjXMLList.length - 1
            ' DATOS DOMICILIO
            wvarStep = 410
            wvarDCLIENSEC = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DCLIENSEC).Text
            wvarDDOMICSEC = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DDOMICSEC).Text
            wvarDOMICCAL = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICCAL).Text
            wvarDOMICDOM = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICDOM).Text
            wvarDOMICDNU = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICDNU).Text
            wvarDOMICESC = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICESC).Text
            wvarDOMICPIS = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICPIS).Text
            wvarDOMICPTA = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICPTA).Text
            wvarDOMICPOB = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICPOB).Text
            wvarDOMICCPO = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DOMICCPO).Text
            wvarPROVICOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_PROVICOD).Text
            wvarDPAISSCOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_DPAISSCOD).Text
            wvarTELCOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_TELCOD).Text
            wvarTELNRO = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_TELNRO).Text
            wvarPRINCIPAL = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_PRINCIPAL).Text

            wvarStep = 420
            Set wobjDBCmd = Nothing
            Set wobjDBCmd = CreateObject("ADODB.Command")
            
            wvarStep = 430
            Set wobjDBCmd.ActiveConnection = wobjDBCnn
            wobjDBCmd.CommandText = mcteStoreProcDom
            wobjDBCmd.CommandType = adCmdStoredProc
            '
            wvarStep = 440
            Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            '
            wvarStep = 450
            Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarDCLIENSEC)
            wobjDBParm.Precision = 9
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
    
            wvarStep = 460
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICSEC", adNumeric, adParamInput, , wvarDDOMICSEC)
            wobjDBParm.Precision = 3
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 470
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICCAL", adChar, adParamInput, 5, wvarDOMICCAL)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 480
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICDOM", adChar, adParamInput, 30, wvarDOMICDOM)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 490
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICDNU", adChar, adParamInput, 5, wvarDOMICDNU)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 500
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICESC", adChar, adParamInput, 1, wvarDOMICESC)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 510
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPIS", adChar, adParamInput, 4, wvarDOMICPIS)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 520
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPTA", adChar, adParamInput, 4, wvarDOMICPTA)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 530
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICPOB", adChar, adParamInput, 60, wvarDOMICPOB)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 540
            Set wobjDBParm = wobjDBCmd.CreateParameter("@DOMICCPO", adNumeric, adParamInput, , wvarDOMICCPO)
            wobjDBParm.Precision = 5
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 550
            Set wobjDBParm = wobjDBCmd.CreateParameter("@PROVICOD", adNumeric, adParamInput, , wvarPROVICOD)
            wobjDBParm.Precision = 2
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
        
            wvarStep = 560
            Set wobjDBParm = wobjDBCmd.CreateParameter("@PAISSCOD", adChar, adParamInput, 2, wvarDPAISSCOD)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            
            wvarStep = 570
            Set wobjDBParm = wobjDBCmd.CreateParameter("@TELCOD", adChar, adParamInput, 5, wvarTELCOD)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            
            wvarStep = 580
            Set wobjDBParm = wobjDBCmd.CreateParameter("@TELNRO", adChar, adParamInput, 30, wvarTELNRO)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            
            wvarStep = 585
            Set wobjDBParm = wobjDBCmd.CreateParameter("@PRINCIPAL", adChar, adParamInput, 1, wvarPRINCIPAL)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            '
            ' Se ejecuta el stored de domicilios
            wvarStep = 590
            wobjDBCmd.Execute adExecuteNoRecords
            '
            wvarStep = 600
            If wobjDBCmd.Parameters("@RETURN_VALUE").Value = 0 Then
                pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><DOMICILIO>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</DOMICILIO></Response>"
            Else   ' Mal el update de personas
                wvarMensaje = "ERROR AL HACER UPDATE DE UN DOMICILIO"
                pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
            End If
            '
        Next
        '
        wvarStep = 610
        Set wobjXMLList = Nothing
        '
        ' Cuentas (Pueden ser varias)
        wvarStep = 620
        Set wobjXMLList = wobjXMLRequest.selectNodes(mcteParam_CUENTAS)
        '
        For wvarCounter = 0 To wobjXMLList.length - 1
            ' DATOS CUENTA
            wvarCCLIENSEC = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CCLIENSEC).Text
            wvarCUENTSEC = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CUENTSEC).Text
            wvarTIPOCUEN = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_TIPOCUEN).Text
            wvarCOBROTIP = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_COBROTIP).Text
            wvarBANCOCOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_BANCOCOD).Text
            wvarSUCURCOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_SUCURCOD).Text
            wvarCUENTDC = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CUENTDC).Text
            wvarCUENNUME = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_CUENNUME).Text
            wvarTARJECOD = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_TARJECOD).Text
            wvarVENCIANN = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_VENCIANN).Text
            wvarVENCIMES = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_VENCIMES).Text
            wvarVENCIDIA = wobjXMLList.Item(wvarCounter).selectSingleNode(mcteParam_VENCIDIA).Text
        
            wvarStep = 630
            Set wobjDBCmd = Nothing
            Set wobjDBCmd = CreateObject("ADODB.Command")

            wvarStep = 640
            Set wobjDBCmd.ActiveConnection = wobjDBCnn
            wobjDBCmd.CommandText = mcteStoreProcCta
            wobjDBCmd.CommandType = adCmdStoredProc
            '
            wvarStep = 650
            Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            '
            wvarStep = 660
            Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENSEC", adNumeric, adParamInput, , wvarCCLIENSEC)
            wobjDBParm.Precision = 9
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 670
            Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENTSEC", adNumeric, adParamInput, , wvarCUENTSEC)
            wobjDBParm.Precision = 3
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 680
            Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPOCUEN", adChar, adParamInput, 1, wvarTIPOCUEN)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 690
            Set wobjDBParm = wobjDBCmd.CreateParameter("@COBROTIP", adChar, adParamInput, 2, wvarCOBROTIP)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 700
            Set wobjDBParm = wobjDBCmd.CreateParameter("@BANCOCOD", adNumeric, adParamInput, , wvarBANCOCOD)
            wobjDBParm.Precision = 4
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 710
            Set wobjDBParm = wobjDBCmd.CreateParameter("@SUCURCOD", adNumeric, adParamInput, , wvarSUCURCOD)
            wobjDBParm.Precision = 4
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 720
            Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENTDC", adChar, adParamInput, 2, wvarCUENTDC)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 730
            Set wobjDBParm = wobjDBCmd.CreateParameter("@CUENNUME", adChar, adParamInput, 16, wvarCUENNUME)
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 740
            Set wobjDBParm = wobjDBCmd.CreateParameter("@TARJECOD", adNumeric, adParamInput, , wvarTARJECOD)
            wobjDBParm.Precision = 2
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 750
            Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIANN", adNumeric, adParamInput, , wvarVENCIANN)
            wobjDBParm.Precision = 4
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 760
            Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIMES", adNumeric, adParamInput, , wvarVENCIMES)
            wobjDBParm.Precision = 2
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing

            wvarStep = 770
            Set wobjDBParm = wobjDBCmd.CreateParameter("@VENCIDIA", adNumeric, adParamInput, , wvarVENCIDIA)
            wobjDBParm.Precision = 2
            wobjDBParm.NumericScale = 0
            wobjDBCmd.Parameters.Append wobjDBParm
            Set wobjDBParm = Nothing
            '
            ' Se ejecuta el stored de cuentas
            wvarStep = 780
            wobjDBCmd.Execute adExecuteNoRecords
            '
            wvarStep = 790
            If wobjDBCmd.Parameters("@RETURN_VALUE").Value = 0 Then
                pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><CUENTA>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</CUENTA></Response>"
            Else   ' Mal el update de cuentas
                wvarMensaje = "ERROR AL HACER UPDATE DE UNA CUENTA"
                pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
            End If
            '
        Next
        '
        wvarStep = 800
        Set wobjXMLList = Nothing
        
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " /><PERSONAID>" & wobjDBCmd.Parameters("@RETURN_VALUE").Value & "</PERSONAID></Response>"
    Else   ' Mal el update de personas
        wvarMensaje = "ERROR AL HACER UPDATE PERSONAS"
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & wvarMensaje & " /></Response>"
    End If
            
    wvarStep = 810
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 820
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub











