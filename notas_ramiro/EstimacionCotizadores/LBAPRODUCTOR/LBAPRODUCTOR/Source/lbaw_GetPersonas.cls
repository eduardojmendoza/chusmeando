VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetPersonas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_ProductorLBA.lbaw_GetPersonas"
Const mcteStoreProc         As String = "SPSNCV_PROD_SIFMPERS_SELECT"

'Parametros XML de Entrada
Const mcteParam_USUARCOD    As String = "//USUARCOD"
Const mcteParam_CLIENNOM    As String = "//CLIENNOM"
Const mcteParam_CLIENAP     As String = "//CLIENAP"
Const mcteParam_NUMEDOCU    As String = "//NUMEDOCU"
Const mcteParam_TIPODOCU    As String = "//TIPODOCU"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarUSUARCOD        As String
    Dim wvarCLIENNOM        As String
    Dim wvarCLIENAP         As String
    Dim wvarNUMEDOCU        As String
    Dim wvarTIPODOCU        As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarUSUARCOD = .selectSingleNode(mcteParam_USUARCOD).Text
        wvarCLIENNOM = .selectSingleNode(mcteParam_CLIENNOM).Text
        wvarCLIENAP = .selectSingleNode(mcteParam_CLIENAP).Text
        wvarNUMEDOCU = .selectSingleNode(mcteParam_NUMEDOCU).Text
        wvarTIPODOCU = .selectSingleNode(mcteParam_TIPODOCU).Text
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    wvarStep = 50
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandText = mcteStoreProc
        .CommandType = adCmdStoredProc
    End With
    
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adVarChar, adParamInput, 10, wvarUSUARCOD)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENNOM", adVarChar, adParamInput, 20, wvarCLIENNOM)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("@CLIENAP", adVarChar, adParamInput, 20, wvarCLIENAP)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 90
    Set wobjDBParm = wobjDBCmd.CreateParameter("@NUMEDOCU", adVarChar, adParamInput, 11, wvarNUMEDOCU)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 100
    Set wobjDBParm = wobjDBCmd.CreateParameter("@TIPODOCU", adNumeric, adParamInput, , wvarTIPODOCU)
    wobjDBParm.Precision = 2
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 110
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    wvarStep = 160
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 170
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 180
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 190
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 200
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 220
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 230
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 240
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 250
    Set wobjDBCmd = Nothing
    '
    wvarStep = 260
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 270
    Set wobjDBCnn = Nothing
    '
    wvarStep = 280
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 290
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    wvarStep = 300
    Set wrstDBResult = Nothing
    '
    wvarStep = 310
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    Set wobjXSLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ROW'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NUMEDOCU'><xsl:value-of select='@NUMEDOCU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='TIPODOCU'><xsl:value-of select='@TIPODOCU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENAP1'><xsl:value-of select='@CLIENAP1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENAP2'><xsl:value-of select='@CLIENAP2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENNOM'><xsl:value-of select='@CLIENNOM' /></xsl:element>"
    ' Agregados para Hogar
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMANN'><xsl:value-of select='@NACIMANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMMES'><xsl:value-of select='@NACIMMES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='NACIMDIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENSEX'><xsl:value-of select='@CLIENSEX' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='CLIENEST'><xsl:value-of select='@CLIENEST' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PAISSCOD'><xsl:value-of select='@PAISSCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='PERSOTIP'><xsl:value-of select='@PERSOTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='DOMICSEC'><xsl:value-of select='@DOMICSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "      <xsl:element name='EMAIL'><xsl:value-of select='@EMAIL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENAP1'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENAP2'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENNOM'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENSEX'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='CLIENEST'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='PERSOTIP'/>"
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements='EMAIL'/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub






