VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetUsuarioBB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
Private mobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "lbawA_ProductorLBA.lbaw_GetUsuarioBB"
Const mcteStoreProc             As String = "P_BB_SELECT_USUARIO"

' Parametros XML de Entrada
Const mcteParam_USUARCOD    As String = "//USUARCOD"
Const mcteParam_USUARIOBB   As String = "//USUARIOBB"
Const mcteParam_MODO        As String = "//MODO"


Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wvarStep            As Long
    Dim wvarMensaje         As String
    '
    'DATOS GENERALES
    Dim wvarUSUARCOD   As String
    Dim wvarUSUARIOBB  As String
    Dim wvarMODO       As String
    '
    'RESPUESTA
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wvarResult          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(pvarRequest)
        '
        wvarStep = 20
        wvarUSUARCOD = .selectSingleNode(mcteParam_USUARCOD).Text
        wvarUSUARIOBB = .selectSingleNode(mcteParam_USUARIOBB).Text
        wvarMODO = .selectSingleNode(mcteParam_MODO).Text
        '
    End With
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    
    Set wobjDBCmd = CreateObject("ADODB.Command")
    
    wvarStep = 40
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = mcteStoreProc
    wobjDBCmd.CommandType = adCmdStoredProc

    wvarStep = 50
    Set wobjDBParm = wobjDBCmd.CreateParameter("@RETURN_VALUE", adInteger, adParamReturnValue, , 0)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
        
    wvarStep = 60
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARCOD", adChar, adParamInput, 10, wvarUSUARCOD)
    wobjDBParm.Precision = 9
    wobjDBParm.NumericScale = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 70
    Set wobjDBParm = wobjDBCmd.CreateParameter("@USUARIOBB", adChar, adParamInput, 20, wvarUSUARIOBB)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing

    wvarStep = 80
    Set wobjDBParm = wobjDBCmd.CreateParameter("MODO", adChar, adParamInput, 1, wvarMODO)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 90
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing

    If Not wrstDBResult.EOF Then
        '
        wvarStep = 100
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 110
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 120
        wobjXSLResponse.async = False
        Call wobjXSLResponse.loadXML(p_GetXSL())
        '
        wvarStep = 130
        wobjXMLResponse.loadXML ("<Response>" & Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "") & "</Response>")
        '
        wvarStep = 140
        'Desencripto la clave
        wobjXMLResponse.selectSingleNode("//PWDBB").Text = CapicomDecrypt(wobjXMLResponse.selectSingleNode("//PWDBB").Text)
        wvarResult = Replace(Replace(wobjXMLResponse.selectSingleNode("//Response").xml, "<Response>", ""), "</Response>", "")
        '
        wvarStep = 141
        Set wobjXMLResponse = Nothing
        Set wobjXSLResponse = Nothing
        '
        wvarStep = 150
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        wvarStep = 160
        pvarResponse = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 170
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 180
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    Set wrstDBResult = Nothing
    '
    wvarStep = 190
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
    wvarStep = 200
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    Set wobjDBCnn = Nothing
    Set wobjDBCmd = Nothing
    Set wobjHSBC_DBCnn = Nothing
    Set wobjXMLRequest = Nothing
    
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    '
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='USUARCOD'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='@USUARCOD' />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='USUARIOBB'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='@USUARIOBB' />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='PWDBB'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='@PWDBB' />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='FECHA'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:value-of select='@FECHA' />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    
    p_GetXSL = wvarStrXSL
End Function

Private Sub ObjectControl_Activate()
   '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
    ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
   '
End Sub







