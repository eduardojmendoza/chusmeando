<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<xsl:for-each select="//POLIZACOLECTIVA">
			<xsl:sort select="./TOMADOR"/>
			<option>
				<xsl:attribute name="value"><xsl:value-of select="POLIZSEC"/></xsl:attribute>
				<xsl:attribute name="polizann"><xsl:value-of select="POLIZANN"/></xsl:attribute>
				<xsl:attribute name="ramopcod"><xsl:value-of select="//RAMOPCOD"/></xsl:attribute>
				<xsl:value-of select="POLIZSEC"/> | <xsl:value-of select="TOMADOR"/>
			</option>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
