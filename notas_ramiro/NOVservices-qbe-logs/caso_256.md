El problema se manifiesta en que para un mismo request, a veces da un timeout porque genera una trama incorrecta, y al reintentar funciona OK.

Analizando los logs encontramos que se generan distintas tramas en cada caso, una inválida y una válida.

Siguiendo el código podemos ver que la clase `com.qbe.services.mqgeneric.impl.AbstractMQMensaje` transforma los requests en tramas para los mensajes genéricos.

Los mensajes genéricos toman un archivo de definición, en este caso es el 1101:

```xml
<MENSAJE xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="..\XSD\MQDefinition.xsd">
	<DEFINICION>
		<MENSAJE>1101</MENSAJE>
		<MQCONFIGFILE>LBAVirtualMQConfig.xml</MQCONFIGFILE>
		<TIMEOUT>1500</TIMEOUT>
		<GENERARLOG CantLlamadasALoguear="5" LogFile="Log1101.XML"/>
	</DEFINICION>
	<ENTRADA>
		<CAMPO Nombre="CIAASCOD" TipoDato="TEXTO" Enteros="4" Default="0001"/>
		<CAMPO Nombre="USUARCOD" TipoDato="TEXTO" Enteros="10"/>
		<CAMPO Nombre="ESTADOMSG" TipoDato="TEXTO" Enteros="2"/>
		<CAMPO Nombre="ERRORMSG" TipoDato="TEXTO" Enteros="2"/>
		<CAMPO Nombre="CLIENAS" TipoDato="ENTERO" Enteros="9"/>
		<CAMPO Nombre="NIVELAS" TipoDato="TEXTO" Enteros="2"/>
		<CAMPO Nombre="NIVELCLA1" TipoDato="TEXTO" Enteros="2"/>
		<CAMPO Nombre="CLIENSEC1" TipoDato="ENTERO" Enteros="9"/>
		<CAMPO Nombre="NIVELCLA2" TipoDato="TEXTO" Enteros="2"/>
		<CAMPO Nombre="CLIENSEC2" TipoDato="ENTERO" Enteros="9"/>
		<CAMPO Nombre="NIVELCLA3" TipoDato="TEXTO" Enteros="2"/>
		<CAMPO Nombre="CLIENSEC3" TipoDato="ENTERO" Enteros="9"/>
		<CAMPO Nombre="RAMOPCOD" TipoDato="TEXTO" Enteros="4"/>
		<CAMPO Nombre="POLIZANN" TipoDato="ENTERO" Enteros="2"/>
		<CAMPO Nombre="POLIZSEC" TipoDato="ENTERO" Enteros="6" Default="0"/>
		<CAMPO Nombre="CERTIPOL" TipoDato="ENTERO" Enteros="4" Default="0"/>
		<CAMPO Nombre="CERTIANN" TipoDato="ENTERO" Enteros="4" Default="0"/>
		<CAMPO Nombre="CERTISEC" TipoDato="ENTERO" Enteros="6"/>
		<CAMPO Nombre="SUPLENUM" TipoDato="ENTERO" Enteros="4" Default="0"/>
	</ENTRADA>
```

El método GetDatoFormateado va recorriendo la definición de los campos. Para cado, se fija si existe ese tag en el Request que recibe. Si está, toma el valor y lo formatea. Si no está, genera un default.

En el caso que encontramos a veces genera el USUARCOD y a veces no. Por ej:

```
1101: wvarOpID
0001: CIAASCOD 		<CAMPO Nombre="CIAASCOD" TipoDato="TEXTO" Enteros="4" Default="0001"/>
          : USUARCOD   <--- este es el que falta!  		<CAMPO Nombre="USUARCOD" TipoDato="TEXTO" Enteros="10"/>     <--- este es el que falta!  ***************<<<<<<<<<<<<<<<<
  : ESTADOMSG		<CAMPO Nombre="ESTADOMSG" TipoDato="TEXTO" Enteros="2"/>
  : ERRORMSG		<CAMPO Nombre="ERRORMSG" TipoDato="TEXTO" Enteros="2"/>
000000000		<CAMPO Nombre="CLIENAS" TipoDato="ENTERO" Enteros="9"/>
  : 		<CAMPO Nombre="NIVELAS" TipoDato="TEXTO" Enteros="2"/>
  : 		<CAMPO Nombre="NIVELCLA1" TipoDato="TEXTO" Enteros="2"/>
000000000: 		<CAMPO Nombre="CLIENSEC1" TipoDato="ENTERO" Enteros="9"/>
  : <CAMPO Nombre="NIVELCLA2" TipoDato="TEXTO" Enteros="2"/>
000000000: <CAMPO Nombre="CLIENSEC2" TipoDato="ENTERO" Enteros="9"/>
  : <CAMPO Nombre="NIVELCLA3" TipoDato="TEXTO" Enteros="2"/>
000000000: <CAMPO Nombre="CLIENSEC3" TipoDato="ENTERO" Enteros="9"/>
AUT1: <CAMPO Nombre="RAMOPCOD" TipoDato="TEXTO" Enteros="4"/>
01
086519
0000
0000
000000
0000
		<CAMPO Nombre="POLIZANN" TipoDato="ENTERO" Enteros="2"/>
		<CAMPO Nombre="POLIZSEC" TipoDato="ENTERO" Enteros="6" Default="0"/>
		<CAMPO Nombre="CERTIPOL" TipoDato="ENTERO" Enteros="4" Default="0"/>
		<CAMPO Nombre="CERTIANN" TipoDato="ENTERO" Enteros="4" Default="0"/>
		<CAMPO Nombre="CERTISEC" TipoDato="ENTERO" Enteros="6"/>
		<CAMPO Nombre="SUPLENUM" TipoDato="ENTERO" Enteros="4" Default="0"/>
```

El problema ocurre acá:

```java
			// pObjLongitud es de la forma Enteros="10"
			// pLongitudStr: 10
			String pLongitudStr = XmlDomExtended.getText( pobjLongitud );
			
			// retorna a veces pLongitudDouble:2.0 y a veces pLongitudDouble:10.0 !!!!!!
			double pLongitudDouble = VB.val( pLongitudStr );
```
Asi que acá manda USUARCOD de un largo variable.
( obviamente esto puede pasar para cualquier variable, no depende de USUARCOD, pero este es el caso que estamos analizando)

`VB` es una clase de la library VBC que tiene clases compatibles con VisualBasic, es un componente de terceros.

Analizamos los fuentes de VBC y encontramos que esta clase crea un NumberFormat y lo usa cada vez que ejecuta VB.val().
Es la misma instancia, compartida entre varios threads.

Estas instancias **no** son ThreadSafe.
De la doc:

https://docs.oracle.com/javase/6/docs/api/java/text/NumberFormat.html
>Synchronization
>Number formats are generally not synchronized. It is recommended to create separate format instances for each thread. If multiple threads access a format concurrently, it must be synchronized externally.

( Esto se da en todos los JDKs, inclusive el 8, así que no es una limitación de la JDK. )

**Así que es por esto que los threads "se pisan", y el error ocurre esporádicamente. Ocurre cuando dos threads usan el "NumberFormat" al mismo tiempo.**

Para validar esto hicimos un test en `com.qbe.services.nov.impl.ConsultaMQServicesImplTest` que levanta 200 threads concurrentes y ahí vemos que se reproduce el problema.


Para resolverlo, en vez de usar el mismo NumberFormat, usamos uno nuevo cada vez. Implementamos una nueva versión del método val(), a partir de los fuentes de la library:

```java
	public static double val(java.lang.String s) {
		java.lang.StringBuffer stringbuffer;
		double d;
		stringbuffer = new StringBuffer();
		int i = s.length();
		for (int j = 0; j < i; j++) {
			char c = s.charAt(j);
			if (c != ' ')
				stringbuffer.append(s.charAt(j));
		}

		d = 0.0D;
		DecimalFormat df = new DecimalFormat();
		java.lang.Number number = df.parse(stringbuffer.toString(), new ParsePosition(0));
		if (number != null) {
			return number.doubleValue();
		} else {
			return d;
		}
	}
```

Con esto resolvemos el problema.

**En resumen este problema se podía dar con cualquier mensaje que se procese por componente genérico.** Podría aparecer, entonces, no solamente con el 1101 sino con cualquier otro genérico.

Este fix corrige el procesamiento de los genéricos. Pero afecta a **todas** llamada de VB.val(), así que tenemos que modificar todas las que se hacen a lo largo de toda la base de código de NOVServices. También afecta a CMS ( en caso de que se siga usando )
Esto lo sigo en otro issue.





