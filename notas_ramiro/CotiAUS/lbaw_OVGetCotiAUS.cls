VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "lbaw_OVGetCotiAUS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'******************************************************************************
'Fecha de Modificaci�n: 30/09/2011
'PPCR: 2011-00390
'Desarrollador: Leonardo Ruiz
'Descripci�n: Anexo I (Nueva Respuesta, Planes y Coberturas Variables).
'-----------------------------------------------------------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING
'           CORPORATION LIMITED 2011. ALL RIGHTS RESERVED
'
'THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPUSE FOR WICH
'IT HAS BEEN PROVIDED. NO PART OF ITS IS TO BE REPROCED,
'DISSAMBLED, TRANSMITTED, STORED IN A RETRIVAL SYSTEM, NOR
'TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY
'FOR ANY PURPOSES WHATSOEVER WITHOUT GTHE PRIOR WRITTEN CONSENT
'OF THE HONGKONK AND SHANGHAI BANKING CORPORATION LIMITED
'INFRINGEMENT OF COPYRIGHT IS A SERIOUS CIVIL AND CRIMINAL
'OFFENSE, WHICH CAN RESULT IN HEAVY FINES AND PAYMENT OF
'SUBSTANTIAL DAMAGES.
'-------------------------------------------------------------------------------------------------------------------------------------
' Module Name : lbaw_OVGetCotiAUS
' File Name : lbaw_OVGetCotiAUS.cls
' Creation Date: 30/09/2011
' Programmer : Desconocido
' Abstract :   Cotizacion de Autoscoring
' *****************************************************************

Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog
'
'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'Datos de la accion
Const mcteClassName             As String = "lbawA_OVMQCotizar.lbaw_OVGetCotiAUS"
Const mcteOpID                  As String = "0047"
'
'Parametros XML de Entrada
Const mcteParam_Cliensec        As String = "//CLIENSEC"
Const mcteParam_NacimAnn        As String = "//NACIMANN"
Const mcteParam_NacimMes        As String = "//NACIMMES"
Const mcteParam_NacimDia        As String = "//NACIMDIA"
Const mcteParam_Sexo            As String = "//SEXO"
Const mcteParam_Estado          As String = "//ESTADO"
Const mcteParam_IVA             As String = "//IVA"
Const mcteParam_IBB             As String = "//IBB"
Const mcteParam_CLIENTIP        As String = "//CLIENTIP"        'Fjo 2009-02-13
Const mcteParam_CobroTip        As String = "//COBROTIP"
Const mcteParam_CobroCod        As String = "//COBROCOD"
Const mcteParam_AgeCod          As String = "//AGECOD"
Const mcteParam_AgeCla          As String = "//AGECLA"          'MMC 2012-11-28
Const mcteParam_DatosPlan       As String = "//DATOSPLAN"
Const mcteParam_ModeAutCod      As String = "//MODEAUTCOD"
Const mcteParam_KMsrngCod       As String = "//KMSRNGCOD"
Const mcteParam_EfectAnn        As String = "//EFECTANN"
Const mcteParam_SiGarage        As String = "//SIGARAGE"
Const mcteParam_Siniestros      As String = "//SINIESTROS"
Const mcteParam_Gas             As String = "//GAS"
Const mcteParam_ClubLBA         As String = "//CLUBLBA"
Const mcteParam_Luneta          As String = "//LUNETA"
Const mcteParam_Green           As String = "//CLUBECO"
Const mcteParam_Granizo         As String = "//GRANIZO"
Const mcteParam_RoboCont        As String = "//ROBOCONT"
Const mcteParam_Provi           As String = "//PROVI"
Const mcteParam_LocalidadCod    As String = "//LOCALIDADCOD"
Const mcteParam_Campacod        As String = "//CAMPACOD"
Const mcteParam_EsCero          As String = "//ESCERO"
Const mcteParam_Portal          As String = "//PORTAL"
Const mcteParam_DEST80          As String = "//DESTRUCCION_80"  '23/01/2006
'Datos de los Hijos
Const mcteNodos_Hijos           As String = "//Request/HIJOS/HIJO"
Const mcteParam_NacimHijo       As String = "NACIMHIJO"
Const mcteParam_SexoHijo        As String = "SEXOHIJO"
Const mcteParam_EstadoHijo      As String = "ESTADOHIJO"
'Datos de los Accesorios
Const mcteNodos_Accesorios      As String = "//Request/ACCESORIOS/ACCESORIO"
Const mcteParam_PrecioAcc       As String = "PRECIOACC"
Const mcteParam_DescripcionAcc  As String = "DESCRIPCIONACC"
Const mcteParam_CodigoAcc       As String = "CODIGOACC"
'Para Mercado Abierto
Const mcteParam_CampaTel        As String = "//TELEFONO"
'Incios y Tama�os
Const mcteInicioCHijos          As Integer = 3 - 1
Const mcteInicioSHijos          As Integer = 2323 - 1
Const mcteInicioOtrosD          As Integer = 6689 - 1
Const mcteTamanoCHijos          As Integer = 116
Const mcteTamanoSHijos          As Integer = 165
Const mcteTamanoOtrosD          As Integer = 129
Const mcteInicioTbDECA          As Integer = 5633 - 1
Const mcteTamanoTbDECA          As Integer = 35
Const mcteInicioMxDECA          As Integer = 6686 - 1

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLParams       As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
    Dim wobjClass           As HSBCInterfaces.IAction
    '
    Dim wvarRequest         As String
    Dim wvarResponse        As String
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    '
    Dim wvarCliensec        As String
    Dim wvarMensaje         As String
    Dim wvarNacimAnn        As String
    Dim wvarNacimMes        As String
    Dim wvarNacimDia        As String
    Dim wvarSexo            As String
    Dim wvarEstado          As String
    Dim wvarIVA             As String
    Dim wvarIBB             As String
    Dim wvarCLIENTIP        As String   'Fjo 2009-02-13
    Dim wvarCobroCod        As String
    Dim wvarCobroTip        As String
    Dim wvarAgeCod          As String
    Dim wvarAgeCla          As String   'MMC 2012-11-28
    Dim wvarDatosPlan       As String
    Dim wvarModeAutCod      As String
    Dim wvarKMsrngCod       As String
    Dim wvarEfectAnn        As String
    Dim wvarSiGarage        As String
    Dim wvarSiniestros      As String
    Dim wvarGas             As String
    Dim wvarClubLBA         As String
    Dim wvarLuneta          As String
    Dim wvarGreen           As String
    Dim wvarGranizo         As String
    Dim wvarRoboCont        As String
    Dim wvarProvi           As String
    Dim wvarLocalidadCod    As String
    Dim wvarHijos           As String
    Dim wvarConHijos        As String
    Dim wvarAccesorios      As String
    Dim wvarCampacod        As String
    Dim wvarEsCero          As String   'Agregado para promos de los 0Km - 07/12/2005
    Dim wvarDEST80          As String   'Agregado para la Destrucci�n al 80% - 23/01/2006
    '
    Dim wvarSumaMin         As String
    Dim wvarSumAseg         As String
    Dim wvarSumaLBA         As String
    Dim wvarCotiID          As String
    Dim wvarFechaDia        As String
    Dim wvarFechaSig        As String
    Dim wvarCampaTelForm    As String
    Dim wvarCampaTel        As String
    '
    Dim strParseString      As String
    Dim wvariCounter        As Integer
    '
    Dim wvarAuxCodAcc       As String
    Dim wvarAuxPlanCicle    As String
    Dim wvarAuxCobeCicle    As String
    '
    Dim wvarContadorPlan    As Integer
    Dim wvarContadorCobs    As Integer
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'Cargar el XML de Entrada
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    wobjXMLRequest.async = False
    Call wobjXMLRequest.loadXML(Request)
    '
    'Obtener el Numero de Cotizaci�n
    wvarStep = 10
    wvarRequest = "<Request></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetNroCot")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 20
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParams
        .async = False
        Call .loadXML(wvarResponse)
        wvarCotiID = .selectSingleNode("//NROCOT").Text
        wvarFechaDia = .selectSingleNode("//FECHA_DIA").Text
        wvarFechaSig = .selectSingleNode("//FECHA_SIGUIENTE").Text
    End With
    '
    Set wobjXMLParams = Nothing
    '
    'Obtener la M�nima Suma
    wvarStep = 30
    wvarRequest = "<Request><CONCEPTO>SA-SCO-MIN</CONCEPTO></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetParamGral")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    Set wobjClass = Nothing
    '
    wvarStep = 40
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParams
        .async = False
        Call .loadXML(wvarResponse)
        wvarSumaMin = Right(String(11, "0") & CStr(CCur(.selectSingleNode("//PARAMNUM").Text) * 100), 11)
    End With
    '
    Set wobjXMLParams = Nothing
    '
    'Buscar la Suma Asegurada para AUPROCOD = 02
    wvarStep = 50
    wvarRequest = "<Request><AUMODCOD>" & wobjXMLRequest.selectSingleNode(mcteParam_ModeAutCod).Text & "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>"
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVLBAMQ.lbaw_GetSumAseg")
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    '
    'Valor de SUMASEG
    wvarStep = 60
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    wobjXMLParams.async = False
    wobjXMLParams.loadXML wvarResponse
    If wobjXMLParams.selectSingleNode("//COMBO/OPTION[@value=" & wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn).Text & "]/@sumaseg") Is Nothing Then
        wvarMensaje = "No se pueden cotizar autos para el a�o " & wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn).Text
        GoTo ErrorHandler
    Else
        wvarSumAseg = Right(String(11, "0") & Replace(wobjXMLParams.selectSingleNode("//COMBO/OPTION[@value=" & wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn).Text & "]/@sumaseg").Text, ",", ""), 11)
    End If
    Set wobjXMLParams = Nothing
    '
    'Buscar la Suma Asegurada para AUPROCOD = 03
    wvarStep = 70
    wvarRequest = "<Request><AUMODCOD>" & wobjXMLRequest.selectSingleNode(mcteParam_ModeAutCod).Text & "</AUMODCOD><AUPROCOD>02</AUPROCOD></Request>"
    Call wobjClass.Execute(wvarRequest, wvarResponse, "")
    '
    'Valor de SUMALBA
    wvarStep = 80
    Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
    wobjXMLParams.async = False
    wobjXMLParams.loadXML wvarResponse
    wvarSumaLBA = Right(String(11, "0") & Replace(wobjXMLParams.selectSingleNode("//COMBO/OPTION[@value=" & wobjXMLRequest.selectSingleNode(mcteParam_EfectAnn).Text & "]/@sumaseg").Text, ",", ""), 11)
    Set wobjXMLParams = Nothing
    Set wobjClass = Nothing
    '
    wvarStep = 90
    With wobjXMLRequest
        If .selectNodes(mcteParam_Cliensec).length = 0 Then
            wvarCliensec = String(9, "0")
        Else
            wvarCliensec = Right(String(9, "0") & .selectSingleNode(mcteParam_Cliensec).Text, 9)
        End If
        wvarNacimAnn = .selectSingleNode(mcteParam_NacimAnn).Text
        wvarNacimMes = .selectSingleNode(mcteParam_NacimMes).Text
        wvarNacimDia = .selectSingleNode(mcteParam_NacimDia).Text
        wvarSexo = .selectSingleNode(mcteParam_Sexo).Text
        wvarEstado = .selectSingleNode(mcteParam_Estado).Text
        wvarIVA = .selectSingleNode(mcteParam_IVA).Text
        wvarIBB = .selectSingleNode(mcteParam_IBB).Text
        '
        If .selectNodes(mcteParam_CLIENTIP).length = 0 Then
            wvarCLIENTIP = "00" 'Para forzar la persona f�sica si no informa el nodo
        Else
            wvarCLIENTIP = Right("00" + Trim(.selectSingleNode(mcteParam_CLIENTIP).Text), 2)
        End If
        '
        'Mercado Abierto siempre Cotiza con COBROCOD = 4 y COBROTIP = VI
        If .selectNodes(mcteParam_CobroCod).length = 0 Then
            wvarCobroCod = "4"
        Else
            wvarCobroCod = .selectSingleNode(mcteParam_CobroCod).Text
        End If
        '
        If .selectNodes(mcteParam_CobroTip).length = 0 Then
            wvarCobroTip = "VI"
        Else
            wvarCobroTip = .selectSingleNode(mcteParam_CobroTip).Text
        End If
        '
        wvarAgeCod = Right(String(4, "0") & Trim(.selectSingleNode(mcteParam_AgeCod).Text), 4)
        '
        If .selectNodes(mcteParam_AgeCla).length = 0 Then
            wvarAgeCla = "PR"
        Else
            wvarAgeCla = Right(String(2, "0") & Trim(.selectSingleNode(mcteParam_AgeCla).Text), 2) '2012-11-28
        End If
        'Plan y Franquicia
        If .selectNodes(mcteParam_DatosPlan).length = 0 Then
            'Cotiza todos los Planes
            wvarDatosPlan = "00000"
        Else
            wvarDatosPlan = Right(String(5, "0") & .selectSingleNode(mcteParam_DatosPlan).Text, 5)
        End If
        '
        wvarStep = 100
        wvarModeAutCod = .selectSingleNode(mcteParam_ModeAutCod).Text
        wvarKMsrngCod = Right(String(7, "0") & .selectSingleNode(mcteParam_KMsrngCod).Text, 7)
        wvarEfectAnn = .selectSingleNode(mcteParam_EfectAnn).Text
        wvarSiGarage = .selectSingleNode(mcteParam_SiGarage).Text
        wvarSiniestros = Right("00" & .selectSingleNode(mcteParam_Siniestros).Text, 2)
        wvarGas = .selectSingleNode(mcteParam_Gas).Text
        wvarClubLBA = .selectSingleNode(mcteParam_ClubLBA).Text
        wvarLuneta = .selectSingleNode(mcteParam_Luneta).Text
        wvarGreen = Left(.selectSingleNode(mcteParam_Green).Text, 1)
        wvarGranizo = Left(.selectSingleNode(mcteParam_Granizo).Text, 1)
        wvarRoboCont = Left(.selectSingleNode(mcteParam_RoboCont).Text, 1)
        wvarProvi = Right("00" & .selectSingleNode(mcteParam_Provi).Text, 2)
        wvarLocalidadCod = Right("0000" & .selectSingleNode(mcteParam_LocalidadCod).Text, 4)
        ' Marca de OKm Se agreg� para las promos - 07/12/2005
        wvarEsCero = .selectSingleNode(mcteParam_EsCero).Text
        ' Destrucci�n al 80%  --- 23/01/2006
        If .selectNodes(mcteParam_DEST80).length > 0 Then wvarDEST80 = .selectSingleNode(mcteParam_DEST80).Text
        If wvarDEST80 = "" Then wvarDEST80 = "N"
        '
        '~~~~~~~~~~~
        'Incio Hijos
        '~~~~~~~~~~~
        wvarStep = 110
        wvarHijos = ""
        wvarConHijos = "N"
        Set wobjXMLList = .selectNodes(mcteNodos_Hijos)
        '
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarConHijos = "S"
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_NacimHijo).Text
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_SexoHijo).Text
            wvarHijos = wvarHijos & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_EstadoHijo).Text
        Next
        '
        For wvariCounter = wobjXMLList.length To 9
            wvarHijos = wvarHijos & String(8, "0") & "  "
        Next
        '~~~~~~~~~
        'Fin Hijos
        '~~~~~~~~~
        '
        '~~~~~~~~~~~~~~~~~
        'Inicio Accesorios
        '~~~~~~~~~~~~~~~~~
        wvarStep = 120
        wvarAccesorios = ""
        Set wobjXMLList = .selectNodes(mcteNodos_Accesorios)
        '
        wvarAuxCodAcc = ""
        For wvariCounter = 0 To wobjXMLList.length - 1
            wvarAuxCodAcc = wvarAuxCodAcc & Right(String(4, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_CodigoAcc).Text, 4)
            wvarAccesorios = wvarAccesorios & Right(String(14, "0") & wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_PrecioAcc).Text, 14)
            wvarAccesorios = wvarAccesorios & Left(wobjXMLList.Item(wvariCounter).selectSingleNode(mcteParam_DescripcionAcc).Text & String(30, " "), 30)
            wvarAccesorios = wvarAccesorios & "S"
        Next
        '
        For wvariCounter = wobjXMLList.length To 9
            wvarAccesorios = wvarAccesorios & String(14, "0") & String(31, " ")
        Next
        '~~~~~~~~~~~~~~
        'Fin Accesorios
        '~~~~~~~~~~~~~~
        wvarCampacod = Right(String(4, "0") & Trim(.selectSingleNode(mcteParam_Campacod).Text), 4)
        '
        'Buscar los datos de Campa�a, Productor (Solo para Mercado Abierto)
        If .selectNodes(mcteParam_Portal).length <> 0 Then
            If Not .selectSingleNode(mcteParam_Portal).Text = "LBA_PRODUCTORES" Then
                wvarStep = 130
                wvarRequest = "<Request><PORTAL>" & .selectSingleNode(mcteParam_Portal).Text & "</PORTAL><RAMOPCOD>AUS1</RAMOPCOD></Request>"
                Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OfVirtualLBA.lbaw_GetPortalComerc")
                Call wobjClass.Execute(wvarRequest, wvarResponse, "")
                '
                wvarStep = 140
                Set wobjXMLParams = CreateObject("MSXML2.DOMDocument")
                wobjXMLParams.async = False
                wobjXMLParams.loadXML wvarResponse
                wvarCampacod = Left(wobjXMLParams.selectSingleNode(mcteParam_Campacod).Text & String(4, " "), 4)
                wvarAgeCod = Trim(wobjXMLParams.selectSingleNode(mcteParam_AgeCod).Text)
                wvarAgeCla = Trim(wobjXMLParams.selectSingleNode(mcteParam_AgeCla).Text) 'MMC 2012-11-28
                wvarCampaTel = wobjXMLParams.selectSingleNode(mcteParam_CampaTel).Text
                wvarCampaTelForm = Formateo_Telefono(wobjXMLParams.selectSingleNode(mcteParam_CampaTel).Text)
                Set wobjXMLParams = Nothing
                Set wobjClass = Nothing
            End If
        End If
    End With
    '
    Set wobjXMLRequest = Nothing
    Set wobjXMLList = Nothing
    '
    wvarStep = 150
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    wvarMensaje = mcteOpID & wvarCliensec & wvarNacimAnn & wvarNacimMes & wvarNacimDia
    wvarMensaje = wvarMensaje & wvarSexo & wvarEstado
    wvarMensaje = wvarMensaje & wvarIVA & wvarIBB
    wvarMensaje = wvarMensaje & wvarCLIENTIP    'Fjo 2009-02-13
    wvarMensaje = wvarMensaje & wvarCobroTip & wvarCobroCod & wvarFechaDia & wvarFechaSig & wvarAgeCod & wvarDatosPlan & wvarSumAseg & wvarSumaLBA
    wvarMensaje = wvarMensaje & wvarModeAutCod & "001" & wvarKMsrngCod & wvarEfectAnn & wvarSiGarage
    wvarMensaje = wvarMensaje & wvarSiniestros & wvarGas & wvarClubLBA & wvarLuneta & wvarGreen & wvarGranizo & wvarRoboCont & "00" & wvarProvi & wvarLocalidadCod
    wvarMensaje = wvarMensaje & wvarHijos
    wvarMensaje = wvarMensaje & wvarAccesorios
    wvarMensaje = wvarMensaje & Right(String(9, "0") & wvarCotiID, 9) & wvarSumaMin & wvarCampacod & String(36, " ")
    wvarMensaje = wvarMensaje & "000000000000000000000000NNNSSSN" & Left$(wvarAuxCodAcc & String(40, "0"), 40)
    wvarMensaje = wvarMensaje & wvarEsCero & wvarDEST80 & wvarAgeCla
    '
    wvarArea = wvarMensaje
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 155
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    'GuardarLog strParseString
    'Cargo un archivo fijo de prueba
    'strParseString = CargarArchivoPrueba
    'strParseString = Replace(Replace(strParseString, Chr(10), ""), Chr(13), "")
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    'HRF Anexo I (Nueva Respuesta, Planes y Coberturas Variables)
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~
    'Incio Response del Mensaje
    '~~~~~~~~~~~~~~~~~~~~~~~~~~
    wvarStep = 200
    wvarResult = ""
    '
    If UCase(Left(strParseString, 2)) = "OK" Then
        wvarResult = wvarResult & "<COT_NRO>" & wvarCotiID & "</COT_NRO>"
        wvarResult = wvarResult & "<CAMPA_COD>" & wvarCampacod & "</CAMPA_COD>"
        wvarResult = wvarResult & "<AGE_COD>" & wvarAgeCod & "</AGE_COD>"
        wvarResult = wvarResult & "<AGE_CLA>" & wvarAgeCla & "</AGE_CLA>"
        'Solo para Mercado Abierto
        wvarResult = wvarResult & "<CAMPA_TEL>" & wvarCampaTel & "</CAMPA_TEL>"
        wvarResult = wvarResult & "<CAMPA_TEL_FORM>" & wvarCampaTelForm & "</CAMPA_TEL_FORM>"
        '
        wvarResult = wvarResult & "<SUMASEG>" & wvarSumAseg & "</SUMASEG>"
        wvarResult = wvarResult & "<SUMALBA>" & wvarSumaLBA & "</SUMALBA>"
        wvarResult = wvarResult & "<SILUNETA>" & Trim(Mid(strParseString, mcteInicioMxDECA + 1 + 1, 1)) & "</SILUNETA>"
        wvarResult = wvarResult & "<TIENEHIJOS>" & wvarConHijos & "</TIENEHIJOS>"
        '
        '~~~~~~~~~~~~
        'Incio Planes
        '~~~~~~~~~~~~
        wvarStep = 210
        wvarResult = wvarResult & "<PLANES>"
        '
        wvarAuxPlanCicle = 1
        wvarContadorPlan = 0
        'Todos los planes
        Do While Val(wvarAuxPlanCicle) > 0 And wvarContadorPlan < 20
            'Plan
            wvarResult = wvarResult & "<PLAN>"
                'Datos
                wvarResult = wvarResult & "<PLANNCOD>" & Trim(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 0 + 1, 5)) & "</PLANNCOD>"
                wvarResult = wvarResult & "<PLANNDES><![CDATA[" & Trim(Mid(strParseString, mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 0 + 1, 121)) & "]]></PLANNDES>"
                wvarResult = wvarResult & "<ORDEN>" & Trim(Mid(strParseString, mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 125 + 1, 4)) & "</ORDEN>"
                wvarResult = wvarResult & "<LUNPAR>" & Trim(Mid(strParseString, mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 121 + 1, 1)) & "</LUNPAR>"
                wvarResult = wvarResult & "<GRANIZO>" & Trim(Mid(strParseString, mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 122 + 1, 1)) & "</GRANIZO>"
                wvarResult = wvarResult & "<ROBOCON>" & Trim(Mid(strParseString, mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 123 + 1, 1)) & "</ROBOCON>"
                wvarResult = wvarResult & "<ESRC>" & Trim(Mid(strParseString, mcteInicioOtrosD + (wvarContadorPlan * mcteTamanoOtrosD) + 124 + 1, 1)) & "</ESRC>"
                'Precio c/Hijos
                wvarResult = wvarResult & "<CON_HIJOS>"
                    'LR 11/10/2011 La PRIMA de CON_HIJOS pasa a ser "05 PLANNDES PIC X(1)"
                    wvarResult = wvarResult & "<PRIMA>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 6 + 1, 11)) & "</PRIMA>"
                    wvarResult = wvarResult & "<RECARGOS>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 17 + 1, 11)) & "</RECARGOS>"
                    wvarResult = wvarResult & "<IVAIMPOR>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 28 + 1, 11)) & "</IVAIMPOR>"
                    wvarResult = wvarResult & "<IVAIMPOA>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 39 + 1, 11)) & "</IVAIMPOA>"
                    wvarResult = wvarResult & "<IVARETEN>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 50 + 1, 11)) & "</IVARETEN>"
                    wvarResult = wvarResult & "<DEREMI>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 61 + 1, 11)) & "</DEREMI>"
                    wvarResult = wvarResult & "<SELLADO>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 72 + 1, 11)) & "</SELLADO>"
                    wvarResult = wvarResult & "<INGBRU>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 83 + 1, 11)) & "</INGBRU>"
                    wvarResult = wvarResult & "<IMPUES>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 94 + 1, 11)) & "</IMPUES>"
                    wvarResult = wvarResult & "<PRECIO>" & FormatoDec(Mid(strParseString, mcteInicioCHijos + (wvarContadorPlan * mcteTamanoCHijos) + 105 + 1, 11)) & "</PRECIO>"
                wvarResult = wvarResult & "</CON_HIJOS>"
                'Precio s/Hijos
                wvarResult = wvarResult & "<SIN_HIJOS>"
                    wvarResult = wvarResult & "<PRIMA>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 55 + 1, 11)) & "</PRIMA>"
                    wvarResult = wvarResult & "<RECARGOS>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 66 + 1, 11)) & "</RECARGOS>"
                    wvarResult = wvarResult & "<IVAIMPOR>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 77 + 1, 11)) & "</IVAIMPOR>"
                    wvarResult = wvarResult & "<IVAIMPOA>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 88 + 1, 11)) & "</IVAIMPOA>"
                    wvarResult = wvarResult & "<IVARETEN>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 99 + 1, 11)) & "</IVARETEN>"
                    wvarResult = wvarResult & "<DEREMI>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 110 + 1, 11)) & "</DEREMI>"
                    wvarResult = wvarResult & "<SELLADO>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 121 + 1, 11)) & "</SELLADO>"
                    wvarResult = wvarResult & "<INGBRU>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 132 + 1, 11)) & "</INGBRU>"
                    wvarResult = wvarResult & "<IMPUES>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 143 + 1, 11)) & "</IMPUES>"
                    wvarResult = wvarResult & "<PRECIO>" & FormatoDec(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 154 + 1, 11)) & "</PRECIO>"
                wvarResult = wvarResult & "</SIN_HIJOS>"
            wvarResult = wvarResult & "</PLAN>"
            '
            wvarContadorPlan = wvarContadorPlan + 1
            '
            'Seleccionar el pr�ximo plan (si es cero cortar el ciclo)
            wvarAuxPlanCicle = Trim(Mid(strParseString, mcteInicioSHijos + (wvarContadorPlan * mcteTamanoSHijos) + 0 + 1, 5))
        Loop
        wvarResult = wvarResult & "</PLANES>"
        '~~~~~~~~~~
        'Fin Planes
        '~~~~~~~~~~
        '
        '~~~~~~~~~~~~~~~~~
        'Inicio Coberturas
        '~~~~~~~~~~~~~~~~~
        wvarStep = 220
        'Coberturas solo si le pasa un plan
        If wvarDatosPlan <> "00000" Then
            'Coberturas
            'wvarResult = wvarResult & "<COBERTURAS>"
            '
            wvarAuxCobeCicle = 1
            wvarContadorCobs = 0
            'Todos las coberturas
            Do While wvarContadorCobs < 30 'Val(wvarAuxCobeCicle) > 0 And
                'Cobertura
                'wvarResult = wvarResult & "<COBERTURA>"
                    wvarResult = wvarResult & "<COBERCOD" & (wvarContadorCobs + 1) & ">" & Trim(Mid(strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 0 + 1, 3)) & "</COBERCOD" & (wvarContadorCobs + 1) & ">"
                    wvarResult = wvarResult & "<COBERORD" & (wvarContadorCobs + 1) & ">" & Trim(Mid(strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 3 + 1, 2)) & "</COBERORD" & (wvarContadorCobs + 1) & ">"
                    wvarResult = wvarResult & "<CAPITASG" & (wvarContadorCobs + 1) & ">" & FormatoDec(Mid(strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 5 + 1, 15)) & "</CAPITASG" & (wvarContadorCobs + 1) & ">"
                    wvarResult = wvarResult & "<CAPITIMP" & (wvarContadorCobs + 1) & ">" & FormatoDec(Mid(strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 20 + 1, 15)) & "</CAPITIMP" & (wvarContadorCobs + 1) & ">"
                'wvarResult = wvarResult & "</COBERTURA>"
                '
                wvarContadorCobs = wvarContadorCobs + 1
                '
                'Seleccionar la pr�xima cobertura (si es cero cortar el ciclo)
            wvarAuxCobeCicle = Trim(Mid(strParseString, mcteInicioTbDECA + (wvarContadorCobs * mcteTamanoTbDECA) + 0 + 1, 3))
            Loop
            'wvarResult = wvarResult & "</COBERTURAS>"
        End If
        '~~~~~~~~~~~~~~
        'Fin Coberturas
        '~~~~~~~~~~~~~~
        '
        wvarStep = 230
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & wvarResult & "</Response>"
    End If
    '~~~~~~~~~~~~~~~~~~~~~~~~
    'Fin Response del Mensaje
    '~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 240
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
ClearObjects:
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
    '
'~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & wvarMensaje & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Function Formateo_Telefono(pvarTelefono As String) As String
Dim wvarNewTelef As String
    '
    If Trim(Left(pvarTelefono, 4)) = "0800" Then
        wvarNewTelef = Trim(Left(pvarTelefono, 4)) & "-" & Mid(pvarTelefono, 5, 3) & "-" & Mid(pvarTelefono, 8, Len(pvarTelefono))
    Else
        wvarNewTelef = pvarTelefono
    End If
    '
    Formateo_Telefono = wvarNewTelef
End Function

Private Sub ObjectControl_Activate()
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   ObjectControl_CanBePooled = True
End Function

Private Sub ObjectControl_Deactivate()
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
End Sub

Private Function FormatoDec(pvarNumero As String) As String
    FormatoDec = Val(Left(pvarNumero, Len(pvarNumero) - 2)) & "," & Right(pvarNumero, 2)
End Function

Private Function CargarArchivoPrueba() As String
Dim w_strLinea      As String
Dim w_strCadena     As String
    '
    Open App.Path & "\Mensaje047.txt" For Input As #1
    w_strCadena = ""
    w_strLinea = ""
    Do While Not EOF(1)
        Line Input #1, w_strLinea
        w_strCadena = w_strCadena & w_strLinea
    Loop
    Close #1
    
    CargarArchivoPrueba = w_strCadena
End Function

Private Sub GuardarLog(p_strCadena As String)
    Open App.Path & "\Mensaje047.log" For Output As #1
    Print #1, p_strCadena
    Close #1
End Sub
