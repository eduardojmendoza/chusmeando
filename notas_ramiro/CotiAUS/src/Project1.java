import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;
import java.applet.*;

public class Project1 extends JApplet
{
  static {
    try {
      UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
    }
    catch (Exception e) { System.out.println(e); }
  }
  public static String Title = "";
  public static String ProductName = "";
  public static int MajorVersion = 1;
  public static int MinorVersion = 0;
  public static int Revision = 0;
  public static String HelpFile = "";
  public static String Comments = "";
  public static String FileDescription = "";
  public static String CompanyName = "GT";
  public static String LegalCopyright = "";
  public static String LegalTrademarks = "";

  public Project1()
  {
  }

  // called only when running as an applet
  public void init()
  {
    getContentPane().setLayout( new java.awt.BorderLayout() );
    Application app = new Application( "Project1" );
  }

  public String getAppletInfo()
  {
    return "Project1" + " " + LegalCopyright;
  }

  // called only when running as a stand-alone application
  public static void main( String args[] )
  {
    final Application app = new Application( "Project1" );
    app.setApplication( new Project1(), args );
    try
    {
    }
    catch(Exception e) { Err.set(e); }
    app.endApplication();
    javax.swing.SwingUtilities.invokeLater( new Runnable() {
      public void run() {
        Application.setApplication( app );
      }
    });
  }
}
