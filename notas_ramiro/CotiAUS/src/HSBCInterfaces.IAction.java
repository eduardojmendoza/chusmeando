import diamondedge.util.*;
import diamondedge.ado.*;
import diamondedge.vb.*;
import java.awt.*;
import javax.swing.*;
import diamondedge.swing.*;

public interface HSBCInterfaces.IAction
{
  public Variant Execute( String arg1, String arg2, String arg3 ) throws Exception;
}
