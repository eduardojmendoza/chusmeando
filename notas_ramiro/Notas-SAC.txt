sacA_Clientes.VerificarProdPendEnt

Es una clase que invoca StoreProcedures. Obtiene los parámetros del XML de entrada, como siempre. Puede invocar a 3 SPs distintos dependiendo de un parámetro.
Obtiene conexión a las bases de datos de componentes HSBC (HSBCInterfaces.IDBConnection ), después usa ADO
Itera el resultSet para armar el XML de retorno. Todas esta lógica a mano, con constantes cableadas, strings en el código etc.
427 lineas


sacA_Clientes.VerificacionDomi
Ejecuta SP, arma resultados a mano. 
202 lineas


sacA_Clientes.TarjetaBanelco
Va contra una base mCte_COMISIONESDB (Public Const mCte_COMISIONESDB = "COMISIONES.UDL" ). Ejecuta P_SAC_TARJETASBANELCO
Procesa resultset simple, pero a mano
230 lineas

sacA_Clientes.Productos
Similar al anterior
215 lineas

sacA_Clientes.ObtenerSectorSucCliente
similar 
190 lineas

sacA_Clientes.Ficha
similar, un poco más de parámetros en la vuelta
244 lineas

sacA_Clientes.Documentos
similar al anterior
233 líneas

sacA_Clientes.BuscarProductosVendedor
similar, con un XSL
259



