FIX Logging

Los nuevos convertidos tienen solamente esto, que  NO CORTA la ejecución:
    catch( Exception _e_ )
    {
      Err.set( _e_ );
      try 
      {
        //~~~~~~~~~~~~~~~
        //
        wobjXMLResponse = null;
        wobjXSLResponse = null;
        //
        mobjEventLog.Log( ErrorConstants.getValue("EventLog_Category.evtLog_Category_Unexpected"), mcteClassName, wcteFnName, wvarStep, Err.getError().getNumber(), "Error= [" + Err.getError().getNumber() + "] - " + Err.getError().getDescription() + " Mensaje:" + mcteOpID + wvarCiaAsCod + wvarUsuario + wvarNivelAs + wvarClienSecAs + wvarNivel1 + wvarClienSec1 + wvarNivel2 + wvarClienSec2 + wvarNivel3 + wvarClienSec3 + wvarContinuar + " Hora:" + DateTime.now(), vbLogEventTypeError );
        IAction_Execute = 1;
        /*TBD mobjCOM_Context.SetAbort() ;*/
        Err.clear();
        return IAction_Execute;
      }


Ah no ojo, que lo que hago en AbstractMQMEnsaje es solamente agregarle una linea de log, pero sigo. Lo que pasa es que sigue y ya retorna, mientras que en el 0047 no se fija si hay error, y por eso no corta. Debería cortar el cotiaus???

		catch( Exception _e_ )
		{
			Err.set( _e_ );
			logger.log(Level.SEVERE, "Exception al ejecutar el request", _e_);
			try 

Uf en algunas clases quedaron como unsup cosas como "          //unsup Resume ClearObjects"

Ok vamos con este:
    catch\( Exception _e_ \)(\s*)\{(\s*)Err\.set\( _e_ \);(\s*)try(\s*)\{
reemplaza por:
	

En:
/Users/ramiro/dev/migracioncomplus/dev/CMS-LBAVIRTUAL/src/main/java/com/qbe/services/ofvirtuallba/impl/
	lbaw_GetNroCot.java	
	lbaw_GetParamGral.java
	lbaw_GetPortalComerc.java
/Users/ramiro/dev/migracioncomplus/dev/CMS-LBAWEBMQ/src/main/java/com/qbe/services/webmq/impl
	lbaw_GetSumAseg.java
/Users/ramiro/dev/migracioncomplus/dev/CMS-nbwsA_Transacciones/src/main/java/com/qbe/services/transacciones/impl/
	ModGeneral.java
/Users/ramiro/dev/migracioncomplus/dev/CMS-ovmq/src/main/java/com/qbe/services/ovmq/impl
	lbaw_OVAnulxPagoDetalles.java
	lbaw_OVAnulxPagoTotales.java
	lbaw_OVAvisoVtoPolDetalles.java
	lbaw_OVAvisoVtoPolTotales.java
	etc son todas iguales

/Users/ramiro/dev/migracioncomplus/dev/CMS-OVMQCotizar/src/main/java/com/qbe/services/ovmqcotizar/impl/lbaw_OVGetCotiAUS
	lbaw_OVGetCotiAUS.java
/Users/ramiro/dev/migracioncomplus/dev/CMS-OVMQEmision/src/main/java/com/qbe/services/ovmqemision/impl
	lbaw_GetCertMercosur.java
	varias
/Users/ramiro/dev/migracioncomplus/dev/CMS-OVMQUsuario/src/main/java/com/qbe/services/ovmqusuario/impl
	varias

En estos no matcheó, como lo maneja?
	CMS-lbawA_OVCotEndosos
	CMS-lbawA_SiniestroDenuncia
		no tienen porque son genéricos, entonces heredan

Listo
