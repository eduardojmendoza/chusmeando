/**
 * 
 */
package com.qbe.services.rightnow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * @author Mauro Plaquin
 *
 */
@WebService(targetNamespace = "http://rightnow.services.qbe.com/",
		// endpointInterface = "com.qbe.services.birt.webservice.BirtReports",
		portName = "RightNowPort", serviceName = "RightNowWS")
public class RightNowImpl {

	static final String METODOGET = "GET";
	static final String URL_PROXY = "10.152.140.252";
	static final int PORT_PROXY = 8080;
	static final String NAME_COL_ID = "id";
	static final String URL_SINIESTRO = "url.rn.siniestro"; // https://qbe.custhelp.com/services/rest/connect/v1.3/Qbe.Siniestro
	static final String URL_POLIZA = "url.rn.poliza";// https://qbe.custhelp.com/services/rest/connect/v1.3/Qbe.Poliza
	static final String URL_INCIDENTS = "url.rn.incident"; // https://qbe.custhelp.com/services/rest/connect/v1.3/incidents
	static final String URL_CONTACT = "url.rn.contact"; // "https://qbe.custhelp.com/services/rest/connect/v1.3/contacts"
	private static final String URL_SENDEMAIL = null;
	protected static Logger logger = Logger.getLogger(RightNowImpl.class.getName());
	private String user = "";
	private String pass = "";
	private static String propsFilename = System.getProperty("crm.config.properties");
	//private static String propsFilename = "crm-prod.properties";

	public RightNowImpl() {
		super();
	}

	/**
	 * @param dniContacto
	 * @param numPoliza
	 * @param numSiniestro
	 * @param producto
	 * @param oficina
	 * @param idProducto
	 * @param idModoContacto
	 * @param idTipoIncidente
	 * @param nameNota
	 * @param idCanal
	 * @param textNota
	 * @param idEstado
	 * @param idTipoEstado
	 * @param idCategory
	 * @param idQueue
	 * @param fechaEfecto
	 * @param usuarioIntra     
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	@WebMethod(operationName = "crearIncidente", action = "crearIncidente")
	public String crearIncidente(@WebParam(name = "dni") String dniContacto, @WebParam(name = "tipoDocumento") String tipoDocumento, @WebParam(name = "usuario") String user,
			@WebParam(name = "password") String password, @WebParam(name = "numPoliza") String numPoliza,
			@WebParam(name = "numSiniestro") String numSiniestro, @WebParam(name = "Producto") String producto,
			@WebParam(name = "Oficina") String oficina, @WebParam(name = "idProduct") Long idProducto,
			@WebParam(name = "idModoContacto") Long idModoContacto,
			@WebParam(name = "idTipoIncidente") Long idTipoIncidente, @WebParam(name = "nameNota") String nameNota,
			@WebParam(name = "idChannel") Long idCanal, @WebParam(name = "text") String textNota,
			@WebParam(name = "idEstado") Long idEstado, @WebParam(name = "idCategory") Long idCategory,
			@WebParam(name = "idQueue") Long idQueue, @WebParam(name = "fechaEfecto") String fechaEfecto, @WebParam(name = "usuarioIntra") String usuarioIntra) throws ParseException, IOException {

		logger.log(Level.FINE, "RECIBIDO_1_usuarioIntra: " + usuarioIntra);//ADD
		
		this.setUser(user);
		this.setPass(password);
		Long idContacto = obtenerIDContacto(dniContacto, tipoDocumento);
		// isVinculadoPoliza();
		Long idPoliza = getIdPoliza(numPoliza);
		if (idPoliza == null) {
			idPoliza = insertarPoliza(numPoliza);
		}
		Long idSiniestro = getIdSiniestro(numSiniestro);
		if (idSiniestro == null) {
			idSiniestro = insertarSiniestro(numSiniestro, producto, oficina);
		}
		
		
		String jsonIncidenteString = insertarIncidente(idContacto, idPoliza, idSiniestro, idProducto, idModoContacto,
				idTipoIncidente, nameNota, idCanal, textNota, idEstado, idCategory, idQueue, fechaEfecto, usuarioIntra);
		return jsonIncidenteString;
		

	}

	/**
	 * @param numSiniestro
	 * @param producto
	 * @param oficina
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private Long insertarSiniestro(String numSiniestro, String producto, String oficina)
			throws ParseException, IOException {
		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_SINIESTRO));
		JSONObject outputJson = new JSONObject();
		JSONObject inputJson = new JSONObject();
		JSONParser parser = new JSONParser();
		inputJson.put("NumeroSiniestro", numSiniestro);
		if (!producto.isEmpty()) {
			inputJson.put("Producto", producto);
		}
		if (!oficina.isEmpty()) {
			inputJson.put("Oficina", oficina);
		}
		Map<String,String> map = new HashMap<String, String>();
		outputJson = (JSONObject) parser.parse(insertarJson(urlService.toString(), inputJson, map));

		return (Long) outputJson.get(NAME_COL_ID);
	}

	/**
	 * @param numSiniestro
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	private Long getIdSiniestro(String numSiniestro) throws IOException, ParseException {
		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_SINIESTRO));
		JSONObject outJson = null;
		urlService.append("?");
		urlService.append("q=numeroSiniestro=\'");
		urlService.append(numSiniestro);
		urlService.append("\'");
		outJson = obtenerJson(urlService.toString());
		JSONArray arrayObject = (JSONArray) outJson.get("items");
		if (arrayObject.isEmpty()) {
			return null;
		}
		return (Long) ((JSONObject) arrayObject.get(0)).get("id");

	}

	/**
	 * @param numPoliza
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	private Long insertarPoliza(String numPoliza) throws IOException, ParseException {
		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_POLIZA));
		JSONObject outputJson = new JSONObject();
		JSONObject inputJson = new JSONObject();
		JSONParser parser = new JSONParser();
		inputJson.put("Numero", numPoliza);
		Map<String,String> map = new HashMap<String, String>();
		outputJson = (JSONObject) parser.parse(insertarJson(urlService.toString(), inputJson, map));

		return (Long) outputJson.get(NAME_COL_ID);

	}

	/**
	 * @param numPoliza
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	private Long getIdPoliza(String numPoliza) throws IOException, ParseException {
		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_POLIZA));
		JSONObject outJson = null;
		urlService.append("?");
		urlService.append("q=numero=\'");
		urlService.append(numPoliza);
		urlService.append("\'");
		outJson = obtenerJson(urlService.toString());
		JSONArray arrayObject = (JSONArray) outJson.get("items");
		if (arrayObject.isEmpty()) {
			return null;
		}
		return (Long) ((JSONObject) arrayObject.get(0)).get("id");

	}

	/**
	 * 
	 * * {"product":{"lookupName":"AUS1 - AUTOMOTORES"}, "customFields":{
	 * "c":{}, "Qbe":{ "ModoContacto":{"ID":18}, "TipoIncidente":{"ID":6},
	 * "Siniestro":{"ID":3119}, "Poliza":{"ID":58177}, "FechaEfecto":"2017-10-05"} },
	 * "primaryContact":{"id":382275}, "threads":[
	 * {"entryType":{"lookupName":"Nota"}, "channel":{"id":6}, "text":"Texto"}],
	 * "statusWithType":{"status":{"lookupName":"Ingresado"}},
	 * "category":{"id":186}, "queue":{"lookupName":"Cac"}}
	 */
	/**
	 * @param idContacto
	 * @param idPoliza
	 * @param idSiniestro
	 * @param idProducto
	 * @param idModoContacto
	 * @param idTipoIncidente
	 * @param nameNota
	 * @param idCanal
	 * @param textNota
	 * @param idEstado
	 * @param idTipoEstado
	 * @param idCategory
	 * @param idQueue
	 * @param fechaEfecto 
	 * @param usuarioIntra
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private String insertarIncidente(Long idContacto, Long idPoliza, Long idSiniestro, Long idProducto,
			Long idModoContacto, Long idTipoIncidente, String nameNota, Long idCanal, String textNota, Long idEstado,
			Long idCategory, Long idQueue, String fechaEfecto, String usuarioIntra) throws IOException {

		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_INCIDENTS));
		JSONObject inputJson = new JSONObject();
		JSONArray arrayObject = new JSONArray();
		JSONObject firstObject, secondObject = new JSONObject();
		JSONObject thirdObject = new JSONObject();//ADD
		inputJson.put("product", setMapJson("id", idProducto));
		firstObject = new JSONObject();
		firstObject.put("c", new JSONObject());
		// secondObject.put("ModoContacto", new JSONObject(new HashMap<String,
		// Long>()).put("id", idModoContacto));
		secondObject.put("ModoContacto", setMapJson(NAME_COL_ID, idModoContacto));
		secondObject.put("TipoIncidente", setMapJson(NAME_COL_ID, idTipoIncidente));
		secondObject.put("Siniestro", setMapJson(NAME_COL_ID, idSiniestro));
		secondObject.put("Poliza", setMapJson(NAME_COL_ID, idPoliza));
		logger.log(Level.FINE, "RECIBIDO_2:_usuarioIntra " + usuarioIntra);//ADD
		thirdObject.put("UsuarioIntra", usuarioIntra);//ADD
		logger.log(Level.FINE, "RECIBIDO_3_thirdObject: " + thirdObject.toString());//ADD
		
		if (!fechaEfecto.isEmpty()) {
		   secondObject.put("FechaEfecto", fechaEfecto); 
		}
		
		logger.log(Level.FINE, "RECIBIDO_4_usuarioIntra: " + usuarioIntra);//ADD
		
		firstObject.put("Qbe", secondObject);
		
		if(!usuarioIntra.isEmpty()){
		 logger.log(Level.FINE, "RECIBIDO_5_IF_usuarioIntra: " + usuarioIntra);//ADD
		 firstObject.put("Qbe2", thirdObject);//ADD
		 logger.log(Level.FINE, "RECIBIDO_6_IF_usuarioIntra: " + usuarioIntra);//ADD
		 logger.log(Level.FINE, "RECIBIDO_7_IF_thirdObject: " + thirdObject.toString());//ADD
		}
		inputJson.put("customFields", firstObject);
		
		logger.log(Level.FINE, "RECIBIDO_8_firstObject: " + firstObject.toString());//ADD

		inputJson.put("primaryContact", setMapJson("id", idContacto));

		arrayObject = new JSONArray();
		firstObject = new JSONObject();
		arrayObject.add(firstObject);
		firstObject.put("entryType", setMapJson("lookupName", nameNota));
		firstObject.put("channel", setMapJson("id", idCanal));
		firstObject.put("text", textNota);
		inputJson.put("threads", arrayObject);

		firstObject = new JSONObject();
		firstObject.put("status", setMapJson("id", idEstado));
		// firstObject.put("statusType", setMapJson("id", idTipoEstado));
		inputJson.put("statusWithType", firstObject);

		inputJson.put("category", setMapJson("id", idCategory));

		inputJson.put("queue", setMapJson("id", idQueue));
		
		logger.log(Level.FINE, "RECIBIDO_9:_inputJson " + inputJson.toJSONString());//ADD
		
		Map<String,String> map = new HashMap<String, String>();
		return insertarJson(urlService.toString(), inputJson, map);
	}

	/**
	 * @param idProducto
	 * @param inputJson
	 * @return
	 */
	private JSONObject setMapJson(String key, Object value) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
		objectMap.put(key, value);
		return new JSONObject(objectMap);
	}

	/**
	 * @param dni
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	private Long obtenerIDContacto(String dni, String tipoDocumento) throws IOException, ParseException {
		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_CONTACT));
		JSONObject contactJson = null;
		urlService.append("?");
		urlService.append("q=customFields.Qbe.Dni=%27");
		urlService.append(dni);
		urlService.append("%27");
		if (tipoDocumento!=null){
			urlService.append("%20and%20");
			urlService.append("customFields.Qbe.TipoDocumento=%27");
			urlService.append(tipoDocumento);		
			urlService.append("%27");
		}
		contactJson = obtenerJson(urlService.toString());
		contactJson = (JSONObject) ((JSONArray) contactJson.get("items")).get(0);

		return (Long) contactJson.get("id");

	}

	/**
	 * @param urlService
	 * @param tipoMetodo
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@SuppressWarnings({ "restriction" })
	private JSONObject obtenerJson(String urlService) throws IOException, ParseException {
		URL url;
		StringBuilder jsonString = new StringBuilder();
		JSONObject json = null;
		JSONParser parser = new JSONParser();

		url = new URL(urlService);
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(URL_PROXY, PORT_PROXY));
		HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
		//HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(METODOGET);
		conn.setRequestProperty("Accept", "application/json");
		String userPassword = this.getUser() + ":" + this.getPass();
		String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
		conn.setRequestProperty("Authorization", "Basic " + encoding);
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " "
					+ conn.getResponseMessage() + " - " + conn.getURL());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			jsonString.append(output + '\n');
		}

		conn.disconnect();
		json = (JSONObject) parser.parse(jsonString.toString());

		return json;
	}

	/**
	 * @param urlService
	 * @param jsonEntidad
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings({ "unused", "restriction" })
	private String insertarJson(String urlService, JSONObject jsonEntidad, Map<String, String> mapProperties) throws IOException {
		URL url;
		StringBuilder jsonString = new StringBuilder();
		JSONObject json = null;

		logger.log(Level.FINE, "RECIBIDO_11:Entre al metodo insertar Json jsonentida" + jsonEntidad.toJSONString());//ADD
		logger.log(Level.FINE, "RECIBIDO_12:Entre al metodo insertar Json jsonentida" + jsonEntidad.toString());//ADD
		
		JSONParser parser = new JSONParser();

		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(URL_PROXY, PORT_PROXY));
		url = new URL(urlService);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
		//HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		//conn.setRequestProperty("Content-Type	", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setDoOutput(Boolean.TRUE);
		String userPassword = this.getUser() + ":" + this.getPass();
		String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
		conn.setRequestProperty("Authorization", "Basic " + encoding);
		for (String key : mapProperties.keySet()) {
			conn.setRequestProperty(key, mapProperties.get(key));
	    }
		OutputStream os = conn.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
		osw.write(jsonEntidad.toJSONString());
		logger.log(Level.FINE, "RECIBIDO_13:Antes del flush" + jsonEntidad.toJSONString());//ADD
		osw.flush();
		logger.log(Level.FINE, "RECIBIDO_14:Despues del flush" + jsonEntidad.toJSONString());//ADD
		
		osw.close();
		logger.fine(jsonEntidad.toJSONString() + " " + conn.getURL());
		if (conn.getResponseCode() != 201 & conn.getResponseCode() != 200) {
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				jsonString.append(output +'\n');
			}
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " "
					+ conn.getResponseMessage() + " - " +jsonString.toString()+ "_" + conn.getURL() + " " + jsonEntidad);
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output;
		while ((output = br.readLine()) != null) {
			jsonString.append(output +'\n');
		}

		conn.disconnect();
		// json = (JSONObject) parser.parse(jsonString.toString());

		return jsonString.toString();
	}

	@WebMethod(operationName = "sendEmailIncidente", action = "sendEmailIncidente")
	public String sendEmailIncidente(@WebParam(name = "tipoDocumento") String tipoDocumento,
			@WebParam(name = "nroDni") String nroDni, @WebParam(name = "idMailing") Long idMailing,
			@WebParam(name = "idIncidente") Long idIncidente, @WebParam(name = "texto") String textoMail,
			@WebParam(name = "usuario") String user, @WebParam(name = "password") String password)
			throws ParseException, IOException {

		this.setUser(user);
		this.setPass(password);
		Long idContacto = obtenerIDContacto(nroDni.toString(), tipoDocumento);
		enviarEmail(idContacto, idMailing, idIncidente);
		insertarMsjIncidente(idIncidente, textoMail);
		return "El envio se ha completado correctamente";
	}

	/**
	 * @param idIncidente
	 * @param textoMail
	 * @throws IOException 
	 */
	private void insertarMsjIncidente(Long idIncidente, String textoMail) throws IOException{

		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_INCIDENTS));
		urlService.append("/");
		urlService.append(idIncidente);
		
		JSONObject inputJson = new JSONObject();
		JSONArray arrayObject = new JSONArray();
		JSONObject firstObject = new JSONObject();
		
		arrayObject = new JSONArray();
		firstObject = new JSONObject();
		
		firstObject.put("entryType", setMapJson("lookupName", "Nota"));
		firstObject.put("channel", setMapJson("id", new Integer(6)));
		firstObject.put("text", textoMail);
		arrayObject.add(firstObject);
		inputJson.put("threads", arrayObject);
		Map<String,String> map = new HashMap<String, String>();
		map.put("X-HTTP-Method-Override","PATCH");
		try {
			insertarJson(urlService.toString(), inputJson, map);
		} catch (IOException e) {
			throw new RuntimeException("Fallo al insertar el mensaje al hilo del incidente: " +e.getMessage());
		}
		logger.info("Envio de correo correcto");
	}

	
	
	/**
	 * @param idContacto
	 * @param idMailing
	 * @param idIncidente
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private void enviarEmail(Long idContacto, Long idMailing, Long idIncidente) throws IOException {

		StringBuilder urlService = new StringBuilder(loadConfigKey(URL_CONTACT));
		urlService.append("/");
		urlService.append(idContacto);
		urlService.append("/");
		urlService.append("sendMailing");
		JSONObject inputJson = new JSONObject();
		inputJson.put("mailing", setMapJson("id", idMailing.intValue()));
		inputJson.put("incident", setMapJson("id", idIncidente.intValue()));
		Map<String,String> map = new HashMap<String, String>();
		try {
			insertarJson(urlService.toString(), inputJson,  map);
		} catch (IOException e) {
			throw new RuntimeException("Fallo el envio de correo: " +e.getMessage() + " - " + urlService + " " + inputJson);
		}
		logger.info("Envio de correo correcto");

	}

	/**
	 * M�todo que retorna el valor de la key que esta dentro del archivo de
	 * propiedades
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	private String loadConfigKey(String key) throws IOException {
		Properties fileProper = new Properties();
		InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(propsFilename);
		fileProper.load(in);
		in.close();
		return fileProper.getProperty(key);

	}

	private String getUser() {
		return user;
	}

	private void setUser(String user) {
		this.user = user;
	}

	private String getPass() {
		return pass;
	}

	private void setPass(String pass) {
		this.pass = pass;
	}

}