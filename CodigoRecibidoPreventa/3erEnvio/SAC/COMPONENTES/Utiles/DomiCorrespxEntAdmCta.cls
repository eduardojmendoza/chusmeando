VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DomiCorrespxEntAdmCta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
' LIMITED 2009. ALL RIGHTS RESERVED
'
' This software is only to be used for the purpose for which it has
' been provided. No part of it is to be reproduced, disassembled,
' transmitted, stored in a retrieval system or translated in any
' human or computer language in any way or for any other purposes
' whatsoever without the prior written consent of the Hong Kong and
' Shanghai Banking Corporation Limited. Infringement of copyright
' is a serious civil and criminal offence, which can result in
' heavy fines and payment of substantial damages.
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' Nombre Modulo : DomiCorrespxEntAdmCta
' Fec. Creaci�n : 23/09/2009
' Desarrollador : Claudia Escudero
' Descripci�n   : Trae el domicilio de correspondencia de un cliente en Adintar por
'                 Entidad administradora y n�mero de cuenta
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.DomiCorrespxEntAdmCta"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstDomiCorresp    As ADODB.Recordset
    Dim wvarEntAdm      As String
    Dim wvarNumCta      As String
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    LogTrace "sacA_Utiles.DomiCorrespxEntAdmCta"
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEntAdm = wobjXMLrequest.selectSingleNode("//Request/ENTADM").Text
    wvarNumCta = wobjXMLrequest.selectSingleNode("//Request/NROPRODUCTO").Text
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteADINTAR_DBCnn)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "P_busca_correspondencia_sac"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Ent_Adm"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = CInt(wvarEntAdm)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@scod_cue_ext"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 20
    wobjDBParm.Value = CDbl(wvarNumCta)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@mensaje"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 100
    wobjDBParm.Value = ""
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wrstDomiCorresp = wobjDBCmd.Execute
    '
    wvarStep = 95
    '
    If wrstDomiCorresp.State = adStateClosed Then
        pvarResponse = "<Response><ESTADO RESULTADO=""FALSE"" MENSAJE=""NO SE ENCONTRO EL DOMICILIO"" CODIGO=""""/></Response>"
    Else
        If Not wrstDomiCorresp.EOF Then
            pvarResponse = "<Response>"
            pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE="""" CODIGO=""""/>"
            pvarResponse = pvarResponse & "<DOMICORRESP>"
'CLAES 06/07/2010 INI
'            pvarResponse = pvarResponse & "<CALLE>" & Replace(ReplaceBadCaracters(wrstDomiCorresp.fields("calle")), "&", "") & "</CALLE>"
'            pvarResponse = pvarResponse & "<NRO>" & wrstDomiCorresp.fields("Nro") & "</NRO>"
'            pvarResponse = pvarResponse & "<PISO>" & wrstDomiCorresp.fields("piso") & "</PISO>"
'            pvarResponse = pvarResponse & "<DEPTO>" & wrstDomiCorresp.fields("depto") & "</DEPTO>"
'            pvarResponse = pvarResponse & "<CODPOS>" & wrstDomiCorresp.fields("codigo_postal") & "</CODPOS>"
'            pvarResponse = pvarResponse & "<LOCALIDAD>" & ReplaceBadCaracters(wrstDomiCorresp.fields("localidad")) & "</LOCALIDAD>"
'            pvarResponse = pvarResponse & "<COD_PROVINCIA>" & wrstDomiCorresp.fields("cod_provincia") & "</COD_PROVINCIA>"
'            pvarResponse = pvarResponse & "<PROVINCIA>" & ReplaceBadCaracters(wrstDomiCorresp.fields("provincia")) & "</PROVINCIA>"
'            pvarResponse = pvarResponse & "<COD_PAIS>" & wrstDomiCorresp.fields("cod_pais") & "</COD_PAIS>"
'            pvarResponse = pvarResponse & "<PAIS>" & wrstDomiCorresp.fields("pais") & "</PAIS>"
            
            pvarResponse = pvarResponse & "<CALLE><![CDATA[" & Replace(Replace(ReplaceBadCaracters(wrstDomiCorresp.fields("calle")), "&", "N"), "#", "N") & "]]></CALLE>"
            pvarResponse = pvarResponse & "<NRO><![CDATA[" & wrstDomiCorresp.fields("Nro") & "]]></NRO>"
            pvarResponse = pvarResponse & "<PISO><![CDATA[" & wrstDomiCorresp.fields("piso") & "]]></PISO>"
            pvarResponse = pvarResponse & "<DEPTO><![CDATA[" & wrstDomiCorresp.fields("depto") & "]]></DEPTO>"
            pvarResponse = pvarResponse & "<CODPOS><![CDATA[" & wrstDomiCorresp.fields("codigo_postal") & "]]></CODPOS>"
            pvarResponse = pvarResponse & "<LOCALIDAD><![CDATA[" & Replace(Replace(ReplaceBadCaracters(wrstDomiCorresp.fields("localidad")), "&", "N"), "#", "N") & "]]></LOCALIDAD>"
            pvarResponse = pvarResponse & "<COD_PROVINCIA><![CDATA[" & wrstDomiCorresp.fields("cod_provincia") & "]]></COD_PROVINCIA>"
            pvarResponse = pvarResponse & "<PROVINCIA><![CDATA[" & ReplaceBadCaracters(wrstDomiCorresp.fields("provincia")) & "]]></PROVINCIA>"
            pvarResponse = pvarResponse & "<COD_PAIS><![CDATA[" & wrstDomiCorresp.fields("cod_pais") & "]]></COD_PAIS>"
            pvarResponse = pvarResponse & "<PAIS><![CDATA[" & ReplaceBadCaracters(wrstDomiCorresp.fields("pais")) & "]]></PAIS>"
'CLAES 06/07/2010 FIN
            pvarResponse = pvarResponse & "</DOMICORRESP>"
            pvarResponse = pvarResponse & "</Response>"
        Else
            pvarResponse = "<Response><ESTADO RESULTADO=""FALSE"" MENSAJE=""Sin datos de domicilio particular"" CODIGO=""""/></Response>"
        End If
    End If
    '
    wvarStep = 100
    Set wrstDomiCorresp = Nothing
    '
    wvarStep = 110
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 150
    
    LogTrace "FIN - sacA_Utiles.DomiCorrespxEntAdmCta - Step: " & wvarStep & " - Respuesta: " & pvarResponse

    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<Response><ESTADO RESULTADO=""FALSE"" MENSAJE="" ERROR: " & Err.Description & ", Paso : " & wvarStep & """ CODIGO=""01""/></Response>"
    LogTrace "ERROR : " & Err.Description & " - sacA_Utiles.DomiCorrespxEntAdmCta - Step: " & wvarStep & " - Respuesta: " & pvarResponse
    mobjCOM_Context.SetComplete
'    If Err.Number = mCte_TimeOutError Then
'        IAction_Execute = xERR_TIMEOUT_RETURN
'        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
'                         mcteClassName, _
'                         wcteFnName, _
'                         wvarStep, _
'                         Err.Number, _
'                         "Error= [" & Err.Number & "] - " & Err.Description, _
'                         vbLogEventTypeError
'    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
'    End If
'    mobjCOM_Context.SetAbort
End Function










