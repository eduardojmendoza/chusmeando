VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Administradoras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'CLAES 20090629
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.Administradoras"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstAdministradoras   As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim wvarNroPedido       As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    LogTrace " - sacA_Utiles.Administradoras -"
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    App.LogEvent pvarRequest
    wvarNroPedido = wobjXMLrequest.selectSingleNode("//TRANSAC/NROPEDIDO").Text
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteADINTAR_DBCnn)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "adp_adso0038_1"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Administradoras - Step: " & wvarStep
    wvarStep = 70
    Set wrstAdministradoras = wobjDBCmd.Execute
    Set wrstAdministradoras.ActiveConnection = Nothing
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Administradoras - Step: " & wvarStep
    wvarStep = 80
    wobjDBCnn.Close
    '
    wvarStep = 90
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 100
    '*********************************************************************
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Administradoras - Step: " & wvarStep
    If Not wrstAdministradoras.EOF Then
        '
        wvarStep = 110
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 120
        wrstAdministradoras.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 130
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 140
        Call wobjXMLDocs.loadXML(wobjXMLDocs.transformNode(wobjXSLDocs))
        '
        Set wobjNodeList = wobjXMLDocs.selectNodes("//ADMINISTRADORAS/ADMINISTRADORA")
        LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Administradoras - Step: " & wvarStep
        For Each wobjNode In wobjNodeList
          wobjNode.selectSingleNode("CODIGO").Text = "<![CDATA[" & wobjNode.selectSingleNode("COD_ADM").Text & "]]>"
          wobjNode.selectSingleNode("DESCRIPCION").Text = "<![CDATA[" & wobjNode.selectSingleNode("DESC_ADM").Text & "]]>"
        Next
        '
        wobjXMLDocs.loadXML "<SUCURSALES><ESTADO RESULTADO='TRUE' MENSAJE='' /></SUCURSALES>"
        pvarResponse = wobjXMLDocs.xml
        LogTrace "pvarResponse: " & pvarResponse & " - sacA_Utiles.Administradoras - Step: " & wvarStep
        Set wobjNode = Nothing
        Set wobjNodeList = Nothing
        Set wobjXMLDocs = Nothing
        Set wobjXSLDocs = Nothing
        Else
            LogTrace "ERROR: Tabla de Administradoras Vacia - sacA_Utiles.Administradoras - Step: " & wvarStep
            wobjXMLDocs.loadXML "<SUCURSALES><ESTADO RESULTADO='FALSE' MENSAJE='Tabla de Administradoras Vacia' /></SUCURSALES>"
            pvarResponse = wobjXMLDocs.xml
    End If
    '**************************************************************************
    wvarStep = 150
    wrstAdministradoras.Close
    '
    wvarStep = 160
    Set wrstAdministradoras = Nothing
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    LogTrace "ERROR: " & Err.Number & " - " & Err.Description & " - sacA_Utiles.Administradoras - Step: " & wvarStep
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ADMINISTRADORAS'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "      <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ADMINISTRADORA'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CODIGO'><xsl:value-of select='@COD_AMD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DESCRIPCION'><xsl:value-of select='@DESC_ADM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function

