VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "tipoProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.tipoProducto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName      As String = "IAction_Execute"
    Dim wvarStep          As Long
    '
    Dim wobjXMLrequest    As MSXML2.DOMDocument
    Dim wvarEmpresa       As String
    Dim wvarEmpresa2      As String
    '
    Dim wobjHSBC_DBCnn    As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn         As ADODB.Connection
    Dim wobjDBCmd         As ADODB.Command
    Dim wobjDBParm        As ADODB.Parameter
    Dim wrstTipoProducto  As ADODB.Recordset
    '
    Dim wobjXMLDocs       As MSXML2.DOMDocument
    Dim wobjXSLDocs       As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DomDocument")
    With wobjXMLrequest
      .async = False
      .loadXML (pvarRequest)
    End With
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//TIPOS/EMPRESA").Text
    wvarStep = 20
    
    On Error Resume Next
    'PARA LA INTEGRACION y NO AFECTAR EL RESTO DE SISTEMAS
    wvarEmpresa2 = wobjXMLrequest.selectSingleNode("//TIPOS/EMPRESA2").Text
    On Error GoTo 0

    
    
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
'    wvarStep = 50
'    Set wobjDBCmd = CreateObject("ADODB.Command")
'    Set wobjDBCmd.ActiveConnection = wobjDBCnn
'    wobjDBCmd.CommandText = "sp_TipoProducto"
'    wobjDBCmd.CommandType = adCmdStoredProc
'    '
'    wvarStep = 60
'    Set wobjDBParm = CreateObject("ADODB.Parameter")
'    wobjDBParm.Name = "@Empresa"
'    wobjDBParm.Direction = adParamInput
'    wobjDBParm.Type = adInteger
'    wobjDBParm.Value = wvarEmpresa
'    wobjDBCmd.Parameters.Append wobjDBParm
    
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_SAC_LIST_TIPOPRODUCTO"
    wobjDBCmd.CommandType = adCmdStoredProc
'    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "usrProducto"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 30
    wobjDBParm.Value = ""
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 64
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "Campania"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = -1
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 66
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "E1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 68
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "E2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarEmpresa2
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wrstTipoProducto = wobjDBCmd.Execute
    Set wrstTipoProducto.ActiveConnection = Nothing
    '
    wvarStep = 80
    wobjDBCnn.Close
    '
    wvarStep = 90
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 100
    '*********************************************************************
    '
    If Not wrstTipoProducto.EOF Then
        '
        wvarStep = 110
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 120
        wrstTipoProducto.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 130
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 140
        pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
    End If
    '**************************************************************************
    wvarStep = 150
    wrstTipoProducto.Close
    '
    wvarStep = 160
    Set wrstTipoProducto = Nothing
    '
    'App.LogEvent pvarResponse
    
    wvarStep = 170
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
'        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='TIPOSPRODUCTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='TIPOPRODUCTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@TIP_PROD_COD' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@TIP_PROD_DESC' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='ESTADO'><xsl:value-of select='@ESTADO' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function




