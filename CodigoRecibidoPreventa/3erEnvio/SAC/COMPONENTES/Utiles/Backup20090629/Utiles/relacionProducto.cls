VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "relacionProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.relacionProducto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wvarCodigoProducto As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstRelProducto    As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DomDocument")
    With wobjXMLrequest
      .loadXML pvarRequest
      .async = False
    End With
    wvarStep = 20
    'If Trim(wobjXMLrequest.xml) <> "" Then
    '  wvarCodigoProducto = wobjXMLrequest.selectSingleNode("//RELACIONPRODUCTO/CODIGOPRODUCTO").Text
    'End If
    'App.LogEvent "PROD : " & wvarCodigoProducto
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_relacionProducto"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    'If wvarCodigoProducto <> "" Then
    '  Set wobjDBParm = CreateObject("ADODB.Parameter")
    '  wobjDBParm.Name = "@COD_PROD"
    '  wobjDBParm.Direction = adParamInput
    '  wobjDBParm.Type = adChar
    '  wobjDBParm.Size = 4
    '  wobjDBParm.Value = wvarCodigoProducto
    '  wobjDBCmd.Parameters.Append wobjDBParm
    'End If
    '
    Set wrstRelProducto = wobjDBCmd.Execute
    Set wrstRelProducto.ActiveConnection = Nothing
    '
    wvarStep = 70
    wobjDBCnn.Close
    '
    wvarStep = 80
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 90
    '*********************************************************************
    '
    If Not wrstRelProducto.EOF Then
        '
        wvarStep = 100
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 110
        wrstRelProducto.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 120
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 130
        pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
    End If
    '**************************************************************************
    wvarStep = 140
    wrstRelProducto.Close
    '
    wvarStep = 150
    Set wrstRelProducto = Nothing
    '
    wvarStep = 160
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='RELACIONESPRODUCTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='RELACIONPRODUCTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='REP_COD_PRO'><xsl:value-of select='@REP_COD_PRO' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='REP_COD_CAU'><xsl:value-of select='@REP_COD_CAU' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='REP_COD_MOT'><xsl:value-of select='@REP_COD_MOT' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='REP_COD_SEC'><xsl:value-of select='@REP_COD_SEC' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='REP_DIA_SOL'><xsl:value-of select='@REP_DIA_SOL' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='LD_COD'><xsl:value-of select='@LD_COD' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function










