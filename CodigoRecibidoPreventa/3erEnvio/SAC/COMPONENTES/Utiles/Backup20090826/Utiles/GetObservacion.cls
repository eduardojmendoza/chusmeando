VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetObservacionES"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.GetObservaciones"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName            As String = "IAction_Execute"
    Dim wvarStep                As Long
    '
    Dim wobjXMLrequest          As MSXML2.DOMDocument
    Dim wobjXMLDocument         As MSXML2.DOMDocument
    Dim wobjXSLDocument         As MSXML2.DOMDocument
    '
    Dim wvarEmpresa             As String
    Dim wvarProducto            As String
    Dim wvarCausa               As String
    Dim wvarMotivo              As String
    '
    Dim wobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn               As ADODB.Connection
    Dim wobjDBCmd               As ADODB.Command
    Dim wobjDBParm              As ADODB.Parameter
    Dim wrstObservaciones       As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarProducto = wobjXMLrequest.selectSingleNode("//OBSERVACIONES/PRODUCTO").Text
    '
    wvarStep = 30
    wvarCausa = wobjXMLrequest.selectSingleNode("//OBSERVACIONES/CAUSA").Text
    '
    wvarStep = 40
    wvarMotivo = wobjXMLrequest.selectSingleNode("//OBSERVACIONES/MOTIVO").Text
    '
    wvarStep = 50
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//OBSERVACIONES/EMPRESA").Text
    '
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 60
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 70
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 80
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_Age_List_TemplateObservacio"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodPro"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarProducto
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodCau"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCausa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodMot"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarMotivo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 120
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 130
    Set wrstObservaciones = wobjDBCmd.Execute
    Set wrstObservaciones.ActiveConnection = Nothing
    '
    wvarStep = 140
    wobjDBCnn.Close
    '
    wvarStep = 150
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 160
    '*********************************************************************
    If Not wrstObservaciones.EOF Then
      '
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wrstObservaciones.save wobjXMLDocument, adPersistXML
      '
      Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
      wobjXSLDocument.async = False
      wobjXSLDocument.loadXML (p_GetXSL)
      '
      Call wobjXMLDocument.loadXML(wobjXMLDocument.transformNode(wobjXSLDocument))
      'ACA SE AGREGA EL TAG CDATA A LAS OBSERVACIONES
      wobjXMLDocument.selectSingleNode("//OBSERVACIONES/OBSERVACION").Text = "<![CDATA[" & wobjXMLDocument.selectSingleNode("//OBSERVACIONES/OBSERVACION").Text & "]]>"
      '
      pvarResponse = wobjXMLDocument.xml
      Set wobjXMLDocument = Nothing
      Set wobjXSLDocument = Nothing
      '
    Else
      pvarResponse = "<OBSERVACIONES><OBSERVACION /></OBSERVACIONES>"
    End If
    '
    wvarStep = 170
    '*********************************************************************
    '
    wvarStep = 180
    If wrstObservaciones.State <> 0 Then wrstObservaciones.Close
    '
    wvarStep = 190
    Set wrstObservaciones = Nothing
    '
    wvarStep = 200
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSL() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='OBSERVACIONES'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='OBSERVACION'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:value-of select='@rep_obs' />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function



