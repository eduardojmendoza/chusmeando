VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PaginaOperAuto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'CLAES 20090714
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.PaginaOperAuto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstPagOperAuto   As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim wvarNroPedido       As String
    Dim wvarCodMotivo       As String
    Dim wvarCodCausa        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    LogTrace " - sacA_Utiles.PaginaOperAuto -"
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    App.LogEvent pvarRequest
    wvarNroPedido = wobjXMLrequest.selectSingleNode("//TRANSAC/NROPEDIDO").Text
    wvarCodMotivo = wobjXMLrequest.selectSingleNode("//TRANSAC/CODMOTIVO").Text
    wvarCodCausa = wobjXMLrequest.selectSingleNode("//TRANSAC/CODCAUSA").Text
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "P_SAC_OPERACIONES_AUTOMATIZADAS_CONSULTA"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_MOT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodMotivo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 65
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_CAU"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodCausa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    LogTrace "Pedido: " & wvarNroPedido & " CodMotivo: " & wvarCodMotivo & " CodCausa: " & wvarCodCausa & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    wvarStep = 70
    Set wrstPagOperAuto = wobjDBCmd.Execute
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    wvarStep = 80
    '*********************************************************************
   '
    '*********************************************************************
'   <RESPONSE>
'        <ESTADO>
     '       <RESULTADO></RESULTADO>
      '      <MENSAJE></MENSAJE>
      '       <CODIGO></CODIGO>
      '   </ESTADO>
        '    <PAGINAOPERAUTO>
'                <DESCRIPCION></DESCRIPCION>
    '        </PAGINAOPERAUTO>
'   </RESPONSE>
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    If wrstPagOperAuto.EOF Then
       pvarResponse = "<RESPONSE>"
       'pvarResponse = pvarResponse & "<ESTADO RESULTADO=""FALSE"" MENSAJE=""No existen Administradoras"" CODIGO=""""/>"
       pvarResponse = pvarResponse & "<ESTADO>"
        pvarResponse = pvarResponse & "<RESULTADO>FALSE</RESULTADO><MENSAJE>No existen Administradoras</MENSAJE><CODIGO></CODIGO>"
         pvarResponse = pvarResponse & "</ESTADO>"
       pvarResponse = pvarResponse & "</RESPONSE>"
        Else
        pvarResponse = "<RESPONSE>"
        pvarResponse = pvarResponse & "<ESTADO>"
        pvarResponse = pvarResponse & "<RESULTADO>TRUE</RESULTADO><MENSAJE></MENSAJE><CODIGO></CODIGO>"
         pvarResponse = pvarResponse & "</ESTADO>"
        pvarResponse = pvarResponse & "<PAGINAOPERAUTO>"
        If Not wrstPagOperAuto.EOF Then
            pvarResponse = pvarResponse & "<DESCRIPCION>" & wrstPagOperAuto.fields("OPA_PAGINAASP").Value & "</DESCRIPCION>"
        End If
        pvarResponse = pvarResponse & "</PAGINAOPERAUTO>"
        pvarResponse = pvarResponse & "</RESPONSE>"
    End If
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep & " pvarResponse: " & pvarResponse
    
    Set wrstPagOperAuto.ActiveConnection = Nothing
    '
    wvarStep = 90
    wobjDBCnn.Close
    '
    wvarStep = 100
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    
    Set wobjNode = Nothing
    Set wobjNodeList = Nothing
    Set wobjXMLDocs = Nothing
    Set wobjXSLDocs = Nothing
    '**************************************************************************
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    wvarStep = 130
    wrstPagOperAuto.Close
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    wvarStep = 150
    Set wrstPagOperAuto = Nothing
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    wvarStep = 170
    mobjCOM_Context.SetComplete
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    Exit Function
  
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
       pvarResponse = "<RESPONSE>"
       'pvarResponse = pvarResponse & "<ESTADO RESULTADO=""FALSE"" MENSAJE=""No existen Administradoras"" CODIGO=""""/>"
       pvarResponse = pvarResponse & "<ESTADO>"
       pvarResponse = pvarResponse & "<RESULTADO>FALSE</RESULTADO><MENSAJE>" & Err.Description & "</MENSAJE><CODIGO>99</CODIGO>"
       pvarResponse = pvarResponse & "</ESTADO>"
       pvarResponse = pvarResponse & "</RESPONSE>"

    LogTrace "ERROR: " & Err.Number & " - " & Err.Description & " - sacA_Utiles.PaginaOperAuto - Step: " & wvarStep
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function





