VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Clases"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'Para los Combos de clase
Const COD_TODOS = "@Todos"
Const DESC_TODOS = "Todos"
Const COD_A_RESOLVER = "@AResolver"
Const DESC_A_RESOLVER = "A Resolver"
'
Const mcteClassName As String = "sacA_Utiles.Clases"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    'Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    'Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    'Dim wobjDBCnn       As ADODB.Connection
    'Dim wobjDBCmd       As ADODB.Command
    'Dim wobjDBParm      As ADODB.Parameter
    'Dim wrstAgenda      As ADODB.Recordset
    '
    'Dim wobjXMLDocs     As MSXML2.DOMDocument
    'Dim wobjXSLDocs     As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    pvarResponse = "<UTILES>"
    pvarResponse = pvarResponse & "<CLASE CODIGOCLASE=" & COD_TODOS
    pvarResponse = pvarResponse & " DESCRIPCIONCLASE=" & DESC_TODOS & "/>"
    pvarResponse = pvarResponse & "<CLASE CODIGOCLASE=" & "P"
    pvarResponse = pvarResponse & " DESCRIPCIONCLASE=" & "PREMIER" & "/>"
    pvarResponse = pvarResponse & "<CLASE CODIGOCLASE=" & "T"
    pvarResponse = pvarResponse & " DESCRIPCIONCLASE=" & "TOP" & "/>"
    pvarResponse = pvarResponse & "</UTILES>"
    'Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    'With wobjXMLrequest
    '    .async = False
    '    Call .loadXML(pvarRequest)
    'End With
    '
    wvarStep = 20
    'Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    'Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteCOMISIONES_DBCnn)
    '
    wvarStep = 50
    'Set wobjDBCmd = CreateObject("ADODB.Command")
    'Set wobjDBCmd.ActiveConnection = wobjDBCnn
    'wobjDBCmd.CommandText = "sp_lista_campanias_agenda"
    'wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    'Set wobjDBParm = CreateObject("ADODB.Parameter")
    'wobjDBParm.Name = "@Estado"
    'wobjDBParm.Direction = adParamInput
    'wobjDBParm.Type = adVarChar
    'wobjDBParm.Size = 1
    'wobjDBParm.Value = wvarEstado
    'wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 63
    'Set wobjDBParm = CreateObject("ADODB.Parameter")
    'wobjDBParm.Name = "@tipoDoc"
    'wobjDBParm.Direction = adParamInput
    'wobjDBParm.Type = adVarChar
    'wobjDBParm.Size = 3
    'wobjDBParm.Value = wvarTipoDoc
    'wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 66
    'Set wobjDBParm = CreateObject("ADODB.Parameter")
    'wobjDBParm.Name = "@nroDoc"
    'wobjDBParm.Direction = adParamInput
    'wobjDBParm.Type = adVarChar
    'wobjDBParm.Size = 15
    'wobjDBParm.Value = wvarNroDoc
    'wobjDBCmd.Parameters.Append wobjDBParm
    
    wvarStep = 70
    'Set wrstAgenda = wobjDBCmd.Execute
    'Set wrstAgenda.ActiveConnection = Nothing
    '
    wvarStep = 80
    'wobjDBCnn.Close
    '
    
    wvarStep = 90
    'Set wobjDBCmd = Nothing
    'Set wobjDBCnn = Nothing
    'Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 100
    '*********************************************************************
    '
    'If Not wrstAgenda.EOF Then
        '
        wvarStep = 110
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 120
        wrstAgenda.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 130
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 140
        pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
    'End If
    '**************************************************************************
    wvarStep = 150
    'wrstAgenda.Close
    '
    wvarStep = 160
    'Set wrstAgenda = Nothing
    '
    wvarStep = 170
    'mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAMPA�AS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAMPA�A'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@age_cam_cod' /></xsl:attribute>"
    'wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGENDA_PENDIENTE'><xsl:value-of select='@age_cerrada' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TIENE_ENTREVISTAS'><xsl:value-of select='@tiene_Entrevistas' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGENDA_INCOMPLETA'><xsl:value-of select='@agenda_Incompleta' /></xsl:attribute>"
    
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@age_cam_des' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TIENEBASE'><xsl:value-of select='@tiene_base' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TIPO'><xsl:value-of select='@age_tipo' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='RAZON_SOCIAL'><xsl:value-of select='@Razon_Social' /></xsl:attribute>"
    
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DOMICILIO'><xsl:value-of select='@Domicilio' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO_POSTAL'><xsl:value-of select='@CodigoPostal' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='LOCALIDAD'><xsl:value-of select='@Localidad' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TELEFONO'><xsl:value-of select='@Telefono' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='INGRESO'><xsl:value-of select='@Ingreso' /></xsl:attribute>"
    
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='LINEACREDITO'><xsl:value-of select='@linea_credito' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='SUCURSAL'><xsl:value-of select='@Sucursal' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='ORIGEN'><xsl:value-of select='@Origen' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='FECHA_NACIMIENTO'><xsl:value-of select='@Fecha_Nacimiento' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='FECHA'><xsl:value-of select='@Fecha' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='HORA'><xsl:value-of select='@Hora' /></xsl:attribute>"
    
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function


