VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "sectores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.sectores"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wvarEmpresa     As String
    Dim wvarEmpresa2     As String
    Dim wvarUsuario     As String
    Dim wvarCargarSectores As Boolean
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstSectores    As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXMLEmpresas As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'App.LogEvent pvarRequest
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DomDocument")
    With wobjXMLrequest
      .async = False
      .loadXML (pvarRequest)
    End With
    wvarStep = 12
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//SECTORES/EMPRESA").Text
    '
    wvarStep = 15
    On Error Resume Next
    wvarStep = 17
    'PARA LA INTEGRACION y NO AFECTAR EL RESTO DE SISTEMAS
    wvarEmpresa2 = wobjXMLrequest.selectSingleNode("//SECTORES/EMPRESA2").Text
    On Error GoTo 0
    '
    If Not wobjXMLrequest.selectSingleNode("//SECTORES/USUARIO") Is Nothing Then
      wvarUsuario = wobjXMLrequest.selectSingleNode("//SECTORES/USUARIO").Text
    Else
      wvarUsuario = ""
    End If
    '
    wvarStep = 20
    Set wobjXMLrequest = Nothing
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarCargarSectores = False
    If Trim(wvarUsuario) <> "" Then
      wvarStep = 40
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_SAC_OBTENERSECTORESADICIONALES"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 50
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Name = "@Usuario"
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = wvarUsuario
      wobjDBCmd.Parameters.Append wobjDBParm
      Set wobjDBParm = Nothing
      '
      wvarStep = 60
      Set wrstSectores = wobjDBCmd.Execute
      Set wrstSectores.ActiveConnection = Nothing
      '
      wvarStep = 70
      'wobjDBCnn.Close
      '
      wvarStep = 80
      Set wobjDBCmd = Nothing
      'Set wobjDBCnn = Nothing
      'Set wobjHSBC_DBCnn = Nothing
      '
      If Not wrstSectores.EOF Then
        wvarStep = 90
        wvarCargarSectores = False
        Set wobjXMLDocs = CreateObject("MSXML2.DomDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DomDocument")
        wvarStep = 100
        wrstSectores.save wobjXMLDocs, adPersistXML
        wvarStep = 110
        wobjXSLDocs.async = False
        wobjXSLDocs.loadXML (p_GetXSLSectoresAdicionales())
        wvarStep = 120
        wobjXMLDocs.loadXML (wobjXMLDocs.transformNode(wobjXSLDocs))
        wvarStep = 130
        wrstSectores.Close
        
        wvarStep = 140
        Set wobjDBCmd = CreateObject("ADODB.Command")
        Set wobjDBCmd.ActiveConnection = wobjDBCnn
        wobjDBCmd.CommandText = "SP_SAC_OBTENEREMPRESASSECTORESADIC"
        wobjDBCmd.CommandType = adCmdStoredProc
        '
        wvarStep = 150
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Name = "@Usuario"
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 10
        wobjDBParm.Value = wvarUsuario
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 160
        Set wrstSectores = wobjDBCmd.Execute
        Set wrstSectores.ActiveConnection = Nothing
        '
        wvarStep = 170
        Set wobjDBCmd = Nothing
        '
        wvarStep = 180
        If Not wrstSectores.EOF Then
          wvarStep = 190
          wvarCargarSectores = False
          Set wobjXMLEmpresas = CreateObject("MSXML2.DomDocument")
          wvarStep = 200
          wrstSectores.save wobjXMLEmpresas, adPersistXML
          wvarStep = 210
          wobjXSLDocs.async = False
          wobjXSLDocs.loadXML (p_GetXSLEmpresasSectoresAdicionales())
          wvarStep = 220
          wobjXMLEmpresas.loadXML (wobjXMLEmpresas.transformNode(wobjXSLDocs))
          wvarStep = 230
          wrstSectores.Close
        Else
          wobjXMLEmpresas.loadXML "<EMPRESAS><ESTADO RESULTADO='TRUE' MENSAJE='' /></EMPRESAS>"
        End If
        Set wobjNode = wobjXMLDocs.selectSingleNode("//SECTORES")
        wobjNode.appendChild wobjXMLEmpresas.selectSingleNode("//EMPRESAS")
        Set wobjNode = Nothing
        wvarStep = 240
        pvarResponse = wobjXMLDocs.xml
        Set wobjXSLDocs = Nothing
        Set wobjXMLDocs = Nothing
      Else
        wvarCargarSectores = True
      End If
    End If
    
    If (Trim(wvarUsuario) = "") Or (wvarCargarSectores) Then
      wvarStep = 250
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "sp_Sectores"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      'App.LogEvent pvarRequest
      wvarStep = 260
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@E1"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarEmpresa)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 265
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@E2"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarEmpresa2)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 270
      Set wrstSectores = wobjDBCmd.Execute
      Set wrstSectores.ActiveConnection = Nothing
      '
      wvarStep = 280
      'wobjDBCnn.Close
      '
      wvarStep = 290
      Set wobjDBCmd = Nothing
      'Set wobjDBCnn = Nothing
      'Set wobjHSBC_DBCnn = Nothing
      '
      If Not wrstSectores.EOF Then
        '
        wvarStep = 300
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 310
        wrstSectores.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 320
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLSectores())
        '
        wvarStep = 330
        pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
        Set wobjXMLDocs = Nothing
        Set wobjXSLDocs = Nothing
      Else
        If wvarCargarSectores Then
          pvarResponse = "<SECTORES><TIENESECTORADIC>N</TIENESECTORADIC></SECTORES>"
        End If
      End If
    End If
    '
    wvarStep = 340
    Set wrstSectores = Nothing
    wvarStep = 350
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
'        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLSectores() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='SECTORES'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:element name='TIENESECTORADIC'>N</xsl:element>"
    wvarStrXSL = wvarStrXSL & "    <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='SECTOR'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@SEC_COD_SEC' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@SEC_DES_SEC' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='ESTADO'><xsl:value-of select='@SEC_ESTADO' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLSectores = wvarStrXSL
End Function


Private Function p_GetXSLSectoresAdicionales() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='SECTORES'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:element name='TIENESECTORADIC'>S</xsl:element>"
    wvarStrXSL = wvarStrXSL & "    <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='SECTOR'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@SEC_COD_SEC' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@SEC_DES_SEC' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLSectoresAdicionales = wvarStrXSL
End Function


Private Function p_GetXSLEmpresasSectoresAdicionales() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='EMPRESAS'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "      <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "      <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "    <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='EMPRESA'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@EMP_COD_EMP' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@EMP_DES_EMP' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLEmpresasSectoresAdicionales = wvarStrXSL
End Function




