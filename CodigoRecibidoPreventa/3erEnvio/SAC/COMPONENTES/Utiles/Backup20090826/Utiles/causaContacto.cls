VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "causaContacto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.causaContacto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName      As String = "IAction_Execute"
    Dim wvarStep          As Long
    '
    Dim wobjXMLrequest    As MSXML2.DOMDocument
    '
    Dim wvarGrupo         As String
    '
    Dim wobjHSBC_DBCnn    As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn         As ADODB.Connection
    Dim wobjDBCmd         As ADODB.Command
    Dim wobjDBParm        As ADODB.Parameter
    Dim wrstCausaContacto As ADODB.Recordset
    '
    Dim wobjXMLDocs       As MSXML2.DOMDocument
    Dim wobjXSLDocs       As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarGrupo = wobjXMLrequest.selectSingleNode("//REGISTRACIONES/USR_GRUPO").Text
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_CausaContacto"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@USR_GRP"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarGrupo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wrstCausaContacto = wobjDBCmd.Execute
    Set wrstCausaContacto.ActiveConnection = Nothing
    '
    wvarStep = 80
    wobjDBCnn.Close
    '
    wvarStep = 90
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 100
    '*********************************************************************
    '
    If Not wrstCausaContacto.EOF Then
        '
        wvarStep = 110
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 120
        wrstCausaContacto.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 130
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 140
        pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
    End If
    '**************************************************************************
    wvarStep = 150
    wrstCausaContacto.Close
    '
    wvarStep = 160
    Set wrstCausaContacto = Nothing
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAUSASCONTACTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAUSACONTACTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@CAU_CON_COD_CAU' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@CAU_CON_DES_CAU' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='ESTADO'><xsl:value-of select='@ESTADO' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function







