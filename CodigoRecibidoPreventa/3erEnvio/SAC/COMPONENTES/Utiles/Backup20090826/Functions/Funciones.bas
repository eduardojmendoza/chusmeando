Attribute VB_Name = "Funciones"
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : Funciones
'
' Module ID : Funciones
'
' File Name : Funciones.bas
'
' Creation Date:
'
' Programmer :
'
' Abstract : Declaraci�n de funciones generales
'
' Entry :
'
' Exit :
'
' Program :
'
' Functions :
'                       NumFilled
'                       TratarCategorias
'                       CalcularEdad
'                       StringParaSQL
'                       NroSocioDcothos
'                       LONG_DOC
'                       NroBeneficiarioDcothos
'                       DateToSQL
'                       DateToString
'                       ConvertirFechaAString
'                       ConvertirStringAFecha
'                       Anio
'                       Mes
'                       Dia
'                       TipoDeSaldo
'                       FechaServer
'                       noNull
'                       SORTMB
'                       EstaVacioElArray
'                       stringToXML
'                       ReplaceBadCaracters
'                       ReplaceAcentos
'                       Formato
'                       TipoDocNYLToSAC
'                       TipoDocNYLToRVP
'                       TipoDocRVPToNYL
'                       TipoDocRVPToSAC
'                       DescTipoDocRVP
'                       DescTipoDocNYL
'                       FormatoCUIT
'                       getUsuario
'                       sacarCDATA
'                       MarcarElArraySiEstaVacio
'                       DiferenciaEnHabilesEntreDosFechas
'                       convertirSegADiasHorasMinSeg
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' The followings should be specified (in chronological
' sequence) for each amendment made to the module:
' Version Version number of the module
' Date Date of amendment (dd/mm/ccyy)
' PPCR Associated PPCR number
' Programmer Name of programmer who made the
' Amendment
' Reason Brief description of the amendment
' *****************************************************************
Option Explicit

Public arrFeriados()
Public wvarHoraHabilDesde As Date
Public wvarHoraHabilHasta As Date
Public wvarHorasHabiles   As Double
Public Const ARRAY_VACIO = "@"
'CLAES 20090629 INI
Public Declare Function GetPrivateProfileString Lib "kernel32.dll" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
'CLAES 20090629 FIN

Public Function NumFilled(ByVal pvarNroDocumento As String, ByVal Chars As Integer) As String
  Dim wvarStrTmp As String
  '*****************************************************
  ' Prop�sito: Completa con ceros el numero de documento hasta llevarlo a 15 chars
  ' Entrada:
  '   pvarNroDocumento : Es el numero de documento a formatear
  '   Chars: Es la cantidad de caracteres a la que se lo quiere llevar
  ' Retorno:
  '   Numero de documento formateado a 15 caracteres
  '
  '*****************************************************
  wvarStrTmp = Trim(Left(pvarNroDocumento, Chars))
  NumFilled = String(Chars - Len(wvarStrTmp), "0") & wvarStrTmp
End Function

Public Function TratarCategorias(ByVal pvarCodCategoria As Variant)
  '*****************************************************
  ' Prop�sito: Devuelve la descripcion de una categoria por su codigo
  ' Entrada:
  '   pvarCodCategoria : Es el codigo de la categoria
  ' Retorno:
  '   Descripcion de la categoria
  '
  '*****************************************************
  If Not IsNull(pvarCodCategoria) Then
    If UCase(pvarCodCategoria) = "P" Or UCase(pvarCodCategoria) = "PREMIER" Then
      TratarCategorias = "PREMIER"
    ElseIf UCase(pvarCodCategoria) = "T" Or UCase(pvarCodCategoria) = "TOP" Then
      TratarCategorias = "TOP"
    Else
      TratarCategorias = ""
    End If
  Else
    TratarCategorias = ""
  End If
End Function

Public Function CalcularEdad(ByVal pvarActual As Double, ByVal pvarNacimiento As Double) As Double
  '*****************************************************
  ' Prop�sito: Devuelve la edad de una persona en a�os
  ' Entrada:
  '   pvarActual : Es la fecha que se va a usar como la actual para el calculo (yyyymmdd)
  '   pvarNacimiento : Es la fecha de nacimiento (yyyymmdd)
  ' Retorno:
  '   Numero de a�os entre las fechas
  '
  '*****************************************************
  Dim wvarMesActual As Integer
  Dim wvarMesNacim As Integer
  Dim wvarAnoActual As Integer
  Dim wvarAnoNacim As Integer
  Dim wvarDiaActual As Integer
  Dim wvarDiaNacim As Integer
  Dim wvarCalculoEdad As Double
  
    wvarCalculoEdad = pvarActual - pvarNacimiento
    wvarAnoActual = Mid(pvarActual, 1, 4)
    wvarMesActual = Mid(pvarActual, 5, 2)
    wvarDiaActual = Mid(pvarActual, 7, 2)
    wvarAnoNacim = Mid(pvarNacimiento, 1, 4)
    wvarMesNacim = Mid(pvarNacimiento, 5, 2)
    wvarDiaNacim = Mid(pvarNacimiento, 7, 2)
    If wvarMesActual < wvarMesNacim Then
        wvarCalculoEdad = (pvarActual - pvarNacimiento) - 1
    Else
        If wvarDiaActual > wvarDiaNacim Then
            wvarCalculoEdad = (pvarActual - pvarNacimiento) - 1
        Else
            wvarCalculoEdad = (pvarActual - pvarNacimiento)
        End If
    End If
    CalcularEdad = Val(Left(wvarCalculoEdad, 2))
End Function

Public Function StringParaSQL(ByRef pvarCadena As String) As String
  '*****************************************************
  ' Prop�sito: Formatea un string con formato para SQL
  ' Entrada:
  '   pvarCadena : Es el string que se quiere pasar al SQL
  ' Retorno:
  '   pvarCadena : String formateado
  '
  '*****************************************************
  Dim wvarret As String
    wvarret = Replace(pvarCadena, "'", "''") 'SearchReplace(pvarCadena, "'", "''")
    wvarret = Replace(wvarret, "%", "") 'SearchReplace(wvarret, "%", "")
    StringParaSQL = Replace(wvarret, "#", "") 'SearchReplace(wvarret, "#", "")
End Function

Public Function NroSocioDcothos(ByVal pvarNrosocio As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el numero de socio sin los digitos del beneficiario
  ' Entrada:
  '   pvarNrosocio : Numero de socio Docthos completo
  ' Retorno:
  '   Numero del socio de 7 caracteres sin los digitos correspondientes al beneficiario
  '
  '*****************************************************
  NroSocioDcothos = Left(Trim(pvarNrosocio), 7)
End Function

Public Function LONG_DOC(Optional pvarConCeros As Boolean = True, Optional pvarDocthos As Boolean)
  '*****************************************************
  ' Prop�sito: Devuelve la cantidad de digitos de un documento segun el tipo
  '            para utilizar conjuntamente con la funcion numfilled
  ' Entrada:
  '   pvarConCeros : true  = (default) para el caso de los documentos no Docthos
  '                  false = para el caso del documento NSO de Docthos
  '   pvarDocthos  : - X -
  ' Retorno:
  '   Cantidad de caracteres del documento
  '
  '*****************************************************
  'cuando sea docthos va a ser 15
  If Not pvarConCeros Then
      LONG_DOC = 9
  Else
      LONG_DOC = 15
  End If
End Function

Public Function NroBeneficiarioDcothos(ByVal pvarNrosocio As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve para un determinado numero de NSO el numero de beneficiario
  ' Entrada:
  '   pvarNrosocio : Es el numero de socio Docthos completo
  ' Retorno:
  '   Numero de beneficiario para el documento proporcionado
  '
  '*****************************************************
  NroBeneficiarioDcothos = Right(pvarNrosocio, 2)
End Function

Public Function DateToSQL(ByVal pvarDate As String)
  '*****************************************************
  ' Prop�sito: Formatea una fecha para ser pasada al SQL
  ' Entrada:
  '   pvarDate : Fecha a formatear (se espera el formato yyyymmdd)
  ' Retorno:
  '   Fecha formateada a la forma yyyy-mm-dd
  '
  '*****************************************************
  If Not IsNull(pvarDate) And Trim(pvarDate) <> "" Then
      DateToSQL = Format(DateSerial(Left(pvarDate, 4), Mid(pvarDate, 5, 2), Mid(pvarDate, 7, 2)), "yyyy-mm-dd")
  Else
      DateToSQL = Empty
  End If
End Function
Public Function DateToString(ByVal pvarDate As Date)
  '*****************************************************
  ' Prop�sito: Convierte una fecha al formato yyyymmdd
  ' Entrada:
  '   pvarDate : Fecha a formatear (se espera date)
  ' Retorno:
  '   Fecha formateada a la forma yyyymmdd
  '
  '*****************************************************
  If Not IsNull(pvarDate) Then
    DateToString = Trim(Str(Year(pvarDate))) & NumFilled(Trim(Str(Month(pvarDate))), 2) & NumFilled(Trim(Str(Day(pvarDate))), 2)
  Else
    DateToString = ""
  End If
End Function
Public Function ConvertirFechaAString(ByVal pvarFecha As Date) As String
  '*****************************************************
  ' Prop�sito: Convierte una fecha al formato yyyymmdd
  ' Entrada:
  '   pvarDate : Fecha a formatear (se espera date)
  ' Retorno:
  '   Fecha formateada a la forma yyyymmdd
  '
  '*****************************************************
  If Not IsNull(pvarFecha) Then
    ConvertirFechaAString = Year(pvarFecha) & NumFilled(Month(pvarFecha), 2) & NumFilled(Day(pvarFecha), 2)
  Else
    ConvertirFechaAString = ""
  End If
End Function
Public Function ConvertirStringAFecha(ByVal pvarFecha As String) As Variant
  '*****************************************************
  ' Prop�sito: Convierte una fecha al formato dd/mm/yyyy
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmdd)
  ' Retorno:
  '   Fecha formateada a la forma dd/mm/yyyy
  '
  '*****************************************************
  ConvertirStringAFecha = Dia(pvarFecha) & "/" & Mes(pvarFecha) & "/" & Anio(pvarFecha)
End Function
Public Function ConvertirStringAHora(ByVal pvarHora As String) As Variant
Dim wvarHora As String
Dim wvarMinuto As String
Dim wvarSegundo As String
  '*****************************************************
  ' Prop�sito: Convierte una hora al formato hh:mm:ss
  ' Entrada:
  '   pvarHora : Hora a formatear (se espera formato hhmm o hhmmss)
  ' Retorno:
  '   hora formateada a la forma hh:mm:ss
  '
  '*****************************************************
  If Len(pvarHora) = 3 Then
    ConvertirStringAHora = CDate(Left(pvarHora, 1) & ":" & Right(pvarHora, 2))
  ElseIf Len(pvarHora) = 4 Then
    ConvertirStringAHora = CDate(Left(pvarHora, 2) & ":" & Right(pvarHora, 2))
  ElseIf Len(pvarHora) = 6 Then
    ConvertirStringAHora = CDate(Left(pvarHora, 2) & ":" & Mid(pvarHora, 3, 2) & ":" & Right(pvarHora, 2))
  End If
End Function
Public Function ConvertirDateAString14(ByVal pvarFecha As Date) As String
Dim wvarDate As String
  '*****************************************************
  ' Prop�sito: Convierte una fecha al formato string 14
  ' Entrada:
  '   pvarFecha : Fecha a formatear
  ' Retorno:
  '   Fecha formateada a la forma aaaammddhhnnss
  '
  '*****************************************************
  ConvertirDateAString14 = CStr(Year(pvarFecha)) & Right("0" & CStr(Month(pvarFecha)), 2) & Right("0" & CStr(Day(pvarFecha)), 2) & Right("0" & CStr(Hour(pvarFecha)), 2) & Right("0" & CStr(Minute(pvarFecha)), 2) & Right("0" & CStr(Second(pvarFecha)), 2)
End Function

Public Function ConvertirString14ADate(ByVal pvarFecha As String) As Variant
Dim wvarDate As String
  '*****************************************************
  ' Prop�sito: Convierte una fecha al formato date
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   Fecha formateada a la forma dd/mm/yyyy hh:mm:ss
  '
  '*****************************************************
  wvarDate = Anio14(pvarFecha) & "/" & Mes14(pvarFecha) & "/" & Dia14(pvarFecha) & " " & Horas14(pvarFecha) & ":" & Minutos14(pvarFecha) & ":" & Segundos14(pvarFecha)
  ConvertirString14ADate = CDate(wvarDate)
End Function

Public Function Anio(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el anio para una fecha en el formato yyyymmdd
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmdd)
  ' Retorno:
  '   A�o de la fecha
  '
  '*****************************************************
  Anio = Left(pvarFecha, 4)
End Function
Public Function Mes(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el mes para una fecha en el formato yyyymmdd
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmdd)
  ' Retorno:
  '   Mes de la fecha
  '
  '*****************************************************
  Mes = Mid(pvarFecha, 5, 2)
End Function

Public Function Dia(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el dia para una fecha en el formato yyyymmdd
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmdd)
  ' Retorno:
  '   Dia de la fecha
  '
  '*****************************************************
  Dia = Right(pvarFecha, 2)
End Function
Public Function Anio14(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el anio para una fecha en el formato yyyymmddhhmmss
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   A�o de la fecha
  '
  '*****************************************************
  Anio14 = Left(pvarFecha, 4)
End Function
Public Function Mes14(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el mes para una fecha en el formato yyyymmddhhmmss
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   Mes de la fecha
  '
  '*****************************************************
  Mes14 = Mid(pvarFecha, 5, 2)
End Function

Public Function Dia14(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve el dia para una fecha en el formato yyyymmddhhmmss
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   Dia de la fecha
  '
  '*****************************************************
  Dia14 = Mid(pvarFecha, 7, 2)
End Function
Public Function Horas14(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve la hora para una fecha en el formato yyyymmddhhmmss
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   Hora de la fecha
  '
  '*****************************************************
  Horas14 = Mid(pvarFecha, 9, 2)
End Function
Public Function Minutos14(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve los minutos para una fecha en el formato yyyymmddhhmmss
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   Minutos de la fecha
  '
  '*****************************************************
  Minutos14 = Mid(pvarFecha, 11, 2)
End Function
Public Function Segundos14(ByVal pvarFecha As String) As String
  '*****************************************************
  ' Prop�sito: Devuelve los segundos para una fecha en el formato yyyymmddhhmmss
  ' Entrada:
  '   pvarFecha : Fecha a formatear (se espera formato yyyymmddhhmmss)
  ' Retorno:
  '   Segundos de la fecha
  '
  '*****************************************************
  Segundos14 = Mid(pvarFecha, 13, 2)
End Function

Public Function TipoDeSaldo(ByVal pvarCodigo As Long) As String
  '*****************************************************
  ' Prop�sito: Devuelve la descripcion de un tipo de saldo segun su codigo
  ' Entrada:
  '   pvarCodigo : Codigo de tipo de saldo
  ' Retorno:
  '   Descripcion del tipo de saldo
  '
  '*****************************************************
    Select Case pvarCodigo
      Case 0
        TipoDeSaldo = "Ok"
      Case 1
        TipoDeSaldo = "Deudor"
      Case 2
        TipoDeSaldo = "Moroso"
      Case 3
        TipoDeSaldo = "Gestion Judicial"
      Case 4
        TipoDeSaldo = "Plan Financiacion"
      Case 5
        TipoDeSaldo = "Pendiente 1er Pago"
      Case Else
        TipoDeSaldo = ""
    End Select
End Function
''Public Function FechaServer() As String
''  '*****************************************************
''  ' Prop�sito: Devuelve la fecha del servidor SQL
''  ' Entrada:
''  '
''  ' Retorno:
''  '   Fecha y hora actual en el formato largo
''  '
''  '*****************************************************
''  Const wcteClassName     As String = "funciones.bas"
''  Const wcteFnName        As String = "FechaServer"
''  Dim wobjCOM_Context     As ObjectContext
''  Dim wobjEventLog        As HSBCInterfaces.IEventLog
''  Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
''  Dim wobjDBCnn           As ADODB.Connection
''  Dim wobjDBCmd           As ADODB.Command
''  Dim wrstConsultas       As ADODB.Recordset
''  Dim wvarStep            As Integer
''  '
''  '~~~~~~~~~~~~~~~~~~~~~~~~~~~
''  On Error GoTo ErrorHandler
''  '~~~~~~~~~~~~~~~~~~~~~~~~~~~
''  '
''  '
''  Set wobjCOM_Context = GetObjectContext()
''  Set wobjEventLog = CreateObject("HSBC.EventLog")
''  wvarStep = 10
''  Set wobjHSBC_DBCnn = wobjCOM_Context.CreateInstance("HSBC.DBConnection")
''  '
''  wvarStep = 20
''  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
''  '
''  wvarStep = 30
''  Set wobjDBCmd = CreateObject("ADODB.Command")
''  Set wobjDBCmd.ActiveConnection = wobjDBCnn
''  wobjDBCmd.CommandText = "SELECT GETDATE()"
''  wobjDBCmd.CommandType = adCmdText
''  '
''  wvarStep = 40
''  Set wrstConsultas = wobjDBCmd.Execute
''  Set wrstConsultas.ActiveConnection = Nothing
''  '
''  wvarStep = 50
''  Set wobjDBCmd = Nothing
''  '
''  wvarStep = 60
''  wobjDBCnn.Close
''  Set wobjDBCnn = Nothing
''  Set wobjHSBC_DBCnn = Nothing
''  '
''  wvarStep = 70
''  If Not wrstConsultas.EOF Then
''    FechaServer = wrstConsultas.Fields(0).Value
''  Else
''    FechaServer = ""
''  End If
''  wrstConsultas.Close
''  Set wrstConsultas = Nothing
''  wvarStep = 80
''  wobjCOM_Context.SetComplete
''  Set wobjCOM_Context = Nothing
''  Set wobjEventLog = Nothing
''  Exit Function
''  '
''  '
'''~~~~~~~~~~~~~~~
''ErrorHandler:
'''~~~~~~~~~~~~~~~
''    If Err.Number = mCte_TimeOutError Then
''        IAction_Execute = xERR_TIMEOUT_RETURN
''    Else
''        wobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
''                         wcteClassName, _
''                         wcteFnName, _
''                         wvarStep, _
''                         Err.Number, _
''                         "Error= [" & Err.Number & "] - " & Err.Description, _
''                         vbLogEventTypeError
''        IAction_Execute = xERR_UNEXPECTED
''    End If
''    wobjCOM_Context.SetAbort
''End Function

Public Function noNull(ByVal pvarstr As Variant) As String
  '*****************************************************
  ' Prop�sito: Evita el uso de null's
  ' Entrada:
  '   pvarstr : Es la cadena a verificar
  ' Retorno:
  '   Devuelve una cadena vacia si la cadena proporcionada es nula o la misma
  '             cadena en caso contrario
  '
  '*****************************************************
  If IsNull(pvarstr) Then
    noNull = ""
  Else
    noNull = pvarstr
  End If
End Function
Public Function SORTMB(ByRef pvararrAux, ByVal pvarColToSort As Integer, ByVal pvarSorting As String)
  '*****************************************************
  ' Prop�sito: Ordena un Array
  ' Entrada:
  '   pvararrAux : Es la cadena a verificar
  ' Retorno:
  '   Devuelve una cadena vacia si la cadena proporcionada es nula o la misma
  '             cadena en caso contrario
  '
  '*****************************************************
Dim lUbound1 As Long
Dim lUbound2 As Long
Dim l As Long
Dim vecreg()
Dim lUboundReg As Long
Dim j As Long
Dim i As Long

    MarcarElArraySiEstaVacio pvararrAux
    If pvararrAux(1, 1) <> ARRAY_VACIO Then
        lUbound1 = UBound(pvararrAux, 2)
        lUbound2 = lUbound1
        lUboundReg = UBound(pvararrAux, 1)
    End If

    
    
    For j = 1 To lUbound1
        For l = j To lUbound2
                If (pvararrAux(pvarColToSort, j) > pvararrAux(pvarColToSort, l) And pvarSorting = "Ascendente") Or _
                    (pvararrAux(pvarColToSort, j) < pvararrAux(pvarColToSort, l) And pvarSorting = "Descendente") Then
                    ReDim Preserve vecreg(1, lUboundReg)
                    For i = 1 To lUboundReg
                        vecreg(1, i) = pvararrAux(i, l)
                        pvararrAux(i, l) = pvararrAux(i, j)
                        pvararrAux(i, j) = vecreg(1, i)
                    Next i
                End If
        Next l
    Next j
End Function
Public Sub MarcarElArraySiEstaVacio(arrArray)
If EstaVacioElArray(arrArray) Then
    ReDim arrArray(1, 1)
    arrArray(1, 1) = ARRAY_VACIO
End If
End Sub
Public Function EstaVacioElArray(arrArray) As Boolean
On Error GoTo ErrArray:
Dim i As Integer
    
i = UBound(arrArray)


Exit Function
ErrArray:
    If Err.Number = 9 Then
        EstaVacioElArray = True
    End If

End Function
Public Function stringToXML(ByVal pvarInString)
  Dim wvarStrTmp
  If Not IsNull(pvarInString) Then
    wvarStrTmp = "" & Replace(pvarInString, "&", "&amp;")
    wvarStrTmp = "" & Replace(wvarStrTmp, "'", Chr(146))
    stringToXML = wvarStrTmp
  Else
    stringToXML = pvarInString
  End If
End Function

Public Function ReplaceBadCaracters(ByVal pvarString As String) As String
'***********************************************************************************
'Version           : 29/01/2002
'Modificada por    : Fernando Martelli
'Proposito         : Reemplaza los caracteres que no pertenecen al alfabeto definido
'                    por algun otro en particular
'Entrada           : String a analizar (byVal)
'salida            : String modificado en el nombre de la funcion
'Alfabeto definido :
'  ! " # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I 'J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g h i j k l m n o p q r s 't u v w x y z { | } ~ � � � � � & < > � � � <LF> <CR> <TAB>
'***********************************************************************************
Dim wvarChar As String
Dim wvarStringTmp As String
Dim i As Long
  '
  On Error Resume Next
  wvarStringTmp = pvarString
  If Not IsNull(wvarStringTmp) Then
    If Len(wvarStringTmp) > 0 Then
      For i = 1 To Len(wvarStringTmp)
        wvarChar = Mid(wvarStringTmp, i, 1)
        If ( _
            Not (Asc(wvarChar) >= 32 And Asc(wvarChar) <= 126) And _
                (Asc(wvarChar) <> 225) And _
                (Asc(wvarChar) <> 233) And _
                (Asc(wvarChar) <> 237) And _
                (Asc(wvarChar) <> 243) And _
                (Asc(wvarChar) <> 250) And _
                (Asc(wvarChar) <> 38) And _
                (Asc(wvarChar) <> 60) And _
                (Asc(wvarChar) <> 62) And _
                (Asc(wvarChar) <> 209) And _
                (Asc(wvarChar) <> 241) And _
                (Asc(wvarChar) <> 13) And _
                (Asc(wvarChar) <> 10) And _
                (Asc(wvarChar) <> 9) And _
                (Asc(wvarChar) <> 146) _
           ) Then
              wvarStringTmp = Mid(wvarStringTmp, 1, (i - 1)) & Replace(wvarStringTmp, wvarChar, " ", i, -1)
        End If
      Next
    End If
  End If
  ReplaceBadCaracters = wvarStringTmp
End Function


Public Function ReplaceAcentos(wvarText As String) As String

    wvarText = Replace(wvarText, "�", "a")
    wvarText = Replace(wvarText, "�", "e")
    wvarText = Replace(wvarText, "�", "i")
    wvarText = Replace(wvarText, "�", "o")
    wvarText = Replace(wvarText, "�", "u")
    ReplaceAcentos = wvarText

End Function

Public Function Formato(pvarNumber As Double) As String
  Dim wvarStr As String
  If Not IsNull(pvarNumber) Then
    wvarStr = Trim(CStr(pvarNumber))
    If Len(wvarStr) > 2 Then
      If Mid(wvarStr, (Len(wvarStr) - 2), 1) = "," Then
        Formato = Mid(wvarStr, 1, (Len(wvarStr) - 3)) & Replace(wvarStr, ",", ".", (Len(wvarStr) - 2))
        Exit Function
      End If
    End If
    Formato = wvarStr
    Exit Function
  End If
  Formato = ""
End Function
Public Function TipoDocNYLToSAC(ByVal pvarEmpresa As Integer, ByVal pvarCodigoNYL As String, ByVal pobjContext As ObjectContext) As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wrstDocs       As ADODB.Recordset
Dim wobjXMLDoc     As MSXML2.DOMDocument
Dim wobjNodeDoc    As MSXML2.IXMLDOMNode
Dim wvarStep       As Integer
  '
  
  'App.LogEvent "TipoDocNYLToSAC: "
  
  'App.LogEvent "pvarEmpresa: " & pvarEmpresa
  
  'App.LogEvent "pvarCodigoNYL: " & pvarCodigoNYL
  
  
  If Not IsNull(pvarCodigoNYL) Then
    
    
    
    
    wvarStep = 10
    Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = pvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    'App.LogEvent "tipodocnyltosac" & pvarCodigoNYL
    wobjDBParm.Value = pvarCodigoNYL
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wrstDocs = wobjDBCmd.Execute
    Set wrstDocs.ActiveConnection = Nothing
    '
    wvarStep = 90
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 100
    If Not wrstDocs.EOF Then
      TipoDocNYLToSAC = Trim(noNull(wrstDocs.fields("TDO_COD_SAC").Value))
    Else
      TipoDocNYLToSAC = pvarCodigoNYL
    End If
    wrstDocs.Close
    Set wrstDocs = Nothing
    
    
  End If
  
  'App.LogEvent "TipoDocNYLToSAC: " & TipoDocNYLToSAC
  End Function

Public Function TipoDocNYLToRVP(ByVal pvarEmpresa As Integer, ByVal pvarCodigoNYL As String, ByVal pobjContext As ObjectContext) As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wrstDocs       As ADODB.Recordset
Dim wobjXMLDoc     As MSXML2.DOMDocument
Dim wobjNodeDoc    As MSXML2.IXMLDOMNode
Dim wvarStep       As Integer
  '
  If Not IsNull(pvarCodigoNYL) Then
    wvarStep = 10
    Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = pvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    'App.LogEvent "tipodocnyltorvp" & pvarCodigoNYL
    wobjDBParm.Value = pvarCodigoNYL
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wrstDocs = wobjDBCmd.Execute
    Set wrstDocs.ActiveConnection = Nothing
    '
    wvarStep = 70
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 80
    
    '/Modificacion Gustavo Alonso para cambio de tipo de documento
    'App.LogEvent "@CodORI1: " & pvarCodigoNYL & "  ---  " & vbLogEventTypeInformation
    
    'App.LogEvent "wrstDocs.Fields(TDO_COD_ORI2).Value: " & wrstDocs.Fields("TDO_COD_ORI2").Value & "  ---  " & vbLogEventTypeInformation
    
    'App.LogEvent "@Empresa: " & pvarEmpresa & "  ---  " & vbLogEventTypeInformation
        
    '/FIN Modificacion Gustavo Alonso para cambio de tipo de documento
    
    
    
    If Not wrstDocs.EOF Then
      TipoDocNYLToRVP = Trim(noNull(wrstDocs.fields("TDO_COD_ORI2").Value))
    Else
      TipoDocNYLToRVP = Null
    End If
    
    'App.LogEvent "TipoDocNYLToRVP: " & TipoDocNYLToRVP & "  ---  " & vbLogEventTypeInformation
    
    wrstDocs.Close
    Set wrstDocs = Nothing
  End If
  End Function

Public Function TipoDocRVPToNYL(ByVal pvarEmpresa As Integer, ByVal pvarCodigoRVP As String, ByVal pobjContext As ObjectContext) As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wrstDocs       As ADODB.Recordset
Dim wobjXMLDoc     As MSXML2.DOMDocument
Dim wobjNodeDoc    As MSXML2.IXMLDOMNode
Dim wvarStep       As Integer
  '
  
  'App.LogEvent "TipoDocRVPToNYL: "
  
  'App.LogEvent "pvarEmpresa: " & pvarEmpresa
  
  'App.LogEvent "pvarCodigoRVP: " & pvarCodigoRVP
  
  
  If Not IsNull(pvarCodigoRVP) Then
    wvarStep = 10
    Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = pvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    'App.LogEvent "tipodocrvptonyl" & pvarCodigoRVP
    wobjDBParm.Value = pvarCodigoRVP
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wrstDocs = wobjDBCmd.Execute
    Set wrstDocs.ActiveConnection = Nothing
    '
    wvarStep = 70
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 80
    If Not wrstDocs.EOF Then
      TipoDocRVPToNYL = Trim(noNull(wrstDocs.fields("TDO_COD_ORI1").Value))
    Else
      TipoDocRVPToNYL = Null
    End If
    wrstDocs.Close
    Set wrstDocs = Nothing
  End If
  
  'App.LogEvent "TipoDocRVPToNYL: " & TipoDocRVPToNYL
  End Function
Public Function TipoDocRVPToSAC(ByVal pvarEmpresa As Integer, ByVal pvarCodigoRVP As String, ByVal pobjContext As ObjectContext) As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wrstDocs       As ADODB.Recordset
Dim wobjXMLDoc     As MSXML2.DOMDocument
Dim wobjNodeDoc    As MSXML2.IXMLDOMNode
Dim wvarStep       As Integer
  '
  
  
  'App.LogEvent "TipoDocRVPToSAC: "
  
  'App.LogEvent "pvarEmpresa: " & pvarEmpresa
  
  'App.LogEvent "pvarCodigoRVP: " & pvarCodigoRVP
  
  
  If Not IsNull(pvarCodigoRVP) Then
    wvarStep = 10
    Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = pvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    'App.LogEvent "tipodocrvptosac" & pvarCodigoRVP
    wobjDBParm.Value = pvarCodigoRVP
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wrstDocs = wobjDBCmd.Execute
    Set wrstDocs.ActiveConnection = Nothing
    '
    wvarStep = 70
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 80
    If Not wrstDocs.EOF Then
      TipoDocRVPToSAC = Trim(noNull(wrstDocs.fields("TDO_COD_SAC").Value))
    Else
      TipoDocRVPToSAC = Null
    End If
    wrstDocs.Close
    Set wrstDocs = Nothing
  End If
  
  'App.LogEvent "TipoDocRVPToSAC: " & TipoDocRVPToSAC
  
  End Function

Public Function DescTipoDocRVP(ByVal pvarEmpresa As Integer, ByVal pvarCodigoRVP As String, ByVal pobjContext As ObjectContext) As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wrstDocs       As ADODB.Recordset
Dim wobjXMLDoc     As MSXML2.DOMDocument
Dim wobjNodeDoc    As MSXML2.IXMLDOMNode
Dim wvarStep       As Integer
  '
  
  'App.LogEvent "CODIGORVP=" & pvarCodigoRVP
  
  If Not IsNull(pvarCodigoRVP) Then
    wvarStep = 10
    Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = pvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = pvarCodigoRVP
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wrstDocs = wobjDBCmd.Execute
    Set wrstDocs.ActiveConnection = Nothing
    '
    wvarStep = 70
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 80
    If Not wrstDocs.EOF Then
      DescTipoDocRVP = Trim(noNull(wrstDocs.fields("TDO_COD_SAC").Value))
    Else
      DescTipoDocRVP = ""
    End If
    wrstDocs.Close
    Set wrstDocs = Nothing
  End If
  End Function
Public Function DescTipoDocNYL(ByVal pvarEmpresa As Integer, ByVal pvarCodigoNYL As String, ByVal pobjContext As ObjectContext) As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wrstDocs       As ADODB.Recordset
Dim wobjXMLDoc     As MSXML2.DOMDocument
Dim wobjNodeDoc    As MSXML2.IXMLDOMNode
Dim wvarStep       As Integer
  '
  If Not IsNull(pvarCodigoNYL) Then
    wvarStep = 10
    Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = pvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = pvarCodigoNYL
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wrstDocs = wobjDBCmd.Execute
    Set wrstDocs.ActiveConnection = Nothing
    '
    wvarStep = 70
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 80
    If Not wrstDocs.EOF Then
      DescTipoDocNYL = Trim(noNull(wrstDocs.fields("TDO_COD_SAC").Value))
    Else
      DescTipoDocNYL = pvarCodigoNYL
    End If
    wrstDocs.Close
    Set wrstDocs = Nothing
  End If
  End Function

Public Function FormatoCUIT(ByVal pvarNumeroCUIT As String) As String
Dim wvarCuit As String
If Not IsNull(pvarNumeroCUIT) Then
  pvarNumeroCUIT = Trim("" & pvarNumeroCUIT)
  If Len(pvarNumeroCUIT) = 11 Then
    wvarCuit = Left(pvarNumeroCUIT, 2) & "-" & Mid(pvarNumeroCUIT, 3, 8) & "/" & Right(pvarNumeroCUIT, 1)
  Else
    wvarCuit = 0
  End If
Else
  wvarCuit = 0
End If
FormatoCUIT = wvarCuit
End Function
Public Function getUsuario(ByVal pvarPath As String) As String
Dim wvarUser
wvarUser = ""
If Not IsNull(pvarPath) Then
    If Trim(pvarPath) <> "" Then
        wvarUser = Mid(pvarPath, InStrRev(pvarPath, "\") + 1)
    End If
End If
getUsuario = wvarUser
End Function
Public Function sacarCDATA(pvarTexto)
  If Not IsNull(pvarTexto) Then
    sacarCDATA = Replace(Replace(pvarTexto, "<![CDATA[", ""), "]]>", "")
  Else
    sacarCDATA = ""
  End If
End Function

Public Function DiferenciaEnHabilesEntreDosFechas(pvarFechaDesde As String, pvarFechaHasta As String, ByVal pobjContext As ObjectContext) As String
'*****************************************************
' Prop�sito: Devuelve el tiempo habil entre dos fechas
' Entrada:
'   pvarFechaDesde : Es la fecha desde
'   pvarFechaHasta : Es la fecha hasta
' Retorno:
'   String expresando el tiempo habil entre las dos fechas
'
'*****************************************************
Dim sSql As String
Dim wobjHSBC_DBCnn As HSBCInterfaces.IDBConnection
Dim wobjDBCnn      As ADODB.Connection
Dim wobjDBCmd      As ADODB.Command
Dim wobjDBParm     As ADODB.Parameter
Dim wobjFer        As New ADODB.Recordset

Dim wvarTiempoTrans As Double

Dim sFechaDesde As Date
Dim sFechaHasta As Date

Dim wvarHorasHabilesDate As Date

Dim wvarHoraTmp As Date
Dim i As Integer
Dim bEntro As Boolean

Dim wvarStep  As Integer

On Error Resume Next
  '
  wvarStep = 10
  Set wobjHSBC_DBCnn = pobjContext.CreateInstance("HSBC.DBConnection")
  '
  wvarStep = 20
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
  '
  'Obtiene los feriados
  If UBound(arrFeriados()) = 0 Then
    On Error GoTo 0
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "BR..spVerFeriados"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjFer = wobjDBCmd.Execute
    Set wobjFer.ActiveConnection = Nothing
    '
    wvarStep = 50
    If Not wobjFer.EOF Then
      Do
        ReDim Preserve arrFeriados(i)
        arrFeriados(i) = wobjFer("Feriado")
        i = i + 1
        wobjFer.MoveNext
      Loop While Not wobjFer.EOF
      wobjFer.Close
    End If
    Set wobjFer = Nothing
  End If

  If wvarHoraHabilDesde = wvarHoraHabilHasta Then
    'Obtiene el horario habil
    wvarStep = 60
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_PAR_OBTENERPARAMETROS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 70
    Set wobjFer = wobjDBCmd.Execute
    Set wobjFer.ActiveConnection = Nothing
  
    If Not wobjFer.EOF Then
      If noNull(wobjFer.fields("HorarioHabilDesde").Value) <> "" And _
          noNull(wobjFer.fields("HorarioHabilHasta").Value) <> "" Then
        wvarHoraHabilDesde = ConvertirStringAHora(CStr(wobjFer.fields("HorarioHabilDesde").Value))
        wvarHoraHabilHasta = ConvertirStringAHora(CStr(wobjFer.fields("HorarioHabilHasta").Value))
        wvarHorasHabilesDate = ConvertirStringAHora(CStr(wobjFer.fields("HorasHabilesxDia").Value))
        wvarHorasHabiles = Hour(wvarHorasHabilesDate) + (Minute(wvarHorasHabilesDate) / 60)
        'If DateDiff("h", wvarHoraHabilDesde, wvarHoraHabilHasta) > 0 Then
        '  wvarHorasHabiles = DateDiff("h", wvarHoraHabilDesde, wvarHoraHabilHasta)
        'ElseIf DateDiff("n", wvarHoraHabilDesde, wvarHoraHabilHasta) > 0 Then
        '  wvarHorasHabiles = DateDiff("h", wvarHoraHabilDesde, wvarHoraHabilHasta) / 100
        'End If
      Else
        Err.Raise vbObjectError + 513, "DiferenciaEnHabilesEntreDosFechas", "No se han cargado los par�metros de horario h�bil!"
      End If
      wobjFer.Close
    Else
      Err.Raise vbObjectError + 513, "DiferenciaEnHabilesEntreDosFechas", "No se han cargado los par�metros de horario h�bil!"
    End If
    Set wobjFer = Nothing
  End If
  '
  wvarStep = 80
  wobjDBCnn.Close
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  '
  sFechaDesde = ConvertirString14ADate(pvarFechaDesde)
  sFechaHasta = ConvertirString14ADate(pvarFechaHasta)
  wvarStep = 90
  Do While DateDiff("d", sFechaDesde, sFechaHasta) > 0
    If Not (Weekday(sFechaDesde) = vbSunday Or Weekday(sFechaDesde) = vbSaturday) And Not esFeriado(arrFeriados, sFechaDesde) Then
      If Not bEntro Then
        bEntro = True
        wvarHoraTmp = Format(sFechaDesde, "hh:nn:ss")
        If wvarHoraTmp >= wvarHoraHabilDesde And wvarHoraTmp <= wvarHoraHabilHasta Then
          wvarTiempoTrans = wvarTiempoTrans + DateDiff("s", wvarHoraTmp, wvarHoraHabilHasta)
        ElseIf wvarHoraTmp < wvarHoraHabilDesde Then
          wvarTiempoTrans = wvarTiempoTrans + ((wvarHorasHabiles * 60) * 60)
        End If
      Else
        wvarTiempoTrans = wvarTiempoTrans + ((wvarHorasHabiles * 60) * 60)
       End If
    End If
    sFechaDesde = DateAdd("d", 1, sFechaDesde)
  Loop
  wvarStep = 100
  wvarHoraTmp = Format(sFechaHasta, "hh:nn:ss")
  If Not (Weekday(sFechaHasta) = vbSunday Or Weekday(sFechaHasta) = vbSaturday) And Not esFeriado(arrFeriados, sFechaHasta) Then
    If Not bEntro Then
      If Format(sFechaDesde, "hh:nn:ss") < wvarHoraHabilDesde Then
        sFechaDesde = CDate(CStr(Format(sFechaDesde, "yyyy/mm/dd")) & " " & CStr(wvarHoraHabilDesde))
      End If
      If Format(sFechaHasta, "hh:nn:ss") > wvarHoraHabilHasta Then
        sFechaHasta = CDate(CStr(Format(sFechaHasta, "yyyy/mm/dd")) & " " & CStr(wvarHoraHabilHasta))
      End If
      wvarTiempoTrans = DateDiff("s", sFechaDesde, sFechaHasta)
    Else
      If wvarHoraTmp >= wvarHoraHabilDesde And wvarHoraTmp <= wvarHoraHabilHasta Then
        wvarTiempoTrans = wvarTiempoTrans + DateDiff("s", wvarHoraHabilDesde, wvarHoraTmp)
      ElseIf wvarHoraTmp > wvarHoraHabilHasta Then
        wvarTiempoTrans = wvarTiempoTrans + ((wvarHorasHabiles * 60) * 60)
      End If
    End If
  End If
  '
  DiferenciaEnHabilesEntreDosFechas = convertirSegADiasHorasMinSeg(wvarTiempoTrans)

End Function

Function esFeriado(ByVal parrFeriado As Variant, ByVal pvarDate As Date) As Boolean
Dim i As Integer
  esFeriado = False
  If Not ArrayVacio(parrFeriado) Then
    For i = 0 To UBound(parrFeriado)
      If ConvertirFechaAString(parrFeriado(i)) = ConvertirFechaAString(pvarDate) Then
        esFeriado = True
        Exit For
      End If
    Next
  End If
End Function

Public Function ArrayVacio(arrArray) As Boolean
On Error GoTo errArr:
Dim a
    
    a = arrArray(0)
    
Exit Function
errArr:
    ArrayVacio = True
End Function

Public Function convertirSegADiasHorasMinSeg(ByVal pvarSegundos As Double) As String
'*****************************************************
' Prop�sito: Devuelve el tiempo expresado en dias, horas, minutos
'            y segundos
' Entrada:
'   pvarSegundos   : Es la cantidad total de segundos
' Retorno:
'   Un string expresando dias, horas, minutos y segundos
'   Ej : 1d 3h 30' 45''
'*****************************************************
Dim wvarDias As Long
Dim wvarHoras As Long
Dim wvarMinutos As Long
Dim wvarSegundos As Long
Dim wvarReturn As String
  '
  'NOTA : La variable wvarHorasHabiles contiene la cantidad de
  'horas habiles por dia y se inicializa en el
  'sub CantidadDeDiasyHorasHabilesEntreDosFechas
  wvarSegundos = pvarSegundos
  Do While wvarSegundos >= 60
    wvarMinutos = pvarSegundos \ 60
    wvarSegundos = pvarSegundos Mod 60
    Do While wvarMinutos >= 60
      wvarHoras = wvarMinutos \ 60
      wvarMinutos = wvarMinutos Mod 60
      Do While wvarHoras >= wvarHorasHabiles
        wvarDias = wvarHoras \ wvarHorasHabiles
        wvarHoras = (wvarHoras Mod wvarHorasHabiles)
      Loop
    Loop
  Loop
  
  If wvarDias > 0 Then
    wvarReturn = CStr(wvarDias) & "d "
  End If
  If wvarHoras > 0 Then
    wvarReturn = wvarReturn & CStr(wvarHoras) & "h "
  End If
  If wvarMinutos > 0 Then
    wvarReturn = wvarReturn & CStr(wvarMinutos) & "' "
  End If
  If wvarSegundos > 0 Then
    wvarReturn = wvarReturn & CStr(wvarSegundos) & "''"
  End If
  If wvarReturn = "" Then
    wvarReturn = "0"
  End If
  convertirSegADiasHorasMinSeg = wvarReturn
End Function

Public Function convertirSegADias(ByVal pvarSegundos As Double) As Integer
'*****************************************************
' Prop�sito: Devuelve el tiempo expresado en dias
' Entrada:
'   pvarSegundos   : Es la cantidad total de segundos
' Retorno:
'   Un entero expresando la cant de dias
'   Ej : 3
'*****************************************************
Dim wvarDias As Long
Dim wvarHoras As Long
Dim wvarMinutos As Long
Dim wvarSegundos As Long
Dim wvarReturn As Integer
  '
  'NOTA : La variable wvarHorasHabiles contiene la cantidad de
  'horas habiles por dia y se inicializa en el
  'sub CantidadDeDiasyHorasHabilesEntreDosFechas
  wvarSegundos = pvarSegundos
  Do While wvarSegundos >= 60
    wvarMinutos = pvarSegundos \ 60
    wvarSegundos = pvarSegundos Mod 60
    Do While wvarMinutos >= 60
      wvarHoras = wvarMinutos \ 60
      wvarMinutos = wvarMinutos Mod 60
      Do While wvarHoras >= wvarHorasHabiles
        wvarDias = wvarHoras \ wvarHorasHabiles
        wvarHoras = (wvarHoras Mod wvarHorasHabiles)
      Loop
    Loop
  Loop
  
  If wvarDias > 0 Then
    wvarReturn = wvarDias
  Else
    wvarReturn = 0
  End If
    
  convertirSegADias = wvarReturn
End Function


Public Function fncCheckXMLNode(pobjXMLRequest As MSXML2.DOMDocument, pvarNode As String, Optional pvarDefault As String = "") As String

    On Error GoTo ErrFunc
    
    Dim wvarReturn  As String
    
    wvarReturn = pobjXMLRequest.selectSingleNode(pvarNode).Text
    
ExitFunc:
    fncCheckXMLNode = wvarReturn
    Exit Function
    
ErrFunc:
    wvarReturn = pvarDefault
    Resume ExitFunc
    
End Function
'CLAES 20090629 INI
Public Sub LogTrace(ByVal pvarText As String)
    
    Dim lFile As Long
    Dim sFile As String
    Dim sPath As String
    
    On Error GoTo errTrace
    
    sPath = fncLeerIni("LOG", "Path", App.Path, "Log.ini")
    sFile = fncLeerIni("LOG", "File", "", "Log.ini")
    
    sFile = sPath & "\" & sFile
    'pvarText = Format(Now, "yyyy-mm-dd hh:mm:ss") & " - " & ComputerName & vbCrLf & pvarText & vbCrLf & vbCrLf
    pvarText = Format(Now, "yyyy-mm-dd hh:mm:ss") & " | " & pvarText & vbCrLf
    

    lFile = FreeFile

    Open sFile For Append As #lFile 'Len = 84
    
        Print #lFile, pvarText
    
    Close #lFile

Exit Sub

errTrace:
    App.LogEvent Err.Description
    App.LogEvent "Input:" & pvarText
    App.LogEvent "Path:" & sFile
    On Error Resume Next
End Sub
Public Function fncLeerIni(Seccion As String, Item As String, porDefecto As String, ArchivoIni As String) As String
    Dim Ret As String
    Dim X As Integer
    If LCase(Dir(App.Path + "\" & ArchivoIni)) <> LCase(ArchivoIni) Then
           App.LogEvent "No es posible abrir " & App.Path & "\" & ArchivoIni
        Exit Function
    End If
    Ret = Space(200)
    X = GetPrivateProfileString(Seccion, Item, porDefecto, Ret, Len(Ret), App.Path + "\" & ArchivoIni)
    Ret = Trim(Ret)
    Ret = Left(Ret, Len(Ret) - 1)
    fncLeerIni = Ret
End Function
'CLAES 20090629 FIN

