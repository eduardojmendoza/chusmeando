Public Function NumFilled(ByVal Nro As String, Chars As Integer) As String

  Nro = Left(Nro, Chars)
  NumFilled = String(Chars - Len(Nro), "0") & Nro

End Function