VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "RelacionProductoSel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
Const mcteClassName As String = "sacA_Utiles.RelacionProductoSel"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstRecordSet   As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim wvarEmpresa As String
    Dim wvarCodPro As String
    Dim wvarCodCau As String
    Dim wvarCodMot As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    LogTrace " - sacA_Utiles.CodigoGeografico -"
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    'App.LogEvent pvarRequest
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//TRANSAC/EMPRESA").Text
    wvarCodPro = wobjXMLrequest.selectSingleNode("//TRANSAC/CODPRO").Text
    wvarCodCau = wobjXMLrequest.selectSingleNode("//TRANSAC/CODCAU").Text
    wvarCodMot = wobjXMLrequest.selectSingleNode("//TRANSAC/CODMOT").Text
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "P_SAC_RELACIONPRODUCTO_SEL"
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@REP_COD_EMP"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@REP_COD_PRO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodPro
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@REP_COD_CAU"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodCau
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@REP_COD_MOT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodMot
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 90
    '
    Set wrstRecordSet = wobjDBCmd.Execute

    wvarStep = 100
    '*********************************************************************
    If wrstRecordSet.EOF Then
       pvarResponse = "<RESPONSE>"
       pvarResponse = pvarResponse & "<ESTADO><RESULTADO>FALSE</RESULTADO><MENSAJE>NO SE ENCONTRARON DATOS</MENSAJE><CODIGO></CODIGO></ESTADO>"
       pvarResponse = pvarResponse & "</RESPONSE>"
        Else
        pvarResponse = "<RESPONSE>"
        pvarResponse = pvarResponse & "<ESTADO><RESULTADO>TRUE</RESULTADO><MENSAJE></MENSAJE><CODIGO></CODIGO></ESTADO>"
        While Not wrstRecordSet.EOF
            pvarResponse = pvarResponse & "<RELACIONPRODUCTO>"
            pvarResponse = pvarResponse & "<REP_COD_EMP>" & wrstRecordSet.fields("REP_COD_EMP").Value & "</REP_COD_EMP>"
            pvarResponse = pvarResponse & "<REP_COD_PRO>" & wrstRecordSet.fields("REP_COD_PRO").Value & "</REP_COD_PRO>"
            pvarResponse = pvarResponse & "<REP_COD_CAU>" & wrstRecordSet.fields("REP_COD_CAU").Value & "</REP_COD_CAU>"
            pvarResponse = pvarResponse & "<REP_COD_MOT>" & wrstRecordSet.fields("REP_COD_MOT").Value & "</REP_COD_MOT>"
            pvarResponse = pvarResponse & "<REP_COD_SEC>" & wrstRecordSet.fields("REP_COD_SEC").Value & "</REP_COD_SEC>"
            pvarResponse = pvarResponse & "<REP_DIA_SOL>" & wrstRecordSet.fields("REP_DIA_SOL").Value & "</REP_DIA_SOL>"
            pvarResponse = pvarResponse & "<REP_OBS><![CDATA[" & wrstRecordSet.fields("REP_OBS").Value & "]]></REP_OBS>"
            pvarResponse = pvarResponse & "<LD_COD>" & wrstRecordSet.fields("LD_COD").Value & "</LD_COD>"
            pvarResponse = pvarResponse & "<REP_ESTADO>" & wrstRecordSet.fields("REP_ESTADO").Value & "</REP_ESTADO>"
            pvarResponse = pvarResponse & "<REP_HELP><![CDATA[" & wrstRecordSet.fields("REP_HELP").Value & "]]></REP_HELP>"
            pvarResponse = pvarResponse & "<REP_SEG_CLA>" & wrstRecordSet.fields("REP_SEG_CLA").Value & "</REP_SEG_CLA>"
            pvarResponse = pvarResponse & "<REP_COD_SACTRAN>" & wrstRecordSet.fields("REP_COD_SACTRAN").Value & "</REP_COD_SACTRAN>"
            pvarResponse = pvarResponse & "<REP_COD_SEC_PRE>" & wrstRecordSet.fields("REP_COD_SEC_PRE").Value & "</REP_COD_SEC_PRE>"
            pvarResponse = pvarResponse & "</RELACIONPRODUCTO>"

            wrstRecordSet.MoveNext
        Wend
        pvarResponse = pvarResponse & "</RESPONSE>"
    End If
    '
    Set wrstRecordSet.ActiveConnection = Nothing
    '
    wvarStep = 110
    wobjDBCnn.Close
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    Set wobjNode = Nothing
    Set wobjNodeList = Nothing
    Set wobjXMLDocs = Nothing
    Set wobjXSLDocs = Nothing
    '
    wvarStep = 130
    wrstRecordSet.Close
    '
    wvarStep = 140
    Set wrstRecordSet = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    '
    'App.LogEvent pvarResponse
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    LogTrace "ERROR: " & Err.Number & " - " & Err.Description & " - sacA_Utiles.RelacionProductoSel - Step: " & wvarStep
    pvarResponse = pvarResponse & "<ESTADO><RESULTADO>FALSE</RESULTADO><MENSAJE>" & Err.Description & "</MENSAJE><CODIGO>99</CODIGO></ESTADO>"
    
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function
