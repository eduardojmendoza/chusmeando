VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ConsultaPreembozado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.ConsultaPreembozado"

Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstRecordSet   As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim wvarCodEntidad      As String
    Dim wvarCodAdmin        As String
    Dim wvarCuenta          As String
    Dim wvarPlastico        As String
    Dim wvarMensaje         As String
      
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    
    LogTrace "sacA_Utiles.ConsultaPreembozado - " & pvarRequest
    wvarStep = 20
    wvarCodAdmin = wobjXMLrequest.selectSingleNode("//Request/COD_ADMINISTRADORA").Text
    wvarStep = 30
    wvarCodEntidad = wobjXMLrequest.selectSingleNode("//Request/COD_ENTIDAD").Text
    wvarStep = 40
    wvarCuenta = wobjXMLrequest.selectSingleNode("//Request/COD_CUENTA").Text
    wvarStep = 50
    wvarPlastico = wobjXMLrequest.selectSingleNode("//Request/COD_PLASTICO").Text
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 60
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteADINTAR_DBCnn)
    '
    wvarStep = 70
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "p_sact_info_plasticos_preembozados"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@cod_adm"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 2
    wobjDBParm.Value = wvarCodAdmin
    '
    wvarStep = 90
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@cod_ent"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarCodEntidad
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@cod_ext_cue"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 20
    wobjDBParm.Value = Val(wvarCuenta)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@cod_pla"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 16
    wobjDBParm.Value = wvarPlastico
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 120
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@mensaje"
    wobjDBParm.Direction = adParamOutput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 100
    wobjDBParm.Value = ""
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    LogTrace "sacA_Utiles.ConsultaPreembozado - p_sact_info_plasticos_preembozados " & wvarCodAdmin & "," & wvarCodEntidad & "," & wvarCuenta & "," & wvarPlastico
    wvarStep = 130
    Set wrstRecordSet = wobjDBCmd.Execute
    
    wvarStep = 140
    If wrstRecordSet.State = adStateClosed Then
        pvarResponse = "<RESPONSE><ESTADO RESULTADO=""FALSE"" MENSAJE=""ERROR Sin Conexion con Adintar"" CODIGO=""01""/></RESPONSE>"
        LogTrace " ERROR - sacA_Utiles.ConsultaPreembozado - Step: " & wvarStep & " - Respuesta: " & pvarResponse
        Exit Function
    End If
    
    If wrstRecordSet.EOF Then
       wvarStep = 150
       pvarResponse = "<RESPONSE>"
       pvarResponse = pvarResponse & "<ESTADO RESULTADO=""FALSE"" MENSAJE=""" & wvarMensaje & """ CODIGO=""""/>"
       pvarResponse = pvarResponse & "</RESPONSE>"
       LogTrace " ERROR - sacA_Utiles.ConsultaPreembozado - Step: " & wvarStep & " - Respuesta: " & pvarResponse
    Else
        wvarStep = 160
        pvarResponse = "<RESPONSE>"
        'pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE=wrstTransac.EOF&"""" CODIGO=""""/>"
        pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE="""" CODIGO=""""/>"
        While Not wrstRecordSet.EOF
            wvarStep = 170
            pvarResponse = pvarResponse & "<APELLIDO>" & Trim(wrstRecordSet.fields("Nom_y_ape").Value) & "</APELLIDO>"
            wvarStep = 171
            pvarResponse = pvarResponse & "<TIPO_PRODUCTO>" & wrstRecordSet.fields("Tipo_Producto").Value & "</TIPO_PRODUCTO>"
            wvarStep = 172
            pvarResponse = pvarResponse & "<FECHA_VTO>" & wrstRecordSet.fields("Fecha_vto").Value & "</FECHA_VTO>"
            wvarStep = 173
            pvarResponse = pvarResponse & "<TIENE_ASEGURADORA>" & wrstRecordSet.fields("Tiene_aseguradora").Value & "</TIENE_ASEGURADORA>"
            wvarStep = 174
            pvarResponse = pvarResponse & "<COD_ASEGURADORA>" & wrstRecordSet.fields("aseguradora").Value & "</COD_ASEGURADORA>"
            wvarStep = 175
            pvarResponse = pvarResponse & "<REWARDS>" & wrstRecordSet.fields("En_rewards").Value & "</REWARDS>"
            wvarStep = 176
            pvarResponse = pvarResponse & "<COD_MOD_LIQ>" & wrstRecordSet.fields("Mod_liq").Value & "</COD_MOD_LIQ>"
            wvarStep = 177
            pvarResponse = pvarResponse & "<DESC_MOD_LIQ>" & wrstRecordSet.fields("Desc_Mod_liq").Value & "</DESC_MOD_LIQ>"
            wvarStep = 178
            pvarResponse = pvarResponse & "<COD_CARTERA>" & wrstRecordSet.fields("cod_car").Value & "</COD_CARTERA>"
            wvarStep = 179
            pvarResponse = pvarResponse & "<SUBMOD_TASAS>" & wrstRecordSet.fields("submod_tasas").Value & "</SUBMOD_TASAS>"
            wvarStep = 180
            pvarResponse = pvarResponse & "<SUBMOD_CARGOS>" & wrstRecordSet.fields("submod_cargos").Value & "</SUBMOD_CARGOS>"
            wvarStep = 181
            pvarResponse = pvarResponse & "<SUBMOD_PARAMETROS>" & wrstRecordSet.fields("submod_parametros").Value & "</SUBMOD_PARAMETROS>"
            wvarStep = 182
            pvarResponse = pvarResponse & "<MOD_AUT>" & wrstRecordSet.fields("mod_aut").Value & "</MOD_AUT>"
            wvarStep = 183
            pvarResponse = pvarResponse & "<GRUPO_AFINIDAD>" & wrstRecordSet.fields("grupo_afinidad").Value & "</GRUPO_AFINIDAD>"
            wvarStep = 184
            pvarResponse = pvarResponse & "<DES_GRUPO_AFINIDAD>" & wrstRecordSet.fields("Des_grupo_afinidad").Value & "</DES_GRUPO_AFINIDAD>"
            wvarStep = 185
            pvarResponse = pvarResponse & "<SINO_CAMPANIAS>" & wrstRecordSet.fields("Sino_grp_afinidad_campa�as").Value & "</SINO_CAMPANIAS>"
            wvarStep = 186
            wrstRecordSet.MoveNext
        Wend
        wvarStep = 190
        pvarResponse = pvarResponse & "</RESPONSE>"
    End If
    LogTrace "sacA_Utiles.ConsultaPreembozadoSSSS - Step: " & wvarStep & " pvarResponse: " & pvarResponse
    Set wrstRecordSet.ActiveConnection = Nothing
    '
    wvarStep = 205
    wobjDBCnn.Close
    '
    wvarStep = 210
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    
    wvarStep = 220
    Set wobjNode = Nothing
    Set wobjNodeList = Nothing
    Set wobjXMLDocs = Nothing
    Set wobjXSLDocs = Nothing
    '**************************************************************************
    wvarStep = 230
    wrstRecordSet.Close
    '
    wvarStep = 240
    Set wrstRecordSet = Nothing
    '
    wvarStep = 250
    mobjCOM_Context.SetComplete
    
    wvarStep = 260
    LogTrace "Termino Ok - sacA_Utiles.ConsultaPreembozado - Step: " & wvarStep
    
    Exit Function
  
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<RESPONSE><ESTADO RESULTADO=""FALSE"" MENSAJE="" ERROR: " & Err.Description & ", Paso : " & wvarStep & """" & " CODIGO=""01""/></RESPONSE>"
    LogTrace "ERROR: " & Err.Number & " - " & Err.Description & " - sacA_Utiles.ConsultaPreembozado - Step: " & wvarStep
    mobjCOM_Context.SetComplete
     
End Function




