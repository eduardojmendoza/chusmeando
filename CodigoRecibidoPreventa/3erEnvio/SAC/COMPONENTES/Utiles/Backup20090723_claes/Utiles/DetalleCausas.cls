VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetalleCausas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.DetalleCausas"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName       As String = "IAction_Execute"
    Dim wvarStep           As Long
    '
    Dim wobjXMLrequest     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn     As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn          As ADODB.Connection
    Dim wobjDBCmd          As ADODB.Command
    Dim wobjDBParm         As ADODB.Parameter
    Dim wrstMotivoContacto As ADODB.Recordset
    '
    Dim wobjXMLDocs        As MSXML2.DOMDocument
    Dim wobjXSLDocs        As MSXML2.DOMDocument
    '
    Dim wvarEmpresa        As String
    Dim wvarProducto       As String
    Dim wvarGrupo          As String
    '
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim i               As Integer
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
      .async = False
      Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarProducto = wobjXMLrequest.selectSingleNode("//CAUSAS/COD_PROD").Text
    '
    wvarStep = 30
    wvarGrupo = wobjXMLrequest.selectSingleNode("//CAUSAS/USR_GRUPO").Text
    '
    wvarStep = 40
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//CAUSAS/EMPRESA").Text
    '
    Set wobjXMLrequest = Nothing

    wvarStep = 50
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 60
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 70
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_sac_detalleCausa "
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@USR_GRP"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarGrupo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodProd"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarProducto
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodCau"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = "A"
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 120
    Set wrstMotivoContacto = wobjDBCmd.Execute
    Set wrstMotivoContacto.ActiveConnection = Nothing
    '
    wvarStep = 130
    wobjDBCnn.Close
    '
    wvarStep = 140
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 150
    '*********************************************************************
    '
    If Not wrstMotivoContacto.EOF Then
        '
        wvarStep = 160
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 170
        wrstMotivoContacto.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 180
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 190
        '
        Call wobjXMLDocs.loadXML(wobjXMLDocs.transformNode(wobjXSLDocs))
        Set wobjNodeList = wobjXMLDocs.selectNodes("//CAUSASCONTACTO/CAUSACONTACTO")
        For i = 0 To (wobjNodeList.length - 1)
          Set wobjNode = wobjNodeList.Item(i)
          wobjNode.selectSingleNode("DESCRIPCION").Text = ReplaceAcentos("<![CDATA[" & wobjNode.selectSingleNode("DESCRIPCION").Text & "]]>")
        Next
        Set wobjNodeList = Nothing
        pvarResponse = wobjXMLDocs.xml
    End If
    '**************************************************************************
    wvarStep = 200
    wrstMotivoContacto.Close
    '
    wvarStep = 210
    Set wrstMotivoContacto = Nothing
    '
    wvarStep = 220
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
'        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAUSASCONTACTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "   <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='CAUSACONTACTO'>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='CODIGO'><xsl:value-of select='@cau_con_cod_cau' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='DESCRIPCION'><xsl:value-of select='@cau_con_des_cau' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function


