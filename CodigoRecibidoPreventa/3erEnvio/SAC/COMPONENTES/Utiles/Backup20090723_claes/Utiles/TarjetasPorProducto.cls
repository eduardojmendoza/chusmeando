VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TarjetasPorProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
' LIMITED 2009. ALL RIGHTS RESERVED
'
' This software is only to be used for the purpose for which it has
' been provided. No part of it is to be reproduced, disassembled,
' transmitted, stored in a retrieval system or translated in any
' human or computer language in any way or for any other purposes
' whatsoever without the prior written consent of the Hong Kong and
' Shanghai Banking Corporation Limited. Infringement of copyright
' is a serious civil and criminal offence, which can result in
' heavy fines and payment of substantial damages.
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
' Nombre Modulo : TarjetasPorProducto
' Fec. Creaci�n : 23/06/2009
' Desarrollador : Jorge San Mart�n
' Descripci�n   : Lista las tarjetas (Titular y adicionales)
'                 de un determinado producto
'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.TarjetasPorProducto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

Const MAX_COMPUTERNAME_LENGTH = 255

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                                As Long
    '
    Const wcteFnName            As String = "IAction_Execute"
    Dim wvarStep                As Long
    '
    Dim wvarTipoDoc             As String
    Dim wvarNroDoc              As String
    Dim wvarTipoTarjeta         As String
    Dim wvarGrupoAfinHabilitado As String
    '
    Dim wobjXMLrequest          As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnnSACTRAN    As HSBCInterfaces.IDBConnection
    Dim wobjHSBC_DBCnnADINTAR    As HSBCInterfaces.IDBConnection
    Dim wobjHSBC_DBCnnBR         As HSBCInterfaces.IDBConnection
    '
    Dim wobjDBCnnSACTRAN               As ADODB.Connection
    Dim wobjDBCnnAdintar        As ADODB.Connection
    Dim wobjDBCnnBR             As ADODB.Connection
    '
    Dim wobjDBCmd               As ADODB.Command
    Dim wobjDBCmdADINTAR        As ADODB.Command
    
    Dim wobjDBParm              As ADODB.Parameter
    Dim wrstParametros          As ADODB.Recordset
    Dim wrstParametros2         As ADODB.Recordset
    
    Dim wrstPlasAdicion         As ADODB.Recordset
    Dim wrstPlasTitular         As ADODB.Recordset
    
    Dim wvarField               As Variant
    Dim wvarValues              As Variant
    
    Dim wvarNroTarjetaAConsultar As String
    Dim wvarStatusContinuar      As Boolean
    
    Dim wvarEnviarTarjeta        As Boolean
    
    Dim wvarNroProducto             As String
    Dim wvarAdiTipoProducto         As String
    Dim wvarTipoProducto            As String
    Dim wvarTipoProductoEnvia       As String
    Dim wobjDBCnnCOMISIONES         As ADODB.Connection
    Dim wobjHSBC_DBCnnCOMISIONES    As HSBCInterfaces.IDBConnection
    Dim wvarORIGEN As String
    
    Dim iCount As Long
    Dim iCount2 As Long
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    
    LogTrace "TarjetasPorProducto"
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//Request/TIPODOC").Text
    wvarNroDoc = wobjXMLrequest.selectSingleNode("//Request/NRODOC").Text
    wvarNroProducto = wobjXMLrequest.selectSingleNode("//Request/NROPRODUCTO").Text
    '
    
    'CONTROLO Q VENGAN DATOS DE LA PAGINA
    
    While Len(wvarNroDoc) < 15
        wvarNroDoc = "0" & wvarNroDoc
    Wend
    '
    Set wobjXMLrequest = Nothing
   
    wvarStep = 30
    
    '
    Set wobjHSBC_DBCnnSACTRAN = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjHSBC_DBCnnBR = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjHSBC_DBCnnADINTAR = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    Set wobjHSBC_DBCnnCOMISIONES = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    
    Set wobjDBCnnCOMISIONES = wobjHSBC_DBCnnCOMISIONES.GetDBConnection(mCte_COMISIONESDB)
   
    Set wobjDBCnnSACTRAN = wobjHSBC_DBCnnSACTRAN.GetDBConnection(mCte_SACTRAN)
    '
    Set wobjDBCnnAdintar = wobjHSBC_DBCnnADINTAR.GetDBConnection(gcteADINTAR_DBCnn)
    '
    Set wobjDBCnnBR = wobjHSBC_DBCnnBR.GetDBConnection(gcteBR_DBCnn)
    '
    
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnnBR
    wobjDBCmd.CommandText = "BR..spProductosCliente1 "
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@tipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@documento"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarNroDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wrstPlasTitular = wobjDBCmd.Execute
    '
    wvarStep = 81
    Set wobjDBCmd = Nothing
    Set wobjDBCmdADINTAR = CreateObject("ADODB.Command")
    Set wobjDBCmdADINTAR.ActiveConnection = wobjDBCnnAdintar
    wobjDBCmdADINTAR.CommandText = "SP_SACT_OBTENERADICIONALES "
    wobjDBCmdADINTAR.CommandType = adCmdStoredProc
    '
    wvarStep = 82
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TIPODNI"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmdADINTAR.Parameters.Append wobjDBParm
    '
    wvarStep = 83
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NRODOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adBigInt
    wobjDBParm.Value = wvarNroDoc
    wobjDBCmdADINTAR.Parameters.Append wobjDBParm
    '
    wvarStep = 85
    Set wrstPlasAdicion = wobjDBCmdADINTAR.Execute
    '
    
    wvarStep = 86
    Set wrstParametros = New ADODB.Recordset
    
    wrstParametros.fields.Append "TipoProducto", adChar, 1
    wrstParametros.fields.Append "TipoProductoAEnviar", adVarChar, 3
    wrstParametros.fields.Append "Producto", adChar, 20
    wrstParametros.fields.Append "IndividualEmpresarial", adChar, 1
    wrstParametros.fields.Append "Adicional", adChar, 1
    wrstParametros.fields.Append "AdiTipoDoc", adVarChar, 3
    wrstParametros.fields.Append "AdiNroDoc", adVarChar, 15
    wrstParametros.fields.Append "AdiNombre", adVarChar, 100
    wrstParametros.fields.Append "AdiCodSts", adVarChar, 2
    wrstParametros.fields.Append "AdiDesSts", adVarChar, 150
    
    wvarStep = 87
    wrstParametros.Open
    
    wvarStep = 88
    
    Do While Not wrstPlasTitular.EOF
        LogTrace "tarjeta titular: " & wrstPlasTitular("Producto").Value & "=" & wvarNroProducto & " // " & wrstPlasTitular("TipoProducto").Value & "=" & wvarTipoProducto
        If Trim(CStr(wrstPlasTitular("Producto").Value)) = Trim(CStr(wvarNroProducto)) Then
            'LogTrace "encontr� titular"
            wvarTipoProducto = wrstPlasTitular("TipoProducto").Value
            wvarField = Array("TipoProducto", "TipoProductoAEnviar", "Producto", "IndividualEmpresarial", "Adicional", "AdiCodSts")
            Select Case CStr(wrstPlasTitular("TipoProducto").Value)
                Case "M": wvarTipoProductoEnvia = "009"
                Case "7": wvarTipoProductoEnvia = "031"
                Case "6": wvarTipoProductoEnvia = "150"
                Case "V": wvarTipoProductoEnvia = "265"
                Case "E": wvarTipoProductoEnvia = "650"
                Case Else:  wvarTipoProductoEnvia = ""
            End Select
            LogTrace wvarTipoProductoEnvia
            wvarValues = Array(wrstPlasTitular("TipoProducto").Value, wvarTipoProductoEnvia, wrstPlasTitular("Producto").Value, wrstPlasTitular("IndividualEmpresarial").Value, "N", "X")
            wrstParametros.AddNew wvarField, wvarValues
            wrstPlasTitular.MoveLast
        End If
        wvarStep = 89
        wrstPlasTitular.MoveNext
        
    Loop
    wvarTipoProductoEnvia = ""

    Do While Not wrstPlasAdicion.EOF
        
        wvarStep = 90

        Select Case wrstPlasAdicion.fields("cod_estpla").Value
        Case 0:    wvarStatusContinuar = False   'INICIAL
        Case 1:    wvarStatusContinuar = False   'EN DISTRIBUCION
        Case 2:    wvarStatusContinuar = False   'EN SUCURSAL
        Case 3:    wvarStatusContinuar = False   'EN STOCK
        Case 4:    wvarStatusContinuar = True   'OPERATIVA
        Case 5:    wvarStatusContinuar = True   'OPERATIVA NO RENOVADA
        Case 6:    wvarStatusContinuar = True   'OPERATIVA CON ORDEN DE BAJA
        Case 7:    wvarStatusContinuar = True   'INHABILITADA
        Case 8:    wvarStatusContinuar = True   'BAJA
        Case 9:    wvarStatusContinuar = True   'VENCIDA
        Case 10:   wvarStatusContinuar = True   'DESTRUIDA
        Case 11:   wvarStatusContinuar = True   'TARJETA CON PROBLEMAS
        Case 12:   wvarStatusContinuar = True   'TARJETA INTERNAC. POR VIAJE
        Case 13:   wvarStatusContinuar = True   'TEMPORARIA
        Case Else: wvarStatusContinuar = True
        End Select
    
        If wvarStatusContinuar = True Then
            
            wvarStep = 94

            ' Conversi�n del c�digo de tarjeta ADINTAR al c�digo de SACT
            wvarStep = 95
            Select Case wrstPlasAdicion("cod_adm").Value
                Case 0
                    wvarTipoProductoEnvia = ""   'SISTEMA CENTRAL SC
                    wvarAdiTipoProducto = ""
                Case 1 'MASTERCARD
                    If wrstPlasAdicion("cod_ent").Value = 1 Then
                        wvarTipoProductoEnvia = "031" '7 - MASTERCARD MC HSBC
                        wvarAdiTipoProducto = "7"
                    ElseIf wrstPlasAdicion("cod_ent").Value = 5 Then
                        wvarTipoProductoEnvia = "009" 'M - MASTERCARD MC EX-BNL
                        wvarAdiTipoProducto = "M"
                    End If
                Case 2
                    wvarTipoProductoEnvia = ""   'CABAL CB
                Case 3 'VISA
                    If wrstPlasAdicion("cod_ent").Value = 1 Then
                        wvarTipoProductoEnvia = "150" '6 - VISA HSBC
                        wvarAdiTipoProducto = "6"
                    ElseIf wrstPlasAdicion("cod_ent").Value = 5 Then
                        wvarTipoProductoEnvia = "265" 'V VISA BNL
                        wvarAdiTipoProducto = "V"
                    End If
                Case 4
                    wvarTipoProductoEnvia = ""   'CARTA FRANCA    CF
                    wvarAdiTipoProducto = ""
                Case 5
                    wvarTipoProductoEnvia = ""   'CREDENCIAL CR
                    wvarAdiTipoProducto = ""
                Case 6
                    wvarTipoProductoEnvia = "650"   'E - AMEX AE
                    wvarAdiTipoProducto = "E"
                Case 7
                    wvarTipoProductoEnvia = "650"   'E - AMERICAN EXPRESS    AX
                    wvarAdiTipoProducto = "E"
            End Select
            
            If wvarTipoProducto = wvarAdiTipoProducto Then
                wvarField = Array("TipoProducto", "TipoProductoAEnviar", "Producto", "IndividualEmpresarial", "Adicional", _
                                  "AdiTipoDoc", "AdiNroDoc", "AdiNombre", "AdiCodSts", "AdiDesSts")
    
                wvarStep = 96
                wvarValues = Array(wvarAdiTipoProducto, wvarTipoProductoEnvia, wrstPlasAdicion("cod_pla").Value, _
                                  "X", "S", wrstPlasAdicion("cod_tipdoc").Value, _
                                  wrstPlasAdicion("nrodoc_cli").Value, wrstPlasAdicion("apell_y_nom_cli").Value, _
                                  wrstPlasAdicion.fields("cod_estpla").Value, wrstPlasAdicion.fields("desc_estpla").Value)
            
                wvarStep = 97
                wrstParametros.AddNew wvarField, wvarValues

            End If
        
        End If
        wrstPlasAdicion.MoveNext
        
    Loop
    
    wvarStep = 98
    pvarResponse = "<Response><ESTADO RESULTADO=""TRUE"" MENSAJE=""""/>"
    If Not wrstParametros.EOF Then
        wrstParametros.MoveFirst
    End If
    
    wvarStep = 99
    If Not wrstParametros.EOF Then
        
        wvarStep = 100
        '
        pvarResponse = pvarResponse & "<TARJETAS>"
        While Not wrstParametros.EOF
            wvarStep = 130
            iCount = iCount + 1
            If iCount > 300 Then
            End If
            
            If iCount > 350 Then
                mobjCOM_Context.SetComplete
                Exit Function
            End If
            
            wvarTipoTarjeta = ""
            wvarStep = 132
            Select Case wrstParametros.fields("TipoProducto")
                Case "6"
                    wvarTipoTarjeta = "VISA"
                    wvarORIGEN = "HSBC"
                Case "7"
                    wvarTipoTarjeta = "MASTERCARD"
                    wvarORIGEN = "HSBC"
                Case "9"
                    wvarTipoTarjeta = "MASTERCARD (ARGENCARD)"
                    wvarORIGEN = "HSBC"
                Case "L"
                    wvarTipoTarjeta = "LIDER"
                    wvarORIGEN = "HSBC"
                Case "G"
                    wvarTipoTarjeta = "GARBARINO"
                    wvarORIGEN = "HSBC"
                Case "X"
                    wvarTipoTarjeta = "MASTERCARD (MAXIMA)"
                    wvarORIGEN = "HSBC"
                Case "E"
                    wvarTipoTarjeta = "AMERICAN EXPRESS"
                    wvarORIGEN = "HSBC"
                Case "V"
                    wvarTipoTarjeta = "VISA"
                    wvarORIGEN = "BNL"
                Case "M"
                    wvarTipoTarjeta = "MASTERCARD"
                    wvarORIGEN = "BNL"
            End Select
            '
            
            If wvarTipoTarjeta <> "" Then
            
                wvarStep = 140
                wvarEnviarTarjeta = False
                If wrstParametros.fields("Adicional") = "N" Then
                    Set wobjDBCmd = Nothing
                    Set wobjDBCmd = CreateObject("ADODB.Command")
                    Set wobjDBCmd.ActiveConnection = wobjDBCnnBR
                    wobjDBCmd.CommandText = "BR..spdatostarjeta "
                    wobjDBCmd.CommandType = adCmdStoredProc
                    '
                    wvarStep = 150
                    Set wobjDBParm = CreateObject("ADODB.Parameter")
                    wobjDBParm.Name = "@tipoProducto"
                    wobjDBParm.Direction = adParamInput
                    wobjDBParm.Type = adVarChar
                    wobjDBParm.Size = 1
                    wobjDBParm.Value = wrstParametros.fields("TipoProducto")
                    wobjDBCmd.Parameters.Append wobjDBParm
                    '
                    wvarStep = 160
                    Set wobjDBParm = CreateObject("ADODB.Parameter")
                    wobjDBParm.Name = "@producto"
                    wobjDBParm.Direction = adParamInput
                    wobjDBParm.Type = adVarChar
                    wobjDBParm.Size = 20
                    wobjDBParm.Value = wrstParametros.fields("Producto")
                    wobjDBCmd.Parameters.Append wobjDBParm
                    '
                    wvarStep = 170
                    Set wrstParametros2 = wobjDBCmd.Execute
                    '
                    If Not wrstParametros2.EOF Then
                        wvarNroTarjetaAConsultar = Trim(wrstParametros2.fields("NroTarjetaTitular").Value)
                        wvarEnviarTarjeta = True
                    End If
                    
                    wvarStep = 175
                    If wrstParametros2.State = adStateOpen Then
                        wrstParametros2.Close
                    End If
                    
                Else
                    wvarStep = 175
                    wvarNroTarjetaAConsultar = Trim(wrstParametros.fields("Producto").Value)
                    wvarEnviarTarjeta = True
                
                End If
                
                If wvarEnviarTarjeta = True Then
                    wvarStep = 180

                    pvarResponse = pvarResponse & "<TARJETA>"
                    pvarResponse = pvarResponse & "<TIPOPRODUCTO>" & wrstParametros.fields("TipoProducto") & "</TIPOPRODUCTO>"
                    pvarResponse = pvarResponse & "<TIPOPRODUCTOCOD>" & wrstParametros.fields("TipoProductoAEnviar") & "</TIPOPRODUCTOCOD>"
                    pvarResponse = pvarResponse & "<ORIGEN>" & wvarORIGEN & "</ORIGEN>"
                    pvarResponse = pvarResponse & "<TIPO>" & wvarTipoTarjeta & "</TIPO>"
                    pvarResponse = pvarResponse & "<NUMERO>" & wvarNroTarjetaAConsultar & "</NUMERO>"
                    
                    pvarResponse = pvarResponse & "<ADICIONAL>" & wrstParametros.fields("Adicional") & "</ADICIONAL>"
                    pvarResponse = pvarResponse & "<ADICTIPODOC>" & wrstParametros.fields("AdiTipoDoc") & "</ADICTIPODOC>"
                    pvarResponse = pvarResponse & "<ADICNRODOC>" & wrstParametros.fields("AdiNroDoc") & "</ADICNRODOC>"
                    pvarResponse = pvarResponse & "<ADICNOMBRE>" & wrstParametros.fields("AdiNombre") & "</ADICNOMBRE>"
                    pvarResponse = pvarResponse & "<ADICCODSTS>" & wrstParametros.fields("AdiCodSts") & "</ADICCODSTS>"
                    pvarResponse = pvarResponse & "<ADICDESSTS>" & wrstParametros.fields("AdiDesSts") & "</ADICDESSTS>"
                    
                    pvarResponse = pvarResponse & "</TARJETA>"

                End If
                wvarStep = 239
            End If
            wvarStep = 242
            wrstParametros.MoveNext

        Wend
        wvarStep = 244
        pvarResponse = pvarResponse & "</TARJETAS>"

    Else
        wvarStep = 246
        pvarResponse = pvarResponse & "<TARJETAS></TARJETAS>"

    End If
    wvarStep = 248
    pvarResponse = pvarResponse & "</Response>"

    '
    wvarStep = 250
    Set wobjDBCmd = Nothing
    '
    wvarStep = 260
    wobjDBCnnSACTRAN.Close
    wobjDBCnnAdintar.Close
    wobjDBCnnBR.Close
    
    Set wobjDBCnnSACTRAN = Nothing
    Set wobjDBCnnAdintar = Nothing
    Set wobjDBCnnBR = Nothing
    
    Set wobjHSBC_DBCnnBR = Nothing
    Set wobjHSBC_DBCnnADINTAR = Nothing
    Set wobjHSBC_DBCnnSACTRAN = Nothing
    
    '
    wvarStep = 270
    Set wrstParametros = Nothing
    '
    wvarStep = 280

    mobjCOM_Context.SetComplete
    '
    
    Exit Function
    
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<Response><ESTADO RESULTADO=""FALSE"" MENSAJE=""Error, Paso : " & wvarStep & """/></Response>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


