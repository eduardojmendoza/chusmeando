VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetalleHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.DetalleHelp"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    Dim wvarEmpresa        As String
    Dim wvarCodigoProducto As String
    Dim wvarCodigoCausa    As String
    Dim wvarCodigoMotivo   As String
    Dim i                  As Integer
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstMotivos     As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DomDocument")
    With wobjXMLrequest
      .loadXML pvarRequest
      .async = False
    End With
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//DETALLEHELP/EMPRESA").Text
    wvarCodigoProducto = wobjXMLrequest.selectSingleNode("//DETALLEHELP/COD_PROD").Text
    wvarCodigoCausa = wobjXMLrequest.selectSingleNode("//DETALLEHELP/COD_CAUSA").Text
    wvarCodigoMotivo = wobjXMLrequest.selectSingleNode("//DETALLEHELP/COD_MOTIVO").Text
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_sac_detalleHelp"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_PROD"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodigoProducto
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_CAU"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodigoCausa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_MOT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodigoMotivo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wrstMotivos = wobjDBCmd.Execute
    Set wrstMotivos.ActiveConnection = Nothing
    '
    wvarStep = 110
    wobjDBCnn.Close
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    '*********************************************************************
    '
    If Not wrstMotivos.EOF Then
        '
        wvarStep = 140
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 150
        wrstMotivos.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 160
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 170
        Call wobjXMLDocs.loadXML(wobjXMLDocs.transformNode(wobjXSLDocs))
        Set wobjNodeList = wobjXMLDocs.selectNodes("//RELACIONPRODUCTO/CONTEXTHELP")
        For i = 0 To (wobjNodeList.length - 1)
          Set wobjNode = wobjNodeList.Item(i)
          wobjNode.selectSingleNode("DESCRIPCION").Text = "<![CDATA[" & wobjNode.selectSingleNode("DESCRIPCION").Text & "]]>"
        Next
        Set wobjNodeList = Nothing
        pvarResponse = wobjXMLDocs.xml
    Else
      pvarResponse = "<RELACIONPRODUCTO><ESTADO RESULTADO=""FALSE"" MENSAJE=""No se ha podido encontrar la ayuda de contexto"" /></RELACIONPRODUCTO>"
    End If
    '**************************************************************************
    
    wvarStep = 180
    
    wvarStep = 190
    wrstMotivos.Close
    '
    wvarStep = 200
    Set wrstMotivos = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    
    
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl' > "
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='RELACIONPRODUCTO'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "    <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "    </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CONTEXTHELP'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DESCRIPCION'><xsl:value-of select='@REP_HELP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function


