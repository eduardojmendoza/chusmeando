VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Campanias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.Campanias"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode    As MSXML2.IXMLDOMNode
    '
    Dim wvarEmpresa     As String
    Dim wvarEstado      As String
    Dim wvarTipoDoc     As String
    Dim wvarNroDoc      As String
    Dim i               As Integer
    Dim wvarTipoDocRVP  As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstCampanias      As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
      .async = False
      Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEstado = wobjXMLrequest.selectSingleNode("//AGENDA/ESTADO").Text
    '
    wvarStep = 30
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//AGENDA/TIPODOC").Text
    '
    wvarStep = 40
    wvarNroDoc = wobjXMLrequest.selectSingleNode("//AGENDA/NRODOC").Text
    wvarNroDoc = NumFilled(wvarNroDoc, 15)
    wvarStep = 45
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//AGENDA/EMPRESA").Text
    wvarTipoDocRVP = wobjXMLrequest.selectSingleNode("//AGENDA/TIPODOCRVP").Text
    Set wobjXMLrequest = Nothing
    '
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      'wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    ElseIf wvarEmpresa = wconstCODIGONYL Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      'wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    End If
    
    'If wvarEmpresa = wconstCODIGONYL Then
    '  wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
    'End If
    '
    wvarStep = 50
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 60
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 70
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_lista_campanias_agenda '" & wvarEstado & "','" & wvarTipoDoc & "','" & wvarNroDoc & "'," & wvarEmpresa
    wobjDBCmd.CommandType = adCmdText

    '
'    wvarStep = 80
'    Set wobjDBParm = CreateObject("ADODB.Parameter")
'    wobjDBParm.Name = "@estado"
'    wobjDBParm.Direction = adParamInput
'    wobjDBParm.Type = adVarChar
'    wobjDBParm.Size = 1
'    wobjDBParm.Value = wvarEstado
'    wobjDBCmd.Parameters.Append wobjDBParm
'    Set wobjDBParm = Nothing
'    '
'    wvarStep = 90
'    Set wobjDBParm = CreateObject("ADODB.Parameter")
'    wobjDBParm.Name = "@tipoDoc"
'    wobjDBParm.Direction = adParamInput
'    wobjDBParm.Type = adChar
'    wobjDBParm.Size = 3
'    wobjDBParm.Value = UCase(Left(Trim(wvarTipoDoc & ""), 3))
'    wobjDBCmd.Parameters.Append wobjDBParm
'    Set wobjDBParm = Nothing
'    '
'    wvarStep = 100
'    Set wobjDBParm = CreateObject("ADODB.Parameter")
'    wobjDBParm.Name = "@nroDoc"
'    wobjDBParm.Direction = adParamInput
'    wobjDBParm.Type = adChar
'    wobjDBParm.Size = 15
'    wobjDBParm.Value = wvarNroDoc
'    wobjDBCmd.Parameters.Append wobjDBParm
'    Set wobjDBParm = Nothing
    
    'wvarStep = 110
    Set wrstCampanias = wobjDBCmd.Execute
    Set wrstCampanias.ActiveConnection = Nothing
    '
    wvarStep = 120
    wobjDBCnn.Close
    '
    wvarStep = 130
    'Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 140
    '*********************************************************************
    '
    If Not wrstCampanias.EOF Then
        '
        
        wvarStep = 150
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 160
        
        wrstCampanias.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 170
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 180
        
        pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
        '
    End If
    '**************************************************************************
    wvarStep = 190
    wrstCampanias.Close
    '
    wvarStep = 200
    Set wrstCampanias = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAMPA�AS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CAMPA�A'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO'><xsl:value-of select='@age_cam_cod' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TIENE_ENTREVISTAS'><xsl:value-of select='@tiene_Entrevistas' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGENDA_INCOMPLETA'><xsl:value-of select='@agenda_Incompleta' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DESCRIPCION'><xsl:value-of select='@age_cam_des' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TIENEBASE'><xsl:value-of select='@tiene_base' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TIPO'><xsl:value-of select='@age_tipo' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='RAZON_SOCIAL'><xsl:value-of select='@Razon_Social' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='DOMICILIO'><xsl:value-of select='@Domicilio' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='CODIGO_POSTAL'><xsl:value-of select='@CodigoPostal' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='LOCALIDAD'><xsl:value-of select='@Localidad' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='TELEFONO'><xsl:value-of select='@Telefono' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='INGRESO'><xsl:value-of select='@Ingreso' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='LINEACREDITO'><xsl:value-of select='@linea_credito' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='SUCURSAL'><xsl:value-of select='@Sucursal' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='ORIGEN'><xsl:value-of select='@Origen' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='FECHA_NACIMIENTO'><xsl:value-of select='@Fecha_Nacimiento' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='FECHA'><xsl:value-of select='@Fecha' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO1'><xsl:value-of select='@AGE_NOMBRECAMPO1' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO1'><xsl:value-of select='@BC_CAMPO1' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO2'><xsl:value-of select='@AGE_NOMBRECAMPO2' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO2'><xsl:value-of select='@BC_CAMPO2' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO3'><xsl:value-of select='@AGE_NOMBRECAMPO3' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO3'><xsl:value-of select='@BC_CAMPO3' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO4'><xsl:value-of select='@AGE_NOMBRECAMPO4' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO4'><xsl:value-of select='@BC_CAMPO4' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO5'><xsl:value-of select='@AGE_NOMBRECAMPO5' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO5'><xsl:value-of select='@BC_CAMPO5' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO6'><xsl:value-of select='@AGE_NOMBRECAMPO6' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO6'><xsl:value-of select='@BC_CAMPO6' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO7'><xsl:value-of select='@AGE_NOMBRECAMPO7' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO7'><xsl:value-of select='@BC_CAMPO7' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO8'><xsl:value-of select='@AGE_NOMBRECAMPO8' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO8'><xsl:value-of select='@BC_CAMPO8' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO9'><xsl:value-of select='@AGE_NOMBRECAMPO9' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO9'><xsl:value-of select='@BC_CAMPO9' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO10'><xsl:value-of select='@AGE_NOMBRECAMPO10' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO10'><xsl:value-of select='@BC_CAMPO10' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO11'><xsl:value-of select='@AGE_NOMBRECAMPO11' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO11'><xsl:value-of select='@BC_CAMPO11' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='AGE_NOMBRECAMPO12'><xsl:value-of select='@AGE_NOMBRECAMPO12' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   <xsl:attribute name='BC_CAMPO12'><xsl:value-of select='@BC_CAMPO12' /></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function
