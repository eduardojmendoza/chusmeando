VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LimiteCompraTC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'CLAES 20090630
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.LimiteCompraTC"

Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstRecordSet   As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim wvarNroPedido       As String
    Dim wvarCodEntidad      As String
    Dim wvarCodAdmin        As String
    Dim wvarCuenta          As String
    Dim wvarMensaje         As String
    
    
    
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'LogTrace " - sacA_Utiles.LimiteCompraTC -"
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    'App.LogEvent pvarRequest
    wvarStep = 20
    wvarNroPedido = wobjXMLrequest.selectSingleNode("//Request/NROPEDIDO").Text
    wvarStep = 21
    wvarCodEntidad = wobjXMLrequest.selectSingleNode("//Request/CODENTIDAD").Text
    wvarStep = 22
    wvarCodAdmin = wobjXMLrequest.selectSingleNode("//Request/CODADMIN").Text
    wvarStep = 23
    wvarCuenta = wobjXMLrequest.selectSingleNode("//Request/CUENTA").Text
    
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.LimiteCompraTC - Parametros:  wvarCodEntidad:" & wvarCodEntidad & " - wvarCodAdmin: " & wvarCodAdmin & " - wvarCuenta: " & wvarCuenta & " - Step: " & wvarStep
    
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteADINTAR_DBCnn)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "P_busca_suc_limcred"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@cod_ent"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCodEntidad
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Cod_Adm"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCodAdmin
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Cuenta"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 20
    wobjDBParm.Value = wvarCuenta
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarMensaje = ""
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@mensaje"
    wobjDBParm.Direction = adParamOutput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 100
    'wobjDBParm.Value = wvarMensaje
    wobjDBCmd.Parameters.Append wobjDBParm
'
  
    '
'    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.LimiteCompraTC - Step: " & wvarStep
    wvarStep = 110
    Set wrstRecordSet = wobjDBCmd.Execute
    'LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.LimiteCompraTC - Step: " & wvarStep
    wvarStep = 120
    '*********************************************************************
   '
    '*********************************************************************
    
    
    wvarStep = 130
    If wrstRecordSet.State = adStateClosed Then
        'pvarResponse = "<RESPONSE><ESTADO RESULTADO=""FALSE"" MENSAJE=""ERROR Sin Conexion con Adintar"" CODIGO=""01""/></RESPONSE>"
        pvarResponse = "<RESPONSE><ESTADO RESULTADO=""FALSE"" MENSAJE=""04-Error No se encontr� la cuenta informada'"" CODIGO=""01""/></RESPONSE>"
        LogTrace " - sacA_Utiles.LimiteCompraTC - Step: " & wvarStep & " - Respuesta: " & pvarResponse
        Exit Function
    End If
    wvarStep = 140
    If wrstRecordSet.EOF Then
       pvarResponse = "<RESPONSE>"
       pvarResponse = pvarResponse & "<ESTADO RESULTADO=""FALSE"" MENSAJE=""" & wvarMensaje & """ CODIGO=""""/>"
       pvarResponse = pvarResponse & "</RESPONSE>"
    Else
        wvarStep = 150
        pvarResponse = "<RESPONSE>"
        'pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE=wrstTransac.EOF&"""" CODIGO=""""/>"
        pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE="""" CODIGO=""""/>"
        While Not wrstRecordSet.EOF
            pvarResponse = pvarResponse & "<LIMITECOMPRATC>"
            pvarResponse = pvarResponse & "<CODSUCADM>" & wrstRecordSet.fields("cod_sucadm").Value & "</CODSUCADM>"
            pvarResponse = pvarResponse & "<DESCSUCADM>" & wrstRecordSet.fields("desc_sucadm").Value & "</DESCSUCADM>"
            pvarResponse = pvarResponse & "<PORCLIMITECOMPRA>" & wrstRecordSet.fields("porc_limite_compra").Value & "</PORCLIMITECOMPRA>"
            pvarResponse = pvarResponse & "<PORCLIMITECUOTA>" & wrstRecordSet.fields("porc_limite_cuota").Value & "</PORCLIMITECUOTA>"
            pvarResponse = pvarResponse & "<PORCLIMITECREDITO>" & wrstRecordSet.fields("porc_limite_credito").Value & "</PORCLIMITECREDITO>"
            pvarResponse = pvarResponse & "<LIMITECOMPRA>" & wrstRecordSet.fields("LimiteCompra").Value & "</LIMITECOMPRA>"
            pvarResponse = pvarResponse & "</LIMITECOMPRATC>"
            wrstRecordSet.MoveNext
        Wend
        pvarResponse = pvarResponse & "</RESPONSE>"
    End If
'    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.LimiteCompraTC - Step: " & wvarStep & " pvarResponse: " & pvarResponse
    wvarStep = 160
    Set wrstRecordSet.ActiveConnection = Nothing
    '
    wvarStep = 170
    wobjDBCnn.Close
    '
    wvarStep = 180
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    
    Set wobjNode = Nothing
    Set wobjNodeList = Nothing
    Set wobjXMLDocs = Nothing
    Set wobjXSLDocs = Nothing
    '**************************************************************************
    wvarStep = 190
    wrstRecordSet.Close
    '
    wvarStep = 200
    Set wrstRecordSet = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    
    LogTrace "Termino Ok - Pedido: " & wvarNroPedido & " - sacA_Utiles.LimiteCompraTC - Step: " & wvarStep
    
    Exit Function
  
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<RESPONSE><ESTADO RESULTADO=""FALSE"" MENSAJE="" ERROR: " & Err.Description & ", Paso : " & wvarStep & """" & " CODIGO=""01""/></RESPONSE>"
    LogTrace "ERROR: " & Err.Number & " - " & Err.Description & " - sacA_Utiles.LimiteCompraTC - Step: " & wvarStep
    mobjCOM_Context.SetComplete
     
'    If Err.Number = mCte_TimeOutError Then
'        IAction_Execute = xERR_TIMEOUT_RETURN
'        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
'                         mcteClassName, _
'                         wcteFnName, _
'                         wvarStep, _
'                         Err.Number, _
'                         "Error= [" & Err.Number & "] - " & Err.Description, _
'                         vbLogEventTypeError
'    Else
'        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
'                         mcteClassName, _
'                         wcteFnName, _
'                         wvarStep, _
'                         Err.Number, _
'                         "Error= [" & Err.Number & "] - " & Err.Description, _
'                         vbLogEventTypeError
'        IAction_Execute = xERR_UNEXPECTED
'    End If
'    mobjCOM_Context.SetAbort
End Function





