VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CargarDocSac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.CargarDocSAC"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName       As String = "IAction_Execute"
    Dim wvarStep           As Long
    '
    Dim wobjXMLrequest     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn     As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn          As ADODB.Connection
    Dim wobjDBCmd          As ADODB.Command
    Dim wobjDBParm         As ADODB.Parameter
    Dim wrstDocumentos     As ADODB.Recordset
    '
    Dim wobjXMLDocs        As MSXML2.DOMDocument
    Dim wobjXSLDocs        As MSXML2.DOMDocument
    '
    Dim wvarEmpresa        As String
    Dim i                  As Integer
    '
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '

    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
      .async = False
      Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//DOCUMENTOS/EMPRESA").Text
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_MUL_TIPOSDOCUMENTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodSAC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodORI2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wrstDocumentos = wobjDBCmd.Execute
    Set wrstDocumentos.ActiveConnection = Nothing
    '
    wvarStep = 90
    wobjDBCnn.Close
    '
    wvarStep = 100
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 110
    '*********************************************************************
    If Not wrstDocumentos.EOF Then
      '
      wvarStep = 120
      Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
      Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
      '
      wvarStep = 130
      wrstDocumentos.save wobjXMLDocs, adPersistXML
      '
      wvarStep = 140
      wobjXSLDocs.async = False
      Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
      '
      wvarStep = 150
      
      Call wobjXMLDocs.loadXML(wobjXMLDocs.transformNode(wobjXSLDocs))
      Set wobjNodeList = wobjXMLDocs.selectNodes("//TIPOSDOCUMENTO/TIPODOCUMENTO")
      wvarStep = 160
      For i = 0 To (wobjNodeList.length - 1)
        Set wobjNode = wobjNodeList.Item(i)
        wobjNode.selectSingleNode("DESCRIPCION").Text = "<![CDATA[" & wobjNode.selectSingleNode("DESCRIPCION").Text & "]]>"
      Next
      wvarStep = 170
      Set wobjNode = Nothing
      Set wobjNodeList = Nothing
      pvarResponse = wobjXMLDocs.xml
    Else
      pvarResponse = "<TIPOSDOCUMENTO>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO=""FALSE"" MENSAJE=""No se han encontrado Documentos para la empresa seleccionada"" />"
      pvarResponse = pvarResponse & "</TIPOSDOCUMENTO>"
    End If
    '**************************************************************************
    wvarStep = 180
    wrstDocumentos.Close
    '
    wvarStep = 190
    Set wrstDocumentos = Nothing
    '
    wvarStep = 200
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='TIPOSDOCUMENTO'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "   <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='TIPODOCUMENTO'>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='TIPO'><xsl:value-of select='@TDO_COD_SAC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='DESCRIPCION'><xsl:value-of select='@TDO_DESCRIPCION' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
End Function


