VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Provincias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'CLAES 20090630
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.Provincias"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstRecordSet   As ADODB.Recordset
    '
    Dim wobjXMLDocs     As MSXML2.DOMDocument
    Dim wobjXSLDocs     As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim wvarNroPedido       As String
    Dim wvarCodAdm          As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    LogTrace " - sacA_Utiles.Provincias -"
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    App.LogEvent pvarRequest
    wvarNroPedido = wobjXMLrequest.selectSingleNode("//TRANSAC/NROPEDIDO").Text
    wvarCodAdm = wobjXMLrequest.selectSingleNode("//TRANSAC/CODADM").Text
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteADINTAR_DBCnn)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "ADP_NOV_CBOPRO"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@nCodAdm"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCodAdm
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    wvarStep = 70
    Set wrstRecordSet = wobjDBCmd.Execute
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    wvarStep = 80
    '*********************************************************************
   '
    '*********************************************************************
'   <RESPONSE>
'        <ESTADO RESULTADO MENSAJE CODIGO />
        '    <PROVINCIAS>
'                <CODIGO></CODIGO>
'                <DESCRIPCION></DESCRIPCION>
    '        </PROVINCIAS>
'   </RESPONSE>
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    If wrstRecordSet.EOF Then
       pvarResponse = "<RESPONSE>"
       pvarResponse = pvarResponse & "<ESTADO RESULTADO=""FALSE"" MENSAJE=""No existen Administradoras"" CODIGO=""""/>"
       pvarResponse = pvarResponse & "</RESPONSE>"
        Else
        pvarResponse = "<RESPONSE>"
        'pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE=wrstTransac.EOF&"""" CODIGO=""""/>"
        pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE="""" CODIGO=""""/>"
        While Not wrstRecordSet.EOF
            pvarResponse = pvarResponse & "<PROVINCIAS>"
            pvarResponse = pvarResponse & "<CODIGO>" & wrstRecordSet.fields("Valor").Value & "</CODIGO>"
            pvarResponse = pvarResponse & "<DESCRIPCION>" & wrstRecordSet.fields("Descri").Value & "</DESCRIPCION>"
            pvarResponse = pvarResponse & "</PROVINCIAS>"
            wrstRecordSet.MoveNext
        Wend
        pvarResponse = pvarResponse & "</RESPONSE>"
    End If
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep & " pvarResponse: " & pvarResponse
    
    Set wrstRecordSet.ActiveConnection = Nothing
    '
    wvarStep = 90
    wobjDBCnn.Close
    '
    wvarStep = 100
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    
    Set wobjNode = Nothing
    Set wobjNodeList = Nothing
    Set wobjXMLDocs = Nothing
    Set wobjXSLDocs = Nothing
    '**************************************************************************
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    wvarStep = 130
    wrstRecordSet.Close
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    wvarStep = 150
    Set wrstRecordSet = Nothing
    '
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    wvarStep = 170
    mobjCOM_Context.SetComplete
    LogTrace "Pedido: " & wvarNroPedido & " - sacA_Utiles.Provincias - Step: " & wvarStep
    Exit Function
  
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    LogTrace "ERROR: " & Err.Number & " - " & Err.Description & " - sacA_Utiles.Provincias - Step: " & wvarStep
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function









