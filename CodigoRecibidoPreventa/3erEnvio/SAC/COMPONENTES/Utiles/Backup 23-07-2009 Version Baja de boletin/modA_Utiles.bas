Attribute VB_Name = "modA_Registraciones"
Option Explicit
'
'
Public Const gcteBR_DBCnn = "BR.UDL"
Public Const mCte_SACTRAN = "SACTRAN.UDL" 'JESM 20090630
Public Const mCte_COMISIONESDB = "COMISIONES.UDL"
Public Const gcteSEGURIDAD_DBCnn = "ADMIN_SEGURIDAD.UDL"
Public Const gcteDOCTHOS_DBCnn = "DOCTHOS.UDL"
'CLAES 20090629 INI
Public Const gcteADINTAR_DBCnn = "ADINTAR.UDL"
'CLAES 20090629 FIN
'
Public Const mCte_TimeOutError = -2147217871
'
'--- Manejado por el 'CmdProcessor'
Public Const xERR_UNEXPECTED = 1
Public Const xERR_TIMEOUT_RETURN = 501
'
'CODIGOS DE EMPRESAS
Public Const wconstCODIGOBANCO = "2"
Public Const wconstCODIGODOCTHOS = "4"
Public Const wconstCODIGONYL = "8"

'SECTOR DE CALL CENTRE
Public Const mCte_SECTOR_CALLCENTRE = "041"

