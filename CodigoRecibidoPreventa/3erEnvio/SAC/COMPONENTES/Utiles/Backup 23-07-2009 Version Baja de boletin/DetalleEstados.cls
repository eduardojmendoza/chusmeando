VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetalleEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.DetalleEstados"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName       As String = "IAction_Execute"
    Dim wvarStep           As Long
    '
    Dim wobjXMLrequest     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn     As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn          As ADODB.Connection
    Dim wobjDBCmd          As ADODB.Command
    Dim wobjDBParm         As ADODB.Parameter
    Dim wrstMotivoContacto As ADODB.Recordset
    '
    Dim wobjXMLDocs        As MSXML2.DOMDocument
    Dim wobjXSLDocs        As MSXML2.DOMDocument
    '
    Dim wvarEmpresa        As String
    Dim wvarCausa          As String
    Dim wvarMotivo         As String
    Dim wvarVerEnConsulta  As String
    '
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    '
    Dim i               As Integer
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 2
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
      .async = False
      Call .loadXML(pvarRequest)
    End With
    '
    'wvarStep = 3
    'wvarEmpresa = wobjXMLrequest.selectSingleNode("//ESTADOS/COD_EMPRESA").Text
    '
    wvarStep = 4
    wvarCausa = wobjXMLrequest.selectSingleNode("//ESTADOS/COD_CAUSA").Text
    '
    wvarStep = 6
    wvarMotivo = wobjXMLrequest.selectSingleNode("//ESTADOS/COD_MOTIVO").Text
    '
    wvarStep = 7
    wvarVerEnConsulta = wobjXMLrequest.selectSingleNode("//ESTADOS/VERENCONSULTA").Text
    
    Set wobjXMLrequest = Nothing

    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_sac_detalleestados "
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    'wvarStep = 32
    'Set wobjDBParm = CreateObject("ADODB.Parameter")
    'wobjDBParm.Name = "@COD_EMP"
    'wobjDBParm.Direction = adParamInput
    'wobjDBParm.Type = adInteger
    'wobjDBParm.Value = wvarEmpresa
    'wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 35
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_CAUSA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCausa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 35
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_MOTIVO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarMotivo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 36
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@ES_CALL"
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarVerEnConsulta
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 40
    Set wrstMotivoContacto = wobjDBCmd.Execute
    Set wrstMotivoContacto.ActiveConnection = Nothing
    '
    wvarStep = 50
    wobjDBCnn.Close
    '
    wvarStep = 60
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 70
    '*********************************************************************
    '
    If Not wrstMotivoContacto.EOF Then
        '
        wvarStep = 80
        Set wobjXMLDocs = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLDocs = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 90
        wrstMotivoContacto.save wobjXMLDocs, adPersistXML
        '
        wvarStep = 100
        wobjXSLDocs.async = False
        Call wobjXSLDocs.loadXML(p_GetXSLDocumentos())
        '
        wvarStep = 110
        'pvarResponse = wobjXMLDocs.transformNode(wobjXSLDocs)
        Call wobjXMLDocs.loadXML(wobjXMLDocs.transformNode(wobjXSLDocs))
        Set wobjNodeList = wobjXMLDocs.selectNodes("//ESTADOSCAUSA/ESTADOCAUSA")
        For i = 0 To (wobjNodeList.length - 1)
          Set wobjNode = wobjNodeList.Item(i)
          wobjNode.selectSingleNode("DESCRIPCION").Text = ReplaceAcentos("<![CDATA[" & wobjNode.selectSingleNode("DESCRIPCION").Text & "]]>")
        Next
        Set wobjNodeList = Nothing
        pvarResponse = wobjXMLDocs.xml
    End If
    '**************************************************************************
    wvarStep = 120
    wrstMotivoContacto.Close
    '
    wvarStep = 130
    Set wrstMotivoContacto = Nothing
    '
    wvarStep = 140
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLDocumentos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ESTADOSCAUSA'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "   <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='ESTADOCAUSA'>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='CODIGO'><xsl:value-of select='@EST_COD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='DESCRIPCION'><xsl:value-of select='@EST_DESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='ESTADO'><xsl:value-of select='@ESTADO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='NODERIVA'><xsl:value-of select='@EST_NODERIVA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='AUTORIZA_CLIENTE'><xsl:value-of select='@EST_AUTCLI' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='DERIVA_SUC_CLI'><xsl:value-of select='@REL_SUC_CLI' /></xsl:element>"
    
    wvarStrXSL = wvarStrXSL & "       <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLDocumentos = wvarStrXSL
    'App.LogEvent wvarStrXSL
End Function


