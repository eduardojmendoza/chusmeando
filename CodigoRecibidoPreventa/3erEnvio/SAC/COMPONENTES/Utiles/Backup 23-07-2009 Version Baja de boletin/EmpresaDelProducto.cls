VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "EmpresaDelProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Utiles.EmpresaDelProducto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    
    '
    'INSERT INTO actions VALUES ('EmpresaDelProducto','Devuelve la empresa correspondiente al producto','sacA_Utiles.EmpresaDelProducto',1,NULL);
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wvarProducCod   As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstTipoProdu    As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DomDocument")
    With wobjXMLrequest
      .async = False
      .loadXML (pvarRequest)
    End With
    wvarProducCod = wobjXMLrequest.selectSingleNode("//PRODUCTOS/PRODUCTOCODIGO").Text
    '
    wvarStep = 20
    Set wobjXMLrequest = Nothing
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_TIPOPRODUCTO_EMPRESA"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    'App.LogEvent pvarRequest
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TIPOPRODUC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarProducCod
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wrstTipoProdu = wobjDBCmd.Execute
    Set wrstTipoProdu.ActiveConnection = Nothing
    '
    wvarStep = 70
    'wobjDBCnn.Close
    '
    wvarStep = 290
    Set wobjDBCmd = Nothing
    '
    If Not wrstTipoProdu.EOF Then
        pvarResponse = "<RESPONSE>"
        '<ESTADO RESULTADO=""TRUE"" MENSAJE=""""/>"
            pvarResponse = pvarResponse & "<TIPOPRODUCTO>"
                pvarResponse = pvarResponse & "<PRO_COD_EMP>" & wrstTipoProdu("PRO_COD_EMP").Value & "</PRO_COD_EMP>"
                'pvarResponse = pvarResponse & "<TIP_PROD_COD>" & wrstTipoProdu("TIP_PROD_COD").Value & "</TIP_PROD_COD>"
                'pvarResponse = pvarResponse & "<TIP_PROD_DESC>" & wrstTipoProdu("TIP_PROD_DESC").Value & "</TIP_PROD_DESC>"
                'pvarResponse = pvarResponse & "<ESTADOREG>" & wrstTipoProdu("ESTADO").Value & "</ESTADOREG>"
            pvarResponse = pvarResponse & "</TIPOPRODUCTO>"
        pvarResponse = pvarResponse & "</RESPONSE>"
      
    Else
        pvarResponse = "<Response><ESTADO RESULTADO=""FALSE"" MENSAJE=""No hay datos""/></Response>"
      
    End If
    '
    
    wvarStep = 340
    Set wrstTipoProdu = Nothing
    
    wvarStep = 350
    wobjDBCnn.Close
    
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 360
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
'        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function
