VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Seguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
Const iCodGru_Desarrollo As String = "642"
Const iCodGru_Supervisor As String = "250"
Const iCodGru_OperadAccesProducto As String = "6"
Const iCodGru_OperadorSinIvr As String = "300"
Const iCodGru_OperadorCallCentre As String = "5"
Const iCodGru_OperadorSinIvrAccesProducto As String = "301"
'
Const mcteClassName As String = "sacA_Login.Seguridad"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'


'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wvarGrupo        As String
    '
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarGrupo = wobjXMLrequest.selectSingleNode("//SEGURIDAD/CODIGOGRUPO").Text
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    '*********************************************************************
    Select Case wvarGrupo
    Case iCodGru_Desarrollo
      wvarStep = 40
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PUEDECONSULTARCLIENTES>TRUE</PUEDECONSULTARCLIENTES>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARRECLAMOS>TRUE</PUEDEREGISTRARRECLAMOS>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARVARIOS>TRUE</PUEDEREGISTRARVARIOS>"
      pvarResponse = pvarResponse & "<PUEDEVERCONSULTAS>TRUE</PUEDEVERCONSULTAS>"
      pvarResponse = pvarResponse & "<PUEDEVERDETALLEPRODUCTO>TRUE</PUEDEVERDETALLEPRODUCTO>"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    Case iCodGru_Supervisor
      wvarStep = 50
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PUEDECONSULTARCLIENTES>TRUE</PUEDECONSULTARCLIENTES>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARRECLAMOS>TRUE</PUEDEREGISTRARRECLAMOS>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARVARIOS>TRUE</PUEDEREGISTRARVARIOS>"
      pvarResponse = pvarResponse & "<PUEDEVERCONSULTAS>TRUE</PUEDEVERCONSULTAS>"
      pvarResponse = pvarResponse & "<PUEDEVERDETALLEPRODUCTO>TRUE</PUEDEVERDETALLEPRODUCTO>"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    Case iCodGru_OperadAccesProducto
      wvarStep = 60
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PUEDECONSULTARCLIENTES>TRUE</PUEDECONSULTARCLIENTES>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARRECLAMOS>TRUE</PUEDEREGISTRARRECLAMOS>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARVARIOS>TRUE</PUEDEREGISTRARVARIOS>"
      pvarResponse = pvarResponse & "<PUEDEVERCONSULTAS>TRUE</PUEDEVERCONSULTAS>"
      pvarResponse = pvarResponse & "<PUEDEVERDETALLEPRODUCTO>TRUE</PUEDEVERDETALLEPRODUCTO>"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    Case iCodGru_OperadorSinIvr
      wvarStep = 70
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PUEDECONSULTARCLIENTES>TRUE</PUEDECONSULTARCLIENTES>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARRECLAMOS>TRUE</PUEDEREGISTRARRECLAMOS>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARVARIOS>TRUE</PUEDEREGISTRARVARIOS>"
      pvarResponse = pvarResponse & "<PUEDEVERCONSULTAS>TRUE</PUEDEVERCONSULTAS>"
      pvarResponse = pvarResponse & "<PUEDEVERDETALLEPRODUCTO>FALSE</PUEDEVERDETALLEPRODUCTO>"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    Case iCodGru_OperadorCallCentre
      wvarStep = 80
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PUEDECONSULTARCLIENTES>FALSE</PUEDECONSULTARCLIENTES>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARRECLAMOS>TRUE</PUEDEREGISTRARRECLAMOS>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARVARIOS>TRUE</PUEDEREGISTRARVARIOS>"
      pvarResponse = pvarResponse & "<PUEDEVERCONSULTAS>TRUE</PUEDEVERCONSULTAS>"
      pvarResponse = pvarResponse & "<PUEDEVERDETALLEPRODUCTO>TRUE</PUEDEVERDETALLEPRODUCTO>"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    Case iCodGru_OperadorSinIvrAccesProducto
      wvarStep = 90
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PUEDECONSULTARCLIENTES>TRUE</PUEDECONSULTARCLIENTES>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARRECLAMOS>TRUE</PUEDEREGISTRARRECLAMOS>"
      pvarResponse = pvarResponse & "<PUEDEREGISTRARVARIOS>TRUE</PUEDEREGISTRARVARIOS>"
      pvarResponse = pvarResponse & "<PUEDEVERCONSULTAS>TRUE</PUEDEVERCONSULTAS>"
      pvarResponse = pvarResponse & "<PUEDEVERDETALLEPRODUCTO>TRUE</PUEDEVERDETALLEPRODUCTO>"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    Case Else
      wvarStep = 100
      pvarResponse = "<SEGURIDAD>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha encontrado el grupo del usuario actual' />"
      pvarResponse = pvarResponse & "</SEGURIDAD>"
    End Select
    '*********************************************************************
    wvarStep = 110
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


