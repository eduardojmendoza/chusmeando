VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "ObtenerAccesos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Login.ObtenerAccesos"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    Dim i               As Integer
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjnode        As MSXML2.IXMLDOMNode
    '
    Dim wvarUser            As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstSeguridad       As ADODB.Recordset
    Dim wrstEmpresas        As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarUser = wobjXMLrequest.selectSingleNode("//ACCESOS/USER").Text
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_SEU_OBTENERDATOSUSUARIO"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@USUARIO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 10
    wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    '
    Set wrstSeguridad = wobjDBCmd.Execute
    Set wrstSeguridad.ActiveConnection = Nothing
    '
    wvarStep = 70
    Set wobjDBCmd = Nothing
    '
    If Not wrstSeguridad.EOF Then
      pvarResponse = "<ACCESOS>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE="""" />"
      For i = 1 To 8 'Unidades de negocio
        If wrstSeguridad.Fields(1 + i).Value Then 'Esta habilitado
          wvarStep = 80
          Set wobjDBCmd = CreateObject("ADODB.Command")
          Set wobjDBCmd.ActiveConnection = wobjDBCnn
          wobjDBCmd.CommandText = "SP_MUL_OBTENEREMPRESA"
          wobjDBCmd.CommandType = adCmdStoredProc
          wvarStep = 90
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@Orden"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adInteger
          wobjDBParm.Value = i
          wobjDBCmd.Parameters.Append wobjDBParm
          '
          wvarStep = 100
          '
          Set wrstEmpresas = wobjDBCmd.Execute
          Set wrstEmpresas.ActiveConnection = Nothing
          '
          wvarStep = 110
          Set wobjDBCmd = Nothing
          If Not wrstEmpresas.EOF Then
            pvarResponse = pvarResponse & "<ACCESO>"
            pvarResponse = pvarResponse & "<CODIGO>" & wrstEmpresas.Fields("EMP_COD_EMP").Value & "</CODIGO>"
            pvarResponse = pvarResponse & "<DESCRIPCION><![CDATA[" & wrstEmpresas.Fields("EMP_DES_EMP").Value & "]]></DESCRIPCION>"
            pvarResponse = pvarResponse & "</ACCESO>"
          End If
          wrstEmpresas.Close
          Set wrstEmpresas = Nothing
        End If
      Next
      pvarResponse = pvarResponse & "</ACCESOS>"
      wvarStep = 120
      Set wobjDBCmd = Nothing
    Else
      pvarResponse = "<ACCESOS><ESTADO RESULTADO=""FALSE"" MENSAJE=""No se han podido encontrar los accesos del usuario " & getUsuario(wvarUser) & """ /></ACCESOS>"
    End If
    wrstSeguridad.Close
    Set wrstSeguridad = Nothing
    '
    wvarStep = 130
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 140
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


