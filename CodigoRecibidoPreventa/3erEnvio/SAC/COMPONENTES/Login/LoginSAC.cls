VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "LoginSAC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Login.LoginSAC"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    Dim i               As Integer
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjNodeList    As MSXML2.IXMLDOMNodeList
    Dim wobjnode        As MSXML2.IXMLDOMNode
    '
    Dim wvarSistema         As String
    Dim wvarUser            As String
    Dim wvarPerfilSAC       As Integer
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstSeguridad       As ADODB.Recordset
    Dim wrstEmpresas        As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    'App.LogEvent "paso:" & wvarStep
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    'App.LogEvent "LOGIN SAC"
    wvarStep = 20
    'App.LogEvent "paso:" & wvarStep
    wvarSistema = wobjXMLrequest.selectSingleNode("//LOGIN/SISTEMA").Text
    wvarUser = wobjXMLrequest.selectSingleNode("//LOGIN/USER").Text
    '
    wvarStep = 30
    'App.LogEvent "paso:" & wvarStep
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    'App.LogEvent "paso:" & wvarStep
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    wvarStep = 45
    '
    Set wobjNodeList = wobjXMLrequest.selectNodes("//LOGIN/PERFILES/PERFIL")
    For Each wobjnode In wobjNodeList
      wvarStep = 50
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wvarStep = 60
      wobjDBCmd.CommandText = "SP_REP_OBTENERPERFILSAC"
      wvarStep = 70
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 80
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@SISTEMA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = UCase(wvarSistema)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 90
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@PERFILBIGI"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = CInt(Val(Trim(wobjnode.Text)))
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      Set wrstSeguridad = wobjDBCmd.Execute
      Set wrstSeguridad.ActiveConnection = Nothing
      '
      wvarStep = 100
      Set wobjDBCmd = Nothing
      If Not wrstSeguridad.EOF Then
        wvarPerfilSAC = wrstSeguridad.Fields("REP_COD_SAC").Value
        Exit For
      End If
    Next
    
    wvarStep = 105
    'App.LogEvent "paso:" & wvarStep
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    Set wobjNodeList = Nothing
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 110
    'App.LogEvent "paso:" & wvarStep
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 111
    'App.LogEvent "paso:" & wvarStep
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    
    wvarStep = 112
    'App.LogEvent "paso:" & wvarStep
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 120
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wvarStep = 130
    wobjDBCmd.CommandText = "SP_SEU_OBTENERDATOSUSUARIO"
    wvarStep = 140
    wobjDBCmd.CommandType = adCmdStoredProc
    wvarStep = 150
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@USUARIO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 10
    wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 160
    '
    Set wrstSeguridad = wobjDBCmd.Execute
    Set wrstSeguridad.ActiveConnection = Nothing
    '
    wvarStep = 170
    Set wobjDBCmd = Nothing
    'wobjDBCnn.Close
    '
    If Not wrstSeguridad.EOF Then
      pvarResponse = "<LOGIN>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<PERFIL>"
      pvarResponse = pvarResponse & "<PERFILSAC>" & wvarPerfilSAC & "</PERFILSAC>"
      pvarResponse = pvarResponse & "<SECTORUSUARIO>" & wrstSeguridad.Fields("SEU_COD_SEC").Value & "</SECTORUSUARIO>"
      pvarResponse = pvarResponse & "</PERFIL>"
      pvarResponse = pvarResponse & "</LOGIN>"
      
      wvarStep = 220
      Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
      '
      wvarStep = 222
      Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
      
      wvarStep = 224
      wobjDBCnn.Execute "SET NOCOUNT ON"
      '
      wvarStep = 230
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wvarStep = 240
      wobjDBCmd.CommandText = "SP_SEU_LOGUEARUSUARIO"
      wvarStep = 250
      wobjDBCmd.CommandType = adCmdStoredProc
      wvarStep = 260
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@USUARIO"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 270
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@SISTEMA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = wvarSistema
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 280
      wobjDBCmd.Execute
      '
      wvarStep = 290
      Set wobjDBCmd = Nothing
      wobjDBCnn.Close
      Set wobjDBCnn = Nothing
      Set wobjHSBC_DBCnn = Nothing
    Else
      pvarResponse = "<LOGIN><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido encontrar el usuario " & getUsuario(wvarUser) & "' /></LOGIN>"
    End If
    wrstSeguridad.Close
    Set wrstSeguridad = Nothing
    '
    wvarStep = 300
    '
    wvarStep = 310
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


