VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetalleObs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.DetalleObs"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarNroPedido          As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstPedido          As ADODB.Recordset
    '
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjNodeList        As IXMLDOMNodeList
    Dim wobjNode            As IXMLDOMNode
    Dim wobjNewNode         As IXMLDOMNode
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarNroPedido = wobjXMLrequest.selectSingleNode("//GENERAL/NROPEDIDO").Text
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    '---------------------------------
    wvarStep = 50
    Set wobjDBCmd = Nothing
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_Age_List_SegEstadoReclaHistO"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumPedido"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarNroPedido
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wrstPedido = wobjDBCmd.Execute
    Set wrstPedido.ActiveConnection = Nothing
    If Not wrstPedido.EOF Then
      wvarStep = 80
      Set wobjXMLResponse = CreateObject("MSXML2.DomDocument")
      wrstPedido.save wobjXMLResponse, adPersistXML
      '
      wvarStep = 90
      pvarResponse = wobjXMLResponse.xml
    End If
    '
    wvarStep = 110
    If wrstPedido.State <> 0 Then wrstPedido.Close
    '
    wvarStep = 120
    Set wrstPedido = Nothing
    '
    wvarStep = 130
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLHistorialEstados() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data/z:row'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEESTADO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='FECHAREGISTRACION'><xsl:value-of select='@his_fec_reg' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ANTIGUEDAD'><xsl:value-of select='@ANTIGUEDAD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'><xsl:value-of select='@est_desc' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='USUARIO'><xsl:value-of select='@his_usu_nom' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='SECTORREGISTRADOR'><xsl:value-of select='@SEC_REG' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='SECTORRESPONSABLE'><xsl:value-of select='@sec_des_sec' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='OBSERVACIONES'><xsl:value-of select='@his_obs' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='HORAREGISTRACION'><xsl:value-of select='@his_hor_reg' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLHistorialEstados = wvarStrXSL
End Function
Private Function p_GetXSLPedidos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data/z:row'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPEDIDO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NROPEDIDO'><xsl:value-of select='@REG_NUM_PED' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='FECHAINGRESO'><xsl:value-of select='@OPE_FEC_REG_OPE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'><xsl:value-of select='@EST_DESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DIASESTIMADOS'><xsl:value-of select='@ESTIMADO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CAUSA'><xsl:value-of select='@CAU_CON_DES_CAU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MOTIVO'><xsl:value-of select='@MOT_CON_DES_MOT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:choose>"
    wvarStrXSL = wvarStrXSL & "     <xsl:when select=""@CIERRE='S'"">"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='RECLAMOCERRADO'>TRUE</xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='FECHACIERRE'><xsl:value-of select='@ODE_FEC_ULT_MAN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:when>"
    wvarStrXSL = wvarStrXSL & "     <xsl:otherwise>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='RECLAMOCERRADO'>FALSE</xsl:element>"
    wvarStrXSL = wvarStrXSL & "       <xsl:element name='FECHACIERRE'><xsl:value-of select='@AHORA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:otherwise>"
    wvarStrXSL = wvarStrXSL & "   </xsl:choose>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPODOC'><xsl:value-of select='@TIPODOC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NRODOCUMENTO'><xsl:value-of select='@DOCUMENTO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MODOCONTACTO'><xsl:value-of select='@MODOCONTACTO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLPedidos = wvarStrXSL
End Function


Private Function p_GetXSLPersona() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DETALLES'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:apply-templates select='//xml/rs:data/z:row'/>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPERSONA'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='RAZONSOCIAL'><xsl:value-of select='@RAZONSOCIAL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CATEGORIA'><xsl:value-of select='@CATEGORIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLPersona = wvarStrXSL
End Function


