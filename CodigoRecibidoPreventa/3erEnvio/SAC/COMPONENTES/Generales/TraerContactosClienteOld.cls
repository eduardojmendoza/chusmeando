VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "TraerContactosCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.TraerContactosCliente"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarEmpresa        As String
    Dim wvarDocumento      As String
    Dim wvarTipoDoc        As String
    Dim wvarCausa          As String
    Dim wvarColSort        As String
    Dim wvarTipoConsulta   As String
    Dim wvarAux            As String
    Dim wvarTipoDocRVP     As String
    'Dim wvarResponseAux    As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstConsultas       As ADODB.Recordset
    '
    Dim wobjXMLResponse      As MSXML2.DOMDocument
    Dim wobjXMLResponseP     As MSXML2.DOMDocument
    Dim wobjXMLResponseR     As MSXML2.DOMDocument
    Dim wobjXSLResponse      As MSXML2.DOMDocument
    Dim wobjXMLNodeListP     As MSXML2.IXMLDOMNodeList
    Dim wobjXMLNodeListR     As MSXML2.IXMLDOMNodeList
    Dim wobjXMLNode          As MSXML2.IXMLDOMNode
    Dim wobjXMLNodeGeneral   As MSXML2.IXMLDOMNode
    Dim wobjXMLNodeContactos As MSXML2.IXMLDOMNode
    Dim wobjXMLNodeP         As MSXML2.IXMLDOMNode
    Dim wobjXMLNodeR         As MSXML2.IXMLDOMNode
    Dim wobjXMLElement       As MSXML2.IXMLDOMElement
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//GENERAL/EMPRESA").Text
    wvarDocumento = wobjXMLrequest.selectSingleNode("//GENERAL/NRODOCUMENTO").Text
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//GENERAL/TIPODOCUMENTO").Text
    wvarCausa = wobjXMLrequest.selectSingleNode("//GENERAL/TIPOCAUSA").Text
    wvarColSort = wobjXMLrequest.selectSingleNode("//GENERAL/ORDEN").Text
    wvarTipoConsulta = wobjXMLrequest.selectSingleNode("//GENERAL/TIPOCONSULTA").Text
    wvarTipoDocRVP = wobjXMLrequest.selectSingleNode("//GENERAL/TIPODOCRVP").Text
    '
    wvarStep = 25
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//GENERAL/TIPODOCUMENTO").Text = wvarTipoDoc
    ElseIf (wvarEmpresa = wconstCODIGONYL) Or (wvarEmpresa = wconstCODIGOLBA) Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//GENERAL/TIPODOCUMENTO").Text = wvarTipoDoc
    End If
    'En caso de Docthos se deben traer las registraciones de los
    'familiares directos tambien
    'If (wvarEmpresa = wconstCODIGODOCTHOS) And (wvarTipoDoc <> "DES") Then
    '  wvarDocumento = Left(NumFilled(wvarDocumento, 15), 13) & "%"
    'Else
      wvarDocumento = NumFilled(wvarDocumento, 15)
    'End If
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_con_TraerConsYOpe"
    wobjDBCmd.CommandType = adCmdStoredProc
    wobjDBCmd.CommandTimeout = 120
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 65
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarDocumento
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoCausa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarCausa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Orden"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarColSort
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoConsulta"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarTipoConsulta
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wrstConsultas = wobjDBCmd.Execute
    Set wrstConsultas.ActiveConnection = Nothing
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    '
    wvarStep = 130
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 140
    Set wobjXMLResponse = CreateObject("MSXML2.DomDocument")
    Set wobjXMLNodeGeneral = wobjXMLResponse.createElement("GENERAL")
    If Not wrstConsultas.EOF Then
      '
      'Set wobjXMLNodeContactos = wobjXMLResponse.createElement("CONTACTOS")
      'Set wobjXMLNodeContactos = wobjXMLNodeGeneral.appendChild(wobjXMLNodeContactos)
      '
      wvarStep = 160
      Set wobjXMLResponseP = CreateObject("MSXML2.DOMDocument")
      Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
      '
      wvarStep = 170
      wrstConsultas.save wobjXMLResponseP, _
                          adPersistXML
      '
      wvarStep = 180
      wobjXSLResponse.async = False
      Call wobjXSLResponse.loadXML(p_GetXSLConsultasPendientes())
      '
      wvarStep = 190
      Call wobjXMLResponseP.loadXML(wobjXMLResponseP.transformNode(wobjXSLResponse))
      '
      wvarStep = 190
      Set wobjXMLNodeListP = wobjXMLResponseP.selectNodes("//CONTACTOS/CONTACTO")
      For Each wobjXMLNodeP In wobjXMLNodeListP
        'Call wobjXMLNodeContactos.appendChild(wobjXMLNodeP)
        Call wobjXMLNodeGeneral.appendChild(wobjXMLNodeP)
      Next
      Set wobjXMLNodeP = Nothing
      Set wobjXMLNodeListP = Nothing
    End If
    'AHORA SE CONSULTAN LOS RESUELTOS
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_CON_TRAERRESUELTOS"
    wobjDBCmd.CommandType = adCmdStoredProc
    wobjDBCmd.CommandTimeout = 120
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 65
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarDocumento
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoCausa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarCausa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Orden"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarColSort
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoConsulta"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarTipoConsulta
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wrstConsultas = wobjDBCmd.Execute
    Set wrstConsultas.ActiveConnection = Nothing
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    '
    wvarStep = 130
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 140
    If Not wrstConsultas.EOF Then
      '
      'If wobjXMLNodeContactos Is Nothing Then
      '  Set wobjXMLNodeContactos = wobjXMLResponse.createElement("CONTACTOS")
      '  Set wobjXMLNodeContactos = wobjXMLNodeGeneral.appendChild(wobjXMLNodeContactos)
      'End If
      '
      wvarStep = 160
      Set wobjXMLResponseR = CreateObject("MSXML2.DOMDocument")
      Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
      '
      wvarStep = 170
      wrstConsultas.save wobjXMLResponseR, _
                          adPersistXML
      '
      wvarStep = 180
      wobjXSLResponse.async = False
      Call wobjXSLResponse.loadXML(p_GetXSLConsultasResueltos())
      '
      wvarStep = 190
      Call wobjXMLResponseR.loadXML(wobjXMLResponseR.transformNode(wobjXSLResponse))
      '
      wvarStep = 190
      Set wobjXMLNodeListR = wobjXMLResponseR.selectNodes("//CONTACTOS/CONTACTO")
      For Each wobjXMLNodeR In wobjXMLNodeListR
        'Call wobjXMLNodeContactos.appendChild(wobjXMLNodeR)
        Call wobjXMLNodeGeneral.appendChild(wobjXMLNodeR)
      Next
      Set wobjXMLNodeR = Nothing
      Set wobjXMLNodeListR = Nothing
      '
    End If
    Call wobjXMLResponse.appendChild(wobjXMLNodeGeneral)
    'Set wobjXMLElement = wobjXMLResponse.createElement("ESTADO")
    'If wobjXMLResponse.selectNodes("//GENERAL/CONTACTOS").length > 0 Then
    'If wobjXMLResponse.selectNodes("//GENERAL/CONTACTO").length > 0 Then
    '  Call wobjXMLElement.setAttribute("RESULTADO", "TRUE")
    '  Call wobjXMLElement.setAttribute("MENSAJE", "")
    '  wobjXMLNodeGeneral.appendChild wobjXMLElement
    'Else
    '  Call wobjXMLElement.setAttribute("RESULTADO", "FALSE")
    '  Call wobjXMLElement.setAttribute("MENSAJE", "No se han podido encontrar registros para el cliente")
    '  wobjXMLNodeGeneral.appendChild wobjXMLElement
    'End If
    'Set wobjXMLElement = Nothing
    'Set wobjXMLNodeContactos = Nothing
    Set wobjXMLNodeGeneral = Nothing
    pvarResponse = wobjXMLResponse.xml
    '
    Set wobjXMLNode = Nothing
    Set wobjXMLResponse = Nothing
    '
    wvarStep = 190
    If wrstConsultas.State <> 0 Then wrstConsultas.Close
    '
    wvarStep = 200
    Set wrstConsultas = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLConsultasPendientes() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CONTACTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CONTACTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NROPEDIDO'><xsl:value-of select='@campo2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='FECHA'><xsl:value-of select='@campo1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='USUARIO'><xsl:value-of select='@campo4' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPOOPERACION'><xsl:value-of select='@campo3' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='PRODUCTO'><xsl:value-of select='@campo5' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CAUSA'><xsl:value-of select='@campo6' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MOTIVO'><xsl:value-of select='@campo7' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'><xsl:value-of select='@campo8' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='SECTORRESP'><xsl:value-of select='@campo9' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "   <xsl:element name='OBSERVACIONES'><xsl:value-of select='@campo10' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MODOCONTACTO'><xsl:value-of select='@campo11' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='HORA'><xsl:value-of select='@campo12' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLConsultasPendientes = wvarStrXSL
End Function
Private Function p_GetXSLConsultasResueltos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CONTACTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='CONTACTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NROPEDIDO'><xsl:value-of select='@campo2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='FECHA'><xsl:value-of select='@campo1' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='USUARIO'><xsl:value-of select='@campo4' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPOOPERACION'><xsl:value-of select='@campo3' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='PRODUCTO'><xsl:value-of select='@campo5' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CAUSA'><xsl:value-of select='@campo6' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MOTIVO'><xsl:value-of select='@campo7' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'><xsl:value-of select='@campo8' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='SECTORRESP'><xsl:value-of select='@campo9' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "   <xsl:element name='OBSERVACIONES'><xsl:value-of select='@campo10' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MODOCONTACTO'><xsl:value-of select='@campo11' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='HORA'><xsl:value-of select='@campo12' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLConsultasResueltos = wvarStrXSL
End Function




