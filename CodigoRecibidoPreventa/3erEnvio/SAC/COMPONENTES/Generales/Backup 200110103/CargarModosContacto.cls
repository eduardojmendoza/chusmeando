VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CargarModosContacto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.CargarModosContacto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstModos           As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_age_list_tipocontacto"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wrstModos = wobjDBCmd.Execute
    Set wrstModos.ActiveConnection = Nothing
    '
    wvarStep = 50
    pvarResponse = "<GENERAL>"
    '
    If Not wrstModos.EOF Then
      '
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<MODOS_CONTACTO>"
      Do
        pvarResponse = pvarResponse & "<MODO " & _
        "CODIGO='" & wrstModos.Fields("MOD_CON_COD").Value & "' " & _
        "DESCRIPCION='" & wrstModos.Fields("MOD_CON_DES").Value & "' />"
        wrstModos.MoveNext
        '
      Loop While Not wrstModos.EOF
      pvarResponse = pvarResponse & "</MODOS_CONTACTO> "
    Else
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha encontrado el detalle de la operación' />"
    End If
    '
    wvarStep = 110
    pvarResponse = pvarResponse & _
                  "</GENERAL>"
    '
    wvarStep = 120
    wobjDBCnn.Close
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    If wrstModos.State <> 0 Then wrstModos.Close
    '
    wvarStep = 140
    Set wrstModos = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function


