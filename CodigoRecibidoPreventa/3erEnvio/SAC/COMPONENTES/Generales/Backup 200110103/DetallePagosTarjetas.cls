VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetallePagosTarjetas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.DetallePagosTarjetas"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarSesion          As String
    Dim wvarFecha          As String
    Dim wvarTipo          As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstOperacion          As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarSesion = wobjXMLrequest.selectSingleNode("//GENERAL/SESION").Text
    wvarFecha = wobjXMLrequest.selectSingleNode("//GENERAL/FECHA").Text
    wvarTipo = wobjXMLrequest.selectSingleNode("//GENERAL/TIPO").Text
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_BRDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_detalleoperacionesclientes"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Sesion"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarSesion)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@FechaReg"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adNumeric
    wobjDBParm.Precision = 14
    wobjDBParm.Value = wvarFecha
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoOperacion"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 16
    wobjDBParm.Value = wvarTipo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wrstOperacion = wobjDBCmd.Execute
    Set wrstOperacion.ActiveConnection = Nothing
    '
    wvarStep = 100
    pvarResponse = "<GENERAL>"
    '
    If Not wrstOperacion.EOF Then
      '
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<DETALLES>"
      Do
        wvarStep = 110
        pvarResponse = pvarResponse & "<DETALLEOPERACION " & _
        "NUMREFERENCIA='" & wrstOperacion.Fields("NumReferencia").Value & "' " & _
        "FECHA='" & DateToString(wrstOperacion.Fields("Fecha").Value) & "' " & _
        "TIPOTARJETA='" & wrstOperacion.Fields("TipoTarjeta").Value & "' " & _
        "CUENTATARJETA='" & wrstOperacion.Fields("CuentaTarjeta").Value & "' " & _
        "TIPOCUENTADEBITO='" & wrstOperacion.Fields("TipoCuentaDebito").Value & "' " & _
        "CUENTADEBITO='" & wrstOperacion.Fields("CuentaDebito").Value & "' " & _
        "MONTOADEBITAR='" & Format(wrstOperacion.Fields("MontoADebitar").Value, ".00") & "' " & _
        "COTIZACIONCOMPRA='" & Format(wrstOperacion.Fields("CotizacionCompra").Value, ".00") & "' " & _
        "COTIZACIONVENTA='" & Format(wrstOperacion.Fields("CotizacionVenta").Value, ".00") & "' " & _
        "OPCIONFECHAPAGO='" & wrstOperacion.Fields("OpcionFechaPago").Value & "' " & _
        "FECHAPAGO='" & DateToString(wrstOperacion.Fields("FechaPago").Value) & "' " & _
        "ESTADO='" & wrstOperacion.Fields("Estado").Value & "' " & _
        "OPERADORA='" & wrstOperacion.Fields("Operadora").Value & "' " & _
        "SUPERVISORA='" & wrstOperacion.Fields("Supervisora").Value & "' " & _
        "FECHAREVERSA='" & wrstOperacion.Fields("FechaReversa").Value & "' />"
        wrstOperacion.MoveNext
        '
      Loop While Not wrstOperacion.EOF
      pvarResponse = pvarResponse & "</DETALLES>"
    Else
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha encontrado el detalle de la operación' />"
    End If
    '
    pvarResponse = pvarResponse & _
                  "</GENERAL>"
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    If wrstOperacion.State <> 0 Then wrstOperacion.Close
    '
    wvarStep = 140
    Set wrstOperacion = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


