--------------------------------------------------
Componente 
	sacA_General
	Clase 
		TraerContactosCliente
--------------------------------------------------
REQUEST String

<GENERAL>
	<NRODOCUMENTO />
	<TIPODOCUMENTO />
	<TIPOCAUSA />
	<ORDEN />
	<TIPOCONSULTA />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<CONTACTOS>
		<CONTACTO NROPEDIDO=''
		FECHA=''
		USUARIO=''	
		TIPOOPERACION=''
		PRODUCTO=''
		CAUSA=''
		MOTIVO=''
		ESTADO=''
		DIASESTIMADOS=''
		SECTORRESP=''
		OBSERVACIONES='' 
		MODOCONTACTO='' 
		HORA='' />
	</CONTACTOS>
</GENERAL>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		DetalleContacto
--------------------------------------------------
REQUEST String

<GENERAL>
	<NROPEDIDO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLEPEDIDO>
		<FECHAINGRESO/>
		<ESTADO/>         <![CDATA[[]]>
		<DIASESTIMADOS/>
		<CAUSA/>          <![CDATA[[]]>
		<MOTIVO/>         <![CDATA[[]]>
		<RECLAMOCERRADO/>
		<FECHACIERRE/>
		<TIPODOC/>
		<NRODOCUMENTO/>
		<MODOCONTACTO/>   <![CDATA[[]]>
	</DETALLEPEDIDO>
	<DETALLEPERSONA>
		<RAZONSOCIAL/>    <![CDATA[[]]>
		<CATEGORIA/>      <![CDATA[[]]>
	</DETALLEPERSONA>
	<ESTADOS>
		<ESTADO RESULTADO='TRUE' MENSAJE='' />
		<DETALLEESTADO>
			<FECHAREGISTRACION/>
			<ANTIGUEDAD/>          <![CDATA[[]]>
			<ESTADO/>              <![CDATA[[]]>
			<USUARIO/>
			<SECTORREGISTRADOR/>   <![CDATA[[]]>
			<SECTORRESPONSABLE/>   <![CDATA[[]]>
			<OBSERVACIONES/>       <![CDATA[[]]>
			<FECHAREGISTRACION/>
		</DETALLEESTADO>
	</ESTADOS>
</GENERAL>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		DetalleCompraVenta
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
		NUMREFERENCIA=''
		FECHA=''	
		TIPOCUENTADEBITO=''
		CUENTADEBITO=''
		TIPOCUENTACREDITO=''
		CUENTACREDITO=''
		MONTO=''
		COTIZACION=''
		ESTADO=''
		OPERADORA='' 
		SUPERVISORA=''
		FECHAREVERSA='' />
	</DETALLES>
</GENERAL>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		DetallePagosTarjetas
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
		NUMREFERENCIA=''
		FECHA=''	
		TIPOTARJETA=''
		CUENTATARJETA=''
		TIPOCUENTADEBITO=''
		CUENTADEBITO=''
		MONTOADEBITAR=''
		COTIZACIONCOMPRA=''
		COTIZACIONVENTA=''
		OPCIONFECHAPAGO=''
		FECHAPAGO='' 
		ESTADO=''
		OPERADORA=''
		SUPERVISORA=''
		FECHAREVERSA='' />
	</DETALLES>
</GENERAL>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		DetallePedidos
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
		NUMREFERENCIA=''
		FECHA=''	
		OPERADORA=''
		TIPOPEDIDO=''
		TIPOCUENTA=''
		CUENTA=''
		SUCURSALRETIRO=''
		TIPOLIBRETA=''
		FECHADESDE=''
		FECHAHASTA=''
		DETALLE='' />
	</DETALLES>
</GENERAL>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		DetallePlazosFijos
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
		NUMREFERENCIA=''
		FECHA=''	
		TIPOCUENTADEBITO=''
		CUENTADEBITO=''
		IMPORTE=''
		MONEDA=''
		MONTO=''
		PLAZO=''
		TASA=''
		COTIZACION=''
		TIPOPLAZOFIJO='' 
		NUMCERTIFICADO=''
		ESTADO=''
		OPERADORA=''
		MONTOACOBRAR=''
		FECHAVENCIMIENTO=''
		SUPERVISORA=''
		FECHAREVERSA='' />
	</DETALLES>
</GENERAL>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		DetalleTransferencias
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
		NUMREFERENCIA=''
		FECHA=''	
		TIPOCUENTADEBITO=''
		CUENTADEBITO=''
		TIPOCUENTACREDITO=''
		CUENTACREDITO=''
		MONTO=''
		ESTADO=''
		OPERADORA=''
		SUPERVISORA=''
		FECHAREVERSA='' />
	</DETALLES>
</GENERAL>

--------------------------------------------------
Componente 
	sacA_Generales
	Clase 
		DetalleFondos
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
        NUMREFERENCIA=''
        NUMINVERSOR=''
        FECHA=''
        TIPOCUENTADEBITO=''
        CUENTADEBITO=''
        IMPORTEDEBITO=''
        MONEDA=''
        CANTIDADCUOTASPARTES=''
        AGRUPACION=''
        TIPOFONDO=''
        CUENTAFONDO=''
        ESTADO=''
        SESION=''
        OPERADORA=''
        SUPERVISORA=''
        FECHAREVERSA=''
        TIPOOPERACION=''
        TIPOCUENTACREDITO=''
        CUENTACREDITO=''
        OBSERVACIONES='' />
	</DETALLES>
</GENERAL>

--------------------------------------------------
Componente 
	sacA_Generales
	Clase 
		DetalleAltaFondo
--------------------------------------------------
REQUEST String

<GENERAL>
	<SESION />
	<FECHA />
	<TIPO />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<DETALLES>
	<DETALLEOPERACION 
	        NUMINVERSOR=''
	        FECHA=''
	        OPERADORA=''
	        SESION='' />
	</DETALLES>
</GENERAL>

--------------------------------------------------
Componente 
	sacA_General
	Clase 
		MotRegVarias			
--------------------------------------------------
REQUEST String



RESPONSE String

<REGISTRACIONES_VARIAS>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<MOTIVO> 
		<CODIGO></CODIGO>
		<DESCRIPCION><![CDATA[ ]]></DESCRIPCION>
	</MOTIVO>
</REGISTRACIONES_VARIAS>
--------------------------------------------------
Componente 
	sacA_General
	Clase 
		CargarModosContacto
--------------------------------------------------
REQUEST String



RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<MODOS_CONTACTO>
		<MODO CODIGO=''
		DESCRIPCION=''/>
	</MODOS_CONTACTO>
</GENERAL>

--------------------------------------------------
Componente 
	sacA_General
	Clase 
		ObtenerSector
--------------------------------------------------
REQUEST String

<GENERAL>
	<CODIGOSECTOR />
</GENERAL>

RESPONSE String

<GENERAL>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	<SECTOR DESCRIPCION='' />
</GENERAL>
