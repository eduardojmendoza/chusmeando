VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "MotRegVarias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.MotRegVarias"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    Dim wvarEmpresa         As String
    Dim i                   As Integer
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstOperacion       As ADODB.Recordset
    '
    Dim wobjXML             As MSXML2.DOMDocument
    Dim wobjXSL             As MSXML2.DOMDocument
    Dim wobjXMLNodeList     As MSXML2.IXMLDOMNodeList
    Dim wobjXMLNode         As MSXML2.IXMLDOMNode
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//REGVARIAS/EMPRESA").Text
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 40
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_age_list_registracion"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 50
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Descripcion"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 20
    wobjDBParm.Value = Null
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wrstOperacion = wobjDBCmd.Execute
    Set wrstOperacion.ActiveConnection = Nothing
    '
    wvarStep = 80
''''''    pvarResponse = "<GENERAL>"
''''''    '
''''''    If Not wrstOperacion.EOF Then
''''''      '
''''''      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
''''''      pvarResponse = pvarResponse & "<REGISTRACIONES_VARIAS>"
''''''      Do
''''''        wvarStep = 80
''''''
''''''        pvarResponse = pvarResponse & "<MOTIVO " & _
''''''        "CODIGO='" & wrstOperacion.Fields("REG_COD").Value & "' " & _
''''''        "DESCRIPCION='" & wrstOperacion.Fields("REG_DES").Value & "' />"
''''''        wrstOperacion.MoveNext
''''''        '
''''''      Loop While Not wrstOperacion.EOF
''''''      pvarResponse = pvarResponse & "</REGISTRACIONES_VARIAS>"
''''''    Else
''''''      pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se han encontrado tipos para las registraciones varias' />"
''''''    End If
''''''    '
''''''    wvarStep = 90
''''''    pvarResponse = pvarResponse & _
''''''                  "</GENERAL>"
    If Not wrstOperacion.EOF Then
      Set wobjXML = CreateObject("MSXML2.DomDocument")
      wobjXML.async = False
      wrstOperacion.save wobjXML, adPersistXML
      '
      wvarStep = 90
      Set wobjXSL = CreateObject("MSXML2.DomDocument")
      wobjXSL.async = False
      wobjXSL.loadXML (p_GetXSLRegVs())
      '
      wvarStep = 100
      wobjXML.loadXML (wobjXML.transformNode(wobjXSL))
      Set wobjXMLNodeList = wobjXML.selectNodes("//REGISTRACIONES_VARIAS/MOTIVO")
      wvarStep = 100
      For Each wobjXMLNode In wobjXMLNodeList
        wobjXMLNode.selectSingleNode("DESCRIPCION").Text = "<![CDATA[" & wobjXMLNode.selectSingleNode("DESCRIPCION").Text & "]]>"
      Next
      wvarStep = 110
      pvarResponse = wobjXML.xml
      Set wobjXMLNode = Nothing
      Set wobjXMLNodeList = Nothing
      Set wobjXSL = Nothing
      Set wobjXML = Nothing
    Else
      pvarResponse = "<REGISTRACIONES_VARIAS><ESTADO RESULTADO=""FALSE"" MENSAJE=""No se han podido encontrar registraciones"" /></REGISTRACIONES_VARIAS>"
    End If
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    If wrstOperacion.State <> 0 Then wrstOperacion.Close
    '
    wvarStep = 140
    Set wrstOperacion = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLRegVs() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='REGISTRACIONES_VARIAS'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "    <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "    <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='MOTIVO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CODIGO'><xsl:value-of select='@reg_cod' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DESCRIPCION'><xsl:value-of select='@reg_des' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLRegVs = wvarStrXSL
End Function


