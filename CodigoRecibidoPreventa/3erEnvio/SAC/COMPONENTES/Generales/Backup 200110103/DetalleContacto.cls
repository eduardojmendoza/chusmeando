VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetalleContacto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.DetalleContacto"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wVarFechaDelReclamo As String
    Dim wVarFechaCierre     As String
    Dim wVarReclamoCerrado  As Boolean
    Dim wVarAntiguedad      As String
    Dim wvarTipDoc          As String
    Dim wvarNroDoc          As String
    Dim wvarResponseTmp     As String
    Dim wvarEmpresa         As String
    Dim wvarLastObs         As String
    Dim wvarFechaReg        As String
    Dim i                   As Integer
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarNroPedido       As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstPedido          As ADODB.Recordset
    '
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    Dim wobjNodeList        As IXMLDOMNodeList
    Dim wobjNode            As IXMLDOMNode
    Dim wobjNewNode         As IXMLDOMNode
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarNroPedido = wobjXMLrequest.selectSingleNode("//GENERAL/NROPEDIDO").Text
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_age_list_seguiestadorecla"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumPedido"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarNroPedido
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wrstPedido = wobjDBCmd.Execute
    Set wrstPedido.ActiveConnection = Nothing
    '
    wvarStep = 80
    pvarResponse = pvarResponse & "<GENERAL>"
    '
    If Not wrstPedido.EOF Then
      wvarStep = 90
      wvarEmpresa = Trim(wrstPedido.Fields("EMPRESA").Value)
      wVarReclamoCerrado = (Trim(wrstPedido.Fields("CIERRE").Value & "") = "S")
      '
      If wVarReclamoCerrado Then
        wvarStep = 100
        'wVarFechaCierre = DateToSQL(CStr(wrstPedido.Fields("ODE_FEC_ULT_MAN").Value))
        wVarFechaCierre = wrstPedido.Fields("FECHAULTMANT").Value
      Else
        wvarStep = 110
        wVarFechaCierre = wrstPedido.Fields("AHORA").Value
      End If
      '
      wvarStep = 120
      'wVarFechaDelReclamo = wrstPedido.Fields("OPE_FEC_REG_OPE").Value
      '
      wvarStep = 130
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      '
      Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
      Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
      Call wrstPedido.save(wobjXMLResponse, adPersistXML)
      '
      wvarStep = 140
      wobjXSLResponse.async = False
      Call wobjXSLResponse.loadXML(p_GetXSLPedidos)
      wvarResponseTmp = wobjXMLResponse.transformNode(wobjXSLResponse)
      wobjXMLResponse.loadXML (wvarResponseTmp)
      Set wobjNode = wobjXMLResponse.selectSingleNode("DETALLEPEDIDO")
      wvarTipDoc = wobjNode.selectSingleNode("TIPODOC").Text
      wvarNroDoc = wobjNode.selectSingleNode("NRODOCUMENTO").Text
      'AHORA AGREGO LA REGION CDATA ADONDE SEA NECESARIO
      wvarStep = 150
      wobjNode.selectSingleNode("ESTADO").Text = "<![CDATA[" & wobjNode.selectSingleNode("ESTADO").Text & "]]>"
      wobjNode.selectSingleNode("CAUSA").Text = "<![CDATA[" & wobjNode.selectSingleNode("CAUSA").Text & "]]>"
      wobjNode.selectSingleNode("MOTIVO").Text = "<![CDATA[" & wobjNode.selectSingleNode("MOTIVO").Text & "]]>"
      wobjNode.selectSingleNode("CAUSADEF").Text = "<![CDATA[" & wobjNode.selectSingleNode("CAUSADEF").Text & "]]>"
      wobjNode.selectSingleNode("MOTIVODEF").Text = "<![CDATA[" & wobjNode.selectSingleNode("MOTIVODEF").Text & "]]>"
      wobjNode.selectSingleNode("MODOCONTACTO").Text = "<![CDATA[" & wobjNode.selectSingleNode("MODOCONTACTO").Text & "]]>"
'      'SE AGREGA EL NODO DE ANTIGUEDAD
'      wvarStep = 160
'      Set wobjNewNode = wobjXMLResponse.createElement("ANTIGUEDADRECLAMO")
'      wobjNewNode.Text = DiferenciaEnHabilesEntreDosFechas(ConvertirDateAString14(wVarFechaDelReclamo), ConvertirDateAString14(wVarFechaCierre), mobjCOM_Context)
'      wobjNode.appendChild wobjNewNode
'      Set wobjNewNode = Nothing
      Set wobjNode = Nothing
      pvarResponse = pvarResponse & wobjXMLResponse.xml
      '
      '---------------------------------
      wvarStep = 170
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_PERSONA"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 180
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TipoDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = wvarTipDoc
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 190
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Doc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = wvarNroDoc
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 200
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Empresa"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 210
      wrstPedido.Close
      Set wrstPedido = wobjDBCmd.Execute
      Set wrstPedido.ActiveConnection = Nothing
      '
      wvarStep = 220
      If Not wrstPedido.EOF Then
        Set wobjXMLResponse = Nothing
        Set wobjXMLResponse = CreateObject("MSXML2.DomDocument")
        wrstPedido.save wobjXMLResponse, adPersistXML
        '
        wvarStep = 230
        wobjXSLResponse.async = False
        wvarStep = 240
        Call wobjXSLResponse.loadXML(p_GetXSLPersona())
        wvarStep = 250
        wvarResponseTmp = wobjXMLResponse.transformNode(wobjXSLResponse)
        wvarStep = 260
        wobjXMLResponse.loadXML (wvarResponseTmp)
        wvarStep = 270
        'AHORA AGREGO LA REGION CDATA ADONDE SEA NECESARIO
        Set wobjNode = wobjXMLResponse.selectSingleNode("//DETALLES/DETALLEPERSONA")
        wobjNode.selectSingleNode("RAZONSOCIAL").Text = "<![CDATA[" & wobjNode.selectSingleNode("RAZONSOCIAL").Text & "]]>"
        wobjNode.selectSingleNode("CATEGORIA").Text = "<![CDATA[" & TratarCategorias(Trim(wobjNode.selectSingleNode("CATEGORIA").Text)) & "]]>"
        pvarResponse = pvarResponse & wobjXMLResponse.xml
        '
      Else
        pvarResponse = pvarResponse & "<DETALLEPERSONA>"
        pvarResponse = pvarResponse & " <RAZONSOCIAL></RAZONSOCIAL>" & _
                                      " <CATEGORIA></CATEGORIA>"
        pvarResponse = pvarResponse & "</DETALLEPERSONA>"
      End If
      '---------------------------------
      wvarStep = 280
      Set wobjDBCmd = Nothing
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "sp_age_list_segestadoreclahist"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
'      wvarStep = 290
'      Set wobjDBParm = CreateObject("ADODB.Parameter")
'      wobjDBParm.Name = "@FechaCierre"
'      wobjDBParm.Direction = adParamInput
'      wobjDBParm.Type = adDate
'      wobjDBParm.Value = wVarFechaDelReclamo 'wVarFechaCierre
'      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 300
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NumPedido"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarNroPedido
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 310
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Empresa"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 320
      wrstPedido.Close
      Set wrstPedido = Nothing
      Set wrstPedido = wobjDBCmd.Execute
      Set wrstPedido.ActiveConnection = Nothing
      '
      pvarResponse = pvarResponse & "<ESTADOS>"
      If Not wrstPedido.EOF Then
        ''''''ACA SE ANULO EL PROCESAMIENTO A TRAVES DE XSL PORQUE
        ''''''EL METODO SAVE FALLABA PARA ALGUNOS CASOS POR EJEMPLO
        ''''''EL CLIENTE NSO 149779100 EN PRODUCCION
        wVarFechaDelReclamo = wrstPedido.Fields("fecharegistracion").Value
        pvarResponse = pvarResponse & "<ESTADO RESULTADO=" & Chr(34) & "TRUE" & Chr(34) & " MENSAJE=" & Chr(34) & Chr(34) & " />"
        Do
          wvarStep = 330
          pvarResponse = pvarResponse & "<DETALLEESTADO>"
          pvarResponse = pvarResponse & "<FECHAREGISTRACION>" & wrstPedido.Fields("fecharegistracion").Value & "</FECHAREGISTRACION>"
          'pvarResponse = pvarResponse & "<ANTIGUEDAD>" & "<![CDATA[" & wrstPedido.Fields("ANTIGUEDAD").Value & "]]>" & "</ANTIGUEDAD>"
          pvarResponse = pvarResponse & "<ANTIGUEDAD></ANTIGUEDAD>"
          pvarResponse = pvarResponse & "<ESTADO>" & "<![CDATA[" & wrstPedido.Fields("est_desc").Value & "]]>" & "</ESTADO>"
          pvarResponse = pvarResponse & "<USUARIO>" & wrstPedido.Fields("his_usu_nom").Value & "</USUARIO>"
          pvarResponse = pvarResponse & "<SECTORREGISTRADOR>" & "<![CDATA[" & wrstPedido.Fields("SEC_REG").Value & "]]>" & "</SECTORREGISTRADOR>"
          pvarResponse = pvarResponse & "<SECTORRESPONSABLE>" & "<![CDATA[" & wrstPedido.Fields("sec_des_sec").Value & "]]>" & "</SECTORRESPONSABLE>"
          pvarResponse = pvarResponse & "<OBSERVACIONES>" & "<![CDATA[" & wrstPedido.Fields("his_obs").Value & "]]>" & "</OBSERVACIONES>"
          'pvarResponse = pvarResponse & "<HORAREGISTRACION>" & Right("0" & CStr(wrstPedido.Fields("his_hor_reg").Value), 6) & "</HORAREGISTRACION>"
          pvarResponse = pvarResponse & "</DETALLEESTADO>"
          wvarLastObs = wrstPedido.Fields("his_obs").Value
          wrstPedido.MoveNext
        Loop While Not wrstPedido.EOF
      Else
        wvarStep = 340
        pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se han encontrado estados para el reclamo' />"
      End If
      pvarResponse = pvarResponse & "</ESTADOS>"
      '
    Else
      '
      wvarStep = 350
      pvarResponse = pvarResponse & _
                     "<ESTADO RESULTADO='FALSE' MENSAJE='No se han encontrado datos para el reclamo solicitado' />"
    End If
    '
    pvarResponse = pvarResponse & _
                  "</GENERAL>"
    '
    ' ACA SE AGREGA EL NODO CON LA ULTIMA OBSERVACION
    Set wobjXMLResponse = CreateObject("MSXML2.DomDocument")
    wobjXMLResponse.async = False
    wobjXMLResponse.loadXML (pvarResponse)
    wvarStep = 360
    If wobjXMLResponse.selectSingleNode("//GENERAL/ESTADO").Attributes.getNamedItem("RESULTADO").Text = "TRUE" Then
      Set wobjNewNode = wobjXMLResponse.createElement("ULTIMAOBS")
      wobjNewNode.Text = "<![CDATA[" & wvarLastObs & "]]>"
      Set wobjNode = wobjXMLResponse.selectSingleNode("//GENERAL/DETALLEPEDIDO")
      wobjNode.appendChild wobjNewNode
      
      'SE AGREGA EL NODO DE ANTIGUEDAD DEL RECLAMO Y FECHA DE INGRESO
      If wobjXMLResponse.selectSingleNode("//GENERAL/ESTADOS/ESTADO").Attributes.getNamedItem("RESULTADO").Text = "TRUE" Then
        wvarStep = 370
        Set wobjNewNode = wobjXMLResponse.createElement("ANTIGUEDADRECLAMO")
        wobjNewNode.Text = DiferenciaEnHabilesEntreDosFechas(wVarFechaDelReclamo, wVarFechaCierre, mobjCOM_Context)
        wobjNode.appendChild wobjNewNode
        wvarStep = 380
        Set wobjNewNode = wobjXMLResponse.createElement("FECHAINGRESO")
        wobjNewNode.Text = wVarFechaDelReclamo
        wvarStep = 390
        wobjNode.appendChild wobjNewNode
        'SE CALCULAN LOS TIEMPOS POR SECTOR
        Set wobjNodeList = wobjXMLResponse.selectNodes("//GENERAL/ESTADOS/DETALLEESTADO")
        If wobjNodeList.length > 0 Then
          wvarStep = 400
          i = 0
          Set wobjNode = wobjNodeList.Item(i)
          wobjNode.selectSingleNode("ANTIGUEDAD").Text = "0"
          wvarFechaReg = wobjNode.selectSingleNode("FECHAREGISTRACION").Text
          wvarStep = 410
          i = i + 1
          Do While i < wobjNodeList.length
            wvarStep = 420
            Set wobjNode = wobjNodeList.Item(i)
            wobjNode.selectSingleNode("ANTIGUEDAD").Text = DiferenciaEnHabilesEntreDosFechas(wvarFechaReg, wobjNode.selectSingleNode("FECHAREGISTRACION").Text, mobjCOM_Context)
            wvarFechaReg = wobjNode.selectSingleNode("FECHAREGISTRACION").Text
            i = i + 1
          Loop
        End If
        Set wobjNodeList = Nothing
      End If
      Set wobjNewNode = Nothing
      Set wobjNode = Nothing
      pvarResponse = wobjXMLResponse.xml
    End If
    Set wobjXMLResponse = Nothing

    wvarStep = 430
    Set wobjDBCmd = Nothing
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 440
    If wrstPedido.State <> 0 Then wrstPedido.Close
    '
    wvarStep = 450
    Set wrstPedido = Nothing
    '
    wvarStep = 460
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLHistorialEstados() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data/z:row'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEESTADO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='FECHAREGISTRACION'><xsl:value-of select='@his_fec_reg' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ANTIGUEDAD'><xsl:value-of select='@ANTIGUEDAD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'><xsl:value-of select='@est_desc' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='USUARIO'><xsl:value-of select='@his_usu_nom' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='SECTORREGISTRADOR'><xsl:value-of select='@SEC_REG' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='SECTORRESPONSABLE'><xsl:value-of select='@sec_des_sec' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='OBSERVACIONES'><xsl:value-of select='@his_obs' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLHistorialEstados = wvarStrXSL
End Function
Private Function p_GetXSLPedidos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data/z:row'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPEDIDO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NROPEDIDO'><xsl:value-of select='@REG_NUM_PED' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "   <xsl:element name='FECHAINGRESO'><xsl:value-of select='@OPE_FEC_REG_OPE' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'><xsl:value-of select='@EST_DESC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DIASESTIMADOS'><xsl:value-of select='@ESTIMADO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CAUSA'><xsl:value-of select='@CAU_CON_DES_CAU' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MOTIVO'><xsl:value-of select='@MOT_CON_DES_MOT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CAUSADEF'><xsl:value-of select='@CAU_CON_DES_CAU_DEF' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MOTIVODEF'><xsl:value-of select='@MOT_CON_DES_MOT_DEF' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "   <xsl:choose>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:when test=""@CIERRE[.='S']"">"
    'wvarStrXSL = wvarStrXSL & "       <xsl:element name='FECHACIERRE'><xsl:value-of select='@ODE_FEC_ULT_MAN' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     </xsl:when>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:otherwise>"
    'wvarStrXSL = wvarStrXSL & "       <xsl:element name='FECHACIERRE'><xsl:value-of select='@AHORA' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     </xsl:otherwise>"
    'wvarStrXSL = wvarStrXSL & "   </xsl:choose>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPODOC'><xsl:value-of select='@TIPODOC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NRODOCUMENTO'><xsl:value-of select='@DOCUMENTO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='MODOCONTACTO'><xsl:value-of select='@MODOCONTACTO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLPedidos = wvarStrXSL
End Function


Private Function p_GetXSLPersona() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DETALLES'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:apply-templates select='//xml/rs:data/z:row'/>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPERSONA'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='RAZONSOCIAL'><xsl:value-of select='@RAZONSOCIAL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CATEGORIA'><xsl:value-of select='@CATEGORIA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLPersona = wvarStrXSL
End Function
