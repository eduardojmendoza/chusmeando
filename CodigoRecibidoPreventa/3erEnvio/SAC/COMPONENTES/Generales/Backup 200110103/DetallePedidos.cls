VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "DetallePedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Generales.DetallePedidos"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarParam1          As String
    Dim wvarParam2          As String
    Dim wvarParam3          As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstOperacion       As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarParam1 = wobjXMLrequest.selectSingleNode("//GENERAL/SESION").Text
    wvarParam2 = wobjXMLrequest.selectSingleNode("//GENERAL/FECHA").Text
    wvarParam3 = wobjXMLrequest.selectSingleNode("//GENERAL/TIPO").Text
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_BRDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_detalleoperacionesclientes"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Sesion"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarParam1)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@FechaReg"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adNumeric
    wobjDBParm.Precision = 14
    wobjDBParm.Value = wvarParam2
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoOperacion"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 16
    wobjDBParm.Value = wvarParam3
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wrstOperacion = wobjDBCmd.Execute
    Set wrstOperacion.ActiveConnection = Nothing
    '
    wvarStep = 100
    pvarResponse = "<GENERAL>"
    '
    If Not wrstOperacion.EOF Then
      '
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      pvarResponse = pvarResponse & "<DETALLES>"
      Do
        wvarStep = 110
        pvarResponse = pvarResponse & "<DETALLEOPERACION " & _
        "NUMREFERENCIA='" & wrstOperacion.Fields("NUMREFERENCIA").Value & "' " & _
        "FECHA='" & DateToString(wrstOperacion.Fields("FECHA").Value) & "' " & _
        "OPERADORA='" & wrstOperacion.Fields("OPERADORA").Value & "' " & _
        "TIPOPEDIDO='" & wrstOperacion.Fields("TIPOPEDIDO").Value & "' " & _
        "TIPOCUENTA='" & wrstOperacion.Fields("TIPOCUENTA").Value & "' " & _
        "CUENTA='" & wrstOperacion.Fields("CUENTA").Value & "' " & _
        "SUCURSALRETIRO='" & wrstOperacion.Fields("SUCURSALRETIRO").Value & "' " & _
        "TIPOLIBRETA='" & wrstOperacion.Fields("TIPOLIBRETA").Value & "' " & _
        "FECHADESDE='" & DateToString(wrstOperacion.Fields("FECHADESDEEXT").Value) & "' " & _
        "FECHAHASTA='" & DateToString(wrstOperacion.Fields("FECHAHASTAEXT").Value) & "' " & _
        "DETALLE='" & wrstOperacion.Fields("DETALLE").Value & "' />"
        wrstOperacion.MoveNext
        '
      Loop While Not wrstOperacion.EOF
      pvarResponse = pvarResponse & "</DETALLES>"
    Else
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha encontrado el detalle de la operación' />"
    End If
    '
    wvarStep = 120
    pvarResponse = pvarResponse & _
                  "</GENERAL>"
    '
    wvarStep = 130
    Set wobjDBCmd = Nothing
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 140
    If wrstOperacion.State <> 0 Then wrstOperacion.Close
    '
    wvarStep = 150
    Set wrstOperacion = Nothing
    '
    wvarStep = 160
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


