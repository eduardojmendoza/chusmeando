Attribute VB_Name = "modA_LBA"
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : modA_LBA
'
' Module ID : modA_LBA
'
' File Name : modA_LBA.bas
'
' Creation Date:
'
' Programmer :
'
' Abstract : Declaración de Constantes del sistema
'
' Entry :
'
' Exit :
'
' Program :
'
' Functions :
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' The followings should be specified (in chronological
' sequence) for each amendment made to the module:
' Version Version number of the module
' Date Date of amendment (dd/mm/ccyy)
' PPCR Associated PPCR number
' Programmer Name of programmer who made the
' Amendment
' Reason Brief description of the amendment
' *****************************************************************

Option Explicit
'
'
Public Const mCte_LBA_DB = "SAC_LBA.UDL"
Public Const mCte_NYL_DB = "ony_NYLOficinaVirtual.udl"
Public Const mCte_RVP_DB = "INFORMACIONGERENCIAL.udl"
Public Const mCte_COMISIONESDB = "COMISIONES.UDL"
'
Public Const mCte_TimeOutError = -2147217871
'
'--- Manejado por el 'CmdProcessor'
Public Const xERR_UNEXPECTED = 1
Public Const xERR_TIMEOUT_RETURN = 501

Public Const mCte_CODIGO_CUIT = 4
Public Const mCte_CODIGO_CUIL = 5
'CODIGOS DE EMPRESAS
Public Const wconstCODIGOBANCO = "2"
Public Const wconstCODIGODOCTHOS = "4"
Public Const wconstCODIGONYL = "8"
Public Const wconstCODIGOLBA = "3"

