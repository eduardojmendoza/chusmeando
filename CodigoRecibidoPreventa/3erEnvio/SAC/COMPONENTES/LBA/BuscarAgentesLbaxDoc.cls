VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "BuscarAgentesLbaxDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : sacA_LBA.BuscarAgentesLbaxDoc
'
' Module ID : BuscarAgentesLbaxDoc
'
' File Name : BuscarAgentesLbaxDoc.cls
'
' Creation Date: 27/06/2003
'
' Programmer : Fernando J. Martelli
'
' Abstract : Busca Agentes de la Compa�ia LBA por su Codigo
'
' Entry :
' - parameters :
'                 DOCUMENTO
'                 CLASE
' Exit :
'               Si el agente es unico
'                 CLIENSECS
'                 DOMICDOM
'                 DOMICDNU
'                 DOMICESC
'                 DOMICPIS
'                 DOMICPTA
'                 CP_PART
'                 LOCALIDAD_PART
'                 TELEFONO_PART
'                 CLASE
'                 INGRESODIA
'                 INGRESOMES
'                 INGRESOANIO
'                 OFICINA
'                 TIPO_DOC_DES
'                 NRO_DOC
'                 NOMBRE
'                 APELLIDO1
'                 APELLIDO2
'               Si hay mas de un Agente con el Codigo ingresado
'                 CODIGO
'                 CLASE
'                 NOMBRE
'
' Program : A list of messages used in this module
'
' Functions :
'                 p_GetXSLCliente()
'                 p_GetXSLAgentes
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' The followings should be specified (in chronological
' sequence) for each amendment made to the module:
' Version Version number of the module
' Date Date of amendment (dd/mm/ccyy)
' PPCR Associated PPCR number
' Programmer Name of programmer who made the
' Amendment
' Reason Brief description of the amendment
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_LBA.BuscarAgentesLbaxDoc"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest    As MSXML2.DOMDocument
    Dim wobjXMLDocument   As MSXML2.DOMDocument
    Dim wobjXMLDocument2  As MSXML2.DOMDocument
    Dim wobjXSLDocument   As MSXML2.DOMDocument
    Dim wobjNodelist      As MSXML2.IXMLDOMNodeList
    Dim wobjNode          As MSXML2.IXMLDOMNode
    Dim wobjNodeNew       As MSXML2.IXMLDOMNode
    Dim wobjNodeClienSec  As MSXML2.IXMLDOMNode
    Dim wobjNodeClienSec1 As MSXML2.IXMLDOMNode
    '
    'Dim wvarEmpresa         As String
    Dim wvarTipoDoc         As String
    Dim wvarDocumento       As String
    Dim wvarClienSec        As String
    Dim wvarClase           As String
    Dim wvarIngresoDia      As String
    Dim wvarIngresoMes      As String
    Dim wvarIngresoAnio     As String
    Dim wvarOficina         As String
    Dim wvarNombre          As String
    Dim wvarApellido1       As String
    Dim wvarApellido2       As String
    Dim wvarResultado       As String
    '
    Dim wvarQuery       As String
    Dim wvarCount       As Integer
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstCliente     As ADODB.Recordset
    '
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    'wvarEmpresa = wobjXMLrequest.selectSingleNode("//CLIENTE/EMPRESA").Text
    'wvarTipoDoc = wobjXMLrequest.selectSingleNode("//CLIENTE/TIPODOC").Text
    wvarDocumento = wobjXMLrequest.selectSingleNode("//CLIENTE/DOCUMENTO").Text
    wvarClase = wobjXMLrequest.selectSingleNode("//CLIENTE/CLASE").Text
    '
    Set wobjXMLrequest = Nothing
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_LBA_DB)
    '
    wvarStep = 50
    wvarQuery = "SP_SACLBA_BUSCARAGENTESLBAXDOC "
    '
    wvarStep = 70
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = wvarQuery
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NroDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 8
    wobjDBParm.Value = wvarDocumento
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Clase"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 2
    wobjDBParm.Value = Trim(wvarClase)
    wobjDBCmd.Parameters.Append wobjDBParm
    Set wobjDBParm = Nothing
    '
    wvarStep = 10
    Set wrstCliente = wobjDBCmd.Execute
    Set wrstCliente.ActiveConnection = Nothing
    If wrstCliente.RecordCount = 1 Then  'Si encuentra un solo agente
      wvarClienSec = wrstCliente.Fields("CLIENSEC").Value
      wvarTipoDoc = Trim(wrstCliente.Fields("DOCUMDAB").Value)
      wvarDocumento = Trim(wrstCliente.Fields("DOCUMDAT").Value)
      wvarClase = Trim(wrstCliente.Fields("AGENTCLA").Value)
      wvarIngresoDia = Trim(wrstCliente.Fields("EFECTDIA").Value)
      wvarIngresoMes = Trim(wrstCliente.Fields("EFECTMES").Value)
      wvarIngresoAnio = Trim(wrstCliente.Fields("EFECTANN").Value)
      wvarOficina = Trim(wrstCliente.Fields("OFICINA").Value)
      wvarNombre = Trim(wrstCliente.Fields("CLIENNOM").Value)
      wvarApellido1 = Trim(wrstCliente.Fields("CLIENAP1").Value)
      wvarApellido2 = Trim(wrstCliente.Fields("CLIENAP2").Value)
      Set wobjXMLDocument2 = CreateObject("MSXML2.DomDocument")
      Set wobjNodeClienSec = wobjXMLDocument2.createNode(NODE_ELEMENT, "CLIENSECS", "")
      Do
        Set wobjNodeClienSec1 = wobjXMLDocument2.createNode(NODE_ELEMENT, "CLIENSEC", "")
        wobjNodeClienSec1.Text = wrstCliente.Fields("CLIENSEC").Value
        wvarStep = 90
        wobjNodeClienSec.appendChild wobjNodeClienSec1
        Set wobjNodeClienSec1 = Nothing
        wrstCliente.MoveNext
      Loop While Not wrstCliente.EOF
      wobjXMLDocument2.appendChild wobjNodeClienSec
      Set wobjNodeClienSec = Nothing
      wvarStep = 100
      wvarQuery = "SP_SACLBA_TRAERDATOSCLIENTELBA " & wvarClienSec
      wvarStep = 110
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = wvarQuery
      wobjDBCmd.CommandType = adCmdText
      Set wrstCliente = wobjDBCmd.Execute
      
      Set wrstCliente.ActiveConnection = Nothing
      wvarStep = 120
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.async = False
      wvarStep = 130
      wrstCliente.Save wobjXMLDocument, adPersistXML
      Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
      wvarStep = 140
      wobjXSLDocument.loadXML (p_GetXSLUnicoAgente)
      wobjXMLDocument.loadXML (wobjXMLDocument.transformNode(wobjXSLDocument))
      Set wobjNode = wobjXMLDocument.selectSingleNode("//FICHACLIENTE/DATOSCLIENTE")
      wvarStep = 150
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "CLASE", "")
      wobjNodeNew.Text = wvarClase
      wobjNode.appendChild wobjNodeNew
      wvarStep = 160
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "INGRESODIA", "")
      wobjNodeNew.Text = Right("0" & wvarIngresoDia, 2)
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 170
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "INGRESOMES", "")
      wobjNodeNew.Text = Right("0" & wvarIngresoMes, 2)
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 180
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "INGRESOANIO", "")
      wobjNodeNew.Text = wvarIngresoAnio
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 190
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "OFICINA", "")
      wobjNodeNew.Text = wvarOficina
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 200
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "TIPO_DOC_DES", "")
      wobjNodeNew.Text = wvarTipoDoc
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 210
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "NRO_DOC", "")
      wobjNodeNew.Text = wvarDocumento
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 220
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "NOMBRE", "")
      wobjNodeNew.Text = wvarNombre
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 230
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "APELLIDO1", "")
      wobjNodeNew.Text = wvarApellido1
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      wvarStep = 240
      Set wobjNodeNew = wobjXMLDocument.createNode(1, "APELLIDO2", "")
      wobjNodeNew.Text = wvarApellido2
      wobjNode.appendChild wobjNodeNew
      Set wobjNodeNew = Nothing
      
      wvarStep = 250
      Set wobjNode = Nothing
      Set wobjNode = wobjXMLDocument.selectSingleNode("//FICHACLIENTE")
      wobjNode.appendChild wobjXMLDocument2.selectSingleNode("//CLIENSECS")
    ElseIf wrstCliente.RecordCount > 1 Then 'Si encuentra mas de uno
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.async = False
      wvarStep = 250
      wrstCliente.Save wobjXMLDocument, adPersistXML
      Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
      wobjXSLDocument.async = False
      wobjXSLDocument.loadXML (p_GetXSLAgentes)
      wvarStep = 250
      wobjXMLDocument.loadXML (wobjXMLDocument.transformNode(wobjXSLDocument))
      
      Set wobjXSLDocument = Nothing
    Else 'No se encontro ningun agente
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.loadXML ("<FICHACLIENTE><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido encontrar el detalle' /></FICHACLIENTE>")
    End If
    
    pvarResponse = wobjXMLDocument.xml
    Set wobjXMLDocument = Nothing
    '
    wvarStep = 260
    wrstCliente.Close
    Set wrstCliente = Nothing
    '
    wvarStep = 270
    Set wobjDBCmd = Nothing
    
    wvarStep = 280
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 290
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLUnicoAgente() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLUnicoAgente
  ' Abstract : Genera el codigo XSL de respuesta para el caso de que
  ' haya un solo nodo de Agente
  ' Synopsis : string = p_GetXSLUnicoAgente()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='FICHACLIENTE'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "   </xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='DATOSCLIENTE'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICDOM'><xsl:value-of select='@DOMICDOM' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICDNU'><xsl:value-of select='@DOMICDNU' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICESC'><xsl:value-of select='@DOMICESC' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICPIS'><xsl:value-of select='@DOMICPIS' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICPTA'><xsl:value-of select='@DOMICPTA' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CP_PART'><xsl:value-of select='@DOMICCPO' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='LOCALIDAD_PART'><xsl:value-of select='@DOMICPOB' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='TELEFONO_PART'><xsl:value-of select='@DOMICTLF' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLUnicoAgente = wvarStrXSL
End Function


Private Function p_GetXSLAgentes() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLAgentes
  ' Abstract : Genera el codigo XSL de respuesta para el caso de que
  ' haya varios nodos de Agentes
  ' Synopsis : string = p_GetXSLAgentes()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='FICHACLIENTE'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "   </xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='DATOSCLIENTE'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CODIGO'><xsl:value-of select='@AGENTCOD' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CLASE'><xsl:value-of select='@AGENTCLA' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NOMBRE'><xsl:value-of select='@CLIENAP1' /> <xsl:value-of select='@CLIENNOM' /> <xsl:value-of select='@CLIENAP2' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLAgentes = wvarStrXSL
End Function

