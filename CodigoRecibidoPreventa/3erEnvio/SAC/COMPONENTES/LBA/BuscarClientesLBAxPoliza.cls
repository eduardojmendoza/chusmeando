VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "BuscarClientesLBAxPoliza"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_LBA.BuscarClientesLBAxPoliza"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjXMLDocument As MSXML2.DOMDocument
    Dim wobjXSLDocument As MSXML2.DOMDocument
    Dim wobjNode        As MSXML2.IXMLDOMNode
    Dim wobjNodeNew     As MSXML2.IXMLDOMNode
    Dim wobjNodelist    As MSXML2.IXMLDOMNodeList
    '
    Dim wvarCompania    As String
    Dim wvarProducto    As String
    Dim wvarPoliza      As String
    Dim wvarPolizAnn    As String
    Dim wvarPolizSec    As String
    Dim wvarCertiPol    As String
    Dim wvarCertiAnn    As String
    Dim wvarCertiSec    As String
    'Dim wvarPosPrimerGuion As Integer
    'Dim wvarPosEspacio     As Integer
    'Dim wvarPosSegundoGuion As Integer
    'Dim wvarPosTercerGuion  As Integer
    Dim wvarPosPrimerGuion  As Integer
    Dim wvarPosSegundoGuion As Integer
    Dim wvarPosTercerGuion  As Integer
    Dim wvarPosCuartoGuion  As Integer
    '
    Dim wvarQuery       As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstCliente     As ADODB.Recordset
    '
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarCompania = wobjXMLrequest.selectSingleNode("//POLIZA/COMPANIA").Text
    wvarProducto = wobjXMLrequest.selectSingleNode("//POLIZA/PRODUCTO").Text
    wvarPoliza = wobjXMLrequest.selectSingleNode("//POLIZA/NUMERO").Text
    '
    Set wobjXMLrequest = Nothing
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    
    
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_LBA_DB)
    '
    wvarStep = 50
    'wvarPosPrimerGuion = InStr(1, wvarPoliza, "-")
    'wvarPosEspacio = InStr(1, wvarPoliza, Chr(32))
    'If Not IsNull(wvarPosPrimerGuion) And Not IsNull(wvarPosEspacio) Then
    '  wvarPolizAnn = Mid(wvarPoliza, 1, wvarPosPrimerGuion - 1)
    '  wvarPolizSec = Mid(wvarPoliza, wvarPosPrimerGuion + 1, (wvarPosEspacio - 1) - wvarPosPrimerGuion)
    '  wvarPosSegundoGuion = InStr(wvarPosEspacio, wvarPoliza, "-")
    '  wvarStep = 60
    '  If Not IsNull(wvarPosSegundoGuion) Then
    '    wvarPosTercerGuion = InStr(wvarPosSegundoGuion + 1, wvarPoliza, "-")
    '    wvarStep = 70
    '    If Not IsNull(wvarPosTercerGuion) Then
    '      wvarCertiPol = Mid(wvarPoliza, wvarPosEspacio + 1, wvarPosSegundoGuion - (wvarPosEspacio + 1))
    '      wvarCertiAnn = Mid(wvarPoliza, wvarPosSegundoGuion + 1, wvarPosTercerGuion - (wvarPosSegundoGuion + 1))
    '      wvarCertiSec = Mid(wvarPoliza, wvarPosTercerGuion + 1)
    '    End If
    '  End If
    'End If
    wvarPosPrimerGuion = InStr(1, wvarPoliza, "-")
    wvarPosSegundoGuion = InStr(wvarPosPrimerGuion + 1, wvarPoliza, "-")
    wvarPosTercerGuion = InStr(wvarPosSegundoGuion + 1, wvarPoliza, "-")
    wvarPosCuartoGuion = InStr(wvarPosTercerGuion + 1, wvarPoliza, "-")
    If Not IsNull(wvarPosPrimerGuion) And Not IsNull(wvarPosSegundoGuion) And Not IsNull(wvarPosTercerGuion) And Not IsNull(wvarPosCuartoGuion) Then
'      wvarPolizAnn = Mid(wvarPoliza, 1, wvarPosPrimerGuion - 1)
'      wvarPolizSec = Mid(wvarPoliza, wvarPosPrimerGuion + 1, (wvarPosSegundoGuion - 1) - wvarPosPrimerGuion)
'      wvarCertiPol = Mid(wvarPoliza, wvarPosSegundoGuion + 1, wvarPosTercerGuion - (wvarPosSegundoGuion + 1))
'      wvarCertiAnn = Mid(wvarPoliza, wvarPosTercerGuion + 1, wvarPosCuartoGuion - (wvarPosTercerGuion + 1))
'      wvarCertiSec = Mid(wvarPoliza, wvarPosCuartoGuion + 1)
      wvarPolizAnn = Mid(wvarPoliza, 1, wvarPosPrimerGuion - 1)
      wvarPolizSec = Mid(wvarPoliza, wvarPosPrimerGuion + 1, (wvarPosSegundoGuion) - (wvarPosPrimerGuion + 1))
      wvarCertiPol = Mid(wvarPoliza, wvarPosSegundoGuion + 1, (wvarPosTercerGuion) - (wvarPosSegundoGuion + 1))
      wvarCertiAnn = Mid(wvarPoliza, wvarPosTercerGuion + 1, (wvarPosCuartoGuion) - (wvarPosTercerGuion + 1))
      wvarCertiSec = Mid(wvarPoliza, wvarPosCuartoGuion + 1)

    Else
      pvarResponse = "<LISTADOCUMENTOS><ESTADO RESULTADO='FALSE' MENSAJE='El n�mero de p�liza es inv�lido' /></LISTADOCUMENTOS>"
      Err.Raise vbObjectError + 505, mcteClassName, "N�mero de p�liza inv�lido"
    End If
    wvarStep = 80
    'wvarQuery = "SELECT DISTINCT Documtip, Documdat, P.CLIENSEC, CLIENAP1, CLIENAP2, CLIENNOM " & _
                "FROM AISPFVIDA.SEFMBENE P INNER JOIN AISPF.SEFMPERS Q " & _
                "ON P.CLIENSEC=Q.CLIENSEC " & _
                "INNER JOIN AISPFVIDA.SEFMPOLI R " & _
                "ON P.CIAASCOD=R.CIAASCOD AND P.RAMOPCOD=R.RAMOPCOD AND " & _
                "P.POLIZANN=R.POLIZANN AND P.POLIZSEC=R.POLIZSEC AND " & _
                "P.CERTIPOL=R.CERTIPOL AND P.CERTIANN=R.CERTIANN AND " & _
                "P.CERTISEC = R.CERTISEC And P.SUPLENUM = R.SUPLENUM " & _
                "WHERE   P.Ciaascod = '" & wvarCompania & "' AND " & _
                "P.Ramopcod =  '" & wvarProducto & "' AND " & _
                "P.Polizann = " & wvarPolizAnn & " AND " & _
                "P.Polizsec = " & wvarPolizSec & " AND " & _
                "P.CertiPol = " & wvarCertiPol & " AND " & _
                "P.CertiAnn = " & wvarCertiAnn & " AND " & _
                "P.CertiSec = " & wvarCertiSec & " AND " & _
                "SUBSTR( CLIENNUM , 25 , 1 ) IN ( 'X', 'U') "
    'wvarQuery = wvarQuery & "Union " & _
                "SELECT DISTINCT Documtip, Documdat, P.CLIENSEC, CLIENAP1, CLIENAP2, CLIENNOM " & _
                "FROM AISPFVIDA.SEFMPECO P INNER JOIN AISPF.SEFMPERS Q " & _
                "ON P.CLIENSEC=Q.CLIENSEC " & _
                "INNER JOIN AISPFVIDA.SEFMPOLI R " & _
                "ON P.CIAASCOD=R.CIAASCOD AND P.RAMOPCOD=R.RAMOPCOD AND " & _
                "P.POLIZANN=R.POLIZANN AND P.POLIZSEC=R.POLIZSEC AND " & _
                "P.CERTIPOL=R.CERTIPOL AND P.CERTIANN=R.CERTIANN AND " & _
                "P.CERTISEC = R.CERTISEC And P.SUPLENUM = R.SUPLENUM " & _
                "WHERE   P.Ciaascod = '" & wvarCompania & "' AND " & _
                "P.Ramopcod =  '" & wvarProducto & "' AND " & _
                "P.Polizann = " & wvarPolizAnn & " AND " & _
                "P.Polizsec = " & wvarPolizSec & "   AND " & _
                "P.Asegutip in ('', 'A', 'C') AND " & _
                "P.CertiPol = " & wvarCertiPol & " AND " & _
                "P.CertiAnn = " & wvarCertiAnn & " AND " & _
                "P.CertiSec = " & wvarCertiSec & " AND " & _
                "SUBSTR( CLIENNUM , 25 , 1 ) IN ( 'X', 'U') "
    'wvarQuery = wvarQuery & "UNION " & _
                "SELECT DISTINCT Documtip, Documdat, P.CLIENSEC, CLIENAP1, CLIENAP2, CLIENNOM " & _
                "FROM    AISPFRETI.SEFMBENE P INNER JOIN AISPF.SEFMPERS Q " & _
                "ON P.CLIENSEC=Q.CLIENSEC " & _
                "WHERE   P.Ciaascod = '" & wvarCompania & "' AND " & _
                "P.Ramopcod =  '" & wvarProducto & "' AND " & _
                "P.Polizann = " & wvarPolizAnn & " AND " & _
                "P.Polizsec = " & wvarPolizSec & " AND " & _
                "P.CertiPol = " & wvarCertiPol & " AND " & _
                "P.CertiAnn = " & wvarCertiAnn & " AND " & _
                "P.CertiSec = " & wvarCertiSec
    'wvarQuery = wvarQuery & " Union " & _
                "SELECT DISTINCT Documtip, Documdat, P.CLIENSEC, CLIENAP1, CLIENAP2, CLIENNOM " & _
                "FROM    AISPFRETI.SEFMPECO P INNER JOIN AISPF.SEFMPERS Q " & _
                "ON P.CLIENSEC=Q.CLIENSEC " & _
                "WHERE   P.Ciaascod = '" & wvarCompania & "' AND " & _
                "P.Ramopcod =  '" & wvarProducto & "' AND " & _
                "P.Polizann = " & wvarPolizAnn & " AND " & _
                "P.Polizsec = " & wvarPolizSec & " AND " & _
                "P.Asegutip in ('', 'A', 'C') AND " & _
                "P.CertiPol = " & wvarCertiPol & " AND " & _
                "P.CertiAnn = " & wvarCertiAnn & " AND " & _
                "P.CertiSec = " & wvarCertiSec
    wvarQuery = "SP_SACLBA_BUSCARCLIENTESLBAXPOLIZA '" & wvarCompania & "'" & _
                ", '" & wvarProducto & "'" & _
                ", " & wvarPolizAnn & _
                ", " & wvarPolizSec & _
                ", " & wvarCertiPol & _
                ", " & wvarCertiAnn & _
                ", " & wvarCertiSec
    wvarStep = 90
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = wvarQuery
    wobjDBCmd.CommandType = adCmdText
    Set wrstCliente = wobjDBCmd.Execute
    Set wrstCliente.ActiveConnection = Nothing
    wvarStep = 100
    If Not wrstCliente.EOF Then
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.async = False
      wvarStep = 110
      wrstCliente.Save wobjXMLDocument, adPersistXML
      Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
      wvarStep = 120
      wobjXSLDocument.loadXML (p_GetXSLClienteMayuscula)
      wobjXMLDocument.loadXML (wobjXMLDocument.transformNode(wobjXSLDocument))
      Set wobjNodelist = wobjXMLDocument.selectNodes("//LISTADOCUMENTOS/DATOSCLIENTE")
      For Each wobjNode In wobjNodelist
        '
        wvarStep = 130
        Set wobjNodeNew = wobjXMLDocument.createNode(1, "TIPO_DOC_DES", "")
        wobjNodeNew.Text = DescTipoDocNYL(wconstCODIGOLBA, wobjNode.selectSingleNode("TIPO_DOC").Text, mobjCOM_Context)
        wobjNode.appendChild wobjNodeNew
        Set wobjNodeNew = Nothing
      Next
      pvarResponse = wobjXMLDocument.xml
    Else
      pvarResponse = pvarResponse & "<LISTADOCUMENTOS><ESTADO RESULTADO='FALSE' MENSAJE='' /></LISTADOCUMENTOS>"
    End If
    '
    wvarStep = 140
    wrstCliente.Close
    Set wrstCliente = Nothing
    '
    wvarStep = 150
    Set wobjDBCmd = Nothing
    
    wvarStep = 160
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
      
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 170
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLClienteMinuscula() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='LISTADOCUMENTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DATOSCLIENTE'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPO_DOC'><xsl:value-of select='@Documtip' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NRO_DOC'><xsl:value-of select='@Documdat' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='RAZON_SOCIAL'><xsl:value-of select='@Cliennom' /> <xsl:value-of select='@Clienap1' /> <xsl:value-of select='@Clienap2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLClienteMinuscula = wvarStrXSL
End Function
Private Function p_GetXSLClienteMayuscula() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='LISTADOCUMENTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DATOSCLIENTE'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPO_DOC'><xsl:value-of select='@DOCUMTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='NRO_DOC'><xsl:value-of select='@DOCUMDAT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='RAZON_SOCIAL'><xsl:value-of select='@CLIENNOM' /> <xsl:value-of select='@CLIENAP1' /> <xsl:value-of select='@CLIENAP2' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLClienteMayuscula = wvarStrXSL
End Function

