VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "BuscarClientesLbaxDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : sacA_LBA.BuscarClientesLBAxDoc
'
' Module ID : BuscarClientesLBAxDoc
'
' File Name : BuscarClientesLBAxDoc.cls
'
' Creation Date: 01/04/2003
'
' Programmer : Fernando J. Martelli
'
' Abstract : Busca Clientes de la Compa�ia LBA por su Documento
'
' Entry :
' - parameters :
'                 EMPRESA
'                 TIPODOC
'                 DOCUMENTO
' Exit :
'                 CLIENSECS
'                 TIPO_DOC_DES
'                 TIPO_DOC_COD
'                 NRO_DOC
'                 NOMBRE
'                 APELLIDO1
'                 APELLIDO2
'                 NACIONALIDAD
'                 NACIMIENTOANIO
'                 NACIMIENTOMES
'                 NACIMIENTODIA
'                 DOMICDOM
'                 DOMICDNU
'                 DOMICESC
'                 DOMICPIS
'                 DOMICPTA
'                 CP_PART
'                 LOCALIDAD_PART
'                 TELEFONO_PART
'                 SUCURSAL_RADICACION
'                 CATEGORIA
'                 EMPLEADO
'                 EDAD
'
' Program : A list of messages used in this module
'
' Functions :
'                 p_GetXSLCliente()
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' Date of amendment : 16/07/2003
' PPCR Associated   : 20031548-1
' Programmer Name   : Fernando Martelli
' Amendment
'   Ahora se devuelve el campo de codigo postal de ocho posiciones
'   alfanumericas.
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_LBA.BuscarClientesLBAxDoc"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest    As MSXML2.DOMDocument
    Dim wobjXMLDocument   As MSXML2.DOMDocument
    Dim wobjXMLDocument2  As MSXML2.DOMDocument
    Dim wobjXSLDocument   As MSXML2.DOMDocument
    Dim wobjNodelist      As MSXML2.IXMLDOMNodeList
    Dim wobjNode          As MSXML2.IXMLDOMNode
    Dim wobjNodeNew       As MSXML2.IXMLDOMNode
    Dim wobjNodeClienSec  As MSXML2.IXMLDOMNode
    Dim wobjNodeClienSec1 As MSXML2.IXMLDOMNode
    '
    Dim wvarEmpresa         As String
    Dim wvarTipoDoc         As String
    Dim wvarDocumento       As String
    Dim wvarClienSec        As String
    Dim wvarFechaNacimiento As Date
    Dim wvarAnioNacimiento  As String
    Dim wvarMesNacimiento   As String
    Dim wvarDiaNacimiento   As String
    Dim wvarEdad            As Integer
    Dim wvarResultado       As String
    '
    Dim wvarQuery       As String
    Dim wvarCount       As Integer
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstCliente     As ADODB.Recordset
    '
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//CLIENTE/EMPRESA").Text
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//CLIENTE/TIPODOC").Text
    wvarDocumento = wobjXMLrequest.selectSingleNode("//CLIENTE/DOCUMENTO").Text
    '
    Set wobjXMLrequest = Nothing
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_LBA_DB)
    '
    wvarStep = 50
    If (wvarTipoDoc <> CInt(mCte_CODIGO_CUIT)) And (wvarTipoDoc <> CInt(mCte_CODIGO_CUIL)) Then
      wvarQuery = "SP_SACLBA_BUSCARCLIENTESLBAXDOC " & wvarTipoDoc & ",'" & wvarDocumento & "'"
    Else
      wvarQuery = "SP_SACLBA_BUSCARCLIENTESLBAXCUITCUIL " & wvarTipoDoc & ",'" & wvarDocumento & "'"
    End If
    '
    
    wvarStep = 70
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = wvarQuery
    wobjDBCmd.CommandType = adCmdText
    Set wrstCliente = wobjDBCmd.Execute
    Set wrstCliente.ActiveConnection = Nothing
    wvarStep = 80
    Set wobjDBCmd = Nothing
    '
    wvarStep = 90
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    
    wvarStep = 80
    If Not wrstCliente.EOF Then 'Si existe
      wvarClienSec = Trim(noNull(wrstCliente.Fields("CLIENSEC").Value))
      If wvarClienSec <> "" Then
        Set wobjXMLDocument2 = CreateObject("MSXML2.DomDocument")
        Set wobjNodeClienSec = wobjXMLDocument2.createNode(NODE_ELEMENT, "CLIENSECS", "")
        Do
          Set wobjNodeClienSec1 = wobjXMLDocument2.createNode(NODE_ELEMENT, "CLIENSEC", "")
          wobjNodeClienSec1.Text = wrstCliente.Fields("CLIENSEC").Value
          wvarStep = 90
          wobjNodeClienSec.appendChild wobjNodeClienSec1
          Set wobjNodeClienSec1 = Nothing
          wrstCliente.MoveNext
        Loop While Not wrstCliente.EOF
        wobjXMLDocument2.appendChild wobjNodeClienSec
        Set wobjNodeClienSec = Nothing
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_LBA_DB)
        
        wvarStep = 100
        wvarQuery = "SP_SACLBA_TRAERDATOSCLIENTELBA " & wvarClienSec
        wvarStep = 110
        Set wobjDBCmd = CreateObject("ADODB.Command")
        Set wobjDBCmd.ActiveConnection = wobjDBCnn
        wobjDBCmd.CommandText = wvarQuery
        wobjDBCmd.CommandType = adCmdText
        Set wrstCliente = wobjDBCmd.Execute
        
        Set wrstCliente.ActiveConnection = Nothing
        wvarStep = 120
        Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
        wobjXMLDocument.async = False
        wvarStep = 130
        wrstCliente.Save wobjXMLDocument, adPersistXML
        Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
        wvarStep = 140
        wobjXSLDocument.loadXML (p_GetXSLCliente)
        wobjXMLDocument.loadXML (wobjXMLDocument.transformNode(wobjXSLDocument))
        Set wobjNode = wobjXMLDocument.selectSingleNode("//FICHACLIENTE/DATOSCLIENTE")
        wvarFechaNacimiento = DateSerial(wobjNode.selectSingleNode("NACIMIENTOANIO").Text, wobjNode.selectSingleNode("NACIMIENTOMES").Text, wobjNode.selectSingleNode("NACIMIENTODIA").Text)
        wvarEdad = Abs(DateDiff("yyyy", CDate(Year(Now) & "/" & Month(Now) & "/" & Day(Now)), wvarFechaNacimiento))
        wvarStep = 150
        If Month(Now) < Month(wvarFechaNacimiento) Then
          wvarEdad = wvarEdad - 1
        ElseIf (Month(wvarFechaNacimiento) = Month(Now)) And (Day(wvarFechaNacimiento) > Day(Now)) Then
          wvarEdad = wvarEdad - 1
        End If
        wvarStep = 160
        Set wobjNodeNew = wobjXMLDocument.createNode(1, "EDAD", "")
        wobjNodeNew.Text = wvarEdad
        wobjNode.appendChild wobjNodeNew
        wvarStep = 170
        Set wobjNode = Nothing
        Set wobjNode = wobjXMLDocument.selectSingleNode("//FICHACLIENTE")
        wobjNode.appendChild wobjXMLDocument2.selectSingleNode("//CLIENSECS")
        Set wobjNodeNew = Nothing
        Set wobjNode = Nothing
        Set wobjXMLDocument2 = Nothing
      Else
        Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
        wobjXMLDocument.loadXML ("<FICHACLIENTE><ESTADO RESULTADO='FALSE' MENSAJE='No se han podido encontrar los detalles del cliente' /></FICHACLIENTE>")
      End If
    Else
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.loadXML ("<FICHACLIENTE><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido encontrar el detalle' /></FICHACLIENTE>")
    End If
    
    pvarResponse = wobjXMLDocument.xml
    '
    wvarStep = 180
    wrstCliente.Close
    Set wrstCliente = Nothing
    '
    wvarStep = 190
    Set wobjDBCmd = Nothing
    
    wvarStep = 200
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 210
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLCliente() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLCliente
  ' Abstract : Genera el codigo XSL de respuesta
  ' Synopsis : string = p_GetXSLCliente()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='FICHACLIENTE'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "   </xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='DATOSCLIENTE'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPO_DOC_DES'><xsl:value-of select='@DOCUMDAB' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='TIPO_DOC_COD'><xsl:value-of select='@DOCUMTIP' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NRO_DOC'><xsl:value-of select='@DOCUMDAT' /></xsl:element>"
  
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NOMBRE'><xsl:value-of select='@CLIENNOM' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='APELLIDO1'><xsl:value-of select='@CLIENAP1' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='APELLIDO2'><xsl:value-of select='@CLIENAP2' /></xsl:element>"
  
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NACIONALIDAD'><xsl:value-of select='@PAISSDES' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NACIMIENTOANIO'><xsl:value-of select='@NACIMANN' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NACIMIENTOMES'><xsl:value-of select='@NACIMMES' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NACIMIENTODIA'><xsl:value-of select='@NACIMDIA' /></xsl:element>"
  
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICDOM'><xsl:value-of select='@DOMICDOM' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICDNU'><xsl:value-of select='@DOMICDNU' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICESC'><xsl:value-of select='@DOMICESC' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICPIS'><xsl:value-of select='@DOMICPIS' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DOMICPTA'><xsl:value-of select='@DOMICPTA' /></xsl:element>"
  
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CP_PART'><xsl:value-of select='@CPACODPO' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='LOCALIDAD_PART'><xsl:value-of select='@DOMICPOB' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='TELEFONO_PART'><xsl:value-of select='@DOMICTLF' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='SUCURSAL_RADICACION'></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CATEGORIA'></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EMPLEADO'></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CLIENSEC'><xsl:value-of select='@CLIENSEC' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLCliente = wvarStrXSL
End Function


