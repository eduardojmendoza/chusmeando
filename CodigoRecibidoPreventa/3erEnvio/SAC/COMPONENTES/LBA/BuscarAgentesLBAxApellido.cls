VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "BuscarAgentesLBAxApellido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : sacA_LBA.BuscarAgentesLBAxApellido
'
' Module ID : BuscarAgentesLBAxApellido
'
' File Name : BuscarAgentesLBAxApellido.cls
'
' Creation Date: 30/06/2003
'
' Programmer : Fernando J. Martelli
'
' Abstract : Busca Agentes de la Compa�ia LBA por su Apellido
'
' Entry :
' - parameters : Apellido
'
' Exit :
'                 TIPO_DOC_DES (Clase del Agente)
'                 TIPO_DOC     ("AGE")
'                 NRO_DOC      (Codigo del Agente)
'                 RAZON_SOCIAL (Apellido1 Nombre Apellido2)
'
' Program : A list of messages used in this module
'
' Functions :
'                 p_GetXSLCliente()
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' The followings should be specified (in chronological
' sequence) for each amendment made to the module:
' Version Version number of the module
' Date Date of amendment (dd/mm/ccyy)
' PPCR Associated PPCR number
' Programmer Name of programmer who made the
' Amendment
' Reason Brief description of the amendment
' *****************************************************************

Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_LBA.BuscarAgentesLBAxApellido"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjXMLDocument As MSXML2.DOMDocument
    Dim wobjXSLDocument As MSXML2.DOMDocument
    Dim wobjNodelist    As MSXML2.IXMLDOMNodeList
    Dim wobjNode        As MSXML2.IXMLDOMNode
    Dim wobjNodeNew     As MSXML2.IXMLDOMNode
    '
    Dim wvarApellido    As String
    '
    Dim wvarQuery       As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstCliente     As ADODB.Recordset
    '
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarApellido = wobjXMLrequest.selectSingleNode("//CLIENTE/APELLIDO").Text
    '
    Set wobjXMLrequest = Nothing
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_LBA_DB)
    '
    wvarStep = 50
    
    wvarQuery = "SP_SACLBA_BUSCARAGENTESLBAXAPELLIDO '" & wvarApellido & "'"
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = wvarQuery
    wobjDBCmd.CommandType = adCmdText
    Set wrstCliente = wobjDBCmd.Execute
    Set wrstCliente.ActiveConnection = Nothing
    wvarStep = 70
    '
    If Not wrstCliente.EOF Then
      '
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.async = False
      wvarStep = 80
      wrstCliente.Save wobjXMLDocument, adPersistXML
      Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
      wvarStep = 90
      wobjXSLDocument.loadXML (p_GetXSLCliente)
      pvarResponse = wobjXMLDocument.transformNode(wobjXSLDocument)
    Else
      pvarResponse = "<GENERAL><LISTADOCUMENTOS><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido encontrar el Agente.' /></LISTADOCUMENTOS></GENERAL>"
    End If
    '
    Set wobjXMLDocument = Nothing
    '
    wvarStep = 100
    wrstCliente.Close
    Set wrstCliente = Nothing
    '
    wvarStep = 110
    Set wobjDBCmd = Nothing
    
    wvarStep = 120
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLCliente() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLCliente
  ' Abstract : Genera el codigo XSL de respuesta
  ' Synopsis : string = p_GetXSLCliente()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='GENERAL'>"
  wvarStrXSL = wvarStrXSL & "    <xsl:element name='LISTADOCUMENTOS'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "       <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "       <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     </xsl:element>"
  wvarStrXSL = wvarStrXSL & "     <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "    </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='DATOSCLIENTE'>"
  wvarStrXSL = wvarStrXSL & "    <xsl:element name='TIPO_DOC_DES'><xsl:value-of select='@AGENTCLA' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "    <xsl:element name='TIPO_DOC'>AGE</xsl:element>"
  wvarStrXSL = wvarStrXSL & "    <xsl:element name='NRO_DOC'><xsl:value-of select='@AGENTCOD' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "    <xsl:element name='RAZON_SOCIAL'><xsl:value-of select='@CLIENAP1' /> <xsl:value-of select='@CLIENNOM' /> <xsl:value-of select='@CLIENAP2' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLCliente = wvarStrXSL
End Function


