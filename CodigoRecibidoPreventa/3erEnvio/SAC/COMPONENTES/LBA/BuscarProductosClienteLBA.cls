VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "BuscarProductosClienteLBA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_LBA.BuscarProductosClienteLBA"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName     As String = "IAction_Execute"
    Dim wvarStep         As Long
    '
    Dim wobjXMLrequest   As MSXML2.DOMDocument
    Dim wobjXMLDocument1 As MSXML2.DOMDocument
    Dim wobjXSLDocument  As MSXML2.DOMDocument
    Dim wobjNodelist     As MSXML2.IXMLDOMNodeList
    Dim wobjNode         As MSXML2.IXMLDOMNode
    Dim wobjNodeUniNegocio As MSXML2.IXMLDOMNode
    Dim wobjNodeDelegacion As MSXML2.IXMLDOMNode
    '
    Dim wvarQuery        As String
    Dim wvarClienSecs    As String
    Dim wvarCompania     As String
    Dim wvarRamopCod     As String
    Dim wvarPolizAnn     As String
    Dim wvarPolizSec     As String
    Dim wvarCertiPol     As String
    Dim wvarCertiAnn     As String
    Dim wvarCertiSec     As String
    '
    Dim wobjHSBC_DBCnn   As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn        As ADODB.Connection
    Dim wobjDBCmd        As ADODB.Command
    Dim wobjDBParm       As ADODB.Parameter
    Dim wrstProductos    As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    Set wobjNodelist = wobjXMLrequest.selectNodes("//PRODUCTOS/CLIENSECS/CLIENSEC")
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    If wobjNodelist.length > 0 Then
      wvarStep = 40
      Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_LBA_DB)
      '
      wvarStep = 50
      For Each wobjNode In wobjNodelist
        wvarClienSecs = wvarClienSecs & wobjNode.Text & ","
      Next
      wvarClienSecs = Mid(wvarClienSecs, 1, Len(wvarClienSecs) - 1)
      '
      wvarQuery = "SP_SACLBA_BUSCARPRODUCTOSCLIENTELBA '" & wvarClienSecs & "'"
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = wvarQuery
      wobjDBCmd.CommandType = adCmdText
      '
      wvarStep = 60
      '
      wobjDBCmd.CommandTimeout = 90
      Set wrstProductos = wobjDBCmd.Execute
      Set wrstProductos.ActiveConnection = Nothing
      '
      wvarStep = 70
      Set wobjDBCmd = Nothing
      '
      wvarStep = 80
      If Not wrstProductos.EOF Then
        Set wobjXMLDocument1 = CreateObject("MSXML2.DomDocument")
        wobjXMLDocument1.async = False
        wvarStep = 90
        wrstProductos.Save wobjXMLDocument1, adPersistXML
        Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
        wvarStep = 100
        wobjXSLDocument.loadXML (p_GetXSLProductos)
        wobjXMLDocument1.loadXML (wobjXMLDocument1.transformNode(wobjXSLDocument))
        '
        Set wobjNodelist = wobjXMLDocument1.selectNodes("//PRODUCTOS/DETALLEPRODUCTO")
        For Each wobjNode In wobjNodelist
          wrstProductos.Close
          '
          wvarStep = 110
          wvarCompania = wobjNode.selectSingleNode("COMPANIA").Text
          wvarRamopCod = wobjNode.selectSingleNode("CODIGO").Text
          wvarPolizAnn = wobjNode.selectSingleNode("POLIZANN").Text
          wvarPolizSec = wobjNode.selectSingleNode("POLIZSEC").Text
          wvarCertiPol = wobjNode.selectSingleNode("CERTIPOL").Text
          wvarCertiAnn = wobjNode.selectSingleNode("CERTIANN").Text
          wvarCertiSec = wobjNode.selectSingleNode("CERTISEC").Text
          wvarStep = 120
          ' OBTENCION DE LA UNIDAD DE NEGOCIO
          Set wobjDBCmd = CreateObject("ADODB.Command")
          Set wobjDBCmd.ActiveConnection = wobjDBCnn
          wobjDBCmd.CommandType = adCmdStoredProc
          wobjDBCmd.CommandText = "SP_SACLBA_OBTENERUNIDADNEGOCIO"
          '
          wvarStep = 130
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@Compania"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adChar
          wobjDBParm.Size = 4
          wobjDBParm.Value = wvarCompania
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 140
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@Ramo"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adChar
          wobjDBParm.Size = 4
          wobjDBParm.Value = wvarRamopCod
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 150
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@PolizAnn"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 2
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarPolizAnn
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 160
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@PolizSec"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 6
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarPolizSec
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 170
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@CertiPol"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 4
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarCertiPol
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 180
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@CertiAnn"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 4
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarCertiAnn
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 190
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@CertiSec"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 6
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarCertiSec
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 200
          Set wrstProductos = wobjDBCmd.Execute
          Set wrstProductos.ActiveConnection = Nothing
          Set wobjNodeUniNegocio = wobjXMLDocument1.createElement("UNIDADNEGOCIO")
          wvarStep = 210
          If Not wrstProductos.EOF Then
            wobjNodeUniNegocio.Text = Mid(wrstProductos.Fields("UNI_NEGO").Value, 5, 1)
          Else
            wobjNodeUniNegocio.Text = ""
          End If
          wobjNode.appendChild wobjNodeUniNegocio
          Set wobjNodeUniNegocio = Nothing
          ' OBTENCION DE LA DELEGACION
          wvarStep = 220
          Set wobjDBCmd = CreateObject("ADODB.Command")
          Set wobjDBCmd.ActiveConnection = wobjDBCnn
          wobjDBCmd.CommandType = adCmdStoredProc
          wobjDBCmd.CommandText = "SP_SACLBA_OBTENERDELEGACION"
          '
          wvarStep = 230
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@Compania"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adChar
          wobjDBParm.Size = 4
          wobjDBParm.Value = wvarCompania
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 240
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@Ramo"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adChar
          wobjDBParm.Size = 4
          wobjDBParm.Value = wvarRamopCod
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 250
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@PolizAnn"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 2
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarPolizAnn
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 260
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@PolizSec"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 6
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarPolizSec
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 270
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@CertiPol"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 4
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarCertiPol
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 280
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@CertiAnn"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 4
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarCertiAnn
          wobjDBCmd.Parameters.Append wobjDBParm
          Set wobjDBParm = Nothing
          '
          wvarStep = 290
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@CertiSec"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adNumeric
          wobjDBParm.Precision = 6
          wobjDBParm.NumericScale = 0
          wobjDBParm.Value = wvarCertiSec
          wobjDBCmd.Parameters.Append wobjDBParm
          '
          Set wobjDBParm = Nothing
          '
          wvarStep = 300
          Set wrstProductos = wobjDBCmd.Execute
          Set wrstProductos.ActiveConnection = Nothing
          Set wobjNodeDelegacion = wobjXMLDocument1.createElement("DELEGACION")
          If Not wrstProductos.EOF Then
            wobjNodeDelegacion.Text = Mid(wrstProductos.Fields("UNI_NEGO").Value, 1, 4)
          Else
            wobjNodeDelegacion.Text = ""
          End If
          wobjNode.appendChild wobjNodeDelegacion
          Set wobjNodeDelegacion = Nothing
        Next
        Set wobjNodelist = Nothing
      Else
        Set wobjXMLDocument1 = CreateObject("MSXML2.DomDocument")
        wobjXMLDocument1.async = False
        wobjXMLDocument1.loadXML ("<PRODUCTOS><ESTADO RESULTADO='FALSE' MENSAJE='No se han podido encontrar productos para el cliente' /></PRODUCTOS>")
      End If
    Else
      Set wobjXMLDocument1 = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument1.async = False
      wobjXMLDocument1.loadXML ("<PRODUCTOS><ESTADO RESULTADO='FALSE' MENSAJE='No se han podido encontrar productos para el cliente' /></PRODUCTOS>")
    End If
    wvarStep = 310
    pvarResponse = wobjXMLDocument1.xml
    '
    wvarStep = 320
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 330
    If wrstProductos.State <> 0 Then
      wrstProductos.Close
    End If
    '
    wvarStep = 340
    Set wrstProductos = Nothing
    '
    wvarStep = 270
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLProductos() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='PRODUCTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPRODUCTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='DESCRIPCION'><xsl:value-of select='@RAMOPDAB' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='POLIZACERTIFICADO'><xsl:value-of select='@RAMOPCOD' />-<xsl:value-of select='@POLIZANN' />-<xsl:value-of select='@POLIZSEC' />-<xsl:value-of select='@CERTIPOL' />-<xsl:value-of select='@CERTIANN' />-<xsl:value-of select='@CERTISEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='COMPANIA'><xsl:value-of select='@CIAASCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CODIGO'><xsl:value-of select='@RAMOPCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='POLIZANN'><xsl:value-of select='@POLIZANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='POLIZSEC'><xsl:value-of select='@POLIZSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CERTIPOL'><xsl:value-of select='@CERTIPOL' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CERTIANN'><xsl:value-of select='@CERTIANN' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='CERTISEC'><xsl:value-of select='@CERTISEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLProductos = wvarStrXSL
End Function
Private Function p_GetXSLProductosRVP() _
                 As String
    '
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='PRODUCTOS'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "   </xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPRODUCTO'>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='PRODUCTO'><xsl:value-of select='@RAMOPDES' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "   <xsl:element name='POLIZACERTIFICADO'><xsl:value-of select='@POLIZANN' />-<xsl:value-of select='@POLIZSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & " </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSLProductosRVP = wvarStrXSL
End Function


