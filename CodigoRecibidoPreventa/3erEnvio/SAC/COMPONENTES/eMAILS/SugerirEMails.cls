VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SugerirEMails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : sacA_Emails.SugerirEMails
'
' Module ID : SugerirEMails
'
' File Name : SugerirEMails.cls
'
' Creation Date: 17/05/2005
'
' Programmer : Leonardo G. Fister
'
' Abstract : Sugiere un mail valido de acuerdo a un mail ingresado
'
' Entry :
' - parameters :
'                 EMAIL'
'
' Functions :
'                 p_GetXSLEMails()
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' Date of amendment :
' PPCR Associated   :
' Programmer Name   :
' Amendment
'
'
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Emails.SugerirEMails"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    Dim wvarEmail           As String
    Dim wvarPos             As String
    Dim wvarNombre          As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstMails           As ADODB.Recordset
    '
    Dim wobjXML             As MSXML2.DOMDocument
    Dim wobjXSL             As MSXML2.DOMDocument
    Dim wobjXMLNodeList     As MSXML2.IXMLDOMNodeList
    Dim wobjXMLNode         As MSXML2.IXMLDOMNode
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEmail = wobjXMLrequest.selectSingleNode("//EMAILS/EMAIL").Text
    'wvarPos = InStr(1, wvarEmail, "@")
    'wvarNombre = Mid(wvarEmail, 1, wvarPos - 1)
    'wvarEmail = Mid(wvarEmail, wvarPos + 1)
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 50
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 60
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_SAC_SUGERIRMAILS"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Email"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 50
    wobjDBParm.Value = wvarEmail
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wrstMails = wobjDBCmd.Execute
    Set wrstMails.ActiveConnection = Nothing
    '
    wvarStep = 90
    If Not wrstMails.EOF Then
      Set wobjXML = CreateObject("MSXML2.DomDocument")
      wobjXML.async = False
      wrstMails.Save wobjXML, adPersistXML
      '
      wvarStep = 100
      Set wobjXSL = CreateObject("MSXML2.DomDocument")
      wobjXSL.async = False
      wobjXSL.loadXML (p_GetXSLEMails())
      '
      wvarStep = 110
      wobjXML.loadXML (wobjXML.transformNode(wobjXSL))
      Set wobjXMLNodeList = wobjXML.selectNodes("//EMAILS/EMAIL")
      wvarStep = 120
      For Each wobjXMLNode In wobjXMLNodeList
        'wobjXMLNode.selectSingleNode("EML_SUGERIDO").Text = "<![CDATA[" & wvarNombre & "@" & wobjXMLNode.selectSingleNode("EML_SUGERIDO").Text & "]]>"
        'wobjXMLNode.selectSingleNode("EML_SUGERIDO").Text = wvarNombre & "@" & wobjXMLNode.selectSingleNode("EML_SUGERIDO").Text
        wobjXMLNode.selectSingleNode("EML_SUGERIDO").Text = wobjXMLNode.selectSingleNode("EML_SUGERIDO").Text
      Next
      wvarStep = 130
      pvarResponse = wobjXML.xml
      Set wobjXMLNode = Nothing
      Set wobjXMLNodeList = Nothing
      Set wobjXSL = Nothing
      Set wobjXML = Nothing
    Else
      pvarResponse = "<EMAILS><ESTADO RESULTADO=""FALSE"" MENSAJE=""No se han podido encontrar mails sugeridos"" /></EMAILS>"
    End If
    '
    wvarStep = 140
    Set wobjDBCmd = Nothing
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 150
    If wrstMails.State <> 0 Then wrstMails.Close
    '
    wvarStep = 160
    Set wrstMails = Nothing
    '
    wvarStep = 180
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLEMails() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLEMails
  ' Abstract : Genera el codigo XSL de respuesta
  ' Synopsis : string = p_GetXSLCliente()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='EMAILS'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "   </xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='EMAIL'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_SUGERIDO'><xsl:value-of select='@sugerido' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLEMails = wvarStrXSL
End Function
