VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "VerificacionEMails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name : sacA_Emails.ObtenerEMails
'
' Module ID : ObtenerEMails
'
' File Name : ObtenerEMails.cls
'
' Creation Date: 17/03/2004
'
' Programmer : Fernando J. Martelli
'
' Abstract : Busca los emails de un Cliente
'
' Entry :
' - parameters :
'                 EMPRESA
'                 TIPODOC
'                 DOCUMENTO
' Exit :
'
' Program : A list of messages used in this module
'
' Functions :
'                 p_GetXSLEMails()
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' Date of amendment :
' PPCR Associated   :
' Programmer Name   :
' Amendment
'
'
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Emails.VerificacionEMails"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    Dim wvarEmpresa         As String
    Dim wvarTipoDoc         As String
    Dim wvarDocumento       As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstMails           As ADODB.Recordset
    '
    Dim wobjXML             As MSXML2.DOMDocument
    Dim wobjXSL             As MSXML2.DOMDocument
    Dim wobjXMLNodeList     As MSXML2.IXMLDOMNodeList
    Dim wobjXMLNode         As MSXML2.IXMLDOMNode
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//EMAILS/EMPRESA").Text
    '
    wvarStep = 30
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//EMAILS/TIPODOC").Text
    '
    wvarStep = 40
    wvarDocumento = wobjXMLrequest.selectSingleNode("//EMAILS/DOCUMENTO").Text
    '
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 50
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 60
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "spGetEstadoEmail"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@tipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@documento"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = NumFilled(wvarDocumento, 15)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wrstMails = wobjDBCmd.Execute
    Set wrstMails.ActiveConnection = Nothing
    '
    wvarStep = 110
    If Not wrstMails.EOF Then
      pvarResponse = "<EMAILS><ESTADO RESULTADO=""TRUE"" MENSAJE="""" /><ESTADO_EMAIL>" & wrstMails.Fields("Estado").Value & "</ESTADO_EMAIL></EMAILS>"
    Else
      pvarResponse = "<EMAILS><ESTADO RESULTADO=""FALSE"" MENSAJE=""No se han podido encontrar datos"" /></EMAILS>"
    End If
    '
    wvarStep = 120
    Set wobjDBCmd = Nothing
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    If wrstMails.State <> 0 Then wrstMails.Close
    '
    wvarStep = 140
    Set wrstMails = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function

Private Function p_GetXSLEMails() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLEMails
  ' Abstract : Genera el codigo XSL de respuesta
  ' Synopsis : string = p_GetXSLCliente()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='EMAILS'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "   </xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='EMAIL'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_ID'><xsl:value-of select='@EML_ID' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_EMAIL'><xsl:value-of select='@EML_EMAIL' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_TIPOEMAIL'><xsl:value-of select='@EML_TIPOEMAIL' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_CANAL'><xsl:value-of select='@EML_CANAL' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_FECHAING'><xsl:value-of select='@EML_FECHAING' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NUM_FECHAING'><xsl:value-of select='@NUM_FECHAING' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_RECIBEINFO'><xsl:value-of select='@EML_RECIBEINFO' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_NIVELSEG'><xsl:value-of select='@EML_NIVELSEG' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_FECHAMOD'><xsl:value-of select='@EML_FECHAMOD' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='NUM_FECHAMOD'><xsl:value-of select='@NUM_FECHAMOD' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='EML_USUARIO'><xsl:value-of select='@EML_USUARIO' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLEMails = wvarStrXSL
End Function

