VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "GrabarEMails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Emails.GrabarEMails"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
Const wcteFnName        As String = "IAction_Execute"
Dim wvarStep            As Long
'
Dim wobjXMLrequest      As MSXML2.DOMDocument
Dim wobjNodeList        As IXMLDOMNodeList
Dim wobjNode            As IXMLDOMNode
'
Dim wvarEmpresa         As String
Dim wvarTipoDoc         As String
Dim wvarDocumento       As String
'
Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
Dim wobjDBCnn           As ADODB.Connection
Dim wobjDBCmd           As ADODB.Command
Dim wobjDBParm          As ADODB.Parameter
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~
On Error GoTo ErrorHandler
'~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'
    wvarStep = 10
    pvarRequest = stringToXML(pvarRequest)
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    With wobjXMLrequest
      wvarEmpresa = .selectSingleNode("//ROOT/EMPRESA").Text
      wvarTipoDoc = .selectSingleNode("//ROOT/TIPODOC").Text
      wvarDocumento = .selectSingleNode("//ROOT/DOCUMENTO").Text
    End With
    '
    wvarStep = 30
    Set wobjNodeList = wobjXMLrequest.selectNodes("//ROOT/EMAILS/EMAIL")
    If wobjNodeList.length > 0 Then
      wvarStep = 40
      Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
      '
      wvarStep = 50
      Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
      '
      wvarStep = 60
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_SAC_BORRAREMAILS"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 70
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Empresa"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 80
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TipoDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = UCase(wvarTipoDoc)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 90
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Docmento"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarDocumento, 15)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 100
      Call wobjDBCmd.Execute
      '
      wvarStep = 110
      For Each wobjNode In wobjNodeList
        wvarStep = 120
        Set wobjDBCmd = CreateObject("ADODB.Command")
        Set wobjDBCmd.ActiveConnection = wobjDBCnn
        wobjDBCmd.CommandText = "SP_SAC_INSERTAREMAILS"
        wobjDBCmd.CommandType = adCmdStoredProc
        '
        wvarStep = 130
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Codemp"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adInteger
        wobjDBParm.Value = wvarEmpresa
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 140
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@TipoDoc"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adChar
        wobjDBParm.Size = 3
        wobjDBParm.Value = UCase(wvarTipoDoc)
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 150
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Documento"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adChar
        wobjDBParm.Size = 15
        wobjDBParm.Value = NumFilled(wvarDocumento, 15)
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 160
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Id"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adInteger
        wobjDBParm.Value = wobjNode.selectSingleNode("ID").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 170
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Canal"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adInteger
        wobjDBParm.Value = wobjNode.selectSingleNode("CANAL").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 180
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Email"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 50
        wobjDBParm.Value = wobjNode.selectSingleNode("ADDRESS").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 190
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@FechaIng"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 14
        wobjDBParm.Value = wobjNode.selectSingleNode("FECHAING").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 200
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@FechaMod"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 14
        wobjDBParm.Value = wobjNode.selectSingleNode("FECHAMOD").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 210
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@RecibeInfo"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 1
        wobjDBParm.Value = wobjNode.selectSingleNode("RECIBEINFO").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 220
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@NivelSeg"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adInteger
        wobjDBParm.Value = wobjNode.selectSingleNode("NIVELSEG").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 230
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Usuario"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 20
        wobjDBParm.Value = wobjNode.selectSingleNode("USUARIO").Text
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 240
        Call wobjDBCmd.Execute
      Next
      wobjDBCnn.Close
      Set wobjNode = Nothing
    End If
    Set wobjNodeList = Nothing
    '
    wvarStep = 250
    pvarResponse = "<ROOT><ESTADO RESULTADO='TRUE' MENSAJE='' /></ROOT>"
    '
    Set wobjDBParm = Nothing
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 260
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<ROOT><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></ROOT>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    App.LogEvent pvarRequest
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function

