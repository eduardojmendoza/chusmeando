VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "Registraciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.Registraciones"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName            As String = "IAction_Execute"
    
    'App.LogEvent pvarRequest
    '
    Dim wvarStep                As Long
    '
    '
    Dim wobjXMLrequest          As MSXML2.DOMDocument
    Dim wobjXMLresponse2        As MSXML2.DOMDocument
    '
    Dim wvarRegCliente          As String
    Dim wvarNroPedido           As Long
    Dim wvarUser                As String
    Dim wvarSucursal            As String
    Dim wvarSector              As String
    Dim wvarModoContacto        As String
    Dim wvarTipoContacto        As String
    Dim wvarEmpresa             As String
    Dim wvarTipoDoc             As String
    Dim wvarNroDoc              As String
    Dim wvarCodProducto         As String
    Dim wvarCodMotivo           As String
    Dim wvarCodEstado           As String
    Dim wvarCodCausaContacto    As String
    Dim wvarObservaciones       As String
    Dim wvarRazonSocial         As String
    Dim wvarCalle               As String
    Dim wvarCategoria           As String
    Dim wvarCategoriaCIS        As String
    Dim wvarLocalidad           As String
    Dim wvarNacionalidad        As String
    Dim wvarFechaNacimiento     As String
    Dim wvarCodigoPostal        As String
    Dim wvarTelefono            As String
    Dim wvarNuevoDocumento      As String
    Dim wvarTipoDocRVP          As String
    Dim wvarInsertoNoCliente    As String
    Dim wvarNoDeriva            As String
    Dim wvarUnidadNegocio       As String
    Dim wvarRegion              As String
    Dim wvarDelegacion          As String
    
    '
    Dim wobjNoCliente               As IAction
''''    Dim wobjNotificacion        As IAction
    Dim wobjRelacionesEspeciales    As IAction
    Dim wobjRegistracion            As IAction
    '
    Dim wvarActionNoCliente             As Long
    Dim wvarActionRelacionesEspeciales  As Long
    '
    '
    Dim wvarRequestNoCliente            As String
    Dim wvarRequestRelacionesEspeciales As String
''''    Dim wvarRequestNotificacion     As String
    '
    Dim wvarResNoCliente            As String
    Dim wvarResRelEspeciales        As String
''''    Dim wvarResNotificacion         As String
    Dim wvarResRegistracion         As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    'App.LogEvent pvarRequest
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    'App.LogEvent "*******Registra:" & wobjXMLrequest.selectSingleNode("//REGISTRACION/REGISTRACLIENTE").Text & wvarStep
    wvarStep = 20
    With wobjXMLrequest
        wvarRegCliente = .selectSingleNode("//REGISTRACION/REGISTRACLIENTE").Text
        wvarEmpresa = .selectSingleNode("//REGISTRACION/EMPRESA").Text
        wvarNroPedido = .selectSingleNode("//REGISTRACION/NRO_PEDIDO").Text
        wvarUser = UCase(.selectSingleNode("//REGISTRACION/USER").Text)
        wvarSucursal = .selectSingleNode("//REGISTRACION/SUCURSAL").Text
        wvarSector = .selectSingleNode("//REGISTRACION/SECTOR").Text
        wvarModoContacto = .selectSingleNode("//REGISTRACION/MODOCONTACTO").Text
        wvarTipoContacto = .selectSingleNode("//REGISTRACION/TIPOCONTACTO").Text
        wvarTipoDoc = .selectSingleNode("//REGISTRACION/TIPO_DOC").Text
        wvarNroDoc = .selectSingleNode("//REGISTRACION/NRO_DOC").Text
        wvarCodProducto = .selectSingleNode("//REGISTRACION/COD_PRODUCTO").Text
        wvarCodMotivo = .selectSingleNode("//REGISTRACION/COD_MOTIVO").Text
        wvarCodEstado = .selectSingleNode("//REGISTRACION/ESTADO").Text
        wvarCodCausaContacto = .selectSingleNode("//REGISTRACION/COD_CAUSA_CONTACTO").Text
        wvarObservaciones = .selectSingleNode("//REGISTRACION/OBSERVACIONES").Text
        wvarRazonSocial = .selectSingleNode("//REGISTRACION/RAZON_SOCIAL").Text
        wvarCalle = .selectSingleNode("//REGISTRACION/CALLE").Text
        wvarLocalidad = .selectSingleNode("//REGISTRACION/LOCALIDAD").Text
        wvarNacionalidad = .selectSingleNode("//REGISTRACION/NACIONALIDAD").Text
        wvarFechaNacimiento = .selectSingleNode("//REGISTRACION/FECHA_NAC").Text
        wvarCodigoPostal = .selectSingleNode("//REGISTRACION/COD_POSTAL").Text
        wvarTelefono = .selectSingleNode("//REGISTRACION/TELEFONO").Text
        wvarTipoDocRVP = .selectSingleNode("//REGISTRACION/TIPODOCRVP").Text
        wvarNoDeriva = .selectSingleNode("//REGISTRACION/NODERIVA").Text
        wvarUnidadNegocio = .selectSingleNode("//REGISTRACION/UNIDADNEGOCIO").Text
        wvarRegion = .selectSingleNode("//REGISTRACION/REGION").Text
        wvarDelegacion = .selectSingleNode("//REGISTRACION/DELEGACION").Text
        wvarStep = 25
        wvarCategoria = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/CATEGORIA")
        wvarCategoriaCIS = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/CATEGORIACIS")

    End With
    wvarStep = 30
    '
    wvarStep = 40
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarStep = 45
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//REGISTRACION/TIPO_DOC").Text = wvarTipoDoc
    ElseIf (wvarEmpresa = wconstCODIGONYL) Or (wvarEmpresa = wconstCODIGOLBA) Then
      wvarStep = 48
      'App.LogEvent "********PASO: " & wvarStep
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      'App.LogEvent "********wvarTipoDoc: " & wvarTipoDoc & " - PASO: " & wvarStep
      wvarStep = 49
      wobjXMLrequest.selectSingleNode("//REGISTRACION/TIPO_DOC").Text = wvarTipoDoc
      'App.LogEvent "********wobjXMLrequest.selectSingleNode(//REGISTRACION/TIPO_DOC).Text: " & wobjXMLrequest.selectSingleNode("//REGISTRACION/TIPO_DOC").Text & " - PASO: " & wvarStep
    End If
    wvarStep = 50
    'App.LogEvent wvarStep
    If CBool(wvarRegCliente) Then
      '
      wvarRequestNoCliente = "<GRABACIONES>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<EMPRESA>" & wvarEmpresa & "</EMPRESA>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<TIPO_DOC>" & wvarTipoDoc & "</TIPO_DOC>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<NRO_DOC>" & wvarNroDoc & "</NRO_DOC>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<RAZON_SOCIAL><![CDATA[" & wvarRazonSocial & "]]></RAZON_SOCIAL>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<CALLE><![CDATA[" & wvarCalle & "]]></CALLE>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<LOCALIDAD><![CDATA[" & wvarLocalidad & "]]></LOCALIDAD>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<NACIONALIDAD><![CDATA[" & wvarNacionalidad & "]]></NACIONALIDAD>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<FECHA_NAC>" & wvarFechaNacimiento & "</FECHA_NAC>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<COD_POSTAL>" & wvarCodigoPostal & "</COD_POSTAL>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<TELEFONO>" & wvarTelefono & "</TELEFONO>"
      wvarRequestNoCliente = wvarRequestNoCliente & "<CATEGORIA>" & IIf(wvarEmpresa = wconstCODIGODOCTHOS, Left(wvarDelegacion & " ", 1), "") & "</CATEGORIA>"  'por ahora nada para reg <> de doctos
      wvarRequestNoCliente = wvarRequestNoCliente & "</GRABACIONES>"
  
      wvarStep = 60
      Set wobjNoCliente = mobjCOM_Context.CreateInstance("sacA_Grabaciones.NoClientes")
      wvarActionNoCliente = wobjNoCliente.Execute(wvarRequestNoCliente, wvarResNoCliente, pvarContextInfo)
      Set wobjNoCliente = Nothing
    End If
    If wvarTipoContacto = "C" And wvarEmpresa <> wconstCODIGOBANCO And wvarEmpresa <> wconstCODIGOSACINT And wvarEmpresa <> wconstCODIGOSACCOMPRAS And wvarEmpresa <> wconstCODIGOHEXAGON Then     'La registracion es de un cliente no banking
      wvarStep = 70
      wvarRequestRelacionesEspeciales = "<RELACIONESESPECIALES>"
      wvarRequestRelacionesEspeciales = wvarRequestRelacionesEspeciales & "<NROPEDIDO>" & wvarNroPedido & "</NROPEDIDO>"
      wvarRequestRelacionesEspeciales = wvarRequestRelacionesEspeciales & "<UNIDADNEGOCIO>" & wvarUnidadNegocio & "</UNIDADNEGOCIO>"
      wvarRequestRelacionesEspeciales = wvarRequestRelacionesEspeciales & "<REGION>" & wvarRegion & "</REGION>"
      wvarRequestRelacionesEspeciales = wvarRequestRelacionesEspeciales & "<DELEGACION>" & wvarDelegacion & "</DELEGACION>"
      'wvarRequestRelacionesEspeciales = wvarRequestRelacionesEspeciales & "<CATEGORIA>" & wvarCategoria & "</CATEGORIA>"
      wvarRequestRelacionesEspeciales = wvarRequestRelacionesEspeciales & "</RELACIONESESPECIALES>"
      Set wobjRelacionesEspeciales = CreateObject("sacA_Grabaciones.RelacionesEspeciales")
      wvarActionRelacionesEspeciales = wobjRelacionesEspeciales.Execute(wvarRequestRelacionesEspeciales, wvarResRelEspeciales, pvarContextInfo)
      Set wobjRelacionesEspeciales = Nothing
    End If
    '
    'Desde este componente no se valida el documento XML de salida ya que de existir
    'el cliente al que se quiere agregar la registracion si se da de alta a la registracion
    '
    If wvarActionNoCliente = 0 Then
        wvarStep = 80
        Set wobjRegistracion = mobjCOM_Context.CreateInstance("sacA_Grabaciones.Registracion")
        If CBool(wvarRegCliente) Then
          wvarStep = 90
          Set wobjXMLresponse2 = CreateObject("MSXML2.DOMDocument")
          With wobjXMLresponse2
              .async = False
              Call .loadXML(wvarResNoCliente)
          End With
          wvarInsertoNoCliente = wobjXMLresponse2.selectSingleNode("//GRABACIONES/ESTADO").Attributes.getNamedItem("INSERCION").Text
          If Trim(UCase(wvarTipoDoc)) = "DES" And CBool(wvarRegCliente) And CBool(wvarInsertoNoCliente) Then
            wvarStep = 100
            wvarNuevoDocumento = wobjXMLresponse2.selectSingleNode("//GRABACIONES/ESTADO").Attributes.getNamedItem("NUEVO_DOC").Text
            wobjXMLrequest.selectSingleNode("//REGISTRACION/NRO_DOC").Text = wvarNuevoDocumento
          End If
          Set wobjXMLresponse2 = Nothing
        End If
        wvarStep = 110
        '
        If wobjRegistracion.Execute(wobjXMLrequest.xml, wvarResRegistracion, pvarContextInfo) = 0 Then
            '
            wvarStep = 120
''''            Set wobjNotificacion = mobjCOM_Context.CreateInstance("sacA_Grabaciones.NotificarReclamo")
''''            wvarRequestNotificacion = "<RECLAMO>"
''''            wvarRequestNotificacion = wvarRequestNotificacion & "<NRO_PEDIDO>" & wvarNroPedido & "</NRO_PEDIDO>"
''''            wvarRequestNotificacion = wvarRequestNotificacion & "<SECTOR>" & wvarSector & "</SECTOR>"
''''            wvarRequestNotificacion = wvarRequestNotificacion & "<COD_PRODUCTO>" & wvarCodProducto & "</COD_PRODUCTO>"
''''            wvarRequestNotificacion = wvarRequestNotificacion & "<COD_MOTIVO>" & wvarCodMotivo & "</COD_MOTIVO>"
''''            wvarRequestNotificacion = wvarRequestNotificacion & "<COD_CAUSACONTACTO>" & wvarCodCausaContacto & "</COD_CAUSACONTACTO>"
''''            wvarRequestNotificacion = wvarRequestNotificacion & "</RECLAMO>"
''''            If wobjNotificacion.Execute(wvarRequestNotificacion, wvarResNotificacion, pvarContextInfo) = 0 Then
              If Trim(UCase(wvarTipoDoc)) = "DES" Then
                Call mobjCOM_Context.SetComplete
                pvarResponse = "<GRABACIONES>"
                pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='" & wvarNuevoDocumento & "' />"
                pvarResponse = pvarResponse & "</GRABACIONES>"
              Else
                Call mobjCOM_Context.SetComplete
                pvarResponse = "<GRABACIONES>"
                pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
                pvarResponse = pvarResponse & "</GRABACIONES>"
              End If
''''            Else
''''                Call mobjCOM_Context.SetAbort
''''                pvarResponse = "<GRABACIONES>"
''''                pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='' />"
''''                pvarResponse = pvarResponse & "</GRABACIONES>"
''''            End If
''''            Set wobjNotificacion = Nothing
        Else
          Call mobjCOM_Context.SetAbort
          'pvarResponse = "<GRABACIONES>"
          'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido grabar el reclamo NRO." & wvarNroPedido & ". Intentelo nuevamente' />"
          'pvarResponse = pvarResponse & "</GRABACIONES>"
          pvarResponse = wvarResRegistracion
        End If
        Set wobjRegistracion = Nothing
    Else
        Call mobjCOM_Context.SetAbort
        'pvarResponse = "<GRABACIONES>"
        'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido grabar el cliente para el reclamo NRO." & wvarNroPedido & ". Intentelo nuevamente' />"
        'pvarResponse = pvarResponse & "</GRABACIONES>"
        pvarResponse = wvarResNoCliente
    End If
    Set wobjXMLrequest = Nothing
Exit Function
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido generar el reclamo nro. : " & wvarNroPedido & " (Step : " & wvarStep & " - " & stringToXML(Err.Description) & ")' /></GRABACIONES>"
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function

