VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "AgendaNoCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.AgendaNoCliente"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName                As String = "IAction_Execute"
    Dim wvarStep                    As Long
    '
    Dim wobjXMLrequest              As MSXML2.DOMDocument
    Dim wobjXMLrequest2             As MSXML2.DOMDocument
    '
    Dim wvarEmpresa                 As String
    Dim wvarRegistraCliente         As Boolean
    Dim wvarFechaDeAgenda           As String
    Dim wvarHora                    As String
    Dim wvarCierra                  As String
    Dim wvarTipoDoc                 As String
    Dim wvarTipoDocNYL              As String
    Dim wvarNroDoc                  As String
    Dim wvarCodCampania             As String
    Dim wvarCodLugar                As String
    Dim wvarCodDestino              As String
    Dim wvarCodCanal                As String
    Dim wvarCodEntrevista           As String
    Dim wvarCodSector               As String
    Dim wvarCodEmpresaSucursal      As String
    Dim wvarCodSucursal             As String
    Dim wvarObservacion             As String
    Dim wvarUser                    As String
    Dim wvarActualizaBase           As Boolean
    Dim wvarExisteCliente           As Boolean
    
    Dim wvarRazonSocial             As String
    Dim wvarCalle                   As String
    Dim wvarLocalidad               As String
    Dim wvarNacionalidad            As String
    Dim wvarFechaNacimiento         As String
    Dim wvarCodPostal               As String
    Dim wvarTelefono                As String
    Dim wvarNuevoDocumento          As String
    Dim wvarProducto                As String
    Dim wvarCategoria               As String
            
    Dim wobjNoCliente               As IAction
    Dim wvarActionNoCliente         As Long
    Dim wvarResponseNoCliente       As String
    
    Dim wobjAgenda                  As IAction
    Dim wvarActionAgenda            As Long
    Dim wvarRequestCliente          As String
    Dim wvarResponseAgenda          As String

    Dim wobjHSBC_DBCnn              As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn                   As ADODB.Connection
    Dim wobjDBCmd                   As ADODB.Command
    Dim wobjDBParm                  As ADODB.Parameter
    '
    Dim wobjTime                    As HSBCInterfaces.ICmdContext
    '
    Set wobjTime = mobjCOM_Context.CreateInstance("HSBC.CmdContext")

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With

    '
    wvarStep = 20
    With wobjXMLrequest
      wvarExisteCliente = .selectSingleNode("//AGREGAR_AGENDA/EXISTE_CLIENTE").Text
      wvarEmpresa = .selectSingleNode("//AGREGAR_AGENDA/EMPRESA").Text
      wvarTipoDoc = .selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text
      wvarNroDoc = .selectSingleNode("//AGREGAR_AGENDA/NRO_DOC").Text
      wvarRazonSocial = .selectSingleNode("//AGREGAR_AGENDA/RAZON_SOCIAL").Text
      wvarCalle = .selectSingleNode("//AGREGAR_AGENDA/CALLE").Text
      wvarLocalidad = .selectSingleNode("//AGREGAR_AGENDA/LOCALIDAD").Text
      wvarNacionalidad = .selectSingleNode("//AGREGAR_AGENDA/NACIONALIDAD").Text
      wvarFechaNacimiento = .selectSingleNode("//AGREGAR_AGENDA/FECHA_NAC").Text
      wvarCodPostal = .selectSingleNode("//AGREGAR_AGENDA/COD_POSTAL").Text
      wvarTelefono = .selectSingleNode("//AGREGAR_AGENDA/TELEFONO").Text
      wvarRegistraCliente = .selectSingleNode("//AGREGAR_AGENDA/REGISTRACLIENTE").Text
      wvarFechaDeAgenda = .selectSingleNode("//AGREGAR_AGENDA/FECHAAGENDA").Text
      wvarHora = .selectSingleNode("//AGREGAR_AGENDA/HORAAGENDA").Text
      wvarCierra = .selectSingleNode("//AGREGAR_AGENDA/CIERRA").Text
      wvarCodCampania = .selectSingleNode("//AGREGAR_AGENDA/COD_CAMPANIA").Text
      wvarCodLugar = .selectSingleNode("//AGREGAR_AGENDA/COD_LUGAR").Text
      wvarCodDestino = .selectSingleNode("//AGREGAR_AGENDA/COD_DESTINO").Text
      wvarCodCanal = .selectSingleNode("//AGREGAR_AGENDA/COD_CANAL").Text
      wvarCodEntrevista = .selectSingleNode("//AGREGAR_AGENDA/COD_ENTREVISTA").Text
      wvarCodSector = .selectSingleNode("//AGREGAR_AGENDA/COD_SECTOR").Text
      wvarCodEmpresaSucursal = .selectSingleNode("//AGREGAR_AGENDA/COD_EMPSUCURSAL").Text
      wvarCodSucursal = .selectSingleNode("//AGREGAR_AGENDA/COD_SUCURSAL").Text
      wvarObservacion = .selectSingleNode("//AGREGAR_AGENDA/OBSERVACION").Text
      wvarUser = .selectSingleNode("//AGREGAR_AGENDA/USUARIO").Text
      wvarActualizaBase = .selectSingleNode("//AGREGAR_AGENDA/ACTUALIZABASE").Text
      wvarProducto = .selectSingleNode("//AGREGAR_AGENDA/PRODUCTO").Text
      wvarCategoria = .selectSingleNode("//AGREGAR_AGENDA/CATEGORIA").Text
    End With
    
    'SE MODIFICA EL TIPODOC PARA EL CASO DE NYL
    If wvarEmpresa = wconstCODIGONYL Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    End If
    '
    wvarActionNoCliente = 0
    '
    wvarStep = 30
    If Not wvarExisteCliente Then
      wvarStep = 40
      wvarRequestCliente = Replace(wobjXMLrequest.xml, "AGREGAR_AGENDA", "GRABACIONES")
      '
      wvarStep = 50
      Set wobjNoCliente = mobjCOM_Context.CreateInstance("sacA_Grabaciones.NoClientes")
      wvarActionNoCliente = wobjNoCliente.Execute(wvarRequestCliente, wvarResponseNoCliente, pvarContextInfo)
      Set wobjNoCliente = Nothing
    End If
    If wvarActionNoCliente = 0 Then
      wvarStep = 50
      Set wobjXMLrequest2 = CreateObject("MSXML2.DOMDocument")
      wvarStep = 60
      With wobjXMLrequest2
        .async = False
        Call .loadXML(wvarResponseNoCliente)
      End With
      wvarStep = 70
      wvarNuevoDocumento = wobjXMLrequest2.selectSingleNode("//GRABACIONES/ESTADO").Attributes.getNamedItem("NUEVO_DOC").Text
      Set wobjXMLrequest2 = Nothing
      '
      If Trim(UCase(wvarTipoDoc)) = "DES" Then
        wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/NRO_DOC").Text = wvarNuevoDocumento
      End If
      wvarStep = 80
      Set wobjAgenda = mobjCOM_Context.CreateInstance("sacA_Grabaciones.Agenda")
      wvarActionAgenda = wobjAgenda.Execute(wobjXMLrequest.xml, wvarResponseAgenda, pvarContextInfo)
      Set wobjAgenda = Nothing
      '
    End If
    Set wobjXMLrequest = Nothing
    '
    If InStr(Trim(UCase(wvarResponseAgenda)), "RESULTADO='FALSE'") = 0 Then
        '
        wvarStep = 90
        If wvarActionAgenda = 0 And wvarActionNoCliente = 0 Then
            '
            wvarStep = 100
            Call mobjCOM_Context.SetComplete
            pvarResponse = "<GRABACIONES><AGREGAR_AGENDA>"
            pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='" & wvarNuevoDocumento & "' />"
            pvarResponse = pvarResponse & "</AGREGAR_AGENDA></GRABACIONES>"
        Else
            '
            wvarStep = 110
            Call mobjCOM_Context.SetAbort
            pvarResponse = "<GRABACIONES><AGREGAR_AGENDA><ESTADO RESULTADO='FALSE' MENSAJE='FALLA EN LA TRANSACCION' /></AGREGAR_AGENDA></GRABACIONES>"
        End If
    Else
        '
        wvarStep = 120
        Call mobjCOM_Context.SetAbort
        pvarResponse = wvarResponseAgenda
    End If
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></GRABACIONES>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function
