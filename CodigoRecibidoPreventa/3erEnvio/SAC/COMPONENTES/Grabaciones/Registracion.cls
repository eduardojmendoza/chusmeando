VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "Registracion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.Registracion"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    
Dim wvarStep            As Long
'
Dim wobjXMLrequest      As MSXML2.DOMDocument


Dim wvarTipoDoc             As String
Dim wvarNroDoc              As String
'
Dim wvarRegistraCliente     As Boolean
Dim wvarEmpresa             As String
Dim wvarParNroPedido        As String
Dim wvarUser                As String
Dim wvarSucurs              As String
Dim wvarCodSector           As String
Dim wvarCodModoContacto     As String
Dim wvarTipoContacto        As String
Dim wvarCodProducto         As String
Dim wvarCodMotivo           As String
Dim wvarEstado              As String
Dim wvarCodCausaContacto    As String
Dim wvarObserv              As String
Dim wvarNoDeriva            As String
Dim wvarIsDiscover          As String
'
Dim wvarSectorResponsable   As String
Dim wvarSectorResponsableCli   As String
Dim wvarDiasEstimados       As Double
'
'
Dim wobjHSBC_DBCnn          As HSBCInterfaces.IDBConnection
Dim wobjDBCnn               As ADODB.Connection
Dim wobjDBCmd               As ADODB.Command
Dim wobjDBParm              As ADODB.Parameter
Dim wrstGrabaciones         As ADODB.Recordset
'
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    pvarRequest = stringToXML(pvarRequest)
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 15
    App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
    
    wvarStep = 18
    With wobjXMLrequest
        wvarStep = 19
        wvarRegistraCliente = .selectSingleNode("//REGISTRACION/REGISTRACLIENTE").Text
        wvarStep = 21
        wvarEmpresa = .selectSingleNode("//REGISTRACION/EMPRESA").Text
        wvarStep = 22
        wvarParNroPedido = .selectSingleNode("//REGISTRACION/NRO_PEDIDO").Text
        wvarStep = 23
        wvarUser = .selectSingleNode("//REGISTRACION/USER").Text
        wvarStep = 24
        wvarSucurs = .selectSingleNode("//REGISTRACION/SUCURSAL").Text
        wvarStep = 25
        wvarCodSector = .selectSingleNode("//REGISTRACION/SECTOR").Text
        wvarStep = 26
        wvarCodModoContacto = .selectSingleNode("//REGISTRACION/MODOCONTACTO").Text
        wvarStep = 27
        wvarTipoContacto = .selectSingleNode("//REGISTRACION/TIPOCONTACTO").Text
        wvarStep = 28
        wvarTipoDoc = .selectSingleNode("//REGISTRACION/TIPO_DOC").Text
        wvarStep = 29
        wvarNroDoc = .selectSingleNode("//REGISTRACION/NRO_DOC").Text
        wvarStep = 30
        wvarNroDoc = NumFilled(wvarNroDoc, LONG_DOC(True))
        wvarStep = 31
        wvarCodProducto = .selectSingleNode("//REGISTRACION/COD_PRODUCTO").Text
        wvarStep = 32
        wvarCodMotivo = .selectSingleNode("//REGISTRACION/COD_MOTIVO").Text
        wvarStep = 33
        wvarEstado = .selectSingleNode("//REGISTRACION/ESTADO").Text
        wvarStep = 34
        wvarCodCausaContacto = .selectSingleNode("//REGISTRACION/COD_CAUSA_CONTACTO").Text
        wvarStep = 35
        wvarObserv = sacarCDATA(.selectSingleNode("//REGISTRACION/OBSERVACIONES").Text)
        wvarStep = 36
        wvarNoDeriva = .selectSingleNode("//REGISTRACION/NODERIVA").Text
        wvarStep = 37
        wvarIsDiscover = .selectSingleNode("//REGISTRACION/ISDISCOVER").Text
        wvarStep = 38
        
        
        Dim wvarRazonSocial     As String
        Dim wvarDomicilio       As String
        Dim wvarLocalidad       As String
        Dim wvarCodPostal       As String
        Dim wvarCategoria       As String
        Dim wvarCategoriaCIS    As String
        Dim wvarTelefono        As String
        
        
        wvarStep = 50
        wvarRazonSocial = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/RAZON_SOCIAL")
        wvarStep = 55
        wvarDomicilio = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/CALLE")
        wvarStep = 60
        wvarLocalidad = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/LOCALIDAD")
        wvarStep = 70
        wvarCodPostal = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/COD_POSTAL")
        wvarStep = 80
        If wvarIsDiscover = "S" Then
            wvarStep = 82
            wvarCategoria = ""
            App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
        Else
            wvarStep = 84
            App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
            On Error Resume Next
            wvarCategoria = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/CATEGORIA")
            On Error GoTo 0
            wvarStep = 85
            App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
        End If
        
        wvarStep = 87
        wvarCategoriaCIS = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/CATEGORIACIS")
        wvarStep = 88
        wvarTelefono = fncCheckXMLNode(wobjXMLrequest, "//REGISTRACION/TELEFONO")
        
        
    End With
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 380
    App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
    '
    If Trim(wvarCodProducto) = vbNullString Then
      wvarStep = 381
      'pvarResponse = "<GRABACIONES>"
      'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='El valor, Producto es requerido' />"
      'pvarResponse = pvarResponse & "</GRABACIONES>"
      Err.Raise vbObjectError + 505, mcteClassName, "No se ha especificado el producto para la registracion"
    'ElseIf Trim(wvarObserv) = vbNullString Then
    '  pvarResponse = "<REGISTRACION>"
    '  pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='El valor, Observacion es requerido' />"
    '  pvarResponse = pvarResponse & "</REGISTRACION>"
    '  Err.Raise vbObjectError + 505, mcteClassName, "No se ha especificado el producto para la registracion"
    ElseIf Trim(wvarCodModoContacto) = vbNullString Then
      wvarStep = 382
      'pvarResponse = "<GRABACIONES>"
      'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='El valor, Modo de Contacto es requerido' />"
      'pvarResponse = pvarResponse & "</GRABACIONES>"
      Err.Raise vbObjectError + 505, mcteClassName, "No se ha especificado el modo de contacto para la registracion"
    ElseIf Trim(wvarCodMotivo) = vbNullString Then
       wvarStep = 383
      'pvarResponse = "<GRABACIONES>"
      'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='El valor, Motivo de Contacto es requerido' />"
      'pvarResponse = pvarResponse & "</GRABACIONES>"
      Err.Raise vbObjectError + 505, mcteClassName, "No se ha especificado el motivo para la registracion"
    ElseIf Trim(wvarCodCausaContacto) = vbNullString Then
        wvarStep = 384
      'pvarResponse = "<GRABACIONES>"
      'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='El valor, Causa de Contacto es requerido' />"
      'pvarResponse = pvarResponse & "</GRABACIONES>"
      Err.Raise vbObjectError + 505, mcteClassName, "No se ha especificado la causa para la registracion"
    ElseIf Trim(wvarEstado) = vbNullString Then
       wvarStep = 385
      'pvarResponse = "<GRABACIONES>"
      'pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='El valor, Estado es requerido' />"
      'pvarResponse = pvarResponse & "</GRABACIONES>"
      Err.Raise vbObjectError + 505, mcteClassName, "No se ha especificado el estado para la registracion"
    End If
    '
    wvarStep = 381
    App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
    
    wvarStep = 40
    
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    '
    wvarStep = 50
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wvarStep = 60
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_obtenerSectorResponsable"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Producto"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodProducto
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Causa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodCausaContacto
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Motivo"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodMotivo
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    'MVA - PREMIER
    'Se agreg� el parametro wvarCategoria para enviar si es premier o no.
    wvarStep = 101
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Categoria"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCategoria
    wobjDBCmd.Parameters.Append wobjDBParm

    Set wrstGrabaciones = wobjDBCmd.Execute
    Set wrstGrabaciones.ActiveConnection = Nothing
    
    If Not wrstGrabaciones.EOF Then
        wvarStep = 102
        wvarSectorResponsable = wrstGrabaciones.Fields("REP_COD_SEC").Value
        wvarDiasEstimados = wrstGrabaciones.Fields("REP_DIA_SOL").Value
        wvarEmpresa = wrstGrabaciones.Fields("REP_COD_EMP").Value
    Else
        wvarSectorResponsable = ""
        wvarDiasEstimados = 0
        wvarEmpresa = wvarEmpresa
    End If
    wvarStep = 111
    If wrstGrabaciones.State <> 0 Then
      wrstGrabaciones.Close
    End If
    Set wrstGrabaciones = Nothing
    

        '---------------------------------------------------------
        '                   SECTOR DEL CLIENTE
        '---------------------------------------------------------
        wvarSectorResponsableCli = BuscarSectorDelCliente(wobjDBCnn, wvarTipoDoc, wvarNroDoc, wvarCodCausaContacto, wvarCodMotivo, wvarEstado)
        If wvarSectorResponsableCli <> "" Then
            wvarSectorResponsable = wvarSectorResponsableCli
        End If
        '---------------------------------------------------------
        '
        '---------------------------------------------------------
        '                 CHEQUEA LA MARCA DE NODERIVA
        '---------------------------------------------------------
        If wvarNoDeriva = "S" Then
          wvarSectorResponsable = wvarCodSector
        End If
        '---------------------------------------------------------
        '
        '---------------------------------------------------------
        '                   CHEQUEA SI ES DISCOVER
        '---------------------------------------------------------
        If wvarIsDiscover = "S" Then
            wvarSectorResponsable = "DISC"
        End If
        '---------------------------------------------------------
        

    
    App.LogEvent "Sector Responsable: " & wvarSectorResponsable & " - CauCon: " & wvarCodCausaContacto & " - NumCau: " & wvarCodMotivo
    
    wvarStep = 120
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_REGISTRACIONES_INSERT"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 130
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@EMPRESA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 140
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NUM_PED"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarParNroPedido
    wobjDBCmd.Parameters.Append wobjDBParm
      
    wvarStep = 150
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NOM_OPE"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
    wobjDBCmd.Parameters.Append wobjDBParm
    
    wvarStep = 160
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@SEC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodSector
    wobjDBCmd.Parameters.Append wobjDBParm
    
    wvarStep = 170
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@SUC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarSucurs
    wobjDBCmd.Parameters.Append wobjDBParm
    
      
    wvarStep = 180
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@MOD_CON"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodModoContacto
    wobjDBCmd.Parameters.Append wobjDBParm
    
    wvarStep = 190
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TIP_CON "
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarTipoContacto
    wobjDBCmd.Parameters.Append wobjDBParm
      
    wvarStep = 200
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_PRO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodProducto
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 210
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAU_CON"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodCausaContacto
    wobjDBCmd.Parameters.Append wobjDBParm
      
    wvarStep = 220
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NUM_CAU"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodMotivo
    wobjDBCmd.Parameters.Append wobjDBParm
      
    wvarStep = 230
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TIP_DOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = Right(Trim(wvarTipoDoc & ""), 3)
    wobjDBCmd.Parameters.Append wobjDBParm
    
    wvarStep = 240
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NUM_DOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarNroDoc
    wobjDBCmd.Parameters.Append wobjDBParm
      
    wvarStep = 250
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@IND_EST"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarEstado
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 260
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@SEC_RES"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarSectorResponsable
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 270
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@DIAS_EST"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Attributes = adParamLong
    wobjDBParm.Type = adNumeric
    wobjDBParm.Precision = 5
    wobjDBParm.NumericScale = 2
    wobjDBParm.Value = wvarDiasEstimados
    wobjDBCmd.Parameters.Append wobjDBParm


    wvarStep = 280
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@IND_CIE"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Attributes = adParamLong
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = "N"
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 290
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@OBS_CON"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 8000
    wobjDBParm.Value = wvarObserv
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    'Desnormalizacion
    '
    '@RazonSocial    varchar(30)=null,
    '@Domicilio  varchar(50)=null,
    '@Localidad  varchar(35)=null,
    '@CodPostal  varchar(8)=null,
    '@Categoria  varchar(1)=null,
    '@CastegoriaCIS  varchar(3)=null
    '@TelefonoPart  varchar(3)=null
    '
    wvarRazonSocial = Left(wvarRazonSocial, 30)
    wvarStep = 295
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@RazonSocial"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 30
    wobjDBParm.Value = wvarRazonSocial
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 297
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    '
    wvarDomicilio = Left(wvarDomicilio, 50)
    wobjDBParm.Name = "@Domicilio"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 50
    wobjDBParm.Value = wvarDomicilio
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 299
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wvarLocalidad = Left(Trim(wvarLocalidad), 35)
    wobjDBParm.Name = "@Localidad"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 35
    wobjDBParm.Value = wvarLocalidad
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 301
    wvarCodPostal = Left(wvarCodPostal, 8)
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wvarCodPostal = Left(wvarCodPostal, 8)
    wobjDBParm.Name = "@CodPostal"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 8
    wobjDBParm.Value = wvarCodPostal
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 302
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Categoria"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCategoria
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 305
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CategoriaCIS"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarCategoriaCIS
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarTelefono = Left(Trim(wvarTelefono), 16)
    wvarStep = 307
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TelefonoPart"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 16
    wobjDBParm.Value = wvarTelefono
    wobjDBCmd.Parameters.Append wobjDBParm
    
    
    
    wvarStep = 309
    App.LogEvent "grabacion step: " & wvarStep & " " & wvarStep & " pedido: " & wvarParNroPedido
    Call wobjDBCmd.Execute
    '
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
      
    '-------------------------------------------------------------------------------
    wvarStep = 310
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 320
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_COMHIS1_INSERT"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    'wvarStep = 95
    'Set wobjDBParm = CreateObject("ADODB.Parameter")
    'wobjDBParm.Name = "@EMPRESA"
    'wobjDBParm.Direction = adParamInput
    'wobjDBParm.Type = adInteger
    'wobjDBParm.Value = wvarEmpresa
    'wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 330
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumPed"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarParNroPedido
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 340
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Estado"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarEstado
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 350
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@EstAnt"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarEstado
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 360
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Usuario"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = UCase(adVarChar)
    wobjDBParm.Size = 10
    wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 370
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Sector"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarSectorResponsable
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 380
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Obs"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = -1
    wobjDBParm.Value = wvarObserv
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 390
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Sec"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodSector
    wobjDBCmd.Parameters.Append wobjDBParm

    wvarStep = 400
    Call wobjDBCmd.Execute
    
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
      
    wvarStep = 410
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 420
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido generar el reclamo nro. : " & wvarParNroPedido & " (Step : " & wvarStep & " - " & stringToXML(Err.Description) & ")' /></GRABACIONES>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Step:" & wvarStep & " Error= [" & Err.Number & "] - " & Err.Description & "wvarCategoria:" & wvarCategoria & " - " & wobjDBCmd.CommandText, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort

End Function


Private Function BuscarSectorDelCliente(pobjDBCnn As ADODB.Connection, pvarTipDoc As String, pvarNrodoc As String, pvarCodCau As String, pvarCodMot As String, pvarCodEst As String) As String

Dim adSelect                As New ADODB.Recordset
Dim wobjSucCli              As HSBCInterfaces.IAction
Dim wvarRequestCli          As String
Dim wvarResponseCli         As String
Dim wvarStep                As Integer
Dim wobjXMLSecCli           As MSXML2.DOMDocument
Dim wvarSql                 As String
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~

    wvarSql = "sp_SAC_ListarEstadoCausas 'A','" & pvarCodCau & "','" & pvarCodMot & "',DEFAULT"
    adSelect.Open wvarSql, pobjDBCnn
    Do While Not adSelect.EOF
        If Trim(adSelect("EST_COD")) = Trim(pvarCodEst) Then
            If adSelect("REL_SUC_CLI") = "S" Then
                wvarRequestCli = "<CLIENTE><TIPDOC>" & pvarTipDoc & "</TIPDOC><NRODOC>" & pvarNrodoc & "</NRODOC></CLIENTE>"
                Set wobjSucCli = mobjCOM_Context.CreateInstance("sacA_Clientes.ObtenerSectorSucCliente")
                wvarStep = 106
                wobjSucCli.Execute wvarRequestCli, wvarResponseCli, ""
                wvarStep = 107
                Set wobjXMLSecCli = CreateObject("MSXML2.DOMDocument")
                With wobjXMLSecCli
                    .async = False
                    wvarStep = 108
                    Call .loadXML(wvarResponseCli)
                    wvarStep = 109
                    If .selectSingleNode("//SECTORCLIENTE/CODIGO").Text <> "" Then
                        wvarStep = 110
                        BuscarSectorDelCliente = Trim(.selectSingleNode("//SECTORCLIENTE/CODIGO").Text)
                    End If
                End With
            End If
            Exit Do
        End If
        adSelect.MoveNext
    Loop
    adSelect.Close

Exit Function
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    App.LogEvent "Error:" & Err.Description

End Function




