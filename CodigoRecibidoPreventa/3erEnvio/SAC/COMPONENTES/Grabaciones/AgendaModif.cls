VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "AgendaModif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName   As String = "sacA_Grabaciones.AgendaModif"
Dim mobjCOM_Context   As ObjectContext
Dim mobjEventLog      As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Const wcteCodProd       As String = "----"
    Const wcteCodNivel      As Long = 1
    Dim wvarStep            As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarTipoDoc                 As String
    Dim wvarNroDoc                  As String
    Dim wvarEmpresa                 As String
    Dim wvarCodCampania             As String
    Dim wvarFechaEntrevistaNueva    As String
    Dim wvarHorarioEntrevistaNueva  As String
    Dim wvarFechaEntrevistaVieja    As String
    Dim wvarHorarioEntrevistaVieja  As String
    Dim wvarCodLugar                As String
    Dim wvarCodDestino              As String
    Dim wvarCodEstado               As String
    Dim wvarCodCanal                As String
    Dim wvarCodSector               As String
    Dim wvarCodEmpresaSucursal      As String
    Dim wvarCodSucursal             As String
    Dim wvarObservacion             As String
    Dim wvarUser                    As String
    Dim wvarGenera                  As Boolean
    Dim wvarTipoDocRVP              As String
    Dim wvarCampoAdic1              As String
    Dim wvarCampoAdic2              As String
    Dim wvarCampoAdic3              As String
    Dim wvarCampoAdic4              As String
    Dim wvarCampoAdic5              As String
    Dim wvarCampoAdic6              As String
    Dim wvarCampoAdic7              As String
    Dim wvarCampoAdic8              As String
    Dim wvarCampoAdic9              As String
    Dim wvarCampoAdic10             As String
    Dim wvarCampoAdic11             As String
    Dim wvarCampoAdic12             As String
    
    Dim wvarHoraServer              As Long
    Dim wvarFechaServer             As Long
    Dim wvarHoraActual              As Long
    Dim wvarFechaActual             As Long
    
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    With wobjXMLrequest
      wvarEmpresa = .selectSingleNode("//GRABACIONES/EMPRESA").Text
      wvarCodCampania = .selectSingleNode("//GRABACIONES/COD_CAMPANIA").Text
      wvarTipoDoc = .selectSingleNode("//GRABACIONES/TIPO_DOC").Text
      wvarNroDoc = .selectSingleNode("//GRABACIONES/NRO_DOC").Text
      wvarFechaEntrevistaNueva = .selectSingleNode("//GRABACIONES/FECHA_NUEVA").Text
      wvarHorarioEntrevistaNueva = .selectSingleNode("//GRABACIONES/HORARIO_NUEVA").Text
      wvarFechaEntrevistaVieja = .selectSingleNode("//GRABACIONES/FECHA_VIEJA").Text
      wvarHorarioEntrevistaVieja = .selectSingleNode("//GRABACIONES/HORARIO_VIEJA").Text
      wvarCodLugar = .selectSingleNode("//GRABACIONES/COD_LUGAR").Text
      wvarCodEstado = .selectSingleNode("//GRABACIONES/COD_ESTADO").Text
      wvarCodDestino = .selectSingleNode("//GRABACIONES/COD_DESTINO").Text
      wvarCodCanal = .selectSingleNode("//GRABACIONES/COD_CANAL").Text
      wvarCodSector = .selectSingleNode("//GRABACIONES/COD_SECTOR").Text
      wvarCodEmpresaSucursal = .selectSingleNode("//GRABACIONES/COD_EMPSUCURSAL").Text
      wvarCodSucursal = .selectSingleNode("//GRABACIONES/COD_SUCURSAL").Text
      wvarObservacion = .selectSingleNode("//GRABACIONES/OBSERVACION").Text
      wvarUser = .selectSingleNode("//GRABACIONES/USUARIO").Text
      'Este parametro debe venir configurado desde el cliente.
      wvarGenera = .selectSingleNode("//GRABACIONES/GENERA_ENTREVISTA").Text
      wvarTipoDocRVP = .selectSingleNode("//GRABACIONES/TIPODOCRVP").Text
      wvarCampoAdic1 = .selectSingleNode("//GRABACIONES/CAMPO1").Text
      wvarCampoAdic2 = .selectSingleNode("//GRABACIONES/CAMPO2").Text
      wvarCampoAdic3 = .selectSingleNode("//GRABACIONES/CAMPO3").Text
      wvarCampoAdic4 = .selectSingleNode("//GRABACIONES/CAMPO4").Text
      wvarCampoAdic5 = .selectSingleNode("//GRABACIONES/CAMPO5").Text
      wvarCampoAdic6 = .selectSingleNode("//GRABACIONES/CAMPO6").Text
      wvarCampoAdic7 = .selectSingleNode("//GRABACIONES/CAMPO7").Text
      wvarCampoAdic8 = .selectSingleNode("//GRABACIONES/CAMPO8").Text
      wvarCampoAdic9 = .selectSingleNode("//GRABACIONES/CAMPO9").Text
      wvarCampoAdic10 = .selectSingleNode("//GRABACIONES/CAMPO10").Text
      wvarCampoAdic11 = .selectSingleNode("//GRABACIONES/CAMPO11").Text
      wvarCampoAdic12 = .selectSingleNode("//GRABACIONES/CAMPO12").Text
      
    End With
    'SE MODIFICA EL TIPODOC PARA EL CASO DE NYL
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      'wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    ElseIf wvarEmpresa = wconstCODIGONYL Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      'wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    End If
    
    'If wvarEmpresa = wconstCODIGONYL Then
    '  wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      'wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    'End If
    '
    wvarHoraServer = Val(Format(Now, "hhmmss"))
    wvarFechaServer = Val(Format(Date, "yyyymmdd"))
    '
    wvarHoraActual = Val(Format(Now, "hhmmss"))
    wvarFechaActual = Val(Format(Date, "yyyymmdd"))
    If Trim(wvarHorarioEntrevistaNueva) <> "" Then
      If Val(wvarFechaEntrevistaNueva) < wvarFechaActual Then
          pvarResponse = "<GRABACIONES><MODIFICAR_AGENDA><ESTADO RESULTADO='FALSE' MENSAJE='La fecha no debe ser inferior al dia actual' /></MODIFICAR_AGENDA></GRABACIONES>"
          Exit Function
      End If
      '
      If Val(wvarHorarioEntrevistaNueva) <= wvarHoraActual And (wvarFechaEntrevistaNueva <= wvarFechaActual) Then
          pvarResponse = "<GRABACIONES><MODIFICAR_AGENDA><ESTADO RESULTADO='FALSE' MENSAJE='La Hora es menor a la actual' /></MODIFICAR_AGENDA></GRABACIONES>"
          Exit Function
      End If
    End If
    'valida la existencia del tipo y nro. de documento
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_age_delete_agendadacontacto"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Campania"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarCodCampania)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
    wobjDBCmd.Parameters.Append wobjDBParm
    'NO SE DEBE COMPARAR EL USUARIO
''    wvarStep = 90
''    Set wobjDBParm = CreateObject("ADODB.Parameter")
''    wobjDBParm.Name = "@Usuario"
''    wobjDBParm.Direction = adParamInput
''     wobjDBParm.Type = adChar
''    wobjDBParm.Size = 10
''    wobjDBParm.Value = wvarUser
''    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@FechaEnt"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = Val(wvarFechaEntrevistaVieja)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@HoraEnt"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = Val(wvarHorarioEntrevistaVieja)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 120
    wobjDBCmd.Execute
    '
    'CAMBIA EL ESTADO DE LA ENTREVISTA VIEJA A 128 -- MODIFICO HORARIO --
    'ACTUALIZA AGE_EVENTOS Y SETEA FECHA Y HORA ACTUAL CON SEGUNDOS
    '****************
    wvarStep = 130
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_AGE_MODIFICO_FECHA"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 140
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMCOD"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarCodCampania)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 150
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TIPDOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 160
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NUMDOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 170
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@FECHA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarFechaEntrevistaVieja)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 180
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@HORA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarHorarioEntrevistaVieja)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 190
    wobjDBCmd.Execute
    '
    '************
    ' GENERA EL ESTADO DE LA ENTREVISTA 127 - CANCELO HORARIO -
    ' GRABA EN AGE_EVENTOS CON FECHA Y HORA CON SEGUNDOS
    '****************
    wvarStep = 200
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "sp_age_insert_agendacanceladae"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 210
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Campania"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = Val(wvarCodCampania)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 220
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 230
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NumDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
    wobjDBCmd.Parameters.Append wobjDBParm
    '
''    wvarStep = 240
''    Set wobjDBParm = CreateObject("ADODB.Parameter")
''    wobjDBParm.Name = "@Fecha"
''    wobjDBParm.Direction = adParamInput
''    wobjDBParm.Type = adInteger
''    wobjDBParm.Value = Val(Format(FechaServer(), "yyyymmdd"))
''    wobjDBCmd.Parameters.Append wobjDBParm
''    '
''    wvarStep = 250
''    Set wobjDBParm = CreateObject("ADODB.Parameter")
''    wobjDBParm.Name = "@Hora"
''    wobjDBParm.Direction = adParamInput
''    wobjDBParm.Type = adInteger
''    wobjDBParm.Value = Val(Format(FechaServer(), "hhnn"))
''    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 260
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Obs"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 255
    wobjDBParm.Value = "Entrevista Cancelada"
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 270
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CodEstEnt"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = 127
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 280
    wobjDBCmd.Execute
    '
    '************
    ' AGREGA LA ENTREVISTA CON EL NUEVO HORARIO
    If (CBool(CStr(wvarGenera))) Then
      wvarStep = 290
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_AGENDA_Insert"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 300
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPANIA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Size = 3
      wobjDBParm.Value = Val(wvarCodCampania)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 310
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TIPODOC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = wvarTipoDoc
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 320
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NUMDOC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 330
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CLI"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 1
      wobjDBParm.Value = "S"
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 340
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@FEC_ENT"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarFechaEntrevistaNueva)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 350
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@HOR_ENT"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarHorarioEntrevistaNueva)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 360
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@LUG_ENT"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarCodLugar)
      wobjDBCmd.Parameters.Append wobjDBParm
       '
      wvarStep = 370
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_EST_ENT"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarCodEstado)
      wobjDBCmd.Parameters.Append wobjDBParm
       '
      wvarStep = 380
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_PRO"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 4
      wobjDBParm.Value = wcteCodProd
      wobjDBCmd.Parameters.Append wobjDBParm
       '
      wvarStep = 390
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_NIVEL"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = 1
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 400
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_DES_PRO"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarCodDestino)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 410
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_CAN"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarCodCanal)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 420
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@OPE"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 430
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@SEC_RES"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 4
      wobjDBParm.Value = wvarCodSector
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 435
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@EMP_SUC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarCodEmpresaSucursal
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 440
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@SUC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 4
      wobjDBParm.Value = wvarCodSucursal
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 450
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@OBS"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 255
      wobjDBParm.Value = Left(wvarObservacion, 255)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 460
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_USU"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 470
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CIERRA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 1
      wobjDBParm.Value = CierraEntrevista(wvarCodEstado, wvarCodCampania)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 480
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@FEC_LLA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = 0
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 490
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@HOR_LLA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = 0
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 500
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@ACTUALIZABASE"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = 0
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 510
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO1"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic1
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 520
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO2"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic2
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 530
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO3"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic3
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 540
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO4"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic4
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 550
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO5"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic5
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 560
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO6"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic6
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 570
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO7"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic7
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 580
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO8"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic8
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 590
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO9"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic9
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 600
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO10"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic10
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 610
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO11"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic11
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 620
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CAMPO12"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 25
      wobjDBParm.Value = wvarCampoAdic12
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 630
      wobjDBCmd.Execute
      '
    Else
      wvarStep = 640
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "sp_Age_UpdateVentas"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 650
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Campania"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = Val(wvarCodCampania)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 660
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TIP_DOC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = wvarTipoDoc
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 670
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NUM_DOC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 680
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@OPE"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 690
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@OBS"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 255
      wobjDBParm.Value = Left(wvarObservacion, 255)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 700
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_USU"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 710
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CIERRA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 1
      wobjDBParm.Value = "S"
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 720
      wobjDBCmd.Execute
      '
    End If
    wvarStep = 730
    wobjDBCnn.Close
    '
    wvarStep = 740
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 750
    '
    pvarResponse = pvarResponse & "<GRABACIONES><MODIFICAR_AGENDA><ESTADO RESULTADO='TRUE' MENSAJE='' /></MODIFICAR_AGENDA></GRABACIONES>"
    '
    wvarStep = 760
    mobjCOM_Context.SetComplete
    Exit Function
    '
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></GRABACIONES>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function

Private Function CierraEntrevista(pvarEstado, pvarCampania)

Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
Dim wobjDBCnn           As ADODB.Connection
Dim wobjDBCmd           As ADODB.Command
Dim wobjDBParm          As ADODB.Parameter
Dim wrstAgendaModif     As ADODB.Recordset
Dim wvarStep            As Long
'
wvarStep = 10
Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
'
wvarStep = 20
Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
'
wvarStep = 30
Set wobjDBCmd = CreateObject("ADODB.Command")
Set wobjDBCmd.ActiveConnection = wobjDBCnn
wobjDBCmd.CommandText = "sp_age_CierraEntrevista"
wobjDBCmd.CommandType = adCmdStoredProc
'
wvarStep = 40
Set wobjDBParm = CreateObject("ADODB.Parameter")
wobjDBParm.Name = "@Estado"
wobjDBParm.Direction = adParamInput
wobjDBParm.Type = adInteger
'wobjDBParm.Size = 3
wobjDBParm.Value = Val(pvarEstado)
wobjDBCmd.Parameters.Append wobjDBParm
'
wvarStep = 50
Set wobjDBParm = CreateObject("ADODB.Parameter")
wobjDBParm.Name = "@Campania"
wobjDBParm.Direction = adParamInput
wobjDBParm.Type = adInteger
'wobjDBParm.Size = 3
wobjDBParm.Value = Val(pvarCampania)
wobjDBCmd.Parameters.Append wobjDBParm
'
wvarStep = 60
Set wrstAgendaModif = wobjDBCmd.Execute
Set wrstAgendaModif.ActiveConnection = Nothing
'
If Not wrstAgendaModif.EOF Then
  CierraEntrevista = Trim(wrstAgendaModif.Fields("AGE_CIE").Value & "")
End If
'
wvarStep = 70
wobjDBCnn.Close
'
wvarStep = 80
Set wobjDBCmd = Nothing
Set wobjDBCnn = Nothing
Set wobjHSBC_DBCnn = Nothing
'
wvarStep = 90
If wrstAgendaModif.State <> 0 Then wrstAgendaModif.Close
'
wvarStep = 100
Set wrstAgendaModif = Nothing
'
End Function

