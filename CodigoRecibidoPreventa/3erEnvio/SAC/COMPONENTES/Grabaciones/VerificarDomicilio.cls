VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "VerificarDomicilio"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.VerificarDomicilio"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
Const wcteFnName        As String = "IAction_Execute"
Dim wvarStep            As Long
'
Dim wobjXMLrequest      As MSXML2.DOMDocument
Dim wobjNoCliente       As IAction
'
Dim wvarEmpresa          As String
Dim wvarTipoDoc          As String
Dim wvarNroDoc           As String
Dim wvarCodOperacion     As String
Dim wvarOperadora        As String
Dim wvarTipoDocRVP       As String
Dim wvarRazonSocial      As String
Dim wvarCalle            As String
Dim wvarLocalidad        As String
Dim wvarNacionalidad     As String
Dim wvarFechaNacimiento  As String
Dim wvarCodigoPostal     As String
Dim wvarTelefono         As String
Dim wvarRequestNoCliente As String
Dim wvarResNoCliente     As String
Dim wvarCategoria        As String
'
Dim wvarActionNoCliente  As Long
'
Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
Dim wobjDBCnn           As ADODB.Connection
Dim wobjDBCmd           As ADODB.Command
Dim wobjDBParm          As ADODB.Parameter
Dim wrstClientes      As ADODB.Recordset
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~
On Error GoTo ErrorHandler
'~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'
    wvarStep = 10
    pvarRequest = stringToXML(pvarRequest)
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    With wobjXMLrequest
      wvarEmpresa = .selectSingleNode("//VERIFDOMICILIO/EMPRESA").Text
      wvarTipoDoc = Trim(UCase(.selectSingleNode("//VERIFDOMICILIO/TIPO_DOC").Text))
      wvarNroDoc = .selectSingleNode("//VERIFDOMICILIO/NRO_DOC").Text
      wvarCodOperacion = .selectSingleNode("//VERIFDOMICILIO/CODOP").Text
      wvarOperadora = .selectSingleNode("//VERIFDOMICILIO/USUARIO").Text
      wvarTipoDocRVP = .selectSingleNode("//VERIFDOMICILIO/TIPODOCRVP").Text
      wvarCategoria = .selectSingleNode("//VERIFDOMICILIO/CATEGORIA").Text
    End With
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/TIPO_DOC").Text = wvarTipoDoc
    ElseIf (wvarEmpresa = wconstCODIGONYL) Or (wvarEmpresa = wconstCODIGOLBA) Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/TIPO_DOC").Text = wvarTipoDoc
    End If
    'REGISTRACION DEL NOCLIENTE
    wvarRequestNoCliente = Replace(wobjXMLrequest.xml, "VERIFDOMICILIO", "GRABACIONES")
    'SOLO SE GRABA LA CATEGORIA PARA HSBC SALUD
    wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/CATEGORIA").Text = IIf(wvarEmpresa = wconstCODIGODOCTHOS, Left(wvarCategoria & " ", 1), "")
    wvarStep = 25
    Set wobjNoCliente = mobjCOM_Context.CreateInstance("sacA_Grabaciones.NoClientes")
    wvarActionNoCliente = wobjNoCliente.Execute(wvarRequestNoCliente, wvarResNoCliente, pvarContextInfo)
    Set wobjNoCliente = Nothing
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_ACTVERIFDOMICILIO"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Documento"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = NumFilled(wvarNroDoc, 15)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@codOp"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarCodOperacion
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@sesion"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = 0
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Operadora"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 10
    wobjDBParm.Value = wvarOperadora
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 120
    Call wobjDBCmd.Execute
    '
    pvarResponse = "<VERIFDOMICILIO><ESTADO RESULTADO='TRUE' MENSAJE='' /></VERIFDOMICILIO>"
    '
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 130
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<VERIFDOMICILIO><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></VERIFDOMICILIO>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
'    App.LogEvent pvarRequest
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function


