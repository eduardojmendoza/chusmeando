VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "NoClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.NoClientes"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
Const wcteFnName        As String = "IAction_Execute"
Dim wvarStep            As Long
'
Dim wobjXMLrequest      As MSXML2.DOMDocument
'
Dim wvarEmpresa         As String
Dim wvarTipoDoc         As String
Dim wvarNroDoc          As String
Dim wvarRazonSocial     As String
Dim wvarCalle           As String
Dim wvarLocalidad       As String
Dim wvarNacionalidad    As String
Dim wvarFechaNacimiento As String
Dim wvarCodPostal       As String
Dim wvarTelefono        As String
Dim wvarNuevoDoc        As String
Dim wvarInsertarNOCLIENTE As Boolean
Dim wvarActualizarNOCLIENTE As Boolean
Dim wvarCategoria       As String
'
Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
Dim wobjDBCnn           As ADODB.Connection
Dim wobjDBCmd           As ADODB.Command
Dim wobjDBParm          As ADODB.Parameter
Dim wrstClientes      As ADODB.Recordset
'
'~~~~~~~~~~~~~~~~~~~~~~~~~~~
On Error GoTo ErrorHandler
'~~~~~~~~~~~~~~~~~~~~~~~~~~~
'
'
    wvarStep = 10
    pvarRequest = stringToXML(pvarRequest)
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    With wobjXMLrequest
      wvarEmpresa = .selectSingleNode("//GRABACIONES/EMPRESA").Text
      wvarTipoDoc = Trim(UCase(.selectSingleNode("//GRABACIONES/TIPO_DOC").Text))
      wvarNroDoc = .selectSingleNode("//GRABACIONES/NRO_DOC").Text
      wvarRazonSocial = sacarCDATA(.selectSingleNode("//GRABACIONES/RAZON_SOCIAL").Text)
      wvarCalle = sacarCDATA(.selectSingleNode("//GRABACIONES/CALLE").Text)
      wvarLocalidad = sacarCDATA(.selectSingleNode("//GRABACIONES/LOCALIDAD").Text)
      wvarNacionalidad = sacarCDATA(.selectSingleNode("//GRABACIONES/NACIONALIDAD").Text)
      wvarFechaNacimiento = .selectSingleNode("//GRABACIONES/FECHA_NAC").Text
      wvarCodPostal = .selectSingleNode("//GRABACIONES/COD_POSTAL").Text
      wvarTelefono = .selectSingleNode("//GRABACIONES/TELEFONO").Text
      wvarCategoria = .selectSingleNode("//GRABACIONES/CATEGORIA").Text
    End With
    If Trim(wvarFechaNacimiento) <> "" Then
      wvarFechaNacimiento = Left(wvarFechaNacimiento, 4) & "/" & Mid(wvarFechaNacimiento, 5, 2) & "/" & Right(wvarFechaNacimiento, 2)
    Else
      wvarFechaNacimiento = "1950/01/01"
    End If
    '
    wvarInsertarNOCLIENTE = False
    wvarActualizarNOCLIENTE = False
    '
    'valida la existencia del tipo y nro. de documento
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    If (wvarEmpresa = wconstCODIGODOCTHOS) Or (wvarEmpresa = wconstCODIGONYL) Or (wvarEmpresa = wconstCODIGOLBA) Or (wvarEmpresa = wconstCODIGOMAXIMA) Or (wvarEmpresa = wconstCODIGOSACINT) Or (wvarEmpresa = wconstCODIGOSACCOMPRAS) Then
      'ACA SE CHEQUEA SOLO CONTRA LA BASE DE NOCLIENTES YA QUE EN ESTE
      'CASO SI IMPORTA LA EMPRESA A LA QUE PERTENECE
      wvarStep = 50
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_CONSULTARNOCLIENTE"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 60
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TipoDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = wvarTipoDoc
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 70
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Doc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarNroDoc, 15)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 80
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Empresa"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 90
      Set wrstClientes = wobjDBCmd.Execute
      Set wrstClientes.ActiveConnection = Nothing
      '
      wvarStep = 100
      wobjDBCnn.Close
      '
      If wrstClientes.EOF Then
        wvarInsertarNOCLIENTE = True
      Else 'EL CLIENTE YA EXISTE
        wvarActualizarNOCLIENTE = True
      End If
    ElseIf (wvarEmpresa = wconstCODIGOBANCO Or wvarEmpresa = wconstCODIGOHEXAGON) Then
      'ACA SE CUEQUEA CONTRA LA BASE DE CLIENTES DEL BANCO PARA LO
      'CUAL NO ES NECESARIO LA EMPRESA
      wvarStep = 110
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_CONSULTARCLIENTE"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 120
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TipoDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = wvarTipoDoc
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 130
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Doc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarNroDoc, 15)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 140
      Set wrstClientes = wobjDBCmd.Execute
      Set wrstClientes.ActiveConnection = Nothing
      '
      If wrstClientes.EOF Then
        'AHORA CHEQUEA QUE NO EXISTA EL NOCLIENTE
        wvarStep = 150
        Set wobjDBCmd = CreateObject("ADODB.Command")
        Set wobjDBCmd.ActiveConnection = wobjDBCnn
        wobjDBCmd.CommandText = "SP_CONSULTARNOCLIENTE"
        wobjDBCmd.CommandType = adCmdStoredProc
        '
        wvarStep = 160
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@TipoDoc"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adChar
        wobjDBParm.Size = 3
        wobjDBParm.Value = wvarTipoDoc
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 170
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Doc"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adVarChar
        wobjDBParm.Size = 15
        wobjDBParm.Value = NumFilled(wvarNroDoc, 15)
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 180
        Set wobjDBParm = CreateObject("ADODB.Parameter")
        wobjDBParm.Name = "@Empresa"
        wobjDBParm.Direction = adParamInput
        wobjDBParm.Type = adInteger
        wobjDBParm.Value = wvarEmpresa
        wobjDBCmd.Parameters.Append wobjDBParm
        '
        wvarStep = 190
        Set wrstClientes = wobjDBCmd.Execute
        Set wrstClientes.ActiveConnection = Nothing
        '
        wvarStep = 200
        wobjDBCnn.Close
        '
        If wrstClientes.EOF Then
          wvarInsertarNOCLIENTE = True
        End If
      End If
    End If
    '
    If wvarInsertarNOCLIENTE Then
      'INSERTA EL NUEVO NOCLIENTE
      wvarStep = 210
      Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
      '
      wvarStep = 220
      Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
      '
      wvarStep = 230
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "sp_alta_nocliente"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 240
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Clave"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 4
      wobjDBParm.Value = vbNull
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 250
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@EMPRESA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 260
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TipoDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 4
      If UCase(Trim(wvarTipoDoc & "")) = "CUIT - SOC" Then
        wobjDBParm.Value = "SOC"
      Else
        wobjDBParm.Value = wvarTipoDoc
      End If
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 270
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NumDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 280
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@RazonSocial"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 30
      wobjDBParm.Value = Left(Trim(wvarRazonSocial & ""), 30)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 290
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Nacionalidad"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = Left(Trim(wvarNacionalidad & ""), 3)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 300
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@FechaNacimiento"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adDate
      'wobjDBParm.Size = 15
      wobjDBParm.Value = wvarFechaNacimiento
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 310
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@DomiPart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 50
      wobjDBParm.Value = Left(Trim(wvarCalle & ""), 50)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 320
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CPPart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 5
      wobjDBParm.Value = Left(Trim(wvarCodPostal & ""), 5)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 330
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@LocaPart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 35
      wobjDBParm.Value = Left(Trim(wvarLocalidad & ""), 35)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 340
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TelePart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 16
      wobjDBParm.Value = Left(Trim(wvarTelefono & ""), 16)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 345
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NC_CATEGORIA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 1
      wobjDBParm.Value = Left(Trim(wvarCategoria), 1)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 350
      
      If Trim(UCase(wvarTipoDoc)) <> "DES" Then
          Call wobjDBCmd.Execute
          '
          '*********************************************************************
          pvarResponse = "<GRABACIONES>"
          pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='Los datos han sido correctamente registrados' NUEVO_DOC='' ACTUALIZACION='FALSE' INSERCION='TRUE'/>"
          pvarResponse = pvarResponse & "</GRABACIONES>"
          '*********************************************************************
      Else
          Set wrstClientes = wobjDBCmd.Execute
          wvarNuevoDoc = wrstClientes.Fields("NroDoc").Value
          wrstClientes.Close
          Set wrstClientes = Nothing
          '
          '*********************************************************************
          pvarResponse = "<GRABACIONES>"
          pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' NUEVO_DOC='" & wvarNuevoDoc
          pvarResponse = pvarResponse & "' MENSAJE='Los datos han sido correctamente registrados' ACTUALIZACION='FALSE' INSERCION='TRUE' />"
          pvarResponse = pvarResponse & "</GRABACIONES>"
          '*********************************************************************
      End If
      '
      wvarStep = 360
    ElseIf wvarActualizarNOCLIENTE Then
      'SE ACTUALIZAN LOS DATOS DE NOCLIENTESROB
      wvarStep = 370
      Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
      '
      wvarStep = 380
      Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
      '
      wvarStep = 390
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_MODIFICA_NOCLIENTE"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 400
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Clave"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 4
      wobjDBParm.Value = vbNull
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 410
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@EMPRESA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 420
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TipoDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 4
      If UCase(Trim(wvarTipoDoc & "")) = "CUIT - SOC" Then
        wobjDBParm.Value = "SOC"
      Else
        wobjDBParm.Value = wvarTipoDoc
      End If
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 430
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NumDoc"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adChar
      wobjDBParm.Size = 15
      wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 440
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@RazonSocial"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 30
      wobjDBParm.Value = Left(Trim(wvarRazonSocial & ""), 30)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 450
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Nacionalidad"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 3
      wobjDBParm.Value = Left(Trim(wvarNacionalidad & ""), 3)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 460
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@FechaNacimiento"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adDate
      'wobjDBParm.Size = 15
      wobjDBParm.Value = wvarFechaNacimiento
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 470
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@DomiPart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 50
      wobjDBParm.Value = Left(Trim(wvarCalle & ""), 50)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 480
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@CPPart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 5
      wobjDBParm.Value = Left(Trim(wvarCodPostal & ""), 5)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 490
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@LocaPart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 35
      wobjDBParm.Value = Left(Trim(wvarLocalidad & ""), 35)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 500
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@TelePart"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 16
      wobjDBParm.Value = Left(Trim(wvarTelefono & ""), 16)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 505
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@NC_CATEGORIA"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 1
      wobjDBParm.Value = Left(Trim(wvarCategoria), 1)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 510
      Call wobjDBCmd.Execute
      '
      pvarResponse = "<GRABACIONES>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='Los datos han sido correctamente registrados' NUEVO_DOC='' ACTUALIZACION='TRUE' INSERCION='FALSE' />"
      pvarResponse = pvarResponse & "</GRABACIONES>"
    Else
      pvarResponse = "<GRABACIONES>"
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='Los datos han sido correctamente registrados' NUEVO_DOC='' ACTUALIZACION='FALSE' INSERCION='FALSE'/>"
      pvarResponse = pvarResponse & "</GRABACIONES>"
    End If
    '
    wvarStep = 520
    '
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 530
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></GRABACIONES>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function
