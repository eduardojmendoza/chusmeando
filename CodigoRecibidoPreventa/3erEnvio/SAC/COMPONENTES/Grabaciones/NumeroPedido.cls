VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "NumeroPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.NumeroPedido"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    Dim wrstReclamo         As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    'insertar el No Cliente
    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_NumeroPedido"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 40
    Set wrstReclamo = wobjDBCmd.Execute
    Set wrstReclamo.ActiveConnection = Nothing
    '
    If wrstReclamo.EOF Then
        pvarResponse = "<NUEVORECLAMO>"
        pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='Se ha producido un error. No se pudo generar un nuevo numero de reclamo' />"
        pvarResponse = pvarResponse & "</NUEVORECLAMO>"
    Else
        pvarResponse = "<NUEVORECLAMO>"
        pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
        pvarResponse = pvarResponse & "<RECLAMO NUMERO='" & wrstReclamo.Fields("NPE_NUM_PED").Value & "' />"
        pvarResponse = pvarResponse & "</NUEVORECLAMO>"
    End If

    wvarStep = 50
    wobjDBCnn.Close
    '
    wvarStep = 60
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 70
    If wrstReclamo.State <> 0 Then wrstReclamo.Close
    '
    wvarStep = 80
    Set wrstReclamo = Nothing
    '
    wvarStep = 90
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<NUEVORECLAMO><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></NUEVORECLAMO>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function








