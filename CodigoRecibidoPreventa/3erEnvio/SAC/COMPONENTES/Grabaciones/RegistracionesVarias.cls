VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "RegistracionesVarias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.RegistracionesVarias"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest      As MSXML2.DOMDocument
    '
    Dim wvarEmpresa         As String
    Dim wvarCodRegistracion As String
    Dim wvarUsuario         As String
    Dim wvarFechaServer     As String
    Dim wvarOperaciones     As String
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wobjDBParm          As ADODB.Parameter
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    'App.LogEvent pvarRequest
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    'App.LogEvent wobjXMLrequest.xml
    '
    wvarStep = 20
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//GRABACIONES/EMPRESA").Text
    wvarCodRegistracion = wobjXMLrequest.selectSingleNode("//GRABACIONES/CODIGOREGISTRACION").Text
    wvarUsuario = wobjXMLrequest.selectSingleNode("//GRABACIONES/USUARIO").Text
    wvarFechaServer = wobjXMLrequest.selectSingleNode("//GRABACIONES/FECHASERVER").Text
    wvarOperaciones = wobjXMLrequest.selectSingleNode("//GRABACIONES/OBSERVACIONES").Text
    '
    pvarResponse = "<GRABACIONES>"
    If Trim(wvarCodRegistracion) = "" Then
      '
      pvarResponse = pvarResponse & _
                     "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido obtener correctamente el n�mero de pedido' />"
      '
    ElseIf Trim(wvarUsuario) = "" Then
      '
      pvarResponse = pvarResponse & _
                     "<ESTADO RESULTADO='FALSE' MENSAJE='No se obtuvo el usuario registrador' />"
      '
    ElseIf Trim(wvarFechaServer) = "" Then
      '
      pvarResponse = pvarResponse & _
                     "<ESTADO RESULTADO='FALSE' MENSAJE='No se obtuvo la fecha para la registraci�n' />"
      '
    Else
      '
      wvarStep = 30
      Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
      '
      wvarStep = 40
      Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
      wobjDBCnn.Execute "SET NOCOUNT ON"
      '
      wvarStep = 50
      Set wobjDBCmd = CreateObject("ADODB.Command")
      Set wobjDBCmd.ActiveConnection = wobjDBCnn
      wobjDBCmd.CommandText = "SP_RegVarias_Insert"
      wobjDBCmd.CommandType = adCmdStoredProc
      '
      wvarStep = 60
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@Empresa"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarEmpresa
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 70
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_REG"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adDouble
      wobjDBParm.Value = Val(wvarCodRegistracion)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 80
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@COD_USU"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 10
      wobjDBParm.Value = wvarUsuario
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 90
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@FEC"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adInteger
      wobjDBParm.Value = wvarFechaServer
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 100
      Set wobjDBParm = CreateObject("ADODB.Parameter")
      wobjDBParm.Name = "@OBS"
      wobjDBParm.Direction = adParamInput
      wobjDBParm.Type = adVarChar
      wobjDBParm.Size = 255
      wobjDBParm.Value = noNull(wvarOperaciones)
      wobjDBCmd.Parameters.Append wobjDBParm
      '
      wvarStep = 110
      Call wobjDBCmd.Execute
      '
      pvarResponse = pvarResponse & _
                     "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      '
      wvarStep = 120
      '
      wobjDBCnn.Close
      Set wobjDBCmd = Nothing
      Set wobjDBCnn = Nothing
      Set wobjHSBC_DBCnn = Nothing
      '
      wvarStep = 130
      '
    End If
    pvarResponse = pvarResponse & _
                   "</GRABACIONES>"
    wvarStep = 140
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></GRABACIONES>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function


