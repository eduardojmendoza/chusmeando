VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 2  'RequiresTransaction
END
Attribute VB_Name = "Agenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Grabaciones.Agenda"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName                As String = "IAction_Execute"
    Const wcteCodNivel              As Long = 1
    Dim wvarStep                    As Long
    '
    Dim wobjXMLrequest              As MSXML2.DOMDocument
    '
    Dim wvarRegistraCliente         As Boolean
    Dim wvarEmpresa                 As String
    Dim wvarFechaDeAgenda           As String
    Dim wvarHora                    As String
    Dim wvarCierra                  As String
    Dim wvarTipoDoc                 As String
    Dim wvarNroDoc                  As String
    Dim wvarCodCampania             As String
    Dim wvarCodLugar                As String
    Dim wvarCodDestino              As String
    Dim wvarCodCanal                As String
    Dim wvarCodEntrevista           As String
    Dim wvarCodSector               As String
    Dim wvarCodEmpresaSucursal      As String
    Dim wvarCodSucursal             As String
    Dim wvarObservacion             As String
    Dim wvarUser                    As String
    Dim wvarActualizaBase           As Boolean
    Dim wvarProducto                As String
    Dim wvarRequestCliente          As String
    Dim wvarTipoDocRVP              As String
    Dim wvarCampoAdic1              As String
    Dim wvarCampoAdic2              As String
    Dim wvarCampoAdic3              As String
    Dim wvarCampoAdic4              As String
    Dim wvarCampoAdic5              As String
    Dim wvarCampoAdic6              As String
    Dim wvarCampoAdic7              As String
    Dim wvarCampoAdic8              As String
    Dim wvarCampoAdic9              As String
    Dim wvarCampoAdic10             As String
    Dim wvarCampoAdic11             As String
    Dim wvarCampoAdic12             As String
    
    Dim wobjNoCliente               As IAction
    Dim wvarActionNoCliente         As Long
    Dim wvarResponseNoCliente       As String
    
    Dim wvarHoraActual              As Long
    Dim wvarFechaActual             As Long
    
    Dim wvarHoraServer              As Long
    Dim wvarFechaServer             As Long
    
    Dim wobjHSBC_DBCnn              As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn                   As ADODB.Connection
    Dim wobjDBCmd                   As ADODB.Command
    Dim wobjDBParm                  As ADODB.Parameter
    Dim wrstNoClientes              As ADODB.Recordset
    '
    Dim wobjTime                    As HSBCInterfaces.ICmdContext
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    With wobjXMLrequest
        wvarRegistraCliente = .selectSingleNode("//AGREGAR_AGENDA/REGISTRACLIENTE").Text
        wvarEmpresa = .selectSingleNode("//AGREGAR_AGENDA/EMPRESA").Text
        wvarFechaDeAgenda = .selectSingleNode("//AGREGAR_AGENDA/FECHAAGENDA").Text
        wvarHora = .selectSingleNode("//AGREGAR_AGENDA/HORAAGENDA").Text
        wvarCierra = .selectSingleNode("//AGREGAR_AGENDA/CIERRA").Text
        'Este valor sale luego de llamar a la clase listaEstados
        'deberia traer S o N. Si esta vacio deberia devolver N tambien
        '
        wvarTipoDoc = .selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text
        wvarNroDoc = .selectSingleNode("//AGREGAR_AGENDA/NRO_DOC").Text
        wvarCodCampania = .selectSingleNode("//AGREGAR_AGENDA/COD_CAMPANIA").Text
        wvarCodLugar = .selectSingleNode("//AGREGAR_AGENDA/COD_LUGAR").Text
        wvarCodDestino = .selectSingleNode("//AGREGAR_AGENDA/COD_DESTINO").Text
        wvarCodCanal = .selectSingleNode("//AGREGAR_AGENDA/COD_CANAL").Text
        wvarCodEntrevista = .selectSingleNode("//AGREGAR_AGENDA/COD_ENTREVISTA").Text
        wvarCodSector = .selectSingleNode("//AGREGAR_AGENDA/COD_SECTOR").Text
        wvarCodEmpresaSucursal = .selectSingleNode("//AGREGAR_AGENDA/COD_EMPSUCURSAL").Text
        wvarCodSucursal = .selectSingleNode("//AGREGAR_AGENDA/COD_SUCURSAL").Text
        wvarObservacion = .selectSingleNode("//AGREGAR_AGENDA/OBSERVACION").Text
        wvarUser = .selectSingleNode("//AGREGAR_AGENDA/USUARIO").Text
        '
        'Este parametro debe venir configurado desde el cliente.
        'Luego de llamar a la clase si el metodo
        ' chequeaAgendaCliente no trae datos debe pasar el valor false
        ' caso contrario true
        '
        wvarActualizaBase = .selectSingleNode("//AGREGAR_AGENDA/ACTUALIZABASE").Text
        wvarProducto = .selectSingleNode("//AGREGAR_AGENDA/PRODUCTO").Text
        wvarTipoDocRVP = .selectSingleNode("//AGREGAR_AGENDA/TIPODOCRVP").Text
        wvarCampoAdic1 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO1").Text
        wvarCampoAdic2 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO2").Text
        wvarCampoAdic3 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO3").Text
        wvarCampoAdic4 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO4").Text
        wvarCampoAdic5 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO5").Text
        wvarCampoAdic6 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO6").Text
        wvarCampoAdic7 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO7").Text
        wvarCampoAdic8 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO8").Text
        wvarCampoAdic9 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO9").Text
        wvarCampoAdic10 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO10").Text
        wvarCampoAdic11 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO11").Text
        wvarCampoAdic12 = .selectSingleNode("//AGREGAR_AGENDA/CAMPO12").Text
    End With
    
    If Trim(wvarCodSucursal) = "" Then
        wvarHoraServer = Val(Format(Now, "hhmmss"))
        wvarFechaServer = Val(Format(Date, "yyyymmdd"))
    Else
        wvarHoraServer = 0
        wvarFechaServer = 0
    End If
    
    wvarHoraActual = Val(Format(Now, "hhmmss"))
    wvarFechaActual = Val(Format(Date, "yyyymmdd"))
    
    If Trim(wvarHora) <> "" Then
      If Val(wvarFechaDeAgenda) < wvarFechaActual Then
          pvarResponse = "<GRABACIONES><AGREGAR_AGENDA><ESTADO RESULTADO='FALSE' MENSAJE='La fecha no debe ser inferior al dia actual' /></AGREGAR_AGENDA></GRABACIONES>"
          Exit Function
      End If
      '
      If Val(wvarHora) <= wvarHoraActual And (wvarFechaDeAgenda <= wvarFechaActual) Then
          pvarResponse = "<GRABACIONES><AGREGAR_AGENDA><ESTADO RESULTADO='FALSE' MENSAJE='La Hora es menor a la actual' /></AGREGAR_AGENDA></GRABACIONES>"
          Exit Function
      End If
    End If
    
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    ElseIf wvarEmpresa = wconstCODIGONYL Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
      wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    End If
            
    'If wvarEmpresa = wconstCODIGONYL Then
    '  wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
    '  wobjXMLrequest.selectSingleNode("//AGREGAR_AGENDA/TIPO_DOC").Text = wvarTipoDoc
    'End If
    
    'SE DA EL ALTA O ACTUALIZA EL NOCLIENTE PARA LOS CASOS DE NYL, DOCTHOS Y MAXIMA
    If wvarEmpresa = wconstCODIGONYL Or wvarEmpresa = wconstCODIGODOCTHOS Or wvarEmpresa = wconstCODIGOMAXIMA Then
      wvarStep = 30
      wvarRequestCliente = Replace(wobjXMLrequest.xml, "AGREGAR_AGENDA", "GRABACIONES")
      '
      wvarStep = 40
      Set wobjNoCliente = mobjCOM_Context.CreateInstance("sacA_Grabaciones.NoClientes")
      wvarActionNoCliente = wobjNoCliente.Execute(wvarRequestCliente, wvarResponseNoCliente, pvarContextInfo)
      Set wobjNoCliente = Nothing
    End If
    
    wvarStep = 50
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnectionTrx")
    '
    wvarStep = 60
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 70
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_AGENDA_Insert"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPANIA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarCodCampania
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TIPODOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 100
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NUMDOC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = NumFilled(wvarNroDoc, LONG_DOC(True))
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 110
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CLI"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = IIf(wvarRegistraCliente, "S", "N")
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 120
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@FEC_ENT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = Val(wvarFechaDeAgenda)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 130
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@HOR_ENT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = Val(wvarHora)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 140
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@LUG_ENT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = Val(wvarCodLugar)
    wobjDBCmd.Parameters.Append wobjDBParm
     '
    wvarStep = 150
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_EST_ENT"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCodEntrevista
    wobjDBCmd.Parameters.Append wobjDBParm
     '
    wvarStep = 160
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_PRO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarProducto
    wobjDBCmd.Parameters.Append wobjDBParm
     '
    wvarStep = 170
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_NIVEL"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = wcteCodNivel
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 180
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_DES_PRO"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCodDestino
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 190
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_CAN"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = Val(wvarCodCanal)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 200
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@OPE"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 10
    wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 210
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@SEC_RES"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodSector
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 220
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@EMP_SUC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarCodEmpresaSucursal
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 225
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@SUC"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 4
    wobjDBParm.Value = wvarCodSucursal
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 230
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@OBS"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 255
    wobjDBParm.Value = wvarObservacion
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 240
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@COD_USU"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 10
    wobjDBParm.Value = Left(Trim(getUsuario(wvarUser)), 10)
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 250
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CIERRA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 1
    wobjDBParm.Value = wvarCierra
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 260
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@FEC_LLA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = wvarFechaServer
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 270
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@HOR_LLA"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = wvarHoraServer
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 280
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@ACTUALIZABASE"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    'wobjDBParm.Size = 1
    wobjDBParm.Value = wvarActualizaBase
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 290
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO1"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic1
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 300
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO2"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic2
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 310
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO3"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic3
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 320
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO4"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic4
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 330
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO5"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic5
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 340
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO6"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic6
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 350
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO7"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic7
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 360
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO8"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic8
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 370
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO9"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic9
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 380
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO10"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic10
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 390
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO11"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic11
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 400
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@CAMPO12"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 25
    wobjDBParm.Value = wvarCampoAdic12
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 410
    wobjDBCmd.Execute
    '
    wvarStep = 420
    wobjDBCnn.Close
    '
    wvarStep = 430
    Set wobjDBCmd = Nothing
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 440
    '
    mobjCOM_Context.SetComplete
    '
    pvarResponse = "<GRABACIONES><AGREGAR_AGENDA><ESTADO RESULTADO='TRUE' MENSAJE='' /></AGREGAR_AGENDA></GRABACIONES>"
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    pvarResponse = "<GRABACIONES><ESTADO RESULTADO='FALSE' MENSAJE='Step : " & wvarStep & " - " & Err.Description & "' /></GRABACIONES>"
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
    Else
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    mobjCOM_Context.SetAbort
End Function








