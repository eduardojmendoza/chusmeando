--------------------------------------------------
Componente 
	sacA_Grabaciones
	Clase 
		NoClientes
--------------------------------------------------
REQUEST String

<GRABACIONES>
	<EMPRESA />
	<TIPODOCUMENTO />
	<NRODOCUMENTO />
	<RAZONSOCIAL />
	<CALLE />
	<LOCALIDAD />
	<NACIONALIDAD />
	<FECHANACIMIENTO />
	<CODIGOPOSTAL />
	<TELEFONO />
</GRABACIONES>

RESPONSE String

<GRABACIONES>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
</GRABACIONES>
--------------------------------------------------
Componente 
	sacA_Grabaciones
	Clase 
		RegistracionesVarias
--------------------------------------------------
REQUEST String

<GRABACIONES>
	<CODIGOREGISTRACION />
 	<USUARIO />
    	<FECHASERVER />
	<OBSERVACIONES />
</GRABACIONES>

RESPONSE String

<GRABACIONES>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
</GRABACIONES>
--------------------------------------------------
Componente 
	sacA_Grabaciones
	Clase 
		Registracion
--------------------------------------------------
REQUEST String
<GRABACIONES>
	<REGISTRA_CLIENTE />
        <NRO_PEDIDO />
        <USER />
        <SUCURSAL />
         <SECTOR />
        <MODOCONTACTO />
        <TIPOCONTACTO />
        <TIPO_DOC />
        <NRO_DOC />
        <COD_PRODUCTO />
        <COD_MOTIVO />
        <ESTADO />
        <COD_CAUSA_CONTACTO />
        <OBSERVACIONES />
</GRABACIONES>

RESPONSE String

<GRABACIONES>
	<REGISTRACION>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	</REGISTRACION>
</GRABACIONES>
--------------------------------------------------
Componente 
	sacA_Grabaciones
	Clase 
		Registraciones
--------------------------------------------------
REQUEST String
<REGISTRACION>
	<REGISTRACLIENTE />
        <NRO_PEDIDO />
        <USER />
        <SUCURSAL />
        <SECTOR />
        <MODOCONTACTO />
        <TIPOCONTACTO />
        <TIPO_DOC />
        <NRO_DOC />
        <COD_PRODUCTO />
        <COD_MOTIVO />
        <ESTADO />
        <COD_CAUSA_CONTACTO />
        <OBSERVACIONES />
        <RAZON_SOCIAL />
        <CALLE />
        <LOCALIDAD />
        <NACIONALIDAD />
        <FECHA_NAC />
        <COD_POSTAL />
        <TELEFONO />     
</REGISTRACION>

RESPONSE String

<GRABACIONES>
	<REGISTRACION>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	</REGISTRACION>
</GRABACIONES>
--------------------------------------------------
Componente 
	sacA_Grabaciones
	Clase 
		AgendaModif
--------------------------------------------------
REQUEST String
<GRABACIONES>
	<COD_CAMPANIA />
        <TIPO_DOC />
        <NRO_DOC />
        <FECHA_NUEVA />
        <HORARIO_NUEVA />
        <FECHA_VIEJA />
        <HORARIO_VIEJA />
        <COD_LUGAR />
        <COD_ESTADO />
        <COD_DESTINO />
        <COD_CANAL />
        <COD_SECTOR />
        <COD_SUCURSAL />
        <OBSERVACION />
        <USUARIO />
        <GENERA_ENTREVISTA />
</GRABACIONES>
RESPONSE String

<GRABACIONES>
	<MODIFICAR_AGENDA>
	<ESTADO RESULTADO='TRUE' MENSAJE='' />
	</MODIFICAR_AGENDA>
</GRABACIONES>

--------------------------------------------------
Componente 
	sacA_Grabaciones
	Clase 
		AgendaNoCliente
--------------------------------------------------
REQUEST String
<AGREGAR_AGENDA>
	<EXISTE_CLIENTE>
	<EMPRESA>
	<TIPO_DOC>
	<NRO_DOC>
	<RAZON_SOCIAL>
	<CALLE>
	<LOCALIDAD>
	<NACIONALIDAD>
	<FECHA_NAC>
	<COD_POSTAL>
	<TELEFONO>
	<REGISTRACLIENTE>
	<FECHAAGENDA>
	<HORAAGENDA>
	<CIERRA>
	<COD_CAMPANIA>
	<COD_LUGAR>
	<COD_DESTINO>
	<COD_CANAL>
	<COD_ENTREVISTA>
	<COD_SECTOR>
	<COD_SUCURSAL>	
	<OBSERVACION>
	<USUARIO>
	<ACTUALIZABASE>
	<PRODUCTO>
</AGREGAR_AGENDA>

RESPONSE String

<GRABACIONES>
	<AGREGAR_AGENDA>	
		<ESTADO RESULTADO='TRUE' MENSAJE='' />
	</AGREGAR_AGENDA>
</GRABACIONES>
