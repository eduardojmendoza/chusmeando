VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "VerificacionDomi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Clientes.VerificacionDomi"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    '
    Dim wvarEmpresa     As String
    Dim wvarTipoDoc     As String
    Dim wvarNroDoc      As String
    Dim wvarTipoDocRVP  As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstClientes       As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/TIPO_DOC").Text
    wvarNroDoc = wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/NRO_DOC").Text
    wvarNroDoc = NumFilled(wvarNroDoc, 15)
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/EMPRESA").Text
    wvarTipoDocRVP = wobjXMLrequest.selectSingleNode("//VERIFDOMICILIO/TIPODOCRVP").Text
    Set wobjXMLrequest = Nothing
    '
    If UCase(wvarTipoDocRVP) = "TRUE" Then
      wvarTipoDoc = TipoDocRVPToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
    ElseIf (wvarEmpresa = wconstCODIGONYL) Or (wvarEmpresa = wconstCODIGOLBA) Then
      wvarTipoDoc = TipoDocNYLToSAC(wvarEmpresa, wvarTipoDoc, mobjCOM_Context)
    End If
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_VERIFICACION_DOMICILIO"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    If UCase(wvarTipoDoc) = "CUIT - SOC" Then
      wobjDBParm.Value = "SOC"
    Else
      wobjDBParm.Value = wvarTipoDoc
    End If
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Documento"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarNroDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 75
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    '
    Set wrstClientes = wobjDBCmd.Execute
    Set wrstClientes.ActiveConnection = Nothing
    '
    wvarStep = 90
    Set wobjDBCmd = Nothing
    '
    wvarStep = 100
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 110
    '*********************************************************************
    pvarResponse = "<VERIFICACIONDOMICILIO>"
    If Not wrstClientes.EOF Then
      '
      wvarStep = 120
      pvarResponse = pvarResponse & "<DEBEVERIFICAR>" & wrstClientes.Fields("VERIFICACION").Value & "</DEBEVERIFICAR>"
      pvarResponse = pvarResponse & "<EXISTECLIENTE>" & wrstClientes.Fields("EXISTECLIENTE").Value & "</EXISTECLIENTE>"
    End If
    '
    pvarResponse = pvarResponse & "</VERIFICACIONDOMICILIO>"

    '*********************************************************************
    '
    wvarStep = 130
    If wrstClientes.State <> 0 Then wrstClientes.Close
    '
    wvarStep = 140
    Set wrstClientes = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        pvarResponse = "<VERIFICACIONDOMICILIO><ESTADO RESULTADO='FALSE' MENSAJE='" & stringToXML(Err.Description) & "' /></VERIFICACIONDOMICILIO>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


