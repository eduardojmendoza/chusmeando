VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "VerificarProdPendEnt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Clientes.VerificarProdPendEnt"
'
Const ta_VISAENDIST     As Integer = 1
Const ta_AMEXENDIST     As Integer = 2
Const ta_MASTERENDIST   As Integer = 3
Const ta_BANELCOENDIST  As Integer = 4
Const ta_RESUMENTC      As Integer = 5
Const ta_EXTRACTOCUENTA As Integer = 6
Const ta_PLASTICOENSUC  As Integer = 7

Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'
'Campos de la grilla para guardar la información de los Plásticos
'que se encuentran en la sucursal.
Private Type PlasticosEnSuc
   NroPlastico As String
   Sucursal    As String
End Type

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) As Long
   '
   Const wcteFnName     As String = "IAction_Execute"
   Const wcteFnPrefix   As String = "[VerificarProdPendEnt__" & wcteFnName & "_Execute] "
   Dim wvarStep         As Long
   
   '
   Dim wobjXMLrequest   As MSXML2.DOMDocument
   '
   Dim wvarTipoDoc      As String
   Dim wvarNroDoc       As String
   Dim wvarTipoConsulta As String
   Dim wvarTipoArchivo  As String
   '
   Dim wvstrTCDist      As String
   Dim wvstrTDDist      As String
   Dim wvstrExtCta      As String
   Dim wvstrResTC       As String
   Dim wvarrPlasticos() As PlasticosEnSuc
   Dim wvarintElem      As Integer
   Dim i                As Integer
   '
   Dim wobjHSBC_DBCnn   As HSBCInterfaces.IDBConnection
   Dim wobjDBCnn        As ADODB.Connection
   Dim wobjDBCmd        As ADODB.Command
   Dim wobjDBParm       As ADODB.Parameter
   Dim wrstClientes     As ADODB.Recordset
   '
   '~~~~~~~~~~~~~~~~~~~~~~~~~~~
   On Error GoTo ErrorHandler
   '~~~~~~~~~~~~~~~~~~~~~~~~~~~
   '
   'Se crea el primer elemento del arreglo para permitir el redimensionamiento.
   LogTrace wcteFnPrefix & "---> INICIO <---"
   LogTrace wcteFnPrefix & "Request: " & pvarRequest
   
   wvarStep = 10
   ReDim wvarrPlasticos(0)
   wvarrPlasticos(0).NroPlastico = ""
   wvarrPlasticos(0).Sucursal = ""
   '
   wvarStep = 20
   Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
   With wobjXMLrequest
       .async = False
       Call .loadXML(pvarRequest)
   End With
   '
   wvarStep = 30
   wvarTipoDoc = wobjXMLrequest.selectSingleNode("//PRODPENDENT/TIPO_DOC").Text
   wvarNroDoc = wobjXMLrequest.selectSingleNode("//PRODPENDENT/NRO_DOC").Text
   wvarTipoConsulta = wobjXMLrequest.selectSingleNode("//PRODPENDENT/TIPOCONS").Text
   wvarTipoArchivo = wobjXMLrequest.selectSingleNode("//PRODPENDENT/TIPOARCHIVO").Text
   LogTrace wcteFnPrefix & "TipoDoc     : " & wvarTipoDoc
   LogTrace wcteFnPrefix & "NroDoc      : " & wvarNroDoc
   LogTrace wcteFnPrefix & "TipoConsulta: " & wvarTipoConsulta
   LogTrace wcteFnPrefix & "TipoArchivo : " & wvarTipoArchivo
   
   
   wvarNroDoc = NumFilled(wvarNroDoc, 15)
   Set wobjXMLrequest = Nothing
   '
   '
   wvarStep = 40
   Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
   '
   wvarStep = 50
   Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteBR_DBCnn)
   '
   wvarStep = 60
   Set wobjDBCmd = CreateObject("ADODB.Command")
   Set wobjDBCmd.ActiveConnection = wobjDBCnn
   '
   wobjDBCmd.CommandType = adCmdStoredProc
   '
   'C: Verificar si el cliente tiene Productos Pendientes de Entrega
   'L: Obtener los productos pendientes de entrega del cliente
   'M: Deshabilitar los registros correspondientes al tipo de archivo
   wvarStep = 70
   Select Case wvarTipoConsulta
      Case "C"
         wobjDBCmd.CommandText = "P_CHK_RechazosOCA"
         LogTrace wcteFnPrefix & "Ejecutar SP P_CHK_RechazosOCA"
         '
         wvarStep = 80
         Set wobjDBParm = CreateObject("ADODB.Parameter")
         wobjDBParm.Name = "@TipoDoc"
         wobjDBParm.Direction = adParamInput
         wobjDBParm.Type = adChar
         wobjDBParm.Size = 3
         
         If UCase(wvarTipoDoc) = "CUIT - SOC" Then
            wobjDBParm.Value = "SOC"
         Else
            wobjDBParm.Value = wvarTipoDoc
         End If
         
         wobjDBCmd.Parameters.Append wobjDBParm
         '
         wvarStep = 90
         Set wobjDBParm = CreateObject("ADODB.Parameter")
         wobjDBParm.Name = "@Documento"
         wobjDBParm.Direction = adParamInput
         wobjDBParm.Type = adVarChar
         wobjDBParm.Size = 15
         wobjDBParm.Value = wvarNroDoc
         wobjDBCmd.Parameters.Append wobjDBParm
          '
      Case "L"
         wobjDBCmd.CommandText = "P_SEL_RechazosOCA"
         LogTrace wcteFnPrefix & "Ejecutar SP P_SEL_RechazosOCA"
         '
         wvarStep = 80
         Set wobjDBParm = CreateObject("ADODB.Parameter")
         wobjDBParm.Name = "@TipoDoc"
         wobjDBParm.Direction = adParamInput
         wobjDBParm.Type = adChar
         wobjDBParm.Size = 3
         
         If UCase(wvarTipoDoc) = "CUIT - SOC" Then
            wobjDBParm.Value = "SOC"
         Else
            wobjDBParm.Value = wvarTipoDoc
         End If
         
         wobjDBCmd.Parameters.Append wobjDBParm
          '
          wvarStep = 90
          Set wobjDBParm = CreateObject("ADODB.Parameter")
          wobjDBParm.Name = "@Documento"
          wobjDBParm.Direction = adParamInput
          wobjDBParm.Type = adVarChar
          wobjDBParm.Size = 15
          wobjDBParm.Value = wvarNroDoc
          wobjDBCmd.Parameters.Append wobjDBParm
          '
      Case "M"
         wobjDBCmd.CommandText = "P_UPD_RechazosOCA"
         LogTrace wcteFnPrefix & "Ejecutar SP P_UPD_RechazosOCA '" & wvarTipoDoc & "', '" & wvarNroDoc & "', '" & wvarTipoArchivo & "'"
         '
         wvarStep = 80
         Set wobjDBParm = CreateObject("ADODB.Parameter")
         wobjDBParm.Name = "@TipoDoc"
         wobjDBParm.Direction = adParamInput
         wobjDBParm.Type = adChar
         wobjDBParm.Size = 3
         
         If UCase(wvarTipoDoc) = "CUIT - SOC" Then
            wobjDBParm.Value = "SOC"
         Else
            wobjDBParm.Value = wvarTipoDoc
         End If
         
         wobjDBCmd.Parameters.Append wobjDBParm
         '
         wvarStep = 90
         Set wobjDBParm = CreateObject("ADODB.Parameter")
         wobjDBParm.Name = "@Documento"
         wobjDBParm.Direction = adParamInput
         wobjDBParm.Type = adVarChar
         wobjDBParm.Size = 15
         wobjDBParm.Value = wvarNroDoc
         wobjDBCmd.Parameters.Append wobjDBParm
         '
         wvarStep = 100
         Set wobjDBParm = CreateObject("ADODB.Parameter")
         wobjDBParm.Name = "@TipoArchivo"
         wobjDBParm.Direction = adParamInput
         wobjDBParm.Type = adInteger
         wobjDBParm.Value = wvarTipoArchivo
         wobjDBCmd.Parameters.Append wobjDBParm
         '
         'wvarStep = 105
         'Set wobjDBParm = CreateObject("ADODB.Parameter")
         'wobjDBParm.Name = "@Resultado"
         'wobjDBParm.Direction = adParamOutput
         'wobjDBParm.Type = adChar
         'wobjDBParm.Size = 5
         'wobjDBParm.Value = ""
         'wobjDBCmd.Parameters.Append wobjDBParm
   End Select
   '
   wvarStep = 110
   '
   Set wrstClientes = wobjDBCmd.Execute
   Set wrstClientes.ActiveConnection = Nothing
   '
   wvarStep = 120
   Set wobjDBCmd = Nothing
   '
   wvarStep = 130
   wobjDBCnn.Close
   Set wobjDBCnn = Nothing
   Set wobjHSBC_DBCnn = Nothing
   '
   wvarStep = 140
   '*********************************************************************
   pvarResponse = "<RESPONSE><CLIENTINFO>"
   LogTrace "[VerificarProdPendEnt__" & wcteFnName & "] Hay datos? " & IIf(wrstClientes.EOF, "No", "Si")
   
   '
   If Not wrstClientes.EOF Then
      '
      Select Case wvarTipoConsulta
         Case "C"
            wvarStep = 150
            pvarResponse = pvarResponse & "<TIENEPROD>" & wrstClientes.Fields("TIENEPROD").Value & "</TIENEPROD>"
         
         Case "L"
            wvarStep = 150
            Do While Not wrstClientes.EOF
               Select Case wrstClientes("TipoArchivo").Value
                  Case ta_VISAENDIST, ta_AMEXENDIST, ta_MASTERENDIST
                     wvarStep = 160
                     If Trim(wvstrTCDist) <> "" Then wvstrTCDist = wvstrTCDist & " - "
                     wvstrTCDist = wvstrTCDist & wrstClientes("Marca").Value
                  
                  Case ta_BANELCOENDIST
                     wvarStep = 160
                     wvstrTDDist = wvstrTDDist & wrstClientes("Marca").Value
                  
                  Case ta_RESUMENTC
                     wvarStep = 160
                     wvstrResTC = "Cta.: " & Val(Trim(wrstClientes("NroPlastico"))) & " - " & Trim(wrstClientes("DetMotRech"))
                  
                  Case ta_EXTRACTOCUENTA
                     wvarStep = 160
                     wvstrExtCta = Trim(wrstClientes("DetMotRech"))
                  
                  Case ta_PLASTICOENSUC
                     wvarStep = 160
                     If wvarintElem > 0 Then
                        ReDim Preserve wvarrPlasticos(wvarintElem)
                     End If
                     
                     wvarStep = 170
                     With wvarrPlasticos(wvarintElem)
                        .NroPlastico = wrstClientes("NroPlastico")
                        .Sucursal = Trim(wrstClientes("Sucursal")) & "-" & _
                                    Trim(wrstClientes("DetSuc"))
                     End With
                     
                     wvarintElem = wvarintElem + 1
               End Select
               
               wvarStep = 180
               wrstClientes.MoveNext
            Loop
            
            wvarStep = 190
            pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"" MENSAJE="""" CODIGO=""""/>"
            pvarResponse = pvarResponse & "<TCENDIST>" & wvstrTCDist & "</TCENDIST>"
            pvarResponse = pvarResponse & "<TDENDIST>" & wvstrTDDist & "</TDENDIST>"
            pvarResponse = pvarResponse & "<EXTRACTOCTA>" & wvstrExtCta & "</EXTRACTOCTA>"
            pvarResponse = pvarResponse & "<RESUMENTC>" & wvstrResTC & "</RESUMENTC>"
            
            wvarStep = 200
            pvarResponse = pvarResponse & "<PLASTICOS>"
            
            If wvarrPlasticos(0).NroPlastico <> "" Then
               wvarStep = 210
               For i = 0 To UBound(wvarrPlasticos) '- 1
                  pvarResponse = pvarResponse & "<PLASTICO>"
                  pvarResponse = pvarResponse & "<PLASTICO_NRO>"
                  pvarResponse = pvarResponse & wvarrPlasticos(i).NroPlastico
                  pvarResponse = pvarResponse & "</PLASTICO_NRO>"
                  pvarResponse = pvarResponse & "<PLASTICO_SUC>"
                  pvarResponse = pvarResponse & wvarrPlasticos(i).Sucursal
                  pvarResponse = pvarResponse & "</PLASTICO_SUC>"
                  pvarResponse = pvarResponse & "</PLASTICO>"
               Next
            End If
            
            wvarStep = 220
            pvarResponse = pvarResponse & "</PLASTICOS>"
            LogTrace "[VerificarProdPendEnt__" & wcteFnName & "] Response: " & pvarResponse
            
         Case "M"
            wvarStep = 150
            pvarResponse = pvarResponse & "<ESTADO RESULTADO=""" & wrstClientes("@Resultado").Value
            pvarResponse = pvarResponse & """ MENSAJE="""
            
            If wrstClientes("@Resultado").Value <> "" Then
               pvarResponse = pvarResponse & "Problemas al modificar: " & DetTipoArchivo(wvarTipoArchivo)
            End If
            
            pvarResponse = pvarResponse & """ CODIGO=""""/>"
            
      End Select
   Else
      pvarResponse = pvarResponse & "<ESTADO RESULTADO=""TRUE"""
      pvarResponse = pvarResponse & " MENSAJE=""SIN DATOS"""
      pvarResponse = pvarResponse & " CODIGO="""" />"
   End If
   '
   wvarStep = 230
   pvarResponse = pvarResponse & "</CLIENTINFO></RESPONSE>"
   LogTrace "[VerificarProdPendEnt__" & wcteFnName & "] Response: " & pvarResponse
   '*********************************************************************
   '
   wvarStep = 240
   If wrstClientes.State <> 0 Then wrstClientes.Close
   '
   wvarStep = 250
   Set wrstClientes = Nothing
   '
   wvarStep = 260
   mobjCOM_Context.SetComplete
   LogTrace wcteFnPrefix & "--->  FIN   <---"
   Exit Function
   '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        pvarResponse = "<PRODPENDENT><ESTADO RESULTADO='FALSE' MENSAJE='" & stringToXML(Err.Description) & "' /></PRODPENDENT>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function DetTipoArchivo(ByVal pTipoArchivo As Integer) As String
   Select Case pTipoArchivo
      Case ta_AMEXENDIST, ta_VISAENDIST, ta_MASTERENDIST
         DetTipoArchivo = "TC en Distribucion"
      Case ta_BANELCOENDIST
         DetTipoArchivo = "TD en Distribucion"
      Case ta_RESUMENTC
         DetTipoArchivo = "Resumen de TC"
      Case ta_EXTRACTOCUENTA
         DetTipoArchivo = "Extracto de Cuenta"
      Case ta_PLASTICOENSUC
         DetTipoArchivo = "Plasticos en Sucursal"
   End Select
End Function

Sub X()
Dim a1 As DOMDocument
Dim b1

Set a1 = New DOMDocument
a1.hasChildNodes
End Sub
