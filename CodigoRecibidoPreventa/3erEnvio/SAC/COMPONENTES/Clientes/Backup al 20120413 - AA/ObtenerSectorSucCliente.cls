VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "ObtenerSectorSucCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Clientes.ObtenerSectorSucCliente"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjXMLDocument As MSXML2.DOMDocument
    Dim wobjVerifDomi   As IAction
    '
    Dim wvarEmpresa     As String
    Dim wvarTipoDoc     As String
    Dim wvarNroDoc      As String
    Dim wvarFechaNacimiento
    Dim wvarEdad
    Dim wvarDebeVerificar As String
    Dim wvarRequestVerifDomi As String
    Dim wvarResponseVerifDomi As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstClientes    As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//CLIENTE/TIPDOC").Text
    wvarNroDoc = NumFilled(wobjXMLrequest.selectSingleNode("//CLIENTE/NRODOC").Text, 15)
    
    wvarNroDoc = NumFilled(wvarNroDoc, 15)
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_SECTORDELCLIENTE"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 3
    wobjDBParm.Value = wvarTipoDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@NroDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarNroDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    '
    wvarStep = 78
    Set wrstClientes = wobjDBCmd.Execute
    Set wrstClientes.ActiveConnection = Nothing
    '
    wvarStep = 100
    Set wobjDBCmd = Nothing
    '
    wvarStep = 110
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 120
    '*********************************************************************
    pvarResponse = "<SECTORCLIENTE>"
    
    If Not wrstClientes.EOF Then
      pvarResponse = pvarResponse & "<CODIGO>" & wrstClientes("COD_SEC") & "</CODIGO><DESCRIPCION>" & wrstClientes("SEC_DES_SEC") & "</DESCRIPCION>"
    Else
      pvarResponse = pvarResponse & "<CODIGO></CODIGO><DESCRIPCION></DESCRIPCION>"
    End If
    '
    wvarStep = 170
    pvarResponse = pvarResponse & "</SECTORCLIENTE>"
    '*********************************************************************
    '
    wvarStep = 180
    If wrstClientes.State <> 0 Then wrstClientes.Close
    '
    wvarStep = 190
    Set wrstClientes = Nothing
    '
    wvarStep = 200
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        pvarResponse = "<SECTORCLIENTE><ESTADO RESULTADO='FALSE' MENSAJE='" & stringToXML(Err.Description) & "' /></SECTORCLIENTE>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


