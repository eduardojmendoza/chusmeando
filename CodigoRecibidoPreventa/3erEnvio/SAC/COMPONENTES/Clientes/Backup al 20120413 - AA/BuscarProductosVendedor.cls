VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "BuscarProductosVendedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
' *****************************************************************
' COPYRIGHT. HSBC HOLDINGS PLC 2003. ALL RIGHTS RESERVED.
'
' THIS SOFTWARE IS ONLY TO BE USED FOR THE PURPOSE FOR WHICH IT
' HAS BEEN PROVIDED. NO PART OF IT IS TO BE REPRODUCED,
' DISASSEMBLED, TRANSMITTED, STORED IN A RETRIEVAL SYSTEM NOR
' TRANSLATED IN ANY HUMAN OR COMPUTER LANGUAGE IN ANY WAY OR
' FOR ANY OTHER PURPOSES WHATSOEVER WITHOUT THE PRIOR WRITTEN
' CONSENT OF HSBC HOLDINGS PLC.
' *****************************************************************
'
' *****************************************************************
' Module Name :sacA_Clientes.BuscarProductosVendedor
'
' Module ID : BuscarProductosVendedor
'
' File Name : BuscarProductosVendedor.cls
'
' Creation Date: 29/03/2004
'
' Programmer : Fernando J. Martelli
'
' Abstract : Busca Productos de un Vendedor de Banco
'
' Entry :
' - parameters :
'                 CODIGO
' Exit :
'                 CODIGO
'                 DESCRIPCION
'                 UNIDADNEGOCIO
'                 DELEGACION
'
' Program : A list of messages used in this module
'
' Functions :
'                 p_GetXSLProductos()
'
' Remarks : Any additional information (optional)
'
' Amendment History:
' The followings should be specified (in chronological
' sequence) for each amendment made to the module:
' Version Version number of the module
' Date Date of amendment (dd/mm/ccyy)
' PPCR Associated PPCR number
' Programmer Name of programmer who made the
' Amendment
' Reason Brief description of the amendment
' *****************************************************************
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Clientes.BuscarProductosVendedor"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName     As String = "IAction_Execute"
    Dim wvarStep         As Long
    '
    'Dim wobjXMLrequest   As MSXML2.DOMDocument
    Dim wobjXMLDocument As MSXML2.DOMDocument
    Dim wobjXSLDocument  As MSXML2.DOMDocument
    Dim wobjNodelist     As MSXML2.IXMLDOMNodeList
    Dim wobjNode         As MSXML2.IXMLDOMNode
    '
    Dim wvarQuery        As String
    'Dim wvarCodigo       As String
    '
    Dim wobjHSBC_DBCnn   As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn        As ADODB.Connection
    Dim wobjDBCmd        As ADODB.Command
    Dim wobjDBParm       As ADODB.Parameter
    Dim wrstProductos    As ADODB.Recordset
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 20
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    '
    wvarStep = 30
    '
    wvarQuery = "SP_SAC_BUSCARPRODUCTOSVEND "
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = wvarQuery
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    Set wrstProductos = wobjDBCmd.Execute
    Set wrstProductos.ActiveConnection = Nothing
    '
    wvarStep = 40
    Set wobjDBCmd = Nothing
    '
    wvarStep = 50
    If Not wrstProductos.EOF Then
      Set wobjXMLDocument = CreateObject("MSXML2.DomDocument")
      wobjXMLDocument.async = False
      wvarStep = 60
      wrstProductos.Save wobjXMLDocument, adPersistXML
      Set wobjXSLDocument = CreateObject("MSXML2.DomDocument")
      wvarStep = 70
      wobjXSLDocument.loadXML (p_GetXSLProductos)
      wobjXMLDocument.loadXML (wobjXMLDocument.transformNode(wobjXSLDocument))
      Set wobjXSLDocument = Nothing
      '
      wvarStep = 80
      Set wobjNodelist = wobjXMLDocument.selectNodes("//PRODUCTOS/DETALLEPRODUCTO")
      For Each wobjNode In wobjNodelist
        wobjNode.selectSingleNode("DESCRIPCION").Text = "<![CDATA[" & wobjNode.selectSingleNode("DESCRIPCION").Text & "]]>"
      Next
      wvarStep = 90
      pvarResponse = wobjXMLDocument.xml
      Set wobjNodelist = Nothing
      Set wobjNode = Nothing
      Set wobjXMLDocument = Nothing
    Else
      pvarResponse = "<PRODUCTOS><ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido encontrar el producto relacionado al Vendedor' /></PRODUCTOS>"
    End If
    '
    wvarStep = 100
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 110
    If wrstProductos.State <> 0 Then
      wrstProductos.Close
    End If
    '
    wvarStep = 120
    Set wrstProductos = Nothing
    '
    wvarStep = 130
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        'App.LogEvent pvarRequest
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function


Private Function p_GetXSLProductos() _
                 As String
  '
  ' *****************************************************************
  ' Function : p_GetXSLProductos
  ' Abstract : Genera el codigo XSL de respuesta
  ' Synopsis : string = p_GetXSLProductos()
  ' Parameter list:
  ' Return values: String conteniendo el codigo XSL
  ' Side effect :
  ' *****************************************************************
  '
  Dim wvarStrXSL  As String
  '
  wvarStrXSL = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/TR/WD-xsl'>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='/'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:copy>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates select='//xml/rs:data'/>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:copy>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='PRODUCTOS'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='ESTADO'>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='RESULTADO'>TRUE</xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "     <xsl:attribute name='MENSAJE'></xsl:attribute>"
  wvarStrXSL = wvarStrXSL & "   </xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
  wvarStrXSL = wvarStrXSL & "  <xsl:element name='DETALLEPRODUCTO'>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='CODIGO'><xsl:value-of select='@TIP_PROD_COD' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "   <xsl:element name='DESCRIPCION'><xsl:value-of select='@TIP_PROD_DESC' /></xsl:element>"
  wvarStrXSL = wvarStrXSL & "  </xsl:element>"
  wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
  wvarStrXSL = wvarStrXSL & " </xsl:template>"
  wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
  '
  p_GetXSLProductos = wvarStrXSL
End Function


