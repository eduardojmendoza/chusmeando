VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "Ficha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'
'
Implements ObjectControl
Implements HSBCInterfaces.IAction
'
'
Const mcteClassName As String = "sacA_Clientes.Ficha"
Dim mobjCOM_Context As ObjectContext
Dim mobjEventLog    As HSBCInterfaces.IEventLog
'

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName    As String = "IAction_Execute"
    Dim wvarStep        As Long
    '
    Dim wobjXMLrequest  As MSXML2.DOMDocument
    Dim wobjXMLDocument As MSXML2.DOMDocument
    Dim wobjVerifDomi   As IAction
    '
    Dim wvarEmpresa     As String
    Dim wvarTipoDoc     As String
    Dim wvarNroDoc      As String
    Dim wvarFechaNacimiento
    Dim wvarEdad
    Dim wvarSegmento    As String
    Dim wvarDebeVerificar As String
    Dim wvarRequestVerifDomi As String
    Dim wvarResponseVerifDomi As String
    '
    Dim wobjHSBC_DBCnn  As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn       As ADODB.Connection
    Dim wobjDBCmd       As ADODB.Command
    Dim wobjDBParm      As ADODB.Parameter
    Dim wrstClientes    As ADODB.Recordset
    
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    '
    wvarStep = 10
    Set wobjXMLrequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLrequest
        .async = False
        Call .loadXML(pvarRequest)
    End With
    '
    wvarStep = 20
    wvarTipoDoc = wobjXMLrequest.selectSingleNode("//CONSULTACLIENTES_TIPONRO/TIPO_DOC").Text
    wvarNroDoc = wobjXMLrequest.selectSingleNode("//CONSULTACLIENTES_TIPONRO/NRO_DOC").Text
    wvarEmpresa = wobjXMLrequest.selectSingleNode("//CONSULTACLIENTES_TIPONRO/EMPRESA").Text
    
    wvarNroDoc = NumFilled(wvarNroDoc, 15)
    Set wobjXMLrequest = Nothing
    '
    wvarStep = 30
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 40
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mCte_COMISIONESDB)
    wobjDBCnn.Execute "SET NOCOUNT ON"
    '
    wvarStep = 50
    Set wobjDBCmd = CreateObject("ADODB.Command")
    Set wobjDBCmd.ActiveConnection = wobjDBCnn
    wobjDBCmd.CommandText = "SP_PERSONA"
    wobjDBCmd.CommandType = adCmdStoredProc
    '
    wvarStep = 60
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@TipoDoc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adChar
    wobjDBParm.Size = 3
    If UCase(wvarTipoDoc) = "CUIT - SOC" Then
      wobjDBParm.Value = "SOC"
    Else
      wobjDBParm.Value = wvarTipoDoc
    End If
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 70
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Doc"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adVarChar
    wobjDBParm.Size = 15
    wobjDBParm.Value = wvarNroDoc
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 80
    Set wobjDBParm = CreateObject("ADODB.Parameter")
    wobjDBParm.Name = "@Empresa"
    wobjDBParm.Direction = adParamInput
    wobjDBParm.Type = adInteger
    wobjDBParm.Value = wvarEmpresa
    wobjDBCmd.Parameters.Append wobjDBParm
    '
    wvarStep = 90
    Set wrstClientes = wobjDBCmd.Execute
    Set wrstClientes.ActiveConnection = Nothing
    '
    wvarStep = 100
    Set wobjDBCmd = Nothing
    '
    wvarStep = 110
    wobjDBCnn.Close
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    wvarStep = 120
    '*********************************************************************
    pvarResponse = "<FICHACLIENTE>"
    If Not wrstClientes.EOF Then
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='TRUE' MENSAJE='' />"
      Do
        '
        wvarStep = 130
        wvarFechaNacimiento = wrstClientes.Fields("NACIMIENTO").Value
        wvarEdad = Abs(DateDiff("yyyy", CDate(Year(Now) & "/" & Month(Now) & "/" & Day(Now)), wvarFechaNacimiento))
        wvarStep = 140
        If Month(Now) < Month(wvarFechaNacimiento) Then
          wvarEdad = wvarEdad - 1
        ElseIf (Month(wvarFechaNacimiento) = Month(Now)) And (Day(wvarFechaNacimiento) > Day(Now)) Then
          wvarEdad = wvarEdad - 1
        End If
        wvarStep = 150
        '
        pvarResponse = pvarResponse & _
                    "<DATOSCLIENTE>" & _
                    "<TIPO_DOC>" & wrstClientes.Fields("TIPODOC").Value & "</TIPO_DOC>" & _
                    "<NRO_DOC>" & wrstClientes.Fields("DOCUMENTO").Value & "</NRO_DOC>" & _
                    "<RAZON_SOCIAL>" & stringToXML(wrstClientes.Fields("RAZONSOCIAL").Value) & "</RAZON_SOCIAL>" & _
                    "<NACIONALIDAD>" & wrstClientes.Fields("NACIONALIDAD").Value & "</NACIONALIDAD>" & _
                    "<NACIMIENTO>" & Format(wrstClientes.Fields("NACIMIENTO").Value, "dd/mm/yyyy") & "</NACIMIENTO>" & _
                    "<DOMICILIO_PART>" & stringToXML(wrstClientes.Fields("DOMICILIOPART").Value) & "</DOMICILIO_PART>" & _
                    "<CP_PART>" & wrstClientes.Fields("CPPART").Value & "</CP_PART>" & _
                    "<LOCALIDAD_PART>" & stringToXML(wrstClientes.Fields("LOCALIDADPART").Value) & "</LOCALIDAD_PART>" & _
                    "<TELEFONO_PART>" & wrstClientes.Fields("TELEFONOPART").Value & "</TELEFONO_PART>" & _
                    "<DOMICILIO_CCIAL>" & stringToXML(wrstClientes.Fields("DOMICILIOCOM").Value) & "</DOMICILIO_CCIAL>" & _
                    "<CP_CCIAL>" & wrstClientes.Fields("CPCOM").Value & "</CP_CCIAL>" & _
                    "<LOCALIDAD_CCIAL>" & stringToXML(wrstClientes.Fields("LOCALIDADCOM").Value) & "</LOCALIDAD_CCIAL>" & _
                    "<TELEFONO_CCIAL>" & wrstClientes.Fields("TELEFONOCOM").Value & "</TELEFONO_CCIAL>" & _
                    "<SUCURSAL_RADICACION>" & stringToXML(wrstClientes.Fields("SUCURSALRADICACION").Value) & "</SUCURSAL_RADICACION>" & _
                    "<CODCATEGORIA>" & wrstClientes.Fields("CATEGORIA").Value & "</CODCATEGORIA>" & _
                    "<CATEGORIA>" & TratarCategorias(wrstClientes.Fields("CATEGORIA").Value) & "</CATEGORIA>" & _
                    "<CATEGORIACIS>" & Replace(Trim(wrstClientes.Fields("CATEGORIACIS").Value & ""), "&", "y") & "</CATEGORIACIS>" & _
                    "<EMPLEADO>" & wrstClientes.Fields("EMPLEADO").Value & "</EMPLEADO>" & _
                    "<BANCO>" & wrstClientes.Fields("BANCO").Value & "</BANCO>" & _
                    "<EDAD>" & wvarEdad & "</EDAD>" & _
                    "<SEGMENTO>" & wrstClientes.Fields("SEGMENTO").Value & "</SEGMENTO>" & _
                    "</DATOSCLIENTE>"
        '
        wvarStep = 160
        wrstClientes.MoveNext
      Loop While Not wrstClientes.EOF
      
    Else
      pvarResponse = pvarResponse & "<ESTADO RESULTADO='FALSE' MENSAJE='No se ha podido encontrar al cliente' />"
    End If
    '
    wvarStep = 170
    pvarResponse = pvarResponse & "</FICHACLIENTE>"
    '*********************************************************************
    '
    wvarStep = 180
    If wrstClientes.State <> 0 Then wrstClientes.Close
    '
    wvarStep = 190
    Set wrstClientes = Nothing
    '
    wvarStep = 200
    mobjCOM_Context.SetComplete
    Exit Function
    '
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    If Err.Number = mCte_TimeOutError Then
        pvarResponse = Empty
        IAction_Execute = xERR_TIMEOUT_RETURN
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    Else
        pvarResponse = "<FICHACLIENTE><ESTADO RESULTADO='FALSE' MENSAJE='" & stringToXML(Err.Description) & "' /></FICHACLIENTE>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         mcteClassName, _
                         wcteFnName, _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
        IAction_Execute = xERR_UNEXPECTED
    End If
    mobjCOM_Context.SetAbort
End Function
