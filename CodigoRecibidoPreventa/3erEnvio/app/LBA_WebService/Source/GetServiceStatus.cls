VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GetServiceStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function GetServiceStatus(pvarBrokerID As Long, pvarRequestID As Long) As String
    Dim wvarResponse As String
    '
    If pvarRequestID < 0 Then
        wvarResponse = "<LBA_WS res_code=""-1"" res_msg=""RequestID invalido""></LBA_WS>"
    Else
        wvarResponse = "<LBA_WS res_code=""0"" res_msg="""">"
        wvarResponse = wvarResponse & "<DATE_TIME><![CDATA[" & CStr(Now()) & "]]></DATE_TIME>"
        wvarResponse = wvarResponse & "</LBA_WS>"
    End If
    '
    GetServiceStatus = wvarResponse
End Function

