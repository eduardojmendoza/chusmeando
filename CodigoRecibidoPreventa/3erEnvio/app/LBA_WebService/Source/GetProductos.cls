VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GetProductos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Dim mobjEventLog                            As HSBCInterfaces.IEventLog

Public Function GetProductos(pvarRequest As String, pvarBrokerID As Long, pvarRequestID As Long) As String
    Dim wvarResponse As String
    
    Call cmdp_ExecuteTrn("GetProductos_broker", _
                         "", _
                         pvarRequest, _
                         wvarResponse)
    GetProductos = wvarResponse
    
End Function

Private Function cmdp_FormatRequest(pvarActionCode As String, _
                                   pvarSchemaFile As String, _
                                   pvarBody As String) As String
    Dim wvarRequest As String
    Dim wvarSchema  As String
    '
    wvarRequest = ""
    wvarSchema = ""
    '
    wvarRequest = wvarRequest & _
                  "<HSBC_MSG" & wvarSchema & ">" & _
                      "<HEADER>" & _
                          "<APPLICATION_CODE>" & _
                              xAPP_CODE & _
                          "</APPLICATION_CODE>" & _
                          "<ACTION_CODE>" & _
                              pvarActionCode & _
                          "</ACTION_CODE>" & _
                      "</HEADER>" & _
                      "<BODY>" & _
                           pvarBody & _
                      "</BODY>" & _
                  "</HSBC_MSG>"
    '
    cmdp_FormatRequest = wvarRequest
End Function

Private Function cmdp_ExecuteTrn(pvarActionCode As String, _
                                pvarSchemaFile As String, _
                                pvarRequest As String, _
                                pvarResponse As String) As Integer
    Dim wobjCmdProcessor    As Object 'HSBC_ASP.CmdProcessor
    Dim wvarRequest         As String
    Dim wvarExecReturn      As Long
    Dim wvarStep            As Integer
    '
    On Error GoTo errorManejado
    '
    Set wobjCmdProcessor = CreateObject("HSBC_ASP.CmdProcessor")
    '
    wvarRequest = cmdp_FormatRequest(pvarActionCode, _
                                     pvarSchemaFile, _
                                     pvarRequest)
    '
    pvarResponse = ""
    wvarExecReturn = wobjCmdProcessor.Execute(wvarRequest, pvarResponse)
    '
    cmdp_ExecuteTrn = wvarExecReturn
    Set wobjCmdProcessor = Nothing
    '
    Exit Function
    
errorManejado:
    
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    
    pvarResponse = "<LBA_WS res_code=""-911"" res_msg=""Problemas de conexion entre el servidor web y el servidor app""></LBA_WS>"
    
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                         "GetProductos", _
                         "cmdp_ExecuteTrn", _
                         wvarStep, _
                         Err.Number, _
                         "Error= [" & Err.Number & "] - " & Err.Description, _
                         vbLogEventTypeError
    
    
    Set mobjEventLog = Nothing
End Function





