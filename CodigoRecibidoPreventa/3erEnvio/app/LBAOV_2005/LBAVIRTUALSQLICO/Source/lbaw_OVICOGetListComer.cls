VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVICOGetListComer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context As ObjectContext
Private mobjEventLog    As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVSQLICO.lbaw_OVICOGetListComer"
Const mcteStoreProc         As String = "SPSNCV_PROD_ICO_COTI_LISTA_UBIC"

'Parametros XML de Entrada
Const mcteParam_RAMOPCOD    As String = "//RAMOPCOD"
Const mcteParam_CERTIPOL    As String = "//CERTIPOL"
Const mcteParam_CERTIANN    As String = "//CERTIANN"
Const mcteParam_CERTISEC    As String = "//CERTISEC"
'
Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
    Dim wobjDBCnn           As ADODB.Connection
    Dim wobjDBCmd           As ADODB.Command
    Dim wrstDBResult        As ADODB.Recordset
    Dim wobjDBParm          As ADODB.Parameter
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarRAMOPCOD        As String
    Dim wvarCERTIPOL        As String
    Dim wvarCERTIANN        As String
    Dim wvarCERTISEC        As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarRAMOPCOD = .selectSingleNode(mcteParam_RAMOPCOD).Text
        wvarCERTIPOL = .selectSingleNode(mcteParam_CERTIPOL).Text
        wvarCERTIANN = .selectSingleNode(mcteParam_CERTIANN).Text
        wvarCERTISEC = .selectSingleNode(mcteParam_CERTISEC).Text
        '
    End With
    '
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 20
    Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
    '
    wvarStep = 30
    Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(gcteDB)
    '
    Set wobjDBCmd = CreateObject("ADODB.Command")
    '
    With wobjDBCmd
        Set .ActiveConnection = wobjDBCnn
        .CommandType = adCmdStoredProc
        .CommandText = mcteStoreProc
        '
        wvarStep = 40
        Set wobjDBParm = wobjDBCmd.CreateParameter("@RAMOPCOD", adChar, adParamInput, 4, wvarRAMOPCOD)
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 50
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIPOL", adNumeric, adParamInput, , wvarCERTIPOL)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 60
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTIANN", adNumeric, adParamInput, , wvarCERTIANN)
        wobjDBParm.Precision = 4
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
        '
        wvarStep = 70
        Set wobjDBParm = wobjDBCmd.CreateParameter("@CERTISEC", adNumeric, adParamInput, , wvarCERTISEC)
        wobjDBParm.Precision = 6
        wobjDBParm.NumericScale = 0
        wobjDBCmd.Parameters.Append wobjDBParm
        Set wobjDBParm = Nothing
    End With
    '
    wvarStep = 50
    Set wrstDBResult = wobjDBCmd.Execute
    Set wrstDBResult.ActiveConnection = Nothing
    '
    ' FORMATEO EL XML DE SALIDA
    wvarStep = 50
    If Not wrstDBResult.EOF Then
        '
        wvarStep = 60
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 70
        wrstDBResult.Save wobjXMLResponse, adPersistXML
        '
        wvarStep = 80
        wobjXSLResponse.async = False
        
        Call wobjXSLResponse.loadXML(p_GetXSL)
        '
        wvarStep = 100
        wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
        '
        wvarStep = 110
        Response = "<Response><Estado resultado=""true"" mensaje="""" />" & wvarResult & "</Response>"
    Else
        wvarStep = 120
        Response = "<Response><Estado resultado=""false"" mensaje=""NO SE ENCONTRARON COMERCIOS."" /></Response>"
    End If
    '
    wvarStep = 130
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    '
    wvarStep = 140
    Set wobjDBCnn = Nothing
    Set wobjHSBC_DBCnn = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    '
    Set wrstDBResult = Nothing
    '
    wvarStep = 150
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    Set wobjXMLResponse = Nothing
    '
    If Not wrstDBResult Is Nothing Then
        If wrstDBResult.State = adStateOpen Then wrstDBResult.Close
    End If
    Set wrstDBResult = Nothing
    '
    Set wobjDBCmd = Nothing
    '
    If Not wobjDBCnn Is Nothing Then
        If wobjDBCnn.State = adStateOpen Then wobjDBCnn.Close
    End If
    Set wobjDBCnn = Nothing
    '
    Set wobjHSBC_DBCnn = Nothing
        
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
End Function
'
' *****************************************************************
' Function : p_GetXSL
' Abstract : Genera el XSL para la transformacion del recordset
' Synopsis : Private Function p_GetXSL() As String
' *****************************************************************
Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0' xmlns:rs='urn:schemas-microsoft-com:rowset' xmlns:z='#RowsetSchema'>"
    '
    wvarStrXSL = wvarStrXSL & "  <xsl:template match='rs:data'>"
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='valoresingresados'>"
    wvarStrXSL = wvarStrXSL & "       <xsl:apply-templates />"
    wvarStrXSL = wvarStrXSL & "     </xsl:element>"
    wvarStrXSL = wvarStrXSL & "  </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "  <xsl:template match='z:row'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>IDCOMERCIO</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@UBICSEC' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>TIPOALARMA</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@ALARMTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>TIPOGUARDIA</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@GUARDTIP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>ALUMNOSCARTELES</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@CANTALUM' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>I_PAIS</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VPAISSCOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>I_PROVINCIA</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VPROVICOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>I_ZONA</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VCODIZONA' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>I_IIBB</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VCLIEIBTP' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>HVCT</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VHVCT' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>GRANIZO</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VGRANIZO' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='dato'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='nombre'>I_CODIGOMODULO</xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='valor'><xsl:value-of select='@VNUMERMOD' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "             <xsl:element name='local'><xsl:value-of select='position()' /></xsl:element>"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    '
    wvarStrXSL = wvarStrXSL & "  </xsl:template>"
    '
    wvarStrXSL = wvarStrXSL & "<xsl:output cdata-section-elements=''/>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VDOMICSEC'><xsl:value-of select='@VDOMICSEC' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VPROVIDES'><xsl:value-of select='@VPROVIDES' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VLOCALIDAD'><xsl:value-of select='@VLOCALIDAD' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VPOSTACOD'><xsl:value-of select='@VPOSTACOD' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VCALLE'><xsl:value-of select='@VCALLE' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VNUMERO'><xsl:value-of select='@VNUMERO' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VPISO'><xsl:value-of select='@VPISO' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VDPTO'><xsl:value-of select='@VDPTO' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VCLIECUIT'><xsl:value-of select='@VCLIECUIT' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VSUMAASEG'><xsl:value-of select='@VSUMAASEG' /></xsl:element>"
    'wvarStrXSL = wvarStrXSL & "     <xsl:element name='VZONADES'><xsl:value-of select='@VZONADES' /></xsl:element>"
    '
    p_GetXSL = wvarStrXSL
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

