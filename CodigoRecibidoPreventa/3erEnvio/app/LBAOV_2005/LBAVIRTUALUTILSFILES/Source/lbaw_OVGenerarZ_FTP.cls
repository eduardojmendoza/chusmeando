VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGenerarZ_FTP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements HSBCInterfaces.IAction
Implements ObjectControl

'Datos de la accion
Const mcteClassName         As String = "lbawA_OfVUtilsFiles.lbaw_OVGenerarZ_FTP"

Const mcteParam_SERVER              As String = "//SERVER"
Const mcteParam_FileName            As String = "//FILENAME"
Const mcteParam_BORRARORIGEN        As String = "//BORRARORIGEN"
Const mcteParam_BORRARDESTINO       As String = "//BORRARDESTINO"
Const mcteParam_FILEDESTINO         As String = "//FILEDESTINO"


Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName            As String = "IAction_Execute"
    Dim wvarStep               As Long
    Dim wvarResult              As String
    Dim wvarMarcaMantenerArch   As Boolean
    '
    Dim wobjXMLRequest          As MSXML2.DOMDocument
    Dim wobjXMLResponse         As MSXML2.DOMDocument
    '
    Dim wobjEle                 As IXMLDOMElement
    Dim wobjXMLServer           As MSXML2.DOMDocument
    '
    Dim wvarFilePath            As String
    Dim wvarFileName            As String
    Dim wvarZipPath             As String
    Dim wvarBorrarOrigen        As Boolean
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    'On Error GoTo errorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10

    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLServer = CreateObject("MSXML2.DOMDocument")
    wobjXMLServer.Load App.Path & "\LBAVirtualServers.xml"
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarBorrarOrigen = (Not .selectSingleNode(mcteParam_BORRARORIGEN) Is Nothing)
        wvarFilePath = wobjXMLServer.selectSingleNode("//" & .selectSingleNode(mcteParam_SERVER).Text).Text
        '
        If Not .selectSingleNode(mcteParam_FILEDESTINO) Is Nothing Then
            wvarZipPath = wvarFilePath & "\" & .selectSingleNode(mcteParam_FILEDESTINO).Text
        End If
        '
        For Each wobjEle In wobjXMLRequest.selectNodes(mcteParam_FileName)
            wvarFileName = wobjEle.Text
            Response = Response & "<ZIP>" & AppendToZip(wvarFilePath, wvarFileName, wvarZipPath, wvarBorrarOrigen) & "</ZIP>"
        Next wobjEle
        '
    End With
    '
    Response = "<Response><Estado resultado='true' mensaje='' />" & Response & "</Response>"
    
    Set wobjXMLServer = Nothing
    Set wobjEle = Nothing
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    wvarStep = 100
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
End Function

Private Function AppendToZip(pvarPath As String, pvarFileName As String, Optional pvarZipPath As String, Optional pvarBorrarArchivoOrigen As Boolean) As String
Dim wvarFile        As Variant
Dim wobjColFiles    As Collection

    If pvarZipPath = "" Then pvarZipPath = pvarPath & "\" & pvarFileName & ".zip"
    
    If (Dir(pvarPath & "\" & pvarFileName & ".zip") <> "") Then
        If Dir(pvarPath & "\" & pvarFileName & "*.txt") <> "" Then 'Bueno, una lastima
            Kill pvarPath & "\" & pvarFileName & ".zip"
        Else
            'Si existe el .ZIP uso directamente ese
            AppendToZip = pvarPath & "\" & pvarFileName & ".zip"
            Exit Function
        End If
    End If
    If Dir(pvarPath & "\" & pvarFileName) <> "" Then
        'Existe el archivo, se agrega y se devuelve el ZIP generado
        GrabarDataEnZip CStr(wvarFile), pvarZipPath, pvarBorrarArchivoOrigen
        AppendToZip = pvarZipPath
    ElseIf Dir(pvarPath & "\" & pvarFileName & "*.*") <> "" Then
        Set wobjColFiles = New Collection
        wvarFile = Dir(pvarPath & "\" & pvarFileName & "*.*")
        Do While wvarFile <> ""
            wobjColFiles.Add wvarFile
            wvarFile = Dir
        Loop
        For Each wvarFile In wobjColFiles
            'Agregamos al ZIP Todos los archivos Similares al enviado
            GrabarDataEnZip pvarPath & "\" & wvarFile, pvarZipPath, pvarBorrarArchivoOrigen
        Next
        
        AppendToZip = pvarZipPath
    End If
    
ClearObjects:
Set wobjColFiles = Nothing
End Function

Private Function GrabarDataEnZip(pvarFile As String, pvarZip As String, pvarBorrarArchivoOrigen As Boolean) As Boolean
Dim wvarAction      As String
Dim wvarRequest     As String
Dim wvarArr()       As String
Dim wvarFileName    As String
Dim wvarResponse    As String
Dim wobjNTFM        As Object

    wvarAction = IIf((Dir(pvarZip) <> ""), "addtozip", "zip")
    wvarArr = Split(pvarFile, "\")
    wvarFileName = wvarArr(UBound(wvarArr))
    wvarRequest = "<Request>" & _
                "<Command>" & wvarAction & "</Command>" & _
                "<FileName>" & pvarFile & "</FileName>" & _
                "<NewFileName>" & pvarZip & "</NewFileName>" & _
                "<ZipName>" & wvarFileName & "</ZipName>" & _
                "</Request>"

    Set wobjNTFM = CreateObject("WD.NTFileManager")
    wobjNTFM.Execute wvarRequest, wvarResponse, ""
    '
    If pvarBorrarArchivoOrigen Then Kill pvarFile
    '
ClearObjects:
    Set wobjNTFM = Nothing
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


