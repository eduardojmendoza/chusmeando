<%
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 20/08/2012
' Ticket: SPLIT LBA
' Desarrollador: Adriana Armati
' Descripci�n: Se elimina el acceso a RRHH
' ------------------------------------------------------------------------------
' Fecha de Modificaci�n: 02/05/2012
' Ticket: 731841
' Desarrollador: Mart�n Cabrera
' Descripci�n: Se recuperan legajos tambien del AIS si no se encuentra en RRHH
' ------------------------------------------------------------------------------
' COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2010. 
' ALL RIGHTS RESERVED
' 
' This software is only to be used for the purpose for which it has been provided.
' No part of it is to be reproduced, disassembled, transmitted, stored in a 
' retrieval system or translated in any human or computer language in any way or
' for any other purposes whatsoever without the prior written consent of the Hong
' Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
' serious civil and criminal offence, which can result in heavy fines and payment 
' of substantial damages.
' 
' Nombre del Fuente: XMLHTTPMsg.asp
' 
' Fecha de Creaci�n: 15/07/2010
' 
' PPcR: 50055/6010661
' 
' Desarrollador: Gabriel D'Agnone
' 
' Descripci�n: Desarrollo de funciones necesarias para la carga din�mica de combos 
'			   con datos del AIS en la aplicaci�n.
' 
' ------------------------------------------------------------------------------
%>

<%
'


Dim mvarXMLHTTP_FUNCION
Dim mvarXMLHTTP_REQUEST
Dim    mvarXMLHTTP_ACTION

Dim mobjXMLhttpRequest
Dim mobjXMLhttpResponse

Dim mvarRequestComboDinamico
Dim mvarResponseComboDinamico




	mvarXMLHTTP_FUNCION = Request("FUNCION")
	mvarXMLHTTP_REQUEST = Request("RequestXMLHTTP")
	
	mvarXMLHTTP_REQUEST = replace(mvarXMLHTTP_REQUEST,"~~","&")

	
	mvarXMLHTTP_ACTION = Request("actionCode")
	
	'Response.Write(mvarXMLHTTP_FUNCION & mvarXMLHTTP_REQUEST )
	        
		
    Set mobjXMLhttpRequest = Server.CreateObject("MSXML2.DOMDocument")
	    mobjXMLhttpRequest.async = false
	    mobjXMLhttpRequest.loadXML(mvarXMLHTTP_REQUEST)
			
    If Trim(mvarXMLHTTP_FUNCION) <> "" Then
        
        Response.ContentType = "text/xml"
        Response.charset     = "iso-8859-1"
       
       
Function EncodeBase64(wvarXMLTexto)
    wvarXMLTexto=Replace(wvarXMLTexto,"~~", chr(38))
			    
    Set objStream = Server.CreateObject("ADODB.Stream")
    objStream.CharSet="Windows-1252":objStream.Mode= 0 :objStream.Type = 2
    objStream.Open
    objStream.WriteText wvarXMLTexto
    objStream.Position = 0
    objStream.Type = 1
    
    Set XMLBase = Server.CreateObject("MSXML2.DOMDocument")
        XMLBase.async = False
    XMLBase.loadXML  "<PDF></PDF>"
    Set xmlNode = XMLBase.createElement("Base64")
    xmlNode.dataType = "bin.base64"
    xmlNode.nodeTypedValue = objStream.Read

    strTmp = xmlNode.xml
    lngS = InStr(strTmp, ">") + 1
    lngE = InStr(lngS, strTmp, "<")
    mvarBase64 = Mid(strTmp, lngS, lngE - lngS)
    objStream.Close
    
    EncodeBase64 = mvarBase64
End Function


Function DecodeBase64(nodoPDF)


	Set XMLPdf2 = Server.CreateObject("MSXML2.DOMDocument") : 
	    XMLPdf2.async = False	
				
		XMLPdf2.loadXML  "<PDF xmlns:dt=""urn:schemas-microsoft-com:datatypes"" dt:dt=""bin.base64"">" & nodoPDF & "</PDF>" 	
				
	 		Set oNode2 = XMLPdf2.documentElement.selectSingleNode("//PDF") 	 		
	 	
	 		Set objStream = Server.CreateObject("ADODB.Stream") 	
	 			
			objStream.CharSet="Windows-1252":objStream.Mode= 0 :objStream.Type = 1 	
				
			objStream.Open
					
			objStream.Write oNode2.nodeTypedValue
					
			objStream.Position = 0				
		
			InStr =objStream.Read	
			lngS = InStr(strTmp, ">") + 1
            lngE = InStr(lngS, strTmp, "<")
             
             mvarBase64 = Mid(strTmp, lngS, lngE - lngS)
             
             DecodeBase64 = mvarBase64
				
			objStream.Close


End Function

        

        Select Case (mvarXMLHTTP_FUNCION)
				
			'*******************************************************************************
			
			
				
			
			case "mensajeMQ"
		
			
			Call cmdp_ExecuteTrn(mvarXMLHTTP_ACTION, mvarXMLHTTP_ACTION & ".biz", mvarXMLHTTP_REQUEST, mvarResponse)
			
		
			
			Response.Write mvarResponse
				
           
            '*******************************************************************************
                    
         
          case "mensajeSQL"
           
          	
			
		        Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", mvarXMLHTTP_REQUEST, mvarResponse)
          
          
                Response.Write mvarResponse
          
           
                
         
                
               '*******************************************************************************
           case "ValidarVendedorLegajo"
            	
	            mvarBanco     = Request("BANCOCOD")
	            mvarSucursal  = Request("SUCURCOD")
	            mvarLegajo    = Request("LEGAJNUM")	
	            mvarGrupoHSBC = Request("GRUPOHSBC")	
            	
	            If mvarGrupoHSBC = "S" Then
		            'Validaci�n si es empleado del grupo
		            mvarRequest= "<Request>" 
		            mvarRequest = mvarRequest & "<DEFINICION>Consulta_LegajoRRHH.xml</DEFINICION>" 
		            mvarRequest = mvarRequest & "<LEGAJO>" & mvarLegajo & "</LEGAJO>"
		            mvarRequest = mvarRequest & "<EMPRESA>0</EMPRESA>"
		            mvarRequest = mvarRequest & "</Request>"       

                    Call cmdp_ExecuteTrn("lbaw_OVSQLGen", _
                                         "lbaw_OVSQLGen.biz", _
                                         mvarRequest, _
                                         mvarResponse)

		            Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
			            mobjXMLDoc.async = False				
            														
		            Call mobjXMLDoc.loadXML(mvarResponse)
            															
		            If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
			            mvarResultado = mobjXMLDoc.xml
			            Set mobjXMLDoc = Nothing
		            Else
			            mvarResultado "ERROR"
		            End If		
                  	
      	            Response.Write mvarResponse
                  	
	            Else
		            'usar la variable value como valor para el filtro
		            mvarRequest = "<Request>"
		            mvarRequest = mvarRequest & "<DEFINICION>1019_LegajoempleadoBanco.xml</DEFINICION>"
		            mvarRequest = mvarRequest & "<BANCOCOD>" & mvarBanco & "</BANCOCOD>"
		            mvarRequest = mvarRequest & "<SUCURCOD>" & mvarSucursal & "</SUCURCOD>"
		            mvarRequest = mvarRequest & "<LEGAJNUM>" & mvarLegajo & "</LEGAJNUM>"
		            mvarRequest = mvarRequest & "</Request>"
            	
		            Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ","lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
		            Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
			            mobjXMLDoc.async = False				
            														
		            Call mobjXMLDoc.loadXML(mvarResponse)
            															
		            If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
			            mvarResultado = mobjXMLDoc.xml
			            Set mobjXMLDoc = Nothing
		            Else
			            mvarResultado "ERROR"
		            End If
            	
		            Response.Write mvarResultado
            	
	            End If
	            
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	            
               Case "ValidarVendedorLegajoLBA"
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                
                    mvarBanco     = Request("BANCOCOD")
                    mvarSucursal  = Request("SUCURCOD")
                    mvarLegajo    = Request("LEGAJNUM")	
                    mvarGrupoHSBC = Request("GRUPOHSBC")	
                    mvarRespLeg   = "<LEGAJOS>"
                	
                    If mvarGrupoHSBC = "S" Then
                            'Adriana 21-08-12 Se elimina el acceso a la base de RRHH
	                    'Validaci�n si es empleado del grupo
	                    'mvarRequest= "<Request>" 
	                    'mvarRequest = mvarRequest & "<DEFINICION>Consulta_LegajoRRHH.xml</DEFINICION>" 
	                    'mvarRequest = mvarRequest & "<LEGAJO>" & mvarLegajo & "</LEGAJO>"
	                    'mvarRequest = mvarRequest & "<EMPRESA>0</EMPRESA>"
	                    'mvarRequest = mvarRequest & "</Request>"       

                            'Call cmdp_ExecuteTrn("lbaw_OVSQLGen", _
                            '                 "lbaw_OVSQLGen.biz", _
                            '                 mvarRequest, _
                            '                 mvarResponse)

	                    'Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
		            '        mobjXMLDoc.async = False				
                	    '													
	                    'Call mobjXMLDoc.loadXML(mvarResponse)
                															
	                    'If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
	                    '    If Ucase(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text) = "TRUE"  Then
                            '    For Each mobjLegajos in mobjXMLDoc.selectNodes("//LEGAJOS/LEGAJO")
                            '      if mobjLegajos.selectSingleNode("COD_EMP").Text <> "3" then
                            '            mvarRespLeg =  mvarRespLeg & "<LEGAJO>"
                            '            mvarRespLeg =  mvarRespLeg & "<LEG>" & mvarLegajo & "</LEG>"
                            '            mvarRespLeg =  mvarRespLeg & "<COD_EMP>" & mobjLegajos.selectSingleNode("COD_EMP").Text & "</COD_EMP>"
                            '            mvarRespLeg =  mvarRespLeg & "<NOM_EMP>" & mobjLegajos.selectSingleNode("NOM_EMP").Text & "</NOM_EMP>"
                            '            mvarRespLeg =  mvarRespLeg & "<APE>" & mobjLegajos.selectSingleNode("APE").Text & "</APE>"
                            '            mvarRespLeg =  mvarRespLeg & "<NOM>" & mobjLegajos.selectSingleNode("NOM").Text & "</NOM>"
                            '            mvarRespLeg =  mvarRespLeg & "<TIP_EMP>" & mobjLegajos.selectSingleNode("TIP_EMP").Text & "</TIP_EMP>"
                            '            mvarRespLeg =  mvarRespLeg & "</LEGAJO>"
                            '      End if
                            '    Next
                            'End if
                            'End if            

                        Set mobjXMLDoc = Nothing
                        mvarRequest = "<Request>"
                        mvarRequest = mvarRequest & "<DEFINICION>1027_LegajoempleadoLba.xml</DEFINICION>"
                        mvarRequest = mvarRequest & "<BANCOCOD>" & mvarBanco & "</BANCOCOD>"
                        mvarRequest = mvarRequest & "<SUCURCOD>3</SUCURCOD>" 'Se manda 3 en sucursal
                        mvarRequest = mvarRequest & "<LEGAJNUM>" & mvarLegajo & "</LEGAJNUM>"
                        mvarRequest = mvarRequest & "</Request>"

                        Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ","lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
                        Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
                        mobjXMLDoc.async = False				
                													
                        Call mobjXMLDoc.loadXML(mvarResponse)
                        If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
                            If Ucase(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text) = "TRUE"  Then
                                mvarRespLeg =  mvarRespLeg & "<LEGAJO>"
                                mvarRespLeg =  mvarRespLeg & "<LEG>" & mvarLegajo & "</LEG>"
                                mvarRespLeg =  mvarRespLeg & "<COD_EMP>3</COD_EMP>"
                                mvarRespLeg =  mvarRespLeg & "<NOM_EMP></NOM_EMP>"
                                mvarRespLeg =  mvarRespLeg & "<APE>" & mobjXMLDoc.selectSingleNode("//CLIENAP1").Text & " " & mobjXMLDoc.selectSingleNode("//CLIENAP2").Text & "</APE>"
                                mvarRespLeg =  mvarRespLeg & "<NOM>" & mobjXMLDoc.selectSingleNode("//CLIENNOM").Text & "</NOM>"
                                mvarRespLeg =  mvarRespLeg & "<TIP_EMP>E</TIP_EMP>"
                                mvarRespLeg =  mvarRespLeg & "</LEGAJO>"
                          End if
                        End if
                        
                        'Adriana 21-8-12 Se buscan los legajos del banco en el AIS
                        Set mobjXMLDoc = Nothing
                        mvarRequest = "<Request>"
                        mvarRequest = mvarRequest & "<DEFINICION>1027_LegajoempleadoLba.xml</DEFINICION>"
                        mvarRequest = mvarRequest & "<BANCOCOD>" & mvarBanco & "</BANCOCOD>"
                        mvarRequest = mvarRequest & "<SUCURCOD>2</SUCURCOD>" 'Se manda 3 en sucursal
                        mvarRequest = mvarRequest & "<LEGAJNUM>" & mvarLegajo & "</LEGAJNUM>"
                        mvarRequest = mvarRequest & "</Request>"

                        Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ","lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
                        Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
                        mobjXMLDoc.async = False				
                													
                        Call mobjXMLDoc.loadXML(mvarResponse)
                        If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
                            If Ucase(mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text) = "TRUE"  Then
                                mvarRespLeg =  mvarRespLeg & "<LEGAJO>"
                                mvarRespLeg =  mvarRespLeg & "<LEG>" & mvarLegajo & "</LEG>"
                                mvarRespLeg =  mvarRespLeg & "<COD_EMP>2</COD_EMP>"
                                mvarRespLeg =  mvarRespLeg & "<NOM_EMP></NOM_EMP>"
                                mvarRespLeg =  mvarRespLeg & "<APE>" & mobjXMLDoc.selectSingleNode("//CLIENAP1").Text & " " & mobjXMLDoc.selectSingleNode("//CLIENAP2").Text & "</APE>"
                                mvarRespLeg =  mvarRespLeg & "<NOM>" & mobjXMLDoc.selectSingleNode("//CLIENNOM").Text & "</NOM>"
                                mvarRespLeg =  mvarRespLeg & "<TIP_EMP>E</TIP_EMP>"
                                mvarRespLeg =  mvarRespLeg & "</LEGAJO>"
                                else
       			' Si no hay un plataformista con el legajo ingresado propone informarlo como nuevo
       			' Adriana 20-08-2012      
               			mvarRespLeg =  mvarRespLeg & "<LEGAJO>"
                		mvarRespLeg =  mvarRespLeg & "<LEG>" & mvarLegajo & "</LEG>"
                		mvarRespLeg =  mvarRespLeg & "<COD_EMP>2</COD_EMP>"
                		mvarRespLeg =  mvarRespLeg & "<NOM_EMP></NOM_EMP>"
                		mvarRespLeg =  mvarRespLeg & "<APE>NUEVO </APE>"
                		mvarRespLeg =  mvarRespLeg & "<NOM>PLATAFORMISTA</NOM>"
                		mvarRespLeg =  mvarRespLeg & "<TIP_EMP>E</TIP_EMP>"
                		mvarRespLeg =  mvarRespLeg & "</LEGAJO>"
                          End if
                        End if
                        
                        
                        mvarRespLeg = mvarRespLeg & "</LEGAJOS>"
                        
                        'if mvarRespLeg = "<LEGAJOS></LEGAJOS>" Then
                        '    mvarRespLeg = "ERROR"
	                    'End if        		              	
                        'mvarResultado = mobjXMLDoc.xml
                        Set mobjXMLDoc = Nothing

  	                    ValidarVendedorLegajoLBA = mvarRespLeg
  	                    
  	                    Response.Write ValidarVendedorLegajoLBA
                      	
                    Else
	                    'usar la variable value como valor para el filtro
	                    mvarRequest = "<Request>"
	                    mvarRequest = mvarRequest & "<DEFINICION>1019_LegajoempleadoBanco.xml</DEFINICION>"
	                    mvarRequest = mvarRequest & "<BANCOCOD>" & mvarBanco & "</BANCOCOD>"
	                    mvarRequest = mvarRequest & "<SUCURCOD>" & mvarSucursal & "</SUCURCOD>"
	                    mvarRequest = mvarRequest & "<LEGAJNUM>" & mvarLegajo & "</LEGAJNUM>"
	                    mvarRequest = mvarRequest & "</Request>"
                	
	                    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ","lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
	                    Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
		                    mobjXMLDoc.async = False				
                														
	                    Call mobjXMLDoc.loadXML(mvarResponse)
                															
	                    If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
	                        mvarRespLeg =  mvarRespLeg & "<LEGAJO>"
                            mvarRespLeg =  mvarRespLeg & "<LEG>" & mvarLegajo & "</LEG>"
                            mvarRespLeg =  mvarRespLeg & "<COD_EMP>3</COD_EMP>"
                            mvarRespLeg =  mvarRespLeg & "<NOM_EMP></NOM_EMP>"
                            mvarRespLeg =  mvarRespLeg & "<APE>" & mobjXMLDoc.selectSingleNode("//CLIENAP1").Text & " " & mobjXMLDoc.selectSingleNode("//CLIENAP2").Text & "</APE>"
                            mvarRespLeg =  mvarRespLeg & "<NOM>" & mobjXMLDoc.selectSingleNode("//CLIENNOM").Text & "</NOM>"
                            mvarRespLeg =  mvarRespLeg & "<TIP_EMP>E</TIP_EMP>"
                            mvarRespLeg =  mvarRespLeg & "</LEGAJO>"
		                    mvarResultado = mobjXMLDoc.xml
                		
	                    End If
                	
	                    mvarRespLeg = mvarRespLeg & "</LEGAJOS>"
                        
                        'if mvarRespLeg = "<LEGAJOS></LEGAJOS>" Then
                        '    mvarRespLeg = "ERROR"
	                    'End if        		              	
                        'mvarResultado = mobjXMLDoc.xml
                        Set mobjXMLDoc = Nothing

  	                    ValidarVendedorLegajoLBA = mvarRespLeg
  	                    
  	                    Response.Write ValidarVendedorLegajoLBA
                	
                    End If
                    	                
	        '*******************************************************************************
	       
	        Case "GUARDARCLIENTE"
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                '
                If (Request.Form("IDCLIENTE") = "" Or Request.Form("IDCLIENTE") = "0") Then
                    '~~~~~~~~~~~~~~~
                    'Proceso de Alta
                    '~~~~~~~~~~~~~~~
                    '
                    '~~~~~~~~
                    'Clientes 
                    '~~~~~~~~
			        mvarRequest = "<Request>"
			        mvarRequest = mvarRequest & "<USUARCOD>" & Request.form("USUARCOD") & "</USUARCOD>"
			        mvarRequest = mvarRequest & "<CLIENSEC>" & Request.Form("IDCLIENTE")& "</CLIENSEC>" 
			        mvarRequest = mvarRequest & "<NUMEDOCU>" & Request.form("NUMEDOCU") & "</NUMEDOCU>" 
			        mvarRequest = mvarRequest & "<TIPODOCU>" & Request.form("TIPODOCU") & "</TIPODOCU>"
			        mvarRequest = mvarRequest & "<CLIENAP1><![CDATA[" & UCase(replace(Request.form("CLIENAP1"),"~~","&")) & "]]></CLIENAP1>"
			        mvarRequest = mvarRequest & "<CLIENAP2><![CDATA[" & UCase(replace(Request.form("CLIENAP2"),"~~","&")) & "]]></CLIENAP2>"
			        mvarRequest = mvarRequest & "<CLIENNOM><![CDATA[" & UCase(replace(Request.form("CLIENNOM"),"~~","&")) & "]]></CLIENNOM>"
			        mvarRequest = mvarRequest & "<NACIMANN>" & Request.form("NACIMANN") &"</NACIMANN>"
			        mvarRequest = mvarRequest & "<NACIMMES>" & Request.form("NACIMMES") &"</NACIMMES>"
			        mvarRequest = mvarRequest & "<NACIMDIA>" & Request.form("NACIMDIA") &"</NACIMDIA>"
			        mvarRequest = mvarRequest & "<CLIENSEX>" & Request.form("CLIENSEX") &"</CLIENSEX>"
			        mvarRequest = mvarRequest & "<CLIENEST>" & Request.form("CLIENEST") &"</CLIENEST>"
			        mvarRequest = mvarRequest & "<PAISSCOD>" & Request.form("PAISSCOD") &"</PAISSCOD>"	
			        mvarRequest = mvarRequest & "<IDIOMCOD></IDIOMCOD>"     ' ? 
			        mvarRequest = mvarRequest & "<NUMHIJOS>0</NUMHIJOS>"    ' ? 
			        mvarRequest = mvarRequest & "<EFECTANN>" & Year(Now()) & "</EFECTANN>"	
			        mvarRequest = mvarRequest & "<EFECTMES>" & Month(Now()) & "</EFECTMES>"
			        mvarRequest = mvarRequest & "<EFECTDIA>" & Day(Now()) & "</EFECTDIA>"
			        mvarRequest = mvarRequest & "<CLIENTIP>" & Request.form("CLIENTIP") & "</CLIENTIP>"
			        mvarRequest = mvarRequest & "<CLIENFUM> </CLIENFUM>"    'Fumardor o no
			        mvarRequest = mvarRequest & "<CLIENDOZ> </CLIENDOZ>"    'Diestro o zurdo
			        mvarRequest = mvarRequest & "<ABRIDTIP> </ABRIDTIP>"    ' ?
			        mvarRequest = mvarRequest & "<ABRIDNUM> </ABRIDNUM>"    ' ?
			        mvarRequest = mvarRequest & "<CLIENORG> </CLIENORG>"    ' ?
			        mvarRequest = mvarRequest & "<PERSOTIP>" &  Request.Form("PERSOTIP") & "</PERSOTIP>" 
			        mvarRequest = mvarRequest & "<DOMICSEC>1</DOMICSEC>"    ' ?
			        mvarRequest = mvarRequest & "<CLIENCLA> </CLIENCLA>"    ' ?
			        mvarRequest = mvarRequest & "<FALLEANN>0</FALLEANN>"    'A�o de fallecimiento
			        mvarRequest = mvarRequest & "<FALLEMES>0</FALLEMES>"    'Mes de fallecimiento
			        mvarRequest = mvarRequest & "<FALLEDIA>0</FALLEDIA>"    'Dia de fallecimiento
			        mvarRequest = mvarRequest & "<FBAJAANN>0</FBAJAANN>"    'A�o de baja
			        mvarRequest = mvarRequest & "<FBAJAMES>0</FBAJAMES>"    'Mes de baja
			        mvarRequest = mvarRequest & "<FBAJADIA>0</FBAJADIA>"    'Dia de baja
			        mvarRequest = mvarRequest & "<EMAIL>" & UCase(Request.form("EMAIL")) & "</EMAIL>"
			        mvarRequest = mvarRequest & "<AGENTCLA>" & Request.form("AGENTCLA") & "</AGENTCLA>"
			        mvarRequest = mvarRequest & "<AGENTCOD>" & Request.form("AGENTCOD") & "</AGENTCOD>"
			        mvarRequest = mvarRequest & "</Request>"
			        '
			        Call cmdp_ExecuteTrn("lbaw_PutPersona","lbaw_PutPersona.biz", UCase(mvarRequest), mvarResponsePersona)
			        Set wvarXMLGrabacion = Server.CreateObject("MSXML2.DOMDocument")
			            wvarXMLGrabacion.async = false
			        Call wvarXMLGrabacion.loadXML(mvarResponsePersona)
					'	  
			        If (Trim(mvarResponsePersona) <> "") Then 
				        If wvarXMLGrabacion.selectSingleNode("//Response/Estado/@resultado").text = "true" Then
                            mvarCLIENSEC = wvarXMLGrabacion.selectSingleNode("//Response/PERSONAID").text
                        Else
                            mvarCLIENSEC = "ERROR"
				        End If
			        Else
				        mvarCLIENSEC = "ERROR"
			        End If
			        '
			        Set wvarXMLGrabacion = Nothing
			        '
		            '~~~~~~~~~
                    'Domicilio
                    '~~~~~~~~~
                    If mvarCLIENSEC <> "ERROR" Then
                        mvarRequest = "<Request>"
                        mvarRequest = mvarRequest & "<CLIENSEC>" & mvarCLIENSEC & "</CLIENSEC>"
                        mvarRequest = mvarRequest & "<DOMICSEC>1</DOMICSEC>"
                        mvarRequest = mvarRequest & "<DOMICCAL></DOMICCAL>"
                        mvarRequest = mvarRequest & "<DOMICDOM>" & Request.Form("DOMICDOM") & "</DOMICDOM>"
                        mvarRequest = mvarRequest & "<DOMICDNU>" & Request.Form("DOMICDNU") & "</DOMICDNU>"
                        mvarRequest = mvarRequest & "<DOMICESC>" & Request.Form("DOMICESC") & "</DOMICESC>"
                        mvarRequest = mvarRequest & "<DOMICPIS>" & Request.Form("DOMICPIS") & "</DOMICPIS>"
                        mvarRequest = mvarRequest & "<DOMICPTA>" & Request.Form("DOMICPTA") & "</DOMICPTA>"
                        mvarRequest = mvarRequest & "<DOMICPOB>" & Request.Form("DOMICPOB") & "</DOMICPOB>"
                        mvarRequest = mvarRequest & "<DOMICCPO>" & Request.Form("DOMICCPO") & "</DOMICCPO>"
                        mvarRequest = mvarRequest & "<PROVICOD>" & Request.Form("PROVICOD") & "</PROVICOD>"
                        mvarRequest = mvarRequest & "<PAISSCOD>0</PAISSCOD>"
                        mvarRequest = mvarRequest & "<TELCOD>" & Request.Form("TELEFONOCOD") & "</TELCOD>"
                        mvarRequest = mvarRequest & "<TELNRO>" & Request.Form("TELEFONONRO") & "</TELNRO>"
                        mvarRequest = mvarRequest & "<PRINCIPAL>S</PRINCIPAL>"
                        mvarRequest = mvarRequest & "</Request>"
                        '
          		        Call cmdp_ExecuteTrn("lbaw_PutDomicilio", "lbaw_PutDomicilio.biz", UCase(mvarRequest), mvarResponseDomicilio)
				        Set wvarXMLGrabacion = Server.CreateObject("MSXML2.DOMDocument")
					        wvarXMLGrabacion.async = false
				        Call wvarXMLGrabacion.loadXML(mvarResponseDomicilio)
                        '
				        If (Trim(mvarResponseDomicilio) <> "") Then 
					        If wvarXMLGrabacion.selectSingleNode("//Response/Estado/@resultado").text = "false" Then
						        mvarCLIENSEC = "ERROR"
					        End If
				        Else
					        mvarCLIENSEC = "ERROR"
				        End if
				        Set wvarXMLGrabacion = Nothing
				    End If
                    '
		            '~~~~~~
                    'Cuenta
                    '~~~~~~
                    '
                    If mvarCLIENSEC <> "ERROR" And Request.Form("GRABACTA") = "S" Then
                        mvarRequest = "<Request>"
                        mvarRequest = mvarRequest & "<CLIENSEC>" & mvarCLIENSEC & "</CLIENSEC>"
                        mvarRequest = mvarRequest & "<CUENTSEC>1</CUENTSEC>"
                        mvarRequest = mvarRequest & "<TIPOCUEN></TIPOCUEN>"
                        mvarRequest = mvarRequest & "<COBROTIP>" & Request.Form("COBROTIP") & "</COBROTIP>"
                        mvarRequest = mvarRequest & "<BANCOCOD>" & Request.Form("BANCOCOD") & "</BANCOCOD>"
                        mvarRequest = mvarRequest & "<SUCURCOD>" & Request.Form("SUCURCOD") & "</SUCURCOD>"
                        mvarRequest = mvarRequest & "<CUENTDC></CUENTDC>"
                        mvarRequest = mvarRequest & "<CUENNUME>" & Request.Form("CUENNUME") & "</CUENNUME>"
                        mvarRequest = mvarRequest & "<TARJECOD>" & Request.Form("TARJECOD") & "</TARJECOD>"
                        mvarRequest = mvarRequest & "<VENCIANN>" & Request.Form("VENCIANN") & "</VENCIANN>"
                        mvarRequest = mvarRequest & "<VENCIMES>" & Request.Form("VENCIMES") & "</VENCIMES>"
                        mvarRequest = mvarRequest & "<VENCIDIA>" & Request.Form("VENCIDIA") & "</VENCIDIA>"
                        mvarRequest = mvarRequest & "</Request>"
                        '
          		        Call cmdp_ExecuteTrn("lbaw_PutCuenta", "lbaw_PutCuenta.biz", UCase(mvarRequest), mvarResponseCuenta)
				        Set wvarXMLGrabacion = Server.CreateObject("MSXML2.DOMDocument")
					        wvarXMLGrabacion.async = false
				        Call wvarXMLGrabacion.loadXML(mvarResponseCuenta)
                        '
				        If (Trim(mvarResponseCuenta) <> "") Then 
					        If wvarXMLGrabacion.selectSingleNode("//Response/Estado/@resultado").text = "false" Then
						        mvarCLIENSEC = "ERROR"
					        End If
				        Else
					        mvarCLIENSEC = "ERROR"
				        End if
				        Set wvarXMLGrabacion = Nothing
                    End If
			    Else
			        '~~~~~~~~~~~~~~~~~~~~~~~~
                    'Proceso de Actualizaci�n
                    '~~~~~~~~~~~~~~~~~~~~~~~~
                    '                   
			        mvarRequest = "<Request>"
			        '~~~~~~~~
                    'Clientes 
                    '~~~~~~~~
			        mvarRequest = mvarRequest & "<USUARCOD>" & Request.form("USUARCOD") & "</USUARCOD>"
			        mvarRequest = mvarRequest & "<CLIENSEC>" & Request.Form("IDCLIENTE")& "</CLIENSEC>" 
			        mvarRequest = mvarRequest & "<NUMEDOCU>" & Request.form("NUMEDOCU") & "</NUMEDOCU>" 
			        mvarRequest = mvarRequest & "<TIPODOCU>" & Request.form("TIPODOCU") & "</TIPODOCU>"
			        mvarRequest = mvarRequest & "<CLIENAP1><![CDATA[" & UCase(replace(Request.form("CLIENAP1"),"~~","&")) & "]]></CLIENAP1>"
			        mvarRequest = mvarRequest & "<CLIENAP2><![CDATA[" & UCase(replace(Request.form("CLIENAP2"),"~~","&")) & "]]></CLIENAP2>"
			        mvarRequest = mvarRequest & "<CLIENNOM><![CDATA[" & UCase(replace(Request.form("CLIENNOM"),"~~","&")) & "]]></CLIENNOM>"
			        mvarRequest = mvarRequest & "<NACIMANN>" & Request.form("NACIMANN") &"</NACIMANN>"
			        mvarRequest = mvarRequest & "<NACIMMES>" & Request.form("NACIMMES") &"</NACIMMES>"
			        mvarRequest = mvarRequest & "<NACIMDIA>" & Request.form("NACIMDIA") &"</NACIMDIA>"
			        mvarRequest = mvarRequest & "<CLIENSEX>" & Request.form("CLIENSEX") &"</CLIENSEX>"
			        mvarRequest = mvarRequest & "<CLIENEST>" & Request.form("CLIENEST") &"</CLIENEST>"
			        mvarRequest = mvarRequest & "<PAISSCOD>" & Request.form("PAISSCOD") &"</PAISSCOD>"	
			        mvarRequest = mvarRequest & "<IDIOMCOD></IDIOMCOD>"     ' ? 
			        mvarRequest = mvarRequest & "<NUMHIJOS>0</NUMHIJOS>"    ' ? 
			        mvarRequest = mvarRequest & "<EFECTANN>" & Year(Now()) & "</EFECTANN>"	
			        mvarRequest = mvarRequest & "<EFECTMES>" & Month(Now()) & "</EFECTMES>"
			        mvarRequest = mvarRequest & "<EFECTDIA>" & Day(Now()) & "</EFECTDIA>"
			        mvarRequest = mvarRequest & "<CLIENTIP>" & Request.form("CLIENTIP") & "</CLIENTIP>"
			        mvarRequest = mvarRequest & "<CLIENFUM> </CLIENFUM>"    'Fumardor o no
			        mvarRequest = mvarRequest & "<CLIENDOZ> </CLIENDOZ>"    'Diestro o zurdo
			        mvarRequest = mvarRequest & "<ABRIDTIP> </ABRIDTIP>"    ' ?
			        mvarRequest = mvarRequest & "<ABRIDNUM> </ABRIDNUM>"    ' ?
			        mvarRequest = mvarRequest & "<CLIENORG> </CLIENORG>"    ' ?
			        mvarRequest = mvarRequest & "<PERSOTIP>" &  Request.Form("PERSOTIP") & "</PERSOTIP>" 
			        mvarRequest = mvarRequest & "<DOMICSEC>1</DOMICSEC>"    ' ?
			        mvarRequest = mvarRequest & "<CLIENCLA> </CLIENCLA>"    ' ?
			        mvarRequest = mvarRequest & "<FALLEANN>0</FALLEANN>"    'A�o de fallecimiento
			        mvarRequest = mvarRequest & "<FALLEMES>0</FALLEMES>"    'Mes de fallecimiento
			        mvarRequest = mvarRequest & "<FALLEDIA>0</FALLEDIA>"    'Dia de fallecimiento
			        mvarRequest = mvarRequest & "<FBAJAANN>0</FBAJAANN>"    'A�o de baja
			        mvarRequest = mvarRequest & "<FBAJAMES>0</FBAJAMES>"    'Mes de baja
			        mvarRequest = mvarRequest & "<FBAJADIA>0</FBAJADIA>"    'Dia de baja
			        mvarRequest = mvarRequest & "<EMAIL>" & UCase(Request.form("EMAIL")) & "</EMAIL>"
			        mvarRequest = mvarRequest & "<AGENTCLA>" & Request.form("AGENTCLA") & "</AGENTCLA>"
			        mvarRequest = mvarRequest & "<AGENTCOD>" & Request.form("AGENTCOD") & "</AGENTCOD>"
			        '~~~~~~~~~
                    'Domicilio
                    '~~~~~~~~~
                    mvarRequest = mvarRequest & "<DOMICILIOS><DOMICILIO>"
			        mvarRequest = mvarRequest & "<CLIENSEC>" & Request.Form("IDCLIENTE")& "</CLIENSEC>"
                    mvarRequest = mvarRequest & "<DOMICSEC>1</DOMICSEC>"
                    mvarRequest = mvarRequest & "<DOMICCAL></DOMICCAL>"
                    mvarRequest = mvarRequest & "<DOMICDOM>" & Request.Form("DOMICDOM") & "</DOMICDOM>"
                    mvarRequest = mvarRequest & "<DOMICDNU>" & Request.Form("DOMICDNU") & "</DOMICDNU>"
                    mvarRequest = mvarRequest & "<DOMICESC>" & Request.Form("DOMICESC") & "</DOMICESC>"
                    mvarRequest = mvarRequest & "<DOMICPIS>" & Request.Form("DOMICPIS") & "</DOMICPIS>"
                    mvarRequest = mvarRequest & "<DOMICPTA>" & Request.Form("DOMICPTA") & "</DOMICPTA>"
                    mvarRequest = mvarRequest & "<DOMICPOB>" & Request.Form("DOMICPOB") & "</DOMICPOB>"
                    mvarRequest = mvarRequest & "<DOMICCPO>" & Request.Form("DOMICCPO") & "</DOMICCPO>"
                    mvarRequest = mvarRequest & "<PROVICOD>" & Request.Form("PROVICOD") & "</PROVICOD>"
                    mvarRequest = mvarRequest & "<PAISSCOD>0</PAISSCOD>"
                    mvarRequest = mvarRequest & "<TELCOD>" & Request.Form("TELEFONOCOD") & "</TELCOD>"
                    mvarRequest = mvarRequest & "<TELNRO>" & Request.Form("TELEFONONRO") & "</TELNRO>"
                    mvarRequest = mvarRequest & "<PRINCIPAL>S</PRINCIPAL>"
                    mvarRequest = mvarRequest & "</DOMICILIO></DOMICILIOS>"
                    '~~~~~~
                    'Cuenta
                    '~~~~~~
                    If Request.Form("GRABACTA") = "S" Then
                        mvarRequest = mvarRequest & "<CUENTAS><CUENTA>"
			            mvarRequest = mvarRequest & "<CLIENSEC>" & Request.Form("IDCLIENTE")& "</CLIENSEC>"
                        mvarRequest = mvarRequest & "<CUENTSEC>1</CUENTSEC>"
                        mvarRequest = mvarRequest & "<TIPOCUEN></TIPOCUEN>"
                        mvarRequest = mvarRequest & "<COBROTIP>" & Request.Form("COBROTIP") & "</COBROTIP>"
                        mvarRequest = mvarRequest & "<BANCOCOD>" & Request.Form("BANCOCOD") & "</BANCOCOD>"
                        mvarRequest = mvarRequest & "<SUCURCOD>" & Request.Form("SUCURCOD") & "</SUCURCOD>"
                        mvarRequest = mvarRequest & "<CUENTDC></CUENTDC>"
                        mvarRequest = mvarRequest & "<CUENNUME>" & Request.Form("CUENNUME") & "</CUENNUME>"
                        mvarRequest = mvarRequest & "<TARJECOD>" & Request.Form("TARJECOD") & "</TARJECOD>"
                        mvarRequest = mvarRequest & "<VENCIANN>" & Request.Form("VENCIANN") & "</VENCIANN>"
                        mvarRequest = mvarRequest & "<VENCIMES>" & Request.Form("VENCIMES") & "</VENCIMES>"
                        mvarRequest = mvarRequest & "<VENCIDIA>" & Request.Form("VENCIDIA") & "</VENCIDIA>"
                        mvarRequest = mvarRequest & "</CUENTA></CUENTAS>"
                    End If
                    '
                    mvarRequest = mvarRequest & "</Request>"
                    'response.Write mvarRequest
                    'response.End
                    '
			        Call cmdp_ExecuteTrn("lbaw_UpdPersona","lbaw_UpdPersona.biz", UCase(mvarRequest), mvarResponsePersona)
			        Set wvarXMLGrabacion = Server.CreateObject("MSXML2.DOMDocument")
			            wvarXMLGrabacion.async = false
			        Call wvarXMLGrabacion.loadXML(mvarResponsePersona)
					'
			        If (Trim(mvarResponsePersona) <> "") Then 
				        If wvarXMLGrabacion.selectSingleNode("//Response/Estado/@resultado").text = "true" Then
					        mvarCLIENSEC = Request.Form("IDCLIENTE")
					    Else
						    mvarCLIENSEC = "ERROR"
				        End If
			        Else
				        mvarCLIENSEC = "ERROR"
			        End If
			        '
			        Set wvarXMLGrabacion = Nothing
			    End If
                '
    			Response.Write mvarCLIENSEC
	      
	      
	      '********************************************************************************  

           Case "GUARDARIMPRESO"
           '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
			mvarREQUESTIMPRESO = replace(Request.Form("REQUESTIMPRESO"),"~~","&")
			
			mvarRequest = "<Request>"
            mvarRequest = mvarRequest & "<DEFINICION>P_PROD_OPER_IMPRESOS_INSERT.XML</DEFINICION>"
            mvarRequest = mvarRequest & "<RAMOPCOD>" & Request.Form("RAMOPCOD") & "</RAMOPCOD>"
            mvarRequest = mvarRequest & "<POLIZANN>0</POLIZANN>"
            mvarRequest = mvarRequest & "<POLIZSEC>0</POLIZSEC>"
            mvarRequest = mvarRequest & "<CERTIPOL>" & Request.Form("CERTIPOL") & "</CERTIPOL>"
            mvarRequest = mvarRequest & "<CERTIANN>0</CERTIANN>"
            mvarRequest = mvarRequest & "<CERTISEC>" & Request.Form("CERTISEC") & "</CERTISEC>"
            mvarRequest = mvarRequest & "<SUPLENUM>0</SUPLENUM>"
	        mvarRequest = mvarRequest & "<XML_COTI_P>" & mvarREQUESTIMPRESO & "</XML_COTI_P>"
	        mvarRequest = mvarRequest & "<XML_COTI_C>" & mvarREQUESTIMPRESO & "</XML_COTI_C>"
	        mvarRequest = mvarRequest & "<XML_SOLI/>"
		    mvarRequest = mvarRequest & "<XML_CERTI/>"
		    mvarRequest = mvarRequest & "<XML_AUTDEB/>"
		    mvarRequest = mvarRequest & "<XML_TARJ_CIR/>"
		    mvarRequest = mvarRequest & "<XML_CLAU_SUB/>"
            mvarRequest = mvarRequest & "</Request>"
            '
	        Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", mvarRequest, mvarResponse)
	        Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
		        mobjXMLDoc.async = false
	        Call mobjXMLDoc.loadXML(mvarResponse)
            '
	        If (Trim(mvarResponse) <> "") Then 
		        If mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text = "true" Then
                    mvarResImpreso = mobjXMLDoc.selectSingleNode("//RESULT").text
                Else
                    mvarResImpreso = "ERROR"
	            End If
	        Else
		        mvarResImpreso = "ERROR"
	        End if
	        Set mobjXMLDoc = Nothing
			'
    		Response.Write mvarResImpreso

'*********************************************************
           Case "GUARDARCLIENTE-bak"
		
			'Guarda el Cliente
			
			dim mvarCLIENTIP
			mvarCLIENTIP = Request.form("CLIENTIP")
			if mvarCLIENTIP = "" then mvarCLIENTIP = "00"
			
			mvarRequest = "<Request><USUARCOD>" & Request.form("USUARCOD") & "</USUARCOD>"
			mvarRequest = mvarRequest & "<CLIENSEC>" & Request.Form("IDCLIENTE") & "</CLIENSEC>" 
			mvarRequest = mvarRequest & "<NUMEDOCU>" & Request.form("NUMEDOCU") & "</NUMEDOCU>" 
			mvarRequest = mvarRequest & "<TIPODOCU>" & Request.form("TIPODOCU") & "</TIPODOCU>"
			mvarRequest = mvarRequest & "<CLIENAP1>" & UCase(Request.form("CLIENAP1")) & "</CLIENAP1>"
			mvarRequest = mvarRequest & "<CLIENAP2>" & UCase(Request.form("CLIENAP2")) & " </CLIENAP2>"
			mvarRequest = mvarRequest & "<CLIENNOM>" & UCase(Request.form("CLIENNOM")) & "</CLIENNOM>"
			mvarRequest = mvarRequest & "<NACIMANN>" & UCase(Request.form("NACIMANN")) & "</NACIMANN>"
			mvarRequest = mvarRequest & "<NACIMMES>" & UCase(Request.form("NACIMMES")) & "</NACIMMES>"
			mvarRequest = mvarRequest & "<NACIMDIA>" & UCase(Request.form("NACIMDIA")) & "</NACIMDIA>"
			mvarRequest = mvarRequest & "<CLIENSEX>" & UCase(Request.form("CLIENSEX")) & "</CLIENSEX>"
			mvarRequest = mvarRequest & "<CLIENEST>" & UCase(Request.form("CLIENEST")) & "</CLIENEST>"
			mvarRequest = mvarRequest & "<PAISSCOD>" & UCase(Request.form("PAISSCOD")) & "</PAISSCOD>"	
			mvarRequest = mvarRequest & "<IDIOMCOD></IDIOMCOD>" ' ? 
			mvarRequest = mvarRequest & "<NUMHIJOS>0</NUMHIJOS>" ' ? 
			mvarRequest = mvarRequest & "<EFECTANN>" & Year(Now()) & "</EFECTANN>"	
			mvarRequest = mvarRequest & "<EFECTMES>" & Month(Now()) & "</EFECTMES>"
			mvarRequest = mvarRequest & "<EFECTDIA>" & Day(Now()) & "</EFECTDIA>"
			mvarRequest = mvarRequest & "<CLIENTIP>" & mvarCLIENTIP & "</CLIENTIP>"'Persona fisica
			mvarRequest = mvarRequest & "<CLIENFUM> </CLIENFUM>" 'Fumardor o no
			mvarRequest = mvarRequest & "<CLIENDOZ> </CLIENDOZ>" 'Diestro o zurdo
			mvarRequest = mvarRequest & "<ABRIDTIP> </ABRIDTIP>" ' ?
			mvarRequest = mvarRequest & "<ABRIDNUM> </ABRIDNUM>" ' ?
			mvarRequest = mvarRequest & "<CLIENORG> </CLIENORG>" ' ?
			mvarRequest = mvarRequest & "<PERSOTIP>" &  Request.Form("PERSOTIP") & "</PERSOTIP>" 
			mvarRequest = mvarRequest & "<DOMICSEC>1</DOMICSEC>" ' ?
			mvarRequest = mvarRequest & "<CLIENCLA> </CLIENCLA>" ' ?
			mvarRequest = mvarRequest & "<FALLEANN>0</FALLEANN>" 'A�o de fallecimiento
			mvarRequest = mvarRequest & "<FALLEMES>0</FALLEMES>" 'Mes de fallecimiento
			mvarRequest = mvarRequest & "<FALLEDIA>0</FALLEDIA>" 'Dia de fallecimiento
			mvarRequest = mvarRequest & "<FBAJAANN>0</FBAJAANN>" 'A�o de baja
			mvarRequest = mvarRequest & "<FBAJAMES>0</FBAJAMES>" 'Mes de baja
			mvarRequest = mvarRequest & "<FBAJADIA>0</FBAJADIA>" 'Dia de baja
			mvarRequest = mvarRequest & "<EMAIL>" & UCase(Request.form("EMAIL")) & "</EMAIL>"
			mvarRequest = mvarRequest & "<AGENTCLA>" & Request.form("AGENTCLA") & "</AGENTCLA>"
			mvarRequest = mvarRequest & "<AGENTCOD>" & Request.form("AGENTCOD") & "</AGENTCOD></Request>"

			If (Request.Form("IDCLIENTE") <> "0" and Request.Form("IDCLIENTE") <> "") Then
				ActionCode = "lbaw_UpdPersona"
			Else
				ActionCode = "lbaw_PutPersona"
			End If
			Call cmdp_ExecuteTrn(ActionCode, _
								 ActionCode + ".biz", _
								 mvarRequest, _
								 mvarResponsePersona)

			Set wvarXMLGrabacion = Server.CreateObject("MSXML2.DOMDocument")
			wvarXMLGrabacion.async = false
			
			Call wvarXMLGrabacion.loadXML(mvarResponsePersona)
						  
			If (Trim(mvarResponsePersona) <> "") Then 
				If wvarXMLGrabacion.selectSingleNode("//Response/Estado/@resultado").text = "true" Then
					if ActionCode = "lbaw_UpdPersona" then 
						mvarCLIENSEC = Request.Form("IDCLIENTE")
					Else
						mvarCLIENSEC = wvarXMLGrabacion.selectSingleNode("//Response/PERSONAID").text						
					End If
				End If
			Else
				mvarCLIENSEC = "ERROR"
			End if
			
			Set mobjXMLDoc = Nothing
			Set wvarXMLGrabacion = Nothing
		
			'If (Request.Form("IDCLIENTE") = "") Then
			if  mvarCLIENSEC <> "" and mvarCLIENSEC <> "ERROR" then 
				' *********************************************************************************
				'Guardo el domicilio
				' *********************************************************************************
				
	           
	            mvarRequest =  "<CLIENSEC>" & mvarCLIENSEC & "</CLIENSEC>"
	            mvarRequest =  mvarRequest & "<DOMICSEC>1</DOMICSEC>"
	            mvarRequest =  mvarRequest & "<DOMICCAL></DOMICCAL>"
	            mvarRequest =  mvarRequest & "<DOMICDOM>" & Request.Form("DOMICDOM") & "</DOMICDOM>"
	            mvarRequest =  mvarRequest & "<DOMICDNU>" & Request.Form("DOMICDNU") & "</DOMICDNU>"
	            mvarRequest =  mvarRequest & "<DOMICESC>"& Request.Form("DOMICESC") & "</DOMICESC>"
	            mvarRequest =  mvarRequest & "<DOMICPIS>"& Request.Form("DOMICPIS") & "</DOMICPIS>"
	            mvarRequest =  mvarRequest & "<DOMICPTA>"& Request.Form("DOMICPTA") & "</DOMICPTA>"
	            mvarRequest =  mvarRequest & "<DOMICPOB>"& Request.Form("DOMICPOB") & "</DOMICPOB>"
	            mvarRequest =  mvarRequest & "<DOMICCPO>"& Request.Form("DOMICCPO") & "</DOMICCPO>"
	            mvarRequest =  mvarRequest & "<PROVICOD>"& Request.Form("PROVICOD") & "</PROVICOD>"
	            mvarRequest =  mvarRequest & "<PAISSCOD>0</PAISSCOD>"
	            mvarRequest =  mvarRequest & "<TELCOD>" & Request.Form("TELEFONOCOD") & "</TELCOD>"
	            mvarRequest =  mvarRequest & "<TELNRO>" & Request.Form("TELEFONONRO") & "</TELNRO>"
	            mvarRequest =  mvarRequest & "<PRINCIPAL>S</PRINCIPAL>"
          

				Call cmdp_ExecuteTrn("lbaw_PutDomicilio", _
									 "lbaw_PutDomicilio.biz", _
									 "<Request>" & UCase(mvarRequest) & "</Request>", _
									 mvarResponseDomicilio)
	
				Set wvarXMLGrabacionDomicilio = Server.CreateObject("MSXML2.DOMDocument")
					wvarXMLGrabacionDomicilio.async = false
				Call wvarXMLGrabacionDomicilio.loadXML(mvarResponseDomicilio)

				'Response.Write mvarRequest
				'Response.End 					
				
				If (Trim(mvarResponseDomicilio) <> "") Then 
					If wvarXMLGrabacionDomicilio.selectSingleNode("//Response/Estado/@resultado").text = "false" Then
						mvarCLIENSEC = "ERROR"
					End If
				Else
					mvarCLIENSEC = "ERROR"
				End if
				Set wvarXMLGrabacionDomicilio = Nothing
				
				
				
			else
			
			mvarRequest =  "<Request>" 
	            mvarRequest =  mvarRequest & "<DEFINICION>P_PROD_SIFMDOMI_UPDATE.XML</DEFINICION>"
	            mvarRequest =  mvarRequest & "<CLIENSEC>" & mvarCLIENSEC & "</CLIENSEC>"
	            mvarRequest =  mvarRequest & "<DOMICSEC>1</DOMICSEC>"
	            mvarRequest =  mvarRequest & "<DOMICCAL>" & Request.Form("DOMICCAL") & "</DOMICCAL>"
	            mvarRequest =  mvarRequest & "<DOMICDOM>" & Request.Form("DOMICDOM") & "</DOMICDOM>"
	            mvarRequest =  mvarRequest & "<DOMICDNU>" & Request.Form("DOMICDNU") & "</DOMICDNU>"
	            mvarRequest =  mvarRequest & "<DOMICESC>"& Request.Form("DOMICESC") & "</DOMICESC>"
	            mvarRequest =  mvarRequest & "<DOMICPIS>"& Request.Form("DOMICPIS") & "</DOMICPIS>"
	            mvarRequest =  mvarRequest & "<DOMICPTA>"& Request.Form("DOMICPTA") & "</DOMICPTA>"
	            mvarRequest =  mvarRequest & "<DOMICPOB>"& Request.Form("DOMICPOB") & "</DOMICPOB>"
	            mvarRequest =  mvarRequest & "<DOMICCPO>"& Request.Form("DOMICCPO") & "</DOMICCPO>"
	            mvarRequest =  mvarRequest & "<PROVICOD>"& Request.Form("PROVICOD") & "</PROVICOD>"
	            mvarRequest =  mvarRequest & "<PAISSCOD>0</PAISSCOD>"
	            mvarRequest =  mvarRequest & "<TELCOD>" & Request.Form("TELEFONOCOD") & "</TELCOD>"
	            mvarRequest =  mvarRequest & "<TELNRO>" & Request.Form("TELEFONONRO") & "</TELNRO>"
	            mvarRequest =  mvarRequest & "<PRINCIPAL>S</PRINCIPAL>"
            mvarRequest =  mvarRequest & "</Request>"
           
              Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", mvarRequest, mvarResponse)  
              
              Set wvarXMLGrabacionDomicilio = Server.CreateObject("MSXML2.DOMDocument")
					wvarXMLGrabacionDomicilio.async = false
				Call wvarXMLGrabacionDomicilio.loadXML(mvarResponse)

							
				
				If (Trim(mvarResponse) <> "") Then 
					If wvarXMLGrabacionDomicilio.selectSingleNode("//Response/Estado/@resultado").text = "false" Then
						mvarCLIENSEC = "ERROR"
					End If
				Else
					mvarCLIENSEC = "ERROR"
				End if
				Set wvarXMLGrabacionDomicilio = Nothing
				
			End If			
			'response.Write mvarResponse
			Response.Write mvarCLIENSEC
	                
	        '*******************************************************************************
              Case "GUARDARCOTIZACION"
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			mvarREQUESTCOTIZ = replace(Request.Form("REQUESTCOTIZACION"),"~~","&")
			    
			    'Impresos
			    mvarIMPRE_COTI_P   = EncodeBase64(Request.Form("IMPRE_COTI_P"))
			    mvarIMPRE_COTI_C   = EncodeBase64(Request.Form("IMPRE_COTI_C"))
			    mvarIMPRE_SOLI     = EncodeBase64(Request.Form("IMPRE_SOLI"))
			    mvarIMPRE_CERTI    = EncodeBase64(Request.Form("IMPRE_CERTI"))
			    mvarIMPRE_AUTDEB   = EncodeBase64(Request.Form("IMPRE_AUTDEB"))
			    mvarIMPRE_TARJ_CIR = EncodeBase64(Request.Form("IMPRE_TARJ_CIR"))
			    mvarIMPRE_CLAU_SUB = EncodeBase64(Request.Form("IMPRE_CLAU_SUB"))
                
			    mvarRequest = "<Request>"
	            mvarRequest = mvarRequest & "<DEFINICION>P_PROD_CON_OPERACION_GRABA.XML</DEFINICION>"
	            mvarRequest = mvarRequest & mvarREQUESTCOTIZ
	            mvarRequest = mvarRequest & "<TEMPL_COTI_P>"   & Request.Form("TEMPL_COTI_P")   & "</TEMPL_COTI_P>"
	            mvarRequest = mvarRequest & "<IMPRE_COTI_P>"   & mvarIMPRE_COTI_P               & "</IMPRE_COTI_P>"
	            mvarRequest = mvarRequest & "<TEMPL_COTI_C>"   & Request.Form("TEMPL_COTI_C")   & "</TEMPL_COTI_C>"
	            mvarRequest = mvarRequest & "<IMPRE_COTI_C>"   & mvarIMPRE_COTI_C               & "</IMPRE_COTI_C>"
	            mvarRequest = mvarRequest & "<TEMPL_SOLI>"     & Request.Form("TEMPL_SOLI")     & "</TEMPL_SOLI>"
	            mvarRequest = mvarRequest & "<IMPRE_SOLI>"     & mvarIMPRE_SOLI                 & "</IMPRE_SOLI>"
	            mvarRequest = mvarRequest & "<TEMPL_CERTI>"    & Request.Form("TEMPL_CERTI")    & "</TEMPL_CERTI>"
	            mvarRequest = mvarRequest & "<IMPRE_CERTI>"    & mvarIMPRE_CERTI                & "</IMPRE_CERTI>"
	            mvarRequest = mvarRequest & "<TEMPL_AUTDEB>"   & Request.Form("TEMPL_AUTDEB")   & "</TEMPL_AUTDEB>"
	            mvarRequest = mvarRequest & "<IMPRE_AUTDEB>"   & mvarIMPRE_AUTDEB               & "</IMPRE_AUTDEB>"
	            mvarRequest = mvarRequest & "<TEMPL_TARJ_CIR>" & Request.Form("TEMPL_TARJ_CIR") & "</TEMPL_TARJ_CIR>"
	            mvarRequest = mvarRequest & "<IMPRE_TARJ_CIR>" & mvarIMPRE_TARJ_CIR             & "</IMPRE_TARJ_CIR>"
	            mvarRequest = mvarRequest & "<TEMPL_CLAU_SUB>" & Request.Form("TEMPL_CLAU_SUB") & "</TEMPL_CLAU_SUB>"
	            mvarRequest = mvarRequest & "<IMPRE_CLAU_SUB>" & mvarIMPRE_CLAU_SUB             & "</IMPRE_CLAU_SUB>"
	            mvarRequest = mvarRequest & "</Request>" 
                			
			    Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", mvarRequest, mvarResponse)
			    Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")		
				    mobjXMLDoc.loadXML (mvarResponse)
					
			    If mobjXMLDoc.selectSingleNode("//Response/Estado[@resultado='true']") Is Nothing Then
				    mvarID_ENCABEZADO_COTIZACION = "ERROR"
			    Else
				    mvarID_ENCABEZADO_COTIZACION = mobjXMLDoc.selectSingleNode("//CERTISEC").text		
			    End If
				
			    Set mobjXMLDoc = Nothing		
          
                Response.Write mvarID_ENCABEZADO_COTIZACION
               
	        'response.Write  mvarRequest
	                
	        '*******************************************************************************

            '**************************************************************

            Case "GUARDARCOTIZACION2"
			
		
			
		'	if (Request.Form("IDENCABEZADO")="") Then
			'	mvarIsNuevaCotizacion = true
		'		mvarIdComercio = ""
		'	Else
		'		mvarIsNuevaCotizacion = false
			'	mvarIdComercio = "1"
		'	End If
			
			mvarREQUESTCOTIZ = replace(Request.Form("REQUESTCOTIZACION"),"~~","&")
			
			//Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")		
			
			//mobjXMLDoc.loadXML (mvarREQUESTCOTIZ)			
		    //Set mobjCOTIZACION	
			//mobjCOTIZACION = mobjXMLDoc.selectSingleNode("//COTIZACION").text
				
			//Set mobjXMLDoc = Nothing
			
			mvarRequest = "<Request>" 
	        mvarRequest = mvarRequest &   "<DEFINICION>P_PROD_CON_OPERACION_GRABA.XML</DEFINICION>" 
	 
	        mvarRequest = mvarRequest &   mvarREQUESTCOTIZ
	        
           mvarRequest = mvarRequest &  "</Request>" 
			
			
		
			
			Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", mvarRequest, mvarResponse)
											 
			Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")		
				mobjXMLDoc.loadXML (mvarResponse)
					
			If mobjXMLDoc.selectSingleNode("//Response/Estado[@resultado='true']") Is Nothing Then
				mvarID_ENCABEZADO_COTIZACION = "ERROR"
			Else		
				mvarID_ENCABEZADO_COTIZACION = mobjXMLDoc.selectSingleNode("//CERTISEC").text		
			End If
				
			Set mobjXMLDoc = Nothing		
          
          Response.Write mvarID_ENCABEZADO_COTIZACION
          
          'response.Write  mvarRequest
          
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Case "CAMBIARESTADO"
			'~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    		
    		   'Impresos
			    mvarIMPRE_COTI_P   = EncodeBase64(Request.Form("IMPRE_COTI_P"))
			    mvarIMPRE_COTI_C   = EncodeBase64(Request.Form("IMPRE_COTI_C"))
			    mvarIMPRE_SOLI     = EncodeBase64(Request.Form("IMPRE_SOLI"))
			    mvarIMPRE_CERTI    = EncodeBase64(Request.Form("IMPRE_CERTI"))
			    mvarIMPRE_AUTDEB   = EncodeBase64(Request.Form("IMPRE_AUTDEB"))
			    mvarIMPRE_TARJ_CIR = EncodeBase64(Request.Form("IMPRE_TARJ_CIR"))
			    mvarIMPRE_CLAU_SUB = EncodeBase64(Request.Form("IMPRE_CLAU_SUB"))
			    
    			mvarRequest = "<Request>"
    			mvarRequest = mvarRequest & "<DEFINICION>P_PROD_OPERACION_CAMBIO_ESTADO.XML</DEFINICION>"
    			mvarRequest = mvarRequest & "<RAMOPCOD>" & Request("RAMOPCOD") & "</RAMOPCOD>"
    			mvarRequest = mvarRequest & "<POLIZANN>" & Request("POLIZANN") & "</POLIZANN>"
    			mvarRequest = mvarRequest & "<POLIZSEC>" & Request("POLIZSEC") & "</POLIZSEC>"
    			mvarRequest = mvarRequest & "<CERTIPOL>" & Request("CERTIPOL") & "</CERTIPOL>"
    			mvarRequest = mvarRequest & "<CERTIANN>" & Request("CERTIANN") & "</CERTIANN>"
    			mvarRequest = mvarRequest & "<CERTISEC>" & Request("CERTISEC") & "</CERTISEC>"
    			mvarRequest = mvarRequest & "<SUPLENUM>" & Request("SUPLENUM") & "</SUPLENUM>"
    			mvarRequest = mvarRequest & "<SITUCPOL>" & Request("SITUCPOL") & "</SITUCPOL>"
    			mvarRequest = mvarRequest & "<USUA_OV>" &  Request("USUARCOD") & "</USUA_OV>"
    			mvarRequest = mvarRequest & "<USUA_INTRA></USUA_INTRA>"
    			mvarRequest = mvarRequest & "<CIRCUITO_APR>"& Request("CIRCUREV") &"</CIRCUITO_APR>"
    			mvarRequest = mvarRequest & "<ANOTACION><![CDATA["& Replace(Request("ANOTACION"),"~~","&") &"]]></ANOTACION>"
    			mvarRequest = mvarRequest & "<EJEC_PPLSOFT>"& Request("EJEC_PPLSOFT") &"</EJEC_PPLSOFT>"
    			mvarRequest = mvarRequest & "<EJEC_APENOM>"& Request("EJEC_APENOM") &"</EJEC_APENOM>"
    			mvarRequest = mvarRequest & "<EJEC_EMAIL>"& Request("EJEC_EMAIL") &"</EJEC_EMAIL>"
    			mvarRequest = mvarRequest & "<AIS_POLIZANN>"& Request("AIS_POLIZANN") &"</AIS_POLIZANN>"
    			mvarRequest = mvarRequest & "<AIS_POLIZSEC>"& Request("AIS_POLIZSEC") &"</AIS_POLIZSEC>"
    			mvarRequest = mvarRequest & "<AIS_CERTIPOL>"& Request("AIS_CERTIPOL") &"</AIS_CERTIPOL>"
    			mvarRequest = mvarRequest & "<AIS_CERTIANN>"& Request("AIS_CERTIANN") &"</AIS_CERTIANN>"
    			mvarRequest = mvarRequest & "<AIS_CERTISEC>"& Request("AIS_CERTISEC") &"</AIS_CERTISEC>"
    			mvarRequest = mvarRequest & "<AIS_SUPLENUM>"& Request("AIS_SUPLENUM") &"</AIS_SUPLENUM>"
                mvarRequest = mvarRequest & "<IMPREGRABAR>" & Request.Form("IMPREGRABAR") & "</IMPREGRABAR>"
                mvarRequest = mvarRequest & "<TEMPL_COTI_P>"   & Request.Form("TEMPL_COTI_P")   & "</TEMPL_COTI_P>"
                mvarRequest = mvarRequest & "<IMPRE_COTI_P>"   & mvarIMPRE_COTI_P               & "</IMPRE_COTI_P>"
                mvarRequest = mvarRequest & "<TEMPL_COTI_C>"   & Request.Form("TEMPL_COTI_C")   & "</TEMPL_COTI_C>"
                mvarRequest = mvarRequest & "<IMPRE_COTI_C>"   & mvarIMPRE_COTI_C               & "</IMPRE_COTI_C>"
                mvarRequest = mvarRequest & "<TEMPL_SOLI>"     & Request.Form("TEMPL_SOLI")     & "</TEMPL_SOLI>"
                mvarRequest = mvarRequest & "<IMPRE_SOLI>"     & mvarIMPRE_SOLI                 & "</IMPRE_SOLI>"
                mvarRequest = mvarRequest & "<TEMPL_CERTI>"    & Request.Form("TEMPL_CERTI")    & "</TEMPL_CERTI>"
                mvarRequest = mvarRequest & "<IMPRE_CERTI>"    & mvarIMPRE_CERTI                & "</IMPRE_CERTI>"
                mvarRequest = mvarRequest & "<TEMPL_AUTDEB>"   & Request.Form("TEMPL_AUTDEB")   & "</TEMPL_AUTDEB>"
                mvarRequest = mvarRequest & "<IMPRE_AUTDEB>"   & mvarIMPRE_AUTDEB               & "</IMPRE_AUTDEB>"
                mvarRequest = mvarRequest & "<TEMPL_TARJ_CIR>" & Request.Form("TEMPL_TARJ_CIR") & "</TEMPL_TARJ_CIR>"
                mvarRequest = mvarRequest & "<IMPRE_TARJ_CIR>" & mvarIMPRE_TARJ_CIR             & "</IMPRE_TARJ_CIR>"
                mvarRequest = mvarRequest & "<TEMPL_CLAU_SUB>" & Request.Form("TEMPL_CLAU_SUB") & "</TEMPL_CLAU_SUB>"
                mvarRequest = mvarRequest & "<IMPRE_CLAU_SUB>" & mvarIMPRE_CLAU_SUB             & "</IMPRE_CLAU_SUB>"
                mvarRequest = mvarRequest & "</Request>"
                '
                Call cmdp_ExecuteTrn("lbaw_OVSQLGen", "lbaw_OVSQLGen.biz", mvarRequest, mvarResponse)
			    Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
				    mobjXMLDoc.loadXML (mvarResponse)
				'
			    If mobjXMLDoc.selectSingleNode("//Response/Estado[@resultado='true']") Is Nothing Then
				    mvarResultado = "ERROR"
			    Else
				    mvarResultado = mobjXMLDoc.selectSingleNode("//RESULT").text
			    End If
			    '
                Response.Write mvarResultado
                Set mobjXMLDoc = Nothing
	                
    		
    		
    		
    		
	                
	        '*******************************************************************************
	      
	         Case "GetActividades"
	         
	         
	         mvarRequest = "<Request><DEFINICION>getActividadOcupacion.xml</DEFINICION><AplicarXSL>getActividadOcupacion.xsl</AplicarXSL><BUSCAR>" & Request.Form("VALOR") & "</BUSCAR></Request>"
	
	    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQGestion","lbaw_GetConsultaMQGestion.biz", mvarRequest, mvarResponse)
	    Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
		mobjXMLDoc.async = False				
													
	    Call mobjXMLDoc.loadXML(mvarResponse)
														
	        If Not mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
		        If (mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado").text) = "true" Then			
			        mvarResultado = Replace(Replace(mvarResponse,"<Response><Estado resultado='true' mensaje='' />",""),"</Response>","")
		        Else
			        mvarResultado "<option VALUE=0>No hay datos</option>"
		        End If
		        Set mobjXMLDoc = Nothing
	        Else
		        mvarResultado "<option VALUE=0>Error al ejecutar el COM+</option>"
	        End If
        	
	        response.write mvarResultado
        	         
        	      
      case "ENVIOCOLDVIEW"
      
       mvarxmlDataSource = Request.form("XMLDATASOURCE")
       mvarReportId = Request.form("REPORTID")
       mvarRAMOPCOD = Request.form("RAMOPCOD")
       mvarPOLIZANN = Request.form("POLIZANN")
       mvarPOLIZSEC = Request.form("POLIZSEC")
       mvarCERTIPOL = Request.form("CERTIPOL")
       mvarCERTIANN = Request.form("CERTIANN")
       mvarCERTISEC = Request.form("CERTISEC")
       mvarSUPLENUM = Request.form("SUPLENUM")
       mvarTIPODOCU = Request.form("TIPODOCU")

'       RAMOPCOD char(4)    		Producto
'		POLIZANN numeric(2)		A�o de P�liza
'		POLIZSEC numerci(6)		Secuencia de P�liza
'		CERTIPOL numeric(4)		Canal de Venta
'		CERTIANN numeric(4)		A�o de certificado
'		CERTISEC numeric(6)		Secuencia de Certificado
'		SUPLENUM numeric(4)		Suplemento
'		TIPODOCU char(2)		Tipo de Documentacion


            mvarRequest = "<Request>" &_
			"<DEFINICION>ismGenerateAndPublishPdfReport.xml</DEFINICION>" &_
			"<Raiz>share:generatePdfReport</Raiz>"&_
			"<xmlDataSource>"& mvarxmlDataSource & "</xmlDataSource>" &_
			"<reportId>" & mvarReportId & "</reportId>" &_
			"<coldViewPubNode>CVUN_RepositoryEstructure/CVUns_Pub_Root/LBAOV/LBACircuitoRevision</coldViewPubNode>" &_
			"<coldViewMetadata>RAMOPCOD=" & mvarRAMOPCOD & ";POLIZANN=" & mvarPOLIZANN & ";POLIZSEC=" & mvarPOLIZSEC & ";CERTIPOL=" & mvarCERTIPOL & ";CERTIANN=" & mvarCERTIANN & ";CERTISEC=" & mvarCERTISEC & ";SUPLENUM=" & mvarSUPLENUM & ";TIPODOCU=" & mvarTIPODOCU & "</coldViewMetadata>" &_
			"</Request>"
			
			'Response.Write "<TEXTAREA>" & mvarRequest & "</TEXTAREA>" 
			'Response.end
			'Control
			'Llamo al COM+ que env�a a middleware
			wvarError = cmdp_ExecuteTrn( "lbaw_OVMWGen", "",  mvarRequest,  mvarResponse)
		
			'Control
			'Response.Write "<!-- Response MW Generacion PDF: " & mvarRequest & " -->"	
			'Verifico la respuesta del COM+		
			Set mObjLocResponse = Server.CreateObject("MSXML2.DOMDocument")
			With mObjLocResponse
			    .async = False
			    Call .loadXML(mvarResponse)
			End With
    
			If Not (mobjLocResponse.selectSingleNode("//Response"))Is Nothing Then
				If mObjLocResponse.selectSingleNode("//faultstring") Is Nothing And ucase(mObjLocResponse.selectSingleNode("//Response/Estado/@resultado").text) = "TRUE" Then
					mvarCodErrPub = 0
				Else
					'Response.Write "<!-- ERROR: Se detecto el siguiente error en el envio a MW: "
					'Response.Write mObjLocResponse.selectSingleNode("//faultstring").text & "-->"
					mvarCodErrPub = 2
				End If
			Else
				mvarCodErrPub = 1
			End If	
	
            response.Write mvarCodErrPub

               '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Case "INTEGRAGRAL"
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                
			    mvarRequest = "<Request>"
	            mvarRequest = mvarRequest & "<DEFINICION>2210_IntegracionGral.xml</DEFINICION>"
	            mvarRequest = mvarRequest &  replace(mvarXMLHTTP_REQUEST,"~~","&")
	            mvarRequest = mvarRequest & "</Request>" 
                			
			    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
			    Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")		
				    mobjXMLDoc.loadXML (mvarResponse)
					
			    If mobjXMLDoc.selectSingleNode("//Response/Estado[@resultado='true']") Is Nothing Then
				    mvarID_ENCABEZADO_COTIZACION = "ERROR"
			    Else
				    mvarID_ENCABEZADO_COTIZACION = mobjXMLDoc.selectSingleNode("//NROSOLTMP").text		
			    End If
				
			    Set mobjXMLDoc = Nothing		
          
                Response.Write mvarID_ENCABEZADO_COTIZACION

            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            Case "INTEGRAITEM"
            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                
			    mvarRequest = "<Request>"
	            mvarRequest = mvarRequest & "<DEFINICION>2213_IntegracionConsorcio.xml</DEFINICION>"
	            mvarRequest = mvarRequest &  replace(mvarXMLHTTP_REQUEST,"~~","&")
	            mvarRequest = mvarRequest & "</Request>" 
                			
			    Call cmdp_ExecuteTrn("lbaw_GetConsultaMQ", "lbaw_GetConsultaMQ.biz", mvarRequest, mvarResponse)
			    
			    Set mobjXMLDoc = Server.CreateObject("MSXML2.DOMDocument")
		            mobjXMLDoc.async = False					
	            Call mobjXMLDoc.loadXML(mvarResponse)
	            
                If mobjXMLDoc.selectSingleNode("//Response/Estado/@resultado") Is Nothing Then
				    mvarResultado = "ERROR"
			    Else
			        mvarResultado = mobjXMLDoc.xml
			    End If
			    '
                Response.Write mvarResultado
                Set mobjXMLDoc = Nothing
            '~~~~~~~~~~~~~~


        	
        	  
	       
        End Select            
            
    End If
%>