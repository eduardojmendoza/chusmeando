<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2010. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: 1538_ListadoCoberturas.xsl

Fecha de Creación: 17/03/2010

Desarrollador: Leonardo Ruiz

Descripción: Este archivo se utiliza para dar formato a los XML resultantes de las llamadas a los archivos.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
		<COBESELM>
			<xsl:value-of select="//COBESELM"/>
		</COBESELM>
		<CANTELEM>
			<xsl:value-of select="//CANTELEM"/>
		</CANTELEM>
		<COBERTURAS>			
				<xsl:for-each select="//COBERTURA">
				<COBERTURA>
					<COBERCOD>
						<xsl:value-of select="COBERCOD"/>
					</COBERCOD>
					<COBERORP>
						<xsl:value-of select="COBEROP"/>
					</COBERORP>
					<SWDETMOD>
						<xsl:value-of select="SWDETMOD"/>
					</SWDETMOD>
					<COBERDES>
						<xsl:value-of select="COBERDES"/>
					</COBERDES>
					<SWOBLIG>
						<xsl:value-of select="SWOBLIG"/>
					</SWOBLIG>
					<COBERPRI>
						<xsl:value-of select="COBERPRI"/>
					</COBERPRI>
					<SWMODSUM>
						<xsl:value-of select="SWMODSUM"/>
					</SWMODSUM>
					<CANTMODU>
						<xsl:value-of select="CANTMODU"/>
					</CANTMODU>
					<MODULOS>
						<xsl:for-each select="//MODULO">
						<MODULO>
							<CODMODUL>
								<xsl:value-of select="CODMODUL"/>
							</CODMODUL>
							<COBECMIN>
								<xsl:value-of select="COBECMIN"/>
							</COBECMIN>
							<COBECMAX>
								<xsl:value-of select="COBECMAX"/>
							</COBECMAX>
							<COBEPMAX>
								<xsl:value-of select="COBEPMAX"/>
							</COBEPMAX>
							<SWSUMTOP>
								<xsl:value-of select="SWSUMTOP"/>
							</SWSUMTOP>
							<FRANQCOD>
								<xsl:value-of select="FRANQCOD"/>
							</FRANQCOD>	
						</MODULO>					
						</xsl:for-each>				
					</MODULOS>
				</COBERTURA>	
				</xsl:for-each>		
				<CANTEXCL>
					<xsl:value-of select="CANTEXCL"/>
		        	</CANTEXCL>					
		        	<COBEREXCLUYENTES>
					<xsl:for-each select="//EXCLUYENTE">
						<COBEREXC>
							<xsl:value-of select="COBEREXC"/>
						</COBEREXC>
					</xsl:for-each>								
				</COBEREXCLUYENTES>
		</COBERTURAS>
	</xsl:template>
</xsl:stylesheet>
