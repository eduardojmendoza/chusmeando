<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<xsl:for-each select="//FORMADEPAGO">
			<!-- <xsl:sort select="./COBRODES"/>    -->
			<OPTION>
				<xsl:attribute name="value"><xsl:value-of select="COBROCOD"/></xsl:attribute>
				<xsl:attribute name="cobrotip"><xsl:value-of select="COBROTIP"/></xsl:attribute>
				<xsl:attribute name="cobrodab"><xsl:value-of select="COBRODAB"/></xsl:attribute>
				<xsl:value-of select="COBRODES"/>
			</OPTION>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
