<!--
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION
LIMITED 2009. ALL RIGHTS RESERVED

This software is only to be used for the purpose for which it has been provided.
No part of it is to be reproduced, disassembled, transmitted, stored in a retrieval
system or translated in any human or computer language in any way or for any other
purposes whatsoever without the prior written consent of the Hong Kong and Shanghai
Banking Corporation Limited. Infringement of copyright is a serious civil and criminal
offence, which can result in heavy fines and payment of substantial damages.

Nombre del Fuente: LBA_1184_ProductosClientes.xsl

Fecha de Creación: 30/07/2009

PPcR: 50055/6010662

Desarrollador: Matias Coaker

Descripción: Este archivo XSL se crea para mostrar los productos de los clientes
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" encoding="iso-8859-1"/>
	<xsl:template match="/">
		<CAMPOS>
			<xsl:copy-of select="//CLIENSEC"/>
			<xsl:copy-of select="//TIPODOCU"/>
			<xsl:copy-of select="//NUMEDOCU"/>
			<xsl:copy-of select="//CLIENAP1"/>
			<xsl:copy-of select="//CLIENAP2"/>
			<xsl:copy-of select="//CLIENNOM"/>
			<PRODUCTOS>
				<xsl:for-each select="//PRODUCTO[RAMOPCOD != '']">
					<xsl:copy-of select="."/>
				</xsl:for-each>
			</PRODUCTOS>
		</CAMPOS>
	</xsl:template>
</xsl:stylesheet>
