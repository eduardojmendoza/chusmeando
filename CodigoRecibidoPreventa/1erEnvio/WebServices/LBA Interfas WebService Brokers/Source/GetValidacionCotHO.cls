VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetValidacionCotHO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "LBA_InterWSBrok.GetValidacionCotHO"

'Archivo de Errores
Const mcteArchivoHOM_XML            As String = "LBA_VALIDACION_COT_HO.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
'EG 14-10 cambia constante
'Const mcteParam_COBROCOD            As String = "COBROCOD"
Const mcteParam_COBROCOD             As String = "FPAGO"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_TIPOHOGAR            As String = "TIPOHOGAR"
'EG 14-10 cambia constante
'Const mcteParam_PLANNCOD            As String = "PLANNCOD"
Const mcteParam_PLANNCOD             As String = "PLAN"
'EG 14-10 cambia constante
'Const mcteParam_Provi                As String = "PROVI"
Const mcteParam_Provi                As String = "PROVCOD"
'EG 14-10 cambia constante
'Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
Const mcteParam_LocalidadCod         As String = "LOCALIDAD"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober                As String = "//Request/COBERTURAS/COBERTURA"
'DATOS DE LAS CAMPA�AS
Const mcteNodos_Campa                As String = "//Request/CAMPANIAS/CAMPANIA"
Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'Declaracion de variables
    Dim wvarMensaje         As String
    Dim pvarRes             As String
    Dim wvarCodErr          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    pvarRes = ""
    wvarCodErr = ""
    wvarMensaje = ""
    If Not fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = pvarRes
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    '
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    'mobjCOM_Context.SetAbort
    mobjCOM_Context.SetComplete
End Function
Private Function fncGetAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName        As String = "fncGetAll"
  Dim wvarStep            As Long
  Dim wobjXMLRequest      As MSXML2.DOMDocument
  Dim wobjXMLCheck        As MSXML2.DOMDocument
  Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
  Dim wobjXMLError        As MSXML2.IXMLDOMNode
  
  Dim wvarContChild       As Long
  Dim wvarContNode        As Long

  wvarStep = 10
'Chequeo que los datos esten OK
'Chequeo que no venga nada en blanco
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  Set wobjXMLCheck = CreateObject("MSXML2.DOMDocument")
  wobjXMLCheck.async = False
  Call wobjXMLCheck.Load(App.Path & "\" & mcteArchivoHOM_XML)
  '
  wvarStep = 20
  Set wobjXMLList = wobjXMLCheck.selectNodes("//ERRORES/HOGAR/VACIOS/CAMPOS/ERROR")
  For wvarContChild = 0 To wobjXMLList.length - 1
    Set wobjXMLError = wobjXMLList(wvarContChild)
    If Not wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text) Is Nothing Then
         wvarStep = 30
         If Trim(wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text).Text) = "" Then
         wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
         wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
         pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
         Exit Function
         End If
    Else
       'error
       wvarStep = 40
       wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
       wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
       Exit Function
    End If
    Set wobjXMLError = Nothing
  Next


  'Chequeo Tipo y Longitud de Datos para el XML Padre
  wvarStep = 50
  
  For wvarContChild = 0 To wobjXMLRequest.selectSingleNode("//Request").childNodes.length - 1
    If Not wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName) Is Nothing Then
        'Chequeo la longitud y si tiene Largo Fijo
        wvarStep = 60
        If wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONG_VARIABLE").Text = "N" Then
           wvarStep = 70
           If Not Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        Else
            wvarStep = 80
           If Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) > wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        End If
        
        'Chequeo el Tipo
        If wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TIPO").Text = "NUMERICO" Then
           wvarStep = 90
           If Not IsNumeric(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) Then
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
             Else
                'Chequeo que el dato numerico no sea 0
                'Excluye los nodos POLIZANN
                wvarStep = 100
                If wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text = 0 And _
                   Not (wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName = "POLIZANN") Then
                    wvarCodErr = -199
                    wvarMensaje = "El valor del dato numerico no puede estar en 0"
                    pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
                    Exit Function
                End If
           End If
        End If
    End If
  Next
  
  'Chequeo Tipo y Longitud de Datos para Coberturas
  For wvarContChild = 0 To wobjXMLRequest.selectNodes(mcteNodos_Cober).length - 1
    wvarStep = 110
    For wvarContNode = 0 To wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes.length - 1
      wvarStep = 120
      If Not wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName) Is Nothing Then
        'Chequeo la longitud y si tiene Largo Fijo
        If wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/LONG_VARIABLE").Text = "N" Then
           wvarStep = 130
           If Not Len(wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).Text) = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/LONGITUD").Text Then
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        Else
           If Len(wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).Text) > wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/LONGITUD").Text Then
                wvarStep = 140
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        End If
       
        'Chequeo el Tipo
        If wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TIPO").Text = "NUMERICO" Then
           wvarStep = 150
           If Not IsNumeric(wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).Text) Then
                wvarStep = 160
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
             Else
                'Chequeo que el dato numerico no sea 0
                If wobjXMLRequest.selectNodes(mcteNodos_Cober).Item(wvarContChild).childNodes(wvarContNode).Text = 0 Then
                    wvarStep = 160
                    wvarCodErr = -199
                    wvarMensaje = "El valor del dato numerico no puede estar en 0"
                    pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                    Exit Function
                End If
           End If
        End If
      End If
    Next
  Next
    

  'Chequeo Tipo y Longitud de Datos para Campa�as
  For wvarContChild = 0 To wobjXMLRequest.selectNodes(mcteNodos_Campa).length - 1
    wvarStep = 170
    For wvarContNode = 0 To wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes.length - 1
      If Not wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName) Is Nothing Then
        wvarStep = 180
        'Chequeo la longitud y si tiene Largo Fijo
        If wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/LONG_VARIABLE").Text = "N" Then
           If Not Len(wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).Text) = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/LONGITUD").Text Then
                wvarStep = 190
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        Else
           If Len(wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).Text) > wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/LONGITUD").Text Then
                wvarStep = 200
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        End If
       
        'Chequeo el Tipo
        If wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TIPO").Text = "NUMERICO" Then
           wvarStep = 210
           If Not IsNumeric(wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).Text) Then
                wvarStep = 220
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/TIPODEDATO/" & wobjXMLRequest.selectNodes(mcteNodos_Campa).Item(wvarContChild).childNodes(wvarContNode).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        End If
      End If
    Next
  Next

'Chequeo los datos FIJOS
  wvarStep = 230
  If wobjXMLCheck.selectNodes("//ERRORES/HOGAR/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/VALOR[.='" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text & "']").length = 0 Then
      'error
      wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/N_ERROR").Text
      wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/HOGAR/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/TEXTOERROR").Text
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If

  Set wobjXMLCheck = Nothing

'Cheque que vengan hasta 50 coberturas
  wvarStep = 240
  Set wobjXMLList = wobjXMLRequest.selectNodes(mcteNodos_Cober)
  If wobjXMLList.length > 50 Then
     wvarCodErr = -48
     wvarMensaje = "Existen mas coberturas de las permitidas"
     pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
     Exit Function
  End If
 
'Chequeo que no vengan las coberturas en blanco
  Set wobjXMLList = Nothing
  
'Cheque que vengan hasta 10 campanias
  wvarStep = 250
  Set wobjXMLList = wobjXMLRequest.selectNodes(mcteNodos_Campa)
  If wobjXMLList.length > 10 Then
     wvarCodErr = -49
     wvarMensaje = "Existen mas campanias de las permitidas"
     pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
     Exit Function
  End If
  
'Chequeo que no vengan las campanias en blanco
  wvarStep = 270
  For wvarContNode = 0 To wobjXMLList.length - 1
      If wobjXMLList(wvarContNode).childNodes(0) Is Nothing Then
        'error
          wvarCodErr = -1
          wvarMensaje = "Falta el dato de la Campania"
          pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
          Exit Function
      End If
  Next
  Set wobjXMLList = Nothing
   
   wvarStep = 280
   If fncValidaABase(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
     fncGetAll = True
   Else
     fncGetAll = False
   End If
  
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr

  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncGetAll = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function
Private Function fncValidaABase(ByVal pvarRequest As String, _
                                ByRef wvarMensaje As String, _
                                ByRef wvarCodErr As String, _
                                ByRef pvarRes As String) As Boolean

  Const wcteFnName        As String = "fncValidaABase"
  Dim wvarStep            As Long
  '
  Dim wobjClass           As HSBCInterfaces.IAction
  Dim wobjXMLRequest      As MSXML2.DOMDocument
  '
  On Error GoTo ErrorHandler
  '
  wvarStep = 10
  Set wobjClass = mobjCOM_Context.CreateInstance("LBA_InterWSBrok.GetNroCotHO")
  Call wobjClass.Execute(pvarRequest, pvarRes, "")
  '
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRes)
  '
  wvarStep = 20
  wvarCodErr = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text
  wvarMensaje = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text
  If Not wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text = 0 Then
        fncValidaABase = False
  Else
       fncValidaABase = True
  End If

fin:
  Set wobjClass = Nothing
  Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod

  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncValidaABase = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
