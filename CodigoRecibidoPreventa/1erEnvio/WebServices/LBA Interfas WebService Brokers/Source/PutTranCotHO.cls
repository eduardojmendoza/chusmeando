VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "PutTranCotHO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ObjectControl
Implements HSBCInterfaces.IAction

Const mcteClassName         As String = "LBA_InterWSBrok.PutTranCotHO"
Const mcteStoreProc         As String = "SPSNCV_BRO_ALTA_OPER"

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
Const mcteParam_COBROCOD             As String = "COBROCOD"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_TIPOHOGAR            As String = "TIPOHOGAR"
Const mcteParam_PLANNCOD             As String = "PLANNCOD"
Const mcteParam_Provi                As String = "PROVI"
Const mcteParam_LocalidadCod         As String = "LOCALIDADCOD"
Const mcteParam_TipoOperac           As String = "TIPOOPERAC"
Const mcteParam_RAMOPCOD             As String = "RAMOPCOD"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"
Const mcteParam_NROCOT               As String = "COT_NRO"
Const mcteParam_EstadoProc           As String = "ESTADOPROC"
Const mcteParam_PedidoAno            As String = "MSGANO"
Const mcteParam_PedidoMes            As String = "MSGMES"
Const mcteParam_PedidoDia            As String = "MSGDIA"
Const mcteParam_PedidoHora           As String = "MSGHORA"
Const mcteParam_PedidoMinuto         As String = "MSGMINUTO"
Const mcteParam_PedidoSegundo        As String = "MSGSEGUNDO"
Const mcteParam_TiempoProceso        As String = "TIEMPOPROCESO"
Const mcteParam_CERTISEC             As String = "CERTISEC"
'DATOS DE LAS COBERTURAS
Const mcteNodos_Cober                As String = "//Request/COBERTURAS"
Const mcteParam_COBERCOD             As String = "COBERCOD"
Const mcteParam_CONTRMOD             As String = "CONTRMOD"
Const mcteParam_CAPITASG             As String = "CAPITASG"
'DATOS DE LAS CAMPA�AS
Const mcteNodos_Campa                As String = "//Request/CAMPANIAS"
Const mcteParam_CampaCod             As String = "CAMPACOD"
'PRODUCTOR
Const mctePORTAL                     As String = "LBA"
Private Sub ObjectControl_Activate()
    '
    Set mobjCOM_Context = GetObjectContext()
    Set mobjEventLog = CreateObject("HSBC.EventLog")
    '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
    '
    ObjectControl_CanBePooled = True
    '
End Function

Private Sub ObjectControl_Deactivate()
    '
    Set mobjCOM_Context = Nothing
    Set mobjEventLog = Nothing
    '
End Sub

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    
    'declaracion de variables
    Dim wvarMensaje         As String
    Dim wvarCodErr          As String
    Dim pvarRes             As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    wvarMensaje = ""
    wvarCodErr = ""
    pvarRes = ""
    If Not fncPutAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
        If wvarCodErr <> mcteErrorInesperadoCod Then
            pvarResponse = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        Else
            pvarResponse = pvarRes
        End If
        
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    '
    pvarResponse = "<LBA_WS res_code=""0"" res_msg="""">" & pvarRes & "</LBA_WS>"
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    'mobjCOM_Context.SetAbort
    mobjCOM_Context.SetComplete
End Function
Private Function fncPutAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName                    As String = "fncPutAll"
  Dim wvarStep                        As Long
  '
  Dim wobjClass                        As HSBCInterfaces.IAction
  Dim wobjHSBC_DBCnn                   As HSBCInterfaces.IDBConnection
  Dim wobjXMLRequest                   As MSXML2.DOMDocument
  '
  Dim wobjDBCnn                        As ADODB.Connection
  Dim wobjDBCmd                        As ADODB.Command
  Dim wobjDBParm                       As ADODB.Parameter
  Dim wrstRes                          As ADODB.Recordset
  '
  Dim mvarWDB_CODINST                  As String
  Dim mvarWDB_REQUESTID                As String
  Dim mvarWDB_NROCOT                   As String
  Dim mvarWDB_TIPOOPERAC               As String
  Dim mvarWDB_ESTADO                   As String
  Dim mvarWDB_RECEPCION_PEDIDOANO      As String
  Dim mvarWDB_RECEPCION_PEDIDOMES      As String
  Dim mvarWDB_RECEPCION_PEDIDODIA      As String
  Dim mvarWDB_RECEPCION_PEDIDOHORA     As String
  Dim mvarWDB_RECEPCION_PEDIDOMINUTO   As String
  Dim mvarWDB_RECEPCION_PEDIDOSEGUNDO  As String
  Dim mvarWDB_RAMOPCOD                 As String
  Dim mvarWDB_POLIZANN                 As String
  Dim mvarWDB_POLIZSEC                 As String
  Dim mvarWDB_CERTISEC                 As String
  Dim mvarWDB_TIEMPOPROCESO            As String
  Dim mvarWDB_XML                      As String

On Error GoTo ErrorHandler

'Alta de la OPERACION
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_CODINST = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 30
  mvarWDB_REQUESTID = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 40
  mvarWDB_NROCOT = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_NROCOT) Is Nothing Then
     mvarWDB_NROCOT = wobjXMLRequest.selectSingleNode("//" & mcteParam_NROCOT).Text
  End If
  '
  wvarStep = 50
  mvarWDB_TIPOOPERAC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TipoOperac) Is Nothing Then
     mvarWDB_TIPOOPERAC = wobjXMLRequest.selectSingleNode("//" & mcteParam_TipoOperac).Text
  End If
  '
  wvarStep = 60
  mvarWDB_ESTADO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_EstadoProc) Is Nothing Then
     mvarWDB_ESTADO = wobjXMLRequest.selectSingleNode("//" & mcteParam_EstadoProc).Text
  End If
  '
  wvarStep = 70
  mvarWDB_RECEPCION_PEDIDOANO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoAno) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOANO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoAno).Text
  End If
  '
  wvarStep = 80
  mvarWDB_RECEPCION_PEDIDOMES = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMes) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOMES = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMes).Text
  End If
  '
  wvarStep = 90
  mvarWDB_RECEPCION_PEDIDODIA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoDia) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDODIA = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoDia).Text
  End If
  '
  wvarStep = 100
  mvarWDB_RECEPCION_PEDIDOHORA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoHora) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOHORA = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoHora).Text
  End If
  '
  wvarStep = 110
  mvarWDB_RECEPCION_PEDIDOMINUTO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMinuto) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOMINUTO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoMinuto).Text
  End If
  '
  wvarStep = 120
  mvarWDB_RECEPCION_PEDIDOSEGUNDO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoSegundo) Is Nothing Then
     mvarWDB_RECEPCION_PEDIDOSEGUNDO = wobjXMLRequest.selectSingleNode("//" & mcteParam_PedidoSegundo).Text
  End If
  '
  wvarStep = 130
  mvarWDB_RAMOPCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_RAMOPCOD) Is Nothing Then
    mvarWDB_RAMOPCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_RAMOPCOD).Text
  End If
  '
  wvarStep = 140
  mvarWDB_POLIZANN = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN) Is Nothing Then
     If IsNumeric(wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text) Then
        If wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text < 100 Then
            mvarWDB_POLIZANN = Val(wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text)
        End If
    End If
  End If
  '
  wvarStep = 150
  mvarWDB_POLIZSEC = 0
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC) Is Nothing Then
     If IsNumeric(wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text) Then
        If wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text < 1000000 Then
             mvarWDB_POLIZSEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text
        End If
     End If
  End If
  '
  wvarStep = 160
  mvarWDB_CERTISEC = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CERTISEC) Is Nothing Then
     mvarWDB_CERTISEC = wobjXMLRequest.selectSingleNode("//" & mcteParam_CERTISEC).Text
  End If
  '
  wvarStep = 170
  mvarWDB_TIEMPOPROCESO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TiempoProceso) Is Nothing Then
     mvarWDB_TIEMPOPROCESO = wobjXMLRequest.selectSingleNode("//" & mcteParam_TiempoProceso).Text
  End If
  '
  mvarWDB_XML = ""
  mvarWDB_XML = pvarRequest
  '
  wvarStep = 180
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  '
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProc
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 190
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_IDENTIFICACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 200
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", 0, mvarWDB_REQUESTID))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 210
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_COTIZACION_LBA", adNumeric, adParamInput, , IIf(mvarWDB_NROCOT = "", 0, mvarWDB_NROCOT))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 18
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 220
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIPO_OPERACION", adChar, adParamInput, 1, IIf(mvarWDB_TIPOOPERAC = "", "", mvarWDB_TIPOOPERAC))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ESTADO", adChar, adParamInput, 2, IIf(mvarWDB_ESTADO = "", "", mvarWDB_ESTADO))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 240
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_RECEPCION_PEDIDO", adDate, adParamInput, 8, _
                                         mvarWDB_RECEPCION_PEDIDOANO & "-" & _
                                         mvarWDB_RECEPCION_PEDIDOMES & "-" & _
                                         mvarWDB_RECEPCION_PEDIDODIA & " " & _
                                         mvarWDB_RECEPCION_PEDIDOHORA & ":" & _
                                         mvarWDB_RECEPCION_PEDIDOMINUTO & ":" & _
                                         mvarWDB_RECEPCION_PEDIDOSEGUNDO)
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 250
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ENVIO_RESPUESTA", adDate, adParamInput, 8, _
                                         Year(Now) & "-" & _
                                         Month(Now) & "-" & _
                                         Day(Now) & " " & _
                                         Hour(Now) & ":" & _
                                         Minute(Now) & ":" & _
                                         Second(Now))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 260
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_RAMOPCOD", adChar, adParamInput, 4, IIf(mvarWDB_RAMOPCOD = "", "", mvarWDB_RAMOPCOD))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 270
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZANN", adNumeric, adParamInput, , IIf(mvarWDB_POLIZANN = "", 0, mvarWDB_POLIZANN))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 280
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_POLIZSEC", adNumeric, adParamInput, , IIf(mvarWDB_POLIZSEC = "", 0, mvarWDB_POLIZSEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 290
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTIPOL", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 300
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTIANN", adNumeric, adParamInput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 310
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CERTISEC", adNumeric, adParamInput, , IIf(mvarWDB_CERTISEC = "", 0, mvarWDB_CERTISEC))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 320
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIEMPO_PROCESO_AIS", adNumeric, adParamInput, , IIf(mvarWDB_TIEMPOPROCESO = "", 0, mvarWDB_TIEMPOPROCESO))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 330
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_OPERACION_XML", adVarChar, adParamInput, 8000, IIf(mvarWDB_XML = "", "", Left(mvarWDB_XML, 8000)))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 340
  '
  wobjDBCmd.Execute adExecuteNoRecords
  '
  wobjDBCnn.Close
  '
  fncPutAll = True

fin:
'libero los objectos
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  Set wobjXMLRequest = Nothing
  Set wobjClass = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod


    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    fncPutAll = False
    mobjCOM_Context.SetComplete


End Function




