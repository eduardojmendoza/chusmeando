VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetValidacionSolATM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "LBA_InterWSBrok.GetValidacionSolATM"
Const mcteStoreProcSelect       As String = "SPSNCV_BRO_SELECT_OPER_ESP"
Const mcteStoreProcValidacion   As String = "SPSNCV_BRO_VALIDA_SOL_ATM"

'Archivo de Errores
Const mcteArchivoATM_XML            As String = "LBA_VALIDACION_SOL_ATM.XML"
Const mcteParamATM_XML              As String = "LBA_PARAM_ATM.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_VALOR                As String = "VALOR"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
Const mcteParam_CERTISEC             As String = "CERTISEC"
Const mcteParam_BANELCO              As String = "BANELCO"
Const mcteParam_COBROCOD             As String = "FPAGO"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_PLANNCOD             As String = "TPLANATM"
Const mcteParam_Provi                As String = "PROVCOD"
Const mcteParam_LocalidadCod         As String = "LOCALIDAD"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"
Const mcteParam_NacimAnn             As String = "NACIMANN"
Const mcteParam_NacimMes             As String = "NACIMMES"
Const mcteParam_NacimDia             As String = "NACIMDIA"
Const mcteParam_EmisiAnn             As String = "EMISIANN"
Const mcteParam_EmisiMes             As String = "EMISIMES"
Const mcteParam_EmisiDia             As String = "EMISIDIA"
Const mcteParam_EfectAnn             As String = "EFECTANN"
Const mcteParam_EfectMes             As String = "EFECTMES"
Const mcteParam_EfectDia             As String = "EFECTDIA"
Const mcteParam_NumeDocu             As String = "NUMEDOCU"
Const mcteParam_TipoDocu             As String = "TIPODOCU"
Const mcteParam_DomicNro             As String = "DOMICNRO"
Const mcteParam_DomicPso             As String = "DOMICPSO"
Const mcteParam_DomicPta             As String = "DOMICPTA"
Const mcteParam_TelefNro             As String = "TELEFNRO"
Const mcteParam_DomicDnu             As String = "DOMICDNU"
Const mcteParam_Sexo                 As String = "SEXO"
Const mcteParam_Estado               As String = "ESTADO"
Const mcteParam_CUENNUME             As String = "CUENNUME"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'Declaracion de variables
    Dim wvarMensaje         As String
    Dim pvarRes             As String
    Dim wvarCodErr          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    pvarRes = ""
    wvarCodErr = ""
    wvarMensaje = ""
    If Not fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = pvarRes
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    'mobjCOM_Context.SetAbort
    mobjCOM_Context.SetComplete
End Function
Private Function fncGetAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName         As String = "fncGetAll"
  Dim wvarStep             As Long
  
  Dim wobjClass            As HSBCInterfaces.IAction
  
  Dim wobjXMLRequest       As MSXML2.DOMDocument
  Dim wobjXMLCheck         As MSXML2.DOMDocument
  Dim wobjXMLParams        As MSXML2.DOMDocument
  Dim wobjXMLList          As MSXML2.IXMLDOMNodeList
  Dim wobjXMLError         As MSXML2.IXMLDOMNode
  Dim wobjXMLStatusTarjeta As MSXML2.DOMDocument
  
  Dim wvarContChild        As Long
  Dim wvarContNode         As Long
  
  Dim wvarTarjeta          As String
  Dim wvarStatusTarjeta    As String
  
  Dim wvarFechaActual      As Date
  Dim wvarFechaVigencia    As Date

  wvarStep = 10
'Chequeo que los datos esten OK
'Chequeo que no venga nada en blanco
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)

  Set wobjXMLCheck = CreateObject("MSXML2.DOMDocument")
  wobjXMLCheck.async = False
  Call wobjXMLCheck.Load(App.Path & "\" & mcteArchivoATM_XML)
  '
  wvarStep = 20
  
  Set wobjXMLList = wobjXMLCheck.selectNodes("//ERRORES/ATM/VACIOS/CAMPOS/ERROR")
  For wvarContChild = 0 To wobjXMLList.length - 1
    Set wobjXMLError = wobjXMLList(wvarContChild)
    If Not wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text) Is Nothing Then
         wvarStep = 30
         If Trim(wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text).Text) = "" Then
         wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
         wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
         pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
         Exit Function
         End If
    Else
       'error
       wvarStep = 40
       wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
       wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
       Exit Function
    End If
    Set wobjXMLError = Nothing
  Next
  
  'Chequeo Tipo y Longitud de Datos para el XML Padre
  wvarStep = 50
  
  For wvarContChild = 0 To wobjXMLRequest.selectSingleNode("//Request").childNodes.length - 1
    If Not wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName) Is Nothing Then
        'Si el Codigo de Cobro no es tarjeta, no chequea el Largo de Tarjeta y su Vencimiento
        If Not Val(wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text) = 4 And _
           (wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName).nodeName = "CUENNUME" Or _
            wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName).nodeName = "VENCIANN" Or _
            wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName).nodeName = "VENCIMES" Or _
            wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName).nodeName = "VENCIDIA") Then
            GoTo Siguiente
        End If
        
        'Chequeo la longitud y si tiene Largo Fijo
        If wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONG_VARIABLE").Text = "N" Then
           wvarStep = 60
           If Not Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                wvarStep = 70
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        Else
           If Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) > wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                wvarStep = 80
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        End If
        
        'Chequeo el Tipo
        
        If wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TIPO").Text = "NUMERICO" Then
           If Not IsNumeric(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) Then
                wvarStep = 90
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
             Else
                'Chequeo que el dato numerico no sea 0
                'Excluye los nodos POLIZANN
                wvarStep = 100
                If wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text = 0 And _
                   Not (wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName = "POLIZANN" Or _
                        wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName = "COT_NRO") Then
                    wvarCodErr = -199
                    wvarMensaje = "El valor del dato numerico no puede estar en 0"
                    pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                    Exit Function
                End If
           End If
        End If
    End If
Siguiente:
  Next
  
  
'Chequeo los datos FIJOS
  wvarStep = 280
  'CLIENIVA
  If wobjXMLCheck.selectNodes("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/VALOR[.='" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text & "']").length = 0 Then
      'error
      wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/N_ERROR").Text
      wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/TEXTOERROR").Text
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If
  
  wvarStep = 290
  'SEXO
  If wobjXMLCheck.selectNodes("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_Sexo & "/VALOR[.='" & wobjXMLRequest.selectSingleNode("//" & mcteParam_Sexo).Text & "']").length = 0 Then
      'error
      wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_Sexo & "/N_ERROR").Text
      wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_Sexo & "/TEXTOERROR").Text
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If
  '
  wvarStep = 300
  'ESTADO
  If wobjXMLCheck.selectNodes("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_Estado & "/VALOR[.='" & wobjXMLRequest.selectSingleNode("//" & mcteParam_Estado).Text & "']").length = 0 Then
      'error
      wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_Estado & "/N_ERROR").Text
      wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_Estado & "/TEXTOERROR").Text
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If
  '
  Set wobjXMLCheck = Nothing
  Set wobjXMLList = Nothing
  '
  
  'Chequeo EDAD
  wvarStep = 410
  If Not ValidarFechayRango(wobjXMLRequest.selectSingleNode("//" & mcteParam_NacimDia).Text & _
                        "/" & wobjXMLRequest.selectSingleNode("//" & mcteParam_NacimMes).Text & _
                        "/" & wobjXMLRequest.selectSingleNode("//" & mcteParam_NacimAnn).Text, 17, 86, wvarMensaje) Then
    'error
      wvarCodErr = -9
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If
    
  'Valida la fecha de la Tarjeta (si Viene)
  wvarStep = 420
  If wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text = 4 Then
    If Not (wobjXMLRequest.selectSingleNode("//VENCIDIA") Is Nothing And _
            wobjXMLRequest.selectSingleNode("//VENCIMES") Is Nothing And _
            wobjXMLRequest.selectSingleNode("//VENCIANN") Is Nothing) Then
        If Not validarFecha(wobjXMLRequest.selectSingleNode("//VENCIDIA").Text & "/" & _
                            wobjXMLRequest.selectSingleNode("//VENCIMES").Text & "/" & _
                            wobjXMLRequest.selectSingleNode("//VENCIANN").Text, wvarMensaje) Then
        'error
           wvarCodErr = -13
           wvarMensaje = "Fecha de Tarjeta Invalida"
           pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
           Exit Function
        End If
    End If
    If wobjXMLRequest.selectSingleNode("//VENCIMES") Is Nothing Then
           wvarCodErr = -13
           wvarMensaje = "Fecha de Tarjeta Invalida"
           pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
           Exit Function
    End If
    If wobjXMLRequest.selectSingleNode("//VENCIANN") Is Nothing Then
           wvarCodErr = -13
           wvarMensaje = "Fecha de Tarjeta Invalida"
           pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
           Exit Function
    End If
    If Val(wobjXMLRequest.selectSingleNode("//VENCIANN").Text) < Year(Now) Or _
      (Val(wobjXMLRequest.selectSingleNode("//VENCIANN").Text) = Year(Now) And _
       Val(wobjXMLRequest.selectSingleNode("//VENCIMES").Text) <= Month(Now)) Then
           wvarCodErr = -18
           wvarMensaje = "Tarjeta de Credito vencida"
           pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
           Exit Function
    End If
    
    'Valida el nro. de Tarjeta
    wvarStep = 430
    wvarTarjeta = "<Request>" & _
                  "<USUARIO/>" & _
                  "<COBROTIP>" & wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text & "</COBROTIP>" & _
                  "<CTANUM>" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CUENNUME).Text & "</CTANUM>" & _
                  "<VENANN>" & wobjXMLRequest.selectSingleNode("//VENCIANN").Text & "</VENANN>" & _
                  "<VENMES>" & wobjXMLRequest.selectSingleNode("//VENCIMES").Text & "</VENMES>" & _
                 "</Request>"

    wvarStep = 440
    Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQCotizar.lbaw_OVValidaCuentas")
    Call wobjClass.Execute(wvarTarjeta, wvarStatusTarjeta, "")
    Set wobjClass = Nothing
    
    wvarStep = 450
    Set wobjXMLStatusTarjeta = CreateObject("MSXML2.DOMDocument")
    wobjXMLStatusTarjeta.async = False
    Call wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta)
    
    wvarStep = 460
    If wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
           wvarCodErr = -198
           'wvarMensaje = "Numero de Tarjeta de Cr�dito invalido"
           wvarMensaje = wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@mensaje").Text
           pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
           Exit Function
    End If
 End If
  
  wvarStep = 461
  If wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text = 5 Then
    If UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text) <> "DB" Then
           wvarCodErr = -13
           wvarMensaje = "Para CBU campo COBROTIP debe valer DB"
           pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
           Exit Function
    End If
    'Se saca validacion de CBU
    'If wobjXMLRequest.selectSingleNode("//VENCIMES") Is Nothing Then
    '       wvarCodErr = -13
    '       wvarMensaje = "Nro. CBU incompleto (vencimes)"
    '       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
    '       Exit Function
    'End If
    'If wobjXMLRequest.selectSingleNode("//VENCIANN") Is Nothing Then
    '       wvarCodErr = -13
    '       wvarMensaje = "Nro. CBU incompleto (venciann)"
    '       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
    '       Exit Function
    'End If
    'Valida el nro. de CBU
    'wvarStep = 462
    '    wvarTarjeta = "<Request>" & _
    '              "<USUARIO/>" & _
    '              "<COBROTIP>" & UCase(wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text) & "</COBROTIP>" & _
    '              "<CTANUM>" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CUENNUME).Text & "</CTANUM>" & _
    '              "<VENANN>" & wobjXMLRequest.selectSingleNode("//VENCIANN").Text & "</VENANN>" & _
    '              "<VENMES>" & wobjXMLRequest.selectSingleNode("//VENCIMES").Text & "</VENMES>" & _
    '              "</Request>"
                                    
   ' wvarStep = 463
   ' Set wobjClass = mobjCOM_Context.CreateInstance("lbawA_OVMQCotizar.lbaw_OVValidaCuentas")
   ' Call wobjClass.Execute(wvarTarjeta, wvarStatusTarjeta, "")
   ' Set wobjClass = Nothing
    
   ' wvarStep = 464
   ' Set wobjXMLStatusTarjeta = CreateObject("MSXML2.DOMDocument")
   ' wobjXMLStatusTarjeta.async = False
   ' Call wobjXMLStatusTarjeta.loadXML(wvarStatusTarjeta)
    
   ' wvarStep = 465
   ' If wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@resultado").Text = "false" Then
   '        wvarCodErr = -198
   '        wvarMensaje = wobjXMLStatusTarjeta.selectSingleNode("//Response/Estado/@mensaje").Text
   '        pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
   '        Exit Function
   ' End If


  End If
'JC FIN CBU

  ' No valido la fecha de vigencia porque debe ser el 1 del mes actual para ATM
  '
  wvarStep = 110
  If fncValidaABase(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
     fncGetAll = True
  Else
     fncGetAll = False
  End If
  
fin:
  
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr
  
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncGetAll = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function
Private Function fncValidaABase(ByVal pvarRequest As String, _
                                ByRef wvarMensaje As String, _
                                ByRef wvarCodErr As String, _
                                ByRef pvarRes As String) As Boolean

  Const wcteFnName        As String = "fncValidaABase"
  Dim wvarStep            As Long
  Dim wvarError           As Boolean
  '
  Dim wobjXMLRequest      As MSXML2.DOMDocument
  Dim wobjXMLCotizacion   As MSXML2.DOMDocument
  
  Dim wobjXMLResponse     As MSXML2.DOMDocument
  Dim wobjXMLReturnVal    As MSXML2.DOMDocument
  Dim wobjXML             As MSXML2.DOMDocument
  Dim wobjXMLError        As MSXML2.DOMDocument
  '
  Dim wobjClass           As HSBCInterfaces.IAction
  '
  Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
  Dim wobjDBCnn           As ADODB.Connection
  Dim wobjDBCmd           As ADODB.Command
  Dim wobjDBParm          As ADODB.Parameter
  Dim wrstRes             As ADODB.Recordset
  '
  Dim wvarContChild As Long
  Dim wvarContNode As Long
  '
  Dim mvarWDB_CODINST       As String
  Dim mvarWDB_REQUESTID     As String
  Dim mvarWDB_PROVI         As String
  Dim mvarWDB_LOCALIDADCOD  As String
  Dim mvarWDB_COBROCOD      As String
  Dim mvarWDB_COBROTIP      As String
  Dim mvarWDB_TIPODOCU      As String

  '
  Dim wvarChequeoLocalidad  As Boolean
  Dim wvarChequeoTarjeta    As Boolean
  '
  Dim wvarMensajeStoreProc  As String
  Dim wvarMensajeNroCot     As String
  ' 10/2010 se agrega una nueva variable para re-cotizar
  Dim pvarCot               As String
  
  
  On Error GoTo ErrorHandler
  
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_REQUESTID = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 30
  mvarWDB_CODINST = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 40
  mvarWDB_PROVI = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi) Is Nothing Then
     mvarWDB_PROVI = wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi).Text
  End If
  '
  wvarStep = 50
  mvarWDB_LOCALIDADCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_LocalidadCod) Is Nothing Then
     mvarWDB_LOCALIDADCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_LocalidadCod).Text
  End If
  '
  wvarStep = 60
  mvarWDB_COBROCOD = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD) Is Nothing Then
     mvarWDB_COBROCOD = wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text
  End If
  '
  wvarStep = 70
  mvarWDB_COBROTIP = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP) Is Nothing Then
     mvarWDB_COBROTIP = wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text
  End If
  '
  wvarStep = 80
  mvarWDB_TIPODOCU = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_TipoDocu) Is Nothing Then
     mvarWDB_TIPODOCU = wobjXMLRequest.selectSingleNode("//" & mcteParam_TipoDocu).Text
  End If
  '
  wvarStep = 120
  '
'Valido los campos de Solicitud
  '
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProcValidacion
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 130
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODINST", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 140
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", 0, mvarWDB_REQUESTID))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 150
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODPROVCO", adNumeric, adParamInput, , IIf(mvarWDB_PROVI = "", 0, mvarWDB_PROVI))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 160
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODPOSTCO", adNumeric, adParamInput, , IIf(mvarWDB_LOCALIDADCOD = "", 0, mvarWDB_LOCALIDADCOD))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 5
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 170
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_COBROCOD", adNumeric, adParamInput, , IIf(mvarWDB_COBROCOD = "", 0, mvarWDB_COBROCOD))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 180
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_COBROTIP", adChar, adParamInput, 2, IIf(mvarWDB_COBROTIP = "", "", mvarWDB_COBROTIP))
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 190
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_DOCUMTIP_TIT", adNumeric, adParamInput, , IIf(mvarWDB_TIPODOCU = "", 0, mvarWDB_TIPODOCU))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 2
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 225
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NROCOT", adNumeric, adParamInputOutput, , 0)
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 6
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  '
  Set wrstRes = CreateObject("ADODB.Recordset")
  wrstRes.Open wobjDBCmd
  Set wrstRes.ActiveConnection = Nothing
  '
  Set wobjXML = CreateObject("MSXML2.DOMDocument")
  wobjXML.async = False
  Call wobjXML.Load(App.Path & "\" & mcteArchivoATM_XML)
  '
  'SI NO HUBO ERROR CONTINUO
  wvarStep = 240
  If Not wrstRes.EOF Then
      If Not UCase(Trim(wrstRes.Fields(0).Value)) = "OK" Then
        'error
        If wobjXML.selectNodes("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']").length > 0 Then
          wvarMensaje = wobjXML.selectSingleNode("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']/TEXTOERROR").Text
          wvarCodErr = wobjXML.selectSingleNode("//ERRORES/ATM/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']/VALORRESPUESTA").Text
          pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        Else
          wvarCodErr = wrstRes.Fields(0).Value
          wvarMensaje = "No existe descripcion para este error"
          pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        End If
        '
        fncValidaABase = False
        Exit Function
      Else
      End If
  Else
      'error
      wvarCodErr = -60
      wvarMensaje = "No se retornaron datos de la base"
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If
  '
  Set wobjXML = Nothing
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  '
  wvarStep = 250
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProcSelect
  wobjDBCmd.CommandType = adCmdStoredProc
  '
'Comparo con el XML guardado de Cotizacion
'Busco el registro guardado de la cotizacion
  wvarStep = 270
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_IDENTIFICACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", Null, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 280
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", Null, mvarWDB_REQUESTID))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 290
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_TIPO_OPERACION", adChar, adParamInput, 1, "C")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 300
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_ESTADO", adChar, adParamInput, 2, "OK")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 310
  Set wrstRes = CreateObject("ADODB.Recordset")
  wrstRes.Open wobjDBCmd
  '
  wvarStep = 320
  Set wrstRes.ActiveConnection = Nothing
  '
'Cargo el campo de XML de cotizacion
  wvarStep = 330
  Set wobjXMLCotizacion = CreateObject("MSXML2.DOMDocument")
  wobjXMLCotizacion.async = False
  Call wobjXMLCotizacion.loadXML(wrstRes.Fields(14).Value)
  '
  'no compara el Codigo Postal
  wvarStep = 340
  wvarChequeoLocalidad = True

  'Si es COBROCOD = 4 no compara el Tipo de Tarjeta
  wvarStep = 350
  If wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text <> 4 Then
         If wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROTIP).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_COBROTIP).Text Then
            wvarChequeoTarjeta = True
         Else
            wvarChequeoTarjeta = False
        End If
  Else
     wvarChequeoTarjeta = True
  End If

  wvarStep = 360
  If Not (wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_POLIZANN).Text And _
         wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_POLIZSEC).Text And _
         wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_COBROCOD).Text And _
         wvarChequeoTarjeta And _
         wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_PLANNCOD).Text And _
         wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_CLIENIVA).Text) _
   Then
    'If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Provi).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_Provi).Text Then
    '    wvarCodErr = -163
    '    wvarMensaje = "Cod. de Provincia: existe una diferencia entre la Cotizacion y la Solicitud"
    'End If
    'If Not wvarChequeoLocalidad Then
    '    wvarCodErr = -164
    '    wvarMensaje = "Cod. de Localidad: existe una diferencia entre la Cotizacion y la Solicitud"
    'End If
    If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_COBROCOD).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_COBROCOD).Text Then
        wvarCodErr = -167
        wvarMensaje = "Cod. de Cobro: existe una diferencia entre la Cotizacion y la Solicitud"
    End If
    If Not wvarChequeoTarjeta Then
        wvarCodErr = -168
        wvarMensaje = "Tipo de cobro: existe una diferencia entre la Cotizacion y la Solicitud"
    End If
    If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_CLIENIVA).Text Then
        wvarCodErr = -169
        wvarMensaje = "Responsabilidad frente al IVA: existe una diferencia entre la Cotizacion y la Solicitud"
    End If
    If Not (wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZANN).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_POLIZANN).Text) Then
        wvarCodErr = -250
        wvarMensaje = "Poliza Anual: existe una diferencia entre la Cotizacion y la Solicitud"
    End If
    If Not (wobjXMLRequest.selectSingleNode("//" & mcteParam_POLIZSEC).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_POLIZSEC).Text) Then
        wvarCodErr = -251
        wvarMensaje = "Poliza Colectiva: existe una diferencia entre la Cotizacion y la Solicitud"
    End If
    If Not (wobjXMLRequest.selectSingleNode("//" & mcteParam_PLANNCOD).Text = wobjXMLCotizacion.selectSingleNode("//" & mcteParam_PLANNCOD).Text) Then
        wvarCodErr = -253
        wvarMensaje = "Cod. de Plan: existe una diferencia entre la Cotizacion y la Solicitud"
    End If
    
    
    pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
    fncValidaABase = False
    Exit Function
    
  End If
  '
  'Le agrego el Nodo de CERTISEC, y los encabezados para mandar el XML a cotizar
  wvarStep = 380
  Call wobjXMLRequest.selectSingleNode("//Request").appendChild(wobjXMLRequest.createNode(NODE_ELEMENT, "CERTISEC", ""))
  pvarRequest = "<LBA_WS res_code=""0"" res_msg="""">" & _
                wobjXMLRequest.xml & _
                "</LBA_WS>"
  Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
  wobjXMLResponse.async = False
  Call wobjXMLResponse.loadXML(pvarRequest)
  
  'Cotizo nuevamente
  wvarStep = 385
  pvarCot = wobjXMLCotizacion.xml
  
  Set wobjClass = mobjCOM_Context.CreateInstance("LBA_InterWSBrok.GetCotizacionATM")
  Call wobjClass.Execute(pvarCot, wvarMensajeStoreProc, "")

  Set wobjClass = Nothing
  '
  Set wobjXMLReturnVal = CreateObject("MSXML2.DOMDocument")
  wobjXMLReturnVal.async = False
  Call wobjXMLReturnVal.loadXML(wvarMensajeStoreProc)
   
   If Not wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_code").Text = 0 Then
     pvarRes = "<LBA_WS res_code=""-300"" res_msg=""" & wobjXMLReturnVal.selectSingleNode("//LBA_WS/@res_msg").Text & """></LBA_WS>"
   Exit Function
   Else
     wvarCodErr = "0"
     wvarMensaje = ""
   End If
   

  wvarStep = 420
  If Not (Val(wobjXMLRequest.selectSingleNode("//" & mcteParam_VALOR).Text) = Val(wobjXMLReturnVal.selectSingleNode("//PRECIO").Text)) Then
    wvarCodErr = -97
    wvarMensaje = "Precio: existe una diferencia entre la COTIZACION y la SOLICITUD"
    pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
    
  End If
  '
  wvarStep = 430
  Call wobjXMLResponse.selectSingleNode("//Request").appendChild(wobjXMLReturnVal.createNode(NODE_ELEMENT, "CODZONA", ""))
  wobjXMLResponse.selectSingleNode("//CODZONA").Text = "0"

  '
  wvarStep = 440
  Call wobjXMLResponse.selectSingleNode("//Request").appendChild(wobjXMLResponse.createNode(NODE_ELEMENT, "TIEMPOPROCESO", ""))
  wobjXMLResponse.selectSingleNode("//TIEMPOPROCESO").Text = "0"
  
  '
    '
  wvarStep = 440
  Call wobjXMLResponse.selectSingleNode("//Request").appendChild(wobjXMLResponse.createNode(NODE_ELEMENT, "USUARCOD", ""))
  wobjXMLResponse.selectSingleNode("//USUARCOD").Text = wobjXMLReturnVal.selectSingleNode("//USUARCOD").Text
  
  '
  pvarRes = wobjXMLResponse.xml
  
  fncValidaABase = True
  '
  wrstRes.Close
  wobjDBCnn.Close

fin:
'libero los objectos
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr

  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncValidaABase = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub


