Attribute VB_Name = "General"
Public Const mcteErrorInesperadoDescr As String = "Ocurrio un error inesperado en la ejecucion del componente"

Public Const mcteErrorInesperadoCod As String = "-50"

Public Const mcteDB As String = "lbawA_InterWSBrok.udl"

Public Const mcteIfFunctionFailed_failClass As Boolean = True
Public Const mcteArchivoAUSCOT_XML As String = "LBA_VALIDACION_COT_AU.xml"
Public Const mcteArchivoAUSSOL_XML As String = "LBA_VALIDACION_SOL_AU.xml"
Public Const mcteArchivoHOM_XML As String = "LBA_VALIDACION_COT_HO.XML"
Public Const mcteArchivoATM_XML As String = "LBA_VALIDACION_COT_ATM.XML"

Public Function fncTransformXSL(ByRef pvarMensaje As String, ByVal wvarXML_IN As String, ByVal pvarNodos As String, ByVal pvarNodo As String, ByRef wvarXML_OUT As String) As Boolean
Const wcteFnName        As String = "fncTransformXSL"
Dim wvarStep            As Long
On Error GoTo ErrorHandler

'declaracion de objetos
Dim wobjXML As MSXML2.DOMDocument
Dim wobjXSL As MSXML2.DOMDocument
Dim wobjXMLNode As MSXML2.IXMLDOMNode


'declaracion de variables

Dim wvarStrXSL As String

wvarStrXSL = "<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" version=""1.0"" xmlns:rs=""urn:schemas-microsoft-com:rowset"" xmlns:z=""#RowsetSchema"">"
wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
wvarStrXSL = wvarStrXSL & "  <xsl:element name='" & pvarNodos & "'>"
wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
wvarStrXSL = wvarStrXSL & "  </xsl:element>"
wvarStrXSL = wvarStrXSL & " </xsl:template>"

wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
wvarStrXSL = wvarStrXSL & "   <xsl:element name='" & pvarNodo & "'>"

Set wobjXML = CreateObject("MSXML2.DOMDocument")
wobjXML.async = False
Call wobjXML.loadXML(wvarXML_IN)

For Each wobjXMLNode In wobjXML.selectSingleNode("//z:row").Attributes
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='" & UCase(wobjXMLNode.nodeName) & "'><xsl:value-of select='@" & wobjXMLNode.nodeName & "'/></xsl:element>"
Next

wvarStrXSL = wvarStrXSL & "  </xsl:element>"
wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
wvarStrXSL = wvarStrXSL & " </xsl:template>"
wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"


Set wobjXSL = CreateObject("MSXML2.DOMDocument")
wobjXSL.async = False
Call wobjXSL.loadXML(wvarStrXSL)


wvarXML_OUT = Replace(wobjXML.transformNode(wobjXSL), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")

fncTransformXSL = True
fin:
'libero los objetos
Set wobjXML = Nothing
Set wobjXSL = Nothing
Set wobjXMLNode = Nothing

Exit Function
ErrorHandler:


    
pvarMensaje = mcteErrorInesperadoDescr

fncTransformXSL = False
Resume fin

End Function

Public Function fncTransformXSLADO(ByRef pvarMensaje As String, ByRef pobjRecordset As ADODB.Recordset, ByVal pvarNodos As String, ByVal pvarNodo As String, ByRef wvarXSL_OUT As String) As Boolean
Const wcteFnName        As String = "fncTransformXSLADO"
Dim wvarStep            As Long
On Error GoTo ErrorHandler

'declaracion de objetos
Dim wobjFieldAdo As ADODB.Field

'declaracion de variables

Dim wvarStrXSL As String

wvarStrXSL = "<xsl:stylesheet xmlns:xsl=""http://www.w3.org/1999/XSL/Transform"" version=""1.0"" xmlns:rs=""urn:schemas-microsoft-com:rowset"" xmlns:z=""#RowsetSchema"">"
wvarStrXSL = wvarStrXSL & " <xsl:template match='rs:data'>"
wvarStrXSL = wvarStrXSL & "  <xsl:element name='" & pvarNodos & "'>"
wvarStrXSL = wvarStrXSL & "   <xsl:apply-templates />"
wvarStrXSL = wvarStrXSL & "  </xsl:element>"
wvarStrXSL = wvarStrXSL & " </xsl:template>"

wvarStrXSL = wvarStrXSL & " <xsl:template match='z:row'>"
wvarStrXSL = wvarStrXSL & "   <xsl:element name='" & pvarNodo & "'>"

For Each wobjFieldAdo In pobjRecordset.Fields
    wvarStrXSL = wvarStrXSL & "     <xsl:element name='" & UCase(wobjFieldAdo.Name) & "' ><xsl:value-of select='@" & wobjFieldAdo.Name & "'/></xsl:element>"
Next

wvarStrXSL = wvarStrXSL & "  </xsl:element>"
wvarStrXSL = wvarStrXSL & "  <xsl:apply-templates />"
wvarStrXSL = wvarStrXSL & " </xsl:template>"

For Each wobjFieldAdo In pobjRecordset.Fields
  If wobjFieldAdo.Type = adChar Or _
      wobjFieldAdo.Type = adLongVarChar Or _
      wobjFieldAdo.Type = adLongVarWChar Or _
      wobjFieldAdo.Type = adVarChar Or _
      wobjFieldAdo.Type = adVarWChar Or _
      wobjFieldAdo.Type = adWChar Then
    wvarStrXSL = wvarStrXSL & "  <xsl:output cdata-section-elements='" & UCase(wobjFieldAdo.Name) & "'/>"
  End If
Next


wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"




wvarXSL_OUT = wvarStrXSL

fncTransformXSLADO = True
fin:
'libero los objetos

Exit Function
ErrorHandler:


    
pvarMensaje = mcteErrorInesperadoDescr

fncTransformXSLADO = False
Resume fin

End Function



