VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetValidacionCotATM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "LBA_InterWSBrok.GetValidacionCotATM"

'Archivo de Errores
Const mcteArchivoATM_XML            As String = "LBA_VALIDACION_COT_ATM.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_Provi                As String = "PROVCOD"
Const mcteParam_LocalidadCod         As String = "LOCALIDAD"
Const mcteParam_POLIZANN             As String = "POLIZANN"
Const mcteParam_POLIZSEC             As String = "POLIZSEC"
Const mcteParam_COBROCOD             As String = "FPAGO"
Const mcteParam_COBROTIP             As String = "COBROTIP"
Const mcteParam_TIPOHOGAR            As String = "TIPOHOGAR"
Const mcteParam_CLIENIVA             As String = "CLIENIVA"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'Declaracion de variables
    Dim wvarMensaje         As String
    Dim pvarRes             As String
    Dim wvarCodErr          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    pvarRes = ""
    wvarCodErr = ""
    wvarMensaje = ""
    If Not fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = pvarRes
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    '
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    'mobjCOM_Context.SetAbort
    mobjCOM_Context.SetComplete
End Function
Private Function fncGetAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName        As String = "fncGetAll"
  Dim wvarStep            As Long
  Dim wobjXMLRequest      As MSXML2.DOMDocument
  Dim wobjXMLCheck        As MSXML2.DOMDocument
  Dim wobjXMLList         As MSXML2.IXMLDOMNodeList
  Dim wobjXMLError        As MSXML2.IXMLDOMNode
  
  Dim wvarContChild       As Long
  Dim wvarContNode        As Long

  wvarStep = 10
'Chequeo que los datos esten OK
'Chequeo que no venga nada en blanco
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  Set wobjXMLCheck = CreateObject("MSXML2.DOMDocument")
  wobjXMLCheck.async = False
  Call wobjXMLCheck.Load(App.Path & "\" & mcteArchivoATM_XML)
  '
  wvarStep = 20
  Set wobjXMLList = wobjXMLCheck.selectNodes("//ERRORES/ATM/VACIOS/CAMPOS/ERROR")
  For wvarContChild = 0 To wobjXMLList.length - 1
    Set wobjXMLError = wobjXMLList(wvarContChild)
    'si existe el campo
    If Not wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text) Is Nothing Then
      'Campos opcionales dependientes
      If wobjXMLError.selectSingleNode("CAMPO").Text = "LOCALIDAD" And wobjXMLRequest.selectSingleNode("PROVCOD") Is Nothing Then
      ElseIf wobjXMLError.selectSingleNode("CAMPO").Text = "COBROTIP" And wobjXMLRequest.selectSingleNode("FPAGO") Is Nothing Then
      Else
         wvarStep = 30
         ' si el campo est� vac�o
         If Trim(wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text).Text) = "" Then
         wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
         wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
         pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
         Exit Function
         End If
      End If
    Else
      'Campos opcionales dependientes
      If wobjXMLError.selectSingleNode("CAMPO").Text = "LOCALIDAD" And wobjXMLRequest.selectSingleNode("PROVCOD") Is Nothing Then
      ElseIf wobjXMLError.selectSingleNode("CAMPO").Text = "COBROTIP" And wobjXMLRequest.selectSingleNode("FPAGO") Is Nothing Then
      Else
       'error
       wvarStep = 40
       wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
       wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
       Exit Function
     End If
    End If
    Set wobjXMLError = Nothing
  Next


  'Chequeo Tipo y Longitud de Datos para el XML Padre
  wvarStep = 50
  
  For wvarContChild = 0 To wobjXMLRequest.selectSingleNode("//Request").childNodes.length - 1
    If Not wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName) Is Nothing Then
        'Chequeo la longitud y si tiene Largo Fijo
        wvarStep = 60
        If wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONG_VARIABLE").Text = "N" Then
           wvarStep = 70
                If Not Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                     wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                     wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                     pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                     Exit Function
                End If
        Else
        wvarStep = 80
                If Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) > wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                     wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                     wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                     pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                     Exit Function
                End If
        End If
        
        'Chequeo el Tipo
        If wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TIPO").Text = "NUMERICO" Then
           wvarStep = 90
           If Not IsNumeric(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) Then
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
             Else
                'Chequeo que el dato numerico no sea 0
                'Excluye los nodos POLIZANN
                wvarStep = 100
                If wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text = 0 And _
                   Not (wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName = "POLIZANN") Then
                    wvarCodErr = -199
                    wvarMensaje = "El valor del dato numerico no puede estar en 0"
                    pvarRes = "<Response><Estado resultado=""false"" mensaje=""" & wvarMensaje & """ /></Response>"
                    Exit Function
                End If
           End If
        End If
    End If
  Next
  
   
'Chequeo los datos FIJOS
  wvarStep = 230
  If wobjXMLCheck.selectNodes("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/VALOR[.='" & wobjXMLRequest.selectSingleNode("//" & mcteParam_CLIENIVA).Text & "']").length = 0 Then
      'error
      wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/N_ERROR").Text
      wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/ATM/DATOS/CAMPOS/" & mcteParam_CLIENIVA & "/TEXTOERROR").Text
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If

  Set wobjXMLCheck = Nothing

   wvarStep = 280
   If fncValidaABase(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
     fncGetAll = True
   Else
     fncGetAll = False
   End If
  
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr

  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncGetAll = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function
Private Function fncValidaABase(ByVal pvarRequest As String, _
                                ByRef wvarMensaje As String, _
                                ByRef wvarCodErr As String, _
                                ByRef pvarRes As String) As Boolean

  Const wcteFnName        As String = "fncValidaABase"
  Dim wvarStep            As Long
  '
  Dim wobjClass           As HSBCInterfaces.IAction
  Dim wobjXMLRequest      As MSXML2.DOMDocument
  '
  On Error GoTo ErrorHandler
  '
  wvarStep = 10
  Set wobjClass = mobjCOM_Context.CreateInstance("LBA_InterWSBrok.GetNroCotATM")
  Call wobjClass.Execute(pvarRequest, pvarRes, "")
  '
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRes)
  '
  wvarStep = 20
  wvarCodErr = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text
  wvarMensaje = wobjXMLRequest.selectSingleNode("//LBA_WS/@res_msg").Text
  If Not wobjXMLRequest.selectSingleNode("//LBA_WS/@res_code").Text = 0 Then
        fncValidaABase = False
  Else
       fncValidaABase = True
  End If

fin:
  Set wobjClass = Nothing
  Set wobjXMLRequest = Nothing
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod

  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncValidaABase = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

