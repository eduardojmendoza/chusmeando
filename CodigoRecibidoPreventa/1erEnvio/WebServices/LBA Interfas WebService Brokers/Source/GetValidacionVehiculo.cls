VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "GetValidacionVehiculo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context         As ObjectContext
Private mobjEventLog            As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName             As String = "LBA_InterWSBrok.GetValidacionVehiculo"
Const mcteStoreProcSelect       As String = "SPSNCV_BRO_SELECT_OPER_ESP"
Const mcteStoreProcValidacion   As String = "SPSNCV_BRO_VALIDA_VEHICULO"

'Archivo de Errores
Const mcteArchivoVehiculo_XML        As String = "LBA_VALIDACION_VEHICULO.XML"

'Parametros XML de Entrada
Const mcteParam_REQUESTID            As String = "REQUESTID"
Const mcteParam_CODINST              As String = "CODINST"
Const mcteParam_Marca                As String = "MARCA"
Const mcteParam_Modelo               As String = "MODELO"
Const mcteParam_EfectAnn             As String = "EFECTANN"

Private Function IAction_Execute(ByVal pvarRequest As String, _
                                 ByRef pvarResponse As String, _
                                 ByVal pvarContextInfo As String) _
                 As Long
    '
    Const wcteFnName        As String = "IAction_Execute"
    Dim wvarStep            As Long
    '
    'Declaracion de variables
    Dim wvarMensaje         As String
    Dim pvarRes             As String
    Dim wvarCodErr          As String

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    pvarRes = ""
    wvarCodErr = ""
    wvarMensaje = ""
    If Not fncGetAll(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
            pvarResponse = pvarRes
        If mcteIfFunctionFailed_failClass Then
            wvarStep = 20
            IAction_Execute = 1
            mobjCOM_Context.SetAbort
        Else
            wvarStep = 30
            IAction_Execute = 0
            mobjCOM_Context.SetComplete
        End If
        Exit Function
    End If
    
    pvarResponse = pvarRes
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarResponse = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"

    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description, _
                     vbLogEventTypeError
    
    IAction_Execute = 1
    'mobjCOM_Context.SetAbort
    mobjCOM_Context.SetComplete
End Function
Private Function fncGetAll(ByVal pvarRequest As String, _
                           ByRef wvarMensaje As String, _
                           ByRef wvarCodErr As String, _
                           ByRef pvarRes As String) As Boolean

  Const wcteFnName         As String = "fncGetAll"
  Dim wvarStep             As Long
  
  Dim wobjClass            As HSBCInterfaces.IAction
  
  Dim wobjXMLRequest       As MSXML2.DOMDocument
  Dim wobjXMLCheck         As MSXML2.DOMDocument
  Dim wobjXMLParams        As MSXML2.DOMDocument
  Dim wobjXMLList          As MSXML2.IXMLDOMNodeList
  Dim wobjXMLError         As MSXML2.IXMLDOMNode
  Dim wobjXMLStatusTarjeta As MSXML2.DOMDocument
  
  Dim wvarContChild        As Long
  Dim wvarContNode         As Long
  
  Dim wvarTarjeta          As String
  Dim wvarStatusTarjeta    As String
  
  Dim wvarFechaActual      As Date
  Dim wvarFechaVigencia    As Date
  
  Dim mvarDif              As Integer
  

  wvarStep = 10
'Chequeo que los datos esten OK
'Chequeo que no venga nada en blanco
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)

  Set wobjXMLCheck = CreateObject("MSXML2.DOMDocument")
  wobjXMLCheck.async = False
  Call wobjXMLCheck.Load(App.Path & "\" & mcteArchivoVehiculo_XML)
  '
  wvarStep = 20
  
  Set wobjXMLList = wobjXMLCheck.selectNodes("//ERRORES/VEHICULO/VACIOS/CAMPOS/ERROR")
  For wvarContChild = 0 To wobjXMLList.length - 1
    Set wobjXMLError = wobjXMLList(wvarContChild)
    If Not wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text) Is Nothing Then
         wvarStep = 30
         If Trim(wobjXMLRequest.selectSingleNode("//" & wobjXMLError.selectSingleNode("CAMPO").Text).Text) = "" Then
         wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
         wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
         pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
         Exit Function
         End If
    Else
       'error
       wvarStep = 40
       wvarCodErr = wobjXMLError.selectSingleNode("VALORRESPUESTA").Text
       wvarMensaje = wobjXMLError.selectSingleNode("TEXTOERROR").Text
       pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
       Exit Function
    End If
    Set wobjXMLError = Nothing
  Next
  
  'Chequeo Tipo y Longitud de Datos para el XML Padre
  wvarStep = 50
  
  For wvarContChild = 0 To wobjXMLRequest.selectSingleNode("//Request").childNodes.length - 1
    If Not wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName) Is Nothing Then
        
        'Chequeo la longitud y si tiene Largo Fijo
        If wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONG_VARIABLE").Text = "N" Then
           wvarStep = 60
           If Not Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                wvarStep = 70
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        Else
           If Len(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) > wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/LONGITUD").Text Then
                wvarStep = 80
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
           End If
        End If
        
        'Chequeo el Tipo
        
        If wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TIPO").Text = "NUMERICO" Then
           If Not IsNumeric(wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text) Then
                wvarStep = 90
                wvarCodErr = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/N_ERROR").Text
                wvarMensaje = wobjXMLCheck.selectSingleNode("//ERRORES/VEHICULO/TIPODEDATO/" & wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).nodeName & "/TEXTOERROR").Text
                pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                Exit Function
             Else
                'Chequeo que el dato numerico no sea 0
                wvarStep = 100
                If wobjXMLRequest.selectSingleNode("//Request").childNodes(wvarContChild).Text = 0 Then
                    wvarMensaje = "El valor del dato numerico no puede estar en 0"
                    pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
                    Exit Function
                End If
           End If
        End If
        
   
    End If
Siguiente:
  Next
'
     'Valido el a�o del Veh�culo
    mvarDif = CInt(Year(Now)) - CInt(wobjXMLRequest.selectSingleNode("//" & mcteParam_EfectAnn).Text)
    
    If mvarDif > 20 Or mvarDif < 0 Then
            wvarMensaje = "Fecha del vehiculo invalida (0-20)"
            pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
            Exit Function
    End If
       
  Set wobjXMLCheck = Nothing
  Set wobjXMLList = Nothing
  '
  wvarStep = 110
  If fncValidaABase(pvarRequest, wvarMensaje, wvarCodErr, pvarRes) Then
     fncGetAll = True
  Else
     fncGetAll = False
  End If
  
fin:
  
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr
  
  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncGetAll = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function
Private Function fncValidaABase(ByVal pvarRequest As String, _
                                ByRef wvarMensaje As String, _
                                ByRef wvarCodErr As String, _
                                ByRef pvarRes As String) As Boolean

  Const wcteFnName        As String = "fncValidaABase"
  Dim wvarStep            As Long
  Dim wvarError           As Boolean
  '
  Dim wobjXMLRequest      As MSXML2.DOMDocument
  Dim wobjXMLCotizacion   As MSXML2.DOMDocument
  
  Dim wobjXMLResponse     As MSXML2.DOMDocument
  Dim wobjXMLReturnVal    As MSXML2.DOMDocument
  Dim wobjXML             As MSXML2.DOMDocument
  Dim wobjXMLError        As MSXML2.DOMDocument
  '
  Dim wobjClass           As HSBCInterfaces.IAction
  '
  Dim wobjHSBC_DBCnn      As HSBCInterfaces.IDBConnection
  Dim wobjDBCnn           As ADODB.Connection
  Dim wobjDBCmd           As ADODB.Command
  Dim wobjDBParm          As ADODB.Parameter
  Dim wrstRes             As ADODB.Recordset
  '
  Dim wvarContChild As Long
  Dim wvarContNode As Long
  '
  Dim mvarWDB_CODINST       As String
  Dim mvarWDB_REQUESTID     As String
  Dim mvarWDB_MARCA         As String
  Dim mvarWDB_MODELO        As String
  '
  Dim wvarMensajeStoreProc  As String
  Dim wvarMensajeNroCot     As String
  
  
  On Error GoTo ErrorHandler
  
  wvarStep = 10
  Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
  wobjXMLRequest.async = False
  Call wobjXMLRequest.loadXML(pvarRequest)
  '
  wvarStep = 20
  mvarWDB_REQUESTID = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID) Is Nothing Then
     mvarWDB_REQUESTID = wobjXMLRequest.selectSingleNode("//" & mcteParam_REQUESTID).Text
  End If
  '
  wvarStep = 30
  mvarWDB_CODINST = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST) Is Nothing Then
     mvarWDB_CODINST = wobjXMLRequest.selectSingleNode("//" & mcteParam_CODINST).Text
  End If
  '
  wvarStep = 40
  mvarWDB_MARCA = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Marca) Is Nothing Then
     mvarWDB_MARCA = wobjXMLRequest.selectSingleNode("//" & mcteParam_Marca).Text
  End If
  '
  wvarStep = 50
  mvarWDB_MODELO = ""
  If Not wobjXMLRequest.selectSingleNode("//" & mcteParam_Modelo) Is Nothing Then
     mvarWDB_MODELO = wobjXMLRequest.selectSingleNode("//" & mcteParam_Modelo).Text
  End If
  '
'Valido los campos de Solicitud
  '
  Set wobjHSBC_DBCnn = mobjCOM_Context.CreateInstance("HSBC.DBConnection")
  Set wobjDBCnn = wobjHSBC_DBCnn.GetDBConnection(mcteDB)
  Set wobjDBCmd = CreateObject("ADODB.Command")
  Set wobjDBCmd.ActiveConnection = wobjDBCnn
  wobjDBCmd.CommandText = mcteStoreProcValidacion
  wobjDBCmd.CommandType = adCmdStoredProc
  '
  wvarStep = 130
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_CODINST", adNumeric, adParamInput, , IIf(mvarWDB_CODINST = "", 0, mvarWDB_CODINST))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 4
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 140
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_NRO_OPERACION_BROKER", adNumeric, adParamInput, , IIf(mvarWDB_REQUESTID = "", 0, mvarWDB_REQUESTID))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 14
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 150
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_MARCA", adNumeric, adParamInput, , IIf(mvarWDB_MARCA = "", 0, mvarWDB_MARCA))
  wobjDBParm.NumericScale = 0
  wobjDBParm.Precision = 5
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
    '
  wvarStep = 160
  Set wobjDBParm = wobjDBCmd.CreateParameter("@WDB_USUARCOD", adChar, adParamInputOutput, 10, "")
  wobjDBCmd.Parameters.Append wobjDBParm
  Set wobjDBParm = Nothing
  '
  wvarStep = 230
  Set wrstRes = CreateObject("ADODB.Recordset")
  wrstRes.Open wobjDBCmd
  Set wrstRes.ActiveConnection = Nothing
  '
  Set wobjXML = CreateObject("MSXML2.DOMDocument")
  wobjXML.async = False
  Call wobjXML.Load(App.Path & "\" & mcteArchivoVehiculo_XML)
  '
  'SI NO HUBO ERROR CONTINUO
  wvarStep = 240
  If Not wrstRes.EOF Then
      If Not UCase(Trim(wrstRes.Fields(0).Value)) = "OK" Then
        'error
        If wobjXML.selectNodes("//ERRORES/VEHICULO/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']").length > 0 Then
          wvarMensaje = wobjXML.selectSingleNode("//ERRORES/VEHICULO/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']/TEXTOERROR").Text
          wvarCodErr = wobjXML.selectSingleNode("//ERRORES/VEHICULO/RESPUESTAS/ERROR[CODIGO='" & wrstRes.Fields(0).Value & "']/VALORRESPUESTA").Text
          pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        Else
          wvarCodErr = wrstRes.Fields(0).Value
          wvarMensaje = "Error de validacion en base de datos"
          pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
        End If
        '
        fncValidaABase = False
        Exit Function
      Else
      End If
  Else
      'error
      wvarCodErr = -60
      wvarMensaje = "No se retornaron datos de la base"
      pvarRes = "<LBA_WS res_code=""" & wvarCodErr & """ res_msg=""" & wvarMensaje & """></LBA_WS>"
      Exit Function
  End If
  '
  wrstRes.Close
  wobjDBCnn.Close
  
  Set wobjXML = Nothing
  Set wrstRes = Nothing
  Set wobjDBCmd = Nothing
  Set wobjDBCnn = Nothing
  Set wobjHSBC_DBCnn = Nothing
  '
  
  fncValidaABase = True
  '
  pvarRes = "<LBA_WS res_code=""0"" res_msg="""">" & _
                wobjXMLRequest.xml & _
                "</LBA_WS>"
fin:
  Exit Function

'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~

pvarRes = "<LBA_WS res_code=""" & mcteErrorInesperadoCod & """ res_msg=""" & mcteErrorInesperadoDescr & """></LBA_WS>"
wvarCodErr = mcteErrorInesperadoCod
wvarMensaje = mcteErrorInesperadoDescr

  mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                   mcteClassName, _
                   wcteFnName, _
                   wvarStep, _
                   Err.Number, _
                   "Error= [" & Err.Number & "] - " & Err.Description, _
                   vbLogEventTypeError
  
  fncValidaABase = False
  mobjCOM_Context.SetComplete
  GoTo fin
End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub




