Attribute VB_Name = "ModGeneral"
Option Explicit

' Parametros XML de Configuracion
Public Const gcteConfFileName       As String = "LBAVirtualMQConfig.xml"
Public Const gcteQueueManager       As String = "//QUEUEMANAGER"
Public Const gctePutQueue           As String = "//PUTQUEUE"
Public Const gcteGetQueue           As String = "//GETQUEUE"
Public Const gcteGMOWaitInterval    As String = "//GMO_WAITINTERVAL"
Public Const gcteClassMQConnection As String = "WD.Frame2MQ"

' Parametros XML del Cotizador
Public Const gcteParamFileName      As String = "ParametrosMQ.xml"
Public Const gcteNodosGenerales     As String = "//GENERALES"
Public Const gcteCIAASCOD           As String = "/CIAASCOD"
Public Const gcteNodosAutoScoring   As String = "//AUTOSCORING"
Public Const gcteRAMOPCOD           As String = "/RAMOPCOD"


