VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetKilometraje"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVGetKilometraje"
Const mcteOpID              As String = "0058"

'Parametros XML de Entrada
Const mcteParam_Ramopcod    As String = "//RAMOPCOD"
Const mcteParam_Campacod    As String = "//CAMPACOD"
Const mcteParam_Agentcod    As String = "//AGENTCOD"
Const mcteParam_AgentCla    As String = "//AGENTCLA"
Const mcteParam_Codizona    As String = "//CODIZONA"
Const mcteParam_Fecinici    As String = "//FECINICI"
Const mcteParam_Fecinvig    As String = "//FECINVIG"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String




    Dim wvarRamopcod        As String
    Dim wvarCampacod        As String
    Dim wvarAgentcod        As String
    Dim wvarAgentCla        As String
    Dim wvarCodiZona        As String
    Dim wvarFecInici        As String
    Dim wvarFecInVig        As String
    '
    Dim strParseString      As String
    Dim wvarError           As String
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deberá venir desde la página
        wvarCampacod = Left(.selectSingleNode(mcteParam_Campacod).Text & Space(4), 4)
        wvarAgentcod = Right(String(4, "0") & .selectSingleNode(mcteParam_Agentcod).Text, 4)
        wvarAgentCla = Left(.selectSingleNode(mcteParam_AgentCla).Text & Space(2), 2)
        wvarCodiZona = Right(String(4, "0") & .selectSingleNode(mcteParam_Codizona).Text, 4)
        wvarFecInici = Right(String(8, "0") & .selectSingleNode(mcteParam_Fecinici).Text, 8)
        wvarFecInVig = Right(String(8, "0") & .selectSingleNode(mcteParam_Fecinvig).Text, 8)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuración
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los parámetros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarRamopcod = .selectSingleNode(gcteNodosAutoScoring & gcteRAMOPCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarRamopcod & wvarCampacod & wvarAgentcod & wvarAgentCla & wvarCodiZona & wvarFecInici & wvarFecInVig
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarStep = 180
    
    If Trim(strParseString) <> "" Then
        wvarStep = 190
        '
        wvarResult = ParseoMensaje(strParseString, 35)
        '
        wvarStep = 200
        Response = "<Response><Estado resultado=" & Chr(34) & "true" & Chr(34) & " mensaje=" & Chr(34) & Chr(34) & " />" & wvarResult & "</Response>"
    
    Else
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "NO SE ENCONTRARON DATOS." & Chr(34) & " /></Response>"
    End If
    '
    wvarStep = 220
    '
    wvarStep = 230
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarRamopcod & wvarCampacod & wvarAgentcod & wvarAgentCla & wvarCodiZona & wvarFecInici & wvarFecInVig & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
    
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function ParseoMensaje(ByVal strParseString As String, ByVal plPos As Long) As String

Dim wvarResult As String
Dim i As Integer
Dim wvarDesde As String
Dim wvarHasta As String
    
    '
    If Trim(strParseString) <> "" Then
        For i = 1 To 50
            wvarDesde = Right(Space(7) & CStr(CLng("0" & Mid(strParseString, plPos, 7))), 7)
            If Trim(wvarDesde) <> "0" Then
                wvarHasta = Right(Space(7) & CStr(CLng("0" & Mid(strParseString, plPos + 7, 7))), 7)
                wvarResult = wvarResult & "<OPTION value='" & Trim(wvarHasta) & "' >" & wvarDesde & " - " & wvarHasta & "</OPTION>"
            End If
            plPos = plPos + 14
        Next
        
    End If

    '
    ParseoMensaje = wvarResult
    '
 
End Function

