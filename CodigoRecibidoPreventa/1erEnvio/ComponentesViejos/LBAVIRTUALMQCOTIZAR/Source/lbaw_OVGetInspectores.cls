VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_OVGetInspectores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements ObjectControl
Implements HSBCInterfaces.IAction

'Datos de la accion
Const mcteClassName         As String = "lbawA_OVMQCotizar.lbaw_OVGetInspectores"
Const mcteOpID              As String = "1513"

'Parametros XML de Entrada
Const mcteParam_Usuario         As String = "//USUARIO"
Const mcteParam_CentroCod       As String = "//CENTROCOD"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName        As String = "IAction_Execute"
    '
    Dim wobjXMLRequest      As MSXML2.DOMDocument
    Dim wobjXMLConfig       As MSXML2.DOMDocument
    Dim wobjXMLParametros   As MSXML2.DOMDocument
    Dim wobjXMLResponse     As MSXML2.DOMDocument
    Dim wobjXSLResponse     As MSXML2.DOMDocument
    '
    Dim wvarMQError         As Long
    Dim wvarArea            As String
    Dim wobjFrame2MQ        As HSBCInterfaces.IAction
    '
    Dim wvarStep            As Long
    Dim wvarResult          As String
    Dim wvarCiaAsCod        As String
    Dim wvarUsuario         As String
    Dim wvarCentroCod        As String
   '
    Dim strParseString      As String
    Dim wvarError           As String
    Dim wvarPos             As Long
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los par�metros que llegan desde la p�gina
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        'Deber� venir desde la p�gina
        wvarUsuario = Left(.selectSingleNode(mcteParam_Usuario).Text & Space(10), 10)
        'wvarCentroCod = Left(.selectSingleNode(mcteParam_CentroCod).Text & Space(4), 4)
        wvarCentroCod = Right(Space(4) & .selectSingleNode(mcteParam_CentroCod).Text, 4)
        '
    End With
    '
    wvarStep = 20
    Set wobjXMLRequest = Nothing
    '
    wvarStep = 30
    'Levanto los datos de la cola de MQ del archivo de configuraci�n
    Set wobjXMLConfig = CreateObject("MSXML2.DOMDocument")
    wobjXMLConfig.async = False
    wobjXMLConfig.Load App.Path & "\" & gcteConfFileName
    '
    'Levanto los par�metros generales (ParametrosMQ.xml)
    wvarStep = 60
    Set wobjXMLParametros = CreateObject("MSXML2.DOMDocument")
    With wobjXMLParametros
        .async = False
        Call .Load(App.Path & "\" & gcteParamFileName)
        wvarCiaAsCod = .selectSingleNode(gcteNodosGenerales & gcteCIAASCOD).Text
    End With
    '
    wvarStep = 70
    Set wobjXMLParametros = Nothing
    '
    wvarStep = 80
    wvarArea = mcteOpID & wvarCiaAsCod & wvarUsuario & "    000000000    000000000  000000000  000000000" & wvarCentroCod
    wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text = Val(wobjXMLConfig.selectSingleNode("//MQCONFIG/GMO_WAITINTERVAL").Text) * 40
    Set wobjFrame2MQ = mobjCOM_Context.CreateInstance(gcteClassMQConnection)
    wvarMQError = wobjFrame2MQ.Execute(wvarArea, strParseString, wobjXMLConfig.selectSingleNode("//MQCONFIG").xml)
    Set wobjFrame2MQ = Nothing
    '
    wvarStep = 150
    If wvarMQError <> 0 Then
        Response = "<Response><Estado resultado=" & Chr(34) & "false" & Chr(34) & " mensaje=" & Chr(34) & "El servicio de consulta no se encuentra disponible" & Chr(34) & " />" & "Codigo Error:" & wvarMQError & "</Response>"
        mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                mcteClassName & "--" & mcteClassName, _
                wcteFnName, _
                wvarStep, _
                Err.Number, _
                "Error= [" & wvarMQError & "] - " & strParseString & " Area:" & wvarArea & " Hora:" & Now(), _
                vbLogEventTypeError
        GoTo ClearObjects:
    End If
    '
    '
    wvarPos = 71 'cantidad de caracteres ocupados por par�metros de entrada
    '
    wvarStep = 190
    If Mid(strParseString, 19, 2) = "ER" Then
        '
        wvarStep = 200
        Response = "<Response><Estado resultado='false' mensaje='SE HA PRODUCIDO UN ERROR INESPERADO EN LA EJECUCION DE LA CONSULTA' /></Response>"
        '
    Else
        '
        wvarResult = ""
        wvarResult = wvarResult & ParseoMensaje(wvarPos, strParseString)
        '
        wvarStep = 210
        Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
        Set wobjXSLResponse = CreateObject("MSXML2.DOMDocument")
        '
        wvarStep = 220
        wobjXMLResponse.async = False
        wobjXMLResponse.loadXML (wvarResult)
        '
        If wobjXMLResponse.selectNodes("//R").length <> 0 Then
            wvarStep = 230
            wobjXSLResponse.async = False
            Call wobjXSLResponse.loadXML(p_GetXSL())
            '
            wvarStep = 240
            wvarResult = Replace(wobjXMLResponse.transformNode(wobjXSLResponse), "<?xml version=""1.0"" encoding=""UTF-16""?>", "")
            '
            wvarStep = 250
            Set wobjXMLResponse = Nothing
            Set wobjXSLResponse = Nothing
            '
            wvarStep = 260
            Response = "<Response><Estado resultado='true' mensaje='' />" & wvarResult & "</Response>"
        
        Else
            wvarStep = 265
            Response = "<Response><Estado resultado='false' mensaje='No se encontraron inspectores para el centro de inspecci�n seleccionado.' /></Response>"
        End If
        '
    End If
    '
    wvarStep = 270
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    '
'~~~~~~~~~~~~~~~
ClearObjects:
'~~~~~~~~~~~~~~~
    ' LIBERO LOS OBJETOS
    Set wobjXMLConfig = Nothing
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     wcteFnName, _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Mensaje:" & mcteOpID & wvarCiaAsCod & wvarUsuario & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1
    mobjCOM_Context.SetAbort
    Resume ClearObjects
End Function

Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub

Private Function p_GetXSL() As String
    Dim wvarStrXSL  As String
    '
    wvarStrXSL = wvarStrXSL & "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>"
    wvarStrXSL = wvarStrXSL & "<xsl:decimal-format name=""european"" decimal-separator="","" grouping-separator="".""/>"
    wvarStrXSL = wvarStrXSL & "     <xsl:template match='/RS/R'>"
    wvarStrXSL = wvarStrXSL & "         <xsl:element name='OPTION'>"
    wvarStrXSL = wvarStrXSL & "             <xsl:attribute name='value'>"
    wvarStrXSL = wvarStrXSL & "                 <xsl:value-of select='normalize-space(substring(.,1,4))' />"
    wvarStrXSL = wvarStrXSL & "             </xsl:attribute>"
    wvarStrXSL = wvarStrXSL & "             <xsl:value-of select='normalize-space(substring(.,5,30))' />"
    wvarStrXSL = wvarStrXSL & "         </xsl:element>"
    wvarStrXSL = wvarStrXSL & "     </xsl:template>"
    wvarStrXSL = wvarStrXSL & "</xsl:stylesheet>"
    '
    p_GetXSL = wvarStrXSL
End Function
Private Function ParseoMensaje(wvarPos As Long, strParseString As String) As String
Dim wvarResult As String
Dim wvarCantLin As Integer
Dim wvarCounter As Integer
    '
    wvarResult = wvarResult & "<RS>"
    '
    If Trim(Mid(strParseString, wvarPos, 3)) <> "" And Trim(Mid(strParseString, wvarPos, 3)) <> "000" Then
        wvarCantLin = Mid(strParseString, wvarPos, 3)
        wvarPos = wvarPos + 3
        '
        For wvarCounter = 0 To wvarCantLin - 1
            wvarResult = wvarResult & "<R><![CDATA[" & Mid(strParseString, wvarPos, 34) & "]]></R>"
            wvarPos = wvarPos + 34
        Next
    '
    End If
    wvarResult = wvarResult & "</RS>"
    '
    ParseoMensaje = wvarResult
    '
End Function








