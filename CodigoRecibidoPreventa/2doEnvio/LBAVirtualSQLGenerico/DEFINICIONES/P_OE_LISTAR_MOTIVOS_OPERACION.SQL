/*
---------------------------------------------------------------------------------------------
COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2007. ALL RIGHTS RESERVED 
This software is only to be used for the purpose for which it has been provided. No part of it is to be 
reproduced, disassembled, transmitted, stored in a retrieval system or translated in any human or computer 
language in any way or for any other purposes whatsoever without the prior written consent of the Hong Kong 
and Shanghai Banking Corporation Limited. Infringement of copyright is a serious civil and criminal offence, 
which can result in heavy fines and payment of substantial damages. 

Nombre del Programador:		Fernando Osores
Nombre del Store:		P_OE_LISTAR_MOTIVOS_OPERACION
Fecha de Creaci�n:		01/11/2007
Descripci�n:			Listado de Productos Habilitados para cargar OEs
---------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE P_OE_LISTAR_MOTIVOS_OPERACION @OPERACION INT, @ENDOSO INT = -1, @MOSTRAR INT = 1 AS

SET NOCOUNT ON

IF @ENDOSO = -1 
begin
	SELECT IDMOTIVOOPERACION, IDTIPOENDOSO, DESCRIPCION AS MOTIVO FROM OE_MOTIVO_OPERACION
	WHERE IDTIPOOPERACION=@OPERACION AND MOSTRAR=@MOSTRAR
	ORDER BY DESCRIPCION
end
else
begin
	SELECT IDMOTIVOOPERACION, IDTIPOENDOSO, DESCRIPCION AS MOTIVO FROM OE_MOTIVO_OPERACION
	WHERE IDTIPOENDOSO=@ENDOSO AND IDTIPOOPERACION=@OPERACION AND MOSTRAR=@MOSTRAR
	ORDER BY DESCRIPCION
end

GO

GRANT EXEC ON P_OE_LISTAR_MOTIVOS_OPERACION TO SIS_SNCV1

