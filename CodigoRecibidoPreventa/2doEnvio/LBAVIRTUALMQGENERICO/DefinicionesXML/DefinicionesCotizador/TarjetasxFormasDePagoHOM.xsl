<?xml version="1.0" encoding="UTF-8"?>
	<!--
 Fecha de Modificación: 17/02/2009
 Ticket numero:352070
 Desarrollador: Marroni Juan Pablo
 Descripción: Correcciones por error en la seleccion del tipo de Tarjeta cuando es distinto de Visa

 COPYRIGHT. THE HONGKONG AND SHANGHAI BANKING CORPORATION LIMITED 2009. 
 ALL RIGHTS RESERVED
 
 This software is only to be used for the purpose for which it has been provided.
 No part of it is to be reproduced, disassembled, transmitted, stored in a 
 retrieval system or translated in any human or computer language in any way or
 for any other purposes whatsoever without the prior written consent of the Hong
 Kong and Shanghai Banking Corporation Limited. Infringement of copyright is a 
 serious civil and criminal offence, which can result in heavy fines and payment 
 of substantial damages.
 
 Nombre del Fuente: TarjetasxFormasDePagoHOM.xsl
 
 Fecha de Creación: 17/02/2009
 
 PPcR: Desconocido
 
 Desarrollador: Desconocido' 
 Descripción: Desconocido' 

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="/">
		<xsl:for-each select="//FORMADEPAGO">
			<xsl:sort select="./COBRODES"/>
			<OPTION>
				<xsl:attribute name="value"><xsl:value-of select="TARJECOD"/></xsl:attribute>
				<xsl:attribute name="cobrocod"><xsl:value-of select="COBROCOD"/></xsl:attribute>
				<xsl:attribute name="cobrotip"><xsl:value-of select="COBROTIP"/></xsl:attribute>
				<xsl:attribute name="cobrodab"><xsl:value-of select="COBRODAB"/></xsl:attribute>
				<xsl:attribute name="optpgm1"><xsl:value-of select="OPTPGM1"/></xsl:attribute>
				<xsl:value-of select="COBRODES"/>
			</OPTION>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
