<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<xsl:for-each select="//CAMPANA">
			<xsl:sort select="./CAMPADES"/>
			<option>
				<xsl:attribute name="value"><xsl:value-of select="CAMPACOD"/></xsl:attribute>
				<xsl:attribute name="POLIZANN"><xsl:value-of select="POLIZANN"/></xsl:attribute>
				<xsl:attribute name="POLIZSEC"><xsl:value-of select="POLIZSEC"/></xsl:attribute>
				<xsl:attribute name="agentcod"><xsl:value-of select="//AGENTCOD"/></xsl:attribute>
				<xsl:attribute name="agentdab"></xsl:attribute>
				<xsl:attribute name="empleado"></xsl:attribute>
				<xsl:attribute name="bancocod"></xsl:attribute>
				<xsl:attribute name="sucurcod"></xsl:attribute>
				<xsl:value-of select="CAMPADES"/>
			</option>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>