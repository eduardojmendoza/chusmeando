VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "lbaw_GetBinPDF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Objetos del FrameWork
Private mobjCOM_Context     As ObjectContext
Private mobjEventLog        As HSBCInterfaces.IEventLog

'Implementacion de los objetos
Implements HSBCInterfaces.IAction
Implements ObjectControl

'Datos de la accion
Const mcteClassName         As String = "lbawA_MWGenerico.lbaw_GetBinPDF"

'Parametros XML de Entrada
Const mcteParam_FileName            As String = "//RUTA"
Const mcteParam_MantenerArchivo     As String = "//MANTENERARCHIVO"
Const mcteParam_SoloBuscarArchivo   As String = "//SOLOBUSCARARCHIVO"

Private Function IAction_Execute(ByVal Request As String, Response As String, ByVal ContextInfo As String) As Long
    Const wcteFnName            As String = "IAction_Execute"
    Dim wvarStep                As Long
    Dim wvarResult              As String
    Dim wvarMarcaMantenerArch   As Boolean
    '
    Dim wobjXMLRequest          As MSXML2.DOMDocument
    Dim wobjXMLResponse         As MSXML2.DOMDocument
    '
    Dim wobjEle                 As IXMLDOMElement
    Dim wobjXMLServer           As MSXML2.DOMDocument
    '
    Dim wvarFileGet             As Integer
    Dim wvarFileName            As String
    Dim wvarArrBytes()          As Byte
    Dim wvarCantidadIntentos    As Long
    Dim wvarCurrIntento         As Long
    '
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    On Error GoTo ErrorHandler
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '
    wvarStep = 10
    'Levanto los parámetros que llegan desde la página
    Set wobjXMLRequest = CreateObject("MSXML2.DOMDocument")
    Set wobjXMLResponse = CreateObject("MSXML2.DOMDocument")
    
    With wobjXMLRequest
        .async = False
        Call .loadXML(Request)
        wvarFileName = .selectSingleNode(mcteParam_FileName).Text
        wvarMarcaMantenerArch = Not .selectSingleNode(mcteParam_MantenerArchivo) Is Nothing
    End With
    '
    wvarStep = 20
    'Set wobjXMLServer = CreateObject("MSXML2.DOMDocument")
    'wobjXMLServer.Load App.Path & "\LBAVirtualServerImpresion.xml"
    'wvarCantidadIntentos = Val(wobjXMLServer.selectSingleNode("//TIMEOUT").Text) * 1000 / 250
    wvarCantidadIntentos = 240 '60 * 1000 / 250
    'Set wobjXMLServer = Nothing
    '
    wvarStep = 21
    '
    'Verifico que exista el archivo que se solicita por parámetro
    For wvarCurrIntento = 1 To wvarCantidadIntentos
        '
        If Dir(wvarFileName) <> "" Then Exit For
        'Sleep (250) 'Espero de a 250ms para ver si lo termina de generar
        wvarCurrIntento = wvarCurrIntento + 1
        '
    Next wvarCurrIntento
    '
    wvarStep = 22
    If Dir(wvarFileName) = "" Then
        
        Response = "<Response><Estado resultado='false' mensaje='El Documento solicitado no se encuentra disponible en este momento.' /></Response>"
    
    Else
        
        Response = "<Response><Estado resultado='true' mensaje='' /></Response>"
        If wobjXMLRequest.selectSingleNode(mcteParam_SoloBuscarArchivo) Is Nothing Then
        
            wvarStep = 30

            wobjXMLResponse.loadXML Response
            
            wobjXMLResponse.resolveExternals = True
        
            Set wobjEle = wobjXMLResponse.createElement("BinData")
        
            wobjXMLResponse.selectSingleNode("//Response").appendChild wobjEle
        
            wobjEle.setAttribute "xmlns:dt", "urn:schemas-microsoft-com:datatypes"
            wobjEle.dataType = "bin.base64"
            
            wvarStep = 40
            
            'Leo el binario y lo guardo en wvarArrBytes
            wvarFileGet = FreeFile()
            Open wvarFileName For Binary Access Read As wvarFileGet
            
            wvarStep = 50
            
            ReDim wvarArrBytes(FileLen(wvarFileName) - 1)
            Get wvarFileGet, , wvarArrBytes
            Close wvarFileGet
            
            'Cargo el Array en el XML
            wobjEle.nodeTypedValue = wvarArrBytes
            Response = wobjXMLResponse.xml
            '
            wvarStep = 60
            '
            If Not wvarMarcaMantenerArch Then Kill wvarFileName
            '
            wvarStep = 65
        End If
        '
    End If
    Set wobjEle = Nothing
    Set wobjXMLRequest = Nothing
    Set wobjXMLResponse = Nothing
    '
    wvarStep = 100
    mobjCOM_Context.SetComplete
    IAction_Execute = 0
    
Exit Function
'
'~~~~~~~~~~~~~~~
ErrorHandler:
'~~~~~~~~~~~~~~~
    '
    mobjEventLog.Log EventLog_Category.evtLog_Category_Unexpected, _
                     mcteClassName, _
                     "", _
                     wvarStep, _
                     Err.Number, _
                     "Error= [" & Err.Number & "] - " & Err.Description & " Hora:" & Now(), _
                     vbLogEventTypeError
    IAction_Execute = 1

End Function

'-------------------------------------------------------------------------------------------------------------------
'// Metodos del Framework
'-------------------------------------------------------------------------------------------------------------------
Private Sub ObjectControl_Activate()
   '
   Set mobjCOM_Context = GetObjectContext()
   Set mobjEventLog = CreateObject("HSBC.EventLog")
   '
End Sub

Private Function ObjectControl_CanBePooled() As Boolean
   '
   ObjectControl_CanBePooled = True
   '
End Function

Private Sub ObjectControl_Deactivate()
   '
   Set mobjCOM_Context = Nothing
   Set mobjEventLog = Nothing
   '
End Sub
